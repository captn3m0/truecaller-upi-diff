package com.mopub.common.privacy;

import android.content.Context;
import com.mopub.common.BaseUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.Preconditions;

public class SyncUrlGenerator
  extends BaseUrlGenerator
{
  private final Context a;
  private String b;
  private String c;
  private String d;
  private String e;
  private final String g;
  private String h;
  private String i;
  private String j;
  private String k;
  private String l;
  private Boolean m;
  private boolean n;
  private Boolean o;
  
  public SyncUrlGenerator(Context paramContext, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramString);
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    g = paramString;
  }
  
  public String generateUrlString(String paramString)
  {
    a(paramString, "/m/gdpr_sync");
    Object localObject = b;
    b("id", (String)localObject);
    b("nv", "5.4.1");
    localObject = d;
    b("last_changed_ms", (String)localObject);
    localObject = e;
    b("last_consent_status", (String)localObject);
    localObject = g;
    b("current_consent_status", (String)localObject);
    localObject = h;
    b("consent_change_reason", (String)localObject);
    localObject = i;
    b("consented_vendor_list_version", (String)localObject);
    localObject = j;
    b("consented_privacy_policy_version", (String)localObject);
    localObject = k;
    b("cached_vendor_list_iab_hash", (String)localObject);
    localObject = l;
    b("extras", (String)localObject);
    localObject = c;
    b("udid", (String)localObject);
    localObject = m;
    a("gdpr_applies", (Boolean)localObject);
    localObject = Boolean.valueOf(n);
    a("force_gdpr_applies", (Boolean)localObject);
    localObject = o;
    a("forced_gdpr_applies_changed", (Boolean)localObject);
    localObject = ClientMetadata.getInstance(a).getAppPackageName();
    b("bundle", (String)localObject);
    b("dnt", "mp_tmpl_do_not_track");
    return f.toString();
  }
  
  public SyncUrlGenerator withAdUnitId(String paramString)
  {
    b = paramString;
    return this;
  }
  
  public SyncUrlGenerator withCachedVendorListIabHash(String paramString)
  {
    k = paramString;
    return this;
  }
  
  public SyncUrlGenerator withConsentChangeReason(String paramString)
  {
    h = paramString;
    return this;
  }
  
  public SyncUrlGenerator withConsentedPrivacyPolicyVersion(String paramString)
  {
    j = paramString;
    return this;
  }
  
  public SyncUrlGenerator withConsentedVendorListVersion(String paramString)
  {
    i = paramString;
    return this;
  }
  
  public SyncUrlGenerator withExtras(String paramString)
  {
    l = paramString;
    return this;
  }
  
  public SyncUrlGenerator withForceGdprApplies(boolean paramBoolean)
  {
    n = paramBoolean;
    return this;
  }
  
  public SyncUrlGenerator withForceGdprAppliesChanged(Boolean paramBoolean)
  {
    o = paramBoolean;
    return this;
  }
  
  public SyncUrlGenerator withGdprApplies(Boolean paramBoolean)
  {
    m = paramBoolean;
    return this;
  }
  
  public SyncUrlGenerator withLastChangedMs(String paramString)
  {
    d = paramString;
    return this;
  }
  
  public SyncUrlGenerator withLastConsentStatus(ConsentStatus paramConsentStatus)
  {
    if (paramConsentStatus == null) {
      paramConsentStatus = null;
    } else {
      paramConsentStatus = paramConsentStatus.getValue();
    }
    e = paramConsentStatus;
    return this;
  }
  
  public SyncUrlGenerator withUdid(String paramString)
  {
    c = paramString;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.SyncUrlGenerator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */