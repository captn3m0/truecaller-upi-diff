package com.mopub.common.privacy;

import android.content.Context;
import com.mopub.common.BaseUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.Preconditions;

public class ConsentDialogUrlGenerator
  extends BaseUrlGenerator
{
  Boolean a;
  boolean b;
  String c;
  String d;
  private final Context e;
  private final String g;
  private final String h;
  
  ConsentDialogUrlGenerator(Context paramContext, String paramString1, String paramString2)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramString1);
    Preconditions.checkNotNull(paramString2);
    paramContext = paramContext.getApplicationContext();
    e = paramContext;
    g = paramString1;
    h = paramString2;
  }
  
  public String generateUrlString(String paramString)
  {
    a(paramString, "/m/gdpr_consent_dialog");
    Object localObject = g;
    b("id", (String)localObject);
    localObject = h;
    b("current_consent_status", (String)localObject);
    b("nv", "5.4.1");
    localObject = ClientMetadata.getCurrentLanguage(e);
    b("language", (String)localObject);
    localObject = a;
    a("gdpr_applies", (Boolean)localObject);
    localObject = Boolean.valueOf(b);
    a("force_gdpr_applies", (Boolean)localObject);
    localObject = c;
    b("consented_vendor_list_version", (String)localObject);
    localObject = d;
    b("consented_privacy_policy_version", (String)localObject);
    localObject = ClientMetadata.getInstance(e).getAppPackageName();
    b("bundle", (String)localObject);
    return f.toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.ConsentDialogUrlGenerator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */