package com.mopub.common.privacy;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;

final class a$2
  extends WebViewClient
{
  a$2(a parama) {}
  
  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    a.b localb = a.b(a);
    if (localb != null)
    {
      localb = a.b(a);
      int i = a.a;
      localb.onLoadProgress(i);
    }
    super.onPageFinished(paramWebView, paramString);
  }
  
  public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    super.onPageStarted(paramWebView, paramString, paramBitmap);
    paramWebView = a.b(a);
    if (paramWebView != null)
    {
      paramWebView = a.b(a);
      paramString = null;
      paramWebView.onLoadProgress(0);
    }
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    Object localObject = "mopub://consent?yes";
    boolean bool1 = ((String)localObject).equals(paramString);
    boolean bool2 = true;
    if (bool1)
    {
      paramWebView = a.a(a);
      if (paramWebView != null)
      {
        paramWebView = a.a(a);
        paramString = ConsentStatus.EXPLICIT_YES;
        paramWebView.onConsentClick(paramString);
      }
      return bool2;
    }
    localObject = "mopub://consent?no";
    bool1 = ((String)localObject).equals(paramString);
    if (bool1)
    {
      paramWebView = a.a(a);
      if (paramWebView != null)
      {
        paramWebView = a.a(a);
        paramString = ConsentStatus.EXPLICIT_NO;
        paramWebView.onConsentClick(paramString);
      }
      return bool2;
    }
    localObject = "mopub://close";
    bool1 = ((String)localObject).equals(paramString);
    if (bool1)
    {
      paramWebView = a.a(a);
      if (paramWebView != null)
      {
        paramWebView = a.a(a);
        paramWebView.onCloseClick();
      }
      return bool2;
    }
    bool1 = TextUtils.isEmpty(paramString);
    if (!bool1) {
      try
      {
        localObject = a;
        localObject = ((a)localObject).getContext();
        Uri localUri = Uri.parse(paramString);
        String str1 = "Cannot open native browser for ";
        String str2 = String.valueOf(paramString);
        str1 = str1.concat(str2);
        Intents.launchActionViewIntent((Context)localObject, localUri, str1);
        return bool2;
      }
      catch (IntentNotResolvableException localIntentNotResolvableException)
      {
        localObject = localIntentNotResolvableException.getMessage();
        MoPubLog.e((String)localObject);
      }
    }
    return super.shouldOverrideUrlLoading(paramWebView, paramString);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.a.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */