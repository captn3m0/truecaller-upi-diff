package com.mopub.common.privacy;

import com.mopub.common.Preconditions;

final class b
{
  private final String a;
  
  b(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    a = paramString;
  }
  
  public final String getHtml()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */