package com.mopub.common.privacy;

import android.text.TextUtils;
import com.mopub.common.Preconditions;
import java.io.Serializable;
import java.util.Calendar;
import java.util.UUID;

public class AdvertisingId
  implements Serializable
{
  final Calendar a;
  final String b;
  final String c;
  final boolean d;
  
  AdvertisingId(String paramString1, String paramString2, boolean paramBoolean, long paramLong)
  {
    Preconditions.checkNotNull(paramString1);
    Preconditions.checkNotNull(paramString1);
    b = paramString1;
    c = paramString2;
    d = paramBoolean;
    paramString1 = Calendar.getInstance();
    a = paramString1;
    a.setTimeInMillis(paramLong);
  }
  
  static AdvertisingId b()
  {
    Calendar localCalendar = Calendar.getInstance();
    String str = UUID.randomUUID().toString();
    AdvertisingId localAdvertisingId = new com/mopub/common/privacy/AdvertisingId;
    long l = localCalendar.getTimeInMillis() - 86400000L - 1L;
    localAdvertisingId.<init>("", str, false, l);
    return localAdvertisingId;
  }
  
  static AdvertisingId c()
  {
    Calendar localCalendar = Calendar.getInstance();
    String str = UUID.randomUUID().toString();
    AdvertisingId localAdvertisingId = new com/mopub/common/privacy/AdvertisingId;
    long l = localCalendar.getTimeInMillis();
    localAdvertisingId.<init>("", str, false, l);
    return localAdvertisingId;
  }
  
  static String d()
  {
    return UUID.randomUUID().toString();
  }
  
  final String a()
  {
    Object localObject = b;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (bool) {
      return "";
    }
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("ifa:");
    String str = b;
    ((StringBuilder)localObject).append(str);
    return ((StringBuilder)localObject).toString();
  }
  
  final boolean e()
  {
    Calendar localCalendar1 = Calendar.getInstance();
    long l1 = localCalendar1.getTimeInMillis();
    Calendar localCalendar2 = a;
    long l2 = localCalendar2.getTimeInMillis();
    l1 -= l2;
    l2 = 86400000L;
    boolean bool = l1 < l2;
    return !bool;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    boolean bool1 = paramObject instanceof AdvertisingId;
    if (!bool1) {
      return false;
    }
    paramObject = (AdvertisingId)paramObject;
    bool1 = d;
    boolean bool2 = d;
    if (bool1 != bool2) {
      return false;
    }
    String str1 = b;
    String str2 = b;
    bool1 = str1.equals(str2);
    if (!bool1) {
      return false;
    }
    str1 = c;
    paramObject = c;
    return str1.equals(paramObject);
  }
  
  public String getIdWithPrefix(boolean paramBoolean)
  {
    boolean bool = d;
    if ((!bool) && (paramBoolean))
    {
      localObject = b;
      paramBoolean = ((String)localObject).isEmpty();
      if (!paramBoolean)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("ifa:");
        str = b;
        ((StringBuilder)localObject).append(str);
        return ((StringBuilder)localObject).toString();
      }
    }
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("mopub:");
    String str = c;
    ((StringBuilder)localObject).append(str);
    return ((StringBuilder)localObject).toString();
  }
  
  public String getIdentifier(boolean paramBoolean)
  {
    boolean bool = d;
    if ((!bool) && (paramBoolean)) {
      return b;
    }
    return c;
  }
  
  public int hashCode()
  {
    int i = b.hashCode() * 31;
    int j = c.hashCode();
    i = (i + j) * 31;
    int k = d;
    return i + k;
  }
  
  public boolean isDoNotTrack()
  {
    return d;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AdvertisingId{mLastRotation=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", mAdvertisingId='");
    localObject = b;
    localStringBuilder.append((String)localObject);
    char c1 = '\'';
    localStringBuilder.append(c1);
    localStringBuilder.append(", mMopubId='");
    String str = c;
    localStringBuilder.append(str);
    localStringBuilder.append(c1);
    localStringBuilder.append(", mDoNotTrack=");
    boolean bool = d;
    localStringBuilder.append(bool);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.AdvertisingId
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */