package com.mopub.common.privacy;

import android.text.TextUtils;
import com.mopub.common.Preconditions;

final class PersonalInfoManager$1
  implements MoPubIdentifier.AdvertisingIdChangeListener
{
  PersonalInfoManager$1(PersonalInfoManager paramPersonalInfoManager) {}
  
  public final void onIdChanged(AdvertisingId paramAdvertisingId1, AdvertisingId paramAdvertisingId2)
  {
    Preconditions.checkNotNull(paramAdvertisingId1);
    Preconditions.checkNotNull(paramAdvertisingId2);
    boolean bool1 = paramAdvertisingId1.isDoNotTrack();
    if (bool1)
    {
      bool1 = paramAdvertisingId2.isDoNotTrack();
      if (bool1) {
        return;
      }
    }
    bool1 = paramAdvertisingId1.isDoNotTrack();
    ConsentChangeReason localConsentChangeReason;
    if (!bool1)
    {
      bool1 = paramAdvertisingId2.isDoNotTrack();
      if (bool1)
      {
        paramAdvertisingId1 = a;
        paramAdvertisingId2 = ConsentStatus.DNT;
        localConsentChangeReason = ConsentChangeReason.DENIED_BY_DNT_ON;
        PersonalInfoManager.a(paramAdvertisingId1, paramAdvertisingId2, localConsentChangeReason);
        a.requestSync(true);
        return;
      }
    }
    boolean bool2 = paramAdvertisingId1.isDoNotTrack();
    if (bool2)
    {
      bool2 = paramAdvertisingId2.isDoNotTrack();
      if (!bool2)
      {
        paramAdvertisingId1 = ConsentStatus.EXPLICIT_NO;
        paramAdvertisingId2 = aa).h;
        bool2 = paramAdvertisingId1.equals(paramAdvertisingId2);
        if (bool2)
        {
          paramAdvertisingId1 = a;
          paramAdvertisingId2 = ConsentStatus.EXPLICIT_NO;
          localConsentChangeReason = ConsentChangeReason.DNT_OFF;
          PersonalInfoManager.a(paramAdvertisingId1, paramAdvertisingId2, localConsentChangeReason);
          return;
        }
        paramAdvertisingId1 = a;
        paramAdvertisingId2 = ConsentStatus.UNKNOWN;
        localConsentChangeReason = ConsentChangeReason.DNT_OFF;
        PersonalInfoManager.a(paramAdvertisingId1, paramAdvertisingId2, localConsentChangeReason);
        return;
      }
    }
    paramAdvertisingId1 = b;
    bool2 = TextUtils.isEmpty(paramAdvertisingId1);
    if (!bool2)
    {
      paramAdvertisingId1 = paramAdvertisingId2.a();
      paramAdvertisingId2 = aa).f;
      bool2 = paramAdvertisingId1.equals(paramAdvertisingId2);
      if (!bool2)
      {
        paramAdvertisingId1 = ConsentStatus.EXPLICIT_YES;
        paramAdvertisingId2 = aa).b;
        bool2 = paramAdvertisingId1.equals(paramAdvertisingId2);
        if (bool2)
        {
          aa).c = null;
          aa).g = null;
          paramAdvertisingId1 = a;
          paramAdvertisingId2 = ConsentStatus.UNKNOWN;
          localConsentChangeReason = ConsentChangeReason.IFA_CHANGED;
          PersonalInfoManager.a(paramAdvertisingId1, paramAdvertisingId2, localConsentChangeReason);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.PersonalInfoManager.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */