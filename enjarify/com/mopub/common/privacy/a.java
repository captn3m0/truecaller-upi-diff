package com.mopub.common.privacy;

import android.content.Context;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout.LayoutParams;
import com.mopub.common.CloseableLayout;
import java.io.File;

final class a
  extends CloseableLayout
{
  static int a = 101;
  final WebView b;
  a.b c;
  a.a d;
  final WebViewClient e;
  
  public a(Context paramContext)
  {
    super(paramContext);
    paramContext = new com/mopub/common/privacy/a$2;
    paramContext.<init>(this);
    e = paramContext;
    paramContext = a();
    b = paramContext;
  }
  
  public a(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = new com/mopub/common/privacy/a$2;
    paramContext.<init>(this);
    e = paramContext;
    paramContext = a();
    b = paramContext;
  }
  
  public a(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramContext = new com/mopub/common/privacy/a$2;
    paramContext.<init>(this);
    e = paramContext;
    paramContext = a();
    b = paramContext;
  }
  
  private WebView a()
  {
    WebView localWebView = new android/webkit/WebView;
    Object localObject = getContext();
    localWebView.<init>((Context)localObject);
    localObject = null;
    localWebView.setVerticalScrollBarEnabled(false);
    localWebView.setHorizontalScrollBarEnabled(false);
    WebSettings localWebSettings = localWebView.getSettings();
    localWebSettings.setSupportZoom(false);
    localWebSettings.setBuiltInZoomControls(false);
    boolean bool = true;
    localWebSettings.setLoadsImagesAutomatically(bool);
    localWebSettings.setLoadWithOverviewMode(bool);
    localWebSettings.setJavaScriptEnabled(bool);
    localWebSettings.setDomStorageEnabled(bool);
    localWebSettings.setAppCacheEnabled(bool);
    String str = getContext().getCacheDir().getAbsolutePath();
    localWebSettings.setAppCachePath(str);
    localWebSettings.setAllowFileAccess(false);
    localWebSettings.setAllowContentAccess(false);
    int i = Build.VERSION.SDK_INT;
    int j = 16;
    if (i >= j) {
      localWebSettings.setAllowUniversalAccessFromFileURLs(false);
    }
    int k = Build.VERSION.SDK_INT;
    i = 17;
    if (k >= i)
    {
      k = View.generateViewId();
      localWebView.setId(k);
    }
    setCloseVisible(false);
    localObject = new android/widget/FrameLayout$LayoutParams;
    k = -1;
    ((FrameLayout.LayoutParams)localObject).<init>(k, k);
    addView(localWebView, (ViewGroup.LayoutParams)localObject);
    return localWebView;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */