package com.mopub.common.privacy;

import android.content.Context;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.MoPubNetworkError.Reason;
import com.mopub.network.MoPubRequest;
import com.mopub.volley.DefaultRetryPolicy;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Response;
import com.mopub.volley.toolbox.HttpHeaderParser;
import org.json.JSONException;
import org.json.JSONObject;

public class SyncRequest
  extends MoPubRequest
{
  private SyncRequest.Listener a;
  
  public SyncRequest(Context paramContext, String paramString, SyncRequest.Listener paramListener)
  {
    super(paramContext, paramString, paramListener);
    a = paramListener;
    paramContext = new com/mopub/volley/DefaultRetryPolicy;
    paramContext.<init>(2500, 0, 1.0F);
    setRetryPolicy(paramContext);
    setShouldCache(false);
  }
  
  public final Response parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    Object localObject1 = new com/mopub/common/privacy/SyncResponse$Builder;
    ((SyncResponse.Builder)localObject1).<init>();
    Object localObject2 = a(paramNetworkResponse);
    try
    {
      Object localObject3 = new org/json/JSONObject;
      ((JSONObject)localObject3).<init>((String)localObject2);
      localObject2 = PrivacyKey.IS_GDPR_REGION;
      localObject2 = ((PrivacyKey)localObject2).getKey();
      localObject2 = ((JSONObject)localObject3).getString((String)localObject2);
      localObject2 = ((SyncResponse.Builder)localObject1).setIsGdprRegion((String)localObject2);
      Object localObject4 = PrivacyKey.FORCE_EXPLICIT_NO;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).optString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setForceExplicitNo((String)localObject4);
      localObject4 = PrivacyKey.INVALIDATE_CONSENT;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).optString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setInvalidateConsent((String)localObject4);
      localObject4 = PrivacyKey.REACQUIRE_CONSENT;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).optString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setReacquireConsent((String)localObject4);
      localObject4 = PrivacyKey.IS_WHITELISTED;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).getString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setIsWhitelisted((String)localObject4);
      localObject4 = PrivacyKey.FORCE_GDPR_APPLIES;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).optString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setForceGdprApplies((String)localObject4);
      localObject4 = PrivacyKey.CURRENT_VENDOR_LIST_VERSION;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).getString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setCurrentVendorListVersion((String)localObject4);
      localObject4 = PrivacyKey.CURRENT_VENDOR_LIST_LINK;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).getString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setCurrentVendorListLink((String)localObject4);
      localObject4 = PrivacyKey.CURRENT_PRIVACY_POLICY_LINK;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).getString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setCurrentPrivacyPolicyLink((String)localObject4);
      localObject4 = PrivacyKey.CURRENT_PRIVACY_POLICY_VERSION;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).getString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setCurrentPrivacyPolicyVersion((String)localObject4);
      localObject4 = PrivacyKey.CURRENT_VENDOR_LIST_IAB_FORMAT;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).optString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setCurrentVendorListIabFormat((String)localObject4);
      localObject4 = PrivacyKey.CURRENT_VENDOR_LIST_IAB_HASH;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).getString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setCurrentVendorListIabHash((String)localObject4);
      localObject4 = PrivacyKey.CALL_AGAIN_AFTER_SECS;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).optString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setCallAgainAfterSecs((String)localObject4);
      localObject4 = PrivacyKey.EXTRAS;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject4 = ((JSONObject)localObject3).optString((String)localObject4);
      localObject2 = ((SyncResponse.Builder)localObject2).setExtras((String)localObject4);
      localObject4 = PrivacyKey.CONSENT_CHANGE_REASON;
      localObject4 = ((PrivacyKey)localObject4).getKey();
      localObject3 = ((JSONObject)localObject3).optString((String)localObject4);
      ((SyncResponse.Builder)localObject2).setConsentChangeReason((String)localObject3);
      localObject1 = ((SyncResponse.Builder)localObject1).build();
      paramNetworkResponse = HttpHeaderParser.parseCacheHeaders(paramNetworkResponse);
      return Response.success(localObject1, paramNetworkResponse);
    }
    catch (JSONException localJSONException)
    {
      paramNetworkResponse = new com/mopub/network/MoPubNetworkError;
      localObject2 = MoPubNetworkError.Reason.BAD_BODY;
      paramNetworkResponse.<init>("Unable to parse sync request network response.", (MoPubNetworkError.Reason)localObject2, null);
    }
    return Response.error(paramNetworkResponse);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.SyncRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */