package com.mopub.common.privacy;

import android.text.TextUtils;
import com.mopub.common.SdkInitializationListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.MultiAdResponse.ServerOverrideListener;
import com.mopub.volley.VolleyError;

final class PersonalInfoManager$b
  implements SyncRequest.Listener
{
  private PersonalInfoManager$b(PersonalInfoManager paramPersonalInfoManager) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    String str = "Failed sync request because of ";
    localStringBuilder.<init>(str);
    boolean bool = paramVolleyError instanceof MoPubNetworkError;
    if (bool) {
      paramVolleyError = ((MoPubNetworkError)paramVolleyError).getReason();
    } else {
      paramVolleyError = paramVolleyError.getMessage();
    }
    localStringBuilder.append(paramVolleyError);
    MoPubLog.d(localStringBuilder.toString());
    PersonalInfoManager.m(a);
    paramVolleyError = PersonalInfoManager.f(a);
    if (paramVolleyError != null)
    {
      MoPubLog.d("Personal Info Manager initialization finished but ran into errors.");
      PersonalInfoManager.f(a).onInitializationFinished();
      paramVolleyError = a;
      PersonalInfoManager.g(paramVolleyError);
    }
  }
  
  public final void onSuccess(SyncResponse paramSyncResponse)
  {
    Object localObject1 = a;
    boolean bool1 = ((PersonalInfoManager)localObject1).canCollectPersonalInformation();
    Object localObject2 = aa).t;
    if (localObject2 == null)
    {
      localObject2 = PersonalInfoManager.a(a);
      bool2 = paramSyncResponse.isGdprRegion();
      Boolean localBoolean = Boolean.valueOf(bool2);
      t = localBoolean;
    }
    boolean bool3 = paramSyncResponse.isForceGdprApplies();
    boolean bool2 = true;
    if (bool3)
    {
      PersonalInfoManager.a(a, bool2);
      aa).e = bool2;
      localObject2 = a;
      bool3 = ((PersonalInfoManager)localObject2).canCollectPersonalInformation();
      if (bool1 != bool3)
      {
        localObject1 = a;
        localObject3 = ab;
        ConsentStatus localConsentStatus = aa).b;
        PersonalInfoManager.a((PersonalInfoManager)localObject1, (ConsentStatus)localObject3, localConsentStatus, bool3);
      }
    }
    localObject1 = PersonalInfoManager.a(a);
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    Object localObject3 = a;
    long l1 = PersonalInfoManager.h((PersonalInfoManager)localObject3);
    ((StringBuilder)localObject2).append(l1);
    localObject2 = ((StringBuilder)localObject2).toString();
    g = ((String)localObject2);
    localObject1 = PersonalInfoManager.a(a);
    localObject2 = PersonalInfoManager.i(a);
    c = ((ConsentStatus)localObject2);
    localObject1 = PersonalInfoManager.a(a);
    bool3 = paramSyncResponse.isWhitelisted();
    i = bool3;
    localObject1 = PersonalInfoManager.a(a);
    localObject2 = paramSyncResponse.getCurrentVendorListVersion();
    j = ((String)localObject2);
    localObject1 = PersonalInfoManager.a(a);
    localObject2 = paramSyncResponse.getCurrentVendorListLink();
    k = ((String)localObject2);
    localObject1 = PersonalInfoManager.a(a);
    localObject2 = paramSyncResponse.getCurrentPrivacyPolicyVersion();
    l = ((String)localObject2);
    localObject1 = PersonalInfoManager.a(a);
    localObject2 = paramSyncResponse.getCurrentPrivacyPolicyLink();
    m = ((String)localObject2);
    localObject1 = paramSyncResponse.getCurrentVendorListIabHash();
    localObject2 = paramSyncResponse.getCurrentVendorListIabFormat();
    boolean bool4 = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool4)
    {
      localObject3 = aa).o;
      bool4 = ((String)localObject1).equals(localObject3);
      if (!bool4)
      {
        bool4 = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool4)
        {
          localObject3 = PersonalInfoManager.a(a);
          n = ((String)localObject2);
          localObject2 = PersonalInfoManager.a(a);
          o = ((String)localObject1);
        }
      }
    }
    localObject1 = a;
    bool3 = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool3)
    {
      localObject2 = PersonalInfoManager.a(a);
      ((c)localObject2).setExtras((String)localObject1);
    }
    localObject1 = paramSyncResponse.getConsentChangeReason();
    bool3 = paramSyncResponse.isForceExplicitNo();
    if (bool3)
    {
      localObject2 = PersonalInfoManager.j(a);
      ((MultiAdResponse.ServerOverrideListener)localObject2).onForceExplicitNo((String)localObject1);
    }
    else
    {
      bool3 = paramSyncResponse.isInvalidateConsent();
      if (bool3)
      {
        localObject2 = PersonalInfoManager.j(a);
        ((MultiAdResponse.ServerOverrideListener)localObject2).onInvalidateConsent((String)localObject1);
      }
      else
      {
        bool3 = paramSyncResponse.isReacquireConsent();
        if (bool3)
        {
          localObject2 = PersonalInfoManager.j(a);
          ((MultiAdResponse.ServerOverrideListener)localObject2).onReacquireConsent((String)localObject1);
        }
      }
    }
    paramSyncResponse = paramSyncResponse.getCallAgainAfterSecs();
    bool1 = TextUtils.isEmpty(paramSyncResponse);
    if (!bool1) {
      try
      {
        long l2 = Long.parseLong(paramSyncResponse);
        l1 = 0L;
        boolean bool5 = l2 < l1;
        if (bool5)
        {
          paramSyncResponse = a;
          l1 = 1000L;
          l2 *= l1;
          PersonalInfoManager.a(paramSyncResponse, l2);
        }
        else
        {
          localObject1 = "callAgainAfterSecs is not positive: ";
          paramSyncResponse = String.valueOf(paramSyncResponse);
          paramSyncResponse = ((String)localObject1).concat(paramSyncResponse);
          MoPubLog.d(paramSyncResponse);
        }
      }
      catch (NumberFormatException localNumberFormatException)
      {
        paramSyncResponse = "Unable to parse callAgainAfterSecs. Ignoring value";
        MoPubLog.d(paramSyncResponse);
      }
    }
    paramSyncResponse = ConsentStatus.EXPLICIT_YES;
    localObject1 = PersonalInfoManager.i(a);
    boolean bool6 = paramSyncResponse.equals(localObject1);
    if (!bool6)
    {
      paramSyncResponse = PersonalInfoManager.a(a);
      bool1 = false;
      localObject1 = null;
      f = null;
    }
    paramSyncResponse = a;
    bool6 = PersonalInfoManager.k(paramSyncResponse);
    if (bool6)
    {
      paramSyncResponse = a;
      bool1 = false;
      localObject1 = null;
      PersonalInfoManager.a(paramSyncResponse, false);
      paramSyncResponse = a;
      PersonalInfoManager.l(paramSyncResponse);
    }
    PersonalInfoManager.a(a).a();
    PersonalInfoManager.m(a);
    paramSyncResponse = ConsentStatus.POTENTIAL_WHITELIST;
    localObject1 = PersonalInfoManager.i(a);
    bool6 = paramSyncResponse.equals(localObject1);
    if (bool6)
    {
      paramSyncResponse = PersonalInfoManager.a(a);
      bool6 = i;
      if (bool6)
      {
        paramSyncResponse = a;
        localObject1 = ConsentStatus.EXPLICIT_YES;
        localObject2 = ConsentChangeReason.GRANTED_BY_WHITELISTED_PUB;
        PersonalInfoManager.a(paramSyncResponse, (ConsentStatus)localObject1, (ConsentChangeReason)localObject2);
        paramSyncResponse = a;
        paramSyncResponse.requestSync(bool2);
      }
    }
    paramSyncResponse = PersonalInfoManager.f(a);
    if (paramSyncResponse != null)
    {
      PersonalInfoManager.f(a).onInitializationFinished();
      paramSyncResponse = a;
      PersonalInfoManager.g(paramSyncResponse);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.PersonalInfoManager.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */