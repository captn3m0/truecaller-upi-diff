package com.mopub.common.privacy;

import com.mopub.mobileads.MoPubErrorCode;

public abstract interface ConsentDialogListener
{
  public abstract void onConsentDialogLoadFailed(MoPubErrorCode paramMoPubErrorCode);
  
  public abstract void onConsentDialogLoaded();
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.ConsentDialogListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */