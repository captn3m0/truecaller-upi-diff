package com.mopub.common.privacy;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.mopub.common.ClientMetadata;
import com.mopub.common.Preconditions;
import com.mopub.common.SharedPreferencesHelper;
import java.util.Locale;

final class c
  implements ConsentData
{
  String a;
  ConsentStatus b;
  ConsentStatus c;
  String d;
  boolean e;
  String f;
  String g;
  ConsentStatus h;
  boolean i;
  String j;
  String k;
  String l;
  String m;
  String n;
  String o;
  String p;
  String q;
  String r;
  boolean s;
  Boolean t;
  private final Context u;
  private String v;
  
  c(Context paramContext, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramString);
    paramContext = paramContext.getApplicationContext();
    u = paramContext;
    paramContext = ConsentStatus.UNKNOWN;
    b = paramContext;
    paramContext = SharedPreferencesHelper.getSharedPreferences(u, "com.mopub.privacy");
    Object localObject = paramContext.getString("info/adunit", "");
    a = ((String)localObject);
    String str = ConsentStatus.UNKNOWN.name();
    localObject = ConsentStatus.fromString(paramContext.getString("info/consent_status", str));
    b = ((ConsentStatus)localObject);
    str = null;
    localObject = paramContext.getString("info/last_successfully_synced_consent_status", null);
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject);
    if (bool1)
    {
      c = null;
    }
    else
    {
      localObject = ConsentStatus.fromString((String)localObject);
      c = ((ConsentStatus)localObject);
    }
    bool1 = false;
    boolean bool2 = paramContext.getBoolean("info/is_whitelisted", false);
    i = bool2;
    localObject = paramContext.getString("info/current_vendor_list_version", null);
    j = ((String)localObject);
    localObject = paramContext.getString("info/current_vendor_list_link", null);
    k = ((String)localObject);
    localObject = paramContext.getString("info/current_privacy_policy_version", null);
    l = ((String)localObject);
    localObject = paramContext.getString("info/current_privacy_policy_link", null);
    m = ((String)localObject);
    localObject = paramContext.getString("info/current_vendor_list_iab_format", null);
    n = ((String)localObject);
    localObject = paramContext.getString("info/current_vendor_list_iab_hash", null);
    o = ((String)localObject);
    localObject = paramContext.getString("info/consented_vendor_list_version", null);
    p = ((String)localObject);
    localObject = paramContext.getString("info/consented_privacy_policy_version", null);
    q = ((String)localObject);
    localObject = paramContext.getString("info/consented_vendor_list_iab_format", null);
    r = ((String)localObject);
    localObject = paramContext.getString("info/extras", null);
    v = ((String)localObject);
    localObject = paramContext.getString("info/consent_change_reason", null);
    d = ((String)localObject);
    bool2 = paramContext.getBoolean("info/reacquire_consent", false);
    s = bool2;
    localObject = paramContext.getString("info/gdpr_applies", null);
    boolean bool3 = TextUtils.isEmpty((CharSequence)localObject);
    if (bool3)
    {
      t = null;
    }
    else
    {
      bool2 = Boolean.parseBoolean((String)localObject);
      localObject = Boolean.valueOf(bool2);
      t = ((Boolean)localObject);
    }
    bool2 = paramContext.getBoolean("info/force_gdpr_applies", false);
    e = bool2;
    localObject = paramContext.getString("info/udid", null);
    f = ((String)localObject);
    localObject = paramContext.getString("info/last_changed_ms", null);
    g = ((String)localObject);
    localObject = "info/consent_status_before_dnt";
    paramContext = paramContext.getString((String)localObject, null);
    bool2 = TextUtils.isEmpty(paramContext);
    if (bool2)
    {
      h = null;
    }
    else
    {
      paramContext = ConsentStatus.fromString(paramContext);
      h = paramContext;
    }
    a = paramString;
  }
  
  private static String a(Context paramContext, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    String[] arrayOfString = Locale.getISOLanguages();
    int i1 = arrayOfString.length;
    int i2 = 0;
    while (i2 < i1)
    {
      String str = arrayOfString[i2];
      if (str != null)
      {
        boolean bool = str.equals(paramString);
        if (bool) {
          return paramString;
        }
      }
      i2 += 1;
    }
    return ClientMetadata.getCurrentLanguage(paramContext);
  }
  
  private static String a(String paramString1, Context paramContext, String paramString2)
  {
    Preconditions.checkNotNull(paramContext);
    boolean bool = TextUtils.isEmpty(paramString1);
    if (bool) {
      return "";
    }
    paramContext = a(paramContext, paramString2);
    return paramString1.replaceAll("%%LANGUAGE%%", paramContext);
  }
  
  final void a()
  {
    SharedPreferences.Editor localEditor = SharedPreferencesHelper.getSharedPreferences(u, "com.mopub.privacy").edit();
    Object localObject = a;
    localEditor.putString("info/adunit", (String)localObject);
    localObject = b.name();
    localEditor.putString("info/consent_status", (String)localObject);
    String str1 = "info/last_successfully_synced_consent_status";
    localObject = c;
    String str2 = null;
    if (localObject == null)
    {
      bool = false;
      localObject = null;
    }
    else
    {
      localObject = ((ConsentStatus)localObject).name();
    }
    localEditor.putString(str1, (String)localObject);
    boolean bool = i;
    localEditor.putBoolean("info/is_whitelisted", bool);
    localObject = j;
    localEditor.putString("info/current_vendor_list_version", (String)localObject);
    localObject = k;
    localEditor.putString("info/current_vendor_list_link", (String)localObject);
    localObject = l;
    localEditor.putString("info/current_privacy_policy_version", (String)localObject);
    localObject = m;
    localEditor.putString("info/current_privacy_policy_link", (String)localObject);
    localObject = n;
    localEditor.putString("info/current_vendor_list_iab_format", (String)localObject);
    localObject = o;
    localEditor.putString("info/current_vendor_list_iab_hash", (String)localObject);
    localObject = p;
    localEditor.putString("info/consented_vendor_list_version", (String)localObject);
    localObject = q;
    localEditor.putString("info/consented_privacy_policy_version", (String)localObject);
    localObject = r;
    localEditor.putString("info/consented_vendor_list_iab_format", (String)localObject);
    localObject = v;
    localEditor.putString("info/extras", (String)localObject);
    localObject = d;
    localEditor.putString("info/consent_change_reason", (String)localObject);
    bool = s;
    localEditor.putBoolean("info/reacquire_consent", bool);
    str1 = "info/gdpr_applies";
    localObject = t;
    if (localObject == null)
    {
      bool = false;
      localObject = null;
    }
    else
    {
      localObject = ((Boolean)localObject).toString();
    }
    localEditor.putString(str1, (String)localObject);
    bool = e;
    localEditor.putBoolean("info/force_gdpr_applies", bool);
    localObject = f;
    localEditor.putString("info/udid", (String)localObject);
    localObject = g;
    localEditor.putString("info/last_changed_ms", (String)localObject);
    str1 = "info/consent_status_before_dnt";
    localObject = h;
    if (localObject != null) {
      str2 = ((ConsentStatus)localObject).name();
    }
    localEditor.putString(str1, str2);
    localEditor.apply();
  }
  
  public final String getConsentedPrivacyPolicyVersion()
  {
    return q;
  }
  
  public final String getConsentedVendorListIabFormat()
  {
    return r;
  }
  
  public final String getConsentedVendorListVersion()
  {
    return p;
  }
  
  public final String getCurrentPrivacyPolicyLink()
  {
    return getCurrentPrivacyPolicyLink(null);
  }
  
  public final String getCurrentPrivacyPolicyLink(String paramString)
  {
    String str = m;
    Context localContext = u;
    return a(str, localContext, paramString);
  }
  
  public final String getCurrentPrivacyPolicyVersion()
  {
    return l;
  }
  
  public final String getCurrentVendorListIabFormat()
  {
    return n;
  }
  
  public final String getCurrentVendorListLink()
  {
    return getCurrentVendorListLink(null);
  }
  
  public final String getCurrentVendorListLink(String paramString)
  {
    String str = k;
    Context localContext = u;
    return a(str, localContext, paramString);
  }
  
  public final String getCurrentVendorListVersion()
  {
    return j;
  }
  
  public final String getExtras()
  {
    return v;
  }
  
  public final boolean isForceGdprApplies()
  {
    return e;
  }
  
  public final void setExtras(String paramString)
  {
    v = paramString;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */