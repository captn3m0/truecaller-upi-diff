package com.mopub.common.privacy;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.mopub.common.ClientMetadata;
import com.mopub.common.MoPub;
import com.mopub.common.Preconditions;
import com.mopub.common.SdkInitializationListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.ManifestUtils;
import com.mopub.mobileads.MoPubConversionTracker;
import com.mopub.network.MoPubRequestQueue;
import com.mopub.network.MultiAdResponse;
import com.mopub.network.MultiAdResponse.ServerOverrideListener;
import com.mopub.network.Networking;
import com.mopub.volley.Request;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class PersonalInfoManager
{
  private final Context a;
  private final Set b;
  private final c c;
  private final ConsentDialogController d;
  private final MoPubConversionTracker e;
  private final SyncRequest.Listener f;
  private MultiAdResponse.ServerOverrideListener g;
  private SdkInitializationListener h;
  private long i;
  private Long j;
  private ConsentStatus k;
  private long l;
  private boolean m;
  private boolean n;
  private boolean o;
  
  public PersonalInfoManager(Context paramContext, String paramString, SdkInitializationListener paramSdkInitializationListener)
  {
    long l1 = 300000L;
    i = l1;
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramString);
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    paramContext = new java/util/HashSet;
    paramContext.<init>();
    paramContext = Collections.synchronizedSet(paramContext);
    b = paramContext;
    paramContext = new com/mopub/common/privacy/PersonalInfoManager$b;
    paramContext.<init>(this, (byte)0);
    f = paramContext;
    paramContext = new com/mopub/common/privacy/PersonalInfoManager$a;
    paramContext.<init>(this, (byte)0);
    g = paramContext;
    MultiAdResponse.setServerOverrideListener(g);
    paramContext = new com/mopub/common/privacy/ConsentDialogController;
    Context localContext = a;
    paramContext.<init>(localContext);
    d = paramContext;
    paramContext = new com/mopub/common/privacy/c;
    localContext = a;
    paramContext.<init>(localContext, paramString);
    c = paramContext;
    paramContext = new com/mopub/mobileads/MoPubConversionTracker;
    paramString = a;
    paramContext.<init>(paramString);
    e = paramContext;
    paramContext = new com/mopub/common/privacy/PersonalInfoManager$1;
    paramContext.<init>(this);
    h = paramSdkInitializationListener;
    paramString = ClientMetadata.getInstance(a).getMoPubIdentifier();
    paramString.setIdChangeListener(paramContext);
    paramContext = new com/mopub/common/privacy/PersonalInfoManager$5;
    paramContext.<init>(this);
    d = paramContext;
    boolean bool = c;
    if (bool) {
      paramString.a();
    }
  }
  
  private void a(ConsentStatus paramConsentStatus1, ConsentStatus paramConsentStatus2, boolean paramBoolean)
  {
    synchronized (b)
    {
      Object localObject1 = b;
      localObject1 = ((Set)localObject1).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        Object localObject2 = ((Iterator)localObject1).next();
        Object localObject3 = localObject2;
        localObject3 = (ConsentStatusChangeListener)localObject2;
        localObject2 = new android/os/Handler;
        Object localObject4 = Looper.getMainLooper();
        ((Handler)localObject2).<init>((Looper)localObject4);
        PersonalInfoManager.4 local4 = new com/mopub/common/privacy/PersonalInfoManager$4;
        localObject4 = local4;
        local4.<init>(this, (ConsentStatusChangeListener)localObject3, paramConsentStatus1, paramConsentStatus2, paramBoolean);
        ((Handler)localObject2).post(local4);
      }
      return;
    }
  }
  
  static boolean a(boolean paramBoolean1, Boolean paramBoolean, boolean paramBoolean2, Long paramLong, long paramLong1, String paramString, boolean paramBoolean3)
  {
    if (paramBoolean1) {
      return false;
    }
    paramBoolean1 = true;
    if (paramBoolean == null) {
      return paramBoolean1;
    }
    boolean bool1 = paramBoolean.booleanValue();
    if (!bool1) {
      return false;
    }
    if (paramBoolean2) {
      return paramBoolean1;
    }
    if (paramBoolean3)
    {
      bool1 = TextUtils.isEmpty(paramString);
      if (bool1) {
        return false;
      }
    }
    if (paramLong == null) {
      return paramBoolean1;
    }
    long l1 = SystemClock.uptimeMillis();
    long l2 = paramLong.longValue();
    l1 -= l2;
    boolean bool2 = l1 < paramLong1;
    if (bool2) {
      return paramBoolean1;
    }
    return false;
  }
  
  final void a()
  {
    Object localObject1 = c.b;
    k = ((ConsentStatus)localObject1);
    localObject1 = Calendar.getInstance();
    long l1 = ((Calendar)localObject1).getTimeInMillis();
    l = l1;
    boolean bool1 = true;
    m = bool1;
    long l2 = SystemClock.uptimeMillis();
    Object localObject2 = Long.valueOf(l2);
    j = ((Long)localObject2);
    localObject2 = new com/mopub/common/privacy/SyncUrlGenerator;
    Object localObject3 = a;
    Object localObject4 = k.getValue();
    ((SyncUrlGenerator)localObject2).<init>((Context)localObject3, (String)localObject4);
    localObject3 = c.a;
    localObject3 = ((SyncUrlGenerator)localObject2).withAdUnitId((String)localObject3);
    localObject4 = c.f;
    localObject3 = ((SyncUrlGenerator)localObject3).withUdid((String)localObject4);
    localObject4 = c.g;
    localObject3 = ((SyncUrlGenerator)localObject3).withLastChangedMs((String)localObject4);
    localObject4 = c.c;
    localObject3 = ((SyncUrlGenerator)localObject3).withLastConsentStatus((ConsentStatus)localObject4);
    localObject4 = c.d;
    localObject3 = ((SyncUrlGenerator)localObject3).withConsentChangeReason((String)localObject4);
    localObject4 = c.getConsentedVendorListVersion();
    localObject3 = ((SyncUrlGenerator)localObject3).withConsentedVendorListVersion((String)localObject4);
    localObject4 = c.getConsentedPrivacyPolicyVersion();
    localObject3 = ((SyncUrlGenerator)localObject3).withConsentedPrivacyPolicyVersion((String)localObject4);
    localObject4 = c.o;
    localObject3 = ((SyncUrlGenerator)localObject3).withCachedVendorListIabHash((String)localObject4);
    localObject4 = c.getExtras();
    localObject3 = ((SyncUrlGenerator)localObject3).withExtras((String)localObject4);
    localObject4 = gdprApplies();
    localObject3 = ((SyncUrlGenerator)localObject3).withGdprApplies((Boolean)localObject4);
    localObject4 = c;
    boolean bool2 = ((c)localObject4).isForceGdprApplies();
    ((SyncUrlGenerator)localObject3).withForceGdprApplies(bool2);
    boolean bool3 = n;
    if (bool3)
    {
      o = bool1;
      localObject1 = Boolean.TRUE;
      ((SyncUrlGenerator)localObject2).withForceGdprAppliesChanged((Boolean)localObject1);
    }
    localObject1 = new com/mopub/common/privacy/SyncRequest;
    localObject3 = a;
    localObject2 = ((SyncUrlGenerator)localObject2).generateUrlString("ads.mopub.com");
    localObject4 = f;
    ((SyncRequest)localObject1).<init>((Context)localObject3, (String)localObject2, (SyncRequest.Listener)localObject4);
    Networking.getRequestQueue(a).add((Request)localObject1);
  }
  
  final void a(ConsentStatus paramConsentStatus, ConsentChangeReason paramConsentChangeReason)
  {
    paramConsentChangeReason = paramConsentChangeReason.getReason();
    a(paramConsentStatus, paramConsentChangeReason);
  }
  
  final void a(ConsentStatus paramConsentStatus, String paramString)
  {
    Preconditions.checkNotNull(paramConsentStatus);
    Preconditions.checkNotNull(paramString);
    ConsentStatus localConsentStatus = c.b;
    boolean bool1 = localConsentStatus.equals(paramConsentStatus);
    if (bool1)
    {
      paramConsentStatus = new java/lang/StringBuilder;
      paramConsentStatus.<init>("Consent status is already ");
      paramConsentStatus.append(localConsentStatus);
      paramConsentStatus.append(". Not doing a state transition.");
      MoPubLog.d(paramConsentStatus.toString());
      return;
    }
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Changing consent status from ");
    ((StringBuilder)localObject1).append(localConsentStatus);
    ((StringBuilder)localObject1).append("to ");
    ((StringBuilder)localObject1).append(paramConsentStatus);
    Object localObject2 = " because ";
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(paramString);
    MoPubLog.d(((StringBuilder)localObject1).toString());
    localObject1 = c;
    d = paramString;
    b = paramConsentStatus;
    paramString = ConsentStatus.POTENTIAL_WHITELIST;
    boolean bool2 = paramString.equals(paramConsentStatus);
    if (!bool2)
    {
      paramString = ConsentStatus.POTENTIAL_WHITELIST;
      bool2 = paramString.equals(localConsentStatus);
      if (!bool2)
      {
        paramString = ConsentStatus.EXPLICIT_YES;
        bool2 = paramString.equals(paramConsentStatus);
        if (!bool2) {}
      }
    }
    else
    {
      paramString = c;
      localObject1 = paramString.getCurrentPrivacyPolicyVersion();
      q = ((String)localObject1);
      paramString = c;
      localObject1 = paramString.getCurrentVendorListVersion();
      p = ((String)localObject1);
      paramString = c;
      localObject1 = paramString.getCurrentVendorListIabFormat();
      r = ((String)localObject1);
    }
    paramString = ConsentStatus.DNT;
    bool2 = paramString.equals(paramConsentStatus);
    if (!bool2)
    {
      paramString = ConsentStatus.EXPLICIT_NO;
      bool2 = paramString.equals(paramConsentStatus);
      if (!bool2)
      {
        paramString = ConsentStatus.UNKNOWN;
        bool2 = paramString.equals(paramConsentStatus);
        if (!bool2) {
          break label316;
        }
      }
    }
    paramString = c;
    bool1 = false;
    localObject1 = null;
    q = null;
    p = null;
    r = null;
    label316:
    paramString = ConsentStatus.EXPLICIT_YES;
    bool2 = paramString.equals(paramConsentStatus);
    if (bool2)
    {
      paramString = c;
      localObject1 = ClientMetadata.getInstance(a).getMoPubIdentifier().getAdvertisingInfo().a();
      f = ((String)localObject1);
    }
    paramString = ConsentStatus.DNT;
    bool2 = paramString.equals(paramConsentStatus);
    if (bool2)
    {
      paramString = c;
      h = localConsentStatus;
    }
    paramString = c;
    bool1 = false;
    localObject1 = null;
    s = false;
    paramString.a();
    bool2 = canCollectPersonalInformation();
    if (bool2)
    {
      ClientMetadata.getInstance(a).repopulateCountryData();
      localObject2 = e;
      boolean bool3 = ((MoPubConversionTracker)localObject2).shouldTrack();
      if (bool3)
      {
        localObject2 = e;
        ((MoPubConversionTracker)localObject2).reportAppOpen(false);
      }
    }
    a(localConsentStatus, paramConsentStatus, bool2);
  }
  
  public boolean canCollectPersonalInformation()
  {
    Object localObject = gdprApplies();
    if (localObject == null) {
      return false;
    }
    boolean bool1 = ((Boolean)localObject).booleanValue();
    boolean bool2 = true;
    if (!bool1) {
      return bool2;
    }
    localObject = getPersonalInfoConsentStatus();
    ConsentStatus localConsentStatus = ConsentStatus.EXPLICIT_YES;
    bool1 = ((ConsentStatus)localObject).equals(localConsentStatus);
    if (bool1)
    {
      localObject = ClientMetadata.getInstance(a).getMoPubIdentifier().getAdvertisingInfo();
      bool1 = ((AdvertisingId)localObject).isDoNotTrack();
      if (!bool1) {
        return bool2;
      }
    }
    return false;
  }
  
  public void forceGdprApplies()
  {
    Object localObject = c;
    boolean bool1 = ((c)localObject).isForceGdprApplies();
    if (bool1) {
      return;
    }
    bool1 = canCollectPersonalInformation();
    c localc = c;
    boolean bool2 = true;
    e = bool2;
    n = bool2;
    localc.a();
    boolean bool3 = canCollectPersonalInformation();
    if (bool1 != bool3)
    {
      localObject = c.b;
      ConsentStatus localConsentStatus = c.b;
      a((ConsentStatus)localObject, localConsentStatus, bool3);
    }
    requestSync(bool2);
  }
  
  public Boolean gdprApplies()
  {
    c localc = c;
    boolean bool = localc.isForceGdprApplies();
    if (bool) {
      return Boolean.TRUE;
    }
    return c.t;
  }
  
  public ConsentData getConsentData()
  {
    c localc = new com/mopub/common/privacy/c;
    Context localContext = a;
    String str = c.a;
    localc.<init>(localContext, str);
    return localc;
  }
  
  public ConsentStatus getPersonalInfoConsentStatus()
  {
    return c.b;
  }
  
  public void grantConsent()
  {
    Object localObject = ClientMetadata.getInstance(a).getMoPubIdentifier().getAdvertisingInfo();
    boolean bool = ((AdvertisingId)localObject).isDoNotTrack();
    if (bool)
    {
      MoPubLog.e("Cannot grant consent because Do Not Track is on.");
      return;
    }
    localObject = c;
    bool = i;
    ConsentChangeReason localConsentChangeReason;
    if (bool)
    {
      localObject = ConsentStatus.EXPLICIT_YES;
      localConsentChangeReason = ConsentChangeReason.GRANTED_BY_WHITELISTED_PUB;
      a((ConsentStatus)localObject, localConsentChangeReason);
    }
    else
    {
      MoPubLog.w("You do not have approval to use the grantConsent API. Please reach out to your account teams or support@mopub.com for more information.");
      localObject = ConsentStatus.POTENTIAL_WHITELIST;
      localConsentChangeReason = ConsentChangeReason.GRANTED_BY_NOT_WHITELISTED_PUB;
      a((ConsentStatus)localObject, localConsentChangeReason);
    }
    requestSync(true);
  }
  
  public boolean isConsentDialogReady()
  {
    return d.c;
  }
  
  public void loadConsentDialog(ConsentDialogListener paramConsentDialogListener)
  {
    ManifestUtils.checkGdprActivitiesDeclared(a);
    Object localObject1 = ClientMetadata.getInstance(a).getMoPubIdentifier().getAdvertisingInfo();
    boolean bool1 = ((AdvertisingId)localObject1).isDoNotTrack();
    if (bool1)
    {
      if (paramConsentDialogListener != null)
      {
        localObject1 = new android/os/Handler;
        ((Handler)localObject1).<init>();
        localObject2 = new com/mopub/common/privacy/PersonalInfoManager$2;
        ((PersonalInfoManager.2)localObject2).<init>(this, paramConsentDialogListener);
        ((Handler)localObject1).post((Runnable)localObject2);
      }
      return;
    }
    localObject1 = gdprApplies();
    if (localObject1 != null)
    {
      boolean bool2 = ((Boolean)localObject1).booleanValue();
      if (!bool2)
      {
        if (paramConsentDialogListener != null)
        {
          localObject1 = new android/os/Handler;
          ((Handler)localObject1).<init>();
          localObject2 = new com/mopub/common/privacy/PersonalInfoManager$3;
          ((PersonalInfoManager.3)localObject2).<init>(this, paramConsentDialogListener);
          ((Handler)localObject1).post((Runnable)localObject2);
        }
        return;
      }
    }
    Object localObject2 = d;
    c localc = c;
    ((ConsentDialogController)localObject2).a(paramConsentDialogListener, (Boolean)localObject1, localc);
  }
  
  public void requestSync(boolean paramBoolean)
  {
    boolean bool1 = MoPub.isSdkInitialized();
    if (!bool1) {
      return;
    }
    AdvertisingId localAdvertisingId = ClientMetadata.getInstance(a).getMoPubIdentifier().getAdvertisingInfo();
    boolean bool2 = m;
    Boolean localBoolean = gdprApplies();
    Long localLong = j;
    long l1 = i;
    c localc = c;
    String str = f;
    boolean bool3 = localAdvertisingId.isDoNotTrack();
    paramBoolean = a(bool2, localBoolean, paramBoolean, localLong, l1, str, bool3);
    if (!paramBoolean) {
      return;
    }
    a();
  }
  
  public void revokeConsent()
  {
    Object localObject = ClientMetadata.getInstance(a).getMoPubIdentifier().getAdvertisingInfo();
    boolean bool = ((AdvertisingId)localObject).isDoNotTrack();
    if (bool)
    {
      MoPubLog.e("Cannot revoke consent because Do Not Track is on.");
      return;
    }
    localObject = ConsentStatus.EXPLICIT_NO;
    ConsentChangeReason localConsentChangeReason = ConsentChangeReason.DENIED_BY_PUB;
    a((ConsentStatus)localObject, localConsentChangeReason);
    requestSync(true);
  }
  
  public boolean shouldShowConsentDialog()
  {
    Object localObject = gdprApplies();
    ConsentStatus localConsentStatus = null;
    if (localObject != null)
    {
      localObject = gdprApplies();
      boolean bool = ((Boolean)localObject).booleanValue();
      if (bool)
      {
        localObject = ClientMetadata.getInstance(a).getMoPubIdentifier().getAdvertisingInfo();
        bool = ((AdvertisingId)localObject).isDoNotTrack();
        if (bool) {
          return false;
        }
        localObject = c;
        bool = s;
        if (bool)
        {
          localObject = c.b;
          localConsentStatus = ConsentStatus.EXPLICIT_YES;
          bool = ((ConsentStatus)localObject).equals(localConsentStatus);
          if (bool) {
            return true;
          }
        }
        localObject = c.b;
        localConsentStatus = ConsentStatus.UNKNOWN;
        return ((ConsentStatus)localObject).equals(localConsentStatus);
      }
    }
    return false;
  }
  
  public boolean showConsentDialog()
  {
    ConsentDialogController localConsentDialogController = d;
    boolean bool = c;
    if (bool)
    {
      Object localObject = b;
      bool = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool)
      {
        localObject = a;
        String str = b;
        ConsentDialogActivity.a((Context)localObject, str);
        localConsentDialogController.a();
        return true;
      }
    }
    return false;
  }
  
  public void subscribeConsentStatusChangeListener(ConsentStatusChangeListener paramConsentStatusChangeListener)
  {
    if (paramConsentStatusChangeListener == null) {
      return;
    }
    b.add(paramConsentStatusChangeListener);
  }
  
  public void unsubscribeConsentStatusChangeListener(ConsentStatusChangeListener paramConsentStatusChangeListener)
  {
    b.remove(paramConsentStatusChangeListener);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.PersonalInfoManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */