package com.mopub.common.privacy;

import android.text.TextUtils;
import com.mopub.network.MultiAdResponse.ServerOverrideListener;

final class PersonalInfoManager$a
  implements MultiAdResponse.ServerOverrideListener
{
  private PersonalInfoManager$a(PersonalInfoManager paramPersonalInfoManager) {}
  
  public final void onForceExplicitNo(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      paramString = a;
      localObject1 = ConsentStatus.EXPLICIT_NO;
      localObject2 = ConsentChangeReason.REVOKED_BY_SERVER;
      PersonalInfoManager.a(paramString, (ConsentStatus)localObject1, (ConsentChangeReason)localObject2);
      return;
    }
    Object localObject1 = a;
    Object localObject2 = ConsentStatus.EXPLICIT_NO;
    ((PersonalInfoManager)localObject1).a((ConsentStatus)localObject2, paramString);
  }
  
  public final void onForceGdprApplies()
  {
    a.forceGdprApplies();
  }
  
  public final void onInvalidateConsent(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      paramString = a;
      localObject1 = ConsentStatus.UNKNOWN;
      localObject2 = ConsentChangeReason.REACQUIRE_BY_SERVER;
      PersonalInfoManager.a(paramString, (ConsentStatus)localObject1, (ConsentChangeReason)localObject2);
      return;
    }
    Object localObject1 = a;
    Object localObject2 = ConsentStatus.UNKNOWN;
    ((PersonalInfoManager)localObject1).a((ConsentStatus)localObject2, paramString);
  }
  
  public final void onReacquireConsent(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      c localc = PersonalInfoManager.a(a);
      d = paramString;
    }
    aa).s = true;
    PersonalInfoManager.a(a).a();
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.PersonalInfoManager.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */