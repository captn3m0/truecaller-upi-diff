package com.mopub.common.privacy;

final class PersonalInfoManager$4
  implements Runnable
{
  PersonalInfoManager$4(PersonalInfoManager paramPersonalInfoManager, ConsentStatusChangeListener paramConsentStatusChangeListener, ConsentStatus paramConsentStatus1, ConsentStatus paramConsentStatus2, boolean paramBoolean) {}
  
  public final void run()
  {
    ConsentStatusChangeListener localConsentStatusChangeListener = a;
    ConsentStatus localConsentStatus1 = b;
    ConsentStatus localConsentStatus2 = c;
    boolean bool = d;
    localConsentStatusChangeListener.onConsentStateChange(localConsentStatus1, localConsentStatus2, bool);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.PersonalInfoManager.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */