package com.mopub.common.privacy;

public abstract interface ConsentData
{
  public abstract String getConsentedPrivacyPolicyVersion();
  
  public abstract String getConsentedVendorListIabFormat();
  
  public abstract String getConsentedVendorListVersion();
  
  public abstract String getCurrentPrivacyPolicyLink();
  
  public abstract String getCurrentPrivacyPolicyLink(String paramString);
  
  public abstract String getCurrentPrivacyPolicyVersion();
  
  public abstract String getCurrentVendorListIabFormat();
  
  public abstract String getCurrentVendorListLink();
  
  public abstract String getCurrentVendorListLink(String paramString);
  
  public abstract String getCurrentVendorListVersion();
  
  public abstract boolean isForceGdprApplies();
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.ConsentData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */