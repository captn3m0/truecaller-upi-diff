package com.mopub.common.privacy;

import com.mopub.mobileads.MoPubErrorCode;

final class PersonalInfoManager$3
  implements Runnable
{
  PersonalInfoManager$3(PersonalInfoManager paramPersonalInfoManager, ConsentDialogListener paramConsentDialogListener) {}
  
  public final void run()
  {
    ConsentDialogListener localConsentDialogListener = a;
    MoPubErrorCode localMoPubErrorCode = MoPubErrorCode.GDPR_DOES_NOT_APPLY;
    localConsentDialogListener.onConsentDialogLoadFailed(localMoPubErrorCode);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.PersonalInfoManager.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */