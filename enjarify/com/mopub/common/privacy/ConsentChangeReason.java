package com.mopub.common.privacy;

public enum ConsentChangeReason
{
  private final String a;
  
  static
  {
    Object localObject = new com/mopub/common/privacy/ConsentChangeReason;
    ((ConsentChangeReason)localObject).<init>("GRANTED_BY_USER", 0, "Consent was explicitly granted by the user");
    GRANTED_BY_USER = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int i = 1;
    ((ConsentChangeReason)localObject).<init>("GRANTED_BY_WHITELISTED_PUB", i, "Consent was explicitly granted by a whitelisted publisher");
    GRANTED_BY_WHITELISTED_PUB = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int j = 2;
    ((ConsentChangeReason)localObject).<init>("GRANTED_BY_NOT_WHITELISTED_PUB", j, "Consent was explicitly granted by a publisher who is not whitelisted");
    GRANTED_BY_NOT_WHITELISTED_PUB = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int k = 3;
    ((ConsentChangeReason)localObject).<init>("DENIED_BY_USER", k, "Consent was explicitly denied by the user");
    DENIED_BY_USER = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int m = 4;
    ((ConsentChangeReason)localObject).<init>("DENIED_BY_PUB", m, "Consent was explicitly denied by the publisher");
    DENIED_BY_PUB = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int n = 5;
    ((ConsentChangeReason)localObject).<init>("DENIED_BY_DNT_ON", n, "Limit ad tracking was enabled and consent implicitly denied by the user");
    DENIED_BY_DNT_ON = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int i1 = 6;
    ((ConsentChangeReason)localObject).<init>("DNT_OFF", i1, "Limit ad tracking was disabled");
    DNT_OFF = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int i2 = 7;
    ((ConsentChangeReason)localObject).<init>("REACQUIRE_BECAUSE_DNT_OFF", i2, "Consent needs to be reacquired because the user disabled limit ad tracking");
    REACQUIRE_BECAUSE_DNT_OFF = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int i3 = 8;
    ((ConsentChangeReason)localObject).<init>("REACQUIRE_BECAUSE_PRIVACY_POLICY", i3, "Consent needs to be reacquired because the privacy policy has changed");
    REACQUIRE_BECAUSE_PRIVACY_POLICY = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int i4 = 9;
    ((ConsentChangeReason)localObject).<init>("REACUIRE_BECAUSE_VENDOR_LIST", i4, "Consent needs to be reacquired because the vendor list has changed");
    REACUIRE_BECAUSE_VENDOR_LIST = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int i5 = 10;
    ((ConsentChangeReason)localObject).<init>("REAQUIRE_BECAUSE_IAB_VENDOR_LIST", i5, "Consent needs to be reacquired because the IAB vendor list has changed");
    REAQUIRE_BECAUSE_IAB_VENDOR_LIST = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int i6 = 11;
    ((ConsentChangeReason)localObject).<init>("REVOKED_BY_SERVER", i6, "Consent was revoked by the server");
    REVOKED_BY_SERVER = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    ((ConsentChangeReason)localObject).<init>("REACQUIRE_BY_SERVER", 12, "Server requires that consent needs to be reacquired");
    REACQUIRE_BY_SERVER = (ConsentChangeReason)localObject;
    localObject = new com/mopub/common/privacy/ConsentChangeReason;
    int i7 = 13;
    ((ConsentChangeReason)localObject).<init>("IFA_CHANGED", i7, "Consent needs to be reacquired because the IFA has changed");
    IFA_CHANGED = (ConsentChangeReason)localObject;
    localObject = new ConsentChangeReason[14];
    ConsentChangeReason localConsentChangeReason = GRANTED_BY_USER;
    localObject[0] = localConsentChangeReason;
    localConsentChangeReason = GRANTED_BY_WHITELISTED_PUB;
    localObject[i] = localConsentChangeReason;
    localConsentChangeReason = GRANTED_BY_NOT_WHITELISTED_PUB;
    localObject[j] = localConsentChangeReason;
    localConsentChangeReason = DENIED_BY_USER;
    localObject[k] = localConsentChangeReason;
    localConsentChangeReason = DENIED_BY_PUB;
    localObject[m] = localConsentChangeReason;
    localConsentChangeReason = DENIED_BY_DNT_ON;
    localObject[n] = localConsentChangeReason;
    localConsentChangeReason = DNT_OFF;
    localObject[i1] = localConsentChangeReason;
    localConsentChangeReason = REACQUIRE_BECAUSE_DNT_OFF;
    localObject[i2] = localConsentChangeReason;
    localConsentChangeReason = REACQUIRE_BECAUSE_PRIVACY_POLICY;
    localObject[i3] = localConsentChangeReason;
    localConsentChangeReason = REACUIRE_BECAUSE_VENDOR_LIST;
    localObject[i4] = localConsentChangeReason;
    localConsentChangeReason = REAQUIRE_BECAUSE_IAB_VENDOR_LIST;
    localObject[i5] = localConsentChangeReason;
    localConsentChangeReason = REVOKED_BY_SERVER;
    localObject[i6] = localConsentChangeReason;
    localConsentChangeReason = REACQUIRE_BY_SERVER;
    localObject[12] = localConsentChangeReason;
    localConsentChangeReason = IFA_CHANGED;
    localObject[i7] = localConsentChangeReason;
    b = (ConsentChangeReason[])localObject;
  }
  
  private ConsentChangeReason(String paramString1)
  {
    a = paramString1;
  }
  
  public final String getReason()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.ConsentChangeReason
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */