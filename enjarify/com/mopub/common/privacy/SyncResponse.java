package com.mopub.common.privacy;

import com.mopub.common.Preconditions;

public class SyncResponse
{
  final String a;
  private final boolean b;
  private final boolean c;
  private final boolean d;
  private final boolean e;
  private final boolean f;
  private final boolean g;
  private final String h;
  private final String i;
  private final String j;
  private final String k;
  private final String l;
  private final String m;
  private final String n;
  private final String o;
  
  private SyncResponse(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, String paramString13, String paramString14, String paramString15)
  {
    Preconditions.checkNotNull(paramString1);
    Preconditions.checkNotNull(paramString5);
    Preconditions.checkNotNull(paramString7);
    Preconditions.checkNotNull(paramString8);
    Preconditions.checkNotNull(paramString9);
    Preconditions.checkNotNull(paramString10);
    Preconditions.checkNotNull(paramString12);
    boolean bool = "0".equals(paramString1) ^ true;
    b = bool;
    bool = "1".equals(paramString2);
    c = bool;
    bool = "1".equals(paramString3);
    d = bool;
    bool = "1".equals(paramString4);
    e = bool;
    bool = "1".equals(paramString5);
    f = bool;
    bool = "1".equals(paramString6);
    g = bool;
    h = paramString7;
    i = paramString8;
    j = paramString9;
    k = paramString10;
    l = paramString11;
    m = paramString12;
    n = paramString13;
    a = paramString14;
    o = paramString15;
  }
  
  public String getCallAgainAfterSecs()
  {
    return n;
  }
  
  public String getConsentChangeReason()
  {
    return o;
  }
  
  public String getCurrentPrivacyPolicyLink()
  {
    return k;
  }
  
  public String getCurrentPrivacyPolicyVersion()
  {
    return j;
  }
  
  public String getCurrentVendorListIabFormat()
  {
    return l;
  }
  
  public String getCurrentVendorListIabHash()
  {
    return m;
  }
  
  public String getCurrentVendorListLink()
  {
    return i;
  }
  
  public String getCurrentVendorListVersion()
  {
    return h;
  }
  
  public boolean isForceExplicitNo()
  {
    return c;
  }
  
  public boolean isForceGdprApplies()
  {
    return g;
  }
  
  public boolean isGdprRegion()
  {
    return b;
  }
  
  public boolean isInvalidateConsent()
  {
    return d;
  }
  
  public boolean isReacquireConsent()
  {
    return e;
  }
  
  public boolean isWhitelisted()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.SyncResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */