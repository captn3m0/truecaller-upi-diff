package com.mopub.common.privacy;

import android.content.Context;
import android.text.TextUtils;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.MoPubNetworkError.Reason;
import com.mopub.network.MoPubRequest;
import com.mopub.volley.DefaultRetryPolicy;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Response;
import com.mopub.volley.toolbox.HttpHeaderParser;
import org.json.JSONException;
import org.json.JSONObject;

final class ConsentDialogRequest
  extends MoPubRequest
{
  private ConsentDialogRequest.Listener a;
  
  ConsentDialogRequest(Context paramContext, String paramString, ConsentDialogRequest.Listener paramListener)
  {
    super(paramContext, paramString, paramListener);
    a = paramListener;
    paramContext = new com/mopub/volley/DefaultRetryPolicy;
    paramContext.<init>(2500, 1, 1.0F);
    setRetryPolicy(paramContext);
    setShouldCache(false);
  }
  
  public final Response parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    String str = a(paramNetworkResponse);
    try
    {
      localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>(str);
      str = "dialog_html";
      str = ((JSONObject)localObject).getString(str);
      boolean bool = TextUtils.isEmpty(str);
      if (!bool)
      {
        localObject = new com/mopub/common/privacy/b;
        ((b)localObject).<init>(str);
        paramNetworkResponse = HttpHeaderParser.parseCacheHeaders(paramNetworkResponse);
        return Response.success(localObject, paramNetworkResponse);
      }
      paramNetworkResponse = new org/json/JSONException;
      str = "Empty HTML body";
      paramNetworkResponse.<init>(str);
      throw paramNetworkResponse;
    }
    catch (JSONException localJSONException)
    {
      Object localObject;
      for (;;) {}
    }
    paramNetworkResponse = new com/mopub/network/MoPubNetworkError;
    localObject = MoPubNetworkError.Reason.BAD_BODY;
    paramNetworkResponse.<init>("Unable to parse consent dialog request network response.", (MoPubNetworkError.Reason)localObject, null);
    return Response.error(paramNetworkResponse);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.ConsentDialogRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */