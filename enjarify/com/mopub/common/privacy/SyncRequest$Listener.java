package com.mopub.common.privacy;

import com.mopub.volley.Response.ErrorListener;

public abstract interface SyncRequest$Listener
  extends Response.ErrorListener
{
  public abstract void onSuccess(SyncResponse paramSyncResponse);
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.SyncRequest.Listener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */