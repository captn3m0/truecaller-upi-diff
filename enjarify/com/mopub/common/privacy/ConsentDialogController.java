package com.mopub.common.privacy;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.MoPubNetworkError.Reason;
import com.mopub.network.MoPubRequestQueue;
import com.mopub.network.Networking;
import com.mopub.volley.VolleyError;

public class ConsentDialogController
  implements ConsentDialogRequest.Listener
{
  final Context a;
  String b;
  volatile boolean c;
  volatile boolean d;
  private ConsentDialogListener e;
  private final Handler f;
  
  ConsentDialogController(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    paramContext = new android/os/Handler;
    paramContext.<init>();
    f = paramContext;
  }
  
  final void a()
  {
    d = false;
    c = false;
    e = null;
    b = null;
  }
  
  final void a(ConsentDialogListener paramConsentDialogListener, Boolean paramBoolean, c paramc)
  {
    try
    {
      Preconditions.checkNotNull(paramc);
      boolean bool1 = c;
      if (bool1)
      {
        if (paramConsentDialogListener != null)
        {
          paramBoolean = f;
          paramc = new com/mopub/common/privacy/ConsentDialogController$1;
          paramc.<init>(this, paramConsentDialogListener);
          paramBoolean.post(paramc);
        }
        return;
      }
      bool1 = d;
      if (bool1)
      {
        paramConsentDialogListener = "Already making a consent dialog load request.";
        MoPubLog.d(paramConsentDialogListener);
        return;
      }
      e = paramConsentDialogListener;
      boolean bool2 = true;
      d = bool2;
      paramConsentDialogListener = new com/mopub/common/privacy/ConsentDialogRequest;
      Context localContext1 = a;
      ConsentDialogUrlGenerator localConsentDialogUrlGenerator = new com/mopub/common/privacy/ConsentDialogUrlGenerator;
      Context localContext2 = a;
      String str = a;
      Object localObject = b;
      localObject = ((ConsentStatus)localObject).getValue();
      localConsentDialogUrlGenerator.<init>(localContext2, str, (String)localObject);
      a = paramBoolean;
      paramBoolean = paramc.getConsentedPrivacyPolicyVersion();
      d = paramBoolean;
      paramBoolean = paramc.getConsentedVendorListVersion();
      c = paramBoolean;
      boolean bool3 = paramc.isForceGdprApplies();
      b = bool3;
      paramBoolean = "ads.mopub.com";
      paramBoolean = localConsentDialogUrlGenerator.generateUrlString(paramBoolean);
      paramConsentDialogListener.<init>(localContext1, paramBoolean, this);
      paramBoolean = a;
      paramBoolean = Networking.getRequestQueue(paramBoolean);
      paramBoolean.add(paramConsentDialogListener);
      return;
    }
    finally {}
  }
  
  public void onErrorResponse(VolleyError paramVolleyError)
  {
    ConsentDialogListener localConsentDialogListener = e;
    a();
    if (localConsentDialogListener == null) {
      return;
    }
    int i = paramVolleyError instanceof MoPubNetworkError;
    if (i != 0)
    {
      int[] arrayOfInt = ConsentDialogController.2.a;
      paramVolleyError = ((MoPubNetworkError)paramVolleyError).getReason();
      int j = paramVolleyError.ordinal();
      j = arrayOfInt[j];
      i = 1;
      if (j == i)
      {
        paramVolleyError = MoPubErrorCode.INTERNAL_ERROR;
        localConsentDialogListener.onConsentDialogLoadFailed(paramVolleyError);
        return;
      }
    }
    paramVolleyError = MoPubErrorCode.UNSPECIFIED;
    localConsentDialogListener.onConsentDialogLoadFailed(paramVolleyError);
  }
  
  public void onSuccess(b paramb)
  {
    MoPubErrorCode localMoPubErrorCode = null;
    d = false;
    paramb = paramb.getHtml();
    b = paramb;
    paramb = b;
    boolean bool = TextUtils.isEmpty(paramb);
    if (bool)
    {
      c = false;
      paramb = e;
      if (paramb != null)
      {
        localMoPubErrorCode = MoPubErrorCode.INTERNAL_ERROR;
        paramb.onConsentDialogLoadFailed(localMoPubErrorCode);
      }
      return;
    }
    bool = true;
    c = bool;
    paramb = e;
    if (paramb != null) {
      paramb.onConsentDialogLoaded();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.ConsentDialogController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */