package com.mopub.common.privacy;

public enum ConsentStatus
{
  private final String a;
  
  static
  {
    Object localObject = new com/mopub/common/privacy/ConsentStatus;
    ((ConsentStatus)localObject).<init>("EXPLICIT_YES", 0, "explicit_yes");
    EXPLICIT_YES = (ConsentStatus)localObject;
    localObject = new com/mopub/common/privacy/ConsentStatus;
    int i = 1;
    ((ConsentStatus)localObject).<init>("EXPLICIT_NO", i, "explicit_no");
    EXPLICIT_NO = (ConsentStatus)localObject;
    localObject = new com/mopub/common/privacy/ConsentStatus;
    int j = 2;
    ((ConsentStatus)localObject).<init>("UNKNOWN", j, "unknown");
    UNKNOWN = (ConsentStatus)localObject;
    localObject = new com/mopub/common/privacy/ConsentStatus;
    int k = 3;
    ((ConsentStatus)localObject).<init>("POTENTIAL_WHITELIST", k, "potential_whitelist");
    POTENTIAL_WHITELIST = (ConsentStatus)localObject;
    localObject = new com/mopub/common/privacy/ConsentStatus;
    int m = 4;
    ((ConsentStatus)localObject).<init>("DNT", m, "dnt");
    DNT = (ConsentStatus)localObject;
    localObject = new ConsentStatus[5];
    ConsentStatus localConsentStatus = EXPLICIT_YES;
    localObject[0] = localConsentStatus;
    localConsentStatus = EXPLICIT_NO;
    localObject[i] = localConsentStatus;
    localConsentStatus = UNKNOWN;
    localObject[j] = localConsentStatus;
    localConsentStatus = POTENTIAL_WHITELIST;
    localObject[k] = localConsentStatus;
    localConsentStatus = DNT;
    localObject[m] = localConsentStatus;
    b = (ConsentStatus[])localObject;
  }
  
  private ConsentStatus(String paramString1)
  {
    a = paramString1;
  }
  
  public static ConsentStatus fromString(String paramString)
  {
    if (paramString == null) {
      return UNKNOWN;
    }
    ConsentStatus[] arrayOfConsentStatus = values();
    int i = arrayOfConsentStatus.length;
    int j = 0;
    while (j < i)
    {
      ConsentStatus localConsentStatus = arrayOfConsentStatus[j];
      String str = localConsentStatus.name();
      boolean bool = paramString.equals(str);
      if (bool) {
        return localConsentStatus;
      }
      j += 1;
    }
    return UNKNOWN;
  }
  
  public final String getValue()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.ConsentStatus
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */