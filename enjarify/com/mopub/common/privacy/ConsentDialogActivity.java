package com.mopub.common.privacy;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mopub.common.CloseableLayout.OnCloseListener;
import com.mopub.common.MoPub;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;

public class ConsentDialogActivity
  extends Activity
{
  a a;
  Handler b;
  ConsentStatus c;
  private Runnable d;
  
  static void a(Context paramContext, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      MoPubLog.e("ConsentDialogActivity htmlData can't be empty string.");
      return;
    }
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramString);
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str = "html-page-content";
    localBundle.putString(str, paramString);
    paramString = Intents.getStartActivityIntent(paramContext, ConsentDialogActivity.class, localBundle);
    try
    {
      Intents.startActivity(paramContext, paramString);
      return;
    }
    catch (ActivityNotFoundException|IntentNotResolvableException localActivityNotFoundException)
    {
      MoPubLog.e("ConsentDialogActivity not found - did you declare it in AndroidManifest.xml?");
    }
  }
  
  final void a(boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject != null)
    {
      Runnable localRunnable = d;
      ((Handler)localObject).removeCallbacks(localRunnable);
    }
    localObject = a;
    if (localObject != null) {
      ((a)localObject).setCloseVisible(paramBoolean);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getIntent();
    Object localObject = "html-page-content";
    String str = paramBundle.getStringExtra((String)localObject);
    boolean bool = TextUtils.isEmpty(str);
    if (bool)
    {
      MoPubLog.e("Web page for ConsentDialogActivity is empty");
      finish();
      return;
    }
    requestWindowFeature(1);
    getWindow().addFlags(1024);
    paramBundle = new com/mopub/common/privacy/a;
    paramBundle.<init>(this);
    a = paramBundle;
    paramBundle = a;
    localObject = new com/mopub/common/privacy/ConsentDialogActivity$1;
    ((ConsentDialogActivity.1)localObject).<init>(this);
    Preconditions.checkNotNull(localObject);
    d = ((a.a)localObject);
    paramBundle = new com/mopub/common/privacy/ConsentDialogActivity$2;
    paramBundle.<init>(this);
    d = paramBundle;
    paramBundle = a;
    setContentView(paramBundle);
    paramBundle = a;
    localObject = new com/mopub/common/privacy/ConsentDialogActivity$3;
    ((ConsentDialogActivity.3)localObject).<init>(this);
    Preconditions.checkNotNull(str);
    c = ((a.b)localObject);
    localObject = b;
    WebViewClient localWebViewClient = e;
    ((WebView)localObject).setWebViewClient(localWebViewClient);
    localObject = new com/mopub/common/privacy/a$1;
    ((a.1)localObject).<init>(paramBundle);
    paramBundle.setOnCloseListener((CloseableLayout.OnCloseListener)localObject);
    b.loadDataWithBaseURL("https://ads.mopub.com/", str, "text/html", "UTF-8", null);
  }
  
  protected void onDestroy()
  {
    Object localObject1 = MoPub.getPersonalInformationManager();
    if (localObject1 != null)
    {
      Object localObject2 = c;
      if (localObject2 != null)
      {
        Preconditions.checkNotNull(localObject2);
        Object localObject3 = PersonalInfoManager.6.a;
        int i = ((ConsentStatus)localObject2).ordinal();
        int j = localObject3[i];
        i = 1;
        switch (j)
        {
        default: 
          localObject1 = new java/lang/StringBuilder;
          localObject3 = "Invalid consent status: ";
          ((StringBuilder)localObject1).<init>((String)localObject3);
          ((StringBuilder)localObject1).append(localObject2);
          localObject2 = ". This is a bug with the use of changeConsentStateFromDialog.";
          ((StringBuilder)localObject1).append((String)localObject2);
          localObject1 = ((StringBuilder)localObject1).toString();
          MoPubLog.d((String)localObject1);
          break;
        case 2: 
          localObject3 = ConsentChangeReason.DENIED_BY_USER;
          ((PersonalInfoManager)localObject1).a((ConsentStatus)localObject2, (ConsentChangeReason)localObject3);
          ((PersonalInfoManager)localObject1).requestSync(i);
          break;
        case 1: 
          localObject3 = ConsentChangeReason.GRANTED_BY_USER;
          ((PersonalInfoManager)localObject1).a((ConsentStatus)localObject2, (ConsentChangeReason)localObject3);
          ((PersonalInfoManager)localObject1).requestSync(i);
        }
      }
    }
    super.onDestroy();
  }
  
  protected void onStart()
  {
    super.onStart();
    Handler localHandler = new android/os/Handler;
    localHandler.<init>();
    b = localHandler;
    localHandler = b;
    Runnable localRunnable = d;
    localHandler.postDelayed(localRunnable, 10000L);
  }
  
  protected void onStop()
  {
    super.onStop();
    a(true);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.ConsentDialogActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */