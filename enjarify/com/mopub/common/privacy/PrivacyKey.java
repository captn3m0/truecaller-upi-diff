package com.mopub.common.privacy;

public enum PrivacyKey
{
  private final String a;
  
  static
  {
    Object localObject = new com/mopub/common/privacy/PrivacyKey;
    ((PrivacyKey)localObject).<init>("IS_GDPR_REGION", 0, "is_gdpr_region");
    IS_GDPR_REGION = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int i = 1;
    ((PrivacyKey)localObject).<init>("IS_WHITELISTED", i, "is_whitelisted");
    IS_WHITELISTED = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int j = 2;
    ((PrivacyKey)localObject).<init>("FORCE_GDPR_APPLIES", j, "force_gdpr_applies");
    FORCE_GDPR_APPLIES = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int k = 3;
    ((PrivacyKey)localObject).<init>("FORCE_EXPLICIT_NO", k, "force_explicit_no");
    FORCE_EXPLICIT_NO = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int m = 4;
    ((PrivacyKey)localObject).<init>("INVALIDATE_CONSENT", m, "invalidate_consent");
    INVALIDATE_CONSENT = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int n = 5;
    ((PrivacyKey)localObject).<init>("REACQUIRE_CONSENT", n, "reacquire_consent");
    REACQUIRE_CONSENT = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int i1 = 6;
    ((PrivacyKey)localObject).<init>("EXTRAS", i1, "extras");
    EXTRAS = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int i2 = 7;
    ((PrivacyKey)localObject).<init>("CURRENT_VENDOR_LIST_VERSION", i2, "current_vendor_list_version");
    CURRENT_VENDOR_LIST_VERSION = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int i3 = 8;
    ((PrivacyKey)localObject).<init>("CURRENT_VENDOR_LIST_LINK", i3, "current_vendor_list_link");
    CURRENT_VENDOR_LIST_LINK = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int i4 = 9;
    ((PrivacyKey)localObject).<init>("CURRENT_PRIVACY_POLICY_VERSION", i4, "current_privacy_policy_version");
    CURRENT_PRIVACY_POLICY_VERSION = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int i5 = 10;
    ((PrivacyKey)localObject).<init>("CURRENT_PRIVACY_POLICY_LINK", i5, "current_privacy_policy_link");
    CURRENT_PRIVACY_POLICY_LINK = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int i6 = 11;
    ((PrivacyKey)localObject).<init>("CURRENT_VENDOR_LIST_IAB_FORMAT", i6, "current_vendor_list_iab_format");
    CURRENT_VENDOR_LIST_IAB_FORMAT = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    ((PrivacyKey)localObject).<init>("CURRENT_VENDOR_LIST_IAB_HASH", 12, "current_vendor_list_iab_hash");
    CURRENT_VENDOR_LIST_IAB_HASH = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    ((PrivacyKey)localObject).<init>("CALL_AGAIN_AFTER_SECS", 13, "call_again_after_secs");
    CALL_AGAIN_AFTER_SECS = (PrivacyKey)localObject;
    localObject = new com/mopub/common/privacy/PrivacyKey;
    int i7 = 14;
    ((PrivacyKey)localObject).<init>("CONSENT_CHANGE_REASON", i7, "consent_change_reason");
    CONSENT_CHANGE_REASON = (PrivacyKey)localObject;
    localObject = new PrivacyKey[15];
    PrivacyKey localPrivacyKey = IS_GDPR_REGION;
    localObject[0] = localPrivacyKey;
    localPrivacyKey = IS_WHITELISTED;
    localObject[i] = localPrivacyKey;
    localPrivacyKey = FORCE_GDPR_APPLIES;
    localObject[j] = localPrivacyKey;
    localPrivacyKey = FORCE_EXPLICIT_NO;
    localObject[k] = localPrivacyKey;
    localPrivacyKey = INVALIDATE_CONSENT;
    localObject[m] = localPrivacyKey;
    localPrivacyKey = REACQUIRE_CONSENT;
    localObject[n] = localPrivacyKey;
    localPrivacyKey = EXTRAS;
    localObject[i1] = localPrivacyKey;
    localPrivacyKey = CURRENT_VENDOR_LIST_VERSION;
    localObject[i2] = localPrivacyKey;
    localPrivacyKey = CURRENT_VENDOR_LIST_LINK;
    localObject[i3] = localPrivacyKey;
    localPrivacyKey = CURRENT_PRIVACY_POLICY_VERSION;
    localObject[i4] = localPrivacyKey;
    localPrivacyKey = CURRENT_PRIVACY_POLICY_LINK;
    localObject[i5] = localPrivacyKey;
    localPrivacyKey = CURRENT_VENDOR_LIST_IAB_FORMAT;
    localObject[i6] = localPrivacyKey;
    localPrivacyKey = CURRENT_VENDOR_LIST_IAB_HASH;
    localObject[12] = localPrivacyKey;
    localPrivacyKey = CALL_AGAIN_AFTER_SECS;
    localObject[13] = localPrivacyKey;
    localPrivacyKey = CONSENT_CHANGE_REASON;
    localObject[i7] = localPrivacyKey;
    b = (PrivacyKey[])localObject;
  }
  
  private PrivacyKey(String paramString1)
  {
    a = paramString1;
  }
  
  public final String getKey()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.PrivacyKey
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */