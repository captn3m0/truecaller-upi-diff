package com.mopub.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.mopub.common.factories.MethodBuilderFactory;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.AsyncTasks;
import com.mopub.common.util.Reflection;
import com.mopub.common.util.Reflection.MethodBuilder;

public class GpsHelper
{
  public static final int GOOGLE_PLAY_SUCCESS_CODE = 0;
  public static final String IS_LIMIT_AD_TRACKING_ENABLED_KEY = "isLimitAdTrackingEnabled";
  private static String a = "com.google.android.gms.ads.identifier.AdvertisingIdClient";
  
  private static String a(Object paramObject)
  {
    String str = "getId";
    try
    {
      paramObject = MethodBuilderFactory.create(paramObject, str);
      paramObject = ((Reflection.MethodBuilder)paramObject).execute();
      return (String)paramObject;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private static boolean b(Object paramObject)
  {
    String str = "isLimitAdTrackingEnabled";
    try
    {
      paramObject = MethodBuilderFactory.create(paramObject, str);
      paramObject = ((Reflection.MethodBuilder)paramObject).execute();
      paramObject = (Boolean)paramObject;
      if (paramObject != null) {
        return ((Boolean)paramObject).booleanValue();
      }
      return false;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public static void fetchAdvertisingInfoAsync(Context paramContext, GpsHelper.GpsHelperListener paramGpsHelperListener)
  {
    Object localObject = a;
    boolean bool = Reflection.classFound((String)localObject);
    if (!bool)
    {
      if (paramGpsHelperListener != null) {
        paramGpsHelperListener.onFetchAdInfoCompleted();
      }
      return;
    }
    try
    {
      localObject = new com/mopub/common/GpsHelper$a;
      ((GpsHelper.a)localObject).<init>(paramContext, paramGpsHelperListener);
      paramContext = null;
      paramContext = new Void[0];
      AsyncTasks.safeExecuteOnExecutor((AsyncTask)localObject, paramContext);
      return;
    }
    catch (Exception paramContext)
    {
      localObject = "Error executing FetchAdvertisingInfoTask";
      MoPubLog.d((String)localObject, paramContext);
      if (paramGpsHelperListener != null) {
        paramGpsHelperListener.onFetchAdInfoCompleted();
      }
    }
  }
  
  public static GpsHelper.AdvertisingInfo fetchAdvertisingInfoSync(Context paramContext)
  {
    GpsHelper.AdvertisingInfo localAdvertisingInfo = null;
    if (paramContext == null) {
      return null;
    }
    Object localObject1 = "getAdvertisingIdInfo";
    try
    {
      localObject1 = MethodBuilderFactory.create(null, (String)localObject1);
      Object localObject2 = a;
      localObject2 = Class.forName((String)localObject2);
      localObject1 = ((Reflection.MethodBuilder)localObject1).setStatic((Class)localObject2);
      localObject2 = Context.class;
      paramContext = ((Reflection.MethodBuilder)localObject1).addParam((Class)localObject2, paramContext);
      paramContext = paramContext.execute();
      localObject1 = a(paramContext);
      boolean bool = b(paramContext);
      localAdvertisingInfo = new com/mopub/common/GpsHelper$AdvertisingInfo;
      localAdvertisingInfo.<init>((String)localObject1, bool);
      return localAdvertisingInfo;
    }
    catch (Exception localException)
    {
      MoPubLog.d("Unable to obtain Google AdvertisingIdClient.Info via reflection.");
    }
    return null;
  }
  
  public static boolean isLimitAdTrackingEnabled(Context paramContext)
  {
    return SharedPreferencesHelper.getSharedPreferences(paramContext).getBoolean("isLimitAdTrackingEnabled", false);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.GpsHelper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */