package com.mopub.common;

import android.content.Context;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Streams;
import com.mopub.common.util.Utils;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CacheService
{
  private static DiskLruCache a;
  
  public static void clearAndNullCaches()
  {
    DiskLruCache localDiskLruCache = a;
    if (localDiskLruCache != null) {
      try
      {
        localDiskLruCache.delete();
        a = null;
        return;
      }
      catch (IOException localIOException)
      {
        a = null;
      }
    }
  }
  
  public static boolean containsKeyDiskCache(String paramString)
  {
    DiskLruCache localDiskLruCache = a;
    if (localDiskLruCache == null) {
      return false;
    }
    try
    {
      paramString = createValidDiskCacheKey(paramString);
      paramString = localDiskLruCache.get(paramString);
      return paramString != null;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public static String createValidDiskCacheKey(String paramString)
  {
    return Utils.sha1(paramString);
  }
  
  public static File getDiskCacheDirectory(Context paramContext)
  {
    paramContext = paramContext.getCacheDir();
    if (paramContext == null) {
      return null;
    }
    paramContext = paramContext.getPath();
    File localFile = new java/io/File;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    paramContext = File.separator;
    localStringBuilder.append(paramContext);
    localStringBuilder.append("mopub-cache");
    paramContext = localStringBuilder.toString();
    localFile.<init>(paramContext);
    return localFile;
  }
  
  public static DiskLruCache getDiskLruCache()
  {
    return a;
  }
  
  public static String getFilePathDiskCache(String paramString)
  {
    Object localObject1 = a;
    if (localObject1 == null) {
      return null;
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = a.getDirectory();
    ((StringBuilder)localObject1).append(localObject2);
    localObject2 = File.separator;
    ((StringBuilder)localObject1).append((String)localObject2);
    paramString = createValidDiskCacheKey(paramString);
    ((StringBuilder)localObject1).append(paramString);
    ((StringBuilder)localObject1).append(".0");
    return ((StringBuilder)localObject1).toString();
  }
  
  /* Error */
  public static byte[] getFromDiskCache(String paramString)
  {
    // Byte code:
    //   0: getstatic 12	com/mopub/common/CacheService:a	Lcom/mopub/common/DiskLruCache;
    //   3: astore_1
    //   4: aconst_null
    //   5: astore_2
    //   6: aload_1
    //   7: ifnonnull +5 -> 12
    //   10: aconst_null
    //   11: areturn
    //   12: aload_0
    //   13: invokestatic 23	com/mopub/common/CacheService:createValidDiskCacheKey	(Ljava/lang/String;)Ljava/lang/String;
    //   16: astore_0
    //   17: aload_1
    //   18: aload_0
    //   19: invokevirtual 27	com/mopub/common/DiskLruCache:get	(Ljava/lang/String;)Lcom/mopub/common/DiskLruCache$Snapshot;
    //   22: astore_0
    //   23: aload_0
    //   24: ifnonnull +13 -> 37
    //   27: aload_0
    //   28: ifnull +7 -> 35
    //   31: aload_0
    //   32: invokevirtual 79	com/mopub/common/DiskLruCache$Snapshot:close	()V
    //   35: aconst_null
    //   36: areturn
    //   37: iconst_0
    //   38: istore_3
    //   39: aconst_null
    //   40: astore_1
    //   41: aload_0
    //   42: iconst_0
    //   43: invokevirtual 83	com/mopub/common/DiskLruCache$Snapshot:getInputStream	(I)Ljava/io/InputStream;
    //   46: astore 4
    //   48: aload 4
    //   50: ifnull +49 -> 99
    //   53: aload_0
    //   54: iconst_0
    //   55: invokevirtual 87	com/mopub/common/DiskLruCache$Snapshot:getLength	(I)J
    //   58: lstore 5
    //   60: lload 5
    //   62: l2i
    //   63: istore_3
    //   64: iload_3
    //   65: newarray <illegal type>
    //   67: astore_2
    //   68: new 89	java/io/BufferedInputStream
    //   71: astore_1
    //   72: aload_1
    //   73: aload 4
    //   75: invokespecial 92	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   78: aload_1
    //   79: aload_2
    //   80: invokestatic 98	com/mopub/common/util/Streams:readStream	(Ljava/io/InputStream;[B)V
    //   83: aload_1
    //   84: invokestatic 102	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   87: goto +12 -> 99
    //   90: astore 4
    //   92: aload_1
    //   93: invokestatic 102	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   96: aload 4
    //   98: athrow
    //   99: aload_0
    //   100: ifnull +7 -> 107
    //   103: aload_0
    //   104: invokevirtual 79	com/mopub/common/DiskLruCache$Snapshot:close	()V
    //   107: aload_2
    //   108: astore_0
    //   109: goto +46 -> 155
    //   112: astore_1
    //   113: goto +44 -> 157
    //   116: astore_1
    //   117: aload_2
    //   118: astore 7
    //   120: aload_0
    //   121: astore_2
    //   122: aload 7
    //   124: astore_0
    //   125: goto +12 -> 137
    //   128: astore_1
    //   129: aload_2
    //   130: astore_0
    //   131: goto +26 -> 157
    //   134: astore_1
    //   135: aconst_null
    //   136: astore_0
    //   137: ldc 104
    //   139: astore 4
    //   141: aload 4
    //   143: aload_1
    //   144: invokestatic 110	com/mopub/common/logging/MoPubLog:d	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   147: aload_2
    //   148: ifnull +7 -> 155
    //   151: aload_2
    //   152: invokevirtual 79	com/mopub/common/DiskLruCache$Snapshot:close	()V
    //   155: aload_0
    //   156: areturn
    //   157: aload_0
    //   158: ifnull +7 -> 165
    //   161: aload_0
    //   162: invokevirtual 79	com/mopub/common/DiskLruCache$Snapshot:close	()V
    //   165: aload_1
    //   166: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	167	0	paramString	String
    //   3	90	1	localObject1	Object
    //   112	1	1	localObject2	Object
    //   116	1	1	localException1	Exception
    //   128	1	1	localObject3	Object
    //   134	32	1	localException2	Exception
    //   5	147	2	localObject4	Object
    //   38	27	3	i	int
    //   46	28	4	localInputStream	InputStream
    //   90	7	4	localObject5	Object
    //   139	3	4	str	String
    //   58	3	5	l	long
    //   118	5	7	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   79	83	90	finally
    //   42	46	112	finally
    //   54	58	112	finally
    //   64	67	112	finally
    //   68	71	112	finally
    //   73	78	112	finally
    //   83	87	112	finally
    //   92	96	112	finally
    //   96	99	112	finally
    //   42	46	116	java/lang/Exception
    //   54	58	116	java/lang/Exception
    //   64	67	116	java/lang/Exception
    //   68	71	116	java/lang/Exception
    //   73	78	116	java/lang/Exception
    //   83	87	116	java/lang/Exception
    //   92	96	116	java/lang/Exception
    //   96	99	116	java/lang/Exception
    //   12	16	128	finally
    //   18	22	128	finally
    //   143	147	128	finally
    //   12	16	134	java/lang/Exception
    //   18	22	134	java/lang/Exception
  }
  
  public static void getFromDiskCacheAsync(String paramString, CacheService.DiskLruCacheGetListener paramDiskLruCacheGetListener)
  {
    CacheService.a locala = new com/mopub/common/CacheService$a;
    locala.<init>(paramString, paramDiskLruCacheGetListener);
    paramString = new Void[0];
    locala.execute(paramString);
  }
  
  public static void initialize(Context paramContext)
  {
    initializeDiskCache(paramContext);
  }
  
  public static boolean initializeDiskCache(Context paramContext)
  {
    if (paramContext == null) {
      return false;
    }
    DiskLruCache localDiskLruCache = a;
    int i = 1;
    if (localDiskLruCache == null)
    {
      paramContext = getDiskCacheDirectory(paramContext);
      if (paramContext == null) {
        return false;
      }
      long l = DeviceUtils.diskCacheSizeBytes(paramContext);
      try
      {
        paramContext = DiskLruCache.open(paramContext, i, i, l);
        a = paramContext;
      }
      catch (IOException paramContext)
      {
        MoPubLog.d("Unable to create DiskLruCache", paramContext);
        return false;
      }
    }
    return i;
  }
  
  public static boolean putToDiskCache(String paramString, InputStream paramInputStream)
  {
    Object localObject = a;
    if (localObject == null) {
      return false;
    }
    DiskLruCache.Editor localEditor = null;
    try
    {
      paramString = createValidDiskCacheKey(paramString);
      localEditor = ((DiskLruCache)localObject).edit(paramString);
      if (localEditor == null) {
        return false;
      }
      paramString = new java/io/BufferedOutputStream;
      localObject = localEditor.newOutputStream(0);
      paramString.<init>((OutputStream)localObject);
      Streams.copyContent(paramInputStream, paramString);
      paramString.flush();
      paramString.close();
      paramString = a;
      paramString.flush();
      localEditor.commit();
      return true;
    }
    catch (Exception paramString)
    {
      paramInputStream = "Unable to put to DiskLruCache";
      MoPubLog.d(paramInputStream, paramString);
      if (localEditor == null) {}
    }
    try
    {
      localEditor.abort();
      return false;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
  
  public static boolean putToDiskCache(String paramString, byte[] paramArrayOfByte)
  {
    ByteArrayInputStream localByteArrayInputStream = new java/io/ByteArrayInputStream;
    localByteArrayInputStream.<init>(paramArrayOfByte);
    return putToDiskCache(paramString, localByteArrayInputStream);
  }
  
  public static void putToDiskCacheAsync(String paramString, byte[] paramArrayOfByte)
  {
    CacheService.b localb = new com/mopub/common/CacheService$b;
    localb.<init>(paramString, paramArrayOfByte);
    paramString = new Void[0];
    localb.execute(paramString);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.CacheService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */