package com.mopub.common;

import android.app.Activity;

public class BaseLifecycleListener
  implements LifecycleListener
{
  public void onBackPressed(Activity paramActivity) {}
  
  public void onCreate(Activity paramActivity) {}
  
  public void onDestroy(Activity paramActivity) {}
  
  public void onPause(Activity paramActivity) {}
  
  public void onRestart(Activity paramActivity) {}
  
  public void onResume(Activity paramActivity) {}
  
  public void onStart(Activity paramActivity) {}
  
  public void onStop(Activity paramActivity) {}
}

/* Location:
 * Qualified Name:     com.mopub.common.BaseLifecycleListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */