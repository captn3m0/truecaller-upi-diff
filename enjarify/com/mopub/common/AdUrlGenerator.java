package com.mopub.common;

import android.content.Context;
import android.graphics.Point;
import android.location.Location;
import android.text.TextUtils;
import com.mopub.common.privacy.ConsentData;
import com.mopub.common.privacy.ConsentStatus;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.common.util.DateAndTime;

public abstract class AdUrlGenerator
  extends BaseUrlGenerator
{
  protected Context a;
  protected String b;
  protected String c;
  protected String d;
  protected Location e;
  private final PersonalInfoManager g;
  private final ConsentData h;
  
  public AdUrlGenerator(Context paramContext)
  {
    a = paramContext;
    paramContext = MoPub.getPersonalInformationManager();
    g = paramContext;
    paramContext = g;
    if (paramContext == null)
    {
      h = null;
      return;
    }
    paramContext = paramContext.getConsentData();
    h = paramContext;
  }
  
  private static int d(String paramString)
  {
    int i = paramString.length();
    return Math.min(3, i);
  }
  
  protected final void a()
  {
    b("mr", "1");
  }
  
  protected final void a(ClientMetadata paramClientMetadata)
  {
    Object localObject1 = b;
    b("id", (String)localObject1);
    localObject1 = paramClientMetadata.getSdkVersion();
    b("nv", (String)localObject1);
    int i = 3;
    float f1 = 4.2E-45F;
    localObject1 = new String[i];
    Object localObject2 = paramClientMetadata.getDeviceManufacturer();
    String str1 = null;
    localObject1[0] = localObject2;
    localObject2 = paramClientMetadata.getDeviceModel();
    localObject1[1] = localObject2;
    localObject2 = paramClientMetadata.getDeviceProduct();
    int j = 2;
    localObject1[j] = localObject2;
    a((String[])localObject1);
    localObject1 = paramClientMetadata.getAppPackageName();
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool2)
    {
      localObject2 = "bundle";
      b((String)localObject2, (String)localObject1);
    }
    localObject1 = c;
    localObject2 = "q";
    b((String)localObject2, (String)localObject1);
    boolean bool1 = MoPub.canCollectPersonalInformation();
    if (bool1)
    {
      localObject1 = d;
      bool2 = MoPub.canCollectPersonalInformation();
      if (bool2)
      {
        localObject2 = "user_data_q";
        b((String)localObject2, (String)localObject1);
      }
      localObject1 = e;
      bool2 = MoPub.canCollectPersonalInformation();
      if (bool2)
      {
        localObject2 = a;
        j = MoPub.getLocationPrecision();
        Object localObject3 = MoPub.getLocationAwareness();
        localObject2 = LocationService.getLastKnownLocation((Context)localObject2, j, (MoPub.LocationAwareness)localObject3);
        if (localObject2 != null) {
          if (localObject1 != null)
          {
            long l1 = ((Location)localObject2).getTime();
            long l2 = ((Location)localObject1).getTime();
            boolean bool3 = l1 < l2;
            if (bool3) {}
          }
          else
          {
            localObject1 = localObject2;
          }
        }
        if (localObject1 != null)
        {
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          double d1 = ((Location)localObject1).getLatitude();
          ((StringBuilder)localObject3).append(d1);
          String str2 = ",";
          ((StringBuilder)localObject3).append(str2);
          d1 = ((Location)localObject1).getLongitude();
          ((StringBuilder)localObject3).append(d1);
          localObject3 = ((StringBuilder)localObject3).toString();
          b("ll", (String)localObject3);
          float f2 = ((Location)localObject1).getAccuracy();
          localObject3 = String.valueOf((int)f2);
          b("lla", (String)localObject3);
          String str3 = "llf";
          Preconditions.checkNotNull(localObject1);
          long l3 = ((Location)localObject1).getTime();
          long l4 = System.currentTimeMillis() - l3;
          int m = (int)l4;
          localObject3 = String.valueOf(m);
          b(str3, (String)localObject3);
          if (localObject1 == localObject2)
          {
            localObject1 = "llsdk";
            localObject2 = "1";
            b((String)localObject1, (String)localObject2);
          }
        }
      }
    }
    localObject1 = DateAndTime.getTimeZoneOffsetString();
    b("z", (String)localObject1);
    localObject1 = paramClientMetadata.getOrientationString();
    b("o", (String)localObject1);
    localObject1 = paramClientMetadata.getDeviceDimensions();
    a((Point)localObject1);
    f1 = paramClientMetadata.getDensity();
    localObject2 = "sc";
    localObject1 = String.valueOf(f1);
    b((String)localObject2, (String)localObject1);
    localObject1 = paramClientMetadata.getNetworkOperatorForUrl();
    int k;
    if (localObject1 == null)
    {
      localObject2 = "";
    }
    else
    {
      k = d((String)localObject1);
      localObject2 = ((String)localObject1).substring(0, k);
    }
    str1 = "mcc";
    b(str1, (String)localObject2);
    if (localObject1 == null)
    {
      localObject1 = "";
    }
    else
    {
      k = d((String)localObject1);
      localObject1 = ((String)localObject1).substring(k);
    }
    b("mnc", (String)localObject1);
    localObject1 = paramClientMetadata.getIsoCountryCode();
    b("iso", (String)localObject1);
    localObject1 = paramClientMetadata.getNetworkOperatorName();
    b("cn", (String)localObject1);
    localObject1 = paramClientMetadata.getActiveNetworkType();
    localObject2 = "ct";
    localObject1 = ((ClientMetadata.MoPubNetworkType)localObject1).toString();
    b((String)localObject2, (String)localObject1);
    paramClientMetadata = paramClientMetadata.getAppVersion();
    c(paramClientMetadata);
    paramClientMetadata = MoPub.a(a);
    localObject1 = "abt";
    b((String)localObject1, paramClientMetadata);
    b();
    paramClientMetadata = g;
    if (paramClientMetadata != null)
    {
      localObject1 = "gdpr_applies";
      paramClientMetadata = paramClientMetadata.gdprApplies();
      a((String)localObject1, paramClientMetadata);
    }
    paramClientMetadata = h;
    if (paramClientMetadata != null)
    {
      localObject1 = "force_gdpr_applies";
      boolean bool4 = paramClientMetadata.isForceGdprApplies();
      paramClientMetadata = Boolean.valueOf(bool4);
      a((String)localObject1, paramClientMetadata);
    }
    paramClientMetadata = g;
    if (paramClientMetadata != null)
    {
      localObject1 = "current_consent_status";
      paramClientMetadata = paramClientMetadata.getPersonalInfoConsentStatus().getValue();
      b((String)localObject1, paramClientMetadata);
    }
    paramClientMetadata = h;
    if (paramClientMetadata != null)
    {
      localObject1 = "consented_privacy_policy_version";
      paramClientMetadata = paramClientMetadata.getConsentedPrivacyPolicyVersion();
      b((String)localObject1, paramClientMetadata);
    }
    paramClientMetadata = h;
    if (paramClientMetadata != null)
    {
      localObject1 = "consented_vendor_list_version";
      paramClientMetadata = paramClientMetadata.getConsentedVendorListVersion();
      b((String)localObject1, paramClientMetadata);
    }
  }
  
  protected final void a(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    b("vv", paramString);
  }
  
  public AdUrlGenerator withAdUnitId(String paramString)
  {
    b = paramString;
    return this;
  }
  
  public AdUrlGenerator withFacebookSupported(boolean paramBoolean)
  {
    return this;
  }
  
  public AdUrlGenerator withKeywords(String paramString)
  {
    c = paramString;
    return this;
  }
  
  public AdUrlGenerator withLocation(Location paramLocation)
  {
    e = paramLocation;
    return this;
  }
  
  public AdUrlGenerator withUserDataKeywords(String paramString)
  {
    d = paramString;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.AdUrlGenerator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */