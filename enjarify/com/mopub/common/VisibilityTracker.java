package com.mopub.common;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Views;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;

public class VisibilityTracker
{
  final ViewTreeObserver.OnPreDrawListener a;
  WeakReference b;
  private final ArrayList c;
  private long d = 0L;
  private final Map e;
  private final VisibilityTracker.VisibilityChecker f;
  private VisibilityTracker.VisibilityTrackerListener g;
  private final VisibilityTracker.b h;
  private final Handler i;
  private boolean j;
  
  public VisibilityTracker(Context paramContext)
  {
    this(paramContext, localWeakHashMap, localVisibilityChecker, localHandler);
  }
  
  private VisibilityTracker(Context paramContext, Map paramMap, VisibilityTracker.VisibilityChecker paramVisibilityChecker, Handler paramHandler)
  {
    e = paramMap;
    f = paramVisibilityChecker;
    i = paramHandler;
    paramMap = new com/mopub/common/VisibilityTracker$b;
    paramMap.<init>(this);
    h = paramMap;
    paramMap = new java/util/ArrayList;
    paramMap.<init>(50);
    c = paramMap;
    paramMap = new com/mopub/common/VisibilityTracker$1;
    paramMap.<init>(this);
    a = paramMap;
    paramMap = new java/lang/ref/WeakReference;
    paramMap.<init>(null);
    b = paramMap;
    a(paramContext, null);
  }
  
  private void a(Context paramContext, View paramView)
  {
    ViewTreeObserver localViewTreeObserver = (ViewTreeObserver)b.get();
    if (localViewTreeObserver != null)
    {
      boolean bool1 = localViewTreeObserver.isAlive();
      if (bool1) {
        return;
      }
    }
    paramContext = Views.getTopmostView(paramContext, paramView);
    if (paramContext == null)
    {
      MoPubLog.d("Unable to set Visibility Tracker due to no available root view.");
      return;
    }
    paramContext = paramContext.getViewTreeObserver();
    boolean bool2 = paramContext.isAlive();
    if (!bool2)
    {
      MoPubLog.w("Visibility Tracker was unable to track views because the root view tree observer was not alive");
      return;
    }
    paramView = new java/lang/ref/WeakReference;
    paramView.<init>(paramContext);
    b = paramView;
    paramView = a;
    paramContext.addOnPreDrawListener(paramView);
  }
  
  public void addView(View paramView, int paramInt, Integer paramInteger)
  {
    addView(paramView, paramView, paramInt, paramInteger);
  }
  
  public void addView(View paramView1, View paramView2, int paramInt1, int paramInt2, Integer paramInteger)
  {
    Object localObject1 = paramView2.getContext();
    a((Context)localObject1, paramView2);
    localObject1 = (VisibilityTracker.a)e.get(paramView2);
    if (localObject1 == null)
    {
      localObject1 = new com/mopub/common/VisibilityTracker$a;
      ((VisibilityTracker.a)localObject1).<init>();
      Map localMap = e;
      localMap.put(paramView2, localObject1);
      scheduleVisibilityCheck();
    }
    int k = Math.min(paramInt2, paramInt1);
    d = paramView1;
    a = paramInt1;
    b = k;
    long l1 = d;
    c = l1;
    e = paramInteger;
    l1 += 1L;
    d = l1;
    l1 = d;
    long l2 = 50;
    long l3 = l1 % l2;
    long l4 = 0L;
    boolean bool2 = l3 < l4;
    if (!bool2)
    {
      l1 -= l2;
      Iterator localIterator = e.entrySet().iterator();
      for (;;)
      {
        paramInt2 = localIterator.hasNext();
        if (paramInt2 == 0) {
          break;
        }
        Object localObject2 = (Map.Entry)localIterator.next();
        paramInteger = (VisibilityTracker.a)((Map.Entry)localObject2).getValue();
        l3 = c;
        bool2 = l3 < l1;
        if (bool2)
        {
          paramInteger = c;
          localObject2 = ((Map.Entry)localObject2).getKey();
          paramInteger.add(localObject2);
        }
      }
      paramView1 = c.iterator();
      for (;;)
      {
        boolean bool1 = paramView1.hasNext();
        if (!bool1) {
          break;
        }
        paramView2 = (View)paramView1.next();
        removeView(paramView2);
      }
      paramView1 = c;
      paramView1.clear();
    }
  }
  
  public void addView(View paramView1, View paramView2, int paramInt, Integer paramInteger)
  {
    addView(paramView1, paramView2, paramInt, paramInt, paramInteger);
  }
  
  public void clear()
  {
    e.clear();
    i.removeMessages(0);
    j = false;
  }
  
  public void destroy()
  {
    clear();
    ViewTreeObserver localViewTreeObserver = (ViewTreeObserver)b.get();
    if (localViewTreeObserver != null)
    {
      boolean bool = localViewTreeObserver.isAlive();
      if (bool)
      {
        ViewTreeObserver.OnPreDrawListener localOnPreDrawListener = a;
        localViewTreeObserver.removeOnPreDrawListener(localOnPreDrawListener);
      }
    }
    b.clear();
    g = null;
  }
  
  public void removeView(View paramView)
  {
    e.remove(paramView);
  }
  
  public void scheduleVisibilityCheck()
  {
    boolean bool = j;
    if (bool) {
      return;
    }
    j = true;
    Handler localHandler = i;
    VisibilityTracker.b localb = h;
    localHandler.postDelayed(localb, 100);
  }
  
  public void setVisibilityTrackerListener(VisibilityTracker.VisibilityTrackerListener paramVisibilityTrackerListener)
  {
    g = paramVisibilityTrackerListener;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.VisibilityTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */