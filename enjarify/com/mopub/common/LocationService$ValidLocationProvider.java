package com.mopub.common;

public enum LocationService$ValidLocationProvider
{
  final String a;
  
  static
  {
    Object localObject = new com/mopub/common/LocationService$ValidLocationProvider;
    ((ValidLocationProvider)localObject).<init>("NETWORK", 0, "network");
    NETWORK = (ValidLocationProvider)localObject;
    localObject = new com/mopub/common/LocationService$ValidLocationProvider;
    int i = 1;
    ((ValidLocationProvider)localObject).<init>("GPS", i, "gps");
    GPS = (ValidLocationProvider)localObject;
    localObject = new ValidLocationProvider[2];
    ValidLocationProvider localValidLocationProvider = NETWORK;
    localObject[0] = localValidLocationProvider;
    localValidLocationProvider = GPS;
    localObject[i] = localValidLocationProvider;
    b = (ValidLocationProvider[])localObject;
  }
  
  private LocationService$ValidLocationProvider(String paramString1)
  {
    a = paramString1;
  }
  
  public final String toString()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.LocationService.ValidLocationProvider
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */