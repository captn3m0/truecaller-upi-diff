package com.mopub.common;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import com.mopub.common.logging.MoPubLog;
import java.math.BigDecimal;

public class LocationService
{
  private static volatile LocationService c;
  Location a;
  long b;
  
  private static Location a(Context paramContext, LocationService.ValidLocationProvider paramValidLocationProvider)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramValidLocationProvider);
    boolean bool = MoPub.canCollectPersonalInformation();
    if (!bool) {
      return null;
    }
    bool = LocationService.ValidLocationProvider.a(paramValidLocationProvider, paramContext);
    if (!bool) {
      return null;
    }
    String str = "location";
    paramContext = (LocationManager)paramContext.getSystemService(str);
    try
    {
      str = paramValidLocationProvider.toString();
      return paramContext.getLastKnownLocation(str);
    }
    catch (NullPointerException localNullPointerException)
    {
      paramContext = new java/lang/StringBuilder;
      str = "Failed to retrieve location: device has no ";
      paramContext.<init>(str);
      paramValidLocationProvider = paramValidLocationProvider.toString();
      paramContext.append(paramValidLocationProvider);
      paramValidLocationProvider = " location provider.";
      paramContext.append(paramValidLocationProvider);
      paramContext = paramContext.toString();
      MoPubLog.d(paramContext);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      paramContext = new java/lang/StringBuilder;
      str = "Failed to retrieve location: device has no ";
      paramContext.<init>(str);
      paramValidLocationProvider = paramValidLocationProvider.toString();
      paramContext.append(paramValidLocationProvider);
      paramValidLocationProvider = " location provider.";
      paramContext.append(paramValidLocationProvider);
      paramContext = paramContext.toString();
      MoPubLog.d(paramContext);
    }
    catch (SecurityException localSecurityException)
    {
      paramContext = new java/lang/StringBuilder;
      str = "Failed to retrieve location from ";
      paramContext.<init>(str);
      paramValidLocationProvider = paramValidLocationProvider.toString();
      paramContext.append(paramValidLocationProvider);
      paramValidLocationProvider = " provider: access appears to be disabled.";
      paramContext.append(paramValidLocationProvider);
      paramContext = paramContext.toString();
      MoPubLog.d(paramContext);
    }
    return null;
  }
  
  private static LocationService a()
  {
    LocationService localLocationService1 = c;
    if (localLocationService1 == null) {
      synchronized (LocationService.class)
      {
        localLocationService1 = c;
        if (localLocationService1 == null)
        {
          localLocationService1 = new com/mopub/common/LocationService;
          localLocationService1.<init>();
          c = localLocationService1;
        }
      }
    }
    return localLocationService2;
  }
  
  public static void clearLastKnownLocation()
  {
    aa = null;
  }
  
  public static Location getLastKnownLocation(Context paramContext, int paramInt, MoPub.LocationAwareness paramLocationAwareness)
  {
    boolean bool1 = MoPub.canCollectPersonalInformation();
    boolean bool2 = false;
    Object localObject1 = null;
    if (!bool1) {
      return null;
    }
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramLocationAwareness);
    Object localObject2 = MoPub.LocationAwareness.DISABLED;
    if (paramLocationAwareness == localObject2) {
      return null;
    }
    localObject2 = a();
    localObject1 = a();
    Object localObject3 = a;
    long l1;
    long l2;
    if (localObject3 != null)
    {
      l1 = SystemClock.elapsedRealtime();
      l2 = b;
      l1 -= l2;
      l2 = MoPub.getMinimumLocationRefreshTimeMillis();
      bool2 = l1 < l2;
      if (!bool2)
      {
        bool2 = true;
        break label107;
      }
    }
    bool2 = false;
    localObject1 = null;
    label107:
    if (bool2) {
      return a;
    }
    localObject1 = LocationService.ValidLocationProvider.GPS;
    localObject1 = a(paramContext, (LocationService.ValidLocationProvider)localObject1);
    localObject3 = LocationService.ValidLocationProvider.NETWORK;
    paramContext = a(paramContext, (LocationService.ValidLocationProvider)localObject3);
    if (localObject1 != null) {
      if (paramContext != null)
      {
        l1 = ((Location)localObject1).getTime();
        l2 = paramContext.getTime();
        boolean bool3 = l1 < l2;
        if (!bool3) {}
      }
      else
      {
        paramContext = (Context)localObject1;
      }
    }
    localObject1 = MoPub.LocationAwareness.TRUNCATED;
    if ((paramLocationAwareness == localObject1) && (paramContext != null) && (paramInt >= 0))
    {
      double d1 = paramContext.getLatitude();
      paramLocationAwareness = BigDecimal.valueOf(d1);
      int i = 5;
      double d2 = paramLocationAwareness.setScale(paramInt, i).doubleValue();
      paramContext.setLatitude(d2);
      d2 = paramContext.getLongitude();
      paramLocationAwareness = BigDecimal.valueOf(d2);
      BigDecimal localBigDecimal = paramLocationAwareness.setScale(paramInt, i);
      double d3 = localBigDecimal.doubleValue();
      paramContext.setLongitude(d3);
    }
    a = paramContext;
    long l3 = SystemClock.elapsedRealtime();
    b = l3;
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.LocationService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */