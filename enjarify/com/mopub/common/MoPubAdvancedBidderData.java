package com.mopub.common;

import com.mopub.common.logging.MoPubLog;
import org.json.JSONException;
import org.json.JSONObject;

public class MoPubAdvancedBidderData
{
  final String a;
  final String b;
  
  public MoPubAdvancedBidderData(String paramString1, String paramString2)
  {
    Preconditions.checkNotNull(paramString1);
    Preconditions.checkNotNull(paramString2);
    a = paramString1;
    b = paramString2;
  }
  
  public JSONObject toJson()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    Object localObject = "token";
    try
    {
      str = a;
      localJSONObject.put((String)localObject, str);
    }
    catch (JSONException localJSONException)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("Invalid token format: ");
      String str = a;
      ((StringBuilder)localObject).append(str);
      localObject = ((StringBuilder)localObject).toString();
      MoPubLog.e((String)localObject);
    }
    return localJSONObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPubAdvancedBidderData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */