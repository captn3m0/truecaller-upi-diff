package com.mopub.common;

import android.content.Context;
import android.net.Uri;
import com.mopub.common.util.Intents;

 enum UrlAction$6
{
  UrlAction$6()
  {
    super(paramString, 4, true, (byte)0);
  }
  
  protected final void a(Context paramContext, Uri paramUri, UrlHandler paramUrlHandler, String paramString)
  {
    Intents.launchApplicationUrl(paramContext, paramUri);
  }
  
  public final boolean shouldTryHandlingUrl(Uri paramUri)
  {
    String str1 = paramUri.getScheme();
    String str2 = paramUri.getHost();
    String str3 = "play.google.com";
    boolean bool1 = str3.equalsIgnoreCase(str2);
    if (!bool1)
    {
      str3 = "market.android.com";
      boolean bool2 = str3.equalsIgnoreCase(str2);
      if (!bool2)
      {
        str2 = "market";
        boolean bool3 = str2.equalsIgnoreCase(str1);
        if (!bool3)
        {
          str1 = paramUri.toString().toLowerCase();
          str2 = "play.google.com/";
          bool3 = str1.startsWith(str2);
          if (!bool3)
          {
            paramUri = paramUri.toString().toLowerCase();
            str1 = "market.android.com/";
            boolean bool4 = paramUri.startsWith(str1);
            if (!bool4) {
              return false;
            }
          }
        }
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlAction.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */