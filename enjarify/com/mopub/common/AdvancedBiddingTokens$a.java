package com.mopub.common;

import android.os.AsyncTask;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Reflection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class AdvancedBiddingTokens$a
  extends AsyncTask
{
  private final List a;
  private final a b;
  
  AdvancedBiddingTokens$a(List paramList, a parama)
  {
    Preconditions.checkNotNull(paramList);
    Preconditions.checkNotNull(parama);
    a = paramList;
    b = parama;
  }
  
  private List a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (Class)localIterator.next();
      try
      {
        localObject2 = ((Class)localObject1).getName();
        localObject3 = MoPubAdvancedBidder.class;
        localObject2 = Reflection.instantiateClassWithEmptyConstructor((String)localObject2, (Class)localObject3);
        localObject2 = (MoPubAdvancedBidder)localObject2;
        localArrayList.add(localObject2);
      }
      catch (Exception localException)
      {
        Object localObject2 = new java/lang/StringBuilder;
        Object localObject3 = "Unable to find class ";
        ((StringBuilder)localObject2).<init>((String)localObject3);
        localObject1 = ((Class)localObject1).getName();
        ((StringBuilder)localObject2).append((String)localObject1);
        localObject1 = ((StringBuilder)localObject2).toString();
        MoPubLog.e((String)localObject1);
      }
    }
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.AdvancedBiddingTokens.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */