package com.mopub.common;

import android.content.Context;

final class UrlHandler$3
  implements UrlResolutionTask.a
{
  UrlHandler$3(UrlHandler paramUrlHandler, Context paramContext, boolean paramBoolean, Iterable paramIterable, String paramString) {}
  
  public final void onFailure(String paramString, Throwable paramThrowable)
  {
    UrlHandler.a(e);
    UrlHandler localUrlHandler = e;
    String str = d;
    UrlHandler.a(localUrlHandler, str, paramString, paramThrowable);
  }
  
  public final void onSuccess(String paramString)
  {
    UrlHandler.a(e);
    UrlHandler localUrlHandler = e;
    Context localContext = a;
    boolean bool = b;
    Iterable localIterable = c;
    localUrlHandler.handleResolvedUrl(localContext, paramString, bool, localIterable);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlHandler.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */