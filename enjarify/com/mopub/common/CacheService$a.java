package com.mopub.common;

import android.os.AsyncTask;

final class CacheService$a
  extends AsyncTask
{
  private final CacheService.DiskLruCacheGetListener a;
  private final String b;
  
  CacheService$a(String paramString, CacheService.DiskLruCacheGetListener paramDiskLruCacheGetListener)
  {
    a = paramDiskLruCacheGetListener;
    b = paramString;
  }
  
  protected final void onCancelled()
  {
    CacheService.DiskLruCacheGetListener localDiskLruCacheGetListener = a;
    if (localDiskLruCacheGetListener != null)
    {
      String str = b;
      localDiskLruCacheGetListener.onComplete(str, null);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.CacheService.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */