package com.mopub.common.util;

import android.text.TextUtils;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Strings
{
  private static Pattern a = Pattern.compile("((\\d{1,2})|(100))%");
  private static Pattern b = Pattern.compile("\\d{2}:\\d{2}:\\d{2}(.\\d{3})?");
  
  public static String fromStream(InputStream paramInputStream)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = 4096;
    byte[] arrayOfByte = new byte[i];
    for (int j = 0;; j = paramInputStream.read(arrayOfByte))
    {
      int k = -1;
      if (j == k) {
        break;
      }
      String str = new java/lang/String;
      str.<init>(arrayOfByte, 0, j);
      localStringBuilder.append(str);
    }
    paramInputStream.close();
    return localStringBuilder.toString();
  }
  
  public static boolean isAbsoluteTracker(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      Pattern localPattern = b;
      paramString = localPattern.matcher(paramString);
      boolean bool2 = paramString.matches();
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public static boolean isPercentageTracker(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      Pattern localPattern = a;
      paramString = localPattern.matcher(paramString);
      boolean bool2 = paramString.matches();
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public static Integer parseAbsoluteOffset(String paramString)
  {
    int i = 0;
    if (paramString == null) {
      return null;
    }
    String str = ":";
    paramString = paramString.split(str);
    int j = paramString.length;
    int k = 3;
    if (j != k) {
      return null;
    }
    i = Integer.parseInt(paramString[0]) * 60 * 60 * 1000;
    j = Integer.parseInt(paramString[1]) * 60 * 1000;
    i += j;
    int m = (int)(Float.parseFloat(paramString[2]) * 1000.0F);
    return Integer.valueOf(i + m);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Strings
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */