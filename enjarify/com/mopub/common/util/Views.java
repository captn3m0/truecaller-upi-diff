package com.mopub.common.util;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.r;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.mopub.common.logging.MoPubLog;

public class Views
{
  public static View getTopmostView(Context paramContext, View paramView)
  {
    boolean bool = paramContext instanceof Activity;
    int i = 16908290;
    View localView = null;
    if (!bool) {
      paramContext = null;
    } else {
      paramContext = ((Activity)paramContext).getWindow().getDecorView().findViewById(i);
    }
    if (paramView != null)
    {
      bool = r.H(paramView);
      if (!bool)
      {
        String str = "Attempting to call View#getRootView() on an unattached View.";
        MoPubLog.d(str);
      }
      paramView = paramView.getRootView();
      if (paramView != null)
      {
        localView = paramView.findViewById(i);
        if (localView == null) {
          localView = paramView;
        }
      }
    }
    if (paramContext != null) {
      return paramContext;
    }
    return localView;
  }
  
  public static void removeFromParent(View paramView)
  {
    if (paramView != null)
    {
      Object localObject = paramView.getParent();
      if (localObject != null)
      {
        localObject = paramView.getParent();
        boolean bool = localObject instanceof ViewGroup;
        if (bool)
        {
          localObject = (ViewGroup)paramView.getParent();
          ((ViewGroup)localObject).removeView(paramView);
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Views
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */