package com.mopub.common.util;

import android.os.AsyncTask;
import java.util.concurrent.Executor;

final class AsyncTasks$1
  implements Runnable
{
  AsyncTasks$1(AsyncTask paramAsyncTask, Object[] paramArrayOfObject) {}
  
  public final void run()
  {
    AsyncTask localAsyncTask = a;
    Executor localExecutor = AsyncTasks.a();
    Object[] arrayOfObject = b;
    localAsyncTask.executeOnExecutor(localExecutor, arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.AsyncTasks.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */