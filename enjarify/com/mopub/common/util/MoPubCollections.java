package com.mopub.common.util;

import com.mopub.common.Preconditions;
import java.util.Collection;
import java.util.Collections;

public class MoPubCollections
{
  public static void addAllNonNull(Collection paramCollection1, Collection paramCollection2)
  {
    Preconditions.checkNotNull(paramCollection1);
    Preconditions.checkNotNull(paramCollection2);
    paramCollection1.addAll(paramCollection2);
    paramCollection2 = Collections.singleton(null);
    paramCollection1.removeAll(paramCollection2);
  }
  
  public static void addAllNonNull(Collection paramCollection, Object... paramVarArgs)
  {
    Collections.addAll(paramCollection, paramVarArgs);
    paramVarArgs = Collections.singleton(null);
    paramCollection.removeAll(paramVarArgs);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.MoPubCollections
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */