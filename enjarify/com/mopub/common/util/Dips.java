package com.mopub.common.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import com.mopub.common.Preconditions;

public class Dips
{
  private static float a(Context paramContext)
  {
    return getResourcesgetDisplayMetricsdensity;
  }
  
  public static float asFloatPixels(float paramFloat, Context paramContext)
  {
    paramContext = paramContext.getResources().getDisplayMetrics();
    return TypedValue.applyDimension(1, paramFloat, paramContext);
  }
  
  public static int asIntPixels(float paramFloat, Context paramContext)
  {
    return (int)(asFloatPixels(paramFloat, paramContext) + 0.5F);
  }
  
  public static float dipsToFloatPixels(float paramFloat, Context paramContext)
  {
    float f = a(paramContext);
    return paramFloat * f;
  }
  
  public static int dipsToIntPixels(float paramFloat, Context paramContext)
  {
    return (int)(dipsToFloatPixels(paramFloat, paramContext) + 0.5F);
  }
  
  public static float pixelsToFloatDips(float paramFloat, Context paramContext)
  {
    float f = a(paramContext);
    return paramFloat / f;
  }
  
  public static int pixelsToIntDips(float paramFloat, Context paramContext)
  {
    return (int)(pixelsToFloatDips(paramFloat, paramContext) + 0.5F);
  }
  
  public static int screenHeightAsIntDips(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    return pixelsToIntDips(getResourcesgetDisplayMetricsheightPixels, paramContext);
  }
  
  public static int screenWidthAsIntDips(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    return pixelsToIntDips(getResourcesgetDisplayMetricswidthPixels, paramContext);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Dips
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */