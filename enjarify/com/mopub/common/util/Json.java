package com.mopub.common.util;

import android.text.TextUtils;
import com.mopub.common.logging.MoPubLog;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Json
{
  public static Object getJsonValue(JSONObject paramJSONObject, String paramString, Class paramClass)
  {
    if ((paramJSONObject != null) && (paramString != null) && (paramClass != null))
    {
      paramJSONObject = paramJSONObject.opt(paramString);
      if (paramJSONObject == null)
      {
        paramJSONObject = new java/lang/StringBuilder;
        paramJSONObject.<init>("Tried to get Json value with key: ");
        paramJSONObject.append(paramString);
        paramJSONObject.append(", but it was null");
        MoPubLog.w(paramJSONObject.toString());
        return null;
      }
      boolean bool = paramClass.isInstance(paramJSONObject);
      if (!bool)
      {
        paramJSONObject = new java/lang/StringBuilder;
        paramJSONObject.<init>("Tried to get Json value with key: ");
        paramJSONObject.append(paramString);
        paramJSONObject.append(", of type: ");
        paramString = paramClass.toString();
        paramJSONObject.append(paramString);
        paramJSONObject.append(", its type did not match");
        MoPubLog.w(paramJSONObject.toString());
        return null;
      }
      return paramClass.cast(paramJSONObject);
    }
    paramJSONObject = new java/lang/IllegalArgumentException;
    paramJSONObject.<init>("Cannot pass any null argument to getJsonValue");
    throw paramJSONObject;
  }
  
  public static String[] jsonArrayToStringArray(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    Object localObject = "{key:";
    localStringBuilder.<init>((String)localObject);
    localStringBuilder.append(paramString);
    localStringBuilder.append("}");
    paramString = localStringBuilder.toString();
    localStringBuilder = null;
    try
    {
      localObject = new org/json/JSONTokener;
      ((JSONTokener)localObject).<init>(paramString);
      paramString = ((JSONTokener)localObject).nextValue();
      paramString = (JSONObject)paramString;
      localObject = "key";
      paramString = paramString.getJSONArray((String)localObject);
      int i = paramString.length();
      localObject = new String[i];
      int j = 0;
      for (;;)
      {
        int k = localObject.length;
        if (j >= k) {
          break;
        }
        String str = paramString.getString(j);
        localObject[j] = str;
        j += 1;
      }
      return (String[])localObject;
    }
    catch (JSONException localJSONException) {}
    return new String[0];
  }
  
  public static Map jsonStringToMap(String paramString)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (bool1) {
      return localHashMap;
    }
    Object localObject = new org/json/JSONTokener;
    ((JSONTokener)localObject).<init>(paramString);
    paramString = (JSONObject)((JSONTokener)localObject).nextValue();
    localObject = paramString.keys();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject).hasNext();
      if (!bool2) {
        break;
      }
      String str1 = (String)((Iterator)localObject).next();
      String str2 = paramString.getString(str1);
      localHashMap.put(str1, str2);
    }
    return localHashMap;
  }
  
  public static String mapToJsonString(Map paramMap)
  {
    if (paramMap == null) {
      return "{}";
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = "{";
    localStringBuilder.append(str);
    int i = 1;
    paramMap = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool = paramMap.hasNext();
      if (!bool) {
        break;
      }
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      if (i == 0)
      {
        str = ",";
        localStringBuilder.append(str);
      }
      localStringBuilder.append("\"");
      str = (String)localEntry.getKey();
      localStringBuilder.append(str);
      localStringBuilder.append("\":\"");
      str = (String)localEntry.getValue();
      localStringBuilder.append(str);
      localStringBuilder.append("\"");
      i = 0;
      str = null;
    }
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Json
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */