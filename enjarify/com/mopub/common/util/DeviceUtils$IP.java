package com.mopub.common.util;

public enum DeviceUtils$IP
{
  static
  {
    Object localObject = new com/mopub/common/util/DeviceUtils$IP;
    ((IP)localObject).<init>("IPv4", 0);
    IPv4 = (IP)localObject;
    localObject = new com/mopub/common/util/DeviceUtils$IP;
    int i = 1;
    ((IP)localObject).<init>("IPv6", i);
    IPv6 = (IP)localObject;
    localObject = new IP[2];
    IP localIP = IPv4;
    localObject[0] = localIP;
    localIP = IPv6;
    localObject[i] = localIP;
    a = (IP[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.DeviceUtils.IP
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */