package com.mopub.common.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.mopub.common.MoPub;
import com.mopub.common.MoPub.BrowserAgent;
import com.mopub.common.MoPubBrowser;
import com.mopub.common.Preconditions;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.UrlAction;
import com.mopub.common.logging.MoPubLog;
import com.mopub.exceptions.IntentNotResolvableException;
import com.mopub.exceptions.UrlParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Intents
{
  private static final Map a;
  
  static
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("market", "https://play.google.com/store/apps/details?%s");
    localHashMap.put("amzn", "http://www.amazon.com/gp/mas/dl/android?%s");
    a = Collections.unmodifiableMap(localHashMap);
  }
  
  private static Uri a(Uri paramUri)
  {
    Preconditions.checkNotNull(paramUri);
    String str1 = "navigate";
    String str2 = paramUri.getHost();
    boolean bool = str1.equals(str2);
    if (bool)
    {
      str1 = "url";
      try
      {
        paramUri = paramUri.getQueryParameter(str1);
        if (paramUri != null) {
          return Uri.parse(paramUri);
        }
        paramUri = new com/mopub/exceptions/UrlParseException;
        paramUri.<init>("URL missing 'url' query parameter.");
        throw paramUri;
      }
      catch (UnsupportedOperationException localUnsupportedOperationException)
      {
        paramUri = String.valueOf(paramUri);
        MoPubLog.w("Could not handle url: ".concat(paramUri));
        paramUri = new com/mopub/exceptions/UrlParseException;
        paramUri.<init>("Passed-in URL did not create a hierarchical URI.");
        throw paramUri;
      }
    }
    paramUri = new com/mopub/exceptions/UrlParseException;
    paramUri.<init>("URL missing 'navigate' host parameter.");
    throw paramUri;
  }
  
  public static boolean canHandleApplicationUrl(Context paramContext, Uri paramUri)
  {
    return false;
  }
  
  public static boolean canHandleApplicationUrl(Context paramContext, Uri paramUri, boolean paramBoolean)
  {
    return false;
  }
  
  public static boolean deviceCanHandleIntent(Context paramContext, Intent paramIntent)
  {
    try
    {
      paramContext = paramContext.getPackageManager();
      paramContext = paramContext.queryIntentActivities(paramIntent, 0);
      boolean bool = paramContext.isEmpty();
      return !bool;
    }
    catch (NullPointerException localNullPointerException) {}
    return false;
  }
  
  public static Uri getPlayStoreUri(Intent paramIntent)
  {
    Preconditions.checkNotNull(paramIntent);
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("market://details?id=");
    paramIntent = paramIntent.getPackage();
    localStringBuilder.append(paramIntent);
    return Uri.parse(localStringBuilder.toString());
  }
  
  public static Intent getStartActivityIntent(Context paramContext, Class paramClass, Bundle paramBundle)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, paramClass);
    boolean bool = paramContext instanceof Activity;
    if (!bool)
    {
      int i = 268435456;
      localIntent.addFlags(i);
    }
    if (paramBundle != null) {
      localIntent.putExtras(paramBundle);
    }
    return localIntent;
  }
  
  public static Intent intentForNativeBrowserScheme(Uri paramUri)
  {
    Preconditions.checkNotNull(paramUri);
    Object localObject1 = UrlAction.OPEN_NATIVE_BROWSER;
    boolean bool = ((UrlAction)localObject1).shouldTryHandlingUrl(paramUri);
    if (!bool)
    {
      paramUri = "mopubnativebrowser://";
      localObject1 = MoPub.getBrowserAgent();
      localObject2 = MoPub.BrowserAgent.NATIVE;
      if (localObject1 == localObject2)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(paramUri);
        ((StringBuilder)localObject1).append(", http://, or https://");
        paramUri = ((StringBuilder)localObject1).toString();
      }
      localObject1 = new com/mopub/exceptions/UrlParseException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("URI does not have ");
      ((StringBuilder)localObject2).append(paramUri);
      ((StringBuilder)localObject2).append(" scheme.");
      paramUri = ((StringBuilder)localObject2).toString();
      ((UrlParseException)localObject1).<init>(paramUri);
      throw ((Throwable)localObject1);
    }
    localObject1 = "mopubnativebrowser";
    Object localObject2 = paramUri.getScheme();
    bool = ((String)localObject1).equalsIgnoreCase((String)localObject2);
    if (bool)
    {
      paramUri = a(paramUri);
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>("android.intent.action.VIEW", paramUri);
      return (Intent)localObject1;
    }
    localObject1 = MoPub.getBrowserAgent();
    localObject2 = MoPub.BrowserAgent.NATIVE;
    if (localObject1 == localObject2)
    {
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>("android.intent.action.VIEW", paramUri);
      return (Intent)localObject1;
    }
    localObject1 = new com/mopub/exceptions/UrlParseException;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Invalid URI: ");
    paramUri = paramUri.toString();
    ((StringBuilder)localObject2).append(paramUri);
    paramUri = ((StringBuilder)localObject2).toString();
    ((UrlParseException)localObject1).<init>(paramUri);
    throw ((Throwable)localObject1);
  }
  
  public static Intent intentForShareTweet(Uri paramUri)
  {
    Object localObject = UrlAction.HANDLE_SHARE_TWEET;
    boolean bool1 = ((UrlAction)localObject).shouldTryHandlingUrl(paramUri);
    if (bool1)
    {
      localObject = "screen_name";
      try
      {
        localObject = paramUri.getQueryParameter((String)localObject);
        String str = "tweet_id";
        paramUri = paramUri.getQueryParameter(str);
        boolean bool2 = TextUtils.isEmpty((CharSequence)localObject);
        if (!bool2)
        {
          bool2 = TextUtils.isEmpty(paramUri);
          if (!bool2)
          {
            int i = 2;
            Object[] arrayOfObject1 = new Object[i];
            arrayOfObject1[0] = localObject;
            int j = 1;
            arrayOfObject1[j] = paramUri;
            paramUri = String.format("https://twitter.com/%s/status/%s", arrayOfObject1);
            Object[] arrayOfObject2 = new Object[i];
            arrayOfObject2[0] = localObject;
            arrayOfObject2[j] = paramUri;
            paramUri = String.format("Check out @%s's Tweet: %s", arrayOfObject2);
            localObject = new android/content/Intent;
            ((Intent)localObject).<init>("android.intent.action.SEND");
            ((Intent)localObject).setType("text/plain");
            ((Intent)localObject).putExtra("android.intent.extra.SUBJECT", paramUri);
            ((Intent)localObject).putExtra("android.intent.extra.TEXT", paramUri);
            return (Intent)localObject;
          }
          paramUri = new com/mopub/exceptions/UrlParseException;
          paramUri.<init>("URL missing non-empty 'tweet_id' query parameter.");
          throw paramUri;
        }
        paramUri = new com/mopub/exceptions/UrlParseException;
        paramUri.<init>("URL missing non-empty 'screen_name' query parameter.");
        throw paramUri;
      }
      catch (UnsupportedOperationException localUnsupportedOperationException)
      {
        paramUri = String.valueOf(paramUri);
        MoPubLog.w("Could not handle url: ".concat(paramUri));
        paramUri = new com/mopub/exceptions/UrlParseException;
        paramUri.<init>("Passed-in URL did not create a hierarchical URI.");
        throw paramUri;
      }
    }
    paramUri = new com/mopub/exceptions/UrlParseException;
    paramUri.<init>("URL does not have mopubshare://tweet? format.");
    throw paramUri;
  }
  
  public static void launchActionViewIntent(Context paramContext, Uri paramUri, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramUri);
    Intent localIntent = new android/content/Intent;
    String str = "android.intent.action.VIEW";
    localIntent.<init>(str, paramUri);
    boolean bool = paramContext instanceof Activity;
    if (!bool)
    {
      int i = 268435456;
      localIntent.addFlags(i);
    }
    launchIntentForUserClick((Context)paramContext, localIntent, paramString);
  }
  
  public static void launchApplicationIntent(Context paramContext, Intent paramIntent)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramIntent);
    boolean bool1 = deviceCanHandleIntent(paramContext, paramIntent);
    if (bool1)
    {
      str = String.valueOf(paramIntent);
      localObject = "Unable to open intent: ".concat(str);
      boolean bool2 = paramContext instanceof Activity;
      if (!bool2)
      {
        int i = 268435456;
        paramIntent.addFlags(i);
      }
      launchIntentForUserClick((Context)paramContext, paramIntent, (String)localObject);
      return;
    }
    Object localObject = paramIntent.getStringExtra("browser_fallback_url");
    boolean bool3 = TextUtils.isEmpty((CharSequence)localObject);
    if (bool3)
    {
      localObject = a;
      str = paramIntent.getScheme();
      bool1 = ((Map)localObject).containsKey(str);
      if (!bool1)
      {
        paramIntent = getPlayStoreUri(paramIntent);
        launchApplicationUrl(paramContext, paramIntent);
        return;
      }
      paramContext = new com/mopub/exceptions/IntentNotResolvableException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("Device could not handle neither intent nor market url.\nIntent: ");
      paramIntent = paramIntent.toString();
      ((StringBuilder)localObject).append(paramIntent);
      paramIntent = ((StringBuilder)localObject).toString();
      paramContext.<init>(paramIntent);
      throw paramContext;
    }
    paramIntent = Uri.parse((String)localObject);
    localObject = paramIntent.getScheme();
    String str = "http";
    bool3 = str.equalsIgnoreCase((String)localObject);
    if (!bool3)
    {
      str = "https";
      bool1 = str.equalsIgnoreCase((String)localObject);
      if (!bool1)
      {
        launchApplicationUrl(paramContext, paramIntent);
        return;
      }
    }
    showMoPubBrowserForUrl(paramContext, paramIntent, null);
  }
  
  public static void launchApplicationUrl(Context paramContext, Uri paramUri)
  {
    Object localObject1 = new android/content/Intent;
    Object localObject2 = "android.intent.action.VIEW";
    ((Intent)localObject1).<init>((String)localObject2, paramUri);
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramUri);
    boolean bool = deviceCanHandleIntent(paramContext, (Intent)localObject1);
    if (bool)
    {
      launchApplicationIntent(paramContext, (Intent)localObject1);
      return;
    }
    localObject2 = a;
    String str = ((Intent)localObject1).getScheme();
    bool = ((Map)localObject2).containsKey(str);
    if (bool)
    {
      localObject2 = ((Intent)localObject1).getData();
      if (localObject2 != null)
      {
        localObject2 = ((Intent)localObject1).getData().getQuery();
        bool = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool)
        {
          paramUri = a;
          localObject2 = ((Intent)localObject1).getScheme();
          paramUri = (String)paramUri.get(localObject2);
          localObject2 = new Object[1];
          localObject1 = ((Intent)localObject1).getData().getQuery();
          localObject2[0] = localObject1;
          paramUri = String.format(paramUri, (Object[])localObject2);
          localObject1 = new android/content/Intent;
          paramUri = Uri.parse(paramUri);
          ((Intent)localObject1).<init>("android.intent.action.VIEW", paramUri);
          launchApplicationIntent(paramContext, (Intent)localObject1);
          return;
        }
      }
    }
    paramContext = new com/mopub/exceptions/IntentNotResolvableException;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Could not handle application specific action: ");
    ((StringBuilder)localObject1).append(paramUri);
    ((StringBuilder)localObject1).append("\n\tYou may be running in the emulator or another device which does not have the required application.");
    paramUri = ((StringBuilder)localObject1).toString();
    paramContext.<init>(paramUri);
    throw paramContext;
  }
  
  public static void launchIntentForUserClick(Context paramContext, Intent paramIntent, String paramString)
  {
    Preconditions.NoThrow.checkNotNull(paramContext);
    Preconditions.NoThrow.checkNotNull(paramIntent);
    try
    {
      startActivity(paramContext, paramIntent);
      return;
    }
    catch (IntentNotResolvableException paramContext)
    {
      paramIntent = new com/mopub/exceptions/IntentNotResolvableException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(paramString);
      localStringBuilder.append("\n");
      paramContext = paramContext.getMessage();
      localStringBuilder.append(paramContext);
      paramContext = localStringBuilder.toString();
      paramIntent.<init>(paramContext);
      throw paramIntent;
    }
  }
  
  public static void showMoPubBrowserForUrl(Context paramContext, Uri paramUri, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramUri);
    String str1 = String.valueOf(paramUri);
    MoPubLog.d("Final URI to show in browser: ".concat(str1));
    Object localObject = new android/os/Bundle;
    ((Bundle)localObject).<init>();
    str1 = "URL";
    String str2 = paramUri.toString();
    ((Bundle)localObject).putString(str1, str2);
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      str1 = "mopub-dsp-creative-id";
      ((Bundle)localObject).putString(str1, paramString);
    }
    paramString = getStartActivityIntent(paramContext, MoPubBrowser.class, (Bundle)localObject);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Could not show MoPubBrowser for url: ");
    ((StringBuilder)localObject).append(paramUri);
    ((StringBuilder)localObject).append("\n\tPerhaps you forgot to declare com.mopub.common.MoPubBrowser in your Android manifest file.");
    paramUri = ((StringBuilder)localObject).toString();
    launchIntentForUserClick(paramContext, paramString, paramUri);
  }
  
  public static void startActivity(Context paramContext, Intent paramIntent)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramIntent);
    boolean bool = paramContext instanceof Activity;
    if (!bool)
    {
      int i = 268435456;
      paramIntent.addFlags(i);
    }
    try
    {
      ((Context)paramContext).startActivity(paramIntent);
      return;
    }
    catch (ActivityNotFoundException paramContext)
    {
      paramIntent = new com/mopub/exceptions/IntentNotResolvableException;
      paramIntent.<init>(paramContext);
      throw paramIntent;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Intents
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */