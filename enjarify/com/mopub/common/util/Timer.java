package com.mopub.common.util;

import java.util.concurrent.TimeUnit;

public class Timer
{
  private long a;
  private long b;
  private Timer.a c;
  
  public Timer()
  {
    Timer.a locala = Timer.a.STOPPED;
    c = locala;
  }
  
  public long getTime()
  {
    Timer.a locala1 = c;
    Timer.a locala2 = Timer.a.STARTED;
    long l1;
    if (locala1 == locala2) {
      l1 = System.nanoTime();
    } else {
      l1 = a;
    }
    TimeUnit localTimeUnit1 = TimeUnit.MILLISECONDS;
    long l2 = b;
    l1 -= l2;
    TimeUnit localTimeUnit2 = TimeUnit.NANOSECONDS;
    return localTimeUnit1.convert(l1, localTimeUnit2);
  }
  
  public void start()
  {
    long l = System.nanoTime();
    b = l;
    Timer.a locala = Timer.a.STARTED;
    c = locala;
  }
  
  public void stop()
  {
    Object localObject = c;
    Timer.a locala = Timer.a.STARTED;
    if (localObject == locala)
    {
      localObject = Timer.a.STOPPED;
      c = ((Timer.a)localObject);
      long l = System.nanoTime();
      a = l;
      return;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("EventTimer was not started.");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Timer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */