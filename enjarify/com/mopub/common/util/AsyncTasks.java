package com.mopub.common.util;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import java.util.concurrent.Executor;

public class AsyncTasks
{
  private static Executor a = AsyncTask.THREAD_POOL_EXECUTOR;
  private static Handler b;
  
  static
  {
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    b = localHandler;
  }
  
  public static void safeExecuteOnExecutor(AsyncTask paramAsyncTask, Object... paramVarArgs)
  {
    Preconditions.checkNotNull(paramAsyncTask, "Unable to execute null AsyncTask.");
    Object localObject1 = Looper.getMainLooper();
    Object localObject2 = Looper.myLooper();
    if (localObject1 == localObject2)
    {
      localObject1 = a;
      paramAsyncTask.executeOnExecutor((Executor)localObject1, paramVarArgs);
      return;
    }
    MoPubLog.d("Posting AsyncTask to main thread for execution.");
    localObject1 = b;
    localObject2 = new com/mopub/common/util/AsyncTasks$1;
    ((AsyncTasks.1)localObject2).<init>(paramAsyncTask, paramVarArgs);
    ((Handler)localObject1).post((Runnable)localObject2);
  }
  
  public static void setExecutor(Executor paramExecutor)
  {
    a = paramExecutor;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.AsyncTasks
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */