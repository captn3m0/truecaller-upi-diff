package com.mopub.common.util;

public enum DeviceUtils$ForceOrientation
{
  private final String a;
  
  static
  {
    Object localObject = new com/mopub/common/util/DeviceUtils$ForceOrientation;
    ((ForceOrientation)localObject).<init>("FORCE_PORTRAIT", 0, "portrait");
    FORCE_PORTRAIT = (ForceOrientation)localObject;
    localObject = new com/mopub/common/util/DeviceUtils$ForceOrientation;
    int i = 1;
    ((ForceOrientation)localObject).<init>("FORCE_LANDSCAPE", i, "landscape");
    FORCE_LANDSCAPE = (ForceOrientation)localObject;
    localObject = new com/mopub/common/util/DeviceUtils$ForceOrientation;
    int j = 2;
    ((ForceOrientation)localObject).<init>("DEVICE_ORIENTATION", j, "device");
    DEVICE_ORIENTATION = (ForceOrientation)localObject;
    localObject = new com/mopub/common/util/DeviceUtils$ForceOrientation;
    int k = 3;
    ((ForceOrientation)localObject).<init>("UNDEFINED", k, "");
    UNDEFINED = (ForceOrientation)localObject;
    localObject = new ForceOrientation[4];
    ForceOrientation localForceOrientation = FORCE_PORTRAIT;
    localObject[0] = localForceOrientation;
    localForceOrientation = FORCE_LANDSCAPE;
    localObject[i] = localForceOrientation;
    localForceOrientation = DEVICE_ORIENTATION;
    localObject[j] = localForceOrientation;
    localForceOrientation = UNDEFINED;
    localObject[k] = localForceOrientation;
    b = (ForceOrientation[])localObject;
  }
  
  private DeviceUtils$ForceOrientation(String paramString1)
  {
    a = paramString1;
  }
  
  public static ForceOrientation getForceOrientation(String paramString)
  {
    ForceOrientation[] arrayOfForceOrientation = values();
    int i = arrayOfForceOrientation.length;
    int j = 0;
    while (j < i)
    {
      ForceOrientation localForceOrientation = arrayOfForceOrientation[j];
      String str = a;
      boolean bool = str.equalsIgnoreCase(paramString);
      if (bool) {
        return localForceOrientation;
      }
      j += 1;
    }
    return UNDEFINED;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.DeviceUtils.ForceOrientation
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */