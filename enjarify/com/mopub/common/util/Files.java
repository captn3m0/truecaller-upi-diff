package com.mopub.common.util;

import java.io.File;

public class Files
{
  public static File createDirectory(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    File localFile = new java/io/File;
    localFile.<init>(paramString);
    boolean bool = localFile.exists();
    if (bool)
    {
      bool = localFile.isDirectory();
      if (bool) {}
    }
    else
    {
      bool = localFile.mkdirs();
      if (!bool) {
        break label53;
      }
      bool = localFile.isDirectory();
      if (!bool) {
        break label53;
      }
    }
    return localFile;
    label53:
    return null;
  }
  
  public static int intLength(File paramFile)
  {
    if (paramFile == null) {
      return 0;
    }
    long l1 = paramFile.length();
    long l2 = 2147483647L;
    boolean bool = l1 < l2;
    if (bool) {
      return (int)l1;
    }
    return -1 >>> 1;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Files
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */