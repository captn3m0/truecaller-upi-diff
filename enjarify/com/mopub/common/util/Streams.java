package com.mopub.common.util;

import com.mopub.common.logging.MoPubLog;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Streams
{
  public static void closeStream(Closeable paramCloseable)
  {
    if (paramCloseable == null) {
      return;
    }
    try
    {
      paramCloseable.close();
      return;
    }
    catch (Exception localException)
    {
      MoPubLog.d("Unable to close stream. Ignoring.");
    }
  }
  
  public static void copyContent(InputStream paramInputStream, OutputStream paramOutputStream)
  {
    if ((paramInputStream != null) && (paramOutputStream != null))
    {
      int i = 16384;
      byte[] arrayOfByte = new byte[i];
      for (;;)
      {
        int j = paramInputStream.read(arrayOfByte);
        int k = -1;
        if (j == k) {
          break;
        }
        k = 0;
        paramOutputStream.write(arrayOfByte, 0, j);
      }
      return;
    }
    paramInputStream = new java/io/IOException;
    paramInputStream.<init>("Unable to copy from or to a null stream.");
    throw paramInputStream;
  }
  
  public static void copyContent(InputStream paramInputStream, OutputStream paramOutputStream, long paramLong)
  {
    if ((paramInputStream != null) && (paramOutputStream != null))
    {
      int i = 16384;
      byte[] arrayOfByte = new byte[i];
      long l1 = 0L;
      for (;;)
      {
        int j = paramInputStream.read(arrayOfByte);
        int k = -1;
        if (j == k) {
          break label131;
        }
        long l2 = j;
        l1 += l2;
        boolean bool = l1 < paramLong;
        if (!bool) {
          break;
        }
        bool = false;
        paramOutputStream.write(arrayOfByte, 0, j);
      }
      paramInputStream = new java/io/IOException;
      paramOutputStream = new java/lang/StringBuilder;
      paramOutputStream.<init>("Error copying content: attempted to copy ");
      paramOutputStream.append(l1);
      paramOutputStream.append(" bytes, with ");
      paramOutputStream.append(paramLong);
      paramOutputStream.append(" maximum.");
      paramOutputStream = paramOutputStream.toString();
      paramInputStream.<init>(paramOutputStream);
      throw paramInputStream;
      label131:
      return;
    }
    paramInputStream = new java/io/IOException;
    paramInputStream.<init>("Unable to copy from or to a null stream.");
    throw paramInputStream;
  }
  
  public static void readStream(InputStream paramInputStream, byte[] paramArrayOfByte)
  {
    int i = paramArrayOfByte.length;
    int j = 0;
    do
    {
      int k = paramInputStream.read(paramArrayOfByte, j, i);
      int m = -1;
      if (k == m) {
        break;
      }
      j += k;
      i -= k;
    } while (i > 0);
    return;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Streams
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */