package com.mopub.common.util;

import java.security.MessageDigest;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

public class Utils
{
  private static final AtomicLong a;
  
  static
  {
    AtomicLong localAtomicLong = new java/util/concurrent/atomic/AtomicLong;
    localAtomicLong.<init>(1L);
    a = localAtomicLong;
  }
  
  public static boolean bitMaskContainsFlag(int paramInt1, int paramInt2)
  {
    paramInt1 &= paramInt2;
    return paramInt1 != 0;
  }
  
  public static long generateUniqueId()
  {
    long l1;
    boolean bool2;
    do
    {
      AtomicLong localAtomicLong1 = a;
      l1 = localAtomicLong1.get();
      long l2 = 1L;
      long l3 = l1 + l2;
      long l4 = 9223372036854775806L;
      boolean bool1 = l3 < l4;
      if (!bool1) {
        l2 = l3;
      }
      AtomicLong localAtomicLong2 = a;
      bool2 = localAtomicLong2.compareAndSet(l1, l2);
    } while (!bool2);
    return l1;
  }
  
  public static String sha1(String paramString)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = "SHA-1";
    try
    {
      localObject2 = MessageDigest.getInstance((String)localObject2);
      String str1 = "UTF-8";
      paramString = paramString.getBytes(str1);
      int i = paramString.length;
      ((MessageDigest)localObject2).update(paramString, 0, i);
      paramString = ((MessageDigest)localObject2).digest();
      int j = paramString.length;
      i = 0;
      str1 = null;
      while (i < j)
      {
        byte b = paramString[i];
        String str2 = "%02X";
        int k = 1;
        Object[] arrayOfObject = new Object[k];
        Object localObject3 = Byte.valueOf(b);
        arrayOfObject[0] = localObject3;
        localObject3 = String.format(str2, arrayOfObject);
        ((StringBuilder)localObject1).append((String)localObject3);
        i += 1;
      }
      paramString = ((StringBuilder)localObject1).toString();
      localObject1 = Locale.US;
      return paramString.toLowerCase((Locale)localObject1);
    }
    catch (Exception localException) {}
    return "";
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Utils
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */