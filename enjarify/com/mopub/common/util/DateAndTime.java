package com.mopub.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateAndTime
{
  protected static DateAndTime a;
  
  static
  {
    DateAndTime localDateAndTime = new com/mopub/common/util/DateAndTime;
    localDateAndTime.<init>();
    a = localDateAndTime;
  }
  
  public static String getTimeZoneOffsetString()
  {
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    Locale localLocale = Locale.US;
    localSimpleDateFormat.<init>("Z", localLocale);
    Object localObject = localTimeZone();
    localSimpleDateFormat.setTimeZone((TimeZone)localObject);
    localObject = now();
    return localSimpleDateFormat.format((Date)localObject);
  }
  
  public static TimeZone localTimeZone()
  {
    return a.internalLocalTimeZone();
  }
  
  public static Date now()
  {
    return a.internalNow();
  }
  
  public static void setInstance(DateAndTime paramDateAndTime)
  {
    a = paramDateAndTime;
  }
  
  public TimeZone internalLocalTimeZone()
  {
    return TimeZone.getDefault();
  }
  
  public Date internalNow()
  {
    Date localDate = new java/util/Date;
    localDate.<init>();
    return localDate;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.DateAndTime
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */