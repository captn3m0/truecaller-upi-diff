package com.mopub.common.util;

import android.graphics.Bitmap;

public class ImageUtils
{
  public static Bitmap applyFastGaussianBlurToBitmap(Bitmap paramBitmap, int paramInt)
  {
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    int[] arrayOfInt = new int[i * j];
    int k = 0;
    int m = 0;
    int n = 0;
    int i1 = i;
    int i2 = i;
    int i3 = j;
    paramBitmap.getPixels(arrayOfInt, 0, i, 0, 0, i, j);
    int i4 = paramInt;
    while (i4 > 0)
    {
      int i5 = i4;
      for (;;)
      {
        k = j - i4;
        if (i5 >= k) {
          break;
        }
        k = i4;
        for (;;)
        {
          i1 = i - i4;
          if (k >= i1) {
            break;
          }
          i1 = (i5 - i4) * i + k;
          m = i1 - i4;
          m = arrayOfInt[m];
          n = i1 + i4;
          n = arrayOfInt[n];
          i1 = arrayOfInt[i1];
          i2 = (i5 + i4) * i + k;
          i3 = i2 - i4;
          i3 = arrayOfInt[i3];
          int i6 = i2 + i4;
          i6 = arrayOfInt[i6];
          i2 = arrayOfInt[i2];
          int i7 = i5 * i + k;
          int i8 = i7 - i4;
          i8 = arrayOfInt[i8];
          int i9 = i7 + i4;
          i9 = arrayOfInt[i9];
          int i10 = m & 0xFF;
          i11 = j;
          j = n & 0xFF;
          i10 += j;
          j = i1 & 0xFF;
          i10 += j;
          j = i3 & 0xFF;
          i10 += j;
          j = i6 & 0xFF;
          i10 += j;
          j = i2 & 0xFF;
          i10 += j;
          j = i8 & 0xFF;
          i10 += j;
          j = i9 & 0xFF;
          j = i10 + j >> 3 & 0xFF | 0xFF000000;
          i10 = 65280;
          int i12 = m & i10;
          int i13 = n & i10;
          i12 += i13;
          i13 = i1 & i10;
          i12 += i13;
          i13 = i3 & i10;
          i12 += i13;
          i13 = i6 & i10;
          i12 += i13;
          i13 = i2 & i10;
          i12 += i13;
          i13 = i8 & i10;
          i12 += i13;
          i13 = i9 & i10;
          i12 = i12 + i13 >> 3;
          i10 = i12 & i10;
          j |= i10;
          i10 = 16711680;
          m &= i10;
          n &= i10;
          m += n;
          i1 &= i10;
          m += i1;
          i1 = i3 & i10;
          m += i1;
          i1 = i6 & i10;
          m += i1;
          i1 = i2 & i10;
          m += i1;
          i1 = i8 & i10;
          m += i1;
          i1 = i9 & i10;
          m += i1;
          i1 = m >> 3 & i10 | j;
          arrayOfInt[i7] = i1;
          k += 1;
          j = i11;
        }
        i11 = j;
        i5 += 1;
      }
      i11 = j;
      i4 /= 2;
    }
    int i11 = j;
    i1 = i;
    i2 = i;
    i3 = j;
    paramBitmap.setPixels(arrayOfInt, 0, i, 0, 0, i, j);
    return paramBitmap;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.ImageUtils
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */