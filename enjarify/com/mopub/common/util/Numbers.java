package com.mopub.common.util;

public class Numbers
{
  public static long convertMillisecondsToSecondsRoundedUp(long paramLong)
  {
    return Math.round(Math.ceil((float)paramLong / 1000.0F));
  }
  
  public static Double parseDouble(Object paramObject)
  {
    boolean bool = paramObject instanceof Number;
    if (bool) {
      return Double.valueOf(((Number)paramObject).doubleValue());
    }
    bool = paramObject instanceof String;
    if (bool)
    {
      localObject = paramObject;
      try
      {
        localObject = (String)paramObject;
        return Double.valueOf((String)localObject);
      }
      catch (NumberFormatException localNumberFormatException)
      {
        localObject = new java/lang/ClassCastException;
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("Unable to parse ");
        localStringBuilder.append(paramObject);
        localStringBuilder.append(" as double.");
        paramObject = localStringBuilder.toString();
        ((ClassCastException)localObject).<init>((String)paramObject);
        throw ((Throwable)localObject);
      }
    }
    Object localObject = new java/lang/ClassCastException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Unable to parse ");
    localStringBuilder.append(paramObject);
    localStringBuilder.append(" as double.");
    paramObject = localStringBuilder.toString();
    ((ClassCastException)localObject).<init>((String)paramObject);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Numbers
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */