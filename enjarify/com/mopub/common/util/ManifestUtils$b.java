package com.mopub.common.util;

final class ManifestUtils$b
{
  public final boolean hasFlag(Class paramClass, int paramInt1, int paramInt2)
  {
    return Utils.bitMaskContainsFlag(paramInt1, paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.ManifestUtils.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */