package com.mopub.common.util;

import com.mopub.common.Preconditions;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Reflection$MethodBuilder
{
  private final Object a;
  private final String b;
  private Class c;
  private List d;
  private List e;
  private boolean f;
  private boolean g;
  
  public Reflection$MethodBuilder(Object paramObject, String paramString)
  {
    Preconditions.checkNotNull(paramString);
    a = paramObject;
    b = paramString;
    paramString = new java/util/ArrayList;
    paramString.<init>();
    d = paramString;
    paramString = new java/util/ArrayList;
    paramString.<init>();
    e = paramString;
    if (paramObject != null) {
      paramObject = paramObject.getClass();
    } else {
      paramObject = null;
    }
    c = ((Class)paramObject);
  }
  
  public MethodBuilder addParam(Class paramClass, Object paramObject)
  {
    Preconditions.checkNotNull(paramClass);
    d.add(paramClass);
    e.add(paramObject);
    return this;
  }
  
  public MethodBuilder addParam(String paramString, Object paramObject)
  {
    Preconditions.checkNotNull(paramString);
    paramString = Class.forName(paramString);
    d.add(paramString);
    e.add(paramObject);
    return this;
  }
  
  public Object execute()
  {
    int i = d.size();
    Object localObject1 = new Class[i];
    localObject1 = (Class[])d.toArray((Object[])localObject1);
    Object localObject2 = c;
    Object localObject3 = b;
    localObject1 = Reflection.getDeclaredMethodWithTraversal((Class)localObject2, (String)localObject3, (Class[])localObject1);
    boolean bool1 = f;
    if (bool1)
    {
      bool1 = true;
      ((Method)localObject1).setAccessible(bool1);
    }
    localObject2 = e.toArray();
    boolean bool2 = g;
    if (bool2) {
      return ((Method)localObject1).invoke(null, (Object[])localObject2);
    }
    localObject3 = a;
    return ((Method)localObject1).invoke(localObject3, (Object[])localObject2);
  }
  
  public MethodBuilder setAccessible()
  {
    f = true;
    return this;
  }
  
  public MethodBuilder setStatic(Class paramClass)
  {
    Preconditions.checkNotNull(paramClass);
    g = true;
    c = paramClass;
    return this;
  }
  
  public MethodBuilder setStatic(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    g = true;
    paramString = Class.forName(paramString);
    c = paramString;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Reflection.MethodBuilder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */