package com.mopub.common.util;

import com.mopub.common.Preconditions;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Reflection
{
  public static boolean classFound(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    try
    {
      Class.forName(paramString);
      return true;
    }
    catch (ClassNotFoundException localClassNotFoundException) {}
    return false;
  }
  
  public static Method getDeclaredMethodWithTraversal(Class paramClass, String paramString, Class... paramVarArgs)
  {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramVarArgs);
    while (paramClass != null) {
      try
      {
        return paramClass.getDeclaredMethod(paramString, paramVarArgs);
      }
      catch (NoSuchMethodException localNoSuchMethodException)
      {
        paramClass = paramClass.getSuperclass();
      }
    }
    paramClass = new java/lang/NoSuchMethodException;
    paramClass.<init>();
    throw paramClass;
  }
  
  public static Field getPrivateField(Class paramClass, String paramString)
  {
    paramClass = paramClass.getDeclaredField(paramString);
    paramClass.setAccessible(true);
    return paramClass;
  }
  
  public static Object instantiateClassWithConstructor(String paramString, Class paramClass, Class[] paramArrayOfClass, Object[] paramArrayOfObject)
  {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramClass);
    Preconditions.checkNotNull(paramArrayOfClass);
    Preconditions.checkNotNull(paramArrayOfObject);
    paramString = Class.forName(paramString).asSubclass(paramClass).getDeclaredConstructor(paramArrayOfClass);
    paramString.setAccessible(true);
    return paramString.newInstance(paramArrayOfObject);
  }
  
  public static Object instantiateClassWithEmptyConstructor(String paramString, Class paramClass)
  {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramClass);
    paramString = Class.forName(paramString).asSubclass(paramClass).getDeclaredConstructor(null);
    paramString.setAccessible(true);
    paramClass = new Object[0];
    return paramString.newInstance(paramClass);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Reflection
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */