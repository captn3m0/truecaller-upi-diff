package com.mopub.common.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.widget.Toast;
import com.mopub.common.MoPubBrowser;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.ConsentDialogActivity;
import com.mopub.mobileads.MraidVideoPlayerActivity;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ManifestUtils
{
  private static ManifestUtils.b a;
  private static final List b;
  private static final List c;
  private static final List d;
  
  static
  {
    Object localObject1 = new com/mopub/common/util/ManifestUtils$b;
    ((ManifestUtils.b)localObject1).<init>();
    a = (ManifestUtils.b)localObject1;
    localObject1 = new java/util/ArrayList;
    int i = 4;
    ((ArrayList)localObject1).<init>(i);
    b = (List)localObject1;
    localObject1 = "com.mopub.mobileads.MoPubActivity";
    try
    {
      localObject1 = Class.forName((String)localObject1);
      Object localObject2 = "com.mopub.mobileads.MraidActivity";
      localObject2 = Class.forName((String)localObject2);
      Object localObject3 = "com.mopub.mobileads.RewardedMraidActivity";
      localObject3 = Class.forName((String)localObject3);
      List localList = b;
      localList.add(localObject1);
      localObject1 = b;
      ((List)localObject1).add(localObject2);
      localObject1 = b;
      ((List)localObject1).add(localObject3);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      localObject1 = "ManifestUtils running without interstitial module";
      MoPubLog.i((String)localObject1);
    }
    b.add(MraidVideoPlayerActivity.class);
    b.add(MoPubBrowser.class);
    localObject1 = new java/util/ArrayList;
    i = 1;
    ((ArrayList)localObject1).<init>(i);
    c = (List)localObject1;
    ((List)localObject1).add(MoPubBrowser.class);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>(i);
    d = (List)localObject1;
    ((List)localObject1).add(ConsentDialogActivity.class);
  }
  
  private static ManifestUtils.a a(Context paramContext, Class paramClass)
  {
    Object localObject1 = paramContext.getPackageManager();
    Object localObject2 = new android/content/ComponentName;
    String str = paramClass.getName();
    ((ComponentName)localObject2).<init>(paramContext, str);
    localObject1 = ((PackageManager)localObject1).getActivityInfo((ComponentName)localObject2, 0);
    localObject2 = new com/mopub/common/util/ManifestUtils$a;
    ((ManifestUtils.a)localObject2).<init>((byte)0);
    paramContext = a;
    int i = configChanges;
    boolean bool = paramContext.hasFlag(paramClass, i, 32);
    hasKeyboardHidden = bool;
    paramContext = a;
    i = configChanges;
    bool = paramContext.hasFlag(paramClass, i, 128);
    hasOrientation = bool;
    hasScreenSize = true;
    paramContext = a;
    int j = configChanges;
    bool = paramContext.hasFlag(paramClass, j, 1024);
    hasScreenSize = bool;
    return (ManifestUtils.a)localObject2;
  }
  
  private static List a(Context paramContext, List paramList, boolean paramBoolean)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      Class localClass = (Class)paramList.next();
      Intent localIntent = new android/content/Intent;
      localIntent.<init>(paramContext, localClass);
      boolean bool2 = Intents.deviceCanHandleIntent(paramContext, localIntent);
      if (bool2 == paramBoolean) {
        localArrayList.add(localClass);
      }
    }
    return localArrayList;
  }
  
  private static void a(Context paramContext)
  {
    boolean bool = isDebuggable(paramContext);
    if (bool)
    {
      String str = "ERROR: YOUR MOPUB INTEGRATION IS INCOMPLETE.\nCheck logcat and update your AndroidManifest.xml with the correct activities and configuration.";
      paramContext = Toast.makeText(paramContext, str, 1);
      int i = 7;
      paramContext.setGravity(i, 0, 0);
      paramContext.show();
    }
  }
  
  private static void a(Context paramContext, List paramList)
  {
    paramList = a(paramContext, paramList, false);
    boolean bool = paramList.isEmpty();
    if (bool) {
      return;
    }
    a(paramContext);
    a(paramList);
  }
  
  private static void a(List paramList)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    Object localObject = "AndroidManifest permissions for the following required MoPub activities are missing:\n";
    localStringBuilder.<init>((String)localObject);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localObject = (Class)paramList.next();
      String str = "\n\t";
      localStringBuilder.append(str);
      localObject = ((Class)localObject).getName();
      localStringBuilder.append((String)localObject);
    }
    localStringBuilder.append("\n\nPlease update your manifest to include them.");
    MoPubLog.w(localStringBuilder.toString());
  }
  
  private static void b(Context paramContext, List paramList)
  {
    paramList = a(paramContext, paramList, true);
    paramList = c(paramContext, paramList);
    boolean bool = paramList.isEmpty();
    if (bool) {
      return;
    }
    a(paramContext);
    d(paramContext, paramList);
  }
  
  private static List c(Context paramContext, List paramList)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      Class localClass = (Class)paramList.next();
      try
      {
        ManifestUtils.a locala = a(paramContext, localClass);
        boolean bool2 = hasKeyboardHidden;
        if (bool2)
        {
          bool2 = hasOrientation;
          if (bool2)
          {
            boolean bool3 = hasScreenSize;
            if (bool3) {
              continue;
            }
          }
        }
        localArrayList.add(localClass);
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    }
    return localArrayList;
  }
  
  public static void checkGdprActivitiesDeclared(Context paramContext)
  {
    Object localObject = "context is not allowed to be null";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramContext, (String)localObject);
    if (!bool) {
      return;
    }
    localObject = d;
    a(paramContext, (List)localObject);
    localObject = d;
    b(paramContext, (List)localObject);
  }
  
  public static void checkNativeActivitiesDeclared(Context paramContext)
  {
    Object localObject = "context is not allowed to be null";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramContext, (String)localObject);
    if (!bool) {
      return;
    }
    localObject = c;
    a(paramContext, (List)localObject);
    localObject = c;
    b(paramContext, (List)localObject);
  }
  
  public static void checkWebViewActivitiesDeclared(Context paramContext)
  {
    Object localObject = "context is not allowed to be null";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramContext, (String)localObject);
    if (!bool) {
      return;
    }
    localObject = b;
    a(paramContext, (List)localObject);
    localObject = b;
    b(paramContext, (List)localObject);
  }
  
  private static void d(Context paramContext, List paramList)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    Object localObject1 = "In AndroidManifest, the android:configChanges param is missing values for the following MoPub activities:\n";
    localStringBuilder.<init>((String)localObject1);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (Class)paramList.next();
      try
      {
        Object localObject2 = a(paramContext, (Class)localObject1);
        boolean bool2 = hasKeyboardHidden;
        Object localObject3;
        String str;
        if (!bool2)
        {
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>("\n\tThe android:configChanges param for activity ");
          str = ((Class)localObject1).getName();
          ((StringBuilder)localObject3).append(str);
          str = " must include keyboardHidden.";
          ((StringBuilder)localObject3).append(str);
          localObject3 = ((StringBuilder)localObject3).toString();
          localStringBuilder.append((String)localObject3);
        }
        bool2 = hasOrientation;
        if (!bool2)
        {
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>("\n\tThe android:configChanges param for activity ");
          str = ((Class)localObject1).getName();
          ((StringBuilder)localObject3).append(str);
          str = " must include orientation.";
          ((StringBuilder)localObject3).append(str);
          localObject3 = ((StringBuilder)localObject3).toString();
          localStringBuilder.append((String)localObject3);
        }
        boolean bool3 = hasScreenSize;
        if (!bool3)
        {
          localObject2 = new java/lang/StringBuilder;
          localObject3 = "\n\tThe android:configChanges param for activity ";
          ((StringBuilder)localObject2).<init>((String)localObject3);
          localObject1 = ((Class)localObject1).getName();
          ((StringBuilder)localObject2).append((String)localObject1);
          ((StringBuilder)localObject2).append(" must include screenSize.");
          localObject1 = ((StringBuilder)localObject2).toString();
          localStringBuilder.append((String)localObject1);
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    }
    localStringBuilder.append("\n\nPlease update your manifest to include them.");
    MoPubLog.w(localStringBuilder.toString());
  }
  
  public static boolean isDebuggable(Context paramContext)
  {
    return Utils.bitMaskContainsFlag(getApplicationInfoflags, 2);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.ManifestUtils
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */