package com.mopub.common.util;

public enum JavaScriptWebViewCallbacks
{
  private String a;
  
  static
  {
    Object localObject = new com/mopub/common/util/JavaScriptWebViewCallbacks;
    ((JavaScriptWebViewCallbacks)localObject).<init>("WEB_VIEW_DID_APPEAR", 0, "webviewDidAppear();");
    WEB_VIEW_DID_APPEAR = (JavaScriptWebViewCallbacks)localObject;
    localObject = new com/mopub/common/util/JavaScriptWebViewCallbacks;
    int i = 1;
    ((JavaScriptWebViewCallbacks)localObject).<init>("WEB_VIEW_DID_CLOSE", i, "webviewDidClose();");
    WEB_VIEW_DID_CLOSE = (JavaScriptWebViewCallbacks)localObject;
    localObject = new JavaScriptWebViewCallbacks[2];
    JavaScriptWebViewCallbacks localJavaScriptWebViewCallbacks = WEB_VIEW_DID_APPEAR;
    localObject[0] = localJavaScriptWebViewCallbacks;
    localJavaScriptWebViewCallbacks = WEB_VIEW_DID_CLOSE;
    localObject[i] = localJavaScriptWebViewCallbacks;
    b = (JavaScriptWebViewCallbacks[])localObject;
  }
  
  private JavaScriptWebViewCallbacks(String paramString1)
  {
    a = paramString1;
  }
  
  public final String getJavascript()
  {
    return a;
  }
  
  public final String getUrl()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("javascript:");
    String str = a;
    localStringBuilder.append(str);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.JavaScriptWebViewCallbacks
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */