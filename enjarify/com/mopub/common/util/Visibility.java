package com.mopub.common.util;

public class Visibility
{
  public static boolean hasScreenVisibilityChanged(int paramInt1, int paramInt2)
  {
    paramInt1 = isScreenVisible(paramInt1);
    paramInt2 = isScreenVisible(paramInt2);
    return paramInt1 != paramInt2;
  }
  
  public static boolean isScreenVisible(int paramInt)
  {
    return paramInt == 0;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.Visibility
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */