package com.mopub.common.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.StatFs;
import android.support.v4.content.b;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.mopub.common.CreativeOrientation;
import com.mopub.common.Preconditions;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.logging.MoPubLog;
import java.io.File;
import java.lang.reflect.Field;

public class DeviceUtils
{
  private static int a(int paramInt1, int paramInt2)
  {
    int i = 9;
    int j = 1;
    if (j == paramInt2)
    {
      switch (paramInt1)
      {
      default: 
        return j;
      }
      return i;
    }
    j = 2;
    if (j == paramInt2)
    {
      switch (paramInt1)
      {
      default: 
        return 0;
      }
      return 8;
    }
    MoPubLog.d("Unknown screen orientation. Defaulting to portrait.");
    return i;
  }
  
  public static long diskCacheSizeBytes(File paramFile)
  {
    return diskCacheSizeBytes(paramFile, 31457280L);
  }
  
  public static long diskCacheSizeBytes(File paramFile, long paramLong)
  {
    try
    {
      StatFs localStatFs = new android/os/StatFs;
      paramFile = paramFile.getAbsolutePath();
      localStatFs.<init>(paramFile);
      int i = localStatFs.getBlockCount();
      long l1 = i;
      i = localStatFs.getBlockSize();
      long l2 = i;
      l1 *= l2;
      l2 = 50;
      paramLong = l1 / l2;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      paramFile = "Unable to calculate 2% of available disk space, defaulting to minimum";
      MoPubLog.d(paramFile);
    }
    return Math.max(Math.min(paramLong, 104857600L), 31457280L);
  }
  
  public static Point getDeviceDimensions(Context paramContext)
  {
    Object localObject1 = ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
    int i = Build.VERSION.SDK_INT;
    Object localObject2 = null;
    int j = 17;
    Object localObject3;
    if (i >= j)
    {
      localObject3 = new android/graphics/Point;
      ((Point)localObject3).<init>();
      ((Display)localObject1).getRealSize((Point)localObject3);
      k = x;
      localObject1 = Integer.valueOf(k);
      i = y;
      localObject2 = Integer.valueOf(i);
      localObject3 = localObject1;
    }
    else
    {
      Object localObject4;
      try
      {
        localObject3 = new com/mopub/common/util/Reflection$MethodBuilder;
        localObject4 = "getRawWidth";
        ((Reflection.MethodBuilder)localObject3).<init>(localObject1, (String)localObject4);
        localObject3 = ((Reflection.MethodBuilder)localObject3).execute();
        localObject3 = (Integer)localObject3;
        try
        {
          localObject4 = new com/mopub/common/util/Reflection$MethodBuilder;
          String str = "getRawHeight";
          ((Reflection.MethodBuilder)localObject4).<init>(localObject1, str);
          localObject1 = ((Reflection.MethodBuilder)localObject4).execute();
          localObject1 = (Integer)localObject1;
          localObject2 = localObject1;
        }
        catch (Exception localException1) {}
        localObject4 = "Display#getRawWidth/Height failed.";
      }
      catch (Exception localException2)
      {
        i = 0;
        localObject3 = null;
      }
      MoPubLog.v((String)localObject4, localException2);
    }
    if ((localObject3 == null) || (localObject2 == null))
    {
      paramContext = paramContext.getResources().getDisplayMetrics();
      k = widthPixels;
      localObject3 = Integer.valueOf(k);
      int m = heightPixels;
      localObject2 = Integer.valueOf(m);
    }
    paramContext = new android/graphics/Point;
    int k = ((Integer)localObject3).intValue();
    i = ((Integer)localObject2).intValue();
    paramContext.<init>(k, i);
    return paramContext;
  }
  
  public static String getHashedUdid(Context paramContext)
  {
    return null;
  }
  
  public static String getIpAddress(DeviceUtils.IP paramIP)
  {
    return null;
  }
  
  public static int getScreenOrientation(Activity paramActivity)
  {
    int i = paramActivity.getWindowManager().getDefaultDisplay().getRotation();
    int j = getResourcesgetConfigurationorientation;
    return a(i, j);
  }
  
  public static boolean isNetworkAvailable(Context paramContext)
  {
    if (paramContext == null) {
      return false;
    }
    String str = "android.permission.INTERNET";
    boolean bool = isPermissionGranted(paramContext, str);
    if (!bool) {
      return false;
    }
    str = "android.permission.ACCESS_NETWORK_STATE";
    bool = isPermissionGranted(paramContext, str);
    if (!bool) {
      return true;
    }
    str = "connectivity";
    try
    {
      paramContext = paramContext.getSystemService(str);
      paramContext = (ConnectivityManager)paramContext;
      paramContext = paramContext.getActiveNetworkInfo();
      return paramContext.isConnected();
    }
    catch (NullPointerException localNullPointerException) {}
    return false;
  }
  
  public static boolean isPermissionGranted(Context paramContext, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramString);
    try
    {
      int i = b.a(paramContext, paramString);
      return i == 0;
    }
    catch (Exception localException) {}
    return false;
  }
  
  public static void lockOrientation(Activity paramActivity, CreativeOrientation paramCreativeOrientation)
  {
    boolean bool = Preconditions.NoThrow.checkNotNull(paramCreativeOrientation);
    if (bool)
    {
      bool = Preconditions.NoThrow.checkNotNull(paramActivity);
      if (bool)
      {
        Display localDisplay = ((WindowManager)paramActivity.getSystemService("window")).getDefaultDisplay();
        int i = localDisplay.getRotation();
        int j = getResourcesgetConfigurationorientation;
        i = a(i, j);
        CreativeOrientation localCreativeOrientation = CreativeOrientation.PORTRAIT;
        int k = 8;
        int m = 9;
        if (localCreativeOrientation == paramCreativeOrientation)
        {
          if (m == i) {
            k = 9;
          } else {
            k = 1;
          }
        }
        else
        {
          localCreativeOrientation = CreativeOrientation.LANDSCAPE;
          if (localCreativeOrientation != paramCreativeOrientation) {
            break label128;
          }
          if (k != i) {
            k = 0;
          }
        }
        paramActivity.setRequestedOrientation(k);
        return;
        label128:
        return;
      }
    }
  }
  
  public static int memoryCacheSizeBytes(Context paramContext)
  {
    ActivityManager localActivityManager = (ActivityManager)paramContext.getSystemService("activity");
    int i = localActivityManager.getMemoryClass();
    long l1 = i;
    Object localObject = ApplicationInfo.class;
    String str = "FLAG_LARGE_HEAP";
    try
    {
      localObject = ((Class)localObject).getDeclaredField(str);
      str = null;
      int j = ((Field)localObject).getInt(null);
      paramContext = paramContext.getApplicationInfo();
      int k = flags;
      boolean bool = Utils.bitMaskContainsFlag(k, j);
      if (bool)
      {
        paramContext = new com/mopub/common/util/Reflection$MethodBuilder;
        localObject = "getLargeMemoryClass";
        paramContext.<init>(localActivityManager, (String)localObject);
        paramContext = paramContext.execute();
        paramContext = (Integer)paramContext;
        int m = paramContext.intValue();
        long l2 = m;
        l1 = l2;
      }
    }
    catch (Exception localException)
    {
      paramContext = "Unable to reflectively determine large heap size.";
      MoPubLog.d(paramContext);
    }
    l1 /= 8;
    long l3 = 1024L;
    l1 = l1 * l3 * l3;
    return (int)Math.min(31457280L, l1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.util.DeviceUtils
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */