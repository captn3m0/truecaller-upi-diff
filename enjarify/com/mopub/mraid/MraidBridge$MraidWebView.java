package com.mopub.mraid;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.View;
import com.mopub.common.VisibilityTracker;
import com.mopub.mobileads.BaseWebView;

public class MraidBridge$MraidWebView
  extends BaseWebView
{
  private MraidBridge.MraidWebView.OnVisibilityChangedListener b;
  private VisibilityTracker c;
  private boolean d;
  
  public MraidBridge$MraidWebView(Context paramContext)
  {
    super(paramContext);
    int i = Build.VERSION.SDK_INT;
    int j = 22;
    if (i <= j)
    {
      int k = getVisibility();
      if (k == 0)
      {
        k = 1;
      }
      else
      {
        k = 0;
        paramContext = null;
      }
      d = k;
      return;
    }
    VisibilityTracker localVisibilityTracker = new com/mopub/common/VisibilityTracker;
    localVisibilityTracker.<init>(paramContext);
    c = localVisibilityTracker;
    paramContext = new com/mopub/mraid/MraidBridge$MraidWebView$1;
    paramContext.<init>(this);
    c.setVisibilityTrackerListener(paramContext);
  }
  
  private void setMraidViewable(boolean paramBoolean)
  {
    boolean bool = d;
    if (bool == paramBoolean) {
      return;
    }
    d = paramBoolean;
    MraidBridge.MraidWebView.OnVisibilityChangedListener localOnVisibilityChangedListener = b;
    if (localOnVisibilityChangedListener != null) {
      localOnVisibilityChangedListener.onVisibilityChanged(paramBoolean);
    }
  }
  
  public void destroy()
  {
    super.destroy();
    c = null;
    b = null;
  }
  
  public boolean isMraidViewable()
  {
    return d;
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt)
  {
    super.onVisibilityChanged(paramView, paramInt);
    VisibilityTracker localVisibilityTracker1 = c;
    int i = 1;
    VisibilityTracker localVisibilityTracker2 = null;
    if (localVisibilityTracker1 == null)
    {
      if (paramInt != 0) {
        i = 0;
      }
      setMraidViewable(i);
      return;
    }
    if (paramInt == 0)
    {
      localVisibilityTracker1.clear();
      localVisibilityTracker2 = c;
      Integer localInteger = Integer.valueOf(i);
      localVisibilityTracker2.addView(paramView, this, 0, 0, localInteger);
      return;
    }
    localVisibilityTracker1.removeView(this);
    setMraidViewable(false);
  }
  
  void setVisibilityChangedListener(MraidBridge.MraidWebView.OnVisibilityChangedListener paramOnVisibilityChangedListener)
  {
    b = paramOnVisibilityChangedListener;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBridge.MraidWebView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */