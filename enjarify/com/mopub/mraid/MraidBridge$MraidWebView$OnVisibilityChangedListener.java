package com.mopub.mraid;

public abstract interface MraidBridge$MraidWebView$OnVisibilityChangedListener
{
  public abstract void onVisibilityChanged(boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBridge.MraidWebView.OnVisibilityChangedListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */