package com.mopub.mraid;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.mopub.common.CloseableLayout;
import com.mopub.common.CloseableLayout.ClosePosition;
import com.mopub.common.util.Dips;
import java.net.URI;

final class MraidController$3
  implements MraidBridge.MraidBridgeListener
{
  MraidController$3(MraidController paramMraidController) {}
  
  public final void onClose()
  {
    a.b();
  }
  
  public final boolean onConsoleMessage(ConsoleMessage paramConsoleMessage)
  {
    return a.a(paramConsoleMessage);
  }
  
  public final void onExpand(URI paramURI, boolean paramBoolean)
  {
    a.a(paramURI, paramBoolean);
  }
  
  public final boolean onJsAlert(String paramString, JsResult paramJsResult)
  {
    return a.a(paramString, paramJsResult);
  }
  
  public final void onOpen(URI paramURI)
  {
    MraidController localMraidController = a;
    paramURI = paramURI.toString();
    localMraidController.b(paramURI);
  }
  
  public final void onPageFailedToLoad()
  {
    MraidController.MraidListener localMraidListener = MraidController.a(a);
    if (localMraidListener != null)
    {
      localMraidListener = MraidController.a(a);
      localMraidListener.onFailedToLoad();
    }
  }
  
  public final void onPageLoaded()
  {
    a.a();
  }
  
  public final void onPlayVideo(URI paramURI)
  {
    MraidController localMraidController = a;
    paramURI = paramURI.toString();
    localMraidController.a(paramURI);
  }
  
  public final void onResize(int paramInt1, int paramInt2, int paramInt3, int paramInt4, CloseableLayout.ClosePosition paramClosePosition, boolean paramBoolean)
  {
    MraidController localMraidController = a;
    Object localObject1 = h;
    if (localObject1 != null)
    {
      localObject1 = g;
      Object localObject2 = ViewState.LOADING;
      if (localObject1 != localObject2)
      {
        localObject1 = g;
        localObject2 = ViewState.HIDDEN;
        if (localObject1 != localObject2)
        {
          localObject1 = g;
          localObject2 = ViewState.EXPANDED;
          if (localObject1 != localObject2)
          {
            localObject1 = c;
            localObject2 = PlacementType.INTERSTITIAL;
            if (localObject1 != localObject2)
            {
              float f1 = paramInt1;
              localObject2 = b;
              int i = Dips.dipsToIntPixels(f1, (Context)localObject2);
              float f2 = paramInt2;
              Object localObject3 = b;
              int j = Dips.dipsToIntPixels(f2, (Context)localObject3);
              float f3 = paramInt3;
              Object localObject4 = b;
              int k = Dips.dipsToIntPixels(f3, (Context)localObject4);
              float f4 = paramInt4;
              Object localObject5 = b;
              int m = Dips.dipsToIntPixels(f4, (Context)localObject5);
              localObject5 = f.g;
              int n = left + k;
              localObject3 = f.g;
              k = top + m;
              localObject4 = new android/graphics/Rect;
              i += n;
              boolean bool2 = k + j;
              ((Rect)localObject4).<init>(n, k, i, bool2);
              if (!paramBoolean)
              {
                localObject6 = f.c;
                i = ((Rect)localObject4).width();
                k = ((Rect)localObject6).width();
                if (i <= k)
                {
                  i = ((Rect)localObject4).height();
                  k = ((Rect)localObject6).height();
                  if (i <= k)
                  {
                    i = left;
                    k = left;
                    n = right;
                    bool2 = ((Rect)localObject4).width();
                    n -= bool2;
                    i = MraidController.a(i, k, n);
                    k = top;
                    n = top;
                    paramBoolean = bottom;
                    bool2 = ((Rect)localObject4).height();
                    paramBoolean -= bool2;
                    paramBoolean = MraidController.a(k, n, paramBoolean);
                    ((Rect)localObject4).offsetTo(i, paramBoolean);
                    break label560;
                  }
                }
                paramClosePosition = new com/mopub/mraid/a;
                localObject6 = new java/lang/StringBuilder;
                ((StringBuilder)localObject6).<init>("resizeProperties specified a size (");
                ((StringBuilder)localObject6).append(paramInt1);
                ((StringBuilder)localObject6).append(", ");
                ((StringBuilder)localObject6).append(paramInt2);
                ((StringBuilder)localObject6).append(") and offset (");
                ((StringBuilder)localObject6).append(paramInt3);
                ((StringBuilder)localObject6).append(", ");
                ((StringBuilder)localObject6).append(paramInt4);
                ((StringBuilder)localObject6).append(") that doesn't allow the ad to appear within the max allowed size (");
                paramInt1 = f.d.width();
                ((StringBuilder)localObject6).append(paramInt1);
                ((StringBuilder)localObject6).append(", ");
                paramInt1 = f.d.height();
                ((StringBuilder)localObject6).append(paramInt1);
                ((StringBuilder)localObject6).append(")");
                localObject7 = ((StringBuilder)localObject6).toString();
                paramClosePosition.<init>((String)localObject7);
                throw paramClosePosition;
              }
              label560:
              Object localObject6 = new android/graphics/Rect;
              ((Rect)localObject6).<init>();
              e.applyCloseRegionBounds(paramClosePosition, (Rect)localObject4, (Rect)localObject6);
              localObject1 = f.c;
              boolean bool1 = ((Rect)localObject1).contains((Rect)localObject6);
              if (bool1)
              {
                paramInt2 = ((Rect)localObject4).contains((Rect)localObject6);
                if (paramInt2 != 0)
                {
                  e.setCloseVisible(false);
                  e.setClosePosition(paramClosePosition);
                  localObject7 = new android/widget/FrameLayout$LayoutParams;
                  paramInt2 = ((Rect)localObject4).width();
                  paramInt3 = ((Rect)localObject4).height();
                  ((FrameLayout.LayoutParams)localObject7).<init>(paramInt2, paramInt3);
                  paramInt2 = left;
                  paramInt3 = f.c.left;
                  paramInt2 -= paramInt3;
                  leftMargin = paramInt2;
                  paramInt2 = top;
                  paramInt3 = f.c.top;
                  paramInt2 -= paramInt3;
                  topMargin = paramInt2;
                  localObject8 = g;
                  Object localObject9 = ViewState.DEFAULT;
                  if (localObject8 == localObject9)
                  {
                    localObject8 = d;
                    localObject9 = h;
                    ((FrameLayout)localObject8).removeView((View)localObject9);
                    localObject8 = d;
                    paramInt3 = 4;
                    ((FrameLayout)localObject8).setVisibility(paramInt3);
                    localObject8 = e;
                    localObject9 = h;
                    FrameLayout.LayoutParams localLayoutParams = new android/widget/FrameLayout$LayoutParams;
                    paramBoolean = true;
                    localLayoutParams.<init>(paramBoolean, paramBoolean);
                    ((CloseableLayout)localObject8).addView((View)localObject9, localLayoutParams);
                    localObject8 = localMraidController.c();
                    localObject9 = e;
                    ((ViewGroup)localObject8).addView((View)localObject9, (ViewGroup.LayoutParams)localObject7);
                  }
                  else
                  {
                    localObject8 = g;
                    localObject9 = ViewState.RESIZED;
                    if (localObject8 == localObject9)
                    {
                      localObject8 = e;
                      ((CloseableLayout)localObject8).setLayoutParams((ViewGroup.LayoutParams)localObject7);
                    }
                  }
                  e.setClosePosition(paramClosePosition);
                  localObject7 = ViewState.RESIZED;
                  localMraidController.a((ViewState)localObject7, null);
                  return;
                }
                Object localObject8 = new com/mopub/mraid/a;
                paramClosePosition = new java/lang/StringBuilder;
                paramClosePosition.<init>("resizeProperties specified a size (");
                paramClosePosition.append(paramInt1);
                paramClosePosition.append(", ");
                paramClosePosition.append(j);
                paramClosePosition.append(") and offset (");
                paramClosePosition.append(paramInt3);
                paramClosePosition.append(", ");
                paramClosePosition.append(paramInt4);
                paramClosePosition.append(") that don't allow the close region to appear within the resized ad.");
                localObject7 = paramClosePosition.toString();
                ((a)localObject8).<init>((String)localObject7);
                throw ((Throwable)localObject8);
              }
              paramClosePosition = new com/mopub/mraid/a;
              localObject6 = new java/lang/StringBuilder;
              ((StringBuilder)localObject6).<init>("resizeProperties specified a size (");
              ((StringBuilder)localObject6).append(paramInt1);
              ((StringBuilder)localObject6).append(", ");
              ((StringBuilder)localObject6).append(paramInt2);
              ((StringBuilder)localObject6).append(") and offset (");
              ((StringBuilder)localObject6).append(paramInt3);
              ((StringBuilder)localObject6).append(", ");
              ((StringBuilder)localObject6).append(paramInt4);
              ((StringBuilder)localObject6).append(") that doesn't allow the close region to appear within the max allowed size (");
              paramInt1 = f.d.width();
              ((StringBuilder)localObject6).append(paramInt1);
              ((StringBuilder)localObject6).append(", ");
              paramInt1 = f.d.height();
              ((StringBuilder)localObject6).append(paramInt1);
              ((StringBuilder)localObject6).append(")");
              localObject7 = ((StringBuilder)localObject6).toString();
              paramClosePosition.<init>((String)localObject7);
              throw paramClosePosition;
            }
            localObject7 = new com/mopub/mraid/a;
            ((a)localObject7).<init>("Not allowed to resize from an interstitial ad");
            throw ((Throwable)localObject7);
          }
          localObject7 = new com/mopub/mraid/a;
          ((a)localObject7).<init>("Not allowed to resize from an already expanded ad");
          throw ((Throwable)localObject7);
        }
      }
      return;
    }
    Object localObject7 = new com/mopub/mraid/a;
    ((a)localObject7).<init>("Unable to resize after the WebView is destroyed");
    throw ((Throwable)localObject7);
  }
  
  public final void onSetOrientationProperties(boolean paramBoolean, b paramb)
  {
    a.a(paramBoolean, paramb);
  }
  
  public final void onUseCustomClose(boolean paramBoolean)
  {
    a.a(paramBoolean);
  }
  
  public final void onVisibilityChanged(boolean paramBoolean)
  {
    MraidBridge localMraidBridge = MraidController.b(a);
    boolean bool = localMraidBridge.c();
    if (!bool)
    {
      localMraidBridge = MraidController.c(a);
      localMraidBridge.a(paramBoolean);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */