package com.mopub.mraid;

import android.os.Handler;
import android.view.View;

final class MraidController$b$a
{
  final View[] a;
  final Handler b;
  Runnable c;
  int d;
  final Runnable e;
  
  private MraidController$b$a(Handler paramHandler, View[] paramArrayOfView)
  {
    MraidController.b.a.1 local1 = new com/mopub/mraid/MraidController$b$a$1;
    local1.<init>(this);
    e = local1;
    b = paramHandler;
    a = paramArrayOfView;
  }
  
  final void a()
  {
    Handler localHandler = b;
    Runnable localRunnable = e;
    localHandler.removeCallbacks(localRunnable);
    c = null;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */