package com.mopub.mraid;

import android.app.Activity;
import android.content.Context;
import com.mopub.common.AdReport;
import com.mopub.common.ExternalViewabilitySessionManager;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.JavaScriptWebViewCallbacks;
import com.mopub.mobileads.CustomEventBanner;
import com.mopub.mobileads.CustomEventBanner.CustomEventBannerListener;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.factories.MraidControllerFactory;
import java.lang.ref.WeakReference;
import java.util.Map;

class MraidBanner
  extends CustomEventBanner
{
  private MraidController b;
  private CustomEventBanner.CustomEventBannerListener c;
  private MraidWebViewDebugListener d;
  private ExternalViewabilitySessionManager e;
  private boolean f = false;
  
  public final void loadBanner(Context paramContext, CustomEventBanner.CustomEventBannerListener paramCustomEventBannerListener, Map paramMap1, Map paramMap2)
  {
    c = paramCustomEventBannerListener;
    paramCustomEventBannerListener = "html-response-body";
    boolean bool1 = paramMap2.containsKey(paramCustomEventBannerListener);
    if (bool1)
    {
      paramCustomEventBannerListener = (String)paramMap2.get("html-response-body");
      paramMap2 = paramMap1.get("banner-impression-pixel-count-enabled");
      boolean bool2 = paramMap2 instanceof Boolean;
      if (bool2)
      {
        paramMap2 = (Boolean)paramMap2;
        boolean bool3 = paramMap2.booleanValue();
        f = bool3;
      }
      paramMap2 = "mopub-intent-ad-report";
      try
      {
        paramMap1 = paramMap1.get(paramMap2);
        paramMap1 = (AdReport)paramMap1;
        paramMap2 = PlacementType.INLINE;
        paramMap1 = MraidControllerFactory.create(paramContext, paramMap1, paramMap2);
        b = paramMap1;
        paramMap1 = b;
        paramMap2 = d;
        paramMap1.setDebugListener(paramMap2);
        paramMap1 = b;
        paramMap2 = new com/mopub/mraid/MraidBanner$1;
        paramMap2.<init>(this);
        paramMap1.setMraidListener(paramMap2);
        paramMap1 = b;
        MraidBanner.2 local2 = new com/mopub/mraid/MraidBanner$2;
        local2.<init>(this, paramContext);
        paramMap1.fillContent(null, paramCustomEventBannerListener, local2);
        return;
      }
      catch (ClassCastException paramContext)
      {
        MoPubLog.w("MRAID banner creating failed:", paramContext);
        paramContext = c;
        paramCustomEventBannerListener = MoPubErrorCode.MRAID_LOAD_ERROR;
        paramContext.onBannerFailed(paramCustomEventBannerListener);
        return;
      }
    }
    paramContext = c;
    paramCustomEventBannerListener = MoPubErrorCode.MRAID_LOAD_ERROR;
    paramContext.onBannerFailed(paramCustomEventBannerListener);
  }
  
  public final void onInvalidate()
  {
    Object localObject = e;
    if (localObject != null)
    {
      ((ExternalViewabilitySessionManager)localObject).endDisplaySession();
      e = null;
    }
    localObject = b;
    if (localObject != null)
    {
      ((MraidController)localObject).setMraidListener(null);
      localObject = b;
      ((MraidController)localObject).destroy();
    }
  }
  
  public void setDebugListener(MraidWebViewDebugListener paramMraidWebViewDebugListener)
  {
    d = paramMraidWebViewDebugListener;
    MraidController localMraidController = b;
    if (localMraidController != null) {
      localMraidController.setDebugListener(paramMraidWebViewDebugListener);
    }
  }
  
  public final void trackMpxAndThirdPartyImpressions()
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    String str = JavaScriptWebViewCallbacks.WEB_VIEW_DID_APPEAR.getJavascript();
    ((MraidController)localObject).loadJavascript(str);
    boolean bool = f;
    if (bool)
    {
      localObject = e;
      if (localObject != null)
      {
        localObject = (Activity)b.a.get();
        if (localObject != null)
        {
          e.startDeferredDisplaySession((Activity)localObject);
          return;
        }
        localObject = "Lost the activity for deferred Viewability tracking. Dropping session.";
        MoPubLog.d((String)localObject);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBanner
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */