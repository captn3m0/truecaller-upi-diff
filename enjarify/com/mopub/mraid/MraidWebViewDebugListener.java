package com.mopub.mraid;

import android.webkit.ConsoleMessage;
import android.webkit.JsResult;

public abstract interface MraidWebViewDebugListener
{
  public abstract boolean onConsoleMessage(ConsoleMessage paramConsoleMessage);
  
  public abstract boolean onJsAlert(String paramString, JsResult paramJsResult);
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidWebViewDebugListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */