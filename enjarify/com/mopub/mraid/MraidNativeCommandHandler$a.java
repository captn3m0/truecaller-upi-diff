package com.mopub.mraid;

import android.content.Context;
import android.os.AsyncTask;
import com.mopub.common.Preconditions;
import com.mopub.common.util.ResponseHeader;
import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.Map;

final class MraidNativeCommandHandler$a
  extends AsyncTask
{
  private final Context a;
  private final MraidNativeCommandHandler.a.a b;
  
  public MraidNativeCommandHandler$a(Context paramContext, MraidNativeCommandHandler.a.a parama)
  {
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    b = parama;
  }
  
  /* Error */
  private Boolean a(String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +296 -> 297
    //   4: aload_1
    //   5: arraylength
    //   6: istore_2
    //   7: iload_2
    //   8: ifeq +289 -> 297
    //   11: iconst_0
    //   12: istore_2
    //   13: aconst_null
    //   14: astore_3
    //   15: aload_1
    //   16: iconst_0
    //   17: aaload
    //   18: astore 4
    //   20: aload 4
    //   22: ifnonnull +6 -> 28
    //   25: goto +272 -> 297
    //   28: new 24	java/io/File
    //   31: astore 4
    //   33: invokestatic 30	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   36: astore 5
    //   38: aload 4
    //   40: aload 5
    //   42: ldc 32
    //   44: invokespecial 35	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   47: aload 4
    //   49: invokevirtual 39	java/io/File:mkdirs	()Z
    //   52: pop
    //   53: aload_1
    //   54: iconst_0
    //   55: aaload
    //   56: astore_1
    //   57: aload_1
    //   58: invokestatic 45	java/net/URI:create	(Ljava/lang/String;)Ljava/net/URI;
    //   61: astore 5
    //   63: aconst_null
    //   64: astore 6
    //   66: aload_1
    //   67: invokestatic 51	com/mopub/common/MoPubHttpUrlConnection:getHttpUrlConnection	(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    //   70: astore_1
    //   71: new 53	java/io/BufferedInputStream
    //   74: astore 7
    //   76: aload_1
    //   77: invokevirtual 59	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   80: astore 8
    //   82: aload 7
    //   84: aload 8
    //   86: invokespecial 62	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   89: getstatic 68	com/mopub/common/util/ResponseHeader:LOCATION	Lcom/mopub/common/util/ResponseHeader;
    //   92: astore 8
    //   94: aload 8
    //   96: invokevirtual 72	com/mopub/common/util/ResponseHeader:getKey	()Ljava/lang/String;
    //   99: astore 8
    //   101: aload_1
    //   102: aload 8
    //   104: invokevirtual 76	java/net/HttpURLConnection:getHeaderField	(Ljava/lang/String;)Ljava/lang/String;
    //   107: astore 8
    //   109: aload 8
    //   111: invokestatic 82	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   114: istore 9
    //   116: iload 9
    //   118: ifne +10 -> 128
    //   121: aload 8
    //   123: invokestatic 45	java/net/URI:create	(Ljava/lang/String;)Ljava/net/URI;
    //   126: astore 5
    //   128: aload_1
    //   129: invokevirtual 86	java/net/HttpURLConnection:getHeaderFields	()Ljava/util/Map;
    //   132: astore_1
    //   133: aload 5
    //   135: aload_1
    //   136: invokestatic 89	com/mopub/mraid/MraidNativeCommandHandler$a:a	(Ljava/net/URI;Ljava/util/Map;)Ljava/lang/String;
    //   139: astore_1
    //   140: new 24	java/io/File
    //   143: astore 5
    //   145: aload 5
    //   147: aload 4
    //   149: aload_1
    //   150: invokespecial 35	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   153: new 91	java/io/FileOutputStream
    //   156: astore_1
    //   157: aload_1
    //   158: aload 5
    //   160: invokespecial 94	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   163: aload 7
    //   165: aload_1
    //   166: invokestatic 100	com/mopub/common/util/Streams:copyContent	(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    //   169: aload 5
    //   171: invokevirtual 103	java/io/File:toString	()Ljava/lang/String;
    //   174: astore 4
    //   176: new 105	com/mopub/mraid/MraidNativeCommandHandler$b
    //   179: astore 5
    //   181: aload 5
    //   183: aload 4
    //   185: iconst_0
    //   186: invokespecial 108	com/mopub/mraid/MraidNativeCommandHandler$b:<init>	(Ljava/lang/String;B)V
    //   189: new 110	android/media/MediaScannerConnection
    //   192: astore_3
    //   193: aload_0
    //   194: getfield 20	com/mopub/mraid/MraidNativeCommandHandler$a:a	Landroid/content/Context;
    //   197: astore 4
    //   199: aload_3
    //   200: aload 4
    //   202: aload 5
    //   204: invokespecial 113	android/media/MediaScannerConnection:<init>	(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V
    //   207: aload 5
    //   209: aload_3
    //   210: invokestatic 116	com/mopub/mraid/MraidNativeCommandHandler$b:a	(Lcom/mopub/mraid/MraidNativeCommandHandler$b;Landroid/media/MediaScannerConnection;)V
    //   213: aload_3
    //   214: invokevirtual 119	android/media/MediaScannerConnection:connect	()V
    //   217: getstatic 125	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   220: astore_3
    //   221: aload 7
    //   223: invokestatic 129	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   226: aload_1
    //   227: invokestatic 129	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   230: aload_3
    //   231: areturn
    //   232: astore_3
    //   233: goto +47 -> 280
    //   236: astore_1
    //   237: goto +48 -> 285
    //   240: pop
    //   241: aconst_null
    //   242: astore_1
    //   243: aload 7
    //   245: astore 6
    //   247: goto +13 -> 260
    //   250: astore_1
    //   251: aconst_null
    //   252: astore 7
    //   254: goto +31 -> 285
    //   257: pop
    //   258: aconst_null
    //   259: astore_1
    //   260: getstatic 132	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   263: astore_3
    //   264: aload 6
    //   266: invokestatic 129	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   269: aload_1
    //   270: invokestatic 129	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   273: aload_3
    //   274: areturn
    //   275: astore_3
    //   276: aload 6
    //   278: astore 7
    //   280: aload_1
    //   281: astore 6
    //   283: aload_3
    //   284: astore_1
    //   285: aload 7
    //   287: invokestatic 129	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   290: aload 6
    //   292: invokestatic 129	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   295: aload_1
    //   296: athrow
    //   297: getstatic 132	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   300: areturn
    //   301: pop
    //   302: goto -59 -> 243
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	305	0	this	a
    //   0	305	1	paramArrayOfString	String[]
    //   6	7	2	i	int
    //   14	217	3	localObject1	Object
    //   232	1	3	localObject2	Object
    //   263	11	3	localBoolean	Boolean
    //   275	9	3	localObject3	Object
    //   18	183	4	localObject4	Object
    //   36	172	5	localObject5	Object
    //   64	227	6	localObject6	Object
    //   74	212	7	localObject7	Object
    //   80	42	8	localObject8	Object
    //   114	3	9	bool	boolean
    //   240	1	13	localException1	Exception
    //   257	1	14	localException2	Exception
    //   301	1	15	localException3	Exception
    // Exception table:
    //   from	to	target	type
    //   165	169	232	finally
    //   169	174	232	finally
    //   176	179	232	finally
    //   185	189	232	finally
    //   189	192	232	finally
    //   193	197	232	finally
    //   202	207	232	finally
    //   209	213	232	finally
    //   213	217	232	finally
    //   217	220	232	finally
    //   89	92	236	finally
    //   94	99	236	finally
    //   102	107	236	finally
    //   109	114	236	finally
    //   121	126	236	finally
    //   128	132	236	finally
    //   135	139	236	finally
    //   140	143	236	finally
    //   149	153	236	finally
    //   153	156	236	finally
    //   158	163	236	finally
    //   89	92	240	java/lang/Exception
    //   94	99	240	java/lang/Exception
    //   102	107	240	java/lang/Exception
    //   109	114	240	java/lang/Exception
    //   121	126	240	java/lang/Exception
    //   128	132	240	java/lang/Exception
    //   135	139	240	java/lang/Exception
    //   140	143	240	java/lang/Exception
    //   149	153	240	java/lang/Exception
    //   153	156	240	java/lang/Exception
    //   158	163	240	java/lang/Exception
    //   66	70	250	finally
    //   71	74	250	finally
    //   76	80	250	finally
    //   84	89	250	finally
    //   66	70	257	java/lang/Exception
    //   71	74	257	java/lang/Exception
    //   76	80	257	java/lang/Exception
    //   84	89	257	java/lang/Exception
    //   260	263	275	finally
    //   165	169	301	java/lang/Exception
    //   169	174	301	java/lang/Exception
    //   176	179	301	java/lang/Exception
    //   185	189	301	java/lang/Exception
    //   189	192	301	java/lang/Exception
    //   193	197	301	java/lang/Exception
    //   202	207	301	java/lang/Exception
    //   209	213	301	java/lang/Exception
    //   213	217	301	java/lang/Exception
    //   217	220	301	java/lang/Exception
  }
  
  private static String a(URI paramURI, Map paramMap)
  {
    Preconditions.checkNotNull(paramURI);
    paramURI = paramURI.getPath();
    if ((paramURI != null) && (paramMap != null))
    {
      Object localObject1 = new java/io/File;
      ((File)localObject1).<init>(paramURI);
      paramURI = ((File)localObject1).getName();
      localObject1 = ResponseHeader.CONTENT_TYPE.getKey();
      paramMap = (List)paramMap.get(localObject1);
      if (paramMap != null)
      {
        int i = paramMap.isEmpty();
        if (i == 0)
        {
          i = 0;
          localObject1 = null;
          Object localObject2 = paramMap.get(0);
          if (localObject2 != null)
          {
            paramMap = (String)paramMap.get(0);
            localObject2 = ";";
            paramMap = paramMap.split((String)localObject2);
            int k = paramMap.length;
            while (i < k)
            {
              Object localObject3 = paramMap[i];
              String str = "image/";
              boolean bool = ((String)localObject3).contains(str);
              if (bool)
              {
                paramMap = new java/lang/StringBuilder;
                paramMap.<init>(".");
                localObject1 = ((String)localObject3).split("/");
                int m = 1;
                localObject1 = localObject1[m];
                paramMap.append((String)localObject1);
                paramMap = paramMap.toString();
                i = paramURI.endsWith(paramMap);
                if (i != 0) {
                  break;
                }
                localObject1 = new java/lang/StringBuilder;
                ((StringBuilder)localObject1).<init>();
                ((StringBuilder)localObject1).append(paramURI);
                ((StringBuilder)localObject1).append(paramMap);
                paramURI = ((StringBuilder)localObject1).toString();
                break;
              }
              int j;
              i += 1;
            }
            return paramURI;
          }
        }
      }
      return paramURI;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidNativeCommandHandler.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */