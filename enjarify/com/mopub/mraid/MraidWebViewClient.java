package com.mopub.mraid;

import android.net.Uri;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mopub.mobileads.resource.MraidJavascript;
import java.io.ByteArrayInputStream;
import java.util.Locale;

public class MraidWebViewClient
  extends WebViewClient
{
  private static final String a;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("javascript:");
    String str = MraidJavascript.JAVASCRIPT_SOURCE;
    localStringBuilder.append(str);
    a = localStringBuilder.toString();
  }
  
  public WebResourceResponse shouldInterceptRequest(WebView paramWebView, String paramString)
  {
    Object localObject = Locale.US;
    localObject = Uri.parse(paramString.toLowerCase((Locale)localObject));
    String str = "mraid.js";
    localObject = ((Uri)localObject).getLastPathSegment();
    boolean bool = str.equals(localObject);
    if (bool)
    {
      paramWebView = new java/io/ByteArrayInputStream;
      paramString = a.getBytes();
      paramWebView.<init>(paramString);
      paramString = new android/webkit/WebResourceResponse;
      paramString.<init>("text/javascript", "UTF-8", paramWebView);
      return paramString;
    }
    return super.shouldInterceptRequest(paramWebView, paramString);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidWebViewClient
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */