package com.mopub.mraid;

import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;

final class MraidNativeCommandHandler$b
  implements MediaScannerConnection.MediaScannerConnectionClient
{
  private final String a;
  private final String b;
  private MediaScannerConnection c;
  
  private MraidNativeCommandHandler$b(String paramString)
  {
    a = paramString;
    b = null;
  }
  
  public final void onMediaScannerConnected()
  {
    MediaScannerConnection localMediaScannerConnection = c;
    if (localMediaScannerConnection != null)
    {
      String str1 = a;
      String str2 = b;
      localMediaScannerConnection.scanFile(str1, str2);
    }
  }
  
  public final void onScanCompleted(String paramString, Uri paramUri)
  {
    paramString = c;
    if (paramString != null) {
      paramString.disconnect();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidNativeCommandHandler.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */