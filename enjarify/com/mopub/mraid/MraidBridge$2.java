package com.mopub.mraid;

import com.mopub.mobileads.ViewGestureDetector.UserClickListener;

final class MraidBridge$2
  implements ViewGestureDetector.UserClickListener
{
  MraidBridge$2(MraidBridge paramMraidBridge) {}
  
  public final void onResetUserClick()
  {
    MraidBridge.a(a, false);
  }
  
  public final void onUserClick()
  {
    MraidBridge.a(a, true);
  }
  
  public final boolean wasClicked()
  {
    return MraidBridge.b(a);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBridge.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */