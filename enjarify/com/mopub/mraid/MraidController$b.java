package com.mopub.mraid;

import android.os.Handler;

final class MraidController$b
{
  final Handler a;
  MraidController.b.a b;
  
  MraidController$b()
  {
    Handler localHandler = new android/os/Handler;
    localHandler.<init>();
    a = localHandler;
  }
  
  final void a()
  {
    MraidController.b.a locala = b;
    if (locala != null)
    {
      locala.a();
      locala = null;
      b = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */