package com.mopub.mraid;

final class MraidController$6
  implements Runnable
{
  MraidController$6(MraidController paramMraidController) {}
  
  public final void run()
  {
    Object localObject = MraidController.b(a);
    MraidController.e(a);
    boolean bool1 = MraidNativeCommandHandler.b(MraidController.d(a));
    MraidController.e(a);
    boolean bool2 = MraidNativeCommandHandler.a(MraidController.d(a));
    MraidController.e(a);
    boolean bool3 = MraidNativeCommandHandler.c(MraidController.d(a));
    MraidController.e(a);
    boolean bool4 = MraidNativeCommandHandler.isStorePictureSupported(MraidController.d(a));
    boolean bool5 = MraidController.f(a);
    ((MraidBridge)localObject).a(bool1, bool2, bool3, bool4, bool5);
    MraidBridge localMraidBridge = MraidController.b(a);
    localObject = MraidController.h(a);
    localMraidBridge.a((ViewState)localObject);
    localMraidBridge = MraidController.b(a);
    localObject = MraidController.g(a);
    localMraidBridge.a((PlacementType)localObject);
    localMraidBridge = MraidController.b(a);
    boolean bool6 = MraidController.b(a).b();
    localMraidBridge.a(bool6);
    MraidController.b(a).a("mraidbridge.notifyReadyEvent();");
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */