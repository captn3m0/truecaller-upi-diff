package com.mopub.mraid;

import android.view.View;

public abstract interface MraidController$MraidListener
{
  public abstract void onClose();
  
  public abstract void onExpand();
  
  public abstract void onFailedToLoad();
  
  public abstract void onLoaded(View paramView);
  
  public abstract void onOpen();
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.MraidListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */