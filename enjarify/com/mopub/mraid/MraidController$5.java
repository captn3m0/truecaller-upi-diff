package com.mopub.mraid;

final class MraidController$5
  implements Runnable
{
  MraidController$5(MraidController paramMraidController) {}
  
  public final void run()
  {
    Object localObject = MraidController.c(a);
    MraidController.e(a);
    boolean bool1 = MraidNativeCommandHandler.b(MraidController.d(a));
    MraidController.e(a);
    boolean bool2 = MraidNativeCommandHandler.a(MraidController.d(a));
    boolean bool3 = MraidNativeCommandHandler.c(MraidController.d(a));
    boolean bool4 = MraidNativeCommandHandler.isStorePictureSupported(MraidController.d(a));
    boolean bool5 = MraidController.f(a);
    ((MraidBridge)localObject).a(bool1, bool2, bool3, bool4, bool5);
    MraidBridge localMraidBridge = MraidController.c(a);
    localObject = MraidController.g(a);
    localMraidBridge.a((PlacementType)localObject);
    localMraidBridge = MraidController.c(a);
    boolean bool6 = MraidController.c(a).b();
    localMraidBridge.a(bool6);
    MraidController.c(a).a("mraidbridge.notifyReadyEvent();");
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */