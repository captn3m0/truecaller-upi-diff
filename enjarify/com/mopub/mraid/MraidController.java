package com.mopub.mraid;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.mopub.common.AdReport;
import com.mopub.common.CloseableLayout;
import com.mopub.common.Preconditions;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.UrlHandler.Builder;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Utils;
import com.mopub.common.util.Views;
import com.mopub.mobileads.MraidVideoPlayerActivity;
import com.mopub.mobileads.WebViewCacheService;
import com.mopub.mobileads.WebViewCacheService.Config;
import com.mopub.mobileads.util.WebViews;
import java.lang.ref.WeakReference;
import java.net.URI;

public class MraidController
{
  final WeakReference a;
  final Context b;
  final PlacementType c;
  final FrameLayout d;
  final CloseableLayout e;
  final c f;
  ViewState g;
  MraidBridge.MraidWebView h;
  private final AdReport i;
  private ViewGroup j;
  private final MraidController.b k;
  private MraidController.MraidListener l;
  private MraidController.UseCustomCloseListener m;
  private MraidWebViewDebugListener n;
  private MraidBridge.MraidWebView o;
  private final MraidBridge p;
  private final MraidBridge q;
  private MraidController.a r;
  private Integer s;
  private boolean t;
  private b u;
  private final MraidNativeCommandHandler v;
  private boolean w;
  private final MraidBridge.MraidBridgeListener x;
  private final MraidBridge.MraidBridgeListener y;
  
  public MraidController(Context paramContext, AdReport paramAdReport, PlacementType paramPlacementType)
  {
    this(paramContext, paramAdReport, paramPlacementType, localMraidBridge1, localMraidBridge2, localb);
  }
  
  private MraidController(Context paramContext, AdReport paramAdReport, PlacementType paramPlacementType, MraidBridge paramMraidBridge1, MraidBridge paramMraidBridge2, MraidController.b paramb)
  {
    Object localObject = ViewState.LOADING;
    g = ((ViewState)localObject);
    localObject = new com/mopub/mraid/MraidController$a;
    ((MraidController.a)localObject).<init>(this);
    r = ((MraidController.a)localObject);
    boolean bool1 = true;
    t = bool1;
    localObject = b.NONE;
    u = ((b)localObject);
    localObject = new com/mopub/mraid/MraidController$3;
    ((MraidController.3)localObject).<init>(this);
    x = ((MraidBridge.MraidBridgeListener)localObject);
    localObject = new com/mopub/mraid/MraidController$4;
    ((MraidController.4)localObject).<init>(this);
    y = ((MraidBridge.MraidBridgeListener)localObject);
    localObject = paramContext.getApplicationContext();
    b = ((Context)localObject);
    localObject = b;
    Preconditions.checkNotNull(localObject);
    i = paramAdReport;
    boolean bool2 = paramContext instanceof Activity;
    if (bool2)
    {
      paramAdReport = new java/lang/ref/WeakReference;
      paramContext = (Activity)paramContext;
      paramAdReport.<init>(paramContext);
      a = paramAdReport;
    }
    else
    {
      paramContext = new java/lang/ref/WeakReference;
      bool2 = false;
      paramAdReport = null;
      paramContext.<init>(null);
      a = paramContext;
    }
    c = paramPlacementType;
    p = paramMraidBridge1;
    q = paramMraidBridge2;
    k = paramb;
    paramContext = ViewState.LOADING;
    g = paramContext;
    paramContext = b.getResources().getDisplayMetrics();
    paramAdReport = new com/mopub/mraid/c;
    paramPlacementType = b;
    float f1 = density;
    paramAdReport.<init>(paramPlacementType, f1);
    f = paramAdReport;
    paramContext = new android/widget/FrameLayout;
    paramAdReport = b;
    paramContext.<init>(paramAdReport);
    d = paramContext;
    paramContext = new com/mopub/common/CloseableLayout;
    paramAdReport = b;
    paramContext.<init>(paramAdReport);
    e = paramContext;
    paramContext = e;
    paramAdReport = new com/mopub/mraid/MraidController$1;
    paramAdReport.<init>(this);
    paramContext.setOnCloseListener(paramAdReport);
    paramContext = new android/view/View;
    paramAdReport = b;
    paramContext.<init>(paramAdReport);
    paramAdReport = new com/mopub/mraid/MraidController$2;
    paramAdReport.<init>(this);
    paramContext.setOnTouchListener(paramAdReport);
    paramAdReport = e;
    paramPlacementType = new android/widget/FrameLayout$LayoutParams;
    int i1 = -1;
    paramPlacementType.<init>(i1, i1);
    paramAdReport.addView(paramContext, paramPlacementType);
    paramContext = r;
    paramAdReport = b;
    paramContext.register(paramAdReport);
    paramContext = p;
    paramAdReport = x;
    a = paramAdReport;
    paramContext = q;
    paramAdReport = y;
    a = paramAdReport;
    paramContext = new com/mopub/mraid/MraidNativeCommandHandler;
    paramContext.<init>();
    v = paramContext;
  }
  
  static int a(int paramInt1, int paramInt2, int paramInt3)
  {
    paramInt2 = Math.min(paramInt2, paramInt3);
    return Math.max(paramInt1, paramInt2);
  }
  
  private void a(int paramInt)
  {
    Object localObject1 = (Activity)a.get();
    if (localObject1 != null)
    {
      localObject2 = u;
      boolean bool = a((b)localObject2);
      if (bool)
      {
        localObject2 = s;
        if (localObject2 == null)
        {
          int i1 = ((Activity)localObject1).getRequestedOrientation();
          localObject2 = Integer.valueOf(i1);
          s = ((Integer)localObject2);
        }
        ((Activity)localObject1).setRequestedOrientation(paramInt);
        return;
      }
    }
    a locala = new com/mopub/mraid/a;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Attempted to lock orientation to unsupported value: ");
    Object localObject2 = u.name();
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    locala.<init>((String)localObject1);
    throw locala;
  }
  
  private boolean a(b paramb)
  {
    Object localObject = b.NONE;
    boolean bool1 = true;
    if (paramb == localObject) {
      return bool1;
    }
    localObject = (Activity)a.get();
    if (localObject == null) {
      return false;
    }
    try
    {
      PackageManager localPackageManager = ((Activity)localObject).getPackageManager();
      ComponentName localComponentName = new android/content/ComponentName;
      Class localClass = localObject.getClass();
      localComponentName.<init>((Context)localObject, localClass);
      localObject = localPackageManager.getActivityInfo(localComponentName, 0);
      int i1 = screenOrientation;
      int i2 = -1;
      if (i1 != i2)
      {
        i3 = a;
        if (i1 == i3) {
          return bool1;
        }
        return false;
      }
      int i3 = configChanges;
      i1 = 128;
      boolean bool2 = Utils.bitMaskContainsFlag(i3, i1);
      if (bool2)
      {
        int i4 = configChanges;
        int i5 = 1024;
        boolean bool3 = Utils.bitMaskContainsFlag(i4, i5);
        if (bool3) {
          return bool1;
        }
      }
      return false;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return false;
  }
  
  private void d()
  {
    q.a();
    o = null;
  }
  
  private ViewGroup e()
  {
    Object localObject = j;
    if (localObject != null) {
      return (ViewGroup)localObject;
    }
    localObject = (Context)a.get();
    FrameLayout localFrameLayout = d;
    localObject = Views.getTopmostView((Context)localObject, localFrameLayout);
    boolean bool = localObject instanceof ViewGroup;
    if (bool) {
      return (ViewGroup)localObject;
    }
    return d;
  }
  
  private void f()
  {
    Object localObject = u;
    b localb = b.NONE;
    if (localObject == localb)
    {
      boolean bool = t;
      if (bool)
      {
        g();
        return;
      }
      localObject = (Activity)a.get();
      if (localObject != null)
      {
        i1 = DeviceUtils.getScreenOrientation((Activity)localObject);
        a(i1);
        return;
      }
      localObject = new com/mopub/mraid/a;
      ((a)localObject).<init>("Unable to set MRAID expand orientation to 'none'; expected passed in Activity Context.");
      throw ((Throwable)localObject);
    }
    int i1 = u.a;
    a(i1);
  }
  
  private void g()
  {
    Activity localActivity = (Activity)a.get();
    if (localActivity != null)
    {
      Integer localInteger = s;
      if (localInteger != null)
      {
        int i1 = localInteger.intValue();
        localActivity.setRequestedOrientation(i1);
      }
    }
    s = null;
  }
  
  final void a()
  {
    Object localObject1 = ViewState.DEFAULT;
    Object localObject2 = new com/mopub/mraid/MraidController$5;
    ((MraidController.5)localObject2).<init>(this);
    a((ViewState)localObject1, (Runnable)localObject2);
    localObject1 = l;
    if (localObject1 != null)
    {
      localObject2 = d;
      ((MraidController.MraidListener)localObject1).onLoaded((View)localObject2);
    }
  }
  
  final void a(ViewState paramViewState, Runnable paramRunnable)
  {
    Object localObject = String.valueOf(paramViewState);
    MoPubLog.d("MRAID state set to ".concat((String)localObject));
    ViewState localViewState = g;
    g = paramViewState;
    p.a(paramViewState);
    localObject = q;
    boolean bool = b;
    if (bool)
    {
      localObject = q;
      ((MraidBridge)localObject).a(paramViewState);
    }
    localObject = l;
    if (localObject != null)
    {
      localObject = ViewState.EXPANDED;
      if (paramViewState == localObject)
      {
        paramViewState = l;
        paramViewState.onExpand();
      }
      else
      {
        localObject = ViewState.EXPANDED;
        if (localViewState == localObject)
        {
          localViewState = ViewState.DEFAULT;
          if (paramViewState == localViewState)
          {
            paramViewState = l;
            paramViewState.onClose();
            break label149;
          }
        }
        localViewState = ViewState.HIDDEN;
        if (paramViewState == localViewState)
        {
          paramViewState = l;
          paramViewState.onClose();
        }
      }
    }
    label149:
    a(paramRunnable);
  }
  
  final void a(Runnable paramRunnable)
  {
    k.a();
    Object localObject1 = getCurrentWebView();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = k;
    Object localObject3 = new View[2];
    Object localObject4 = d;
    localObject3[0] = localObject4;
    localObject3[1] = localObject1;
    localObject4 = new com/mopub/mraid/MraidController$b$a;
    Handler localHandler = a;
    ((MraidController.b.a)localObject4).<init>(localHandler, (View[])localObject3, (byte)0);
    b = ((MraidController.b.a)localObject4);
    localObject2 = b;
    localObject3 = new com/mopub/mraid/MraidController$7;
    ((MraidController.7)localObject3).<init>(this, (View)localObject1, paramRunnable);
    c = ((Runnable)localObject3);
    int i1 = a.length;
    d = i1;
    paramRunnable = b;
    localObject1 = e;
    paramRunnable.post((Runnable)localObject1);
  }
  
  final void a(String paramString)
  {
    MraidVideoPlayerActivity.startMraid(b, paramString);
  }
  
  final void a(URI paramURI, boolean paramBoolean)
  {
    Object localObject1 = h;
    if (localObject1 != null)
    {
      localObject1 = c;
      Object localObject2 = PlacementType.INTERSTITIAL;
      if (localObject1 == localObject2) {
        return;
      }
      localObject1 = g;
      localObject2 = ViewState.DEFAULT;
      if (localObject1 != localObject2)
      {
        localObject1 = g;
        localObject2 = ViewState.RESIZED;
        if (localObject1 != localObject2) {
          return;
        }
      }
      f();
      int i1;
      if (paramURI != null)
      {
        i1 = 1;
      }
      else
      {
        i1 = 0;
        localObject1 = null;
      }
      if (i1 != 0)
      {
        localObject2 = new com/mopub/mraid/MraidBridge$MraidWebView;
        localObject3 = b;
        ((MraidBridge.MraidWebView)localObject2).<init>((Context)localObject3);
        o = ((MraidBridge.MraidWebView)localObject2);
        localObject2 = q;
        localObject3 = o;
        ((MraidBridge)localObject2).a((MraidBridge.MraidWebView)localObject3);
        localObject2 = q;
        paramURI = paramURI.toString();
        ((MraidBridge)localObject2).setContentUrl(paramURI);
      }
      paramURI = new android/widget/FrameLayout$LayoutParams;
      int i2 = -1;
      paramURI.<init>(i2, i2);
      Object localObject3 = g;
      Object localObject4 = ViewState.DEFAULT;
      int i3 = 4;
      if (localObject3 == localObject4)
      {
        if (i1 != 0)
        {
          localObject1 = e;
          localObject3 = o;
          ((CloseableLayout)localObject1).addView((View)localObject3, paramURI);
        }
        else
        {
          localObject1 = d;
          localObject3 = h;
          ((FrameLayout)localObject1).removeView((View)localObject3);
          d.setVisibility(i3);
          localObject1 = e;
          localObject3 = h;
          ((CloseableLayout)localObject1).addView((View)localObject3, paramURI);
        }
        localObject1 = c();
        localObject3 = e;
        localObject4 = new android/widget/FrameLayout$LayoutParams;
        ((FrameLayout.LayoutParams)localObject4).<init>(i2, i2);
        ((ViewGroup)localObject1).addView((View)localObject3, (ViewGroup.LayoutParams)localObject4);
      }
      else
      {
        localObject2 = g;
        localObject3 = ViewState.RESIZED;
        if ((localObject2 == localObject3) && (i1 != 0))
        {
          localObject1 = e;
          localObject2 = h;
          ((CloseableLayout)localObject1).removeView((View)localObject2);
          localObject1 = d;
          localObject2 = h;
          ((FrameLayout)localObject1).addView((View)localObject2, paramURI);
          d.setVisibility(i3);
          localObject1 = e;
          localObject2 = o;
          ((CloseableLayout)localObject1).addView((View)localObject2, paramURI);
        }
      }
      e.setLayoutParams(paramURI);
      a(paramBoolean);
      paramURI = ViewState.EXPANDED;
      a(paramURI, null);
      return;
    }
    paramURI = new com/mopub/mraid/a;
    paramURI.<init>("Unable to expand after the WebView is destroyed");
    throw paramURI;
  }
  
  protected final void a(boolean paramBoolean)
  {
    Object localObject = e;
    boolean bool1 = ((CloseableLayout)localObject).isCloseVisible() ^ true;
    if (paramBoolean == bool1) {
      return;
    }
    localObject = e;
    boolean bool2 = paramBoolean ^ true;
    ((CloseableLayout)localObject).setCloseVisible(bool2);
    localObject = m;
    if (localObject != null) {
      ((MraidController.UseCustomCloseListener)localObject).useCustomCloseChanged(paramBoolean);
    }
  }
  
  final void a(boolean paramBoolean, b paramb)
  {
    boolean bool = a(paramb);
    if (bool)
    {
      t = paramBoolean;
      u = paramb;
      localObject = g;
      paramb = ViewState.EXPANDED;
      if (localObject != paramb)
      {
        localObject = c;
        paramb = PlacementType.INTERSTITIAL;
        if (localObject != paramb) {}
      }
      else
      {
        f();
      }
      return;
    }
    Object localObject = new com/mopub/mraid/a;
    paramb = String.valueOf(paramb);
    paramb = "Unable to force orientation to ".concat(paramb);
    ((a)localObject).<init>(paramb);
    throw ((Throwable)localObject);
  }
  
  final boolean a(ConsoleMessage paramConsoleMessage)
  {
    MraidWebViewDebugListener localMraidWebViewDebugListener = n;
    if (localMraidWebViewDebugListener != null) {
      return localMraidWebViewDebugListener.onConsoleMessage(paramConsoleMessage);
    }
    return true;
  }
  
  final boolean a(String paramString, JsResult paramJsResult)
  {
    MraidWebViewDebugListener localMraidWebViewDebugListener = n;
    if (localMraidWebViewDebugListener != null) {
      return localMraidWebViewDebugListener.onJsAlert(paramString, paramJsResult);
    }
    paramJsResult.confirm();
    return true;
  }
  
  protected final void b()
  {
    Object localObject1 = h;
    if (localObject1 == null) {
      return;
    }
    localObject1 = g;
    Object localObject2 = ViewState.LOADING;
    if (localObject1 != localObject2)
    {
      localObject1 = g;
      localObject2 = ViewState.HIDDEN;
      if (localObject1 != localObject2)
      {
        localObject1 = g;
        localObject2 = ViewState.EXPANDED;
        if (localObject1 != localObject2)
        {
          localObject1 = c;
          localObject2 = PlacementType.INTERSTITIAL;
          if (localObject1 != localObject2) {}
        }
        else
        {
          g();
        }
        localObject1 = g;
        localObject2 = ViewState.RESIZED;
        if (localObject1 != localObject2)
        {
          localObject1 = g;
          localObject2 = ViewState.EXPANDED;
          if (localObject1 != localObject2)
          {
            localObject1 = g;
            localObject2 = ViewState.DEFAULT;
            if (localObject1 == localObject2)
            {
              localObject1 = d;
              i1 = 4;
              ((FrameLayout)localObject1).setVisibility(i1);
              localObject1 = ViewState.HIDDEN;
              a((ViewState)localObject1, null);
            }
            return;
          }
        }
        localObject1 = q;
        boolean bool = ((MraidBridge)localObject1).c();
        if (bool)
        {
          localObject1 = o;
          if (localObject1 != null)
          {
            d();
            localObject2 = e;
            ((CloseableLayout)localObject2).removeView((View)localObject1);
            break label246;
          }
        }
        localObject1 = e;
        localObject2 = h;
        ((CloseableLayout)localObject1).removeView((View)localObject2);
        localObject1 = d;
        localObject2 = h;
        FrameLayout.LayoutParams localLayoutParams = new android/widget/FrameLayout$LayoutParams;
        int i2 = -1;
        localLayoutParams.<init>(i2, i2);
        ((FrameLayout)localObject1).addView((View)localObject2, localLayoutParams);
        localObject1 = d;
        int i1 = 0;
        localObject2 = null;
        ((FrameLayout)localObject1).setVisibility(0);
        label246:
        Views.removeFromParent(e);
        localObject1 = ViewState.DEFAULT;
        a((ViewState)localObject1, null);
        return;
      }
    }
  }
  
  final void b(String paramString)
  {
    Object localObject1 = l;
    if (localObject1 != null) {
      ((MraidController.MraidListener)localObject1).onOpen();
    }
    localObject1 = new com/mopub/common/UrlHandler$Builder;
    ((UrlHandler.Builder)localObject1).<init>();
    Object localObject2 = i;
    if (localObject2 != null)
    {
      localObject2 = ((AdReport)localObject2).getDspCreativeId();
      ((UrlHandler.Builder)localObject1).withDspCreativeId((String)localObject2);
    }
    localObject2 = UrlAction.IGNORE_ABOUT_SCHEME;
    UrlAction[] arrayOfUrlAction = new UrlAction[5];
    UrlAction localUrlAction = UrlAction.OPEN_NATIVE_BROWSER;
    arrayOfUrlAction[0] = localUrlAction;
    localUrlAction = UrlAction.OPEN_IN_APP_BROWSER;
    arrayOfUrlAction[1] = localUrlAction;
    localUrlAction = UrlAction.HANDLE_SHARE_TWEET;
    arrayOfUrlAction[2] = localUrlAction;
    localUrlAction = UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK;
    arrayOfUrlAction[3] = localUrlAction;
    localUrlAction = UrlAction.FOLLOW_DEEP_LINK;
    arrayOfUrlAction[4] = localUrlAction;
    localObject1 = ((UrlHandler.Builder)localObject1).withSupportedUrlActions((UrlAction)localObject2, arrayOfUrlAction).build();
    localObject2 = b;
    ((UrlHandler)localObject1).handleUrl((Context)localObject2, paramString);
  }
  
  final ViewGroup c()
  {
    ViewGroup localViewGroup = j;
    if (localViewGroup == null)
    {
      localViewGroup = e();
      j = localViewGroup;
    }
    return j;
  }
  
  public void destroy()
  {
    Object localObject = k;
    ((MraidController.b)localObject).a();
    try
    {
      localObject = r;
      ((MraidController.a)localObject).unregister();
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      String str1 = localIllegalArgumentException.getMessage();
      String str2 = "Receiver not registered";
      boolean bool1 = str1.contains(str2);
      if (!bool1) {
        break label87;
      }
    }
    boolean bool2 = w;
    if (!bool2)
    {
      bool2 = true;
      pause(bool2);
    }
    Views.removeFromParent(e);
    p.a();
    h = null;
    d();
    return;
    label87:
    throw localIllegalArgumentException;
  }
  
  public void fillContent(Long paramLong, String paramString, MraidController.MraidWebViewCacheListener paramMraidWebViewCacheListener)
  {
    Object localObject1 = "htmlData cannot be null";
    Preconditions.checkNotNull(paramString, (String)localObject1);
    boolean bool1 = true;
    if (paramLong != null)
    {
      paramLong = WebViewCacheService.popWebViewConfig(paramLong);
      if (paramLong != null)
      {
        localObject2 = paramLong.getWebView();
        boolean bool2 = localObject2 instanceof MraidBridge.MraidWebView;
        if (bool2)
        {
          localObject2 = (MraidBridge.MraidWebView)paramLong.getWebView();
          h = ((MraidBridge.MraidWebView)localObject2);
          localObject2 = h;
          ((MraidBridge.MraidWebView)localObject2).enablePlugins(bool1);
          if (paramMraidWebViewCacheListener == null) {
            break label156;
          }
          localObject2 = h;
          paramLong = paramLong.getViewabilityManager();
          paramMraidWebViewCacheListener.onReady((MraidBridge.MraidWebView)localObject2, paramLong);
          break label156;
        }
      }
    }
    MoPubLog.d("WebView cache miss. Creating a new MraidWebView.");
    paramLong = new com/mopub/mraid/MraidBridge$MraidWebView;
    localObject1 = b;
    paramLong.<init>((Context)localObject1);
    h = paramLong;
    if (paramMraidWebViewCacheListener != null)
    {
      paramLong = h;
      bool1 = false;
      localObject1 = null;
      paramMraidWebViewCacheListener.onReady(paramLong, null);
    }
    bool1 = false;
    localObject1 = null;
    label156:
    Preconditions.NoThrow.checkNotNull(h, "mMraidWebView cannot be null");
    paramLong = p;
    paramMraidWebViewCacheListener = h;
    paramLong.a(paramMraidWebViewCacheListener);
    paramLong = d;
    paramMraidWebViewCacheListener = h;
    Object localObject2 = new android/widget/FrameLayout$LayoutParams;
    int i1 = -1;
    ((FrameLayout.LayoutParams)localObject2).<init>(i1, i1);
    paramLong.addView(paramMraidWebViewCacheListener, (ViewGroup.LayoutParams)localObject2);
    if (bool1)
    {
      a();
      return;
    }
    p.setContentHtml(paramString);
  }
  
  public FrameLayout getAdContainer()
  {
    return d;
  }
  
  public Context getContext()
  {
    return b;
  }
  
  public MraidBridge.MraidWebView getCurrentWebView()
  {
    MraidBridge localMraidBridge = q;
    boolean bool = localMraidBridge.c();
    if (bool) {
      return o;
    }
    return h;
  }
  
  public void loadJavascript(String paramString)
  {
    p.a(paramString);
  }
  
  public void pause(boolean paramBoolean)
  {
    boolean bool = true;
    w = bool;
    MraidBridge.MraidWebView localMraidWebView = h;
    if (localMraidWebView != null) {
      WebViews.onPause(localMraidWebView, paramBoolean);
    }
    localMraidWebView = o;
    if (localMraidWebView != null) {
      WebViews.onPause(localMraidWebView, paramBoolean);
    }
  }
  
  public void resume()
  {
    w = false;
    MraidBridge.MraidWebView localMraidWebView = h;
    if (localMraidWebView != null) {
      localMraidWebView.onResume();
    }
    localMraidWebView = o;
    if (localMraidWebView != null) {
      localMraidWebView.onResume();
    }
  }
  
  public void setDebugListener(MraidWebViewDebugListener paramMraidWebViewDebugListener)
  {
    n = paramMraidWebViewDebugListener;
  }
  
  public void setMraidListener(MraidController.MraidListener paramMraidListener)
  {
    l = paramMraidListener;
  }
  
  public void setUseCustomCloseListener(MraidController.UseCustomCloseListener paramUseCustomCloseListener)
  {
    m = paramUseCustomCloseListener;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */