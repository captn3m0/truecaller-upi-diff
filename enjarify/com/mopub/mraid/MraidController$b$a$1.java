package com.mopub.mraid;

import android.view.View;
import android.view.ViewTreeObserver;

final class MraidController$b$a$1
  implements Runnable
{
  MraidController$b$a$1(MraidController.b.a parama) {}
  
  public final void run()
  {
    View[] arrayOfView = a.a;
    int i = arrayOfView.length;
    int j = 0;
    while (j < i)
    {
      Object localObject = arrayOfView[j];
      int k = ((View)localObject).getHeight();
      if (k <= 0)
      {
        k = ((View)localObject).getWidth();
        if (k <= 0)
        {
          ViewTreeObserver localViewTreeObserver = ((View)localObject).getViewTreeObserver();
          MraidController.b.a.1.1 local1 = new com/mopub/mraid/MraidController$b$a$1$1;
          local1.<init>(this, (View)localObject);
          localViewTreeObserver.addOnPreDrawListener(local1);
          break label91;
        }
      }
      localObject = a;
      MraidController.b.a.a((MraidController.b.a)localObject);
      label91:
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.b.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */