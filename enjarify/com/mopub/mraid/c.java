package com.mopub.mraid;

import android.content.Context;
import android.graphics.Rect;
import com.mopub.common.util.Dips;

final class c
{
  final Rect a;
  final Rect b;
  final Rect c;
  final Rect d;
  final Rect e;
  final Rect f;
  final Rect g;
  final Rect h;
  private final Context i;
  private final float j;
  
  c(Context paramContext, float paramFloat)
  {
    paramContext = paramContext.getApplicationContext();
    i = paramContext;
    j = paramFloat;
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    a = paramContext;
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    b = paramContext;
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    c = paramContext;
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    d = paramContext;
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    e = paramContext;
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    f = paramContext;
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    g = paramContext;
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    h = paramContext;
  }
  
  final void a(Rect paramRect1, Rect paramRect2)
  {
    float f1 = left;
    Context localContext1 = i;
    int k = Dips.pixelsToIntDips(f1, localContext1);
    float f2 = top;
    Context localContext2 = i;
    int m = Dips.pixelsToIntDips(f2, localContext2);
    float f3 = right;
    Context localContext3 = i;
    int n = Dips.pixelsToIntDips(f3, localContext3);
    float f4 = bottom;
    localContext3 = i;
    int i1 = Dips.pixelsToIntDips(f4, localContext3);
    paramRect2.set(k, m, n, i1);
  }
  
  public final float getDensity()
  {
    return j;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */