package com.mopub.mraid;

import com.mopub.common.ExternalViewabilitySessionManager;

public abstract interface MraidController$MraidWebViewCacheListener
{
  public abstract void onReady(MraidBridge.MraidWebView paramMraidWebView, ExternalViewabilitySessionManager paramExternalViewabilitySessionManager);
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.MraidWebViewCacheListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */