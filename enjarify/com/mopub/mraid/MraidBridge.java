package com.mopub.mraid;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.mopub.common.AdReport;
import com.mopub.common.CloseableLayout.ClosePosition;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.ViewGestureDetector;
import com.mopub.mobileads.ViewGestureDetector.UserClickListener;
import com.mopub.network.Networking;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONObject;

public class MraidBridge
{
  MraidBridge.MraidBridgeListener a;
  boolean b;
  private final AdReport c;
  private final PlacementType d;
  private final MraidNativeCommandHandler e;
  private MraidBridge.MraidWebView f;
  private boolean g;
  private final WebViewClient h;
  
  MraidBridge(AdReport paramAdReport, PlacementType paramPlacementType)
  {
    this(paramAdReport, paramPlacementType, localMraidNativeCommandHandler);
  }
  
  private MraidBridge(AdReport paramAdReport, PlacementType paramPlacementType, MraidNativeCommandHandler paramMraidNativeCommandHandler)
  {
    MraidBridge.5 local5 = new com/mopub/mraid/MraidBridge$5;
    local5.<init>(this);
    h = local5;
    c = paramAdReport;
    d = paramPlacementType;
    e = paramMraidNativeCommandHandler;
  }
  
  private static int a(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= paramInt2)
    {
      paramInt2 = 100000;
      if (paramInt1 <= paramInt2) {
        return paramInt1;
      }
    }
    a locala = new com/mopub/mraid/a;
    String str = String.valueOf(paramInt1);
    str = "Integer parameter out of range: ".concat(str);
    locala.<init>(str);
    throw locala;
  }
  
  private static String a(Rect paramRect)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = left;
    localStringBuilder.append(i);
    localStringBuilder.append(",");
    i = top;
    localStringBuilder.append(i);
    localStringBuilder.append(",");
    i = paramRect.width();
    localStringBuilder.append(i);
    localStringBuilder.append(",");
    int j = paramRect.height();
    localStringBuilder.append(j);
    return localStringBuilder.toString();
  }
  
  private void a(MraidJavascriptCommand paramMraidJavascriptCommand, String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("window.mraidbridge.notifyErrorEvent(");
    paramMraidJavascriptCommand = JSONObject.quote(a);
    localStringBuilder.append(paramMraidJavascriptCommand);
    localStringBuilder.append(", ");
    paramMraidJavascriptCommand = JSONObject.quote(paramString);
    localStringBuilder.append(paramMraidJavascriptCommand);
    localStringBuilder.append(")");
    paramMraidJavascriptCommand = localStringBuilder.toString();
    a(paramMraidJavascriptCommand);
  }
  
  private static boolean a(String paramString, boolean paramBoolean)
  {
    if (paramString == null) {
      return paramBoolean;
    }
    return d(paramString);
  }
  
  private static String b(Rect paramRect)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = paramRect.width();
    localStringBuilder.append(i);
    localStringBuilder.append(",");
    int j = paramRect.height();
    localStringBuilder.append(j);
    return localStringBuilder.toString();
  }
  
  private static int c(String paramString)
  {
    int i = 10;
    try
    {
      return Integer.parseInt(paramString, i);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      a locala = new com/mopub/mraid/a;
      paramString = String.valueOf(paramString);
      paramString = "Invalid numeric parameter: ".concat(paramString);
      locala.<init>(paramString);
      throw locala;
    }
  }
  
  private static boolean d(String paramString)
  {
    Object localObject = "true";
    boolean bool = ((String)localObject).equals(paramString);
    if (bool) {
      return true;
    }
    localObject = "false";
    bool = ((String)localObject).equals(paramString);
    if (bool) {
      return false;
    }
    localObject = new com/mopub/mraid/a;
    paramString = String.valueOf(paramString);
    paramString = "Invalid boolean parameter: ".concat(paramString);
    ((a)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
  
  private static URI e(String paramString)
  {
    if (paramString != null) {
      try
      {
        localObject = new java/net/URI;
        ((URI)localObject).<init>(paramString);
        return (URI)localObject;
      }
      catch (URISyntaxException localURISyntaxException)
      {
        Object localObject = new com/mopub/mraid/a;
        paramString = String.valueOf(paramString);
        paramString = "Invalid URL parameter: ".concat(paramString);
        ((a)localObject).<init>(paramString);
        throw ((Throwable)localObject);
      }
    }
    paramString = new com/mopub/mraid/a;
    paramString.<init>("Parameter cannot be null");
    throw paramString;
  }
  
  final void a()
  {
    MraidBridge.MraidWebView localMraidWebView = f;
    if (localMraidWebView != null)
    {
      localMraidWebView.destroy();
      localMraidWebView = null;
      f = null;
    }
  }
  
  final void a(MraidBridge.MraidWebView paramMraidWebView)
  {
    f = paramMraidWebView;
    Object localObject1 = f.getSettings();
    ((WebSettings)localObject1).setJavaScriptEnabled(true);
    int i = Build.VERSION.SDK_INT;
    Object localObject2 = null;
    int j = 17;
    if (i >= j)
    {
      localObject1 = d;
      localObject3 = PlacementType.INTERSTITIAL;
      if (localObject1 == localObject3)
      {
        paramMraidWebView = paramMraidWebView.getSettings();
        paramMraidWebView.setMediaPlaybackRequiresUserGesture(false);
      }
    }
    f.setScrollContainer(false);
    f.setVerticalScrollBarEnabled(false);
    f.setHorizontalScrollBarEnabled(false);
    f.setBackgroundColor(-16777216);
    paramMraidWebView = f;
    localObject1 = h;
    paramMraidWebView.setWebViewClient((WebViewClient)localObject1);
    paramMraidWebView = f;
    localObject1 = new com/mopub/mraid/MraidBridge$1;
    ((MraidBridge.1)localObject1).<init>(this);
    paramMraidWebView.setWebChromeClient((WebChromeClient)localObject1);
    paramMraidWebView = new com/mopub/mobileads/ViewGestureDetector;
    localObject1 = f.getContext();
    localObject2 = f;
    Object localObject3 = c;
    paramMraidWebView.<init>((Context)localObject1, (View)localObject2, (AdReport)localObject3);
    localObject1 = new com/mopub/mraid/MraidBridge$2;
    ((MraidBridge.2)localObject1).<init>(this);
    paramMraidWebView.setUserClickListener((ViewGestureDetector.UserClickListener)localObject1);
    localObject1 = f;
    localObject2 = new com/mopub/mraid/MraidBridge$3;
    ((MraidBridge.3)localObject2).<init>(this, paramMraidWebView);
    ((MraidBridge.MraidWebView)localObject1).setOnTouchListener((View.OnTouchListener)localObject2);
    paramMraidWebView = f;
    localObject1 = new com/mopub/mraid/MraidBridge$4;
    ((MraidBridge.4)localObject1).<init>(this);
    paramMraidWebView.setVisibilityChangedListener((MraidBridge.MraidWebView.OnVisibilityChangedListener)localObject1);
  }
  
  final void a(PlacementType paramPlacementType)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("mraidbridge.setPlacementType(");
    paramPlacementType = paramPlacementType.toString();
    Locale localLocale = Locale.US;
    paramPlacementType = JSONObject.quote(paramPlacementType.toLowerCase(localLocale));
    localStringBuilder.append(paramPlacementType);
    localStringBuilder.append(")");
    paramPlacementType = localStringBuilder.toString();
    a(paramPlacementType);
  }
  
  final void a(ViewState paramViewState)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("mraidbridge.setState(");
    paramViewState = JSONObject.quote(paramViewState.toJavascriptString());
    localStringBuilder.append(paramViewState);
    localStringBuilder.append(")");
    paramViewState = localStringBuilder.toString();
    a(paramViewState);
  }
  
  final void a(String paramString)
  {
    MraidBridge.MraidWebView localMraidWebView = f;
    if (localMraidWebView == null)
    {
      paramString = String.valueOf(paramString);
      MoPubLog.d("Attempted to inject Javascript into MRAID WebView while was not attached:\n\t".concat(paramString));
      return;
    }
    String str = String.valueOf(paramString);
    MoPubLog.d("Injecting Javascript into MRAID WebView:\n\t".concat(str));
    localMraidWebView = f;
    paramString = String.valueOf(paramString);
    paramString = "javascript:".concat(paramString);
    localMraidWebView.loadUrl(paramString);
  }
  
  final void a(boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("mraidbridge.setIsViewable(");
    localStringBuilder.append(paramBoolean);
    localStringBuilder.append(")");
    String str = localStringBuilder.toString();
    a(str);
  }
  
  final void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("mraidbridge.setSupports(");
    localStringBuilder.append(paramBoolean1);
    localStringBuilder.append(",");
    localStringBuilder.append(paramBoolean2);
    localStringBuilder.append(",");
    localStringBuilder.append(paramBoolean3);
    localStringBuilder.append(",");
    localStringBuilder.append(paramBoolean4);
    localStringBuilder.append(",");
    localStringBuilder.append(paramBoolean5);
    localStringBuilder.append(")");
    String str = localStringBuilder.toString();
    a(str);
  }
  
  final boolean b()
  {
    MraidBridge.MraidWebView localMraidWebView = f;
    if (localMraidWebView != null)
    {
      boolean bool = localMraidWebView.isMraidViewable();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  final boolean b(String paramString)
  {
    boolean bool1 = true;
    try
    {
      Object localObject1 = new java/net/URI;
      ((URI)localObject1).<init>(paramString);
      Object localObject2 = ((URI)localObject1).getScheme();
      Object localObject3 = ((URI)localObject1).getHost();
      Object localObject4 = "mopub";
      boolean bool2 = ((String)localObject4).equals(localObject2);
      boolean bool3;
      if (bool2)
      {
        paramString = "failLoad";
        bool3 = paramString.equals(localObject3);
        if (bool3)
        {
          paramString = d;
          localObject1 = PlacementType.INLINE;
          if (paramString == localObject1)
          {
            paramString = a;
            if (paramString != null) {
              paramString.onPageFailedToLoad();
            }
          }
        }
        return bool1;
      }
      boolean bool4 = "mraid".equals(localObject2);
      bool2 = false;
      localObject4 = null;
      label823:
      int j;
      if (bool4)
      {
        paramString = new java/util/HashMap;
        paramString.<init>();
        localObject2 = "UTF-8";
        localObject1 = URLEncodedUtils.parse((URI)localObject1, (String)localObject2).iterator();
        Object localObject5;
        for (;;)
        {
          bool4 = ((Iterator)localObject1).hasNext();
          if (!bool4) {
            break;
          }
          localObject2 = (NameValuePair)((Iterator)localObject1).next();
          localObject5 = ((NameValuePair)localObject2).getName();
          localObject2 = ((NameValuePair)localObject2).getValue();
          paramString.put(localObject5, localObject2);
        }
        localObject1 = MraidJavascriptCommand.a((String)localObject3);
        try
        {
          localObject2 = d;
          bool4 = ((MraidJavascriptCommand)localObject1).a((PlacementType)localObject2);
          if (bool4)
          {
            bool4 = g;
            if (!bool4)
            {
              paramString = new com/mopub/mraid/a;
              localObject2 = "Cannot execute this command unless the user clicks";
              paramString.<init>((String)localObject2);
              throw paramString;
            }
          }
          localObject2 = a;
          if (localObject2 != null)
          {
            localObject2 = f;
            if (localObject2 != null)
            {
              localObject2 = MraidBridge.7.a;
              int k = ((MraidJavascriptCommand)localObject1).ordinal();
              int i = localObject2[k];
              k = 0;
              localObject3 = null;
              switch (i)
              {
              default: 
                break;
              case 10: 
                paramString = new com/mopub/mraid/a;
                localObject2 = "Unspecified MRAID Javascript command";
                paramString.<init>((String)localObject2);
                throw paramString;
              case 9: 
                localObject2 = f;
                localObject2 = ((MraidBridge.MraidWebView)localObject2).getContext();
                MraidNativeCommandHandler.a((Context)localObject2, paramString);
                break;
              case 8: 
                localObject2 = "uri";
                paramString = paramString.get(localObject2);
                paramString = (String)paramString;
                paramString = e(paramString);
                localObject2 = e;
                localObject5 = f;
                localObject5 = ((MraidBridge.MraidWebView)localObject5).getContext();
                paramString = paramString.toString();
                MraidBridge.6 local6 = new com/mopub/mraid/MraidBridge$6;
                local6.<init>(this, (MraidJavascriptCommand)localObject1);
                boolean bool8 = MraidNativeCommandHandler.isStorePictureSupported((Context)localObject5);
                if (bool8)
                {
                  bool8 = localObject5 instanceof Activity;
                  if (bool8)
                  {
                    localObject4 = new android/app/AlertDialog$Builder;
                    ((AlertDialog.Builder)localObject4).<init>((Context)localObject5);
                    Object localObject6 = "Save Image";
                    localObject4 = ((AlertDialog.Builder)localObject4).setTitle((CharSequence)localObject6);
                    localObject6 = "Download image to Picture gallery?";
                    localObject4 = ((AlertDialog.Builder)localObject4).setMessage((CharSequence)localObject6);
                    localObject6 = "Cancel";
                    localObject3 = ((AlertDialog.Builder)localObject4).setNegativeButton((CharSequence)localObject6, null);
                    localObject4 = "Okay";
                    localObject6 = new com/mopub/mraid/MraidNativeCommandHandler$2;
                    ((MraidNativeCommandHandler.2)localObject6).<init>((MraidNativeCommandHandler)localObject2, (Context)localObject5, paramString, local6);
                    paramString = ((AlertDialog.Builder)localObject3).setPositiveButton((CharSequence)localObject4, (DialogInterface.OnClickListener)localObject6);
                    paramString = paramString.setCancelable(bool1);
                    paramString.show();
                    break;
                  }
                  localObject3 = "Downloading image to Picture gallery...";
                  localObject3 = Toast.makeText((Context)localObject5, (CharSequence)localObject3, 0);
                  ((Toast)localObject3).show();
                  ((MraidNativeCommandHandler)localObject2).a((Context)localObject5, paramString, local6);
                  break;
                }
                paramString = "Error downloading file - the device does not have an SD card mounted, or the Android permission is not granted.";
                MoPubLog.d(paramString);
                paramString = new com/mopub/mraid/a;
                localObject2 = "Error downloading file  - the device does not have an SD card mounted, or the Android permission is not granted.";
                paramString.<init>((String)localObject2);
                throw paramString;
              case 7: 
                localObject2 = "uri";
                paramString = paramString.get(localObject2);
                paramString = (String)paramString;
                paramString = e(paramString);
                localObject2 = a;
                ((MraidBridge.MraidBridgeListener)localObject2).onPlayVideo(paramString);
                break;
              case 6: 
                localObject2 = "allowOrientationChange";
                localObject2 = paramString.get(localObject2);
                localObject2 = (String)localObject2;
                boolean bool5 = d((String)localObject2);
                localObject3 = "forceOrientation";
                paramString = paramString.get(localObject3);
                paramString = (String)paramString;
                localObject3 = "portrait";
                boolean bool6 = ((String)localObject3).equals(paramString);
                if (bool6)
                {
                  paramString = b.PORTRAIT;
                }
                else
                {
                  localObject3 = "landscape";
                  bool6 = ((String)localObject3).equals(paramString);
                  if (bool6)
                  {
                    paramString = b.LANDSCAPE;
                  }
                  else
                  {
                    localObject3 = "none";
                    bool6 = ((String)localObject3).equals(paramString);
                    if (!bool6) {
                      break label823;
                    }
                    paramString = b.NONE;
                  }
                }
                localObject3 = a;
                ((MraidBridge.MraidBridgeListener)localObject3).onSetOrientationProperties(bool5, paramString);
                break;
                localObject2 = new com/mopub/mraid/a;
                localObject3 = "Invalid orientation: ";
                paramString = String.valueOf(paramString);
                paramString = ((String)localObject3).concat(paramString);
                ((a)localObject2).<init>(paramString);
                throw ((Throwable)localObject2);
              case 5: 
                localObject2 = "url";
                paramString = paramString.get(localObject2);
                paramString = (String)paramString;
                paramString = e(paramString);
                localObject2 = a;
                ((MraidBridge.MraidBridgeListener)localObject2).onOpen(paramString);
                break;
              case 4: 
                localObject2 = "shouldUseCustomClose";
                paramString = paramString.get(localObject2);
                paramString = (String)paramString;
                bool3 = a(paramString, false);
                localObject2 = a;
                ((MraidBridge.MraidBridgeListener)localObject2).onUseCustomClose(bool3);
                break;
              case 3: 
                localObject2 = "url";
                localObject2 = paramString.get(localObject2);
                localObject2 = (String)localObject2;
                if (localObject2 != null) {
                  localObject3 = e((String)localObject2);
                }
                localObject2 = "shouldUseCustomClose";
                paramString = paramString.get(localObject2);
                paramString = (String)paramString;
                bool3 = a(paramString, false);
                localObject2 = a;
                ((MraidBridge.MraidBridgeListener)localObject2).onExpand((URI)localObject3, bool3);
                break;
              case 2: 
                localObject2 = "width";
                localObject2 = paramString.get(localObject2);
                localObject2 = (String)localObject2;
                j = c((String)localObject2);
                int i1 = a(j, 0);
                localObject2 = "height";
                localObject2 = paramString.get(localObject2);
                localObject2 = (String)localObject2;
                j = c((String)localObject2);
                int n = a(j, 0);
                localObject2 = "offsetX";
                localObject2 = paramString.get(localObject2);
                localObject2 = (String)localObject2;
                j = c((String)localObject2);
                int m = -100000;
                int i2 = a(j, m);
                localObject2 = "offsetY";
                localObject2 = paramString.get(localObject2);
                localObject2 = (String)localObject2;
                j = c((String)localObject2);
                int i3 = a(j, m);
                localObject2 = "customClosePosition";
                localObject2 = paramString.get(localObject2);
                localObject2 = (String)localObject2;
                localObject3 = CloseableLayout.ClosePosition.TOP_RIGHT;
                bool2 = TextUtils.isEmpty((CharSequence)localObject2);
                Object localObject7;
                if (bool2)
                {
                  localObject7 = localObject3;
                }
                else
                {
                  localObject3 = "top-left";
                  boolean bool7 = ((String)localObject2).equals(localObject3);
                  if (bool7)
                  {
                    localObject2 = CloseableLayout.ClosePosition.TOP_LEFT;
                    localObject7 = localObject2;
                  }
                  else
                  {
                    localObject3 = "top-right";
                    bool7 = ((String)localObject2).equals(localObject3);
                    if (bool7)
                    {
                      localObject2 = CloseableLayout.ClosePosition.TOP_RIGHT;
                      localObject7 = localObject2;
                    }
                    else
                    {
                      localObject3 = "center";
                      bool7 = ((String)localObject2).equals(localObject3);
                      if (bool7)
                      {
                        localObject2 = CloseableLayout.ClosePosition.CENTER;
                        localObject7 = localObject2;
                      }
                      else
                      {
                        localObject3 = "bottom-left";
                        bool7 = ((String)localObject2).equals(localObject3);
                        if (bool7)
                        {
                          localObject2 = CloseableLayout.ClosePosition.BOTTOM_LEFT;
                          localObject7 = localObject2;
                        }
                        else
                        {
                          localObject3 = "bottom-right";
                          bool7 = ((String)localObject2).equals(localObject3);
                          if (bool7)
                          {
                            localObject2 = CloseableLayout.ClosePosition.BOTTOM_RIGHT;
                            localObject7 = localObject2;
                          }
                          else
                          {
                            localObject3 = "top-center";
                            bool7 = ((String)localObject2).equals(localObject3);
                            if (bool7)
                            {
                              localObject2 = CloseableLayout.ClosePosition.TOP_CENTER;
                              localObject7 = localObject2;
                            }
                            else
                            {
                              localObject3 = "bottom-center";
                              bool7 = ((String)localObject2).equals(localObject3);
                              if (!bool7) {
                                break label1491;
                              }
                              localObject2 = CloseableLayout.ClosePosition.BOTTOM_CENTER;
                              localObject7 = localObject2;
                            }
                          }
                        }
                      }
                    }
                  }
                }
                localObject2 = "allowOffscreen";
                paramString = paramString.get(localObject2);
                paramString = (String)paramString;
                boolean bool9 = a(paramString, bool1);
                localObject5 = a;
                ((MraidBridge.MraidBridgeListener)localObject5).onResize(i1, n, i2, i3, (CloseableLayout.ClosePosition)localObject7, bool9);
                break;
                paramString = new com/mopub/mraid/a;
                localObject3 = "Invalid close position: ";
                localObject2 = String.valueOf(localObject2);
                localObject2 = ((String)localObject3).concat((String)localObject2);
                paramString.<init>((String)localObject2);
                throw paramString;
              case 1: 
                label1491:
                paramString = a;
                paramString.onClose();
                break;
              }
            }
            else
            {
              paramString = new com/mopub/mraid/a;
              localObject2 = "The current WebView is being destroyed";
              paramString.<init>((String)localObject2);
              throw paramString;
            }
          }
          else
          {
            paramString = new com/mopub/mraid/a;
            localObject2 = "Invalid state to execute this command";
            paramString.<init>((String)localObject2);
            throw paramString;
          }
        }
        catch (a locala)
        {
          paramString = locala.getMessage();
          a((MraidJavascriptCommand)localObject1, paramString);
          paramString = new java/lang/StringBuilder;
          paramString.<init>("window.mraidbridge.nativeCallComplete(");
          localObject1 = JSONObject.quote(a);
          paramString.append((String)localObject1);
          paramString.append(")");
          paramString = paramString.toString();
          a(paramString);
          return bool1;
        }
      }
      boolean bool10 = g;
      if (bool10)
      {
        localObject1 = new android/content/Intent;
        ((Intent)localObject1).<init>();
        ((Intent)localObject1).setAction("android.intent.action.VIEW");
        localObject2 = Uri.parse(paramString);
        ((Intent)localObject1).setData((Uri)localObject2);
        j = 268435456;
        ((Intent)localObject1).addFlags(j);
        try
        {
          localObject2 = f;
          if (localObject2 == null)
          {
            localObject1 = "WebView was detached. Unable to load a URL";
            MoPubLog.d((String)localObject1);
            return bool1;
          }
          localObject2 = f;
          localObject2 = ((MraidBridge.MraidWebView)localObject2).getContext();
          ((Context)localObject2).startActivity((Intent)localObject1);
          return bool1;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
          paramString = String.valueOf(paramString);
          MoPubLog.d("No activity found to handle this URL ".concat(paramString));
          return false;
        }
      }
      return false;
    }
    catch (URISyntaxException localURISyntaxException)
    {
      paramString = String.valueOf(paramString);
      MoPubLog.d("Invalid MRAID URL: ".concat(paramString));
      paramString = MraidJavascriptCommand.UNSPECIFIED;
      a(paramString, "Mraid command sent an invalid URL");
    }
    return bool1;
  }
  
  final boolean c()
  {
    MraidBridge.MraidWebView localMraidWebView = f;
    return localMraidWebView != null;
  }
  
  public void notifyScreenMetrics(c paramc)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("mraidbridge.setScreenSize(");
    String str = b(b);
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(");mraidbridge.setMaxSize(");
    str = b(d);
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(");mraidbridge.setCurrentPosition(");
    str = a(f);
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(");mraidbridge.setDefaultPosition(");
    str = a(h);
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(")");
    localObject = ((StringBuilder)localObject).toString();
    a((String)localObject);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("mraidbridge.notifySizeChangeEvent(");
    paramc = b(f);
    ((StringBuilder)localObject).append(paramc);
    ((StringBuilder)localObject).append(")");
    paramc = ((StringBuilder)localObject).toString();
    a(paramc);
  }
  
  public void setContentHtml(String paramString)
  {
    MraidBridge.MraidWebView localMraidWebView = f;
    if (localMraidWebView == null)
    {
      MoPubLog.d("MRAID bridge called setContentHtml before WebView was attached");
      return;
    }
    b = false;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = Networking.getBaseUrlScheme();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("://ads.mopub.com/");
    localObject = ((StringBuilder)localObject).toString();
    str = paramString;
    localMraidWebView.loadDataWithBaseURL((String)localObject, paramString, "text/html", "UTF-8", null);
  }
  
  public void setContentUrl(String paramString)
  {
    MraidBridge.MraidWebView localMraidWebView = f;
    if (localMraidWebView == null)
    {
      MoPubLog.d("MRAID bridge called setContentHtml while WebView was not attached");
      return;
    }
    b = false;
    localMraidWebView.loadUrl(paramString);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBridge
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */