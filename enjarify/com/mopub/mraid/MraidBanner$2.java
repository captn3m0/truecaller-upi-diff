package com.mopub.mraid;

import android.app.Activity;
import android.content.Context;
import android.webkit.WebSettings;
import com.mopub.common.ExternalViewabilitySessionManager;

final class MraidBanner$2
  implements MraidController.MraidWebViewCacheListener
{
  MraidBanner$2(MraidBanner paramMraidBanner, Context paramContext) {}
  
  public final void onReady(MraidBridge.MraidWebView paramMraidWebView, ExternalViewabilitySessionManager paramExternalViewabilitySessionManager)
  {
    paramMraidWebView.getSettings().setJavaScriptEnabled(true);
    paramExternalViewabilitySessionManager = a;
    boolean bool1 = paramExternalViewabilitySessionManager instanceof Activity;
    if (bool1)
    {
      Object localObject1 = b;
      Object localObject2 = new com/mopub/common/ExternalViewabilitySessionManager;
      ((ExternalViewabilitySessionManager)localObject2).<init>((Context)paramExternalViewabilitySessionManager);
      MraidBanner.a((MraidBanner)localObject1, (ExternalViewabilitySessionManager)localObject2);
      paramExternalViewabilitySessionManager = MraidBanner.c(b);
      localObject1 = a;
      localObject2 = b;
      boolean bool2 = MraidBanner.b((MraidBanner)localObject2);
      paramExternalViewabilitySessionManager.createDisplaySession((Context)localObject1, paramMraidWebView, bool2);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBanner.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */