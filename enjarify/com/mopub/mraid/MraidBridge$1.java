package com.mopub.mraid;

import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebView;

final class MraidBridge$1
  extends WebChromeClient
{
  MraidBridge$1(MraidBridge paramMraidBridge) {}
  
  public final boolean onConsoleMessage(ConsoleMessage paramConsoleMessage)
  {
    MraidBridge.MraidBridgeListener localMraidBridgeListener = MraidBridge.a(a);
    if (localMraidBridgeListener != null) {
      return MraidBridge.a(a).onConsoleMessage(paramConsoleMessage);
    }
    return super.onConsoleMessage(paramConsoleMessage);
  }
  
  public final boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
  {
    MraidBridge.MraidBridgeListener localMraidBridgeListener = MraidBridge.a(a);
    if (localMraidBridgeListener != null) {
      return MraidBridge.a(a).onJsAlert(paramString2, paramJsResult);
    }
    return super.onJsAlert(paramWebView, paramString1, paramString2, paramJsResult);
  }
  
  public final void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback)
  {
    super.onShowCustomView(paramView, paramCustomViewCallback);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBridge.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */