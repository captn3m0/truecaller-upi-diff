package com.mopub.mraid;

public enum PlacementType
{
  static
  {
    Object localObject = new com/mopub/mraid/PlacementType;
    ((PlacementType)localObject).<init>("INLINE", 0);
    INLINE = (PlacementType)localObject;
    localObject = new com/mopub/mraid/PlacementType;
    int i = 1;
    ((PlacementType)localObject).<init>("INTERSTITIAL", i);
    INTERSTITIAL = (PlacementType)localObject;
    localObject = new PlacementType[2];
    PlacementType localPlacementType = INLINE;
    localObject[0] = localPlacementType;
    localPlacementType = INTERSTITIAL;
    localObject[i] = localPlacementType;
    a = (PlacementType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.PlacementType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */