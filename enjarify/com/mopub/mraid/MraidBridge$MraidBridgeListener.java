package com.mopub.mraid;

import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import com.mopub.common.CloseableLayout.ClosePosition;
import java.net.URI;

public abstract interface MraidBridge$MraidBridgeListener
{
  public abstract void onClose();
  
  public abstract boolean onConsoleMessage(ConsoleMessage paramConsoleMessage);
  
  public abstract void onExpand(URI paramURI, boolean paramBoolean);
  
  public abstract boolean onJsAlert(String paramString, JsResult paramJsResult);
  
  public abstract void onOpen(URI paramURI);
  
  public abstract void onPageFailedToLoad();
  
  public abstract void onPageLoaded();
  
  public abstract void onPlayVideo(URI paramURI);
  
  public abstract void onResize(int paramInt1, int paramInt2, int paramInt3, int paramInt4, CloseableLayout.ClosePosition paramClosePosition, boolean paramBoolean);
  
  public abstract void onSetOrientationProperties(boolean paramBoolean, b paramb);
  
  public abstract void onUseCustomClose(boolean paramBoolean);
  
  public abstract void onVisibilityChanged(boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBridge.MraidBridgeListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */