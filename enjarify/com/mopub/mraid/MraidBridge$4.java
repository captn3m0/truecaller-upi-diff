package com.mopub.mraid;

final class MraidBridge$4
  implements MraidBridge.MraidWebView.OnVisibilityChangedListener
{
  MraidBridge$4(MraidBridge paramMraidBridge) {}
  
  public final void onVisibilityChanged(boolean paramBoolean)
  {
    MraidBridge.MraidBridgeListener localMraidBridgeListener = MraidBridge.a(a);
    if (localMraidBridgeListener != null)
    {
      localMraidBridgeListener = MraidBridge.a(a);
      localMraidBridgeListener.onVisibilityChanged(paramBoolean);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBridge.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */