package com.mopub.mraid;

import android.view.View;
import com.mopub.mobileads.AdViewController;
import com.mopub.mobileads.CustomEventBanner.CustomEventBannerListener;
import com.mopub.mobileads.MoPubErrorCode;

final class MraidBanner$1
  implements MraidController.MraidListener
{
  MraidBanner$1(MraidBanner paramMraidBanner) {}
  
  public final void onClose()
  {
    MraidBanner.a(a).onBannerCollapsed();
  }
  
  public final void onExpand()
  {
    MraidBanner.a(a).onBannerExpanded();
    MraidBanner.a(a).onBannerClicked();
  }
  
  public final void onFailedToLoad()
  {
    CustomEventBanner.CustomEventBannerListener localCustomEventBannerListener = MraidBanner.a(a);
    MoPubErrorCode localMoPubErrorCode = MoPubErrorCode.MRAID_LOAD_ERROR;
    localCustomEventBannerListener.onBannerFailed(localMoPubErrorCode);
  }
  
  public final void onLoaded(View paramView)
  {
    AdViewController.setShouldHonorServerDimensions(paramView);
    MraidBanner.a(a).onBannerLoaded(paramView);
  }
  
  public final void onOpen()
  {
    MraidBanner.a(a).onBannerClicked();
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBanner.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */