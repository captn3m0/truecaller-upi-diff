package com.mopub.mraid;

import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import com.mopub.common.CloseableLayout.ClosePosition;
import java.net.URI;

final class MraidController$4
  implements MraidBridge.MraidBridgeListener
{
  MraidController$4(MraidController paramMraidController) {}
  
  public final void onClose()
  {
    a.b();
  }
  
  public final boolean onConsoleMessage(ConsoleMessage paramConsoleMessage)
  {
    return a.a(paramConsoleMessage);
  }
  
  public final void onExpand(URI paramURI, boolean paramBoolean) {}
  
  public final boolean onJsAlert(String paramString, JsResult paramJsResult)
  {
    return a.a(paramString, paramJsResult);
  }
  
  public final void onOpen(URI paramURI)
  {
    MraidController localMraidController = a;
    paramURI = paramURI.toString();
    localMraidController.b(paramURI);
  }
  
  public final void onPageFailedToLoad() {}
  
  public final void onPageLoaded()
  {
    MraidController localMraidController = a;
    MraidController.6 local6 = new com/mopub/mraid/MraidController$6;
    local6.<init>(localMraidController);
    localMraidController.a(local6);
  }
  
  public final void onPlayVideo(URI paramURI)
  {
    MraidController localMraidController = a;
    paramURI = paramURI.toString();
    localMraidController.a(paramURI);
  }
  
  public final void onResize(int paramInt1, int paramInt2, int paramInt3, int paramInt4, CloseableLayout.ClosePosition paramClosePosition, boolean paramBoolean)
  {
    a locala = new com/mopub/mraid/a;
    locala.<init>("Not allowed to resize from an expanded state");
    throw locala;
  }
  
  public final void onSetOrientationProperties(boolean paramBoolean, b paramb)
  {
    a.a(paramBoolean, paramb);
  }
  
  public final void onUseCustomClose(boolean paramBoolean)
  {
    a.a(paramBoolean);
  }
  
  public final void onVisibilityChanged(boolean paramBoolean)
  {
    MraidController.c(a).a(paramBoolean);
    MraidController.b(a).a(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */