package com.mopub.mraid;

import android.content.Context;
import android.widget.Toast;
import com.mopub.common.logging.MoPubLog;

final class MraidNativeCommandHandler$1
  implements MraidNativeCommandHandler.a.a
{
  MraidNativeCommandHandler$1(MraidNativeCommandHandler paramMraidNativeCommandHandler, Context paramContext, MraidNativeCommandHandler.c paramc) {}
  
  public final void onFailure()
  {
    Toast.makeText(a, "Image failed to download.", 0).show();
    MoPubLog.d("Error downloading and saving image file.");
    MraidNativeCommandHandler.c localc = b;
    a locala = new com/mopub/mraid/a;
    locala.<init>("Error downloading and saving image file.");
    localc.onFailure(locala);
  }
  
  public final void onSuccess()
  {
    MoPubLog.d("Image successfully saved.");
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidNativeCommandHandler.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */