package com.mopub.mraid;

import com.mopub.common.Preconditions;
import com.mopub.common.VisibilityTracker.VisibilityTrackerListener;
import java.util.List;

final class MraidBridge$MraidWebView$1
  implements VisibilityTracker.VisibilityTrackerListener
{
  MraidBridge$MraidWebView$1(MraidBridge.MraidWebView paramMraidWebView) {}
  
  public final void onVisibilityChanged(List paramList1, List paramList2)
  {
    Preconditions.checkNotNull(paramList1);
    Preconditions.checkNotNull(paramList2);
    paramList2 = a;
    boolean bool = paramList1.contains(paramList2);
    MraidBridge.MraidWebView.a(paramList2, bool);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBridge.MraidWebView.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */