package com.mopub.mraid;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;

final class MraidController$7
  implements Runnable
{
  MraidController$7(MraidController paramMraidController, View paramView, Runnable paramRunnable) {}
  
  public final void run()
  {
    Object localObject1 = MraidController.d(c).getResources().getDisplayMetrics();
    Object localObject2 = MraidController.i(c);
    int i = widthPixels;
    int j = heightPixels;
    a.set(0, 0, i, j);
    localObject1 = a;
    Object localObject3 = b;
    ((c)localObject2).a((Rect)localObject1, (Rect)localObject3);
    localObject1 = new int[2];
    localObject2 = MraidController.j(c);
    ((View)localObject2).getLocationOnScreen((int[])localObject1);
    localObject3 = MraidController.i(c);
    int k = localObject1[0];
    int m = 1;
    int n = localObject1[m];
    int i1 = ((View)localObject2).getWidth();
    int i2 = ((View)localObject2).getHeight();
    Rect localRect1 = c;
    i1 += k;
    i2 += n;
    localRect1.set(k, n, i1, i2);
    localObject2 = c;
    Object localObject4 = d;
    ((c)localObject3).a((Rect)localObject2, (Rect)localObject4);
    MraidController.k(c).getLocationOnScreen((int[])localObject1);
    localObject2 = MraidController.i(c);
    i = localObject1[0];
    k = localObject1[m];
    FrameLayout localFrameLayout1 = MraidController.k(c);
    n = localFrameLayout1.getWidth();
    FrameLayout localFrameLayout2 = MraidController.k(c);
    i1 = localFrameLayout2.getHeight();
    localRect1 = g;
    n += i;
    i1 += k;
    localRect1.set(i, k, n, i1);
    localObject3 = g;
    localObject4 = h;
    ((c)localObject2).a((Rect)localObject3, (Rect)localObject4);
    a.getLocationOnScreen((int[])localObject1);
    localObject2 = MraidController.i(c);
    i = localObject1[0];
    j = localObject1[m];
    localObject4 = a;
    k = ((View)localObject4).getWidth();
    View localView = a;
    int i3 = localView.getHeight();
    Rect localRect2 = e;
    k += i;
    i3 += j;
    localRect2.set(i, j, k, i3);
    localObject1 = e;
    localObject3 = f;
    ((c)localObject2).a((Rect)localObject1, (Rect)localObject3);
    localObject1 = MraidController.c(c);
    localObject2 = MraidController.i(c);
    ((MraidBridge)localObject1).notifyScreenMetrics((c)localObject2);
    localObject1 = MraidController.b(c);
    boolean bool = ((MraidBridge)localObject1).c();
    if (bool)
    {
      localObject1 = MraidController.b(c);
      localObject2 = MraidController.i(c);
      ((MraidBridge)localObject1).notifyScreenMetrics((c)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null) {
      ((Runnable)localObject1).run();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */