package com.mopub.mraid;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.AsyncTasks;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Intents;
import com.mopub.common.util.Utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class MraidNativeCommandHandler
{
  public static final String ANDROID_CALENDAR_CONTENT_TYPE = "vnd.android.cursor.item/event";
  private static final String[] a = { "yyyy-MM-dd'T'HH:mm:ssZZZZZ", "yyyy-MM-dd'T'HH:mmZZZZZ" };
  
  private static Date a(String paramString)
  {
    String[] arrayOfString = a;
    int i = arrayOfString.length;
    Date localDate = null;
    int j = 0;
    for (;;)
    {
      if (j < i)
      {
        String str = arrayOfString[j];
        try
        {
          SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
          Locale localLocale = Locale.US;
          localSimpleDateFormat.<init>(str, localLocale);
          localDate = localSimpleDateFormat.parse(paramString);
          if (localDate != null) {
            return localDate;
          }
        }
        catch (ParseException localParseException)
        {
          j += 1;
        }
      }
    }
    return localDate;
  }
  
  static void a(Context paramContext, Map paramMap)
  {
    boolean bool1 = c(paramContext);
    if (bool1) {
      try
      {
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        Object localObject2 = "description";
        boolean bool2 = paramMap.containsKey(localObject2);
        if (bool2)
        {
          localObject2 = "start";
          bool2 = paramMap.containsKey(localObject2);
          if (bool2)
          {
            localObject2 = "title";
            Object localObject3 = "description";
            localObject3 = paramMap.get(localObject3);
            ((Map)localObject1).put(localObject2, localObject3);
            localObject2 = "start";
            bool2 = paramMap.containsKey(localObject2);
            if (bool2)
            {
              localObject2 = "start";
              localObject2 = paramMap.get(localObject2);
              if (localObject2 != null)
              {
                localObject2 = "start";
                localObject2 = paramMap.get(localObject2);
                localObject2 = (String)localObject2;
                localObject2 = a((String)localObject2);
                if (localObject2 != null)
                {
                  localObject3 = "beginTime";
                  long l = ((Date)localObject2).getTime();
                  localObject2 = Long.valueOf(l);
                  ((Map)localObject1).put(localObject3, localObject2);
                  localObject2 = "end";
                  bool2 = paramMap.containsKey(localObject2);
                  if (bool2)
                  {
                    localObject2 = "end";
                    localObject2 = paramMap.get(localObject2);
                    if (localObject2 != null)
                    {
                      localObject2 = "end";
                      localObject2 = paramMap.get(localObject2);
                      localObject2 = (String)localObject2;
                      localObject2 = a((String)localObject2);
                      if (localObject2 != null)
                      {
                        localObject3 = "endTime";
                        l = ((Date)localObject2).getTime();
                        localObject2 = Long.valueOf(l);
                        ((Map)localObject1).put(localObject3, localObject2);
                      }
                      else
                      {
                        paramContext = new java/lang/IllegalArgumentException;
                        paramMap = "Invalid calendar event: end time is malformed. Date format expecting (yyyy-MM-DDTHH:MM:SS-xx:xx) or (yyyy-MM-DDTHH:MM-xx:xx) i.e. 2013-08-14T09:00:01-08:00";
                        paramContext.<init>(paramMap);
                        throw paramContext;
                      }
                    }
                  }
                  localObject2 = "location";
                  bool2 = paramMap.containsKey(localObject2);
                  if (bool2)
                  {
                    localObject2 = "eventLocation";
                    localObject3 = "location";
                    localObject3 = paramMap.get(localObject3);
                    ((Map)localObject1).put(localObject2, localObject3);
                  }
                  localObject2 = "summary";
                  bool2 = paramMap.containsKey(localObject2);
                  if (bool2)
                  {
                    localObject2 = "description";
                    localObject3 = "summary";
                    localObject3 = paramMap.get(localObject3);
                    ((Map)localObject1).put(localObject2, localObject3);
                  }
                  localObject2 = "transparency";
                  bool2 = paramMap.containsKey(localObject2);
                  if (bool2)
                  {
                    localObject2 = "availability";
                    localObject3 = "transparency";
                    localObject3 = paramMap.get(localObject3);
                    localObject3 = (String)localObject3;
                    localObject4 = "transparent";
                    int j = ((String)localObject3).equals(localObject4);
                    if (j != 0)
                    {
                      j = 1;
                    }
                    else
                    {
                      j = 0;
                      localObject3 = null;
                    }
                    localObject3 = Integer.valueOf(j);
                    ((Map)localObject1).put(localObject2, localObject3);
                  }
                  localObject2 = "rrule";
                  localObject3 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject3).<init>();
                  Object localObject4 = "frequency";
                  boolean bool4 = paramMap.containsKey(localObject4);
                  if (bool4)
                  {
                    localObject4 = "frequency";
                    localObject4 = paramMap.get(localObject4);
                    localObject4 = (String)localObject4;
                    Object localObject5 = "interval";
                    boolean bool5 = paramMap.containsKey(localObject5);
                    int n = -1;
                    int m;
                    if (bool5)
                    {
                      localObject5 = "interval";
                      localObject5 = paramMap.get(localObject5);
                      localObject5 = (String)localObject5;
                      m = Integer.parseInt((String)localObject5);
                    }
                    else
                    {
                      m = -1;
                    }
                    String str1 = "daily";
                    boolean bool7 = str1.equals(localObject4);
                    if (bool7)
                    {
                      paramMap = "FREQ=DAILY;";
                      ((StringBuilder)localObject3).append(paramMap);
                      if (m != n)
                      {
                        paramMap = new java/lang/StringBuilder;
                        localObject4 = "INTERVAL=";
                        paramMap.<init>((String)localObject4);
                        paramMap.append(m);
                        localObject4 = ";";
                        paramMap.append((String)localObject4);
                        paramMap = paramMap.toString();
                        ((StringBuilder)localObject3).append(paramMap);
                      }
                    }
                    else
                    {
                      str1 = "weekly";
                      bool7 = str1.equals(localObject4);
                      String str2;
                      if (bool7)
                      {
                        localObject4 = "FREQ=WEEKLY;";
                        ((StringBuilder)localObject3).append((String)localObject4);
                        if (m != n)
                        {
                          localObject4 = new java/lang/StringBuilder;
                          str2 = "INTERVAL=";
                          ((StringBuilder)localObject4).<init>(str2);
                          ((StringBuilder)localObject4).append(m);
                          localObject5 = ";";
                          ((StringBuilder)localObject4).append((String)localObject5);
                          localObject4 = ((StringBuilder)localObject4).toString();
                          ((StringBuilder)localObject3).append((String)localObject4);
                        }
                        localObject4 = "daysInWeek";
                        bool4 = paramMap.containsKey(localObject4);
                        if (bool4)
                        {
                          localObject4 = "daysInWeek";
                          paramMap = paramMap.get(localObject4);
                          paramMap = (String)paramMap;
                          paramMap = b(paramMap);
                          if (paramMap != null)
                          {
                            localObject4 = new java/lang/StringBuilder;
                            localObject5 = "BYDAY=";
                            ((StringBuilder)localObject4).<init>((String)localObject5);
                            ((StringBuilder)localObject4).append(paramMap);
                            paramMap = ";";
                            ((StringBuilder)localObject4).append(paramMap);
                            paramMap = ((StringBuilder)localObject4).toString();
                            ((StringBuilder)localObject3).append(paramMap);
                          }
                          else
                          {
                            paramContext = new java/lang/IllegalArgumentException;
                            paramMap = "invalid ";
                            paramContext.<init>(paramMap);
                            throw paramContext;
                          }
                        }
                      }
                      else
                      {
                        str1 = "monthly";
                        bool4 = str1.equals(localObject4);
                        if (bool4)
                        {
                          localObject4 = "FREQ=MONTHLY;";
                          ((StringBuilder)localObject3).append((String)localObject4);
                          if (m != n)
                          {
                            localObject4 = new java/lang/StringBuilder;
                            str2 = "INTERVAL=";
                            ((StringBuilder)localObject4).<init>(str2);
                            ((StringBuilder)localObject4).append(m);
                            localObject5 = ";";
                            ((StringBuilder)localObject4).append((String)localObject5);
                            localObject4 = ((StringBuilder)localObject4).toString();
                            ((StringBuilder)localObject3).append((String)localObject4);
                          }
                          localObject4 = "daysInMonth";
                          bool4 = paramMap.containsKey(localObject4);
                          if (bool4)
                          {
                            localObject4 = "daysInMonth";
                            paramMap = paramMap.get(localObject4);
                            paramMap = (String)paramMap;
                            paramMap = c(paramMap);
                            if (paramMap != null)
                            {
                              localObject4 = new java/lang/StringBuilder;
                              localObject5 = "BYMONTHDAY=";
                              ((StringBuilder)localObject4).<init>((String)localObject5);
                              ((StringBuilder)localObject4).append(paramMap);
                              paramMap = ";";
                              ((StringBuilder)localObject4).append(paramMap);
                              paramMap = ((StringBuilder)localObject4).toString();
                              ((StringBuilder)localObject3).append(paramMap);
                            }
                            else
                            {
                              paramContext = new java/lang/IllegalArgumentException;
                              paramContext.<init>();
                              throw paramContext;
                            }
                          }
                        }
                        else
                        {
                          paramContext = new java/lang/IllegalArgumentException;
                          paramMap = "frequency is only supported for daily, weekly, and monthly.";
                          paramContext.<init>(paramMap);
                          throw paramContext;
                        }
                      }
                    }
                  }
                  paramMap = ((StringBuilder)localObject3).toString();
                  ((Map)localObject1).put(localObject2, paramMap);
                  paramMap = new android/content/Intent;
                  localObject2 = "android.intent.action.INSERT";
                  paramMap.<init>((String)localObject2);
                  localObject2 = "vnd.android.cursor.item/event";
                  paramMap = paramMap.setType((String)localObject2);
                  localObject2 = ((Map)localObject1).keySet();
                  localObject2 = ((Set)localObject2).iterator();
                  for (;;)
                  {
                    boolean bool3 = ((Iterator)localObject2).hasNext();
                    if (!bool3) {
                      break;
                    }
                    localObject3 = ((Iterator)localObject2).next();
                    localObject3 = (String)localObject3;
                    localObject4 = ((Map)localObject1).get(localObject3);
                    boolean bool6 = localObject4 instanceof Long;
                    if (bool6)
                    {
                      localObject4 = (Long)localObject4;
                      l = ((Long)localObject4).longValue();
                      paramMap.putExtra((String)localObject3, l);
                    }
                    else
                    {
                      bool6 = localObject4 instanceof Integer;
                      if (bool6)
                      {
                        localObject4 = (Integer)localObject4;
                        int k = ((Integer)localObject4).intValue();
                        paramMap.putExtra((String)localObject3, k);
                      }
                      else
                      {
                        localObject4 = (String)localObject4;
                        paramMap.putExtra((String)localObject3, (String)localObject4);
                      }
                    }
                  }
                  int i = 268435456;
                  paramMap.setFlags(i);
                  paramContext.startActivity(paramMap);
                  return;
                }
                paramContext = new java/lang/IllegalArgumentException;
                paramMap = "Invalid calendar event: start time is malformed. Date format expecting (yyyy-MM-DDTHH:MM:SS-xx:xx) or (yyyy-MM-DDTHH:MM-xx:xx) i.e. 2013-08-14T09:00:01-08:00";
                paramContext.<init>(paramMap);
                throw paramContext;
              }
            }
            paramContext = new java/lang/IllegalArgumentException;
            paramMap = "Invalid calendar event: start is null.";
            paramContext.<init>(paramMap);
            throw paramContext;
          }
        }
        paramContext = new java/lang/IllegalArgumentException;
        paramMap = "Missing start and description fields";
        paramContext.<init>(paramMap);
        throw paramContext;
      }
      catch (Exception paramContext)
      {
        MoPubLog.d("could not create calendar event");
        paramMap = new com/mopub/mraid/a;
        paramMap.<init>(paramContext);
        throw paramMap;
      }
      catch (IllegalArgumentException paramContext)
      {
        paramMap = new java/lang/StringBuilder;
        paramMap.<init>("create calendar: invalid parameters ");
        Object localObject1 = paramContext.getMessage();
        paramMap.append((String)localObject1);
        MoPubLog.d(paramMap.toString());
        paramMap = new com/mopub/mraid/a;
        paramMap.<init>(paramContext);
        throw paramMap;
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        MoPubLog.d("no calendar app installed");
        paramContext = new com/mopub/mraid/a;
        paramContext.<init>("Action is unsupported on this device - no calendar app installed");
        throw paramContext;
      }
    }
    MoPubLog.d("unsupported action createCalendarEvent for devices pre-ICS");
    paramContext = new com/mopub/mraid/a;
    paramContext.<init>("Action is unsupported on this device (need Android version Ice Cream Sandwich or above)");
    throw paramContext;
  }
  
  static boolean a(Activity paramActivity, View paramView)
  {
    int j;
    for (;;)
    {
      boolean bool1 = paramView.isHardwareAccelerated();
      if (!bool1) {
        break label99;
      }
      int i = paramView.getLayerType();
      j = 1;
      boolean bool2 = Utils.bitMaskContainsFlag(i, j);
      if (bool2) {
        break label99;
      }
      ViewParent localViewParent = paramView.getParent();
      bool2 = localViewParent instanceof View;
      if (!bool2) {
        break;
      }
      paramView = (View)paramView.getParent();
    }
    paramActivity = paramActivity.getWindow();
    if (paramActivity != null)
    {
      paramActivity = paramActivity.getAttributes();
      int k = flags;
      int m = 16777216;
      boolean bool3 = Utils.bitMaskContainsFlag(k, m);
      if (bool3) {
        return j;
      }
    }
    return false;
    label99:
    return false;
  }
  
  static boolean a(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.DIAL");
    Uri localUri = Uri.parse("tel:");
    localIntent.setData(localUri);
    return Intents.deviceCanHandleIntent(paramContext, localIntent);
  }
  
  private static String b(String paramString)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    int i = 7;
    boolean[] arrayOfBoolean = new boolean[i];
    String str1 = ",";
    paramString = paramString.split(str1);
    int j = paramString.length;
    int k = 0;
    int m;
    for (;;)
    {
      m = 1;
      if (k >= j) {
        break;
      }
      String str2 = paramString[k];
      int n = Integer.parseInt(str2);
      if (n == i)
      {
        n = 0;
        str2 = null;
      }
      int i1 = arrayOfBoolean[n];
      if (i1 == 0)
      {
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        switch (n)
        {
        default: 
          paramString = new java/lang/IllegalArgumentException;
          localObject1 = String.valueOf(n);
          localObject1 = "invalid day of week ".concat((String)localObject1);
          paramString.<init>((String)localObject1);
          throw paramString;
        case 6: 
          str3 = "SA";
          break;
        case 5: 
          str3 = "FR";
          break;
        case 4: 
          str3 = "TH";
          break;
        case 3: 
          str3 = "WE";
          break;
        case 2: 
          str3 = "TU";
          break;
        case 1: 
          str3 = "MO";
          break;
        case 0: 
          str3 = "SU";
        }
        ((StringBuilder)localObject2).append(str3);
        String str3 = ",";
        ((StringBuilder)localObject2).append(str3);
        localObject2 = ((StringBuilder)localObject2).toString();
        ((StringBuilder)localObject1).append((String)localObject2);
        arrayOfBoolean[n] = m;
      }
      k += 1;
    }
    int i2 = paramString.length;
    if (i2 != 0)
    {
      i2 = ((StringBuilder)localObject1).length() - m;
      ((StringBuilder)localObject1).deleteCharAt(i2);
      return ((StringBuilder)localObject1).toString();
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("must have at least 1 day of the week if specifying repeating weekly");
    throw paramString;
  }
  
  static boolean b(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.VIEW");
    Uri localUri = Uri.parse("sms:");
    localIntent.setData(localUri);
    return Intents.deviceCanHandleIntent(paramContext, localIntent);
  }
  
  private static String c(String paramString)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    int i = 63;
    boolean[] arrayOfBoolean = new boolean[i];
    String str1 = ",";
    paramString = paramString.split(str1);
    int j = paramString.length;
    int k = 0;
    int m;
    for (;;)
    {
      m = 1;
      if (k >= j) {
        break;
      }
      String str2 = paramString[k];
      int n = Integer.parseInt(str2);
      int i1 = n + 31;
      int i2 = arrayOfBoolean[i1];
      if (i2 == 0)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        if (n != 0)
        {
          int i3 = -31;
          if (n >= i3)
          {
            i3 = 31;
            if (n <= i3)
            {
              str2 = String.valueOf(n);
              localStringBuilder.append(str2);
              localStringBuilder.append(",");
              str2 = localStringBuilder.toString();
              ((StringBuilder)localObject).append(str2);
              arrayOfBoolean[i1] = m;
              break label184;
            }
          }
        }
        paramString = new java/lang/IllegalArgumentException;
        localObject = String.valueOf(n);
        localObject = "invalid day of month ".concat((String)localObject);
        paramString.<init>((String)localObject);
        throw paramString;
      }
      label184:
      k += 1;
    }
    int i4 = paramString.length;
    if (i4 != 0)
    {
      i4 = ((StringBuilder)localObject).length() - m;
      ((StringBuilder)localObject).deleteCharAt(i4);
      return ((StringBuilder)localObject).toString();
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("must have at least 1 day of the month if specifying repeating weekly");
    throw paramString;
  }
  
  static boolean c(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.INSERT");
    localIntent = localIntent.setType("vnd.android.cursor.item/event");
    return Intents.deviceCanHandleIntent(paramContext, localIntent);
  }
  
  public static boolean isStorePictureSupported(Context paramContext)
  {
    String str1 = "mounted";
    String str2 = Environment.getExternalStorageState();
    boolean bool1 = str1.equals(str2);
    if (bool1)
    {
      str1 = "android.permission.WRITE_EXTERNAL_STORAGE";
      boolean bool2 = DeviceUtils.isPermissionGranted(paramContext, str1);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  final void a(Context paramContext, String paramString, MraidNativeCommandHandler.c paramc)
  {
    MraidNativeCommandHandler.a locala = new com/mopub/mraid/MraidNativeCommandHandler$a;
    MraidNativeCommandHandler.1 local1 = new com/mopub/mraid/MraidNativeCommandHandler$1;
    local1.<init>(this, paramContext, paramc);
    locala.<init>(paramContext, local1);
    paramContext = new String[1];
    paramContext[0] = paramString;
    AsyncTasks.safeExecuteOnExecutor(locala, paramContext);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidNativeCommandHandler
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */