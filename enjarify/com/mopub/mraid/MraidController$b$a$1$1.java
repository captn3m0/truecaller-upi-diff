package com.mopub.mraid;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;

final class MraidController$b$a$1$1
  implements ViewTreeObserver.OnPreDrawListener
{
  MraidController$b$a$1$1(MraidController.b.a.1 param1, View paramView) {}
  
  public final boolean onPreDraw()
  {
    a.getViewTreeObserver().removeOnPreDrawListener(this);
    MraidController.b.a.a(b.a);
    return true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.b.a.1.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */