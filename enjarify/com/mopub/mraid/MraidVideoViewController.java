package com.mopub.mraid;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.VideoView;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Drawables;
import com.mopub.mobileads.BaseVideoViewController;
import com.mopub.mobileads.BaseVideoViewController.BaseVideoViewControllerListener;

public class MraidVideoViewController
  extends BaseVideoViewController
{
  private final VideoView a;
  private ImageButton b;
  private int c;
  private int d;
  
  public MraidVideoViewController(Context paramContext, Bundle paramBundle1, Bundle paramBundle2, BaseVideoViewController.BaseVideoViewControllerListener paramBaseVideoViewControllerListener)
  {
    super(paramContext, null, paramBaseVideoViewControllerListener);
    paramBundle2 = new android/widget/VideoView;
    paramBundle2.<init>(paramContext);
    a = paramBundle2;
    paramContext = a;
    paramBundle2 = new com/mopub/mraid/MraidVideoViewController$1;
    paramBundle2.<init>(this);
    paramContext.setOnCompletionListener(paramBundle2);
    paramContext = a;
    paramBundle2 = new com/mopub/mraid/MraidVideoViewController$2;
    paramBundle2.<init>(this);
    paramContext.setOnErrorListener(paramBundle2);
    paramContext = a;
    paramBundle1 = paramBundle1.getString("video_url");
    paramContext.setVideoPath(paramBundle1);
  }
  
  public final VideoView getVideoView()
  {
    return a;
  }
  
  public final void onBackPressed() {}
  
  public final void onConfigurationChanged(Configuration paramConfiguration) {}
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject1 = mContext;
    int i = Dips.asIntPixels(50.0F, (Context)localObject1);
    d = i;
    localObject1 = mContext;
    i = Dips.asIntPixels(8.0F, (Context)localObject1);
    c = i;
    localObject1 = new android/widget/ImageButton;
    Object localObject2 = mContext;
    ((ImageButton)localObject1).<init>((Context)localObject2);
    b = ((ImageButton)localObject1);
    localObject1 = new android/graphics/drawable/StateListDrawable;
    ((StateListDrawable)localObject1).<init>();
    int j = 1;
    Object localObject3 = new int[j];
    localObject3[0] = -16842919;
    Object localObject4 = Drawables.INTERSTITIAL_CLOSE_BUTTON_NORMAL;
    Context localContext = mContext;
    localObject4 = ((Drawables)localObject4).createDrawable(localContext);
    ((StateListDrawable)localObject1).addState((int[])localObject3, (Drawable)localObject4);
    localObject2 = new int[j];
    localObject2[0] = 16842919;
    localObject3 = Drawables.INTERSTITIAL_CLOSE_BUTTON_PRESSED;
    localObject4 = mContext;
    localObject3 = ((Drawables)localObject3).createDrawable((Context)localObject4);
    ((StateListDrawable)localObject1).addState((int[])localObject2, (Drawable)localObject3);
    b.setImageDrawable((Drawable)localObject1);
    b.setBackgroundDrawable(null);
    localObject1 = b;
    localObject2 = new com/mopub/mraid/MraidVideoViewController$3;
    ((MraidVideoViewController.3)localObject2).<init>(this);
    ((ImageButton)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = new android/widget/RelativeLayout$LayoutParams;
    j = d;
    ((RelativeLayout.LayoutParams)localObject1).<init>(j, j);
    ((RelativeLayout.LayoutParams)localObject1).addRule(11);
    j = c;
    ((RelativeLayout.LayoutParams)localObject1).setMargins(j, 0, j, 0);
    localObject2 = getLayout();
    localObject3 = b;
    ((ViewGroup)localObject2).addView((View)localObject3, (ViewGroup.LayoutParams)localObject1);
    b.setVisibility(8);
    a.start();
  }
  
  public final void onDestroy() {}
  
  public final void onPause() {}
  
  public final void onResume() {}
  
  public final void onSaveInstanceState(Bundle paramBundle) {}
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidVideoViewController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */