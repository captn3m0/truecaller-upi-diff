package com.mopub.mraid;

import java.util.Locale;

public enum ViewState
{
  static
  {
    Object localObject = new com/mopub/mraid/ViewState;
    ((ViewState)localObject).<init>("LOADING", 0);
    LOADING = (ViewState)localObject;
    localObject = new com/mopub/mraid/ViewState;
    int i = 1;
    ((ViewState)localObject).<init>("DEFAULT", i);
    DEFAULT = (ViewState)localObject;
    localObject = new com/mopub/mraid/ViewState;
    int j = 2;
    ((ViewState)localObject).<init>("RESIZED", j);
    RESIZED = (ViewState)localObject;
    localObject = new com/mopub/mraid/ViewState;
    int k = 3;
    ((ViewState)localObject).<init>("EXPANDED", k);
    EXPANDED = (ViewState)localObject;
    localObject = new com/mopub/mraid/ViewState;
    int m = 4;
    ((ViewState)localObject).<init>("HIDDEN", m);
    HIDDEN = (ViewState)localObject;
    localObject = new ViewState[5];
    ViewState localViewState = LOADING;
    localObject[0] = localViewState;
    localViewState = DEFAULT;
    localObject[i] = localViewState;
    localViewState = RESIZED;
    localObject[j] = localViewState;
    localViewState = EXPANDED;
    localObject[k] = localViewState;
    localViewState = HIDDEN;
    localObject[m] = localViewState;
    a = (ViewState[])localObject;
  }
  
  public final String toJavascriptString()
  {
    String str = toString();
    Locale localLocale = Locale.US;
    return str.toLowerCase(localLocale);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.ViewState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */