package com.mopub.mraid;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.mopub.mobileads.ViewGestureDetector;

final class MraidBridge$3
  implements View.OnTouchListener
{
  MraidBridge$3(MraidBridge paramMraidBridge, ViewGestureDetector paramViewGestureDetector) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    ViewGestureDetector localViewGestureDetector = a;
    localViewGestureDetector.sendTouchEvent(paramMotionEvent);
    int i = paramMotionEvent.getAction();
    switch (i)
    {
    default: 
      break;
    case 0: 
    case 1: 
      boolean bool = paramView.hasFocus();
      if (!bool) {
        paramView.requestFocus();
      }
      break;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBridge.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */