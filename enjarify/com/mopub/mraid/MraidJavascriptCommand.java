package com.mopub.mraid;

public enum MraidJavascriptCommand
{
  final String a;
  
  static
  {
    Object localObject = new com/mopub/mraid/MraidJavascriptCommand;
    ((MraidJavascriptCommand)localObject).<init>("CLOSE", 0, "close");
    CLOSE = (MraidJavascriptCommand)localObject;
    localObject = new com/mopub/mraid/MraidJavascriptCommand$1;
    ((MraidJavascriptCommand.1)localObject).<init>("EXPAND", "expand");
    EXPAND = (MraidJavascriptCommand)localObject;
    localObject = new com/mopub/mraid/MraidJavascriptCommand;
    int i = 2;
    ((MraidJavascriptCommand)localObject).<init>("USE_CUSTOM_CLOSE", i, "usecustomclose");
    USE_CUSTOM_CLOSE = (MraidJavascriptCommand)localObject;
    localObject = new com/mopub/mraid/MraidJavascriptCommand$2;
    ((MraidJavascriptCommand.2)localObject).<init>("OPEN", "open");
    OPEN = (MraidJavascriptCommand)localObject;
    localObject = new com/mopub/mraid/MraidJavascriptCommand$3;
    ((MraidJavascriptCommand.3)localObject).<init>("RESIZE", "resize");
    RESIZE = (MraidJavascriptCommand)localObject;
    localObject = new com/mopub/mraid/MraidJavascriptCommand;
    int j = 5;
    ((MraidJavascriptCommand)localObject).<init>("SET_ORIENTATION_PROPERTIES", j, "setOrientationProperties");
    SET_ORIENTATION_PROPERTIES = (MraidJavascriptCommand)localObject;
    localObject = new com/mopub/mraid/MraidJavascriptCommand$4;
    ((MraidJavascriptCommand.4)localObject).<init>("PLAY_VIDEO", "playVideo");
    PLAY_VIDEO = (MraidJavascriptCommand)localObject;
    localObject = new com/mopub/mraid/MraidJavascriptCommand$5;
    ((MraidJavascriptCommand.5)localObject).<init>("STORE_PICTURE", "storePicture");
    STORE_PICTURE = (MraidJavascriptCommand)localObject;
    localObject = new com/mopub/mraid/MraidJavascriptCommand$6;
    ((MraidJavascriptCommand.6)localObject).<init>("CREATE_CALENDAR_EVENT", "createCalendarEvent");
    CREATE_CALENDAR_EVENT = (MraidJavascriptCommand)localObject;
    localObject = new com/mopub/mraid/MraidJavascriptCommand;
    int k = 9;
    ((MraidJavascriptCommand)localObject).<init>("UNSPECIFIED", k, "");
    UNSPECIFIED = (MraidJavascriptCommand)localObject;
    localObject = new MraidJavascriptCommand[10];
    MraidJavascriptCommand localMraidJavascriptCommand = CLOSE;
    localObject[0] = localMraidJavascriptCommand;
    localMraidJavascriptCommand = EXPAND;
    localObject[1] = localMraidJavascriptCommand;
    localMraidJavascriptCommand = USE_CUSTOM_CLOSE;
    localObject[i] = localMraidJavascriptCommand;
    localMraidJavascriptCommand = OPEN;
    localObject[3] = localMraidJavascriptCommand;
    localMraidJavascriptCommand = RESIZE;
    localObject[4] = localMraidJavascriptCommand;
    localMraidJavascriptCommand = SET_ORIENTATION_PROPERTIES;
    localObject[j] = localMraidJavascriptCommand;
    localMraidJavascriptCommand = PLAY_VIDEO;
    localObject[6] = localMraidJavascriptCommand;
    localMraidJavascriptCommand = STORE_PICTURE;
    localObject[7] = localMraidJavascriptCommand;
    localMraidJavascriptCommand = CREATE_CALENDAR_EVENT;
    localObject[8] = localMraidJavascriptCommand;
    localMraidJavascriptCommand = UNSPECIFIED;
    localObject[k] = localMraidJavascriptCommand;
    b = (MraidJavascriptCommand[])localObject;
  }
  
  private MraidJavascriptCommand(String paramString1)
  {
    a = paramString1;
  }
  
  static MraidJavascriptCommand a(String paramString)
  {
    MraidJavascriptCommand[] arrayOfMraidJavascriptCommand = values();
    int i = arrayOfMraidJavascriptCommand.length;
    int j = 0;
    while (j < i)
    {
      MraidJavascriptCommand localMraidJavascriptCommand = arrayOfMraidJavascriptCommand[j];
      String str = a;
      boolean bool = str.equals(paramString);
      if (bool) {
        return localMraidJavascriptCommand;
      }
      j += 1;
    }
    return UNSPECIFIED;
  }
  
  boolean a(PlacementType paramPlacementType)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidJavascriptCommand
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */