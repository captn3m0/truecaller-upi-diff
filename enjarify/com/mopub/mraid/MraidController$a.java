package com.mopub.mraid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.mopub.common.Preconditions;

final class MraidController$a
  extends BroadcastReceiver
{
  private Context b;
  private int c = -1;
  
  MraidController$a(MraidController paramMraidController) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = b;
    if (paramContext == null) {
      return;
    }
    paramContext = "android.intent.action.CONFIGURATION_CHANGED";
    paramIntent = paramIntent.getAction();
    boolean bool = paramContext.equals(paramIntent);
    if (bool)
    {
      paramContext = a;
      int i = MraidController.l(paramContext);
      int j = c;
      if (i != j)
      {
        c = i;
        paramContext = a;
        j = 0;
        paramIntent = null;
        paramContext.a(null);
      }
    }
  }
  
  public final void register(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
    paramContext = b;
    if (paramContext != null)
    {
      IntentFilter localIntentFilter = new android/content/IntentFilter;
      String str = "android.intent.action.CONFIGURATION_CHANGED";
      localIntentFilter.<init>(str);
      paramContext.registerReceiver(this, localIntentFilter);
    }
  }
  
  public final void unregister()
  {
    Context localContext = b;
    if (localContext != null)
    {
      localContext.unregisterReceiver(this);
      localContext = null;
      b = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidController.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */