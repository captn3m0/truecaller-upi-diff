package com.mopub.mraid;

import android.webkit.WebView;
import com.mopub.common.logging.MoPubLog;

final class MraidBridge$5
  extends MraidWebViewClient
{
  MraidBridge$5(MraidBridge paramMraidBridge) {}
  
  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    MraidBridge.c(a);
  }
  
  public final void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
  {
    String str = String.valueOf(paramString1);
    MoPubLog.d("Error: ".concat(str));
    super.onReceivedError(paramWebView, paramInt, paramString1, paramString2);
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    return a.b(paramString);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mraid.MraidBridge.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */