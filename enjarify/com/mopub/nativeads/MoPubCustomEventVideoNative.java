package com.mopub.nativeads;

import android.content.Context;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class MoPubCustomEventVideoNative
  extends CustomEventNative
{
  private MoPubCustomEventVideoNative.MoPubVideoNativeAd a;
  
  protected final void a()
  {
    MoPubCustomEventVideoNative.MoPubVideoNativeAd localMoPubVideoNativeAd = a;
    if (localMoPubVideoNativeAd == null) {
      return;
    }
    localMoPubVideoNativeAd.invalidate();
  }
  
  protected final void a(Context paramContext, CustomEventNative.CustomEventNativeListener paramCustomEventNativeListener, Map paramMap1, Map paramMap2)
  {
    Object localObject1 = paramMap1.get("com_mopub_native_json");
    boolean bool1 = localObject1 instanceof JSONObject;
    if (!bool1)
    {
      paramContext = NativeErrorCode.INVALID_RESPONSE;
      paramCustomEventNativeListener.onNativeAdFailed(paramContext);
      return;
    }
    Object localObject2 = "event-details";
    paramMap1.get(localObject2);
    MoPubCustomEventVideoNative.d locald = new com/mopub/nativeads/MoPubCustomEventVideoNative$d;
    locald.<init>(paramMap2);
    boolean bool2 = a;
    if (!bool2)
    {
      paramContext = NativeErrorCode.INVALID_RESPONSE;
      paramCustomEventNativeListener.onNativeAdFailed(paramContext);
      return;
    }
    paramMap2 = "click-tracking-url";
    paramMap1 = paramMap1.get(paramMap2);
    bool2 = paramMap1 instanceof String;
    boolean bool3;
    if (bool2)
    {
      Object localObject3 = paramMap1;
      localObject3 = (String)paramMap1;
      bool3 = TextUtils.isEmpty((CharSequence)localObject3);
      if (!bool3)
      {
        paramMap1 = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd;
        Object localObject4 = localObject1;
        localObject4 = (JSONObject)localObject1;
        paramMap1.<init>(paramContext, (JSONObject)localObject4, paramCustomEventNativeListener, locald, (String)localObject3);
        a = paramMap1;
      }
    }
    try
    {
      paramContext = a;
      paramMap1 = d;
      bool3 = MoPubCustomEventVideoNative.MoPubVideoNativeAd.a(paramMap1);
      if (bool3)
      {
        paramMap1 = d;
        paramMap1 = paramMap1.keys();
        for (;;)
        {
          bool2 = paramMap1.hasNext();
          if (!bool2) {
            break;
          }
          paramMap2 = paramMap1.next();
          paramMap2 = (String)paramMap2;
          localObject1 = MoPubCustomEventVideoNative.MoPubVideoNativeAd.a.a(paramMap2);
          if (localObject1 != null)
          {
            try
            {
              localObject2 = d;
              localObject2 = ((JSONObject)localObject2).opt(paramMap2);
              paramContext.a((MoPubCustomEventVideoNative.MoPubVideoNativeAd.a)localObject1, localObject2);
            }
            catch (ClassCastException localClassCastException)
            {
              paramContext = new java/lang/IllegalArgumentException;
              paramMap1 = new java/lang/StringBuilder;
              localObject1 = "JSONObject key (";
              paramMap1.<init>((String)localObject1);
              paramMap1.append(paramMap2);
              paramMap2 = ") contained unexpected value.";
              paramMap1.append(paramMap2);
              paramMap1 = paramMap1.toString();
              paramContext.<init>(paramMap1);
              throw paramContext;
            }
          }
          else
          {
            localObject1 = d;
            localObject1 = ((JSONObject)localObject1).opt(paramMap2);
            paramContext.addExtra(paramMap2, localObject1);
          }
        }
        paramMap1 = paramContext.getPrivacyInformationIconClickThroughUrl();
        bool3 = TextUtils.isEmpty(paramMap1);
        if (bool3)
        {
          paramMap1 = "https://www.mopub.com/optout/";
          paramContext.setPrivacyInformationIconClickThroughUrl(paramMap1);
        }
        paramMap1 = c;
        paramMap2 = new java/util/ArrayList;
        paramMap2.<init>();
        localObject1 = paramContext.getMainImageUrl();
        boolean bool4 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool4)
        {
          localObject1 = paramContext.getMainImageUrl();
          paramMap2.add(localObject1);
        }
        localObject1 = paramContext.getIconImageUrl();
        bool4 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool4)
        {
          localObject1 = paramContext.getIconImageUrl();
          paramMap2.add(localObject1);
        }
        localObject1 = paramContext.getPrivacyInformationIconImageUrl();
        bool4 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool4)
        {
          localObject1 = paramContext.getPrivacyInformationIconImageUrl();
          paramMap2.add(localObject1);
        }
        localObject1 = paramContext.c();
        paramMap2.addAll((Collection)localObject1);
        localObject1 = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$2;
        ((MoPubCustomEventVideoNative.MoPubVideoNativeAd.2)localObject1).<init>(paramContext);
        NativeImageHelper.preCacheImages(paramMap1, paramMap2, (NativeImageHelper.ImageListener)localObject1);
        return;
      }
      paramContext = new java/lang/IllegalArgumentException;
      paramMap1 = "JSONObject did not contain required keys.";
      paramContext.<init>(paramMap1);
      throw paramContext;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
    paramContext = NativeErrorCode.UNSPECIFIED;
    paramCustomEventNativeListener.onNativeAdFailed(paramContext);
    return;
    paramContext = NativeErrorCode.UNSPECIFIED;
    paramCustomEventNativeListener.onNativeAdFailed(paramContext);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */