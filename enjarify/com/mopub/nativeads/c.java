package com.mopub.nativeads;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.mopub.common.logging.MoPubLog;

final class c
{
  static final c h;
  View a;
  MediaLayout b;
  TextView c;
  TextView d;
  ImageView e;
  TextView f;
  ImageView g;
  
  static
  {
    c localc = new com/mopub/nativeads/c;
    localc.<init>();
    h = localc;
  }
  
  static c a(View paramView, MediaViewBinder paramMediaViewBinder)
  {
    c localc = new com/mopub/nativeads/c;
    localc.<init>();
    a = paramView;
    try
    {
      int i = c;
      Object localObject = paramView.findViewById(i);
      localObject = (TextView)localObject;
      c = ((TextView)localObject);
      i = d;
      localObject = paramView.findViewById(i);
      localObject = (TextView)localObject;
      d = ((TextView)localObject);
      i = e;
      localObject = paramView.findViewById(i);
      localObject = (TextView)localObject;
      f = ((TextView)localObject);
      i = b;
      localObject = paramView.findViewById(i);
      localObject = (MediaLayout)localObject;
      b = ((MediaLayout)localObject);
      i = f;
      localObject = paramView.findViewById(i);
      localObject = (ImageView)localObject;
      e = ((ImageView)localObject);
      int j = g;
      paramView = paramView.findViewById(j);
      paramView = (ImageView)paramView;
      g = paramView;
      return localc;
    }
    catch (ClassCastException paramView)
    {
      MoPubLog.w("Could not cast from id in MediaViewBinder to expected View type", paramView);
    }
    return h;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */