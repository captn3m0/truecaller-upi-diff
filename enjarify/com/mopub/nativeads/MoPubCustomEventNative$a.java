package com.mopub.nativeads;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Numbers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

final class MoPubCustomEventNative$a
  extends StaticNativeAd
{
  private final Context c;
  private final CustomEventNative.CustomEventNativeListener d;
  private final JSONObject e;
  private final ImpressionTracker f;
  private final NativeClickHandler g;
  
  MoPubCustomEventNative$a(Context paramContext, JSONObject paramJSONObject, ImpressionTracker paramImpressionTracker, NativeClickHandler paramNativeClickHandler, CustomEventNative.CustomEventNativeListener paramCustomEventNativeListener)
  {
    e = paramJSONObject;
    paramContext = paramContext.getApplicationContext();
    c = paramContext;
    f = paramImpressionTracker;
    g = paramNativeClickHandler;
    d = paramCustomEventNativeListener;
  }
  
  private List d()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int i = getExtras().size();
    localArrayList.<init>(i);
    Iterator localIterator = getExtras().entrySet().iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Map.Entry)localIterator.next();
      Object localObject2 = (String)((Map.Entry)localObject1).getKey();
      if (localObject2 != null)
      {
        Object localObject3 = Locale.US;
        localObject2 = ((String)localObject2).toLowerCase((Locale)localObject3);
        localObject3 = "image";
        bool2 = ((String)localObject2).endsWith((String)localObject3);
        if (bool2)
        {
          bool2 = true;
          break label119;
        }
      }
      boolean bool2 = false;
      localObject2 = null;
      label119:
      if (bool2)
      {
        localObject2 = ((Map.Entry)localObject1).getValue();
        bool2 = localObject2 instanceof String;
        if (bool2)
        {
          localObject1 = (String)((Map.Entry)localObject1).getValue();
          localArrayList.add(localObject1);
        }
      }
    }
    return localArrayList;
  }
  
  final void c()
  {
    Object localObject1 = e;
    Object localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>();
    localObject1 = ((JSONObject)localObject1).keys();
    boolean bool1;
    Object localObject3;
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = ((Iterator)localObject1).next();
      ((Set)localObject2).add(localObject3);
    }
    localObject1 = MoPubCustomEventNative.a.a.c;
    boolean bool2 = ((Set)localObject2).containsAll((Collection)localObject1);
    if (bool2)
    {
      localObject1 = e.keys();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject1).hasNext();
        if (!bool3) {
          break;
        }
        localObject2 = (String)((Iterator)localObject1).next();
        localObject3 = MoPubCustomEventNative.a.a.a((String)localObject2);
        if (localObject3 != null) {}
        try
        {
          Object localObject4 = e;
          localObject4 = ((JSONObject)localObject4).opt((String)localObject2);
          try
          {
            localObject5 = MoPubCustomEventNative.1.a;
            int i = ((MoPubCustomEventNative.a.a)localObject3).ordinal();
            int j = localObject5[i];
            switch (j)
            {
            default: 
              localObject4 = new java/lang/StringBuilder;
              break;
            case 11: 
              localObject4 = (String)localObject4;
              setPrivacyInformationIconClickThroughUrl((String)localObject4);
              break;
            case 10: 
              localObject4 = (String)localObject4;
              setPrivacyInformationIconImageUrl((String)localObject4);
              break;
            case 9: 
              localObject4 = Numbers.parseDouble(localObject4);
              setStarRating((Double)localObject4);
              break;
            case 8: 
              localObject4 = (String)localObject4;
              setText((String)localObject4);
              break;
            case 7: 
              localObject4 = (String)localObject4;
              setTitle((String)localObject4);
              break;
            case 6: 
              localObject4 = (String)localObject4;
              setCallToAction((String)localObject4);
              break;
            case 5: 
              bool4 = localObject4 instanceof JSONArray;
              if (bool4)
              {
                b(localObject4);
                continue;
              }
              localObject4 = (String)localObject4;
              addClickTracker((String)localObject4);
              break;
            case 4: 
              localObject4 = (String)localObject4;
              setClickDestinationUrl((String)localObject4);
              break;
            case 3: 
              a(localObject4);
              break;
            case 2: 
              localObject4 = (String)localObject4;
              setIconImageUrl((String)localObject4);
              break;
            case 1: 
              localObject4 = (String)localObject4;
              setMainImageUrl((String)localObject4);
              continue;
              localObject5 = "Unable to add JSON key to internal mapping: ";
              ((StringBuilder)localObject4).<init>((String)localObject5);
              localObject5 = a;
              ((StringBuilder)localObject4).append((String)localObject5);
              localObject4 = ((StringBuilder)localObject4).toString();
              MoPubLog.d((String)localObject4);
            }
          }
          catch (ClassCastException localClassCastException1)
          {
            Object localObject5;
            boolean bool4 = b;
            StringBuilder localStringBuilder;
            if (!bool4)
            {
              localStringBuilder = new java/lang/StringBuilder;
              localObject5 = "Ignoring class cast exception for optional key: ";
              localStringBuilder.<init>((String)localObject5);
              localObject3 = a;
              localStringBuilder.append((String)localObject3);
              localObject3 = localStringBuilder.toString();
              MoPubLog.d((String)localObject3);
              continue;
            }
            throw localStringBuilder;
          }
        }
        catch (ClassCastException localClassCastException2)
        {
          for (;;) {}
        }
        localObject1 = new java/lang/IllegalArgumentException;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("JSONObject key (");
        ((StringBuilder)localObject3).append((String)localObject2);
        ((StringBuilder)localObject3).append(") contained unexpected value.");
        localObject2 = ((StringBuilder)localObject3).toString();
        ((IllegalArgumentException)localObject1).<init>((String)localObject2);
        throw ((Throwable)localObject1);
        localObject3 = e.opt((String)localObject2);
        addExtra((String)localObject2, localObject3);
      }
      localObject1 = getPrivacyInformationIconClickThroughUrl();
      bool2 = TextUtils.isEmpty((CharSequence)localObject1);
      if (bool2)
      {
        localObject1 = "https://www.mopub.com/optout";
        setPrivacyInformationIconClickThroughUrl((String)localObject1);
      }
      localObject1 = c;
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      localObject3 = getMainImageUrl();
      bool1 = TextUtils.isEmpty((CharSequence)localObject3);
      if (!bool1)
      {
        localObject3 = getMainImageUrl();
        ((List)localObject2).add(localObject3);
      }
      localObject3 = getIconImageUrl();
      bool1 = TextUtils.isEmpty((CharSequence)localObject3);
      if (!bool1)
      {
        localObject3 = getIconImageUrl();
        ((List)localObject2).add(localObject3);
      }
      localObject3 = getPrivacyInformationIconImageUrl();
      bool1 = TextUtils.isEmpty((CharSequence)localObject3);
      if (!bool1)
      {
        localObject3 = getPrivacyInformationIconImageUrl();
        ((List)localObject2).add(localObject3);
      }
      localObject3 = d();
      ((List)localObject2).addAll((Collection)localObject3);
      localObject3 = new com/mopub/nativeads/MoPubCustomEventNative$a$1;
      ((MoPubCustomEventNative.a.1)localObject3).<init>(this);
      NativeImageHelper.preCacheImages((Context)localObject1, (List)localObject2, (NativeImageHelper.ImageListener)localObject3);
      return;
    }
    localObject1 = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject1).<init>("JSONObject did not contain required keys.");
    throw ((Throwable)localObject1);
  }
  
  public final void clear(View paramView)
  {
    f.removeView(paramView);
    g.clearOnClickListener(paramView);
  }
  
  public final void destroy()
  {
    f.destroy();
    super.destroy();
  }
  
  public final void handleClick(View paramView)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = b;
      ((BaseNativeAd.NativeEventListener)localObject).onAdClicked();
    }
    localObject = g;
    String str = getClickDestinationUrl();
    ((NativeClickHandler)localObject).openClickDestinationUrl(str, paramView);
  }
  
  public final void prepare(View paramView)
  {
    f.addView(paramView, this);
    g.setOnClickListener(paramView, this);
  }
  
  public final void recordImpression(View paramView)
  {
    a();
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventNative.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */