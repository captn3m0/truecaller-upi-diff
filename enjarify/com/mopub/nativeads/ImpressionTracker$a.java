package com.mopub.nativeads;

import android.view.View;
import com.mopub.common.VisibilityTracker.VisibilityChecker;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class ImpressionTracker$a
  implements Runnable
{
  private final ArrayList b;
  
  ImpressionTracker$a(ImpressionTracker paramImpressionTracker)
  {
    paramImpressionTracker = new java/util/ArrayList;
    paramImpressionTracker.<init>();
    b = paramImpressionTracker;
  }
  
  public final void run()
  {
    Object localObject1 = ImpressionTracker.b(a).entrySet().iterator();
    boolean bool1;
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      localObject3 = (View)((Map.Entry)localObject2).getKey();
      localObject2 = (l)((Map.Entry)localObject2).getValue();
      Object localObject4 = ImpressionTracker.c(a);
      long l = b;
      ImpressionInterface localImpressionInterface = (ImpressionInterface)a;
      int i = localImpressionInterface.getImpressionMinTimeViewed();
      boolean bool2 = ((VisibilityTracker.VisibilityChecker)localObject4).hasRequiredTimeElapsed(l, i);
      if (bool2)
      {
        localObject4 = (ImpressionInterface)a;
        ((ImpressionInterface)localObject4).recordImpression((View)localObject3);
        ((ImpressionInterface)a).setImpressionRecorded();
        localObject2 = b;
        ((ArrayList)localObject2).add(localObject3);
      }
    }
    localObject1 = b.iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (View)((Iterator)localObject1).next();
      localObject3 = a;
      ((ImpressionTracker)localObject3).removeView((View)localObject2);
    }
    b.clear();
    localObject1 = ImpressionTracker.b(a);
    boolean bool3 = ((Map)localObject1).isEmpty();
    if (!bool3)
    {
      localObject1 = a;
      ((ImpressionTracker)localObject1).a();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.ImpressionTracker.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */