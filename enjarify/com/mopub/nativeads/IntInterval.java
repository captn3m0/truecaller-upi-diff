package com.mopub.nativeads;

public class IntInterval
  implements Comparable
{
  private int a;
  private int b;
  
  public IntInterval(int paramInt1, int paramInt2)
  {
    a = paramInt1;
    b = paramInt2;
  }
  
  public int compareTo(IntInterval paramIntInterval)
  {
    int i = a;
    int j = a;
    if (i == j)
    {
      i = b;
      int k = b;
      return i - k;
    }
    return i - j;
  }
  
  public boolean equals(int paramInt1, int paramInt2)
  {
    int i = a;
    if (i == paramInt1)
    {
      paramInt1 = b;
      if (paramInt1 == paramInt2) {
        return true;
      }
    }
    return false;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof IntInterval;
    if (!bool2) {
      return false;
    }
    paramObject = (IntInterval)paramObject;
    int i = a;
    int j = a;
    if (i == j)
    {
      i = b;
      int k = b;
      if (i == k) {
        return bool1;
      }
    }
    return false;
  }
  
  public int getLength()
  {
    return b;
  }
  
  public int getStart()
  {
    return a;
  }
  
  public int hashCode()
  {
    int i = (a + 899) * 31;
    int j = b;
    return i + j;
  }
  
  public void setLength(int paramInt)
  {
    b = paramInt;
  }
  
  public void setStart(int paramInt)
  {
    a = paramInt;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{start : ");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", length : ");
    i = b;
    localStringBuilder.append(i);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.IntInterval
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */