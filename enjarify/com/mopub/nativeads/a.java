package com.mopub.nativeads;

import android.os.Handler;

final class a
  implements PositioningSource
{
  final MoPubNativeAdPositioning.MoPubClientPositioning a;
  private final Handler b;
  
  a(MoPubNativeAdPositioning.MoPubClientPositioning paramMoPubClientPositioning)
  {
    Handler localHandler = new android/os/Handler;
    localHandler.<init>();
    b = localHandler;
    paramMoPubClientPositioning = MoPubNativeAdPositioning.a(paramMoPubClientPositioning);
    a = paramMoPubClientPositioning;
  }
  
  public final void loadPositions(String paramString, PositioningSource.PositioningListener paramPositioningListener)
  {
    paramString = b;
    a.1 local1 = new com/mopub/nativeads/a$1;
    local1.<init>(this, paramPositioningListener);
    paramString.post(local1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */