package com.mopub.nativeads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.mopub.common.logging.MoPubLog;
import java.util.WeakHashMap;

final class e
{
  private static final WeakHashMap a;
  
  static
  {
    WeakHashMap localWeakHashMap = new java/util/WeakHashMap;
    localWeakHashMap.<init>();
    a = localWeakHashMap;
  }
  
  static View a(View paramView, ViewGroup paramViewGroup, Context paramContext, NativeAd paramNativeAd)
  {
    Object localObject1;
    if (paramView != null)
    {
      localObject1 = (NativeAd)a.get(paramView);
      if (localObject1 != null) {
        ((NativeAd)localObject1).clear(paramView);
      }
    }
    if (paramNativeAd != null)
    {
      boolean bool1 = paramNativeAd.isDestroyed();
      if (!bool1)
      {
        if (paramView != null)
        {
          localObject1 = e.a.AD;
          Object localObject2 = paramView.getTag();
          bool1 = ((e.a)localObject1).equals(localObject2);
          if (bool1) {}
        }
        else
        {
          paramView = paramNativeAd.createAdView(paramContext, paramViewGroup);
          paramViewGroup = e.a.AD;
          paramView.setTag(paramViewGroup);
        }
        paramViewGroup = a;
        paramViewGroup.put(paramView, paramNativeAd);
        paramNativeAd.prepare(paramView);
        paramNativeAd.renderAdView(paramView);
        return paramView;
      }
    }
    paramViewGroup = "NativeAd null or invalid. Returning empty view";
    MoPubLog.d(paramViewGroup);
    if (paramView != null)
    {
      paramViewGroup = e.a.EMPTY;
      paramNativeAd = paramView.getTag();
      boolean bool2 = paramViewGroup.equals(paramNativeAd);
      if (bool2) {}
    }
    else
    {
      paramView = new android/view/View;
      paramView.<init>(paramContext);
      paramViewGroup = e.a.EMPTY;
      paramView.setTag(paramViewGroup);
      int i = 8;
      paramView.setVisibility(i);
    }
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */