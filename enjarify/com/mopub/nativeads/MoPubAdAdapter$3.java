package com.mopub.nativeads;

final class MoPubAdAdapter$3
  implements MoPubNativeAdLoadedListener
{
  MoPubAdAdapter$3(MoPubAdAdapter paramMoPubAdAdapter) {}
  
  public final void onAdLoaded(int paramInt)
  {
    MoPubAdAdapter localMoPubAdAdapter = a;
    MoPubNativeAdLoadedListener localMoPubNativeAdLoadedListener = a;
    if (localMoPubNativeAdLoadedListener != null)
    {
      localMoPubNativeAdLoadedListener = a;
      localMoPubNativeAdLoadedListener.onAdLoaded(paramInt);
    }
    localMoPubAdAdapter.notifyDataSetChanged();
  }
  
  public final void onAdRemoved(int paramInt)
  {
    MoPubAdAdapter localMoPubAdAdapter = a;
    MoPubNativeAdLoadedListener localMoPubNativeAdLoadedListener = a;
    if (localMoPubNativeAdLoadedListener != null)
    {
      localMoPubNativeAdLoadedListener = a;
      localMoPubNativeAdLoadedListener.onAdRemoved(paramInt);
    }
    localMoPubAdAdapter.notifyDataSetChanged();
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubAdAdapter.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */