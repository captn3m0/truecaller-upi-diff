package com.mopub.nativeads;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.network.Networking;
import com.mopub.volley.toolbox.ImageLoader;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class NativeImageHelper
{
  public static void loadImageView(String paramString, ImageView paramImageView)
  {
    Object localObject = "Cannot load image into null ImageView";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramImageView, (String)localObject);
    if (!bool) {
      return;
    }
    localObject = "Cannot load image with null url";
    bool = Preconditions.NoThrow.checkNotNull(paramString, (String)localObject);
    if (!bool)
    {
      paramImageView.setImageDrawable(null);
      return;
    }
    localObject = Networking.getImageLoader(paramImageView.getContext());
    NativeImageHelper.2 local2 = new com/mopub/nativeads/NativeImageHelper$2;
    local2.<init>(paramImageView);
    ((ImageLoader)localObject).get(paramString, local2);
  }
  
  public static void preCacheImages(Context paramContext, List paramList, NativeImageHelper.ImageListener paramImageListener)
  {
    paramContext = Networking.getImageLoader(paramContext);
    Object localObject = new java/util/concurrent/atomic/AtomicInteger;
    int i = paramList.size();
    ((AtomicInteger)localObject).<init>(i);
    AtomicBoolean localAtomicBoolean = new java/util/concurrent/atomic/AtomicBoolean;
    localAtomicBoolean.<init>(false);
    NativeImageHelper.1 local1 = new com/mopub/nativeads/NativeImageHelper$1;
    local1.<init>((AtomicInteger)localObject, localAtomicBoolean, paramImageListener);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject = (String)paramList.next();
      boolean bool2 = TextUtils.isEmpty((CharSequence)localObject);
      if (bool2)
      {
        localAtomicBoolean.set(true);
        paramContext = NativeErrorCode.IMAGE_DOWNLOAD_FAILURE;
        paramImageListener.onImagesFailedToCache(paramContext);
        return;
      }
      paramContext.get((String)localObject, local1);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeImageHelper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */