package com.mopub.nativeads;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Drawables;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class NativeRendererHelper
{
  public static void addCtaButton(TextView paramTextView, View paramView, String paramString)
  {
    addTextView(paramTextView, paramString);
    if ((paramTextView != null) && (paramView != null))
    {
      paramString = new com/mopub/nativeads/NativeRendererHelper$2;
      paramString.<init>(paramView);
      paramTextView.setOnClickListener(paramString);
      return;
    }
  }
  
  public static void addPrivacyInformationIcon(ImageView paramImageView, String paramString1, String paramString2)
  {
    if (paramImageView == null) {
      return;
    }
    if (paramString2 == null)
    {
      paramImageView.setImageDrawable(null);
      paramImageView.setOnClickListener(null);
      paramImageView.setVisibility(4);
      return;
    }
    Context localContext = paramImageView.getContext();
    if (localContext == null) {
      return;
    }
    if (paramString1 == null)
    {
      paramString1 = Drawables.NATIVE_PRIVACY_INFORMATION_ICON.createDrawable(localContext);
      paramImageView.setImageDrawable(paramString1);
    }
    else
    {
      NativeImageHelper.loadImageView(paramString1, paramImageView);
    }
    paramString1 = new com/mopub/nativeads/NativeRendererHelper$1;
    paramString1.<init>(localContext, paramString2);
    paramImageView.setOnClickListener(paramString1);
    paramImageView.setVisibility(0);
  }
  
  public static void addTextView(TextView paramTextView, String paramString)
  {
    if (paramTextView == null)
    {
      paramTextView = new java/lang/StringBuilder;
      paramTextView.<init>("Attempted to add text (");
      paramTextView.append(paramString);
      paramTextView.append(") to null TextView.");
      MoPubLog.d(paramTextView.toString());
      return;
    }
    paramTextView.setText(null);
    if (paramString == null)
    {
      MoPubLog.d("Attempted to set TextView contents to null.");
      return;
    }
    paramTextView.setText(paramString);
  }
  
  public static void updateExtras(View paramView, Map paramMap1, Map paramMap2)
  {
    if (paramView == null)
    {
      MoPubLog.w("Attempted to bind extras on a null main view.");
      return;
    }
    Iterator localIterator = paramMap1.keySet().iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (String)localIterator.next();
      int i = ((Integer)paramMap1.get(localObject1)).intValue();
      Object localObject2 = paramView.findViewById(i);
      Object localObject3 = paramMap2.get(localObject1);
      boolean bool2 = localObject2 instanceof ImageView;
      if (bool2)
      {
        localObject2 = (ImageView)localObject2;
        ((ImageView)localObject2).setImageDrawable(null);
        localObject1 = paramMap2.get(localObject1);
        if (localObject1 != null)
        {
          boolean bool3 = localObject1 instanceof String;
          if (bool3)
          {
            localObject1 = (String)localObject1;
            NativeImageHelper.loadImageView((String)localObject1, (ImageView)localObject2);
          }
        }
      }
      else
      {
        bool2 = localObject2 instanceof TextView;
        if (bool2)
        {
          localObject2 = (TextView)localObject2;
          ((TextView)localObject2).setText(null);
          bool1 = localObject3 instanceof String;
          if (bool1)
          {
            localObject3 = (String)localObject3;
            addTextView((TextView)localObject2, (String)localObject3);
          }
        }
        else
        {
          localObject2 = new java/lang/StringBuilder;
          localObject3 = "View bound to ";
          ((StringBuilder)localObject2).<init>((String)localObject3);
          ((StringBuilder)localObject2).append((String)localObject1);
          ((StringBuilder)localObject2).append(" should be an instance of TextView or ImageView.");
          localObject1 = ((StringBuilder)localObject2).toString();
          MoPubLog.d((String)localObject1);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeRendererHelper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */