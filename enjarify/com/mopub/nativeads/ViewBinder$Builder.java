package com.mopub.nativeads;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class ViewBinder$Builder
{
  private final int a;
  private int b;
  private int c;
  private int d;
  private int e;
  private int f;
  private int g;
  private Map h;
  
  public ViewBinder$Builder(int paramInt)
  {
    Map localMap = Collections.emptyMap();
    h = localMap;
    a = paramInt;
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    h = localHashMap;
  }
  
  public final Builder addExtra(String paramString, int paramInt)
  {
    Map localMap = h;
    Integer localInteger = Integer.valueOf(paramInt);
    localMap.put(paramString, localInteger);
    return this;
  }
  
  public final Builder addExtras(Map paramMap)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>(paramMap);
    h = localHashMap;
    return this;
  }
  
  public final ViewBinder build()
  {
    ViewBinder localViewBinder = new com/mopub/nativeads/ViewBinder;
    localViewBinder.<init>(this, (byte)0);
    return localViewBinder;
  }
  
  public final Builder callToActionId(int paramInt)
  {
    d = paramInt;
    return this;
  }
  
  public final Builder iconImageId(int paramInt)
  {
    f = paramInt;
    return this;
  }
  
  public final Builder mainImageId(int paramInt)
  {
    e = paramInt;
    return this;
  }
  
  public final Builder privacyInformationIconImageId(int paramInt)
  {
    g = paramInt;
    return this;
  }
  
  public final Builder textId(int paramInt)
  {
    c = paramInt;
    return this;
  }
  
  public final Builder titleId(int paramInt)
  {
    b = paramInt;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.ViewBinder.Builder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */