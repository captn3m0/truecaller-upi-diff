package com.mopub.nativeads;

import com.mopub.common.logging.MoPubLog;

final class b$1
  implements Runnable
{
  b$1(b paramb) {}
  
  public final void run()
  {
    Object localObject1 = a;
    boolean bool = b;
    if (bool) {
      return;
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Timeout loading native ad content. ");
    Object localObject2 = a.toString();
    ((StringBuilder)localObject1).append((String)localObject2);
    MoPubLog.d(((StringBuilder)localObject1).toString());
    a.a();
    localObject1 = a.a;
    localObject2 = NativeErrorCode.NETWORK_TIMEOUT;
    ((CustomEventNative.CustomEventNativeListener)localObject1).onNativeAdFailed((NativeErrorCode)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */