package com.mopub.nativeads;

import android.content.Context;
import android.os.Handler;
import android.view.TextureView;
import com.google.android.exoplayer2.ExoPlayer;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibilityTracker.VisibilityChecker;
import com.mopub.mobileads.RepeatingHandlerRunnable;
import com.mopub.mobileads.VastTracker;
import com.mopub.mobileads.VastVideoConfig;
import com.mopub.network.TrackingRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class NativeVideoController$NativeVideoProgressRunnable
  extends RepeatingHandlerRunnable
{
  ExoPlayer c;
  TextureView d;
  NativeVideoController.NativeVideoProgressRunnable.ProgressListener e;
  long f;
  long g;
  boolean h;
  private final Context i;
  private final VisibilityTracker.VisibilityChecker j;
  private final List k;
  private final VastVideoConfig l;
  
  private NativeVideoController$NativeVideoProgressRunnable(Context paramContext, Handler paramHandler, List paramList, VisibilityTracker.VisibilityChecker paramVisibilityChecker, VastVideoConfig paramVastVideoConfig)
  {
    super(paramHandler);
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramHandler);
    Preconditions.checkNotNull(paramList);
    Preconditions.checkNotNull(paramVastVideoConfig);
    paramContext = paramContext.getApplicationContext();
    i = paramContext;
    k = paramList;
    j = paramVisibilityChecker;
    l = paramVastVideoConfig;
    g = -1;
    h = false;
  }
  
  NativeVideoController$NativeVideoProgressRunnable(Context paramContext, Handler paramHandler, List paramList, VastVideoConfig paramVastVideoConfig)
  {
    this(paramContext, paramHandler, paramList, localVisibilityChecker, paramVastVideoConfig);
  }
  
  final void a(boolean paramBoolean)
  {
    Iterator localIterator = k.iterator();
    boolean bool1 = false;
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      NativeVideoController.b localb = (NativeVideoController.b)localIterator.next();
      boolean bool3 = e;
      if (bool3)
      {
        bool1 += true;
      }
      else
      {
        Object localObject;
        if (!paramBoolean)
        {
          localObject = j;
          TextureView localTextureView = d;
          int n = b;
          Integer localInteger = f;
          bool3 = ((VisibilityTracker.VisibilityChecker)localObject).isVisible(localTextureView, localTextureView, n, localInteger);
          if (!bool3) {}
        }
        else
        {
          int m = d;
          long l1 = m;
          long l2 = b;
          l1 += l2;
          int i1 = (int)l1;
          d = i1;
          if (!paramBoolean)
          {
            m = d;
            i1 = c;
            if (m < i1) {}
          }
          else
          {
            localObject = a;
            ((NativeVideoController.b.a)localObject).execute();
            m = 1;
            e = m;
            bool1 += true;
          }
        }
      }
    }
    List localList = k;
    paramBoolean = localList.size();
    if (bool1 == paramBoolean)
    {
      paramBoolean = h;
      if (paramBoolean) {
        stop();
      }
    }
  }
  
  public final void doWork()
  {
    Object localObject = c;
    if (localObject != null)
    {
      boolean bool1 = ((ExoPlayer)localObject).getPlayWhenReady();
      if (bool1)
      {
        long l1 = c.getCurrentPosition();
        f = l1;
        l1 = c.getDuration();
        g = l1;
        bool1 = false;
        a(false);
        localObject = e;
        if (localObject != null)
        {
          l2 = f;
          float f1 = (float)l2;
          long l3 = g;
          float f2 = (float)l3;
          f1 /= f2;
          m = 1148846080;
          f2 = 1000.0F;
          f1 *= f2;
          n = (int)f1;
          ((NativeVideoController.NativeVideoProgressRunnable.ProgressListener)localObject).updateProgress(n);
        }
        localObject = l;
        long l2 = f;
        int m = (int)l2;
        long l4 = g;
        int n = (int)l4;
        localObject = ((VastVideoConfig)localObject).getUntriggeredTrackersBefore(m, n);
        boolean bool3 = ((List)localObject).isEmpty();
        if (!bool3)
        {
          ArrayList localArrayList = new java/util/ArrayList;
          localArrayList.<init>();
          localObject = ((List)localObject).iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject).hasNext();
            if (!bool2) {
              break;
            }
            VastTracker localVastTracker = (VastTracker)((Iterator)localObject).next();
            boolean bool4 = localVastTracker.isTracked();
            if (!bool4)
            {
              String str = localVastTracker.getContent();
              localArrayList.add(str);
              localVastTracker.setTracked();
            }
          }
          localObject = i;
          TrackingRequest.makeTrackingHttpRequest(localArrayList, (Context)localObject);
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeVideoController.NativeVideoProgressRunnable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */