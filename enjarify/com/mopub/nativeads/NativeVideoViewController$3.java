package com.mopub.nativeads;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;

final class NativeVideoViewController$3
  implements View.OnClickListener
{
  NativeVideoViewController$3(NativeVideoViewController paramNativeVideoViewController) {}
  
  public final void onClick(View paramView)
  {
    NativeVideoViewController.d(a).setPlayWhenReady(false);
    paramView = a;
    Object localObject = NativeVideoViewController.c(paramView).getTextureView().getBitmap();
    NativeVideoViewController.a(paramView, (Bitmap)localObject);
    paramView = NativeVideoViewController.d(a);
    localObject = (Activity)NativeVideoViewController.f(a);
    paramView.handleCtaClick((Context)localObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeVideoViewController.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */