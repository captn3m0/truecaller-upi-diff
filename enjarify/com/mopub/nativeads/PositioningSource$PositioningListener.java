package com.mopub.nativeads;

public abstract interface PositioningSource$PositioningListener
{
  public abstract void onFailed();
  
  public abstract void onLoad(MoPubNativeAdPositioning.MoPubClientPositioning paramMoPubClientPositioning);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.PositioningSource.PositioningListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */