package com.mopub.nativeads;

import com.mopub.common.Preconditions;
import java.util.ArrayList;
import java.util.Iterator;

public class AdRendererRegistry
{
  private final ArrayList a;
  
  public AdRendererRegistry()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
  }
  
  public int getAdRendererCount()
  {
    return a.size();
  }
  
  public MoPubAdRenderer getRendererForAd(BaseNativeAd paramBaseNativeAd)
  {
    Preconditions.checkNotNull(paramBaseNativeAd);
    Iterator localIterator = a.iterator();
    MoPubAdRenderer localMoPubAdRenderer;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localMoPubAdRenderer = (MoPubAdRenderer)localIterator.next();
      bool2 = localMoPubAdRenderer.supports(paramBaseNativeAd);
    } while (!bool2);
    return localMoPubAdRenderer;
    return null;
  }
  
  public MoPubAdRenderer getRendererForViewType(int paramInt)
  {
    try
    {
      ArrayList localArrayList = a;
      paramInt += -1;
      Object localObject = localArrayList.get(paramInt);
      return (MoPubAdRenderer)localObject;
    }
    catch (IndexOutOfBoundsException localIndexOutOfBoundsException) {}
    return null;
  }
  
  public Iterable getRendererIterable()
  {
    return a;
  }
  
  public int getViewTypeForAd(NativeAd paramNativeAd)
  {
    Preconditions.checkNotNull(paramNativeAd);
    int i = 0;
    for (;;)
    {
      Object localObject1 = a;
      int j = ((ArrayList)localObject1).size();
      if (i >= j) {
        break;
      }
      localObject1 = paramNativeAd.getMoPubAdRenderer();
      Object localObject2 = a.get(i);
      if (localObject1 == localObject2) {
        return i + 1;
      }
      i += 1;
    }
    return 0;
  }
  
  public void registerAdRenderer(MoPubAdRenderer paramMoPubAdRenderer)
  {
    a.add(paramMoPubAdRenderer);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.AdRendererRegistry
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */