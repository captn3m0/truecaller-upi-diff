package com.mopub.nativeads;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import com.mopub.common.VisibilityTracker;
import com.mopub.common.VisibilityTracker.VisibilityChecker;
import com.mopub.common.VisibilityTracker.VisibilityTrackerListener;
import java.util.Map;
import java.util.WeakHashMap;

public class ImpressionTracker
{
  private final VisibilityTracker a;
  private final Map b;
  private final Map c;
  private final Handler d;
  private final ImpressionTracker.a e;
  private final VisibilityTracker.VisibilityChecker f;
  private VisibilityTracker.VisibilityTrackerListener g;
  
  public ImpressionTracker(Context paramContext)
  {
    this(localWeakHashMap1, localWeakHashMap2, localVisibilityChecker, localVisibilityTracker, localHandler);
  }
  
  private ImpressionTracker(Map paramMap1, Map paramMap2, VisibilityTracker.VisibilityChecker paramVisibilityChecker, VisibilityTracker paramVisibilityTracker, Handler paramHandler)
  {
    b = paramMap1;
    c = paramMap2;
    f = paramVisibilityChecker;
    a = paramVisibilityTracker;
    paramMap1 = new com/mopub/nativeads/ImpressionTracker$1;
    paramMap1.<init>(this);
    g = paramMap1;
    paramMap1 = a;
    paramMap2 = g;
    paramMap1.setVisibilityTrackerListener(paramMap2);
    d = paramHandler;
    paramMap1 = new com/mopub/nativeads/ImpressionTracker$a;
    paramMap1.<init>(this);
    e = paramMap1;
  }
  
  final void a()
  {
    Handler localHandler = d;
    ImpressionTracker.a locala = null;
    boolean bool = localHandler.hasMessages(0);
    if (bool) {
      return;
    }
    localHandler = d;
    locala = e;
    localHandler.postDelayed(locala, 250L);
  }
  
  public void addView(View paramView, ImpressionInterface paramImpressionInterface)
  {
    Object localObject = b.get(paramView);
    if (localObject == paramImpressionInterface) {
      return;
    }
    removeView(paramView);
    boolean bool = paramImpressionInterface.isImpressionRecorded();
    if (bool) {
      return;
    }
    b.put(paramView, paramImpressionInterface);
    localObject = a;
    int i = paramImpressionInterface.getImpressionMinPercentageViewed();
    paramImpressionInterface = paramImpressionInterface.getImpressionMinVisiblePx();
    ((VisibilityTracker)localObject).addView(paramView, i, paramImpressionInterface);
  }
  
  public void clear()
  {
    b.clear();
    c.clear();
    a.clear();
    d.removeMessages(0);
  }
  
  public void destroy()
  {
    clear();
    a.destroy();
    g = null;
  }
  
  public void removeView(View paramView)
  {
    b.remove(paramView);
    c.remove(paramView);
    a.removeView(paramView);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.ImpressionTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */