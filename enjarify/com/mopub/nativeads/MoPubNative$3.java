package com.mopub.nativeads;

import android.content.Context;
import com.mopub.common.logging.MoPubLog;
import com.mopub.network.AdLoader;
import com.mopub.network.AdResponse;
import java.util.List;

final class MoPubNative$3
  implements CustomEventNative.CustomEventNativeListener
{
  MoPubNative$3(MoPubNative paramMoPubNative, AdResponse paramAdResponse) {}
  
  public final void onNativeAdFailed(NativeErrorCode paramNativeErrorCode)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramNativeErrorCode;
    MoPubLog.v(String.format("Native Ad failed to load with error: %s.", arrayOfObject));
    MoPubNative.b(b);
    b.a("", paramNativeErrorCode);
  }
  
  public final void onNativeAdLoaded(BaseNativeAd paramBaseNativeAd)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("MoPubNative.onNativeAdLoaded ");
    Object localObject2 = MoPubNative.a(b);
    ((StringBuilder)localObject1).append(localObject2);
    MoPubLog.w(((StringBuilder)localObject1).toString());
    MoPubNative.b(b);
    localObject1 = b;
    Context localContext = ((MoPubNative)localObject1).a();
    if (localContext == null) {
      return;
    }
    localObject1 = b.d;
    MoPubAdRenderer localMoPubAdRenderer = ((AdRendererRegistry)localObject1).getRendererForAd(paramBaseNativeAd);
    if (localMoPubAdRenderer == null)
    {
      paramBaseNativeAd = NativeErrorCode.NATIVE_RENDERER_CONFIGURATION_ERROR;
      onNativeAdFailed(paramBaseNativeAd);
      return;
    }
    localObject1 = MoPubNative.c(b);
    if (localObject1 != null)
    {
      localObject1 = MoPubNative.c(b);
      ((AdLoader)localObject1).creativeDownloadSuccess();
    }
    localObject1 = MoPubNative.e(b);
    NativeAd localNativeAd = new com/mopub/nativeads/NativeAd;
    List localList = a.getImpressionTrackingUrls();
    String str1 = a.getClickTrackingUrl();
    String str2 = MoPubNative.d(b);
    localObject2 = localNativeAd;
    localNativeAd.<init>(localContext, localList, str1, str2, paramBaseNativeAd, localMoPubAdRenderer);
    ((MoPubNative.MoPubNativeNetworkListener)localObject1).onNativeLoad(localNativeAd);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubNative.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */