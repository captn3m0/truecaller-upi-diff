package com.mopub.nativeads;

import android.view.View;

public abstract interface NativeAd$MoPubNativeEventListener
{
  public abstract void onClick(View paramView);
  
  public abstract void onImpression(View paramView);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeAd.MoPubNativeEventListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */