package com.mopub.nativeads;

public enum MoPubCustomEventVideoNative$MoPubVideoNativeAd$VideoState
{
  static
  {
    Object localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$VideoState;
    ((VideoState)localObject).<init>("CREATED", 0);
    CREATED = (VideoState)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$VideoState;
    int i = 1;
    ((VideoState)localObject).<init>("LOADING", i);
    LOADING = (VideoState)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$VideoState;
    int j = 2;
    ((VideoState)localObject).<init>("BUFFERING", j);
    BUFFERING = (VideoState)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$VideoState;
    int k = 3;
    ((VideoState)localObject).<init>("PAUSED", k);
    PAUSED = (VideoState)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$VideoState;
    int m = 4;
    ((VideoState)localObject).<init>("PLAYING", m);
    PLAYING = (VideoState)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$VideoState;
    int n = 5;
    ((VideoState)localObject).<init>("PLAYING_MUTED", n);
    PLAYING_MUTED = (VideoState)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$VideoState;
    int i1 = 6;
    ((VideoState)localObject).<init>("ENDED", i1);
    ENDED = (VideoState)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$VideoState;
    int i2 = 7;
    ((VideoState)localObject).<init>("FAILED_LOAD", i2);
    FAILED_LOAD = (VideoState)localObject;
    localObject = new VideoState[8];
    VideoState localVideoState = CREATED;
    localObject[0] = localVideoState;
    localVideoState = LOADING;
    localObject[i] = localVideoState;
    localVideoState = BUFFERING;
    localObject[j] = localVideoState;
    localVideoState = PAUSED;
    localObject[k] = localVideoState;
    localVideoState = PLAYING;
    localObject[m] = localVideoState;
    localVideoState = PLAYING_MUTED;
    localObject[n] = localVideoState;
    localVideoState = ENDED;
    localObject[i1] = localVideoState;
    localVideoState = FAILED_LOAD;
    localObject[i2] = localVideoState;
    a = (VideoState[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */