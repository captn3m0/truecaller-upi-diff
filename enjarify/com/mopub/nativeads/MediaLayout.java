package com.mopub.nativeads;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.util.AttributeSet;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Drawables;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.VastVideoProgressBarWidget;
import com.mopub.mobileads.resource.DrawableConstants.GradientStrip;

public class MediaLayout
  extends RelativeLayout
{
  private volatile MediaLayout.Mode a;
  private MediaLayout.MuteState b;
  private ImageView c;
  private TextureView d;
  private ProgressBar e;
  private ImageView f;
  private ImageView g;
  private ImageView h;
  private VastVideoProgressBarWidget i;
  private ImageView j;
  private View k;
  private Drawable l;
  private Drawable m;
  private boolean n;
  private final int o;
  private final int p;
  private final int q;
  private final int r;
  
  public MediaLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public MediaLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public MediaLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramAttributeSet = MediaLayout.Mode.IMAGE;
    a = paramAttributeSet;
    Preconditions.checkNotNull(paramContext);
    paramAttributeSet = MediaLayout.MuteState.MUTED;
    b = paramAttributeSet;
    paramAttributeSet = new android/widget/RelativeLayout$LayoutParams;
    paramInt = -1;
    paramAttributeSet.<init>(paramInt, paramInt);
    paramAttributeSet.addRule(13);
    Object localObject = new android/widget/ImageView;
    ((ImageView)localObject).<init>(paramContext);
    c = ((ImageView)localObject);
    c.setLayoutParams(paramAttributeSet);
    paramAttributeSet = c;
    localObject = ImageView.ScaleType.CENTER_CROP;
    paramAttributeSet.setScaleType((ImageView.ScaleType)localObject);
    paramAttributeSet = c;
    addView(paramAttributeSet);
    int i1 = Dips.asIntPixels(40.0F, paramContext);
    o = i1;
    i1 = Dips.asIntPixels(35.0F, paramContext);
    p = i1;
    i1 = Dips.asIntPixels(36.0F, paramContext);
    q = i1;
    int i2 = Dips.asIntPixels(10.0F, paramContext);
    r = i2;
  }
  
  private void a()
  {
    int[] arrayOfInt = MediaLayout.2.b;
    int i1 = a.ordinal();
    int i2 = arrayOfInt[i1];
    i1 = 0;
    int i3 = 4;
    switch (i2)
    {
    default: 
      break;
    case 6: 
      setMainImageVisibility(0);
      setLoadingSpinnerVisibility(i3);
      setVideoControlVisibility(i3);
      setPlayButtonVisibility(0);
      break;
    case 5: 
      setMainImageVisibility(i3);
      setLoadingSpinnerVisibility(i3);
      setVideoControlVisibility(0);
      setPlayButtonVisibility(0);
      return;
    case 3: 
      setMainImageVisibility(i3);
      setLoadingSpinnerVisibility(0);
      setVideoControlVisibility(0);
      setPlayButtonVisibility(i3);
    case 4: 
      setMainImageVisibility(i3);
      setLoadingSpinnerVisibility(i3);
      setVideoControlVisibility(0);
      setPlayButtonVisibility(i3);
      return;
    case 2: 
      setMainImageVisibility(0);
      setLoadingSpinnerVisibility(0);
      setVideoControlVisibility(i3);
      setPlayButtonVisibility(i3);
      return;
    case 1: 
      setMainImageVisibility(0);
      setLoadingSpinnerVisibility(i3);
      setVideoControlVisibility(i3);
      setPlayButtonVisibility(i3);
      return;
    }
  }
  
  private void setLoadingSpinnerVisibility(int paramInt)
  {
    Object localObject = e;
    if (localObject != null) {
      ((ProgressBar)localObject).setVisibility(paramInt);
    }
    localObject = h;
    if (localObject != null) {
      ((ImageView)localObject).setVisibility(paramInt);
    }
  }
  
  private void setMainImageVisibility(int paramInt)
  {
    c.setVisibility(paramInt);
  }
  
  private void setPlayButtonVisibility(int paramInt)
  {
    Object localObject = f;
    if (localObject != null)
    {
      View localView = k;
      if (localView != null)
      {
        ((ImageView)localObject).setVisibility(paramInt);
        localObject = k;
        ((View)localObject).setVisibility(paramInt);
      }
    }
  }
  
  private void setVideoControlVisibility(int paramInt)
  {
    Object localObject = g;
    if (localObject != null) {
      ((ImageView)localObject).setVisibility(paramInt);
    }
    localObject = i;
    if (localObject != null) {
      ((VastVideoProgressBarWidget)localObject).setVisibility(paramInt);
    }
    localObject = j;
    if (localObject != null) {
      ((ImageView)localObject).setVisibility(paramInt);
    }
  }
  
  public ImageView getMainImageView()
  {
    return c;
  }
  
  public TextureView getTextureView()
  {
    return d;
  }
  
  public void initForVideo()
  {
    boolean bool = n;
    if (bool) {
      return;
    }
    Object localObject1 = new android/widget/RelativeLayout$LayoutParams;
    int i1 = -1;
    ((RelativeLayout.LayoutParams)localObject1).<init>(i1, i1);
    int i2 = 13;
    ((RelativeLayout.LayoutParams)localObject1).addRule(i2);
    Object localObject2 = new android/view/TextureView;
    Context localContext1 = getContext();
    ((TextureView)localObject2).<init>(localContext1);
    d = ((TextureView)localObject2);
    d.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    localObject1 = d;
    int i3 = (int)Utils.generateUniqueId();
    ((TextureView)localObject1).setId(i3);
    localObject1 = d;
    addView((View)localObject1);
    c.bringToFront();
    localObject1 = new android/widget/RelativeLayout$LayoutParams;
    int i4 = o;
    ((RelativeLayout.LayoutParams)localObject1).<init>(i4, i4);
    ((RelativeLayout.LayoutParams)localObject1).addRule(10);
    ((RelativeLayout.LayoutParams)localObject1).addRule(11);
    localObject2 = new android/widget/ProgressBar;
    localContext1 = getContext();
    ((ProgressBar)localObject2).<init>(localContext1);
    e = ((ProgressBar)localObject2);
    e.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    localObject1 = e;
    i4 = r;
    ((ProgressBar)localObject1).setPadding(0, i4, i4, 0);
    localObject1 = e;
    i4 = 1;
    ((ProgressBar)localObject1).setIndeterminate(i4);
    localObject1 = e;
    addView((View)localObject1);
    localObject1 = new android/widget/RelativeLayout$LayoutParams;
    int i5 = p;
    ((RelativeLayout.LayoutParams)localObject1).<init>(i1, i5);
    int i6 = d.getId();
    ((RelativeLayout.LayoutParams)localObject1).addRule(8, i6);
    Object localObject3 = new android/widget/ImageView;
    Context localContext2 = getContext();
    ((ImageView)localObject3).<init>(localContext2);
    g = ((ImageView)localObject3);
    g.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    localObject1 = new android/graphics/drawable/GradientDrawable;
    localObject3 = GradientDrawable.Orientation.BOTTOM_TOP;
    i6 = 2;
    Object localObject4 = new int[i6];
    int i7 = DrawableConstants.GradientStrip.START_COLOR;
    localObject4[0] = i7;
    i7 = DrawableConstants.GradientStrip.END_COLOR;
    localObject4[i4] = i7;
    ((GradientDrawable)localObject1).<init>((GradientDrawable.Orientation)localObject3, (int[])localObject4);
    g.setImageDrawable((Drawable)localObject1);
    localObject1 = g;
    addView((View)localObject1);
    localObject1 = new android/widget/RelativeLayout$LayoutParams;
    i5 = p;
    ((RelativeLayout.LayoutParams)localObject1).<init>(i1, i5);
    int i8 = d.getId();
    ((RelativeLayout.LayoutParams)localObject1).addRule(6, i8);
    localObject3 = new android/widget/ImageView;
    localObject4 = getContext();
    ((ImageView)localObject3).<init>((Context)localObject4);
    h = ((ImageView)localObject3);
    h.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    localObject1 = new android/graphics/drawable/GradientDrawable;
    localObject3 = GradientDrawable.Orientation.TOP_BOTTOM;
    localObject4 = new int[i6];
    i7 = DrawableConstants.GradientStrip.START_COLOR;
    localObject4[0] = i7;
    i7 = DrawableConstants.GradientStrip.END_COLOR;
    localObject4[i4] = i7;
    ((GradientDrawable)localObject1).<init>((GradientDrawable.Orientation)localObject3, (int[])localObject4);
    h.setImageDrawable((Drawable)localObject1);
    localObject1 = h;
    addView((View)localObject1);
    localObject1 = new com/mopub/mobileads/VastVideoProgressBarWidget;
    localObject3 = getContext();
    ((VastVideoProgressBarWidget)localObject1).<init>((Context)localObject3);
    i = ((VastVideoProgressBarWidget)localObject1);
    localObject1 = i;
    i5 = d.getId();
    ((VastVideoProgressBarWidget)localObject1).setAnchorId(i5);
    i.calibrateAndMakeVisible(1000, 0);
    localObject1 = i;
    addView((View)localObject1);
    localObject1 = Drawables.NATIVE_MUTED;
    localObject3 = getContext();
    localObject1 = ((Drawables)localObject1).createDrawable((Context)localObject3);
    l = ((Drawable)localObject1);
    localObject1 = Drawables.NATIVE_UNMUTED;
    localObject3 = getContext();
    localObject1 = ((Drawables)localObject1).createDrawable((Context)localObject3);
    m = ((Drawable)localObject1);
    localObject1 = new android/widget/RelativeLayout$LayoutParams;
    i5 = q;
    ((RelativeLayout.LayoutParams)localObject1).<init>(i5, i5);
    ((RelativeLayout.LayoutParams)localObject1).addRule(9);
    i5 = i.getId();
    ((RelativeLayout.LayoutParams)localObject1).addRule(i6, i5);
    localObject3 = new android/widget/ImageView;
    localContext2 = getContext();
    ((ImageView)localObject3).<init>(localContext2);
    j = ((ImageView)localObject3);
    j.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    localObject1 = j;
    localObject3 = ImageView.ScaleType.CENTER_INSIDE;
    ((ImageView)localObject1).setScaleType((ImageView.ScaleType)localObject3);
    localObject1 = j;
    i5 = r;
    ((ImageView)localObject1).setPadding(i5, i5, i5, i5);
    localObject1 = j;
    localObject3 = l;
    ((ImageView)localObject1).setImageDrawable((Drawable)localObject3);
    localObject1 = j;
    addView((View)localObject1);
    localObject1 = new android/widget/RelativeLayout$LayoutParams;
    ((RelativeLayout.LayoutParams)localObject1).<init>(i1, i1);
    ((RelativeLayout.LayoutParams)localObject1).addRule(i2);
    Object localObject5 = new android/view/View;
    localObject3 = getContext();
    ((View)localObject5).<init>((Context)localObject3);
    k = ((View)localObject5);
    k.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    k.setBackgroundColor(0);
    localObject1 = k;
    addView((View)localObject1);
    localObject1 = new android/widget/RelativeLayout$LayoutParams;
    i1 = o;
    ((RelativeLayout.LayoutParams)localObject1).<init>(i1, i1);
    ((RelativeLayout.LayoutParams)localObject1).addRule(i2);
    localObject5 = new android/widget/ImageView;
    Context localContext3 = getContext();
    ((ImageView)localObject5).<init>(localContext3);
    f = ((ImageView)localObject5);
    f.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    localObject1 = f;
    localObject5 = Drawables.NATIVE_PLAY;
    localContext3 = getContext();
    localObject5 = ((Drawables)localObject5).createDrawable(localContext3);
    ((ImageView)localObject1).setImageDrawable((Drawable)localObject5);
    localObject1 = f;
    addView((View)localObject1);
    n = i4;
    a();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i1 = View.MeasureSpec.getMode(paramInt1);
    int i2 = View.MeasureSpec.getMode(paramInt2);
    int i3 = View.MeasureSpec.getSize(paramInt1);
    int i4 = View.MeasureSpec.getSize(paramInt2);
    int i5 = getMeasuredWidth();
    int i6 = getMeasuredHeight();
    int i7 = 1073741824;
    if (i1 != i7)
    {
      int i8 = -1 << -1;
      f1 = -0.0F;
      if (i1 == i8) {
        i3 = Math.min(i3, i5);
      } else {
        i3 = i5;
      }
    }
    float f2 = 0.5625F;
    float f1 = i3 * f2;
    i1 = (int)f1;
    if ((i2 == i7) && (i4 < i1))
    {
      f2 = 1.7777778F;
      float f3 = i4 * f2;
      i3 = (int)f3;
      i1 = i4;
    }
    i2 = Math.abs(i1 - i6);
    i4 = 2;
    if (i2 < i4)
    {
      i2 = Math.abs(i3 - i5);
      if (i2 < i4) {}
    }
    else
    {
      Object[] arrayOfObject = new Object[i4];
      Integer localInteger = Integer.valueOf(i3);
      arrayOfObject[0] = localInteger;
      i5 = 1;
      localInteger = Integer.valueOf(i1);
      arrayOfObject[i5] = localInteger;
      MoPubLog.v(String.format("Resetting mediaLayout size to w: %d h: %d", arrayOfObject));
      getLayoutParamswidth = i3;
      ViewGroup.LayoutParams localLayoutParams = getLayoutParams();
      height = i1;
    }
    super.onMeasure(paramInt1, paramInt2);
  }
  
  public void reset()
  {
    MediaLayout.Mode localMode = MediaLayout.Mode.IMAGE;
    setMode(localMode);
    setPlayButtonClickListener(null);
    setMuteControlClickListener(null);
    setVideoClickListener(null);
  }
  
  public void resetProgress()
  {
    VastVideoProgressBarWidget localVastVideoProgressBarWidget = i;
    if (localVastVideoProgressBarWidget != null) {
      localVastVideoProgressBarWidget.reset();
    }
  }
  
  public void setMainImageDrawable(Drawable paramDrawable)
  {
    Preconditions.checkNotNull(paramDrawable);
    c.setImageDrawable(paramDrawable);
  }
  
  public void setMode(MediaLayout.Mode paramMode)
  {
    Preconditions.checkNotNull(paramMode);
    a = paramMode;
    paramMode = new com/mopub/nativeads/MediaLayout$1;
    paramMode.<init>(this);
    post(paramMode);
  }
  
  public void setMuteControlClickListener(View.OnClickListener paramOnClickListener)
  {
    ImageView localImageView = j;
    if (localImageView != null) {
      localImageView.setOnClickListener(paramOnClickListener);
    }
  }
  
  public void setMuteState(MediaLayout.MuteState paramMuteState)
  {
    Preconditions.checkNotNull(paramMuteState);
    Object localObject = b;
    if (paramMuteState == localObject) {
      return;
    }
    b = paramMuteState;
    paramMuteState = j;
    if (paramMuteState != null)
    {
      paramMuteState = MediaLayout.2.a;
      localObject = b;
      int i1 = ((MediaLayout.MuteState)localObject).ordinal();
      int i2 = paramMuteState[i1];
      i1 = 1;
      if (i2 != i1)
      {
        paramMuteState = j;
        localObject = m;
        paramMuteState.setImageDrawable((Drawable)localObject);
      }
      else
      {
        paramMuteState = j;
        localObject = l;
        paramMuteState.setImageDrawable((Drawable)localObject);
        return;
      }
    }
  }
  
  public void setPlayButtonClickListener(View.OnClickListener paramOnClickListener)
  {
    Object localObject = f;
    if (localObject != null)
    {
      localObject = k;
      if (localObject != null)
      {
        ((View)localObject).setOnClickListener(paramOnClickListener);
        localObject = f;
        ((ImageView)localObject).setOnClickListener(paramOnClickListener);
      }
    }
  }
  
  public void setSurfaceTextureListener(TextureView.SurfaceTextureListener paramSurfaceTextureListener)
  {
    Object localObject = d;
    if (localObject != null)
    {
      ((TextureView)localObject).setSurfaceTextureListener(paramSurfaceTextureListener);
      localObject = d.getSurfaceTexture();
      if ((localObject != null) && (paramSurfaceTextureListener != null))
      {
        TextureView localTextureView1 = d;
        int i1 = localTextureView1.getWidth();
        TextureView localTextureView2 = d;
        int i2 = localTextureView2.getHeight();
        paramSurfaceTextureListener.onSurfaceTextureAvailable((SurfaceTexture)localObject, i1, i2);
      }
    }
  }
  
  public void setVideoClickListener(View.OnClickListener paramOnClickListener)
  {
    TextureView localTextureView = d;
    if (localTextureView != null) {
      localTextureView.setOnClickListener(paramOnClickListener);
    }
  }
  
  public void updateProgress(int paramInt)
  {
    VastVideoProgressBarWidget localVastVideoProgressBarWidget = i;
    if (localVastVideoProgressBarWidget != null) {
      localVastVideoProgressBarWidget.updateProgress(paramInt);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MediaLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */