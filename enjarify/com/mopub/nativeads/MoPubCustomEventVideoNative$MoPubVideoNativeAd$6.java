package com.mopub.nativeads;

import android.view.View;
import android.view.View.OnClickListener;
import com.mopub.mobileads.MraidVideoPlayerActivity;
import com.mopub.mobileads.VastVideoConfig;

final class MoPubCustomEventVideoNative$MoPubVideoNativeAd$6
  implements View.OnClickListener
{
  MoPubCustomEventVideoNative$MoPubVideoNativeAd$6(MoPubCustomEventVideoNative.MoPubVideoNativeAd paramMoPubVideoNativeAd) {}
  
  public final void onClick(View paramView)
  {
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.k(a);
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a).a();
    paramView = MoPubCustomEventVideoNative.MoPubVideoNativeAd.c(a);
    long l = MoPubCustomEventVideoNative.MoPubVideoNativeAd.l(a);
    VastVideoConfig localVastVideoConfig = a.e;
    MraidVideoPlayerActivity.startNativeVideo(paramView, l, localVastVideoConfig);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.MoPubVideoNativeAd.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */