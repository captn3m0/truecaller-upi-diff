package com.mopub.nativeads;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.VisibilityTracker;
import com.mopub.common.logging.MoPubLog;
import java.util.WeakHashMap;

public final class MoPubRecyclerAdapter
  extends RecyclerView.Adapter
{
  MoPubNativeAdLoadedListener a;
  private final RecyclerView.AdapterDataObserver b;
  private RecyclerView c;
  private final MoPubStreamAdPlacer d;
  private final RecyclerView.Adapter e;
  private final VisibilityTracker f;
  private final WeakHashMap g;
  private MoPubRecyclerAdapter.ContentChangeStrategy h;
  
  public MoPubRecyclerAdapter(Activity paramActivity, RecyclerView.Adapter paramAdapter)
  {
    this(paramActivity, paramAdapter, localMoPubServerPositioning);
  }
  
  public MoPubRecyclerAdapter(Activity paramActivity, RecyclerView.Adapter paramAdapter, MoPubNativeAdPositioning.MoPubClientPositioning paramMoPubClientPositioning)
  {
    this(localMoPubStreamAdPlacer, paramAdapter, paramMoPubClientPositioning);
  }
  
  public MoPubRecyclerAdapter(Activity paramActivity, RecyclerView.Adapter paramAdapter, MoPubNativeAdPositioning.MoPubServerPositioning paramMoPubServerPositioning)
  {
    this(localMoPubStreamAdPlacer, paramAdapter, paramMoPubServerPositioning);
  }
  
  private MoPubRecyclerAdapter(MoPubStreamAdPlacer paramMoPubStreamAdPlacer, RecyclerView.Adapter paramAdapter, VisibilityTracker paramVisibilityTracker)
  {
    Object localObject = MoPubRecyclerAdapter.ContentChangeStrategy.INSERT_AT_END;
    h = ((MoPubRecyclerAdapter.ContentChangeStrategy)localObject);
    localObject = new java/util/WeakHashMap;
    ((WeakHashMap)localObject).<init>();
    g = ((WeakHashMap)localObject);
    e = paramAdapter;
    f = paramVisibilityTracker;
    paramAdapter = f;
    paramVisibilityTracker = new com/mopub/nativeads/MoPubRecyclerAdapter$1;
    paramVisibilityTracker.<init>(this);
    paramAdapter.setVisibilityTrackerListener(paramVisibilityTracker);
    boolean bool = e.hasStableIds();
    super.setHasStableIds(bool);
    d = paramMoPubStreamAdPlacer;
    paramMoPubStreamAdPlacer = d;
    paramAdapter = new com/mopub/nativeads/MoPubRecyclerAdapter$2;
    paramAdapter.<init>(this);
    paramMoPubStreamAdPlacer.setAdLoadedListener(paramAdapter);
    paramMoPubStreamAdPlacer = d;
    int i = e.getItemCount();
    paramMoPubStreamAdPlacer.setItemCount(i);
    paramMoPubStreamAdPlacer = new com/mopub/nativeads/MoPubRecyclerAdapter$3;
    paramMoPubStreamAdPlacer.<init>(this);
    b = paramMoPubStreamAdPlacer;
    paramMoPubStreamAdPlacer = e;
    paramAdapter = b;
    paramMoPubStreamAdPlacer.registerAdapterDataObserver(paramAdapter);
  }
  
  public static int computeScrollOffset(LinearLayoutManager paramLinearLayoutManager, RecyclerView.ViewHolder paramViewHolder)
  {
    int i = 0;
    if (paramViewHolder == null) {
      return 0;
    }
    paramViewHolder = itemView;
    boolean bool1 = paramLinearLayoutManager.canScrollVertically();
    boolean bool2;
    if (bool1)
    {
      bool2 = paramLinearLayoutManager.getStackFromEnd();
      if (bool2) {
        i = paramViewHolder.getBottom();
      } else {
        i = paramViewHolder.getTop();
      }
    }
    else
    {
      bool1 = paramLinearLayoutManager.canScrollHorizontally();
      if (bool1)
      {
        bool2 = paramLinearLayoutManager.getStackFromEnd();
        if (bool2) {
          i = paramViewHolder.getRight();
        } else {
          i = paramViewHolder.getLeft();
        }
      }
    }
    return i;
  }
  
  public final void clearAds()
  {
    d.clearAds();
  }
  
  public final void destroy()
  {
    RecyclerView.Adapter localAdapter = e;
    RecyclerView.AdapterDataObserver localAdapterDataObserver = b;
    localAdapter.unregisterAdapterDataObserver(localAdapterDataObserver);
    d.destroy();
    f.destroy();
  }
  
  public final int getAdjustedPosition(int paramInt)
  {
    return d.getAdjustedPosition(paramInt);
  }
  
  public final int getItemCount()
  {
    MoPubStreamAdPlacer localMoPubStreamAdPlacer = d;
    int i = e.getItemCount();
    return localMoPubStreamAdPlacer.getAdjustedCount(i);
  }
  
  public final long getItemId(int paramInt)
  {
    Object localObject = e;
    boolean bool = ((RecyclerView.Adapter)localObject).hasStableIds();
    if (!bool) {
      return -1;
    }
    localObject = d.getAdData(paramInt);
    if (localObject != null) {
      return -System.identityHashCode(localObject);
    }
    localObject = e;
    paramInt = d.getOriginalPosition(paramInt);
    return ((RecyclerView.Adapter)localObject).getItemId(paramInt);
  }
  
  public final int getItemViewType(int paramInt)
  {
    Object localObject = d;
    int i = ((MoPubStreamAdPlacer)localObject).getAdViewType(paramInt);
    if (i != 0) {
      return i + -56;
    }
    localObject = e;
    paramInt = d.getOriginalPosition(paramInt);
    return ((RecyclerView.Adapter)localObject).getItemViewType(paramInt);
  }
  
  public final int getOriginalPosition(int paramInt)
  {
    return d.getOriginalPosition(paramInt);
  }
  
  public final boolean isAd(int paramInt)
  {
    return d.isAd(paramInt);
  }
  
  public final void loadAds(String paramString)
  {
    d.loadAds(paramString);
  }
  
  public final void loadAds(String paramString, RequestParameters paramRequestParameters)
  {
    d.loadAds(paramString, paramRequestParameters);
  }
  
  public final void onAttachedToRecyclerView(RecyclerView paramRecyclerView)
  {
    super.onAttachedToRecyclerView(paramRecyclerView);
    c = paramRecyclerView;
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    Object localObject = d.getAdData(paramInt);
    if (localObject != null)
    {
      MoPubStreamAdPlacer localMoPubStreamAdPlacer = d;
      localObject = (NativeAd)localObject;
      paramViewHolder = itemView;
      localMoPubStreamAdPlacer.bindAdView((NativeAd)localObject, paramViewHolder);
      return;
    }
    localObject = g;
    View localView = itemView;
    Integer localInteger = Integer.valueOf(paramInt);
    ((WeakHashMap)localObject).put(localView, localInteger);
    localObject = f;
    localView = itemView;
    ((VisibilityTracker)localObject).addView(localView, 0, null);
    localObject = e;
    paramInt = d.getOriginalPosition(paramInt);
    ((RecyclerView.Adapter)localObject).onBindViewHolder(paramViewHolder, paramInt);
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    int i = -56;
    if (paramInt >= i)
    {
      Object localObject = d;
      int j = ((MoPubStreamAdPlacer)localObject).getAdViewTypeCount() + i;
      if (paramInt <= j)
      {
        localObject = d;
        paramInt -= i;
        MoPubAdRenderer localMoPubAdRenderer = ((MoPubStreamAdPlacer)localObject).getAdRendererForViewType(paramInt);
        if (localMoPubAdRenderer == null)
        {
          MoPubLog.w("No view binder was registered for ads in MoPubRecyclerAdapter.");
          return null;
        }
        MoPubRecyclerViewHolder localMoPubRecyclerViewHolder = new com/mopub/nativeads/MoPubRecyclerViewHolder;
        localObject = (Activity)paramViewGroup.getContext();
        paramViewGroup = localMoPubAdRenderer.createAdView((Context)localObject, paramViewGroup);
        localMoPubRecyclerViewHolder.<init>(paramViewGroup);
        return localMoPubRecyclerViewHolder;
      }
    }
    return e.onCreateViewHolder(paramViewGroup, paramInt);
  }
  
  public final void onDetachedFromRecyclerView(RecyclerView paramRecyclerView)
  {
    super.onDetachedFromRecyclerView(paramRecyclerView);
    c = null;
  }
  
  public final boolean onFailedToRecycleView(RecyclerView.ViewHolder paramViewHolder)
  {
    boolean bool = paramViewHolder instanceof MoPubRecyclerViewHolder;
    if (bool) {
      return super.onFailedToRecycleView((RecyclerView.ViewHolder)paramViewHolder);
    }
    return e.onFailedToRecycleView(paramViewHolder);
  }
  
  public final void onViewAttachedToWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    boolean bool = paramViewHolder instanceof MoPubRecyclerViewHolder;
    if (bool)
    {
      super.onViewAttachedToWindow((RecyclerView.ViewHolder)paramViewHolder);
      return;
    }
    e.onViewAttachedToWindow(paramViewHolder);
  }
  
  public final void onViewDetachedFromWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    boolean bool = paramViewHolder instanceof MoPubRecyclerViewHolder;
    if (bool)
    {
      super.onViewDetachedFromWindow((RecyclerView.ViewHolder)paramViewHolder);
      return;
    }
    e.onViewDetachedFromWindow(paramViewHolder);
  }
  
  public final void onViewRecycled(RecyclerView.ViewHolder paramViewHolder)
  {
    boolean bool = paramViewHolder instanceof MoPubRecyclerViewHolder;
    if (bool)
    {
      super.onViewRecycled((RecyclerView.ViewHolder)paramViewHolder);
      return;
    }
    e.onViewRecycled(paramViewHolder);
  }
  
  public final void refreshAds(String paramString)
  {
    refreshAds(paramString, null);
  }
  
  public final void refreshAds(String paramString, RequestParameters paramRequestParameters)
  {
    Object localObject = c;
    if (localObject == null)
    {
      MoPubLog.w("This adapter is not attached to a RecyclerView and cannot be refreshed.");
      return;
    }
    localObject = ((RecyclerView)localObject).getLayoutManager();
    if (localObject == null)
    {
      MoPubLog.w("Can't refresh ads when there is no layout manager on a RecyclerView.");
      return;
    }
    boolean bool1 = localObject instanceof LinearLayoutManager;
    if (bool1)
    {
      localObject = (LinearLayoutManager)localObject;
      int i = ((LinearLayoutManager)localObject).findFirstVisibleItemPosition();
      RecyclerView.ViewHolder localViewHolder = c.findViewHolderForLayoutPosition(i);
      int j = computeScrollOffset((LinearLayoutManager)localObject, localViewHolder);
      int k = i + -1;
      k = Math.max(0, k);
      for (;;)
      {
        localMoPubStreamAdPlacer1 = d;
        boolean bool2 = localMoPubStreamAdPlacer1.isAd(k);
        if ((!bool2) || (k <= 0)) {
          break;
        }
        k += -1;
      }
      int m = getItemCount();
      int n = ((LinearLayoutManager)localObject).findLastVisibleItemPosition();
      for (;;)
      {
        localMoPubStreamAdPlacer2 = d;
        boolean bool3 = localMoPubStreamAdPlacer2.isAd(n);
        if (!bool3) {
          break;
        }
        int i1 = m + -1;
        if (n >= i1) {
          break;
        }
        n += 1;
      }
      k = d.getOriginalPosition(k);
      m = d.getOriginalPosition(n);
      RecyclerView.Adapter localAdapter = e;
      n = localAdapter.getItemCount();
      MoPubStreamAdPlacer localMoPubStreamAdPlacer2 = d;
      localMoPubStreamAdPlacer2.removeAdsInRange(m, n);
      MoPubStreamAdPlacer localMoPubStreamAdPlacer1 = d;
      k = localMoPubStreamAdPlacer1.removeAdsInRange(0, k);
      if (k > 0)
      {
        i -= k;
        ((LinearLayoutManager)localObject).scrollToPositionWithOffset(i, j);
      }
      loadAds(paramString, paramRequestParameters);
      return;
    }
    MoPubLog.w("This LayoutManager can't be refreshed.");
  }
  
  public final void registerAdRenderer(MoPubAdRenderer paramMoPubAdRenderer)
  {
    String str = "Cannot register a null adRenderer";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramMoPubAdRenderer, str);
    if (!bool) {
      return;
    }
    d.registerAdRenderer(paramMoPubAdRenderer);
  }
  
  public final void setAdLoadedListener(MoPubNativeAdLoadedListener paramMoPubNativeAdLoadedListener)
  {
    a = paramMoPubNativeAdLoadedListener;
  }
  
  public final void setContentChangeStrategy(MoPubRecyclerAdapter.ContentChangeStrategy paramContentChangeStrategy)
  {
    boolean bool = Preconditions.NoThrow.checkNotNull(paramContentChangeStrategy);
    if (!bool) {
      return;
    }
    h = paramContentChangeStrategy;
  }
  
  public final void setHasStableIds(boolean paramBoolean)
  {
    super.setHasStableIds(paramBoolean);
    Object localObject = e;
    RecyclerView.AdapterDataObserver localAdapterDataObserver = b;
    ((RecyclerView.Adapter)localObject).unregisterAdapterDataObserver(localAdapterDataObserver);
    e.setHasStableIds(paramBoolean);
    RecyclerView.Adapter localAdapter = e;
    localObject = b;
    localAdapter.registerAdapterDataObserver((RecyclerView.AdapterDataObserver)localObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubRecyclerAdapter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */