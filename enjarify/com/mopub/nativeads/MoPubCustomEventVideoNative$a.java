package com.mopub.nativeads;

import java.lang.ref.WeakReference;

final class MoPubCustomEventVideoNative$a
  implements NativeVideoController.b.a
{
  private final WeakReference a;
  
  MoPubCustomEventVideoNative$a(MoPubCustomEventVideoNative.MoPubVideoNativeAd paramMoPubVideoNativeAd)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramMoPubVideoNativeAd);
    a = localWeakReference;
  }
  
  public final void execute()
  {
    MoPubCustomEventVideoNative.MoPubVideoNativeAd localMoPubVideoNativeAd = (MoPubCustomEventVideoNative.MoPubVideoNativeAd)a.get();
    if (localMoPubVideoNativeAd != null) {
      localMoPubVideoNativeAd.a();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */