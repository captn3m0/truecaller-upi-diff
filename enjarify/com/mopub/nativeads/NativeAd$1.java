package com.mopub.nativeads;

import android.content.Context;
import com.mopub.network.TrackingRequest;

final class NativeAd$1
  implements BaseNativeAd.NativeEventListener
{
  NativeAd$1(NativeAd paramNativeAd) {}
  
  public final void onAdClicked()
  {
    NativeAd localNativeAd = a;
    boolean bool = f;
    if (!bool)
    {
      bool = g;
      if (!bool)
      {
        Object localObject = c;
        Context localContext = a;
        TrackingRequest.makeTrackingHttpRequest((Iterable)localObject, localContext);
        localObject = d;
        if (localObject != null)
        {
          localObject = d;
          localContext = null;
          ((NativeAd.MoPubNativeEventListener)localObject).onClick(null);
        }
        f = true;
        return;
      }
    }
  }
  
  public final void onAdImpressed()
  {
    NativeAd localNativeAd = a;
    boolean bool = e;
    if (!bool)
    {
      bool = g;
      if (!bool)
      {
        Object localObject = b;
        Context localContext = a;
        TrackingRequest.makeTrackingHttpRequest((Iterable)localObject, localContext);
        localObject = d;
        if (localObject != null)
        {
          localObject = d;
          localContext = null;
          ((NativeAd.MoPubNativeEventListener)localObject).onImpression(null);
        }
        e = true;
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeAd.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */