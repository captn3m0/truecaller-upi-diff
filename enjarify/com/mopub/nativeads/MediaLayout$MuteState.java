package com.mopub.nativeads;

public enum MediaLayout$MuteState
{
  static
  {
    Object localObject = new com/mopub/nativeads/MediaLayout$MuteState;
    ((MuteState)localObject).<init>("MUTED", 0);
    MUTED = (MuteState)localObject;
    localObject = new com/mopub/nativeads/MediaLayout$MuteState;
    int i = 1;
    ((MuteState)localObject).<init>("UNMUTED", i);
    UNMUTED = (MuteState)localObject;
    localObject = new MuteState[2];
    MuteState localMuteState = MUTED;
    localObject[0] = localMuteState;
    localMuteState = UNMUTED;
    localObject[i] = localMuteState;
    a = (MuteState[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MediaLayout.MuteState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */