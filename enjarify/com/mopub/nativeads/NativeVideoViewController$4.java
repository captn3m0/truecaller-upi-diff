package com.mopub.nativeads;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.UrlHandler.Builder;
import com.mopub.mobileads.VastVideoConfig;

final class NativeVideoViewController$4
  implements View.OnClickListener
{
  NativeVideoViewController$4(NativeVideoViewController paramNativeVideoViewController) {}
  
  public final void onClick(View paramView)
  {
    paramView = NativeVideoViewController.d(a);
    Object localObject1 = null;
    paramView.setPlayWhenReady(false);
    paramView = a;
    Object localObject2 = NativeVideoViewController.c(paramView).getTextureView().getBitmap();
    NativeVideoViewController.a(paramView, (Bitmap)localObject2);
    paramView = NativeVideoViewController.g(a).getPrivacyInformationIconClickthroughUrl();
    boolean bool = TextUtils.isEmpty(paramView);
    if (bool) {
      paramView = "https://www.mopub.com/optout/";
    }
    localObject2 = new com/mopub/common/UrlHandler$Builder;
    ((UrlHandler.Builder)localObject2).<init>();
    UrlAction localUrlAction = UrlAction.OPEN_IN_APP_BROWSER;
    localObject1 = new UrlAction[0];
    localObject1 = ((UrlHandler.Builder)localObject2).withSupportedUrlActions(localUrlAction, (UrlAction[])localObject1).build();
    localObject2 = NativeVideoViewController.h(a);
    ((UrlHandler)localObject1).handleUrl((Context)localObject2, paramView);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeVideoViewController.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */