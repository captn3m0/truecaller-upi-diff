package com.mopub.nativeads;

import android.content.Context;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSource.Factory;

final class NativeVideoController$1
  implements DataSource.Factory
{
  NativeVideoController$1(NativeVideoController paramNativeVideoController) {}
  
  public final DataSource createDataSource()
  {
    HttpDiskCompositeDataSource localHttpDiskCompositeDataSource = new com/mopub/nativeads/HttpDiskCompositeDataSource;
    Context localContext = NativeVideoController.a(a);
    localHttpDiskCompositeDataSource.<init>(localContext, "exo_demo");
    return localHttpDiskCompositeDataSource;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeVideoController.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */