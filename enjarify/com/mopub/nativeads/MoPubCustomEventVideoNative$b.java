package com.mopub.nativeads;

import android.content.Context;
import com.mopub.mobileads.VastVideoConfig;
import java.util.List;

final class MoPubCustomEventVideoNative$b
{
  public final NativeVideoController createForId(long paramLong, Context paramContext, List paramList, VastVideoConfig paramVastVideoConfig)
  {
    return NativeVideoController.createForId(paramLong, paramContext, paramList, paramVastVideoConfig);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */