package com.mopub.nativeads;

import android.content.Context;
import java.util.Map;

public abstract class CustomEventNative
{
  protected void a() {}
  
  protected abstract void a(Context paramContext, CustomEventNative.CustomEventNativeListener paramCustomEventNativeListener, Map paramMap1, Map paramMap2);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.CustomEventNative
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */