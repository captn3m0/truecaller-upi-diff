package com.mopub.nativeads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import java.lang.ref.WeakReference;

public final class AdapterHelper
{
  private final WeakReference a;
  private final Context b;
  private final int c;
  private final int d;
  
  public AdapterHelper(Context paramContext, int paramInt1, int paramInt2)
  {
    Object localObject = "Context cannot be null.";
    Preconditions.checkNotNull(paramContext, (String)localObject);
    boolean bool1 = true;
    boolean bool2;
    if (paramInt1 >= 0) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    String str = "start position must be non-negative";
    Preconditions.checkArgument(bool2, str);
    int i = 2;
    if (paramInt2 < i)
    {
      bool1 = false;
      localObject = null;
    }
    Preconditions.checkArgument(bool1, "interval must be at least 2");
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramContext);
    a = ((WeakReference)localObject);
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
    c = paramInt1;
    d = paramInt2;
  }
  
  public final View getAdView(View paramView, ViewGroup paramViewGroup, NativeAd paramNativeAd)
  {
    return getAdView(paramView, paramViewGroup, paramNativeAd, null);
  }
  
  public final View getAdView(View paramView, ViewGroup paramViewGroup, NativeAd paramNativeAd, ViewBinder paramViewBinder)
  {
    paramViewBinder = (Context)a.get();
    if (paramViewBinder == null)
    {
      MoPubLog.w("Weak reference to Context in AdapterHelper became null. Returning empty view.");
      paramView = new android/view/View;
      paramViewGroup = b;
      paramView.<init>(paramViewGroup);
      return paramView;
    }
    return e.a(paramView, paramViewGroup, paramViewBinder, paramNativeAd);
  }
  
  public final boolean isAdPosition(int paramInt)
  {
    int i = c;
    if (paramInt < i) {
      return false;
    }
    paramInt -= i;
    i = d;
    paramInt %= i;
    return paramInt == 0;
  }
  
  public final int shiftedCount(int paramInt)
  {
    int i = c;
    if (paramInt <= i)
    {
      i = 0;
    }
    else
    {
      int j = d + -1;
      int k = (paramInt - i) % j;
      if (k == 0)
      {
        i = (paramInt - i) / j;
      }
      else
      {
        double d1 = paramInt - i;
        double d2 = j;
        Double.isNaN(d1);
        Double.isNaN(d2);
        d1 /= d2;
        d2 = Math.floor(d1);
        i = (int)d2 + 1;
      }
    }
    return paramInt + i;
  }
  
  public final int shiftedPosition(int paramInt)
  {
    int i = c;
    if (paramInt <= i)
    {
      i = 0;
    }
    else
    {
      double d1 = paramInt - i;
      int j = d;
      double d2 = j;
      Double.isNaN(d1);
      Double.isNaN(d2);
      d1 = Math.floor(d1 / d2);
      i = (int)d1 + 1;
    }
    return paramInt - i;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.AdapterHelper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */