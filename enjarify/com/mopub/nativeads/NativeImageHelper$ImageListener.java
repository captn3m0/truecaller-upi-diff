package com.mopub.nativeads;

public abstract interface NativeImageHelper$ImageListener
{
  public abstract void onImagesCached();
  
  public abstract void onImagesFailedToCache(NativeErrorCode paramNativeErrorCode);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeImageHelper.ImageListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */