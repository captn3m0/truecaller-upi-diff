package com.mopub.nativeads;

import com.mopub.common.VisibilityTracker.VisibilityTrackerListener;
import java.util.List;

final class MoPubCustomEventVideoNative$MoPubVideoNativeAd$1
  implements VisibilityTracker.VisibilityTrackerListener
{
  MoPubCustomEventVideoNative$MoPubVideoNativeAd$1(MoPubCustomEventVideoNative.MoPubVideoNativeAd paramMoPubVideoNativeAd) {}
  
  public final void onVisibilityChanged(List paramList1, List paramList2)
  {
    boolean bool = paramList1.isEmpty();
    if (!bool)
    {
      paramList1 = a;
      bool = MoPubCustomEventVideoNative.MoPubVideoNativeAd.a(paramList1);
      if (!bool)
      {
        MoPubCustomEventVideoNative.MoPubVideoNativeAd.a(a, true);
        MoPubCustomEventVideoNative.MoPubVideoNativeAd.b(a);
        return;
      }
    }
    bool = paramList2.isEmpty();
    if (!bool)
    {
      paramList1 = a;
      bool = MoPubCustomEventVideoNative.MoPubVideoNativeAd.a(paramList1);
      if (bool)
      {
        paramList1 = a;
        paramList2 = null;
        MoPubCustomEventVideoNative.MoPubVideoNativeAd.a(paramList1, false);
        paramList1 = a;
        MoPubCustomEventVideoNative.MoPubVideoNativeAd.b(paramList1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.MoPubVideoNativeAd.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */