package com.mopub.nativeads;

import android.content.Context;
import android.os.Handler;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.nativeads.factories.CustomEventNativeFactory;
import com.mopub.network.AdResponse;
import java.util.Map;

final class b
{
  CustomEventNative.CustomEventNativeListener a;
  volatile boolean b;
  private final Handler c;
  private final Runnable d;
  private CustomEventNative e;
  
  b(CustomEventNative.CustomEventNativeListener paramCustomEventNativeListener)
  {
    Preconditions.checkNotNull(paramCustomEventNativeListener);
    a = paramCustomEventNativeListener;
    b = false;
    paramCustomEventNativeListener = new android/os/Handler;
    paramCustomEventNativeListener.<init>();
    c = paramCustomEventNativeListener;
    paramCustomEventNativeListener = new com/mopub/nativeads/b$1;
    paramCustomEventNativeListener.<init>(this);
    d = paramCustomEventNativeListener;
  }
  
  final void a()
  {
    try
    {
      localObject = e;
      if (localObject != null)
      {
        localObject = e;
        ((CustomEventNative)localObject).a();
      }
    }
    catch (Exception localException)
    {
      Object localObject = localException.toString();
      MoPubLog.e((String)localObject);
    }
    b();
  }
  
  final void b()
  {
    try
    {
      boolean bool = b;
      if (!bool)
      {
        bool = true;
        b = bool;
        Handler localHandler = c;
        Runnable localRunnable = d;
        localHandler.removeCallbacks(localRunnable);
        bool = false;
        localHandler = null;
        e = null;
      }
      return;
    }
    finally {}
  }
  
  public final void loadNativeAd(Context paramContext, Map paramMap, AdResponse paramAdResponse)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramMap);
    Preconditions.checkNotNull(paramAdResponse);
    Object localObject1 = paramAdResponse.getCustomEventClassName();
    Object localObject2 = String.valueOf(localObject1);
    Object localObject3 = "Attempting to invoke custom event: ".concat((String)localObject2);
    MoPubLog.d((String)localObject3);
    try
    {
      localObject3 = CustomEventNativeFactory.create((String)localObject1);
      e = ((CustomEventNative)localObject3);
      boolean bool = paramAdResponse.hasJson();
      if (bool)
      {
        localObject1 = "com_mopub_native_json";
        localObject3 = paramAdResponse.getJsonBody();
        paramMap.put(localObject1, localObject3);
      }
      localObject1 = "click-tracking-url";
      localObject3 = paramAdResponse.getClickTrackingUrl();
      paramMap.put(localObject1, localObject3);
      try
      {
        localObject1 = e;
        localObject3 = new com/mopub/nativeads/b$2;
        ((b.2)localObject3).<init>(this);
        localObject2 = paramAdResponse.getServerExtras();
        ((CustomEventNative)localObject1).a(paramContext, (CustomEventNative.CustomEventNativeListener)localObject3, paramMap, (Map)localObject2);
        int i = 30000;
        paramContext = paramAdResponse.getAdTimeoutMillis(i);
        i = paramContext.intValue();
        long l = i;
        paramAdResponse = c;
        localObject1 = d;
        paramAdResponse.postDelayed((Runnable)localObject1, l);
        return;
      }
      catch (Exception paramContext)
      {
        MoPubLog.w("Loading custom event native threw an error.", paramContext);
        paramContext = a;
        paramMap = NativeErrorCode.NATIVE_ADAPTER_NOT_FOUND;
        paramContext.onNativeAdFailed(paramMap);
        return;
      }
      return;
    }
    catch (Exception localException)
    {
      paramMap = String.valueOf(localObject1);
      MoPubLog.e("Failed to load Custom Event Native class: ".concat(paramMap));
      paramContext = a;
      paramMap = NativeErrorCode.NATIVE_ADAPTER_NOT_FOUND;
      paramContext.onNativeAdFailed(paramMap);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */