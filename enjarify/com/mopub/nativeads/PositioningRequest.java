package com.mopub.nativeads;

import android.content.Context;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.MoPubNetworkError.Reason;
import com.mopub.network.MoPubRequestUtils;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Response;
import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.Response.Listener;
import com.mopub.volley.VolleyError;
import com.mopub.volley.toolbox.HttpHeaderParser;
import com.mopub.volley.toolbox.JsonRequest;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PositioningRequest
  extends JsonRequest
{
  private final String a;
  private final Context b;
  
  public PositioningRequest(Context paramContext, String paramString, Response.Listener paramListener, Response.ErrorListener paramErrorListener)
  {
    super(i, str, null, paramListener, paramErrorListener);
    a = paramString;
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
  }
  
  private static void a(JSONArray paramJSONArray, MoPubNativeAdPositioning.MoPubClientPositioning paramMoPubClientPositioning)
  {
    int i = 0;
    int k;
    for (;;)
    {
      int j = paramJSONArray.length();
      if (i >= j) {
        return;
      }
      JSONObject localJSONObject = paramJSONArray.getJSONObject(i);
      String str = "section";
      k = localJSONObject.optInt(str, 0);
      if (k < 0) {
        break;
      }
      if (k <= 0)
      {
        str = "position";
        j = localJSONObject.getInt(str);
        if (j >= 0)
        {
          k = 65536;
          if (j <= k)
          {
            paramMoPubClientPositioning.addFixedPosition(j);
            break label117;
          }
        }
        paramJSONArray = new org/json/JSONException;
        paramMoPubClientPositioning = new java/lang/StringBuilder;
        paramMoPubClientPositioning.<init>("Invalid position ");
        paramMoPubClientPositioning.append(j);
        paramMoPubClientPositioning.append(" in JSON response");
        paramMoPubClientPositioning = paramMoPubClientPositioning.toString();
        paramJSONArray.<init>(paramMoPubClientPositioning);
        throw paramJSONArray;
      }
      label117:
      i += 1;
    }
    paramJSONArray = new org/json/JSONException;
    paramMoPubClientPositioning = new java/lang/StringBuilder;
    paramMoPubClientPositioning.<init>("Invalid section ");
    paramMoPubClientPositioning.append(k);
    paramMoPubClientPositioning.append(" in JSON response");
    paramMoPubClientPositioning = paramMoPubClientPositioning.toString();
    paramJSONArray.<init>(paramMoPubClientPositioning);
    throw paramJSONArray;
  }
  
  public byte[] getBody()
  {
    Object localObject = getParams();
    String str = getUrl();
    localObject = MoPubRequestUtils.generateBodyFromParams((Map)localObject, str);
    if (localObject == null) {
      return null;
    }
    return ((String)localObject).getBytes();
  }
  
  public final Map getParams()
  {
    Object localObject = getUrl();
    boolean bool = MoPubRequestUtils.isMoPubRequest((String)localObject);
    if (!bool) {
      return null;
    }
    localObject = b;
    String str = a;
    return MoPubRequestUtils.convertQueryToMap((Context)localObject, str);
  }
  
  public final Response parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    int i = statusCode;
    int j = 200;
    if (i != j)
    {
      localObject1 = new com/mopub/volley/VolleyError;
      ((VolleyError)localObject1).<init>(paramNetworkResponse);
      return Response.error((VolleyError)localObject1);
    }
    Object localObject1 = data;
    i = localObject1.length;
    Object localObject2;
    if (i == 0)
    {
      paramNetworkResponse = new com/mopub/volley/VolleyError;
      localObject2 = new org/json/JSONException;
      ((JSONException)localObject2).<init>("Empty response");
      paramNetworkResponse.<init>("Empty positioning response", (Throwable)localObject2);
      return Response.error(paramNetworkResponse);
    }
    try
    {
      localObject1 = new java/lang/String;
      localObject2 = data;
      Object localObject3 = headers;
      localObject3 = HttpHeaderParser.parseCharset((Map)localObject3);
      ((String)localObject1).<init>((byte[])localObject2, (String)localObject3);
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>((String)localObject1);
      localObject1 = "error";
      localObject3 = null;
      localObject1 = ((JSONObject)localObject2).optString((String)localObject1, null);
      if (localObject1 != null)
      {
        paramNetworkResponse = "WARMING_UP";
        boolean bool = ((String)localObject1).equalsIgnoreCase(paramNetworkResponse);
        if (bool)
        {
          paramNetworkResponse = new com/mopub/network/MoPubNetworkError;
          localObject1 = MoPubNetworkError.Reason.WARMING_UP;
          paramNetworkResponse.<init>((MoPubNetworkError.Reason)localObject1);
          throw paramNetworkResponse;
        }
        paramNetworkResponse = new org/json/JSONException;
        paramNetworkResponse.<init>((String)localObject1);
        throw paramNetworkResponse;
      }
      localObject1 = "fixed";
      localObject1 = ((JSONObject)localObject2).optJSONArray((String)localObject1);
      localObject3 = "repeating";
      localObject2 = ((JSONObject)localObject2).optJSONObject((String)localObject3);
      if ((localObject1 == null) && (localObject2 == null))
      {
        paramNetworkResponse = new org/json/JSONException;
        localObject1 = "Must contain fixed or repeating positions";
        paramNetworkResponse.<init>((String)localObject1);
        throw paramNetworkResponse;
      }
      localObject3 = new com/mopub/nativeads/MoPubNativeAdPositioning$MoPubClientPositioning;
      ((MoPubNativeAdPositioning.MoPubClientPositioning)localObject3).<init>();
      if (localObject1 != null) {
        a((JSONArray)localObject1, (MoPubNativeAdPositioning.MoPubClientPositioning)localObject3);
      }
      if (localObject2 != null)
      {
        localObject1 = "interval";
        i = ((JSONObject)localObject2).getInt((String)localObject1);
        j = 2;
        if (i >= j)
        {
          j = 65536;
          if (i <= j)
          {
            ((MoPubNativeAdPositioning.MoPubClientPositioning)localObject3).enableRepeatingPositions(i);
            break label359;
          }
        }
        paramNetworkResponse = new org/json/JSONException;
        localObject2 = new java/lang/StringBuilder;
        localObject3 = "Invalid interval ";
        ((StringBuilder)localObject2).<init>((String)localObject3);
        ((StringBuilder)localObject2).append(i);
        localObject1 = " in JSON response";
        ((StringBuilder)localObject2).append((String)localObject1);
        localObject1 = ((StringBuilder)localObject2).toString();
        paramNetworkResponse.<init>((String)localObject1);
        throw paramNetworkResponse;
      }
      label359:
      paramNetworkResponse = HttpHeaderParser.parseCacheHeaders(paramNetworkResponse);
      return Response.success(localObject3, paramNetworkResponse);
    }
    catch (MoPubNetworkError localMoPubNetworkError)
    {
      return Response.error(localMoPubNetworkError);
    }
    catch (JSONException paramNetworkResponse)
    {
      localObject1 = new com/mopub/volley/VolleyError;
      ((VolleyError)localObject1).<init>("JSON Parsing Error", paramNetworkResponse);
      return Response.error((VolleyError)localObject1);
    }
    catch (UnsupportedEncodingException paramNetworkResponse)
    {
      localObject1 = new com/mopub/volley/VolleyError;
      ((VolleyError)localObject1).<init>("Couldn't parse JSON from Charset", paramNetworkResponse);
    }
    return Response.error((VolleyError)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.PositioningRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */