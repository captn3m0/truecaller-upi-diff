package com.mopub.nativeads;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.mopub.common.AdUrlGenerator;
import com.mopub.common.BaseUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.MoPub;

final class f
  extends AdUrlGenerator
{
  String g;
  private String h;
  
  f(Context paramContext)
  {
    super(paramContext);
  }
  
  final f a(RequestParameters paramRequestParameters)
  {
    if (paramRequestParameters != null)
    {
      boolean bool = MoPub.canCollectPersonalInformation();
      Location localLocation = null;
      String str1;
      if (bool) {
        str1 = paramRequestParameters.getUserDataKeywords();
      } else {
        str1 = null;
      }
      d = str1;
      if (bool) {
        localLocation = paramRequestParameters.getLocation();
      }
      e = localLocation;
      String str2 = paramRequestParameters.getKeywords();
      c = str2;
      paramRequestParameters = paramRequestParameters.getDesiredAssets();
      h = paramRequestParameters;
    }
    return this;
  }
  
  public final String generateUrlString(String paramString)
  {
    String str = "/m/ad";
    a(paramString, str);
    paramString = ClientMetadata.getInstance(a);
    a(paramString);
    paramString = h;
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      paramString = "assets";
      str = h;
      b(paramString, str);
    }
    paramString = g;
    bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      paramString = "MAGIC_NO";
      str = g;
      b(paramString, str);
    }
    return f.toString();
  }
  
  public final f withAdUnitId(String paramString)
  {
    b = paramString;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */