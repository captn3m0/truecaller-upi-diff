package com.mopub.nativeads;

public abstract interface BaseNativeAd$NativeEventListener
{
  public abstract void onAdClicked();
  
  public abstract void onAdImpressed();
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.BaseNativeAd.NativeEventListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */