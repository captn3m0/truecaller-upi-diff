package com.mopub.nativeads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.mopub.common.Preconditions;
import com.mopub.common.util.Dips;

final class NativeFullScreenVideoView$a
  extends Drawable
{
  final int a;
  private final RectF b;
  private final Paint c;
  
  NativeFullScreenVideoView$a(Context paramContext)
  {
    this(paramContext, localRectF, localPaint);
  }
  
  private NativeFullScreenVideoView$a(Context paramContext, RectF paramRectF, Paint paramPaint)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramRectF);
    Preconditions.checkNotNull(paramPaint);
    b = paramRectF;
    c = paramPaint;
    c.setColor(-16777216);
    c.setAlpha(128);
    c.setAntiAlias(true);
    int i = Dips.asIntPixels(5.0F, paramContext);
    a = i;
  }
  
  public final void draw(Canvas paramCanvas)
  {
    RectF localRectF = b;
    Rect localRect = getBounds();
    localRectF.set(localRect);
    localRectF = b;
    int i = a;
    float f1 = i;
    float f2 = i;
    Paint localPaint = c;
    paramCanvas.drawRoundRect(localRectF, f1, f2, localPaint);
  }
  
  public final int getOpacity()
  {
    return 0;
  }
  
  public final void setAlpha(int paramInt) {}
  
  public final void setColorFilter(ColorFilter paramColorFilter) {}
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeFullScreenVideoView.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */