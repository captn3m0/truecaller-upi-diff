package com.mopub.nativeads;

import android.content.Context;
import com.mopub.network.TrackingRequest;

final class MoPubCustomEventVideoNative$c
  implements NativeVideoController.b.a
{
  private final Context a;
  private final String b;
  
  MoPubCustomEventVideoNative$c(Context paramContext, String paramString)
  {
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    b = paramString;
  }
  
  public final void execute()
  {
    String str = b;
    Context localContext = a;
    TrackingRequest.makeTrackingHttpRequest(str, localContext);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */