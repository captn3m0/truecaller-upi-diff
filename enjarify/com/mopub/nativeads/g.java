package com.mopub.nativeads;

import com.mopub.common.logging.MoPubLog;

final class g
{
  public static final int NOT_FOUND = 255;
  final int[] a;
  int b;
  private final int[] c;
  private final int[] d;
  private int e;
  private final int[] f;
  private final NativeAd[] g;
  
  g(int[] paramArrayOfInt)
  {
    int i = 200;
    int[] arrayOfInt1 = new int[i];
    c = arrayOfInt1;
    arrayOfInt1 = new int[i];
    d = arrayOfInt1;
    e = 0;
    Object localObject = new int[i];
    f = ((int[])localObject);
    localObject = new int[i];
    a = ((int[])localObject);
    localObject = new NativeAd[i];
    g = ((NativeAd[])localObject);
    b = 0;
    i = Math.min(paramArrayOfInt.length, i);
    e = i;
    int[] arrayOfInt2 = d;
    int j = e;
    System.arraycopy(paramArrayOfInt, 0, arrayOfInt2, 0, j);
    arrayOfInt2 = c;
    j = e;
    System.arraycopy(paramArrayOfInt, 0, arrayOfInt2, 0, j);
  }
  
  static int a(int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    paramInt1 += -1;
    int i = 0;
    while (i <= paramInt1)
    {
      int j = i + paramInt1 >>> 1;
      int k = paramArrayOfInt[j];
      if (k < paramInt2) {
        i = j + 1;
      } else if (k > paramInt2) {
        paramInt1 = j + -1;
      } else {
        return j;
      }
    }
    return i ^ 0xFFFFFFFF;
  }
  
  private static int b(int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    paramInt1 = a(paramArrayOfInt, paramInt1, paramInt2);
    if (paramInt1 < 0) {
      return paramInt1 ^ 0xFFFFFFFF;
    }
    paramInt2 = paramArrayOfInt[paramInt1];
    while (paramInt1 >= 0)
    {
      int i = paramArrayOfInt[paramInt1];
      if (i != paramInt2) {
        break;
      }
      paramInt1 += -1;
    }
    return paramInt1 + 1;
  }
  
  private static int c(int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    paramInt2 = a(paramArrayOfInt, paramInt1, paramInt2);
    if (paramInt2 < 0) {
      return paramInt2 ^ 0xFFFFFFFF;
    }
    int i = paramArrayOfInt[paramInt2];
    while (paramInt2 < paramInt1)
    {
      int j = paramArrayOfInt[paramInt2];
      if (j != i) {
        break;
      }
      paramInt2 += 1;
    }
    return paramInt2;
  }
  
  final int a(int paramInt1, int paramInt2)
  {
    int i = b;
    int[] arrayOfInt1 = new int[i];
    int[] arrayOfInt2 = new int[i];
    int j = 0;
    int k = 0;
    int[] arrayOfInt3 = null;
    int m = 0;
    int n;
    int i1;
    for (;;)
    {
      n = b;
      if (k >= n) {
        break;
      }
      Object localObject = f;
      n = localObject[k];
      int[] arrayOfInt4 = a;
      i1 = arrayOfInt4[k];
      if ((paramInt1 <= i1) && (i1 < paramInt2))
      {
        arrayOfInt1[m] = n;
        i1 -= m;
        arrayOfInt2[m] = i1;
        g[k].destroy();
        localObject = g;
        i1 = 0;
        arrayOfInt4 = null;
        localObject[k] = null;
        m += 1;
      }
      else if (m > 0)
      {
        int i2 = k - m;
        int[] arrayOfInt5 = f;
        arrayOfInt5[i2] = n;
        localObject = a;
        i1 -= m;
        localObject[i2] = i1;
        localObject = g;
        arrayOfInt4 = localObject[k];
        localObject[i2] = arrayOfInt4;
      }
      k += 1;
    }
    if (m == 0) {
      return 0;
    }
    paramInt1 = arrayOfInt2[0];
    int[] arrayOfInt6 = d;
    k = e;
    paramInt1 = b(arrayOfInt6, k, paramInt1);
    paramInt2 = e + -1;
    while (paramInt2 >= paramInt1)
    {
      arrayOfInt3 = c;
      n = paramInt2 + m;
      i1 = arrayOfInt3[paramInt2];
      arrayOfInt3[n] = i1;
      arrayOfInt3 = d;
      i1 = arrayOfInt3[paramInt2] - m;
      arrayOfInt3[n] = i1;
      paramInt2 += -1;
    }
    while (j < m)
    {
      arrayOfInt6 = c;
      k = paramInt1 + j;
      n = arrayOfInt1[j];
      arrayOfInt6[k] = n;
      arrayOfInt6 = d;
      n = arrayOfInt2[j];
      arrayOfInt6[k] = n;
      j += 1;
    }
    paramInt1 = e + m;
    e = paramInt1;
    paramInt1 = b - m;
    b = paramInt1;
    return m;
  }
  
  final void a(int paramInt, NativeAd paramNativeAd)
  {
    int[] arrayOfInt1 = d;
    int i = e;
    int j = b(arrayOfInt1, i, paramInt);
    i = e;
    if (j != i)
    {
      int[] arrayOfInt2 = d;
      i = arrayOfInt2[j];
      if (i == paramInt)
      {
        arrayOfInt2 = c;
        i = arrayOfInt2[j];
        int[] arrayOfInt3 = f;
        int k = b;
        int m = c(arrayOfInt3, k, i);
        k = b;
        if (m < k)
        {
          k -= m;
          Object localObject1 = f;
          int n = m + 1;
          System.arraycopy(localObject1, m, localObject1, n, k);
          localObject1 = a;
          System.arraycopy(localObject1, m, localObject1, n, k);
          localObject1 = g;
          System.arraycopy(localObject1, m, localObject1, n, k);
        }
        int[] arrayOfInt4 = f;
        arrayOfInt4[m] = i;
        arrayOfInt2 = a;
        arrayOfInt2[m] = paramInt;
        Object localObject2 = g;
        localObject2[m] = paramNativeAd;
        paramInt = b + 1;
        b = paramInt;
        paramInt = e - j + -1;
        paramNativeAd = d;
        i = j + 1;
        System.arraycopy(paramNativeAd, i, paramNativeAd, j, paramInt);
        paramNativeAd = c;
        System.arraycopy(paramNativeAd, i, paramNativeAd, j, paramInt);
        paramInt = e + -1;
        e = paramInt;
        int i1;
        for (;;)
        {
          paramInt = e;
          if (j >= paramInt) {
            break;
          }
          localObject2 = d;
          i1 = localObject2[j] + 1;
          localObject2[j] = i1;
          j += 1;
        }
        for (;;)
        {
          m += 1;
          paramInt = b;
          if (m >= paramInt) {
            break;
          }
          localObject2 = a;
          i1 = localObject2[m] + 1;
          localObject2[m] = i1;
        }
        return;
      }
    }
    MoPubLog.w("Attempted to insert an ad at an invalid position");
  }
  
  final boolean a(int paramInt)
  {
    int[] arrayOfInt = d;
    int i = e;
    paramInt = a(arrayOfInt, i, paramInt);
    return paramInt >= 0;
  }
  
  final int b(int paramInt)
  {
    int[] arrayOfInt = d;
    int i = e;
    paramInt = c(arrayOfInt, i, paramInt);
    int j = e;
    if (paramInt == j) {
      return -1;
    }
    return d[paramInt];
  }
  
  final NativeAd c(int paramInt)
  {
    int[] arrayOfInt = a;
    int i = b;
    paramInt = a(arrayOfInt, i, paramInt);
    if (paramInt < 0) {
      return null;
    }
    return g[paramInt];
  }
  
  final int d(int paramInt)
  {
    int[] arrayOfInt = a;
    int i = b;
    int j = a(arrayOfInt, i, paramInt);
    i = -1;
    if (j < 0)
    {
      j ^= i;
      return paramInt - j;
    }
    return i;
  }
  
  final int e(int paramInt)
  {
    int[] arrayOfInt = f;
    int i = b;
    int j = c(arrayOfInt, i, paramInt);
    return paramInt + j;
  }
  
  final int f(int paramInt)
  {
    if (paramInt == 0) {
      return 0;
    }
    paramInt += -1;
    return e(paramInt) + 1;
  }
  
  final void g(int paramInt)
  {
    int[] arrayOfInt1 = c;
    int i = e;
    int j = b(arrayOfInt1, i, paramInt);
    for (;;)
    {
      i = e;
      if (j >= i) {
        break;
      }
      int[] arrayOfInt2 = c;
      int k = arrayOfInt2[j] + 1;
      arrayOfInt2[j] = k;
      arrayOfInt2 = d;
      k = arrayOfInt2[j] + 1;
      arrayOfInt2[j] = k;
      j += 1;
    }
    arrayOfInt1 = f;
    i = b;
    paramInt = b(arrayOfInt1, i, paramInt);
    for (;;)
    {
      j = b;
      if (paramInt >= j) {
        break;
      }
      arrayOfInt1 = f;
      i = arrayOfInt1[paramInt] + 1;
      arrayOfInt1[paramInt] = i;
      arrayOfInt1 = a;
      i = arrayOfInt1[paramInt] + 1;
      arrayOfInt1[paramInt] = i;
      paramInt += 1;
    }
  }
  
  final void h(int paramInt)
  {
    int[] arrayOfInt1 = c;
    int i = e;
    int j = c(arrayOfInt1, i, paramInt);
    for (;;)
    {
      i = e;
      if (j >= i) {
        break;
      }
      int[] arrayOfInt2 = c;
      int k = arrayOfInt2[j] + -1;
      arrayOfInt2[j] = k;
      arrayOfInt2 = d;
      k = arrayOfInt2[j] + -1;
      arrayOfInt2[j] = k;
      j += 1;
    }
    arrayOfInt1 = f;
    i = b;
    paramInt = c(arrayOfInt1, i, paramInt);
    for (;;)
    {
      j = b;
      if (paramInt >= j) {
        break;
      }
      arrayOfInt1 = f;
      i = arrayOfInt1[paramInt] + -1;
      arrayOfInt1[paramInt] = i;
      arrayOfInt1 = a;
      i = arrayOfInt1[paramInt] + -1;
      arrayOfInt1[paramInt] = i;
      paramInt += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */