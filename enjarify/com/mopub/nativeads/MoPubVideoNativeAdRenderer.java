package com.mopub.nativeads;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.mopub.common.Preconditions;
import java.util.Map;
import java.util.WeakHashMap;

public class MoPubVideoNativeAdRenderer
  implements MoPubAdRenderer
{
  final WeakHashMap a;
  private final MediaViewBinder b;
  
  public MoPubVideoNativeAdRenderer(MediaViewBinder paramMediaViewBinder)
  {
    b = paramMediaViewBinder;
    paramMediaViewBinder = new java/util/WeakHashMap;
    paramMediaViewBinder.<init>();
    a = paramMediaViewBinder;
  }
  
  public View createAdView(Context paramContext, ViewGroup paramViewGroup)
  {
    paramContext = LayoutInflater.from(paramContext);
    int i = b.a;
    return paramContext.inflate(i, paramViewGroup, false);
  }
  
  public void renderAdView(View paramView, VideoNativeAd paramVideoNativeAd)
  {
    Object localObject1 = (c)a.get(paramView);
    if (localObject1 == null)
    {
      localObject1 = b;
      localObject1 = c.a(paramView, (MediaViewBinder)localObject1);
      localObject2 = a;
      ((WeakHashMap)localObject2).put(paramView, localObject1);
    }
    Object localObject2 = c;
    Object localObject3 = paramVideoNativeAd.getTitle();
    NativeRendererHelper.addTextView((TextView)localObject2, (String)localObject3);
    localObject2 = d;
    localObject3 = paramVideoNativeAd.getText();
    NativeRendererHelper.addTextView((TextView)localObject2, (String)localObject3);
    localObject2 = f;
    localObject3 = a;
    Object localObject4 = paramVideoNativeAd.getCallToAction();
    NativeRendererHelper.addCtaButton((TextView)localObject2, (View)localObject3, (String)localObject4);
    localObject2 = b;
    if (localObject2 != null)
    {
      localObject2 = paramVideoNativeAd.getMainImageUrl();
      localObject3 = b.getMainImageView();
      NativeImageHelper.loadImageView((String)localObject2, (ImageView)localObject3);
    }
    localObject2 = paramVideoNativeAd.getIconImageUrl();
    localObject3 = e;
    NativeImageHelper.loadImageView((String)localObject2, (ImageView)localObject3);
    localObject2 = g;
    localObject3 = paramVideoNativeAd.getPrivacyInformationIconImageUrl();
    localObject4 = paramVideoNativeAd.getPrivacyInformationIconClickThroughUrl();
    NativeRendererHelper.addPrivacyInformationIcon((ImageView)localObject2, (String)localObject3, (String)localObject4);
    localObject2 = a;
    localObject3 = b.h;
    localObject4 = paramVideoNativeAd.getExtras();
    NativeRendererHelper.updateExtras((View)localObject2, (Map)localObject3, (Map)localObject4);
    localObject2 = a;
    if (localObject2 != null)
    {
      localObject1 = a;
      localObject2 = null;
      ((View)localObject1).setVisibility(0);
    }
    int i = b.b;
    paramView = (MediaLayout)paramView.findViewById(i);
    paramVideoNativeAd.render(paramView);
  }
  
  public boolean supports(BaseNativeAd paramBaseNativeAd)
  {
    Preconditions.checkNotNull(paramBaseNativeAd);
    return paramBaseNativeAd instanceof VideoNativeAd;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubVideoNativeAdRenderer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */