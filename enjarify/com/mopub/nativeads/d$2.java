package com.mopub.nativeads;

import android.os.Handler;
import java.util.List;

final class d$2
  implements MoPubNative.MoPubNativeNetworkListener
{
  d$2(d paramd) {}
  
  public final void onNativeFail(NativeErrorCode paramNativeErrorCode)
  {
    paramNativeErrorCode = a;
    int i = 0;
    Runnable localRunnable = null;
    c = false;
    int j = f;
    Object localObject = d.a;
    int k = localObject.length;
    int m = 1;
    k -= m;
    if (j >= k)
    {
      a.f = 0;
      return;
    }
    paramNativeErrorCode = a;
    i = f;
    localObject = d.a;
    k = localObject.length - m;
    if (i < k)
    {
      i = f + m;
      f = i;
    }
    paramNativeErrorCode = a;
    d = m;
    paramNativeErrorCode = d.e(paramNativeErrorCode);
    localRunnable = d.d(a);
    localObject = a;
    int n = f;
    int[] arrayOfInt1 = d.a;
    int i1 = arrayOfInt1.length;
    if (n >= i1)
    {
      int[] arrayOfInt2 = d.a;
      n = arrayOfInt2.length - m;
      f = n;
    }
    int[] arrayOfInt3 = d.a;
    k = f;
    long l = arrayOfInt3[k];
    paramNativeErrorCode.postDelayed(localRunnable, l);
  }
  
  public final void onNativeLoad(NativeAd paramNativeAd)
  {
    Object localObject = d.a(a);
    if (localObject == null) {
      return;
    }
    localObject = a;
    c = false;
    int i = e;
    int j = 1;
    i += j;
    e = i;
    localObject = a;
    f = 0;
    localObject = d.b((d)localObject);
    l locall = new com/mopub/nativeads/l;
    locall.<init>(paramNativeAd);
    ((List)localObject).add(locall);
    paramNativeAd = d.b(a);
    int k = paramNativeAd.size();
    if (k == j)
    {
      paramNativeAd = d.c(a);
      if (paramNativeAd != null)
      {
        paramNativeAd = d.c(a);
        paramNativeAd.onAdsAvailable();
      }
    }
    a.c();
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.d.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */