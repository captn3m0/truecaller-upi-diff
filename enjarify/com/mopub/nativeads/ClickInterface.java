package com.mopub.nativeads;

import android.view.View;

public abstract interface ClickInterface
{
  public abstract void handleClick(View paramView);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.ClickInterface
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */