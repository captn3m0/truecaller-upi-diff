package com.mopub.nativeads;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;

final class MoPubAdAdapter$5
  implements AdapterView.OnItemLongClickListener
{
  MoPubAdAdapter$5(MoPubAdAdapter paramMoPubAdAdapter, AdapterView.OnItemLongClickListener paramOnItemLongClickListener) {}
  
  public final boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    Object localObject = b;
    boolean bool1 = ((MoPubAdAdapter)localObject).isAd(paramInt);
    if (!bool1)
    {
      AdapterView.OnItemLongClickListener localOnItemLongClickListener = a;
      localObject = MoPubAdAdapter.b(b);
      int i = ((MoPubStreamAdPlacer)localObject).getOriginalPosition(paramInt);
      boolean bool2 = localOnItemLongClickListener.onItemLongClick(paramAdapterView, paramView, i, paramLong);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubAdAdapter.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */