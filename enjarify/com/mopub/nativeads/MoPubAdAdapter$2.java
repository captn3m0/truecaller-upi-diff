package com.mopub.nativeads;

import android.database.DataSetObserver;
import android.widget.Adapter;

final class MoPubAdAdapter$2
  extends DataSetObserver
{
  MoPubAdAdapter$2(MoPubAdAdapter paramMoPubAdAdapter) {}
  
  public final void onChanged()
  {
    MoPubStreamAdPlacer localMoPubStreamAdPlacer = MoPubAdAdapter.b(a);
    int i = MoPubAdAdapter.a(a).getCount();
    localMoPubStreamAdPlacer.setItemCount(i);
    a.notifyDataSetChanged();
  }
  
  public final void onInvalidated()
  {
    a.notifyDataSetInvalidated();
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubAdAdapter.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */