package com.mopub.nativeads;

import com.mopub.common.Preconditions.NoThrow;
import java.util.ArrayList;
import java.util.Collections;

public class MoPubNativeAdPositioning$MoPubClientPositioning
{
  public static final int NO_REPEAT = Integer.MAX_VALUE;
  final ArrayList a;
  int b;
  
  public MoPubNativeAdPositioning$MoPubClientPositioning()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
    b = (-1 >>> 1);
  }
  
  public MoPubClientPositioning addFixedPosition(int paramInt)
  {
    if (paramInt >= 0)
    {
      bool = true;
    }
    else
    {
      bool = false;
      localArrayList = null;
    }
    boolean bool = Preconditions.NoThrow.checkArgument(bool);
    if (!bool) {
      return this;
    }
    ArrayList localArrayList = a;
    Object localObject = Integer.valueOf(paramInt);
    int i = Collections.binarySearch(localArrayList, localObject);
    if (i < 0)
    {
      localObject = a;
      i ^= 0xFFFFFFFF;
      Integer localInteger = Integer.valueOf(paramInt);
      ((ArrayList)localObject).add(i, localInteger);
    }
    return this;
  }
  
  public MoPubClientPositioning enableRepeatingPositions(int paramInt)
  {
    int i = 1;
    if (paramInt <= i) {
      i = 0;
    }
    String str = "Repeating interval must be greater than 1";
    boolean bool = Preconditions.NoThrow.checkArgument(i, str);
    if (!bool)
    {
      b = (-1 >>> 1);
      return this;
    }
    b = paramInt;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubNativeAdPositioning.MoPubClientPositioning
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */