package com.mopub.nativeads;

import com.mopub.mobileads.MoPubError;

public enum NativeErrorCode
  implements MoPubError
{
  private final String a;
  
  static
  {
    Object localObject = new com/mopub/nativeads/NativeErrorCode;
    ((NativeErrorCode)localObject).<init>("AD_SUCCESS", 0, "ad successfully loaded.");
    AD_SUCCESS = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int i = 1;
    ((NativeErrorCode)localObject).<init>("EMPTY_AD_RESPONSE", i, "Server returned empty response.");
    EMPTY_AD_RESPONSE = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int j = 2;
    ((NativeErrorCode)localObject).<init>("INVALID_RESPONSE", j, "Unable to parse response from server.");
    INVALID_RESPONSE = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int k = 3;
    ((NativeErrorCode)localObject).<init>("IMAGE_DOWNLOAD_FAILURE", k, "Unable to download images associated with ad.");
    IMAGE_DOWNLOAD_FAILURE = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int m = 4;
    ((NativeErrorCode)localObject).<init>("INVALID_REQUEST_URL", m, "Invalid request url.");
    INVALID_REQUEST_URL = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int n = 5;
    ((NativeErrorCode)localObject).<init>("UNEXPECTED_RESPONSE_CODE", n, "Received unexpected response code from server.");
    UNEXPECTED_RESPONSE_CODE = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int i1 = 6;
    ((NativeErrorCode)localObject).<init>("SERVER_ERROR_RESPONSE_CODE", i1, "Server returned erroneous response code.");
    SERVER_ERROR_RESPONSE_CODE = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int i2 = 7;
    ((NativeErrorCode)localObject).<init>("CONNECTION_ERROR", i2, "Network is unavailable.");
    CONNECTION_ERROR = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int i3 = 8;
    ((NativeErrorCode)localObject).<init>("UNSPECIFIED", i3, "Unspecified error occurred.");
    UNSPECIFIED = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int i4 = 9;
    ((NativeErrorCode)localObject).<init>("NETWORK_INVALID_REQUEST", i4, "Third-party network received invalid request.");
    NETWORK_INVALID_REQUEST = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int i5 = 10;
    ((NativeErrorCode)localObject).<init>("NETWORK_TIMEOUT", i5, "Third-party network failed to respond in a timely manner.");
    NETWORK_TIMEOUT = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    int i6 = 11;
    ((NativeErrorCode)localObject).<init>("NETWORK_NO_FILL", i6, "Third-party network failed to provide an ad.");
    NETWORK_NO_FILL = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    ((NativeErrorCode)localObject).<init>("NETWORK_INVALID_STATE", 12, "Third-party network failed due to invalid internal state.");
    NETWORK_INVALID_STATE = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    ((NativeErrorCode)localObject).<init>("NATIVE_RENDERER_CONFIGURATION_ERROR", 13, "A required renderer was not registered for the CustomEventNative.");
    NATIVE_RENDERER_CONFIGURATION_ERROR = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    ((NativeErrorCode)localObject).<init>("NATIVE_ADAPTER_CONFIGURATION_ERROR", 14, "CustomEventNative was configured incorrectly.");
    NATIVE_ADAPTER_CONFIGURATION_ERROR = (NativeErrorCode)localObject;
    localObject = new com/mopub/nativeads/NativeErrorCode;
    ((NativeErrorCode)localObject).<init>("NATIVE_ADAPTER_NOT_FOUND", 15, "Unable to find CustomEventNative.");
    NATIVE_ADAPTER_NOT_FOUND = (NativeErrorCode)localObject;
    localObject = new NativeErrorCode[16];
    NativeErrorCode localNativeErrorCode = AD_SUCCESS;
    localObject[0] = localNativeErrorCode;
    localNativeErrorCode = EMPTY_AD_RESPONSE;
    localObject[i] = localNativeErrorCode;
    localNativeErrorCode = INVALID_RESPONSE;
    localObject[j] = localNativeErrorCode;
    localNativeErrorCode = IMAGE_DOWNLOAD_FAILURE;
    localObject[k] = localNativeErrorCode;
    localNativeErrorCode = INVALID_REQUEST_URL;
    localObject[m] = localNativeErrorCode;
    localNativeErrorCode = UNEXPECTED_RESPONSE_CODE;
    localObject[n] = localNativeErrorCode;
    localNativeErrorCode = SERVER_ERROR_RESPONSE_CODE;
    localObject[i1] = localNativeErrorCode;
    localNativeErrorCode = CONNECTION_ERROR;
    localObject[i2] = localNativeErrorCode;
    localNativeErrorCode = UNSPECIFIED;
    localObject[i3] = localNativeErrorCode;
    localNativeErrorCode = NETWORK_INVALID_REQUEST;
    localObject[i4] = localNativeErrorCode;
    localNativeErrorCode = NETWORK_TIMEOUT;
    localObject[i5] = localNativeErrorCode;
    localNativeErrorCode = NETWORK_NO_FILL;
    localObject[i6] = localNativeErrorCode;
    localNativeErrorCode = NETWORK_INVALID_STATE;
    localObject[12] = localNativeErrorCode;
    localNativeErrorCode = NATIVE_RENDERER_CONFIGURATION_ERROR;
    localObject[13] = localNativeErrorCode;
    localNativeErrorCode = NATIVE_ADAPTER_CONFIGURATION_ERROR;
    localObject[14] = localNativeErrorCode;
    localNativeErrorCode = NATIVE_ADAPTER_NOT_FOUND;
    localObject[15] = localNativeErrorCode;
    b = (NativeErrorCode[])localObject;
  }
  
  private NativeErrorCode(String paramString1)
  {
    a = paramString1;
  }
  
  public final int getIntCode()
  {
    int[] arrayOfInt = NativeErrorCode.1.a;
    int i = ordinal();
    int j = arrayOfInt[i];
    switch (j)
    {
    default: 
      return 10000;
    case 3: 
      return 0;
    case 2: 
      return 1;
    }
    return 2;
  }
  
  public final String toString()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeErrorCode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */