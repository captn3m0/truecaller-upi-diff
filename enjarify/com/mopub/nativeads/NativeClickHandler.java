package com.mopub.nativeads;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.mopub.common.Preconditions;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.UrlHandler.Builder;
import com.mopub.common.UrlHandler.ResultActions;

public class NativeClickHandler
{
  private final Context a;
  private final String b;
  private boolean c;
  
  public NativeClickHandler(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public NativeClickHandler(Context paramContext, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    b = paramString;
  }
  
  private void a(View paramView, View.OnClickListener paramOnClickListener)
  {
    paramView.setOnClickListener(paramOnClickListener);
    int i = paramView instanceof ViewGroup;
    if (i != 0)
    {
      paramView = (ViewGroup)paramView;
      i = 0;
      for (;;)
      {
        int k = paramView.getChildCount();
        if (i >= k) {
          break;
        }
        View localView = paramView.getChildAt(i);
        a(localView, paramOnClickListener);
        int j;
        i += 1;
      }
    }
  }
  
  public void clearOnClickListener(View paramView)
  {
    String str = "Cannot clear click listener from a null view";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramView, str);
    if (!bool) {
      return;
    }
    a(paramView, null);
  }
  
  public void openClickDestinationUrl(String paramString, View paramView)
  {
    Object localObject1 = new com/mopub/nativeads/j;
    Object localObject2 = a;
    ((j)localObject1).<init>((Context)localObject2);
    localObject2 = "Cannot open a null click destination url";
    boolean bool1 = Preconditions.NoThrow.checkNotNull(paramString, (String)localObject2);
    if (bool1)
    {
      Preconditions.checkNotNull(localObject1);
      bool1 = c;
      if (!bool1)
      {
        bool1 = true;
        c = bool1;
        if (paramView != null) {
          ((j)localObject1).a(paramView);
        }
        Object localObject3 = new com/mopub/common/UrlHandler$Builder;
        ((UrlHandler.Builder)localObject3).<init>();
        Object localObject4 = b;
        boolean bool2 = TextUtils.isEmpty((CharSequence)localObject4);
        if (!bool2)
        {
          localObject4 = b;
          ((UrlHandler.Builder)localObject3).withDspCreativeId((String)localObject4);
        }
        localObject4 = UrlAction.IGNORE_ABOUT_SCHEME;
        int j = 6;
        UrlAction[] arrayOfUrlAction = new UrlAction[j];
        UrlAction localUrlAction1 = UrlAction.OPEN_NATIVE_BROWSER;
        arrayOfUrlAction[0] = localUrlAction1;
        UrlAction localUrlAction2 = UrlAction.OPEN_APP_MARKET;
        arrayOfUrlAction[bool1] = localUrlAction2;
        localUrlAction2 = UrlAction.OPEN_IN_APP_BROWSER;
        arrayOfUrlAction[2] = localUrlAction2;
        localUrlAction2 = UrlAction.HANDLE_SHARE_TWEET;
        arrayOfUrlAction[3] = localUrlAction2;
        localUrlAction2 = UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK;
        arrayOfUrlAction[4] = localUrlAction2;
        int i = 5;
        localUrlAction2 = UrlAction.FOLLOW_DEEP_LINK;
        arrayOfUrlAction[i] = localUrlAction2;
        localObject2 = ((UrlHandler.Builder)localObject3).withSupportedUrlActions((UrlAction)localObject4, arrayOfUrlAction);
        localObject3 = new com/mopub/nativeads/NativeClickHandler$2;
        ((NativeClickHandler.2)localObject3).<init>(this, paramView, (j)localObject1);
        paramView = ((UrlHandler.Builder)localObject2).withResultActions((UrlHandler.ResultActions)localObject3).build();
        localObject1 = a;
        paramView.handleUrl((Context)localObject1, paramString);
      }
    }
  }
  
  public void setOnClickListener(View paramView, ClickInterface paramClickInterface)
  {
    Object localObject = "Cannot set click listener on a null view";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramView, (String)localObject);
    if (!bool) {
      return;
    }
    localObject = "Cannot set click listener with a null ClickInterface";
    bool = Preconditions.NoThrow.checkNotNull(paramClickInterface, (String)localObject);
    if (!bool) {
      return;
    }
    localObject = new com/mopub/nativeads/NativeClickHandler$1;
    ((NativeClickHandler.1)localObject).<init>(this, paramClickInterface);
    a(paramView, (View.OnClickListener)localObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeClickHandler
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */