package com.mopub.nativeads;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.extractor.mp4.Mp4Extractor;

final class NativeVideoController$2
  implements ExtractorsFactory
{
  NativeVideoController$2(NativeVideoController paramNativeVideoController) {}
  
  public final Extractor[] createExtractors()
  {
    Extractor[] arrayOfExtractor = new Extractor[1];
    Mp4Extractor localMp4Extractor = new com/google/android/exoplayer2/extractor/mp4/Mp4Extractor;
    localMp4Extractor.<init>();
    arrayOfExtractor[0] = localMp4Extractor;
    return arrayOfExtractor;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeVideoController.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */