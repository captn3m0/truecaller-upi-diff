package com.mopub.nativeads;

import android.os.Handler;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.MoPubNetworkError.Reason;
import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.VolleyError;

final class i$3
  implements Response.ErrorListener
{
  i$3(i parami) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    boolean bool1 = paramVolleyError instanceof MoPubNetworkError;
    Object localObject1;
    if (bool1)
    {
      localObject1 = paramVolleyError;
      localObject1 = ((MoPubNetworkError)paramVolleyError).getReason();
      localObject2 = MoPubNetworkError.Reason.WARMING_UP;
      bool1 = ((MoPubNetworkError.Reason)localObject1).equals(localObject2);
      if (!bool1) {}
    }
    else
    {
      localObject1 = "Failed to load positioning data";
      MoPubLog.e((String)localObject1, (Throwable)paramVolleyError);
      paramVolleyError = networkResponse;
      if (paramVolleyError == null)
      {
        paramVolleyError = a.b;
        boolean bool2 = DeviceUtils.isNetworkAvailable(paramVolleyError);
        if (!bool2)
        {
          paramVolleyError = String.valueOf(MoPubErrorCode.NO_CONNECTION.toString());
          MoPubLog.c(paramVolleyError);
        }
      }
    }
    paramVolleyError = a;
    int j = f + 1;
    double d1 = j;
    double d2 = Math.pow(2.0D, d1);
    long l = 4652007308841189376L;
    d1 = 1000.0D;
    d2 *= d1;
    int i = (int)d2;
    int k = a;
    if (i >= k)
    {
      MoPubLog.d("Error downloading positioning information");
      localObject1 = e;
      if (localObject1 != null)
      {
        localObject1 = e;
        ((PositioningSource.PositioningListener)localObject1).onFailed();
      }
      e = null;
      return;
    }
    k = f + 1;
    f = k;
    Object localObject2 = c;
    paramVolleyError = d;
    l = i;
    ((Handler)localObject2).postDelayed(paramVolleyError, l);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.i.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */