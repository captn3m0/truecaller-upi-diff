package com.mopub.nativeads;

public abstract interface MoPubNative$MoPubNativeNetworkListener
{
  public abstract void onNativeFail(NativeErrorCode paramNativeErrorCode);
  
  public abstract void onNativeLoad(NativeAd paramNativeAd);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubNative.MoPubNativeNetworkListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */