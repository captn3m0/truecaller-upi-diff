package com.mopub.nativeads;

import android.location.Location;
import com.mopub.common.MoPub;
import java.util.EnumSet;

public final class RequestParameters$Builder
{
  private String a;
  private String b;
  private Location c;
  private EnumSet d;
  
  public final RequestParameters build()
  {
    RequestParameters localRequestParameters = new com/mopub/nativeads/RequestParameters;
    localRequestParameters.<init>(this, (byte)0);
    return localRequestParameters;
  }
  
  public final Builder desiredAssets(EnumSet paramEnumSet)
  {
    paramEnumSet = EnumSet.copyOf(paramEnumSet);
    d = paramEnumSet;
    return this;
  }
  
  public final Builder keywords(String paramString)
  {
    a = paramString;
    return this;
  }
  
  public final Builder location(Location paramLocation)
  {
    boolean bool = MoPub.canCollectPersonalInformation();
    if (!bool) {
      paramLocation = null;
    }
    c = paramLocation;
    return this;
  }
  
  public final Builder userDataKeywords(String paramString)
  {
    boolean bool = MoPub.canCollectPersonalInformation();
    if (!bool) {
      paramString = null;
    }
    b = paramString;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.RequestParameters.Builder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */