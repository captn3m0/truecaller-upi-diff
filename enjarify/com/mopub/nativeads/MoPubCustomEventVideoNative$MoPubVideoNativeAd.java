package com.mopub.nativeads;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.OnClickListener;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibilityTracker;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.VastManager;
import com.mopub.mobileads.VastManager.VastManagerListener;
import com.mopub.mobileads.VastTracker;
import com.mopub.mobileads.VastVideoConfig;
import com.mopub.mobileads.VideoViewabilityTracker;
import com.mopub.network.TrackingRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class MoPubCustomEventVideoNative$MoPubVideoNativeAd
  extends VideoNativeAd
  implements AudioManager.OnAudioFocusChangeListener, VastManager.VastManagerListener, NativeVideoController.NativeVideoProgressRunnable.ProgressListener
{
  final Context c;
  final JSONObject d;
  VastVideoConfig e;
  private MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState f;
  private final VisibilityTracker g;
  private final String h;
  private final CustomEventNative.CustomEventNativeListener i;
  private final MoPubCustomEventVideoNative.d j;
  private final MoPubCustomEventVideoNative.b k;
  private NativeVideoController l;
  private final VastManager m;
  private MediaLayout n;
  private View o;
  private final long p;
  private boolean q;
  private boolean r;
  private boolean s = false;
  private boolean t = false;
  private int u;
  private boolean v;
  private boolean w;
  private boolean x;
  private boolean y;
  
  private MoPubCustomEventVideoNative$MoPubVideoNativeAd(Context paramContext, JSONObject paramJSONObject, CustomEventNative.CustomEventNativeListener paramCustomEventNativeListener, MoPubCustomEventVideoNative.d paramd, VisibilityTracker paramVisibilityTracker, MoPubCustomEventVideoNative.b paramb, String paramString, VastManager paramVastManager)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramJSONObject);
    Preconditions.checkNotNull(paramCustomEventNativeListener);
    Preconditions.checkNotNull(paramd);
    Preconditions.checkNotNull(paramVisibilityTracker);
    Preconditions.checkNotNull(paramb);
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramVastManager);
    paramContext = paramContext.getApplicationContext();
    c = paramContext;
    d = paramJSONObject;
    i = paramCustomEventNativeListener;
    j = paramd;
    k = paramb;
    h = paramString;
    long l1 = Utils.generateUniqueId();
    p = l1;
    int i1 = 1;
    q = i1;
    paramJSONObject = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.CREATED;
    f = paramJSONObject;
    r = i1;
    u = i1;
    x = i1;
    g = paramVisibilityTracker;
    paramContext = g;
    paramJSONObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$1;
    paramJSONObject.<init>(this);
    paramContext.setVisibilityTrackerListener(paramJSONObject);
    m = paramVastManager;
  }
  
  public MoPubCustomEventVideoNative$MoPubVideoNativeAd(Context paramContext, JSONObject paramJSONObject, CustomEventNative.CustomEventNativeListener paramCustomEventNativeListener, MoPubCustomEventVideoNative.d paramd, String paramString)
  {
    this(paramContext, paramJSONObject, paramCustomEventNativeListener, paramd, localVisibilityTracker, localb, paramString, localVastManager);
  }
  
  private void a(MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState paramVideoState)
  {
    boolean bool1 = t;
    if (bool1)
    {
      Object localObject = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.PLAYING;
      if (paramVideoState != localObject)
      {
        localObject = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.PLAYING_MUTED;
        if (paramVideoState != localObject)
        {
          paramVideoState = e.getResumeTrackers();
          long l1 = l.getCurrentPosition();
          int i1 = (int)l1;
          localObject = Integer.valueOf(i1);
          Context localContext = c;
          TrackingRequest.makeVastTrackingHttpRequest(paramVideoState, null, (Integer)localObject, null, localContext);
          t = false;
        }
      }
    }
    s = true;
    boolean bool2 = q;
    if (bool2)
    {
      q = false;
      paramVideoState = l;
      long l2 = paramVideoState.getCurrentPosition();
      paramVideoState.seekTo(l2);
    }
  }
  
  static boolean a(JSONObject paramJSONObject)
  {
    Preconditions.checkNotNull(paramJSONObject);
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    paramJSONObject = paramJSONObject.keys();
    for (;;)
    {
      boolean bool = paramJSONObject.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = paramJSONObject.next();
      localHashSet.add(localObject);
    }
    paramJSONObject = MoPubCustomEventVideoNative.MoPubVideoNativeAd.a.c;
    return localHashSet.containsAll(paramJSONObject);
  }
  
  private void d()
  {
    Object localObject = n;
    if (localObject != null)
    {
      MediaLayout.Mode localMode = MediaLayout.Mode.IMAGE;
      ((MediaLayout)localObject).setMode(localMode);
      localObject = n;
      localMode = null;
      ((MediaLayout)localObject).setSurfaceTextureListener(null);
      n.setPlayButtonClickListener(null);
      n.setMuteControlClickListener(null);
      n.setOnClickListener(null);
      localObject = g;
      MediaLayout localMediaLayout = n;
      ((VisibilityTracker)localObject).removeView(localMediaLayout);
      n = null;
    }
  }
  
  private void e()
  {
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState localVideoState = f;
    boolean bool1 = v;
    if (bool1)
    {
      localVideoState = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.FAILED_LOAD;
    }
    else
    {
      bool1 = y;
      if (bool1)
      {
        localVideoState = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.ENDED;
      }
      else
      {
        int i1 = u;
        int i2 = 1;
        if (i1 == i2)
        {
          localVideoState = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.LOADING;
        }
        else
        {
          int i3 = 2;
          if (i1 == i3)
          {
            localVideoState = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.BUFFERING;
          }
          else
          {
            i3 = 4;
            if (i1 == i3)
            {
              y = i2;
              localVideoState = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.ENDED;
            }
            else
            {
              i2 = 3;
              if (i1 == i2)
              {
                boolean bool2 = w;
                if (bool2)
                {
                  bool2 = x;
                  if (bool2) {
                    localVideoState = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.PLAYING_MUTED;
                  } else {
                    localVideoState = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.PLAYING;
                  }
                }
                else
                {
                  localVideoState = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.PAUSED;
                }
              }
            }
          }
        }
      }
    }
    a(localVideoState, false);
  }
  
  final void a(MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState paramVideoState, boolean paramBoolean)
  {
    Preconditions.checkNotNull(paramVideoState);
    Object localObject1 = e;
    if (localObject1 != null)
    {
      localObject1 = l;
      if (localObject1 != null)
      {
        localObject1 = n;
        if (localObject1 != null)
        {
          localObject1 = f;
          if (localObject1 == paramVideoState) {
            return;
          }
          f = paramVideoState;
          int[] arrayOfInt = MoPubCustomEventVideoNative.1.b;
          int i1 = paramVideoState.ordinal();
          i1 = arrayOfInt[i1];
          arrayOfInt = null;
          boolean bool2 = true;
          boolean bool1;
          Object localObject2;
          switch (i1)
          {
          default: 
            break;
          case 8: 
            paramVideoState = l;
            bool1 = paramVideoState.hasFinalFrame();
            if (bool1)
            {
              paramVideoState = n;
              localObject2 = l.getFinalFrame();
              paramVideoState.setMainImageDrawable((Drawable)localObject2);
            }
            s = false;
            t = false;
            paramVideoState = e;
            localObject2 = c;
            paramVideoState.handleComplete((Context)localObject2, 0);
            l.setAppAudioEnabled(false);
            paramVideoState = n;
            localObject2 = MediaLayout.Mode.FINISHED;
            paramVideoState.setMode((MediaLayout.Mode)localObject2);
            paramVideoState = n;
            paramBoolean = true;
            paramVideoState.updateProgress(paramBoolean);
            break;
          case 7: 
            a((MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState)localObject1);
            l.setPlayWhenReady(bool2);
            l.setAudioEnabled(false);
            l.setAppAudioEnabled(false);
            paramVideoState = n;
            localObject2 = MediaLayout.Mode.PLAYING;
            paramVideoState.setMode((MediaLayout.Mode)localObject2);
            paramVideoState = n;
            localObject2 = MediaLayout.MuteState.MUTED;
            paramVideoState.setMuteState((MediaLayout.MuteState)localObject2);
            return;
          case 6: 
            a((MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState)localObject1);
            l.setPlayWhenReady(bool2);
            l.setAudioEnabled(bool2);
            l.setAppAudioEnabled(bool2);
            paramVideoState = n;
            localObject2 = MediaLayout.Mode.PLAYING;
            paramVideoState.setMode((MediaLayout.Mode)localObject2);
            paramVideoState = n;
            localObject2 = MediaLayout.MuteState.UNMUTED;
            paramVideoState.setMuteState((MediaLayout.MuteState)localObject2);
            return;
          case 5: 
            if (paramBoolean) {
              t = false;
            }
            if (!paramBoolean)
            {
              paramVideoState = l;
              paramVideoState.setAppAudioEnabled(false);
              bool1 = s;
              if (bool1)
              {
                paramVideoState = e.getPauseTrackers();
                long l1 = l.getCurrentPosition();
                paramBoolean = (int)l1;
                localObject2 = Integer.valueOf(paramBoolean);
                localObject1 = c;
                TrackingRequest.makeVastTrackingHttpRequest(paramVideoState, null, (Integer)localObject2, null, (Context)localObject1);
                s = false;
                t = bool2;
              }
            }
            l.setPlayWhenReady(false);
            paramVideoState = n;
            localObject2 = MediaLayout.Mode.PAUSED;
            paramVideoState.setMode((MediaLayout.Mode)localObject2);
            return;
          case 4: 
            l.setPlayWhenReady(bool2);
            paramVideoState = n;
            localObject2 = MediaLayout.Mode.BUFFERING;
            paramVideoState.setMode((MediaLayout.Mode)localObject2);
            return;
          case 2: 
          case 3: 
            l.setPlayWhenReady(bool2);
            paramVideoState = n;
            localObject2 = MediaLayout.Mode.LOADING;
            paramVideoState.setMode((MediaLayout.Mode)localObject2);
            return;
          case 1: 
            paramVideoState = e;
            localObject2 = c;
            paramVideoState.handleError((Context)localObject2, null, 0);
            l.setAppAudioEnabled(false);
            paramVideoState = n;
            localObject2 = MediaLayout.Mode.IMAGE;
            paramVideoState.setMode((MediaLayout.Mode)localObject2);
            return;
          }
          return;
        }
      }
    }
  }
  
  final void a(MoPubCustomEventVideoNative.MoPubVideoNativeAd.a parama, Object paramObject)
  {
    Preconditions.checkNotNull(parama);
    Preconditions.checkNotNull(paramObject);
    try
    {
      Object localObject = MoPubCustomEventVideoNative.1.a;
      int i1 = parama.ordinal();
      int i2 = localObject[i1];
      switch (i2)
      {
      default: 
        paramObject = new java/lang/StringBuilder;
        break;
      case 11: 
        paramObject = (String)paramObject;
        setPrivacyInformationIconClickThroughUrl((String)paramObject);
        return;
      case 10: 
        paramObject = (String)paramObject;
        setPrivacyInformationIconImageUrl((String)paramObject);
        return;
      case 9: 
        paramObject = (String)paramObject;
        setVastVideo((String)paramObject);
        return;
      case 8: 
        paramObject = (String)paramObject;
        setCallToAction((String)paramObject);
        return;
      case 7: 
        bool = paramObject instanceof JSONArray;
        if (bool)
        {
          b(paramObject);
          return;
        }
        paramObject = (String)paramObject;
        addClickTracker((String)paramObject);
        return;
      case 6: 
        paramObject = (String)paramObject;
        setClickDestinationUrl((String)paramObject);
        return;
      case 5: 
        paramObject = (String)paramObject;
        setIconImageUrl((String)paramObject);
        return;
      case 4: 
        paramObject = (String)paramObject;
        setMainImageUrl((String)paramObject);
        return;
      case 3: 
        paramObject = (String)paramObject;
        setText((String)paramObject);
        return;
      case 2: 
        paramObject = (String)paramObject;
        setTitle((String)paramObject);
        return;
      case 1: 
        a(paramObject);
        return;
      }
      localObject = "Unable to add JSON key to internal mapping: ";
      ((StringBuilder)paramObject).<init>((String)localObject);
      localObject = a;
      ((StringBuilder)paramObject).append((String)localObject);
      paramObject = ((StringBuilder)paramObject).toString();
      MoPubLog.d((String)paramObject);
      return;
    }
    catch (ClassCastException paramObject)
    {
      boolean bool = b;
      if (!bool)
      {
        paramObject = new java/lang/StringBuilder;
        ((StringBuilder)paramObject).<init>("Ignoring class cast exception for optional key: ");
        parama = a;
        ((StringBuilder)paramObject).append(parama);
        MoPubLog.d(((StringBuilder)paramObject).toString());
        return;
      }
      throw ((Throwable)paramObject);
    }
  }
  
  final List c()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int i1 = getExtras().size();
    localArrayList.<init>(i1);
    Iterator localIterator = getExtras().entrySet().iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Map.Entry)localIterator.next();
      Object localObject2 = (String)((Map.Entry)localObject1).getKey();
      if (localObject2 != null)
      {
        Object localObject3 = Locale.US;
        localObject2 = ((String)localObject2).toLowerCase((Locale)localObject3);
        localObject3 = "image";
        bool2 = ((String)localObject2).endsWith((String)localObject3);
        if (bool2)
        {
          bool2 = true;
          break label120;
        }
      }
      boolean bool2 = false;
      localObject2 = null;
      label120:
      if (bool2)
      {
        localObject2 = ((Map.Entry)localObject1).getValue();
        bool2 = localObject2 instanceof String;
        if (bool2)
        {
          localObject1 = (String)((Map.Entry)localObject1).getValue();
          localArrayList.add(localObject1);
        }
      }
    }
    return localArrayList;
  }
  
  public void clear(View paramView)
  {
    Preconditions.checkNotNull(paramView);
    l.clear();
    d();
  }
  
  public void destroy()
  {
    invalidate();
    d();
    l.setPlayWhenReady(false);
    l.release(this);
    NativeVideoController.remove(p);
    g.destroy();
  }
  
  public void onAudioFocusChange(int paramInt)
  {
    int i1 = 1;
    float f1 = Float.MIN_VALUE;
    int i2 = -1;
    if (paramInt != i2)
    {
      i2 = -2;
      if (paramInt != i2)
      {
        i2 = -3;
        if (paramInt == i2)
        {
          l.setAudioVolume(0.3F);
          return;
        }
        if (paramInt == i1)
        {
          NativeVideoController localNativeVideoController = l;
          i1 = 1065353216;
          f1 = 1.0F;
          localNativeVideoController.setAudioVolume(f1);
          e();
        }
        return;
      }
    }
    x = i1;
    e();
  }
  
  public void onError(Exception paramException)
  {
    MoPubLog.w("Error playing back video.", paramException);
    v = true;
    e();
  }
  
  public void onStateChanged(boolean paramBoolean, int paramInt)
  {
    u = paramInt;
    e();
  }
  
  public void onVastVideoConfigurationPrepared(VastVideoConfig paramVastVideoConfig)
  {
    if (paramVastVideoConfig == null)
    {
      paramVastVideoConfig = i;
      localObject1 = NativeErrorCode.INVALID_RESPONSE;
      paramVastVideoConfig.onNativeAdFailed((NativeErrorCode)localObject1);
      return;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = new com/mopub/nativeads/NativeVideoController$b;
    ((NativeVideoController.b)localObject1).<init>();
    Object localObject2 = new com/mopub/nativeads/MoPubCustomEventVideoNative$a;
    ((MoPubCustomEventVideoNative.a)localObject2).<init>(this);
    a = ((NativeVideoController.b.a)localObject2);
    int i1 = j.d;
    b = i1;
    i1 = j.e;
    c = i1;
    localArrayList.add(localObject1);
    localObject2 = j.f;
    f = ((Integer)localObject2);
    localObject1 = paramVastVideoConfig.getImpressionTrackers().iterator();
    Object localObject3;
    Object localObject4;
    int i2;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (VastTracker)((Iterator)localObject1).next();
      localObject3 = new com/mopub/nativeads/NativeVideoController$b;
      ((NativeVideoController.b)localObject3).<init>();
      localObject4 = new com/mopub/nativeads/MoPubCustomEventVideoNative$c;
      localContext = c;
      localObject2 = ((VastTracker)localObject2).getContent();
      ((MoPubCustomEventVideoNative.c)localObject4).<init>(localContext, (String)localObject2);
      a = ((NativeVideoController.b.a)localObject4);
      i2 = j.d;
      b = i2;
      i2 = j.e;
      c = i2;
      localArrayList.add(localObject3);
      localObject2 = j.f;
      f = ((Integer)localObject2);
    }
    e = paramVastVideoConfig;
    paramVastVideoConfig = e.getVideoViewabilityTracker();
    if (paramVastVideoConfig != null)
    {
      localObject1 = new com/mopub/nativeads/NativeVideoController$b;
      ((NativeVideoController.b)localObject1).<init>();
      localObject2 = new com/mopub/nativeads/MoPubCustomEventVideoNative$c;
      localObject3 = c;
      localObject4 = paramVastVideoConfig.getContent();
      ((MoPubCustomEventVideoNative.c)localObject2).<init>((Context)localObject3, (String)localObject4);
      a = ((NativeVideoController.b.a)localObject2);
      i2 = paramVastVideoConfig.getPercentViewable();
      b = i2;
      int i3 = paramVastVideoConfig.getViewablePlaytimeMS();
      c = i3;
      localArrayList.add(localObject1);
    }
    paramVastVideoConfig = e;
    localObject1 = getPrivacyInformationIconImageUrl();
    paramVastVideoConfig.setPrivacyInformationIconImageUrl((String)localObject1);
    paramVastVideoConfig = e;
    localObject1 = getPrivacyInformationIconClickThroughUrl();
    paramVastVideoConfig.setPrivacyInformationIconClickthroughUrl((String)localObject1);
    paramVastVideoConfig = new java/util/HashSet;
    paramVastVideoConfig.<init>();
    localObject1 = h;
    paramVastVideoConfig.add(localObject1);
    localObject1 = b();
    paramVastVideoConfig.addAll((Collection)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    paramVastVideoConfig = paramVastVideoConfig.iterator();
    for (;;)
    {
      boolean bool2 = paramVastVideoConfig.hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (String)paramVastVideoConfig.next();
      localObject3 = new com/mopub/mobileads/VastTracker;
      localObject4 = null;
      ((VastTracker)localObject3).<init>((String)localObject2, false);
      ((ArrayList)localObject1).add(localObject3);
    }
    e.addClickTrackers((List)localObject1);
    paramVastVideoConfig = e;
    localObject1 = getClickDestinationUrl();
    paramVastVideoConfig.setClickThroughUrl((String)localObject1);
    localObject2 = k;
    long l1 = p;
    Context localContext = c;
    VastVideoConfig localVastVideoConfig = e;
    paramVastVideoConfig = ((MoPubCustomEventVideoNative.b)localObject2).createForId(l1, localContext, localArrayList, localVastVideoConfig);
    l = paramVastVideoConfig;
    i.onNativeAdLoaded(this);
    paramVastVideoConfig = j.g;
    if (paramVastVideoConfig != null)
    {
      localObject1 = e;
      ((VastVideoConfig)localObject1).addVideoTrackers(paramVastVideoConfig);
    }
  }
  
  public void prepare(View paramView)
  {
    Preconditions.checkNotNull(paramView);
    o = paramView;
    paramView = o;
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.7 local7 = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$7;
    local7.<init>(this);
    paramView.setOnClickListener(local7);
  }
  
  public void render(MediaLayout paramMediaLayout)
  {
    Preconditions.checkNotNull(paramMediaLayout);
    Object localObject = g;
    View localView = o;
    int i1 = j.b;
    int i2 = j.c;
    Integer localInteger = j.f;
    ((VisibilityTracker)localObject).addView(localView, paramMediaLayout, i1, i2, localInteger);
    n = paramMediaLayout;
    n.initForVideo();
    paramMediaLayout = n;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$3;
    ((MoPubCustomEventVideoNative.MoPubVideoNativeAd.3)localObject).<init>(this);
    paramMediaLayout.setSurfaceTextureListener((TextureView.SurfaceTextureListener)localObject);
    paramMediaLayout = n;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$4;
    ((MoPubCustomEventVideoNative.MoPubVideoNativeAd.4)localObject).<init>(this);
    paramMediaLayout.setPlayButtonClickListener((View.OnClickListener)localObject);
    paramMediaLayout = n;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$5;
    ((MoPubCustomEventVideoNative.MoPubVideoNativeAd.5)localObject).<init>(this);
    paramMediaLayout.setMuteControlClickListener((View.OnClickListener)localObject);
    paramMediaLayout = n;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$6;
    ((MoPubCustomEventVideoNative.MoPubVideoNativeAd.6)localObject).<init>(this);
    paramMediaLayout.setOnClickListener((View.OnClickListener)localObject);
    paramMediaLayout = l;
    int i3 = paramMediaLayout.getPlaybackState();
    int i4 = 5;
    if (i3 == i4)
    {
      paramMediaLayout = l;
      paramMediaLayout.prepare(this);
    }
    paramMediaLayout = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.PAUSED;
    a(paramMediaLayout, false);
  }
  
  public void updateProgress(int paramInt)
  {
    n.updateProgress(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.MoPubVideoNativeAd
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */