package com.mopub.nativeads;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;

final class MoPubRecyclerAdapter$3
  extends RecyclerView.AdapterDataObserver
{
  MoPubRecyclerAdapter$3(MoPubRecyclerAdapter paramMoPubRecyclerAdapter) {}
  
  public final void onChanged()
  {
    MoPubStreamAdPlacer localMoPubStreamAdPlacer = MoPubRecyclerAdapter.b(a);
    int i = MoPubRecyclerAdapter.a(a).getItemCount();
    localMoPubStreamAdPlacer.setItemCount(i);
    a.notifyDataSetChanged();
  }
  
  public final void onItemRangeChanged(int paramInt1, int paramInt2)
  {
    MoPubStreamAdPlacer localMoPubStreamAdPlacer = MoPubRecyclerAdapter.b(a);
    paramInt2 = paramInt2 + paramInt1 + -1;
    paramInt2 = localMoPubStreamAdPlacer.getAdjustedPosition(paramInt2);
    paramInt1 = MoPubRecyclerAdapter.b(a).getAdjustedPosition(paramInt1);
    paramInt2 = paramInt2 - paramInt1 + 1;
    a.notifyItemRangeChanged(paramInt1, paramInt2);
  }
  
  public final void onItemRangeInserted(int paramInt1, int paramInt2)
  {
    MoPubStreamAdPlacer localMoPubStreamAdPlacer = MoPubRecyclerAdapter.b(a);
    int i = localMoPubStreamAdPlacer.getAdjustedPosition(paramInt1);
    Object localObject1 = MoPubRecyclerAdapter.a(a);
    int j = ((RecyclerView.Adapter)localObject1).getItemCount();
    Object localObject2 = MoPubRecyclerAdapter.b(a);
    ((MoPubStreamAdPlacer)localObject2).setItemCount(j);
    int k = paramInt1 + paramInt2;
    int m = 0;
    if (k >= j)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject1 = null;
    }
    localObject2 = MoPubRecyclerAdapter.ContentChangeStrategy.KEEP_ADS_FIXED;
    MoPubRecyclerAdapter.ContentChangeStrategy localContentChangeStrategy = MoPubRecyclerAdapter.c(a);
    if (localObject2 != localContentChangeStrategy)
    {
      localObject2 = MoPubRecyclerAdapter.ContentChangeStrategy.INSERT_AT_END;
      localContentChangeStrategy = MoPubRecyclerAdapter.c(a);
      if ((localObject2 != localContentChangeStrategy) || (j == 0))
      {
        while (m < paramInt2)
        {
          localObject1 = MoPubRecyclerAdapter.b(a);
          ((MoPubStreamAdPlacer)localObject1).insertItem(paramInt1);
          m += 1;
        }
        a.notifyItemRangeInserted(i, paramInt2);
        return;
      }
    }
    a.notifyDataSetChanged();
  }
  
  public final void onItemRangeMoved(int paramInt1, int paramInt2, int paramInt3)
  {
    a.notifyDataSetChanged();
  }
  
  public final void onItemRangeRemoved(int paramInt1, int paramInt2)
  {
    MoPubStreamAdPlacer localMoPubStreamAdPlacer1 = MoPubRecyclerAdapter.b(a);
    int i = localMoPubStreamAdPlacer1.getAdjustedPosition(paramInt1);
    RecyclerView.Adapter localAdapter = MoPubRecyclerAdapter.a(a);
    int j = localAdapter.getItemCount();
    MoPubStreamAdPlacer localMoPubStreamAdPlacer2 = MoPubRecyclerAdapter.b(a);
    localMoPubStreamAdPlacer2.setItemCount(j);
    int k = paramInt1 + paramInt2;
    int m = 0;
    if (k >= j)
    {
      k = 1;
    }
    else
    {
      k = 0;
      localMoPubStreamAdPlacer2 = null;
    }
    Object localObject = MoPubRecyclerAdapter.ContentChangeStrategy.KEEP_ADS_FIXED;
    MoPubRecyclerAdapter.ContentChangeStrategy localContentChangeStrategy = MoPubRecyclerAdapter.c(a);
    if (localObject != localContentChangeStrategy)
    {
      localObject = MoPubRecyclerAdapter.ContentChangeStrategy.INSERT_AT_END;
      localContentChangeStrategy = MoPubRecyclerAdapter.c(a);
      if ((localObject != localContentChangeStrategy) || (k == 0))
      {
        localMoPubStreamAdPlacer2 = MoPubRecyclerAdapter.b(a);
        int n = j + paramInt2;
        k = localMoPubStreamAdPlacer2.getAdjustedCount(n);
        while (m < paramInt2)
        {
          localObject = MoPubRecyclerAdapter.b(a);
          ((MoPubStreamAdPlacer)localObject).removeItem(paramInt1);
          m += 1;
        }
        paramInt1 = MoPubRecyclerAdapter.b(a).getAdjustedCount(j);
        k -= paramInt1;
        paramInt1 = k - paramInt2;
        i -= paramInt1;
        a.notifyItemRangeRemoved(i, k);
        return;
      }
    }
    a.notifyDataSetChanged();
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubRecyclerAdapter.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */