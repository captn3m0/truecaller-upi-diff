package com.mopub.nativeads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

public abstract interface MoPubAdRenderer
{
  public abstract View createAdView(Context paramContext, ViewGroup paramViewGroup);
  
  public abstract void renderAdView(View paramView, BaseNativeAd paramBaseNativeAd);
  
  public abstract boolean supports(BaseNativeAd paramBaseNativeAd);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubAdRenderer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */