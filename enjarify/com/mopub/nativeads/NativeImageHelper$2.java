package com.mopub.nativeads;

import android.widget.ImageView;
import com.mopub.common.logging.MoPubLog;
import com.mopub.volley.VolleyError;
import com.mopub.volley.toolbox.ImageLoader.ImageContainer;
import com.mopub.volley.toolbox.ImageLoader.ImageListener;

final class NativeImageHelper$2
  implements ImageLoader.ImageListener
{
  NativeImageHelper$2(ImageView paramImageView) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    MoPubLog.d("Failed to load image.", paramVolleyError);
    a.setImageDrawable(null);
  }
  
  public final void onResponse(ImageLoader.ImageContainer paramImageContainer, boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      localObject = "Image was not loaded immediately into your ad view. You should call preCacheImages as part of your custom event loading process.";
      MoPubLog.d((String)localObject);
    }
    Object localObject = a;
    paramImageContainer = paramImageContainer.getBitmap();
    ((ImageView)localObject).setImageBitmap(paramImageContainer);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeImageHelper.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */