package com.mopub.nativeads;

import android.content.Context;
import android.text.TextUtils;
import com.mopub.common.AdFormat;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.ManifestUtils;
import com.mopub.network.AdLoader;
import com.mopub.network.AdLoader.Listener;
import com.mopub.volley.Request;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.TreeMap;

public class MoPubNative
{
  static final MoPubNative.MoPubNativeNetworkListener a;
  final WeakReference b;
  MoPubNative.MoPubNativeNetworkListener c;
  AdRendererRegistry d;
  private final String e;
  private Map f;
  private AdLoader g;
  private b h;
  private final AdLoader.Listener i;
  private Request j;
  
  static
  {
    MoPubNative.1 local1 = new com/mopub/nativeads/MoPubNative$1;
    local1.<init>();
    a = local1;
  }
  
  public MoPubNative(Context paramContext, String paramString, AdRendererRegistry paramAdRendererRegistry, MoPubNative.MoPubNativeNetworkListener paramMoPubNativeNetworkListener)
  {
    Object localObject = new java/util/TreeMap;
    ((TreeMap)localObject).<init>();
    f = ((Map)localObject);
    Preconditions.checkNotNull(paramContext, "context may not be null.");
    Preconditions.checkNotNull(paramString, "AdUnitId may not be null.");
    Preconditions.checkNotNull(paramAdRendererRegistry, "AdRendererRegistry may not be null.");
    Preconditions.checkNotNull(paramMoPubNativeNetworkListener, "MoPubNativeNetworkListener may not be null.");
    ManifestUtils.checkNativeActivitiesDeclared(paramContext);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramContext);
    b = ((WeakReference)localObject);
    e = paramString;
    c = paramMoPubNativeNetworkListener;
    d = paramAdRendererRegistry;
    paramContext = new com/mopub/nativeads/MoPubNative$2;
    paramContext.<init>(this);
    i = paramContext;
  }
  
  public MoPubNative(Context paramContext, String paramString, MoPubNative.MoPubNativeNetworkListener paramMoPubNativeNetworkListener)
  {
    this(paramContext, paramString, localAdRendererRegistry, paramMoPubNativeNetworkListener);
  }
  
  final Context a()
  {
    Context localContext = (Context)b.get();
    if (localContext == null)
    {
      destroy();
      String str = "Weak reference to Context in MoPubNative became null. This instance of MoPubNative is destroyed and No more requests will be processed.";
      MoPubLog.d(str);
    }
    return localContext;
  }
  
  final void a(String paramString, NativeErrorCode paramNativeErrorCode)
  {
    Context localContext = a();
    if (localContext == null) {
      return;
    }
    Object localObject = g;
    boolean bool;
    if (localObject != null)
    {
      bool = ((AdLoader)localObject).hasMoreAds();
      if (bool) {}
    }
    else
    {
      bool = TextUtils.isEmpty(paramString);
      if (bool)
      {
        paramString = c;
        if (paramNativeErrorCode == null) {
          paramNativeErrorCode = NativeErrorCode.INVALID_REQUEST_URL;
        }
        paramString.onNativeFail(paramNativeErrorCode);
        return;
      }
      AdLoader localAdLoader = new com/mopub/network/AdLoader;
      AdFormat localAdFormat = AdFormat.NATIVE;
      String str = e;
      AdLoader.Listener localListener = i;
      localObject = localAdLoader;
      localAdLoader.<init>(paramString, localAdFormat, str, localContext, localListener);
      g = localAdLoader;
    }
    paramString = g.loadNextAd(paramNativeErrorCode);
    j = paramString;
  }
  
  public void destroy()
  {
    b.clear();
    Object localObject = j;
    if (localObject != null)
    {
      ((Request)localObject).cancel();
      j = null;
    }
    g = null;
    localObject = a;
    c = ((MoPubNative.MoPubNativeNetworkListener)localObject);
  }
  
  public void makeRequest()
  {
    makeRequest(null);
  }
  
  public void makeRequest(RequestParameters paramRequestParameters)
  {
    makeRequest(paramRequestParameters, null);
  }
  
  public void makeRequest(RequestParameters paramRequestParameters, Integer paramInteger)
  {
    Object localObject = a();
    if (localObject == null) {
      return;
    }
    boolean bool = DeviceUtils.isNetworkAvailable((Context)localObject);
    if (!bool)
    {
      paramRequestParameters = c;
      paramInteger = NativeErrorCode.CONNECTION_ERROR;
      paramRequestParameters.onNativeFail(paramInteger);
      return;
    }
    localObject = a();
    if (localObject != null)
    {
      f localf = new com/mopub/nativeads/f;
      localf.<init>((Context)localObject);
      localObject = e;
      localObject = localf.withAdUnitId((String)localObject);
      paramRequestParameters = ((f)localObject).a(paramRequestParameters);
      if (paramInteger != null)
      {
        k = paramInteger.intValue();
        paramInteger = String.valueOf(k);
        g = paramInteger;
      }
      paramInteger = "ads.mopub.com";
      paramRequestParameters = paramRequestParameters.generateUrlString(paramInteger);
      if (paramRequestParameters != null)
      {
        localObject = String.valueOf(paramRequestParameters);
        paramInteger = "MoPubNative Loading ad from: ".concat((String)localObject);
        MoPubLog.d(paramInteger);
      }
      int k = 0;
      paramInteger = null;
      a(paramRequestParameters, null);
    }
  }
  
  public void registerAdRenderer(MoPubAdRenderer paramMoPubAdRenderer)
  {
    d.registerAdRenderer(paramMoPubAdRenderer);
  }
  
  public void setLocalExtras(Map paramMap)
  {
    if (paramMap == null)
    {
      paramMap = new java/util/TreeMap;
      paramMap.<init>();
      f = paramMap;
      return;
    }
    TreeMap localTreeMap = new java/util/TreeMap;
    localTreeMap.<init>(paramMap);
    f = localTreeMap;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubNative
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */