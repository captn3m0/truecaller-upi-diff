package com.mopub.nativeads;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.view.Surface;
import android.view.TextureView;
import com.google.android.exoplayer2.DefaultLoadControl.Builder;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player.DefaultEventListener;
import com.google.android.exoplayer2.PlayerMessage;
import com.google.android.exoplayer2.Renderer;
import com.google.android.exoplayer2.audio.MediaCodecAudioRenderer;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;
import com.google.android.exoplayer2.source.ExtractorMediaSource.Factory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DataSource.Factory;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.video.MediaCodecVideoRenderer;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.VastVideoConfig;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NativeVideoController
  extends Player.DefaultEventListener
  implements AudioManager.OnAudioFocusChangeListener
{
  public static final long RESUME_FINISHED_THRESHOLD = 750L;
  public static final int STATE_BUFFERING = 2;
  public static final int STATE_CLEARED = 5;
  public static final int STATE_ENDED = 4;
  public static final int STATE_IDLE = 1;
  public static final int STATE_READY = 3;
  private static final Map a;
  private final Context b;
  private final Handler c;
  private final NativeVideoController.a d;
  private VastVideoConfig e;
  private NativeVideoController.NativeVideoProgressRunnable f;
  private AudioManager g;
  private NativeVideoController.Listener h;
  private AudioManager.OnAudioFocusChangeListener i;
  private Surface j;
  private TextureView k;
  private WeakReference l;
  private volatile ExoPlayer m;
  private BitmapDrawable n;
  private MediaCodecAudioRenderer o;
  private MediaCodecVideoRenderer p;
  private boolean q;
  private boolean r;
  private boolean s;
  private int t;
  private boolean u;
  
  static
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>(4);
    a = localHashMap;
  }
  
  private NativeVideoController(Context paramContext, VastVideoConfig paramVastVideoConfig, NativeVideoController.NativeVideoProgressRunnable paramNativeVideoProgressRunnable, NativeVideoController.a parama, AudioManager paramAudioManager)
  {
    int i1 = 1;
    t = i1;
    u = i1;
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramVastVideoConfig);
    Preconditions.checkNotNull(parama);
    Preconditions.checkNotNull(paramAudioManager);
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
    paramContext = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    paramContext.<init>(localLooper);
    c = paramContext;
    e = paramVastVideoConfig;
    f = paramNativeVideoProgressRunnable;
    d = parama;
    g = paramAudioManager;
  }
  
  private NativeVideoController(Context paramContext, List paramList, VastVideoConfig paramVastVideoConfig)
  {
    this(paramContext, paramVastVideoConfig, localNativeVideoProgressRunnable, locala, (AudioManager)localObject3);
  }
  
  private void a(float paramFloat)
  {
    Object localObject = m;
    MediaCodecAudioRenderer localMediaCodecAudioRenderer = o;
    if ((localObject != null) && (localMediaCodecAudioRenderer != null))
    {
      localObject = ((ExoPlayer)localObject).createMessage(localMediaCodecAudioRenderer);
      if (localObject == null)
      {
        MoPubLog.d("ExoPlayer.createMessage returned null.");
        return;
      }
      localObject = ((PlayerMessage)localObject).setType(2);
      Float localFloat = Float.valueOf(paramFloat);
      ((PlayerMessage)localObject).setPayload(localFloat).send();
      return;
    }
  }
  
  private void a(Surface paramSurface)
  {
    Object localObject = m;
    MediaCodecVideoRenderer localMediaCodecVideoRenderer = p;
    if ((localObject != null) && (localMediaCodecVideoRenderer != null))
    {
      localObject = ((ExoPlayer)localObject).createMessage(localMediaCodecVideoRenderer);
      if (localObject == null)
      {
        MoPubLog.d("ExoPlayer.createMessage returned null.");
        return;
      }
      ((PlayerMessage)localObject).setType(1).setPayload(paramSurface).send();
      return;
    }
  }
  
  private void b()
  {
    ExoPlayer localExoPlayer = m;
    if (localExoPlayer == null) {
      return;
    }
    a(null);
    m.stop();
    m.release();
    m = null;
    f.stop();
    f.c = null;
  }
  
  private void c()
  {
    ExoPlayer localExoPlayer = m;
    if (localExoPlayer == null) {
      return;
    }
    localExoPlayer = m;
    boolean bool = q;
    localExoPlayer.setPlayWhenReady(bool);
  }
  
  public static NativeVideoController createForId(long paramLong, Context paramContext, VastVideoConfig paramVastVideoConfig, NativeVideoController.NativeVideoProgressRunnable paramNativeVideoProgressRunnable, NativeVideoController.a parama, AudioManager paramAudioManager)
  {
    NativeVideoController localNativeVideoController = new com/mopub/nativeads/NativeVideoController;
    localNativeVideoController.<init>(paramContext, paramVastVideoConfig, paramNativeVideoProgressRunnable, parama, paramAudioManager);
    paramContext = a;
    Long localLong = Long.valueOf(paramLong);
    paramContext.put(localLong, localNativeVideoController);
    return localNativeVideoController;
  }
  
  public static NativeVideoController createForId(long paramLong, Context paramContext, List paramList, VastVideoConfig paramVastVideoConfig)
  {
    NativeVideoController localNativeVideoController = new com/mopub/nativeads/NativeVideoController;
    localNativeVideoController.<init>(paramContext, paramList, paramVastVideoConfig);
    paramContext = a;
    Long localLong = Long.valueOf(paramLong);
    paramContext.put(localLong, localNativeVideoController);
    return localNativeVideoController;
  }
  
  private void d()
  {
    boolean bool = r;
    int i1;
    float f1;
    if (bool)
    {
      i1 = 1065353216;
      f1 = 1.0F;
    }
    else
    {
      i1 = 0;
      f1 = 0.0F;
    }
    a(f1);
  }
  
  public static NativeVideoController getForId(long paramLong)
  {
    Map localMap = a;
    Long localLong = Long.valueOf(paramLong);
    return (NativeVideoController)localMap.get(localLong);
  }
  
  public static NativeVideoController remove(long paramLong)
  {
    Map localMap = a;
    Long localLong = Long.valueOf(paramLong);
    return (NativeVideoController)localMap.remove(localLong);
  }
  
  public static void setForId(long paramLong, NativeVideoController paramNativeVideoController)
  {
    Map localMap = a;
    Long localLong = Long.valueOf(paramLong);
    localMap.put(localLong, paramNativeVideoController);
  }
  
  final void a()
  {
    f.a(true);
  }
  
  public void clear()
  {
    setPlayWhenReady(false);
    j = null;
    b();
  }
  
  public long getCurrentPosition()
  {
    return f.f;
  }
  
  public long getDuration()
  {
    return f.g;
  }
  
  public Drawable getFinalFrame()
  {
    return n;
  }
  
  public int getPlaybackState()
  {
    ExoPlayer localExoPlayer = m;
    if (localExoPlayer == null) {
      return 5;
    }
    return m.getPlaybackState();
  }
  
  public void handleCtaClick(Context paramContext)
  {
    a();
    e.handleClickWithoutResult(paramContext, 0);
  }
  
  public boolean hasFinalFrame()
  {
    BitmapDrawable localBitmapDrawable = n;
    return localBitmapDrawable != null;
  }
  
  public void onAudioFocusChange(int paramInt)
  {
    AudioManager.OnAudioFocusChangeListener localOnAudioFocusChangeListener = i;
    if (localOnAudioFocusChangeListener == null) {
      return;
    }
    localOnAudioFocusChangeListener.onAudioFocusChange(paramInt);
  }
  
  public void onLoadingChanged(boolean paramBoolean) {}
  
  public void onPlaybackParametersChanged(PlaybackParameters paramPlaybackParameters) {}
  
  public void onPlayerError(ExoPlaybackException paramExoPlaybackException)
  {
    NativeVideoController.Listener localListener = h;
    if (localListener == null) {
      return;
    }
    localListener.onError(paramExoPlaybackException);
    f.h = true;
  }
  
  public void onPlayerStateChanged(boolean paramBoolean, int paramInt)
  {
    int i1 = 1;
    int i2 = 4;
    if (paramInt == i2)
    {
      Object localObject = n;
      if (localObject == null)
      {
        localObject = m;
        if (localObject != null)
        {
          localObject = j;
          if (localObject != null)
          {
            localObject = k;
            if (localObject != null)
            {
              localObject = new android/graphics/drawable/BitmapDrawable;
              Resources localResources = b.getResources();
              Bitmap localBitmap = k.getBitmap();
              ((BitmapDrawable)localObject).<init>(localResources, localBitmap);
              n = ((BitmapDrawable)localObject);
              localObject = f;
              h = i1;
              break label118;
            }
          }
        }
        MoPubLog.w("onPlayerStateChanged called afer view has been recycled.");
        return;
      }
    }
    label118:
    t = paramInt;
    i2 = 3;
    if (paramInt == i2)
    {
      i1 = 0;
      localListener = null;
      u = false;
    }
    else if (paramInt == i1)
    {
      u = i1;
    }
    NativeVideoController.Listener localListener = h;
    if (localListener != null) {
      localListener.onStateChanged(paramBoolean, paramInt);
    }
  }
  
  public void onTracksChanged(TrackGroupArray paramTrackGroupArray, TrackSelectionArray paramTrackSelectionArray) {}
  
  public void prepare(Object paramObject)
  {
    Preconditions.checkNotNull(paramObject);
    Object localObject1 = new java/lang/ref/WeakReference;
    ((WeakReference)localObject1).<init>(paramObject);
    l = ((WeakReference)localObject1);
    b();
    paramObject = m;
    if (paramObject == null)
    {
      paramObject = new com/google/android/exoplayer2/video/MediaCodecVideoRenderer;
      Object localObject2 = b;
      Object localObject3 = MediaCodecSelector.DEFAULT;
      long l1 = 0L;
      Handler localHandler = c;
      int i1 = 10;
      localObject1 = paramObject;
      ((MediaCodecVideoRenderer)paramObject).<init>((Context)localObject2, (MediaCodecSelector)localObject3, l1, localHandler, null, i1);
      p = ((MediaCodecVideoRenderer)paramObject);
      paramObject = new com/google/android/exoplayer2/audio/MediaCodecAudioRenderer;
      localObject1 = b;
      localObject2 = MediaCodecSelector.DEFAULT;
      ((MediaCodecAudioRenderer)paramObject).<init>((Context)localObject1, (MediaCodecSelector)localObject2);
      o = ((MediaCodecAudioRenderer)paramObject);
      paramObject = new com/google/android/exoplayer2/upstream/DefaultAllocator;
      int i2 = 65536;
      boolean bool = true;
      ((DefaultAllocator)paramObject).<init>(bool, i2, 32);
      localObject1 = new com/google/android/exoplayer2/DefaultLoadControl$Builder;
      ((DefaultLoadControl.Builder)localObject1).<init>();
      ((DefaultLoadControl.Builder)localObject1).setAllocator((DefaultAllocator)paramObject);
      paramObject = d;
      int i3 = 2;
      localObject2 = new Renderer[i3];
      MediaCodecVideoRenderer localMediaCodecVideoRenderer = p;
      localObject2[0] = localMediaCodecVideoRenderer;
      MediaCodecAudioRenderer localMediaCodecAudioRenderer = o;
      localObject2[bool] = localMediaCodecAudioRenderer;
      localObject3 = new com/google/android/exoplayer2/trackselection/DefaultTrackSelector;
      ((DefaultTrackSelector)localObject3).<init>();
      localObject1 = ((DefaultLoadControl.Builder)localObject1).createDefaultLoadControl();
      paramObject = ((NativeVideoController.a)paramObject).newInstance((Renderer[])localObject2, (TrackSelector)localObject3, (LoadControl)localObject1);
      m = ((ExoPlayer)paramObject);
      paramObject = f;
      localObject1 = m;
      c = ((ExoPlayer)localObject1);
      m.addListener(this);
      paramObject = new com/mopub/nativeads/NativeVideoController$1;
      ((NativeVideoController.1)paramObject).<init>(this);
      localObject1 = new com/mopub/nativeads/NativeVideoController$2;
      ((NativeVideoController.2)localObject1).<init>(this);
      localObject2 = new com/google/android/exoplayer2/source/ExtractorMediaSource$Factory;
      ((ExtractorMediaSource.Factory)localObject2).<init>((DataSource.Factory)paramObject);
      ((ExtractorMediaSource.Factory)localObject2).setExtractorsFactory((ExtractorsFactory)localObject1);
      paramObject = Uri.parse(e.getNetworkMediaFileUrl());
      paramObject = ((ExtractorMediaSource.Factory)localObject2).createMediaSource((Uri)paramObject);
      localObject1 = m;
      ((ExoPlayer)localObject1).prepare((MediaSource)paramObject);
      paramObject = f;
      long l2 = 50;
      ((NativeVideoController.NativeVideoProgressRunnable)paramObject).startRepeating(l2);
    }
    d();
    c();
    paramObject = j;
    a((Surface)paramObject);
  }
  
  public void release(Object paramObject)
  {
    Preconditions.checkNotNull(paramObject);
    Object localObject = l;
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = ((WeakReference)localObject).get();
    }
    if (localObject == paramObject) {
      b();
    }
  }
  
  public void seekTo(long paramLong)
  {
    ExoPlayer localExoPlayer = m;
    if (localExoPlayer == null) {
      return;
    }
    m.seekTo(paramLong);
    f.f = paramLong;
  }
  
  public void setAppAudioEnabled(boolean paramBoolean)
  {
    boolean bool = s;
    if (bool == paramBoolean) {
      return;
    }
    s = paramBoolean;
    paramBoolean = s;
    if (paramBoolean)
    {
      g.requestAudioFocus(this, 3, 1);
      return;
    }
    g.abandonAudioFocus(this);
  }
  
  public void setAudioEnabled(boolean paramBoolean)
  {
    r = paramBoolean;
    d();
  }
  
  public void setAudioVolume(float paramFloat)
  {
    boolean bool = r;
    if (!bool) {
      return;
    }
    a(paramFloat);
  }
  
  public void setListener(NativeVideoController.Listener paramListener)
  {
    h = paramListener;
  }
  
  public void setOnAudioFocusChangeListener(AudioManager.OnAudioFocusChangeListener paramOnAudioFocusChangeListener)
  {
    i = paramOnAudioFocusChangeListener;
  }
  
  public void setPlayWhenReady(boolean paramBoolean)
  {
    boolean bool = q;
    if (bool == paramBoolean) {
      return;
    }
    q = paramBoolean;
    c();
  }
  
  public void setProgressListener(NativeVideoController.NativeVideoProgressRunnable.ProgressListener paramProgressListener)
  {
    f.e = paramProgressListener;
  }
  
  public void setTextureView(TextureView paramTextureView)
  {
    Preconditions.checkNotNull(paramTextureView);
    Object localObject = new android/view/Surface;
    SurfaceTexture localSurfaceTexture = paramTextureView.getSurfaceTexture();
    ((Surface)localObject).<init>(localSurfaceTexture);
    j = ((Surface)localObject);
    k = paramTextureView;
    paramTextureView = f;
    localObject = k;
    d = ((TextureView)localObject);
    paramTextureView = j;
    a(paramTextureView);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeVideoController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */