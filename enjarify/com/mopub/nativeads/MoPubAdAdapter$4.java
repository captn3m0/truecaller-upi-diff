package com.mopub.nativeads;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

final class MoPubAdAdapter$4
  implements AdapterView.OnItemClickListener
{
  MoPubAdAdapter$4(MoPubAdAdapter paramMoPubAdAdapter, AdapterView.OnItemClickListener paramOnItemClickListener) {}
  
  public final void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    MoPubStreamAdPlacer localMoPubStreamAdPlacer = MoPubAdAdapter.b(b);
    boolean bool = localMoPubStreamAdPlacer.isAd(paramInt);
    if (!bool)
    {
      AdapterView.OnItemClickListener localOnItemClickListener = a;
      localMoPubStreamAdPlacer = MoPubAdAdapter.b(b);
      int i = localMoPubStreamAdPlacer.getOriginalPosition(paramInt);
      localOnItemClickListener.onItemClick(paramAdapterView, paramView, i, paramLong);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubAdAdapter.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */