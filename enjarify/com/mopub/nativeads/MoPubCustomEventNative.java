package com.mopub.nativeads;

import android.content.Context;
import com.mopub.common.logging.MoPubLog;
import java.util.Map;
import org.json.JSONObject;

public class MoPubCustomEventNative
  extends CustomEventNative
{
  private MoPubCustomEventNative.a a;
  
  protected final void a()
  {
    MoPubCustomEventNative.a locala = a;
    if (locala == null) {
      return;
    }
    locala.invalidate();
  }
  
  protected final void a(Context paramContext, CustomEventNative.CustomEventNativeListener paramCustomEventNativeListener, Map paramMap1, Map paramMap2)
  {
    Object localObject1 = a;
    if (localObject1 != null)
    {
      bool1 = ((MoPubCustomEventNative.a)localObject1).isInvalidated();
      if (!bool1) {
        return;
      }
    }
    localObject1 = "com_mopub_native_json";
    paramMap1 = paramMap1.get(localObject1);
    boolean bool1 = paramMap1 instanceof JSONObject;
    if (!bool1)
    {
      paramContext = NativeErrorCode.INVALID_RESPONSE;
      paramCustomEventNativeListener.onNativeAdFailed(paramContext);
      return;
    }
    MoPubCustomEventNative.a locala = new com/mopub/nativeads/MoPubCustomEventNative$a;
    Object localObject2 = paramMap1;
    localObject2 = (JSONObject)paramMap1;
    ImpressionTracker localImpressionTracker = new com/mopub/nativeads/ImpressionTracker;
    localImpressionTracker.<init>(paramContext);
    NativeClickHandler localNativeClickHandler = new com/mopub/nativeads/NativeClickHandler;
    localNativeClickHandler.<init>(paramContext);
    localObject1 = locala;
    locala.<init>(paramContext, (JSONObject)localObject2, localImpressionTracker, localNativeClickHandler, paramCustomEventNativeListener);
    a = locala;
    paramContext = "impression-min-visible-percent";
    boolean bool2 = paramMap2.containsKey(paramContext);
    int i;
    if (bool2) {
      try
      {
        paramContext = a;
        paramMap1 = "impression-min-visible-percent";
        paramMap1 = paramMap2.get(paramMap1);
        paramMap1 = (String)paramMap1;
        i = Integer.parseInt(paramMap1);
        paramContext.setImpressionMinPercentageViewed(i);
      }
      catch (NumberFormatException localNumberFormatException1)
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>("Unable to format min visible percent: ");
        paramMap1 = (String)paramMap2.get("impression-min-visible-percent");
        paramContext.append(paramMap1);
        paramContext = paramContext.toString();
        MoPubLog.d(paramContext);
      }
    }
    paramContext = "impression-visible-ms";
    bool2 = paramMap2.containsKey(paramContext);
    if (bool2) {
      try
      {
        paramContext = a;
        paramMap1 = "impression-visible-ms";
        paramMap1 = paramMap2.get(paramMap1);
        paramMap1 = (String)paramMap1;
        i = Integer.parseInt(paramMap1);
        paramContext.setImpressionMinTimeViewed(i);
      }
      catch (NumberFormatException localNumberFormatException2)
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>("Unable to format min time: ");
        paramMap1 = (String)paramMap2.get("impression-visible-ms");
        paramContext.append(paramMap1);
        paramContext = paramContext.toString();
        MoPubLog.d(paramContext);
      }
    }
    paramContext = "impression-min-visible-px";
    bool2 = paramMap2.containsKey(paramContext);
    if (bool2) {
      try
      {
        paramContext = a;
        paramMap1 = "impression-min-visible-px";
        paramMap1 = paramMap2.get(paramMap1);
        paramMap1 = (String)paramMap1;
        i = Integer.parseInt(paramMap1);
        paramMap1 = Integer.valueOf(i);
        paramContext.setImpressionMinVisiblePx(paramMap1);
      }
      catch (NumberFormatException localNumberFormatException3)
      {
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>("Unable to format min visible px: ");
        paramMap1 = (String)paramMap2.get("impression-min-visible-px");
        paramContext.append(paramMap1);
        paramContext = paramContext.toString();
        MoPubLog.d(paramContext);
      }
    }
    try
    {
      paramContext = a;
      paramContext.c();
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      paramContext = NativeErrorCode.UNSPECIFIED;
      paramCustomEventNativeListener.onNativeAdFailed(paramContext);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventNative
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */