package com.mopub.nativeads;

import android.content.Context;
import com.mopub.mobileads.VastManager;

final class MoPubCustomEventVideoNative$MoPubVideoNativeAd$2
  implements NativeImageHelper.ImageListener
{
  MoPubCustomEventVideoNative$MoPubVideoNativeAd$2(MoPubCustomEventVideoNative.MoPubVideoNativeAd paramMoPubVideoNativeAd) {}
  
  public final void onImagesCached()
  {
    Object localObject = a;
    boolean bool = ((MoPubCustomEventVideoNative.MoPubVideoNativeAd)localObject).isInvalidated();
    if (bool) {
      return;
    }
    localObject = MoPubCustomEventVideoNative.MoPubVideoNativeAd.d(a);
    String str = a.getVastVideo();
    MoPubCustomEventVideoNative.MoPubVideoNativeAd localMoPubVideoNativeAd = a;
    Context localContext = MoPubCustomEventVideoNative.MoPubVideoNativeAd.c(localMoPubVideoNativeAd);
    ((VastManager)localObject).prepareVastVideoConfiguration(str, localMoPubVideoNativeAd, null, localContext);
  }
  
  public final void onImagesFailedToCache(NativeErrorCode paramNativeErrorCode)
  {
    MoPubCustomEventVideoNative.MoPubVideoNativeAd localMoPubVideoNativeAd = a;
    boolean bool = localMoPubVideoNativeAd.isInvalidated();
    if (bool) {
      return;
    }
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.e(a).onNativeAdFailed(paramNativeErrorCode);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.MoPubVideoNativeAd.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */