package com.mopub.nativeads;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.mopub.common.Preconditions;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.logging.MoPubLog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.WeakHashMap;

public class MoPubStreamAdPlacer
{
  public static final int CONTENT_VIEW_TYPE;
  private static final MoPubNativeAdLoadedListener e;
  boolean a;
  g b;
  boolean c;
  boolean d;
  private final Activity f;
  private final Handler g;
  private final Runnable h;
  private final PositioningSource i;
  private final d j;
  private final HashMap k;
  private final WeakHashMap l;
  private g m;
  private String n;
  private MoPubNativeAdLoadedListener o;
  private int p;
  private int q;
  private int r;
  private boolean s;
  
  static
  {
    MoPubStreamAdPlacer.1 local1 = new com/mopub/nativeads/MoPubStreamAdPlacer$1;
    local1.<init>();
    e = local1;
  }
  
  public MoPubStreamAdPlacer(Activity paramActivity)
  {
    this(paramActivity, localMoPubServerPositioning);
  }
  
  public MoPubStreamAdPlacer(Activity paramActivity, MoPubNativeAdPositioning.MoPubClientPositioning paramMoPubClientPositioning)
  {
    this(paramActivity, locald, locala);
  }
  
  public MoPubStreamAdPlacer(Activity paramActivity, MoPubNativeAdPositioning.MoPubServerPositioning paramMoPubServerPositioning)
  {
    this(paramActivity, paramMoPubServerPositioning, locali);
  }
  
  private MoPubStreamAdPlacer(Activity paramActivity, d paramd, PositioningSource paramPositioningSource)
  {
    MoPubNativeAdLoadedListener localMoPubNativeAdLoadedListener = e;
    o = localMoPubNativeAdLoadedListener;
    Preconditions.checkNotNull(paramActivity, "activity is not allowed to be null");
    Preconditions.checkNotNull(paramd, "adSource is not allowed to be null");
    Preconditions.checkNotNull(paramPositioningSource, "positioningSource is not allowed to be null");
    f = paramActivity;
    i = paramPositioningSource;
    j = paramd;
    paramActivity = new com/mopub/nativeads/g;
    paramPositioningSource = new int[0];
    paramActivity.<init>(paramPositioningSource);
    m = paramActivity;
    paramActivity = new java/util/WeakHashMap;
    paramActivity.<init>();
    l = paramActivity;
    paramActivity = new java/util/HashMap;
    paramActivity.<init>();
    k = paramActivity;
    paramActivity = new android/os/Handler;
    paramActivity.<init>();
    g = paramActivity;
    paramActivity = new com/mopub/nativeads/MoPubStreamAdPlacer$2;
    paramActivity.<init>(this);
    h = paramActivity;
    p = 0;
    q = 0;
  }
  
  private void a(View paramView)
  {
    if (paramView == null) {
      return;
    }
    NativeAd localNativeAd = (NativeAd)l.get(paramView);
    if (localNativeAd != null)
    {
      localNativeAd.clear(paramView);
      WeakHashMap localWeakHashMap = l;
      localWeakHashMap.remove(paramView);
      paramView = k;
      paramView.remove(localNativeAd);
    }
  }
  
  private boolean a(int paramInt)
  {
    NativeAd localNativeAd = j.b();
    if (localNativeAd == null) {
      return false;
    }
    m.a(paramInt, localNativeAd);
    int i1 = r;
    int i2 = 1;
    i1 += i2;
    r = i1;
    o.onAdLoaded(paramInt);
    return i2;
  }
  
  private boolean a(int paramInt1, int paramInt2)
  {
    int i1 = 1;
    paramInt2 -= i1;
    while (paramInt1 <= paramInt2)
    {
      int i2 = -1;
      if (paramInt1 == i2) {
        break;
      }
      i2 = r;
      if (paramInt1 >= i2) {
        break;
      }
      g localg = m;
      boolean bool = localg.a(paramInt1);
      if (bool)
      {
        bool = a(paramInt1);
        if (!bool) {
          return false;
        }
        paramInt2 += 1;
      }
      localg = m;
      paramInt1 = localg.b(paramInt1);
    }
    return i1;
  }
  
  private void b()
  {
    int i1 = p;
    int i3 = q;
    boolean bool = a(i1, i3);
    if (!bool) {
      return;
    }
    int i2 = q;
    i3 = i2 + 6;
    a(i2, i3);
  }
  
  final void a()
  {
    boolean bool = s;
    if (bool) {
      return;
    }
    s = true;
    Handler localHandler = g;
    Runnable localRunnable = h;
    localHandler.post(localRunnable);
  }
  
  final void a(g paramg)
  {
    int i1 = r;
    removeAdsInRange(0, i1);
    m = paramg;
    b();
    d = true;
  }
  
  public void bindAdView(NativeAd paramNativeAd, View paramView)
  {
    Object localObject = (WeakReference)k.get(paramNativeAd);
    if (localObject != null) {
      localObject = (View)((WeakReference)localObject).get();
    } else {
      localObject = null;
    }
    boolean bool = paramView.equals(localObject);
    if (!bool)
    {
      a((View)localObject);
      a(paramView);
      localObject = k;
      WeakReference localWeakReference = new java/lang/ref/WeakReference;
      localWeakReference.<init>(paramView);
      ((HashMap)localObject).put(paramNativeAd, localWeakReference);
      localObject = l;
      ((WeakHashMap)localObject).put(paramView, paramNativeAd);
      paramNativeAd.prepare(paramView);
      paramNativeAd.renderAdView(paramView);
    }
  }
  
  public void clearAds()
  {
    int i1 = r;
    removeAdsInRange(0, i1);
    j.a();
  }
  
  public void destroy()
  {
    g.removeMessages(0);
    j.a();
    g localg = m;
    int i1 = b;
    if (i1 != 0)
    {
      int[] arrayOfInt = a;
      int i2 = b + -1;
      i1 = arrayOfInt[i2] + 1;
      localg.a(0, i1);
    }
  }
  
  public Object getAdData(int paramInt)
  {
    return m.c(paramInt);
  }
  
  public MoPubAdRenderer getAdRendererForViewType(int paramInt)
  {
    return j.getAdRendererForViewType(paramInt);
  }
  
  public View getAdView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    g localg = m;
    NativeAd localNativeAd = localg.c(paramInt);
    if (localNativeAd == null) {
      return null;
    }
    if (paramView == null)
    {
      paramView = f;
      paramView = localNativeAd.createAdView(paramView, paramViewGroup);
    }
    bindAdView(localNativeAd, paramView);
    return paramView;
  }
  
  public int getAdViewType(int paramInt)
  {
    g localg = m;
    NativeAd localNativeAd = localg.c(paramInt);
    if (localNativeAd == null) {
      return 0;
    }
    return j.getViewTypeForAd(localNativeAd);
  }
  
  public int getAdViewTypeCount()
  {
    return j.j.getAdRendererCount();
  }
  
  public int getAdjustedCount(int paramInt)
  {
    return m.f(paramInt);
  }
  
  public int getAdjustedPosition(int paramInt)
  {
    return m.e(paramInt);
  }
  
  public int getOriginalCount(int paramInt)
  {
    g localg = m;
    if (paramInt == 0) {
      return 0;
    }
    paramInt += -1;
    paramInt = localg.d(paramInt);
    int i1 = -1;
    if (paramInt == i1) {
      return i1;
    }
    return paramInt + 1;
  }
  
  public int getOriginalPosition(int paramInt)
  {
    return m.d(paramInt);
  }
  
  public void insertItem(int paramInt)
  {
    m.g(paramInt);
  }
  
  public boolean isAd(int paramInt)
  {
    g localg = m;
    int[] arrayOfInt = a;
    int i1 = b;
    paramInt = g.a(arrayOfInt, i1, paramInt);
    return paramInt >= 0;
  }
  
  public void loadAds(String paramString)
  {
    loadAds(paramString, null);
  }
  
  public void loadAds(String paramString, RequestParameters paramRequestParameters)
  {
    Object localObject1 = "Cannot load ads with a null ad unit ID";
    boolean bool1 = Preconditions.NoThrow.checkNotNull(paramString, (String)localObject1);
    if (!bool1) {
      return;
    }
    localObject1 = j.j;
    int i1 = ((AdRendererRegistry)localObject1).getAdRendererCount();
    if (i1 == 0)
    {
      MoPubLog.w("You must register at least 1 ad renderer by calling registerAdRenderer before loading ads");
      return;
    }
    n = paramString;
    i1 = 0;
    d = false;
    a = false;
    c = false;
    localObject1 = i;
    Object localObject2 = new com/mopub/nativeads/MoPubStreamAdPlacer$3;
    ((MoPubStreamAdPlacer.3)localObject2).<init>(this);
    ((PositioningSource)localObject1).loadPositions(paramString, (PositioningSource.PositioningListener)localObject2);
    localObject1 = j;
    localObject2 = new com/mopub/nativeads/MoPubStreamAdPlacer$4;
    ((MoPubStreamAdPlacer.4)localObject2).<init>(this);
    g = ((d.a)localObject2);
    localObject1 = j;
    localObject2 = f;
    MoPubNative localMoPubNative = new com/mopub/nativeads/MoPubNative;
    MoPubNative.MoPubNativeNetworkListener localMoPubNativeNetworkListener = b;
    localMoPubNative.<init>((Context)localObject2, paramString, localMoPubNativeNetworkListener);
    ((d)localObject1).a();
    paramString = j.getRendererIterable().iterator();
    for (;;)
    {
      boolean bool2 = paramString.hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (MoPubAdRenderer)paramString.next();
      localMoPubNative.registerAdRenderer((MoPubAdRenderer)localObject2);
    }
    h = paramRequestParameters;
    i = localMoPubNative;
    ((d)localObject1).c();
  }
  
  public void moveItem(int paramInt1, int paramInt2)
  {
    g localg = m;
    localg.h(paramInt1);
    localg.g(paramInt2);
  }
  
  public void placeAdsInRange(int paramInt1, int paramInt2)
  {
    p = paramInt1;
    paramInt1 += 100;
    paramInt1 = Math.min(paramInt2, paramInt1);
    q = paramInt1;
    a();
  }
  
  public void registerAdRenderer(MoPubAdRenderer paramMoPubAdRenderer)
  {
    Object localObject = "Cannot register a null adRenderer";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramMoPubAdRenderer, (String)localObject);
    if (!bool) {
      return;
    }
    localObject = j;
    j.registerAdRenderer(paramMoPubAdRenderer);
    MoPubNative localMoPubNative = i;
    if (localMoPubNative != null)
    {
      localObject = i;
      ((MoPubNative)localObject).registerAdRenderer(paramMoPubAdRenderer);
    }
  }
  
  public int removeAdsInRange(int paramInt1, int paramInt2)
  {
    Object localObject1 = m;
    int i1 = b;
    Object localObject2 = new int[i1];
    int[] arrayOfInt = a;
    int i2 = b;
    int i4 = 0;
    System.arraycopy(arrayOfInt, 0, localObject2, 0, i2);
    paramInt1 = m.e(paramInt1);
    paramInt2 = m.e(paramInt2);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    int i5 = localObject2.length + -1;
    while (i5 >= 0)
    {
      i4 = localObject2[i5];
      if ((i4 >= paramInt1) && (i4 < paramInt2))
      {
        Integer localInteger = Integer.valueOf(i4);
        ((ArrayList)localObject1).add(localInteger);
        int i6 = p;
        if (i4 < i6)
        {
          i6 += -1;
          p = i6;
        }
        i4 = r + -1;
        r = i4;
      }
      i5 += -1;
    }
    localObject2 = m;
    paramInt1 = ((g)localObject2).a(paramInt1, paramInt2);
    Iterator localIterator = ((ArrayList)localObject1).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (Integer)localIterator.next();
      int i3 = ((Integer)localObject1).intValue();
      localObject2 = o;
      ((MoPubNativeAdLoadedListener)localObject2).onAdRemoved(i3);
    }
    return paramInt1;
  }
  
  public void removeItem(int paramInt)
  {
    m.h(paramInt);
  }
  
  public void setAdLoadedListener(MoPubNativeAdLoadedListener paramMoPubNativeAdLoadedListener)
  {
    if (paramMoPubNativeAdLoadedListener == null) {
      paramMoPubNativeAdLoadedListener = e;
    }
    o = paramMoPubNativeAdLoadedListener;
  }
  
  public void setItemCount(int paramInt)
  {
    g localg = m;
    paramInt = localg.f(paramInt);
    r = paramInt;
    paramInt = d;
    if (paramInt != 0) {
      a();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubStreamAdPlacer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */