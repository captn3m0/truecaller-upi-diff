package com.mopub.nativeads;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.os.Bundle;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.VideoView;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.BaseVideoViewController;
import com.mopub.mobileads.BaseVideoViewController.BaseVideoViewControllerListener;
import com.mopub.mobileads.VastVideoConfig;

public class NativeVideoViewController
  extends BaseVideoViewController
  implements AudioManager.OnAudioFocusChangeListener, TextureView.SurfaceTextureListener, NativeVideoController.Listener
{
  private NativeVideoViewController.a a;
  private VastVideoConfig b;
  private final NativeFullScreenVideoView c;
  private final NativeVideoController d;
  private Bitmap e;
  private boolean f;
  private boolean g;
  private int h;
  
  public NativeVideoViewController(Context paramContext, Bundle paramBundle1, Bundle paramBundle2, BaseVideoViewController.BaseVideoViewControllerListener paramBaseVideoViewControllerListener)
  {
    this(paramContext, paramBundle1, paramBaseVideoViewControllerListener, paramBundle2);
  }
  
  private NativeVideoViewController(Context paramContext, Bundle paramBundle, BaseVideoViewController.BaseVideoViewControllerListener paramBaseVideoViewControllerListener, NativeFullScreenVideoView paramNativeFullScreenVideoView)
  {
    super(paramContext, null, paramBaseVideoViewControllerListener);
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramBundle);
    Preconditions.checkNotNull(paramBaseVideoViewControllerListener);
    Preconditions.checkNotNull(paramNativeFullScreenVideoView);
    paramContext = NativeVideoViewController.a.NONE;
    a = paramContext;
    paramContext = (VastVideoConfig)paramBundle.get("native_vast_video_config");
    b = paramContext;
    c = paramNativeFullScreenVideoView;
    paramContext = NativeVideoController.getForId(((Long)paramBundle.get("native_video_id")).longValue());
    d = paramContext;
    Preconditions.checkNotNull(b);
    Preconditions.checkNotNull(d);
  }
  
  private void a()
  {
    NativeVideoViewController.a locala = a;
    boolean bool = g;
    if (bool)
    {
      locala = NativeVideoViewController.a.FAILED_LOAD;
    }
    else
    {
      bool = f;
      if (!bool)
      {
        int i = h;
        int j = 1;
        if (i == j)
        {
          locala = NativeVideoViewController.a.LOADING;
          break label95;
        }
        j = 2;
        if (i == j)
        {
          locala = NativeVideoViewController.a.BUFFERING;
          break label95;
        }
        j = 3;
        if (i == j)
        {
          locala = NativeVideoViewController.a.PLAYING;
          break label95;
        }
        j = 4;
        if (i != j)
        {
          j = 5;
          if (i != j) {
            break label95;
          }
        }
      }
      locala = NativeVideoViewController.a.ENDED;
    }
    label95:
    a(locala, false);
  }
  
  final void a(NativeVideoViewController.a parama, boolean paramBoolean)
  {
    Preconditions.checkNotNull(parama);
    Object localObject1 = a;
    if (localObject1 == parama) {
      return;
    }
    localObject1 = NativeVideoViewController.6.a;
    int i = parama.ordinal();
    int k = localObject1[i];
    i = 1;
    Object localObject2;
    switch (k)
    {
    default: 
      break;
    case 6: 
      f = i;
      d.setAppAudioEnabled(false);
      localObject2 = c;
      k = 1000;
      ((NativeFullScreenVideoView)localObject2).updateProgress(k);
      localObject2 = c;
      localObject1 = NativeFullScreenVideoView.Mode.FINISHED;
      ((NativeFullScreenVideoView)localObject2).setMode((NativeFullScreenVideoView.Mode)localObject1);
      localObject2 = b;
      localObject1 = mContext;
      ((VastVideoConfig)localObject2).handleComplete((Context)localObject1, 0);
      break;
    case 5: 
      if (!paramBoolean)
      {
        localObject2 = d;
        ((NativeVideoController)localObject2).setAppAudioEnabled(false);
      }
      d.setPlayWhenReady(false);
      localObject2 = c;
      localObject1 = NativeFullScreenVideoView.Mode.PAUSED;
      ((NativeFullScreenVideoView)localObject2).setMode((NativeFullScreenVideoView.Mode)localObject1);
      break;
    case 4: 
      d.setPlayWhenReady(i);
      d.setAudioEnabled(i);
      d.setAppAudioEnabled(i);
      localObject2 = c;
      localObject1 = NativeFullScreenVideoView.Mode.PLAYING;
      ((NativeFullScreenVideoView)localObject2).setMode((NativeFullScreenVideoView.Mode)localObject1);
      break;
    case 2: 
    case 3: 
      d.setPlayWhenReady(i);
      localObject2 = c;
      localObject1 = NativeFullScreenVideoView.Mode.LOADING;
      ((NativeFullScreenVideoView)localObject2).setMode((NativeFullScreenVideoView.Mode)localObject1);
      break;
    case 1: 
      d.setPlayWhenReady(false);
      d.setAudioEnabled(false);
      d.setAppAudioEnabled(false);
      localObject2 = c;
      localObject1 = NativeFullScreenVideoView.Mode.LOADING;
      ((NativeFullScreenVideoView)localObject2).setMode((NativeFullScreenVideoView.Mode)localObject1);
      localObject2 = b;
      localObject1 = mContext;
      int j = 0;
      ((VastVideoConfig)localObject2).handleError((Context)localObject1, null, 0);
    }
    a = parama;
  }
  
  public final VideoView getVideoView()
  {
    return null;
  }
  
  public void onAudioFocusChange(int paramInt)
  {
    int i = -1;
    float f1 = 0.0F / 0.0F;
    if (paramInt != i)
    {
      i = -2;
      f1 = 0.0F / 0.0F;
      if (paramInt != i)
      {
        i = -3;
        f1 = 0.0F / 0.0F;
        if (paramInt == i)
        {
          d.setAudioVolume(0.3F);
          return;
        }
        i = 1;
        f1 = Float.MIN_VALUE;
        if (paramInt == i)
        {
          localObject = d;
          i = 1065353216;
          f1 = 1.0F;
          ((NativeVideoController)localObject).setAudioVolume(f1);
          a();
        }
        return;
      }
    }
    Object localObject = NativeVideoViewController.a.PAUSED;
    a((NativeVideoViewController.a)localObject, false);
  }
  
  public final void onBackPressed()
  {
    NativeVideoViewController.a locala = NativeVideoViewController.a.PAUSED;
    a(locala, true);
  }
  
  public final void onConfigurationChanged(Configuration paramConfiguration)
  {
    NativeFullScreenVideoView localNativeFullScreenVideoView = c;
    int i = orientation;
    localNativeFullScreenVideoView.setOrientation(i);
  }
  
  public final void onCreate()
  {
    c.setSurfaceTextureListener(this);
    Object localObject1 = c;
    Object localObject2 = NativeFullScreenVideoView.Mode.LOADING;
    ((NativeFullScreenVideoView)localObject1).setMode((NativeFullScreenVideoView.Mode)localObject2);
    localObject1 = c;
    localObject2 = new com/mopub/nativeads/NativeVideoViewController$1;
    ((NativeVideoViewController.1)localObject2).<init>(this);
    ((NativeFullScreenVideoView)localObject1).setPlayControlClickListener((View.OnClickListener)localObject2);
    localObject1 = c;
    localObject2 = new com/mopub/nativeads/NativeVideoViewController$2;
    ((NativeVideoViewController.2)localObject2).<init>(this);
    ((NativeFullScreenVideoView)localObject1).setCloseControlListener((View.OnClickListener)localObject2);
    localObject1 = c;
    localObject2 = new com/mopub/nativeads/NativeVideoViewController$3;
    ((NativeVideoViewController.3)localObject2).<init>(this);
    ((NativeFullScreenVideoView)localObject1).setCtaClickListener((View.OnClickListener)localObject2);
    localObject1 = c;
    localObject2 = new com/mopub/nativeads/NativeVideoViewController$4;
    ((NativeVideoViewController.4)localObject2).<init>(this);
    ((NativeFullScreenVideoView)localObject1).setPrivacyInformationClickListener((View.OnClickListener)localObject2);
    localObject1 = c;
    localObject2 = b.getPrivacyInformationIconImageUrl();
    ((NativeFullScreenVideoView)localObject1).setPrivacyInformationIconImageUrl((String)localObject2);
    localObject1 = new android/view/ViewGroup$LayoutParams;
    int i = -1;
    ((ViewGroup.LayoutParams)localObject1).<init>(i, i);
    c.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    localObject1 = mBaseVideoViewControllerListener;
    localObject2 = c;
    ((BaseVideoViewController.BaseVideoViewControllerListener)localObject1).onSetContentView((View)localObject2);
    localObject1 = d;
    localObject2 = new com/mopub/nativeads/NativeVideoViewController$5;
    ((NativeVideoViewController.5)localObject2).<init>(this);
    ((NativeVideoController)localObject1).setProgressListener((NativeVideoController.NativeVideoProgressRunnable.ProgressListener)localObject2);
  }
  
  public final void onDestroy() {}
  
  public void onError(Exception paramException)
  {
    MoPubLog.w("Error playing back video.", paramException);
    g = true;
    a();
  }
  
  public final void onPause() {}
  
  public final void onResume()
  {
    Bitmap localBitmap = e;
    if (localBitmap != null)
    {
      NativeFullScreenVideoView localNativeFullScreenVideoView = c;
      localNativeFullScreenVideoView.setCachedVideoFrame(localBitmap);
    }
    d.prepare(this);
    d.setListener(this);
    d.setOnAudioFocusChangeListener(this);
  }
  
  public final void onSaveInstanceState(Bundle paramBundle) {}
  
  public void onStateChanged(boolean paramBoolean, int paramInt)
  {
    h = paramInt;
    a();
  }
  
  public void onSurfaceTextureAvailable(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2)
  {
    paramSurfaceTexture = d;
    TextureView localTextureView = c.getTextureView();
    paramSurfaceTexture.setTextureView(localTextureView);
    boolean bool1 = f;
    if (!bool1)
    {
      paramSurfaceTexture = d;
      long l1 = paramSurfaceTexture.getCurrentPosition();
      paramSurfaceTexture.seekTo(l1);
    }
    paramSurfaceTexture = d;
    paramInt1 = f;
    paramInt2 = 1;
    paramInt1 ^= paramInt2;
    paramSurfaceTexture.setPlayWhenReady(paramInt1);
    paramSurfaceTexture = d;
    long l2 = paramSurfaceTexture.getCurrentPosition();
    NativeVideoController localNativeVideoController = d;
    long l3 = localNativeVideoController.getDuration() - l2;
    l2 = 750L;
    boolean bool2 = l3 < l2;
    if (bool2)
    {
      f = paramInt2;
      a();
    }
  }
  
  public boolean onSurfaceTextureDestroyed(SurfaceTexture paramSurfaceTexture)
  {
    d.release(this);
    paramSurfaceTexture = NativeVideoViewController.a.PAUSED;
    a(paramSurfaceTexture, false);
    return true;
  }
  
  public void onSurfaceTextureSizeChanged(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2) {}
  
  public void onSurfaceTextureUpdated(SurfaceTexture paramSurfaceTexture) {}
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeVideoViewController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */