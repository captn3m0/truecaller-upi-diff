package com.mopub.nativeads;

import android.location.Location;
import android.text.TextUtils;
import com.mopub.common.MoPub;
import java.util.EnumSet;

public class RequestParameters
{
  private final String a;
  private final String b;
  private final Location c;
  private final EnumSet d;
  
  private RequestParameters(RequestParameters.Builder paramBuilder)
  {
    Object localObject = RequestParameters.Builder.a(paramBuilder);
    a = ((String)localObject);
    localObject = RequestParameters.Builder.b(paramBuilder);
    d = ((EnumSet)localObject);
    boolean bool = MoPub.canCollectPersonalInformation();
    Location localLocation = null;
    String str;
    if (bool) {
      str = RequestParameters.Builder.c(paramBuilder);
    } else {
      str = null;
    }
    b = str;
    if (bool) {
      localLocation = RequestParameters.Builder.d(paramBuilder);
    }
    c = localLocation;
  }
  
  public final String getDesiredAssets()
  {
    String str = "";
    Object localObject = d;
    if (localObject != null)
    {
      localObject = ((EnumSet)localObject).toArray();
      str = TextUtils.join(",", (Object[])localObject);
    }
    return str;
  }
  
  public final String getKeywords()
  {
    return a;
  }
  
  public final Location getLocation()
  {
    return c;
  }
  
  public final String getUserDataKeywords()
  {
    boolean bool = MoPub.canCollectPersonalInformation();
    if (!bool) {
      return null;
    }
    return b;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.RequestParameters
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */