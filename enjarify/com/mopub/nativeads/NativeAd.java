package com.mopub.nativeads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NativeAd
{
  final Context a;
  final Set b;
  final Set c;
  NativeAd.MoPubNativeEventListener d;
  boolean e;
  boolean f;
  boolean g;
  private final BaseNativeAd h;
  private final MoPubAdRenderer i;
  private final String j;
  
  public NativeAd(Context paramContext, List paramList, String paramString1, String paramString2, BaseNativeAd paramBaseNativeAd, MoPubAdRenderer paramMoPubAdRenderer)
  {
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    j = paramString2;
    paramContext = new java/util/HashSet;
    paramContext.<init>();
    b = paramContext;
    b.addAll(paramList);
    paramContext = b;
    paramList = new java/util/HashSet;
    paramString2 = a;
    paramList.<init>(paramString2);
    paramContext.addAll(paramList);
    paramContext = new java/util/HashSet;
    paramContext.<init>();
    c = paramContext;
    c.add(paramString1);
    paramContext = c;
    paramList = paramBaseNativeAd.b();
    paramContext.addAll(paramList);
    h = paramBaseNativeAd;
    paramContext = h;
    paramList = new com/mopub/nativeads/NativeAd$1;
    paramList.<init>(this);
    paramContext.setNativeEventListener(paramList);
    i = paramMoPubAdRenderer;
  }
  
  public void clear(View paramView)
  {
    boolean bool = g;
    if (bool) {
      return;
    }
    h.clear(paramView);
  }
  
  public View createAdView(Context paramContext, ViewGroup paramViewGroup)
  {
    return i.createAdView(paramContext, paramViewGroup);
  }
  
  public void destroy()
  {
    boolean bool = g;
    if (bool) {
      return;
    }
    h.destroy();
    g = true;
  }
  
  public String getAdUnitId()
  {
    return j;
  }
  
  public BaseNativeAd getBaseNativeAd()
  {
    return h;
  }
  
  public MoPubAdRenderer getMoPubAdRenderer()
  {
    return i;
  }
  
  public boolean isDestroyed()
  {
    return g;
  }
  
  public void prepare(View paramView)
  {
    boolean bool = g;
    if (bool) {
      return;
    }
    h.prepare(paramView);
  }
  
  public void renderAdView(View paramView)
  {
    MoPubAdRenderer localMoPubAdRenderer = i;
    BaseNativeAd localBaseNativeAd = h;
    localMoPubAdRenderer.renderAdView(paramView, localBaseNativeAd);
  }
  
  public void setMoPubNativeEventListener(NativeAd.MoPubNativeEventListener paramMoPubNativeEventListener)
  {
    d = paramMoPubNativeEventListener;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("\n");
    localStringBuilder.append("impressionTrackers:");
    Set localSet = b;
    localStringBuilder.append(localSet);
    localStringBuilder.append("\n");
    localStringBuilder.append("clickTrackers:");
    localSet = c;
    localStringBuilder.append(localSet);
    localStringBuilder.append("\n");
    localStringBuilder.append("recordedImpression:");
    boolean bool = e;
    localStringBuilder.append(bool);
    localStringBuilder.append("\n");
    localStringBuilder.append("isClicked:");
    bool = f;
    localStringBuilder.append(bool);
    localStringBuilder.append("\n");
    localStringBuilder.append("isDestroyed:");
    bool = g;
    localStringBuilder.append(bool);
    localStringBuilder.append("\n");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeAd
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */