package com.mopub.nativeads;

abstract interface PositioningSource
{
  public abstract void loadPositions(String paramString, PositioningSource.PositioningListener paramPositioningListener);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.PositioningSource
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */