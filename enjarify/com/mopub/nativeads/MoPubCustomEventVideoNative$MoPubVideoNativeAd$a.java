package com.mopub.nativeads;

import com.mopub.common.Preconditions;
import java.util.HashSet;
import java.util.Set;

 enum MoPubCustomEventVideoNative$MoPubVideoNativeAd$a
{
  static final Set c;
  final String a;
  final boolean b;
  
  static
  {
    Object localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    int i = 1;
    int j = 0;
    ((a)localObject).<init>("IMPRESSION_TRACKER", 0, "imptracker", i);
    IMPRESSION_TRACKER = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    ((a)localObject).<init>("CLICK_TRACKER", i, "clktracker", i);
    CLICK_TRACKER = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    int k = 2;
    ((a)localObject).<init>("TITLE", k, "title", false);
    TITLE = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    int m = 3;
    ((a)localObject).<init>("TEXT", m, "text", false);
    TEXT = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    int n = 4;
    ((a)localObject).<init>("IMAGE_URL", n, "mainimage", false);
    IMAGE_URL = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    int i1 = 5;
    ((a)localObject).<init>("ICON_URL", i1, "iconimage", false);
    ICON_URL = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    int i2 = 6;
    ((a)localObject).<init>("CLICK_DESTINATION", i2, "clk", false);
    CLICK_DESTINATION = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    int i3 = 7;
    ((a)localObject).<init>("FALLBACK", i3, "fallback", false);
    FALLBACK = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    int i4 = 8;
    ((a)localObject).<init>("CALL_TO_ACTION", i4, "ctatext", false);
    CALL_TO_ACTION = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    int i5 = 9;
    ((a)localObject).<init>("VAST_VIDEO", i5, "video", false);
    VAST_VIDEO = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    int i6 = 10;
    ((a)localObject).<init>("PRIVACY_INFORMATION_ICON_IMAGE_URL", i6, "privacyicon", false);
    PRIVACY_INFORMATION_ICON_IMAGE_URL = (a)localObject;
    localObject = new com/mopub/nativeads/MoPubCustomEventVideoNative$MoPubVideoNativeAd$a;
    String str = "privacyclkurl";
    int i7 = 11;
    ((a)localObject).<init>("PRIVACY_INFORMATION_ICON_CLICKTHROUGH_URL", i7, str, false);
    PRIVACY_INFORMATION_ICON_CLICKTHROUGH_URL = (a)localObject;
    int i8 = 12;
    localObject = new a[i8];
    a locala = IMPRESSION_TRACKER;
    localObject[0] = locala;
    locala = CLICK_TRACKER;
    localObject[i] = locala;
    locala = TITLE;
    localObject[k] = locala;
    locala = TEXT;
    localObject[m] = locala;
    locala = IMAGE_URL;
    localObject[n] = locala;
    locala = ICON_URL;
    localObject[i1] = locala;
    locala = CLICK_DESTINATION;
    localObject[i2] = locala;
    locala = FALLBACK;
    localObject[i3] = locala;
    locala = CALL_TO_ACTION;
    localObject[i4] = locala;
    locala = VAST_VIDEO;
    localObject[i5] = locala;
    locala = PRIVACY_INFORMATION_ICON_IMAGE_URL;
    localObject[i6] = locala;
    locala = PRIVACY_INFORMATION_ICON_CLICKTHROUGH_URL;
    localObject[i7] = locala;
    d = (a[])localObject;
    localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    c = (Set)localObject;
    localObject = values();
    int i9 = localObject.length;
    while (j < i9)
    {
      str = localObject[j];
      boolean bool = b;
      if (bool)
      {
        Set localSet = c;
        str = a;
        localSet.add(str);
      }
      j += 1;
    }
  }
  
  private MoPubCustomEventVideoNative$MoPubVideoNativeAd$a(String paramString1, boolean paramBoolean)
  {
    Preconditions.checkNotNull(paramString1);
    a = paramString1;
    b = paramBoolean;
  }
  
  static a a(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    a[] arrayOfa = values();
    int i = arrayOfa.length;
    int j = 0;
    while (j < i)
    {
      a locala = arrayOfa[j];
      String str = a;
      boolean bool = str.equals(paramString);
      if (bool) {
        return locala;
      }
      j += 1;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.MoPubVideoNativeAd.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */