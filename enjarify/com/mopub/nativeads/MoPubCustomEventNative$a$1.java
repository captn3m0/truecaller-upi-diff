package com.mopub.nativeads;

final class MoPubCustomEventNative$a$1
  implements NativeImageHelper.ImageListener
{
  MoPubCustomEventNative$a$1(MoPubCustomEventNative.a parama) {}
  
  public final void onImagesCached()
  {
    Object localObject = a;
    boolean bool = ((MoPubCustomEventNative.a)localObject).isInvalidated();
    if (bool) {
      return;
    }
    localObject = MoPubCustomEventNative.a.a(a);
    MoPubCustomEventNative.a locala = a;
    ((CustomEventNative.CustomEventNativeListener)localObject).onNativeAdLoaded(locala);
  }
  
  public final void onImagesFailedToCache(NativeErrorCode paramNativeErrorCode)
  {
    MoPubCustomEventNative.a locala = a;
    boolean bool = locala.isInvalidated();
    if (bool) {
      return;
    }
    MoPubCustomEventNative.a.a(a).onNativeAdFailed(paramNativeErrorCode);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventNative.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */