package com.mopub.nativeads;

public abstract interface MoPubNativeAdLoadedListener
{
  public abstract void onAdLoaded(int paramInt);
  
  public abstract void onAdRemoved(int paramInt);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubNativeAdLoadedListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */