package com.mopub.nativeads;

import com.mopub.common.logging.MoPubLog;
import com.mopub.volley.VolleyError;
import com.mopub.volley.toolbox.ImageLoader.ImageContainer;
import com.mopub.volley.toolbox.ImageLoader.ImageListener;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

final class NativeImageHelper$1
  implements ImageLoader.ImageListener
{
  NativeImageHelper$1(AtomicInteger paramAtomicInteger, AtomicBoolean paramAtomicBoolean, NativeImageHelper.ImageListener paramImageListener) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    MoPubLog.d("Failed to download a native ads image:", paramVolleyError);
    paramVolleyError = b;
    boolean bool1 = true;
    boolean bool2 = paramVolleyError.getAndSet(bool1);
    Object localObject = a;
    ((AtomicInteger)localObject).decrementAndGet();
    if (!bool2)
    {
      paramVolleyError = c;
      localObject = NativeErrorCode.IMAGE_DOWNLOAD_FAILURE;
      paramVolleyError.onImagesFailedToCache((NativeErrorCode)localObject);
    }
  }
  
  public final void onResponse(ImageLoader.ImageContainer paramImageContainer, boolean paramBoolean)
  {
    paramImageContainer = paramImageContainer.getBitmap();
    if (paramImageContainer != null)
    {
      paramImageContainer = a;
      int i = paramImageContainer.decrementAndGet();
      if (i == 0)
      {
        paramImageContainer = b;
        boolean bool = paramImageContainer.get();
        if (!bool)
        {
          paramImageContainer = c;
          paramImageContainer.onImagesCached();
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeImageHelper.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */