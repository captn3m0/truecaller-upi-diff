package com.mopub.nativeads;

public abstract interface NativeVideoController$NativeVideoProgressRunnable$ProgressListener
{
  public abstract void updateProgress(int paramInt);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeVideoController.NativeVideoProgressRunnable.ProgressListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */