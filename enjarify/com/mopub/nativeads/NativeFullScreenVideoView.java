package com.mopub.nativeads;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.text.TextUtils;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.mopub.common.Preconditions;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Drawables;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.VastVideoProgressBarWidget;
import com.mopub.mobileads.resource.CloseButtonDrawable;
import com.mopub.mobileads.resource.CtaButtonDrawable;
import com.mopub.mobileads.resource.DrawableConstants.GradientStrip;

public class NativeFullScreenVideoView
  extends RelativeLayout
{
  NativeFullScreenVideoView.Mode a;
  final int b;
  final int c;
  final int d;
  final int e;
  final int f;
  final int g;
  final int h;
  final int i;
  private int j;
  private final ImageView k;
  private final TextureView l;
  private final ProgressBar m;
  private final ImageView n;
  private final ImageView o;
  private final VastVideoProgressBarWidget p;
  private final View q;
  private final ImageView r;
  private final ImageView s;
  private final ImageView t;
  private final ImageView u;
  
  public NativeFullScreenVideoView(Context paramContext, int paramInt, String paramString)
  {
    this(paramContext, paramInt, paramString, localImageView1, localTextureView, localProgressBar, localImageView2, localImageView3, localVastVideoProgressBarWidget, localView, localImageView4, localImageView5, localImageView6, localImageView7);
  }
  
  private NativeFullScreenVideoView(Context paramContext, int paramInt, String paramString, ImageView paramImageView1, TextureView paramTextureView, ProgressBar paramProgressBar, ImageView paramImageView2, ImageView paramImageView3, VastVideoProgressBarWidget paramVastVideoProgressBarWidget, View paramView, ImageView paramImageView4, ImageView paramImageView5, ImageView paramImageView6, ImageView paramImageView7)
  {
    super(paramContext);
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramImageView1);
    Preconditions.checkNotNull(paramTextureView);
    Preconditions.checkNotNull(paramProgressBar);
    Preconditions.checkNotNull(paramImageView2);
    Preconditions.checkNotNull(paramImageView3);
    Preconditions.checkNotNull(paramVastVideoProgressBarWidget);
    Preconditions.checkNotNull(paramView);
    Preconditions.checkNotNull(paramImageView4);
    Preconditions.checkNotNull(paramImageView5);
    Preconditions.checkNotNull(paramImageView6);
    Preconditions.checkNotNull(paramImageView7);
    int i1 = paramInt;
    j = paramInt;
    Object localObject2 = NativeFullScreenVideoView.Mode.LOADING;
    a = ((NativeFullScreenVideoView.Mode)localObject2);
    i1 = Dips.asIntPixels(200.0F, paramContext);
    b = i1;
    i1 = Dips.asIntPixels(42.0F, paramContext);
    c = i1;
    i1 = Dips.asIntPixels(10.0F, paramContext);
    d = i1;
    float f1 = 50.0F;
    int i2 = Dips.asIntPixels(f1, paramContext);
    e = i2;
    i2 = Dips.asIntPixels(8.0F, paramContext);
    f = i2;
    i2 = Dips.asIntPixels(44.0F, paramContext);
    g = i2;
    i1 = Dips.asIntPixels(f1, paramContext);
    h = i1;
    f1 = 45.0F;
    i1 = Dips.asIntPixels(f1, paramContext);
    i = i1;
    localObject2 = new android/widget/RelativeLayout$LayoutParams;
    i2 = -1;
    ((RelativeLayout.LayoutParams)localObject2).<init>(i2, i2);
    int i3 = 13;
    ((RelativeLayout.LayoutParams)localObject2).addRule(i3);
    Object localObject3 = paramTextureView;
    l = paramTextureView;
    localObject3 = l;
    int i4 = (int)Utils.generateUniqueId();
    ((TextureView)localObject3).setId(i4);
    l.setLayoutParams((ViewGroup.LayoutParams)localObject2);
    localObject3 = l;
    addView((View)localObject3);
    localObject3 = paramImageView1;
    k = paramImageView1;
    localObject3 = k;
    i4 = (int)Utils.generateUniqueId();
    ((ImageView)localObject3).setId(i4);
    k.setLayoutParams((ViewGroup.LayoutParams)localObject2);
    localObject2 = k;
    localObject3 = null;
    ((ImageView)localObject2).setBackgroundColor(0);
    localObject2 = k;
    addView((View)localObject2);
    localObject2 = new android/widget/RelativeLayout$LayoutParams;
    int i5 = h;
    ((RelativeLayout.LayoutParams)localObject2).<init>(i5, i5);
    ((RelativeLayout.LayoutParams)localObject2).addRule(i3);
    Object localObject4 = paramProgressBar;
    m = paramProgressBar;
    localObject4 = m;
    long l1 = Utils.generateUniqueId();
    int i7 = (int)l1;
    ((ProgressBar)localObject4).setId(i7);
    localObject4 = m;
    Object localObject5 = new com/mopub/nativeads/NativeFullScreenVideoView$a;
    ((NativeFullScreenVideoView.a)localObject5).<init>(paramContext);
    ((ProgressBar)localObject4).setBackground((Drawable)localObject5);
    m.setLayoutParams((ViewGroup.LayoutParams)localObject2);
    localObject2 = m;
    i5 = 1;
    ((ProgressBar)localObject2).setIndeterminate(i5);
    localObject2 = m;
    addView((View)localObject2);
    localObject2 = new android/widget/RelativeLayout$LayoutParams;
    i4 = i;
    ((RelativeLayout.LayoutParams)localObject2).<init>(i2, i4);
    i4 = l.getId();
    ((RelativeLayout.LayoutParams)localObject2).addRule(8, i4);
    localObject5 = paramImageView2;
    n = paramImageView2;
    localObject5 = n;
    long l2 = Utils.generateUniqueId();
    int i8 = (int)l2;
    ((ImageView)localObject5).setId(i8);
    n.setLayoutParams((ViewGroup.LayoutParams)localObject2);
    localObject2 = new android/graphics/drawable/GradientDrawable;
    localObject5 = GradientDrawable.Orientation.BOTTOM_TOP;
    i7 = 2;
    int[] arrayOfInt1 = new int[i7];
    int i9 = DrawableConstants.GradientStrip.START_COLOR;
    arrayOfInt1[0] = i9;
    i9 = DrawableConstants.GradientStrip.END_COLOR;
    arrayOfInt1[i5] = i9;
    ((GradientDrawable)localObject2).<init>((GradientDrawable.Orientation)localObject5, arrayOfInt1);
    n.setImageDrawable((Drawable)localObject2);
    localObject2 = n;
    addView((View)localObject2);
    localObject2 = new android/widget/RelativeLayout$LayoutParams;
    i4 = i;
    ((RelativeLayout.LayoutParams)localObject2).<init>(i2, i4);
    ((RelativeLayout.LayoutParams)localObject2).addRule(10);
    localObject5 = paramImageView3;
    o = paramImageView3;
    localObject5 = o;
    long l3 = Utils.generateUniqueId();
    i9 = (int)l3;
    ((ImageView)localObject5).setId(i9);
    o.setLayoutParams((ViewGroup.LayoutParams)localObject2);
    localObject2 = new android/graphics/drawable/GradientDrawable;
    localObject5 = GradientDrawable.Orientation.TOP_BOTTOM;
    int[] arrayOfInt2 = new int[i7];
    i8 = DrawableConstants.GradientStrip.START_COLOR;
    arrayOfInt2[0] = i8;
    i8 = DrawableConstants.GradientStrip.END_COLOR;
    arrayOfInt2[i5] = i8;
    ((GradientDrawable)localObject2).<init>((GradientDrawable.Orientation)localObject5, arrayOfInt2);
    o.setImageDrawable((Drawable)localObject2);
    localObject2 = o;
    addView((View)localObject2);
    localObject2 = paramVastVideoProgressBarWidget;
    p = paramVastVideoProgressBarWidget;
    localObject2 = p;
    long l4 = Utils.generateUniqueId();
    i4 = (int)l4;
    ((VastVideoProgressBarWidget)localObject2).setId(i4);
    localObject2 = p;
    localObject4 = l;
    int i6 = ((TextureView)localObject4).getId();
    ((VastVideoProgressBarWidget)localObject2).setAnchorId(i6);
    p.calibrateAndMakeVisible(1000, 0);
    localObject2 = p;
    addView((View)localObject2);
    localObject2 = new android/widget/RelativeLayout$LayoutParams;
    ((RelativeLayout.LayoutParams)localObject2).<init>(i2, i2);
    ((RelativeLayout.LayoutParams)localObject2).addRule(i3);
    Object localObject6 = paramView;
    q = paramView;
    localObject6 = q;
    long l5 = Utils.generateUniqueId();
    i6 = (int)l5;
    ((View)localObject6).setId(i6);
    q.setLayoutParams((ViewGroup.LayoutParams)localObject2);
    q.setBackgroundColor(-2013265920);
    localObject2 = q;
    addView((View)localObject2);
    localObject2 = new android/widget/RelativeLayout$LayoutParams;
    i2 = h;
    ((RelativeLayout.LayoutParams)localObject2).<init>(i2, i2);
    ((RelativeLayout.LayoutParams)localObject2).addRule(i3);
    localObject6 = paramImageView4;
    r = paramImageView4;
    localObject6 = r;
    long l6 = Utils.generateUniqueId();
    int i10 = (int)l6;
    ((ImageView)localObject6).setId(i10);
    r.setLayoutParams((ViewGroup.LayoutParams)localObject2);
    localObject2 = r;
    localObject6 = Drawables.NATIVE_PLAY.createDrawable(paramContext);
    ((ImageView)localObject2).setImageDrawable((Drawable)localObject6);
    localObject2 = r;
    addView((View)localObject2);
    localObject2 = paramImageView5;
    s = paramImageView5;
    localObject2 = s;
    long l7 = Utils.generateUniqueId();
    i3 = (int)l7;
    ((ImageView)localObject2).setId(i3);
    localObject2 = s;
    i2 = f;
    i3 = i2 * 2;
    i10 = i2 * 2;
    ((ImageView)localObject2).setPadding(i2, i2, i3, i10);
    localObject2 = s;
    addView((View)localObject2);
    localObject2 = new com/mopub/mobileads/resource/CtaButtonDrawable;
    ((CtaButtonDrawable)localObject2).<init>(paramContext);
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      localObject1 = paramString;
      ((CtaButtonDrawable)localObject2).setCtaText(paramString);
      localObject1 = paramImageView6;
    }
    else
    {
      localObject1 = paramImageView6;
    }
    t = ((ImageView)localObject1);
    localObject1 = t;
    i3 = (int)Utils.generateUniqueId();
    ((ImageView)localObject1).setId(i3);
    t.setImageDrawable((Drawable)localObject2);
    localObject1 = t;
    addView((View)localObject1);
    localObject1 = paramImageView7;
    u = paramImageView7;
    localObject1 = u;
    i2 = (int)Utils.generateUniqueId();
    ((ImageView)localObject1).setId(i2);
    localObject1 = u;
    localObject2 = new com/mopub/mobileads/resource/CloseButtonDrawable;
    ((CloseButtonDrawable)localObject2).<init>();
    ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
    localObject1 = u;
    i1 = f;
    i2 = i1 * 3;
    i3 = i1 * 3;
    ((ImageView)localObject1).setPadding(i2, i1, i1, i3);
    localObject1 = u;
    addView((View)localObject1);
    a();
  }
  
  private void a()
  {
    int[] arrayOfInt = NativeFullScreenVideoView.1.a;
    int i1 = a.ordinal();
    int i2 = arrayOfInt[i1];
    i1 = 0;
    int i3 = 4;
    switch (i2)
    {
    default: 
      break;
    case 4: 
      setCachedImageVisibility(0);
      setLoadingSpinnerVisibility(i3);
      setVideoProgressVisibility(i3);
      setPlayButtonVisibility(0);
      break;
    case 3: 
      setCachedImageVisibility(i3);
      setLoadingSpinnerVisibility(i3);
      setVideoProgressVisibility(0);
      setPlayButtonVisibility(0);
      break;
    case 2: 
      setCachedImageVisibility(i3);
      setLoadingSpinnerVisibility(i3);
      setVideoProgressVisibility(0);
      setPlayButtonVisibility(i3);
      break;
    case 1: 
      setCachedImageVisibility(0);
      setLoadingSpinnerVisibility(0);
      setVideoProgressVisibility(i3);
      setPlayButtonVisibility(i3);
    }
    b();
    c();
  }
  
  private void b()
  {
    Configuration localConfiguration = getContext().getResources().getConfiguration();
    ViewGroup.LayoutParams localLayoutParams = l.getLayoutParams();
    float f1 = screenWidthDp;
    Context localContext1 = getContext();
    int i1 = Dips.dipsToIntPixels(f1, localContext1);
    int i2 = width;
    if (i1 != i2) {
      width = i1;
    }
    float f2 = screenWidthDp * 9.0F;
    f1 = 16.0F;
    f2 /= f1;
    Context localContext2 = getContext();
    int i3 = Dips.dipsToIntPixels(f2, localContext2);
    i1 = height;
    if (i3 != i1) {
      height = i3;
    }
  }
  
  private void c()
  {
    RelativeLayout.LayoutParams localLayoutParams1 = new android/widget/RelativeLayout$LayoutParams;
    int i1 = b;
    int i2 = c;
    localLayoutParams1.<init>(i1, i2);
    i1 = d;
    localLayoutParams1.setMargins(i1, i1, i1, i1);
    RelativeLayout.LayoutParams localLayoutParams2 = new android/widget/RelativeLayout$LayoutParams;
    i2 = g;
    localLayoutParams2.<init>(i2, i2);
    RelativeLayout.LayoutParams localLayoutParams3 = new android/widget/RelativeLayout$LayoutParams;
    int i3 = e;
    localLayoutParams3.<init>(i3, i3);
    i3 = j;
    int i4 = 11;
    int i5;
    TextureView localTextureView1;
    switch (i3)
    {
    default: 
      break;
    case 2: 
      i5 = p.getId();
      localLayoutParams1.addRule(2, i5);
      localLayoutParams1.addRule(i4);
      i3 = l.getId();
      i4 = 6;
      localLayoutParams2.addRule(i4, i3);
      localTextureView1 = l;
      i5 = localTextureView1.getId();
      localLayoutParams2.addRule(5, i5);
      TextureView localTextureView2 = l;
      i3 = localTextureView2.getId();
      localLayoutParams3.addRule(i4, i3);
      i3 = 7;
      TextureView localTextureView3 = l;
      i4 = localTextureView3.getId();
      localLayoutParams3.addRule(i3, i4);
      break;
    case 1: 
      localTextureView1 = l;
      i5 = localTextureView1.getId();
      localLayoutParams1.addRule(3, i5);
      localLayoutParams1.addRule(14);
      i3 = 10;
      localLayoutParams2.addRule(i3);
      i5 = 9;
      localLayoutParams2.addRule(i5);
      localLayoutParams3.addRule(i3);
      localLayoutParams3.addRule(i4);
    }
    t.setLayoutParams(localLayoutParams1);
    s.setLayoutParams(localLayoutParams2);
    u.setLayoutParams(localLayoutParams3);
  }
  
  private void setCachedImageVisibility(int paramInt)
  {
    k.setVisibility(paramInt);
  }
  
  private void setLoadingSpinnerVisibility(int paramInt)
  {
    m.setVisibility(paramInt);
  }
  
  private void setPlayButtonVisibility(int paramInt)
  {
    r.setVisibility(paramInt);
    q.setVisibility(paramInt);
  }
  
  private void setVideoProgressVisibility(int paramInt)
  {
    p.setVisibility(paramInt);
  }
  
  ImageView getCtaButton()
  {
    return t;
  }
  
  public TextureView getTextureView()
  {
    return l;
  }
  
  public void resetProgress()
  {
    p.reset();
  }
  
  public void setCachedVideoFrame(Bitmap paramBitmap)
  {
    k.setImageBitmap(paramBitmap);
  }
  
  public void setCloseControlListener(View.OnClickListener paramOnClickListener)
  {
    u.setOnClickListener(paramOnClickListener);
  }
  
  public void setCtaClickListener(View.OnClickListener paramOnClickListener)
  {
    t.setOnClickListener(paramOnClickListener);
  }
  
  public void setMode(NativeFullScreenVideoView.Mode paramMode)
  {
    Preconditions.checkNotNull(paramMode);
    NativeFullScreenVideoView.Mode localMode = a;
    if (localMode == paramMode) {
      return;
    }
    a = paramMode;
    a();
  }
  
  public void setOrientation(int paramInt)
  {
    int i1 = j;
    if (i1 == paramInt) {
      return;
    }
    j = paramInt;
    a();
  }
  
  public void setPlayControlClickListener(View.OnClickListener paramOnClickListener)
  {
    r.setOnClickListener(paramOnClickListener);
    q.setOnClickListener(paramOnClickListener);
  }
  
  public void setPrivacyInformationClickListener(View.OnClickListener paramOnClickListener)
  {
    s.setOnClickListener(paramOnClickListener);
  }
  
  public void setPrivacyInformationIconImageUrl(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      paramString = s;
      localObject = Drawables.NATIVE_PRIVACY_INFORMATION_ICON;
      Context localContext = s.getContext();
      localObject = ((Drawables)localObject).createDrawable(localContext);
      paramString.setImageDrawable((Drawable)localObject);
      return;
    }
    Object localObject = s;
    NativeImageHelper.loadImageView(paramString, (ImageView)localObject);
  }
  
  public void setSurfaceTextureListener(TextureView.SurfaceTextureListener paramSurfaceTextureListener)
  {
    l.setSurfaceTextureListener(paramSurfaceTextureListener);
    SurfaceTexture localSurfaceTexture = l.getSurfaceTexture();
    if ((localSurfaceTexture != null) && (paramSurfaceTextureListener != null))
    {
      TextureView localTextureView1 = l;
      int i1 = localTextureView1.getWidth();
      TextureView localTextureView2 = l;
      int i2 = localTextureView2.getHeight();
      paramSurfaceTextureListener.onSurfaceTextureAvailable(localSurfaceTexture, i1, i2);
    }
  }
  
  public void updateProgress(int paramInt)
  {
    p.updateProgress(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeFullScreenVideoView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */