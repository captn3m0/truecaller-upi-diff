package com.mopub.nativeads;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.mopub.common.logging.MoPubLog;

final class k
{
  static final k h;
  View a;
  TextView b;
  TextView c;
  TextView d;
  ImageView e;
  ImageView f;
  ImageView g;
  
  static
  {
    k localk = new com/mopub/nativeads/k;
    localk.<init>();
    h = localk;
  }
  
  static k a(View paramView, ViewBinder paramViewBinder)
  {
    k localk = new com/mopub/nativeads/k;
    localk.<init>();
    a = paramView;
    try
    {
      int i = b;
      Object localObject = paramView.findViewById(i);
      localObject = (TextView)localObject;
      b = ((TextView)localObject);
      i = c;
      localObject = paramView.findViewById(i);
      localObject = (TextView)localObject;
      c = ((TextView)localObject);
      i = d;
      localObject = paramView.findViewById(i);
      localObject = (TextView)localObject;
      d = ((TextView)localObject);
      i = e;
      localObject = paramView.findViewById(i);
      localObject = (ImageView)localObject;
      e = ((ImageView)localObject);
      i = f;
      localObject = paramView.findViewById(i);
      localObject = (ImageView)localObject;
      f = ((ImageView)localObject);
      int j = g;
      paramView = paramView.findViewById(j);
      paramView = (ImageView)paramView;
      g = paramView;
      return localk;
    }
    catch (ClassCastException paramView)
    {
      MoPubLog.w("Could not cast from id in ViewBinder to expected View type", paramView);
    }
    return h;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */