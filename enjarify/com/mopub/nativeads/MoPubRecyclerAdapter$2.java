package com.mopub.nativeads;

final class MoPubRecyclerAdapter$2
  implements MoPubNativeAdLoadedListener
{
  MoPubRecyclerAdapter$2(MoPubRecyclerAdapter paramMoPubRecyclerAdapter) {}
  
  public final void onAdLoaded(int paramInt)
  {
    MoPubRecyclerAdapter localMoPubRecyclerAdapter = a;
    MoPubNativeAdLoadedListener localMoPubNativeAdLoadedListener = a;
    if (localMoPubNativeAdLoadedListener != null)
    {
      localMoPubNativeAdLoadedListener = a;
      localMoPubNativeAdLoadedListener.onAdLoaded(paramInt);
    }
    localMoPubRecyclerAdapter.notifyItemInserted(paramInt);
  }
  
  public final void onAdRemoved(int paramInt)
  {
    MoPubRecyclerAdapter localMoPubRecyclerAdapter = a;
    MoPubNativeAdLoadedListener localMoPubNativeAdLoadedListener = a;
    if (localMoPubNativeAdLoadedListener != null)
    {
      localMoPubNativeAdLoadedListener = a;
      localMoPubNativeAdLoadedListener.onAdRemoved(paramInt);
    }
    localMoPubRecyclerAdapter.notifyItemRemoved(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubRecyclerAdapter.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */