package com.mopub.nativeads;

import android.text.TextUtils;
import com.mopub.common.logging.MoPubLog;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

final class MoPubCustomEventVideoNative$d
{
  boolean a;
  int b;
  int c;
  int d;
  int e;
  Integer f;
  JSONObject g;
  private int h;
  
  MoPubCustomEventVideoNative$d(Map paramMap)
  {
    boolean bool1 = false;
    Object localObject1 = null;
    Object localObject2 = "play-visible-percent";
    int i;
    try
    {
      localObject2 = paramMap.get(localObject2);
      localObject2 = (String)localObject2;
      i = Integer.parseInt((String)localObject2);
      b = i;
      localObject2 = "pause-visible-percent";
      localObject2 = paramMap.get(localObject2);
      localObject2 = (String)localObject2;
      i = Integer.parseInt((String)localObject2);
      c = i;
      localObject2 = "impression-visible-ms";
      localObject2 = paramMap.get(localObject2);
      localObject2 = (String)localObject2;
      i = Integer.parseInt((String)localObject2);
      e = i;
      localObject2 = "max-buffer-ms";
      localObject2 = paramMap.get(localObject2);
      localObject2 = (String)localObject2;
      i = Integer.parseInt((String)localObject2);
      h = i;
      i = 1;
      a = i;
    }
    catch (NumberFormatException localNumberFormatException1)
    {
      a = false;
    }
    localObject2 = (String)paramMap.get("impression-min-visible-px");
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject2);
    if (!bool2) {
      try
      {
        i = Integer.parseInt((String)localObject2);
        localObject2 = Integer.valueOf(i);
        f = ((Integer)localObject2);
      }
      catch (NumberFormatException localNumberFormatException2)
      {
        localObject2 = "Unable to parse impression min visible px from server extras.";
        MoPubLog.d((String)localObject2);
      }
    }
    localObject2 = "impression-min-visible-percent";
    try
    {
      localObject2 = paramMap.get(localObject2);
      localObject2 = (String)localObject2;
      i = Integer.parseInt((String)localObject2);
      d = i;
    }
    catch (NumberFormatException localNumberFormatException3)
    {
      MoPubLog.d("Unable to parse impression min visible percent from server extras.");
      localObject2 = f;
      if (localObject2 != null)
      {
        i = ((Integer)localObject2).intValue();
        if (i >= 0) {}
      }
      else
      {
        a = false;
      }
    }
    localObject1 = "video-trackers";
    paramMap = (String)paramMap.get(localObject1);
    bool1 = TextUtils.isEmpty(paramMap);
    if (bool1) {
      return;
    }
    try
    {
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>(paramMap);
      g = ((JSONObject)localObject1);
      return;
    }
    catch (JSONException localJSONException)
    {
      paramMap = String.valueOf(paramMap);
      MoPubLog.d("Failed to parse video trackers to JSON: ".concat(paramMap), localJSONException);
      g = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */