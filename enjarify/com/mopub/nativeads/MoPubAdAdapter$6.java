package com.mopub.nativeads;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

final class MoPubAdAdapter$6
  implements AdapterView.OnItemSelectedListener
{
  MoPubAdAdapter$6(MoPubAdAdapter paramMoPubAdAdapter, AdapterView.OnItemSelectedListener paramOnItemSelectedListener) {}
  
  public final void onItemSelected(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    Object localObject = b;
    boolean bool = ((MoPubAdAdapter)localObject).isAd(paramInt);
    if (!bool)
    {
      AdapterView.OnItemSelectedListener localOnItemSelectedListener = a;
      localObject = MoPubAdAdapter.b(b);
      int i = ((MoPubStreamAdPlacer)localObject).getOriginalPosition(paramInt);
      localOnItemSelectedListener.onItemSelected(paramAdapterView, paramView, i, paramLong);
    }
  }
  
  public final void onNothingSelected(AdapterView paramAdapterView)
  {
    a.onNothingSelected(paramAdapterView);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubAdAdapter.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */