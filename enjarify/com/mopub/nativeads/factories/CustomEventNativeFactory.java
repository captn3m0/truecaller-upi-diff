package com.mopub.nativeads.factories;

import com.mopub.common.Preconditions;
import com.mopub.nativeads.CustomEventNative;
import com.mopub.nativeads.MoPubCustomEventNative;
import java.lang.reflect.Constructor;

public class CustomEventNativeFactory
{
  protected static CustomEventNativeFactory a;
  
  static
  {
    CustomEventNativeFactory localCustomEventNativeFactory = new com/mopub/nativeads/factories/CustomEventNativeFactory;
    localCustomEventNativeFactory.<init>();
    a = localCustomEventNativeFactory;
  }
  
  public static CustomEventNative create(String paramString)
  {
    if (paramString != null)
    {
      paramString = Class.forName(paramString).asSubclass(CustomEventNative.class);
      Preconditions.checkNotNull(paramString);
      paramString = paramString.getDeclaredConstructor(null);
      paramString.setAccessible(true);
      Object[] arrayOfObject = new Object[0];
      return (CustomEventNative)paramString.newInstance(arrayOfObject);
    }
    paramString = new com/mopub/nativeads/MoPubCustomEventNative;
    paramString.<init>();
    return paramString;
  }
  
  public static void setInstance(CustomEventNativeFactory paramCustomEventNativeFactory)
  {
    Preconditions.checkNotNull(paramCustomEventNativeFactory);
    a = paramCustomEventNativeFactory;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.factories.CustomEventNativeFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */