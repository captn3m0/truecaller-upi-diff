package com.mopub.nativeads;

import android.view.View;
import android.view.View.OnClickListener;

final class MoPubCustomEventVideoNative$MoPubVideoNativeAd$4
  implements View.OnClickListener
{
  MoPubCustomEventVideoNative$MoPubVideoNativeAd$4(MoPubCustomEventVideoNative.MoPubVideoNativeAd paramMoPubVideoNativeAd) {}
  
  public final void onClick(View paramView)
  {
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.g(a).resetProgress();
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a).seekTo(0L);
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.b(a, false);
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.d(a, false);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.MoPubVideoNativeAd.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */