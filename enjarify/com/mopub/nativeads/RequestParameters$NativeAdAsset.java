package com.mopub.nativeads;

public enum RequestParameters$NativeAdAsset
{
  private final String a;
  
  static
  {
    Object localObject = new com/mopub/nativeads/RequestParameters$NativeAdAsset;
    ((NativeAdAsset)localObject).<init>("TITLE", 0, "title");
    TITLE = (NativeAdAsset)localObject;
    localObject = new com/mopub/nativeads/RequestParameters$NativeAdAsset;
    int i = 1;
    ((NativeAdAsset)localObject).<init>("TEXT", i, "text");
    TEXT = (NativeAdAsset)localObject;
    localObject = new com/mopub/nativeads/RequestParameters$NativeAdAsset;
    int j = 2;
    ((NativeAdAsset)localObject).<init>("ICON_IMAGE", j, "iconimage");
    ICON_IMAGE = (NativeAdAsset)localObject;
    localObject = new com/mopub/nativeads/RequestParameters$NativeAdAsset;
    int k = 3;
    ((NativeAdAsset)localObject).<init>("MAIN_IMAGE", k, "mainimage");
    MAIN_IMAGE = (NativeAdAsset)localObject;
    localObject = new com/mopub/nativeads/RequestParameters$NativeAdAsset;
    int m = 4;
    ((NativeAdAsset)localObject).<init>("CALL_TO_ACTION_TEXT", m, "ctatext");
    CALL_TO_ACTION_TEXT = (NativeAdAsset)localObject;
    localObject = new com/mopub/nativeads/RequestParameters$NativeAdAsset;
    int n = 5;
    ((NativeAdAsset)localObject).<init>("STAR_RATING", n, "starrating");
    STAR_RATING = (NativeAdAsset)localObject;
    localObject = new NativeAdAsset[6];
    NativeAdAsset localNativeAdAsset = TITLE;
    localObject[0] = localNativeAdAsset;
    localNativeAdAsset = TEXT;
    localObject[i] = localNativeAdAsset;
    localNativeAdAsset = ICON_IMAGE;
    localObject[j] = localNativeAdAsset;
    localNativeAdAsset = MAIN_IMAGE;
    localObject[k] = localNativeAdAsset;
    localNativeAdAsset = CALL_TO_ACTION_TEXT;
    localObject[m] = localNativeAdAsset;
    localNativeAdAsset = STAR_RATING;
    localObject[n] = localNativeAdAsset;
    b = (NativeAdAsset[])localObject;
  }
  
  private RequestParameters$NativeAdAsset(String paramString1)
  {
    a = paramString1;
  }
  
  public final String toString()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.RequestParameters.NativeAdAsset
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */