package com.mopub.nativeads;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

final class MoPubCustomEventVideoNative$MoPubVideoNativeAd$7
  implements View.OnClickListener
{
  MoPubCustomEventVideoNative$MoPubVideoNativeAd$7(MoPubCustomEventVideoNative.MoPubVideoNativeAd paramMoPubVideoNativeAd) {}
  
  public final void onClick(View paramView)
  {
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.k(a);
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a).a();
    paramView = MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a);
    Context localContext = MoPubCustomEventVideoNative.MoPubVideoNativeAd.c(a);
    paramView.handleCtaClick(localContext);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.MoPubVideoNativeAd.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */