package com.mopub.nativeads;

import android.view.View;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.logging.MoPubLog;
import java.util.HashMap;
import java.util.Map;

public abstract class StaticNativeAd
  extends BaseNativeAd
  implements ClickInterface, ImpressionInterface
{
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private String h;
  private Double i;
  private String j;
  private String k;
  private boolean l;
  private int m = 1000;
  private int n = 50;
  private Integer o = null;
  private final Map p;
  
  public StaticNativeAd()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    p = localHashMap;
  }
  
  public final void addExtra(String paramString, Object paramObject)
  {
    String str = "addExtra key is not allowed to be null";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramString, str);
    if (!bool) {
      return;
    }
    p.put(paramString, paramObject);
  }
  
  public void clear(View paramView) {}
  
  public void destroy()
  {
    invalidate();
  }
  
  public final String getCallToAction()
  {
    return f;
  }
  
  public final String getClickDestinationUrl()
  {
    return e;
  }
  
  public final Object getExtra(String paramString)
  {
    String str = "getExtra key is not allowed to be null";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramString, str);
    if (!bool) {
      return null;
    }
    return p.get(paramString);
  }
  
  public final Map getExtras()
  {
    HashMap localHashMap = new java/util/HashMap;
    Map localMap = p;
    localHashMap.<init>(localMap);
    return localHashMap;
  }
  
  public final String getIconImageUrl()
  {
    return d;
  }
  
  public final int getImpressionMinPercentageViewed()
  {
    return n;
  }
  
  public final int getImpressionMinTimeViewed()
  {
    return m;
  }
  
  public final Integer getImpressionMinVisiblePx()
  {
    return o;
  }
  
  public final String getMainImageUrl()
  {
    return c;
  }
  
  public final String getPrivacyInformationIconClickThroughUrl()
  {
    return j;
  }
  
  public String getPrivacyInformationIconImageUrl()
  {
    return k;
  }
  
  public final Double getStarRating()
  {
    return i;
  }
  
  public final String getText()
  {
    return h;
  }
  
  public final String getTitle()
  {
    return g;
  }
  
  public void handleClick(View paramView) {}
  
  public final boolean isImpressionRecorded()
  {
    return l;
  }
  
  public void prepare(View paramView) {}
  
  public void recordImpression(View paramView) {}
  
  public final void setCallToAction(String paramString)
  {
    f = paramString;
  }
  
  public final void setClickDestinationUrl(String paramString)
  {
    e = paramString;
  }
  
  public final void setIconImageUrl(String paramString)
  {
    d = paramString;
  }
  
  public final void setImpressionMinPercentageViewed(int paramInt)
  {
    if (paramInt >= 0)
    {
      int i1 = 100;
      if (paramInt <= i1)
      {
        n = paramInt;
        return;
      }
    }
    String str = String.valueOf(paramInt);
    MoPubLog.d("Ignoring impressionMinTimeViewed that's not a percent [0, 100]: ".concat(str));
  }
  
  public final void setImpressionMinTimeViewed(int paramInt)
  {
    if (paramInt > 0)
    {
      m = paramInt;
      return;
    }
    String str = String.valueOf(paramInt);
    MoPubLog.d("Ignoring non-positive impressionMinTimeViewed: ".concat(str));
  }
  
  public final void setImpressionMinVisiblePx(Integer paramInteger)
  {
    if (paramInteger != null)
    {
      int i1 = paramInteger.intValue();
      if (i1 > 0)
      {
        o = paramInteger;
        return;
      }
    }
    paramInteger = String.valueOf(paramInteger);
    MoPubLog.d("Ignoring null or non-positive impressionMinVisiblePx: ".concat(paramInteger));
  }
  
  public final void setImpressionRecorded()
  {
    l = true;
  }
  
  public final void setMainImageUrl(String paramString)
  {
    c = paramString;
  }
  
  public final void setPrivacyInformationIconClickThroughUrl(String paramString)
  {
    j = paramString;
  }
  
  public final void setPrivacyInformationIconImageUrl(String paramString)
  {
    k = paramString;
  }
  
  public final void setStarRating(Double paramDouble)
  {
    if (paramDouble == null)
    {
      i = null;
      return;
    }
    double d1 = paramDouble.doubleValue();
    double d2 = 0.0D;
    boolean bool = d1 < d2;
    if (!bool)
    {
      d1 = paramDouble.doubleValue();
      d2 = 5.0D;
      bool = d1 < d2;
      if (!bool)
      {
        i = paramDouble;
        return;
      }
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Ignoring attempt to set invalid star rating (");
    localStringBuilder.append(paramDouble);
    localStringBuilder.append("). Must be between 0.0 and 5.0.");
    MoPubLog.d(localStringBuilder.toString());
  }
  
  public final void setText(String paramString)
  {
    h = paramString;
  }
  
  public final void setTitle(String paramString)
  {
    g = paramString;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.StaticNativeAd
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */