package com.mopub.nativeads;

public enum MediaLayout$Mode
{
  static
  {
    Object localObject = new com/mopub/nativeads/MediaLayout$Mode;
    ((Mode)localObject).<init>("IMAGE", 0);
    IMAGE = (Mode)localObject;
    localObject = new com/mopub/nativeads/MediaLayout$Mode;
    int i = 1;
    ((Mode)localObject).<init>("PLAYING", i);
    PLAYING = (Mode)localObject;
    localObject = new com/mopub/nativeads/MediaLayout$Mode;
    int j = 2;
    ((Mode)localObject).<init>("LOADING", j);
    LOADING = (Mode)localObject;
    localObject = new com/mopub/nativeads/MediaLayout$Mode;
    int k = 3;
    ((Mode)localObject).<init>("BUFFERING", k);
    BUFFERING = (Mode)localObject;
    localObject = new com/mopub/nativeads/MediaLayout$Mode;
    int m = 4;
    ((Mode)localObject).<init>("PAUSED", m);
    PAUSED = (Mode)localObject;
    localObject = new com/mopub/nativeads/MediaLayout$Mode;
    int n = 5;
    ((Mode)localObject).<init>("FINISHED", n);
    FINISHED = (Mode)localObject;
    localObject = new Mode[6];
    Mode localMode = IMAGE;
    localObject[0] = localMode;
    localMode = PLAYING;
    localObject[i] = localMode;
    localMode = LOADING;
    localObject[j] = localMode;
    localMode = BUFFERING;
    localObject[k] = localMode;
    localMode = PAUSED;
    localObject[m] = localMode;
    localMode = FINISHED;
    localObject[n] = localMode;
    a = (Mode[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MediaLayout.Mode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */