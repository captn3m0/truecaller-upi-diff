package com.mopub.nativeads;

import android.view.View;
import android.view.View.OnClickListener;

final class NativeClickHandler$1
  implements View.OnClickListener
{
  NativeClickHandler$1(NativeClickHandler paramNativeClickHandler, ClickInterface paramClickInterface) {}
  
  public final void onClick(View paramView)
  {
    a.handleClick(paramView);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeClickHandler.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */