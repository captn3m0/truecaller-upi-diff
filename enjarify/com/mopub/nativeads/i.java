package com.mopub.nativeads;

import android.content.Context;
import android.os.Handler;
import com.mopub.common.logging.MoPubLog;
import com.mopub.network.Networking;
import com.mopub.volley.Request;
import com.mopub.volley.RequestQueue;
import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.Response.Listener;

final class i
  implements PositioningSource
{
  int a = 300000;
  final Context b;
  final Handler c;
  final Runnable d;
  PositioningSource.PositioningListener e;
  int f;
  private final Response.Listener g;
  private final Response.ErrorListener h;
  private String i;
  private PositioningRequest j;
  
  i(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
    paramContext = new android/os/Handler;
    paramContext.<init>();
    c = paramContext;
    paramContext = new com/mopub/nativeads/i$1;
    paramContext.<init>(this);
    d = paramContext;
    paramContext = new com/mopub/nativeads/i$2;
    paramContext.<init>(this);
    g = paramContext;
    paramContext = new com/mopub/nativeads/i$3;
    paramContext.<init>(this);
    h = paramContext;
  }
  
  final void a()
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Loading positioning from: ");
    Object localObject2 = i;
    ((StringBuilder)localObject1).append((String)localObject2);
    MoPubLog.d(((StringBuilder)localObject1).toString());
    localObject1 = new com/mopub/nativeads/PositioningRequest;
    localObject2 = b;
    String str = i;
    Response.Listener localListener = g;
    Response.ErrorListener localErrorListener = h;
    ((PositioningRequest)localObject1).<init>((Context)localObject2, str, localListener, localErrorListener);
    j = ((PositioningRequest)localObject1);
    localObject1 = Networking.getRequestQueue(b);
    localObject2 = j;
    ((RequestQueue)localObject1).add((Request)localObject2);
  }
  
  public final void loadPositions(String paramString, PositioningSource.PositioningListener paramPositioningListener)
  {
    Object localObject = j;
    if (localObject != null)
    {
      ((PositioningRequest)localObject).cancel();
      k = 0;
      localObject = null;
      j = null;
    }
    int k = f;
    if (k > 0)
    {
      localObject = c;
      Runnable localRunnable = d;
      ((Handler)localObject).removeCallbacks(localRunnable);
      k = 0;
      localObject = null;
      f = 0;
    }
    e = paramPositioningListener;
    paramPositioningListener = new com/mopub/nativeads/h;
    localObject = b;
    paramPositioningListener.<init>((Context)localObject);
    paramString = paramPositioningListener.withAdUnitId(paramString).generateUrlString("ads.mopub.com");
    i = paramString;
    a();
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */