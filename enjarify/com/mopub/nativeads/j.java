package com.mopub.nativeads;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import com.mopub.common.Preconditions;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Views;

final class j
  extends ViewGroup
{
  private final ProgressBar a;
  private int b;
  
  j(Context paramContext)
  {
    super(paramContext);
    Object localObject = new android/widget/LinearLayout$LayoutParams;
    int i = -1;
    ((LinearLayout.LayoutParams)localObject).<init>(i, i);
    gravity = 17;
    setLayoutParams((ViewGroup.LayoutParams)localObject);
    setVisibility(8);
    setBackgroundColor(-16777216);
    getBackground().setAlpha(180);
    localObject = new android/widget/ProgressBar;
    ((ProgressBar)localObject).<init>(paramContext);
    a = ((ProgressBar)localObject);
    paramContext = getContext();
    int j = Dips.asIntPixels(25.0F, paramContext);
    b = j;
    a.setIndeterminate(true);
    paramContext = a;
    addView(paramContext);
  }
  
  final boolean a()
  {
    Views.removeFromParent(this);
    setVisibility(8);
    return true;
  }
  
  final boolean a(View paramView)
  {
    Preconditions.checkNotNull(paramView);
    paramView = paramView.getRootView();
    int i = 0;
    if (paramView != null)
    {
      boolean bool = paramView instanceof ViewGroup;
      if (bool)
      {
        Object localObject = paramView;
        localObject = (ViewGroup)paramView;
        setVisibility(0);
        i = ((View)paramView).getWidth();
        int j = ((View)paramView).getHeight();
        setMeasuredDimension(i, j);
        ((ViewGroup)localObject).addView(this);
        forceLayout();
        return true;
      }
    }
    return false;
  }
  
  protected final void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (paramBoolean)
    {
      paramInt1 = (paramInt1 + paramInt3) / 2;
      paramInt2 = (paramInt2 + paramInt4) / 2;
      ProgressBar localProgressBar = a;
      paramInt3 = b;
      paramInt4 = paramInt1 - paramInt3;
      int i = paramInt2 - paramInt3;
      paramInt1 += paramInt3;
      paramInt2 += paramInt3;
      localProgressBar.layout(paramInt4, i, paramInt1, paramInt2);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */