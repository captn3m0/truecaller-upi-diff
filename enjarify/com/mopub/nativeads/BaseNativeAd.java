package com.mopub.nativeads;

import android.view.View;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.logging.MoPubLog;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;

public abstract class BaseNativeAd
{
  final Set a;
  BaseNativeAd.NativeEventListener b;
  private final Set c;
  private boolean d;
  
  protected BaseNativeAd()
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    a = localHashSet;
    localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    c = localHashSet;
    d = false;
  }
  
  protected final void a()
  {
    BaseNativeAd.NativeEventListener localNativeEventListener = b;
    if (localNativeEventListener != null) {
      localNativeEventListener.onAdImpressed();
    }
  }
  
  protected final void a(Object paramObject)
  {
    int i = paramObject instanceof JSONArray;
    if (i != 0)
    {
      paramObject = (JSONArray)paramObject;
      i = 0;
      for (;;)
      {
        int k = ((JSONArray)paramObject).length();
        if (i >= k) {
          break;
        }
        try
        {
          str = ((JSONArray)paramObject).getString(i);
          addImpressionTracker(str);
        }
        catch (JSONException localJSONException)
        {
          String str = "Unable to parse impression trackers.";
          MoPubLog.d(str);
        }
        int j;
        i += 1;
      }
      return;
    }
    paramObject = new java/lang/ClassCastException;
    ((ClassCastException)paramObject).<init>("Expected impression trackers of type JSONArray.");
    throw ((Throwable)paramObject);
  }
  
  public final void addClickTracker(String paramString)
  {
    String str = "clickTracker url is not allowed to be null";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramString, str);
    if (!bool) {
      return;
    }
    c.add(paramString);
  }
  
  public final void addImpressionTracker(String paramString)
  {
    String str = "impressionTracker url is not allowed to be null";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramString, str);
    if (!bool) {
      return;
    }
    a.add(paramString);
  }
  
  final Set b()
  {
    HashSet localHashSet = new java/util/HashSet;
    Set localSet = c;
    localHashSet.<init>(localSet);
    return localHashSet;
  }
  
  protected final void b(Object paramObject)
  {
    int i = paramObject instanceof JSONArray;
    if (i != 0)
    {
      paramObject = (JSONArray)paramObject;
      i = 0;
      for (;;)
      {
        int k = ((JSONArray)paramObject).length();
        if (i >= k) {
          break;
        }
        try
        {
          str = ((JSONArray)paramObject).getString(i);
          addClickTracker(str);
        }
        catch (JSONException localJSONException)
        {
          String str = "Unable to parse click trackers.";
          MoPubLog.d(str);
        }
        int j;
        i += 1;
      }
      return;
    }
    paramObject = new java/lang/ClassCastException;
    ((ClassCastException)paramObject).<init>("Expected click trackers of type JSONArray.");
    throw ((Throwable)paramObject);
  }
  
  public abstract void clear(View paramView);
  
  public abstract void destroy();
  
  public void invalidate()
  {
    d = true;
  }
  
  public boolean isInvalidated()
  {
    return d;
  }
  
  public abstract void prepare(View paramView);
  
  public void setNativeEventListener(BaseNativeAd.NativeEventListener paramNativeEventListener)
  {
    b = paramNativeEventListener;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.BaseNativeAd
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */