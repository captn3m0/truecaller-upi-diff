package com.mopub.nativeads;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.VisibilityTracker;
import java.util.WeakHashMap;

public class MoPubAdAdapter
  extends BaseAdapter
{
  MoPubNativeAdLoadedListener a;
  private final WeakHashMap b;
  private final Adapter c;
  private final MoPubStreamAdPlacer d;
  private final VisibilityTracker e;
  
  public MoPubAdAdapter(Activity paramActivity, Adapter paramAdapter)
  {
    this(paramActivity, paramAdapter, localMoPubServerPositioning);
  }
  
  public MoPubAdAdapter(Activity paramActivity, Adapter paramAdapter, MoPubNativeAdPositioning.MoPubClientPositioning paramMoPubClientPositioning)
  {
    this(localMoPubStreamAdPlacer, paramAdapter, paramMoPubClientPositioning);
  }
  
  public MoPubAdAdapter(Activity paramActivity, Adapter paramAdapter, MoPubNativeAdPositioning.MoPubServerPositioning paramMoPubServerPositioning)
  {
    this(localMoPubStreamAdPlacer, paramAdapter, paramMoPubServerPositioning);
  }
  
  private MoPubAdAdapter(MoPubStreamAdPlacer paramMoPubStreamAdPlacer, Adapter paramAdapter, VisibilityTracker paramVisibilityTracker)
  {
    c = paramAdapter;
    d = paramMoPubStreamAdPlacer;
    paramMoPubStreamAdPlacer = new java/util/WeakHashMap;
    paramMoPubStreamAdPlacer.<init>();
    b = paramMoPubStreamAdPlacer;
    e = paramVisibilityTracker;
    paramMoPubStreamAdPlacer = e;
    paramAdapter = new com/mopub/nativeads/MoPubAdAdapter$1;
    paramAdapter.<init>(this);
    paramMoPubStreamAdPlacer.setVisibilityTrackerListener(paramAdapter);
    paramMoPubStreamAdPlacer = c;
    paramAdapter = new com/mopub/nativeads/MoPubAdAdapter$2;
    paramAdapter.<init>(this);
    paramMoPubStreamAdPlacer.registerDataSetObserver(paramAdapter);
    paramMoPubStreamAdPlacer = d;
    paramAdapter = new com/mopub/nativeads/MoPubAdAdapter$3;
    paramAdapter.<init>(this);
    paramMoPubStreamAdPlacer.setAdLoadedListener(paramAdapter);
    paramMoPubStreamAdPlacer = d;
    int i = c.getCount();
    paramMoPubStreamAdPlacer.setItemCount(i);
  }
  
  public boolean areAllItemsEnabled()
  {
    Object localObject = c;
    boolean bool1 = localObject instanceof ListAdapter;
    if (bool1)
    {
      localObject = (ListAdapter)localObject;
      boolean bool2 = ((ListAdapter)localObject).areAllItemsEnabled();
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public void clearAds()
  {
    d.clearAds();
  }
  
  public void destroy()
  {
    d.destroy();
    e.destroy();
  }
  
  public int getAdjustedPosition(int paramInt)
  {
    return d.getAdjustedPosition(paramInt);
  }
  
  public int getCount()
  {
    MoPubStreamAdPlacer localMoPubStreamAdPlacer = d;
    int i = c.getCount();
    return localMoPubStreamAdPlacer.getAdjustedCount(i);
  }
  
  public Object getItem(int paramInt)
  {
    Object localObject = d.getAdData(paramInt);
    if (localObject != null) {
      return localObject;
    }
    localObject = c;
    paramInt = d.getOriginalPosition(paramInt);
    return ((Adapter)localObject).getItem(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    Object localObject = d.getAdData(paramInt);
    if (localObject != null) {
      return -System.identityHashCode(localObject);
    }
    localObject = c;
    paramInt = d.getOriginalPosition(paramInt);
    return ((Adapter)localObject).getItemId(paramInt);
  }
  
  public int getItemViewType(int paramInt)
  {
    Object localObject = d;
    int i = ((MoPubStreamAdPlacer)localObject).getAdViewType(paramInt);
    if (i != 0)
    {
      paramInt = c.getViewTypeCount();
      return i + paramInt + -1;
    }
    localObject = c;
    paramInt = d.getOriginalPosition(paramInt);
    return ((Adapter)localObject).getItemViewType(paramInt);
  }
  
  public int getOriginalPosition(int paramInt)
  {
    return d.getOriginalPosition(paramInt);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject = d.getAdView(paramInt, paramView, paramViewGroup);
    if (localObject == null)
    {
      localObject = c;
      MoPubStreamAdPlacer localMoPubStreamAdPlacer = d;
      int i = localMoPubStreamAdPlacer.getOriginalPosition(paramInt);
      localObject = ((Adapter)localObject).getView(i, paramView, paramViewGroup);
    }
    paramView = b;
    Integer localInteger = Integer.valueOf(paramInt);
    paramView.put(localObject, localInteger);
    e.addView((View)localObject, 0, null);
    return (View)localObject;
  }
  
  public int getViewTypeCount()
  {
    int i = c.getViewTypeCount();
    int j = d.getAdViewTypeCount();
    return i + j;
  }
  
  public boolean hasStableIds()
  {
    return c.hasStableIds();
  }
  
  public void insertItem(int paramInt)
  {
    d.insertItem(paramInt);
  }
  
  public boolean isAd(int paramInt)
  {
    return d.isAd(paramInt);
  }
  
  public boolean isEmpty()
  {
    Object localObject = c;
    boolean bool = ((Adapter)localObject).isEmpty();
    if (bool)
    {
      localObject = d;
      int i = ((MoPubStreamAdPlacer)localObject).getAdjustedCount(0);
      if (i == 0) {
        return true;
      }
    }
    return false;
  }
  
  public boolean isEnabled(int paramInt)
  {
    boolean bool1 = isAd(paramInt);
    if (!bool1)
    {
      Object localObject = c;
      boolean bool2 = localObject instanceof ListAdapter;
      if (bool2)
      {
        localObject = (ListAdapter)localObject;
        MoPubStreamAdPlacer localMoPubStreamAdPlacer = d;
        paramInt = localMoPubStreamAdPlacer.getOriginalPosition(paramInt);
        paramInt = ((ListAdapter)localObject).isEnabled(paramInt);
        if (paramInt != 0) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public void loadAds(String paramString)
  {
    d.loadAds(paramString);
  }
  
  public void loadAds(String paramString, RequestParameters paramRequestParameters)
  {
    d.loadAds(paramString, paramRequestParameters);
  }
  
  public void refreshAds(ListView paramListView, String paramString)
  {
    refreshAds(paramListView, paramString, null);
  }
  
  public void refreshAds(ListView paramListView, String paramString, RequestParameters paramRequestParameters)
  {
    String str = "You called MoPubAdAdapter.refreshAds with a null ListView";
    boolean bool1 = Preconditions.NoThrow.checkNotNull(paramListView, str);
    if (!bool1) {
      return;
    }
    bool1 = false;
    str = null;
    View localView = paramListView.getChildAt(0);
    int j;
    if (localView == null)
    {
      j = 0;
      localView = null;
    }
    else
    {
      j = localView.getTop();
    }
    int k = paramListView.getFirstVisiblePosition();
    int m = Math.max(k + -1, 0);
    for (;;)
    {
      localMoPubStreamAdPlacer1 = d;
      boolean bool2 = localMoPubStreamAdPlacer1.isAd(m);
      if ((!bool2) || (m <= 0)) {
        break;
      }
      m += -1;
    }
    int n = paramListView.getLastVisiblePosition();
    for (;;)
    {
      localMoPubStreamAdPlacer2 = d;
      boolean bool3 = localMoPubStreamAdPlacer2.isAd(n);
      if (!bool3) {
        break;
      }
      i1 = getCount() + -1;
      if (n >= i1) {
        break;
      }
      n += 1;
    }
    m = d.getOriginalPosition(m);
    MoPubStreamAdPlacer localMoPubStreamAdPlacer2 = d;
    n += 1;
    n = localMoPubStreamAdPlacer2.getOriginalCount(n);
    localMoPubStreamAdPlacer2 = d;
    int i2 = getCount();
    int i1 = localMoPubStreamAdPlacer2.getOriginalCount(i2);
    MoPubStreamAdPlacer localMoPubStreamAdPlacer3 = d;
    localMoPubStreamAdPlacer3.removeAdsInRange(n, i1);
    MoPubStreamAdPlacer localMoPubStreamAdPlacer1 = d;
    int i = localMoPubStreamAdPlacer1.removeAdsInRange(0, m);
    if (i > 0)
    {
      k -= i;
      paramListView.setSelectionFromTop(k, j);
    }
    loadAds(paramString, paramRequestParameters);
  }
  
  public final void registerAdRenderer(MoPubAdRenderer paramMoPubAdRenderer)
  {
    String str = "Tried to set a null ad renderer on the placer.";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramMoPubAdRenderer, str);
    if (!bool) {
      return;
    }
    d.registerAdRenderer(paramMoPubAdRenderer);
  }
  
  public void removeItem(int paramInt)
  {
    d.removeItem(paramInt);
  }
  
  public final void setAdLoadedListener(MoPubNativeAdLoadedListener paramMoPubNativeAdLoadedListener)
  {
    a = paramMoPubNativeAdLoadedListener;
  }
  
  public void setOnClickListener(ListView paramListView, AdapterView.OnItemClickListener paramOnItemClickListener)
  {
    Object localObject = "You called MoPubAdAdapter.setOnClickListener with a null ListView";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramListView, (String)localObject);
    if (!bool) {
      return;
    }
    if (paramOnItemClickListener == null)
    {
      paramListView.setOnItemClickListener(null);
      return;
    }
    localObject = new com/mopub/nativeads/MoPubAdAdapter$4;
    ((MoPubAdAdapter.4)localObject).<init>(this, paramOnItemClickListener);
    paramListView.setOnItemClickListener((AdapterView.OnItemClickListener)localObject);
  }
  
  public void setOnItemLongClickListener(ListView paramListView, AdapterView.OnItemLongClickListener paramOnItemLongClickListener)
  {
    Object localObject = "You called MoPubAdAdapter.setOnItemLongClickListener with a null ListView";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramListView, (String)localObject);
    if (!bool) {
      return;
    }
    if (paramOnItemLongClickListener == null)
    {
      paramListView.setOnItemLongClickListener(null);
      return;
    }
    localObject = new com/mopub/nativeads/MoPubAdAdapter$5;
    ((MoPubAdAdapter.5)localObject).<init>(this, paramOnItemLongClickListener);
    paramListView.setOnItemLongClickListener((AdapterView.OnItemLongClickListener)localObject);
  }
  
  public void setOnItemSelectedListener(ListView paramListView, AdapterView.OnItemSelectedListener paramOnItemSelectedListener)
  {
    Object localObject = "You called MoPubAdAdapter.setOnItemSelectedListener with a null ListView";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramListView, (String)localObject);
    if (!bool) {
      return;
    }
    if (paramOnItemSelectedListener == null)
    {
      paramListView.setOnItemSelectedListener(null);
      return;
    }
    localObject = new com/mopub/nativeads/MoPubAdAdapter$6;
    ((MoPubAdAdapter.6)localObject).<init>(this, paramOnItemSelectedListener);
    paramListView.setOnItemSelectedListener((AdapterView.OnItemSelectedListener)localObject);
  }
  
  public void setSelection(ListView paramListView, int paramInt)
  {
    String str = "You called MoPubAdAdapter.setSelection with a null ListView";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramListView, str);
    if (!bool) {
      return;
    }
    paramInt = d.getAdjustedPosition(paramInt);
    paramListView.setSelection(paramInt);
  }
  
  public void smoothScrollToPosition(ListView paramListView, int paramInt)
  {
    String str = "You called MoPubAdAdapter.smoothScrollToPosition with a null ListView";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramListView, str);
    if (!bool) {
      return;
    }
    paramInt = d.getAdjustedPosition(paramInt);
    paramListView.smoothScrollToPosition(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubAdAdapter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */