package com.mopub.nativeads;

import android.content.Context;
import com.mopub.common.BaseUrlGenerator;
import com.mopub.common.ClientMetadata;

final class h
  extends BaseUrlGenerator
{
  private final Context a;
  private String b;
  
  public h(Context paramContext)
  {
    a = paramContext;
  }
  
  public final String generateUrlString(String paramString)
  {
    a(paramString, "/m/pos");
    paramString = b;
    b("id", paramString);
    b("1");
    paramString = ClientMetadata.getInstance(a);
    Object localObject = paramString.getSdkVersion();
    b("nv", (String)localObject);
    localObject = new String[3];
    String str = paramString.getDeviceManufacturer();
    localObject[0] = str;
    str = paramString.getDeviceModel();
    localObject[1] = str;
    str = paramString.getDeviceProduct();
    localObject[2] = str;
    a((String[])localObject);
    paramString = paramString.getAppVersion();
    c(paramString);
    b();
    return f.toString();
  }
  
  public final h withAdUnitId(String paramString)
  {
    b = paramString;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */