package com.mopub.nativeads;

import android.view.View;
import com.mopub.common.Preconditions.NoThrow;
import java.util.HashMap;
import java.util.Map;

public abstract class VideoNativeAd
  extends BaseNativeAd
  implements NativeVideoController.Listener
{
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private String h;
  private String i;
  private String j;
  private String k;
  private final Map l;
  
  public VideoNativeAd()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    l = localHashMap;
  }
  
  public final void addExtra(String paramString, Object paramObject)
  {
    String str = "addExtra key is not allowed to be null";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramString, str);
    if (!bool) {
      return;
    }
    l.put(paramString, paramObject);
  }
  
  public void clear(View paramView) {}
  
  public void destroy() {}
  
  public String getCallToAction()
  {
    return f;
  }
  
  public String getClickDestinationUrl()
  {
    return e;
  }
  
  public final Object getExtra(String paramString)
  {
    String str = "getExtra key is not allowed to be null";
    boolean bool = Preconditions.NoThrow.checkNotNull(paramString, str);
    if (!bool) {
      return null;
    }
    return l.get(paramString);
  }
  
  public final Map getExtras()
  {
    return l;
  }
  
  public String getIconImageUrl()
  {
    return d;
  }
  
  public String getMainImageUrl()
  {
    return c;
  }
  
  public String getPrivacyInformationIconClickThroughUrl()
  {
    return i;
  }
  
  public String getPrivacyInformationIconImageUrl()
  {
    return j;
  }
  
  public String getText()
  {
    return h;
  }
  
  public String getTitle()
  {
    return g;
  }
  
  public String getVastVideo()
  {
    return k;
  }
  
  public void prepare(View paramView) {}
  
  public void render(MediaLayout paramMediaLayout) {}
  
  public void setCallToAction(String paramString)
  {
    f = paramString;
  }
  
  public void setClickDestinationUrl(String paramString)
  {
    e = paramString;
  }
  
  public void setIconImageUrl(String paramString)
  {
    d = paramString;
  }
  
  public void setMainImageUrl(String paramString)
  {
    c = paramString;
  }
  
  public void setPrivacyInformationIconClickThroughUrl(String paramString)
  {
    i = paramString;
  }
  
  public void setPrivacyInformationIconImageUrl(String paramString)
  {
    j = paramString;
  }
  
  public void setText(String paramString)
  {
    h = paramString;
  }
  
  public void setTitle(String paramString)
  {
    g = paramString;
  }
  
  public void setVastVideo(String paramString)
  {
    k = paramString;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.VideoNativeAd
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */