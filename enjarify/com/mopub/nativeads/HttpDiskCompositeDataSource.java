package com.mopub.nativeads;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource.InvalidResponseCodeException;
import com.mopub.common.CacheService;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import java.util.Iterator;
import java.util.TreeSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HttpDiskCompositeDataSource
  implements DataSource
{
  private final HttpDataSource a;
  private byte[] b;
  private String c;
  private final TreeSet d;
  private int e;
  private int f;
  private int g;
  private int h;
  private boolean i;
  private Integer j = null;
  private DataSpec k;
  private boolean l;
  
  private HttpDiskCompositeDataSource(Context paramContext, HttpDataSource paramHttpDataSource)
  {
    a = paramHttpDataSource;
    CacheService.initialize(paramContext);
    paramContext = new java/util/TreeSet;
    paramContext.<init>();
    d = paramContext;
  }
  
  public HttpDiskCompositeDataSource(Context paramContext, String paramString)
  {
    this(paramContext, localDefaultHttpDataSource);
  }
  
  private static int a(int paramInt, TreeSet paramTreeSet)
  {
    Preconditions.checkNotNull(paramTreeSet);
    paramTreeSet = paramTreeSet.iterator();
    for (;;)
    {
      boolean bool = paramTreeSet.hasNext();
      if (!bool) {
        break;
      }
      IntInterval localIntInterval = (IntInterval)paramTreeSet.next();
      int n = localIntInterval.getStart();
      if (n <= paramInt)
      {
        n = localIntInterval.getStart();
        int m = localIntInterval.getLength();
        n += m;
        paramInt = Math.max(paramInt, n);
      }
    }
    return paramInt;
  }
  
  private static Integer a(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    paramString = String.valueOf(paramString);
    paramString = CacheService.getFromDiskCache("expectedsize-".concat(paramString));
    if (paramString != null) {
      try
      {
        String str = new java/lang/String;
        str.<init>(paramString);
        int m = Integer.parseInt(str);
        return Integer.valueOf(m);
      }
      catch (NumberFormatException localNumberFormatException)
      {
        return null;
      }
    }
    return null;
  }
  
  private void a()
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    int m = g;
    ((StringBuilder)localObject1).append(m);
    Object localObject2 = c;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject2 = b;
    CacheService.putToDiskCache((String)localObject1, (byte[])localObject2);
    localObject1 = d;
    m = e;
    int n = f;
    a((TreeSet)localObject1, m, n);
    h = 0;
    m = e;
    n = f;
    m += n;
    e = m;
    f = 0;
    int i1 = e / 512000;
    g = i1;
  }
  
  private static void a(String paramString, TreeSet paramTreeSet)
  {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramTreeSet);
    paramTreeSet.clear();
    String str1 = "intervals-sorted-";
    paramString = String.valueOf(paramString);
    paramString = CacheService.getFromDiskCache(str1.concat(paramString));
    if (paramString != null)
    {
      str1 = new java/lang/String;
      str1.<init>(paramString);
      try
      {
        paramString = new org/json/JSONArray;
        paramString.<init>(str1);
        int m = 0;
        str1 = null;
        for (;;)
        {
          int n = paramString.length();
          if (m >= n) {
            break;
          }
          JSONObject localJSONObject = new org/json/JSONObject;
          Object localObject = paramString.get(m);
          localObject = (String)localObject;
          localJSONObject.<init>((String)localObject);
          localObject = new com/mopub/nativeads/IntInterval;
          String str2 = "start";
          int i1 = localJSONObject.getInt(str2);
          String str3 = "length";
          n = localJSONObject.getInt(str3);
          ((IntInterval)localObject).<init>(i1, n);
          paramTreeSet.add(localObject);
          m += 1;
        }
        return;
      }
      catch (ClassCastException localClassCastException)
      {
        paramString = "clearing cache since unable to read json data";
        MoPubLog.d(paramString);
        paramTreeSet.clear();
      }
      catch (JSONException paramString)
      {
        MoPubLog.d("clearing cache since invalid json intervals found", paramString);
        paramTreeSet.clear();
        return;
      }
    }
  }
  
  private static void a(TreeSet paramTreeSet, int paramInt1, int paramInt2)
  {
    Preconditions.checkNotNull(paramTreeSet);
    int m = a(paramInt1, paramTreeSet);
    int n = paramInt1 + paramInt2;
    if (m >= n) {
      return;
    }
    IntInterval localIntInterval = new com/mopub/nativeads/IntInterval;
    localIntInterval.<init>(paramInt1, paramInt2);
    paramTreeSet.add(localIntInterval);
  }
  
  private static void a(TreeSet paramTreeSet, String paramString)
  {
    Preconditions.checkNotNull(paramTreeSet);
    Preconditions.checkNotNull(paramString);
    JSONArray localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>();
    paramTreeSet = paramTreeSet.iterator();
    for (;;)
    {
      boolean bool = paramTreeSet.hasNext();
      if (!bool) {
        break;
      }
      IntInterval localIntInterval = (IntInterval)paramTreeSet.next();
      localJSONArray.put(localIntInterval);
    }
    paramString = String.valueOf(paramString);
    paramTreeSet = "intervals-sorted-".concat(paramString);
    paramString = localJSONArray.toString().getBytes();
    CacheService.putToDiskCache(paramTreeSet, paramString);
  }
  
  public void close()
  {
    Object localObject1 = c;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        int m = g;
        ((StringBuilder)localObject1).append(m);
        Object localObject2 = c;
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        localObject2 = b;
        CacheService.putToDiskCache((String)localObject1, (byte[])localObject2);
        localObject1 = d;
        m = e;
        int n = f;
        a((TreeSet)localObject1, m, n);
        localObject1 = d;
        localObject2 = c;
        a((TreeSet)localObject1, (String)localObject2);
      }
    }
    b = null;
    a.close();
    i = false;
    e = 0;
    f = 0;
    h = 0;
    j = null;
    l = false;
  }
  
  public Uri getUri()
  {
    DataSpec localDataSpec = k;
    if (localDataSpec != null) {
      return uri;
    }
    return null;
  }
  
  public long open(DataSpec paramDataSpec)
  {
    HttpDiskCompositeDataSource localHttpDiskCompositeDataSource = this;
    Object localObject1 = paramDataSpec;
    Preconditions.checkNotNull(paramDataSpec);
    Object localObject2 = uri;
    long l1 = -1;
    if (localObject2 == null) {
      return l1;
    }
    boolean bool1 = false;
    localObject2 = null;
    l = false;
    k = paramDataSpec;
    Object localObject3 = uri.toString();
    c = ((String)localObject3);
    localObject3 = c;
    if (localObject3 == null) {
      return l1;
    }
    long l2 = absoluteStreamPosition;
    int n = (int)l2;
    e = n;
    int i1 = e;
    n = 512000;
    i1 /= n;
    g = i1;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    int i2 = g;
    ((StringBuilder)localObject3).append(i2);
    Object localObject4 = c;
    ((StringBuilder)localObject3).append((String)localObject4);
    localObject3 = CacheService.getFromDiskCache(((StringBuilder)localObject3).toString());
    b = ((byte[])localObject3);
    i1 = e % n;
    h = i1;
    f = 0;
    localObject3 = a(c);
    j = ((Integer)localObject3);
    localObject3 = c;
    localObject4 = d;
    a((String)localObject3, (TreeSet)localObject4);
    i1 = e;
    localObject4 = d;
    i1 = a(i1, (TreeSet)localObject4);
    localObject4 = b;
    if (localObject4 == null)
    {
      localObject5 = new byte[n];
      b = ((byte[])localObject5);
      n = e;
      if (i1 > n)
      {
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("Cache segment ");
        n = g;
        ((StringBuilder)localObject3).append(n);
        localObject5 = " was evicted. Invalidating cache";
        ((StringBuilder)localObject3).append((String)localObject5);
        MoPubLog.d(((StringBuilder)localObject3).toString());
        localObject3 = d;
        ((TreeSet)localObject3).clear();
        l2 = absoluteStreamPosition;
        i1 = (int)l2;
      }
    }
    Object localObject5 = j;
    int m;
    long l3;
    if (localObject5 != null)
    {
      n = ((Integer)localObject5).intValue();
      if (i1 == n)
      {
        l2 = length;
        bool1 = l2 < l1;
        if (!bool1)
        {
          localObject1 = j;
          i3 = ((Integer)localObject1).intValue();
          m = e;
          i3 -= m;
          l3 = i3;
          break label764;
        }
        l3 = length;
        break label764;
      }
    }
    localObject5 = k;
    long l4 = length;
    boolean bool3 = l4 < l1;
    long l5;
    if (!bool3)
    {
      l4 = l1;
    }
    else
    {
      localObject5 = k;
      l4 = length;
      int i5 = e;
      i5 = i1 - i5;
      l5 = i5;
      l4 -= l5;
    }
    DataSpec localDataSpec = new com/google/android/exoplayer2/upstream/DataSpec;
    Uri localUri = uri;
    long l6 = i1;
    String str = key;
    int i3 = flags;
    localObject2 = localDataSpec;
    localDataSpec.<init>(localUri, l6, l4, str, i3);
    int i4;
    try
    {
      localObject1 = a;
      l5 = ((HttpDataSource)localObject1).open(localDataSpec);
      localObject1 = j;
      if (localObject1 == null)
      {
        boolean bool2 = l4 < l1;
        if (!bool2)
        {
          l3 = e + l5;
          i4 = (int)l3;
          localObject1 = Integer.valueOf(i4);
          j = ((Integer)localObject1);
          localObject1 = new java/lang/StringBuilder;
          localObject2 = "expectedsize-";
          ((StringBuilder)localObject1).<init>((String)localObject2);
          localObject2 = c;
          ((StringBuilder)localObject1).append((String)localObject2);
          localObject1 = ((StringBuilder)localObject1).toString();
          localObject2 = j;
          localObject2 = String.valueOf(localObject2);
          localObject2 = ((String)localObject2).getBytes();
          CacheService.putToDiskCache((String)localObject1, (byte[])localObject2);
        }
      }
      i4 = 1;
      i = i4;
      l3 = l5;
    }
    catch (HttpDataSource.InvalidResponseCodeException localInvalidResponseCodeException)
    {
      m = responseCode;
      int i6 = 416;
      if (m != i6) {
        break label767;
      }
    }
    Integer localInteger = j;
    if (localInteger == null)
    {
      i4 = e;
      i1 -= i4;
      l3 = i1;
    }
    else
    {
      i4 = localInteger.intValue();
      m = e;
      i4 -= m;
      l3 = i4;
    }
    i = false;
    label764:
    return l3;
    label767:
    throw localInteger;
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    HttpDiskCompositeDataSource localHttpDiskCompositeDataSource = this;
    byte[] arrayOfByte1 = paramArrayOfByte;
    int m = paramInt1;
    int n = paramInt2;
    int i1 = -1;
    int i2 = 512000;
    Object localObject1;
    if (paramInt2 > i2)
    {
      localObject1 = String.valueOf(paramInt2);
      MoPubLog.d("Reading more than the block size (512000 bytes) at once is not possible. length = ".concat((String)localObject1));
      return i1;
    }
    Object localObject2 = k;
    if (localObject2 == null)
    {
      MoPubLog.d("Unable to read from data source when no spec provided");
      return i1;
    }
    localObject2 = b;
    if (localObject2 == null)
    {
      MoPubLog.d("No cache set up. Call open before read.");
      return i1;
    }
    int i3 = h;
    i3 = i2 - i3;
    int i5 = f;
    i3 -= i5;
    int i6 = e + i5;
    Object localObject3 = d;
    i5 = a(i6, (TreeSet)localObject3);
    i6 = e;
    i6 = i5 - i6;
    int i7 = f;
    i6 = Math.min(i6 - i7, paramInt2);
    i7 = e;
    int i8 = f;
    i7 += i8;
    i8 = 1;
    int i9 = 0;
    if (i5 > i7)
    {
      i5 = 1;
    }
    else
    {
      i5 = 0;
      localObject3 = null;
    }
    DataSpec localDataSpec;
    if (i5 != 0)
    {
      if (i6 <= i3)
      {
        localObject2 = b;
        i5 = h;
        i7 = f;
        i5 += i7;
        System.arraycopy(localObject2, i5, arrayOfByte1, m, i6);
        i3 = f + i6;
        f = i3;
        i6 += 0;
      }
      else
      {
        localObject3 = b;
        i7 = h;
        int i10 = f;
        i7 += i10;
        System.arraycopy(localObject3, i7, arrayOfByte1, m, i3);
        i5 = f + i3;
        f = i5;
        i3 += 0;
        a();
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        i7 = g;
        ((StringBuilder)localObject3).append(i7);
        String str = c;
        ((StringBuilder)localObject3).append(str);
        localObject3 = CacheService.getFromDiskCache(((StringBuilder)localObject3).toString());
        b = ((byte[])localObject3);
        localObject3 = b;
        if (localObject3 == null)
        {
          MoPubLog.d("Unexpected cache miss. Invalidating cache");
          d.clear();
          localObject3 = new byte[i2];
          b = ((byte[])localObject3);
          a.close();
          localObject3 = a;
          localDataSpec = new com/google/android/exoplayer2/upstream/DataSpec;
          Uri localUri = k.uri;
          i7 = e;
          i9 = f;
          i7 += i9;
          long l1 = i7;
          long l2 = -1;
          str = k.key;
          i9 = k.flags;
          localDataSpec.<init>(localUri, l1, l2, str, i9);
          ((HttpDataSource)localObject3).open(localDataSpec);
          i = i8;
          i6 = i3;
        }
        else
        {
          i7 = h;
          i9 = f;
          i7 += i9;
          i9 = m + i3;
          i3 = i6 - i3;
          System.arraycopy(localObject3, i7, arrayOfByte1, i9, i3);
          i5 = f + i3;
          f = i5;
        }
      }
    }
    else
    {
      i6 = 0;
      localDataSpec = null;
    }
    n -= i6;
    if (n <= 0) {
      return i6;
    }
    l = i8;
    boolean bool = i;
    if (!bool)
    {
      MoPubLog.d("end of cache reached. No http source open");
      return i1;
    }
    Object localObject4 = a;
    int i4 = m + i6;
    n = ((HttpDataSource)localObject4).read(arrayOfByte1, i4, n);
    i1 = h;
    i5 = i2 - i1;
    i7 = f;
    i5 -= i7;
    int i11;
    if (i5 < n)
    {
      byte[] arrayOfByte2 = b;
      i1 += i7;
      System.arraycopy(arrayOfByte1, i4, arrayOfByte2, i1, i5);
      i1 = f + i5;
      f = i1;
      a();
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      i4 = g;
      ((StringBuilder)localObject4).append(i4);
      localObject2 = c;
      ((StringBuilder)localObject4).append((String)localObject2);
      localObject4 = CacheService.getFromDiskCache(((StringBuilder)localObject4).toString());
      b = ((byte[])localObject4);
      localObject4 = b;
      if (localObject4 == null)
      {
        localObject4 = new byte[i2];
        b = ((byte[])localObject4);
      }
      m = m + i5 + i6;
      localObject4 = b;
      i2 = h;
      i4 = f;
      i2 += i4;
      i4 = n - i5;
      System.arraycopy(arrayOfByte1, m, localObject4, i2, i4);
      i11 = f + i4;
      f = i11;
    }
    else
    {
      localObject1 = b;
      i1 += i7;
      System.arraycopy(arrayOfByte1, i4, localObject1, i1, n);
      i11 = f + n;
      f = i11;
    }
    return n + i6;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.HttpDiskCompositeDataSource
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */