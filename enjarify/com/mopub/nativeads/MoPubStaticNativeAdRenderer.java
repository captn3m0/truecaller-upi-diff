package com.mopub.nativeads;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.mopub.common.Preconditions;
import java.util.Map;
import java.util.WeakHashMap;

public class MoPubStaticNativeAdRenderer
  implements MoPubAdRenderer
{
  final WeakHashMap a;
  private final ViewBinder b;
  
  public MoPubStaticNativeAdRenderer(ViewBinder paramViewBinder)
  {
    b = paramViewBinder;
    paramViewBinder = new java/util/WeakHashMap;
    paramViewBinder.<init>();
    a = paramViewBinder;
  }
  
  public View createAdView(Context paramContext, ViewGroup paramViewGroup)
  {
    paramContext = LayoutInflater.from(paramContext);
    int i = b.a;
    return paramContext.inflate(i, paramViewGroup, false);
  }
  
  public void renderAdView(View paramView, StaticNativeAd paramStaticNativeAd)
  {
    Object localObject1 = (k)a.get(paramView);
    if (localObject1 == null)
    {
      localObject1 = b;
      localObject1 = k.a(paramView, (ViewBinder)localObject1);
      localObject2 = a;
      ((WeakHashMap)localObject2).put(paramView, localObject1);
    }
    paramView = b;
    Object localObject2 = paramStaticNativeAd.getTitle();
    NativeRendererHelper.addTextView(paramView, (String)localObject2);
    paramView = c;
    localObject2 = paramStaticNativeAd.getText();
    NativeRendererHelper.addTextView(paramView, (String)localObject2);
    paramView = d;
    localObject2 = paramStaticNativeAd.getCallToAction();
    NativeRendererHelper.addTextView(paramView, (String)localObject2);
    paramView = paramStaticNativeAd.getMainImageUrl();
    localObject2 = e;
    NativeImageHelper.loadImageView(paramView, (ImageView)localObject2);
    paramView = paramStaticNativeAd.getIconImageUrl();
    localObject2 = f;
    NativeImageHelper.loadImageView(paramView, (ImageView)localObject2);
    paramView = g;
    localObject2 = paramStaticNativeAd.getPrivacyInformationIconImageUrl();
    String str = paramStaticNativeAd.getPrivacyInformationIconClickThroughUrl();
    NativeRendererHelper.addPrivacyInformationIcon(paramView, (String)localObject2, str);
    paramView = a;
    localObject2 = b.h;
    paramStaticNativeAd = paramStaticNativeAd.getExtras();
    NativeRendererHelper.updateExtras(paramView, (Map)localObject2, paramStaticNativeAd);
    paramView = a;
    if (paramView != null)
    {
      paramView = a;
      paramStaticNativeAd = null;
      paramView.setVisibility(0);
    }
  }
  
  public boolean supports(BaseNativeAd paramBaseNativeAd)
  {
    Preconditions.checkNotNull(paramBaseNativeAd);
    return paramBaseNativeAd instanceof StaticNativeAd;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubStaticNativeAdRenderer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */