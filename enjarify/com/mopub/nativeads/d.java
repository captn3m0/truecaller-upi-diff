package com.mopub.nativeads;

import android.os.Handler;
import android.os.SystemClock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class d
{
  static final int[] a;
  final MoPubNative.MoPubNativeNetworkListener b;
  boolean c;
  boolean d;
  int e;
  int f;
  d.a g;
  RequestParameters h;
  MoPubNative i;
  final AdRendererRegistry j;
  private final List k;
  private final Handler l;
  private final Runnable m;
  
  static
  {
    int[] arrayOfInt = new int[6];
    arrayOfInt[0] = 'Ϩ';
    arrayOfInt[1] = 'ஸ';
    arrayOfInt[2] = 'ᎈ';
    arrayOfInt[3] = '憨';
    arrayOfInt[4] = 60000;
    arrayOfInt[5] = 300000;
    a = arrayOfInt;
  }
  
  d()
  {
    this(localArrayList, localHandler, localAdRendererRegistry);
  }
  
  private d(List paramList, Handler paramHandler, AdRendererRegistry paramAdRendererRegistry)
  {
    k = paramList;
    l = paramHandler;
    paramList = new com/mopub/nativeads/d$1;
    paramList.<init>(this);
    m = paramList;
    j = paramAdRendererRegistry;
    paramList = new com/mopub/nativeads/d$2;
    paramList.<init>(this);
    b = paramList;
    e = 0;
    f = 0;
  }
  
  final void a()
  {
    Object localObject = i;
    boolean bool = false;
    NativeAd localNativeAd = null;
    if (localObject != null)
    {
      ((MoPubNative)localObject).destroy();
      i = null;
    }
    h = null;
    localObject = k.iterator();
    for (;;)
    {
      bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      localNativeAd = (NativeAd)nexta;
      localNativeAd.destroy();
    }
    k.clear();
    l.removeMessages(0);
    c = false;
    e = 0;
    f = 0;
  }
  
  final NativeAd b()
  {
    long l1 = SystemClock.uptimeMillis();
    boolean bool1 = c;
    Object localObject;
    Runnable localRunnable;
    if (!bool1)
    {
      bool1 = d;
      if (!bool1)
      {
        localObject = l;
        localRunnable = m;
        ((Handler)localObject).post(localRunnable);
      }
    }
    boolean bool2;
    do
    {
      localObject = k;
      bool1 = ((List)localObject).isEmpty();
      if (bool1) {
        break;
      }
      localObject = k;
      localRunnable = null;
      localObject = (l)((List)localObject).remove(0);
      long l2 = b;
      l2 = l1 - l2;
      long l3 = 14400000L;
      bool2 = l2 < l3;
    } while (!bool2);
    return (NativeAd)a;
    return null;
  }
  
  final void c()
  {
    boolean bool = c;
    if (!bool)
    {
      Object localObject = i;
      if (localObject != null)
      {
        localObject = k;
        int n = ((List)localObject).size();
        if (n <= 0)
        {
          n = 1;
          c = n;
          localObject = i;
          RequestParameters localRequestParameters = h;
          int i1 = e;
          Integer localInteger = Integer.valueOf(i1);
          ((MoPubNative)localObject).makeRequest(localRequestParameters, localInteger);
        }
      }
    }
  }
  
  public final MoPubAdRenderer getAdRendererForViewType(int paramInt)
  {
    return j.getRendererForViewType(paramInt);
  }
  
  public final int getViewTypeForAd(NativeAd paramNativeAd)
  {
    return j.getViewTypeForAd(paramNativeAd);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */