package com.mopub.nativeads;

public enum NativeFullScreenVideoView$Mode
{
  static
  {
    Object localObject = new com/mopub/nativeads/NativeFullScreenVideoView$Mode;
    ((Mode)localObject).<init>("LOADING", 0);
    LOADING = (Mode)localObject;
    localObject = new com/mopub/nativeads/NativeFullScreenVideoView$Mode;
    int i = 1;
    ((Mode)localObject).<init>("PLAYING", i);
    PLAYING = (Mode)localObject;
    localObject = new com/mopub/nativeads/NativeFullScreenVideoView$Mode;
    int j = 2;
    ((Mode)localObject).<init>("PAUSED", j);
    PAUSED = (Mode)localObject;
    localObject = new com/mopub/nativeads/NativeFullScreenVideoView$Mode;
    int k = 3;
    ((Mode)localObject).<init>("FINISHED", k);
    FINISHED = (Mode)localObject;
    localObject = new Mode[4];
    Mode localMode = LOADING;
    localObject[0] = localMode;
    localMode = PLAYING;
    localObject[i] = localMode;
    localMode = PAUSED;
    localObject[j] = localMode;
    localMode = FINISHED;
    localObject[k] = localMode;
    a = (Mode[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeFullScreenVideoView.Mode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */