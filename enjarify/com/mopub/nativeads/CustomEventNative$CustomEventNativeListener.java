package com.mopub.nativeads;

public abstract interface CustomEventNative$CustomEventNativeListener
{
  public abstract void onNativeAdFailed(NativeErrorCode paramNativeErrorCode);
  
  public abstract void onNativeAdLoaded(BaseNativeAd paramBaseNativeAd);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.CustomEventNative.CustomEventNativeListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */