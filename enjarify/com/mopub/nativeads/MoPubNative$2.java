package com.mopub.nativeads;

import android.content.Context;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.network.AdLoader.Listener;
import com.mopub.network.AdResponse;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.MoPubNetworkError.Reason;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.VolleyError;
import java.lang.ref.WeakReference;

final class MoPubNative$2
  implements AdLoader.Listener
{
  MoPubNative$2(MoPubNative paramMoPubNative) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    Object localObject1 = a;
    Object localObject2 = "Native ad request failed.";
    MoPubLog.d((String)localObject2, paramVolleyError);
    boolean bool1 = paramVolleyError instanceof MoPubNetworkError;
    if (bool1)
    {
      paramVolleyError = (MoPubNetworkError)paramVolleyError;
      localObject2 = MoPubNative.4.a;
      paramVolleyError = paramVolleyError.getReason();
      int j = paramVolleyError.ordinal();
      j = localObject2[j];
      switch (j)
      {
      default: 
        paramVolleyError = c;
        localObject1 = NativeErrorCode.UNSPECIFIED;
        paramVolleyError.onNativeFail((NativeErrorCode)localObject1);
        return;
      case 4: 
        paramVolleyError = c;
        localObject1 = NativeErrorCode.EMPTY_AD_RESPONSE;
        paramVolleyError.onNativeFail((NativeErrorCode)localObject1);
        return;
      case 3: 
        MoPubLog.c(MoPubErrorCode.WARMUP.toString());
        paramVolleyError = c;
        localObject1 = NativeErrorCode.EMPTY_AD_RESPONSE;
        paramVolleyError.onNativeFail((NativeErrorCode)localObject1);
        return;
      case 2: 
        paramVolleyError = c;
        localObject1 = NativeErrorCode.INVALID_RESPONSE;
        paramVolleyError.onNativeFail((NativeErrorCode)localObject1);
        return;
      }
      paramVolleyError = c;
      localObject1 = NativeErrorCode.INVALID_RESPONSE;
      paramVolleyError.onNativeFail((NativeErrorCode)localObject1);
      return;
    }
    paramVolleyError = networkResponse;
    if (paramVolleyError != null)
    {
      int i = statusCode;
      int k = 500;
      if (i >= k)
      {
        i = statusCode;
        k = 600;
        if (i < k)
        {
          paramVolleyError = c;
          localObject1 = NativeErrorCode.SERVER_ERROR_RESPONSE_CODE;
          paramVolleyError.onNativeFail((NativeErrorCode)localObject1);
          return;
        }
      }
    }
    if (paramVolleyError == null)
    {
      paramVolleyError = (Context)b.get();
      boolean bool2 = DeviceUtils.isNetworkAvailable(paramVolleyError);
      if (!bool2)
      {
        MoPubLog.c(String.valueOf(MoPubErrorCode.NO_CONNECTION.toString()));
        paramVolleyError = c;
        localObject1 = NativeErrorCode.CONNECTION_ERROR;
        paramVolleyError.onNativeFail((NativeErrorCode)localObject1);
        return;
      }
    }
    paramVolleyError = c;
    localObject1 = NativeErrorCode.UNSPECIFIED;
    paramVolleyError.onNativeFail((NativeErrorCode)localObject1);
  }
  
  public final void onSuccess(AdResponse paramAdResponse)
  {
    MoPubNative.a(a, paramAdResponse);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubNative.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */