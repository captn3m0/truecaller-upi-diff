package com.mopub.nativeads;

public abstract interface NativeVideoController$Listener
{
  public abstract void onError(Exception paramException);
  
  public abstract void onStateChanged(boolean paramBoolean, int paramInt);
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeVideoController.Listener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */