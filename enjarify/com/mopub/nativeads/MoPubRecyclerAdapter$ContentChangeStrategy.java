package com.mopub.nativeads;

public enum MoPubRecyclerAdapter$ContentChangeStrategy
{
  static
  {
    Object localObject = new com/mopub/nativeads/MoPubRecyclerAdapter$ContentChangeStrategy;
    ((ContentChangeStrategy)localObject).<init>("INSERT_AT_END", 0);
    INSERT_AT_END = (ContentChangeStrategy)localObject;
    localObject = new com/mopub/nativeads/MoPubRecyclerAdapter$ContentChangeStrategy;
    int i = 1;
    ((ContentChangeStrategy)localObject).<init>("MOVE_ALL_ADS_WITH_CONTENT", i);
    MOVE_ALL_ADS_WITH_CONTENT = (ContentChangeStrategy)localObject;
    localObject = new com/mopub/nativeads/MoPubRecyclerAdapter$ContentChangeStrategy;
    int j = 2;
    ((ContentChangeStrategy)localObject).<init>("KEEP_ADS_FIXED", j);
    KEEP_ADS_FIXED = (ContentChangeStrategy)localObject;
    localObject = new ContentChangeStrategy[3];
    ContentChangeStrategy localContentChangeStrategy = INSERT_AT_END;
    localObject[0] = localContentChangeStrategy;
    localContentChangeStrategy = MOVE_ALL_ADS_WITH_CONTENT;
    localObject[i] = localContentChangeStrategy;
    localContentChangeStrategy = KEEP_ADS_FIXED;
    localObject[j] = localContentChangeStrategy;
    a = (ContentChangeStrategy[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubRecyclerAdapter.ContentChangeStrategy
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */