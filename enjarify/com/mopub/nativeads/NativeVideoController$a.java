package com.mopub.nativeads;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Renderer;
import com.google.android.exoplayer2.trackselection.TrackSelector;

final class NativeVideoController$a
{
  public final ExoPlayer newInstance(Renderer[] paramArrayOfRenderer, TrackSelector paramTrackSelector, LoadControl paramLoadControl)
  {
    return ExoPlayerFactory.newInstance(paramArrayOfRenderer, paramTrackSelector, paramLoadControl);
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.NativeVideoController.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */