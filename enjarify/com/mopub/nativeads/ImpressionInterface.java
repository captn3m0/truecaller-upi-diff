package com.mopub.nativeads;

import android.view.View;

public abstract interface ImpressionInterface
{
  public abstract int getImpressionMinPercentageViewed();
  
  public abstract int getImpressionMinTimeViewed();
  
  public abstract Integer getImpressionMinVisiblePx();
  
  public abstract boolean isImpressionRecorded();
  
  public abstract void recordImpression(View paramView);
  
  public abstract void setImpressionRecorded();
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.ImpressionInterface
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */