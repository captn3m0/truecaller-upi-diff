package com.mopub.nativeads;

import com.mopub.common.Preconditions;
import java.util.ArrayList;

public final class MoPubNativeAdPositioning
{
  static MoPubNativeAdPositioning.MoPubClientPositioning a(MoPubNativeAdPositioning.MoPubClientPositioning paramMoPubClientPositioning)
  {
    Preconditions.checkNotNull(paramMoPubClientPositioning);
    MoPubNativeAdPositioning.MoPubClientPositioning localMoPubClientPositioning = new com/mopub/nativeads/MoPubNativeAdPositioning$MoPubClientPositioning;
    localMoPubClientPositioning.<init>();
    ArrayList localArrayList1 = MoPubNativeAdPositioning.MoPubClientPositioning.a(localMoPubClientPositioning);
    ArrayList localArrayList2 = MoPubNativeAdPositioning.MoPubClientPositioning.a(paramMoPubClientPositioning);
    localArrayList1.addAll(localArrayList2);
    int i = MoPubNativeAdPositioning.MoPubClientPositioning.b(paramMoPubClientPositioning);
    MoPubNativeAdPositioning.MoPubClientPositioning.a(localMoPubClientPositioning, i);
    return localMoPubClientPositioning;
  }
  
  public static MoPubNativeAdPositioning.MoPubClientPositioning clientPositioning()
  {
    MoPubNativeAdPositioning.MoPubClientPositioning localMoPubClientPositioning = new com/mopub/nativeads/MoPubNativeAdPositioning$MoPubClientPositioning;
    localMoPubClientPositioning.<init>();
    return localMoPubClientPositioning;
  }
  
  public static MoPubNativeAdPositioning.MoPubServerPositioning serverPositioning()
  {
    MoPubNativeAdPositioning.MoPubServerPositioning localMoPubServerPositioning = new com/mopub/nativeads/MoPubNativeAdPositioning$MoPubServerPositioning;
    localMoPubServerPositioning.<init>();
    return localMoPubServerPositioning;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubNativeAdPositioning
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */