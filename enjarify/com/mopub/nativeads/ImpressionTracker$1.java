package com.mopub.nativeads;

import android.view.View;
import com.mopub.common.VisibilityTracker.VisibilityTrackerListener;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class ImpressionTracker$1
  implements VisibilityTracker.VisibilityTrackerListener
{
  ImpressionTracker$1(ImpressionTracker paramImpressionTracker) {}
  
  public final void onVisibilityChanged(List paramList1, List paramList2)
  {
    paramList1 = paramList1.iterator();
    Object localObject1;
    for (;;)
    {
      boolean bool1 = paramList1.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (View)paramList1.next();
      Object localObject2 = (ImpressionInterface)ImpressionTracker.a(a).get(localObject1);
      if (localObject2 == null)
      {
        localObject2 = a;
        ((ImpressionTracker)localObject2).removeView((View)localObject1);
      }
      else
      {
        Object localObject3 = (l)ImpressionTracker.b(a).get(localObject1);
        if (localObject3 != null)
        {
          localObject3 = a;
          boolean bool2 = localObject2.equals(localObject3);
          if (bool2) {}
        }
        else
        {
          localObject3 = ImpressionTracker.b(a);
          l locall = new com/mopub/nativeads/l;
          locall.<init>(localObject2);
          ((Map)localObject3).put(localObject1, locall);
        }
      }
    }
    paramList1 = paramList2.iterator();
    for (;;)
    {
      boolean bool3 = paramList1.hasNext();
      if (!bool3) {
        break;
      }
      paramList2 = (View)paramList1.next();
      localObject1 = ImpressionTracker.b(a);
      ((Map)localObject1).remove(paramList2);
    }
    a.a();
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.ImpressionTracker.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */