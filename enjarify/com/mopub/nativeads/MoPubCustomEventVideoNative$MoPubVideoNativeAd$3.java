package com.mopub.nativeads;

import android.graphics.SurfaceTexture;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;

final class MoPubCustomEventVideoNative$MoPubVideoNativeAd$3
  implements TextureView.SurfaceTextureListener
{
  MoPubCustomEventVideoNative$MoPubVideoNativeAd$3(MoPubCustomEventVideoNative.MoPubVideoNativeAd paramMoPubVideoNativeAd) {}
  
  public final void onSurfaceTextureAvailable(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2)
  {
    paramSurfaceTexture = MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a);
    Object localObject = a;
    paramSurfaceTexture.setListener((NativeVideoController.Listener)localObject);
    paramSurfaceTexture = MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a);
    localObject = a;
    paramSurfaceTexture.setOnAudioFocusChangeListener((AudioManager.OnAudioFocusChangeListener)localObject);
    paramSurfaceTexture = MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a);
    localObject = a;
    paramSurfaceTexture.setProgressListener((NativeVideoController.NativeVideoProgressRunnable.ProgressListener)localObject);
    paramSurfaceTexture = MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a);
    localObject = MoPubCustomEventVideoNative.MoPubVideoNativeAd.g(a).getTextureView();
    paramSurfaceTexture.setTextureView((TextureView)localObject);
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.g(a).resetProgress();
    paramSurfaceTexture = MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a);
    long l1 = paramSurfaceTexture.getDuration();
    long l2 = MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a).getCurrentPosition();
    MoPubCustomEventVideoNative.MoPubVideoNativeAd localMoPubVideoNativeAd = a;
    paramInt2 = MoPubCustomEventVideoNative.MoPubVideoNativeAd.h(localMoPubVideoNativeAd);
    boolean bool1 = true;
    int i = 4;
    if (paramInt2 != i)
    {
      long l3 = 0L;
      paramInt2 = l1 < l3;
      if (paramInt2 > 0)
      {
        l1 -= l2;
        l2 = 750L;
        paramInt2 = l1 < l2;
        if (paramInt2 >= 0) {}
      }
    }
    else
    {
      paramSurfaceTexture = a;
      MoPubCustomEventVideoNative.MoPubVideoNativeAd.b(paramSurfaceTexture, bool1);
    }
    paramSurfaceTexture = a;
    boolean bool2 = MoPubCustomEventVideoNative.MoPubVideoNativeAd.i(paramSurfaceTexture);
    if (bool2)
    {
      MoPubCustomEventVideoNative.MoPubVideoNativeAd.c(a, false);
      paramSurfaceTexture = MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a);
      localObject = a;
      paramSurfaceTexture.prepare(localObject);
    }
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.d(a, bool1);
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.b(a);
  }
  
  public final boolean onSurfaceTextureDestroyed(SurfaceTexture paramSurfaceTexture)
  {
    paramSurfaceTexture = a;
    boolean bool = true;
    MoPubCustomEventVideoNative.MoPubVideoNativeAd.c(paramSurfaceTexture, bool);
    paramSurfaceTexture = MoPubCustomEventVideoNative.MoPubVideoNativeAd.f(a);
    Object localObject = a;
    paramSurfaceTexture.release(localObject);
    paramSurfaceTexture = a;
    localObject = MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState.PAUSED;
    paramSurfaceTexture.a((MoPubCustomEventVideoNative.MoPubVideoNativeAd.VideoState)localObject, false);
    return bool;
  }
  
  public final void onSurfaceTextureSizeChanged(SurfaceTexture paramSurfaceTexture, int paramInt1, int paramInt2) {}
  
  public final void onSurfaceTextureUpdated(SurfaceTexture paramSurfaceTexture) {}
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubCustomEventVideoNative.MoPubVideoNativeAd.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */