package com.mopub.nativeads;

import com.mopub.common.logging.MoPubLog;
import java.util.Iterator;
import java.util.List;

final class MoPubStreamAdPlacer$3
  implements PositioningSource.PositioningListener
{
  MoPubStreamAdPlacer$3(MoPubStreamAdPlacer paramMoPubStreamAdPlacer) {}
  
  public final void onFailed()
  {
    MoPubLog.d("Unable to show ads because ad positions could not be loaded from the MoPub ad server.");
  }
  
  public final void onLoad(MoPubNativeAdPositioning.MoPubClientPositioning paramMoPubClientPositioning)
  {
    MoPubStreamAdPlacer localMoPubStreamAdPlacer = a;
    Object localObject = a;
    int i = b;
    int j = -1 >>> 1;
    if (i == j) {
      j = ((List)localObject).size();
    } else {
      j = 200;
    }
    int[] arrayOfInt = new int[j];
    localObject = ((List)localObject).iterator();
    int k = 0;
    int m = 0;
    Integer localInteger = null;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject).hasNext();
      if (!bool2) {
        break;
      }
      localInteger = (Integer)((Iterator)localObject).next();
      m = localInteger.intValue() - k;
      int n = k + 1;
      arrayOfInt[k] = m;
      k = n;
    }
    int i1;
    for (;;)
    {
      i1 = 1;
      if (k >= j) {
        break;
      }
      m = m + i - i1;
      i1 = k + 1;
      arrayOfInt[k] = m;
      k = i1;
    }
    paramMoPubClientPositioning = new com/mopub/nativeads/g;
    paramMoPubClientPositioning.<init>(arrayOfInt);
    boolean bool1 = c;
    if (bool1) {
      localMoPubStreamAdPlacer.a(paramMoPubClientPositioning);
    } else {
      b = paramMoPubClientPositioning;
    }
    a = i1;
  }
}

/* Location:
 * Qualified Name:     com.mopub.nativeads.MoPubStreamAdPlacer.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */