package com.a.a.a;

public final class g
{
  public int a;
  public long b;
  public Object c;
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[cursor, index: ");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", key: ");
    long l = b;
    localStringBuilder.append(l);
    localStringBuilder.append(", value: ");
    Object localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.a.a.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */