package com.a.a;

import com.a.a.a.c;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

public class n
  implements o, Cloneable
{
  public char[] a;
  public Object[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public n()
  {
    this((byte)0);
  }
  
  private n(byte paramByte)
  {
    this(4, 0.75D);
  }
  
  public n(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, locals);
  }
  
  private n(int paramInt, double paramDouble, s params)
  {
    i = params;
    q.a(paramDouble);
    h = paramDouble;
    int k = f;
    char[] arrayOfChar;
    if (paramInt <= k)
    {
      arrayOfChar = a;
      if (arrayOfChar != null) {}
    }
    else
    {
      arrayOfChar = a;
      Object[] arrayOfObject = (Object[])b;
      double d1 = h;
      paramInt = q.a(paramInt, d1);
      a(paramInt);
      if (arrayOfChar != null)
      {
        paramInt = b();
        if (paramInt == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(arrayOfChar, arrayOfObject);
        }
      }
    }
  }
  
  private void a(int paramInt)
  {
    boolean bool = j;
    int m = 1;
    Object localObject1;
    if (!bool)
    {
      k = Integer.bitCount(paramInt);
      if (k != m)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    s locals = i;
    int k = locals.a(paramInt);
    Object localObject2 = a;
    Object[] arrayOfObject1 = (Object[])b;
    int n = paramInt + 1;
    try
    {
      localObject3 = new char[n];
      a = ((char[])localObject3);
      Object[] arrayOfObject2 = new Object[n];
      arrayOfObject2 = (Object[])arrayOfObject2;
      b = arrayOfObject2;
      double d1 = h;
      int i1 = q.b(paramInt, d1);
      f = i1;
      c = k;
      paramInt -= m;
      e = paramInt;
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = ((char[])localObject2);
      b = arrayOfObject1;
      localObject2 = new com/a/a/f;
      arrayOfObject1 = new Object[2];
      Object localObject3 = Integer.valueOf(e + m);
      arrayOfObject1[0] = localObject3;
      localObject1 = Integer.valueOf(paramInt);
      arrayOfObject1[m] = localObject1;
      ((f)localObject2).<init>("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, arrayOfObject1);
      throw ((Throwable)localObject2);
    }
  }
  
  private void a(char[] paramArrayOfChar, Object[] paramArrayOfObject)
  {
    boolean bool = j;
    if (!bool)
    {
      int k = paramArrayOfChar.length;
      int m = paramArrayOfObject.length;
      if (k == m)
      {
        k = paramArrayOfChar.length + -1;
        q.a(k);
      }
      else
      {
        paramArrayOfChar = new java/lang/AssertionError;
        paramArrayOfChar.<init>();
        throw paramArrayOfChar;
      }
    }
    char[] arrayOfChar = a;
    Object[] arrayOfObject = (Object[])b;
    int n = e;
    int i1 = paramArrayOfChar.length + -1;
    int i2 = arrayOfChar.length + -1;
    int i3 = paramArrayOfChar[i1];
    arrayOfChar[i2] = i3;
    i2 = arrayOfObject.length + -1;
    Object localObject1 = paramArrayOfObject[i1];
    arrayOfObject[i2] = localObject1;
    for (;;)
    {
      i1 += -1;
      if (i1 < 0) {
        break;
      }
      i2 = paramArrayOfChar[i1];
      if (i2 != 0)
      {
        for (i3 = b(i2) & n;; i3 = i3 + 1 & n)
        {
          int i4 = arrayOfChar[i3];
          if (i4 == 0) {
            break;
          }
        }
        arrayOfChar[i3] = i2;
        Object localObject2 = paramArrayOfObject[i1];
        arrayOfObject[i3] = localObject2;
      }
    }
  }
  
  private int b()
  {
    int k = d;
    int m = g;
    return k + m;
  }
  
  private int b(char paramChar)
  {
    boolean bool = j;
    if ((!bool) && (paramChar == 0))
    {
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>();
      throw localAssertionError;
    }
    int k = c;
    return d.a(paramChar, k);
  }
  
  private n c()
  {
    try
    {
      Object localObject1 = super.clone();
      localObject1 = (n)localObject1;
      localObject2 = a;
      localObject2 = ((char[])localObject2).clone();
      localObject2 = (char[])localObject2;
      a = ((char[])localObject2);
      localObject2 = b;
      localObject2 = ((Object[])localObject2).clone();
      localObject2 = (Object[])localObject2;
      b = ((Object[])localObject2);
      boolean bool = g;
      g = bool;
      localObject2 = i;
      localObject2 = ((s)localObject2).a();
      i = ((s)localObject2);
      return (n)localObject1;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      Object localObject2 = new java/lang/RuntimeException;
      ((RuntimeException)localObject2).<init>(localCloneNotSupportedException);
      throw ((Throwable)localObject2);
    }
  }
  
  public final Object a(char paramChar)
  {
    int k = 0;
    if (paramChar == 0)
    {
      paramChar = g;
      if (paramChar != 0)
      {
        Object[] arrayOfObject = b;
        k = e + 1;
        return arrayOfObject[k];
      }
      return null;
    }
    char[] arrayOfChar = a;
    int m = e;
    for (int n = b(paramChar) & m;; n = n + 1 & m)
    {
      char c1 = arrayOfChar[n];
      if (c1 == 0) {
        break;
      }
      if (c1 == paramChar) {
        return b[n];
      }
    }
    return null;
  }
  
  public final Object a(char paramChar, Object paramObject)
  {
    boolean bool1 = j;
    int k = 1;
    Object localObject1;
    if (!bool1)
    {
      c1 = d;
      int m = e + k;
      if (c1 >= m)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    char c1 = e;
    if (paramChar == 0)
    {
      g = k;
      localObject1 = b;
      c1 += k;
      Object localObject2 = localObject1[c1];
      localObject1[c1] = paramObject;
      return localObject2;
    }
    Object localObject3 = a;
    Object localObject4;
    for (int n = b(paramChar) & c1;; n = n + 1 & c1)
    {
      c2 = localObject3[n];
      if (c2 == 0) {
        break;
      }
      if (c2 == paramChar)
      {
        localObject1 = b;
        localObject4 = localObject1[n];
        localObject1[n] = paramObject;
        return localObject4;
      }
    }
    c1 = d;
    char c2 = f;
    if (c1 == c2)
    {
      boolean bool2 = j;
      if (!bool2) {
        if (c1 == c2)
        {
          localObject4 = a;
          c1 = localObject4[n];
          if ((c1 == 0) && (paramChar != 0)) {}
        }
        else
        {
          localObject1 = new java/lang/AssertionError;
          ((AssertionError)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
      }
      localObject4 = a;
      localObject3 = (Object[])b;
      c2 = e + k;
      int i2 = b();
      double d1 = h;
      c2 = q.a(c2, i2, d1);
      a(c2);
      boolean bool3 = j;
      if (!bool3)
      {
        char[] arrayOfChar = a;
        int i1 = arrayOfChar.length;
        i2 = localObject4.length;
        if (i1 <= i2)
        {
          localObject1 = new java/lang/AssertionError;
          ((AssertionError)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
      }
      localObject4[n] = paramChar;
      localObject3[n] = paramObject;
      a((char[])localObject4, (Object[])localObject3);
    }
    else
    {
      localObject3[n] = paramChar;
      localObject1 = b;
      localObject1[n] = paramObject;
    }
    paramChar = d + k;
    d = paramChar;
    return null;
  }
  
  public final void a()
  {
    d = 0;
    g = false;
    Arrays.fill(a, '\000');
    Arrays.fill(b, null);
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        localObject1 = getClass();
        paramObject = (n)((Class)localObject1).cast(paramObject);
        int k = ((n)paramObject).b();
        int m = b();
        boolean bool2 = true;
        int n;
        if (k != m)
        {
          n = 0;
          paramObject = null;
        }
        else
        {
          paramObject = ((n)paramObject).iterator();
          boolean bool1;
          label177:
          do
          {
            bool1 = ((Iterator)paramObject).hasNext();
            if (!bool1) {
              break label214;
            }
            localObject1 = (c)((Iterator)paramObject).next();
            m = b;
            boolean bool3;
            if (m == 0)
            {
              bool3 = g;
            }
            else
            {
              char[] arrayOfChar = a;
              int i1 = e;
              for (int i2 = b(m) & i1;; i2 = i2 + 1 & i1)
              {
                int i3 = arrayOfChar[i2];
                if (i3 == 0) {
                  break;
                }
                if (i3 == m)
                {
                  bool3 = true;
                  break label177;
                }
              }
              bool3 = false;
              arrayOfChar = null;
            }
            if (!bool3) {
              break;
            }
            localObject2 = a(m);
            localObject1 = c;
            bool1 = Objects.equals(localObject2, localObject1);
          } while (bool1);
          n = 0;
          paramObject = null;
          break label217;
          label214:
          n = 1;
        }
        label217:
        if (n != 0) {
          return bool2;
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    boolean bool1 = g;
    int k;
    if (bool1) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject = (c)localIterator.next();
      int n = d.a(b);
      localObject = c;
      int m = d.a(localObject);
      n += m;
      k += n;
    }
    return k;
  }
  
  public Iterator iterator()
  {
    n.a locala = new com/a/a/n$a;
    locala.<init>(this);
    return locala;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    char c1 = '\001';
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      c localc = (c)localIterator.next();
      if (c1 == 0)
      {
        localObject = ", ";
        localStringBuilder.append((String)localObject);
      }
      c1 = b;
      localStringBuilder.append(c1);
      localStringBuilder.append("=>");
      Object localObject = c;
      localStringBuilder.append(localObject);
      c1 = '\000';
      localObject = null;
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.a.a.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */