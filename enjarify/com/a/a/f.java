package com.a.a;

import java.util.IllegalFormatException;
import java.util.Locale;

public final class f
  extends RuntimeException
{
  private f(String paramString)
  {
    super(paramString);
  }
  
  public f(String paramString, Throwable paramThrowable, Object... paramVarArgs)
  {
    super(paramString, paramThrowable);
  }
  
  public f(String paramString, Object... paramVarArgs)
  {
    this(paramString, null, paramVarArgs);
  }
  
  private static String a(String paramString, Throwable paramThrowable, Object... paramVarArgs)
  {
    try
    {
      localObject = Locale.ROOT;
      return String.format((Locale)localObject, paramString, paramVarArgs);
    }
    catch (IllegalFormatException paramVarArgs)
    {
      Object localObject = new com/a/a/f;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(paramString);
      localStringBuilder.append(" [ILLEGAL FORMAT, ARGS SUPPRESSED]");
      paramString = localStringBuilder.toString();
      ((f)localObject).<init>(paramString);
      if (paramThrowable != null) {
        ((f)localObject).addSuppressed(paramThrowable);
      }
      ((f)localObject).addSuppressed(paramVarArgs);
      throw ((Throwable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.a.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */