package com.a.a;

public final class q
{
  static int a(int paramInt, double paramDouble)
  {
    if (paramInt >= 0)
    {
      double d = paramInt;
      Double.isNaN(d);
      d = Math.ceil(d / paramDouble);
      long l1 = d;
      long l2 = paramInt;
      long l3 = 1L;
      boolean bool1 = l1 < l2;
      if (!bool1) {
        l1 += l3;
      }
      l1 -= l3;
      bool1 = true;
      long l4 = l1 >> bool1;
      l1 |= l4;
      int i = 2;
      long l5 = l1 >> i;
      l1 |= l5;
      l5 = l1 >> 4;
      l1 |= l5;
      l5 = l1 >> 8;
      l1 |= l5;
      l5 = l1 >> 16;
      l1 |= l5;
      int j = 32;
      l5 = l1 >> j;
      l1 = (l1 | l5) + l3;
      l1 = Math.max(4, l1);
      l2 = 1073741824L;
      boolean bool2 = l1 < l2;
      if (!bool2) {
        return (int)l1;
      }
      f localf = new com/a/a/f;
      Object[] arrayOfObject = new Object[i];
      localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      localObject = Double.valueOf(paramDouble);
      arrayOfObject[bool1] = localObject;
      localf.<init>("Maximum array size exceeded for this load factor (elements: %d, load factor: %f)", arrayOfObject);
      throw localf;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    Object localObject = String.valueOf(paramInt);
    localObject = "Number of elements must be >= 0: ".concat((String)localObject);
    localIllegalArgumentException.<init>((String)localObject);
    throw localIllegalArgumentException;
  }
  
  static int a(int paramInt1, int paramInt2, double paramDouble)
  {
    boolean bool = a;
    if (!bool) {
      a(paramInt1);
    }
    int i = 1073741824;
    int j = 1;
    if (paramInt1 != i) {
      return paramInt1 << j;
    }
    f localf = new com/a/a/f;
    Object[] arrayOfObject = new Object[2];
    Object localObject = Integer.valueOf(paramInt2);
    arrayOfObject[0] = localObject;
    localObject = Double.valueOf(paramDouble);
    arrayOfObject[j] = localObject;
    localf.<init>("Maximum array size exceeded for this load factor (elements: %d, load factor: %f)", arrayOfObject);
    throw localf;
  }
  
  static void a(double paramDouble)
  {
    double d1 = 0.9900000095367432D;
    double d2 = 0.009999999776482582D;
    boolean bool = paramDouble < d2;
    if (!bool)
    {
      bool = paramDouble < d1;
      if (!bool) {
        return;
      }
    }
    f localf = new com/a/a/f;
    Object[] arrayOfObject = new Object[3];
    Double localDouble1 = Double.valueOf(d2);
    arrayOfObject[0] = localDouble1;
    Double localDouble2 = Double.valueOf(d1);
    arrayOfObject[1] = localDouble2;
    Double localDouble3 = Double.valueOf(paramDouble);
    arrayOfObject[2] = localDouble3;
    localf.<init>("The load factor should be in range [%.2f, %.2f]: %f", arrayOfObject);
    throw localf;
  }
  
  static boolean a(int paramInt)
  {
    boolean bool = a;
    int j = 1;
    AssertionError localAssertionError;
    if ((!bool) && (paramInt <= j))
    {
      localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>();
      throw localAssertionError;
    }
    bool = a;
    if (!bool)
    {
      int i = e.a(paramInt);
      if (i != paramInt)
      {
        localAssertionError = new java/lang/AssertionError;
        localAssertionError.<init>();
        throw localAssertionError;
      }
    }
    return j;
  }
  
  static int b(int paramInt, double paramDouble)
  {
    boolean bool = a;
    if (!bool) {
      a(paramInt);
    }
    int i = paramInt + -1;
    double d = paramInt;
    Double.isNaN(d);
    paramInt = (int)Math.ceil(d * paramDouble);
    return Math.min(i, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.a.a.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */