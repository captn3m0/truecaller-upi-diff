package com.a.a;

import com.a.a.a.e;
import java.util.Iterator;

public class y
  extends b
  implements ac, ag, Cloneable
{
  public long[] a;
  protected int b;
  protected int c;
  protected int d;
  protected int e;
  protected boolean f;
  protected double g;
  protected s h;
  
  public y()
  {
    this(4, 0.75D);
  }
  
  public y(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, locals);
  }
  
  private y(int paramInt, double paramDouble, s params)
  {
    h = params;
    q.a(paramDouble);
    g = paramDouble;
    int j = e;
    long[] arrayOfLong;
    if (paramInt <= j)
    {
      arrayOfLong = a;
      if (arrayOfLong != null) {}
    }
    else
    {
      arrayOfLong = a;
      double d1 = g;
      paramInt = q.a(paramInt, d1);
      a(paramInt);
      if (arrayOfLong != null)
      {
        paramInt = b();
        if (paramInt == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(arrayOfLong);
        }
      }
    }
  }
  
  private void a(int paramInt)
  {
    boolean bool = i;
    int k = 1;
    Object localObject1;
    if (!bool)
    {
      j = Integer.bitCount(paramInt);
      if (j != k)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    s locals = h;
    int j = locals.a(paramInt);
    Object localObject2 = a;
    int m = paramInt + 1;
    try
    {
      localObject3 = new long[m];
      a = ((long[])localObject3);
      double d1 = g;
      int n = q.b(paramInt, d1);
      e = n;
      d = j;
      paramInt -= k;
      c = paramInt;
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = ((long[])localObject2);
      localObject2 = new com/a/a/f;
      m = 2;
      Object localObject3 = new Object[m];
      Object localObject4 = a;
      int i1;
      if (localObject4 == null)
      {
        i1 = 0;
        localObject4 = null;
      }
      else
      {
        i1 = b();
      }
      localObject4 = Integer.valueOf(i1);
      localObject3[0] = localObject4;
      localObject1 = Integer.valueOf(paramInt);
      localObject3[k] = localObject1;
      ((f)localObject2).<init>("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, (Object[])localObject3);
      throw ((Throwable)localObject2);
    }
  }
  
  private void a(long[] paramArrayOfLong)
  {
    boolean bool1 = i;
    if (!bool1)
    {
      int j = paramArrayOfLong.length + -1;
      q.a(j);
    }
    long[] arrayOfLong = a;
    int k = c;
    int m = paramArrayOfLong.length + -1;
    for (;;)
    {
      m += -1;
      if (m < 0) {
        break;
      }
      long l1 = paramArrayOfLong[m];
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (bool2)
      {
        for (int n = c(l1) & k;; n = n + 1 & k)
        {
          long l3 = arrayOfLong[n];
          boolean bool3 = l3 < l2;
          if (!bool3) {
            break;
          }
        }
        arrayOfLong[n] = l1;
      }
    }
  }
  
  private int c(long paramLong)
  {
    boolean bool1 = i;
    if (!bool1)
    {
      long l = 0L;
      boolean bool2 = paramLong < l;
      if (!bool2)
      {
        AssertionError localAssertionError = new java/lang/AssertionError;
        localAssertionError.<init>();
        throw localAssertionError;
      }
    }
    int j = d;
    return d.a(paramLong, j);
  }
  
  private y c()
  {
    try
    {
      Object localObject1 = super.clone();
      localObject1 = (y)localObject1;
      localObject2 = a;
      localObject2 = ((long[])localObject2).clone();
      localObject2 = (long[])localObject2;
      a = ((long[])localObject2);
      boolean bool = f;
      f = bool;
      localObject2 = h;
      localObject2 = ((s)localObject2).a();
      h = ((s)localObject2);
      return (y)localObject1;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      Object localObject2 = new java/lang/RuntimeException;
      ((RuntimeException)localObject2).<init>(localCloneNotSupportedException);
      throw ((Throwable)localObject2);
    }
  }
  
  public final boolean a(long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1) {
      return f;
    }
    long[] arrayOfLong = a;
    int j = c;
    for (int k = c(paramLong) & j;; k = k + 1 & j)
    {
      long l2 = arrayOfLong[k];
      boolean bool2 = l2 < l1;
      if (!bool2) {
        break;
      }
      bool2 = l2 < paramLong;
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final long[] a()
  {
    int j = b();
    long[] arrayOfLong1 = new long[j];
    boolean bool1 = f;
    long l1 = 0L;
    int k = 0;
    if (bool1)
    {
      arrayOfLong1[0] = l1;
      bool1 = true;
    }
    else
    {
      bool1 = false;
    }
    long[] arrayOfLong2 = a;
    int m = c;
    while (k <= m)
    {
      long l2 = arrayOfLong2[k];
      boolean bool2 = l2 < l1;
      if (bool2)
      {
        int n = bool1 + true;
        arrayOfLong1[bool1] = l2;
        bool1 = n;
      }
      k += 1;
    }
    return arrayOfLong1;
  }
  
  public final int b()
  {
    int j = b;
    int k = f;
    return j + k;
  }
  
  public final boolean b(long paramLong)
  {
    long l1 = 0L;
    int j = 1;
    boolean bool1 = paramLong < l1;
    Object localObject;
    if (!bool1)
    {
      boolean bool2 = i;
      if (!bool2)
      {
        localObject = a;
        int n = c + j;
        long l2 = localObject[n];
        bool2 = l2 < l1;
        if (bool2)
        {
          localObject = new java/lang/AssertionError;
          ((AssertionError)localObject).<init>();
          throw ((Throwable)localObject);
        }
      }
      bool2 = f ^ j;
      f = j;
      return bool2;
    }
    long[] arrayOfLong1 = a;
    int i1 = c;
    long l3;
    for (int i2 = c(paramLong) & i1;; i2 = i2 + 1 & i1)
    {
      l3 = arrayOfLong1[i2];
      boolean bool3 = l3 < l1;
      if (!bool3) {
        break;
      }
      bool3 = l3 < paramLong;
      if (!bool3) {
        return false;
      }
    }
    i1 = b;
    int i3 = e;
    if (i1 == i3)
    {
      bool1 = i;
      if (!bool1)
      {
        if (i1 == i3)
        {
          arrayOfLong1 = a;
          l3 = arrayOfLong1[i2];
          bool1 = l3 < l1;
          if (!bool1)
          {
            bool1 = paramLong < l1;
            if (bool1) {
              break label243;
            }
          }
        }
        localObject = new java/lang/AssertionError;
        ((AssertionError)localObject).<init>();
        throw ((Throwable)localObject);
      }
      label243:
      long[] arrayOfLong2 = a;
      int i4 = c + j;
      int k = b();
      double d1 = g;
      i4 = q.a(i4, k, d1);
      a(i4);
      boolean bool4 = i;
      if (!bool4)
      {
        long[] arrayOfLong3 = a;
        int i5 = arrayOfLong3.length;
        k = arrayOfLong2.length;
        if (i5 <= k)
        {
          localObject = new java/lang/AssertionError;
          ((AssertionError)localObject).<init>();
          throw ((Throwable)localObject);
        }
      }
      arrayOfLong2[i2] = paramLong;
      a(arrayOfLong2);
    }
    else
    {
      arrayOfLong1[i2] = paramLong;
    }
    int m = b + j;
    b = m;
    return j;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject != null)
    {
      Object localObject = getClass();
      Class localClass = paramObject.getClass();
      if (localObject == localClass)
      {
        localObject = getClass();
        paramObject = (ag)((Class)localObject).cast(paramObject);
        int j = ((ag)paramObject).b();
        int k = b();
        boolean bool2 = true;
        int m;
        if (j != k)
        {
          m = 0;
          paramObject = null;
        }
        else
        {
          paramObject = ((ag)paramObject).iterator();
          boolean bool1;
          do
          {
            bool1 = ((Iterator)paramObject).hasNext();
            if (!bool1) {
              break;
            }
            localObject = (e)((Iterator)paramObject).next();
            long l = b;
            bool1 = a(l);
          } while (bool1);
          m = 0;
          paramObject = null;
          break label125;
          m = 1;
        }
        label125:
        if (m != 0) {
          return bool2;
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    boolean bool1 = f;
    int j;
    if (bool1) {
      j = -559038737;
    } else {
      j = 0;
    }
    long[] arrayOfLong = a;
    int k = c;
    while (k >= 0)
    {
      long l1 = arrayOfLong[k];
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (bool2)
      {
        l1 = d.a(l1);
        int m = (int)l1;
        j += m;
      }
      k += -1;
    }
    return j;
  }
  
  public Iterator iterator()
  {
    y.a locala = new com/a/a/y$a;
    locala.<init>(this);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.a.a.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */