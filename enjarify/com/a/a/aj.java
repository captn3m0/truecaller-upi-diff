package com.a.a;

import java.util.concurrent.atomic.AtomicLong;

public final class aj
  implements s
{
  public static final aj a;
  protected final AtomicLong b;
  
  static
  {
    aj localaj = new com/a/a/aj;
    localaj.<init>();
    a = localaj;
  }
  
  public aj()
  {
    this(l);
  }
  
  private aj(long paramLong)
  {
    AtomicLong localAtomicLong = new java/util/concurrent/atomic/AtomicLong;
    localAtomicLong.<init>(paramLong);
    b = localAtomicLong;
  }
  
  public final int a(int paramInt)
  {
    return (int)d.a(b.incrementAndGet());
  }
  
  public final s a()
  {
    return this;
  }
}

/* Location:
 * Qualified Name:     com.a.a.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */