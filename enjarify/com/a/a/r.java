package com.a.a;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class r
{
  private static r.a a;
  private static final s b;
  
  static
  {
    r.1 local1 = new com/a/a/r$1;
    local1.<init>();
    b = local1;
  }
  
  public static s a()
  {
    return aj.a;
  }
  
  public static s b()
  {
    return b;
  }
  
  public static s c()
  {
    r.2 local2 = new com/a/a/r$2;
    local2.<init>();
    return local2;
  }
  
  public static s d()
  {
    Object localObject1 = a;
    if (localObject1 == null) {
      try
      {
        localObject1 = new com/a/a/r$3;
        ((r.3)localObject1).<init>();
        localObject1 = AccessController.doPrivileged((PrivilegedAction)localObject1);
        localObject1 = (String)localObject1;
        Object localObject3;
        String str1;
        if (localObject1 != null)
        {
          localObject3 = r.a.values();
          int i = localObject3.length;
          int j = 0;
          str1 = null;
          while (j < i)
          {
            r.a locala = localObject3[j];
            String str2 = locala.name();
            boolean bool = str2.equalsIgnoreCase((String)localObject1);
            if (bool)
            {
              a = locala;
              break;
            }
            j += 1;
          }
        }
        try
        {
          Level localLevel;
          Object localObject2 = a;
          localObject2 = ((r.a)localObject2).call();
          return (s)localObject2;
        }
        catch (Exception localException)
        {
          localObject3 = new java/lang/RuntimeException;
          ((RuntimeException)localObject3).<init>(localException);
          throw ((Throwable)localObject3);
        }
      }
      catch (SecurityException localSecurityException)
      {
        localObject3 = Logger.getLogger(p.class.getName());
        localLevel = Level.INFO;
        str1 = "Failed to read 'tests.seed' property for initial random seed.";
        ((Logger)localObject3).log(localLevel, str1, localSecurityException);
        localObject2 = a;
        if (localObject2 == null)
        {
          localObject2 = r.a.a;
          a = (r.a)localObject2;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.a.a.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */