package com.a.a;

import java.util.Iterator;
import java.util.Objects;

public class u
  implements v, Cloneable
{
  public int[] a;
  public Object[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public u()
  {
    this((byte)0);
  }
  
  private u(byte paramByte)
  {
    this(4, 0.75D);
  }
  
  public u(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, locals);
  }
  
  private u(int paramInt, double paramDouble, s params)
  {
    i = params;
    q.a(paramDouble);
    h = paramDouble;
    int k = f;
    int[] arrayOfInt;
    if (paramInt <= k)
    {
      arrayOfInt = a;
      if (arrayOfInt != null) {}
    }
    else
    {
      arrayOfInt = a;
      Object[] arrayOfObject = (Object[])b;
      double d1 = h;
      paramInt = q.a(paramInt, d1);
      c(paramInt);
      if (arrayOfInt != null)
      {
        paramInt = b();
        if (paramInt == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(arrayOfInt, arrayOfObject);
        }
      }
    }
  }
  
  private void a(int[] paramArrayOfInt, Object[] paramArrayOfObject)
  {
    boolean bool = j;
    if (!bool)
    {
      int k = paramArrayOfInt.length;
      int m = paramArrayOfObject.length;
      if (k == m)
      {
        k = paramArrayOfInt.length + -1;
        q.a(k);
      }
      else
      {
        paramArrayOfInt = new java/lang/AssertionError;
        paramArrayOfInt.<init>();
        throw paramArrayOfInt;
      }
    }
    int[] arrayOfInt = a;
    Object[] arrayOfObject = (Object[])b;
    int n = e;
    int i1 = paramArrayOfInt.length + -1;
    int i2 = arrayOfInt.length + -1;
    int i3 = paramArrayOfInt[i1];
    arrayOfInt[i2] = i3;
    i2 = arrayOfObject.length + -1;
    Object localObject1 = paramArrayOfObject[i1];
    arrayOfObject[i2] = localObject1;
    for (;;)
    {
      i1 += -1;
      if (i1 < 0) {
        break;
      }
      i2 = paramArrayOfInt[i1];
      if (i2 != 0)
      {
        for (i3 = b(i2) & n;; i3 = i3 + 1 & n)
        {
          int i4 = arrayOfInt[i3];
          if (i4 == 0) {
            break;
          }
        }
        arrayOfInt[i3] = i2;
        Object localObject2 = paramArrayOfObject[i1];
        arrayOfObject[i3] = localObject2;
      }
    }
  }
  
  private int b(int paramInt)
  {
    boolean bool = j;
    if ((!bool) && (paramInt == 0))
    {
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>();
      throw localAssertionError;
    }
    int k = c;
    return d.a(paramInt ^ k);
  }
  
  private u c()
  {
    try
    {
      Object localObject1 = super.clone();
      localObject1 = (u)localObject1;
      localObject2 = a;
      localObject2 = ((int[])localObject2).clone();
      localObject2 = (int[])localObject2;
      a = ((int[])localObject2);
      localObject2 = b;
      localObject2 = ((Object[])localObject2).clone();
      localObject2 = (Object[])localObject2;
      b = ((Object[])localObject2);
      boolean bool = g;
      g = bool;
      localObject2 = i;
      localObject2 = ((s)localObject2).a();
      i = ((s)localObject2);
      return (u)localObject1;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      Object localObject2 = new java/lang/RuntimeException;
      ((RuntimeException)localObject2).<init>(localCloneNotSupportedException);
      throw ((Throwable)localObject2);
    }
  }
  
  private void c(int paramInt)
  {
    boolean bool = j;
    int m = 1;
    Object localObject1;
    if (!bool)
    {
      k = Integer.bitCount(paramInt);
      if (k != m)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    s locals = i;
    int k = locals.a(paramInt);
    Object localObject2 = a;
    Object[] arrayOfObject1 = (Object[])b;
    int n = paramInt + 1;
    try
    {
      localObject3 = new int[n];
      a = ((int[])localObject3);
      Object[] arrayOfObject2 = new Object[n];
      arrayOfObject2 = (Object[])arrayOfObject2;
      b = arrayOfObject2;
      double d1 = h;
      int i1 = q.b(paramInt, d1);
      f = i1;
      c = k;
      paramInt -= m;
      e = paramInt;
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = ((int[])localObject2);
      b = arrayOfObject1;
      localObject2 = new com/a/a/f;
      arrayOfObject1 = new Object[2];
      Object localObject3 = Integer.valueOf(e + m);
      arrayOfObject1[0] = localObject3;
      localObject1 = Integer.valueOf(paramInt);
      arrayOfObject1[m] = localObject1;
      ((f)localObject2).<init>("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, arrayOfObject1);
      throw ((Throwable)localObject2);
    }
  }
  
  public final Object a(int paramInt)
  {
    int k = 0;
    if (paramInt == 0)
    {
      paramInt = g;
      if (paramInt != 0)
      {
        Object[] arrayOfObject = b;
        k = e + 1;
        return arrayOfObject[k];
      }
      return null;
    }
    int[] arrayOfInt = a;
    int m = e;
    for (int n = b(paramInt) & m;; n = n + 1 & m)
    {
      int i1 = arrayOfInt[n];
      if (i1 == 0) {
        break;
      }
      if (i1 == paramInt) {
        return b[n];
      }
    }
    return null;
  }
  
  public final Object a(int paramInt, Object paramObject)
  {
    boolean bool1 = j;
    int m = 1;
    Object localObject1;
    if (!bool1)
    {
      k = d;
      int n = e + m;
      if (k >= n)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    int k = e;
    if (paramInt == 0)
    {
      g = m;
      localObject1 = b;
      k += m;
      Object localObject2 = localObject1[k];
      localObject1[k] = paramObject;
      return localObject2;
    }
    Object localObject3 = a;
    Object localObject4;
    for (int i1 = b(paramInt) & k;; i1 = i1 + 1 & k)
    {
      i2 = localObject3[i1];
      if (i2 == 0) {
        break;
      }
      if (i2 == paramInt)
      {
        localObject1 = b;
        localObject4 = localObject1[i1];
        localObject1[i1] = paramObject;
        return localObject4;
      }
    }
    k = d;
    int i2 = f;
    if (k == i2)
    {
      boolean bool2 = j;
      if (!bool2) {
        if (k == i2)
        {
          localObject4 = a;
          k = localObject4[i1];
          if ((k == 0) && (paramInt != 0)) {}
        }
        else
        {
          localObject1 = new java/lang/AssertionError;
          ((AssertionError)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
      }
      localObject4 = a;
      localObject3 = (Object[])b;
      i2 = e + m;
      int i4 = b();
      double d1 = h;
      i2 = q.a(i2, i4, d1);
      c(i2);
      boolean bool3 = j;
      if (!bool3)
      {
        int[] arrayOfInt = a;
        int i3 = arrayOfInt.length;
        i4 = localObject4.length;
        if (i3 <= i4)
        {
          localObject1 = new java/lang/AssertionError;
          ((AssertionError)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
      }
      localObject4[i1] = paramInt;
      localObject3[i1] = paramObject;
      a((int[])localObject4, (Object[])localObject3);
    }
    else
    {
      localObject3[i1] = paramInt;
      localObject1 = b;
      localObject1[i1] = paramObject;
    }
    paramInt = d + m;
    d = paramInt;
    return null;
  }
  
  public final int b()
  {
    int k = d;
    int m = g;
    return k + m;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        localObject1 = getClass();
        paramObject = (u)((Class)localObject1).cast(paramObject);
        int k = ((u)paramObject).b();
        int m = b();
        boolean bool2 = true;
        int n;
        if (k != m)
        {
          n = 0;
          paramObject = null;
        }
        else
        {
          paramObject = ((u)paramObject).iterator();
          boolean bool1;
          label177:
          do
          {
            bool1 = ((Iterator)paramObject).hasNext();
            if (!bool1) {
              break label214;
            }
            localObject1 = (com.a.a.a.d)((Iterator)paramObject).next();
            m = b;
            boolean bool3;
            if (m == 0)
            {
              bool3 = g;
            }
            else
            {
              int[] arrayOfInt = a;
              int i1 = e;
              for (int i2 = b(m) & i1;; i2 = i2 + 1 & i1)
              {
                int i3 = arrayOfInt[i2];
                if (i3 == 0) {
                  break;
                }
                if (i3 == m)
                {
                  bool3 = true;
                  break label177;
                }
              }
              bool3 = false;
              arrayOfInt = null;
            }
            if (!bool3) {
              break;
            }
            localObject2 = a(m);
            localObject1 = c;
            bool1 = Objects.equals(localObject2, localObject1);
          } while (bool1);
          n = 0;
          paramObject = null;
          break label217;
          label214:
          n = 1;
        }
        label217:
        if (n != 0) {
          return bool2;
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    boolean bool1 = g;
    int k;
    if (bool1) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject = (com.a.a.a.d)localIterator.next();
      int n = d.a(b);
      localObject = c;
      int m = d.a(localObject);
      n += m;
      k += n;
    }
    return k;
  }
  
  public Iterator iterator()
  {
    u.a locala = new com/a/a/u$a;
    locala.<init>(this);
    return locala;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    int k = 1;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      com.a.a.a.d locald = (com.a.a.a.d)localIterator.next();
      if (k == 0)
      {
        localObject = ", ";
        localStringBuilder.append((String)localObject);
      }
      k = b;
      localStringBuilder.append(k);
      localStringBuilder.append("=>");
      Object localObject = c;
      localStringBuilder.append(localObject);
      k = 0;
      localObject = null;
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.a.a.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */