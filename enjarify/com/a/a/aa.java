package com.a.a;

import java.util.Iterator;

public class aa
  implements ab, Cloneable
{
  public long[] a;
  public int[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public aa()
  {
    this(4);
  }
  
  public aa(int paramInt)
  {
    this(paramInt, (byte)0);
  }
  
  private aa(int paramInt, byte paramByte)
  {
    this(paramInt, locals);
  }
  
  private aa(int paramInt, s params)
  {
    i = params;
    double d1 = 0.75D;
    q.a(d1);
    h = d1;
    int k = f;
    if (paramInt <= k)
    {
      params = a;
      if (params != null) {}
    }
    else
    {
      params = a;
      int[] arrayOfInt = b;
      double d2 = h;
      paramInt = q.a(paramInt, d2);
      a(paramInt);
      if (params != null)
      {
        paramInt = a();
        if (paramInt == 0) {
          a(params, arrayOfInt);
        }
      }
    }
  }
  
  private void a(int paramInt)
  {
    boolean bool = j;
    int m = 1;
    Object localObject1;
    if (!bool)
    {
      k = Integer.bitCount(paramInt);
      if (k != m)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    s locals = i;
    int k = locals.a(paramInt);
    Object localObject2 = a;
    Object localObject3 = b;
    int n = paramInt + 1;
    try
    {
      localObject4 = new long[n];
      a = ((long[])localObject4);
      int[] arrayOfInt = new int[n];
      b = arrayOfInt;
      double d1 = h;
      int i1 = q.b(paramInt, d1);
      f = i1;
      c = k;
      paramInt -= m;
      e = paramInt;
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = ((long[])localObject2);
      b = ((int[])localObject3);
      localObject2 = new com/a/a/f;
      localObject3 = new Object[2];
      Object localObject4 = Integer.valueOf(e + m);
      localObject3[0] = localObject4;
      localObject1 = Integer.valueOf(paramInt);
      localObject3[m] = localObject1;
      ((f)localObject2).<init>("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, (Object[])localObject3);
      throw ((Throwable)localObject2);
    }
  }
  
  private void a(long[] paramArrayOfLong, int[] paramArrayOfInt)
  {
    boolean bool1 = j;
    if (!bool1)
    {
      int k = paramArrayOfLong.length;
      int m = paramArrayOfInt.length;
      if (k == m)
      {
        k = paramArrayOfLong.length + -1;
        q.a(k);
      }
      else
      {
        paramArrayOfLong = new java/lang/AssertionError;
        paramArrayOfLong.<init>();
        throw paramArrayOfLong;
      }
    }
    long[] arrayOfLong = a;
    int[] arrayOfInt = b;
    int n = e;
    int i1 = paramArrayOfLong.length + -1;
    int i2 = arrayOfLong.length + -1;
    long l1 = paramArrayOfLong[i1];
    arrayOfLong[i2] = l1;
    i2 = arrayOfInt.length + -1;
    int i3 = paramArrayOfInt[i1];
    arrayOfInt[i2] = i3;
    for (;;)
    {
      i1 += -1;
      if (i1 < 0) {
        break;
      }
      long l2 = paramArrayOfLong[i1];
      long l3 = 0L;
      boolean bool2 = l2 < l3;
      if (bool2)
      {
        for (int i4 = b(l2) & n;; i4 = i4 + 1 & n)
        {
          long l4 = arrayOfLong[i4];
          boolean bool3 = l4 < l3;
          if (!bool3) {
            break;
          }
        }
        arrayOfLong[i4] = l2;
        i2 = paramArrayOfInt[i1];
        arrayOfInt[i4] = i2;
      }
    }
  }
  
  private int b()
  {
    int k = d;
    int m = g;
    return k + m;
  }
  
  private int b(long paramLong)
  {
    boolean bool1 = j;
    if (!bool1)
    {
      long l = 0L;
      boolean bool2 = paramLong < l;
      if (!bool2)
      {
        AssertionError localAssertionError = new java/lang/AssertionError;
        localAssertionError.<init>();
        throw localAssertionError;
      }
    }
    int k = c;
    return d.a(paramLong, k);
  }
  
  private aa c()
  {
    try
    {
      Object localObject1 = super.clone();
      localObject1 = (aa)localObject1;
      localObject2 = a;
      localObject2 = ((long[])localObject2).clone();
      localObject2 = (long[])localObject2;
      a = ((long[])localObject2);
      localObject2 = b;
      localObject2 = ((int[])localObject2).clone();
      localObject2 = (int[])localObject2;
      b = ((int[])localObject2);
      boolean bool = g;
      g = bool;
      localObject2 = i;
      localObject2 = ((s)localObject2).a();
      i = ((s)localObject2);
      return (aa)localObject1;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      Object localObject2 = new java/lang/RuntimeException;
      ((RuntimeException)localObject2).<init>(localCloneNotSupportedException);
      throw ((Throwable)localObject2);
    }
  }
  
  public final int a(long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      boolean bool2 = g;
      if (bool2)
      {
        int[] arrayOfInt = b;
        int k = e + 1;
        return arrayOfInt[k];
      }
      return 0;
    }
    long[] arrayOfLong = a;
    int m = e;
    for (int n = b(paramLong) & m;; n = n + 1 & m)
    {
      long l2 = arrayOfLong[n];
      boolean bool3 = l2 < l1;
      if (!bool3) {
        break;
      }
      bool3 = l2 < paramLong;
      if (!bool3) {
        return b[n];
      }
    }
    return 0;
  }
  
  public final int a(long paramLong, int paramInt)
  {
    boolean bool1 = j;
    int m = 1;
    Object localObject;
    if (!bool1)
    {
      k = d;
      int n = e + m;
      if (k >= n)
      {
        localObject = new java/lang/AssertionError;
        ((AssertionError)localObject).<init>();
        throw ((Throwable)localObject);
      }
    }
    int k = e;
    long l1 = 0L;
    boolean bool3 = paramLong < l1;
    int i2;
    if (!bool3)
    {
      g = m;
      localObject = b;
      k += m;
      i2 = localObject[k];
      localObject[k] = paramInt;
      return i2;
    }
    long[] arrayOfLong1 = a;
    long l2;
    for (int i3 = b(paramLong) & k;; i3 = i3 + 1 & k)
    {
      l2 = arrayOfLong1[i3];
      boolean bool4 = l2 < l1;
      if (!bool4) {
        break;
      }
      bool4 = l2 < paramLong;
      if (!bool4)
      {
        localObject = b;
        i2 = localObject[i3];
        localObject[i3] = paramInt;
        return i2;
      }
    }
    k = d;
    int i4 = f;
    if (k == i4)
    {
      bool3 = j;
      if (!bool3)
      {
        if (k == i4)
        {
          arrayOfLong2 = a;
          l2 = arrayOfLong2[i3];
          boolean bool2 = l2 < l1;
          if (!bool2)
          {
            bool2 = paramLong < l1;
            if (bool2) {
              break label274;
            }
          }
        }
        localObject = new java/lang/AssertionError;
        ((AssertionError)localObject).<init>();
        throw ((Throwable)localObject);
      }
      label274:
      long[] arrayOfLong2 = a;
      int[] arrayOfInt = b;
      int i5 = e + m;
      int i1 = b();
      double d1 = h;
      i5 = q.a(i5, i1, d1);
      a(i5);
      boolean bool5 = j;
      if (!bool5)
      {
        long[] arrayOfLong3 = a;
        int i6 = arrayOfLong3.length;
        i1 = arrayOfLong2.length;
        if (i6 <= i1)
        {
          localObject = new java/lang/AssertionError;
          ((AssertionError)localObject).<init>();
          throw ((Throwable)localObject);
        }
      }
      arrayOfLong2[i3] = paramLong;
      arrayOfInt[i3] = paramInt;
      a(arrayOfLong2, arrayOfInt);
    }
    else
    {
      arrayOfLong1[i3] = paramLong;
      localObject = b;
      localObject[i3] = paramInt;
    }
    int i7 = d + m;
    d = i7;
    return 0;
  }
  
  public final boolean a()
  {
    int k = b();
    return k == 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        localObject1 = getClass();
        paramObject = (aa)((Class)localObject1).cast(paramObject);
        int k = ((aa)paramObject).b();
        int n = b();
        int i2 = 1;
        int i3;
        if (k != n)
        {
          i3 = 0;
          paramObject = null;
        }
        else
        {
          paramObject = ((aa)paramObject).iterator();
          label196:
          int i1;
          label338:
          int m;
          do
          {
            boolean bool1 = ((Iterator)paramObject).hasNext();
            if (!bool1) {
              break label359;
            }
            localObject1 = (com.a.a.a.f)((Iterator)paramObject).next();
            long l1 = b;
            long l2 = 0L;
            boolean bool2 = l1 < l2;
            int i4;
            int i5;
            long l3;
            boolean bool3;
            if (!bool2)
            {
              bool2 = g;
            }
            else
            {
              localObject2 = a;
              i4 = e;
              for (i5 = b(l1) & i4;; i5 = i5 + 1 & i4)
              {
                l3 = localObject2[i5];
                bool3 = l3 < l2;
                if (!bool3) {
                  break;
                }
                bool3 = l3 < l1;
                if (!bool3)
                {
                  bool2 = true;
                  break label196;
                }
              }
              bool2 = false;
              localObject2 = null;
            }
            if (!bool2) {
              break;
            }
            bool2 = l1 < l2;
            if (!bool2)
            {
              bool2 = g;
              if (bool2)
              {
                localObject2 = b;
                int i6 = e + i2;
                i1 = localObject2[i6];
              }
              else
              {
                i1 = 0;
                localObject2 = null;
              }
            }
            else
            {
              localObject2 = a;
              i4 = e;
              for (i5 = b(l1) & i4;; i5 = i5 + 1 & i4)
              {
                l3 = localObject2[i5];
                bool3 = l3 < l2;
                if (!bool3) {
                  break;
                }
                bool3 = l3 < l1;
                if (!bool3)
                {
                  localObject2 = b;
                  i1 = localObject2[i5];
                  break label338;
                }
              }
              i1 = 0;
              localObject2 = null;
            }
            m = c;
          } while (i1 == m);
          i3 = 0;
          paramObject = null;
          break label362;
          label359:
          i3 = 1;
        }
        label362:
        if (i3 != 0) {
          return i2;
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    boolean bool1 = g;
    int k;
    if (bool1) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      com.a.a.a.f localf = (com.a.a.a.f)localIterator.next();
      long l = d.a(b);
      int n = (int)l;
      int m = d.a(c);
      n += m;
      k += n;
    }
    return k;
  }
  
  public Iterator iterator()
  {
    aa.a locala = new com/a/a/aa$a;
    locala.<init>(this);
    return locala;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    int k = 1;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      com.a.a.a.f localf = (com.a.a.a.f)localIterator.next();
      if (k == 0)
      {
        str = ", ";
        localStringBuilder.append(str);
      }
      long l = b;
      localStringBuilder.append(l);
      localStringBuilder.append("=>");
      k = c;
      localStringBuilder.append(k);
      k = 0;
      String str = null;
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.a.a.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */