package com.a.a;

import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class a
  implements Iterator
{
  int a = 0;
  private Object b;
  
  protected abstract Object a();
  
  public boolean hasNext()
  {
    int i = a;
    int j = 1;
    if (i == 0)
    {
      a = j;
      Object localObject = a();
      b = localObject;
    }
    i = a;
    if (i == j) {
      return j;
    }
    return false;
  }
  
  public Object next()
  {
    boolean bool = hasNext();
    if (bool)
    {
      a = 0;
      return b;
    }
    NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
    localNoSuchElementException.<init>();
    throw localNoSuchElementException;
  }
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>();
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     com.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */