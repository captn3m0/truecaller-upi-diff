package com.a.a;

import com.a.a.a.b;
import java.util.Iterator;

public class k
  implements l, Cloneable
{
  public char[] a;
  public int[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public k()
  {
    this((byte)0);
  }
  
  private k(byte paramByte)
  {
    this(4, 0.75D);
  }
  
  public k(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, locals);
  }
  
  private k(int paramInt, double paramDouble, s params)
  {
    i = params;
    q.a(paramDouble);
    h = paramDouble;
    int k = f;
    char[] arrayOfChar;
    if (paramInt <= k)
    {
      arrayOfChar = a;
      if (arrayOfChar != null) {}
    }
    else
    {
      arrayOfChar = a;
      int[] arrayOfInt = b;
      double d1 = h;
      paramInt = q.a(paramInt, d1);
      a(paramInt);
      if (arrayOfChar != null)
      {
        paramInt = a();
        if (paramInt == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(arrayOfChar, arrayOfInt);
        }
      }
    }
  }
  
  private int a()
  {
    int k = d;
    int m = g;
    return k + m;
  }
  
  private void a(int paramInt)
  {
    boolean bool = j;
    int m = 1;
    Object localObject1;
    if (!bool)
    {
      k = Integer.bitCount(paramInt);
      if (k != m)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    s locals = i;
    int k = locals.a(paramInt);
    Object localObject2 = a;
    Object localObject3 = b;
    int n = paramInt + 1;
    try
    {
      localObject4 = new char[n];
      a = ((char[])localObject4);
      int[] arrayOfInt = new int[n];
      b = arrayOfInt;
      double d1 = h;
      int i1 = q.b(paramInt, d1);
      f = i1;
      c = k;
      paramInt -= m;
      e = paramInt;
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = ((char[])localObject2);
      b = ((int[])localObject3);
      localObject2 = new com/a/a/f;
      localObject3 = new Object[2];
      Object localObject4 = Integer.valueOf(e + m);
      localObject3[0] = localObject4;
      localObject1 = Integer.valueOf(paramInt);
      localObject3[m] = localObject1;
      ((f)localObject2).<init>("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, (Object[])localObject3);
      throw ((Throwable)localObject2);
    }
  }
  
  private void a(char[] paramArrayOfChar, int[] paramArrayOfInt)
  {
    boolean bool = j;
    if (!bool)
    {
      int k = paramArrayOfChar.length;
      int m = paramArrayOfInt.length;
      if (k == m)
      {
        k = paramArrayOfChar.length + -1;
        q.a(k);
      }
      else
      {
        paramArrayOfChar = new java/lang/AssertionError;
        paramArrayOfChar.<init>();
        throw paramArrayOfChar;
      }
    }
    char[] arrayOfChar = a;
    int[] arrayOfInt = b;
    int n = e;
    int i1 = paramArrayOfChar.length + -1;
    int i2 = arrayOfChar.length + -1;
    int i4 = paramArrayOfChar[i1];
    arrayOfChar[i2] = i4;
    i2 = arrayOfInt.length + -1;
    i4 = paramArrayOfInt[i1];
    arrayOfInt[i2] = i4;
    for (;;)
    {
      i1 += -1;
      if (i1 < 0) {
        break;
      }
      i2 = paramArrayOfChar[i1];
      if (i2 != 0)
      {
        for (i4 = b(i2) & n;; i4 = i4 + 1 & n)
        {
          int i5 = arrayOfChar[i4];
          if (i5 == 0) {
            break;
          }
        }
        arrayOfChar[i4] = i2;
        int i3 = paramArrayOfInt[i1];
        arrayOfInt[i4] = i3;
      }
    }
  }
  
  private int b(char paramChar)
  {
    boolean bool = j;
    if ((!bool) && (paramChar == 0))
    {
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>();
      throw localAssertionError;
    }
    int k = c;
    return d.a(paramChar, k);
  }
  
  private k b()
  {
    try
    {
      Object localObject1 = super.clone();
      localObject1 = (k)localObject1;
      localObject2 = a;
      localObject2 = ((char[])localObject2).clone();
      localObject2 = (char[])localObject2;
      a = ((char[])localObject2);
      localObject2 = b;
      localObject2 = ((int[])localObject2).clone();
      localObject2 = (int[])localObject2;
      b = ((int[])localObject2);
      boolean bool = g;
      g = bool;
      localObject2 = i;
      localObject2 = ((s)localObject2).a();
      i = ((s)localObject2);
      return (k)localObject1;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      Object localObject2 = new java/lang/RuntimeException;
      ((RuntimeException)localObject2).<init>(localCloneNotSupportedException);
      throw ((Throwable)localObject2);
    }
  }
  
  public final int a(char paramChar)
  {
    int k = -1;
    if (paramChar == 0)
    {
      paramChar = g;
      if (paramChar != 0)
      {
        int[] arrayOfInt = b;
        k = e + 1;
        return arrayOfInt[k];
      }
      return k;
    }
    char[] arrayOfChar = a;
    int m = e;
    for (int n = b(paramChar) & m;; n = n + 1 & m)
    {
      char c1 = arrayOfChar[n];
      if (c1 == 0) {
        break;
      }
      if (c1 == paramChar) {
        return b[n];
      }
    }
    return k;
  }
  
  public final int a(char paramChar, int paramInt)
  {
    boolean bool1 = j;
    int k = 1;
    Object localObject1;
    if (!bool1)
    {
      c1 = d;
      int m = e + k;
      if (c1 >= m)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    char c1 = e;
    if (paramChar == 0)
    {
      g = k;
      localObject1 = b;
      c1 += k;
      k = localObject1[c1];
      localObject1[c1] = paramInt;
      return k;
    }
    Object localObject2 = a;
    for (int n = b(paramChar) & c1;; n = n + 1 & c1)
    {
      c2 = localObject2[n];
      if (c2 == 0) {
        break;
      }
      if (c2 == paramChar)
      {
        localObject1 = b;
        c1 = localObject1[n];
        localObject1[n] = paramInt;
        return c1;
      }
    }
    c1 = d;
    char c2 = f;
    if (c1 == c2)
    {
      boolean bool2 = j;
      if (!bool2) {
        if (c1 == c2)
        {
          arrayOfChar1 = a;
          c1 = arrayOfChar1[n];
          if ((c1 == 0) && (paramChar != 0)) {}
        }
        else
        {
          localObject1 = new java/lang/AssertionError;
          ((AssertionError)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
      }
      char[] arrayOfChar1 = a;
      localObject2 = b;
      c2 = e + k;
      int i2 = a();
      double d1 = h;
      c2 = q.a(c2, i2, d1);
      a(c2);
      boolean bool3 = j;
      if (!bool3)
      {
        char[] arrayOfChar2 = a;
        int i1 = arrayOfChar2.length;
        i2 = arrayOfChar1.length;
        if (i1 <= i2)
        {
          localObject1 = new java/lang/AssertionError;
          ((AssertionError)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
      }
      arrayOfChar1[n] = paramChar;
      localObject2[n] = paramInt;
      a(arrayOfChar1, (int[])localObject2);
    }
    else
    {
      localObject2[n] = paramChar;
      localObject1 = b;
      localObject1[n] = paramInt;
    }
    paramChar = d + k;
    d = paramChar;
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        localObject1 = getClass();
        paramObject = (k)((Class)localObject1).cast(paramObject);
        int k = ((k)paramObject).a();
        int n = a();
        int i3 = 1;
        int i4;
        if (k != n)
        {
          i4 = 0;
          paramObject = null;
        }
        else
        {
          paramObject = ((k)paramObject).iterator();
          label177:
          int i2;
          label302:
          int m;
          do
          {
            boolean bool1 = ((Iterator)paramObject).hasNext();
            if (!bool1) {
              break label323;
            }
            localObject1 = (b)((Iterator)paramObject).next();
            n = b;
            boolean bool3;
            char[] arrayOfChar;
            int i6;
            int i7;
            if (n == 0)
            {
              bool3 = g;
            }
            else
            {
              arrayOfChar = a;
              i6 = e;
              for (i7 = b(n) & i6;; i7 = i7 + 1 & i6)
              {
                int i8 = arrayOfChar[i7];
                if (i8 == 0) {
                  break;
                }
                if (i8 == n)
                {
                  bool3 = true;
                  break label177;
                }
              }
              bool3 = false;
              arrayOfChar = null;
            }
            if (!bool3) {
              break;
            }
            int i1;
            if (n == 0)
            {
              boolean bool2 = g;
              if (bool2)
              {
                localObject2 = b;
                int i5 = e + i3;
                i1 = localObject2[i5];
              }
              else
              {
                i1 = 0;
                localObject2 = null;
              }
            }
            else
            {
              arrayOfChar = a;
              i6 = e;
              for (i7 = b(i1) & i6;; i7 = i7 + 1 & i6)
              {
                int i9 = arrayOfChar[i7];
                if (i9 == 0) {
                  break;
                }
                if (i9 == i1)
                {
                  localObject2 = b;
                  i2 = localObject2[i7];
                  break label302;
                }
              }
              i2 = 0;
              localObject2 = null;
            }
            m = c;
          } while (i2 == m);
          i4 = 0;
          paramObject = null;
          break label326;
          label323:
          i4 = 1;
        }
        label326:
        if (i4 != 0) {
          return i3;
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    boolean bool1 = g;
    int k;
    if (bool1) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      b localb = (b)localIterator.next();
      int n = d.a(b);
      int m = d.a(c);
      n += m;
      k += n;
    }
    return k;
  }
  
  public Iterator iterator()
  {
    k.a locala = new com/a/a/k$a;
    locala.<init>(this);
    return locala;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    char c1 = '\001';
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      b localb = (b)localIterator.next();
      if (c1 == 0)
      {
        str = ", ";
        localStringBuilder.append(str);
      }
      c1 = b;
      localStringBuilder.append(c1);
      localStringBuilder.append("=>");
      int k = c;
      localStringBuilder.append(k);
      k = 0;
      String str = null;
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.a.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */