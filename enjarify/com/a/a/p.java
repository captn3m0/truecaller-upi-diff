package com.a.a;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class p
{
  private static String a;
  private static final String b;
  
  static
  {
    String str = new java/lang/String;
    str.<init>();
    b = str;
  }
  
  public static long a()
  {
    Object localObject1 = a;
    Object localObject3;
    if (localObject1 == null) {
      try
      {
        localObject1 = new com/a/a/p$1;
        ((p.1)localObject1).<init>();
        localObject1 = AccessController.doPrivileged((PrivilegedAction)localObject1);
        localObject1 = (String)localObject1;
        a = (String)localObject1;
      }
      catch (SecurityException localSecurityException)
      {
        a = b;
        localObject2 = Logger.getLogger(p.class.getName());
        localObject3 = Level.INFO;
        String str2 = "Failed to read 'tests.seed' property for initial random seed.";
        ((Logger)localObject2).log((Level)localObject3, str2, localSecurityException);
      }
    }
    String str1 = a;
    Object localObject2 = b;
    long l1;
    if (str1 != localObject2)
    {
      int i = str1.hashCode();
      l1 = i;
    }
    else
    {
      l1 = System.nanoTime();
      localObject3 = new java/lang/Object;
      localObject3.<init>();
      int j = System.identityHashCode(localObject3);
      long l2 = j;
      l1 ^= l2;
    }
    return d.a(l1);
  }
}

/* Location:
 * Qualified Name:     com.a.a.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */