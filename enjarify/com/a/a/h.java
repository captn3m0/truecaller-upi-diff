package com.a.a;

import com.a.a.a.a;
import java.util.Iterator;

public class h
  implements i, Cloneable
{
  public char[] a;
  public char[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public h()
  {
    this(4);
  }
  
  public h(int paramInt)
  {
    this(paramInt, 0.75D);
  }
  
  public h(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, locals);
  }
  
  private h(int paramInt, double paramDouble, s params)
  {
    i = params;
    q.a(paramDouble);
    h = paramDouble;
    int k = f;
    char[] arrayOfChar1;
    if (paramInt <= k)
    {
      arrayOfChar1 = a;
      if (arrayOfChar1 != null) {}
    }
    else
    {
      arrayOfChar1 = a;
      char[] arrayOfChar2 = b;
      double d1 = h;
      paramInt = q.a(paramInt, d1);
      a(paramInt);
      if (arrayOfChar1 != null)
      {
        paramInt = a();
        if (paramInt == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(arrayOfChar1, arrayOfChar2);
        }
      }
    }
  }
  
  private int a()
  {
    int k = d;
    int m = g;
    return k + m;
  }
  
  private int a(char paramChar)
  {
    boolean bool = j;
    if ((!bool) && (paramChar == 0))
    {
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>();
      throw localAssertionError;
    }
    int k = c;
    return d.a(paramChar, k);
  }
  
  private void a(int paramInt)
  {
    boolean bool = j;
    int m = 1;
    Object localObject1;
    if (!bool)
    {
      k = Integer.bitCount(paramInt);
      if (k != m)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    s locals = i;
    int k = locals.a(paramInt);
    Object localObject2 = a;
    Object localObject3 = b;
    int n = paramInt + 1;
    try
    {
      localObject4 = new char[n];
      a = ((char[])localObject4);
      char[] arrayOfChar = new char[n];
      b = arrayOfChar;
      double d1 = h;
      int i1 = q.b(paramInt, d1);
      f = i1;
      c = k;
      paramInt -= m;
      e = paramInt;
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = ((char[])localObject2);
      b = ((char[])localObject3);
      localObject2 = new com/a/a/f;
      localObject3 = new Object[2];
      Object localObject4 = Integer.valueOf(e + m);
      localObject3[0] = localObject4;
      localObject1 = Integer.valueOf(paramInt);
      localObject3[m] = localObject1;
      ((f)localObject2).<init>("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, (Object[])localObject3);
      throw ((Throwable)localObject2);
    }
  }
  
  private void a(char[] paramArrayOfChar1, char[] paramArrayOfChar2)
  {
    boolean bool = j;
    if (!bool)
    {
      int k = paramArrayOfChar1.length;
      int m = paramArrayOfChar2.length;
      if (k == m)
      {
        k = paramArrayOfChar1.length + -1;
        q.a(k);
      }
      else
      {
        paramArrayOfChar1 = new java/lang/AssertionError;
        paramArrayOfChar1.<init>();
        throw paramArrayOfChar1;
      }
    }
    char[] arrayOfChar1 = a;
    char[] arrayOfChar2 = b;
    int n = e;
    int i1 = paramArrayOfChar1.length + -1;
    int i2 = arrayOfChar1.length + -1;
    int i4 = paramArrayOfChar1[i1];
    arrayOfChar1[i2] = i4;
    i2 = arrayOfChar2.length + -1;
    i4 = paramArrayOfChar2[i1];
    arrayOfChar2[i2] = i4;
    for (;;)
    {
      i1 += -1;
      if (i1 < 0) {
        break;
      }
      i2 = paramArrayOfChar1[i1];
      if (i2 != 0)
      {
        for (i4 = a(i2) & n;; i4 = i4 + 1 & n)
        {
          int i5 = arrayOfChar1[i4];
          if (i5 == 0) {
            break;
          }
        }
        arrayOfChar1[i4] = i2;
        int i3 = paramArrayOfChar2[i1];
        arrayOfChar2[i4] = i3;
      }
    }
  }
  
  private h b()
  {
    try
    {
      Object localObject1 = super.clone();
      localObject1 = (h)localObject1;
      localObject2 = a;
      localObject2 = ((char[])localObject2).clone();
      localObject2 = (char[])localObject2;
      a = ((char[])localObject2);
      localObject2 = b;
      localObject2 = ((char[])localObject2).clone();
      localObject2 = (char[])localObject2;
      b = ((char[])localObject2);
      boolean bool = g;
      g = bool;
      localObject2 = i;
      localObject2 = ((s)localObject2).a();
      i = ((s)localObject2);
      return (h)localObject1;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      Object localObject2 = new java/lang/RuntimeException;
      ((RuntimeException)localObject2).<init>(localCloneNotSupportedException);
      throw ((Throwable)localObject2);
    }
  }
  
  public final char a(char paramChar1, char paramChar2)
  {
    boolean bool1 = j;
    int m = 1;
    Object localObject;
    if (!bool1)
    {
      k = d;
      int i1 = e + m;
      if (k >= i1)
      {
        localObject = new java/lang/AssertionError;
        ((AssertionError)localObject).<init>();
        throw ((Throwable)localObject);
      }
    }
    int k = e;
    int n;
    if (paramChar1 == 0)
    {
      g = m;
      localObject = b;
      k += m;
      n = localObject[k];
      localObject[k] = paramChar2;
      return n;
    }
    char[] arrayOfChar1 = a;
    for (int i2 = a(paramChar1) & k;; i2 = i2 + 1 & k)
    {
      c2 = arrayOfChar1[i2];
      if (c2 == 0) {
        break;
      }
      if (c2 == paramChar1)
      {
        localObject = b;
        k = localObject[i2];
        localObject[i2] = paramChar2;
        return k;
      }
    }
    char c1 = d;
    char c2 = f;
    if (c1 == c2)
    {
      boolean bool2 = j;
      if (!bool2) {
        if (c1 == c2)
        {
          arrayOfChar2 = a;
          c1 = arrayOfChar2[i2];
          if ((c1 == 0) && (paramChar1 != 0)) {}
        }
        else
        {
          localObject = new java/lang/AssertionError;
          ((AssertionError)localObject).<init>();
          throw ((Throwable)localObject);
        }
      }
      char[] arrayOfChar2 = a;
      arrayOfChar1 = b;
      c2 = e + n;
      int i4 = a();
      double d1 = h;
      c2 = q.a(c2, i4, d1);
      a(c2);
      boolean bool3 = j;
      if (!bool3)
      {
        char[] arrayOfChar3 = a;
        int i3 = arrayOfChar3.length;
        i4 = arrayOfChar2.length;
        if (i3 <= i4)
        {
          localObject = new java/lang/AssertionError;
          ((AssertionError)localObject).<init>();
          throw ((Throwable)localObject);
        }
      }
      arrayOfChar2[i2] = paramChar1;
      arrayOfChar1[i2] = paramChar2;
      a(arrayOfChar2, arrayOfChar1);
    }
    else
    {
      arrayOfChar1[i2] = paramChar1;
      localObject = b;
      localObject[i2] = paramChar2;
    }
    paramChar1 = d + n;
    d = paramChar1;
    return '\000';
  }
  
  public final int a(g paramg)
  {
    int k = a();
    paramg = paramg.iterator();
    for (;;)
    {
      boolean bool = paramg.hasNext();
      if (!bool) {
        break;
      }
      a locala = (a)paramg.next();
      char c2 = b;
      char c1 = c;
      a(c2, c1);
    }
    return a() - k;
  }
  
  public final char b(char paramChar1, char paramChar2)
  {
    if (paramChar1 == 0)
    {
      paramChar1 = g;
      if (paramChar1 != 0)
      {
        char[] arrayOfChar1 = b;
        paramChar2 = e + 1;
        return arrayOfChar1[paramChar2];
      }
      return paramChar2;
    }
    char[] arrayOfChar2 = a;
    int k = e;
    for (int m = a(paramChar1) & k;; m = m + 1 & k)
    {
      char c1 = arrayOfChar2[m];
      if (c1 == 0) {
        break;
      }
      if (c1 == paramChar1) {
        return b[m];
      }
    }
    return paramChar2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        localObject1 = getClass();
        paramObject = (h)((Class)localObject1).cast(paramObject);
        int k = ((h)paramObject).a();
        int m = a();
        int n = 1;
        int i1;
        if (k != m)
        {
          i1 = 0;
          paramObject = null;
        }
        else
        {
          paramObject = ((h)paramObject).iterator();
          label177:
          char c2;
          label302:
          char c1;
          do
          {
            boolean bool1 = ((Iterator)paramObject).hasNext();
            if (!bool1) {
              break label323;
            }
            localObject1 = (a)((Iterator)paramObject).next();
            m = b;
            boolean bool3;
            char[] arrayOfChar;
            int i3;
            int i4;
            if (m == 0)
            {
              bool3 = g;
            }
            else
            {
              arrayOfChar = a;
              i3 = e;
              for (i4 = a(m) & i3;; i4 = i4 + 1 & i3)
              {
                int i5 = arrayOfChar[i4];
                if (i5 == 0) {
                  break;
                }
                if (i5 == m)
                {
                  bool3 = true;
                  break label177;
                }
              }
              bool3 = false;
              arrayOfChar = null;
            }
            if (!bool3) {
              break;
            }
            if (m == 0)
            {
              boolean bool2 = g;
              if (bool2)
              {
                localObject2 = b;
                int i2 = e + n;
                c2 = localObject2[i2];
              }
              else
              {
                c2 = '\000';
                localObject2 = null;
              }
            }
            else
            {
              arrayOfChar = a;
              i3 = e;
              for (i4 = a(c2) & i3;; i4 = i4 + 1 & i3)
              {
                char c3 = arrayOfChar[i4];
                if (c3 == 0) {
                  break;
                }
                if (c3 == c2)
                {
                  localObject2 = b;
                  c2 = localObject2[i4];
                  break label302;
                }
              }
              c2 = '\000';
              localObject2 = null;
            }
            c1 = c;
          } while (c2 == c1);
          i1 = 0;
          paramObject = null;
          break label326;
          label323:
          i1 = 1;
        }
        label326:
        if (i1 != 0) {
          return n;
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    boolean bool1 = g;
    int k;
    if (bool1) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      a locala = (a)localIterator.next();
      int n = d.a(b);
      int m = d.a(c);
      n += m;
      k += n;
    }
    return k;
  }
  
  public Iterator iterator()
  {
    h.a locala = new com/a/a/h$a;
    locala.<init>(this);
    return locala;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    char c1 = '\001';
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      a locala = (a)localIterator.next();
      if (c1 == 0)
      {
        str = ", ";
        localStringBuilder.append(str);
      }
      c1 = b;
      localStringBuilder.append(c1);
      localStringBuilder.append("=>");
      c1 = c;
      localStringBuilder.append(c1);
      c1 = '\000';
      String str = null;
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.a.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */