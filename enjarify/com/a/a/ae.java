package com.a.a;

import com.a.a.a.g;
import java.util.Iterator;
import java.util.Objects;

public class ae
  implements af, Cloneable
{
  public long[] a;
  public Object[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public ae()
  {
    this(4);
  }
  
  public ae(int paramInt)
  {
    this(paramInt, 0.75D);
  }
  
  public ae(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, locals);
  }
  
  private ae(int paramInt, double paramDouble, s params)
  {
    i = params;
    q.a(paramDouble);
    h = paramDouble;
    int k = f;
    long[] arrayOfLong;
    if (paramInt <= k)
    {
      arrayOfLong = a;
      if (arrayOfLong != null) {}
    }
    else
    {
      arrayOfLong = a;
      Object[] arrayOfObject = (Object[])b;
      double d1 = h;
      paramInt = q.a(paramInt, d1);
      a(paramInt);
      if (arrayOfLong != null)
      {
        paramInt = a();
        if (paramInt == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(arrayOfLong, arrayOfObject);
        }
      }
    }
  }
  
  public ae(ad paramad)
  {
    this(k);
    paramad = paramad.iterator();
    for (;;)
    {
      boolean bool = paramad.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (g)paramad.next();
      long l = b;
      localObject = c;
      a(l, localObject);
    }
  }
  
  private void a(int paramInt)
  {
    boolean bool = j;
    int m = 1;
    Object localObject1;
    if (!bool)
    {
      k = Integer.bitCount(paramInt);
      if (k != m)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    s locals = i;
    int k = locals.a(paramInt);
    Object localObject2 = a;
    Object[] arrayOfObject1 = (Object[])b;
    int n = paramInt + 1;
    try
    {
      localObject3 = new long[n];
      a = ((long[])localObject3);
      Object[] arrayOfObject2 = new Object[n];
      arrayOfObject2 = (Object[])arrayOfObject2;
      b = arrayOfObject2;
      double d1 = h;
      int i1 = q.b(paramInt, d1);
      f = i1;
      c = k;
      paramInt -= m;
      e = paramInt;
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = ((long[])localObject2);
      b = arrayOfObject1;
      localObject2 = new com/a/a/f;
      arrayOfObject1 = new Object[2];
      Object localObject3 = Integer.valueOf(e + m);
      arrayOfObject1[0] = localObject3;
      localObject1 = Integer.valueOf(paramInt);
      arrayOfObject1[m] = localObject1;
      ((f)localObject2).<init>("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, arrayOfObject1);
      throw ((Throwable)localObject2);
    }
  }
  
  private void a(int paramInt, long paramLong, Object paramObject)
  {
    boolean bool1 = j;
    AssertionError localAssertionError;
    if (!bool1)
    {
      int k = d;
      int m = f;
      if (k == m)
      {
        arrayOfLong1 = a;
        long l1 = arrayOfLong1[paramInt];
        long l2 = 0L;
        boolean bool2 = l1 < l2;
        if (!bool2)
        {
          bool2 = paramLong < l2;
          if (bool2) {
            break label83;
          }
        }
      }
      localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>();
      throw localAssertionError;
    }
    label83:
    long[] arrayOfLong1 = a;
    Object[] arrayOfObject = (Object[])b;
    int n = e + 1;
    int i2 = a();
    double d1 = h;
    n = q.a(n, i2, d1);
    a(n);
    boolean bool3 = j;
    if (!bool3)
    {
      long[] arrayOfLong2 = a;
      int i1 = arrayOfLong2.length;
      i2 = arrayOfLong1.length;
      if (i1 <= i2)
      {
        localAssertionError = new java/lang/AssertionError;
        localAssertionError.<init>();
        throw localAssertionError;
      }
    }
    arrayOfLong1[paramInt] = paramLong;
    arrayOfObject[paramInt] = paramObject;
    a(arrayOfLong1, arrayOfObject);
  }
  
  private void a(long[] paramArrayOfLong, Object[] paramArrayOfObject)
  {
    boolean bool1 = j;
    if (!bool1)
    {
      int k = paramArrayOfLong.length;
      int m = paramArrayOfObject.length;
      if (k == m)
      {
        k = paramArrayOfLong.length + -1;
        q.a(k);
      }
      else
      {
        paramArrayOfLong = new java/lang/AssertionError;
        paramArrayOfLong.<init>();
        throw paramArrayOfLong;
      }
    }
    long[] arrayOfLong = a;
    Object[] arrayOfObject = (Object[])b;
    int n = e;
    int i1 = paramArrayOfLong.length + -1;
    int i2 = arrayOfLong.length + -1;
    long l1 = paramArrayOfLong[i1];
    arrayOfLong[i2] = l1;
    i2 = arrayOfObject.length + -1;
    Object localObject1 = paramArrayOfObject[i1];
    arrayOfObject[i2] = localObject1;
    for (;;)
    {
      i1 += -1;
      if (i1 < 0) {
        break;
      }
      long l2 = paramArrayOfLong[i1];
      long l3 = 0L;
      boolean bool2 = l2 < l3;
      if (bool2)
      {
        for (int i3 = c(l2) & n;; i3 = i3 + 1 & n)
        {
          long l4 = arrayOfLong[i3];
          boolean bool3 = l4 < l3;
          if (!bool3) {
            break;
          }
        }
        arrayOfLong[i3] = l2;
        Object localObject2 = paramArrayOfObject[i1];
        arrayOfObject[i3] = localObject2;
      }
    }
  }
  
  private int c(long paramLong)
  {
    boolean bool1 = j;
    if (!bool1)
    {
      long l = 0L;
      boolean bool2 = paramLong < l;
      if (!bool2)
      {
        AssertionError localAssertionError = new java/lang/AssertionError;
        localAssertionError.<init>();
        throw localAssertionError;
      }
    }
    int k = c;
    return d.a(paramLong, k);
  }
  
  private ae c()
  {
    try
    {
      Object localObject1 = super.clone();
      localObject1 = (ae)localObject1;
      localObject2 = a;
      localObject2 = ((long[])localObject2).clone();
      localObject2 = (long[])localObject2;
      a = ((long[])localObject2);
      localObject2 = b;
      localObject2 = ((Object[])localObject2).clone();
      localObject2 = (Object[])localObject2;
      b = ((Object[])localObject2);
      boolean bool = g;
      g = bool;
      localObject2 = i;
      localObject2 = ((s)localObject2).a();
      i = ((s)localObject2);
      return (ae)localObject1;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      Object localObject2 = new java/lang/RuntimeException;
      ((RuntimeException)localObject2).<init>(localCloneNotSupportedException);
      throw ((Throwable)localObject2);
    }
  }
  
  public final int a()
  {
    int k = d;
    int m = g;
    return k + m;
  }
  
  public final Object a(long paramLong, Object paramObject)
  {
    boolean bool1 = j;
    int m = 1;
    Object localObject1;
    if (!bool1)
    {
      k = d;
      n = e + m;
      if (k >= n)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    int k = e;
    long l1 = 0L;
    boolean bool2 = paramLong < l1;
    Object localObject2;
    if (!bool2)
    {
      g = m;
      localObject1 = b;
      k += m;
      localObject2 = localObject1[k];
      localObject1[k] = paramObject;
      return localObject2;
    }
    long[] arrayOfLong = a;
    for (int i1 = c(paramLong) & k;; i1 = i1 + 1 & k)
    {
      long l2 = arrayOfLong[i1];
      boolean bool3 = l2 < l1;
      if (!bool3) {
        break;
      }
      bool3 = l2 < paramLong;
      if (!bool3)
      {
        localObject1 = b;
        localObject2 = localObject1[i1];
        localObject1[i1] = paramObject;
        return localObject2;
      }
    }
    k = d;
    int n = f;
    if (k == n)
    {
      a(i1, paramLong, paramObject);
    }
    else
    {
      arrayOfLong[i1] = paramLong;
      localObject1 = b;
      localObject1[i1] = paramObject;
    }
    int i2 = d + m;
    d = i2;
    return null;
  }
  
  public final boolean a(long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1) {
      return g;
    }
    long[] arrayOfLong = a;
    int k = e;
    for (int m = c(paramLong) & k;; m = m + 1 & k)
    {
      long l2 = arrayOfLong[m];
      boolean bool2 = l2 < l1;
      if (!bool2) {
        break;
      }
      bool2 = l2 < paramLong;
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final Object b(long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      boolean bool2 = g;
      if (bool2)
      {
        Object[] arrayOfObject = b;
        int k = e + 1;
        return arrayOfObject[k];
      }
      return null;
    }
    long[] arrayOfLong = a;
    int m = e;
    for (int n = c(paramLong) & m;; n = n + 1 & m)
    {
      long l2 = arrayOfLong[n];
      boolean bool3 = l2 < l1;
      if (!bool3) {
        break;
      }
      bool3 = l2 < paramLong;
      if (!bool3) {
        return b[n];
      }
    }
    return null;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        localObject1 = getClass();
        paramObject = (ae)((Class)localObject1).cast(paramObject);
        int k = ((ae)paramObject).a();
        int m = a();
        boolean bool3 = true;
        int n;
        if (k != m)
        {
          n = 0;
          paramObject = null;
        }
        else
        {
          paramObject = ((ae)paramObject).iterator();
          boolean bool1;
          do
          {
            bool1 = ((Iterator)paramObject).hasNext();
            if (!bool1) {
              break label142;
            }
            localObject1 = (g)((Iterator)paramObject).next();
            long l = b;
            boolean bool2 = a(l);
            if (!bool2) {
              break;
            }
            localObject2 = b(l);
            localObject1 = c;
            bool1 = Objects.equals(localObject2, localObject1);
          } while (bool1);
          n = 0;
          paramObject = null;
          break label145;
          label142:
          n = 1;
        }
        label145:
        if (n != 0) {
          return bool3;
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    boolean bool1 = g;
    int k;
    if (bool1) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject = (g)localIterator.next();
      long l = d.a(b);
      int n = (int)l;
      localObject = c;
      int m = d.a(localObject);
      n += m;
      k += n;
    }
    return k;
  }
  
  public Iterator iterator()
  {
    ae.a locala = new com/a/a/ae$a;
    locala.<init>(this);
    return locala;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    int k = 1;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      g localg = (g)localIterator.next();
      if (k == 0)
      {
        localObject = ", ";
        localStringBuilder.append((String)localObject);
      }
      long l = b;
      localStringBuilder.append(l);
      localStringBuilder.append("=>");
      Object localObject = c;
      localStringBuilder.append(localObject);
      k = 0;
      localObject = null;
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.a.a.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */