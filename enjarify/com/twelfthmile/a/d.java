package com.twelfthmile.a;

import com.twelfthmile.a.a.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public final class d
{
  private a a;
  private b b;
  
  private d()
  {
    Object localObject1 = c.a;
    if (localObject1 == null)
    {
      localObject1 = new com/twelfthmile/d/a;
      ((com.twelfthmile.d.a)localObject1).<init>();
      c.a = (com.twelfthmile.d.a)localObject1;
    }
    localObject1 = c.a.a("bhavani_vendor_operators");
    Object localObject2 = new org/json/JSONArray;
    ((JSONArray)localObject2).<init>((String)localObject1);
    localObject1 = c.a;
    if (localObject1 == null)
    {
      localObject1 = new com/twelfthmile/d/a;
      ((com.twelfthmile.d.a)localObject1).<init>();
      c.a = (com.twelfthmile.d.a)localObject1;
    }
    localObject1 = c.a.a("bhavani_vendor_seed");
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>((String)localObject1);
    localObject1 = c.a;
    if (localObject1 == null)
    {
      localObject1 = new com/twelfthmile/d/a;
      ((com.twelfthmile.d.a)localObject1).<init>();
      c.a = (com.twelfthmile.d.a)localObject1;
    }
    localObject1 = c.a.a("bhavani_vendor_banks");
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>((String)localObject1);
    localObject1 = c.a;
    if (localObject1 == null)
    {
      localObject1 = new com/twelfthmile/d/a;
      ((com.twelfthmile.d.a)localObject1).<init>();
      c.a = (com.twelfthmile.d.a)localObject1;
    }
    localObject1 = c.a.a("bhavani_vendor_brands");
    JSONObject localJSONObject3 = new org/json/JSONObject;
    localJSONObject3.<init>((String)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    int i = 0;
    for (;;)
    {
      int j = ((JSONArray)localObject2).length();
      if (i >= j) {
        break;
      }
      String str = ((JSONArray)localObject2).getString(i);
      ((List)localObject1).add(str);
      i += 1;
    }
    localObject2 = new com/twelfthmile/a/a;
    ((a)localObject2).<init>(localJSONObject1, localJSONObject2, localJSONObject3);
    a = ((a)localObject2);
    localObject2 = new com/twelfthmile/a/b;
    ((b)localObject2).<init>((List)localObject1);
    b = ((b)localObject2);
  }
  
  public static com.twelfthmile.b.a.a a(com.twelfthmile.b.a.a parama)
  {
    Object localObject = parama.get("vendor").trim();
    String str1 = " +";
    localObject = ((String)localObject).replaceAll(str1, " ");
    int i = 32;
    int j = 1;
    int k = 0;
    String str2 = null;
    int n = 1;
    char c = ' ';
    int i4;
    int i5;
    int i6;
    int i1;
    for (;;)
    {
      int i2 = ((String)localObject).length();
      i4 = 11;
      i5 = 7;
      i6 = 10;
      if (k >= i2) {
        break;
      }
      int i3 = ((String)localObject).charAt(k);
      int i7 = 64;
      int i8 = 42;
      int i9 = 47;
      if (n != j) {}
      switch (n)
      {
      default: 
        switch (n)
        {
        default: 
          break;
        case 12: 
          if ((i3 == i9) || (i3 == i8))
          {
            c = i3;
            i1 = 10;
          }
          break;
        case 11: 
          if ((i3 != i9) && (i3 != i8))
          {
            if (i3 == i7) {
              i1 = 7;
            }
          }
          else
          {
            c = i3;
            i1 = 10;
          }
          break;
        }
        break;
      case 7: 
        String str3 = ((String)localObject).substring(k);
        i5 = 0;
        for (;;)
        {
          i6 = str3.length();
          if (i5 >= i6) {
            break;
          }
          i6 = str3.charAt(i5);
          if (i6 == i)
          {
            i4 = i5;
            break label284;
          }
          i5 += 1;
        }
        i4 = str3.length() - j;
        label284:
        i5 = 46;
        if (i3 == i5)
        {
          i3 = 3;
          if (i4 != i3)
          {
            i3 = 2;
            if (i4 != i3)
            {
              i3 = 5;
              if (i4 != i3) {
                break;
              }
            }
          }
          k = i4;
          i1 = 8;
          break;
          if (i3 == i7)
          {
            i1 = 7;
          }
          else if ((i3 != i9) && (i3 != i8))
          {
            i5 = 45;
            if (i3 == i5)
            {
              c = i3;
              i1 = 11;
            }
            else
            {
              i4 = 58;
              if (i3 == i4)
              {
                i1 = 12;
                c = i3;
              }
            }
          }
          else
          {
            c = i3;
            i1 = 10;
          }
        }
        break;
      }
      int m;
      k += j;
    }
    if ((i1 != i6) && (i1 != i4))
    {
      if (i1 != i5)
      {
        str1 = " ";
        localObject = ((String)localObject).split(str1);
        i = localObject.length;
        if (i > j) {
          a((String[])localObject, parama, j);
        }
      }
    }
    else
    {
      str2 = String.valueOf(c);
      str1 = "\\".concat(str2);
      localObject = ((String)localObject).split(str1);
      i = localObject.length;
      if (i > j)
      {
        str1 = localObject[0];
        i = str1.length();
        if (i > j) {
          a((String[])localObject, parama, false);
        }
      }
    }
    str1 = parama.get("vendor").trim().replaceAll(" +", " ");
    parama.put("vendor", str1);
    return parama;
  }
  
  private static Set a(String paramString, Map paramMap)
  {
    Object localObject1 = paramString;
    Map localMap = paramMap;
    Object localObject2 = paramMap.keySet();
    Object localObject3 = "";
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    localObject2 = ((Set)localObject2).iterator();
    double d1 = 0.0D;
    double d2 = d1;
    int j;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      boolean bool2 = true;
      if (!bool1) {
        break label432;
      }
      String str = (String)((Iterator)localObject2).next();
      com.twelfthmile.a.a.d.a.a();
      if ((str == null) || (localObject1 == null)) {
        break;
      }
      int[] arrayOfInt = com.twelfthmile.a.a.d.a(str, (CharSequence)localObject1);
      int k = arrayOfInt[0];
      double d3 = k;
      boolean bool3 = d3 < d1;
      Double localDouble;
      if (!bool3)
      {
        localDouble = Double.valueOf(d1);
      }
      else
      {
        int m = str.length();
        double d4 = m;
        Double.isNaN(d3);
        Double.isNaN(d4);
        d4 = d3 / d4;
        d1 = paramString.length();
        Double.isNaN(d3);
        Double.isNaN(d1);
        d1 = d3 / d1;
        d4 += d1;
        n = arrayOfInt[bool2];
        d1 = n;
        double d5 = 2.0D;
        Double.isNaN(d1);
        d1 /= d5;
        Double.isNaN(d3);
        d1 = d3 - d1;
        Double.isNaN(d3);
        d1 /= d3;
        d4 = (d4 + d1) / 3.0D;
        d1 = 0.7D;
        bool2 = d4 < d1;
        if (!bool2)
        {
          int i = arrayOfInt[2];
          double d6 = i;
          Double.isNaN(d6);
          d6 *= 0.1D;
          d1 = 1.0D - d4;
          d6 *= d1;
          d4 += d6;
        }
        localDouble = Double.valueOf(d4);
      }
      d1 = localDouble.doubleValue();
      j = d2 < d1;
      if (j <= 0)
      {
        d2 = d1;
        localObject3 = str;
      }
      int n = str.length();
      int i2 = 3;
      if (n > i2)
      {
        boolean bool4 = ((String)localObject1).contains(str);
        if (bool4)
        {
          Collection localCollection = (Collection)localMap.get(str);
          localHashSet.addAll(localCollection);
        }
      }
      d1 = 0.0D;
    }
    localObject1 = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject1).<init>("CharSequences must not be null");
    throw ((Throwable)localObject1);
    label432:
    int i3 = paramString.length();
    int i1 = 4;
    boolean bool5;
    if (i3 <= i1)
    {
      localObject2 = ((String)localObject3).toCharArray();
      localObject1 = paramString.toCharArray();
      int i4 = com.twelfthmile.a.a.a.a((char[])localObject2, (char[])localObject1);
      i3 = ((String)localObject3).length();
      if (i3 > i1)
      {
        i3 = ((String)localObject3).length() - j;
        d1 = i3;
      }
      else
      {
        i3 = ((String)localObject3).length();
        d1 = i3;
      }
      d2 = i4;
      bool5 = d2 < d1;
      if (!bool5)
      {
        d1 = 0.0D;
        bool5 = d1 < d2;
        if (bool5)
        {
          localObject1 = (Collection)localMap.get(localObject3);
          localHashSet.addAll((Collection)localObject1);
        }
      }
    }
    else
    {
      d1 = 0.9D;
      bool5 = d2 < d1;
      if (bool5)
      {
        localObject1 = (Collection)localMap.get(localObject3);
        localHashSet.addAll((Collection)localObject1);
      }
    }
    return localHashSet;
  }
  
  private static Set a(List paramList, Map paramMap)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      Set localSet = a((String)paramList.next(), paramMap);
      localHashSet.addAll(localSet);
    }
    return localHashSet;
  }
  
  private Set a(Set paramSet)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    paramSet = paramSet.iterator();
    Map localMap = a.d;
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (String)paramSet.next();
      localObject = (List)localMap.get(localObject);
      localHashSet.addAll((Collection)localObject);
    }
    return localHashSet;
  }
  
  private static void a(String[] paramArrayOfString, com.twelfthmile.b.a.a parama, boolean paramBoolean)
  {
    Object localObject1 = paramArrayOfString;
    com.twelfthmile.b.a.a locala = parama;
    int i = 0;
    Object localObject2 = null;
    Object localObject3 = null;
    label118:
    Object localObject5;
    label1371:
    boolean bool20;
    Object localObject6;
    for (;;)
    {
      int j = localObject1.length;
      if (i >= j) {
        break;
      }
      Object localObject4 = localObject1[i];
      if (localObject4 != null)
      {
        k = ((String)localObject4).length();
        if (k != 0)
        {
          k = 0;
          str1 = null;
          for (;;)
          {
            int i15 = ((String)localObject4).length();
            if (k >= i15) {
              break;
            }
            boolean bool17 = com.twelfthmile.b.b.a.c(((String)localObject4).charAt(k));
            if (!bool17)
            {
              k = 0;
              str1 = null;
              break label118;
            }
            k += 1;
          }
          k = 1;
          break label118;
        }
      }
      int k = 0;
      String str1 = null;
      int i16 = 5;
      int i17 = 2;
      if (k != 0)
      {
        k = ((String)localObject4).hashCode();
        boolean bool7;
        boolean bool15;
        switch (k)
        {
        default: 
          break;
        case 806512005: 
          str1 = "CASH-WDL";
          boolean bool1 = ((String)localObject4).equals(str1);
          if (bool1) {
            int m = 5;
          }
          break;
        case 2392261: 
          str1 = "NEFT";
          boolean bool2 = ((String)localObject4).equals(str1);
          if (bool2) {
            int n = 10;
          }
          break;
        case 2388312: 
          str1 = "NACH";
          boolean bool3 = ((String)localObject4).equals(str1);
          if (bool3) {
            int i1 = 16;
          }
          break;
        case 2251303: 
          str1 = "IMPS";
          boolean bool4 = ((String)localObject4).equals(str1);
          if (bool4) {
            int i2 = 7;
          }
          break;
        case 2061107: 
          str1 = "CASH";
          boolean bool5 = ((String)localObject4).equals(str1);
          if (bool5) {
            int i3 = 4;
          }
          break;
        case 84745: 
          str1 = "VAT";
          boolean bool6 = ((String)localObject4).equals(str1);
          if (bool6) {
            int i4 = 3;
          }
          break;
        case 77211: 
          str1 = "NFS";
          bool7 = ((String)localObject4).equals(str1);
          if (bool7)
          {
            bool7 = false;
            str1 = null;
          }
          break;
        case 76512: 
          str1 = "MOB";
          bool7 = ((String)localObject4).equals(str1);
          if (bool7) {
            int i5 = 12;
          }
          break;
        case 76468: 
          str1 = "MMT";
          boolean bool8 = ((String)localObject4).equals(str1);
          if (bool8) {
            int i6 = 6;
          }
          break;
        case 76096: 
          str1 = "MAT";
          boolean bool9 = ((String)localObject4).equals(str1);
          if (bool9) {
            int i7 = 2;
          }
          break;
        case 72641: 
          str1 = "INF";
          boolean bool10 = ((String)localObject4).equals(str1);
          if (bool10) {
            int i8 = 11;
          }
          break;
        case 72637: 
          str1 = "INB";
          boolean bool11 = ((String)localObject4).equals(str1);
          if (bool11) {
            int i9 = 9;
          }
          break;
        case 68469: 
          str1 = "ECS";
          boolean bool12 = ((String)localObject4).equals(str1);
          if (bool12) {
            int i10 = 15;
          }
          break;
        case 66046: 
          str1 = "BRN";
          boolean bool13 = ((String)localObject4).equals(str1);
          if (bool13) {
            int i11 = 13;
          }
          break;
        case 65765: 
          str1 = "BIL";
          boolean bool14 = ((String)localObject4).equals(str1);
          if (bool14) {
            int i12 = 8;
          }
          break;
        case 65146: 
          str1 = "ATM";
          bool15 = ((String)localObject4).equals(str1);
          if (bool15) {
            bool15 = true;
          }
          break;
        case -1881484424: 
          str1 = "REFUND";
          bool15 = ((String)localObject4).equals(str1);
          if (bool15) {
            i13 = 14;
          }
          break;
        }
        int i13 = -1;
        String str2;
        switch (i13)
        {
        default: 
          str1 = ((String)localObject4).toLowerCase();
          bool16 = b(str1);
          break;
        case 16: 
          str1 = "trx_subcategory";
          str2 = "nach";
          locala.put(str1, str2);
          break;
        case 15: 
          str1 = "trx_subcategory";
          str2 = "ecs";
          locala.put(str1, str2);
          break;
        case 14: 
          str1 = "trx_subcategory";
          str2 = "refund";
          locala.put(str1, str2);
          break;
        case 12: 
        case 13: 
          locala.put("trx_category", "transfer");
          str1 = "trx_subcategory";
          str2 = "bank";
          locala.put(str1, str2);
          break;
        case 10: 
        case 11: 
          locala.put("trx_category", "transfer");
          str1 = "trx_subcategory";
          str2 = "neft";
          locala.put(str1, str2);
          break;
        case 8: 
        case 9: 
          locala.put("trx_category", "transfer");
          str1 = "trx_subcategory";
          str2 = "bill";
          locala.put(str1, str2);
          break;
        case 6: 
        case 7: 
          locala.put("trx_category", "transfer");
          str1 = "trx_subcategory";
          str2 = "imps";
          locala.put(str1, str2);
          break;
        case 0: 
        case 1: 
        case 2: 
        case 3: 
        case 4: 
        case 5: 
          str1 = locala.get("trx_type");
          str2 = "debit";
          bool16 = str1.equals(str2);
          if (bool16)
          {
            str1 = "trx_subcategory";
            str2 = "withdraw";
            locala.put(str1, str2);
          }
          break;
        }
        boolean bool16 = true;
        if (bool16) {}
      }
      else if (localObject4 != null)
      {
        int i14 = ((String)localObject4).length();
        if (i14 != 0)
        {
          i14 = 0;
          str1 = null;
          int i18 = 1;
          int i20;
          for (;;)
          {
            int i21 = ((String)localObject4).length();
            if ((i14 >= i21) || (i18 <= 0)) {
              break;
            }
            i21 = ((String)localObject4).charAt(i14);
            int i25 = 45;
            int i29 = 32;
            int i22;
            int i23;
            switch (i18)
            {
            default: 
              break;
            case 6: 
              i22 = com.twelfthmile.b.b.a.b(i21);
              if (i22 != 0) {
                i18 = 5;
              }
              break;
            case 2: 
              boolean bool18 = com.twelfthmile.b.b.a.b(i22);
              int i19;
              if ((!bool18) && (i22 != i29) && (i22 != i25))
              {
                i23 = com.twelfthmile.b.b.a.a(i22);
                if (i23 != 0) {
                  i19 = 4;
                } else {
                  i19 = -1;
                }
              }
              else
              {
                i19 = 2;
              }
              break;
            case 1: 
              boolean bool19 = com.twelfthmile.b.b.a.c(i23);
              if ((!bool19) && (i23 != i29) && (i23 != i25))
              {
                int i26 = 97;
                if (i23 >= i26)
                {
                  int i27 = 122;
                  if (i23 <= i27)
                  {
                    bool22 = true;
                    break label1371;
                  }
                }
                bool22 = false;
                localObject5 = null;
                if (bool22)
                {
                  i20 = 2;
                }
                else
                {
                  bool20 = com.twelfthmile.b.b.a.a(i23);
                  if (bool20)
                  {
                    if (i14 == 0) {
                      i20 = 6;
                    } else if (i14 >= i17) {
                      i20 = 3;
                    } else {
                      i20 = -1;
                    }
                  }
                  else {
                    i20 = -1;
                  }
                }
              }
              else
              {
                i20 = 1;
              }
              break;
            }
            i14 += 1;
          }
          localObject6 = ((String)localObject4).toLowerCase();
          localObject5 = "salary";
          boolean bool22 = ((String)localObject6).contains((CharSequence)localObject5);
          if (bool22)
          {
            locala.put("trx_category", "income");
            localObject6 = "trx_subcategory";
            localObject5 = "salary";
            locala.put((String)localObject6, (String)localObject5);
            bool20 = true;
          }
          else
          {
            localObject5 = "interest";
            bool22 = ((String)localObject6).contains((CharSequence)localObject5);
            if (bool22)
            {
              localObject6 = "trx_subcategory";
              localObject5 = "interest";
              locala.put((String)localObject6, (String)localObject5);
              bool20 = true;
            }
            else
            {
              localObject5 = "loan";
              bool22 = ((String)localObject6).contains((CharSequence)localObject5);
              if (bool22)
              {
                localObject6 = "trx_subcategory";
                localObject5 = "loan";
                locala.put((String)localObject6, (String)localObject5);
                bool20 = true;
              }
              else
              {
                localObject5 = Pattern.compile("c.*s.*w.*d.*l");
                localObject6 = ((Pattern)localObject5).matcher((CharSequence)localObject6);
                bool20 = ((Matcher)localObject6).find();
                if (bool20)
                {
                  localObject6 = "trx_subcategory";
                  localObject5 = "withdraw";
                  locala.put((String)localObject6, (String)localObject5);
                  bool20 = true;
                }
                else
                {
                  bool20 = false;
                  localObject6 = null;
                }
              }
            }
          }
          if (bool20)
          {
            i20 = 1;
          }
          else
          {
            localObject6 = "loc";
            bool20 = locala.containsKey((String)localObject6);
            if (bool20)
            {
              localObject6 = locala.get("loc");
              bool20 = ((String)localObject6).equalsIgnoreCase((String)localObject4);
              if (bool20) {
                i20 = -1;
              }
            }
          }
          if (i20 >= 0)
          {
            localObject6 = new com/twelfthmile/a/d$b;
            ((d.b)localObject6).<init>((String)localObject4, i20, i);
            if (localObject3 != null)
            {
              localObject5 = localObject3;
              while (localObject5 != null)
              {
                j = b;
                int i30 = b;
                if (j > i30)
                {
                  localObject4 = d;
                  if (localObject4 != null)
                  {
                    localObject5 = d;
                  }
                  else
                  {
                    d = ((d.b)localObject6);
                    bool22 = false;
                    localObject5 = null;
                  }
                }
                else
                {
                  localObject4 = c;
                  if (localObject4 != null)
                  {
                    localObject5 = c;
                  }
                  else
                  {
                    c = ((d.b)localObject6);
                    bool22 = false;
                    localObject5 = null;
                  }
                }
              }
            }
            localObject3 = localObject6;
          }
        }
      }
      i += 1;
    }
    int i28;
    if (!paramBoolean)
    {
      localObject1 = localObject3;
      bool20 = false;
      localObject6 = null;
      i28 = -1;
      while (localObject1 != null)
      {
        i = b;
        if (i > i28)
        {
          i24 = b;
          localObject5 = a;
          i28 = i24;
          localObject6 = localObject5;
        }
        localObject2 = d;
        if (localObject2 != null)
        {
          localObject2 = d;
          i = b;
          if (i > i28)
          {
            localObject1 = d;
            i28 = b;
            localObject6 = a;
            continue;
          }
        }
        localObject2 = c;
        if (localObject2 != null)
        {
          localObject2 = c;
          i = b;
          if (i > i28)
          {
            localObject1 = c;
            i28 = b;
            localObject6 = a;
            continue;
          }
        }
        i31 = 0;
        localObject1 = null;
      }
      if (localObject6 != null)
      {
        localObject1 = "vendor_norm";
        locala.put((String)localObject1, (String)localObject6);
      }
    }
    int i31 = 0;
    localObject1 = null;
    int i24 = 101;
    while (localObject3 != null)
    {
      i28 = b;
      if (i28 < i24)
      {
        i31 = b;
        localObject6 = a;
        i24 = i31;
        localObject1 = localObject6;
      }
      localObject5 = c;
      if (localObject5 != null)
      {
        localObject5 = c;
        i28 = b;
        if (i28 < i24)
        {
          localObject3 = c;
          i24 = b;
          localObject1 = a;
          continue;
        }
      }
      localObject5 = d;
      if (localObject5 != null)
      {
        localObject5 = d;
        i28 = b;
        if (i28 < i24)
        {
          localObject3 = d;
          i24 = b;
          localObject1 = a;
          continue;
        }
      }
      localObject3 = null;
    }
    if (localObject1 != null)
    {
      i28 = 60;
      if (i24 <= i28)
      {
        localObject6 = "ref_id";
        boolean bool21 = locala.containsKey((String)localObject6);
        if (!bool21)
        {
          localObject6 = "ref_id";
          locala.put((String)localObject6, (String)localObject1);
        }
        if (paramBoolean)
        {
          localObject6 = "vendor_norm";
          bool21 = locala.containsKey((String)localObject6);
          if (bool21)
          {
            localObject1 = locala.get("vendor_norm").replace((CharSequence)localObject1, "");
            locala.put("vendor_norm", (String)localObject1);
            return;
          }
          localObject6 = "vendor_norm";
          localObject5 = locala.get("vendor");
          localObject2 = "";
          localObject1 = ((String)localObject5).replace((CharSequence)localObject1, (CharSequence)localObject2);
          locala.put((String)localObject6, (String)localObject1);
        }
      }
    }
  }
  
  private static boolean b(String paramString)
  {
    try
    {
      Object localObject = d.a.a;
      paramString = ((d)localObject).a(paramString);
      localObject = "tags";
      localObject = paramString.get(localObject);
      localObject = (Set)localObject;
      int i = ((Set)localObject).size();
      if (i > 0)
      {
        localObject = "tags";
        localObject = paramString.get(localObject);
        localObject = (Set)localObject;
        String str = "transfer";
        boolean bool1 = ((Set)localObject).contains(str);
        if (!bool1)
        {
          localObject = "tags";
          localObject = paramString.get(localObject);
          localObject = (Set)localObject;
          str = "withdrawal";
          bool1 = ((Set)localObject).contains(str);
          if (!bool1)
          {
            localObject = "tags";
            paramString = paramString.get(localObject);
            paramString = (Set)paramString;
            localObject = "payments";
            boolean bool2 = paramString.contains(localObject);
            if (!bool2) {
              return false;
            }
          }
        }
        return true;
      }
    }
    catch (com.twelfthmile.a.a.b localb) {}
    return false;
  }
  
  public final Map a(String paramString)
  {
    Object localObject1 = b;
    paramString = paramString.toLowerCase();
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    paramString = ((b)localObject1).a(paramString).iterator();
    for (;;)
    {
      boolean bool = paramString.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (String)paramString.next();
      localObject3 = " ";
      localObject1 = Arrays.asList(((String)localObject1).split((String)localObject3));
      ((List)localObject2).addAll((Collection)localObject1);
    }
    paramString = a.a;
    paramString = a((List)localObject2, paramString);
    localObject1 = a.b;
    localObject1 = a((List)localObject2, (Map)localObject1);
    Object localObject3 = a.c;
    localObject2 = a((List)localObject2, (Map)localObject3);
    localObject3 = a((Set)localObject2);
    paramString.addAll((Collection)localObject3);
    localObject3 = new com/twelfthmile/a/d$1;
    ((d.1)localObject3).<init>(this, paramString, (Set)localObject1, (Set)localObject2);
    return (Map)localObject3;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */