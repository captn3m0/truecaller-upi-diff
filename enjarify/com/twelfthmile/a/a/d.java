package com.twelfthmile.a.a;

import java.util.Arrays;

public final class d
{
  public static int[] a(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    int i = paramCharSequence1.length();
    int j = paramCharSequence2.length();
    CharSequence localCharSequence;
    if (i > j)
    {
      localObject1 = paramCharSequence1;
      localCharSequence = paramCharSequence2;
    }
    else
    {
      localCharSequence = paramCharSequence1;
      localObject1 = paramCharSequence2;
    }
    int k = ((CharSequence)localObject1).length();
    int m = 2;
    k /= m;
    int n = 1;
    k = Math.max(k - n, 0);
    int i1 = localCharSequence.length();
    int[] arrayOfInt = new int[i1];
    int i2 = -1;
    Arrays.fill(arrayOfInt, i2);
    int i3 = ((CharSequence)localObject1).length();
    boolean[] arrayOfBoolean = new boolean[i3];
    int i4 = 0;
    Object localObject2 = null;
    int i5 = 0;
    int i8;
    for (;;)
    {
      i6 = localCharSequence.length();
      if (i4 >= i6) {
        break;
      }
      i6 = localCharSequence.charAt(i4);
      i7 = Math.max(i4 - k, 0);
      i8 = i4 + k + n;
      int i9 = ((CharSequence)localObject1).length();
      i8 = Math.min(i8, i9);
      while (i7 < i8)
      {
        i9 = arrayOfBoolean[i7];
        if (i9 == 0)
        {
          i9 = ((CharSequence)localObject1).charAt(i7);
          if (i6 == i9)
          {
            arrayOfInt[i4] = i7;
            arrayOfBoolean[i7] = n;
            i5 += 1;
            break;
          }
        }
        i7 += 1;
      }
      i4 += 1;
    }
    char[] arrayOfChar = new char[i5];
    localObject2 = new char[i5];
    int i6 = 0;
    int i7 = 0;
    for (;;)
    {
      i8 = localCharSequence.length();
      if (i6 >= i8) {
        break;
      }
      i8 = arrayOfInt[i6];
      if (i8 != i2)
      {
        i8 = localCharSequence.charAt(i6);
        arrayOfChar[i7] = i8;
        i7 += 1;
      }
      i6 += 1;
    }
    i1 = 0;
    arrayOfInt = null;
    i2 = 0;
    for (;;)
    {
      i6 = ((CharSequence)localObject1).length();
      if (i1 >= i6) {
        break;
      }
      i6 = arrayOfBoolean[i1];
      if (i6 != 0)
      {
        i6 = ((CharSequence)localObject1).charAt(i1);
        localObject2[i2] = i6;
        i2 += 1;
      }
      i1 += 1;
    }
    i = 0;
    Object localObject1 = null;
    i1 = 0;
    arrayOfInt = null;
    for (;;)
    {
      i2 = arrayOfChar.length;
      if (i >= i2) {
        break;
      }
      i2 = arrayOfChar[i];
      i3 = localObject2[i];
      if (i2 != i3) {
        i1 += 1;
      }
      i += 1;
    }
    i = 0;
    localObject1 = null;
    k = 0;
    arrayOfChar = null;
    for (;;)
    {
      i3 = localCharSequence.length();
      i2 = Math.min(4, i3);
      if (i >= i2) {
        break;
      }
      i3 = paramCharSequence1.charAt(i);
      localObject2 = paramCharSequence2;
      i6 = paramCharSequence2.charAt(i);
      if (i3 != i6) {
        break;
      }
      k += 1;
      i += 1;
    }
    localObject1 = new int[3];
    localObject1[0] = i5;
    localObject1[n] = i1;
    localObject1[m] = k;
    return (int[])localObject1;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.a.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */