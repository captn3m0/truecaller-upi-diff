package com.twelfthmile.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

final class a
{
  Map a;
  Map b;
  Map c;
  Map d;
  
  a(JSONObject paramJSONObject1, JSONObject paramJSONObject2, JSONObject paramJSONObject3)
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    a = ((Map)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = ((Map)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    c = ((Map)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    d = ((Map)localObject);
    localObject = a;
    a(paramJSONObject1, (Map)localObject);
    paramJSONObject1 = b;
    a(paramJSONObject2, paramJSONObject1);
    paramJSONObject1 = c;
    paramJSONObject2 = d;
    a(paramJSONObject3, paramJSONObject1, paramJSONObject2);
  }
  
  private static void a(String paramString1, String paramString2, Map paramMap)
  {
    Object localObject = (List)paramMap.get(paramString1);
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
    }
    ((List)localObject).add(paramString2);
    paramMap.put(paramString1, localObject);
  }
  
  private static void a(JSONObject paramJSONObject, Map paramMap)
  {
    Iterator localIterator = paramJSONObject.keys();
    boolean bool = localIterator.hasNext();
    if (bool)
    {
      String str1 = localIterator.next().toString();
      JSONArray localJSONArray = paramJSONObject.getJSONArray(str1);
      int i = 0;
      for (;;)
      {
        int j = localJSONArray.length();
        if (i >= j) {
          break;
        }
        String str2 = localJSONArray.getString(i);
        a(str2, str1, paramMap);
        i += 1;
      }
    }
  }
  
  private static void a(JSONObject paramJSONObject, Map paramMap1, Map paramMap2)
  {
    Iterator localIterator = paramJSONObject.keys();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str1 = localIterator.next().toString();
      Object localObject1 = paramJSONObject.getJSONObject(str1);
      Object localObject2 = ((JSONObject)localObject1).getJSONArray("tokens");
      int i = 0;
      int j = 0;
      String str2 = null;
      for (;;)
      {
        int k = ((JSONArray)localObject2).length();
        if (j >= k) {
          break;
        }
        String str3 = ((JSONArray)localObject2).getString(j);
        a(str3, str1, paramMap1);
        j += 1;
      }
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      str2 = "tags";
      localObject1 = ((JSONObject)localObject1).getJSONArray(str2);
      for (;;)
      {
        j = ((JSONArray)localObject1).length();
        if (i >= j) {
          break;
        }
        str2 = ((JSONArray)localObject1).getString(i);
        ((List)localObject2).add(str2);
        i += 1;
      }
      paramMap2.put(str1, localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */