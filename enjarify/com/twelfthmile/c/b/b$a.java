package com.twelfthmile.c.b;

import java.util.HashMap;
import java.util.Map;

public final class b$a
{
  Map a;
  
  private b$a()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    a = localHashMap;
  }
  
  public final boolean a(long paramLong)
  {
    Object localObject1 = a;
    Long localLong = Long.valueOf(paramLong);
    localObject1 = ((Map)localObject1).get(localLong);
    if (localObject1 != null)
    {
      localObject1 = a;
      Object localObject2 = Long.valueOf(paramLong);
      localObject2 = (Boolean)((Map)localObject1).get(localObject2);
      boolean bool = ((Boolean)localObject2).booleanValue();
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.c.b.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */