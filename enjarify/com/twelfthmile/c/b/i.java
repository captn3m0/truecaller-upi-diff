package com.twelfthmile.c.b;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public final class i
{
  h a;
  b b;
  
  i(h paramh, b paramb)
  {
    a = paramh;
    b = paramb;
  }
  
  private void a(h paramh)
  {
    boolean bool = paramh.a();
    if (bool)
    {
      paramh = a.entrySet().iterator();
      for (;;)
      {
        bool = paramh.hasNext();
        if (!bool) {
          break;
        }
        Object localObject1 = (Map.Entry)paramh.next();
        Object localObject2 = (String)((Map.Entry)localObject1).getKey();
        localObject1 = (h)((Map.Entry)localObject1).getValue();
        String str1 = ":";
        localObject2 = ((String)localObject2).split(str1);
        int i = localObject2.length;
        int j = 2;
        if (i > j)
        {
          str1 = localObject2[j];
        }
        else
        {
          i = 0;
          str1 = null;
        }
        d locald = b;
        int k = 1;
        String str2 = localObject2[k];
        localObject2 = localObject2[0];
        b localb = b;
        locald.a(str2, str1, (String)localObject2, localb);
        a((h)localObject1);
      }
    }
  }
  
  final void a()
  {
    h localh = a;
    a(localh);
  }
  
  public final void a(e parame)
  {
    h localh = a;
    c localc = new com/twelfthmile/c/b/c;
    long l1 = a;
    long l2 = b;
    localc.<init>(l1, l2);
    int i = 0;
    for (;;)
    {
      Object localObject = d;
      int j = ((List)localObject).size();
      if ((i >= j) || (localh == null)) {
        break;
      }
      localObject = b;
      localh = localh.a(parame, (b)localObject, i);
      localh.a(localc);
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.c.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */