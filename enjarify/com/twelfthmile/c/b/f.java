package com.twelfthmile.c.b;

import com.twelfthmile.c.c.c;
import com.twelfthmile.c.c.c.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class f
{
  private int a;
  private i b;
  private i c;
  private JSONObject d;
  private JSONObject e;
  private boolean f;
  
  public f(b paramb, String paramString)
  {
    Object localObject = null;
    f = false;
    try
    {
      localObject = new com/google/gson/f;
      ((com.google.gson.f)localObject).<init>();
      Class localClass = a.class;
      paramString = ((com.google.gson.f)localObject).a(paramString, localClass);
      paramString = (a)paramString;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      paramString = null;
    }
    if (paramString != null)
    {
      localObject = a;
      if (localObject != null)
      {
        localObject = new com/twelfthmile/c/b/i;
        paramString = a;
        ((i)localObject).<init>(paramString, paramb);
        break label93;
      }
    }
    localObject = new com/twelfthmile/c/b/i;
    paramString = new com/twelfthmile/c/b/h;
    paramString.<init>();
    ((i)localObject).<init>(paramString, paramb);
    label93:
    b = ((i)localObject);
    int i = e;
    a = i;
    i = a;
    if (i == 0) {
      paramb = ab;
    } else {
      paramb = aa;
    }
    d = paramb;
  }
  
  private void b(g paramg)
  {
    paramg = e(paramg).iterator();
    for (;;)
    {
      boolean bool = paramg.hasNext();
      if (!bool) {
        break;
      }
      e locale = (e)paramg.next();
      i locali = c;
      locali.a(locale);
    }
  }
  
  private void c(g paramg)
  {
    Object localObject = d(paramg);
    if (localObject != null)
    {
      i locali = b;
      locali.a((e)localObject);
    }
    paramg = f(paramg);
    if (paramg != null)
    {
      localObject = e;
      if (localObject != null)
      {
        localObject = b;
        ((i)localObject).a(paramg);
      }
    }
  }
  
  private e d(g paramg)
  {
    Object localObject1 = null;
    e locale;
    try
    {
      localObject2 = d;
      if (localObject2 != null)
      {
        localObject2 = d;
        Object localObject3 = paramg.a();
        boolean bool1 = ((JSONObject)localObject2).has((String)localObject3);
        if (bool1)
        {
          localObject2 = new java/util/ArrayList;
          ((ArrayList)localObject2).<init>();
          localObject3 = new java/util/ArrayList;
          ((ArrayList)localObject3).<init>();
          locale = new com/twelfthmile/c/b/e;
          Object localObject4 = paramg.a();
          long l1 = paramg.b();
          long l2 = paramg.c();
          Object localObject5 = locale;
          locale.<init>((String)localObject4, l1, l2);
          try
          {
            localObject1 = paramg.a();
            c = ((String)localObject1);
            localObject1 = d;
            localObject5 = paramg.a();
            localObject1 = ((JSONObject)localObject1).getJSONObject((String)localObject5);
            localObject5 = "attributes";
            localObject1 = ((JSONObject)localObject1).getJSONArray((String)localObject5);
            Object localObject6 = "";
            localObject4 = "";
            int i = 0;
            localObject5 = null;
            for (;;)
            {
              int j = ((JSONArray)localObject1).length();
              if (i >= j) {
                break;
              }
              try
              {
                j = a;
                int k = 1;
                if (j == k)
                {
                  localObject7 = ((JSONArray)localObject1).getJSONArray(i);
                  Object localObject8 = localObject6;
                  localObject6 = localObject4;
                  int m = 0;
                  float f1 = 0.0F;
                  localObject4 = null;
                  try
                  {
                    for (;;)
                    {
                      int i1 = ((JSONArray)localObject7).length();
                      if (m < i1)
                      {
                        localObject8 = ((JSONArray)localObject7).getString(m);
                        try
                        {
                          localObject6 = paramg.a((String)localObject8);
                          String str = "";
                          boolean bool4 = ((String)localObject6).equals(str);
                          int n;
                          if (!bool4)
                          {
                            localObject4 = "amt";
                            boolean bool3 = ((String)localObject8).contains((CharSequence)localObject4);
                            if (bool3)
                            {
                              localObject4 = paramg.b((String)localObject8);
                              float f2 = ((Float)localObject4).floatValue();
                              bool4 = true;
                              float f3 = Float.MIN_VALUE;
                              boolean bool2 = f2 < f3;
                              if (!bool2)
                              {
                                localObject4 = "";
                              }
                              else
                              {
                                f1 = ((Float)localObject4).floatValue();
                                double d1 = f1;
                                d1 = Math.ceil(d1);
                                n = (int)d1;
                                localObject4 = String.valueOf(n);
                              }
                              localObject6 = localObject8;
                              break label426;
                            }
                          }
                          else
                          {
                            n += 1;
                          }
                        }
                        catch (Exception localException1)
                        {
                          localObject4 = localObject6;
                          localObject6 = localObject8;
                          break label449;
                        }
                      }
                    }
                    localObject4 = localObject6;
                    localObject6 = localObject8;
                  }
                  catch (Exception localException2)
                  {
                    localObject4 = localObject6;
                    localObject6 = localObject8;
                    break label449;
                  }
                }
                localObject6 = ((JSONArray)localObject1).getString(i);
                localObject4 = paramg.a((String)localObject6);
                label426:
                ((List)localObject3).add(localObject6);
                ((List)localObject2).add(localObject4);
              }
              catch (Exception localException3)
              {
                label449:
                ((List)localObject3).add(localObject6);
                Object localObject7 = "";
                ((List)localObject2).add(localObject7);
              }
              i += 1;
            }
            e = ((List)localObject3);
            d = ((List)localObject2);
          }
          catch (Exception paramg)
          {
            break label507;
          }
        }
      }
      return null;
    }
    catch (Exception paramg)
    {
      locale = null;
      label507:
      localObject1 = new java/lang/StringBuilder;
      Object localObject2 = "Error while createPruneDataTemplate: ";
      ((StringBuilder)localObject1).<init>((String)localObject2);
      paramg = paramg.getMessage();
      ((StringBuilder)localObject1).append(paramg);
      paramg = ((StringBuilder)localObject1).toString();
      com.twelfthmile.c.c.b.a(paramg);
    }
    return locale;
  }
  
  private List e(g paramg)
  {
    Object localObject1 = this;
    g localg = paramg;
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    for (;;)
    {
      try
      {
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        localArrayList2 = new java/util/ArrayList;
        localArrayList2.<init>();
        JSONObject localJSONObject = e;
        Object localObject3 = "rules";
        localJSONObject = localJSONObject.getJSONObject((String)localObject3);
        localObject3 = e;
        localObject4 = "normalisations";
        localObject3 = ((JSONObject)localObject3).getJSONObject((String)localObject4);
        localObject4 = paramg.a();
        localObject3 = ((JSONObject)localObject3).optJSONObject((String)localObject4);
        if (localObject3 == null) {
          return localArrayList1;
        }
        localObject4 = "";
        localObject5 = "";
        Object localObject6 = localJSONObject.keySet();
        localObject6 = ((Set)localObject6).iterator();
        boolean bool1 = ((Iterator)localObject6).hasNext();
        if (bool1)
        {
          Object localObject7 = ((Iterator)localObject6).next();
          localObject7 = (String)localObject7;
          Object localObject8 = new com/twelfthmile/c/b/e;
          localObject9 = paramg.a();
          long l1 = paramg.b();
          long l2 = paramg.c();
          localObject10 = localObject8;
          localObject1 = localObject8;
          ((e)localObject8).<init>((String)localObject9, l1, l2);
          c = ((String)localObject7);
          localObject7 = localJSONObject.getJSONObject((String)localObject7);
          localObject10 = "rule";
          localObject7 = ((JSONObject)localObject7).getJSONObject((String)localObject10);
          localObject10 = "attributes";
          localObject7 = ((JSONObject)localObject7).getJSONArray((String)localObject10);
          localObject9 = localObject5;
          localObject5 = localObject4;
          i = 0;
          localObject4 = null;
          int j = ((JSONArray)localObject7).length();
          if (i < j) {
            try
            {
              JSONArray localJSONArray = ((JSONArray)localObject7).getJSONArray(i);
              localObject11 = localObject9;
              localObject9 = localObject5;
              int k = 0;
              float f1 = 0.0F;
              localObject5 = null;
              try
              {
                int n = localJSONArray.length();
                if (k < n)
                {
                  localObject11 = localJSONArray.getString(k);
                  localObject8 = "attributes";
                  localObject8 = ((JSONObject)localObject3).optJSONObject((String)localObject8);
                  if (localObject8 != null)
                  {
                    localObject8 = ((JSONObject)localObject8).getString((String)localObject11);
                    localObject9 = localg.a((String)localObject8);
                  }
                  else
                  {
                    localObject9 = "";
                  }
                  localObject8 = "";
                  try
                  {
                    boolean bool4 = ((String)localObject9).equals(localObject8);
                    if (!bool4)
                    {
                      localObject5 = "amt";
                      boolean bool3 = ((String)localObject11).contains((CharSequence)localObject5);
                      if (bool3)
                      {
                        localObject5 = "attributes";
                        localObject5 = ((JSONObject)localObject3).optJSONObject((String)localObject5);
                        if (localObject5 != null)
                        {
                          localObject5 = ((JSONObject)localObject5).getString((String)localObject11);
                          localObject5 = localg.b((String)localObject5);
                        }
                        else
                        {
                          localObject5 = localg.b((String)localObject11);
                        }
                        float f2 = ((Float)localObject5).floatValue();
                        bool4 = true;
                        float f3 = Float.MIN_VALUE;
                        boolean bool2 = f2 < f3;
                        if (!bool2)
                        {
                          localObject5 = "";
                        }
                        else
                        {
                          f1 = ((Float)localObject5).floatValue();
                          localObject12 = localObject9;
                          d1 = f1;
                        }
                      }
                    }
                  }
                  catch (Exception localException2)
                  {
                    double d1;
                    int m;
                    localObject12 = localObject9;
                  }
                }
              }
              catch (Exception localException4)
              {
                Object localObject12;
                localObject5 = localObject9;
              }
            }
            catch (Exception localException5)
            {
              localObject11 = localObject9;
            }
          }
        }
      }
      catch (Exception localException1)
      {
        ArrayList localArrayList2;
        Object localObject4;
        Object localObject5;
        Object localObject9;
        Object localObject10;
        int i;
        Object localObject11;
        localObject1 = new java/lang/StringBuilder;
        Object localObject2 = "Error while createHeteroPruneDataTemplate: ";
        ((StringBuilder)localObject1).<init>((String)localObject2);
        String str = localException1.getMessage();
        ((StringBuilder)localObject1).append(str);
        str = ((StringBuilder)localObject1).toString();
        com.twelfthmile.c.c.b.a(str);
      }
      try
      {
        d1 = Math.ceil(d1);
        m = (int)d1;
        localObject5 = String.valueOf(m);
        localObject9 = localObject11;
      }
      catch (Exception localException6)
      {
        continue;
      }
      localObject12 = localObject9;
      localObject9 = localObject11;
      localObject5 = localObject12;
      continue;
      localObject12 = localObject9;
      m += 1;
      continue;
      localObject5 = localObject12;
      continue;
      localObject5 = localObject9;
      localObject9 = localObject11;
      try
      {
        localArrayList2.add(localObject9);
        ((List)localObject2).add(localObject5);
      }
      catch (Exception localException3)
      {
        localObject11 = localObject9;
      }
      localArrayList2.add(localObject11);
      localObject10 = "";
      ((List)localObject2).add(localObject10);
      localObject9 = localObject11;
      i += 1;
      continue;
      e = localArrayList2;
      d = ((List)localObject2);
      localArrayList1.add(localObject1);
      localObject4 = localObject5;
      localObject5 = localObject9;
      localObject1 = this;
    }
    return localArrayList1;
  }
  
  private e f(g paramg)
  {
    Object localObject1 = null;
    e locale;
    try
    {
      localObject2 = d;
      if (localObject2 == null) {
        return null;
      }
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      locale = new com/twelfthmile/c/b/e;
      String str1 = paramg.a();
      long l1 = paramg.b();
      long l2 = paramg.c();
      Object localObject3 = locale;
      locale.<init>(str1, l1, l2);
      try
      {
        localObject3 = paramg.a();
        c = ((String)localObject3);
        localObject3 = paramg.a();
        int i = -1;
        int j = ((String)localObject3).hashCode();
        int k = -1301422793;
        String str2;
        boolean bool2;
        if (j != k)
        {
          k = -194258755;
          if (j != k)
          {
            k = 1240550297;
            if (j == k)
            {
              str2 = "GRM_BANK";
              bool2 = ((String)localObject3).equals(str2);
              if (bool2)
              {
                i = 0;
                str1 = null;
              }
            }
          }
          else
          {
            str2 = "GRM_EVENT";
            bool2 = ((String)localObject3).equals(str2);
            if (bool2) {
              i = 1;
            }
          }
        }
        else
        {
          str2 = "GRM_TRAVEL";
          bool2 = ((String)localObject3).equals(str2);
          if (bool2) {
            i = 2;
          }
        }
        switch (i)
        {
        default: 
          paramg = null;
          break;
        case 2: 
          localObject3 = "pnr_id";
          localObject3 = paramg.a((String)localObject3);
          str1 = "";
          boolean bool1 = ((String)localObject3).equals(str1);
          if (!bool1)
          {
            localObject1 = "pnr_id";
            paramg = (g)localObject3;
          }
          else
          {
            localObject3 = "alert_id";
            paramg = paramg.a((String)localObject3);
            localObject3 = "";
            bool2 = paramg.equals(localObject3);
            if (!bool2) {
              localObject1 = "pnr_id";
            }
          }
          break;
        case 1: 
          localObject3 = "booking_id";
          paramg = paramg.a((String)localObject3);
          localObject3 = "";
          bool2 = paramg.equals(localObject3);
          if (!bool2) {
            localObject1 = "booking_id";
          }
          break;
        case 0: 
          localObject3 = "ref_id";
          paramg = paramg.a((String)localObject3);
          localObject3 = "";
          bool2 = paramg.equals(localObject3);
          if (!bool2) {
            localObject1 = "ref_id";
          }
          break;
        }
        if (localObject1 == null) {
          break label473;
        }
        localArrayList.add(localObject1);
        ((List)localObject2).add(paramg);
        e = localArrayList;
        d = ((List)localObject2);
      }
      catch (Exception paramg) {}
      localObject1 = new java/lang/StringBuilder;
    }
    catch (Exception paramg)
    {
      locale = null;
    }
    Object localObject2 = "Error while createPruneDataTemplateID: ";
    ((StringBuilder)localObject1).<init>((String)localObject2);
    paramg = paramg.getMessage();
    ((StringBuilder)localObject1).append(paramg);
    paramg = ((StringBuilder)localObject1).toString();
    com.twelfthmile.c.c.b.a(paramg);
    label473:
    return locale;
  }
  
  protected final void a()
  {
    int i = a;
    int j = 1;
    if (i == j)
    {
      localObject = b;
      ((i)localObject).a();
    }
    boolean bool = f;
    if (bool)
    {
      localObject = c;
      ((i)localObject).a();
    }
    b.b.a();
    Object localObject = c;
    if (localObject != null)
    {
      localObject = b;
      ((b)localObject).a();
    }
  }
  
  protected final void a(g paramg)
  {
    c(paramg);
    JSONObject localJSONObject = e;
    if (localJSONObject != null) {
      b(paramg);
    }
  }
  
  public final String b()
  {
    com.google.gson.f localf = new com/google/gson/f;
    localf.<init>();
    a locala = new com/twelfthmile/c/b/a;
    locala.<init>();
    Object localObject = c;
    if (localObject != null)
    {
      localObject = a;
      b = ((h)localObject);
    }
    localObject = b;
    if (localObject != null)
    {
      localObject = a;
      a = ((h)localObject);
    }
    return localf.b(locala);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.c.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */