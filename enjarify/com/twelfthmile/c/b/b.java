package com.twelfthmile.c.b;

import com.twelfthmile.c.c.c;
import com.twelfthmile.c.c.c.a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public abstract class b
{
  private HashMap a;
  List d;
  int e;
  
  protected b(int paramInt)
  {
    e = paramInt;
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    d = ((List)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    a = ((HashMap)localObject);
  }
  
  protected abstract int a(String paramString1, long paramLong, String paramString2);
  
  protected final String a(String paramString)
  {
    int i = e;
    int j = 1;
    if (i == j) {
      return aa.getJSONObject(paramString).getString("delta");
    }
    j = 2;
    if (i == j) {
      return ac.getJSONObject("rules").getJSONObject(paramString).getJSONObject("rule").getString("delta");
    }
    return ab.getJSONObject(paramString).getString("delta");
  }
  
  final void a()
  {
    List localList = d;
    HashMap localHashMap = a;
    a(localList, localHashMap);
  }
  
  final void a(long paramLong1, long paramLong2, int paramInt)
  {
    int i = e;
    int k = 2;
    if (i == k)
    {
      localObject1 = b.a.a.a();
      bool1 = ((b.a)localObject1).a(paramLong1);
      if (bool1)
      {
        localObject1 = b.a.a.a();
        bool1 = ((b.a)localObject1).a(paramLong2);
        if (bool1) {
          return;
        }
      }
    }
    Object localObject1 = a;
    Object localObject2 = Long.valueOf(paramLong1);
    boolean bool1 = ((HashMap)localObject1).containsKey(localObject2);
    if (bool1)
    {
      localObject1 = a;
      localObject2 = Long.valueOf(paramLong1);
      localObject1 = (b.b)((HashMap)localObject1).get(localObject2);
      int j = b;
      if (j < paramInt)
      {
        localObject1 = a;
        localObject2 = Long.valueOf(paramLong1);
        localObject1 = (b.b)((HashMap)localObject1).get(localObject2);
        b = paramInt;
      }
      localObject3 = a;
      localObject1 = Long.valueOf(paramLong1);
      localObject3 = geta;
      localObject1 = Long.valueOf(paramLong2);
      paramInt = ((List)localObject3).contains(localObject1);
      if (paramInt == 0)
      {
        localObject3 = a;
        localObject1 = Long.valueOf(paramLong1);
        localObject3 = geta;
        localObject1 = Long.valueOf(paramLong2);
        ((List)localObject3).add(localObject1);
      }
    }
    else
    {
      localObject1 = a.keySet().iterator();
      long l1;
      boolean bool4;
      do
      {
        int m;
        do
        {
          boolean bool3 = ((Iterator)localObject1).hasNext();
          l1 = Long.MAX_VALUE;
          if (!bool3) {
            break;
          }
          l2 = ((Long)((Iterator)localObject1).next()).longValue();
          localObject2 = a;
          localLong1 = Long.valueOf(l2);
          localObject2 = (b.b)((HashMap)localObject2).get(localLong1);
          m = b;
        } while (m != paramInt);
        localObject2 = a;
        Long localLong1 = Long.valueOf(l2);
        localObject2 = geta;
        localLong1 = Long.valueOf(paramLong1);
        bool4 = ((List)localObject2).contains(localLong1);
      } while (!bool4);
      break label387;
      long l2 = l1;
      label387:
      boolean bool2 = l2 < l1;
      if (bool2)
      {
        localObject3 = a;
        localObject1 = Long.valueOf(l2);
        localObject3 = geta;
        localObject1 = Long.valueOf(paramLong2);
        paramInt = ((List)localObject3).contains(localObject1);
        if (paramInt == 0)
        {
          localObject3 = a;
          localObject1 = Long.valueOf(l2);
          localObject3 = geta;
          localObject1 = Long.valueOf(paramLong2);
          ((List)localObject3).add(localObject1);
        }
      }
      else
      {
        localObject1 = a;
        localObject2 = Long.valueOf(paramLong1);
        b.b localb = new com/twelfthmile/c/b/b$b;
        localb.<init>(paramInt);
        ((HashMap)localObject1).put(localObject2, localb);
        localObject3 = a;
        localObject1 = Long.valueOf(paramLong1);
        localObject3 = geta;
        localObject1 = Long.valueOf(paramLong1);
        ((List)localObject3).add(localObject1);
        localObject3 = a;
        localObject1 = Long.valueOf(paramLong1);
        localObject3 = geta;
        localObject1 = Long.valueOf(paramLong2);
        ((List)localObject3).add(localObject1);
      }
    }
    Object localObject3 = b.a.a.a();
    localObject1 = new Long[k];
    k = 0;
    Object localObject4 = Long.valueOf(paramLong1);
    localObject1[0] = localObject4;
    int n = 1;
    Long localLong2 = Long.valueOf(paramLong2);
    localObject1[n] = localLong2;
    localObject4 = Arrays.asList((Object[])localObject1).iterator();
    for (;;)
    {
      boolean bool5 = ((Iterator)localObject4).hasNext();
      if (!bool5) {
        break;
      }
      long l3 = ((Long)((Iterator)localObject4).next()).longValue();
      Map localMap = a;
      localLong2 = Long.valueOf(l3);
      Boolean localBoolean = Boolean.TRUE;
      localMap.put(localLong2, localBoolean);
    }
  }
  
  protected abstract void a(List paramList, HashMap paramHashMap);
  
  protected abstract boolean a(long paramLong, String paramString);
}

/* Location:
 * Qualified Name:     com.twelfthmile.c.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */