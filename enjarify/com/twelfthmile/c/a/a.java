package com.twelfthmile.c.a;

import com.twelfthmile.c.b.b;
import com.twelfthmile.c.c.d;
import java.util.HashMap;
import java.util.List;

public abstract class a
  extends b
{
  protected a()
  {
    super(0);
  }
  
  public final int a(String paramString1, long paramLong, String paramString2)
  {
    return 0;
  }
  
  public abstract void a(List paramList);
  
  public final void a(List paramList, HashMap paramHashMap)
  {
    a(paramList);
  }
  
  public final boolean a(long paramLong, String paramString)
  {
    paramString = a(paramString);
    float f1 = d.a(paramString);
    long l = 3600000L;
    paramLong /= l;
    float f2 = (float)paramLong;
    boolean bool = f2 < f1;
    return !bool;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.c.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */