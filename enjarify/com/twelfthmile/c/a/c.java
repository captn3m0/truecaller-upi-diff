package com.twelfthmile.c.a;

import com.twelfthmile.c.b.b;
import com.twelfthmile.c.c.d;
import java.util.HashMap;
import java.util.List;

public abstract class c
  extends b
{
  protected c()
  {
    super(1);
  }
  
  public final int a(String paramString1, long paramLong, String paramString2)
  {
    paramString2 = a(paramString2);
    float f1 = d.a(paramString2);
    int i = paramString1.hashCode();
    int j = 2;
    String str;
    boolean bool3;
    boolean bool4;
    switch (i)
    {
    default: 
      break;
    case 2001868029: 
      str = "due_amt";
      boolean bool1 = paramString1.equals(str);
      if (bool1)
      {
        int k = 6;
        f2 = 8.4E-45F;
      }
      break;
    case 1615403742: 
      str = "alert_id";
      boolean bool2 = paramString1.equals(str);
      if (bool2)
      {
        int m = 4;
        f2 = 5.6E-45F;
      }
      break;
    case -820075192: 
      str = "vendor";
      bool3 = paramString1.equals(str);
      if (bool3)
      {
        bool3 = true;
        f2 = Float.MIN_VALUE;
      }
      break;
    case -868537476: 
      str = "to_loc";
      bool3 = paramString1.equals(str);
      if (bool3)
      {
        int n = 5;
        f2 = 7.0E-45F;
      }
      break;
    case -934835129: 
      str = "ref_id";
      bool4 = paramString1.equals(str);
      if (bool4)
      {
        bool4 = false;
        f2 = 0.0F;
        paramString1 = null;
      }
      break;
    case -983424250: 
      str = "pnr_id";
      bool4 = paramString1.equals(str);
      if (bool4)
      {
        int i1 = 3;
        f2 = 4.2E-45F;
      }
      break;
    case -1374560575: 
      str = "booking_id";
      boolean bool5 = paramString1.equals(str);
      if (bool5)
      {
        i2 = 2;
        f2 = 2.8E-45F;
      }
      break;
    }
    int i2 = -1;
    float f2 = 0.0F / 0.0F;
    switch (i2)
    {
    default: 
      break;
    case 0: 
    case 1: 
    case 2: 
    case 3: 
    case 4: 
    case 5: 
    case 6: 
      long l = 3600000L;
      paramLong /= l;
      f2 = (float)paramLong;
      boolean bool6 = f2 < f1;
      if (!bool6) {
        return j;
      }
      break;
    }
    return 0;
  }
  
  protected abstract void a(HashMap paramHashMap);
  
  public final void a(List paramList, HashMap paramHashMap)
  {
    a(paramHashMap);
  }
  
  public final boolean a(long paramLong, String paramString)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.c.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */