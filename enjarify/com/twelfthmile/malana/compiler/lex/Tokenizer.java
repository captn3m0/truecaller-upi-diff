package com.twelfthmile.malana.compiler.lex;

import com.twelfthmile.malana.compiler.Compiler;
import com.twelfthmile.malana.compiler.Seeder;
import com.twelfthmile.malana.compiler.datastructure.TokenTrie;
import com.twelfthmile.malana.compiler.parser.Parser;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.types.CustomTokenResponse;
import com.twelfthmile.malana.compiler.types.Pair;
import com.twelfthmile.malana.compiler.types.Req;
import com.twelfthmile.malana.compiler.types.Request;
import com.twelfthmile.malana.compiler.types.Response;
import com.twelfthmile.malana.compiler.types.TokenizerType;
import com.twelfthmile.malana.compiler.types.Triplet;
import com.twelfthmile.malana.controller.Controller;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Tokenizer
{
  private static boolean checkAheadForToken(TokenTrie paramTokenTrie, String paramString, int paramInt, char paramChar)
  {
    int i = paramInt + 2;
    int j = paramString.length();
    if (i < j)
    {
      Object localObject1 = next;
      Object localObject2 = Character.valueOf(paramChar);
      localObject1 = getnext.keySet().iterator();
      int k;
      boolean bool3;
      int n;
      do
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (Character)((Iterator)localObject1).next();
        k = ((Character)localObject2).charValue();
        bool3 = true;
        n = paramInt + 1;
        n = Character.toLowerCase(paramString.charAt(n));
      } while (k != n);
      Object localObject3 = next;
      Character localCharacter = Character.valueOf(paramChar);
      localObject3 = (TokenTrie)getnext.get(localObject2);
      boolean bool2 = leaf;
      if (bool2) {
        return bool3;
      }
      localObject3 = next;
      localCharacter = Character.valueOf(paramChar);
      localObject3 = getnext;
      localObject2 = getnext.keySet().iterator();
      int m;
      do
      {
        bool2 = ((Iterator)localObject2).hasNext();
        if (!bool2) {
          break;
        }
        localObject3 = (Character)((Iterator)localObject2).next();
        m = ((Character)localObject3).charValue();
        n = Character.toLowerCase(paramString.charAt(i));
      } while (m != n);
      return bool3;
    }
    return false;
  }
  
  private static Pair checkIfTokenizableWithoutSpace(Request paramRequest, TokenizerType paramTokenizerType, HashMap paramHashMap)
  {
    Object localObject1 = paramRequest;
    Object localObject2 = paramTokenizerType;
    String str1 = message;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    TokenTrie localTokenTrie = Controller.getInstance().getSeedData().getTokenTrie();
    String str2 = str1.toLowerCase();
    int i = index;
    i = getTokenEndDelimeterIndex(str1, i);
    int j = index;
    index = j;
    Object localObject3 = localTokenTrie;
    for (;;)
    {
      int m = index;
      if (m >= i) {
        break label1786;
      }
      m = index;
      m = str2.charAt(m);
      boolean bool2 = child;
      int i2 = 2;
      int i3 = 0;
      Object localObject4 = null;
      int i4 = 1;
      Object localObject5;
      Object localObject6;
      Object localObject7;
      Object localObject8;
      int i6;
      Object localObject10;
      Object localObject11;
      Object localObject12;
      int i8;
      String str3;
      if (bool2)
      {
        localObject5 = next;
        localObject6 = Character.valueOf(m);
        bool2 = ((HashMap)localObject5).containsKey(localObject6);
        if (bool2)
        {
          localObject5 = next;
          localObject6 = Character.valueOf(m);
          localObject5 = (TokenTrie)((HashMap)localObject5).get(localObject6);
          bool2 = leaf;
          if (bool2)
          {
            localObject5 = next;
            localObject6 = Character.valueOf(m);
            localObject7 = gettoken_name;
            localObject5 = next;
            localObject6 = Character.valueOf(m);
            localObject5 = gettoken_name_aux;
            int i5 = index;
            boolean bool3 = checkAheadForToken((TokenTrie)localObject3, str1, i5, m);
            if (bool3)
            {
              localObject5 = next;
              localObject8 = Character.valueOf(m);
              localObject5 = (TokenTrie)((HashMap)localObject5).get(localObject8);
              possibleToken = ((TokenTrie)localObject5);
              i1 = index;
              possibleTokenIndex = i1;
            }
            else
            {
              i6 = index;
              int i7 = start;
              i6 -= i7;
              if (i6 >= i2)
              {
                i7 = index + i4;
                localObject9 = isSuffix(str2, i7);
                if (localObject9 != null)
                {
                  localObject6 = next;
                  localObject10 = Character.valueOf(m);
                  localObject10 = tokenExistsWithSuffix((TokenTrie)((HashMap)localObject6).get(localObject10), (String)localObject9);
                  if (localObject10 != null)
                  {
                    localObject7 = token_name;
                    localObject5 = token_name_aux;
                    localObject11 = localObject5;
                    localObject12 = localObject7;
                  }
                  else
                  {
                    localObject11 = localObject5;
                    localObject12 = localObject7;
                  }
                  localObject5 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
                  i4 = start;
                  i6 = index;
                  i8 = ((String)localObject9).length();
                  i6 += i8;
                  str3 = str1.substring(i4, i6);
                  i4 = start;
                  localObject8 = new String[i2];
                  localObject6 = next;
                  localObject7 = Character.valueOf(m);
                  localObject6 = gettoken_type_name;
                  localObject8[0] = localObject6;
                  localObject4 = next;
                  localObject6 = Character.valueOf(m);
                  localObject4 = gettoken_type_val;
                  i6 = 1;
                  localObject8[i6] = localObject4;
                  ((GrammarDataObject)localObject5).<init>((String)localObject12, (String)localObject11, str3, i4, (String[])localObject8);
                  localArrayList.add(localObject5);
                  i1 = index;
                  i7 = ((String)localObject9).length();
                  i1 += i7;
                  nextIndex = i1;
                  i4 = 1;
                  break label1002;
                }
              }
              i7 = start;
              localObject9 = str1.substring(i7);
              localObject10 = next;
              localObject6 = Character.valueOf(m);
              localObject10 = gettoken_type_val;
              i6 = index;
              i3 = start;
              i6 -= i3;
              i3 = 1;
              i6 += i3;
              localObject9 = Classifier.classifyUsingPrefix((String)localObject9, (String)localObject7, (String)localObject10, i6, (Request)localObject1);
              if (localObject9 != null)
              {
                localObject5 = (GrammarDataObject)((Pair)localObject9).getB();
                i2 = start;
                index = i2;
                getBclassified = i3;
                localObject5 = ((Pair)localObject9).getB();
                localArrayList.add(localObject5);
                i1 = start;
                localObject9 = (Integer)((Pair)localObject9).getA();
                i7 = ((Integer)localObject9).intValue();
                i1 += i7;
                nextIndex = i1;
                i4 = 1;
                break label1002;
              }
              localObject9 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
              i3 = start;
              i4 = index + 1;
              String str4 = str1.substring(i3, i4);
              i3 = start;
              localObject8 = new String[i2];
              localObject10 = next;
              localObject6 = Character.valueOf(m);
              localObject10 = gettoken_type_name;
              i6 = 0;
              localObject8[0] = localObject10;
              localObject10 = next;
              localObject6 = Character.valueOf(m);
              localObject10 = gettoken_type_val;
              int i9 = 1;
              localObject8[i9] = localObject10;
              localObject6 = localObject9;
              ((GrammarDataObject)localObject9).<init>((String)localObject7, (String)localObject5, str4, i3, (String[])localObject8);
              localArrayList.add(localObject9);
              i1 = index + 1;
              nextIndex = i1;
              i4 = 1;
              break label1002;
            }
          }
          i4 = 0;
          localObject10 = null;
          label1002:
          if (i4 == 0)
          {
            localObject3 = next;
            localObject13 = Character.valueOf(m);
            localObject3 = (TokenTrie)((HashMap)localObject3).get(localObject13);
            localObject9 = paramHashMap;
            break label1600;
          }
          localObject9 = paramHashMap;
          break label1600;
        }
      }
      int n = index;
      int i1 = start;
      n -= i1;
      i1 = 3;
      if (n >= i1)
      {
        n = 1;
      }
      else
      {
        n = 0;
        localObject13 = null;
      }
      if (n != 0)
      {
        localObject13 = ((TokenTrie)localObject3).fastForward();
        if (localObject13 != null)
        {
          localObject5 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
          localObject9 = token_name;
          localObject13 = token_name_aux;
          i3 = start;
          i4 = index;
          str3 = str1.substring(i3, i4);
          i3 = start;
          localObject8 = new String[i2];
          localObject10 = token_type_name;
          localObject6 = null;
          localObject8[0] = localObject10;
          localObject10 = token_type_val;
          i6 = 1;
          localObject8[i6] = localObject10;
          localObject12 = localObject9;
          localObject11 = localObject13;
          ((GrammarDataObject)localObject5).<init>((String)localObject9, (String)localObject13, str3, i3, (String[])localObject8);
          localArrayList.add(localObject5);
          n = index;
          nextIndex = n;
          localObject9 = paramHashMap;
          break label1597;
        }
      }
      n = start;
      Object localObject13 = str1.substring(n);
      i1 = start;
      Object localObject9 = paramHashMap;
      localObject13 = Classifier.classifyUsingGrammar((String)localObject13, i1, (Request)localObject1, paramHashMap);
      if (localObject13 != null)
      {
        localObject5 = (GrammarDataObject)((Pair)localObject13).getB();
        i1 = index;
        if (i1 < 0)
        {
          localObject5 = (GrammarDataObject)((Pair)localObject13).getB();
          i2 = start;
          index = i2;
        }
        localObject5 = (GrammarDataObject)((Pair)localObject13).getB();
        i2 = 1;
        classified = i2;
        localObject5 = ((Pair)localObject13).getB();
        localArrayList.add(localObject5);
        i1 = start;
        localObject13 = (Integer)((Pair)localObject13).getA();
        n = ((Integer)localObject13).intValue();
        i1 += n;
        nextIndex = i1;
      }
      else
      {
        localObject13 = possibleToken;
        if (localObject13 == null) {
          break;
        }
        n = possibleTokenIndex;
        i1 = possibleTokenIndex;
        i1 = nextSpace(str1.substring(i1));
        n += i1;
        nextIndex = n;
        localObject13 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
        localObject5 = possibleToken.token_name;
        localObject4 = possibleToken.token_name_aux;
        i4 = start;
        i6 = nextIndex;
        str3 = str1.substring(i4, i6);
        i4 = start;
        localObject8 = new String[i2];
        localObject6 = possibleToken.token_type_name;
        localObject7 = null;
        localObject8[0] = localObject6;
        localObject6 = possibleToken.token_type_val;
        i8 = 1;
        localObject8[i8] = localObject6;
        localObject12 = localObject5;
        localObject11 = localObject4;
        ((GrammarDataObject)localObject13).<init>((String)localObject5, (String)localObject4, str3, i4, (String[])localObject8);
        localArrayList.add(localObject13);
      }
      label1597:
      i4 = 1;
      label1600:
      if (i4 != 0)
      {
        j = nextIndex;
        n = str1.length();
        if (j < n)
        {
          j = nextIndex;
          boolean bool1 = Character.isLetterOrDigit(str1.charAt(j));
          if (!bool1)
          {
            k = nextIndex;
            k = str1.charAt(k);
            n = 41;
            if (k != n) {}
          }
          else
          {
            k = nextIndex;
            n = 1;
            k -= n;
            nextIndex = k;
            break label1707;
          }
        }
        n = 1;
        label1707:
        int k = nextIndex;
        index = k;
        k = index + n;
        start = k;
        possibleToken = null;
        k = -1;
        possibleTokenIndex = k;
        localObject3 = localTokenTrie;
      }
      else
      {
        n = 1;
      }
      i1 = index + n;
      index = i1;
    }
    return null;
    label1786:
    localObject1 = new com/twelfthmile/malana/compiler/types/Pair;
    localObject2 = Integer.valueOf(index);
    ((Pair)localObject1).<init>(localObject2, localArrayList);
    return (Pair)localObject1;
  }
  
  private static void classifyBank(Response paramResponse, String paramString)
  {
    Object localObject1 = Controller.getInstance().getSeedData().getBankRoot();
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      Object localObject2 = next;
      Object localObject3 = Character.valueOf(j);
      boolean bool2 = ((HashMap)localObject2).containsKey(localObject3);
      if (!bool2) {
        break;
      }
      localObject1 = next;
      Object localObject4 = Character.valueOf(j);
      localObject1 = (TokenTrie)((HashMap)localObject1).get(localObject4);
      boolean bool1 = leaf;
      if (bool1)
      {
        localObject4 = paramResponse.getValMap();
        localObject2 = "bank";
        localObject3 = token_name;
        ((com.twelfthmile.b.a.a)localObject4).put((String)localObject2, (String)localObject3);
      }
      i += 1;
    }
  }
  
  private static void classifyOffer(Response paramResponse, String paramString)
  {
    Object localObject1 = Controller.getInstance().getSeedData().getOfferRoot();
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      Object localObject2 = next;
      Object localObject3 = Character.valueOf(j);
      boolean bool2 = ((HashMap)localObject2).containsKey(localObject3);
      if (!bool2) {
        break;
      }
      localObject1 = next;
      Object localObject4 = Character.valueOf(j);
      localObject1 = (TokenTrie)((HashMap)localObject1).get(localObject4);
      boolean bool1 = leaf;
      if (bool1)
      {
        localObject4 = paramResponse.getValMap();
        localObject2 = "classification";
        localObject3 = token_name;
        ((com.twelfthmile.b.a.a)localObject4).put((String)localObject2, (String)localObject3);
      }
      i += 1;
    }
  }
  
  private static boolean climax(String paramString, int paramInt)
  {
    int i = paramString.length();
    if (paramInt != i)
    {
      boolean bool = goodEndings(paramString.charAt(paramInt));
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  private static boolean delimeterWithinNonDet(String paramString, int paramInt)
  {
    int i = paramString.length();
    if (paramInt < i)
    {
      i = paramString.charAt(paramInt);
      int j = 46;
      if (i != j)
      {
        i = paramString.charAt(paramInt);
        j = 13;
        if (i != j)
        {
          int k = paramString.charAt(paramInt);
          paramInt = 10;
          if (k != paramInt) {
            break label56;
          }
        }
      }
      return true;
    }
    label56:
    return false;
  }
  
  public static String getSemantics(Request paramRequest)
  {
    TokenizerType localTokenizerType = new com/twelfthmile/malana/compiler/types/TokenizerType;
    Object localObject = localTokenizerType;
    localTokenizerType.<init>(0, 0, 0, null, -1);
    localObject = initConfigMapForClassifier(paramRequest);
    parseInternal(localTokenizerType, paramRequest, (HashMap)localObject);
    return parser.semantics();
  }
  
  private static int getTokenEndDelimeterIndex(String paramString, int paramInt)
  {
    return nextDelimeterImmediate(paramString.substring(paramInt)) + paramInt;
  }
  
  public static String getTokens(Request paramRequest, List paramList)
  {
    TokenizerType localTokenizerType = new com/twelfthmile/malana/compiler/types/TokenizerType;
    Object localObject = localTokenizerType;
    localTokenizerType.<init>(0, 0, 0, null, -1);
    localObject = initConfigMapForClassifier(paramRequest);
    parseInternal(localTokenizerType, paramRequest, (HashMap)localObject);
    return parser.tokens(paramList);
  }
  
  public static String getYugaTokens(Request paramRequest, List paramList)
  {
    TokenizerType localTokenizerType = new com/twelfthmile/malana/compiler/types/TokenizerType;
    Object localObject = localTokenizerType;
    localTokenizerType.<init>(0, 0, 0, null, -1);
    localObject = initConfigMapForClassifier(paramRequest);
    parseInternal(localTokenizerType, paramRequest, (HashMap)localObject);
    return parser.yugaTokens(paramList);
  }
  
  static boolean goodEndings(char paramChar)
  {
    char c = ' ';
    if (paramChar != c)
    {
      c = ',';
      if (paramChar != c)
      {
        c = ':';
        if (paramChar != c)
        {
          c = '.';
          if (paramChar != c)
          {
            c = ')';
            if (paramChar != c)
            {
              c = '/';
              if (paramChar != c)
              {
                c = '-';
                if (paramChar != c)
                {
                  c = '(';
                  if (paramChar != c)
                  {
                    c = '"';
                    if (paramChar != c)
                    {
                      c = '=';
                      if (paramChar != c)
                      {
                        c = '<';
                        if (paramChar != c)
                        {
                          c = '>';
                          if (paramChar != c)
                          {
                            c = '\r';
                            if (paramChar != c)
                            {
                              c = '\n';
                              if (paramChar != c) {
                                return false;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return true;
  }
  
  static boolean goodEndingsLtd(char paramChar)
  {
    char c = ' ';
    if (paramChar != c)
    {
      c = ',';
      if (paramChar != c)
      {
        c = ':';
        if (paramChar != c)
        {
          c = '.';
          if (paramChar != c)
          {
            c = '/';
            if (paramChar != c)
            {
              c = '-';
              if (paramChar != c)
              {
                c = '=';
                if (paramChar != c)
                {
                  c = '\r';
                  if (paramChar != c)
                  {
                    c = '\n';
                    if (paramChar != c) {
                      return false;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return true;
  }
  
  public static String grammarOutput(Request paramRequest, List paramList)
  {
    Object localObject1 = new com/twelfthmile/malana/compiler/types/TokenizerType;
    Object localObject2 = null;
    Object localObject3 = null;
    Object localObject4 = null;
    int i = -1;
    Object localObject5 = localObject1;
    ((TokenizerType)localObject1).<init>(0, 0, 0, null, i);
    localObject5 = initConfigMapForClassifier(paramRequest);
    parseInternal((TokenizerType)localObject1, paramRequest, (HashMap)localObject5);
    localObject5 = parser;
    Object localObject6 = new com/twelfthmile/malana/compiler/types/Req;
    ((Req)localObject6).<init>(paramRequest);
    localObject5 = ((Parser)localObject5).output((Req)localObject6);
    boolean bool = ((Response)localObject5).isValid();
    if (bool) {
      Regex.refill(paramRequest, (Response)localObject5);
    } else {
      Regex.fallback(paramRequest, (Response)localObject5);
    }
    bool = ((Response)localObject5).isValid();
    if (bool)
    {
      localObject6 = ((Response)localObject5).getValMap();
      localObject2 = "date";
      bool = ((com.twelfthmile.b.a.a)localObject6).containsKey((String)localObject2);
      if (!bool)
      {
        localObject6 = ((Response)localObject5).getValMap();
        localObject2 = "date";
        localObject3 = com.twelfthmile.e.b.a.a();
        localObject4 = messageDate;
        localObject3 = ((SimpleDateFormat)localObject3).format((Date)localObject4);
        ((com.twelfthmile.b.a.a)localObject6).put((String)localObject2, (String)localObject3);
      }
      localObject6 = "GRM_BANK";
      localObject2 = ((Response)localObject5).getCategory();
      bool = ((String)localObject6).equals(localObject2);
      if (bool)
      {
        localObject6 = address;
        classifyBank((Response)localObject5, (String)localObject6);
      }
      localObject6 = "GRM_OFFERS";
      localObject2 = ((Response)localObject5).getCategory();
      bool = ((String)localObject6).equals(localObject2);
      if (bool)
      {
        localObject6 = address;
        classifyOffer((Response)localObject5, (String)localObject6);
      }
      localObject2 = parser;
      localObject3 = message;
      localObject4 = address;
      localObject7 = ((Response)localObject5).getCategory();
      localObject1 = ((Response)localObject5).getValMap();
      return ((Parser)localObject2).grammarOutput((String)localObject3, (String)localObject4, (String)localObject7, (com.twelfthmile.b.a.a)localObject1, paramList);
    }
    localObject5 = parser;
    localObject6 = message;
    localObject2 = address;
    Object localObject7 = paramList;
    return ((Parser)localObject5).grammarOutput((String)localObject6, (String)localObject2, null, null, paramList);
  }
  
  public static boolean grammarPresent(Request paramRequest, String paramString, String... paramVarArgs)
  {
    TokenizerType localTokenizerType = new com/twelfthmile/malana/compiler/types/TokenizerType;
    Object localObject = localTokenizerType;
    localTokenizerType.<init>(0, 0, 0, null, -1);
    localObject = initConfigMapForClassifier(paramRequest);
    parseInternal(localTokenizerType, paramRequest, (HashMap)localObject);
    return parser.grammarPresent(paramString, paramVarArgs);
  }
  
  private static HashMap initConfigMapForClassifier(Request paramRequest)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    SimpleDateFormat localSimpleDateFormat = com.twelfthmile.e.b.a.a();
    paramRequest = messageDate;
    paramRequest = localSimpleDateFormat.format(paramRequest);
    localHashMap.put("YUGA_CONF_DATE", paramRequest);
    return localHashMap;
  }
  
  private static boolean isNumber(char paramChar)
  {
    char c = '0';
    if (paramChar >= c)
    {
      c = '9';
      if (paramChar <= c) {
        return true;
      }
    }
    return false;
  }
  
  private static String isSuffix(String paramString, int paramInt)
  {
    paramString = paramString.substring(paramInt);
    String str = "ing";
    paramInt = paramString.startsWith(str);
    if (paramInt != 0) {
      return "ing";
    }
    str = "ed";
    paramInt = paramString.startsWith(str);
    if (paramInt != 0) {
      return "ed";
    }
    str = "s";
    paramInt = paramString.startsWith(str);
    if (paramInt != 0) {
      return "s";
    }
    str = "d";
    paramInt = paramString.startsWith(str);
    if (paramInt != 0) {
      return "d";
    }
    str = "al";
    paramInt = paramString.startsWith(str);
    if (paramInt != 0) {
      return "al";
    }
    str = "n";
    boolean bool = paramString.startsWith(str);
    if (bool) {
      return "n";
    }
    return null;
  }
  
  private static boolean isSuffix(String paramString)
  {
    String str = "ing";
    boolean bool1 = paramString.equals(str);
    if (!bool1)
    {
      str = "ed";
      bool1 = paramString.equals(str);
      if (!bool1)
      {
        str = "s";
        bool1 = paramString.equals(str);
        if (!bool1)
        {
          str = "d";
          bool1 = paramString.equals(str);
          if (!bool1)
          {
            str = "al";
            bool1 = paramString.equals(str);
            if (!bool1)
            {
              str = "n";
              boolean bool2 = paramString.equals(str);
              if (!bool2) {
                return false;
              }
            }
          }
        }
      }
    }
    return true;
  }
  
  private static int nextDelimeterImmediate(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      boolean bool = goodEndings(paramString.charAt(i));
      if (bool) {
        return i;
      }
      i += 1;
    }
    return i;
  }
  
  private static int nextDelimeterImmediateLtd(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      boolean bool = goodEndingsLtd(paramString.charAt(i));
      if (bool) {
        return i;
      }
      i += 1;
    }
    return i;
  }
  
  private static Pair nextDelimeterLtd(String paramString)
  {
    int i = 0;
    Integer localInteger1 = null;
    int j = 0;
    Integer localInteger2 = null;
    for (;;)
    {
      int k = paramString.length();
      if (j >= k) {
        break;
      }
      boolean bool1 = goodEndingsLtd(paramString.charAt(j));
      if (bool1)
      {
        for (;;)
        {
          int m = j + 1;
          int n = paramString.length();
          if (m >= n) {
            break;
          }
          boolean bool2 = goodEndingsLtd(paramString.charAt(m));
          if (!bool2) {
            break;
          }
          i += 1;
          j = m;
        }
        paramString = new com/twelfthmile/malana/compiler/types/Pair;
        localInteger2 = Integer.valueOf(j);
        localInteger1 = Integer.valueOf(i);
        paramString.<init>(localInteger2, localInteger1);
        return paramString;
      }
      j += 1;
    }
    paramString = new com/twelfthmile/malana/compiler/types/Pair;
    localInteger2 = Integer.valueOf(j);
    localInteger1 = Integer.valueOf(0);
    paramString.<init>(localInteger2, localInteger1);
    return paramString;
  }
  
  private static int nextFullStop(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int k = 46;
      if (j == k)
      {
        j = i + 1;
        k = paramString.length();
        if (j != k)
        {
          k = paramString.charAt(j);
          int m = 32;
          if (k != m)
          {
            k = paramString.charAt(j);
            m = 13;
            if (k != m)
            {
              j = paramString.charAt(j);
              k = 10;
              if (j != k) {
                break label88;
              }
            }
          }
        }
        return i;
      }
      label88:
      i += 1;
    }
    int n = paramString.length();
    if (i == n) {
      return i;
    }
    return -1;
  }
  
  private static int nextSpace(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int k = 32;
      if (j == k)
      {
        for (;;)
        {
          j = i + 1;
          int m = paramString.length();
          if (j >= m) {
            break;
          }
          m = paramString.charAt(j);
          if (m != k) {
            break;
          }
          i = j;
        }
        return i;
      }
      i += 1;
    }
    int n = paramString.length();
    if (i == n) {
      return i;
    }
    return -1;
  }
  
  private static Triplet nonDetDelimeter(String paramString, Parser paramParser)
  {
    Object localObject1 = nextDelimeterLtd(paramString);
    Object localObject2 = (Integer)((Pair)localObject1).getA();
    int i = ((Integer)localObject2).intValue();
    int j = nextDelimeterImmediateLtd(paramString);
    localObject1 = (Integer)((Pair)localObject1).getB();
    int k = ((Integer)localObject1).intValue();
    if (i == k)
    {
      paramString = new com/twelfthmile/malana/compiler/types/Triplet;
      paramParser = Integer.valueOf(i);
      localObject1 = Integer.valueOf(j);
      localObject2 = Boolean.FALSE;
      paramString.<init>(paramParser, localObject1, localObject2);
      return paramString;
    }
    k = 0;
    localObject1 = null;
    Object localObject3 = paramString.substring(0, i);
    int m;
    if (paramParser != null)
    {
      m = paramString.charAt(j);
      int i1 = 46;
      if ((m == i1) && (j <= i))
      {
        m = 1;
        paramString = paramParser.shouldEnd(paramString, j, m);
        if (paramString != null)
        {
          paramString = (Boolean)paramString.getA();
          boolean bool1 = paramString.booleanValue();
          if (bool1)
          {
            paramString = new com/twelfthmile/malana/compiler/types/Triplet;
            paramParser = Integer.valueOf(j);
            localObject1 = Integer.valueOf(j);
            localObject2 = Boolean.TRUE;
            paramString.<init>(paramParser, localObject1, localObject2);
            return paramString;
          }
        }
      }
    }
    paramString = ((String)localObject3).toLowerCase();
    for (;;)
    {
      int i2 = paramString.length();
      if (k >= i2) {
        break;
      }
      boolean bool2 = delimeterWithinNonDet(paramString, k);
      if (bool2)
      {
        int i3 = k + 3;
        int i4 = paramString.length();
        if (i3 < i4)
        {
          localObject3 = Controller.getInstance().getSeedData().getTokenTrie();
          try
          {
            localObject3 = next;
            m = k + 1;
            m = paramString.charAt(m);
            Character localCharacter = Character.valueOf(m);
            localObject3 = ((HashMap)localObject3).get(localCharacter);
            localObject3 = (TokenTrie)localObject3;
            localObject3 = next;
            int n = k + 2;
            n = paramString.charAt(n);
            localCharacter = Character.valueOf(n);
            localObject3 = ((HashMap)localObject3).get(localCharacter);
            localObject3 = (TokenTrie)localObject3;
            localObject3 = next;
            i3 = paramString.charAt(i3);
            paramParser = Character.valueOf(i3);
            boolean bool3 = ((HashMap)localObject3).containsKey(paramParser);
            if (bool3) {
              break label385;
            }
            k += 1;
          }
          catch (Exception localException) {}
        }
      }
    }
    k = i;
    label385:
    paramString = new com/twelfthmile/malana/compiler/types/Triplet;
    paramParser = Integer.valueOf(k);
    localObject1 = Integer.valueOf(j);
    localObject2 = Boolean.TRUE;
    paramString.<init>(paramParser, localObject1, localObject2);
    return paramString;
  }
  
  public static Response parse(Request paramRequest)
  {
    TokenizerType localTokenizerType = new com/twelfthmile/malana/compiler/types/TokenizerType;
    String str = null;
    Object localObject1 = null;
    Date localDate = null;
    int i = -1;
    Object localObject2 = localTokenizerType;
    localTokenizerType.<init>(0, 0, 0, null, i);
    localObject2 = initConfigMapForClassifier(paramRequest);
    parseInternal(localTokenizerType, paramRequest, (HashMap)localObject2);
    localObject2 = parser;
    Object localObject3 = new com/twelfthmile/malana/compiler/types/Req;
    ((Req)localObject3).<init>(paramRequest);
    localObject2 = ((Parser)localObject2).output((Req)localObject3);
    boolean bool = ((Response)localObject2).isValid();
    if (bool) {
      Regex.refill(paramRequest, (Response)localObject2);
    } else {
      Regex.fallback(paramRequest, (Response)localObject2);
    }
    bool = ((Response)localObject2).isValid();
    if (bool)
    {
      localObject3 = ((Response)localObject2).getValMap();
      str = "date";
      bool = ((com.twelfthmile.b.a.a)localObject3).containsKey(str);
      if (!bool)
      {
        localObject3 = ((Response)localObject2).getValMap();
        str = "date";
        localObject1 = com.twelfthmile.e.b.a.a();
        localDate = messageDate;
        localObject1 = ((SimpleDateFormat)localObject1).format(localDate);
        ((com.twelfthmile.b.a.a)localObject3).put(str, (String)localObject1);
      }
      localObject3 = "GRM_BANK";
      str = ((Response)localObject2).getCategory();
      bool = ((String)localObject3).equals(str);
      if (bool)
      {
        localObject3 = address;
        classifyBank((Response)localObject2, (String)localObject3);
      }
      localObject3 = "GRM_OFFERS";
      str = ((Response)localObject2).getCategory();
      bool = ((String)localObject3).equals(str);
      if (bool)
      {
        paramRequest = address;
        classifyOffer((Response)localObject2, paramRequest);
      }
    }
    else
    {
      localObject2 = null;
    }
    return (Response)localObject2;
  }
  
  private static void parseInternal(TokenizerType paramTokenizerType, Request paramRequest, HashMap paramHashMap)
  {
    TokenizerType localTokenizerType = paramTokenizerType;
    Request localRequest = paramRequest;
    HashMap localHashMap = paramHashMap;
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = message;
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(" ");
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject2 = parser;
    Object localObject3 = Controller.getInstance().getSeedData().getTokenTrie();
    Object localObject4 = Controller.getInstance().getSeedData().getCustomRoot();
    Object localObject5 = ((String)localObject1).toLowerCase();
    int i = index;
    i = getTokenEndDelimeterIndex((String)localObject1, i);
    Object localObject6 = new com/twelfthmile/malana/compiler/lex/Custom;
    ((Custom)localObject6).<init>((TokenTrie)localObject4);
    int j = 0;
    localObject4 = null;
    index = 0;
    Object localObject7 = localObject3;
    for (;;)
    {
      int m = index;
      int i2 = ((String)localObject1).length();
      if (m >= i2) {
        break;
      }
      m = index;
      m = ((String)localObject5).charAt(m);
      int i3 = index;
      if (i3 > i)
      {
        i = index;
        i = getTokenEndDelimeterIndex((String)localObject1, i);
      }
      boolean bool1 = leaf;
      int i14 = 46;
      int i16 = 1;
      Object localObject8;
      Object localObject9;
      int i19;
      int i15;
      int n;
      label591:
      int i22;
      Object localObject14;
      boolean bool10;
      if (!bool1)
      {
        int i4 = index;
        boolean bool2 = skipCharacter((String)localObject1, i4, m);
        if (bool2)
        {
          int i5 = start;
          int i17 = start;
          i17 = ((String)localObject1).charAt(i17);
          boolean bool3 = skipCharacter((String)localObject1, i5, i17);
          if (bool3)
          {
            int i6 = 45;
            if (m != i6)
            {
              int i7 = 43;
              if (m != i7) {}
            }
            else
            {
              int i8 = index + i16;
              int i18 = ((String)localObject1).length();
              if (i8 < i18)
              {
                i8 = index + i16;
                boolean bool4 = isNumber(((String)localObject1).charAt(i8));
                if (bool4) {
                  break label619;
                }
              }
            }
            Object localObject10;
            if (m == i14)
            {
              i9 = index;
              localObject8 = ((Parser)localObject2).shouldEnd((String)localObject1, i9, 0);
              if (localObject8 != null)
              {
                localObject9 = (Boolean)((Pair)localObject8).getA();
                boolean bool6 = ((Boolean)localObject9).booleanValue();
                if (bool6)
                {
                  i19 = index;
                  ((Parser)localObject2).end(i19);
                }
              }
              localObject10 = localObject8;
            }
            else
            {
              localObject10 = null;
            }
            int i9 = 40;
            Object localObject11;
            if (m != i9)
            {
              int i10 = 41;
              if (m != i10) {}
            }
            else
            {
              localObject8 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
              localObject9 = "GDO_NONDET";
              localObject11 = String.valueOf(m);
              i15 = start;
              localObject12 = new String[0];
              ((GrammarDataObject)localObject8).<init>((String)localObject9, (String)localObject11, i15, (String[])localObject12);
              ((Parser)localObject2).add((GrammarDataObject)localObject8);
            }
            if (localObject10 != null)
            {
              localObject11 = (Integer)((Pair)localObject10).getB();
              n = ((Integer)localObject11).intValue();
              if (n >= 0)
              {
                localObject11 = (Integer)((Pair)localObject10).getB();
                n = ((Integer)localObject11).intValue();
                nextIndex = n;
                n = nextIndex;
                index = n;
                n = index;
                int i11 = 1;
                n += i11;
                start = n;
                break label591;
              }
            }
            i12 = 1;
            n = start + i12;
            start = n;
            localObject13 = localObject3;
            i21 = i;
            localObject4 = localObject6;
            i22 = 1;
            localObject14 = localObject5;
            bool10 = false;
            localObject5 = null;
            break label3737;
          }
        }
      }
      label619:
      int i12 = index;
      i12 = ((String)localObject1).charAt(i12);
      ((Custom)localObject6).hasNext(i12);
      boolean bool5 = child;
      int i13;
      Object localObject15;
      Object localObject17;
      Object localObject19;
      Object localObject20;
      String str;
      if (bool5)
      {
        localObject8 = next;
        localObject12 = Character.valueOf(n);
        bool5 = ((HashMap)localObject8).containsKey(localObject12);
        if (bool5)
        {
          localObject8 = next;
          localObject12 = Character.valueOf(n);
          localObject8 = (TokenTrie)((HashMap)localObject8).get(localObject12);
          bool5 = leaf;
          if (bool5)
          {
            i13 = index;
            localObject8 = ((String)localObject1).substring(i13);
            localObject8 = ((Custom)localObject6).isToken((String)localObject8);
            localObject12 = next;
            localObject15 = Character.valueOf(n);
            localObject15 = gettoken_name;
            localObject12 = next;
            localObject4 = Character.valueOf(n);
            localObject4 = gettoken_name_aux;
            int i27;
            if (localObject8 != null)
            {
              localObject4 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
              localObject15 = ((CustomTokenResponse)localObject8).getToken();
              i16 = start;
              i19 = index;
              i27 = ((CustomTokenResponse)localObject8).getIndex();
              i19 += i27;
              int i28 = 1;
              i19 += 1;
              localObject9 = ((String)localObject1).substring(i16, i19);
              i16 = start;
              localObject13 = localObject3;
              localObject16 = localObject6;
              localObject6 = new String[2];
              localObject3 = ((CustomTokenResponse)localObject8).getTokenTypeName();
              i27 = 0;
              localObject17 = null;
              localObject6[0] = localObject3;
              localObject3 = ((CustomTokenResponse)localObject8).getTokenTypeValue();
              localObject6[i28] = localObject3;
              ((GrammarDataObject)localObject4).<init>((String)localObject15, (String)localObject9, i16, (String[])localObject6);
              ((Parser)localObject2).add((GrammarDataObject)localObject4);
              i29 = index;
              j = ((CustomTokenResponse)localObject8).getIndex();
              i29 = i29 + j + 1;
              nextIndex = i29;
              localObject18 = localObject5;
              i21 = i;
              i29 = 1;
              j = 1;
              break label2533;
            }
            localObject13 = localObject3;
            localObject16 = localObject6;
            int i29 = index;
            boolean bool12 = checkAheadForToken((TokenTrie)localObject7, (String)localObject1, i29, n);
            int i30;
            if (bool12)
            {
              localObject3 = next;
              localObject4 = Character.valueOf(n);
              localObject3 = (TokenTrie)((HashMap)localObject3).get(localObject4);
              possibleToken = ((TokenTrie)localObject3);
              i30 = index;
              possibleTokenIndex = i30;
              localObject18 = localObject5;
              i21 = i;
            }
            else
            {
              i30 = index;
              i22 = start;
              i30 -= i22;
              i22 = 2;
              if (i30 >= i22)
              {
                i30 = index;
                i30 = i - i30;
                if (i30 > 0)
                {
                  i30 = index;
                  i30 = i - i30;
                  i22 = 4;
                  if (i30 <= i22)
                  {
                    i30 = index;
                    i22 = 1;
                    i30 += i22;
                    localObject3 = ((String)localObject5).substring(i30, i);
                    if (localObject3 != null)
                    {
                      boolean bool8 = isSuffix((String)localObject3);
                      if (bool8)
                      {
                        localObject6 = next;
                        localObject8 = Character.valueOf(n);
                        localObject6 = (TokenTrie)((HashMap)localObject6).get(localObject8);
                        localObject3 = tokenExistsWithSuffix((TokenTrie)localObject6, (String)localObject3);
                        if (localObject3 != null)
                        {
                          localObject15 = token_name;
                          localObject4 = token_name_aux;
                          localObject19 = localObject4;
                          localObject20 = localObject15;
                        }
                        else
                        {
                          localObject19 = localObject4;
                          localObject20 = localObject15;
                        }
                        localObject3 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
                        j = start;
                        str = ((String)localObject1).substring(j, i);
                        j = start;
                        i23 = 2;
                        localObject8 = new String[i23];
                        localObject6 = next;
                        localObject9 = Character.valueOf(n);
                        localObject6 = gettoken_type_name;
                        localObject8[0] = localObject6;
                        localObject6 = next;
                        localObject9 = Character.valueOf(n);
                        localObject6 = gettoken_type_val;
                        i19 = 1;
                        localObject8[i19] = localObject6;
                        localObject17 = localObject3;
                        ((GrammarDataObject)localObject3).<init>((String)localObject20, (String)localObject19, str, j, (String[])localObject8);
                        ((Parser)localObject2).add((GrammarDataObject)localObject3);
                        nextIndex = i;
                        localObject18 = localObject5;
                        i21 = i;
                        i30 = 1;
                        j = 1;
                        break label2533;
                      }
                    }
                  }
                }
              }
              i30 = start;
              localObject3 = ((String)localObject1).substring(i30);
              localObject6 = next;
              localObject8 = Character.valueOf(n);
              localObject6 = gettoken_type_val;
              i13 = index;
              i19 = start;
              i13 -= i19;
              i19 = 1;
              i13 += i19;
              localObject3 = Classifier.classifyUsingPrefix((String)localObject3, (String)localObject15, (String)localObject6, i13, localRequest);
              if (localObject3 != null)
              {
                localObject4 = (GrammarDataObject)((Pair)localObject3).getB();
                i23 = start;
                index = i23;
                getBclassified = i19;
                localObject4 = (GrammarDataObject)((Pair)localObject3).getB();
                ((Parser)localObject2).add((GrammarDataObject)localObject4);
                j = start;
                localObject3 = (Integer)((Pair)localObject3).getA();
                i30 = ((Integer)localObject3).intValue();
                j += i30;
                nextIndex = j;
                localObject18 = localObject5;
                i21 = i;
                i30 = 1;
                j = 1;
                break label2533;
              }
              i30 = index;
              int i23 = 1;
              i30 += i23;
              boolean bool13 = climax((String)localObject1, i30);
              if (bool13)
              {
                localObject3 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
                i13 = start;
                i19 = index + i23;
                str = ((String)localObject1).substring(i13, i19);
                i23 = start;
                i13 = 2;
                localObject9 = new String[i13];
                localObject8 = next;
                localObject12 = Character.valueOf(n);
                localObject8 = gettoken_type_name;
                localObject9[0] = localObject8;
                localObject8 = next;
                localObject12 = Character.valueOf(n);
                localObject8 = gettoken_type_val;
                i16 = 1;
                localObject9[i16] = localObject8;
                localObject17 = localObject3;
                localObject20 = localObject15;
                localObject19 = localObject4;
                ((GrammarDataObject)localObject3).<init>((String)localObject15, (String)localObject4, str, i23, (String[])localObject9);
                ((Parser)localObject2).add((GrammarDataObject)localObject3);
                i31 = index + i16;
                nextIndex = i31;
                localObject18 = localObject5;
                i21 = i;
                i31 = 1;
                j = 1;
                break label2533;
              }
              localObject3 = paramTokenizerType.getClone();
              localObject3 = checkIfTokenizableWithoutSpace(localRequest, (TokenizerType)localObject3, localHashMap);
              if (localObject3 != null)
              {
                localObject6 = (ArrayList)((Pair)localObject3).getB();
                i23 = ((ArrayList)localObject6).size();
                if (i23 > 0)
                {
                  localObject6 = (ArrayList)((Pair)localObject3).getB();
                  i23 = ((ArrayList)localObject6).size();
                  i13 = 1;
                  if (i23 == i13)
                  {
                    localObject6 = (ArrayList)((Pair)localObject3).getB();
                    i13 = 0;
                    localObject6 = get0label;
                    localObject8 = "IDVAL";
                    boolean bool9 = ((String)localObject6).equals(localObject8);
                    if (bool9)
                    {
                      localObject4 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
                      localObject6 = "IDVAL";
                      i13 = start;
                      localObject9 = (Integer)((Pair)localObject3).getA();
                      i19 = ((Integer)localObject9).intValue();
                      localObject8 = ((String)localObject1).substring(i13, i19);
                      i19 = start;
                      localObject12 = new String[2];
                      localObject15 = "id";
                      i27 = 0;
                      localObject12[0] = localObject15;
                      i15 = start;
                      localObject17 = (Integer)((Pair)localObject3).getA();
                      i21 = i;
                      i = ((Integer)localObject17).intValue();
                      localObject14 = ((String)localObject1).substring(i15, i);
                      i15 = 1;
                      localObject12[i15] = localObject14;
                      ((GrammarDataObject)localObject4).<init>((String)localObject6, (String)localObject8, i19, (String[])localObject12);
                      ((Parser)localObject2).add((GrammarDataObject)localObject4);
                      localObject3 = (Integer)((Pair)localObject3).getA();
                      i31 = ((Integer)localObject3).intValue();
                      nextIndex = i31;
                      localObject18 = localObject5;
                      i31 = 1;
                      j = 1;
                      break label2533;
                    }
                  }
                  i21 = i;
                  localObject14 = (ArrayList)((Pair)localObject3).getB();
                  localObject6 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
                  i13 = start;
                  i19 = index + 1;
                  str = ((String)localObject1).substring(i13, i19);
                  i13 = start;
                  localObject12 = new String[2];
                  localObject9 = next;
                  localObject18 = localObject5;
                  localObject5 = Character.valueOf(n);
                  localObject5 = gettoken_type_name;
                  localObject12[0] = localObject5;
                  localObject5 = next;
                  localObject9 = Character.valueOf(n);
                  localObject5 = gettoken_type_val;
                  i19 = 1;
                  localObject12[i19] = localObject5;
                  localObject17 = localObject6;
                  localObject20 = localObject15;
                  localObject19 = localObject4;
                  ((GrammarDataObject)localObject6).<init>((String)localObject15, (String)localObject4, str, i13, (String[])localObject12);
                  k = mature;
                  localObject5 = new java/util/ArrayList;
                  ((ArrayList)localObject5).<init>();
                  ((ArrayList)localObject5).add(localObject6);
                  localObject8 = ((ArrayList)localObject14).iterator();
                  i16 = k;
                  for (;;)
                  {
                    k = ((Iterator)localObject8).hasNext();
                    if (k == 0) {
                      break;
                    }
                    localObject4 = (GrammarDataObject)((Iterator)localObject8).next();
                    boolean bool7 = mature;
                    if (bool7) {
                      i16 = 1;
                    }
                    ((ArrayList)localObject5).add(localObject4);
                  }
                  if (i16 == 0)
                  {
                    k = ((Parser)localObject2).nextTokens((ArrayList)localObject5);
                    if (k != 0) {
                      i16 = 1;
                    }
                  }
                  if (i16 != 0)
                  {
                    ((Parser)localObject2).add((GrammarDataObject)localObject6);
                    localObject4 = ((ArrayList)localObject14).iterator();
                    for (;;)
                    {
                      bool10 = ((Iterator)localObject4).hasNext();
                      if (!bool10) {
                        break;
                      }
                      localObject5 = (GrammarDataObject)((Iterator)localObject4).next();
                      ((Parser)localObject2).add((GrammarDataObject)localObject5);
                    }
                    localObject3 = (Integer)((Pair)localObject3).getA();
                    i31 = ((Integer)localObject3).intValue();
                    nextIndex = i31;
                    i31 = 1;
                    k = 1;
                    break label2533;
                  }
                  i31 = 0;
                  localObject3 = null;
                  k = 0;
                  localObject4 = null;
                  break label2533;
                }
              }
              localObject18 = localObject5;
              i21 = i;
            }
          }
          else
          {
            localObject13 = localObject3;
            localObject18 = localObject5;
            i21 = i;
            localObject16 = localObject6;
          }
          i31 = 1;
          k = 0;
          localObject4 = null;
          label2533:
          if (k == 0)
          {
            localObject5 = next;
            localObject14 = Character.valueOf(n);
            localObject5 = ((HashMap)localObject5).get(localObject14);
            localObject7 = localObject5;
            localObject7 = (TokenTrie)localObject5;
            i16 = k;
            break label2614;
          }
          i16 = k;
          break label2614;
        }
      }
      Object localObject13 = localObject3;
      Object localObject18 = localObject5;
      int i21 = i;
      Object localObject16 = localObject6;
      int i31 = 0;
      localObject3 = null;
      i16 = 0;
      Object localObject12 = null;
      label2614:
      int i24;
      int i32;
      if (i31 == 0)
      {
        i31 = index;
        localObject3 = ((String)localObject1).substring(i31);
        localObject4 = localObject16;
        localObject3 = ((Custom)localObject16).isToken((String)localObject3);
        int i1;
        int i25;
        if (localObject3 != null)
        {
          localObject5 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
          localObject14 = ((CustomTokenResponse)localObject3).getToken();
          i24 = start;
          i1 = index;
          i13 = ((CustomTokenResponse)localObject3).getIndex();
          i1 += i13;
          i13 = 1;
          i1 += i13;
          localObject6 = ((String)localObject1).substring(i24, i1);
          i1 = start;
          int i20 = 2;
          localObject9 = new String[i20];
          localObject15 = ((CustomTokenResponse)localObject3).getTokenTypeName();
          i16 = 0;
          localObject12 = null;
          localObject9[0] = localObject15;
          localObject15 = ((CustomTokenResponse)localObject3).getTokenTypeValue();
          localObject9[i13] = localObject15;
          ((GrammarDataObject)localObject5).<init>((String)localObject14, (String)localObject6, i1, (String[])localObject9);
          ((Parser)localObject2).add((GrammarDataObject)localObject5);
          i25 = index;
          i31 = ((CustomTokenResponse)localObject3).getIndex();
          i25 = i25 + i31 + i13;
          nextIndex = i25;
          localObject14 = localObject18;
        }
        else
        {
          i31 = start;
          localObject3 = ((String)localObject1).substring(i31);
          i25 = start;
          localObject3 = Classifier.classifyUsingGrammar((String)localObject3, i25, localRequest, localHashMap);
          if (localObject3 != null)
          {
            localObject5 = (GrammarDataObject)((Pair)localObject3).getB();
            i25 = index;
            if (i25 < 0)
            {
              localObject5 = (GrammarDataObject)((Pair)localObject3).getB();
              i = start;
              index = i;
            }
            localObject5 = (GrammarDataObject)((Pair)localObject3).getB();
            i = 1;
            classified = i;
            localObject5 = (GrammarDataObject)((Pair)localObject3).getB();
            ((Parser)localObject2).add((GrammarDataObject)localObject5);
            i25 = start;
            localObject3 = (Integer)((Pair)localObject3).getA();
            i31 = ((Integer)localObject3).intValue();
            i25 += i31;
            nextIndex = i25;
            localObject14 = localObject18;
          }
          else
          {
            localObject3 = possibleToken;
            if (localObject3 != null)
            {
              i31 = possibleTokenIndex;
              i25 = 1;
              i31 += i25;
              i = ((String)localObject18).length();
              if (i31 < i)
              {
                i31 = possibleTokenIndex + i25;
                localObject14 = localObject18;
                boolean bool14 = climax((String)localObject18, i31);
                if (!bool14) {
                  break label3203;
                }
                i32 = possibleTokenIndex + i25;
                nextIndex = i32;
                localObject3 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
                localObject5 = possibleToken.token_name;
                localObject6 = possibleToken.token_name_aux;
                i1 = start;
                i13 = nextIndex;
                str = ((String)localObject1).substring(i1, i13);
                i1 = start;
                i13 = 2;
                localObject8 = new String[i13];
                localObject9 = possibleToken.token_type_name;
                localObject15 = null;
                localObject8[0] = localObject9;
                localObject9 = possibleToken.token_type_val;
                i15 = 1;
                localObject8[i15] = localObject9;
                localObject17 = localObject3;
                localObject20 = localObject5;
                localObject19 = localObject6;
                ((GrammarDataObject)localObject3).<init>((String)localObject5, (String)localObject6, str, i1, (String[])localObject8);
                ((Parser)localObject2).add((GrammarDataObject)localObject3);
                break label3377;
              }
            }
            localObject14 = localObject18;
            label3203:
            i32 = start;
            localObject3 = nonDetDelimeter(((String)localObject1).substring(i32), (Parser)localObject2);
            i25 = start;
            localObject6 = (Integer)((Triplet)localObject3).getA();
            i24 = ((Integer)localObject6).intValue();
            i25 += i24;
            nextIndex = i25;
            localObject5 = (Boolean)((Triplet)localObject3).getC();
            boolean bool11 = ((Boolean)localObject5).booleanValue();
            if (bool11)
            {
              localObject5 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
              localObject6 = "GDO_NONDET";
              i1 = start;
              i13 = start;
              i32 = ((Integer)((Triplet)localObject3).getB()).intValue();
              i13 += i32;
              localObject3 = ((String)localObject1).substring(i1, i13);
              i1 = start;
              i13 = 0;
              localObject8 = null;
              localObject9 = new String[0];
              ((GrammarDataObject)localObject5).<init>((String)localObject6, (String)localObject3, i1, (String[])localObject9);
              Classifier.classifyNonDet((GrammarDataObject)localObject5);
              ((Parser)localObject2).add((GrammarDataObject)localObject5);
            }
          }
        }
        label3377:
        i16 = 1;
      }
      else
      {
        localObject4 = localObject16;
        localObject14 = localObject18;
      }
      int i26;
      if (i16 != 0)
      {
        i32 = nextIndex;
        i26 = ((String)localObject1).length();
        if (i32 < i26)
        {
          i32 = nextIndex;
          boolean bool15 = Character.isLetterOrDigit(((String)localObject1).charAt(i32));
          if (!bool15)
          {
            i33 = nextIndex;
            i33 = ((String)localObject1).charAt(i33);
            i26 = 41;
            if (i33 != i26) {}
          }
          else
          {
            i33 = nextIndex;
            i26 = 1;
            i33 -= i26;
            nextIndex = i33;
          }
        }
        int i33 = nextIndex;
        i26 = ((String)localObject1).length();
        if (i33 < i26)
        {
          i33 = nextIndex;
          i33 = ((String)localObject1).charAt(i33);
          i26 = 46;
          if (i33 != i26)
          {
            i26 = 0;
            localObject5 = null;
            i16 = 0;
            localObject12 = null;
            break label3615;
          }
        }
        i33 = nextIndex;
        i26 = 0;
        localObject5 = null;
        localObject12 = ((Parser)localObject2).shouldEnd((String)localObject1, i33, 0);
        if (localObject12 != null)
        {
          localObject3 = (Boolean)((Pair)localObject12).getA();
          boolean bool16 = ((Boolean)localObject3).booleanValue();
          if (bool16)
          {
            i34 = nextIndex;
            ((Parser)localObject2).end(i34);
          }
        }
        label3615:
        if (localObject12 != null)
        {
          localObject3 = (Integer)((Pair)localObject12).getB();
          i34 = ((Integer)localObject3).intValue();
          if (i34 >= 0)
          {
            localObject3 = (Integer)((Pair)localObject12).getB();
            i34 = ((Integer)localObject3).intValue();
            nextIndex = i34;
          }
        }
        i34 = nextIndex;
        index = i34;
        i34 = index;
        i24 = 1;
        i34 += i24;
        start = i34;
        localObject3 = null;
        possibleToken = null;
        i34 = -1;
        possibleTokenIndex = i34;
        ((Custom)localObject4).reset();
        localObject7 = localObject13;
      }
      else
      {
        i26 = 0;
        localObject5 = null;
        i24 = 1;
      }
      label3737:
      int i34 = index + i24;
      index = i34;
      localObject6 = localObject4;
      localObject5 = localObject14;
      localObject3 = localObject13;
      i = i21;
      int k = 0;
      localObject4 = null;
    }
  }
  
  public static boolean skipCharacter(String paramString, int paramInt, char paramChar)
  {
    boolean bool = goodEndings(paramString.charAt(paramInt));
    if (!bool)
    {
      char c = '+';
      if (paramChar != c)
      {
        c = '\\';
        if (paramChar != c) {
          return false;
        }
      }
    }
    return true;
  }
  
  private static String smartlcs(String paramString, int paramInt, TokenTrie paramTokenTrie)
  {
    Iterator localIterator = next.keySet().iterator();
    Object localObject2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      int j = 0;
      Object localObject1 = null;
      if (!bool1) {
        break;
      }
      int i = paramString.length();
      if (paramInt >= i) {
        break;
      }
      localObject2 = (Character)localIterator.next();
      int k = ((Character)localObject2).charValue();
      int n = 32;
      if (k == n)
      {
        String str = " ";
        boolean bool2 = paramString.contains(str);
        if (!bool2) {
          return null;
        }
      }
      localObject1 = paramString.toLowerCase();
      j = ((String)localObject1).charAt(paramInt);
      int m = ((Character)localObject2).charValue();
      if (j == m) {
        paramInt += 1;
      }
      localObject1 = next;
      localObject2 = (TokenTrie)((HashMap)localObject1).get(localObject2);
      localObject2 = smartlcs(paramString, paramInt, (TokenTrie)localObject2);
    } while (localObject2 == null);
    return (String)localObject2;
    int i1 = paramString.length();
    if (paramInt == i1)
    {
      boolean bool3 = leaf;
      if (bool3) {
        return token_name;
      }
      paramString = fastForwardtoken_name;
      if (paramString != null) {
        return paramString;
      }
    }
    return null;
  }
  
  private static TokenTrie tokenExistsWithSuffix(TokenTrie paramTokenTrie, String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      Object localObject = next;
      char c2 = paramString.charAt(i);
      Character localCharacter = Character.valueOf(c2);
      boolean bool1 = ((HashMap)localObject).containsKey(localCharacter);
      if (!bool1) {
        return null;
      }
      paramTokenTrie = next;
      char c1 = paramString.charAt(i);
      localObject = Character.valueOf(c1);
      paramTokenTrie = (TokenTrie)paramTokenTrie.get(localObject);
      i += 1;
    }
    boolean bool2 = leaf;
    if (bool2) {
      return paramTokenTrie;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.lex.Tokenizer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */