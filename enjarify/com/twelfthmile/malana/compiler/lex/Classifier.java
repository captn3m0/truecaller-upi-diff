package com.twelfthmile.malana.compiler.lex;

import com.twelfthmile.e.a.b;
import com.twelfthmile.malana.compiler.Compiler;
import com.twelfthmile.malana.compiler.Seeder;
import com.twelfthmile.malana.compiler.datastructure.TokenTrie;
import com.twelfthmile.malana.compiler.parser.Parser;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.types.Pair;
import com.twelfthmile.malana.compiler.types.Quad;
import com.twelfthmile.malana.compiler.types.Request;
import com.twelfthmile.malana.compiler.types.Triplet;
import com.twelfthmile.malana.controller.Controller;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Classifier
{
  private static int cdrParse(String paramString)
  {
    int i = -1;
    int j = 1;
    int k = 0;
    int m = -1;
    while (j > 0)
    {
      int n = paramString.length();
      if (k >= n) {
        break;
      }
      n = paramString.charAt(k);
      switch (j)
      {
      default: 
        break;
      case 3: 
        int i1 = 32;
        if (n != i1)
        {
          i1 = 46;
          if (n != i1)
          {
            i1 = 44;
            if (n != i1)
            {
              i1 = 59;
              if (n != i1) {
                return i;
              }
            }
          }
        }
        return m;
      case 2: 
        j = 82;
        if (n != j)
        {
          j = 114;
          if (n != j)
          {
            j = -1;
            break;
          }
        }
        j = 3;
        break;
      case 1: 
        j = 67;
        if (n == j)
        {
          j = 2;
          m = 1;
        }
        else
        {
          j = 68;
          if (n == j)
          {
            j = 2;
            m = 0;
          }
          else
          {
            j = -1;
          }
        }
        break;
      }
      k += 1;
    }
    return i;
  }
  
  private static Pair checkForId(String paramString, int paramInt)
  {
    Object localObject1 = new java/lang/StringBuilder;
    Object localObject2 = "";
    ((StringBuilder)localObject1).<init>((String)localObject2);
    int i = paramString.length();
    int i1 = 39;
    int i2 = 1;
    if (i > 0)
    {
      i = paramString.charAt(0);
      if (i == i1)
      {
        i = 1;
        i3 = 1;
        i4 = 0;
        localGrammarDataObject = null;
        i5 = 0;
        i6 = 0;
        break label86;
      }
    }
    i = 1;
    int i3 = 0;
    String[] arrayOfString = null;
    int i4 = 0;
    GrammarDataObject localGrammarDataObject = null;
    int i5 = 0;
    int i6 = 0;
    label86:
    int i7;
    int n;
    for (;;)
    {
      i7 = 2;
      if (i <= 0) {
        break;
      }
      int i8 = paramString.length();
      if (i3 >= i8) {
        break;
      }
      i8 = paramString.charAt(i3);
      switch (i)
      {
      default: 
        break;
      case 2: 
        boolean bool1 = com.twelfthmile.e.b.c.a(i8);
        if (!bool1)
        {
          bool1 = com.twelfthmile.e.b.c.e(i8);
          if (!bool1)
          {
            int j = 95;
            if (i8 != j)
            {
              if (i8 == i1)
              {
                j = 2;
                break;
              }
              j = -1;
              break;
            }
          }
        }
        boolean bool2 = com.twelfthmile.e.b.c.f(i8);
        if (bool2)
        {
          i4 = 1;
        }
        else
        {
          bool2 = com.twelfthmile.e.b.c.g(i8);
          if (bool2)
          {
            i6 = 1;
          }
          else
          {
            bool2 = com.twelfthmile.e.b.c.a(i8);
            if (bool2) {
              i5 = 1;
            }
          }
        }
        ((StringBuilder)localObject1).append(i8);
        int k = 2;
        break;
      case 1: 
        boolean bool3 = com.twelfthmile.e.b.c.a(i8);
        if (!bool3)
        {
          bool3 = com.twelfthmile.e.b.c.e(i8);
          if (!bool3)
          {
            int m = -1;
            break;
          }
        }
        boolean bool4 = com.twelfthmile.e.b.c.f(i8);
        if (bool4)
        {
          i4 = 1;
        }
        else
        {
          bool4 = com.twelfthmile.e.b.c.g(i8);
          if (bool4) {
            i6 = 1;
          } else {
            i5 = 1;
          }
        }
        ((StringBuilder)localObject1).append(i8);
        n = 2;
      }
      i3 += 1;
    }
    if (i4 != 0)
    {
      if (i5 == 0) {
        n = i6 ^ 0x1;
      } else {
        n = 1;
      }
    }
    else if (i5 == 0)
    {
      n = 0;
      localObject2 = null;
    }
    else
    {
      n = i6;
    }
    i1 = ((StringBuilder)localObject1).length();
    if ((i1 > 0) && (n != 0))
    {
      localObject2 = new com/twelfthmile/malana/compiler/types/Pair;
      i3 -= i2;
      Integer localInteger = Integer.valueOf(i3);
      localGrammarDataObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
      paramString = paramString.substring(0, i3);
      arrayOfString = new String[i7];
      arrayOfString[0] = "id";
      localObject1 = ((StringBuilder)localObject1).toString();
      arrayOfString[i2] = localObject1;
      localGrammarDataObject.<init>("IDVAL", paramString, paramInt, arrayOfString);
      ((Pair)localObject2).<init>(localInteger, localGrammarDataObject);
      return (Pair)localObject2;
    }
    return null;
  }
  
  private static Pair checkForKeyVal(String paramString1, String paramString2, int paramInt)
  {
    paramString1 = paramString1.substring(paramInt);
    int i = nonDetDelimeterTillToken(paramString1);
    int j = -1;
    if (i != j)
    {
      paramString1 = paramString1.substring(0, i);
      Pair localPair = new com/twelfthmile/malana/compiler/types/Pair;
      Integer localInteger = Integer.valueOf(paramInt + i);
      GrammarDataObject localGrammarDataObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(paramString2);
      ((StringBuilder)localObject).append("VAL");
      localObject = ((StringBuilder)localObject).toString();
      String[] arrayOfString = new String[2];
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      paramString2 = paramString2.toLowerCase();
      localStringBuilder.append(paramString2);
      localStringBuilder.append("_val");
      paramString2 = localStringBuilder.toString();
      arrayOfString[0] = paramString2;
      arrayOfString[1] = paramString1;
      localGrammarDataObject.<init>((String)localObject, paramString1, 0, arrayOfString);
      localPair.<init>(localInteger, localGrammarDataObject);
      return localPair;
    }
    return null;
  }
  
  private static Quad checkForSeatNo(String paramString)
  {
    String str1 = paramString;
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("");
    Object localObject2 = new java/lang/StringBuilder;
    Object localObject3 = "";
    ((StringBuilder)localObject2).<init>((String)localObject3);
    int i = paramString.length();
    int k = 2;
    int m = 3;
    if (i > m)
    {
      localObject3 = paramString.toLowerCase();
      String str2 = "row";
      boolean bool1 = ((String)localObject3).startsWith(str2);
      if (bool1)
      {
        j = 4;
        break label78;
      }
    }
    int j = 2;
    label78:
    int n = 1;
    int i1 = 1;
    int i4 = 0;
    int i5 = 0;
    int i6 = 0;
    int i7 = 0;
    while (i1 > 0)
    {
      int i8 = paramString.length();
      if (i4 >= i8) {
        break;
      }
      i8 = str1.charAt(i4);
      int i10 = 58;
      int i19 = 32;
      int i20 = -1;
      int i11;
      boolean bool5;
      int i14;
      int i9;
      switch (i1)
      {
      case 6: 
      default: 
        break;
      case 10: 
        i11 = com.twelfthmile.e.b.c.a(i8);
        if (i11 != 0)
        {
          ((StringBuilder)localObject2).append(i8);
          i1 = 5;
        }
        else
        {
          i4 += -3;
          i1 = -1;
        }
        break;
      case 9: 
        if (i8 == i11) {
          i1 = 5;
        } else if (i8 == i19) {
          i1 = 9;
        } else {
          i1 = -1;
        }
        break;
      case 8: 
        boolean bool4 = com.twelfthmile.e.b.c.a(i8);
        if (!bool4)
        {
          int i12 = 46;
          if (i8 != i12)
          {
            if (i8 == i19)
            {
              i1 = 8;
              break label901;
            }
            int i13 = 41;
            if (i8 == i13)
            {
              i1 = 9;
              break label901;
            }
            bool5 = com.twelfthmile.e.b.c.e(i8);
            if ((bool5) && (i4 <= k))
            {
              i1 = 1;
              break label901;
            }
            i1 = -1;
            break label901;
          }
        }
        ((StringBuilder)localObject1).append(i8);
        i1 = 8;
        break;
      case 7: 
        bool5 = com.twelfthmile.e.b.c.e(i8);
        if ((!bool5) && (i8 != i19))
        {
          bool5 = com.twelfthmile.e.b.c.a(i8);
          if (bool5)
          {
            i6 += 1;
            i1 = 3;
          }
          else
          {
            i1 = -1;
          }
        }
        else
        {
          i1 = 2;
        }
        break;
      case 5: 
        bool5 = com.twelfthmile.e.b.c.a(i8);
        if (bool5)
        {
          ((StringBuilder)localObject2).append(i8);
          i1 = 5;
        }
        else if (i8 == i19)
        {
          i1 = 5;
        }
        else
        {
          i14 = lookAheadSpaceForMovie(str1, i4);
          str3 = str1.substring(i4, i14);
          String str4 = "ticket(s)";
          i9 = str3.startsWith(str4);
          if (i9 == 0)
          {
            str4 = "seat(s)";
            boolean bool2 = str3.startsWith(str4);
            if (!bool2)
            {
              i4 += -3;
              i2 = -1;
              break label901;
            }
          }
          if (i5 != 0) {
            break;
          }
        }
        break;
      }
      try
      {
        str3 = ((StringBuilder)localObject2).toString();
        i5 = Integer.parseInt(str3);
      }
      catch (Exception localException)
      {
        boolean bool6;
        int i15;
        int i16;
        boolean bool3;
        int i3;
        int i17;
        boolean bool7;
        int i18;
        label901:
        String str5;
        for (;;) {}
      }
      String str3 = str1.substring(i14);
      int i2 = lookAheadInteger(str3);
      if ((i2 != i20) && (i2 < m))
      {
        i4 = i14 + i2 + -1;
        i14 = 10;
        i2 = 10;
      }
      else
      {
        i4 = i14 + -1;
        i2 = -1;
        break label901;
        bool6 = com.twelfthmile.e.b.c.a(i9);
        if (bool6)
        {
          ((StringBuilder)localObject2).append(i9);
          i2 = 5;
        }
        else
        {
          i4 += -2;
          i2 = -1;
          break label901;
          bool6 = com.twelfthmile.e.b.c.a(i9);
          if (!bool6)
          {
            bool6 = com.twelfthmile.e.b.c.e(i9);
            if (!bool6)
            {
              i15 = 44;
              if (i9 == i15)
              {
                i2 = 2;
                break label901;
              }
              if (i6 > 0)
              {
                i2 = 4;
                break label901;
              }
              i2 = -1;
              break label901;
            }
          }
          i2 = 3;
          break label901;
          i16 = com.twelfthmile.e.b.c.e(i9);
          if (i16 != 0)
          {
            i2 = 2;
          }
          else
          {
            i16 = com.twelfthmile.e.b.c.a(i9);
            if (i16 != 0)
            {
              i6 += 1;
              i2 = 3;
            }
            else if (i6 > 0)
            {
              i2 = 4;
            }
            else
            {
              i2 = -1;
              break label901;
              bool3 = com.twelfthmile.e.b.c.e(i9);
              if (bool3)
              {
                bool3 = true;
              }
              else if (i9 == i19)
              {
                i7 += 1;
                if (i7 >= j) {
                  i3 = -1;
                } else {
                  i3 = 1;
                }
              }
              else
              {
                i3 = 45;
                if ((i9 != i3) && (i9 != i16))
                {
                  i17 = 40;
                  if (i9 == i17)
                  {
                    i3 = 8;
                  }
                  else
                  {
                    bool7 = com.twelfthmile.e.b.c.a(i9);
                    if (bool7)
                    {
                      i6 += 1;
                      i3 = 3;
                    }
                    else
                    {
                      i3 = -1;
                    }
                  }
                }
                else
                {
                  i18 = 7;
                  i3 = 7;
                }
              }
            }
          }
        }
      }
      i4 += n;
    }
    if ((i6 != 0) || (i5 <= 0)) {
      i5 = i6;
    }
    if (i5 > 0)
    {
      localObject2 = new com/twelfthmile/malana/compiler/types/Quad;
      localObject3 = Integer.valueOf(i4);
      str1 = str1.substring(0, i4);
      str5 = String.valueOf(i5);
      localObject1 = ((StringBuilder)localObject1).toString();
      ((Quad)localObject2).<init>(localObject3, str1, str5, localObject1);
      return (Quad)localObject2;
    }
    return null;
  }
  
  private static Pair checkForUPI(String paramString1, String paramString2)
  {
    String str1 = paramString1;
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("");
    com.twelfthmile.e.a.a();
    Object localObject2 = new java/util/HashMap;
    ((HashMap)localObject2).<init>();
    String str2 = null;
    int i = -1;
    int j = 1;
    Object localObject3 = localObject1;
    Object localObject4 = "";
    boolean bool1 = true;
    int i9 = 0;
    String[] arrayOfString = null;
    Object localObject5 = null;
    int i11 = 0;
    Object localObject6 = null;
    int i12 = -1;
    int i13;
    int i16;
    Object localObject7;
    int i15;
    int i10;
    for (;;)
    {
      i13 = 6;
      int i14 = 32;
      i16 = 2;
      if (!bool1) {
        break;
      }
      int i17 = paramString1.length();
      if (i9 >= i17) {
        break;
      }
      int i18 = str1.charAt(i9);
      int i5;
      int i6;
      switch (bool1)
      {
      default: 
        break;
      case 6: 
        bool1 = com.twelfthmile.e.b.c.a(i18);
        int k;
        if (bool1)
        {
          ((StringBuilder)localObject3).append(i18);
          k = 6;
        }
        else
        {
          localObject1 = ((StringBuilder)localObject3).toString();
          k = ((String)localObject1).length();
          if (k > i13) {
            localObject5 = ((StringBuilder)localObject3).toString();
          }
          localObject1 = new java/lang/StringBuilder;
          localObject7 = "";
          ((StringBuilder)localObject1).<init>((String)localObject7);
          i9 += -1;
          localObject3 = localObject1;
          k = -1;
        }
        break;
      case 5: 
        boolean bool2 = com.twelfthmile.e.b.c.f(i18);
        if (!bool2)
        {
          bool2 = com.twelfthmile.e.b.c.g(i18);
          if (!bool2)
          {
            char c = '.';
            if (i18 != c)
            {
              boolean bool3 = isUPIDelimeter(i18);
              if (!bool3)
              {
                bool3 = com.twelfthmile.e.b.c.a(i18);
                if (bool3)
                {
                  int m = i9 + -1;
                  boolean bool4 = isUPIDelimeter(str1.charAt(m));
                  if (bool4)
                  {
                    ((StringBuilder)localObject3).append(i18);
                    n = 6;
                    break;
                  }
                  int n = 5;
                  break;
                }
                if (i9 >= i16)
                {
                  localObject1 = "to";
                  i14 = i9 + -2;
                  localObject7 = str1.substring(i14, i9);
                  boolean bool5 = ((String)localObject1).equalsIgnoreCase((String)localObject7);
                  if (bool5)
                  {
                    localObject1 = "to";
                    localObject7 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject7).<init>("");
                    localObject4 = localObject1;
                    localObject3 = localObject7;
                    i1 = 4;
                    break;
                  }
                }
                i9 += -1;
                i1 = -1;
                break;
              }
            }
          }
        }
        int i1 = 5;
        break;
      case 4: 
        boolean bool6 = com.twelfthmile.e.b.c.a(i18);
        if (!bool6)
        {
          bool6 = com.twelfthmile.e.b.c.f(i18);
          if (!bool6)
          {
            bool6 = com.twelfthmile.e.b.c.g(i18);
            if (!bool6)
            {
              if ((i12 >= 0) && (i12 < i16) && (i18 == i14))
              {
                i12 += 1;
                ((StringBuilder)localObject3).append(i18);
                i2 = i9 + 1;
                i14 = paramString1.length();
                if (i2 == i14)
                {
                  localObject1 = ((StringBuilder)localObject3).toString();
                  localObject7 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject7).<init>("");
                  localObject6 = localObject1;
                  localObject3 = localObject7;
                  i2 = 4;
                  break;
                }
                i2 = 4;
                break;
              }
              int i2 = 64;
              if (i18 == i2)
              {
                localObject1 = "";
                boolean bool7 = ((String)localObject4).equals(localObject1);
                if (!bool7)
                {
                  localObject1 = ((StringBuilder)localObject3).toString();
                  ((HashMap)localObject2).put(localObject4, localObject1);
                }
                localObject1 = ((StringBuilder)localObject3).toString();
                localObject7 = new java/lang/StringBuilder;
                ((StringBuilder)localObject7).<init>("");
                localObject6 = localObject1;
                localObject3 = localObject7;
                i3 = 5;
                break;
              }
              localObject1 = ((StringBuilder)localObject3).toString();
              localObject7 = "from";
              boolean bool12 = ((String)localObject7).equalsIgnoreCase((String)localObject1);
              if (bool12)
              {
                localObject1 = "from";
                localObject7 = new java/lang/StringBuilder;
                ((StringBuilder)localObject7).<init>("");
                localObject4 = localObject1;
                localObject3 = localObject7;
                i3 = 4;
                break;
              }
              localObject7 = new java/lang/StringBuilder;
              ((StringBuilder)localObject7).<init>("");
              i9 += -1;
              localObject6 = localObject1;
              localObject3 = localObject7;
              i3 = -1;
              break;
            }
          }
        }
        if (i12 == i) {
          i12 = 0;
        }
        ((StringBuilder)localObject3).append(i18);
        int i3 = i9 + 1;
        i15 = paramString1.length();
        if (i3 == i15)
        {
          localObject1 = ((StringBuilder)localObject3).toString();
          localObject7 = new java/lang/StringBuilder;
          ((StringBuilder)localObject7).<init>("");
          localObject6 = localObject1;
          localObject3 = localObject7;
          i3 = 4;
        }
        else
        {
          i3 = 4;
        }
        break;
      case 3: 
        boolean bool8 = com.twelfthmile.e.b.c.a(i18);
        if (bool8)
        {
          ((StringBuilder)localObject3).append(i18);
          int i4 = 3;
        }
        else
        {
          boolean bool9 = isUPIDelimeter(i18);
          if (bool9)
          {
            localObject1 = ((StringBuilder)localObject3).toString();
            localObject7 = new java/lang/StringBuilder;
            ((StringBuilder)localObject7).<init>("");
            localObject5 = localObject1;
            localObject3 = localObject7;
            i5 = 4;
          }
          else
          {
            i5 = -1;
          }
        }
        break;
      case 2: 
        localObject1 = str1.substring(i9);
        i5 = lookAheadIntegerForUPI((String)localObject1);
        if ((i5 != i) && (i5 < i13))
        {
          i9 += i5;
          i5 = str1.charAt(i10);
          ((StringBuilder)localObject3).append(i5);
          i6 = 3;
        }
        else
        {
          i6 = -1;
        }
        break;
      case 1: 
        localObject7 = str1.substring(i10);
        localObject1 = com.twelfthmile.e.a.a("FSA_UPI", (String)localObject7);
        if (localObject1 != null)
        {
          localObject1 = (Integer)((b)localObject1).a();
          i6 = ((Integer)localObject1).intValue();
          i10 += i6;
          i6 = 2;
        }
        else
        {
          i6 = -1;
        }
        break;
      }
      i10 += j;
    }
    if (localObject5 != null)
    {
      localObject1 = paramString2.toLowerCase();
      int i19 = ((String)localObject1).hashCode();
      i12 = 108244;
      boolean bool11;
      if (i19 != i12)
      {
        i12 = 116014;
        if (i19 != i12)
        {
          i12 = 3377349;
          if (i19 == i12)
          {
            localObject3 = "neft";
            boolean bool10 = ((String)localObject1).equals(localObject3);
            if (bool10)
            {
              int i7 = 2;
              break label1096;
            }
          }
        }
        else
        {
          localObject3 = "upi";
          bool11 = ((String)localObject1).equals(localObject3);
          if (bool11)
          {
            bool11 = false;
            localObject1 = null;
            break label1096;
          }
        }
      }
      else
      {
        localObject3 = "mmt";
        bool11 = ((String)localObject1).equals(localObject3);
        if (bool11)
        {
          bool11 = true;
          break label1096;
        }
      }
      int i8 = -1;
      label1096:
      int i20;
      switch (i8)
      {
      default: 
        str1 = null;
        break;
      case 2: 
        if (localObject6 != null)
        {
          localObject1 = new com/twelfthmile/malana/compiler/types/Pair;
          i20 = paramString1.length();
          if (i10 < i20)
          {
            i20 = str1.charAt(i10);
            if (i20 != i15)
            {
              i20 = i10 + 1;
              break label1179;
            }
          }
          i20 = i10;
          localObject2 = Integer.valueOf(i20);
          localObject7 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
          str1 = str1.substring(0, i10);
          arrayOfString = new String[4];
          arrayOfString[0] = "neft_num";
          arrayOfString[j] = localObject5;
          arrayOfString[i16] = "vendor";
          arrayOfString[3] = localObject6;
          ((GrammarDataObject)localObject7).<init>("NEFT", str1, i, arrayOfString);
          ((Pair)localObject1).<init>(localObject2, localObject7);
          return (Pair)localObject1;
        }
        localObject1 = new com/twelfthmile/malana/compiler/types/Pair;
        i20 = paramString1.length();
        if (i10 < i20)
        {
          i20 = str1.charAt(i10);
          if (i20 != i15)
          {
            i20 = i10 + 1;
            break label1298;
          }
        }
        i20 = i10;
        localObject2 = Integer.valueOf(i20);
        localObject7 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
        str1 = str1.substring(0, i10);
        arrayOfString = new String[i16];
        arrayOfString[0] = "neft_num";
        arrayOfString[j] = localObject5;
        ((GrammarDataObject)localObject7).<init>("NEFT", str1, i, arrayOfString);
        ((Pair)localObject1).<init>(localObject2, localObject7);
        return (Pair)localObject1;
      case 1: 
        localObject1 = new com/twelfthmile/malana/compiler/types/Pair;
        i20 = paramString1.length();
        if (i10 < i20)
        {
          i20 = str1.charAt(i10);
          if (i20 != i15)
          {
            i20 = i10 + 1;
            break label1405;
          }
        }
        i20 = i10;
        localObject2 = Integer.valueOf(i20);
        localObject7 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
        str1 = str1.substring(0, i10);
        arrayOfString = new String[i16];
        arrayOfString[0] = "imps_num";
        arrayOfString[j] = localObject5;
        ((GrammarDataObject)localObject7).<init>("IMPS", str1, i, arrayOfString);
        ((Pair)localObject1).<init>(localObject2, localObject7);
        return (Pair)localObject1;
      case 0: 
        label1179:
        label1298:
        label1405:
        i8 = ((HashMap)localObject2).size();
        if (i8 == i16)
        {
          localObject1 = new com/twelfthmile/malana/compiler/types/Pair;
          i11 = paramString1.length();
          if (i10 < i11)
          {
            i11 = str1.charAt(i10);
            if (i11 != i15)
            {
              i15 = i10 + 1;
              break label1526;
            }
          }
          i15 = i10;
          label1526:
          localObject7 = Integer.valueOf(i15);
          localObject6 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
          str1 = str1.substring(0, i10);
          arrayOfString = new String[i13];
          arrayOfString[0] = "upi_num";
          arrayOfString[j] = localObject5;
          arrayOfString[i16] = "upi_user_from";
          str2 = (String)((HashMap)localObject2).get("from");
          arrayOfString[3] = str2;
          arrayOfString[4] = "upi_user_to";
          localObject2 = (String)((HashMap)localObject2).get("to");
          arrayOfString[5] = localObject2;
          ((GrammarDataObject)localObject6).<init>("UPI", str1, i, arrayOfString);
          ((Pair)localObject1).<init>(localObject7, localObject6);
          return (Pair)localObject1;
        }
        if (localObject6 != null)
        {
          localObject1 = new com/twelfthmile/malana/compiler/types/Pair;
          i20 = paramString1.length();
          if (i10 < i20)
          {
            i20 = str1.charAt(i10);
            if (i20 != i15)
            {
              i20 = i10 + 1;
              break label1687;
            }
          }
          i20 = i10;
          label1687:
          localObject2 = Integer.valueOf(i20);
          localObject7 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
          str1 = str1.substring(0, i10);
          arrayOfString = new String[4];
          arrayOfString[0] = "upi_num";
          arrayOfString[j] = localObject5;
          arrayOfString[i16] = "upi_user";
          arrayOfString[3] = localObject6;
          ((GrammarDataObject)localObject7).<init>("UPI", str1, i, arrayOfString);
          ((Pair)localObject1).<init>(localObject2, localObject7);
          return (Pair)localObject1;
        }
        localObject1 = new com/twelfthmile/malana/compiler/types/Pair;
        i20 = paramString1.length();
        if (i10 < i20)
        {
          i20 = str1.charAt(i10);
          if (i20 != i15)
          {
            i20 = i10 + 1;
            break label1806;
          }
        }
        i20 = i10;
        label1806:
        localObject2 = Integer.valueOf(i20);
        localObject7 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
        str1 = str1.substring(0, i10);
        arrayOfString = new String[i16];
        arrayOfString[0] = "upi_num";
        arrayOfString[j] = localObject5;
        ((GrammarDataObject)localObject7).<init>("UPI", str1, i, arrayOfString);
        ((Pair)localObject1).<init>(localObject2, localObject7);
        return (Pair)localObject1;
      }
    }
    else
    {
      str1 = null;
    }
    return null;
  }
  
  private static Quad checkForVehRegNo(String paramString)
  {
    int i = 1;
    Object localObject = null;
    int j = 1;
    int m = 0;
    String str1 = null;
    while (j > 0)
    {
      int n = paramString.length();
      if (m >= n) {
        break;
      }
      n = paramString.charAt(m);
      int i4 = 4;
      int k;
      switch (j)
      {
      default: 
        break;
      case 4: 
        n = m + 3;
        i4 = paramString.length();
        if (n < i4)
        {
          n = m + 4;
          String str2 = paramString.substring(m, n);
          boolean bool5 = com.twelfthmile.e.b.c.a(str2);
          if (bool5)
          {
            Quad localQuad = new com/twelfthmile/malana/compiler/types/Quad;
            Integer localInteger = Integer.valueOf(n);
            paramString = paramString.substring(0, n);
            localQuad.<init>(localInteger, paramString, localObject, null);
            return localQuad;
          }
        }
        else
        {
          j = -1;
        }
        break;
      case 3: 
        boolean bool1 = com.twelfthmile.e.b.c.f(n);
        if (bool1)
        {
          k = m + 1;
          int i1 = paramString.length();
          if (k < i1)
          {
            boolean bool2 = com.twelfthmile.e.b.c.f(paramString.charAt(k));
            if (bool2)
            {
              m = k;
              k = 4;
              break;
            }
          }
          k = 4;
        }
        else
        {
          k = -1;
        }
        break;
      case 2: 
        k = m + 1;
        int i2 = paramString.length();
        if (k < i2)
        {
          boolean bool3 = com.twelfthmile.e.b.c.a(paramString.charAt(k));
          if (bool3)
          {
            m = k;
            k = 3;
            break;
          }
        }
        k = -1;
        break;
      case 1: 
        k = m + 1;
        int i3 = paramString.length();
        if (k < i3)
        {
          HashMap localHashMap = getRegState();
          int i5 = m + 2;
          String str3 = paramString.substring(m, i5);
          boolean bool4 = localHashMap.containsKey(str3);
          if (bool4)
          {
            localObject = getRegState();
            str1 = paramString.substring(m, i5);
            str1 = (String)((HashMap)localObject).get(str1);
            localObject = str1;
            m = k;
            k = 2;
            break;
          }
        }
        k = -1;
      }
      m += i;
    }
    return null;
  }
  
  private static Pair classifyInGeneral(String paramString, int paramInt, Request paramRequest, HashMap paramHashMap)
  {
    paramHashMap = yuga(paramString, paramHashMap);
    int i = 1;
    String str1 = null;
    if (paramHashMap != null) {}
    try
    {
      paramString = paramHashMap.getB();
      paramString = (GrammarDataObject)paramString;
      paramString = label;
      localObject1 = "NUM";
      bool1 = paramString.equals(localObject1);
      if (bool1)
      {
        paramString = parser;
        paramString = paramString.prevTokens(0);
        localObject1 = "AMNT";
        bool1 = paramString.equals(localObject1);
        if (!bool1)
        {
          paramString = parser;
          paramString = paramString.prevTokens(i);
          localObject1 = "AMNT";
          bool1 = paramString.equals(localObject1);
          if (bool1)
          {
            paramString = parser;
            paramString = paramString.prevGDO(0);
            paramString = str;
            localObject1 = "of";
            bool1 = paramString.equals(localObject1);
            if (!bool1) {}
          }
        }
        else
        {
          paramString = paramHashMap.getB();
          paramString = (GrammarDataObject)paramString;
          localObject1 = "AMT";
          label = ((String)localObject1);
          paramString = paramHashMap.getB();
          paramString = (GrammarDataObject)paramString;
          paramString = values;
          localObject1 = "amt";
          paramRequest = paramHashMap.getB();
          paramRequest = (GrammarDataObject)paramRequest;
          paramRequest = values;
          String str2 = "num";
          paramRequest = paramRequest.remove(str2);
          paramString.put((String)localObject1, paramRequest);
        }
      }
    }
    catch (Exception localException2)
    {
      Object localObject1;
      boolean bool1;
      for (;;) {}
    }
    return paramHashMap;
    paramHashMap = Controller.getInstance().getSeedData().getClassifierRoot();
    try
    {
      Object localObject2 = paramHashMap.keySet();
      localObject2 = ((Set)localObject2).iterator();
      do
      {
        boolean bool3;
        do
        {
          boolean bool2 = ((Iterator)localObject2).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = ((Iterator)localObject2).next();
          localObject3 = (String)localObject3;
          localObject4 = paramHashMap.get(localObject3);
          localObject4 = (List)localObject4;
          bool3 = prevTokenChecker(paramRequest, 0, (List)localObject4);
        } while (!bool3);
        localObject3 = classifySelector(paramString, paramInt, (String)localObject3);
      } while (localObject3 == null);
      return (Pair)localObject3;
    }
    catch (Exception localException1)
    {
      Object localObject3;
      Object localObject4;
      localObject1 = parser.prevTokens(0);
      paramRequest = "INS";
      paramInt = ((String)localObject1).equals(paramRequest);
      if (paramInt != 0)
      {
        localObject1 = reCheckForAccount(paramString);
        if (localObject1 != null)
        {
          paramRequest = "INSTRNO";
          paramHashMap = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
          int j = ((Integer)((Pair)localObject1).getA()).intValue();
          paramString = paramString.substring(0, j);
          localObject3 = new String[2];
          localObject4 = getValType(paramRequest);
          localObject3[0] = localObject4;
          str1 = (String)((Pair)localObject1).getB();
          localObject3[i] = str1;
          paramHashMap.<init>(paramRequest, paramString, -1, (String[])localObject3);
          bool1 = isMature(paramRequest);
          mature = bool1;
          paramString = new com/twelfthmile/malana/compiler/types/Pair;
          localObject1 = ((Pair)localObject1).getA();
          paramString.<init>(localObject1, paramHashMap);
          return paramString;
        }
      }
      return null;
    }
  }
  
  public static void classifyNonDet(GrammarDataObject paramGrammarDataObject)
  {
    String str1 = str;
    int i = GrammarDataObject.SC_NORM;
    int j = 1;
    int k = 0;
    int m = 1;
    int i2;
    for (;;)
    {
      int i3 = str1.length();
      if ((k >= i3) || (m <= 0)) {
        break;
      }
      i3 = str1.charAt(k);
      int i5 = 5;
      int i7 = 3;
      int i8 = 7;
      int i10 = 2;
      char c1;
      int i6;
      if (m != i8)
      {
        int i11 = 64;
        switch (m)
        {
        default: 
          break;
        case 5: 
          if (i3 != i11) {
            break;
          }
          m = 7;
          break;
        case 4: 
          if (i3 != i11) {
            break;
          }
          m = 7;
          break;
        case 3: 
          if (i3 != i11) {
            break;
          }
          m = 7;
          break;
        case 2: 
          boolean bool4 = com.twelfthmile.e.b.c.f(i3);
          if (bool4)
          {
            m = 2;
          }
          else
          {
            bool4 = com.twelfthmile.e.b.c.g(i3);
            double d1 = 0.3D;
            int i4;
            if (bool4)
            {
              if (k == j)
              {
                m = 4;
              }
              else if (k > i10)
              {
                double d2 = k;
                double d3 = str1.length();
                Double.isNaN(d3);
                d3 *= d1;
                i4 = d2 < d3;
                if (i4 >= 0) {
                  m = 5;
                }
              }
            }
            else
            {
              boolean bool1 = com.twelfthmile.e.b.c.a(i4);
              int n;
              if (bool1)
              {
                if (k == j)
                {
                  n = str1.charAt(0);
                  c1 = 'P';
                  if (n == c1)
                  {
                    n = 2;
                    break;
                  }
                }
                if (k > i10)
                {
                  double d4 = k;
                  double d5 = str1.length();
                  Double.isNaN(d5);
                  d5 *= d1;
                  i6 = d4 < d5;
                  if (i6 >= 0)
                  {
                    n = 9;
                    break;
                  }
                }
                n = 3;
              }
              else
              {
                n = 47;
                if (c1 != n)
                {
                  n = 42;
                  if (c1 != n)
                  {
                    n = 45;
                    if (c1 != n)
                    {
                      n = 58;
                      if (c1 != n)
                      {
                        n = 3;
                        break;
                      }
                    }
                  }
                }
                n = 2;
              }
            }
          }
          break;
        case 1: 
          boolean bool2 = com.twelfthmile.e.b.c.f(c1);
          if (bool2)
          {
            int i1 = 2;
          }
          else
          {
            boolean bool3 = com.twelfthmile.e.b.c.g(c1);
            if (bool3) {
              i2 = 3;
            } else {
              i2 = -1;
            }
          }
          break;
        }
      }
      else
      {
        String str2 = str1.substring(k);
        int i9 = lookAheadSpaceForEmail(str2);
        char c2 = '.';
        if ((c1 == c2) && ((i9 == i7) || (i9 == i10) || (i9 == i6)))
        {
          k = i9;
          i2 = 8;
        }
      }
      k += j;
    }
    switch (i2)
    {
    case 3: 
    case 6: 
    default: 
      break;
    case 9: 
      i = GrammarDataObject.SC_FCSN;
      break;
    case 8: 
      i = GrammarDataObject.SC_EML;
      break;
    case 7: 
      i = GrammarDataObject.SC_VPD;
      break;
    case 5: 
      i = GrammarDataObject.SC_FCSC;
      break;
    case 4: 
      i = GrammarDataObject.SC_CAMEL;
      break;
    case 2: 
      i = GrammarDataObject.SC_CAPS;
    }
    strType = i;
  }
  
  private static Pair classifySelector(String paramString1, int paramInt, String paramString2)
  {
    int i = paramString2.hashCode();
    int j = 1990906992;
    if (i == j)
    {
      str = "CLS_ID";
      boolean bool = paramString2.equals(str);
      if (bool)
      {
        bool = false;
        paramString2 = null;
        break label48;
      }
    }
    int k = -1;
    label48:
    i = 0;
    String str = null;
    if (k != 0) {
      return null;
    }
    paramString1 = checkForId(paramString1, paramInt);
    if (paramString1 != null) {
      return paramString1;
    }
    return null;
  }
  
  static Pair classifyUsingGrammar(String paramString, int paramInt, Request paramRequest, HashMap paramHashMap)
  {
    Object localObject1 = "SEATNO";
    Object localObject2 = { "SEAT", localObject1 };
    int i = 1;
    boolean bool1 = prevTokenChecker(paramRequest, i, (String[])localObject2);
    int m = 3;
    int n = 4;
    int i1 = -1;
    int i2 = 2;
    String str1 = null;
    Object localObject3;
    int i3;
    String[] arrayOfString2;
    if (bool1)
    {
      localObject2 = checkForSeatNo(paramString);
      if (localObject2 != null)
      {
        localObject3 = (String)((Quad)localObject2).getD();
        paramRequest = "";
        paramInt = ((String)localObject3).equals(paramRequest);
        if (paramInt == 0)
        {
          localObject3 = new com/twelfthmile/malana/compiler/types/Pair;
          paramRequest = ((Quad)localObject2).getA();
          paramHashMap = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
          i3 = ((Integer)((Quad)localObject2).getA()).intValue();
          paramString = paramString.substring(0, i3);
          String[] arrayOfString1 = new String[6];
          arrayOfString1[0] = "seat";
          str1 = (String)((Quad)localObject2).getB();
          arrayOfString1[i] = str1;
          arrayOfString1[i2] = "seatnum";
          localObject1 = (String)((Quad)localObject2).getC();
          arrayOfString1[m] = localObject1;
          arrayOfString1[n] = "booking_amt";
          localObject2 = (String)((Quad)localObject2).getD();
          arrayOfString1[5] = localObject2;
          paramHashMap.<init>("SEATNUM", paramString, i1, arrayOfString1);
          ((Pair)localObject3).<init>(paramRequest, paramHashMap);
          return (Pair)localObject3;
        }
        localObject3 = new com/twelfthmile/malana/compiler/types/Pair;
        paramRequest = ((Quad)localObject2).getA();
        paramHashMap = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
        i3 = ((Integer)((Quad)localObject2).getA()).intValue();
        paramString = paramString.substring(0, i3);
        arrayOfString2 = new String[n];
        arrayOfString2[0] = "seat";
        str1 = (String)((Quad)localObject2).getB();
        arrayOfString2[i] = str1;
        arrayOfString2[i2] = "seatnum";
        localObject2 = (String)((Quad)localObject2).getC();
        arrayOfString2[m] = localObject2;
        paramHashMap.<init>("SEATNUM", paramString, i1, arrayOfString2);
        ((Pair)localObject3).<init>(paramRequest, paramHashMap);
        return (Pair)localObject3;
      }
    }
    String str2 = "CARCL";
    localObject2 = new String[] { "CAR", str2 };
    bool1 = prevTokenChecker(paramRequest, 0, (String[])localObject2);
    if (bool1)
    {
      localObject2 = checkForVehRegNo(paramString);
      if (localObject2 != null)
      {
        localObject3 = new com/twelfthmile/malana/compiler/types/Pair;
        paramRequest = ((Quad)localObject2).getA();
        paramHashMap = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
        i3 = ((Integer)((Quad)localObject2).getA()).intValue();
        paramString = paramString.substring(0, i3);
        arrayOfString2 = new String[n];
        arrayOfString2[0] = "regno";
        str1 = (String)((Quad)localObject2).getB();
        arrayOfString2[i] = str1;
        arrayOfString2[i2] = "state";
        localObject2 = (String)((Quad)localObject2).getC();
        arrayOfString2[m] = localObject2;
        paramHashMap.<init>("VHRGID", paramString, i1, arrayOfString2);
        ((Pair)localObject3).<init>(paramRequest, paramHashMap);
        return (Pair)localObject3;
      }
    }
    int j = paramString.charAt(0);
    i = 35;
    if (j == i)
    {
      j = lookAheadHash(paramString);
      localObject1 = yuga(paramString.substring(j), paramHashMap);
      if (localObject1 != null)
      {
        paramString = Integer.valueOf(((Integer)((Pair)localObject1).getA()).intValue() + j);
        ((Pair)localObject1).setA(paramString);
        paramString = (GrammarDataObject)((Pair)localObject1).getB();
        paramInt += j;
        index = paramInt;
        return (Pair)localObject1;
      }
    }
    else
    {
      localObject2 = new String[] { "AMT" };
      boolean bool2 = prevTokenChecker(paramRequest, 0, (String[])localObject2);
      if (bool2)
      {
        int k = cdrParse(paramString);
        if (k >= 0)
        {
          paramString = new com/twelfthmile/malana/compiler/types/Pair;
          paramRequest = Integer.valueOf(i2);
          paramHashMap = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
          if (k == 0) {
            localObject1 = "DR";
          } else {
            localObject1 = "CR";
          }
          if (k == 0) {
            localObject2 = "DR";
          } else {
            localObject2 = "CR";
          }
          paramInt += i2;
          String[] arrayOfString3 = new String[0];
          paramHashMap.<init>((String)localObject1, (String)localObject2, paramInt, arrayOfString3);
          paramString.<init>(paramRequest, paramHashMap);
          return paramString;
        }
      }
    }
    return classifyInGeneral(paramString, paramInt, paramRequest, paramHashMap);
  }
  
  static Pair classifyUsingPrefix(String paramString1, String paramString2, String paramString3, int paramInt, Request paramRequest)
  {
    Object localObject1 = "CRNCY";
    boolean bool1 = paramString2.equals(localObject1);
    int j = 4;
    int k = 0;
    Object localObject2 = null;
    int m = -1;
    int n = 3;
    int i1 = 2;
    int i2 = 1;
    label180:
    label259:
    Object localObject3;
    Object localObject4;
    Object localObject5;
    int i8;
    if (!bool1)
    {
      localObject1 = "AMNT";
      bool1 = paramString2.equals(localObject1);
      if (!bool1)
      {
        paramRequest = "TRANSFER";
        boolean bool2 = paramString2.equalsIgnoreCase(paramRequest);
        if (bool2)
        {
          paramRequest = paramString1.substring(0, paramInt);
          localObject1 = "upi";
          bool2 = paramRequest.equalsIgnoreCase((String)localObject1);
          if (!bool2)
          {
            paramRequest = paramString1.substring(0, paramInt);
            localObject1 = "mmt";
            bool2 = paramRequest.equalsIgnoreCase((String)localObject1);
            if (!bool2)
            {
              paramRequest = paramString1.substring(0, paramInt);
              localObject1 = "neft";
              bool2 = paramRequest.equalsIgnoreCase((String)localObject1);
              if (!bool2) {
                break label180;
              }
            }
          }
          paramString1 = checkForUPI(paramString1, paramString3);
          if (paramString1 == null) {
            break label717;
          }
          getBmature = i2;
          return paramString1;
        }
        paramRequest = "FLTID";
        bool2 = paramString2.equalsIgnoreCase(paramRequest);
        if (bool2)
        {
          paramString2 = paramString1.substring(paramInt);
          int i5 = lookAheadInteger(paramString2);
          int i3 = i5 + 1 + paramInt;
          i = paramString1.length();
          if (i3 < i)
          {
            bool3 = com.twelfthmile.e.b.c.a(paramString1.charAt(i3));
            if (bool3)
            {
              bool3 = true;
              break label259;
            }
          }
          boolean bool3 = false;
          paramRequest = null;
          if (i5 >= 0)
          {
            i = 5;
            if ((i5 <= i) && (bool3))
            {
              i5 += paramInt;
              paramRequest = numberParse(paramString1.substring(i5));
              if (paramRequest != null)
              {
                localObject1 = new com/twelfthmile/malana/compiler/types/Pair;
                localObject2 = Integer.valueOf(((Integer)paramRequest.getA()).intValue() + i5);
                localObject3 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
                int i7 = ((Integer)paramRequest.getA()).intValue();
                i5 += i7;
                paramString2 = paramString1.substring(0, i5);
                localObject4 = new String[j];
                localObject4[0] = "flight_id";
                localObject5 = new java/lang/StringBuilder;
                ((StringBuilder)localObject5).<init>();
                paramString1 = paramString1.substring(0, paramInt);
                ((StringBuilder)localObject5).append(paramString1);
                ((StringBuilder)localObject5).append(" ");
                paramString1 = (String)paramRequest.getB();
                ((StringBuilder)localObject5).append(paramString1);
                paramString1 = ((StringBuilder)localObject5).toString();
                localObject4[i2] = paramString1;
                localObject4[i1] = "flight_name";
                localObject4[n] = paramString3;
                ((GrammarDataObject)localObject3).<init>("FLTIDVAL", paramString2, m, (String[])localObject4);
                ((Pair)localObject1).<init>(localObject2, localObject3);
                getBmature = i2;
                return (Pair)localObject1;
              }
            }
          }
        }
        else
        {
          paramString3 = "HTTP";
          boolean bool6 = paramString2.equalsIgnoreCase(paramString3);
          if (bool6) {
            break label719;
          }
          paramString3 = "WWW";
          bool6 = paramString2.equalsIgnoreCase(paramString3);
          if (bool6) {
            break label719;
          }
          paramString3 = "IDPRX";
          boolean bool4 = paramString2.equalsIgnoreCase(paramString3);
          if (bool4)
          {
            paramString2 = paramString1.substring(paramInt);
            int i6 = lookAheadNumberForIdPrx(paramString2);
            if ((i6 >= 0) && (i6 <= i1))
            {
              i6 += paramInt;
              paramString3 = numberParse(paramString1.substring(i6));
              if (paramString3 != null)
              {
                Pair localPair = new com/twelfthmile/malana/compiler/types/Pair;
                paramRequest = Integer.valueOf(((Integer)paramString3.getA()).intValue() + i6);
                localObject1 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
                k = ((Integer)paramString3.getA()).intValue() + i6;
                localObject2 = paramString1.substring(0, k);
                String[] arrayOfString = new String[i1];
                arrayOfString[0] = "num";
                i8 = ((Integer)paramString3.getA()).intValue();
                i6 += i8;
                paramString1 = paramString1.substring(0, i6);
                arrayOfString[i2] = paramString1;
                ((GrammarDataObject)localObject1).<init>("NUM", (String)localObject2, m, arrayOfString);
                localPair.<init>(paramRequest, localObject1);
                getBmature = i2;
                return localPair;
              }
            }
          }
        }
        label717:
        return null;
        label719:
        return linkParse(paramString1, paramString2, paramInt);
      }
    }
    localObject1 = paramString1.substring(paramInt);
    int i = lookAheadIntegerForAmt((String)localObject1);
    if (i >= 0) {
      if (i > n)
      {
        int i9 = paramInt + i;
        localObject3 = paramString1.substring(paramInt, i9);
        boolean bool7 = isEmpty((String)localObject3);
        if (!bool7) {}
      }
      else
      {
        localObject3 = new java/util/HashMap;
        ((HashMap)localObject3).<init>();
        localObject5 = "YUGA_SC_CURR";
        ((HashMap)localObject3).put("YUGA_SOURCE_CONTEXT", localObject5);
        i += paramInt;
        String str = paramString1.substring(i);
        localObject3 = com.twelfthmile.e.a.a(str, (Map)localObject3);
        if (localObject3 == null) {
          return null;
        }
        str = ((com.twelfthmile.e.a.c)localObject3).a();
        localObject5 = "AMT";
        boolean bool8 = str.equals(localObject5);
        if (!bool8)
        {
          str = ((com.twelfthmile.e.a.c)localObject3).a();
          localObject5 = "NUM";
          bool8 = str.equals(localObject5);
          if (!bool8) {}
        }
        else
        {
          paramString2 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
          int i4 = ((com.twelfthmile.e.a.c)localObject3).c() + i;
          paramString1 = paramString1.substring(0, i4);
          paramRequest = new String[j];
          paramRequest[0] = "amt";
          localObject4 = ((com.twelfthmile.e.a.c)localObject3).b();
          paramRequest[i2] = localObject4;
          paramRequest[i1] = "currency";
          paramString3 = getCurrencySymbol(paramString3);
          paramRequest[n] = paramString3;
          paramString2.<init>("AMT", paramString1, m, paramRequest);
          mature = i2;
          paramString1 = new com/twelfthmile/malana/compiler/types/Pair;
          i8 = ((com.twelfthmile.e.a.c)localObject3).c();
          paramString3 = Integer.valueOf(i + i8);
          paramString1.<init>(paramString3, paramString2);
          return paramString1;
        }
      }
    }
    localObject1 = "CRNCY";
    boolean bool5 = paramString2.equals(localObject1);
    if (bool5)
    {
      paramString2 = parser.prevTokens(0);
      localObject1 = "NUM";
      bool5 = paramString2.equals(localObject1);
      if (bool5)
      {
        paramString2 = parser.prevTokens(i2);
        localObject1 = "PREP";
        bool5 = paramString2.equals(localObject1);
        if (bool5)
        {
          paramString2 = parser.prevGDO(0);
          if (paramString2 != null)
          {
            paramRequest = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
            paramString1 = paramString1.substring(0, paramInt);
            localObject4 = new String[j];
            localObject4[0] = "amt";
            paramString2 = str;
            localObject4[i2] = paramString2;
            localObject4[i1] = "currency";
            paramString2 = getCurrencySymbol(paramString3);
            localObject4[n] = paramString2;
            paramRequest.<init>("AMT", paramString1, m, (String[])localObject4);
            mature = i2;
            paramString1 = new com/twelfthmile/malana/compiler/types/Pair;
            paramString2 = Integer.valueOf(paramInt);
            paramString1.<init>(paramString2, paramRequest);
            return paramString1;
          }
        }
      }
    }
    return null;
  }
  
  private static boolean delimeterWithinKeyVal(String paramString, int paramInt)
  {
    int i = paramString.length();
    if (paramInt < i)
    {
      i = paramString.charAt(paramInt);
      int j = 58;
      if (i != j)
      {
        i = paramString.charAt(paramInt);
        j = 32;
        if (i != j)
        {
          int k = paramString.charAt(paramInt);
          paramInt = 46;
          if (k != paramInt) {
            break label56;
          }
        }
      }
      return true;
    }
    label56:
    return false;
  }
  
  private static String getCurrencySymbol(String paramString)
  {
    if (paramString == null) {
      return "Rs";
    }
    int i = -1;
    int j = paramString.hashCode();
    int k = 104429;
    if (j == k)
    {
      String str = "inr";
      boolean bool = paramString.equals(str);
      if (bool) {
        i = 0;
      }
    }
    if (i != 0) {
      return paramString;
    }
    return "Rs";
  }
  
  private static HashMap getRegState()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("AP", "Andhra Pradesh");
    localHashMap.put("AR", "Arunachal Pradesh");
    localHashMap.put("BR", "Bihar");
    localHashMap.put("CG", "Chhattisgarh");
    localHashMap.put("CH", "Chandigarh");
    localHashMap.put("DD", "Daman and Diu");
    localHashMap.put("DL", "Delhi");
    localHashMap.put("DN", "Dadra and Nagar Haveli");
    localHashMap.put("GA", "Goa");
    localHashMap.put("GJ", "Gujarat");
    localHashMap.put("HP", "Himachal Pradesh");
    localHashMap.put("HR", "Haryana");
    localHashMap.put("JH", "Jharkhand");
    localHashMap.put("JK", "Jammu and Kashmir");
    localHashMap.put("KA", "Karnataka");
    localHashMap.put("KL", "Kerala");
    localHashMap.put("LD", "Lakshadweep");
    localHashMap.put("MH", "Maharashtra");
    localHashMap.put("ML", "Meghalaya");
    localHashMap.put("MN", "Manipur");
    localHashMap.put("MP", "Madhya Pradesh");
    localHashMap.put("MZ", "Mizoram");
    localHashMap.put("NL", "Nagaland");
    localHashMap.put("OD", "OR—Odisha");
    localHashMap.put("PB", "Punjab");
    localHashMap.put("PY", "Puducherry");
    localHashMap.put("RJ", "Rajasthan");
    localHashMap.put("SK", "Sikkim");
    localHashMap.put("TN", "Tamil Nadu");
    localHashMap.put("TS", "Telangana");
    localHashMap.put("TR", "Tripura");
    localHashMap.put("UP", "Uttar Pradesh");
    localHashMap.put("UK", "Uttarakhand");
    localHashMap.put("WB", "West Bengal");
    return localHashMap;
  }
  
  private static String getValType(String paramString)
  {
    int i = paramString.hashCode();
    String str;
    boolean bool1;
    switch (i)
    {
    default: 
      break;
    case 2090926: 
      str = "DATE";
      bool1 = paramString.equals(str);
      if (bool1)
      {
        bool1 = false;
        str = null;
      }
      break;
    case 79041: 
      str = "PCT";
      bool1 = paramString.equals(str);
      if (bool1) {
        bool1 = true;
      }
      break;
    case 77670: 
      str = "NUM";
      bool1 = paramString.equals(str);
      if (bool1) {
        int j = 4;
      }
      break;
    case 64936: 
      str = "AMT";
      boolean bool2 = paramString.equals(str);
      if (bool2) {
        int k = 2;
      }
      break;
    case -1619398259: 
      str = "INSTRNO";
      boolean bool3 = paramString.equals(str);
      if (bool3) {
        m = 3;
      }
      break;
    }
    int m = -1;
    switch (m)
    {
    default: 
      return paramString.toLowerCase();
    case 4: 
      return "num";
    case 3: 
      return "acc_num";
    case 2: 
      return "amt";
    case 1: 
      return "pct";
    }
    return "date";
  }
  
  private static String instrnoValidate(String paramString)
  {
    int i = paramString.length();
    int j = 4;
    if (i < j) {
      return null;
    }
    i = paramString.length() - j;
    return paramString.substring(i);
  }
  
  private static boolean isEmpty(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int k = 32;
      if (j != k)
      {
        j = paramString.charAt(i);
        k = 46;
        if (j != k) {
          return false;
        }
      }
      i += 1;
    }
    return true;
  }
  
  private static boolean isMature(String paramString)
  {
    String str = "INSTRNO";
    boolean bool1 = paramString.equals(str);
    if (!bool1)
    {
      str = "DATE";
      bool1 = paramString.equals(str);
      if (!bool1)
      {
        str = "TIME";
        bool1 = paramString.equals(str);
        if (!bool1)
        {
          str = "TIMES";
          boolean bool2 = paramString.equals(str);
          if (!bool2) {
            return false;
          }
        }
      }
    }
    return true;
  }
  
  private static boolean isUPIDelimeter(char paramChar)
  {
    char c = '-';
    if (paramChar != c)
    {
      c = '/';
      if (paramChar != c)
      {
        c = '*';
        if (paramChar != c) {
          return false;
        }
      }
    }
    return true;
  }
  
  private static Pair linkParse(String paramString1, String paramString2, int paramInt)
  {
    int i = 1;
    int j = paramInt - i;
    paramInt = 1;
    while (paramInt > 0)
    {
      int k = paramString1.length();
      if (j >= k) {
        break;
      }
      k = paramString1.charAt(j);
      int m = 4;
      int n = 2;
      int i1 = -1;
      Object localObject;
      switch (paramInt)
      {
      default: 
        break;
      case 4: 
        m = 32;
        if (k == m)
        {
          paramString2 = new com/twelfthmile/malana/compiler/types/Pair;
          localObject = Integer.valueOf(j);
          GrammarDataObject localGrammarDataObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
          String str = paramString1.substring(0, j);
          String[] arrayOfString = new String[n];
          arrayOfString[0] = "url";
          paramString1 = paramString1.substring(0, j);
          arrayOfString[i] = paramString1;
          localGrammarDataObject.<init>("URL", str, i1, arrayOfString);
          paramString2.<init>(localObject, localGrammarDataObject);
          getBmature = i;
          return paramString2;
        }
        break;
      case 3: 
        paramInt = 46;
        if (k == paramInt) {
          paramInt = 4;
        } else {
          paramInt = -1;
        }
        break;
      case 2: 
        paramInt = 58;
        if (k == paramInt)
        {
          paramInt = j + 1;
          k = paramString1.length();
          if (paramInt < k)
          {
            paramInt = paramString1.charAt(paramInt);
            k = 47;
            if (paramInt == k)
            {
              paramInt = 4;
              break;
            }
          }
        }
        paramInt = -1;
        break;
      case 1: 
        localObject = "HTTP";
        paramInt = paramString2.equalsIgnoreCase((String)localObject);
        if (paramInt != 0)
        {
          paramInt = 2;
        }
        else
        {
          localObject = "WWW";
          paramInt = paramString2.equalsIgnoreCase((String)localObject);
          if (paramInt != 0) {
            paramInt = 3;
          } else {
            paramInt = -1;
          }
        }
        break;
      }
      j += 1;
    }
    return null;
  }
  
  private static int lookAheadHash(String paramString)
  {
    int i = 1;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int k = 32;
      if (j != k) {
        break;
      }
      i += 1;
    }
    return i;
  }
  
  private static int lookAheadInteger(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int k = 32;
      if (j != k)
      {
        int m = 46;
        if (j != m)
        {
          int n = 58;
          if (j != n)
          {
            int i1 = 45;
            if (j != i1)
            {
              int i2 = 44;
              if (j != i2)
              {
                boolean bool = com.twelfthmile.e.b.c.a(j);
                if (bool) {
                  break;
                }
                return -1;
              }
            }
          }
        }
      }
      i += 1;
    }
    return i;
  }
  
  private static int lookAheadIntegerForAmt(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int k = 32;
      if (j != k)
      {
        int m = 46;
        if (j != m)
        {
          int n = 58;
          if (j != n)
          {
            int i1 = 60;
            if (j != i1)
            {
              int i2 = 63;
              if (j != i2)
              {
                int i3 = 124;
                if (j != i3)
                {
                  boolean bool = com.twelfthmile.e.b.c.a(j);
                  if (bool) {
                    break;
                  }
                  int i4 = 45;
                  if (j == i4) {
                    break;
                  }
                  int i5 = 42;
                  if (j == i5) {
                    break;
                  }
                  return -1;
                }
              }
            }
          }
        }
      }
      i += 1;
    }
    return i;
  }
  
  private static int lookAheadIntegerForUPI(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      boolean bool3 = isUPIDelimeter(j);
      if (!bool3)
      {
        bool3 = com.twelfthmile.e.b.c.g(j);
        if (!bool3)
        {
          bool3 = com.twelfthmile.e.b.c.f(j);
          if (!bool3)
          {
            boolean bool1 = com.twelfthmile.e.b.c.a(j);
            if (bool1)
            {
              int k = i + 1;
              int m = paramString.length();
              if (k < m)
              {
                boolean bool2 = com.twelfthmile.e.b.c.a(paramString.charAt(k));
                if (bool2) {
                  break;
                }
              }
            }
            else
            {
              return -1;
            }
          }
        }
      }
      i += 1;
    }
    return i;
  }
  
  private static int lookAheadNumberForIdPrx(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int m = 32;
      if (j != m)
      {
        int n = 45;
        if (j != n)
        {
          boolean bool = com.twelfthmile.e.b.c.a(j);
          if (bool) {
            break;
          }
          int k = 3;
          if (i > k) {
            break;
          }
        }
      }
      i += 1;
    }
    return i;
  }
  
  private static int lookAheadSpaceForEmail(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int k = 32;
      if (j == k) {
        return i;
      }
      i += 1;
    }
    return paramString.length() + -1;
  }
  
  private static int lookAheadSpaceForMovie(String paramString, int paramInt)
  {
    for (;;)
    {
      int i = paramString.length();
      if (paramInt >= i) {
        break;
      }
      i = paramString.charAt(paramInt);
      int j = 32;
      if (i == j) {
        break;
      }
      j = 46;
      if (i == j) {
        break;
      }
      j = 44;
      if (i == j) {
        break;
      }
      paramInt += 1;
    }
    return paramInt;
  }
  
  private static int nonDetDelimeterTillToken(String paramString)
  {
    paramString = paramString.toLowerCase();
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      boolean bool1 = delimeterWithinKeyVal(paramString, i);
      if (bool1)
      {
        int k = i + 3;
        int m = paramString.length();
        if (k < m)
        {
          Object localObject = Controller.getInstance().getSeedData().getTokenTrie();
          try
          {
            localObject = next;
            int n = i + 1;
            n = paramString.charAt(n);
            Character localCharacter1 = Character.valueOf(n);
            localObject = ((HashMap)localObject).get(localCharacter1);
            localObject = (TokenTrie)localObject;
            localObject = next;
            int i1 = i + 2;
            i1 = paramString.charAt(i1);
            localCharacter1 = Character.valueOf(i1);
            localObject = ((HashMap)localObject).get(localCharacter1);
            localObject = (TokenTrie)localObject;
            localObject = next;
            k = paramString.charAt(k);
            Character localCharacter2 = Character.valueOf(k);
            boolean bool2 = ((HashMap)localObject).containsKey(localCharacter2);
            if (bool2) {
              return i;
            }
            i += 1;
          }
          catch (Exception localException) {}
        }
      }
    }
    i = -1;
    return i;
  }
  
  private static Triplet numberParse(String paramString)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    Integer localInteger = null;
    int i = 1;
    int j = 1;
    int m = 0;
    int n = 0;
    int i1;
    int k;
    for (;;)
    {
      i1 = -1;
      if (j <= 0) {
        break;
      }
      int i2 = paramString.length();
      if (m >= i2) {
        break;
      }
      i2 = paramString.charAt(m);
      boolean bool2;
      switch (j)
      {
      default: 
        break;
      case 3: 
        bool2 = com.twelfthmile.e.b.c.a(i2);
        if (bool2)
        {
          ((StringBuilder)localObject).append(i2);
          n = 1;
        }
        else
        {
          if (n == 0)
          {
            j = ((StringBuilder)localObject).length();
            if (j > 0)
            {
              j = ((StringBuilder)localObject).length() - i;
              ((StringBuilder)localObject).deleteCharAt(j);
            }
          }
          j = -1;
        }
        break;
      case 2: 
        bool2 = com.twelfthmile.e.b.c.a(i2);
        if (bool2)
        {
          ((StringBuilder)localObject).append(i2);
        }
        else
        {
          int i3 = 44;
          if (i2 != i3)
          {
            j = 46;
            if (i2 == j)
            {
              ((StringBuilder)localObject).append(i2);
              j = 3;
            }
            else
            {
              j = -1;
            }
          }
        }
        break;
      case 1: 
        boolean bool1 = com.twelfthmile.e.b.c.a(i2);
        if (!bool1)
        {
          k = 45;
          if (i2 != k)
          {
            k = -1;
            break;
          }
        }
        k = 2;
        ((StringBuilder)localObject).append(i2);
      }
      m += 1;
    }
    int i4 = paramString.length();
    if (m == i4) {
      m += 1;
    }
    if (k == i1)
    {
      i4 = ((StringBuilder)localObject).length();
      if (i4 == 0) {
        return null;
      }
    }
    if (n != 0)
    {
      paramString = new com/twelfthmile/malana/compiler/types/Triplet;
      localInteger = Integer.valueOf(m - i);
      localObject = ((StringBuilder)localObject).toString();
      paramString.<init>(localInteger, localObject, "AMT");
      return paramString;
    }
    paramString = new com/twelfthmile/malana/compiler/types/Triplet;
    localInteger = Integer.valueOf(m - i);
    localObject = ((StringBuilder)localObject).toString();
    paramString.<init>(localInteger, localObject, "NUM");
    return paramString;
  }
  
  private static boolean prevTokenChecker(Request paramRequest, int paramInt, List paramList)
  {
    int i = 0;
    while (i <= paramInt)
    {
      String str1 = parser.prevTokens(i);
      Object localObject = parser;
      boolean bool1 = true;
      localObject = ((Parser)localObject).prevTokens(i, bool1);
      Iterator localIterator = paramList.iterator();
      boolean bool2;
      do
      {
        bool2 = localIterator.hasNext();
        if (!bool2) {
          break label101;
        }
        String str2 = (String)localIterator.next();
        boolean bool3 = str2.equals(str1);
        if (bool3) {
          break;
        }
        bool2 = str2.equals(localObject);
      } while (!bool2);
      return bool1;
      label101:
      i += 1;
    }
    return false;
  }
  
  private static boolean prevTokenChecker(Request paramRequest, int paramInt, String... paramVarArgs)
  {
    int i = 0;
    while (i <= paramInt)
    {
      String str1 = parser.prevTokens(i);
      Object localObject = parser;
      boolean bool1 = true;
      localObject = ((Parser)localObject).prevTokens(i, bool1);
      int j = paramVarArgs.length;
      int k = 0;
      while (k < j)
      {
        String str2 = paramVarArgs[k];
        boolean bool2 = str2.equals(str1);
        if (!bool2)
        {
          boolean bool3 = str2.equals(localObject);
          if (!bool3)
          {
            k += 1;
            continue;
          }
        }
        return bool1;
      }
      i += 1;
    }
    return false;
  }
  
  private static Pair reCheckForAccount(String paramString)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("");
    Integer localInteger = null;
    int i = 1;
    boolean bool1 = true;
    int i1 = 0;
    int i2 = 0;
    int i3 = 0;
    while (bool1)
    {
      int i4 = paramString.length();
      if (i1 >= i4) {
        break;
      }
      i4 = paramString.charAt(i1);
      switch (bool1)
      {
      default: 
        break;
      case 2: 
        bool1 = com.twelfthmile.e.b.c.e(i4);
        if (!bool1)
        {
          int j = 47;
          if (i4 != j)
          {
            j = 45;
            if (i4 != j)
            {
              boolean bool2 = com.twelfthmile.e.b.c.a(i4);
              if (bool2)
              {
                ((StringBuilder)localObject).append(i4);
                i3 += 1;
                k = 2;
                break;
              }
              k = -1;
              i3 = 0;
              break;
            }
          }
        }
        int k = 2;
        i3 = 0;
        break;
      case 1: 
        boolean bool3 = com.twelfthmile.e.b.c.e(i4);
        if (bool3)
        {
          int m = 2;
        }
        else
        {
          boolean bool4 = com.twelfthmile.e.b.c.a(i4);
          int n;
          if (bool4)
          {
            ((StringBuilder)localObject).append(i4);
            i3 += 1;
            n = 2;
          }
          else
          {
            n = -1;
          }
        }
        break;
      }
      int i5 = 4;
      if (i3 >= i5) {
        i2 = 1;
      }
      i1 += 1;
    }
    if (i2 != 0)
    {
      paramString = new com/twelfthmile/malana/compiler/types/Pair;
      localInteger = Integer.valueOf(i1 - i);
      localObject = instrnoValidate(((StringBuilder)localObject).toString());
      paramString.<init>(localInteger, localObject);
      return paramString;
    }
    return null;
  }
  
  private static Pair yuga(String paramString, HashMap paramHashMap)
  {
    paramHashMap = com.twelfthmile.e.a.a(paramString, paramHashMap);
    GrammarDataObject localGrammarDataObject = null;
    if (paramHashMap == null) {
      return null;
    }
    String str1 = paramHashMap.a();
    String str2 = "STR";
    boolean bool1 = str1.equals(str2);
    if (!bool1)
    {
      str1 = paramHashMap.a();
      str2 = "INSTRNO";
      bool1 = str1.equals(str2);
      if (bool1)
      {
        str1 = instrnoValidate(paramHashMap.b());
        if (str1 != null)
        {
          str2 = "24X7";
          boolean bool2 = str1.equals(str2);
          if (!bool2)
          {
            paramHashMap.a(str1);
            break label103;
          }
        }
        return null;
      }
      label103:
      localGrammarDataObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
      str1 = paramHashMap.a();
      int i = paramHashMap.c();
      paramString = paramString.substring(0, i);
      i = -1;
      int j = 2;
      String[] arrayOfString = new String[j];
      String str3 = getValType(paramHashMap.a());
      arrayOfString[0] = str3;
      int k = 1;
      str3 = paramHashMap.b();
      arrayOfString[k] = str3;
      localGrammarDataObject.<init>(str1, paramString, i, arrayOfString);
      paramString = paramHashMap.d();
      int m = paramString.size();
      if (m > 0)
      {
        paramString = (HashMap)paramHashMap.d();
        localGrammarDataObject.addValues(paramString);
      }
      boolean bool3 = isMature(paramHashMap.a());
      mature = bool3;
      paramString = new com/twelfthmile/malana/compiler/types/Pair;
      paramHashMap = Integer.valueOf(paramHashMap.c());
      paramString.<init>(paramHashMap, localGrammarDataObject);
      return paramString;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.lex.Classifier
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */