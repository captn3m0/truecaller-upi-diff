package com.twelfthmile.malana.compiler.lex;

import com.twelfthmile.a.d;
import com.twelfthmile.e.a.b;
import com.twelfthmile.e.a.c;
import com.twelfthmile.malana.compiler.Compiler;
import com.twelfthmile.malana.compiler.Seeder;
import com.twelfthmile.malana.compiler.datastructure.TokenTrie;
import com.twelfthmile.malana.compiler.types.Request;
import com.twelfthmile.malana.compiler.types.Response;
import com.twelfthmile.malana.controller.Controller;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex
{
  public static void fallback(Request paramRequest, Response paramResponse)
  {
    Object localObject = address;
    String str1 = "HDFCBK";
    boolean bool = ((String)localObject).equals(str1);
    if (bool)
    {
      localObject = paramResponse.getValMap();
      if (localObject != null)
      {
        localObject = paramResponse.getValMap();
        str1 = "trx_amt";
        bool = ((com.twelfthmile.b.a.a)localObject).containsKey(str1);
        if (bool)
        {
          localObject = Pattern.compile("(?:Your\\strx\\sis\\sdebited\\sto\\sHDFC\\sBank\\sCREDIT\\sCard\\sfor)\\sRs.\\s([0-9]*.\\d{2})\\sin\\s([a-zA-Z]*(?:\\s*.*)?)\\sat\\s((?:[a-zA-Z0-9]*)(?:\\s*.*)?)\\son\\s(\\d{4}-\\d{2}-\\d{2}:\\d{2}:\\d{2}:\\d{2}).*");
          paramRequest = message;
          paramRequest = ((Pattern)localObject).matcher(paramRequest);
          bool = paramRequest.find();
          if (bool)
          {
            localObject = paramResponse.getValMap();
            str1 = "vendor";
            int j = 3;
            String str2 = paramRequest.group(j);
            ((com.twelfthmile.b.a.a)localObject).put(str1, str2);
            int i = 4;
            paramRequest = com.twelfthmile.e.a.b(paramRequest.group(i));
            if (paramRequest != null)
            {
              localObject = paramResponse.getValMap();
              str1 = "date";
              paramRequest = paramRequest.b();
              ((com.twelfthmile.b.a.a)localObject).put(str1, paramRequest);
            }
            paramRequest = "GRM_BANK";
            paramResponse.setCategory(paramRequest);
          }
        }
      }
    }
  }
  
  public static void refill(Request paramRequest, Response paramResponse)
  {
    try
    {
      Object localObject1 = paramResponse.getCategory();
      int i = -1;
      int k = ((String)localObject1).hashCode();
      int n = -1301422793;
      int i1 = 3;
      int i2 = 2;
      int i3 = 0;
      int i5 = 1;
      Object localObject2;
      boolean bool6;
      Object localObject3;
      if (k != n)
      {
        n = -194258755;
        if (k != n)
        {
          n = 1240550297;
          if (k != n)
          {
            n = 1240557924;
            if (k == n)
            {
              localObject2 = "GRM_BILL";
              bool6 = ((String)localObject1).equals(localObject2);
              if (bool6) {
                i = 1;
              }
            }
          }
          else
          {
            localObject2 = "GRM_BANK";
            bool6 = ((String)localObject1).equals(localObject2);
            if (bool6)
            {
              i = 0;
              localObject3 = null;
            }
          }
        }
        else
        {
          localObject2 = "GRM_EVENT";
          bool6 = ((String)localObject1).equals(localObject2);
          if (bool6) {
            i = 3;
          }
        }
      }
      else
      {
        localObject2 = "GRM_TRAVEL";
        bool6 = ((String)localObject1).equals(localObject2);
        if (bool6) {
          i = 2;
        }
      }
      boolean bool1;
      Object localObject4;
      switch (i)
      {
      default: 
        break;
      case 3: 
        localObject1 = address;
        localObject3 = "INSIDR";
        bool6 = ((String)localObject1).equals(localObject3);
        if (bool6)
        {
          localObject1 = "Thank you for buying ([0-9]+) (?:.*) to (.*) on Insider.in. (?:.*) is (.*)";
          localObject1 = Pattern.compile((String)localObject1);
          localObject3 = message;
          localObject1 = ((Pattern)localObject1).matcher((CharSequence)localObject3);
          bool1 = ((Matcher)localObject1).find();
          if (bool1)
          {
            paramRequest = paramResponse.getValMap();
            localObject3 = "seat";
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            localObject4 = ((Matcher)localObject1).group(i5);
            ((StringBuilder)localObject2).append((String)localObject4);
            localObject4 = " ticket(s)";
            ((StringBuilder)localObject2).append((String)localObject4);
            localObject2 = ((StringBuilder)localObject2).toString();
            paramRequest.put((String)localObject3, (String)localObject2);
            paramRequest = paramResponse.getValMap();
            localObject3 = "movie";
            localObject2 = ((Matcher)localObject1).group(i2);
            paramRequest.put((String)localObject3, (String)localObject2);
            paramRequest = paramResponse.getValMap();
            paramResponse = "booking_id";
            localObject1 = ((Matcher)localObject1).group(i1);
            paramRequest.put(paramResponse, (String)localObject1);
            return;
          }
          localObject1 = "Thank you for your RSVP to (.*) on Insider.in. (?:.*) is ([^.]*)";
          localObject1 = Pattern.compile((String)localObject1);
          paramRequest = message;
          paramRequest = ((Pattern)localObject1).matcher(paramRequest);
          bool6 = paramRequest.find();
          if (bool6)
          {
            localObject1 = paramResponse.getValMap();
            localObject3 = "movie";
            localObject2 = paramRequest.group(i5);
            ((com.twelfthmile.b.a.a)localObject1).put((String)localObject3, (String)localObject2);
            paramResponse = paramResponse.getValMap();
            localObject1 = "booking_id";
            paramRequest = paramRequest.group(i2);
            paramResponse.put((String)localObject1, paramRequest);
            return;
          }
          paramResponse.setValid(false);
        }
        break;
      case 2: 
        localObject1 = address;
        localObject3 = "IRCTCI";
        bool6 = ((String)localObject1).equals(localObject3);
        Object localObject5;
        int m;
        Object localObject6;
        if (bool6)
        {
          localObject1 = "PNR:(\\d{8,}),TRAIN:(\\d{5}),DOJ:(\\d{2}-\\d{2}-\\d{2}),([^\\,]+),([A-Z]{2,5})-([A-Z]{2,5}),Dep:((?:\\d{2}:\\d{2})|(?:N.A.)),\\s(.*),\\sFare:(\\d{2,}),(?:.*)";
          localObject1 = Pattern.compile((String)localObject1);
          localObject3 = message;
          localObject1 = ((Pattern)localObject1).matcher((CharSequence)localObject3);
          bool1 = ((Matcher)localObject1).find();
          if (bool1)
          {
            localObject3 = paramResponse.getValMap();
            localObject2 = "travel_category";
            localObject4 = "Train";
            ((com.twelfthmile.b.a.a)localObject3).put((String)localObject2, (String)localObject4);
            localObject3 = paramResponse.getValMap();
            localObject2 = "travel_vendor";
            localObject4 = "IRCTC";
            ((com.twelfthmile.b.a.a)localObject3).put((String)localObject2, (String)localObject4);
            localObject3 = paramResponse.getValMap();
            localObject2 = "pnr_id";
            localObject4 = ((Matcher)localObject1).group(i5);
            ((com.twelfthmile.b.a.a)localObject3).put((String)localObject2, (String)localObject4);
            localObject3 = paramResponse.getValMap();
            localObject2 = "trip_id";
            localObject4 = ((Matcher)localObject1).group(i2);
            ((com.twelfthmile.b.a.a)localObject3).put((String)localObject2, (String)localObject4);
            localObject3 = paramResponse.getValMap();
            localObject2 = "dept_date";
            bool1 = ((com.twelfthmile.b.a.a)localObject3).containsKey((String)localObject2);
            if (!bool1)
            {
              localObject3 = ((Matcher)localObject1).group(i1);
              localObject3 = com.twelfthmile.e.a.a((String)localObject3);
              if (localObject3 != null)
              {
                localObject2 = paramResponse.getValMap();
                localObject4 = "date";
                localObject5 = com.twelfthmile.e.b.a.a();
                localObject3 = ((b)localObject3).b();
                localObject3 = (Date)localObject3;
                localObject3 = ((SimpleDateFormat)localObject5).format((Date)localObject3);
                ((com.twelfthmile.b.a.a)localObject2).put((String)localObject4, (String)localObject3);
              }
            }
            localObject3 = paramResponse.getValMap();
            localObject2 = "trip_class";
            n = 4;
            localObject4 = ((Matcher)localObject1).group(n);
            ((com.twelfthmile.b.a.a)localObject3).put((String)localObject2, (String)localObject4);
            localObject3 = paramResponse.getValMap();
            localObject2 = "from_loc";
            n = 5;
            localObject4 = ((Matcher)localObject1).group(n);
            ((com.twelfthmile.b.a.a)localObject3).put((String)localObject2, (String)localObject4);
            localObject3 = paramResponse.getValMap();
            localObject2 = "to_loc";
            n = 6;
            localObject4 = ((Matcher)localObject1).group(n);
            ((com.twelfthmile.b.a.a)localObject3).put((String)localObject2, (String)localObject4);
            localObject3 = paramResponse.getValMap();
            localObject2 = "dept_time";
            n = 7;
            localObject4 = ((Matcher)localObject1).group(n);
            ((com.twelfthmile.b.a.a)localObject3).put((String)localObject2, (String)localObject4);
            int j = 8;
            localObject2 = ((Matcher)localObject1).group(j);
            localObject4 = ",";
            boolean bool3 = ((String)localObject2).contains((CharSequence)localObject4);
            if (bool3)
            {
              localObject3 = ((Matcher)localObject1).group(j);
              localObject2 = ",";
              m = ((String)localObject3).indexOf((String)localObject2);
              localObject4 = paramResponse.getValMap();
              localObject5 = "seat";
              i2 = m + 1;
              localObject6 = ((String)localObject3).substring(i2);
              ((com.twelfthmile.b.a.a)localObject4).put((String)localObject5, (String)localObject6);
              localObject4 = paramResponse.getValMap();
              localObject5 = "psngrname";
              localObject3 = ((String)localObject3).substring(0, m);
              ((com.twelfthmile.b.a.a)localObject4).put((String)localObject5, (String)localObject3);
            }
            else
            {
              localObject2 = paramResponse.getValMap();
              localObject4 = "seat";
              localObject3 = ((Matcher)localObject1).group(j);
              ((com.twelfthmile.b.a.a)localObject2).put((String)localObject4, (String)localObject3);
            }
            localObject3 = paramResponse.getValMap();
            localObject2 = "fare_amt";
            n = 9;
            localObject1 = ((Matcher)localObject1).group(n);
            ((com.twelfthmile.b.a.a)localObject3).put((String)localObject2, (String)localObject1);
          }
        }
        else
        {
          localObject1 = address;
          localObject3 = "INDIGO";
          bool6 = ((String)localObject1).equals(localObject3);
          if (bool6)
          {
            localObject1 = paramResponse.getValMap();
            localObject3 = "itinerary";
            bool6 = ((com.twelfthmile.b.a.a)localObject1).containsKey((String)localObject3);
            if (bool6)
            {
              localObject1 = "([A-Z]{3})([A-Z]{3})";
              localObject1 = Pattern.compile((String)localObject1);
              localObject3 = paramResponse.getValMap();
              localObject2 = "itinerary";
              localObject3 = ((com.twelfthmile.b.a.a)localObject3).get((String)localObject2);
              localObject1 = ((Pattern)localObject1).matcher((CharSequence)localObject3);
              boolean bool2 = ((Matcher)localObject1).find();
              if (bool2)
              {
                localObject3 = Controller.getInstance();
                localObject3 = ((Compiler)localObject3).getSeedData();
                localObject3 = ((Seeder)localObject3).getCustomRoot();
                localObject2 = ((Matcher)localObject1).group(i5);
                localObject1 = ((Matcher)localObject1).group(i2);
                localObject5 = localObject3;
                n = 0;
                localObject4 = null;
                i2 = 0;
                localObject6 = null;
                for (;;)
                {
                  int i6 = ((String)localObject2).length();
                  if (n >= i6) {
                    break;
                  }
                  i6 = ((String)localObject2).charAt(n);
                  char c2 = Character.toLowerCase(i6);
                  Object localObject7 = next;
                  Character localCharacter = Character.valueOf(c2);
                  boolean bool8 = ((HashMap)localObject7).containsKey(localCharacter);
                  if (!bool8) {
                    break;
                  }
                  localObject5 = next;
                  Object localObject8 = Character.valueOf(c2);
                  localObject5 = ((HashMap)localObject5).get(localObject8);
                  localObject5 = (TokenTrie)localObject5;
                  boolean bool7 = leaf;
                  if (bool7)
                  {
                    localObject6 = paramResponse.getValMap();
                    localObject8 = "from_loc";
                    localObject7 = token_type_val;
                    ((com.twelfthmile.b.a.a)localObject6).put((String)localObject8, (String)localObject7);
                    i2 = 1;
                  }
                  n += 1;
                }
                if (i2 != 0) {
                  for (;;)
                  {
                    m = ((String)localObject1).length();
                    if (i3 >= m) {
                      break;
                    }
                    m = ((String)localObject1).charAt(i3);
                    char c1 = Character.toLowerCase(m);
                    localObject4 = next;
                    localObject5 = Character.valueOf(c1);
                    boolean bool5 = ((HashMap)localObject4).containsKey(localObject5);
                    if (!bool5) {
                      break;
                    }
                    localObject3 = next;
                    localObject2 = Character.valueOf(c1);
                    localObject3 = ((HashMap)localObject3).get(localObject2);
                    localObject3 = (TokenTrie)localObject3;
                    boolean bool4 = leaf;
                    if (bool4)
                    {
                      localObject2 = paramResponse.getValMap();
                      localObject4 = "to_loc";
                      localObject5 = token_type_val;
                      ((com.twelfthmile.b.a.a)localObject2).put((String)localObject4, (String)localObject5);
                      localObject2 = paramResponse.getValMap();
                      localObject4 = "travel_category";
                      localObject5 = "Flight";
                      ((com.twelfthmile.b.a.a)localObject2).put((String)localObject4, (String)localObject5);
                    }
                    int i4;
                    i3 += 1;
                  }
                }
              }
            }
          }
        }
        localObject1 = paramResponse.getValMap();
        localObject3 = "travel_category";
        localObject1 = ((com.twelfthmile.b.a.a)localObject1).get((String)localObject3);
        localObject3 = "Bus";
        bool6 = ((String)localObject1).equals(localObject3);
        if (bool6)
        {
          localObject1 = address;
          localObject3 = "SPICEJ";
          bool6 = ((String)localObject1).equals(localObject3);
          if (!bool6)
          {
            localObject1 = address;
            localObject3 = "INDIGO";
            bool6 = ((String)localObject1).equals(localObject3);
            if (!bool6)
            {
              paramRequest = address;
              localObject1 = "GOAIRL";
              boolean bool9 = paramRequest.equals(localObject1);
              if (!bool9) {
                break;
              }
            }
          }
          paramRequest = paramResponse.getValMap();
          paramResponse = "travel_category";
          localObject1 = "Flight";
          paramRequest.put(paramResponse, (String)localObject1);
          return;
        }
        break;
      case 1: 
        paramResponse = paramResponse.getValMap();
        localObject1 = "vendor";
        paramRequest = address;
        paramResponse.put((String)localObject1, paramRequest);
        return;
      case 0: 
        paramRequest = paramResponse.getValMap();
        localObject1 = "vendor";
        paramRequest = paramRequest.get((String)localObject1);
        if (paramRequest != null)
        {
          paramRequest = paramResponse.getValMap();
          paramResponse = paramResponse.getValMap();
          paramResponse = d.a(paramResponse);
          paramResponse = paramResponse.getAll();
          paramRequest.putAll(paramResponse);
          return;
        }
        break;
      }
      return;
    }
    catch (Exception localException) {}
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.lex.Regex
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */