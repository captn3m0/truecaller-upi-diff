package com.twelfthmile.malana.compiler.lex;

import com.twelfthmile.malana.compiler.datastructure.TokenTrie;
import com.twelfthmile.malana.compiler.types.CustomTokenResponse;
import java.util.HashMap;

public class Custom
{
  private TokenTrie customHead;
  private TokenTrie customRoot;
  private boolean tokenStackValid;
  
  public Custom(TokenTrie paramTokenTrie)
  {
    customRoot = paramTokenTrie;
    reset();
  }
  
  private boolean checkNext(char paramChar)
  {
    paramChar = Character.toLowerCase(paramChar);
    HashMap localHashMap = customHead.next;
    Character localCharacter = Character.valueOf(paramChar);
    boolean bool = localHashMap.containsKey(localCharacter);
    if (bool)
    {
      localHashMap = customHead.next;
      Object localObject = Character.valueOf(paramChar);
      localObject = (TokenTrie)localHashMap.get(localObject);
      customHead = ((TokenTrie)localObject);
      return true;
    }
    return false;
  }
  
  public void hasNext(char paramChar)
  {
    boolean bool = tokenStackValid;
    if (bool)
    {
      paramChar = checkNext(paramChar);
      if (paramChar == 0)
      {
        paramChar = '\000';
        tokenStackValid = false;
      }
    }
  }
  
  public CustomTokenResponse isToken(String paramString)
  {
    boolean bool1 = tokenStackValid;
    String str = null;
    if (!bool1) {
      return null;
    }
    Object localObject1 = customHead;
    bool1 = leaf;
    int j = 1;
    Object localObject2;
    if (bool1)
    {
      int i = paramString.length();
      if (j != i)
      {
        boolean bool2 = Tokenizer.goodEndings(paramString.charAt(j));
        if (!bool2) {}
      }
      else
      {
        paramString = new com/twelfthmile/malana/compiler/types/CustomTokenResponse;
        localObject1 = customHead.token_name;
        str = customHead.token_type_name;
        localObject2 = customHead.token_type_val;
        paramString.<init>(j, (String)localObject1, str, (String)localObject2);
        return paramString;
      }
    }
    localObject1 = customHead;
    for (;;)
    {
      int k = paramString.length();
      if (j >= k) {
        break;
      }
      k = paramString.charAt(j);
      boolean bool3 = checkNext(k);
      if (!bool3)
      {
        customHead = ((TokenTrie)localObject1);
        return null;
      }
      localObject2 = customHead;
      bool3 = leaf;
      if (bool3)
      {
        int m = j + 1;
        int n = paramString.length();
        if (m < n)
        {
          boolean bool4 = Tokenizer.goodEndings(paramString.charAt(m));
          if (bool4)
          {
            paramString = new com/twelfthmile/malana/compiler/types/CustomTokenResponse;
            localObject1 = customHead.token_name;
            str = customHead.token_type_name;
            localObject2 = customHead.token_type_val;
            paramString.<init>(j, (String)localObject1, str, (String)localObject2);
            return paramString;
          }
        }
      }
      j += 1;
    }
    customHead = ((TokenTrie)localObject1);
    return null;
  }
  
  public void reset()
  {
    tokenStackValid = true;
    TokenTrie localTokenTrie = customRoot;
    customHead = localTokenTrie;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.lex.Custom
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */