package com.twelfthmile.malana.compiler;

import com.twelfthmile.malana.compiler.datastructure.AddressTrie;
import com.twelfthmile.malana.compiler.datastructure.GrammarTrie;
import com.twelfthmile.malana.compiler.datastructure.RootTrie;
import com.twelfthmile.malana.compiler.datastructure.TokenTrie;
import com.twelfthmile.malana.compiler.parser.score.Score;
import com.twelfthmile.malana.compiler.parser.semantic.SemanticSeedConstants;
import com.twelfthmile.malana.compiler.parser.semantic.SemanticSeedObject;
import com.twelfthmile.malana.compiler.types.ConfType;
import com.twelfthmile.malana.compiler.types.SemanticSeed;
import com.twelfthmile.malana.compiler.util.L;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class Seeder
{
  private static final String CHILDREN = "children";
  private static final String CLASSIFIER = "CLASSIFIER";
  private static final String GRAMMAR = "GRAMMAR";
  private static final String GRMR = "GRMR";
  private static final String STRUCT = "STRUCT";
  private static final String S_ATTR = "attr";
  private static final String S_OPERATION = "operation";
  private static final String S_TOKENS = "tokens";
  private static final String S_TYPE = "type";
  private static final String TOKENS = "TOKENS";
  private AddressTrie mAddressRoot;
  private TokenTrie mBankRoot;
  private HashMap mClassifierRoot;
  private TokenTrie mCustomRoot;
  private HashMap mGrammarFirstLevelMap;
  private HashMap mGrammarRootMap;
  private TokenTrie mOfferRoot;
  private JSONObject mSeedJsonObj;
  private SemanticSeed mSemanticSeed;
  private TokenTrie mTokenRoot;
  Score score;
  
  public Seeder()
  {
    Object localObject = new com/twelfthmile/malana/compiler/datastructure/TokenTrie;
    ((TokenTrie)localObject).<init>();
    mTokenRoot = ((TokenTrie)localObject);
    localObject = new com/twelfthmile/malana/compiler/datastructure/TokenTrie;
    ((TokenTrie)localObject).<init>();
    mOfferRoot = ((TokenTrie)localObject);
    localObject = new com/twelfthmile/malana/compiler/datastructure/TokenTrie;
    ((TokenTrie)localObject).<init>();
    mCustomRoot = ((TokenTrie)localObject);
    localObject = new com/twelfthmile/malana/compiler/datastructure/TokenTrie;
    ((TokenTrie)localObject).<init>();
    mBankRoot = ((TokenTrie)localObject);
    localObject = new com/twelfthmile/malana/compiler/datastructure/AddressTrie;
    ((AddressTrie)localObject).<init>();
    mAddressRoot = ((AddressTrie)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    mClassifierRoot = ((HashMap)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    mGrammarRootMap = ((HashMap)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    mGrammarFirstLevelMap = ((HashMap)localObject);
    localObject = new com/twelfthmile/malana/compiler/types/SemanticSeed;
    ((SemanticSeed)localObject).<init>();
    mSemanticSeed = ((SemanticSeed)localObject);
  }
  
  private void addAddress(String paramString1, String paramString2)
  {
    Object localObject = mAddressRoot;
    int i = paramString1.length();
    int j = 0;
    while (j < i)
    {
      char c = paramString1.charAt(j);
      boolean bool1 = true;
      child = bool1;
      HashMap localHashMap = next;
      Character localCharacter1 = Character.valueOf(c);
      boolean bool2 = localHashMap.containsKey(localCharacter1);
      if (!bool2)
      {
        localHashMap = next;
        localCharacter1 = Character.valueOf(c);
        AddressTrie localAddressTrie = new com/twelfthmile/malana/compiler/datastructure/AddressTrie;
        localAddressTrie.<init>();
        localHashMap.put(localCharacter1, localAddressTrie);
      }
      int k = i + -1;
      if (j == k)
      {
        paramString1 = next;
        Character localCharacter2 = Character.valueOf(c);
        getleaf = bool1;
        paramString1 = next;
        localCharacter2 = Character.valueOf(c);
        paramString1 = getgrammarList;
        boolean bool3 = paramString1.contains(paramString2);
        if (bool3) {
          break;
        }
        paramString1 = next;
        localObject = Character.valueOf(c);
        getgrammarList.add(paramString2);
        return;
      }
      localObject = next;
      Character localCharacter3 = Character.valueOf(c);
      localObject = (AddressTrie)((HashMap)localObject).get(localCharacter3);
      j += 1;
    }
  }
  
  private void addGrammarStruct(String paramString, GrammarTrie paramGrammarTrie)
  {
    String str = " ";
    paramString = paramString.split(str);
    int i = paramString.length;
    int j = 0;
    boolean bool1;
    for (;;)
    {
      int k = 1;
      if (j >= i) {
        break;
      }
      Object localObject1 = paramString[j];
      child = k;
      Object localObject2 = "#";
      boolean bool2 = ((String)localObject1).startsWith((String)localObject2);
      Object localObject3;
      if (bool2)
      {
        value = k;
        localObject3 = ((String)localObject1).substring(k);
        grammar_name = ((String)localObject3);
      }
      else
      {
        localObject3 = next;
        bool1 = ((HashMap)localObject3).containsKey(localObject1);
        if (!bool1)
        {
          localObject3 = next;
          localObject2 = new com/twelfthmile/malana/compiler/datastructure/GrammarTrie;
          ((GrammarTrie)localObject2).<init>();
          ((HashMap)localObject3).put(localObject1, localObject2);
        }
        paramGrammarTrie = (GrammarTrie)next.get(localObject1);
      }
      j += 1;
    }
    leaf = bool1;
  }
  
  private void addWord(String paramString1, String paramString2, TokenTrie paramTokenTrie, String paramString3, String paramString4)
  {
    int i = paramString2.length();
    int j = 0;
    while (j < i)
    {
      char c = paramString2.charAt(j);
      int k = 1;
      child = k;
      Object localObject1 = next;
      Character localCharacter1 = Character.valueOf(c);
      boolean bool = ((HashMap)localObject1).containsKey(localCharacter1);
      if (!bool)
      {
        localObject1 = next;
        localCharacter1 = Character.valueOf(c);
        TokenTrie localTokenTrie = new com/twelfthmile/malana/compiler/datastructure/TokenTrie;
        localTokenTrie.<init>();
        ((HashMap)localObject1).put(localCharacter1, localTokenTrie);
      }
      int m = color;
      if (m == 0)
      {
        color = k;
      }
      else
      {
        m = color;
        if (m == k)
        {
          m = 2;
          color = m;
        }
      }
      m = i + -1;
      if (j == m)
      {
        localObject1 = next;
        localCharacter1 = Character.valueOf(c);
        getleaf = k;
        Object localObject2 = next;
        localObject1 = Character.valueOf(c);
        localObject2 = (TokenTrie)((HashMap)localObject2).get(localObject1);
        token_name = paramString1;
        if (paramString3 != null)
        {
          localObject2 = next;
          localObject1 = Character.valueOf(c);
          localObject2 = (TokenTrie)((HashMap)localObject2).get(localObject1);
          token_type_val = paramString3;
          if (paramString4 != null)
          {
            localObject2 = next;
            localObject1 = Character.valueOf(c);
            localObject2 = (TokenTrie)((HashMap)localObject2).get(localObject1);
            token_type_name = paramString4;
          }
        }
      }
      paramTokenTrie = next;
      Character localCharacter2 = Character.valueOf(c);
      paramTokenTrie = (TokenTrie)paramTokenTrie.get(localCharacter2);
      j += 1;
    }
  }
  
  private void addWord(String paramString1, String paramString2, String paramString3, TokenTrie paramTokenTrie, String paramString4, String paramString5)
  {
    int i = paramString3.length();
    int j = 0;
    while (j < i)
    {
      char c = paramString3.charAt(j);
      int k = 1;
      child = k;
      Object localObject1 = next;
      Character localCharacter1 = Character.valueOf(c);
      boolean bool = ((HashMap)localObject1).containsKey(localCharacter1);
      if (!bool)
      {
        localObject1 = next;
        localCharacter1 = Character.valueOf(c);
        TokenTrie localTokenTrie = new com/twelfthmile/malana/compiler/datastructure/TokenTrie;
        localTokenTrie.<init>();
        ((HashMap)localObject1).put(localCharacter1, localTokenTrie);
      }
      int m = color;
      if (m == 0)
      {
        color = k;
      }
      else
      {
        m = color;
        if (m == k)
        {
          m = 2;
          color = m;
        }
      }
      m = i + -1;
      if (j == m)
      {
        localObject1 = next;
        localCharacter1 = Character.valueOf(c);
        getleaf = k;
        Object localObject2 = next;
        localObject1 = Character.valueOf(c);
        gettoken_name = paramString1;
        localObject2 = next;
        localObject1 = Character.valueOf(c);
        localObject2 = (TokenTrie)((HashMap)localObject2).get(localObject1);
        token_name_aux = paramString2;
        if (paramString4 != null)
        {
          localObject2 = next;
          localObject1 = Character.valueOf(c);
          localObject2 = (TokenTrie)((HashMap)localObject2).get(localObject1);
          token_type_val = paramString4;
          if (paramString5 != null)
          {
            localObject2 = next;
            localObject1 = Character.valueOf(c);
            localObject2 = (TokenTrie)((HashMap)localObject2).get(localObject1);
            token_type_name = paramString5;
          }
        }
      }
      paramTokenTrie = next;
      Character localCharacter2 = Character.valueOf(c);
      paramTokenTrie = (TokenTrie)paramTokenTrie.get(localCharacter2);
      j += 1;
    }
  }
  
  private void addWord(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Object localObject1 = mTokenRoot;
    String str1 = "|";
    boolean bool1 = paramString3.contains(str1);
    int i = 0;
    int j = 1;
    if (bool1)
    {
      paramString3 = paramString3.split("\\|");
      str1 = paramString3[0];
      paramString3 = paramString3[j];
      String str2 = str1;
      str1 = paramString3;
      paramString3 = str2;
    }
    else
    {
      bool1 = false;
      str1 = null;
    }
    int k = paramString3.length();
    while (i < k)
    {
      char c = paramString3.charAt(i);
      child = j;
      Object localObject2 = next;
      Character localCharacter1 = Character.valueOf(c);
      boolean bool2 = ((HashMap)localObject2).containsKey(localCharacter1);
      if (!bool2)
      {
        localObject2 = next;
        localCharacter1 = Character.valueOf(c);
        TokenTrie localTokenTrie = new com/twelfthmile/malana/compiler/datastructure/TokenTrie;
        localTokenTrie.<init>();
        ((HashMap)localObject2).put(localCharacter1, localTokenTrie);
      }
      int m = color;
      if (m == 0)
      {
        color = j;
      }
      else
      {
        m = color;
        if (m == j)
        {
          m = 2;
          color = m;
        }
      }
      m = k + -1;
      if (i == m)
      {
        localObject2 = next;
        localCharacter1 = Character.valueOf(c);
        getleaf = j;
        localObject2 = next;
        localCharacter1 = Character.valueOf(c);
        gettoken_name = paramString1;
        localObject2 = next;
        localCharacter1 = Character.valueOf(c);
        localObject2 = (TokenTrie)((HashMap)localObject2).get(localCharacter1);
        token_name_aux = paramString2;
        if (str1 != null)
        {
          localObject2 = next;
          localCharacter1 = Character.valueOf(c);
          localObject2 = (TokenTrie)((HashMap)localObject2).get(localCharacter1);
          token_type_val = str1;
          if (paramString4 != null)
          {
            localObject2 = next;
            localCharacter1 = Character.valueOf(c);
            localObject2 = (TokenTrie)((HashMap)localObject2).get(localCharacter1);
            token_type_name = paramString4;
          }
        }
      }
      localObject1 = next;
      Character localCharacter2 = Character.valueOf(c);
      localObject1 = (TokenTrie)((HashMap)localObject1).get(localCharacter2);
      i += 1;
    }
  }
  
  private void addressSeeding(JSONArray paramJSONArray, String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramJSONArray.length();
      if (i >= j) {
        break;
      }
      String str = paramJSONArray.getString(i);
      addAddress(str, paramString);
      i += 1;
    }
  }
  
  private void seedContextBreaks(JSONObject paramJSONObject)
  {
    paramJSONObject = paramJSONObject.getJSONArray("contextbreak");
    int i = 0;
    for (;;)
    {
      int j = paramJSONObject.length();
      if (i >= j) {
        break;
      }
      List localList = mSemanticSeed.getContextBreak();
      String str = paramJSONObject.getString(i);
      localList.add(str);
      i += 1;
    }
  }
  
  private void seedSemanticConstants(JSONObject paramJSONObject)
  {
    Object localObject1 = paramJSONObject.getJSONObject("seedconstants");
    Object localObject2 = ((JSONObject)localObject1).keys();
    Object localObject3;
    SemanticSeedConstants localSemanticSeedConstants1;
    int i;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = (String)((Iterator)localObject2).next();
      localSemanticSeedConstants1 = mSemanticSeed.getSemanticConstants();
      i = ((JSONObject)localObject1).getInt((String)localObject3);
      localSemanticSeedConstants1.put((String)localObject3, i);
    }
    paramJSONObject = paramJSONObject.getJSONObject("coredims");
    localObject1 = paramJSONObject.keys();
    boolean bool2 = ((Iterator)localObject1).hasNext();
    if (bool2)
    {
      localObject2 = (String)((Iterator)localObject1).next();
      localObject3 = paramJSONObject.getJSONArray((String)localObject2);
      int j = 0;
      localSemanticSeedConstants1 = null;
      for (;;)
      {
        i = ((JSONArray)localObject3).length();
        if (j >= i) {
          break;
        }
        SemanticSeedConstants localSemanticSeedConstants2 = mSemanticSeed.getSemanticConstants();
        String str = ((JSONArray)localObject3).getString(j);
        localSemanticSeedConstants2.put((String)localObject2, str);
        j += 1;
      }
    }
  }
  
  private void semanticSeeder(JSONObject paramJSONObject1, JSONObject paramJSONObject2, String paramString, List paramList)
  {
    Seeder localSeeder = this;
    JSONObject localJSONObject1 = paramJSONObject2;
    Iterator localIterator = paramJSONObject1.keys();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      String str1 = (String)localIterator.next();
      JSONObject localJSONObject2 = paramJSONObject1.getJSONObject(str1);
      Object localObject1 = "tokens";
      boolean bool2 = localJSONObject2.has((String)localObject1);
      String str2 = null;
      if (bool2)
      {
        localObject1 = localJSONObject2.getJSONArray("tokens");
      }
      else
      {
        bool2 = false;
        localObject1 = null;
      }
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append(paramString);
      Object localObject3 = ":";
      ((StringBuilder)localObject2).append((String)localObject3);
      ((StringBuilder)localObject2).append(str1);
      str1 = ((StringBuilder)localObject2).toString();
      localObject2 = "attr";
      boolean bool3 = localJSONObject2.has((String)localObject2);
      if (bool3)
      {
        localObject2 = localJSONObject2.getJSONObject("attr");
      }
      else
      {
        bool3 = false;
        localObject2 = null;
      }
      localObject3 = "operation";
      int i = localJSONObject2.has((String)localObject3);
      if (i != 0)
      {
        localObject3 = localJSONObject2.getJSONArray("operation");
      }
      else
      {
        i = 0;
        localObject3 = null;
      }
      Object localObject4 = "type";
      boolean bool4 = localJSONObject2.has((String)localObject4);
      if (bool4) {
        str2 = localJSONObject2.getString("type");
      }
      localObject4 = new java/util/ArrayList;
      ((ArrayList)localObject4).<init>();
      ((List)localObject4).addAll(paramList);
      int k = localJSONObject1.has(str2);
      Object localObject5;
      SemanticSeedObject localSemanticSeedObject;
      int i1;
      Object localObject6;
      if (k != 0)
      {
        localObject5 = localJSONObject1.getJSONArray(str2);
        int n = 0;
        localSemanticSeedObject = null;
        for (;;)
        {
          i1 = ((JSONArray)localObject5).length();
          if (n >= i1) {
            break;
          }
          localObject6 = ((JSONArray)localObject5).getJSONObject(n);
          ((List)localObject4).add(localObject6);
          n += 1;
        }
      }
      int m;
      if (localObject3 != null)
      {
        k = 0;
        localObject5 = null;
        for (;;)
        {
          i1 = ((JSONArray)localObject3).length();
          if (k >= i1) {
            break;
          }
          localObject6 = ((JSONArray)localObject3).getJSONObject(k);
          ((List)localObject4).add(localObject6);
          k += 1;
        }
      }
      if (localObject1 != null)
      {
        i = 0;
        localObject3 = null;
        for (;;)
        {
          m = ((JSONArray)localObject1).length();
          if (i >= m) {
            break;
          }
          localObject5 = mSemanticSeed.getSemanticMap();
          localObject6 = ((JSONArray)localObject1).getString(i);
          localSemanticSeedObject = new com/twelfthmile/malana/compiler/parser/semantic/SemanticSeedObject;
          localSemanticSeedObject.<init>(str2, (JSONObject)localObject2, (List)localObject4, str1);
          ((HashMap)localObject5).put(localObject6, localSemanticSeedObject);
          int j;
          i += 1;
        }
      }
      localObject1 = "children";
      bool2 = localJSONObject2.has((String)localObject1);
      if (bool2)
      {
        localObject1 = "children";
        localJSONObject2 = localJSONObject2.getJSONObject((String)localObject1);
        localSeeder.semanticSeeder(localJSONObject2, localJSONObject1, str1, (List)localObject4);
      }
    }
  }
  
  public GrammarTrie addGrammar(String paramString, RootTrie paramRootTrie)
  {
    paramRootTrie = grammar;
    paramString = paramString.split(" ");
    int i = 1;
    Object localObject1 = paramRootTrie;
    int j = 0;
    paramRootTrie = null;
    boolean bool1 = true;
    for (;;)
    {
      int k = paramString.length;
      if (j >= k) {
        break;
      }
      Object localObject2 = paramString[j];
      Object localObject3 = "{";
      int m = ((String)localObject2).startsWith((String)localObject3);
      Object localObject4;
      if (m != 0)
      {
        localObject3 = Pattern.compile("\\{([^\\}]*)\\}(.*)");
        localObject2 = ((Pattern)localObject3).matcher((CharSequence)localObject2);
        m = ((Matcher)localObject2).matches();
        int n = ((Matcher)localObject2).groupCount();
        int i1 = 2;
        if (n == i1)
        {
          n = 1;
        }
        else
        {
          n = 0;
          localObject4 = null;
        }
        m &= n;
        if (m != 0)
        {
          localObject3 = ((Matcher)localObject2).group(i1);
          if (j > 0)
          {
            localObject2 = ((Matcher)localObject2).group(i);
            localObject4 = ":";
            boolean bool2 = ((String)localObject2).contains((CharSequence)localObject4);
            Object localObject5;
            int i2;
            Object localObject6;
            if (bool2)
            {
              localObject2 = ((String)localObject2).split(":");
              localObject4 = mSemanticSeed.getConfidenceMap();
              localObject5 = new java/lang/StringBuilder;
              ((StringBuilder)localObject5).<init>();
              i2 = j + -1;
              localObject6 = paramString[i2];
              ((StringBuilder)localObject5).append((String)localObject6);
              ((StringBuilder)localObject5).append("-");
              ((StringBuilder)localObject5).append((String)localObject3);
              localObject5 = ((StringBuilder)localObject5).toString();
              localObject6 = new com/twelfthmile/malana/compiler/types/ConfType;
              String str1 = localObject2[0];
              int i3 = Integer.parseInt(str1);
              localObject2 = localObject2[i];
              String str2 = ";";
              localObject2 = ((String)localObject2).split(str2);
              ((ConfType)localObject6).<init>(i3, (String[])localObject2);
              ((HashMap)localObject4).put(localObject5, localObject6);
            }
            else
            {
              localObject4 = mSemanticSeed.getConfidenceMap();
              localObject5 = new java/lang/StringBuilder;
              ((StringBuilder)localObject5).<init>();
              i2 = j + -1;
              localObject6 = paramString[i2];
              ((StringBuilder)localObject5).append((String)localObject6);
              ((StringBuilder)localObject5).append("-");
              ((StringBuilder)localObject5).append((String)localObject3);
              localObject5 = ((StringBuilder)localObject5).toString();
              localObject6 = new com/twelfthmile/malana/compiler/types/ConfType;
              k = Integer.parseInt((String)localObject2);
              ((ConfType)localObject6).<init>(k);
              ((HashMap)localObject4).put(localObject5, localObject6);
            }
          }
          localObject2 = localObject3;
        }
        else
        {
          paramString = new java/lang/RuntimeException;
          paramString.<init>("Seeder: confMap not seeded correctly");
          throw paramString;
        }
      }
      child = i;
      if (bool1)
      {
        Object localObject7 = mGrammarFirstLevelMap;
        bool1 = ((HashMap)localObject7).containsKey(localObject2);
        if (!bool1)
        {
          localObject7 = new com/twelfthmile/malana/compiler/datastructure/GrammarTrie;
          ((GrammarTrie)localObject7).<init>();
          localObject3 = mGrammarFirstLevelMap;
          ((HashMap)localObject3).put(localObject2, localObject7);
        }
        localObject7 = next;
        bool1 = ((HashMap)localObject7).containsKey(localObject2);
        if (!bool1)
        {
          localObject7 = next;
          localObject3 = mGrammarFirstLevelMap.get(localObject2);
          ((HashMap)localObject7).put(localObject2, localObject3);
        }
        bool1 = false;
        localObject7 = null;
      }
      else
      {
        localObject3 = next;
        m = ((HashMap)localObject3).containsKey(localObject2);
        if (m == 0)
        {
          localObject3 = next;
          localObject4 = new com/twelfthmile/malana/compiler/datastructure/GrammarTrie;
          ((GrammarTrie)localObject4).<init>();
          ((HashMap)localObject3).put(localObject2, localObject4);
        }
      }
      localObject1 = (GrammarTrie)next.get(localObject2);
      j += 1;
    }
    return (GrammarTrie)localObject1;
  }
  
  public void addStruct(RootTrie paramRootTrie)
  {
    JSONArray localJSONArray = structJSON;
    int i = 0;
    try
    {
      for (;;)
      {
        int j = localJSONArray.length();
        if (i >= j) {
          break;
        }
        String str = localJSONArray.getString(i);
        GrammarTrie localGrammarTrie = struct;
        addGrammarStruct(str, localGrammarTrie);
        i += 1;
      }
      return;
    }
    catch (Exception localException)
    {
      L.error(localException;
    }
  }
  
  public void classifierSeeding()
  {
    JSONObject localJSONObject = mSeedJsonObj.getJSONObject("CLASSIFIER");
    Iterator localIterator = localJSONObject.keys();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str1 = (String)localIterator.next();
      Object localObject = localJSONObject.getJSONArray(str1);
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      int i = 0;
      for (;;)
      {
        int j = ((JSONArray)localObject).length();
        if (i >= j) {
          break;
        }
        String str2 = ((JSONArray)localObject).getString(i);
        localArrayList.add(str2);
        i += 1;
      }
      localObject = mClassifierRoot;
      ((HashMap)localObject).put(str1, localArrayList);
    }
  }
  
  public AddressTrie getAddressTrie()
  {
    return mAddressRoot;
  }
  
  public TokenTrie getBankRoot()
  {
    return mBankRoot;
  }
  
  public HashMap getClassifierRoot()
  {
    return mClassifierRoot;
  }
  
  public TokenTrie getCustomRoot()
  {
    return mCustomRoot;
  }
  
  public HashMap getGrammarRootMap()
  {
    return mGrammarRootMap;
  }
  
  public TokenTrie getOfferRoot()
  {
    return mOfferRoot;
  }
  
  public SemanticSeed getSemanticSeed()
  {
    return mSemanticSeed;
  }
  
  public TokenTrie getTokenTrie()
  {
    return mTokenRoot;
  }
  
  public int grammarSeeding()
  {
    JSONObject localJSONObject1 = mSeedJsonObj.getJSONObject("GRAMMAR");
    Iterator localIterator = localJSONObject1.keys();
    int i = 0;
    JSONArray localJSONArray1 = null;
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (String)localIterator.next();
      JSONObject localJSONObject2 = localJSONObject1.getJSONObject((String)localObject);
      HashMap localHashMap = mGrammarRootMap;
      boolean bool2 = localHashMap.containsKey(localObject);
      if (!bool2)
      {
        localHashMap = mGrammarRootMap;
        RootTrie localRootTrie = new com/twelfthmile/malana/compiler/datastructure/RootTrie;
        JSONArray localJSONArray2 = localJSONObject2.getJSONArray("GRMR");
        JSONArray localJSONArray3 = localJSONObject2.getJSONArray("STRUCT");
        localRootTrie.<init>(localJSONArray2, localJSONArray3);
        localHashMap.put(localObject, localRootTrie);
      }
      localObject = localJSONObject2.getJSONArray("GRMR");
      int j = ((JSONArray)localObject).length();
      if (j > i)
      {
        localJSONArray1 = localJSONObject2.getJSONArray("GRMR");
        i = localJSONArray1.length();
      }
    }
    return i;
  }
  
  HashMap parseGrammarName(String paramString)
  {
    Object localObject1 = "";
    String str1 = "";
    String str2 = "1";
    Object localObject2 = Pattern.compile("(.*)<(.*)>(.*)").matcher(paramString);
    boolean bool1 = ((Matcher)localObject2).matches();
    int j = 2;
    int k = 1;
    if (bool1)
    {
      paramString = ((Matcher)localObject2).group(k);
      str2 = ((Matcher)localObject2).group(j);
      int i = 3;
      localObject2 = ((Matcher)localObject2).group(i);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(paramString);
      localStringBuilder.append((String)localObject2);
      paramString = localStringBuilder.toString();
    }
    localObject2 = Pattern.compile("(.*)\\[(.*)\\]").matcher(paramString);
    boolean bool2 = ((Matcher)localObject2).matches();
    if (bool2)
    {
      localObject1 = ((Matcher)localObject2).group(k);
      str1 = ((Matcher)localObject2).group(j);
    }
    localObject2 = "";
    boolean bool3 = ((String)localObject1).equals(localObject2);
    if (!bool3) {
      paramString = (String)localObject1;
    }
    paramString = paramString.replaceAll("\\s+", "");
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    ((HashMap)localObject1).put("multiplier", str2);
    ((HashMap)localObject1).put("contextIdentifier", str1);
    ((HashMap)localObject1).put("grammarName", paramString);
    return (HashMap)localObject1;
  }
  
  public void seedAddr(JSONObject paramJSONObject)
  {
    Iterator localIterator = paramJSONObject.keys();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)localIterator.next();
      JSONArray localJSONArray = paramJSONObject.getJSONArray(str);
      addressSeeding(localJSONArray, str);
    }
  }
  
  public void seedAirport(JSONObject paramJSONObject)
  {
    Iterator localIterator = paramJSONObject.keys();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = localIterator.next();
      Object localObject2 = localObject1;
      localObject2 = (String)localObject1;
      String str1 = "AIRPORT";
      String str2 = paramJSONObject.getString((String)localObject2);
      TokenTrie localTokenTrie = mCustomRoot;
      String str3 = "loc";
      addWord(str1, str2, localTokenTrie, (String)localObject2, str3);
    }
  }
  
  void seedAllGrammars()
  {
    Object localObject1 = new java/util/HashSet;
    ((HashSet)localObject1).<init>();
    Object localObject2 = mSeedJsonObj;
    Object localObject3 = "GRAMMAR";
    localObject2 = ((JSONObject)localObject2).getJSONObject((String)localObject3).keys();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = ((Iterator)localObject2).next();
      ((Set)localObject1).add(localObject3);
    }
    localObject1 = ((Set)localObject1).iterator();
    do
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      localObject3 = (RootTrie)getGrammarRootMap().get(localObject2);
      localGrammarTrie = grammar;
    } while (localGrammarTrie != null);
    GrammarTrie localGrammarTrie = new com/twelfthmile/malana/compiler/datastructure/GrammarTrie;
    localGrammarTrie.<init>();
    grammar = localGrammarTrie;
    localGrammarTrie = new com/twelfthmile/malana/compiler/datastructure/GrammarTrie;
    localGrammarTrie.<init>();
    struct = localGrammarTrie;
    score.addGrammar((String)localObject2);
    addStruct((RootTrie)localObject3);
    boolean bool2 = false;
    localObject2 = null;
    int i = 0;
    localGrammarTrie = null;
    for (;;)
    {
      Object localObject4 = grammarJSON;
      int j = ((JSONArray)localObject4).length();
      if (i >= j) {
        break;
      }
      localObject4 = grammarJSON.getJSONObject(i);
      Iterator localIterator = ((JSONObject)localObject4).keys();
      for (;;)
      {
        boolean bool3 = localIterator.hasNext();
        if (!bool3) {
          break;
        }
        Object localObject5 = (String)localIterator.next();
        Object localObject6 = parseGrammarName((String)localObject5);
        String str1 = (String)((HashMap)localObject6).get("grammarName");
        String str2 = (String)((HashMap)localObject6).get("multiplier");
        int k = Integer.parseInt(str2);
        localObject6 = (String)((HashMap)localObject6).get("contextIdentifier");
        localObject5 = ((JSONObject)localObject4).getString((String)localObject5);
        String str3 = ",";
        localObject5 = ((String)localObject5).split(str3);
        int m = localObject5.length;
        int n = 0;
        while (n < m)
        {
          Object localObject7 = localObject5[n];
          localObject7 = addGrammar((String)localObject7, (RootTrie)localObject3);
          boolean bool4 = true;
          leaf = bool4;
          grammar_name = str1;
          grammar_context = ((String)localObject6);
          n += 1;
        }
        localObject5 = score;
        ((Score)localObject5).addAux(str1, i, k);
      }
      i += 1;
    }
  }
  
  public void seedBank(JSONObject paramJSONObject)
  {
    Iterator localIterator = paramJSONObject.keys();
    boolean bool = localIterator.hasNext();
    if (bool)
    {
      String str1 = (String)localIterator.next();
      Object localObject1 = paramJSONObject.get(str1);
      Object localObject2 = localObject1;
      localObject2 = (JSONArray)localObject1;
      int i = 0;
      localObject1 = null;
      int j = 0;
      for (;;)
      {
        i = ((JSONArray)localObject2).length();
        if (j >= i) {
          break;
        }
        String str2 = ((JSONArray)localObject2).getString(j);
        TokenTrie localTokenTrie = mBankRoot;
        localObject1 = this;
        addWord(str1, str2, localTokenTrie, null, null);
        j += 1;
      }
    }
  }
  
  public void seedLocation(JSONArray paramJSONArray)
  {
    int i = 0;
    for (;;)
    {
      int j = paramJSONArray.length();
      if (i >= j) {
        break;
      }
      String str1 = "LOCATION";
      String str2 = paramJSONArray.getString(i);
      TokenTrie localTokenTrie = mCustomRoot;
      String str3 = paramJSONArray.getString(i);
      String str4 = "loc";
      addWord(str1, str2, localTokenTrie, str3, str4);
      i += 1;
    }
  }
  
  public void seedOffers(JSONObject paramJSONObject)
  {
    Iterator localIterator = paramJSONObject.keys();
    boolean bool = localIterator.hasNext();
    if (bool)
    {
      String str1 = (String)localIterator.next();
      Object localObject1 = paramJSONObject.get(str1);
      Object localObject2 = localObject1;
      localObject2 = (JSONArray)localObject1;
      int i = 0;
      localObject1 = null;
      int j = 0;
      for (;;)
      {
        i = ((JSONArray)localObject2).length();
        if (j >= i) {
          break;
        }
        String str2 = ((JSONArray)localObject2).getString(j).toUpperCase();
        TokenTrie localTokenTrie = mOfferRoot;
        localObject1 = this;
        addWord(str1, str2, localTokenTrie, null, null);
        j += 1;
      }
    }
  }
  
  public void semanticSeeding(JSONObject paramJSONObject)
  {
    Object localObject1 = "ontology";
    try
    {
      localObject1 = paramJSONObject.getJSONObject((String)localObject1);
      Object localObject2 = "typology";
      localObject2 = paramJSONObject.getJSONObject((String)localObject2);
      String str = "";
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      semanticSeeder((JSONObject)localObject1, (JSONObject)localObject2, str, localArrayList);
      seedContextBreaks(paramJSONObject);
      seedSemanticConstants(paramJSONObject);
      return;
    }
    catch (Exception localException)
    {
      L.error(localException;
    }
  }
  
  public void setSeedJsonObj(JSONObject paramJSONObject)
  {
    mSeedJsonObj = paramJSONObject;
  }
  
  public void tokenSeeding()
  {
    JSONObject localJSONObject = mSeedJsonObj.getJSONObject("TOKENS");
    Iterator localIterator = localJSONObject.keys();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (String)localIterator.next();
      Object localObject2 = "type";
      Object localObject3 = Pattern.compile("(.*)\\[(.*)\\]").matcher((CharSequence)localObject1);
      boolean bool2 = ((Matcher)localObject3).matches();
      int j = 1;
      int k = 2;
      if (bool2)
      {
        int i = ((Matcher)localObject3).groupCount();
        if (i == k)
        {
          localObject2 = ((Matcher)localObject3).group(j);
          localObject3 = ((Matcher)localObject3).group(k);
          break label115;
        }
      }
      localObject3 = localObject2;
      localObject2 = localObject1;
      label115:
      Object localObject4 = Pattern.compile("([^0-9]*)([0-9]+)").matcher((CharSequence)localObject2);
      boolean bool3 = ((Matcher)localObject4).matches();
      if (bool3)
      {
        int m = ((Matcher)localObject4).groupCount();
        if (m == k)
        {
          localObject4 = ((Matcher)localObject4).group(j);
          break label170;
        }
      }
      localObject4 = localObject2;
      label170:
      localObject1 = (String)localJSONObject.get((String)localObject1);
      String str1 = ",";
      localObject1 = ((String)localObject1).split(str1);
      j = localObject1.length;
      k = 0;
      while (k < j)
      {
        String str2 = localObject1[k];
        addWord((String)localObject4, (String)localObject2, str2, (String)localObject3);
        k += 1;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.Seeder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */