package com.twelfthmile.malana.compiler;

import java.io.InputStream;

public class Helper
{
  private static InputStream getFileFromPath(Class paramClass, String paramString)
  {
    return paramClass.getClassLoader().getResourceAsStream(paramString);
  }
  
  /* Error */
  public static String readFile(java.io.File paramFile)
  {
    // Byte code:
    //   0: ldc 22
    //   2: astore_1
    //   3: aconst_null
    //   4: astore_2
    //   5: new 24	java/io/FileInputStream
    //   8: astore_3
    //   9: aload_3
    //   10: aload_0
    //   11: invokespecial 27	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   14: new 29	java/io/BufferedReader
    //   17: astore_0
    //   18: new 31	java/io/InputStreamReader
    //   21: astore 4
    //   23: getstatic 37	java/nio/charset/StandardCharsets:UTF_8	Ljava/nio/charset/Charset;
    //   26: astore 5
    //   28: aload 4
    //   30: aload_3
    //   31: aload 5
    //   33: invokespecial 40	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    //   36: aload_0
    //   37: aload 4
    //   39: invokespecial 43	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   42: new 45	java/lang/StringBuilder
    //   45: astore_2
    //   46: aload_2
    //   47: invokespecial 46	java/lang/StringBuilder:<init>	()V
    //   50: aload_0
    //   51: invokevirtual 50	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   54: astore 4
    //   56: aload 4
    //   58: ifnull +19 -> 77
    //   61: aload_2
    //   62: aload 4
    //   64: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   67: pop
    //   68: aload_0
    //   69: invokevirtual 50	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   72: astore 4
    //   74: goto -18 -> 56
    //   77: aload_2
    //   78: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   81: astore_1
    //   82: aload_3
    //   83: invokevirtual 62	java/io/InputStream:close	()V
    //   86: aload_0
    //   87: invokevirtual 63	java/io/BufferedReader:close	()V
    //   90: goto +73 -> 163
    //   93: astore_0
    //   94: aload_0
    //   95: invokevirtual 68	java/lang/Exception:printStackTrace	()V
    //   98: goto +65 -> 163
    //   101: astore_1
    //   102: aload_0
    //   103: astore_2
    //   104: goto +70 -> 174
    //   107: astore_2
    //   108: aload_3
    //   109: astore 6
    //   111: aload_0
    //   112: astore_3
    //   113: aload_2
    //   114: astore_0
    //   115: goto +13 -> 128
    //   118: astore_1
    //   119: goto +55 -> 174
    //   122: astore_0
    //   123: aload_3
    //   124: astore 6
    //   126: aconst_null
    //   127: astore_3
    //   128: aload 6
    //   130: astore_2
    //   131: goto +12 -> 143
    //   134: astore_1
    //   135: aconst_null
    //   136: astore_3
    //   137: goto +37 -> 174
    //   140: astore_0
    //   141: aconst_null
    //   142: astore_3
    //   143: aload_0
    //   144: invokevirtual 68	java/lang/Exception:printStackTrace	()V
    //   147: aload_2
    //   148: ifnull +7 -> 155
    //   151: aload_2
    //   152: invokevirtual 62	java/io/InputStream:close	()V
    //   155: aload_3
    //   156: ifnull +7 -> 163
    //   159: aload_3
    //   160: invokevirtual 63	java/io/BufferedReader:close	()V
    //   163: aload_1
    //   164: areturn
    //   165: astore_1
    //   166: aload_3
    //   167: astore 6
    //   169: aload_2
    //   170: astore_3
    //   171: aload 6
    //   173: astore_2
    //   174: aload_3
    //   175: ifnull +14 -> 189
    //   178: aload_3
    //   179: invokevirtual 62	java/io/InputStream:close	()V
    //   182: goto +7 -> 189
    //   185: astore_0
    //   186: goto +14 -> 200
    //   189: aload_2
    //   190: ifnull +14 -> 204
    //   193: aload_2
    //   194: invokevirtual 63	java/io/BufferedReader:close	()V
    //   197: goto +7 -> 204
    //   200: aload_0
    //   201: invokevirtual 68	java/lang/Exception:printStackTrace	()V
    //   204: aload_1
    //   205: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	206	0	paramFile	java.io.File
    //   2	80	1	str1	String
    //   101	1	1	localObject1	Object
    //   118	1	1	localObject2	Object
    //   134	30	1	str2	String
    //   165	40	1	localObject3	Object
    //   4	100	2	localObject4	Object
    //   107	7	2	localException	Exception
    //   130	64	2	localObject5	Object
    //   8	171	3	localObject6	Object
    //   21	52	4	localObject7	Object
    //   26	6	5	localCharset	java.nio.charset.Charset
    //   109	63	6	localObject8	Object
    // Exception table:
    //   from	to	target	type
    //   82	86	93	java/lang/Exception
    //   86	90	93	java/lang/Exception
    //   151	155	93	java/lang/Exception
    //   159	163	93	java/lang/Exception
    //   42	45	101	finally
    //   46	50	101	finally
    //   50	54	101	finally
    //   62	68	101	finally
    //   68	72	101	finally
    //   77	81	101	finally
    //   42	45	107	java/lang/Exception
    //   46	50	107	java/lang/Exception
    //   50	54	107	java/lang/Exception
    //   62	68	107	java/lang/Exception
    //   68	72	107	java/lang/Exception
    //   77	81	107	java/lang/Exception
    //   14	17	118	finally
    //   18	21	118	finally
    //   23	26	118	finally
    //   31	36	118	finally
    //   37	42	118	finally
    //   14	17	122	java/lang/Exception
    //   18	21	122	java/lang/Exception
    //   23	26	122	java/lang/Exception
    //   31	36	122	java/lang/Exception
    //   37	42	122	java/lang/Exception
    //   5	8	134	finally
    //   10	14	134	finally
    //   5	8	140	java/lang/Exception
    //   10	14	140	java/lang/Exception
    //   143	147	165	finally
    //   178	182	185	java/lang/Exception
    //   193	197	185	java/lang/Exception
  }
  
  /* Error */
  public static String readFile(Class paramClass, String paramString)
  {
    // Byte code:
    //   0: ldc 22
    //   2: astore_2
    //   3: aconst_null
    //   4: astore_3
    //   5: aload_0
    //   6: aload_1
    //   7: invokestatic 72	com/twelfthmile/malana/compiler/Helper:getFileFromPath	(Ljava/lang/Class;Ljava/lang/String;)Ljava/io/InputStream;
    //   10: astore_0
    //   11: new 29	java/io/BufferedReader
    //   14: astore_1
    //   15: new 31	java/io/InputStreamReader
    //   18: astore 4
    //   20: getstatic 37	java/nio/charset/StandardCharsets:UTF_8	Ljava/nio/charset/Charset;
    //   23: astore 5
    //   25: aload 4
    //   27: aload_0
    //   28: aload 5
    //   30: invokespecial 40	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    //   33: aload_1
    //   34: aload 4
    //   36: invokespecial 43	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   39: new 45	java/lang/StringBuilder
    //   42: astore_3
    //   43: aload_3
    //   44: invokespecial 46	java/lang/StringBuilder:<init>	()V
    //   47: aload_1
    //   48: invokevirtual 50	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   51: astore 4
    //   53: aload 4
    //   55: ifnull +19 -> 74
    //   58: aload_3
    //   59: aload 4
    //   61: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   64: pop
    //   65: aload_1
    //   66: invokevirtual 50	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   69: astore 4
    //   71: goto -18 -> 53
    //   74: aload_3
    //   75: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   78: astore_2
    //   79: aload_0
    //   80: ifnull +7 -> 87
    //   83: aload_0
    //   84: invokevirtual 62	java/io/InputStream:close	()V
    //   87: aload_1
    //   88: invokevirtual 63	java/io/BufferedReader:close	()V
    //   91: goto +62 -> 153
    //   94: astore_0
    //   95: aload_0
    //   96: invokevirtual 68	java/lang/Exception:printStackTrace	()V
    //   99: goto +54 -> 153
    //   102: astore_3
    //   103: goto +30 -> 133
    //   106: astore_2
    //   107: aconst_null
    //   108: astore_1
    //   109: goto +47 -> 156
    //   112: astore_3
    //   113: aconst_null
    //   114: astore_1
    //   115: goto +18 -> 133
    //   118: astore_2
    //   119: aconst_null
    //   120: astore_0
    //   121: aconst_null
    //   122: astore_1
    //   123: goto +33 -> 156
    //   126: astore_0
    //   127: aconst_null
    //   128: astore_1
    //   129: aload_0
    //   130: astore_3
    //   131: aconst_null
    //   132: astore_0
    //   133: aload_3
    //   134: invokevirtual 68	java/lang/Exception:printStackTrace	()V
    //   137: aload_0
    //   138: ifnull +7 -> 145
    //   141: aload_0
    //   142: invokevirtual 62	java/io/InputStream:close	()V
    //   145: aload_1
    //   146: ifnull +7 -> 153
    //   149: aload_1
    //   150: invokevirtual 63	java/io/BufferedReader:close	()V
    //   153: aload_2
    //   154: areturn
    //   155: astore_2
    //   156: aload_0
    //   157: ifnull +14 -> 171
    //   160: aload_0
    //   161: invokevirtual 62	java/io/InputStream:close	()V
    //   164: goto +7 -> 171
    //   167: astore_0
    //   168: goto +14 -> 182
    //   171: aload_1
    //   172: ifnull +14 -> 186
    //   175: aload_1
    //   176: invokevirtual 63	java/io/BufferedReader:close	()V
    //   179: goto +7 -> 186
    //   182: aload_0
    //   183: invokevirtual 68	java/lang/Exception:printStackTrace	()V
    //   186: aload_2
    //   187: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	188	0	paramClass	Class
    //   0	188	1	paramString	String
    //   2	77	2	str1	String
    //   106	1	2	localObject1	Object
    //   118	36	2	str2	String
    //   155	32	2	localObject2	Object
    //   4	71	3	localStringBuilder	StringBuilder
    //   102	1	3	localException1	Exception
    //   112	1	3	localException2	Exception
    //   130	4	3	localClass	Class
    //   18	52	4	localObject3	Object
    //   23	6	5	localCharset	java.nio.charset.Charset
    // Exception table:
    //   from	to	target	type
    //   83	87	94	java/lang/Exception
    //   87	91	94	java/lang/Exception
    //   141	145	94	java/lang/Exception
    //   149	153	94	java/lang/Exception
    //   39	42	102	java/lang/Exception
    //   43	47	102	java/lang/Exception
    //   47	51	102	java/lang/Exception
    //   59	65	102	java/lang/Exception
    //   65	69	102	java/lang/Exception
    //   74	78	102	java/lang/Exception
    //   11	14	106	finally
    //   15	18	106	finally
    //   20	23	106	finally
    //   28	33	106	finally
    //   34	39	106	finally
    //   11	14	112	java/lang/Exception
    //   15	18	112	java/lang/Exception
    //   20	23	112	java/lang/Exception
    //   28	33	112	java/lang/Exception
    //   34	39	112	java/lang/Exception
    //   6	10	118	finally
    //   6	10	126	java/lang/Exception
    //   39	42	155	finally
    //   43	47	155	finally
    //   47	51	155	finally
    //   59	65	155	finally
    //   65	69	155	finally
    //   74	78	155	finally
    //   133	137	155	finally
    //   160	164	167	java/lang/Exception
    //   175	179	167	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.Helper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */