package com.twelfthmile.malana.compiler.datastructure;

import org.json.JSONArray;

public class RootTrie
  extends Trie
{
  public GrammarTrie grammar;
  public JSONArray grammarJSON;
  public GrammarTrie struct;
  public JSONArray structJSON;
  
  public RootTrie() {}
  
  public RootTrie(JSONArray paramJSONArray1, JSONArray paramJSONArray2)
  {
    grammarJSON = paramJSONArray1;
    structJSON = paramJSONArray2;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.datastructure.RootTrie
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */