package com.twelfthmile.malana.compiler.datastructure;

import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

public class GrammarTrie
  extends Trie
{
  public String grammar_context;
  public String grammar_name;
  public HashMap next;
  public boolean origin = false;
  
  public GrammarTrie()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    next = localHashMap;
    value = false;
    leaf = false;
    child = false;
    grammar_name = "";
  }
  
  private String originPeek(String paramString)
  {
    boolean bool1 = origin;
    if (bool1)
    {
      Iterator localIterator = next.entrySet().iterator();
      Map.Entry localEntry;
      boolean bool3;
      do
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        localEntry = (Map.Entry)localIterator.next();
        HashMap localHashMap = getValuenext;
        bool3 = localHashMap.containsKey(paramString);
      } while (!bool3);
      return (String)localEntry.getKey();
    }
    return null;
  }
  
  public boolean canPeek(GrammarDataObject paramGrammarDataObject)
  {
    String str = label;
    boolean bool1 = canPeek(str);
    if (!bool1)
    {
      paramGrammarDataObject = labelAux;
      boolean bool2 = canPeek(paramGrammarDataObject);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  public boolean canPeek(String paramString)
  {
    boolean bool = child;
    if (bool)
    {
      HashMap localHashMap = next;
      bool = localHashMap.containsKey(paramString);
      if (bool) {}
    }
    else
    {
      paramString = originPeek(paramString);
    }
    return paramString != null;
  }
  
  public boolean canPeekWithoutOrigin(String paramString)
  {
    boolean bool1 = child;
    if (bool1)
    {
      HashMap localHashMap = next;
      boolean bool2 = localHashMap.containsKey(paramString);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public int children()
  {
    boolean bool = child;
    if (!bool) {
      return 0;
    }
    return next.size();
  }
  
  public Set getLeafs()
  {
    TreeSet localTreeSet = new java/util/TreeSet;
    localTreeSet.<init>();
    Iterator localIterator = next.entrySet().iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (Map.Entry)localIterator.next();
      GrammarTrie localGrammarTrie = (GrammarTrie)((Map.Entry)localObject).getValue();
      boolean bool2 = leaf;
      if (bool2)
      {
        localObject = getValuegrammar_name;
        localTreeSet.add(localObject);
      }
      else
      {
        localObject = ((GrammarTrie)((Map.Entry)localObject).getValue()).getLeafs();
        localTreeSet.addAll((Collection)localObject);
      }
    }
    return localTreeSet;
  }
  
  public GrammarTrie getNext(GrammarDataObject paramGrammarDataObject)
  {
    String str = label;
    boolean bool = canPeek(str);
    if (bool) {}
    for (paramGrammarDataObject = label;; paramGrammarDataObject = labelAux) {
      return getNext(paramGrammarDataObject);
    }
  }
  
  public GrammarTrie getNext(String paramString)
  {
    boolean bool = origin;
    if (bool)
    {
      HashMap localHashMap = next;
      String str = originPeek(paramString);
      return (GrammarTrie)getnext.get(paramString);
    }
    return (GrammarTrie)next.get(paramString);
  }
  
  public GrammarTrie getNextWithoutOrigin(String paramString)
  {
    return (GrammarTrie)next.get(paramString);
  }
  
  public Set originGetNext()
  {
    TreeSet localTreeSet = new java/util/TreeSet;
    localTreeSet.<init>();
    boolean bool1 = origin;
    if (bool1)
    {
      Iterator localIterator = next.entrySet().iterator();
      for (;;)
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        Set localSet = nextgetValuenext.keySet();
        localTreeSet.addAll(localSet);
      }
      return localTreeSet;
    }
    return null;
  }
  
  public void putNext(String paramString, GrammarTrie paramGrammarTrie)
  {
    next.put(paramString, paramGrammarTrie);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.datastructure.GrammarTrie
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */