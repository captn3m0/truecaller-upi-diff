package com.twelfthmile.malana.compiler.datastructure;

import java.io.Serializable;

public class Trie
  implements Serializable
{
  public boolean child = false;
  public boolean leaf = false;
  public boolean value = false;
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.datastructure.Trie
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */