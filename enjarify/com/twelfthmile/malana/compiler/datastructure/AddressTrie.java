package com.twelfthmile.malana.compiler.datastructure;

import java.util.ArrayList;
import java.util.HashMap;

public class AddressTrie
  extends Trie
{
  public ArrayList grammarList;
  public HashMap next;
  
  public AddressTrie()
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    next = ((HashMap)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    grammarList = ((ArrayList)localObject);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.datastructure.AddressTrie
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */