package com.twelfthmile.malana.compiler.datastructure;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class TokenTrie
  extends Trie
{
  public int color = 0;
  public HashMap next;
  public String token_name = "";
  public String token_name_aux = "";
  public String token_type_name = "type";
  public String token_type_val = null;
  
  public TokenTrie()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    next = localHashMap;
  }
  
  private TokenTrie getNext(TokenTrie paramTokenTrie)
  {
    Iterator localIterator = next.values().iterator();
    boolean bool1 = localIterator.hasNext();
    if (bool1)
    {
      Object localObject = next.keySet();
      int i = ((Set)localObject).size();
      int j = 1;
      if (i <= j)
      {
        paramTokenTrie = next;
        i = 32;
        localObject = Character.valueOf(i);
        boolean bool2 = paramTokenTrie.containsKey(localObject);
        if (bool2) {}
      }
      else
      {
        return (TokenTrie)localIterator.next();
      }
    }
    return null;
  }
  
  public TokenTrie fastForward()
  {
    int i = color;
    int j = 1;
    if (i != j) {
      return null;
    }
    Object localObject1 = this;
    Object localObject2 = this;
    while (localObject1 != null)
    {
      TokenTrie localTokenTrie = getNext((TokenTrie)localObject1);
      localObject2 = localObject1;
      localObject1 = localTokenTrie;
    }
    localObject1 = token_name;
    String str = "";
    boolean bool = ((String)localObject1).equals(str);
    if (bool) {
      return null;
    }
    return (TokenTrie)localObject2;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.datastructure.TokenTrie
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */