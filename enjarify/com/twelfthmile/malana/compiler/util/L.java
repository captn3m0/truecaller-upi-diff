package com.twelfthmile.malana.compiler.util;

import java.io.PrintStream;

public class L
{
  public static final boolean D_BRANCH = false;
  public static final boolean D_BRANCHCNTRL = false;
  public static final boolean D_CTXSTCK = false;
  public static final boolean D_PARSE = false;
  public static final boolean D_PRINTCOND = false;
  public static final boolean D_PRINTMDT = false;
  public static final boolean D_PRINTMSG = false;
  public static final boolean D_PRINTSCR = false;
  public static final boolean D_PRINTTOKEN = false;
  public static final boolean D_SEMANTIC = false;
  public static final boolean D_STACK = false;
  public static final boolean D_STRUCT = false;
  
  public static void error(Exception paramException)
  {
    paramException.printStackTrace();
  }
  
  public static void msg(Object paramObject)
  {
    System.out.println(paramObject);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.util.L
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */