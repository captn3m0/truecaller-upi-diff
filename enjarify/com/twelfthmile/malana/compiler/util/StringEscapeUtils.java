package com.twelfthmile.malana.compiler.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;

public class StringEscapeUtils
{
  public static String escapeJava(String paramString)
  {
    return escapeJavaStyleString(paramString, false, false);
  }
  
  private static String escapeJavaStyleString(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramString == null) {
      return null;
    }
    try
    {
      StringWriter localStringWriter = new java/io/StringWriter;
      int i = paramString.length() * 2;
      localStringWriter.<init>(i);
      escapeJavaStyleString(localStringWriter, paramString, paramBoolean1, paramBoolean2);
      return localStringWriter.toString();
    }
    catch (IOException paramString)
    {
      RuntimeException localRuntimeException = new java/lang/RuntimeException;
      localRuntimeException.<init>(paramString);
      throw localRuntimeException;
    }
  }
  
  private static void escapeJavaStyleString(Writer paramWriter, String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramWriter != null)
    {
      if (paramString == null) {
        return;
      }
      int i = paramString.length();
      int j = 0;
      while (j < i)
      {
        char c1 = paramString.charAt(j);
        char c2 = '࿿';
        StringBuilder localStringBuilder;
        String str1;
        String str2;
        if (c1 > c2)
        {
          localStringBuilder = new java/lang/StringBuilder;
          str1 = "\\u";
          localStringBuilder.<init>(str1);
          str2 = hex(c1);
          localStringBuilder.append(str2);
          str2 = localStringBuilder.toString();
          paramWriter.write(str2);
        }
        else
        {
          c2 = 'ÿ';
          if (c1 > c2)
          {
            localStringBuilder = new java/lang/StringBuilder;
            str1 = "\\u0";
            localStringBuilder.<init>(str1);
            str2 = hex(c1);
            localStringBuilder.append(str2);
            str2 = localStringBuilder.toString();
            paramWriter.write(str2);
          }
          else
          {
            c2 = '';
            if (c1 > c2)
            {
              localStringBuilder = new java/lang/StringBuilder;
              str1 = "\\u00";
              localStringBuilder.<init>(str1);
              str2 = hex(c1);
              localStringBuilder.append(str2);
              str2 = localStringBuilder.toString();
              paramWriter.write(str2);
            }
            else
            {
              c2 = ' ';
              char c3 = '\\';
              if (c1 < c2)
              {
                switch (c1)
                {
                case '\013': 
                default: 
                  c2 = '\017';
                  if (c1 > c2)
                  {
                    localStringBuilder = new java/lang/StringBuilder;
                    str1 = "\\u00";
                    localStringBuilder.<init>(str1);
                    str2 = hex(c1);
                    localStringBuilder.append(str2);
                    str2 = localStringBuilder.toString();
                    paramWriter.write(str2);
                  }
                  break;
                case '\r': 
                  paramWriter.write(c3);
                  c1 = 'r';
                  paramWriter.write(c1);
                  break;
                case '\f': 
                  paramWriter.write(c3);
                  c1 = 'f';
                  paramWriter.write(c1);
                  break;
                case '\n': 
                  paramWriter.write(c3);
                  c1 = 'n';
                  paramWriter.write(c1);
                  break;
                case '\t': 
                  paramWriter.write(c3);
                  c1 = 't';
                  paramWriter.write(c1);
                  break;
                case '\b': 
                  paramWriter.write(c3);
                  c1 = 'b';
                  paramWriter.write(c1);
                  break;
                }
                localStringBuilder = new java/lang/StringBuilder;
                str1 = "\\u000";
                localStringBuilder.<init>(str1);
                str2 = hex(c1);
                localStringBuilder.append(str2);
                str2 = localStringBuilder.toString();
                paramWriter.write(str2);
              }
              else
              {
                c2 = '"';
                if (c1 != c2)
                {
                  c2 = '\'';
                  if (c1 != c2)
                  {
                    c2 = '/';
                    if (c1 != c2)
                    {
                      if (c1 != c3)
                      {
                        paramWriter.write(c1);
                      }
                      else
                      {
                        paramWriter.write(c3);
                        paramWriter.write(c3);
                      }
                    }
                    else
                    {
                      if (paramBoolean2) {
                        paramWriter.write(c3);
                      }
                      paramWriter.write(c2);
                    }
                  }
                  else
                  {
                    if (paramBoolean1) {
                      paramWriter.write(c3);
                    }
                    paramWriter.write(c2);
                  }
                }
                else
                {
                  paramWriter.write(c3);
                  paramWriter.write(c2);
                }
              }
            }
          }
        }
        j += 1;
      }
      return;
    }
    paramWriter = new java/lang/IllegalArgumentException;
    paramWriter.<init>("The Writer must not be null");
    throw paramWriter;
  }
  
  private static String hex(char paramChar)
  {
    String str = Integer.toHexString(paramChar);
    Locale localLocale = Locale.ENGLISH;
    return str.toUpperCase(localLocale);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.util.StringEscapeUtils
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */