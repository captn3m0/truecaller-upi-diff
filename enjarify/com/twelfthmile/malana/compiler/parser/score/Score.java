package com.twelfthmile.malana.compiler.parser.score;

import com.twelfthmile.malana.compiler.types.Pair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Score
{
  public int auxGrammarBaseSize;
  public HashMap auxMap;
  public ArrayList grammarList;
  
  public Score(int paramInt)
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    auxMap = ((HashMap)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    grammarList = ((ArrayList)localObject);
    auxGrammarBaseSize = paramInt;
  }
  
  public Score(HashMap paramHashMap, ArrayList paramArrayList, int paramInt)
  {
    auxMap = paramHashMap;
    grammarList = paramArrayList;
    auxGrammarBaseSize = paramInt;
  }
  
  public void addAux(String paramString, int paramInt1, int paramInt2)
  {
    int i = auxGrammarBaseSize + -1 - paramInt1;
    paramInt1 = grammarList.size() + -1;
    int j = auxGrammarBaseSize;
    paramInt1 *= j;
    i += paramInt1;
    Object localObject = auxMap;
    paramInt1 = ((HashMap)localObject).containsKey(paramString);
    if (paramInt1 != 0)
    {
      localObject = (ArrayList)auxMap.get(paramString);
    }
    else
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
    }
    Pair localPair = new com/twelfthmile/malana/compiler/types/Pair;
    Integer localInteger1 = Integer.valueOf(i);
    Integer localInteger2 = Integer.valueOf(paramInt2);
    localPair.<init>(localInteger1, localInteger2);
    ((ArrayList)localObject).add(localPair);
    auxMap.put(paramString, localObject);
  }
  
  public void addGrammar(String paramString)
  {
    ArrayList localArrayList = grammarList;
    boolean bool = localArrayList.contains(paramString);
    if (!bool)
    {
      localArrayList = grammarList;
      localArrayList.add(paramString);
    }
  }
  
  public ArrayList getScore(String paramString)
  {
    Object localObject1 = auxMap;
    paramString = (ArrayList)((HashMap)localObject1).get(paramString);
    if (paramString == null) {
      return null;
    }
    localObject1 = new java/util/ArrayList;
    int i = paramString.size();
    ((ArrayList)localObject1).<init>(i);
    paramString = paramString.iterator();
    for (;;)
    {
      boolean bool = paramString.hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (Pair)paramString.next();
      Integer localInteger1 = (Integer)((Pair)localObject2).getA();
      int k = localInteger1.intValue();
      int j = ((Integer)((Pair)localObject2).getB()).intValue();
      Pair localPair = new com/twelfthmile/malana/compiler/types/Pair;
      int m = auxGrammarBaseSize;
      m = k / m;
      Integer localInteger2 = Integer.valueOf(m);
      double d1 = k;
      k = auxGrammarBaseSize;
      double d2 = k;
      Double.isNaN(d1);
      Double.isNaN(d2);
      d1 %= d2;
      double d3 = Math.pow(2.0D, d1);
      double d4 = j;
      Double.isNaN(d4);
      d3 *= d4;
      localObject2 = Double.valueOf(d3);
      localPair.<init>(localInteger2, localObject2);
      ((ArrayList)localObject1).add(localPair);
    }
    return (ArrayList)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.score.Score
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */