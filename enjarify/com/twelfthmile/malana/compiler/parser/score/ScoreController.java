package com.twelfthmile.malana.compiler.parser.score;

import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.types.Pair;
import java.util.ArrayList;
import java.util.Iterator;

public class ScoreController
{
  private static final double minScoreLimit = 6.0D;
  private double highScore;
  private int highScoreIndex;
  private Score score;
  private ArrayList scoreTypes;
  
  public ScoreController(Score paramScore)
  {
    double d = 0.0D;
    highScore = d;
    score = paramScore;
    ArrayList localArrayList1 = new java/util/ArrayList;
    ArrayList localArrayList2 = grammarList;
    int i = localArrayList2.size();
    localArrayList1.<init>(i);
    scoreTypes = localArrayList1;
    int j = 0;
    localArrayList1 = null;
    for (;;)
    {
      localArrayList2 = grammarList;
      i = localArrayList2.size();
      if (j >= i) {
        break;
      }
      localArrayList2 = scoreTypes;
      ScoreType localScoreType = new com/twelfthmile/malana/compiler/parser/score/ScoreType;
      Double localDouble = Double.valueOf(d);
      localScoreType.<init>(localDouble);
      localArrayList2.add(localScoreType);
      j += 1;
    }
  }
  
  private void updateScore(int paramInt, double paramDouble)
  {
    ScoreType localScoreType = (ScoreType)scoreTypes.get(paramInt);
    Double localDouble1 = grammarScore;
    double d1 = localDouble1.doubleValue();
    Double localDouble2 = Double.valueOf(paramDouble + d1);
    grammarScore = localDouble2;
    scoreTypes.set(paramInt, localScoreType);
    localDouble2 = grammarScore;
    paramDouble = localDouble2.doubleValue();
    d1 = highScore;
    boolean bool = paramDouble < d1;
    if (bool)
    {
      highScoreIndex = paramInt;
      Double localDouble3 = grammarScore;
      double d2 = localDouble3.doubleValue();
      highScore = d2;
    }
  }
  
  public String getCategory()
  {
    double d1 = highScore;
    double d2 = 6.0D;
    boolean bool = d1 < d2;
    if (!bool)
    {
      Object localObject = score.grammarList;
      int i = highScoreIndex;
      localObject = (String)((ArrayList)localObject).get(i);
      return overrideCategory((String)localObject);
    }
    return null;
  }
  
  public String overrideCategory(String paramString)
  {
    String str1 = "GRM_OFFERS";
    boolean bool1 = str1.equals(paramString);
    if (bool1)
    {
      double d1 = Double.MIN_VALUE;
      Iterator localIterator = score.grammarList.iterator();
      int i = 0;
      for (;;)
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        String str2 = (String)localIterator.next();
        Object localObject = "GRM_OFFERS";
        boolean bool3 = str2.equals(localObject);
        if (!bool3)
        {
          localObject = scoreTypes.get(i)).grammarScore;
          double d2 = ((Double)localObject).doubleValue();
          double d3 = 6.0D;
          boolean bool4 = d2 < d3;
          if (!bool4)
          {
            localObject = scoreTypes.get(i)).grammarScore;
            d2 = ((Double)localObject).doubleValue();
            boolean bool5 = d2 < d1;
            if (bool5)
            {
              d1 = scoreTypes.get(i)).grammarScore.doubleValue();
              paramString = str2;
            }
          }
        }
        i += 1;
      }
    }
    return paramString;
  }
  
  public String scorePrint()
  {
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    localStringBuilder1.<init>("");
    Iterator localIterator = score.grammarList.iterator();
    localStringBuilder1.append("{");
    int i = 0;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (String)localIterator.next();
      StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
      String str = "\"";
      localStringBuilder2.<init>(str);
      localStringBuilder2.append((String)localObject);
      localStringBuilder2.append("\" : \"");
      localObject = scoreTypes.get(i)).grammarScore;
      localStringBuilder2.append(localObject);
      localStringBuilder2.append("\"");
      localObject = localStringBuilder2.toString();
      localStringBuilder1.append((String)localObject);
      bool = localIterator.hasNext();
      if (bool)
      {
        localObject = ",";
        localStringBuilder1.append((String)localObject);
      }
      i += 1;
    }
    localStringBuilder1.append("}");
    return localStringBuilder1.toString();
  }
  
  public void updateScore(GrammarDataObject paramGrammarDataObject)
  {
    Object localObject1 = score;
    paramGrammarDataObject = label;
    paramGrammarDataObject = ((Score)localObject1).getScore(paramGrammarDataObject).iterator();
    for (;;)
    {
      boolean bool = paramGrammarDataObject.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (Pair)paramGrammarDataObject.next();
      Object localObject2 = score.grammarList;
      Integer localInteger = (Integer)((Pair)localObject1).getA();
      int i = localInteger.intValue();
      ((ArrayList)localObject2).get(i);
      localObject2 = (Integer)((Pair)localObject1).getA();
      int j = ((Integer)localObject2).intValue();
      localObject1 = (Double)((Pair)localObject1).getB();
      double d = ((Double)localObject1).doubleValue();
      updateScore(j, d);
    }
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.score.ScoreController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */