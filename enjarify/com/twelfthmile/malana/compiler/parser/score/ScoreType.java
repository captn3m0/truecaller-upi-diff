package com.twelfthmile.malana.compiler.parser.score;

public class ScoreType
{
  public Double grammarScore;
  
  public ScoreType(Double paramDouble)
  {
    grammarScore = paramDouble;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.score.ScoreType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */