package com.twelfthmile.malana.compiler.parser;

import com.twelfthmile.b.a.a;
import com.twelfthmile.malana.compiler.datastructure.GrammarTrie;
import com.twelfthmile.malana.compiler.parser.branch.BranchController;
import com.twelfthmile.malana.compiler.parser.branch.BranchDebug;
import com.twelfthmile.malana.compiler.parser.branch.BranchRules;
import com.twelfthmile.malana.compiler.parser.context.ContextController;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataLinkedListObject;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.parser.score.Score;
import com.twelfthmile.malana.compiler.parser.score.ScoreController;
import com.twelfthmile.malana.compiler.parser.struct.StructController;
import com.twelfthmile.malana.compiler.types.Finalise;
import com.twelfthmile.malana.compiler.types.Pair;
import com.twelfthmile.malana.compiler.types.ParserListener;
import com.twelfthmile.malana.compiler.types.Req;
import com.twelfthmile.malana.compiler.types.Response;
import com.twelfthmile.malana.compiler.types.SemanticSeed;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Parser
  implements ParserListener
{
  public static final int DEL_CHECK_ALL = 0;
  public static final int DEL_CHECK_STRCT = 2;
  public static final int DEL_CHECK_WORD = 1;
  private BranchController branchController;
  private ContextController contextController;
  private GrammarDataLinkedListObject gdoListRoot;
  private ScoreController scoreController;
  private StructController structController;
  
  public Parser(GrammarTrie paramGrammarTrie1, GrammarTrie paramGrammarTrie2, Score paramScore, SemanticSeed paramSemanticSeed)
  {
    Object localObject = new com/twelfthmile/malana/compiler/parser/score/ScoreController;
    ((ScoreController)localObject).<init>(paramScore);
    scoreController = ((ScoreController)localObject);
    paramScore = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataLinkedListObject;
    paramScore.<init>(null, null);
    gdoListRoot = paramScore;
    paramScore = new com/twelfthmile/malana/compiler/parser/context/ContextController;
    localObject = gdoListRoot;
    paramScore.<init>(paramSemanticSeed, (GrammarDataLinkedListObject)localObject);
    contextController = paramScore;
    paramScore = new com/twelfthmile/malana/compiler/parser/struct/StructController;
    paramScore.<init>(paramGrammarTrie2, this);
    structController = paramScore;
    paramGrammarTrie2 = new com/twelfthmile/malana/compiler/parser/branch/BranchController;
    paramScore = gdoListRoot;
    paramGrammarTrie2.<init>(paramGrammarTrie1, paramScore, this);
    branchController = paramGrammarTrie2;
  }
  
  private boolean grammarExclusionCheck(String paramString, List paramList)
  {
    paramList = paramList.iterator();
    boolean bool;
    do
    {
      bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramList.next();
      bool = str.equals(paramString);
    } while (!bool);
    return true;
    return false;
  }
  
  public void add(GrammarDataObject paramGrammarDataObject)
  {
    contextController.add(paramGrammarDataObject);
    branchController.add(paramGrammarDataObject);
  }
  
  public void condensation(GrammarDataObject paramGrammarDataObject)
  {
    contextController.add(paramGrammarDataObject);
  }
  
  public List contextIndexList()
  {
    return contextController.getIndexList();
  }
  
  public void end(int paramInt)
  {
    contextController.end(paramInt);
    branchController.end();
  }
  
  public String grammarOutput(String paramString1, String paramString2, String paramString3, a parama, List paramList)
  {
    end(-1);
    int i = 1;
    int j;
    if (paramList != null) {
      j = 1;
    } else {
      j = 0;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    localStringBuilder1.<init>("[");
    StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
    localStringBuilder2.<init>("[");
    for (GrammarDataLinkedListObject localGrammarDataLinkedListObject = gdoListRoot.next; localGrammarDataLinkedListObject != null; localGrammarDataLinkedListObject = next)
    {
      Object localObject;
      boolean bool1;
      if (j == 0)
      {
        localObject = grammarDataObject;
        bool1 = lock;
        if (bool1) {}
      }
      else
      {
        if (j != 0)
        {
          localObject = grammarDataObject;
          bool1 = condensed;
          if (bool1)
          {
            localObject = grammarDataObject.label;
            bool1 = grammarExclusionCheck((String)localObject, paramList);
            if (!bool1) {
              break label243;
            }
          }
        }
        localObject = grammarDataObject;
        bool1 = ((GrammarDataObject)localObject).hasChildren();
        if (bool1)
        {
          localObject = grammarDataObject.children;
          localArrayList.addAll((Collection)localObject);
        }
        localObject = grammarDataObject;
        bool1 = localArrayList.contains(localObject);
        if (!bool1)
        {
          localStringBuilder1.append("\"");
          localObject = grammarDataObject.printLabel();
          localStringBuilder1.append((String)localObject);
          localObject = "\",";
          localStringBuilder1.append((String)localObject);
        }
        label243:
        localObject = grammarDataObject;
        bool1 = condensed;
        if (bool1)
        {
          localObject = grammarDataObject.printLabel();
          localStringBuilder2.append((String)localObject);
          localStringBuilder2.append("->");
          localObject = grammarDataObject.children.iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject).hasNext();
            if (!bool2) {
              break;
            }
            String str1 = ((GrammarDataObject)((Iterator)localObject).next()).printLabel();
            localStringBuilder2.append(str1);
            str1 = "|";
            localStringBuilder2.append(str1);
          }
          int k = localStringBuilder2.toString().length() - i;
          localStringBuilder2.setLength(k);
          localObject = ",";
          localStringBuilder2.append((String)localObject);
        }
      }
    }
    paramList = localStringBuilder1.toString();
    int m = paramList.length();
    if (m > i)
    {
      m -= i;
      localStringBuilder1.setLength(m);
    }
    localStringBuilder1.append("]");
    paramList = localStringBuilder2.toString();
    m = paramList.length();
    if (m > i)
    {
      m -= i;
      localStringBuilder2.setLength(m);
    }
    localStringBuilder2.append("]");
    paramList = new java/lang/StringBuilder;
    paramList.<init>("{\"tokens\":");
    String str2 = localStringBuilder1.toString();
    paramList.append(str2);
    paramList.append(",\"grammars\":");
    str2 = localStringBuilder2.toString();
    paramList.append(str2);
    str2 = ",\"category\":\"";
    paramList.append(str2);
    if (paramString3 == null) {
      paramString3 = scoreController.getCategory();
    }
    paramList.append(paramString3);
    paramList.append("\",\"message\":\"");
    paramString3 = "[\"'\r\n]";
    str2 = "";
    paramString1 = paramString1.replaceAll(paramString3, str2);
    paramList.append(paramString1);
    paramList.append("\",\"address\":\"");
    paramList.append(paramString2);
    paramString1 = "\",\"values\":";
    paramList.append(paramString1);
    if (parama != null) {
      paramString1 = BranchDebug.valuePrint(parama);
    } else {
      paramString1 = "{}";
    }
    paramList.append(paramString1);
    paramList.append("}");
    return paramList.toString();
  }
  
  public boolean grammarPresent(String paramString, String... paramVarArgs)
  {
    int i = -1;
    end(i);
    for (GrammarDataLinkedListObject localGrammarDataLinkedListObject = gdoListRoot.next;; localGrammarDataLinkedListObject = next)
    {
      int j = 0;
      if (localGrammarDataLinkedListObject == null) {
        break;
      }
      Object localObject1 = grammarDataObject;
      boolean bool1 = condensed;
      if (bool1)
      {
        localObject1 = grammarDataObject.label;
        bool1 = ((String)localObject1).equalsIgnoreCase(paramString);
        if (bool1)
        {
          localObject1 = grammarDataObject;
          bool1 = ((GrammarDataObject)localObject1).hasChildren();
          if (bool1)
          {
            localObject1 = grammarDataObject.children.iterator();
            for (;;)
            {
              boolean bool2 = ((Iterator)localObject1).hasNext();
              if (!bool2) {
                break;
              }
              Object localObject2 = (GrammarDataObject)((Iterator)localObject1).next();
              int m = paramVarArgs.length;
              if (j >= m) {
                break;
              }
              localObject2 = label;
              String str = paramVarArgs[j];
              bool2 = ((String)localObject2).equalsIgnoreCase(str);
              if (!bool2) {
                break;
              }
              j += 1;
            }
            localObject1 = grammarDataObject.children;
            int k = ((List)localObject1).size();
            if (j == k) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  public boolean nextTokens(GrammarDataObject paramGrammarDataObject)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localArrayList.add(paramGrammarDataObject);
    return nextTokens(localArrayList);
  }
  
  public boolean nextTokens(ArrayList paramArrayList)
  {
    return branchController.nextTokenCheck(paramArrayList);
  }
  
  public Response output(Req paramReq)
  {
    int i = -1;
    end(i);
    Object localObject1 = new com/twelfthmile/malana/compiler/types/Finalise;
    Object localObject2 = scoreController.getCategory();
    ((Finalise)localObject1).<init>((String)localObject2, paramReq);
    localObject2 = BranchRules.valueOverrideBeforeFinalise(gdoListRoot, (Finalise)localObject1);
    Object localObject3 = ((Finalise)localObject1).getCategory();
    if (localObject3 != null)
    {
      localObject3 = ((Finalise)localObject1).getCategory();
      Object localObject4 = "GRM_VOID";
      boolean bool = ((String)localObject3).equals(localObject4);
      if (!bool)
      {
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        localObject3 = structController;
        localObject4 = gdoListRoot;
        ((StructController)localObject3).finalise(paramReq, (ArrayList)localObject2, (Finalise)localObject1, (GrammarDataLinkedListObject)localObject4);
        paramReq = ((Finalise)localObject1).getValueMap();
        localObject3 = structController.getValues();
        BranchRules.addValues(paramReq, (a)localObject3);
        BranchRules.valueOverrideAfterFinalise((Finalise)localObject1, (ArrayList)localObject2);
        paramReq = new com/twelfthmile/malana/compiler/types/Response;
        paramReq.<init>();
        localObject3 = ((Finalise)localObject1).getCategory();
        if (localObject3 != null)
        {
          localObject3 = ((Finalise)localObject1).getCategory();
          paramReq.setCategory((String)localObject3);
        }
        localObject2 = ((ArrayList)localObject2).toString();
        paramReq.setNodeList((String)localObject2);
        localObject1 = ((Finalise)localObject1).getValueMap();
        paramReq.setValMap((a)localObject1);
        return paramReq;
      }
    }
    paramReq = new com/twelfthmile/malana/compiler/types/Response;
    paramReq.<init>();
    paramReq.setValMap((a)localObject2);
    return paramReq;
  }
  
  public GrammarDataObject prevGDO(int paramInt)
  {
    return prevGDO(paramInt, false);
  }
  
  public GrammarDataObject prevGDO(int paramInt, boolean paramBoolean)
  {
    Object localObject = contextController;
    if (localObject != null)
    {
      localObject = ((ContextController)localObject).getGdoList();
      int i = ((List)localObject).size();
      if (i > paramInt)
      {
        localObject = contextController.getGdoList();
        List localList = contextController.getGdoList();
        int j = localList.size() + -1 - paramInt;
        GrammarDataObject localGrammarDataObject = (GrammarDataObject)((List)localObject).get(j);
        if (paramBoolean)
        {
          paramBoolean = localGrammarDataObject.hasChildren();
          if (paramBoolean) {
            return localGrammarDataObject.getLastDescendant();
          }
        }
        return localGrammarDataObject;
      }
    }
    return null;
  }
  
  public String prevTokens(int paramInt)
  {
    return prevTokens(paramInt, false);
  }
  
  public String prevTokens(int paramInt, boolean paramBoolean)
  {
    GrammarDataObject localGrammarDataObject = prevGDO(paramInt, paramBoolean);
    if (localGrammarDataObject != null) {
      return label;
    }
    return "null";
  }
  
  public void score(GrammarDataObject paramGrammarDataObject)
  {
    scoreController.updateScore(paramGrammarDataObject);
  }
  
  public String semantics()
  {
    end(-1);
    return "";
  }
  
  public boolean shouldCondense(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2, GrammarDataObject paramGrammarDataObject3)
  {
    return contextController.shouldCondense(paramGrammarDataObject1, paramGrammarDataObject2, paramGrammarDataObject3);
  }
  
  public Pair shouldEnd(String paramString, int paramInt1, int paramInt2)
  {
    int i = 2;
    if (paramInt2 == i)
    {
      Object localObject1 = contextController;
      boolean bool = ((ContextController)localObject1).shouldPopH(paramString);
      paramInt1 = -1;
      if (bool)
      {
        paramString = new com/twelfthmile/malana/compiler/types/Pair;
        localObject2 = Boolean.TRUE;
        localObject1 = Integer.valueOf(paramInt1);
        paramString.<init>(localObject2, localObject1);
        return paramString;
      }
      paramString = new com/twelfthmile/malana/compiler/types/Pair;
      localObject2 = Boolean.FALSE;
      localObject1 = Integer.valueOf(paramInt1);
      paramString.<init>(localObject2, localObject1);
      return paramString;
    }
    i = 0;
    String str = null;
    if (paramInt2 == 0)
    {
      localObject2 = contextController;
      str = paramString.substring(0, paramInt1);
      paramInt2 = ((ContextController)localObject2).shouldPop(str);
      if (paramInt2 == 0) {
        break label169;
      }
    }
    else
    {
      localObject2 = contextController;
      str = paramString.substring(0, paramInt1);
      paramInt2 = ((ContextController)localObject2).shouldPopWord(str);
      if (paramInt2 == 0) {
        break label169;
      }
    }
    Object localObject2 = contextController;
    paramInt1 += 1;
    paramString = ((ContextController)localObject2).isNotEllipsis(paramString, paramInt1);
    if (paramString != null) {
      return paramString;
    }
    label169:
    return null;
  }
  
  public String tokens(List paramList)
  {
    end(-1);
    int i = 1;
    int j;
    if (paramList != null) {
      j = 1;
    } else {
      j = 0;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[");
    for (GrammarDataLinkedListObject localGrammarDataLinkedListObject = gdoListRoot.next; localGrammarDataLinkedListObject != null; localGrammarDataLinkedListObject = next)
    {
      Object localObject;
      boolean bool;
      if (j == 0)
      {
        localObject = grammarDataObject;
        bool = lock;
        if (bool) {}
      }
      else
      {
        if (j != 0)
        {
          localObject = grammarDataObject;
          bool = condensed;
          if (bool)
          {
            localObject = grammarDataObject.label;
            bool = grammarExclusionCheck((String)localObject, paramList);
            if (!bool) {
              continue;
            }
          }
        }
        localObject = grammarDataObject;
        bool = ((GrammarDataObject)localObject).hasChildren();
        if (bool)
        {
          localObject = grammarDataObject.children;
          localArrayList.addAll((Collection)localObject);
        }
        localObject = grammarDataObject;
        bool = localArrayList.contains(localObject);
        if (!bool)
        {
          localObject = grammarDataObject.printJSON();
          localStringBuilder.append((String)localObject);
          localObject = ",";
          localStringBuilder.append((String)localObject);
        }
      }
    }
    paramList = localStringBuilder.toString();
    int k = paramList.length();
    if (k > i)
    {
      k -= i;
      localStringBuilder.setLength(k);
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  public String yugaTokens(List paramList)
  {
    int i = -1;
    end(i);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[");
    for (GrammarDataLinkedListObject localGrammarDataLinkedListObject = gdoListRoot.next; localGrammarDataLinkedListObject != null; localGrammarDataLinkedListObject = next)
    {
      Object localObject = grammarDataObject;
      boolean bool = classified;
      if (bool)
      {
        localObject = grammarDataObject;
        bool = localArrayList.contains(localObject);
        if (!bool)
        {
          localStringBuilder.append("\"");
          localObject = grammarDataObject.label;
          localStringBuilder.append((String)localObject);
          localObject = "\",";
          localStringBuilder.append((String)localObject);
          continue;
        }
      }
      localObject = grammarDataObject;
      bool = condensed;
      if (bool)
      {
        if (paramList != null)
        {
          localObject = grammarDataObject.label;
          bool = grammarExclusionCheck((String)localObject, paramList);
          if (!bool) {}
        }
      }
      else
      {
        localObject = grammarDataObject;
        bool = ((GrammarDataObject)localObject).hasChildren();
        if (bool)
        {
          localObject = grammarDataObject.children;
          localArrayList.addAll((Collection)localObject);
        }
        localObject = grammarDataObject;
        bool = localArrayList.contains(localObject);
        if (!bool)
        {
          localObject = grammarDataObject;
          bool = condensed;
          if (bool)
          {
            localStringBuilder.append("\"");
            localObject = grammarDataObject.label;
            localStringBuilder.append((String)localObject);
            localObject = "\",";
            localStringBuilder.append((String)localObject);
          }
          else
          {
            localStringBuilder.append("\"");
            localObject = grammarDataObject.str;
            localStringBuilder.append((String)localObject);
            localObject = "\",";
            localStringBuilder.append((String)localObject);
          }
        }
      }
    }
    paramList = localStringBuilder.toString();
    int j = paramList.length();
    i = 1;
    if (j > i)
    {
      j -= i;
      localStringBuilder.setLength(j);
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.Parser
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */