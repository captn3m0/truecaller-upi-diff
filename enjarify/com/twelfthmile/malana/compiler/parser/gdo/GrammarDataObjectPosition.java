package com.twelfthmile.malana.compiler.parser.gdo;

import com.twelfthmile.malana.compiler.types.GDOPos;
import com.twelfthmile.malana.compiler.types.Pair;

public class GrammarDataObjectPosition
  implements GDOPos
{
  private GrammarDataLinkedListObject gdoListRoot;
  
  public GrammarDataObjectPosition(GrammarDataLinkedListObject paramGrammarDataLinkedListObject)
  {
    gdoListRoot = paramGrammarDataLinkedListObject;
  }
  
  public Pair getGDOsAfter(GrammarDataObject paramGrammarDataObject)
  {
    paramGrammarDataObject = new com/twelfthmile/malana/compiler/types/Pair;
    GrammarDataObjectList localGrammarDataObjectList1 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectList;
    localGrammarDataObjectList1.<init>();
    GrammarDataObjectList localGrammarDataObjectList2 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectList;
    localGrammarDataObjectList2.<init>();
    paramGrammarDataObject.<init>(localGrammarDataObjectList1, localGrammarDataObjectList2);
    return paramGrammarDataObject;
  }
  
  public Pair getGDOsBefore(GrammarDataObject paramGrammarDataObject)
  {
    GrammarDataObjectList localGrammarDataObjectList1 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectList;
    localGrammarDataObjectList1.<init>();
    GrammarDataObjectList localGrammarDataObjectList2 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectList;
    localGrammarDataObjectList2.<init>();
    for (GrammarDataLinkedListObject localGrammarDataLinkedListObject = gdoListRoot.next; localGrammarDataLinkedListObject != null; localGrammarDataLinkedListObject = next)
    {
      GrammarDataObject localGrammarDataObject = grammarDataObject;
      if (localGrammarDataObject == paramGrammarDataObject) {
        break;
      }
      localGrammarDataObject = grammarDataObject;
      if (localGrammarDataObject != null)
      {
        localGrammarDataObject = grammarDataObject;
        boolean bool = lock;
        if (!bool)
        {
          localGrammarDataObject = grammarDataObject;
          localGrammarDataObjectList1.add(localGrammarDataObject);
        }
        localGrammarDataObject = grammarDataObject;
        localGrammarDataObjectList2.add(localGrammarDataObject);
      }
    }
    paramGrammarDataObject = new com/twelfthmile/malana/compiler/types/Pair;
    paramGrammarDataObject.<init>(localGrammarDataObjectList1, localGrammarDataObjectList2);
    return paramGrammarDataObject;
  }
  
  public Pair getGDOsBetween(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2)
  {
    GrammarDataObjectList localGrammarDataObjectList1 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectList;
    localGrammarDataObjectList1.<init>();
    GrammarDataObjectList localGrammarDataObjectList2 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectList;
    localGrammarDataObjectList2.<init>();
    GrammarDataLinkedListObject localGrammarDataLinkedListObject = gdoListRoot.next;
    int i = 0;
    int j = 0;
    while (localGrammarDataLinkedListObject != null)
    {
      GrammarDataObject localGrammarDataObject = grammarDataObject;
      if (localGrammarDataObject == paramGrammarDataObject2) {
        break;
      }
      localGrammarDataObject = grammarDataObject;
      if (localGrammarDataObject != null)
      {
        localGrammarDataObject = grammarDataObject;
        boolean bool = lock;
        if (!bool)
        {
          if (i != 0)
          {
            localGrammarDataObject = grammarDataObject;
            localGrammarDataObjectList1.add(localGrammarDataObject);
          }
          localGrammarDataObject = grammarDataObject;
          if (localGrammarDataObject == paramGrammarDataObject1) {
            i = 1;
          }
        }
        if (j != 0)
        {
          localGrammarDataObject = grammarDataObject;
          localGrammarDataObjectList2.add(localGrammarDataObject);
        }
        localGrammarDataObject = grammarDataObject;
        if (localGrammarDataObject == paramGrammarDataObject1) {
          j = 1;
        }
      }
      localGrammarDataLinkedListObject = next;
    }
    paramGrammarDataObject1 = new com/twelfthmile/malana/compiler/types/Pair;
    paramGrammarDataObject1.<init>(localGrammarDataObjectList1, localGrammarDataObjectList2);
    return paramGrammarDataObject1;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObjectPosition
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */