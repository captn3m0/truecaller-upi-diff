package com.twelfthmile.malana.compiler.parser.gdo;

import com.twelfthmile.malana.compiler.types.Pair;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class GrammarDataObjectList
{
  private ArrayList gdoList;
  
  public GrammarDataObjectList()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    gdoList = localArrayList;
  }
  
  public void add(GrammarDataObject paramGrammarDataObject)
  {
    gdoList.add(paramGrammarDataObject);
  }
  
  public boolean containsGdo(GrammarDataObject paramGrammarDataObject)
  {
    Iterator localIterator = gdoList.iterator();
    boolean bool;
    do
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      GrammarDataObject localGrammarDataObject = (GrammarDataObject)localIterator.next();
      bool = localGrammarDataObject.equals(paramGrammarDataObject);
    } while (!bool);
    return true;
    return false;
  }
  
  public boolean containsStr(String paramString)
  {
    Iterator localIterator = gdoList.iterator();
    boolean bool;
    do
    {
      String str;
      do
      {
        bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject = (GrammarDataObject)localIterator.next();
        str = str;
      } while (str == null);
      Object localObject = str.toLowerCase();
      bool = paramString.equals(localObject);
    } while (!bool);
    return true;
    return false;
  }
  
  public boolean containsToken(String paramString)
  {
    Iterator localIterator = gdoList.iterator();
    boolean bool;
    do
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str = nextlabel;
      bool = paramString.equals(str);
    } while (!bool);
    return true;
    return false;
  }
  
  public List getGdoList()
  {
    return gdoList;
  }
  
  public String getLastToken()
  {
    ArrayList localArrayList = gdoList;
    int i = localArrayList.size();
    if (i == 0) {
      return null;
    }
    localArrayList = gdoList;
    int j = localArrayList.size() + -1;
    return getlabel;
  }
  
  public Pair match(String paramString)
  {
    String str1 = " ";
    paramString = Arrays.asList(paramString.split(str1));
    int i = paramString.size();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("");
    int j = 0;
    Object localObject1 = null;
    int k = 0;
    Object localObject2 = null;
    int m = 0;
    int n = 0;
    for (;;)
    {
      Object localObject3 = gdoList;
      int i1 = ((ArrayList)localObject3).size();
      if (j >= i1) {
        break;
      }
      localObject3 = gdoList.get(j)).label;
      if (n >= i) {
        return null;
      }
      if (localObject1 != null)
      {
        boolean bool2;
        if (localObject2 != null)
        {
          bool2 = ((String)localObject3).equals(localObject2);
          if (!bool2)
          {
            str2 = gdoList.get(j)).str;
            localStringBuilder.append(str2);
            str2 = " ";
            localStringBuilder.append(str2);
          }
          else
          {
            paramString = new com/twelfthmile/malana/compiler/types/Pair;
            str1 = localStringBuilder.toString();
            paramString.<init>(localObject1, str1);
            return paramString;
          }
        }
        else
        {
          str2 = "GDO_NONDET";
          bool2 = ((String)localObject3).equals(str2);
          if (bool2)
          {
            str2 = gdoList.get(j)).str;
            localStringBuilder.append(str2);
            str2 = " ";
            localStringBuilder.append(str2);
          }
          else
          {
            paramString = new com/twelfthmile/malana/compiler/types/Pair;
            str1 = localStringBuilder.toString();
            paramString.<init>(localObject1, str1);
            return paramString;
          }
        }
      }
      String str2 = (String)paramString.get(n);
      String str3 = "!";
      boolean bool3 = str2.startsWith(str3);
      int i3 = 1;
      if (bool3)
      {
        str3 = gdoList.get(j)).str;
        int i4 = str2.length();
        String str4 = str2.substring(i3, i4);
        bool3 = str3.equals(str4);
        if (bool3)
        {
          n += 1;
          break label366;
        }
      }
      boolean bool1 = ((String)localObject3).equals(str2);
      if (bool1) {
        n += 1;
      }
      label366:
      localObject3 = (String)paramString.get(n);
      str2 = "#";
      bool1 = ((String)localObject3).startsWith(str2);
      if (bool1)
      {
        localObject1 = (String)paramString.get(n);
        k = ((String)localObject1).length();
        localObject1 = ((String)localObject1).substring(i3, k);
        k = n + 1;
        int i2 = paramString.size();
        if (k < i2)
        {
          localObject2 = (String)paramString.get(k);
        }
        else
        {
          k = 0;
          localObject2 = null;
          m = 1;
        }
      }
      j += 1;
    }
    if (m != 0)
    {
      paramString = new com/twelfthmile/malana/compiler/types/Pair;
      str1 = localStringBuilder.toString();
      paramString.<init>(localObject1, str1);
      return paramString;
    }
    return null;
  }
  
  public String print()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("");
    Iterator localIterator = gdoList.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str = ((GrammarDataObject)localIterator.next()).print();
      localStringBuilder.append(str);
      str = "\n";
      localStringBuilder.append(str);
    }
    return localStringBuilder.toString();
  }
  
  public int size()
  {
    return gdoList.size();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObjectList
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */