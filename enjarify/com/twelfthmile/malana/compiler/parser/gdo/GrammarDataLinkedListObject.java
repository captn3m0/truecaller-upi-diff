package com.twelfthmile.malana.compiler.parser.gdo;

public class GrammarDataLinkedListObject
{
  public GrammarDataObject grammarDataObject;
  public GrammarDataLinkedListObject next;
  
  public GrammarDataLinkedListObject(GrammarDataObject paramGrammarDataObject, GrammarDataLinkedListObject paramGrammarDataLinkedListObject)
  {
    grammarDataObject = paramGrammarDataObject;
    next = paramGrammarDataLinkedListObject;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.gdo.GrammarDataLinkedListObject
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */