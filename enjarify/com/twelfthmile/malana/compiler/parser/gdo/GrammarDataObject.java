package com.twelfthmile.malana.compiler.parser.gdo;

import com.twelfthmile.b.a.a;
import com.twelfthmile.malana.compiler.types.ValueMap;
import com.twelfthmile.malana.compiler.util.StringEscapeUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class GrammarDataObject
{
  public static int SC_CAMEL = 2;
  public static int SC_CAPS = 1;
  public static int SC_EML = 5;
  public static int SC_FCSC = 3;
  public static int SC_FCSN = 4;
  public static int SC_NORM = 0;
  public static int SC_VPD = 6;
  public List children;
  public boolean classified;
  public boolean condensed;
  public int index = -1;
  public List infectedChildren;
  public String label;
  public String labelAux;
  public boolean lock;
  public boolean mature;
  public GrammarDataObject parent;
  public String str;
  public int strType;
  public a values;
  
  public GrammarDataObject()
  {
    int i = SC_NORM;
    strType = i;
    lock = false;
    mature = false;
    condensed = false;
    classified = false;
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    children = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    infectedChildren = ((List)localObject);
    localObject = new com/twelfthmile/malana/compiler/types/ValueMap;
    ((ValueMap)localObject).<init>();
    values = ((a)localObject);
  }
  
  public GrammarDataObject(String paramString1, String paramString2, int paramInt, String... paramVarArgs)
  {
    this(paramString1, paramString1, paramString2, paramInt, paramVarArgs);
  }
  
  public GrammarDataObject(String paramString1, String paramString2, String paramString3, int paramInt, String... paramVarArgs)
  {
    int i = SC_NORM;
    strType = i;
    i = 0;
    lock = false;
    mature = false;
    condensed = false;
    classified = false;
    label = paramString1;
    labelAux = paramString2;
    paramString1 = StringEscapeUtils.escapeJava(paramString3);
    paramString2 = "\\u";
    boolean bool = paramString1.startsWith(paramString2);
    if (bool) {
      paramString1 = paramString3;
    }
    str = paramString1;
    index = paramInt;
    paramString1 = new com/twelfthmile/malana/compiler/types/ValueMap;
    paramString1.<init>();
    values = paramString1;
    if (paramVarArgs != null)
    {
      int k = paramVarArgs.length;
      int j = 2;
      if (k >= j)
      {
        k = paramVarArgs.length % j;
        if (k == 0) {
          for (;;)
          {
            k = paramVarArgs.length;
            if (i >= k) {
              break;
            }
            k = i + 1;
            paramString2 = paramVarArgs[k];
            if (paramString2 != null)
            {
              paramString2 = values;
              paramString3 = paramVarArgs[i];
              paramString1 = paramVarArgs[k];
              paramString2.put(paramString3, paramString1);
            }
            i += 2;
          }
        }
        paramString1 = new java/lang/RuntimeException;
        paramString1.<init>("Please pass both Key and Value while instantiating GDO");
        throw paramString1;
      }
    }
  }
  
  private String printValues()
  {
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    localStringBuilder1.<init>("{");
    Object localObject1 = values;
    if (localObject1 != null)
    {
      localObject1 = ((a)localObject1).entrySet().iterator();
      for (;;)
      {
        i = ((Iterator)localObject1).hasNext();
        if (i == 0) {
          break;
        }
        Object localObject2 = (Map.Entry)((Iterator)localObject1).next();
        StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
        localStringBuilder2.<init>("\"");
        String str1 = (String)((Map.Entry)localObject2).getKey();
        localStringBuilder2.append(str1);
        str1 = "\":\"";
        localStringBuilder2.append(str1);
        localObject2 = StringEscapeUtils.escapeJava((String)((Map.Entry)localObject2).getValue());
        localStringBuilder2.append((String)localObject2);
        localStringBuilder2.append("\"");
        localObject2 = localStringBuilder2.toString();
        localStringBuilder1.append((String)localObject2);
        localObject2 = ",";
        localStringBuilder1.append((String)localObject2);
      }
    }
    int j = localStringBuilder1.length();
    int i = 1;
    if (j > i)
    {
      int k = localStringBuilder1.length() - i;
      localStringBuilder1.setLength(k);
    }
    localStringBuilder1.append("}");
    return localStringBuilder1.toString();
  }
  
  public void addValues(String paramString1, String paramString2)
  {
    if (paramString2 != null)
    {
      a locala = values;
      locala.put(paramString1, paramString2);
    }
  }
  
  public void addValues(HashMap paramHashMap)
  {
    values.putAll(paramHashMap);
  }
  
  public boolean allCaps()
  {
    String str1 = str;
    if (str1 != null)
    {
      int i = str1.length();
      if (i > 0)
      {
        str1 = str;
        boolean bool = Character.isUpperCase(str1.charAt(0));
        if (bool)
        {
          str1 = str;
          int j = str1.length();
          int k = 1;
          j -= k;
          bool = Character.isUpperCase(str1.charAt(j));
          if (bool) {
            return k;
          }
        }
      }
    }
    return false;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (GrammarDataObject)paramObject;
        int i = index;
        int j = index;
        if (i != j) {
          return false;
        }
        localObject1 = label;
        localObject2 = label;
        boolean bool2 = ((String)localObject1).equals(localObject2);
        if (!bool2) {
          return false;
        }
        localObject1 = str;
        if (localObject1 != null)
        {
          paramObject = str;
          return ((String)localObject1).equals(paramObject);
        }
        paramObject = str;
        if (paramObject == null) {
          return bool1;
        }
        return false;
      }
    }
    return false;
  }
  
  public GrammarDataObject getAncestor()
  {
    for (GrammarDataObject localGrammarDataObject = this;; localGrammarDataObject = parent)
    {
      boolean bool = localGrammarDataObject.hasParent();
      if (!bool) {
        break;
      }
    }
    return localGrammarDataObject;
  }
  
  public GrammarDataObject getChild(String paramString)
  {
    Object localObject = children;
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      GrammarDataObject localGrammarDataObject;
      boolean bool2;
      do
      {
        boolean bool1 = ((Iterator)localObject).hasNext();
        if (!bool1) {
          break;
        }
        localGrammarDataObject = (GrammarDataObject)((Iterator)localObject).next();
        String str1 = label;
        bool2 = str1.equals(paramString);
      } while (!bool2);
      return localGrammarDataObject;
    }
    return null;
  }
  
  public GrammarDataObject getLastChild()
  {
    boolean bool = hasChildren();
    if (bool)
    {
      List localList = children;
      int i = localList.size() + -1;
      return (GrammarDataObject)localList.get(i);
    }
    return null;
  }
  
  public GrammarDataObject getLastDescendant()
  {
    Object localObject1 = getLastChild();
    Object localObject2 = null;
    Object localObject3 = localObject1;
    for (localObject1 = null; localObject3 != null; localObject1 = localObject2)
    {
      localObject1 = ((GrammarDataObject)localObject3).getLastChild();
      localObject2 = localObject3;
      localObject3 = localObject1;
    }
    return (GrammarDataObject)localObject1;
  }
  
  public GrammarDataObject getParent()
  {
    return parent;
  }
  
  public List getSiblings()
  {
    boolean bool = hasParent();
    if (bool) {
      return parent.children;
    }
    return null;
  }
  
  public boolean hasChild(String paramString)
  {
    Object localObject = children;
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      boolean bool;
      do
      {
        bool = ((Iterator)localObject).hasNext();
        if (!bool) {
          break;
        }
        String str1 = nextlabel;
        bool = str1.equals(paramString);
      } while (!bool);
      return true;
    }
    return false;
  }
  
  public boolean hasChildren()
  {
    List localList = children;
    if (localList != null)
    {
      int i = localList.size();
      if (i > 0) {
        return true;
      }
    }
    return false;
  }
  
  public boolean hasLockedChildren()
  {
    Iterator localIterator = children.iterator();
    boolean bool;
    do
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      GrammarDataObject localGrammarDataObject = (GrammarDataObject)localIterator.next();
      bool = lock;
    } while (!bool);
    return true;
    return false;
  }
  
  public boolean hasNonMatureValues()
  {
    boolean bool1 = mature;
    if (!bool1)
    {
      a locala = values;
      int i = locala.size();
      int j = 1;
      if (i <= j)
      {
        locala = values;
        i = locala.size();
        if (i == j)
        {
          locala = values;
          String str1 = "type";
          boolean bool2 = locala.containsKey(str1);
          if (!bool2)
          {
            locala = values;
            str1 = "reason";
            bool2 = locala.containsKey(str1);
            if (bool2) {}
          }
        }
      }
      else
      {
        return j;
      }
    }
    return false;
  }
  
  public boolean hasParent()
  {
    GrammarDataObject localGrammarDataObject = parent;
    return localGrammarDataObject != null;
  }
  
  public boolean hasParent(String paramString)
  {
    boolean bool1 = hasParent();
    if (bool1)
    {
      String str1 = parent.label;
      boolean bool2 = str1.equals(paramString);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public boolean hasSibling(String paramString)
  {
    boolean bool1 = hasParent();
    if (bool1)
    {
      Iterator localIterator = parent.children.iterator();
      boolean bool2;
      do
      {
        bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        String str1 = nextlabel;
        bool2 = str1.equals(paramString);
      } while (!bool2);
      return true;
    }
    return false;
  }
  
  public int hashCode()
  {
    String str1 = label;
    int i = str1.hashCode() * 31;
    String str2 = str;
    if (str2 != null)
    {
      j = str2.hashCode();
    }
    else
    {
      j = 0;
      str2 = null;
    }
    i = (i + j) * 31;
    int j = index;
    return i + j;
  }
  
  public boolean isNonDet()
  {
    return label.equals("GDO_NONDET");
  }
  
  public String print()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{[");
    String str1 = label;
    localStringBuilder.append(str1);
    localStringBuilder.append("] <");
    str1 = str;
    if (str1 != null)
    {
      String str2 = "\"";
      String str3 = "\\\\\"";
      str1 = str1.replaceAll(str2, str3);
    }
    else
    {
      str1 = "";
    }
    localStringBuilder.append(str1);
    localStringBuilder.append("> ");
    str1 = printValues();
    localStringBuilder.append(str1);
    localStringBuilder.append(" lock:");
    boolean bool = lock;
    localStringBuilder.append(bool);
    localStringBuilder.append(" strType:");
    int i = strType;
    localStringBuilder.append(i);
    localStringBuilder.append(" }");
    return localStringBuilder.toString();
  }
  
  public String printJSON()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{\"token\":\"");
    String str1 = label;
    localStringBuilder.append(str1);
    localStringBuilder.append("\",\"str\":\"");
    str1 = str;
    localStringBuilder.append(str1);
    localStringBuilder.append("\",\"values\":");
    str1 = printValues();
    localStringBuilder.append(str1);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  public String printLabel()
  {
    String str1 = label;
    String str2 = "GDO_NONDET";
    boolean bool = str1.equals(str2);
    if (!bool) {
      return label;
    }
    return str.replaceAll("[\"'\\\\]", "");
  }
  
  public void setLabel(String paramString)
  {
    label = paramString;
  }
  
  public void setLabelAux(String paramString)
  {
    labelAux = paramString;
  }
  
  public boolean startWithCaps()
  {
    String str1 = str;
    if (str1 != null)
    {
      int i = str1.length();
      if (i > 0)
      {
        str1 = str;
        boolean bool = Character.isUpperCase(str1.charAt(0));
        if (bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  public String toString()
  {
    return printJSON();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */