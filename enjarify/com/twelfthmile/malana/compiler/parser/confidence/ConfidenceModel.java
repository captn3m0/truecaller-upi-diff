package com.twelfthmile.malana.compiler.parser.confidence;

import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObjectList;
import com.twelfthmile.malana.compiler.types.ConfType;
import com.twelfthmile.malana.compiler.types.GDOPos;
import com.twelfthmile.malana.compiler.types.Pair;
import java.util.HashMap;

public class ConfidenceModel
{
  private static int checkConfidence(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2, GrammarDataObject paramGrammarDataObject3, GrammarDataObjectList paramGrammarDataObjectList1, GrammarDataObjectList paramGrammarDataObjectList2, GrammarDataObjectList paramGrammarDataObjectList3, GrammarDataObjectList paramGrammarDataObjectList4)
  {
    Object localObject = label;
    boolean bool1 = ((String)localObject).equals("BLNC");
    if (bool1)
    {
      localObject = label;
      str1 = "NUM";
      bool1 = ((String)localObject).equals(str1);
      if (bool1)
      {
        localObject = values;
        int i = ((com.twelfthmile.b.a.a)localObject).size();
        if (i > 0)
        {
          localObject = values;
          str1 = "num_class";
          boolean bool2 = ((com.twelfthmile.b.a.a)localObject).containsKey(str1);
          if (bool2)
          {
            localObject = values.get("num_class");
            str1 = "PHN";
            bool2 = ((String)localObject).equals(str1);
            if (bool2) {}
          }
          else
          {
            localObject = values;
            str1 = "num";
            localObject = ((com.twelfthmile.b.a.a)localObject).get(str1);
            int j = ((String)localObject).length();
            i2 = 11;
            if (j <= i2) {
              break label162;
            }
          }
          return 0;
        }
      }
    }
    label162:
    localObject = label;
    String str1 = "MOB";
    boolean bool3 = ((String)localObject).equals(str1);
    int i2 = 4;
    if (bool3)
    {
      localObject = label;
      str2 = "NUM";
      bool3 = ((String)localObject).equals(str2);
      if (bool3)
      {
        localObject = values;
        int k = ((com.twelfthmile.b.a.a)localObject).size();
        if (k > 0)
        {
          localObject = values;
          str2 = "num_class";
          boolean bool4 = ((com.twelfthmile.b.a.a)localObject).containsKey(str2);
          if (bool4)
          {
            localObject = values.get("num_class");
            str2 = "NUM";
            bool4 = ((String)localObject).equals(str2);
            if (bool4)
            {
              localObject = values;
              str2 = "num";
              localObject = ((com.twelfthmile.b.a.a)localObject).get(str2);
              int m = ((String)localObject).length();
              i3 = 12;
              if (m <= i3)
              {
                localObject = values;
                str2 = "num";
                localObject = ((com.twelfthmile.b.a.a)localObject).get(str2);
                m = ((String)localObject).length();
                if (m >= i2) {}
              }
              else
              {
                return 0;
              }
            }
          }
        }
      }
    }
    localObject = label;
    String str2 = "MOB";
    boolean bool5 = ((String)localObject).equals(str2);
    if (bool5)
    {
      localObject = label;
      str2 = "NUM";
      bool5 = ((String)localObject).equals(str2);
      if (bool5)
      {
        localObject = "OTP";
        bool5 = paramGrammarDataObjectList1.containsToken((String)localObject);
        if (bool5)
        {
          localObject = "AUX";
          bool5 = paramGrammarDataObjectList2.containsToken((String)localObject);
          if (bool5) {
            return 0;
          }
        }
      }
    }
    localObject = label;
    str2 = "INS";
    bool5 = ((String)localObject).equals(str2);
    int i3 = 3;
    if (bool5)
    {
      localObject = label;
      str3 = "NUM";
      bool5 = ((String)localObject).equals(str3);
      if (bool5)
      {
        localObject = values;
        str3 = "num_class";
        bool5 = ((com.twelfthmile.b.a.a)localObject).containsKey(str3);
        if (bool5)
        {
          localObject = values.get("num_class");
          str3 = "NUM";
          bool5 = ((String)localObject).equals(str3);
          if (bool5)
          {
            localObject = values;
            str3 = "num";
            localObject = ((com.twelfthmile.b.a.a)localObject).get(str3);
            int n = ((String)localObject).length();
            if (n < i3) {
              return 0;
            }
          }
        }
      }
    }
    localObject = label;
    String str3 = "INS";
    boolean bool6 = ((String)localObject).equals(str3);
    if (bool6)
    {
      localObject = label;
      str3 = "NUM";
      bool6 = ((String)localObject).equals(str3);
      if (bool6)
      {
        localObject = "OTP";
        bool6 = paramGrammarDataObjectList1.containsToken((String)localObject);
        if (bool6)
        {
          localObject = "AUX";
          bool6 = paramGrammarDataObjectList2.containsToken((String)localObject);
          if (bool6) {}
        }
        else
        {
          localObject = "DT";
          bool6 = paramGrammarDataObjectList2.containsToken((String)localObject);
          if (!bool6)
          {
            localObject = "TRXID";
            bool6 = paramGrammarDataObjectList2.containsToken((String)localObject);
            if (!bool6) {
              break label717;
            }
          }
        }
        return 0;
      }
    }
    label717:
    localObject = label;
    str3 = "LOCATION";
    bool6 = ((String)localObject).equals(str3);
    int i4 = 2;
    int i5 = 1;
    int i6 = 100;
    if (bool6)
    {
      localObject = label;
      str4 = "LOCATION";
      bool6 = ((String)localObject).equals(str4);
      if (bool6)
      {
        int i7 = paramGrammarDataObjectList2.size();
        if (i7 != 0)
        {
          i7 = paramGrammarDataObjectList2.size();
          if (i7 == i5)
          {
            paramGrammarDataObject1 = "to";
            boolean bool7 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
            if (!bool7)
            {
              paramGrammarDataObject1 = "TO";
              bool7 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (!bool7)
              {
                paramGrammarDataObject1 = "AIRPORT";
                bool7 = paramGrammarDataObjectList2.containsToken(paramGrammarDataObject1);
                if (!bool7)
                {
                  paramGrammarDataObject1 = "GDO_NONDET";
                  bool7 = paramGrammarDataObjectList2.containsToken(paramGrammarDataObject1);
                  if (bool7) {}
                }
              }
            }
          }
          else
          {
            int i8 = paramGrammarDataObjectList2.size();
            if (i8 == i4)
            {
              paramGrammarDataObject1 = "to";
              boolean bool8 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (bool8) {}
            }
            else
            {
              return 0;
            }
          }
        }
        return i6;
      }
    }
    localObject = label;
    String str4 = "AIRPORT";
    bool6 = ((String)localObject).equals(str4);
    if (bool6)
    {
      localObject = label;
      str4 = "AIRPORT";
      bool6 = ((String)localObject).equals(str4);
      if (bool6)
      {
        int i9 = paramGrammarDataObjectList2.size();
        if (i9 != 0)
        {
          i9 = paramGrammarDataObjectList2.size();
          if (i9 == i5)
          {
            paramGrammarDataObject1 = "to";
            boolean bool9 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
            if (!bool9)
            {
              paramGrammarDataObject1 = "TO";
              bool9 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (!bool9)
              {
                paramGrammarDataObject1 = "GDO_NONDET";
                bool9 = paramGrammarDataObjectList2.containsToken(paramGrammarDataObject1);
                if (bool9) {}
              }
            }
          }
          else
          {
            return 0;
          }
        }
        return i6;
      }
    }
    localObject = label;
    str4 = "DUE";
    bool6 = ((String)localObject).equals(str4);
    if (bool6)
    {
      localObject = label;
      str4 = "DATE";
      bool6 = ((String)localObject).equals(str4);
      if (bool6)
      {
        int i10 = paramGrammarDataObjectList2.size();
        if (i10 != 0)
        {
          i10 = paramGrammarDataObjectList2.size();
          if (i10 == i5)
          {
            paramGrammarDataObject1 = "on";
            boolean bool10 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
            if (!bool10)
            {
              paramGrammarDataObject1 = "ON";
              bool10 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (!bool10)
              {
                paramGrammarDataObject1 = "DT";
                bool10 = paramGrammarDataObjectList2.containsToken(paramGrammarDataObject1);
                if (bool10) {}
              }
            }
          }
          else
          {
            return 0;
          }
        }
        return i6;
      }
    }
    localObject = label;
    str4 = "DEPART";
    bool6 = ((String)localObject).equals(str4);
    int i11;
    if (!bool6)
    {
      localObject = label;
      str4 = "REPORTTM";
      bool6 = ((String)localObject).equals(str4);
      if (!bool6) {}
    }
    else
    {
      localObject = label;
      str4 = "NUM";
      bool6 = ((String)localObject).equals(str4);
      if (bool6)
      {
        i11 = paramGrammarDataObjectList2.size();
        if (i11 == 0)
        {
          paramGrammarDataObject1 = str;
          i11 = paramGrammarDataObject1.length();
          if (i11 == i2)
          {
            paramGrammarDataObject1 = values;
            paramGrammarDataObject2 = values.remove("num");
            paramGrammarDataObject1.put("time", paramGrammarDataObject2);
            return i6;
          }
        }
        return 0;
      }
    }
    localObject = label;
    str4 = "ORDSTATUS";
    bool6 = ((String)localObject).equals(str4);
    if (bool6)
    {
      localObject = label;
      str4 = "ORDERIDVAL";
      bool6 = ((String)localObject).equals(str4);
      if (bool6)
      {
        i11 = paramGrammarDataObjectList1.size();
        if (i11 != 0) {
          return 0;
        }
        paramGrammarDataObjectList1 = "PICKUP PREP #item";
        String[] tmp1365_1362 = new String[3];
        String[] tmp1366_1365 = tmp1365_1362;
        String[] tmp1366_1365 = tmp1365_1362;
        tmp1366_1365[0] = "ORDER PREPV #item PREP";
        tmp1366_1365[1] = "DET #item PREP";
        tmp1366_1365[2] = paramGrammarDataObjectList1;
        paramGrammarDataObject1 = tmp1366_1365;
        i21 = 0;
        paramGrammarDataObject3 = null;
        while (i21 < i3)
        {
          paramGrammarDataObjectList1 = paramGrammarDataObject1[i21];
          paramGrammarDataObjectList1 = paramGrammarDataObjectList2.match(paramGrammarDataObjectList1);
          if (paramGrammarDataObjectList1 != null)
          {
            paramGrammarDataObject1 = values;
            paramGrammarDataObject2 = (String)paramGrammarDataObjectList1.getA();
            paramGrammarDataObject3 = (String)paramGrammarDataObjectList1.getB();
            paramGrammarDataObject1.put(paramGrammarDataObject2, paramGrammarDataObject3);
            return i6;
          }
          i21 += 1;
        }
        return 0;
      }
    }
    localObject = label;
    str4 = "ORDSTATUS";
    bool6 = ((String)localObject).equals(str4);
    if (bool6)
    {
      localObject = label;
      str4 = "ORDSTATUS";
      bool6 = ((String)localObject).equals(str4);
      if (bool6)
      {
        i21 = paramGrammarDataObjectList1.size();
        if (i21 != 0) {
          return 0;
        }
        paramGrammarDataObjectList3 = "#item AUX";
        paramGrammarDataObjectList4 = "#item";
        String[] tmp1524_1521 = new String[4];
        String[] tmp1525_1524 = tmp1524_1521;
        String[] tmp1525_1524 = tmp1524_1521;
        tmp1525_1524[0] = "PACKAGE PREP #item AUX";
        tmp1525_1524[1] = "PACKAGE PREP #item";
        tmp1525_1524[2] = paramGrammarDataObjectList3;
        String[] tmp1538_1525 = tmp1525_1524;
        tmp1538_1525[3] = paramGrammarDataObjectList4;
        paramGrammarDataObject3 = tmp1538_1525;
        int i24 = 0;
        paramGrammarDataObjectList1 = null;
        while (i24 < i2)
        {
          paramGrammarDataObjectList3 = paramGrammarDataObject3[i24];
          paramGrammarDataObjectList3 = paramGrammarDataObjectList2.match(paramGrammarDataObjectList3);
          if (paramGrammarDataObjectList3 != null)
          {
            paramGrammarDataObject3 = values;
            paramGrammarDataObjectList1 = (String)paramGrammarDataObjectList3.getA();
            paramGrammarDataObjectList2 = (String)paramGrammarDataObjectList3.getB();
            paramGrammarDataObject3.put(paramGrammarDataObjectList1, paramGrammarDataObjectList2);
            paramGrammarDataObject2 = values;
            paramGrammarDataObject1 = values.get("status");
            paramGrammarDataObject2.put("status", paramGrammarDataObject1);
            return i6;
          }
          i24 += 1;
        }
        return 0;
      }
    }
    localObject = label;
    str1 = "ORDER";
    bool6 = ((String)localObject).equals(str1);
    if (bool6)
    {
      localObject = label;
      str1 = "ORDSTATUS";
      bool6 = ((String)localObject).equals(str1);
      if (bool6)
      {
        i11 = paramGrammarDataObjectList1.size();
        if (i11 == i5)
        {
          paramGrammarDataObject1 = "DET";
          boolean bool11 = paramGrammarDataObjectList1.containsToken(paramGrammarDataObject1);
          if (bool11)
          {
            paramGrammarDataObject1 = new String[] { "#item AUX" };
            i21 = 0;
            paramGrammarDataObject3 = null;
            while (i21 <= 0)
            {
              paramGrammarDataObjectList1 = paramGrammarDataObject1[0];
              paramGrammarDataObjectList1 = paramGrammarDataObjectList2.match(paramGrammarDataObjectList1);
              if (paramGrammarDataObjectList1 != null)
              {
                paramGrammarDataObject1 = values;
                paramGrammarDataObject2 = (String)paramGrammarDataObjectList1.getA();
                paramGrammarDataObject3 = (String)paramGrammarDataObjectList1.getB();
                paramGrammarDataObject1.put(paramGrammarDataObject2, paramGrammarDataObject3);
                return i6;
              }
              i21 += 1;
            }
            return 0;
          }
        }
        return 0;
      }
    }
    paramGrammarDataObjectList1 = label;
    localObject = "ORDSTATUS";
    boolean bool23 = paramGrammarDataObjectList1.equals(localObject);
    if (bool23)
    {
      paramGrammarDataObjectList1 = label;
      localObject = "ORDCNCLRESN";
      bool23 = paramGrammarDataObjectList1.equals(localObject);
      if (bool23) {}
    }
    else
    {
      paramGrammarDataObjectList1 = label;
      localObject = "RETPICKUP";
      bool23 = paramGrammarDataObjectList1.equals(localObject);
      if (bool23)
      {
        paramGrammarDataObjectList1 = label;
        localObject = "RESCHE";
        bool23 = paramGrammarDataObjectList1.equals(localObject);
        if (bool23) {}
      }
      else
      {
        paramGrammarDataObjectList1 = label;
        localObject = "RETPICKUP";
        bool23 = paramGrammarDataObjectList1.equals(localObject);
        if (!bool23) {
          break label2022;
        }
        paramGrammarDataObjectList1 = label;
        localObject = "SCHED";
        bool23 = paramGrammarDataObjectList1.equals(localObject);
        if (!bool23) {
          break label2022;
        }
      }
    }
    paramGrammarDataObject1 = new String[] { "#item" };
    int i21 = 0;
    paramGrammarDataObject3 = null;
    while (i21 <= 0)
    {
      paramGrammarDataObjectList1 = paramGrammarDataObject1[0];
      paramGrammarDataObjectList1 = paramGrammarDataObjectList2.match(paramGrammarDataObjectList1);
      if (paramGrammarDataObjectList1 != null)
      {
        paramGrammarDataObject1 = values;
        paramGrammarDataObject2 = (String)paramGrammarDataObjectList1.getA();
        paramGrammarDataObject3 = (String)paramGrammarDataObjectList1.getB();
        paramGrammarDataObject1.put(paramGrammarDataObject2, paramGrammarDataObject3);
        return i6;
      }
      i21 += 1;
    }
    return 0;
    label2022:
    paramGrammarDataObjectList1 = label;
    localObject = "CHQNO";
    bool23 = paramGrammarDataObjectList1.equals(localObject);
    int i1 = 5;
    if (bool23)
    {
      paramGrammarDataObjectList1 = label;
      str1 = "AMT";
      bool23 = paramGrammarDataObjectList1.equals(str1);
      if (bool23)
      {
        int i12 = paramGrammarDataObjectList2.size();
        if (i12 != 0)
        {
          i12 = paramGrammarDataObjectList2.size();
          if (i12 <= i1)
          {
            paramGrammarDataObject1 = "cheque";
            paramGrammarDataObject2 = values;
            paramGrammarDataObjectList1 = "acc_type";
            paramGrammarDataObject2 = paramGrammarDataObject2.get(paramGrammarDataObjectList1);
            boolean bool12 = paramGrammarDataObject1.equals(paramGrammarDataObject2);
            if (bool12)
            {
              paramGrammarDataObject1 = "OF";
              bool12 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (bool12) {
                break label2324;
              }
              paramGrammarDataObject1 = "of";
              bool12 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (bool12) {
                break label2324;
              }
              paramGrammarDataObject1 = "FOR";
              bool12 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (bool12) {
                break label2324;
              }
              paramGrammarDataObject1 = "for";
              bool12 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (bool12) {
                break label2324;
              }
            }
          }
          int i13 = paramGrammarDataObjectList2.size();
          int i25 = 10;
          if (i13 <= i25)
          {
            paramGrammarDataObject1 = "cheque";
            paramGrammarDataObject2 = values;
            paramGrammarDataObject3 = "acc_type";
            paramGrammarDataObject2 = paramGrammarDataObject2.get(paramGrammarDataObject3);
            boolean bool13 = paramGrammarDataObject1.equals(paramGrammarDataObject2);
            if (bool13)
            {
              paramGrammarDataObject1 = "INSTR";
              bool13 = paramGrammarDataObjectList2.containsToken(paramGrammarDataObject1);
              if (bool13)
              {
                paramGrammarDataObject1 = "OF";
                bool13 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
                if (bool13) {
                  break label2324;
                }
                paramGrammarDataObject1 = "of";
                bool13 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
                if (bool13) {
                  break label2324;
                }
                paramGrammarDataObject1 = "FOR";
                bool13 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
                if (bool13) {
                  break label2324;
                }
                paramGrammarDataObject1 = "for";
                bool13 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
                if (bool13) {
                  break label2324;
                }
              }
            }
          }
          return 0;
        }
        label2324:
        return i6;
      }
    }
    paramGrammarDataObjectList1 = label;
    str1 = "CHQ";
    bool23 = paramGrammarDataObjectList1.equals(str1);
    boolean bool14;
    if (bool23)
    {
      paramGrammarDataObjectList1 = label;
      str1 = "AMT";
      bool23 = paramGrammarDataObjectList1.equals(str1);
      if (bool23)
      {
        int i14 = paramGrammarDataObjectList2.size();
        if (i14 != 0)
        {
          i14 = paramGrammarDataObjectList2.size();
          if (i14 == i5)
          {
            paramGrammarDataObject1 = "cheque";
            paramGrammarDataObject2 = values;
            paramGrammarDataObject3 = "acc_type";
            paramGrammarDataObject2 = paramGrammarDataObject2.get(paramGrammarDataObject3);
            bool14 = paramGrammarDataObject1.equals(paramGrammarDataObject2);
            if (bool14)
            {
              paramGrammarDataObject1 = "OF";
              bool14 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (bool14) {
                break label2497;
              }
              paramGrammarDataObject1 = "of";
              bool14 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (bool14) {
                break label2497;
              }
              paramGrammarDataObject1 = "FOR";
              bool14 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (bool14) {
                break label2497;
              }
              paramGrammarDataObject1 = "for";
              bool14 = paramGrammarDataObjectList2.containsStr(paramGrammarDataObject1);
              if (bool14) {
                break label2497;
              }
            }
          }
          return 0;
        }
        label2497:
        return i6;
      }
    }
    paramGrammarDataObjectList1 = label;
    str1 = "CHQAMT";
    bool23 = paramGrammarDataObjectList1.equals(str1);
    if (bool23)
    {
      paramGrammarDataObjectList1 = label;
      str1 = "ORDNTF";
      bool23 = paramGrammarDataObjectList1.equals(str1);
      if (bool23)
      {
        paramGrammarDataObject1 = "cheque";
        paramGrammarDataObject2 = values;
        paramGrammarDataObject3 = "acc_type";
        paramGrammarDataObject2 = paramGrammarDataObject2.get(paramGrammarDataObject3);
        bool14 = paramGrammarDataObject1.equals(paramGrammarDataObject2);
        if (bool14) {
          return i6;
        }
        return 0;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "TRANS";
    boolean bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    if (bool20)
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "AMT";
      bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (bool20)
      {
        paramGrammarDataObject1 = "ORDER";
        bool14 = paramGrammarDataObjectList4.containsToken(paramGrammarDataObject1);
        if (bool14) {
          return 0;
        }
        return i6;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "TOTAL";
    bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    boolean bool24;
    if (!bool20)
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "MIN";
      bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (!bool20) {}
    }
    else
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "TRANSINTENT";
      bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (bool20)
      {
        int i15 = paramGrammarDataObjectList2.size();
        if (i15 == 0)
        {
          paramGrammarDataObject1 = paramGrammarDataObject2.getChild("TRANS");
          if (paramGrammarDataObject1 != null)
          {
            paramGrammarDataObject2 = str;
            paramGrammarDataObject3 = "payment";
            bool24 = paramGrammarDataObject2.equals(paramGrammarDataObject3);
            if (!bool24)
            {
              paramGrammarDataObject2 = str;
              paramGrammarDataObject3 = "paying";
              bool24 = paramGrammarDataObject2.equals(paramGrammarDataObject3);
              if (!bool24)
              {
                paramGrammarDataObject2 = str;
                paramGrammarDataObject3 = "pymt";
                bool24 = paramGrammarDataObject2.equals(paramGrammarDataObject3);
                if (!bool24)
                {
                  paramGrammarDataObject2 = str;
                  paramGrammarDataObject3 = "pmt";
                  bool24 = paramGrammarDataObject2.equals(paramGrammarDataObject3);
                  if (!bool24)
                  {
                    paramGrammarDataObject2 = str;
                    paramGrammarDataObject3 = "pmnt";
                    bool24 = paramGrammarDataObject2.equals(paramGrammarDataObject3);
                    if (!bool24)
                    {
                      paramGrammarDataObject1 = str;
                      paramGrammarDataObject2 = "pyt";
                      boolean bool15 = paramGrammarDataObject1.equals(paramGrammarDataObject2);
                      if (!bool15) {
                        break label2847;
                      }
                    }
                  }
                }
              }
            }
            return i6;
          }
          label2847:
          return 0;
        }
        return 0;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "AMT";
    bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    if (bool20)
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "ORDNTF";
      bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (bool20)
      {
        int i16 = paramGrammarDataObjectList3.size();
        if (i16 == 0)
        {
          i16 = paramGrammarDataObjectList4.size();
          if (i16 == 0)
          {
            paramGrammarDataObject1 = str;
            paramGrammarDataObject3 = "returned";
            boolean bool16 = paramGrammarDataObject1.equals(paramGrammarDataObject3);
            if (bool16)
            {
              values.put("trx_type", "credit");
              return i6;
            }
          }
        }
        return 0;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "AMT";
    bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    boolean bool17;
    if (bool20)
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "TRANS";
      bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (bool20)
      {
        int i17 = paramGrammarDataObjectList4.size();
        if (i17 != 0)
        {
          i17 = paramGrammarDataObjectList4.size();
          if (i17 == i5)
          {
            paramGrammarDataObject1 = "AUX";
            bool17 = paramGrammarDataObjectList4.containsToken(paramGrammarDataObject1);
            if (!bool17) {}
          }
        }
        else
        {
          paramGrammarDataObject1 = str;
          paramGrammarDataObject3 = "transferred";
          bool17 = paramGrammarDataObject1.equals(paramGrammarDataObject3);
          if (bool17) {
            break label3101;
          }
          paramGrammarDataObject1 = str;
          paramGrammarDataObject3 = "charged";
          bool17 = paramGrammarDataObject1.equals(paramGrammarDataObject3);
          if (bool17) {
            break label3101;
          }
          paramGrammarDataObject1 = str;
          paramGrammarDataObject2 = "received";
          bool17 = paramGrammarDataObject1.equals(paramGrammarDataObject2);
          if (bool17) {
            break label3101;
          }
        }
        return 0;
        label3101:
        return i6;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "TRANSINTENT";
    bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    if (bool20)
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "TRANS";
      bool20 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (bool20)
      {
        int i22 = paramGrammarDataObjectList2.size();
        if (i22 != 0)
        {
          i22 = paramGrammarDataObjectList2.size();
          if (i22 <= i1)
          {
            paramGrammarDataObject3 = "AUX";
            bool21 = paramGrammarDataObjectList2.containsToken(paramGrammarDataObject3);
            if (!bool21) {}
          }
        }
        else
        {
          paramGrammarDataObject3 = "received";
          paramGrammarDataObject2 = str;
          bool24 = paramGrammarDataObject3.equalsIgnoreCase(paramGrammarDataObject2);
          if (bool24)
          {
            paramGrammarDataObject2 = "payment";
            paramGrammarDataObject3 = getChild"TRANS"str;
            bool24 = paramGrammarDataObject2.equalsIgnoreCase(paramGrammarDataObject3);
            if (!bool24)
            {
              paramGrammarDataObject2 = "pyt";
              paramGrammarDataObject3 = getChild"TRANS"str;
              bool24 = paramGrammarDataObject2.equalsIgnoreCase(paramGrammarDataObject3);
              if (!bool24)
              {
                paramGrammarDataObject2 = "pymt";
                paramGrammarDataObject3 = "TRANS";
                paramGrammarDataObject1 = getChildstr;
                bool17 = paramGrammarDataObject2.equalsIgnoreCase(paramGrammarDataObject1);
                if (!bool17) {
                  break label3286;
                }
              }
            }
            return i6;
          }
        }
        label3286:
        return 0;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "TRANS";
    boolean bool21 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    if (bool21)
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "TRANSINTENT";
      bool21 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (bool21)
      {
        int i23 = paramGrammarDataObjectList2.size();
        if (i23 <= i5)
        {
          paramGrammarDataObject3 = "received";
          paramGrammarDataObject1 = str;
          bool17 = paramGrammarDataObject3.equalsIgnoreCase(paramGrammarDataObject1);
          if (bool17)
          {
            paramGrammarDataObject1 = "payment";
            paramGrammarDataObject3 = getChild"TRANS"str;
            bool17 = paramGrammarDataObject1.equalsIgnoreCase(paramGrammarDataObject3);
            if (!bool17)
            {
              paramGrammarDataObject1 = "pyt";
              paramGrammarDataObject3 = getChild"TRANS"str;
              bool17 = paramGrammarDataObject1.equalsIgnoreCase(paramGrammarDataObject3);
              if (!bool17)
              {
                paramGrammarDataObject1 = "pymt";
                paramGrammarDataObject3 = "TRANS";
                paramGrammarDataObject2 = getChildstr;
                bool17 = paramGrammarDataObject1.equalsIgnoreCase(paramGrammarDataObject2);
                if (!bool17) {
                  break label3442;
                }
              }
            }
            return i6;
          }
        }
        label3442:
        return 0;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "REACH";
    boolean bool22 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    int i18;
    if (bool22)
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "DATE";
      bool22 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (bool22)
      {
        paramGrammarDataObject1 = "ORDSTATUS";
        bool17 = paramGrammarDataObjectList3.containsToken(paramGrammarDataObject1);
        if (bool17)
        {
          i18 = paramGrammarDataObjectList2.size();
          if (i18 <= i3) {
            return i6;
          }
        }
        return 0;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "FARE";
    bool22 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    if (bool22)
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "NUM";
      bool22 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (bool22)
      {
        i18 = paramGrammarDataObjectList2.size();
        if (i18 == 0) {
          return i6;
        }
        return 0;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "AUX";
    bool22 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    boolean bool18;
    if (bool22)
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "SUCCESSFUL";
      bool22 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (bool22)
      {
        paramGrammarDataObject1 = "TRANSINTENT";
        bool18 = paramGrammarDataObjectList3.containsToken(paramGrammarDataObject1);
        if (bool18)
        {
          paramGrammarDataObject1 = "INSTR";
          bool18 = paramGrammarDataObjectList3.containsToken(paramGrammarDataObject1);
          if (!bool18)
          {
            paramGrammarDataObject1 = "INSTRNO";
            bool18 = paramGrammarDataObjectList3.containsToken(paramGrammarDataObject1);
            if (!bool18) {}
          }
          else
          {
            return 0;
          }
        }
        return i6;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "DISCOUNT";
    bool22 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    if (bool22)
    {
      paramGrammarDataObject3 = label;
      paramGrammarDataObjectList1 = "FAREAMT";
      bool22 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
      if (bool22)
      {
        paramGrammarDataObject1 = str;
        paramGrammarDataObject2 = "discounted";
        bool18 = paramGrammarDataObject1.equalsIgnoreCase(paramGrammarDataObject2);
        if (bool18)
        {
          int i19 = paramGrammarDataObjectList2.size();
          if (i19 == 0) {
            return i6;
          }
        }
        return 0;
      }
    }
    paramGrammarDataObject3 = label;
    paramGrammarDataObjectList1 = "AVBL";
    bool22 = paramGrammarDataObject3.equals(paramGrammarDataObjectList1);
    if (bool22)
    {
      paramGrammarDataObject2 = label;
      paramGrammarDataObject3 = "INS";
      bool24 = paramGrammarDataObject2.equals(paramGrammarDataObject3);
      if (bool24)
      {
        paramGrammarDataObject1 = str;
        paramGrammarDataObject2 = "avail";
        boolean bool19 = paramGrammarDataObject1.equalsIgnoreCase(paramGrammarDataObject2);
        if (bool19)
        {
          int i20 = paramGrammarDataObjectList2.size();
          if (i20 <= i4) {
            return i6;
          }
        }
        return 0;
      }
    }
    return i6;
  }
  
  public static int checkConfidence(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2, GrammarDataObject paramGrammarDataObject3, GDOPos paramGDOPos, HashMap paramHashMap)
  {
    Object localObject1 = paramGDOPos.getGDOsBefore(paramGrammarDataObject1);
    paramGDOPos = paramGDOPos.getGDOsBetween(paramGrammarDataObject1, paramGrammarDataObject2);
    Object localObject2 = ((Pair)localObject1).getA();
    Object localObject3 = localObject2;
    localObject3 = (GrammarDataObjectList)localObject2;
    localObject2 = paramGDOPos.getA();
    Object localObject4 = localObject2;
    localObject4 = (GrammarDataObjectList)localObject2;
    localObject1 = ((Pair)localObject1).getB();
    Object localObject5 = localObject1;
    localObject5 = (GrammarDataObjectList)localObject1;
    paramGDOPos = paramGDOPos.getB();
    Object localObject6 = paramGDOPos;
    localObject6 = (GrammarDataObjectList)paramGDOPos;
    int i = checkConfidence(paramGrammarDataObject1, paramGrammarDataObject2, (GrammarDataObjectList)localObject4, paramHashMap);
    if (i >= 0) {
      return i;
    }
    return checkConfidence(paramGrammarDataObject1, paramGrammarDataObject2, paramGrammarDataObject3, (GrammarDataObjectList)localObject3, (GrammarDataObjectList)localObject4, (GrammarDataObjectList)localObject5, (GrammarDataObjectList)localObject6);
  }
  
  private static int checkConfidence(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2, GrammarDataObjectList paramGrammarDataObjectList, HashMap paramHashMap)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str1 = labelAux;
    ((StringBuilder)localObject).append(str1);
    ((StringBuilder)localObject).append("-");
    str1 = labelAux;
    ((StringBuilder)localObject).append(str1);
    localObject = ((StringBuilder)localObject).toString();
    boolean bool1 = paramHashMap.containsKey(localObject);
    int j;
    if (bool1)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      paramGrammarDataObject1 = labelAux;
      ((StringBuilder)localObject).append(paramGrammarDataObject1);
      ((StringBuilder)localObject).append("-");
      paramGrammarDataObject1 = labelAux;
      ((StringBuilder)localObject).append(paramGrammarDataObject1);
      paramGrammarDataObject1 = ((StringBuilder)localObject).toString();
      paramGrammarDataObject1 = (ConfType)paramHashMap.get(paramGrammarDataObject1);
    }
    else
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      str1 = label;
      ((StringBuilder)localObject).append(str1);
      ((StringBuilder)localObject).append("-");
      str1 = label;
      ((StringBuilder)localObject).append(str1);
      localObject = ((StringBuilder)localObject).toString();
      bool1 = paramHashMap.containsKey(localObject);
      if (bool1)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        paramGrammarDataObject1 = label;
        ((StringBuilder)localObject).append(paramGrammarDataObject1);
        ((StringBuilder)localObject).append("-");
        paramGrammarDataObject1 = label;
        ((StringBuilder)localObject).append(paramGrammarDataObject1);
        paramGrammarDataObject1 = ((StringBuilder)localObject).toString();
        paramGrammarDataObject1 = (ConfType)paramHashMap.get(paramGrammarDataObject1);
      }
      else
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        str1 = label;
        ((StringBuilder)localObject).append(str1);
        ((StringBuilder)localObject).append("-");
        str1 = labelAux;
        ((StringBuilder)localObject).append(str1);
        localObject = ((StringBuilder)localObject).toString();
        bool1 = paramHashMap.containsKey(localObject);
        if (bool1)
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          paramGrammarDataObject1 = label;
          ((StringBuilder)localObject).append(paramGrammarDataObject1);
          ((StringBuilder)localObject).append("-");
          paramGrammarDataObject1 = labelAux;
          ((StringBuilder)localObject).append(paramGrammarDataObject1);
          paramGrammarDataObject1 = ((StringBuilder)localObject).toString();
          paramGrammarDataObject1 = (ConfType)paramHashMap.get(paramGrammarDataObject1);
        }
        else
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          str1 = labelAux;
          ((StringBuilder)localObject).append(str1);
          ((StringBuilder)localObject).append("-");
          str1 = label;
          ((StringBuilder)localObject).append(str1);
          localObject = ((StringBuilder)localObject).toString();
          bool1 = paramHashMap.containsKey(localObject);
          if (bool1)
          {
            localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>();
            paramGrammarDataObject1 = labelAux;
            ((StringBuilder)localObject).append(paramGrammarDataObject1);
            ((StringBuilder)localObject).append("-");
            paramGrammarDataObject1 = label;
            ((StringBuilder)localObject).append(paramGrammarDataObject1);
            paramGrammarDataObject1 = ((StringBuilder)localObject).toString();
            paramGrammarDataObject1 = (ConfType)paramHashMap.get(paramGrammarDataObject1);
          }
          else
          {
            j = 0;
            paramGrammarDataObject1 = null;
          }
        }
      }
    }
    if (paramGrammarDataObject1 != null) {
      try
      {
        int k = paramGrammarDataObject1.getN();
        paramHashMap = paramGrammarDataObject1.getStrArr();
        int i = 100;
        str1 = null;
        if (paramHashMap == null)
        {
          if (k == 0)
          {
            j = paramGrammarDataObjectList.size();
            if (j == 0) {
              return i;
            }
            return 0;
          }
          j = paramGrammarDataObjectList.size();
          if (j <= k) {
            return i;
          }
          return 0;
        }
        int m = paramGrammarDataObjectList.size();
        if (m == 0)
        {
          m = 1;
        }
        else
        {
          m = 0;
          paramHashMap = null;
        }
        paramGrammarDataObject1 = paramGrammarDataObject1.getStrArr();
        int n = paramGrammarDataObject1.length;
        int i1 = 0;
        boolean bool2 = false;
        while (i1 < n)
        {
          String str2 = paramGrammarDataObject1[i1];
          char c = str2.charAt(0);
          boolean bool3 = com.twelfthmile.b.b.a.c(c);
          if (bool3)
          {
            if (!bool2)
            {
              bool2 = paramGrammarDataObjectList.containsToken(str2);
              if (!bool2)
              {
                bool2 = false;
                break label715;
              }
            }
            bool2 = true;
          }
          else
          {
            if (!bool2)
            {
              bool2 = paramGrammarDataObjectList.containsStr(str2);
              if (!bool2)
              {
                bool2 = false;
                break label715;
              }
            }
            bool2 = true;
          }
          label715:
          i1 += 1;
        }
        if (m == 0)
        {
          j = paramGrammarDataObjectList.size();
          if ((j > k) || (!bool2)) {
            return 0;
          }
        }
        return i;
      }
      catch (Exception localException) {}
    }
    return -1;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.confidence.ConfidenceModel
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */