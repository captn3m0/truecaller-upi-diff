package com.twelfthmile.malana.compiler.parser.semantic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SemanticSeedConstants
{
  HashMap coreDims;
  HashMap semConstants;
  
  public SemanticSeedConstants()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    semConstants = localHashMap;
    localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    coreDims = localHashMap;
  }
  
  public boolean containsKey(String paramString)
  {
    return semConstants.containsKey(paramString);
  }
  
  public int get(String paramString)
  {
    return ((Integer)semConstants.get(paramString)).intValue();
  }
  
  public void put(String paramString, int paramInt)
  {
    HashMap localHashMap = semConstants;
    Integer localInteger = Integer.valueOf(paramInt);
    localHashMap.put(paramString, localInteger);
  }
  
  public void put(String paramString1, String paramString2)
  {
    HashMap localHashMap = coreDims;
    boolean bool = localHashMap.containsKey(paramString1);
    if (!bool)
    {
      localHashMap = coreDims;
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      localHashMap.put(paramString1, localArrayList);
    }
    ((List)coreDims.get(paramString1)).add(paramString2);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticSeedConstants
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */