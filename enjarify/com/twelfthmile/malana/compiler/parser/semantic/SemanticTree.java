package com.twelfthmile.malana.compiler.parser.semantic;

import com.twelfthmile.malana.compiler.types.Pair;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

class SemanticTree
{
  private SemanticTreeContext context;
  private Stack pathStack;
  private SemanticNode rootNode;
  private List sdoStack;
  private SemanticOperationObjectList sooList;
  
  SemanticTree()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    sdoStack = ((List)localObject);
    localObject = new com/twelfthmile/malana/compiler/parser/semantic/SemanticNode;
    ((SemanticNode)localObject).<init>();
    rootNode = ((SemanticNode)localObject);
    localObject = new java/util/Stack;
    ((Stack)localObject).<init>();
    pathStack = ((Stack)localObject);
    localObject = new com/twelfthmile/malana/compiler/parser/semantic/SemanticOperationObjectList;
    ((SemanticOperationObjectList)localObject).<init>();
    sooList = ((SemanticOperationObjectList)localObject);
    localObject = new com/twelfthmile/malana/compiler/parser/semantic/SemanticTreeContext;
    ((SemanticTreeContext)localObject).<init>();
    context = ((SemanticTreeContext)localObject);
  }
  
  private SemanticOperandObject returnFromOperandList(List paramList, SemanticOperandObject paramSemanticOperandObject)
  {
    Iterator localIterator = paramList.iterator();
    SemanticOperandObject localSemanticOperandObject;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localSemanticOperandObject = (SemanticOperandObject)localIterator.next();
      bool2 = localSemanticOperandObject.equals(paramSemanticOperandObject);
    } while (!bool2);
    return localSemanticOperandObject;
    paramList.add(paramSemanticOperandObject);
    return paramSemanticOperandObject;
  }
  
  private SemanticOperatorObject returnFromOperatorList(List paramList, SemanticOperatorObject paramSemanticOperatorObject)
  {
    Iterator localIterator = paramList.iterator();
    SemanticOperatorObject localSemanticOperatorObject;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localSemanticOperatorObject = (SemanticOperatorObject)localIterator.next();
      bool2 = localSemanticOperatorObject.equals(paramSemanticOperatorObject);
    } while (!bool2);
    return localSemanticOperatorObject;
    paramList.add(paramSemanticOperatorObject);
    return paramSemanticOperatorObject;
  }
  
  void add(SemanticDataObject paramSemanticDataObject)
  {
    Object localObject1 = paramSemanticDataObject.getType();
    if (localObject1 != null)
    {
      localObject1 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticNode;
      ((SemanticNode)localObject1).<init>(paramSemanticDataObject);
      Object localObject2 = rootNode.isExplicitPath(paramSemanticDataObject);
      Object localObject4;
      if (localObject2 != null)
      {
        localObject3 = pathStack;
        boolean bool1 = ((Stack)localObject3).empty();
        if (!bool1)
        {
          localObject3 = pathStack;
          ((Stack)localObject3).pop();
        }
        localObject3 = pathStack;
        localObject4 = new com/twelfthmile/malana/compiler/types/Pair;
        ((Pair)localObject4).<init>(localObject2, localObject1);
        ((Stack)localObject3).push(localObject4);
      }
      else
      {
        localObject2 = pathStack;
        bool2 = ((Stack)localObject2).empty();
        if (!bool2)
        {
          localObject2 = (Pair)pathStack.pop();
          localObject3 = rootNode;
          localObject4 = (String)((Pair)localObject2).getA();
          localObject2 = (SemanticNode)((Pair)localObject2).getB();
          int i = 2;
          ((SemanticNode)localObject3).addNode((String)localObject4, (SemanticNode)localObject2, (SemanticNode)localObject1, i);
        }
        else
        {
          localObject2 = rootNode;
          ((SemanticNode)localObject2).relate((SemanticNode)localObject1);
        }
      }
      localObject2 = paramSemanticDataObject.getType();
      Object localObject3 = "subject";
      boolean bool2 = ((String)localObject2).equals(localObject3);
      if (bool2)
      {
        localObject2 = context;
        localObject3 = rootNode.getPath((SemanticNode)localObject1);
        ((SemanticTreeContext)localObject2).setSubject((SemanticPath)localObject3, (SemanticNode)localObject1);
      }
    }
    sdoStack.add(paramSemanticDataObject);
  }
  
  public void buildTree()
  {
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    ArrayList localArrayList2 = new java/util/ArrayList;
    localArrayList2.<init>();
    SemanticRelations localSemanticRelations = rootNode.getSemanticRelations();
    Iterator localIterator = localSemanticRelations.getPaths().iterator();
    SemanticPath localSemanticPath;
    SemanticNode localSemanticNode;
    do
    {
      boolean bool2;
      do
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localSemanticPath = (SemanticPath)localIterator.next();
        localSemanticNode = localSemanticPath.getPathNode();
        bool2 = localSemanticPath.isExplicitPath();
      } while ((!bool2) || (localSemanticNode == null));
      localObject1 = localSemanticNode.getOperations();
    } while (localObject1 == null);
    Object localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      Object localObject2 = (SemanticOperation)((Iterator)localObject1).next();
      Object localObject3 = ((SemanticOperation)localObject2).getOperator();
      Object localObject4 = "accstv";
      boolean bool4 = ((String)localObject3).equals(localObject4);
      if (bool4)
      {
        localObject3 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticOperatorObject;
        ((SemanticOperatorObject)localObject3).<init>(localSemanticNode, (SemanticOperation)localObject2);
        localObject2 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticOperandObject;
        localObject4 = localSemanticRelations.get(localSemanticPath);
        ((SemanticOperandObject)localObject2).<init>((SemanticNode)localObject4);
        localObject4 = sooList;
        SemanticOperationObject localSemanticOperationObject = new com/twelfthmile/malana/compiler/parser/semantic/SemanticOperationObject;
        localObject3 = returnFromOperatorList(localArrayList2, (SemanticOperatorObject)localObject3);
        localObject2 = returnFromOperandList(localArrayList1, (SemanticOperandObject)localObject2);
        int i = size();
        localSemanticOperationObject.<init>((SemanticOperatorObject)localObject3, (SemanticOperandObject)localObject2, i);
        ((SemanticOperationObjectList)localObject4).add(localSemanticOperationObject);
      }
    }
  }
  
  List getSdoStack()
  {
    return sdoStack;
  }
  
  public void runOperations(SemanticSeedConstants paramSemanticSeedConstants)
  {
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    ArrayList localArrayList2 = new java/util/ArrayList;
    localArrayList2.<init>();
    ArrayList localArrayList3 = new java/util/ArrayList;
    localArrayList3.<init>();
    Object localObject2 = getSdoStack().iterator();
    Object localObject3;
    Object localObject4;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = (SemanticDataObject)((Iterator)localObject2).next();
      localObject4 = ((SemanticDataObject)localObject3).getOperations();
      if (localObject4 != null) {
        ((List)localObject1).add(localObject3);
      }
      localObject4 = ((SemanticDataObject)localObject3).getType();
      if (localObject4 != null) {
        localArrayList1.add(localObject3);
      }
    }
    localObject1 = ((List)localObject1).iterator();
    boolean bool2 = ((Iterator)localObject1).hasNext();
    if (bool2)
    {
      localObject2 = (SemanticDataObject)((Iterator)localObject1).next();
      localObject3 = ((SemanticDataObject)localObject2).getOperations().iterator();
      Object localObject6;
      boolean bool4;
      do
      {
        do
        {
          boolean bool3 = ((Iterator)localObject3).hasNext();
          if (!bool3) {
            break;
          }
          localObject4 = (SemanticOperation)((Iterator)localObject3).next();
          localObject5 = ((SemanticOperation)localObject4).getOperator();
          localObject6 = "relate";
          bool4 = ((String)localObject5).equals(localObject6);
        } while (bool4);
        localObject5 = ((SemanticOperation)localObject4).getOperator();
        localObject6 = "accstv";
        bool4 = ((String)localObject5).equals(localObject6);
      } while (bool4);
      Object localObject5 = localArrayList1.iterator();
      for (;;)
      {
        boolean bool5 = ((Iterator)localObject5).hasNext();
        if (!bool5) {
          break;
        }
        localObject6 = (SemanticDataObject)((Iterator)localObject5).next();
        Object localObject7 = ((SemanticOperation)localObject4).getType();
        Object localObject8 = ((SemanticDataObject)localObject6).getType();
        boolean bool6 = ((List)localObject7).contains(localObject8);
        if (bool6)
        {
          localObject7 = ((SemanticOperation)localObject4).getNames();
          localObject8 = "*";
          bool6 = ((List)localObject7).contains(localObject8);
          if (!bool6)
          {
            localObject7 = ((SemanticOperation)localObject4).getNames();
            bool6 = ((SemanticDataObject)localObject6).check((List)localObject7);
            if (!bool6) {}
          }
          else
          {
            int i = ((SemanticOperation)localObject4).getDir();
            int j = 1;
            if (i == j)
            {
              i = ((SemanticDataObject)localObject6).getIndex();
              k = ((SemanticDataObject)localObject2).getIndex();
              if (i > k)
              {
                i = ((SemanticOperation)localObject4).getScope();
                if (i == j) {
                  break label521;
                }
                i = ((SemanticDataObject)localObject6).getIndex();
                k = ((SemanticDataObject)localObject2).getIndex();
                i -= k;
                if (i == j) {
                  break label521;
                }
              }
            }
            i = ((SemanticOperation)localObject4).getDir();
            int k = -1;
            if (i == k)
            {
              i = ((SemanticDataObject)localObject6).getIndex();
              k = ((SemanticDataObject)localObject2).getIndex();
              if (i < k)
              {
                i = ((SemanticOperation)localObject4).getScope();
                if (i == j) {
                  break label521;
                }
                i = ((SemanticDataObject)localObject2).getIndex();
                k = ((SemanticDataObject)localObject6).getIndex();
                i -= k;
                if (i == j) {
                  break label521;
                }
              }
            }
            i = ((SemanticOperation)localObject4).getDir();
            if (i == 0)
            {
              label521:
              localObject7 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticOperatorObject;
              localObject8 = search((SemanticDataObject)localObject2);
              ((SemanticOperatorObject)localObject7).<init>((SemanticNode)localObject8, (SemanticOperation)localObject4);
              localObject8 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticOperandObject;
              localObject6 = search((SemanticDataObject)localObject6);
              ((SemanticOperandObject)localObject8).<init>((SemanticNode)localObject6);
              localObject6 = sooList;
              SemanticOperationObject localSemanticOperationObject = new com/twelfthmile/malana/compiler/parser/semantic/SemanticOperationObject;
              localObject7 = returnFromOperatorList(localArrayList3, (SemanticOperatorObject)localObject7);
              localObject8 = returnFromOperandList(localArrayList2, (SemanticOperandObject)localObject8);
              int m = size();
              localSemanticOperationObject.<init>((SemanticOperatorObject)localObject7, (SemanticOperandObject)localObject8, m);
              ((SemanticOperationObjectList)localObject6).add(localSemanticOperationObject);
            }
          }
        }
      }
    }
    sooList.runOperations(paramSemanticSeedConstants);
  }
  
  SemanticNode search(SemanticDataObject paramSemanticDataObject)
  {
    SemanticNode localSemanticNode = rootNode;
    return search(localSemanticNode, paramSemanticDataObject);
  }
  
  SemanticNode search(SemanticNode paramSemanticNode, SemanticDataObject paramSemanticDataObject)
  {
    boolean bool = paramSemanticNode.hasSDO(paramSemanticDataObject);
    if (bool) {
      return paramSemanticNode;
    }
    paramSemanticNode = paramSemanticNode.getNodes().iterator();
    SemanticNode localSemanticNode;
    do
    {
      bool = paramSemanticNode.hasNext();
      if (!bool) {
        break;
      }
      localSemanticNode = (SemanticNode)paramSemanticNode.next();
      localSemanticNode = search(localSemanticNode, paramSemanticDataObject);
    } while (localSemanticNode == null);
    return localSemanticNode;
    return null;
  }
  
  void setVals()
  {
    Iterator localIterator = sdoStack.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      SemanticDataObject localSemanticDataObject = (SemanticDataObject)localIterator.next();
      localSemanticDataObject.setVals();
    }
  }
  
  int size()
  {
    return sdoStack.size();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{\"SemanticTree\":");
    SemanticNode localSemanticNode = rootNode;
    localStringBuilder.append(localSemanticNode);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticTree
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */