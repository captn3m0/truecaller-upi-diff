package com.twelfthmile.malana.compiler.parser.semantic;

public class SemanticConstants
{
  public static final String CHILD = "child";
  public static final String CHILD_PATH = "child_path";
  public static final int DIM_STATUS = 1;
  public static final int DIM_TENSE = 10;
  public static final int DIM_TYPE = 100;
  public static final int DIR_BACK = 255;
  public static final int DIR_FORW = 1;
  public static final int DIR_NEUT = 0;
  public static final String NODE_NAME = "name";
  public static final String NODE_STR = "str";
  public static final String NODE_TOKEN = "token";
  public static final int OPERAND = 1;
  public static final String OPERATION = "operation";
  public static final int OPERATOR = 0;
  public static final String OPR_ACCSTV = "accstv";
  public static final String OPR_ATTACH = "attach";
  public static final String OPR_CUSTOM = "custom";
  public static final String OPR_GT = "gt";
  public static final String OPR_MULT = "mult";
  public static final String OPR_RELATE = "relate";
  public static final String PARENT = "parent";
  public static final String PARENT_PATH = "parent_path";
  public static final String PREDICATE = "predicate";
  public static final String RULE_THAT = "that";
  public static final String RULE_THIS = "this";
  public static final String SEM = "sem";
  public static final String SIBLING = "sibling";
  public static final String SUBJECT = "subject";
  public static final String SYN = "syn";
  public static final int TENSE_FUT = 20;
  public static final int TENSE_PRE = 10;
  public static final int TENSE_PST = 0;
  public static final int TENSE_QST = 40;
  public static final int TENSE_REQ = 30;
  public static final int TYPE_BNK = 100;
  public static final int TYPE_NOT = 0;
  public static final int TYPE_OFR = 600;
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticConstants
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */