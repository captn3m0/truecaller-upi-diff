package com.twelfthmile.malana.compiler.parser.semantic;

public class SemanticOperationObjectList
{
  private SemanticOperationQueue sooList;
  
  public SemanticOperationObjectList()
  {
    SemanticOperationQueue localSemanticOperationQueue = new com/twelfthmile/malana/compiler/parser/semantic/SemanticOperationQueue;
    localSemanticOperationQueue.<init>();
    sooList = localSemanticOperationQueue;
  }
  
  public void add(SemanticOperationObject paramSemanticOperationObject)
  {
    sooList.add(paramSemanticOperationObject);
  }
  
  public void runOperations(SemanticSeedConstants paramSemanticSeedConstants)
  {
    for (;;)
    {
      Object localObject = sooList.isEmpty();
      boolean bool = ((Boolean)localObject).booleanValue();
      if (bool) {
        break;
      }
      localObject = sooList.poll();
      int i = ((SemanticOperationObject)localObject).runOperations(paramSemanticSeedConstants);
      if (i != 0)
      {
        String str1 = ((SemanticOperationObject)localObject).getOperator();
        String str2 = "attach";
        i = str1.equals(str2);
        if (i != 0)
        {
          i = 1;
          ((SemanticOperationObject)localObject).setLock(i);
        }
        else
        {
          int j = 0;
          str1 = null;
          ((SemanticOperationObject)localObject).setLock(0);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticOperationObjectList
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */