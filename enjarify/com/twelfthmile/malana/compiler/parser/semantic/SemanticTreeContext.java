package com.twelfthmile.malana.compiler.parser.semantic;

public class SemanticTreeContext
{
  private SemanticNode primeSubject;
  
  public void setSubject(SemanticPath paramSemanticPath, SemanticNode paramSemanticNode)
  {
    paramSemanticPath = primeSubject;
    if (paramSemanticPath == null) {
      primeSubject = paramSemanticNode;
    }
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticTreeContext
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */