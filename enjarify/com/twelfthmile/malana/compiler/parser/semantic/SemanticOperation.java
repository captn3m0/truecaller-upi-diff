package com.twelfthmile.malana.compiler.parser.semantic;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class SemanticOperation
{
  private int dim;
  private int dir;
  private List names;
  private String operator;
  private int scope;
  private List semanticRules;
  private List types;
  
  public SemanticOperation(JSONObject paramJSONObject, SemanticSeedConstants paramSemanticSeedConstants)
  {
    Object localObject1 = paramJSONObject.getString("operator");
    operator = ((String)localObject1);
    localObject1 = "dim";
    boolean bool1 = paramJSONObject.has((String)localObject1);
    int j = 1;
    if (bool1)
    {
      localObject1 = paramJSONObject.getString("dim");
      bool1 = paramSemanticSeedConstants.containsKey((String)localObject1);
      int k;
      if (bool1)
      {
        localObject1 = paramJSONObject.getString("dim");
        k = paramSemanticSeedConstants.get((String)localObject1);
        dim = k;
      }
      else
      {
        paramSemanticSeedConstants = paramJSONObject.getString("dim");
        k = Integer.parseInt(paramSemanticSeedConstants);
        dim = k;
      }
    }
    else
    {
      dim = j;
    }
    paramSemanticSeedConstants = paramJSONObject.getJSONObject("condition");
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    types = ((List)localObject1);
    localObject1 = paramSemanticSeedConstants.getJSONArray("type");
    int n = 0;
    int i1 = 0;
    Object localObject2 = null;
    int i2;
    Object localObject3;
    String str;
    for (;;)
    {
      i2 = ((JSONArray)localObject1).length();
      if (i1 >= i2) {
        break;
      }
      localObject3 = types;
      str = ((JSONArray)localObject1).getString(i1);
      ((List)localObject3).add(str);
      i1 += 1;
    }
    localObject1 = paramSemanticSeedConstants.getJSONArray("name");
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    names = ((List)localObject2);
    i1 = 0;
    localObject2 = null;
    for (;;)
    {
      i2 = ((JSONArray)localObject1).length();
      if (i1 >= i2) {
        break;
      }
      localObject3 = names;
      str = ((JSONArray)localObject1).getString(i1);
      ((List)localObject3).add(str);
      i1 += 1;
    }
    localObject1 = "dir";
    bool1 = paramSemanticSeedConstants.has((String)localObject1);
    int i;
    if (bool1)
    {
      localObject1 = "dir";
      i = paramSemanticSeedConstants.getInt((String)localObject1);
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    dir = i;
    localObject1 = "scope";
    boolean bool2 = paramSemanticSeedConstants.has((String)localObject1);
    if (bool2)
    {
      localObject1 = "scope";
      j = paramSemanticSeedConstants.getInt((String)localObject1);
    }
    scope = j;
    paramSemanticSeedConstants = "rules";
    boolean bool3 = paramJSONObject.has(paramSemanticSeedConstants);
    if (bool3)
    {
      paramSemanticSeedConstants = new java/util/ArrayList;
      paramSemanticSeedConstants.<init>();
      semanticRules = paramSemanticSeedConstants;
      paramSemanticSeedConstants = "rules";
      paramJSONObject = paramJSONObject.getJSONArray(paramSemanticSeedConstants);
      for (;;)
      {
        int m = paramJSONObject.length();
        if (n >= m) {
          break;
        }
        paramSemanticSeedConstants = paramJSONObject.getJSONObject(n);
        localObject1 = semanticRules;
        SemanticRule localSemanticRule = new com/twelfthmile/malana/compiler/parser/semantic/SemanticRule;
        localObject2 = paramSemanticSeedConstants.getJSONArray("in");
        localObject3 = "out";
        paramSemanticSeedConstants = paramSemanticSeedConstants.getString((String)localObject3);
        localSemanticRule.<init>((JSONArray)localObject2, paramSemanticSeedConstants);
        ((List)localObject1).add(localSemanticRule);
        n += 1;
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (SemanticOperation)paramObject;
        int i = dim;
        int j = dim;
        if (i != j) {
          return false;
        }
        localObject1 = operator;
        localObject2 = operator;
        boolean bool2 = ((String)localObject1).equals(localObject2);
        if (!bool2) {
          return false;
        }
        localObject1 = types;
        if (localObject1 != null)
        {
          localObject2 = types;
          bool2 = ((List)localObject1).equals(localObject2);
          if (bool2) {
            break label129;
          }
        }
        else
        {
          localObject1 = types;
          if (localObject1 == null) {
            break label129;
          }
        }
        return false;
        label129:
        localObject1 = names;
        if (localObject1 != null)
        {
          localObject2 = names;
          bool2 = ((List)localObject1).equals(localObject2);
          if (bool2) {
            break label173;
          }
        }
        else
        {
          localObject1 = names;
          if (localObject1 == null) {
            break label173;
          }
        }
        return false;
        label173:
        localObject1 = semanticRules;
        if (localObject1 != null)
        {
          paramObject = semanticRules;
          return ((List)localObject1).equals(paramObject);
        }
        paramObject = semanticRules;
        if (paramObject == null) {
          return bool1;
        }
        return false;
      }
    }
    return false;
  }
  
  public int getDim()
  {
    return dim;
  }
  
  public int getDir()
  {
    return dir;
  }
  
  public List getNames()
  {
    return names;
  }
  
  public String getOperator()
  {
    return operator;
  }
  
  public int getScope()
  {
    return scope;
  }
  
  public List getSemanticRules()
  {
    return semanticRules;
  }
  
  public List getType()
  {
    return types;
  }
  
  public int hashCode()
  {
    String str = operator;
    int i = str.hashCode() * 31;
    List localList = types;
    int j = 0;
    if (localList != null)
    {
      k = localList.hashCode();
    }
    else
    {
      k = 0;
      localList = null;
    }
    i = (i + k) * 31;
    int k = dim;
    i = (i + k) * 31;
    localList = names;
    if (localList != null)
    {
      k = localList.hashCode();
    }
    else
    {
      k = 0;
      localList = null;
    }
    i = (i + k) * 31;
    localList = semanticRules;
    if (localList != null) {
      j = localList.hashCode();
    }
    return i + j;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{\"operator\":\"");
    Object localObject = operator;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append("\",\"type\":\"");
    localObject = types;
    localStringBuilder.append(localObject);
    localStringBuilder.append("\",\"names\":\"");
    localObject = names;
    localStringBuilder.append(localObject);
    localStringBuilder.append("\",\"dim\":\"");
    int i = dim;
    localStringBuilder.append(i);
    localStringBuilder.append("\"}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticOperation
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */