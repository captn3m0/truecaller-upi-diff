package com.twelfthmile.malana.compiler.parser.semantic;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SemanticOperationObject
{
  private int score;
  private SemanticOperandObject semanticOperandObject;
  private SemanticOperatorObject semanticOperatorObject;
  
  public SemanticOperationObject(SemanticOperatorObject paramSemanticOperatorObject, SemanticOperandObject paramSemanticOperandObject, int paramInt)
  {
    semanticOperatorObject = paramSemanticOperatorObject;
    semanticOperandObject = paramSemanticOperandObject;
    setScore(paramInt);
  }
  
  private int getOperatorScore(String paramString)
  {
    int i = paramString.hashCode();
    int j = -1423447532;
    int k = 1;
    int m = 2;
    int n = 3;
    int i1 = 4;
    String str;
    if (i != j)
    {
      j = -1407259067;
      if (i != j)
      {
        j = -1349088399;
        if (i != j)
        {
          j = 3309;
          boolean bool1;
          if (i != j)
          {
            j = 3363120;
            if (i == j)
            {
              str = "mult";
              bool1 = paramString.equals(str);
              if (bool1)
              {
                bool1 = true;
                break label181;
              }
            }
          }
          else
          {
            str = "gt";
            bool1 = paramString.equals(str);
            if (bool1)
            {
              int i2 = 2;
              break label181;
            }
          }
        }
        else
        {
          str = "custom";
          boolean bool2 = paramString.equals(str);
          if (bool2)
          {
            int i3 = 3;
            break label181;
          }
        }
      }
      else
      {
        str = "attach";
        boolean bool3 = paramString.equals(str);
        if (bool3)
        {
          int i4 = 4;
          break label181;
        }
      }
    }
    else
    {
      str = "accstv";
      boolean bool4 = paramString.equals(str);
      if (bool4)
      {
        bool4 = false;
        paramString = null;
        break label181;
      }
    }
    int i5 = -1;
    switch (i5)
    {
    default: 
      return 0;
    case 4: 
      return k;
    case 3: 
      return m;
    case 2: 
      return n;
    case 1: 
      label181:
      return i1;
    }
    return 5;
  }
  
  private boolean ifIn(boolean[] paramArrayOfBoolean)
  {
    int i = 0;
    for (;;)
    {
      int j = paramArrayOfBoolean.length;
      if (i >= j) {
        break;
      }
      j = paramArrayOfBoolean[i];
      if (j == 0) {
        return false;
      }
      i += 1;
    }
    return true;
  }
  
  private boolean inOutParity(String paramString, String[] paramArrayOfString, boolean[] paramArrayOfBoolean)
  {
    int i = paramArrayOfString.length;
    int j = 0;
    int k = 0;
    while (j < i)
    {
      String str = paramArrayOfString[j];
      boolean bool = str.equals(paramString);
      if (bool)
      {
        int m = paramArrayOfBoolean[k];
        if (m == 0) {
          return false;
        }
      }
      k += 1;
      j += 1;
    }
    return true;
  }
  
  private int performCustomOperation(SemanticRule paramSemanticRule, String paramString, SemanticSeedConstants paramSemanticSeedConstants)
  {
    SemanticOperationObject localSemanticOperationObject = this;
    Object localObject1 = paramSemanticSeedConstants;
    Object localObject2 = paramSemanticRule.getIn();
    int i = localObject2.length;
    Object localObject3 = { i, 3 };
    localObject3 = (String[][])Array.newInstance(String.class, (int[])localObject3);
    int j = 0;
    Object localObject4 = null;
    int n = 0;
    Object localObject5 = null;
    for (;;)
    {
      i3 = localObject2.length;
      if (n >= i3) {
        break;
      }
      arrayOfString = localObject2[n];
      localObject6 = ",";
      arrayOfString = arrayOfString.split((String)localObject6);
      localObject3[n] = arrayOfString;
      n += 1;
    }
    localObject5 = paramSemanticRule.getOut().split(",");
    int i3 = localObject2.length;
    String[] arrayOfString = new String[i3];
    int i4 = localObject2.length;
    Object localObject6 = new SemStrInt[i4];
    int i5 = localObject2.length;
    SemStrInt[] arrayOfSemStrInt1 = new SemStrInt[i5];
    Object localObject7 = new java/util/HashMap;
    ((HashMap)localObject7).<init>();
    int i6 = 0;
    SemStrInt[] arrayOfSemStrInt2 = null;
    Object localObject8 = null;
    int i7 = 0;
    String str = null;
    int i13;
    Object localObject9;
    Object localObject10;
    for (;;)
    {
      i9 = localObject2.length;
      i12 = 2;
      i13 = 1;
      if (i6 >= i9) {
        break;
      }
      localObject9 = localObject3[i6][0];
      arrayOfString[i6] = localObject9;
      localObject9 = arrayOfString[i6];
      localObject10 = "this";
      boolean bool5 = ((String)localObject9).equals(localObject10);
      if (!bool5)
      {
        localObject10 = arrayOfString[i6];
        localObject9 = "that";
        bool5 = ((String)localObject10).equals(localObject9);
        if (!bool5) {
          if (localObject8 == null)
          {
            localObject10 = arrayOfString[i6];
            localObject8 = localObject10;
          }
          else
          {
            localObject10 = arrayOfString[i6];
            bool5 = ((String)localObject8).equals(localObject10);
            if (!bool5) {
              i7 = 1;
            }
          }
        }
      }
      localObject10 = new com/twelfthmile/malana/compiler/parser/semantic/SemStrInt;
      ((SemStrInt)localObject10).<init>();
      localObject6[i6] = localObject10;
      localObject10 = localObject3[i6][i13];
      localObject9 = localObject6[i6];
      localSemanticOperationObject.setter((SemanticSeedConstants)localObject1, (String)localObject10, (SemStrInt)localObject9);
      localObject10 = new com/twelfthmile/malana/compiler/parser/semantic/SemStrInt;
      ((SemStrInt)localObject10).<init>();
      arrayOfSemStrInt1[i6] = localObject10;
      localObject10 = localObject3[i6][i12];
      localObject9 = arrayOfSemStrInt1[i6];
      localSemanticOperationObject.setter((SemanticSeedConstants)localObject1, (String)localObject10, (SemStrInt)localObject9);
      i6 += 1;
    }
    localObject3 = localObject5[0];
    int i14 = (localObject5.length + -1) / i12;
    if (i14 <= 0) {
      return 0;
    }
    arrayOfSemStrInt2 = new SemStrInt[i14];
    localObject8 = new SemStrInt[i14];
    int i9 = 1;
    int i12 = 0;
    Object localObject11 = null;
    for (;;)
    {
      j = localObject5.length;
      if (i9 >= j) {
        break;
      }
      localObject4 = new com/twelfthmile/malana/compiler/parser/semantic/SemStrInt;
      ((SemStrInt)localObject4).<init>();
      arrayOfSemStrInt2[i12] = localObject4;
      localObject4 = localObject5[i9];
      i15 = i14;
      localObject10 = arrayOfSemStrInt2[i12];
      localSemanticOperationObject.setter((SemanticSeedConstants)localObject1, (String)localObject4, (SemStrInt)localObject10);
      localObject4 = new com/twelfthmile/malana/compiler/parser/semantic/SemStrInt;
      ((SemStrInt)localObject4).<init>();
      localObject8[i12] = localObject4;
      i9 += 1;
      localObject4 = localObject5[i9];
      localObject10 = localObject8[i12];
      localSemanticOperationObject.setter((SemanticSeedConstants)localObject1, (String)localObject4, (SemStrInt)localObject10);
      i12 += 1;
      i9 += 1;
    }
    int i15 = i14;
    int i16 = localObject2.length;
    localObject1 = new boolean[i16];
    j = 0;
    localObject4 = null;
    boolean bool1;
    for (;;)
    {
      n = localObject2.length;
      if (j >= n) {
        break;
      }
      localObject5 = arrayOfString[j];
      localObject10 = "this";
      bool1 = ((String)localObject5).equals(localObject10);
      if (bool1)
      {
        localObject5 = semanticOperatorObject;
        localObject10 = localObject6[j];
        localObject5 = ((SemanticOperatorObject)localObject5).getVal((SemStrInt)localObject10);
        localObject10 = arrayOfSemStrInt1[j];
        bool1 = ((SemStrInt)localObject5).equalsOrContains((SemStrInt)localObject10);
        localObject1[j] = bool1;
      }
      else
      {
        localObject5 = arrayOfString[j];
        localObject10 = "that";
        bool1 = ((String)localObject5).equals(localObject10);
        if (bool1)
        {
          localObject5 = semanticOperandObject;
          localObject10 = localObject6[j];
          localObject5 = ((SemanticOperandObject)localObject5).getVal((SemStrInt)localObject10);
          localObject10 = arrayOfSemStrInt1[j];
          bool1 = ((SemStrInt)localObject5).equalsOrContains((SemStrInt)localObject10);
          localObject1[j] = bool1;
        }
      }
      j += 1;
    }
    localObject4 = "this";
    int k = ((String)localObject3).equals(localObject4);
    if (k != 0)
    {
      localObject10 = semanticOperatorObject;
    }
    else
    {
      i14 = 0;
      localObject10 = null;
    }
    localObject4 = "that";
    k = ((String)localObject3).equals(localObject4);
    if (k != 0) {
      localObject10 = semanticOperandObject;
    }
    localObject4 = "attach";
    localObject5 = paramString;
    k = paramString.equals(localObject4);
    if ((k != 0) && (i7 != 0))
    {
      localObject4 = semanticOperatorObject.getNodesAsOperand();
      localObject5 = semanticOperandObject;
      ((List)localObject4).add(localObject5);
      localObject4 = ((List)localObject4).iterator();
      for (;;)
      {
        bool1 = ((Iterator)localObject4).hasNext();
        if (!bool1) {
          break;
        }
        localObject5 = (SemanticOperandObject)((Iterator)localObject4).next();
        i7 = localSemanticOperationObject.ifIn((boolean[])localObject1);
        if ((i7 != 0) && (localObject10 != null)) {
          break;
        }
        i7 = 0;
        str = null;
        for (;;)
        {
          i9 = localObject2.length;
          if (i7 >= i9) {
            break;
          }
          int i10 = localObject1[i7];
          if (i10 == 0)
          {
            localObject9 = ((SemanticOperandObject)localObject5).getName();
            localObject11 = arrayOfString[i7];
            boolean bool4 = ((String)localObject9).equals(localObject11);
            if (bool4)
            {
              localObject9 = arrayOfString[i7];
              bool4 = ((Map)localObject7).containsKey(localObject9);
              if (bool4)
              {
                localObject9 = arrayOfString[i7];
                localObject9 = (SemanticOperandObject)((Map)localObject7).get(localObject9);
                bool4 = ((SemanticOperandObject)localObject9).equals(localObject5);
                if (!bool4) {}
              }
              else
              {
                localObject9 = localObject6[i7];
                localObject9 = ((SemanticOperandObject)localObject5).getVal((SemStrInt)localObject9);
                localObject11 = arrayOfSemStrInt1[i7];
                bool4 = ((SemStrInt)localObject9).equalsOrContains((SemStrInt)localObject11);
                localObject1[i7] = bool4;
                int i11 = localObject1[i7];
                if (i11 != 0)
                {
                  localObject9 = arrayOfString[i7];
                  ((Map)localObject7).put(localObject9, localObject5);
                }
              }
            }
          }
          int i8;
          i7 += 1;
        }
        if (localObject10 == null)
        {
          str = ((SemanticOperandObject)localObject5).getName();
          boolean bool3 = str.equals(localObject3);
          if (bool3)
          {
            bool3 = localSemanticOperationObject.inOutParity((String)localObject3, arrayOfString, (boolean[])localObject1);
            if (bool3) {
              localObject10 = localObject5;
            }
          }
        }
      }
    }
    k = 0;
    localObject4 = null;
    for (;;)
    {
      int i1 = localObject2.length;
      if (k >= i1) {
        break;
      }
      int i2 = localObject1[k];
      if (i2 == 0)
      {
        localObject5 = semanticOperandObject.getName();
        localObject7 = arrayOfString[k];
        boolean bool2 = ((String)localObject5).equals(localObject7);
        if (bool2)
        {
          localObject5 = semanticOperandObject;
          localObject7 = localObject6[k];
          localObject5 = ((SemanticOperandObject)localObject5).getVal((SemStrInt)localObject7);
          localObject7 = arrayOfSemStrInt1[k];
          bool2 = ((SemStrInt)localObject5).equalsOrContains((SemStrInt)localObject7);
          localObject1[k] = bool2;
        }
      }
      int m;
      k += 1;
    }
    boolean bool7;
    if (localObject10 == null)
    {
      localObject2 = semanticOperandObject.getName();
      bool7 = ((String)localObject2).equals(localObject3);
      if (bool7)
      {
        bool7 = localSemanticOperationObject.inOutParity((String)localObject3, arrayOfString, (boolean[])localObject1);
        if (bool7) {
          localObject10 = semanticOperandObject;
        }
      }
    }
    boolean bool6 = localSemanticOperationObject.ifIn((boolean[])localObject1);
    if ((bool6) && (localObject10 != null))
    {
      bool6 = i15;
      bool7 = false;
      localObject2 = null;
      while (bool7 < bool6)
      {
        localObject3 = arrayOfSemStrInt2[bool7];
        localObject4 = localObject8[bool7];
        ((SemanticOper)localObject10).setVal((SemStrInt)localObject3, (SemStrInt)localObject4);
        int i17;
        bool7 += true;
      }
      return i13;
    }
    return 0;
  }
  
  private void setScore(int paramInt)
  {
    String str = semanticOperatorObject.getOperator();
    int i = getOperatorScore(str);
    int j = semanticOperandObject.getInd();
    int k = semanticOperatorObject.getInd();
    j = Math.abs(j - k);
    paramInt -= j;
    i += paramInt;
    score = i;
  }
  
  private void setter(SemanticSeedConstants paramSemanticSeedConstants, String paramString, SemStrInt paramSemStrInt)
  {
    Object localObject1 = ";";
    boolean bool1 = paramString.contains((CharSequence)localObject1);
    int m;
    if (bool1)
    {
      paramString = paramString.split(";");
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      int i = paramString.length;
      int j = 0;
      while (j < i)
      {
        Object localObject2 = paramString[j];
        boolean bool2 = paramSemanticSeedConstants.containsKey((String)localObject2);
        if (bool2)
        {
          int k = paramSemanticSeedConstants.get((String)localObject2);
          localObject2 = Integer.valueOf(k);
          ((List)localObject1).add(localObject2);
        }
        j += 1;
      }
      m = paramString.length;
      i = ((List)localObject1).size();
      if (m == i)
      {
        paramSemStrInt.setIL((List)localObject1);
        return;
      }
      paramSemStrInt.set(paramString);
      return;
    }
    bool1 = paramSemanticSeedConstants.containsKey(paramString);
    if (bool1)
    {
      m = paramSemanticSeedConstants.get(paramString);
      paramSemStrInt.set(m);
      return;
    }
    paramSemanticSeedConstants = "-?\\d+";
    boolean bool3 = paramString.matches(paramSemanticSeedConstants);
    if (bool3)
    {
      int n = Integer.parseInt(paramString);
      paramSemStrInt.set(n);
      return;
    }
    paramSemStrInt.set(paramString);
  }
  
  public String getOperator()
  {
    return semanticOperatorObject.getOperator();
  }
  
  public int getScore()
  {
    return score;
  }
  
  public boolean isLock()
  {
    Object localObject = semanticOperatorObject;
    boolean bool = ((SemanticOperatorObject)localObject).isLock();
    if (!bool)
    {
      localObject = semanticOperandObject;
      bool = ((SemanticOperandObject)localObject).isLock();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public boolean runOperations(SemanticSeedConstants paramSemanticSeedConstants)
  {
    boolean bool1 = false;
    int i = 1;
    try
    {
      boolean bool2 = isLock();
      if (!bool2)
      {
        Object localObject1 = semanticOperatorObject;
        int j = ((SemanticOperatorObject)localObject1).getDim();
        Object localObject2 = semanticOperandObject;
        int m = ((SemanticOperandObject)localObject2).getVal(j);
        Object localObject3 = semanticOperatorObject;
        int i1 = ((SemanticOperatorObject)localObject3).getVal(j);
        Object localObject4 = semanticOperatorObject;
        localObject4 = ((SemanticOperatorObject)localObject4).getOperator();
        int i2 = ((String)localObject4).hashCode();
        int i3 = -1423447532;
        String str;
        if (i2 != i3)
        {
          i3 = -1407259067;
          if (i2 != i3)
          {
            i3 = -1349088399;
            boolean bool4;
            if (i2 != i3)
            {
              i3 = 3309;
              if (i2 != i3)
              {
                i3 = 3363120;
                if (i2 == i3)
                {
                  str = "mult";
                  bool4 = ((String)localObject4).equals(str);
                  if (bool4)
                  {
                    bool4 = false;
                    localObject4 = null;
                    break label263;
                  }
                }
              }
              else
              {
                str = "gt";
                bool4 = ((String)localObject4).equals(str);
                if (bool4)
                {
                  bool4 = true;
                  break label263;
                }
              }
            }
            else
            {
              str = "custom";
              bool4 = ((String)localObject4).equals(str);
              if (bool4)
              {
                int i4 = 4;
                break label263;
              }
            }
          }
          else
          {
            str = "attach";
            boolean bool5 = ((String)localObject4).equals(str);
            if (bool5)
            {
              int i5 = 2;
              break label263;
            }
          }
        }
        else
        {
          str = "accstv";
          boolean bool6 = ((String)localObject4).equals(str);
          if (bool6)
          {
            i6 = 3;
            break label263;
          }
        }
        int i6 = -1;
        label263:
        int k;
        int n;
        switch (i6)
        {
        default: 
          break;
        case 2: 
          localObject1 = semanticOperatorObject;
          localObject2 = semanticOperandObject;
          localObject2 = ((SemanticOperandObject)localObject2).getName();
          k = ((SemanticOperatorObject)localObject1).hasNode((String)localObject2);
          if (k != 0) {
            break;
          }
        case 3: 
        case 4: 
          localObject1 = semanticOperatorObject;
          localObject1 = ((SemanticOperatorObject)localObject1).getSemanticRules();
          localObject1 = ((List)localObject1).iterator();
          do
          {
            boolean bool3 = ((Iterator)localObject1).hasNext();
            if (!bool3) {
              break;
            }
            localObject2 = ((Iterator)localObject1).next();
            localObject2 = (SemanticRule)localObject2;
            localObject3 = semanticOperatorObject;
            localObject3 = ((SemanticOperatorObject)localObject3).getOperator();
            n = performCustomOperation((SemanticRule)localObject2, (String)localObject3, paramSemanticSeedConstants);
          } while (n != i);
          paramSemanticSeedConstants = semanticOperatorObject;
          paramSemanticSeedConstants = paramSemanticSeedConstants.getOperator();
          Object localObject5 = "attach";
          boolean bool7 = paramSemanticSeedConstants.equals(localObject5);
          if (bool7)
          {
            paramSemanticSeedConstants = semanticOperatorObject;
            localObject5 = semanticOperandObject;
            bool1 = paramSemanticSeedConstants.attach((SemanticOperandObject)localObject5);
          }
          break;
        case 1: 
          if (i1 > n)
          {
            paramSemanticSeedConstants = semanticOperandObject;
            paramSemanticSeedConstants.setVal(k, i1);
          }
          bool1 = true;
          break;
        case 0: 
          paramSemanticSeedConstants = semanticOperandObject;
          n *= i1;
          paramSemanticSeedConstants.setVal(k, n);
          bool1 = true;
        }
      }
    }
    catch (Exception paramSemanticSeedConstants)
    {
      paramSemanticSeedConstants.printStackTrace();
    }
    return bool1;
  }
  
  public void setLock(int paramInt)
  {
    boolean bool = true;
    if (paramInt == 0)
    {
      semanticOperatorObject.setLock(bool);
      return;
    }
    semanticOperandObject.setLock(bool);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticOperationObject
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */