package com.twelfthmile.malana.compiler.parser.semantic;

public class SemanticPath
{
  public static final int ATTACH = 3;
  public static final int EXPLICIT = 2;
  public static final int IMPLICIT = 1;
  public static final int PRIMITIVE;
  private int index;
  private String pathName;
  private SemanticNode pathNode;
  private int type;
  
  public SemanticPath(String paramString, SemanticNode paramSemanticNode, int paramInt1, int paramInt2)
  {
    pathName = paramString;
    pathNode = paramSemanticNode;
    index = paramInt1;
    type = paramInt2;
  }
  
  private String getTypeStr(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "";
    case 3: 
      return "ATTACH";
    case 2: 
      return "EXPLICIT";
    case 1: 
      return "IMPLICIT";
    }
    return "PRIMITIVE";
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    String str = null;
    if (paramObject != null)
    {
      Class localClass1 = getClass();
      Class localClass2 = paramObject.getClass();
      if (localClass1 == localClass2)
      {
        paramObject = (SemanticPath)paramObject;
        int i = index;
        int j = index;
        if (i != j) {
          return false;
        }
        str = pathName;
        paramObject = pathName;
        return str.equals(paramObject);
      }
    }
    return false;
  }
  
  public String getPathName()
  {
    return pathName;
  }
  
  public SemanticNode getPathNode()
  {
    return pathNode;
  }
  
  public int getType()
  {
    return type;
  }
  
  public int hashCode()
  {
    int i = pathName.hashCode() * 31;
    int j = index;
    return i + j;
  }
  
  boolean isExplicitPath()
  {
    int i = type;
    int j = 2;
    return i == j;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{pathName='");
    String str = pathName;
    localStringBuilder.append(str);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", pathNodeStr='");
    Object localObject = pathNode;
    if (localObject != null) {
      localObject = ((SemanticNode)localObject).getStr();
    } else {
      localObject = "";
    }
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(c);
    localStringBuilder.append(", index=");
    int i = index;
    localStringBuilder.append(i);
    localStringBuilder.append(", type='");
    i = type;
    localObject = getTypeStr(i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(c);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticPath
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */