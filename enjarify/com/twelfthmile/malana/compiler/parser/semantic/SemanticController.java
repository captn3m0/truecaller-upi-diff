package com.twelfthmile.malana.compiler.parser.semantic;

import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.types.SemanticSeed;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SemanticController
{
  private int sdoListSize = 0;
  private SemanticSeedConstants semConstants;
  private HashMap semSeedMap;
  private List semanticForest;
  private SemanticTree semanticTree;
  
  public SemanticController(SemanticSeed paramSemanticSeed)
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    semanticForest = ((List)localObject);
    localObject = new com/twelfthmile/malana/compiler/parser/semantic/SemanticTree;
    ((SemanticTree)localObject).<init>();
    semanticTree = ((SemanticTree)localObject);
    localObject = paramSemanticSeed.getSemanticMap();
    semSeedMap = ((HashMap)localObject);
    paramSemanticSeed = paramSemanticSeed.getSemanticConstants();
    semConstants = paramSemanticSeed;
  }
  
  private void add(SemanticDataObject paramSemanticDataObject)
  {
    semanticTree.add(paramSemanticDataObject);
    int i = sdoListSize + 1;
    sdoListSize = i;
  }
  
  public void add(GrammarDataObject paramGrammarDataObject)
  {
    try
    {
      Object localObject1 = semSeedMap;
      Object localObject2 = new java/lang/StringBuilder;
      Object localObject3 = "$";
      ((StringBuilder)localObject2).<init>((String)localObject3);
      localObject3 = labelAux;
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      boolean bool = ((HashMap)localObject1).containsKey(localObject2);
      Object localObject4;
      int i;
      if (bool)
      {
        localObject1 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticDataObject;
        localObject2 = semSeedMap;
        localObject3 = new java/lang/StringBuilder;
        localObject4 = "$";
        ((StringBuilder)localObject3).<init>((String)localObject4);
        localObject4 = labelAux;
        ((StringBuilder)localObject3).append((String)localObject4);
        localObject3 = ((StringBuilder)localObject3).toString();
        localObject2 = ((HashMap)localObject2).get(localObject3);
        localObject2 = (SemanticSeedObject)localObject2;
        i = sdoListSize;
        localObject4 = semConstants;
        ((SemanticDataObject)localObject1).<init>((SemanticSeedObject)localObject2, paramGrammarDataObject, i, (SemanticSeedConstants)localObject4);
        add((SemanticDataObject)localObject1);
        return;
      }
      localObject1 = str;
      if (localObject1 != null)
      {
        localObject1 = semSeedMap;
        localObject2 = str;
        localObject2 = ((String)localObject2).toLowerCase();
        bool = ((HashMap)localObject1).containsKey(localObject2);
        if (bool)
        {
          localObject1 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticDataObject;
          localObject2 = semSeedMap;
          localObject3 = str;
          localObject3 = ((String)localObject3).toLowerCase();
          localObject2 = ((HashMap)localObject2).get(localObject3);
          localObject2 = (SemanticSeedObject)localObject2;
          i = sdoListSize;
          localObject4 = semConstants;
          ((SemanticDataObject)localObject1).<init>((SemanticSeedObject)localObject2, paramGrammarDataObject, i, (SemanticSeedConstants)localObject4);
          add((SemanticDataObject)localObject1);
        }
      }
      return;
    }
    catch (Exception localException)
    {
      localException;
    }
  }
  
  public void end()
  {
    semanticTree.buildTree();
    Object localObject1 = semanticTree;
    Object localObject2 = semConstants;
    ((SemanticTree)localObject1).runOperations((SemanticSeedConstants)localObject2);
    semanticTree.setVals();
    localObject1 = semanticForest;
    localObject2 = semanticTree;
    ((List)localObject1).add(localObject2);
    localObject1 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticTree;
    ((SemanticTree)localObject1).<init>();
    semanticTree = ((SemanticTree)localObject1);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[");
    Iterator localIterator = semanticForest.iterator();
    for (;;)
    {
      i = localIterator.hasNext();
      if (i == 0) {
        break;
      }
      String str = ((SemanticTree)localIterator.next()).toString();
      localStringBuilder.append(str);
      str = ",";
      localStringBuilder.append(str);
    }
    int j = localStringBuilder.length();
    int i = 1;
    if (j > i)
    {
      int k = localStringBuilder.length() - i;
      localStringBuilder.setLength(k);
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */