package com.twelfthmile.malana.compiler.parser.semantic;

import com.twelfthmile.b.a.a;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

class SemanticNode
{
  private boolean isRoot = false;
  private SemanticNode parentNode;
  private SemanticPath parentPath;
  private SemanticDataObject sdo;
  private SemanticRelations semanticRelations;
  
  SemanticNode()
  {
    isRoot = true;
    SemanticRelations localSemanticRelations = new com/twelfthmile/malana/compiler/parser/semantic/SemanticRelations;
    localSemanticRelations.<init>();
    semanticRelations = localSemanticRelations;
  }
  
  SemanticNode(SemanticDataObject paramSemanticDataObject)
  {
    sdo = paramSemanticDataObject;
    paramSemanticDataObject = new com/twelfthmile/malana/compiler/parser/semantic/SemanticRelations;
    paramSemanticDataObject.<init>();
    semanticRelations = paramSemanticDataObject;
  }
  
  void addNode(String paramString, SemanticNode paramSemanticNode1, SemanticNode paramSemanticNode2, int paramInt)
  {
    addNode(paramString, paramSemanticNode1, paramSemanticNode2, paramInt, false);
  }
  
  void addNode(String paramString, SemanticNode paramSemanticNode1, SemanticNode paramSemanticNode2, int paramInt, boolean paramBoolean)
  {
    boolean bool = canHaveChildren();
    if ((bool) || (paramBoolean))
    {
      SemanticPath localSemanticPath = new com/twelfthmile/malana/compiler/parser/semantic/SemanticPath;
      int i;
      if (paramSemanticNode1 != null) {
        i = paramSemanticNode1.getIndex();
      } else {
        i = paramSemanticNode2.getIndex();
      }
      localSemanticPath.<init>(paramString, paramSemanticNode1, i, paramInt);
      paramSemanticNode2.setParent(localSemanticPath, this);
      paramString = semanticRelations;
      paramString.put(localSemanticPath, paramSemanticNode2);
    }
  }
  
  boolean attach(SemanticOperandObject paramSemanticOperandObject)
  {
    Object localObject1 = paramSemanticOperandObject.getNode();
    if (localObject1 != this)
    {
      localObject1 = paramSemanticOperandObject.getNode();
      boolean bool1 = hasNode((SemanticNode)localObject1);
      if (!bool1)
      {
        localObject1 = paramSemanticOperandObject.getNode().getParentPath();
        Object localObject2 = paramSemanticOperandObject.getNode();
        SemanticNode localSemanticNode1 = ((SemanticNode)localObject2).getParentNode();
        if (localObject1 != null)
        {
          int i = ((SemanticPath)localObject1).getType();
          j = 2;
          if (i == j)
          {
            localObject2 = ((SemanticPath)localObject1).getPathName();
            localSemanticNode2 = ((SemanticPath)localObject1).getPathNode();
            localSemanticNode3 = paramSemanticOperandObject.getNode();
            k = 2;
            bool2 = true;
            localObject1 = this;
            addNode((String)localObject2, localSemanticNode2, localSemanticNode3, k, bool2);
            break label146;
          }
        }
        localObject2 = "attach";
        int j = 0;
        SemanticNode localSemanticNode2 = null;
        SemanticNode localSemanticNode3 = paramSemanticOperandObject.getNode();
        int k = 3;
        boolean bool2 = true;
        localObject1 = this;
        addNode((String)localObject2, null, localSemanticNode3, k, bool2);
        label146:
        localObject1 = paramSemanticOperandObject.getNode();
        localSemanticNode1.removeNode((SemanticNode)localObject1);
        return true;
      }
    }
    return false;
  }
  
  boolean canHaveChildren()
  {
    int i = getChildrenCount();
    int j = getDim();
    if (i >= j)
    {
      boolean bool = isRoot;
      if (!bool)
      {
        String str1 = getType();
        String str2 = "nondet";
        bool = str1.equals(str2);
        if (!bool) {
          return false;
        }
      }
    }
    return true;
  }
  
  String canRelate(SemanticNode paramSemanticNode)
  {
    if (paramSemanticNode == null) {
      return null;
    }
    Iterator localIterator = getTypes().iterator();
    Object localObject;
    boolean bool2;
    do
    {
      int i;
      int j;
      do
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localObject = (String)localIterator.next();
        str1 = ",";
        localObject = ((String)localObject).split(str1);
        i = localObject.length;
        j = 2;
      } while (i != j);
      String str1 = localObject[1];
      String str2 = paramSemanticNode.getType();
      bool2 = str1.equals(str2);
    } while (!bool2);
    return localObject[0];
    return null;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (paramObject != null)
    {
      Object localObject = getClass();
      Class localClass = paramObject.getClass();
      if (localObject == localClass)
      {
        paramObject = (SemanticNode)paramObject;
        localObject = sdo;
        paramObject = sdo;
        return ((SemanticDataObject)localObject).equals(paramObject);
      }
    }
    return false;
  }
  
  public List gdoChildren()
  {
    return sdo.gdoChildren();
  }
  
  public GrammarDataObject gdoParent()
  {
    return sdo.gdoParent();
  }
  
  public List gdoSiblings()
  {
    return sdo.gdoSiblings();
  }
  
  String getAllStr()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    Object localObject1 = getStr();
    localStringBuilder.<init>((String)localObject1);
    localObject1 = getNodes().iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (SemanticNode)((Iterator)localObject1).next();
      String str = " ";
      localStringBuilder.append(str);
      localObject2 = ((SemanticNode)localObject2).getAllStr();
      localStringBuilder.append((String)localObject2);
    }
    return localStringBuilder.toString();
  }
  
  int getChildrenCount()
  {
    return semanticRelations.size();
  }
  
  int getDim()
  {
    Object localObject = sdo;
    if (localObject != null)
    {
      localObject = ((SemanticDataObject)localObject).getRelateOperation();
      if (localObject != null) {
        return sdo.getRelateOperation().getDim();
      }
    }
    return 0;
  }
  
  int getDir()
  {
    Object localObject = sdo;
    if (localObject != null)
    {
      localObject = ((SemanticDataObject)localObject).getRelateOperation();
      if (localObject != null) {
        return sdo.getRelateOperation().getDir();
      }
    }
    return 0;
  }
  
  public int getIndex()
  {
    return sdo.getIndex();
  }
  
  String getName()
  {
    return sdo.getName();
  }
  
  SemanticNode getNode(SemanticPath paramSemanticPath)
  {
    return semanticRelations.get(paramSemanticPath);
  }
  
  SemanticNode getNodeForName(String paramString)
  {
    Object localObject = sdo.getName();
    boolean bool1 = paramString.equals(localObject);
    if (bool1) {
      return this;
    }
    localObject = getNodes().iterator();
    SemanticNode localSemanticNode;
    do
    {
      boolean bool2 = ((Iterator)localObject).hasNext();
      if (!bool2) {
        break;
      }
      localSemanticNode = ((SemanticNode)((Iterator)localObject).next()).getNodeForName(paramString);
    } while (localSemanticNode == null);
    return localSemanticNode;
    return null;
  }
  
  Collection getNodes()
  {
    boolean bool = hasChild();
    if (!bool)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      return localArrayList;
    }
    return semanticRelations.getNodes();
  }
  
  public List getOperations()
  {
    return sdo.getOperations();
  }
  
  public SemanticNode getParentNode()
  {
    return parentNode;
  }
  
  public SemanticPath getParentPath()
  {
    return parentPath;
  }
  
  SemanticPath getPath(SemanticNode paramSemanticNode)
  {
    return semanticRelations.get(paramSemanticNode);
  }
  
  List getPaths()
  {
    boolean bool = hasChild();
    if (!bool)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      return localArrayList;
    }
    return semanticRelations.getPaths();
  }
  
  int getScope()
  {
    Object localObject = sdo;
    if (localObject != null)
    {
      localObject = ((SemanticDataObject)localObject).getRelateOperation();
      if (localObject != null) {
        return sdo.getRelateOperation().getScope();
      }
    }
    return 0;
  }
  
  public SemanticRelations getSemanticRelations()
  {
    return semanticRelations;
  }
  
  String getStr()
  {
    return sdo.getStr();
  }
  
  String getToken()
  {
    return sdo.getToken();
  }
  
  String getType()
  {
    return sdo.getType();
  }
  
  List getTypes()
  {
    Object localObject = sdo;
    if (localObject != null)
    {
      localObject = ((SemanticDataObject)localObject).getRelateOperation();
      if (localObject != null) {
        return sdo.getRelateOperation().getType();
      }
    }
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    return (List)localObject;
  }
  
  public int getVal(int paramInt)
  {
    return sdo.getVal(paramInt);
  }
  
  public a getVal()
  {
    return sdo.getVal();
  }
  
  public String getVal(String paramString)
  {
    return sdo.getVal(paramString);
  }
  
  boolean hasChild()
  {
    int i = getChildrenCount();
    return i > 0;
  }
  
  boolean hasNode(SemanticNode paramSemanticNode)
  {
    paramSemanticNode = getPath(paramSemanticNode);
    return paramSemanticNode != null;
  }
  
  boolean hasNode(String paramString)
  {
    Iterator localIterator = getNodes().iterator();
    boolean bool;
    do
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str = ((SemanticNode)localIterator.next()).getName();
      bool = str.equals(paramString);
    } while (!bool);
    return true;
    return false;
  }
  
  boolean hasSDO(SemanticDataObject paramSemanticDataObject)
  {
    SemanticDataObject localSemanticDataObject = sdo;
    return paramSemanticDataObject.equals(localSemanticDataObject);
  }
  
  public int hashCode()
  {
    return sdo.hashCode();
  }
  
  String isExplicitPath(SemanticDataObject paramSemanticDataObject)
  {
    paramSemanticDataObject = paramSemanticDataObject.getType();
    String str = "accusative";
    boolean bool1 = paramSemanticDataObject.equals(str);
    if (!bool1)
    {
      str = "adjective";
      boolean bool2 = paramSemanticDataObject.equals(str);
      if (!bool2) {
        return null;
      }
    }
    return "acc";
  }
  
  void lockAll()
  {
    Object localObject1 = getName();
    Object localObject2 = "nondet";
    boolean bool1 = ((String)localObject1).equals(localObject2);
    if (bool1)
    {
      localObject1 = sdo;
      ((SemanticDataObject)localObject1).lockGDO();
    }
    localObject1 = getNodes().iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (SemanticNode)((Iterator)localObject1).next();
      ((SemanticNode)localObject2).lockAll();
    }
  }
  
  public void relate(SemanticNode paramSemanticNode)
  {
    Object localObject1 = getPaths().iterator();
    int i = 0;
    String str1 = null;
    int j = 0;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (SemanticPath)((Iterator)localObject1).next();
      SemanticNode localSemanticNode1 = getNode((SemanticPath)localObject2);
      int m = paramSemanticNode.getDir();
      int i1 = 2;
      int i2 = 1;
      Object localObject3;
      if (m <= 0)
      {
        boolean bool2 = paramSemanticNode.canHaveChildren();
        if (bool2)
        {
          int n = paramSemanticNode.getScope();
          if ((n > 0) || (j == 0))
          {
            localObject3 = paramSemanticNode.canRelate(localSemanticNode1);
            if (localObject3 != null)
            {
              int i3 = ((SemanticPath)localObject2).getType();
              if (i3 == i1)
              {
                str1 = ((SemanticPath)localObject2).getPathName();
                localObject2 = ((SemanticPath)localObject2).getPathNode();
                addNode(str1, (SemanticNode)localObject2, paramSemanticNode, i1);
                i = 1;
              }
              paramSemanticNode.addNode((String)localObject3, null, localSemanticNode1, i2);
              removeNode(localSemanticNode1);
            }
          }
        }
      }
      if (i == 0)
      {
        int k = localSemanticNode1.getDir();
        if (k > 0)
        {
          k = localSemanticNode1.getScope();
          if ((k > 0) || (j == 0))
          {
            localObject2 = localSemanticNode1.canRelate(paramSemanticNode);
            if (localObject2 != null)
            {
              boolean bool3 = localSemanticNode1.canHaveChildren();
              if (bool3)
              {
                localSemanticNode1.addNode((String)localObject2, null, paramSemanticNode, i2);
                i = 1;
              }
              else if (j == 0)
              {
                localObject2 = localSemanticNode1.getPaths().iterator();
                SemanticNode localSemanticNode2;
                String str2;
                int i4;
                do
                {
                  do
                  {
                    boolean bool4;
                    do
                    {
                      bool3 = ((Iterator)localObject2).hasNext();
                      if (!bool3) {
                        break;
                      }
                      localObject3 = (SemanticPath)((Iterator)localObject2).next();
                      localSemanticNode2 = localSemanticNode1.getNode((SemanticPath)localObject3);
                      bool4 = localSemanticNode2.canHaveChildren();
                      if (bool4)
                      {
                        str2 = localSemanticNode2.canRelate(paramSemanticNode);
                        if (str2 != null)
                        {
                          localSemanticNode2.addNode(str2, null, paramSemanticNode, i2);
                          i = 1;
                          break;
                        }
                      }
                      bool4 = paramSemanticNode.canHaveChildren();
                    } while (!bool4);
                    str2 = paramSemanticNode.canRelate(localSemanticNode2);
                  } while (str2 == null);
                  i4 = ((SemanticPath)localObject3).getType();
                } while (i4 != i1);
                localSemanticNode1.replaceNode((SemanticPath)localObject3, paramSemanticNode);
                paramSemanticNode.addNode(str2, null, localSemanticNode2, i2);
                i = 1;
              }
            }
          }
        }
      }
      j += 1;
    }
    if (i == 0)
    {
      localObject1 = paramSemanticNode.getType();
      addNode((String)localObject1, null, paramSemanticNode, 0);
    }
  }
  
  void removeNode(SemanticNode paramSemanticNode)
  {
    boolean bool = hasChild();
    if (bool)
    {
      bool = hasNode(paramSemanticNode);
      if (bool)
      {
        SemanticRelations localSemanticRelations = semanticRelations;
        localSemanticRelations.remove(paramSemanticNode);
      }
    }
  }
  
  void replaceNode(SemanticPath paramSemanticPath, SemanticNode paramSemanticNode)
  {
    semanticRelations.put(paramSemanticPath, paramSemanticNode);
  }
  
  void setParent(SemanticPath paramSemanticPath, SemanticNode paramSemanticNode)
  {
    parentPath = paramSemanticPath;
    parentNode = paramSemanticNode;
  }
  
  public void setVal(int paramInt1, int paramInt2)
  {
    sdo.setVal(paramInt1, paramInt2);
  }
  
  public void setVal(String paramString1, String paramString2)
  {
    sdo.setVal(paramString1, paramString2);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{\"semanticRelations\":");
    Object localObject = semanticRelations;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", \"sdo\":");
    localObject = sdo;
    localStringBuilder.append(localObject);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticNode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */