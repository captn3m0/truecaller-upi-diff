package com.twelfthmile.malana.compiler.parser.semantic;

public class SemanticOperandObject
  extends SemanticOper
{
  SemanticOperandObject(SemanticNode paramSemanticNode)
  {
    node = paramSemanticNode;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (paramObject != null)
    {
      Object localObject = getClass();
      Class localClass = paramObject.getClass();
      if (localObject == localClass)
      {
        paramObject = (SemanticOperandObject)paramObject;
        localObject = node;
        paramObject = node;
        return ((SemanticNode)localObject).equals(paramObject);
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    return node.hashCode();
  }
  
  void setVal(int paramInt1, int paramInt2)
  {
    node.setVal(paramInt1, paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticOperandObject
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */