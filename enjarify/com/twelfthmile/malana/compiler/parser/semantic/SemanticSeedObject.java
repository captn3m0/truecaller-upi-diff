package com.twelfthmile.malana.compiler.parser.semantic;

import java.util.List;
import org.json.JSONObject;

public class SemanticSeedObject
{
  private JSONObject attr;
  private String nodeStr;
  private List operList;
  private String type;
  
  public SemanticSeedObject(String paramString1, JSONObject paramJSONObject, List paramList, String paramString2)
  {
    type = paramString1;
    attr = paramJSONObject;
    operList = paramList;
    nodeStr = paramString2;
  }
  
  public JSONObject getAttr()
  {
    return attr;
  }
  
  public String getNodeStr()
  {
    return nodeStr;
  }
  
  public List getOperList()
  {
    return operList;
  }
  
  public String getType()
  {
    return type;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[");
    Object localObject = type;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append("], attr: ");
    localObject = attr;
    if (localObject != null) {
      localObject = ((JSONObject)localObject).toString();
    }
    localStringBuilder.append(localObject);
    localStringBuilder.append(", oper: ");
    localObject = operList;
    if (localObject != null) {
      localObject = localObject.toString();
    }
    localStringBuilder.append(localObject);
    localStringBuilder.append(", nodeStr: ");
    localObject = nodeStr;
    localStringBuilder.append((String)localObject);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticSeedObject
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */