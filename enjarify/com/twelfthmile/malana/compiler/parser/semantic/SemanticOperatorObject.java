package com.twelfthmile.malana.compiler.parser.semantic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class SemanticOperatorObject
  extends SemanticOper
{
  private SemanticOperation operation;
  
  SemanticOperatorObject(SemanticNode paramSemanticNode, SemanticOperation paramSemanticOperation)
  {
    node = paramSemanticNode;
    operation = paramSemanticOperation;
  }
  
  public boolean attach(SemanticOperandObject paramSemanticOperandObject)
  {
    return node.attach(paramSemanticOperandObject);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    SemanticOperation localSemanticOperation = null;
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (SemanticOperatorObject)paramObject;
        localObject1 = node;
        localObject2 = node;
        boolean bool = ((SemanticNode)localObject1).equals(localObject2);
        if (!bool) {
          return false;
        }
        localSemanticOperation = operation;
        paramObject = operation;
        return localSemanticOperation.equals(paramObject);
      }
    }
    return false;
  }
  
  int getDim()
  {
    return operation.getDim();
  }
  
  public List getNodesAsOperand()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Iterator localIterator = node.getNodes().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      SemanticNode localSemanticNode = (SemanticNode)localIterator.next();
      SemanticOperandObject localSemanticOperandObject = new com/twelfthmile/malana/compiler/parser/semantic/SemanticOperandObject;
      localSemanticOperandObject.<init>(localSemanticNode);
      localArrayList.add(localSemanticOperandObject);
    }
    return localArrayList;
  }
  
  String getOperator()
  {
    return operation.getOperator();
  }
  
  public List getSemanticRules()
  {
    return operation.getSemanticRules();
  }
  
  public boolean hasNode(String paramString)
  {
    return node.hasNode(paramString);
  }
  
  public int hashCode()
  {
    int i = node.hashCode() * 31;
    int j = operation.hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticOperatorObject
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */