package com.twelfthmile.malana.compiler.parser.semantic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class SemanticRelations
{
  private HashMap relations;
  
  public SemanticRelations()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    relations = localHashMap;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (paramObject != null)
    {
      Object localObject = getClass();
      Class localClass = paramObject.getClass();
      if (localObject == localClass)
      {
        paramObject = (SemanticRelations)paramObject;
        localObject = relations;
        paramObject = relations;
        return ((HashMap)localObject).equals(paramObject);
      }
    }
    return false;
  }
  
  public SemanticNode get(SemanticPath paramSemanticPath)
  {
    return (SemanticNode)relations.get(paramSemanticPath);
  }
  
  public SemanticPath get(SemanticNode paramSemanticNode)
  {
    Iterator localIterator = relations.entrySet().iterator();
    Map.Entry localEntry;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localEntry = (Map.Entry)localIterator.next();
      SemanticNode localSemanticNode = (SemanticNode)localEntry.getValue();
      bool2 = localSemanticNode.equals(paramSemanticNode);
    } while (!bool2);
    return (SemanticPath)localEntry.getKey();
    return null;
  }
  
  public Collection getNodes()
  {
    return relations.values();
  }
  
  public List getPaths()
  {
    Object localObject1 = new java/util/LinkedList;
    Object localObject2 = relations.entrySet();
    ((LinkedList)localObject1).<init>((Collection)localObject2);
    localObject2 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticRelations$1;
    ((SemanticRelations.1)localObject2).<init>(this);
    Collections.sort((List)localObject1, (Comparator)localObject2);
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      Object localObject3 = ((Map.Entry)((Iterator)localObject1).next()).getKey();
      ((List)localObject2).add(localObject3);
    }
    return (List)localObject2;
  }
  
  public List getPathsOrdered()
  {
    Object localObject1 = new java/util/LinkedList;
    Object localObject2 = relations.entrySet();
    ((LinkedList)localObject1).<init>((Collection)localObject2);
    localObject2 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticRelations$2;
    ((SemanticRelations.2)localObject2).<init>(this);
    Collections.sort((List)localObject1, (Comparator)localObject2);
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      Object localObject3 = ((Map.Entry)((Iterator)localObject1).next()).getKey();
      ((List)localObject2).add(localObject3);
    }
    return (List)localObject2;
  }
  
  public int hashCode()
  {
    return relations.hashCode();
  }
  
  public void put(SemanticPath paramSemanticPath, SemanticNode paramSemanticNode)
  {
    relations.put(paramSemanticPath, paramSemanticNode);
  }
  
  public void remove(SemanticNode paramSemanticNode)
  {
    Object localObject = relations.entrySet().iterator();
    Map.Entry localEntry;
    boolean bool2;
    do
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      localEntry = (Map.Entry)((Iterator)localObject).next();
      SemanticNode localSemanticNode = (SemanticNode)localEntry.getValue();
      bool2 = localSemanticNode.equals(paramSemanticNode);
    } while (!bool2);
    paramSemanticNode = relations;
    localObject = localEntry.getKey();
    paramSemanticNode.remove(localObject);
    return;
  }
  
  public int size()
  {
    return relations.keySet().size();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    localStringBuilder1.<init>("[");
    Iterator localIterator = getPathsOrdered().iterator();
    for (;;)
    {
      i = localIterator.hasNext();
      if (i == 0) {
        break;
      }
      Object localObject = (SemanticPath)localIterator.next();
      StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
      localStringBuilder2.<init>("{\"path\":\"");
      String str = ((SemanticPath)localObject).toString();
      localStringBuilder2.append(str);
      str = "\",\"node\":";
      localStringBuilder2.append(str);
      localObject = get((SemanticPath)localObject);
      localStringBuilder2.append(localObject);
      localStringBuilder2.append("}");
      localObject = localStringBuilder2.toString();
      localStringBuilder1.append((String)localObject);
      localObject = ",";
      localStringBuilder1.append((String)localObject);
    }
    int j = localStringBuilder1.length();
    int i = 1;
    if (j > i)
    {
      int k = localStringBuilder1.length() - i;
      localStringBuilder1.setLength(k);
    }
    localStringBuilder1.append("]");
    return localStringBuilder1.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticRelations
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */