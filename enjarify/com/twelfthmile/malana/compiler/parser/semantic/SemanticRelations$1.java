package com.twelfthmile.malana.compiler.parser.semantic;

import java.util.Comparator;
import java.util.Map.Entry;

class SemanticRelations$1
  implements Comparator
{
  SemanticRelations$1(SemanticRelations paramSemanticRelations) {}
  
  public int compare(Map.Entry paramEntry1, Map.Entry paramEntry2)
  {
    int i = ((SemanticNode)paramEntry1.getValue()).getIndex();
    int j = ((SemanticNode)paramEntry2.getValue()).getIndex();
    return -Integer.compare(i, j);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticRelations.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */