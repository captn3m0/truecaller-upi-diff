package com.twelfthmile.malana.compiler.parser.semantic;

import java.io.PrintStream;
import java.util.Vector;

public class SemanticOperationQueue
{
  private Vector sooVector;
  
  public SemanticOperationQueue()
  {
    Vector localVector = new java/util/Vector;
    localVector.<init>();
    sooVector = localVector;
  }
  
  public SemanticOperationQueue(int paramInt)
  {
    Vector localVector = new java/util/Vector;
    localVector.<init>(paramInt);
    sooVector = localVector;
  }
  
  private int LEFT(int paramInt)
  {
    return paramInt * 2 + 1;
  }
  
  private int RIGHT(int paramInt)
  {
    return paramInt * 2 + 2;
  }
  
  private void heapify_down(int paramInt)
  {
    for (;;)
    {
      int i = LEFT(paramInt);
      int j = RIGHT(paramInt);
      int k = size();
      SemanticOperationObject localSemanticOperationObject1;
      SemanticOperationObject localSemanticOperationObject2;
      int m;
      if (i < k)
      {
        localSemanticOperationObject1 = (SemanticOperationObject)sooVector.get(i);
        k = localSemanticOperationObject1.getScore();
        localSemanticOperationObject2 = (SemanticOperationObject)sooVector.get(paramInt);
        m = localSemanticOperationObject2.getScore();
        if (k > m) {}
      }
      else
      {
        i = paramInt;
      }
      k = size();
      if (j < k)
      {
        localSemanticOperationObject1 = (SemanticOperationObject)sooVector.get(j);
        k = localSemanticOperationObject1.getScore();
        localSemanticOperationObject2 = (SemanticOperationObject)sooVector.get(i);
        m = localSemanticOperationObject2.getScore();
        if (k > m) {
          i = j;
        }
      }
      if (i == paramInt) {
        break;
      }
      swap(paramInt, i);
      paramInt = i;
    }
  }
  
  private void heapify_up(int paramInt)
  {
    while (paramInt > 0)
    {
      Object localObject = sooVector;
      int i = parent(paramInt);
      localObject = (SemanticOperationObject)((Vector)localObject).get(i);
      int j = ((SemanticOperationObject)localObject).getScore();
      SemanticOperationObject localSemanticOperationObject = (SemanticOperationObject)sooVector.get(paramInt);
      i = localSemanticOperationObject.getScore();
      if (j >= i) {
        break;
      }
      j = parent(paramInt);
      swap(paramInt, j);
      paramInt = parent(paramInt);
    }
  }
  
  private int parent(int paramInt)
  {
    if (paramInt == 0) {
      return 0;
    }
    return (paramInt + -1) / 2;
  }
  
  public void add(SemanticOperationObject paramSemanticOperationObject)
  {
    sooVector.addElement(paramSemanticOperationObject);
    int i = size() + -1;
    heapify_up(i);
  }
  
  public void clear()
  {
    Object localObject1 = System.out;
    Object localObject2 = "Emptying queue: ";
    ((PrintStream)localObject1).print((String)localObject2);
    for (;;)
    {
      localObject1 = sooVector;
      boolean bool = ((Vector)localObject1).isEmpty();
      if (bool) {
        break;
      }
      localObject1 = System.out;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      Object localObject3 = poll();
      ((StringBuilder)localObject2).append(localObject3);
      localObject3 = " ";
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      ((PrintStream)localObject1).print((String)localObject2);
    }
    System.out.println();
  }
  
  public Boolean contains(Integer paramInteger)
  {
    return Boolean.valueOf(sooVector.contains(paramInteger));
  }
  
  public Boolean isEmpty()
  {
    return Boolean.valueOf(sooVector.isEmpty());
  }
  
  public SemanticOperationObject peek()
  {
    try
    {
      int i = size();
      if (i != 0)
      {
        localObject = sooVector;
        localObject = ((Vector)localObject).firstElement();
        return (SemanticOperationObject)localObject;
      }
      Object localObject = new java/lang/Exception;
      String str = "Index out of range (Heap underflow)";
      ((Exception)localObject).<init>(str);
      throw ((Throwable)localObject);
    }
    catch (Exception localException)
    {
      System.out.println(localException);
    }
    return null;
  }
  
  public SemanticOperationObject poll()
  {
    try
    {
      int i = size();
      if (i != 0)
      {
        localObject1 = sooVector;
        localObject1 = ((Vector)localObject1).firstElement();
        localObject1 = (SemanticOperationObject)localObject1;
        localObject2 = sooVector;
        Object localObject3 = sooVector;
        localObject3 = ((Vector)localObject3).lastElement();
        ((Vector)localObject2).setElementAt(localObject3, 0);
        localObject2 = sooVector;
        int j = size() + -1;
        ((Vector)localObject2).remove(j);
        heapify_down(0);
        return (SemanticOperationObject)localObject1;
      }
      Object localObject1 = new java/lang/Exception;
      Object localObject2 = "Index is out of range (Heap underflow)";
      ((Exception)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    catch (Exception localException)
    {
      System.out.println(localException);
    }
    return null;
  }
  
  public int size()
  {
    return sooVector.size();
  }
  
  void swap(int paramInt1, int paramInt2)
  {
    SemanticOperationObject localSemanticOperationObject = (SemanticOperationObject)sooVector.get(paramInt1);
    Vector localVector = sooVector;
    Object localObject = localVector.get(paramInt2);
    localVector.setElementAt(localObject, paramInt1);
    sooVector.setElementAt(localSemanticOperationObject, paramInt2);
  }
  
  public SemanticOperationObject[] toArray()
  {
    Vector localVector = sooVector;
    SemanticOperationObject[] arrayOfSemanticOperationObject = new SemanticOperationObject[size()];
    return (SemanticOperationObject[])localVector.toArray(arrayOfSemanticOperationObject);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticOperationQueue
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */