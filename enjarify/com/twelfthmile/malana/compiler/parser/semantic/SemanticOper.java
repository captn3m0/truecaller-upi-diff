package com.twelfthmile.malana.compiler.parser.semantic;

import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public abstract class SemanticOper
{
  private boolean lock;
  SemanticNode node;
  
  private void getValHelperGDO(SemStrInt paramSemStrInt, GrammarDataObject paramGrammarDataObject, String paramString)
  {
    int i = paramString.hashCode();
    int j = 114225;
    String str;
    boolean bool2;
    if (i != j)
    {
      j = 3373707;
      if (i != j)
      {
        j = 110541305;
        if (i == j)
        {
          str = "token";
          boolean bool1 = paramString.equals(str);
          if (bool1)
          {
            int k = 2;
            break label116;
          }
        }
      }
      else
      {
        str = "name";
        bool2 = paramString.equals(str);
        if (bool2)
        {
          bool2 = false;
          paramString = null;
          break label116;
        }
      }
    }
    else
    {
      str = "str";
      bool2 = paramString.equals(str);
      if (bool2)
      {
        bool2 = true;
        break label116;
      }
    }
    int m = -1;
    switch (m)
    {
    default: 
      break;
    case 2: 
      paramGrammarDataObject = labelAux;
      paramSemStrInt.set(paramGrammarDataObject);
      break;
    case 1: 
      paramGrammarDataObject = str;
      paramSemStrInt.set(paramGrammarDataObject);
      return;
    case 0: 
      label116:
      paramGrammarDataObject = label;
      paramSemStrInt.set(paramGrammarDataObject);
      return;
    }
  }
  
  private void getValHelperGDO(SemStrInt paramSemStrInt, List paramList, String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    if (paramList != null)
    {
      paramList = paramList.iterator();
      for (;;)
      {
        boolean bool1 = paramList.hasNext();
        if (!bool1) {
          break;
        }
        Object localObject = (GrammarDataObject)paramList.next();
        int i = -1;
        int j = paramString.hashCode();
        int k = 114225;
        String str;
        boolean bool2;
        if (j != k)
        {
          k = 3373707;
          if (j != k)
          {
            k = 110541305;
            if (j == k)
            {
              str = "token";
              bool2 = paramString.equals(str);
              if (bool2) {
                i = 2;
              }
            }
          }
          else
          {
            str = "name";
            bool2 = paramString.equals(str);
            if (bool2) {
              i = 0;
            }
          }
        }
        else
        {
          str = "str";
          bool2 = paramString.equals(str);
          if (bool2) {
            i = 1;
          }
        }
        switch (i)
        {
        default: 
          break;
        case 2: 
          localObject = labelAux;
          localArrayList.add(localObject);
          break;
        case 1: 
          localObject = str;
          localArrayList.add(localObject);
          break;
        case 0: 
          localObject = label;
          localArrayList.add(localObject);
        }
      }
    }
    int m = localArrayList.size();
    if (m > 0) {
      paramSemStrInt.set(localArrayList);
    }
  }
  
  private void getValHelperSDO(SemStrInt paramSemStrInt, SemanticNode paramSemanticNode, String paramString)
  {
    int i = paramString.hashCode();
    int j = 114225;
    String str;
    boolean bool2;
    if (i != j)
    {
      j = 3373707;
      if (i != j)
      {
        j = 110541305;
        if (i == j)
        {
          str = "token";
          boolean bool1 = paramString.equals(str);
          if (bool1)
          {
            int k = 2;
            break label116;
          }
        }
      }
      else
      {
        str = "name";
        bool2 = paramString.equals(str);
        if (bool2)
        {
          bool2 = false;
          paramString = null;
          break label116;
        }
      }
    }
    else
    {
      str = "str";
      bool2 = paramString.equals(str);
      if (bool2)
      {
        bool2 = true;
        break label116;
      }
    }
    int m = -1;
    switch (m)
    {
    default: 
      break;
    case 2: 
      paramSemanticNode = paramSemanticNode.getToken();
      paramSemStrInt.set(paramSemanticNode);
      break;
    case 1: 
      paramSemanticNode = paramSemanticNode.getStr();
      paramSemStrInt.set(paramSemanticNode);
      return;
    case 0: 
      label116:
      paramSemanticNode = paramSemanticNode.getName();
      paramSemStrInt.set(paramSemanticNode);
      return;
    }
  }
  
  private void getValHelperSDO(SemStrInt paramSemStrInt, SemanticPath paramSemanticPath)
  {
    paramSemanticPath = paramSemanticPath.getPathName();
    paramSemStrInt.set(paramSemanticPath);
  }
  
  private void getValHelperSDO(SemStrInt paramSemStrInt, Collection paramCollection)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    if (paramCollection != null)
    {
      paramCollection = paramCollection.iterator();
      for (;;)
      {
        boolean bool = paramCollection.hasNext();
        if (!bool) {
          break;
        }
        String str = ((SemanticPath)paramCollection.next()).getPathName();
        localArrayList.add(str);
      }
    }
    int i = localArrayList.size();
    if (i > 0) {
      paramSemStrInt.set(localArrayList);
    }
  }
  
  private void getValHelperSDO(SemStrInt paramSemStrInt, Collection paramCollection, String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    if (paramCollection != null)
    {
      paramCollection = paramCollection.iterator();
      for (;;)
      {
        boolean bool1 = paramCollection.hasNext();
        if (!bool1) {
          break;
        }
        Object localObject = (SemanticNode)paramCollection.next();
        int i = -1;
        int j = paramString.hashCode();
        int k = 114225;
        String str;
        boolean bool2;
        if (j != k)
        {
          k = 3373707;
          if (j != k)
          {
            k = 110541305;
            if (j == k)
            {
              str = "token";
              bool2 = paramString.equals(str);
              if (bool2) {
                i = 2;
              }
            }
          }
          else
          {
            str = "name";
            bool2 = paramString.equals(str);
            if (bool2) {
              i = 0;
            }
          }
        }
        else
        {
          str = "str";
          bool2 = paramString.equals(str);
          if (bool2) {
            i = 1;
          }
        }
        switch (i)
        {
        default: 
          break;
        case 2: 
          localObject = ((SemanticNode)localObject).getToken();
          localArrayList.add(localObject);
          break;
        case 1: 
          localObject = ((SemanticNode)localObject).getStr();
          localArrayList.add(localObject);
          break;
        case 0: 
          localObject = ((SemanticNode)localObject).getName();
          localArrayList.add(localObject);
        }
      }
    }
    int m = localArrayList.size();
    if (m > 0) {
      paramSemStrInt.set(localArrayList);
    }
  }
  
  int getInd()
  {
    return node.getIndex();
  }
  
  String getName()
  {
    return node.getName();
  }
  
  SemanticNode getNode()
  {
    return node;
  }
  
  int getVal(int paramInt)
  {
    return node.getVal(paramInt);
  }
  
  SemStrInt getVal(SemStrInt paramSemStrInt)
  {
    SemStrInt localSemStrInt = new com/twelfthmile/malana/compiler/parser/semantic/SemStrInt;
    localSemStrInt.<init>();
    boolean bool1 = paramSemStrInt.isInt();
    if (bool1)
    {
      int j = paramSemStrInt.getI();
      j = getVal(j);
      localSemStrInt.set(j);
    }
    else
    {
      Object localObject = paramSemStrInt.getS();
      String str1 = ".";
      bool1 = ((String)localObject).contains(str1);
      if (bool1)
      {
        paramSemStrInt = paramSemStrInt.getS();
        localObject = "\\.";
        paramSemStrInt = paramSemStrInt.split((String)localObject);
        int i = paramSemStrInt.length;
        int k = 2;
        if (i >= k)
        {
          i = 0;
          localObject = null;
          String str2 = paramSemStrInt[0];
          String str3 = "sem";
          boolean bool4 = str2.equals(str3);
          int m = 1;
          boolean bool2;
          if (bool4)
          {
            localObject = paramSemStrInt[m];
            str2 = "parent";
            bool2 = ((String)localObject).equals(str2);
            if (bool2)
            {
              localObject = node.getParentNode();
              paramSemStrInt = paramSemStrInt[k];
              getValHelperSDO(localSemStrInt, (SemanticNode)localObject, paramSemStrInt);
            }
            else
            {
              localObject = paramSemStrInt[m];
              str2 = "parent_path";
              bool2 = ((String)localObject).equals(str2);
              if (bool2)
              {
                paramSemStrInt = node.getParentPath();
                getValHelperSDO(localSemStrInt, paramSemStrInt);
              }
              else
              {
                localObject = paramSemStrInt[m];
                str2 = "child";
                bool2 = ((String)localObject).equals(str2);
                if (bool2)
                {
                  localObject = node.getNodes();
                  paramSemStrInt = paramSemStrInt[k];
                  getValHelperSDO(localSemStrInt, (Collection)localObject, paramSemStrInt);
                }
                else
                {
                  paramSemStrInt = paramSemStrInt[m];
                  localObject = "child_path";
                  boolean bool3 = paramSemStrInt.equals(localObject);
                  if (bool3)
                  {
                    paramSemStrInt = node.getPaths();
                    getValHelperSDO(localSemStrInt, paramSemStrInt);
                  }
                }
              }
            }
          }
          else
          {
            localObject = paramSemStrInt[0];
            str2 = "syn";
            bool2 = ((String)localObject).equals(str2);
            if (bool2)
            {
              localObject = paramSemStrInt[m];
              str2 = "parent";
              bool2 = ((String)localObject).equals(str2);
              if (bool2)
              {
                localObject = node.gdoParent();
                paramSemStrInt = paramSemStrInt[k];
                getValHelperGDO(localSemStrInt, (GrammarDataObject)localObject, paramSemStrInt);
              }
              else
              {
                localObject = paramSemStrInt[m];
                str2 = "child";
                bool2 = ((String)localObject).equals(str2);
                if (bool2)
                {
                  localObject = node.gdoChildren();
                  paramSemStrInt = paramSemStrInt[k];
                  getValHelperGDO(localSemStrInt, (List)localObject, paramSemStrInt);
                }
                else
                {
                  localObject = paramSemStrInt[m];
                  str2 = "sibling";
                  bool2 = ((String)localObject).equals(str2);
                  if (bool2)
                  {
                    localObject = node.gdoSiblings();
                    paramSemStrInt = paramSemStrInt[k];
                    getValHelperGDO(localSemStrInt, (List)localObject, paramSemStrInt);
                  }
                }
              }
            }
          }
        }
      }
    }
    return localSemStrInt;
  }
  
  public boolean isLock()
  {
    return lock;
  }
  
  public void setLock(boolean paramBoolean)
  {
    lock = paramBoolean;
  }
  
  void setVal(SemStrInt paramSemStrInt1, SemStrInt paramSemStrInt2)
  {
    boolean bool1 = paramSemStrInt2.isStr();
    if (bool1)
    {
      localObject = node;
      paramSemStrInt2 = paramSemStrInt2.getS();
      paramSemStrInt2 = ((SemanticNode)localObject).getNodeForName(paramSemStrInt2);
      localObject = node;
      paramSemStrInt1 = paramSemStrInt1.getS();
      String str = paramSemStrInt2.getAllStr();
      ((SemanticNode)localObject).setVal(paramSemStrInt1, str);
      paramSemStrInt1 = paramSemStrInt2.getName();
      localObject = "nondet";
      boolean bool2 = paramSemStrInt1.equals(localObject);
      if (bool2) {
        paramSemStrInt2.lockAll();
      }
      return;
    }
    Object localObject = node;
    int i = paramSemStrInt1.getI();
    int j = paramSemStrInt2.getI();
    ((SemanticNode)localObject).setVal(i, j);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticOper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */