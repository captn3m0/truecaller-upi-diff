package com.twelfthmile.malana.compiler.parser.semantic;

import com.twelfthmile.b.a.a;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class SemanticDataObject
{
  private HashMap attrVals;
  private int dimSize;
  private GrammarDataObject gdo;
  private int index;
  private String name;
  private List nodes;
  private List operations;
  private SemanticOperation relateOperation;
  private String type;
  
  public SemanticDataObject(SemanticSeedObject paramSemanticSeedObject, GrammarDataObject paramGrammarDataObject, int paramInt, SemanticSeedConstants paramSemanticSeedConstants)
  {
    this((String)localObject1, paramInt, paramSemanticSeedConstants);
    Object localObject2 = paramSemanticSeedObject.getType();
    type = ((String)localObject2);
    gdo = paramGrammarDataObject;
    localObject2 = gdo.values;
    localObject1 = "dim";
    int i = paramSemanticSeedConstants.get((String)localObject1);
    ((a)localObject2).initList(i);
    localObject2 = paramSemanticSeedObject.getAttr();
    paramSemanticSeedObject = paramSemanticSeedObject.getOperList();
    if (localObject2 != null)
    {
      localObject1 = ((JSONObject)localObject2).keys();
      Object localObject4;
      boolean bool2;
      int n;
      int i1;
      Object localObject5;
      do
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break label724;
        }
        localObject3 = (String)((Iterator)localObject1).next();
        localObject4 = "*";
        bool2 = ((String)localObject3).equals(localObject4);
        n = 0;
        if (bool2) {
          break;
        }
        localObject4 = "#";
        bool2 = ((String)localObject3).startsWith((String)localObject4);
        i1 = 1;
        if (bool2)
        {
          localObject4 = ((String)localObject3).substring(i1);
          localObject5 = str;
          bool2 = ((String)localObject4).equals(localObject5);
          if (bool2) {
            break;
          }
        }
        localObject4 = "$";
        bool2 = ((String)localObject3).startsWith((String)localObject4);
        if (bool2)
        {
          localObject4 = ((String)localObject3).substring(i1);
          localObject5 = labelAux;
          bool2 = ((String)localObject4).equals(localObject5);
          if (bool2) {
            break;
          }
        }
        localObject4 = values;
        bool2 = ((a)localObject4).containsValue((String)localObject3);
        if (bool2) {
          break;
        }
        localObject4 = "@";
        bool2 = ((String)localObject3).equals(localObject4);
      } while (!bool2);
      Object localObject3 = ((JSONObject)localObject2).getJSONArray((String)localObject3);
      for (;;)
      {
        int j = ((JSONArray)localObject3).length();
        if (n >= j) {
          break;
        }
        localObject4 = attrVals;
        localObject5 = ((JSONArray)localObject3).getString(n);
        boolean bool3 = ((HashMap)localObject4).containsKey(localObject5);
        if (!bool3)
        {
          localObject4 = (HashMap)attrVals.get("core");
          localObject5 = ((JSONArray)localObject3).getString(n);
          i3 = paramSemanticSeedConstants.get((String)localObject5);
          localObject5 = Integer.valueOf(i3);
          bool3 = ((HashMap)localObject4).containsKey(localObject5);
          if (!bool3)
          {
            localObject4 = (HashMap)attrVals.get("core");
            localObject5 = ((JSONArray)localObject3).getString(n);
            i3 = paramSemanticSeedConstants.get((String)localObject5);
            localObject5 = Integer.valueOf(i3);
            localObject6 = ((JSONArray)localObject3).getString(n);
            ((HashMap)localObject4).put(localObject5, localObject6);
          }
          localObject4 = attrVals;
          localObject5 = ((JSONArray)localObject3).getString(n);
          localObject6 = new java/util/HashMap;
          ((HashMap)localObject6).<init>();
          ((HashMap)localObject4).put(localObject5, localObject6);
        }
        localObject4 = attrVals;
        localObject5 = ((JSONArray)localObject3).getString(n);
        localObject4 = (HashMap)((HashMap)localObject4).get(localObject5);
        n += 1;
        localObject5 = ((JSONArray)localObject3).getString(n);
        int i3 = paramSemanticSeedConstants.get((String)localObject5);
        localObject5 = Integer.valueOf(i3);
        Object localObject6 = ((JSONArray)localObject3).getString(n);
        ((HashMap)localObject4).put(localObject5, localObject6);
        n += i1;
      }
      localObject3 = ((JSONObject)localObject2).getJSONArray((String)localObject3);
      for (;;)
      {
        int k = ((JSONArray)localObject3).length();
        if (n >= k) {
          break;
        }
        localObject4 = ((JSONArray)localObject3).getString(n);
        boolean bool4 = paramSemanticSeedConstants.containsKey((String)localObject4);
        int m;
        if (bool4)
        {
          localObject4 = ((JSONArray)localObject3).getString(n);
          m = paramSemanticSeedConstants.get((String)localObject4);
        }
        else
        {
          localObject4 = ((JSONArray)localObject3).getString(n);
          m = Integer.parseInt((String)localObject4);
        }
        n += 1;
        String str = ((JSONArray)localObject3).getString(n);
        boolean bool5 = paramSemanticSeedConstants.containsKey(str);
        int i2;
        if (bool5)
        {
          str = ((JSONArray)localObject3).getString(n);
          i2 = paramSemanticSeedConstants.get(str);
        }
        else
        {
          str = ((JSONArray)localObject3).getString(n);
          i2 = Integer.parseInt(str);
        }
        setVal(m, i2);
        n += 1;
      }
    }
    label724:
    if (paramSemanticSeedObject != null)
    {
      int i4 = paramSemanticSeedObject.size();
      if (i4 > 0)
      {
        paramGrammarDataObject = new java/util/ArrayList;
        paramGrammarDataObject.<init>();
        operations = paramGrammarDataObject;
        paramSemanticSeedObject = paramSemanticSeedObject.iterator();
        for (;;)
        {
          boolean bool6 = paramSemanticSeedObject.hasNext();
          if (!bool6) {
            break;
          }
          paramGrammarDataObject = (JSONObject)paramSemanticSeedObject.next();
          localObject2 = new com/twelfthmile/malana/compiler/parser/semantic/SemanticOperation;
          ((SemanticOperation)localObject2).<init>(paramGrammarDataObject, paramSemanticSeedConstants);
          paramGrammarDataObject = ((SemanticOperation)localObject2).getOperator();
          localObject1 = "relate";
          bool6 = paramGrammarDataObject.equals(localObject1);
          if (bool6)
          {
            relateOperation = ((SemanticOperation)localObject2);
          }
          else
          {
            paramGrammarDataObject = operations;
            paramGrammarDataObject.add(localObject2);
          }
        }
      }
    }
  }
  
  private SemanticDataObject(String paramString, int paramInt, SemanticSeedConstants paramSemanticSeedConstants)
  {
    if (paramString != null)
    {
      int i = paramString.length();
      int j = 2;
      if (i >= j)
      {
        Object localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        nodes = ((List)localObject1);
        localObject1 = ":";
        paramString = paramString.split((String)localObject1);
        i = 1;
        j = 1;
        Object localObject2;
        Object localObject3;
        for (;;)
        {
          int k = paramString.length;
          if (j >= k) {
            break;
          }
          localObject2 = nodes;
          localObject3 = paramString[j];
          ((List)localObject2).add(localObject3);
          j += 1;
        }
        paramString = nodes;
        j = paramString.size() - i;
        paramString = (String)paramString.get(j);
        name = paramString;
        index = paramInt;
        int m = ((Integer)semConstants.get("dim")).intValue();
        dimSize = m;
        paramString = new java/util/HashMap;
        paramString.<init>();
        attrVals = paramString;
        paramString = coreDims;
        Iterator localIterator = paramString.keySet().iterator();
        boolean bool1 = localIterator.hasNext();
        if (bool1)
        {
          localObject1 = (String)localIterator.next();
          Object localObject4 = attrVals;
          localObject2 = new java/util/HashMap;
          ((HashMap)localObject2).<init>();
          ((HashMap)localObject4).put(localObject1, localObject2);
          localObject4 = ((List)paramString.get(localObject1)).iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject4).hasNext();
            if (!bool2) {
              break;
            }
            localObject2 = (String)((Iterator)localObject4).next();
            localObject3 = (HashMap)attrVals.get(localObject1);
            int n = paramSemanticSeedConstants.get((String)localObject2);
            Integer localInteger = Integer.valueOf(n);
            ((HashMap)localObject3).put(localInteger, localObject2);
          }
        }
        return;
      }
    }
  }
  
  public boolean check(String paramString)
  {
    return nodes.contains(paramString);
  }
  
  public boolean check(List paramList)
  {
    paramList = paramList.iterator();
    boolean bool;
    do
    {
      bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramList.next();
      bool = check(str);
    } while (!bool);
    return true;
    return false;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    GrammarDataObject localGrammarDataObject = null;
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (SemanticDataObject)paramObject;
        int i = index;
        int j = index;
        if (i != j) {
          return false;
        }
        localObject1 = type;
        localObject2 = type;
        boolean bool = ((String)localObject1).equals(localObject2);
        if (!bool) {
          return false;
        }
        localObject1 = nodes;
        localObject2 = nodes;
        bool = ((List)localObject1).equals(localObject2);
        if (!bool) {
          return false;
        }
        localObject1 = gdo.values;
        localObject2 = gdo.values;
        bool = localObject1.equals(localObject2);
        if (!bool) {
          return false;
        }
        localGrammarDataObject = gdo;
        paramObject = gdo;
        return localGrammarDataObject.equals(paramObject);
      }
    }
    return false;
  }
  
  public List gdoChildren()
  {
    return gdo.children;
  }
  
  public GrammarDataObject gdoParent()
  {
    return gdo.getParent();
  }
  
  public List gdoSiblings()
  {
    return gdo.getSiblings();
  }
  
  public int getIndex()
  {
    return index;
  }
  
  public String getName()
  {
    return name;
  }
  
  public List getOperations()
  {
    return operations;
  }
  
  public SemanticOperation getRelateOperation()
  {
    return relateOperation;
  }
  
  public String getStr()
  {
    return gdo.str;
  }
  
  public String getToken()
  {
    return gdo.labelAux;
  }
  
  public String getType()
  {
    return type;
  }
  
  public int getVal(int paramInt)
  {
    return gdo.values.get(paramInt);
  }
  
  public a getVal()
  {
    return gdo.values;
  }
  
  public String getVal(String paramString)
  {
    return gdo.values.get(paramString);
  }
  
  public int hashCode()
  {
    int i = index * 31;
    int j = type.hashCode();
    i = (i + j) * 31;
    j = nodes.hashCode();
    i = (i + j) * 31;
    j = gdo.values.hashCode();
    i = (i + j) * 31;
    j = gdo.hashCode();
    return i + j;
  }
  
  public void lockGDO()
  {
    gdo.lock = true;
  }
  
  public void setVal(int paramInt1, int paramInt2)
  {
    gdo.values.add(paramInt1, paramInt2);
  }
  
  public void setVal(String paramString1, String paramString2)
  {
    gdo.values.put(paramString1, paramString2);
  }
  
  public void setVals()
  {
    Object localObject1 = type;
    Object localObject2 = "subject";
    boolean bool1 = ((String)localObject1).equals(localObject2);
    String str;
    Object localObject3;
    Object localObject4;
    boolean bool4;
    if (bool1)
    {
      localObject1 = gdo.getAncestor();
      localObject2 = null;
      int i = 0;
      str = null;
      for (;;)
      {
        int j = dimSize;
        if (i >= j) {
          break;
        }
        localObject3 = attrVals;
        localObject4 = (HashMap)((HashMap)localObject3).get("core");
        Object localObject5 = Integer.valueOf(i);
        localObject4 = ((HashMap)localObject4).get(localObject5);
        boolean bool3 = ((HashMap)localObject3).containsKey(localObject4);
        if (bool3)
        {
          localObject3 = gdo.values;
          int k = ((a)localObject3).get(i);
          if (k >= 0)
          {
            k = 1;
            if (i == k)
            {
              localObject3 = gdo.values;
              k = ((a)localObject3).get(i);
              if (k > 0)
              {
                localObject3 = gdo.values;
                k = ((a)localObject3).get(0);
                if (k <= 0) {}
              }
            }
          }
          else
          {
            localObject3 = gdo;
            bool4 = ((GrammarDataObject)localObject3).hasChildren();
            if (!bool4)
            {
              localObject3 = gdo;
              bool4 = ((GrammarDataObject)localObject3).hasParent();
              if (!bool4) {}
            }
            else
            {
              localObject3 = (HashMap)attrVals.get("core");
              localObject4 = Integer.valueOf(i);
              localObject3 = (String)((HashMap)localObject3).get(localObject4);
              localObject4 = gdo.values;
              localObject5 = (HashMap)attrVals.get(localObject3);
              int m = gdo.values.get(i);
              Integer localInteger = Integer.valueOf(m);
              localObject5 = (String)((HashMap)localObject5).get(localInteger);
              ((a)localObject4).put((String)localObject3, (String)localObject5);
              localObject4 = gdo;
              if (localObject4 != localObject1)
              {
                localObject4 = values;
                localObject5 = (HashMap)attrVals.get(localObject3);
                m = gdo.values.get(i);
                localInteger = Integer.valueOf(m);
                localObject5 = (String)((HashMap)localObject5).get(localInteger);
                ((a)localObject4).put((String)localObject3, (String)localObject5);
              }
            }
          }
        }
        i += 1;
      }
    }
    localObject1 = type;
    localObject2 = "subject";
    bool1 = ((String)localObject1).equals(localObject2);
    if (bool1)
    {
      localObject1 = gdo.getAncestor();
      localObject2 = gdo;
      if (localObject2 != localObject1)
      {
        localObject2 = values.keySet().iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject2).hasNext();
          if (!bool2) {
            break;
          }
          str = (String)((Iterator)localObject2).next();
          localObject3 = values;
          bool4 = ((a)localObject3).containsKey(str);
          if (!bool4)
          {
            localObject3 = values;
            localObject4 = gdo.values.get(str);
            ((a)localObject3).put(str, (String)localObject4);
          }
        }
      }
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{\"name\":\"");
    Object localObject = name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append("\",\"type\":\"");
    localObject = type;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append("\",\"nodeStr\":\"");
    localObject = nodes.toString();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append("\",\"attr\":\"");
    localObject = gdo.values;
    localStringBuilder.append(localObject);
    localStringBuilder.append("\",\"str\":\"");
    localObject = gdo.str;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append("\"}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticDataObject
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */