package com.twelfthmile.malana.compiler.parser.semantic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class SemStrInt
{
  private int i = -1 << -1;
  private List il = null;
  private String s = null;
  private List sl = null;
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (SemStrInt)paramObject;
        int j = i;
        int k = i;
        if (j != k) {
          return false;
        }
        localObject1 = s;
        boolean bool2;
        if (localObject1 != null)
        {
          localObject2 = s;
          bool2 = ((String)localObject1).equals(localObject2);
          if (bool2) {
            break label101;
          }
        }
        else
        {
          localObject1 = s;
          if (localObject1 == null) {
            break label101;
          }
        }
        return false;
        label101:
        localObject1 = il;
        if (localObject1 != null)
        {
          localObject2 = il;
          bool2 = ((List)localObject1).equals(localObject2);
          if (bool2) {
            break label145;
          }
        }
        else
        {
          localObject1 = il;
          if (localObject1 == null) {
            break label145;
          }
        }
        return false;
        label145:
        localObject1 = sl;
        if (localObject1 != null)
        {
          paramObject = sl;
          return ((List)localObject1).equals(paramObject);
        }
        paramObject = sl;
        if (paramObject == null) {
          return bool1;
        }
        return false;
      }
    }
    return false;
  }
  
  public boolean equalsOrContains(SemStrInt paramSemStrInt)
  {
    boolean bool1 = equals(paramSemStrInt);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    Object localObject1 = sl;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = s;
      if (localObject2 != null) {
        return ((List)localObject1).contains(localObject2);
      }
    }
    localObject1 = sl;
    if (localObject1 != null)
    {
      localObject2 = s;
      if (localObject2 != null) {
        return ((List)localObject1).contains(localObject2);
      }
    }
    localObject1 = il;
    if (localObject1 != null)
    {
      bool1 = isInt();
      if (bool1)
      {
        paramSemStrInt = il;
        localObject1 = Integer.valueOf(i);
        return paramSemStrInt.contains(localObject1);
      }
    }
    localObject1 = sl;
    if (localObject1 != null)
    {
      localObject2 = sl;
      if (localObject2 != null)
      {
        localObject1 = ((List)localObject1).iterator();
        boolean bool3;
        do
        {
          bool3 = ((Iterator)localObject1).hasNext();
          if (!bool3) {
            break;
          }
          localObject2 = (String)((Iterator)localObject1).next();
          List localList = sl;
          bool3 = localList.contains(localObject2);
        } while (!bool3);
        return bool2;
      }
    }
    return false;
  }
  
  public int getI()
  {
    return i;
  }
  
  public List getIL()
  {
    return il;
  }
  
  public String getS()
  {
    return s;
  }
  
  public List getSL()
  {
    return sl;
  }
  
  public int hashCode()
  {
    int j = i * 31;
    Object localObject = s;
    int k = 0;
    int m;
    if (localObject != null)
    {
      m = ((String)localObject).hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = sl;
    if (localObject != null)
    {
      m = ((List)localObject).hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = il;
    if (localObject != null) {
      k = ((List)localObject).hashCode();
    }
    return j + k;
  }
  
  public boolean isInt()
  {
    Object localObject = getS();
    if (localObject == null)
    {
      localObject = getSL();
      if (localObject == null)
      {
        int j = getI();
        int k = -1 << -1;
        if (j > k) {
          return true;
        }
      }
    }
    return false;
  }
  
  public boolean isStr()
  {
    Object localObject = getS();
    if (localObject == null)
    {
      localObject = getSL();
      if (localObject == null) {}
    }
    else
    {
      int j = getI();
      int k = -1 << -1;
      if (j == k) {
        return true;
      }
    }
    return false;
  }
  
  public void set(int paramInt)
  {
    i = paramInt;
  }
  
  public void set(String paramString)
  {
    s = paramString;
  }
  
  public void set(List paramList)
  {
    sl = paramList;
  }
  
  public void set(String[] paramArrayOfString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    paramArrayOfString = Arrays.asList(paramArrayOfString);
    localArrayList.<init>(paramArrayOfString);
    sl = localArrayList;
  }
  
  public void setIL(List paramList)
  {
    il = paramList;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemStrInt
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */