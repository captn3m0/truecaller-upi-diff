package com.twelfthmile.malana.compiler.parser.semantic;

import org.json.JSONArray;

public class SemanticRule
{
  private String[] in;
  private String out;
  
  public SemanticRule(JSONArray paramJSONArray, String paramString)
  {
    String[] arrayOfString1 = new String[paramJSONArray.length()];
    in = arrayOfString1;
    int i = 0;
    arrayOfString1 = null;
    for (;;)
    {
      int j = paramJSONArray.length();
      if (i >= j) {
        break;
      }
      String[] arrayOfString2 = in;
      String str = paramJSONArray.getString(i);
      arrayOfString2[i] = str;
      i += 1;
    }
    out = paramString;
  }
  
  public String[] getIn()
  {
    return in;
  }
  
  public String getOut()
  {
    return out;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.semantic.SemanticRule
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */