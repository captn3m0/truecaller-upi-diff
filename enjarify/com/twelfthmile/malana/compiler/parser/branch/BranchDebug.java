package com.twelfthmile.malana.compiler.parser.branch;

import com.twelfthmile.b.a.a;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataLinkedListObject;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.util.L;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class BranchDebug
{
  static void branchPrint(Branch paramBranch)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("");
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    int i = branchIndex;
    ((StringBuilder)localObject).append(i);
    String str = " : ";
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    localStringBuilder.append((String)localObject);
    for (paramBranch = gdoListRoot.next; paramBranch != null; paramBranch = next)
    {
      localObject = grammarDataObject;
      if (localObject != null)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("[");
        str = grammarDataObject.print();
        ((StringBuilder)localObject).append(str);
        str = "]";
        ((StringBuilder)localObject).append(str);
        localObject = ((StringBuilder)localObject).toString();
        localStringBuilder.append((String)localObject);
      }
    }
    L.msg(localStringBuilder);
  }
  
  static void branchPrint(BranchList paramBranchList)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    String str1 = "";
    localStringBuilder.<init>(str1);
    int i = -1;
    Object localObject1 = paramBranchList.getList();
    if (localObject1 != null)
    {
      localObject1 = ((ArrayList)localObject1).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        Object localObject2 = (Branch)((Iterator)localObject1).next();
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        int k = branchIndex;
        ((StringBuilder)localObject3).append(k);
        Object localObject4 = " : ";
        ((StringBuilder)localObject3).append((String)localObject4);
        localObject3 = ((StringBuilder)localObject3).toString();
        localStringBuilder.append((String)localObject3);
        int m = branchIndex;
        if (m == 0) {
          i = ((Branch)localObject2).getEndIndex();
        }
        for (localObject3 = gdoListRoot.next; localObject3 != null; localObject3 = next)
        {
          localObject4 = grammarDataObject;
          if (localObject4 != null)
          {
            localObject4 = new java/lang/StringBuilder;
            ((StringBuilder)localObject4).<init>(" [");
            String str2 = grammarDataObject.print();
            ((StringBuilder)localObject4).append(str2);
            str2 = "] ";
            ((StringBuilder)localObject4).append(str2);
            localObject4 = ((StringBuilder)localObject4).toString();
            localStringBuilder.append((String)localObject4);
          }
        }
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>(" (");
        k = branchSize;
        ((StringBuilder)localObject3).append(k);
        localObject4 = "+";
        ((StringBuilder)localObject3).append((String)localObject4);
        int j = ((Branch)localObject2).getBranchLength(i);
        ((StringBuilder)localObject3).append(j);
        ((StringBuilder)localObject3).append(") \n");
        localObject2 = ((StringBuilder)localObject3).toString();
        localStringBuilder.append((String)localObject2);
      }
    }
    L.msg(localStringBuilder);
  }
  
  public static String gdoListPrint(ArrayList paramArrayList)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[");
    String str = "";
    paramArrayList = paramArrayList.iterator();
    for (;;)
    {
      boolean bool = paramArrayList.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (GrammarDataObject)paramArrayList.next();
      localStringBuilder.append(str);
      str = ",";
      localObject = ((GrammarDataObject)localObject).print();
      localStringBuilder.append((String)localObject);
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  public static String gdoListPrintLabels(List paramList)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("");
    String str = "";
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (GrammarDataObject)paramList.next();
      boolean bool2 = lock;
      if (!bool2)
      {
        localStringBuilder.append(str);
        str = ",";
        localObject = ((GrammarDataObject)localObject).printLabel();
        localStringBuilder.append((String)localObject);
      }
    }
    return localStringBuilder.toString();
  }
  
  public static String valuePrint(a parama)
  {
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    Object localObject = "{";
    localStringBuilder1.<init>((String)localObject);
    if (parama != null)
    {
      parama = parama.entrySet().iterator();
      for (;;)
      {
        i = parama.hasNext();
        if (i == 0) {
          break;
        }
        localObject = (Map.Entry)parama.next();
        StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
        localStringBuilder2.<init>("\"");
        String str = (String)((Map.Entry)localObject).getKey();
        localStringBuilder2.append(str);
        str = "\" : \"";
        localStringBuilder2.append(str);
        localObject = (String)((Map.Entry)localObject).getValue();
        localStringBuilder2.append((String)localObject);
        localStringBuilder2.append("\"");
        localObject = localStringBuilder2.toString();
        localStringBuilder1.append((String)localObject);
        localObject = ",";
        localStringBuilder1.append((String)localObject);
      }
    }
    int j = localStringBuilder1.length();
    int i = 1;
    if (j > i)
    {
      int k = localStringBuilder1.length() - i;
      localStringBuilder1.setLength(k);
    }
    localStringBuilder1.append("}");
    return localStringBuilder1.toString();
  }
  
  public static String valuePrint(HashMap paramHashMap)
  {
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    Object localObject = "{";
    localStringBuilder1.<init>((String)localObject);
    if (paramHashMap != null)
    {
      paramHashMap = paramHashMap.entrySet().iterator();
      for (;;)
      {
        i = paramHashMap.hasNext();
        if (i == 0) {
          break;
        }
        localObject = (Map.Entry)paramHashMap.next();
        StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
        localStringBuilder2.<init>("\"");
        String str = (String)((Map.Entry)localObject).getKey();
        localStringBuilder2.append(str);
        str = "\" : \"";
        localStringBuilder2.append(str);
        localObject = (String)((Map.Entry)localObject).getValue();
        localStringBuilder2.append((String)localObject);
        localStringBuilder2.append("\"");
        localObject = localStringBuilder2.toString();
        localStringBuilder1.append((String)localObject);
        localObject = ",";
        localStringBuilder1.append((String)localObject);
      }
    }
    int j = localStringBuilder1.length();
    int i = 1;
    if (j > i)
    {
      int k = localStringBuilder1.length() - i;
      localStringBuilder1.setLength(k);
    }
    localStringBuilder1.append("}");
    return localStringBuilder1.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.branch.BranchDebug
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */