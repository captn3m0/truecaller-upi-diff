package com.twelfthmile.malana.compiler.parser.branch;

import com.twelfthmile.malana.compiler.datastructure.GrammarTrie;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataLinkedListObject;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.types.GrammarCallback;
import com.twelfthmile.malana.compiler.types.ParserListener;
import java.lang.ref.WeakReference;
import java.util.HashMap;

class BranchController$GrammarCallbackImpl
  implements GrammarCallback
{
  WeakReference branchControllerWeakReference;
  
  public BranchController$GrammarCallbackImpl(BranchController paramBranchController)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramBranchController);
    branchControllerWeakReference = localWeakReference;
  }
  
  public void add(GrammarDataObject paramGrammarDataObject)
  {
    paramGrammarDataObject = (BranchController)branchControllerWeakReference.get();
    if (paramGrammarDataObject == null) {
      return;
    }
    Branch localBranch = new com/twelfthmile/malana/compiler/parser/branch/Branch;
    GrammarTrie localGrammarTrie = BranchController.access$000(paramGrammarDataObject);
    GrammarDataLinkedListObject localGrammarDataLinkedListObject1 = access$100maingdoListHead;
    Object localObject = access$100maingdoListHead;
    GrammarDataLinkedListObject localGrammarDataLinkedListObject2 = BranchController.access$200(paramGrammarDataObject, (GrammarDataLinkedListObject)localObject);
    GrammarCallbackImpl localGrammarCallbackImpl = new com/twelfthmile/malana/compiler/parser/branch/BranchController$GrammarCallbackImpl;
    localGrammarCallbackImpl.<init>(paramGrammarDataObject);
    int i = BranchController.access$304(paramGrammarDataObject);
    localObject = localBranch;
    localBranch.<init>(localGrammarTrie, localGrammarDataLinkedListObject1, localGrammarDataLinkedListObject2, localGrammarCallbackImpl, i);
    BranchController.access$100(paramGrammarDataObject).add(localBranch);
  }
  
  public void add(GrammarDataObject paramGrammarDataObject, String paramString, int paramInt)
  {
    Object localObject1 = (BranchController)branchControllerWeakReference.get();
    if (localObject1 == null) {
      return;
    }
    Branch localBranch = new com/twelfthmile/malana/compiler/parser/branch/Branch;
    GrammarTrie localGrammarTrie = BranchController.access$000((BranchController)localObject1);
    GrammarDataLinkedListObject localGrammarDataLinkedListObject1 = access$100maingdoListHead;
    Object localObject2 = access$100maingdoListHead;
    GrammarDataLinkedListObject localGrammarDataLinkedListObject2 = BranchController.access$200((BranchController)localObject1, (GrammarDataLinkedListObject)localObject2);
    GrammarCallbackImpl localGrammarCallbackImpl = new com/twelfthmile/malana/compiler/parser/branch/BranchController$GrammarCallbackImpl;
    localGrammarCallbackImpl.<init>((BranchController)localObject1);
    int i = BranchController.access$304((BranchController)localObject1);
    localObject2 = localBranch;
    localBranch.<init>(localGrammarTrie, localGrammarDataLinkedListObject1, localGrammarDataLinkedListObject2, localGrammarCallbackImpl, i);
    branchSize = paramInt;
    HashMap localHashMap = BranchController.access$400((BranchController)localObject1);
    localObject1 = new com/twelfthmile/malana/compiler/parser/branch/BranchController$ParallelIntent;
    ((BranchController.ParallelIntent)localObject1).<init>(localBranch, paramGrammarDataObject);
    localHashMap.put(paramString, localObject1);
  }
  
  public void condensation(GrammarDataObject paramGrammarDataObject)
  {
    BranchController localBranchController = (BranchController)branchControllerWeakReference.get();
    if (localBranchController == null) {
      return;
    }
    BranchController.access$500(localBranchController).condensation(paramGrammarDataObject);
  }
  
  public boolean shouldCondense(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2, GrammarDataObject paramGrammarDataObject3)
  {
    Object localObject = (BranchController)branchControllerWeakReference.get();
    if (localObject != null)
    {
      localObject = BranchController.access$500((BranchController)localObject);
      boolean bool = ((ParserListener)localObject).shouldCondense(paramGrammarDataObject1, paramGrammarDataObject2, paramGrammarDataObject3);
      if (!bool) {
        return false;
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.branch.BranchController.GrammarCallbackImpl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */