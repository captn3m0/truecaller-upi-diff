package com.twelfthmile.malana.compiler.parser.branch;

import com.twelfthmile.malana.compiler.datastructure.GrammarTrie;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataLinkedListObject;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.types.ParserListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class BranchController
{
  private int branchIndexCount = 0;
  private BranchList branchList;
  private GrammarTrie grammarRoot;
  private HashMap parallelIntents;
  private ParserListener parserListener;
  
  public BranchController(GrammarTrie paramGrammarTrie, GrammarDataLinkedListObject paramGrammarDataLinkedListObject, ParserListener paramParserListener)
  {
    parserListener = paramParserListener;
    paramParserListener = new com/twelfthmile/malana/compiler/parser/branch/BranchList;
    paramParserListener.<init>();
    branchList = paramParserListener;
    paramParserListener = branchList;
    Branch localBranch = new com/twelfthmile/malana/compiler/parser/branch/Branch;
    BranchController.GrammarCallbackImpl localGrammarCallbackImpl = new com/twelfthmile/malana/compiler/parser/branch/BranchController$GrammarCallbackImpl;
    localGrammarCallbackImpl.<init>(this);
    localBranch.<init>(paramGrammarTrie, paramGrammarDataLinkedListObject, paramGrammarDataLinkedListObject, localGrammarCallbackImpl, 0);
    paramParserListener.add(localBranch);
    grammarRoot = paramGrammarTrie;
    paramGrammarTrie = new java/util/HashMap;
    paramGrammarTrie.<init>();
    parallelIntents = paramGrammarTrie;
  }
  
  private Branch checkForCondense()
  {
    int i = -1 >>> 1;
    Object localObject1 = null;
    int j = -1;
    int k = -1 >>> 1;
    Object localObject2 = branchList.getList();
    if (localObject2 != null)
    {
      localObject2 = ((ArrayList)localObject2).iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject2).hasNext();
        if (!bool3) {
          break;
        }
        Branch localBranch1 = (Branch)((Iterator)localObject2).next();
        int m = branchIndex;
        if (m == 0)
        {
          j = localBranch1.getEndIndex();
        }
        else
        {
          boolean bool4 = condensationFlag;
          if (bool4)
          {
            int n = localBranch1.getBranchLength(j);
            if (n <= i)
            {
              n = branchSize;
              if (n <= k)
              {
                i = localBranch1.getBranchLength(j);
                k = branchSize;
                localObject1 = localBranch1;
              }
            }
          }
        }
      }
    }
    if (localObject1 != null)
    {
      Object localObject3 = branchList.getList();
      if (localObject3 != null)
      {
        localObject3 = ((ArrayList)localObject3).iterator();
        for (;;)
        {
          boolean bool1 = ((Iterator)localObject3).hasNext();
          if (!bool1) {
            break;
          }
          Branch localBranch2 = (Branch)((Iterator)localObject3).next();
          k = branchIndex;
          if (k != 0)
          {
            boolean bool2 = condensationFlag;
            if ((bool2) && (localBranch2 != localObject1)) {
              localBranch2.annulCondense();
            }
          }
        }
      }
    }
    return (Branch)localObject1;
  }
  
  private GrammarDataLinkedListObject cloneObject(GrammarDataLinkedListObject paramGrammarDataLinkedListObject)
  {
    GrammarDataLinkedListObject localGrammarDataLinkedListObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataLinkedListObject;
    GrammarDataObject localGrammarDataObject = grammarDataObject;
    paramGrammarDataLinkedListObject = next;
    localGrammarDataLinkedListObject.<init>(localGrammarDataObject, paramGrammarDataLinkedListObject);
    return localGrammarDataLinkedListObject;
  }
  
  private void condense(Branch paramBranch)
  {
    paramBranch.doCondense();
    Object localObject1 = gdoListRoot;
    Object localObject2 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataLinkedListObject;
    Object localObject3 = condensedGdo;
    ((GrammarDataLinkedListObject)localObject2).<init>((GrammarDataObject)localObject3, null);
    next = ((GrammarDataLinkedListObject)localObject2);
    boolean bool = reCondensed;
    if (!bool)
    {
      localObject1 = parserListener;
      localObject2 = condensedGdo;
      ((ParserListener)localObject1).score((GrammarDataObject)localObject2);
    }
    localObject1 = gdoListParent.next;
    localObject2 = gdoListParent;
    localObject3 = gdoListRoot.next;
    next = ((GrammarDataLinkedListObject)localObject3);
    gdoListRoot.next.next = ((GrammarDataLinkedListObject)localObject1);
    localObject2 = branchList.main().gdoListHead;
    localObject3 = gdoListParent;
    if ((localObject2 == localObject3) && (localObject1 == null))
    {
      localObject1 = branchList.main();
      paramBranch = gdoListRoot.next;
      gdoListHead = paramBranch;
    }
  }
  
  private void createParallelBranch(BranchController.ParallelIntent paramParallelIntent)
  {
    Object localObject = gdo;
    boolean bool = lock;
    if (!bool)
    {
      localObject = branchList;
      Branch localBranch = gdoContext;
      ((BranchList)localObject).add(localBranch);
      localObject = gdoContext;
      paramParallelIntent = gdo;
      ((Branch)localObject).add(paramParallelIntent);
    }
  }
  
  private boolean isInSet(Set paramSet1, Set paramSet2)
  {
    paramSet1 = paramSet1.iterator();
    boolean bool;
    do
    {
      bool = paramSet1.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramSet1.next();
      bool = paramSet2.contains(str);
    } while (!bool);
    return true;
    return false;
  }
  
  private Branch nextTokenCheckHelper(GrammarDataObject paramGrammarDataObject)
  {
    Object localObject1 = null;
    Object localObject2 = branchList.getList();
    if (localObject2 != null)
    {
      localObject2 = ((ArrayList)localObject2).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject2).hasNext();
        if (!bool1) {
          break;
        }
        Branch localBranch = (Branch)((Iterator)localObject2).next();
        Object localObject3 = grammarHead;
        boolean bool2 = origin;
        String str;
        if (bool2)
        {
          localObject3 = grammarHead.originGetNext();
          str = label;
          bool2 = ((Set)localObject3).contains(str);
          if (!bool2)
          {
            localObject3 = grammarHead.originGetNext();
            str = labelAux;
            bool2 = ((Set)localObject3).contains(str);
            if (!bool2) {}
          }
          else
          {
            localObject1 = localBranch;
          }
        }
        else
        {
          localObject3 = grammarHead.next.keySet();
          str = label;
          bool2 = ((Set)localObject3).contains(str);
          if (!bool2)
          {
            localObject3 = grammarHead.next.keySet();
            str = labelAux;
            bool2 = ((Set)localObject3).contains(str);
            if (!bool2) {}
          }
          else
          {
            localObject1 = localBranch;
          }
        }
      }
    }
    return (Branch)localObject1;
  }
  
  private Branch preCondense(Branch paramBranch)
  {
    boolean bool1;
    do
    {
      condense(paramBranch);
      bool1 = false;
      Object localObject1 = null;
      Object localObject2 = branchList.getList();
      if (localObject2 != null)
      {
        localObject2 = ((ArrayList)localObject2).iterator();
        ((Iterator)localObject2).next();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject2).hasNext();
          if (!bool2) {
            break;
          }
          Branch localBranch = (Branch)((Iterator)localObject2).next();
          GrammarTrie localGrammarTrie = grammarHead;
          GrammarDataObject localGrammarDataObject = condensedGdo;
          boolean bool3 = localGrammarTrie.canPeek(localGrammarDataObject);
          if (bool3) {
            localObject1 = localBranch;
          }
        }
      }
      if (localObject1 != null)
      {
        localObject2 = condensedGdo;
        boolean bool4 = ((Branch)localObject1).add((GrammarDataObject)localObject2);
        if (bool4)
        {
          bool4 = condensationFlag;
          if (bool4) {
            paramBranch = (Branch)localObject1;
          }
        }
      }
      bool1 = condensationFlag;
    } while (bool1);
    return paramBranch;
  }
  
  private void serveParallelIntents()
  {
    Iterator localIterator = parallelIntents.values().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      BranchController.ParallelIntent localParallelIntent = (BranchController.ParallelIntent)localIterator.next();
      createParallelBranch(localParallelIntent);
    }
    parallelIntents.clear();
  }
  
  public void add(GrammarDataObject paramGrammarDataObject)
  {
    Object localObject1 = branchList.getList();
    Object localObject2;
    if (localObject1 != null)
    {
      localObject1 = ((ArrayList)localObject1).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (Branch)((Iterator)localObject1).next();
        ((Branch)localObject2).add(paramGrammarDataObject);
      }
    }
    serveParallelIntents();
    paramGrammarDataObject = checkForCondense();
    if (paramGrammarDataObject != null)
    {
      paramGrammarDataObject = preCondense(paramGrammarDataObject);
      branchList.clear();
      localObject1 = grammarRoot;
      localObject2 = condensedGdo;
      boolean bool2 = ((GrammarTrie)localObject1).canPeek((GrammarDataObject)localObject2);
      if (bool2)
      {
        localObject1 = new com/twelfthmile/malana/compiler/parser/branch/Branch;
        GrammarTrie localGrammarTrie = grammarRoot;
        GrammarDataLinkedListObject localGrammarDataLinkedListObject1 = branchList.main().gdoListHead;
        localObject2 = branchList.main().gdoListHead;
        GrammarDataLinkedListObject localGrammarDataLinkedListObject2 = cloneObject((GrammarDataLinkedListObject)localObject2);
        BranchController.GrammarCallbackImpl localGrammarCallbackImpl = new com/twelfthmile/malana/compiler/parser/branch/BranchController$GrammarCallbackImpl;
        localGrammarCallbackImpl.<init>(this);
        int i = branchIndexCount;
        int j = i + 1;
        branchIndexCount = j;
        localObject2 = localObject1;
        ((Branch)localObject1).<init>(localGrammarTrie, localGrammarDataLinkedListObject1, localGrammarDataLinkedListObject2, localGrammarCallbackImpl, j);
        localObject2 = branchList;
        ((BranchList)localObject2).add((Branch)localObject1);
        paramGrammarDataObject = condensedGdo;
        ((Branch)localObject1).add(paramGrammarDataObject);
      }
      serveParallelIntents();
    }
  }
  
  public void end()
  {
    branchList.clearAll();
  }
  
  public boolean nextTokenCheck(ArrayList paramArrayList)
  {
    if (paramArrayList != null)
    {
      int i = paramArrayList.size();
      if (i > 0)
      {
        Object localObject1 = (GrammarDataObject)paramArrayList.remove(0);
        Branch localBranch = nextTokenCheckHelper((GrammarDataObject)localObject1);
        if (localBranch == null) {
          return false;
        }
        int j = paramArrayList.size();
        boolean bool3 = true;
        if (j == 0) {
          return bool3;
        }
        Object localObject2 = grammarHead;
        localObject1 = ((GrammarTrie)localObject2).getNext((GrammarDataObject)localObject1);
        paramArrayList = paramArrayList.iterator();
        for (;;)
        {
          boolean bool2 = paramArrayList.hasNext();
          if (!bool2) {
            break label278;
          }
          localObject2 = (GrammarDataObject)paramArrayList.next();
          if (localObject1 == null) {
            return false;
          }
          Object localObject3 = next;
          String str = label;
          boolean bool4 = ((HashMap)localObject3).containsKey(str);
          if (bool4)
          {
            localObject1 = next;
            localObject2 = label;
            localObject1 = (GrammarTrie)((HashMap)localObject1).get(localObject2);
          }
          else
          {
            localObject3 = next;
            str = labelAux;
            bool4 = ((HashMap)localObject3).containsKey(str);
            if (bool4)
            {
              localObject1 = next;
              localObject2 = labelAux;
              localObject1 = (GrammarTrie)((HashMap)localObject1).get(localObject2);
            }
            else
            {
              localObject3 = grammarHead;
              bool4 = ((GrammarTrie)localObject3).canPeek((GrammarDataObject)localObject2);
              if (!bool4) {
                break;
              }
              localObject1 = next.keySet();
              localObject3 = grammarHead.getNext((GrammarDataObject)localObject2).getLeafs();
              boolean bool1 = isInSet((Set)localObject1, (Set)localObject3);
              if (!bool1) {
                break;
              }
              localObject1 = grammarHead.getNext((GrammarDataObject)localObject2);
            }
          }
        }
        return false;
        label278:
        return bool3;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.branch.BranchController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */