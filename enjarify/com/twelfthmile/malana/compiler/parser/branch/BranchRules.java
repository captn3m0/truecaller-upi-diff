package com.twelfthmile.malana.compiler.parser.branch;

import com.twelfthmile.b.a.a;
import com.twelfthmile.malana.compiler.Compiler;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataLinkedListObject;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.types.Finalise;
import com.twelfthmile.malana.compiler.types.ValueMap;
import com.twelfthmile.malana.controller.Controller;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BranchRules
{
  private static final String MULT = "mult";
  
  public static void addValues(a parama1, a parama2)
  {
    Iterator localIterator = parama2.entrySet().iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Map.Entry)localIterator.next();
      String str1 = (String)((Map.Entry)localObject1).getKey();
      localObject1 = (String)((Map.Entry)localObject1).getValue();
      if (localObject1 != null)
      {
        superValues((String)localObject1, parama1);
        boolean bool2 = parama1.containsKey(str1);
        if (bool2)
        {
          Object localObject2 = parama1.get(str1);
          bool2 = ((String)localObject2).equals(localObject1);
          if (!bool2)
          {
            localObject2 = parama1.get(str1);
            localObject2 = valueOverrideInMult(str1, (String)localObject2, (String)localObject1, parama2);
            if (localObject2 != null)
            {
              parama1.put(str1, (String)localObject2);
            }
            else
            {
              String str2 = String.valueOf(str1);
              localObject2 = "mult_".concat(str2);
              bool2 = parama1.containsKey((String)localObject2);
              if (!bool2)
              {
                str2 = String.valueOf(str1);
                localObject2 = "mult_".concat(str2);
                str2 = "1";
                parama1.put((String)localObject2, str2);
              }
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              ((StringBuilder)localObject2).append(str1);
              String str3 = String.valueOf(str1);
              str2 = "mult_".concat(str3);
              str2 = parama1.get(str2);
              ((StringBuilder)localObject2).append(str2);
              localObject2 = ((StringBuilder)localObject2).toString();
              parama1.put((String)localObject2, (String)localObject1);
              localObject2 = String.valueOf(str1);
              localObject1 = "mult_".concat((String)localObject2);
              localObject2 = "mult_";
              str1 = String.valueOf(str1);
              str1 = ((String)localObject2).concat(str1);
              int i = Integer.parseInt(parama1.get(str1)) + 1;
              str1 = String.valueOf(i);
              parama1.put((String)localObject1, str1);
            }
          }
        }
        else
        {
          parama1.put(str1, (String)localObject1);
        }
      }
    }
  }
  
  static void cleanup(a parama)
  {
    String str1 = "type";
    boolean bool1 = parama.containsKey(str1);
    if (bool1)
    {
      parama.remove("type");
      str1 = "mult_type";
      bool1 = parama.containsKey(str1);
      if (bool1)
      {
        str1 = parama.get("mult_type");
        int i = Integer.parseInt(str1);
        int j = 1;
        while (j < i)
        {
          String str2 = String.valueOf(j);
          String str3 = "type".concat(str2);
          boolean bool3 = parama.containsKey(str3);
          if (bool3)
          {
            str2 = String.valueOf(j);
            str3 = "type".concat(str2);
            parama.remove(str3);
          }
          j += 1;
        }
        str1 = "mult_type";
        parama.remove(str1);
      }
    }
    str1 = "num_class";
    boolean bool2 = parama.containsKey(str1);
    if (bool2)
    {
      parama.remove("num_class");
      return;
    }
    str1 = "due";
    bool2 = parama.containsKey(str1);
    if (bool2)
    {
      str1 = "due";
      parama.remove(str1);
    }
  }
  
  private static HashMap getAccTypeValScore()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i = 2;
    Integer localInteger1 = Integer.valueOf(i);
    localHashMap.put("creditcard", localInteger1);
    Integer localInteger2 = Integer.valueOf(i);
    localHashMap.put("debitcard", localInteger2);
    localInteger2 = Integer.valueOf(1);
    localHashMap.put("card", localInteger2);
    localInteger2 = Integer.valueOf(0);
    localHashMap.put("acc", localInteger2);
    return localHashMap;
  }
  
  private static String getCabFromAddr(String paramString)
  {
    String str1 = paramString.toUpperCase();
    String str2 = "OLA";
    boolean bool1 = str1.contains(str2);
    if (bool1) {
      return "Ola";
    }
    paramString = paramString.toUpperCase();
    str1 = "UBER";
    boolean bool2 = paramString.contains(str1);
    if (bool2) {
      return "Uber";
    }
    return null;
  }
  
  private static HashMap getTrxCateg()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("imps", "transfer");
    localHashMap.put("upi", "transfer");
    localHashMap.put("aeps", "transfer");
    localHashMap.put("neft", "transfer");
    localHashMap.put("rtgs", "transfer");
    localHashMap.put("autdbt", "expense");
    localHashMap.put("netb", "expense");
    localHashMap.put("withdraw", "expense");
    localHashMap.put("deposit", "income");
    localHashMap.put("refund", "income");
    localHashMap.put("salary", "income");
    localHashMap.put("benef", "notif");
    localHashMap.put("balenq", "notif");
    localHashMap.put("incrdlmt", "notif");
    localHashMap.put("futexpense", "expense");
    localHashMap.put("futincome", "income");
    return localHashMap;
  }
  
  private static HashMap getTrxSubCategScore()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i = 5;
    Integer localInteger1 = Integer.valueOf(i);
    localHashMap.put("autdbt", localInteger1);
    Integer localInteger2 = Integer.valueOf(i);
    localHashMap.put("salary", localInteger2);
    i = 4;
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("benef", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("balenq", localInteger1);
    localInteger2 = Integer.valueOf(i);
    localHashMap.put("incrdlmt", localInteger2);
    i = 3;
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("neft", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("imps", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("rtgs", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("upi", localInteger1);
    localInteger2 = Integer.valueOf(i);
    localHashMap.put("aeps", localInteger2);
    i = 2;
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("deposit", localInteger1);
    localInteger2 = Integer.valueOf(i);
    localHashMap.put("refund", localInteger2);
    localInteger2 = Integer.valueOf(1);
    localHashMap.put("withdraw", localInteger2);
    localInteger2 = Integer.valueOf(0);
    localHashMap.put("netb", localInteger2);
    return localHashMap;
  }
  
  private static HashMap getTrxTypeScore()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i = 1;
    Integer localInteger1 = Integer.valueOf(i);
    localHashMap.put("payment", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("pyt", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("pymt", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("pmnt", localInteger1);
    int j = 2;
    Integer localInteger2 = Integer.valueOf(j);
    localHashMap.put("cash deposit", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("received", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("withdraw", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("debit", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("credit", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("deposit", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("debited", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("credited", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("deposited", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("charged", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("refunded", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("reversed", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("instalment", localInteger2);
    localInteger2 = Integer.valueOf(j);
    localHashMap.put("paid", localInteger2);
    localInteger1 = Integer.valueOf(j);
    localHashMap.put("dr", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("transaction", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("tranx", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("txn", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localHashMap.put("trx", localInteger1);
    Integer localInteger3 = Integer.valueOf(i);
    localHashMap.put("purchase", localInteger3);
    return localHashMap;
  }
  
  private static String sanitizeAccNum(String paramString)
  {
    int i = 0;
    if (paramString == null) {
      return null;
    }
    int j = paramString.length();
    int k = 4;
    if (j == k) {
      return paramString;
    }
    j = paramString.length();
    if (j < k) {
      return null;
    }
    i = paramString.length() - k;
    return paramString.substring(i);
  }
  
  static void superValues(String paramString, a parama)
  {
    Object localObject1 = "salary";
    boolean bool1 = paramString.equals(localObject1);
    Object localObject2;
    if (bool1)
    {
      localObject1 = "trx_type";
      bool1 = parama.containsKey((String)localObject1);
      if (bool1)
      {
        localObject1 = parama.get("trx_type");
        localObject2 = "credit";
        bool1 = ((String)localObject1).equals(localObject2);
        if (bool1)
        {
          parama.put("trx_subcategory", "salary");
          return;
        }
      }
    }
    localObject1 = "autdbt";
    bool1 = paramString.equals(localObject1);
    if (!bool1)
    {
      localObject1 = "netb";
      bool1 = paramString.equals(localObject1);
      if (!bool1)
      {
        localObject1 = "deposit";
        bool1 = paramString.equals(localObject1);
        if (!bool1)
        {
          localObject1 = "refund";
          bool1 = paramString.equals(localObject1);
          if (!bool1)
          {
            localObject1 = "withdraw";
            bool1 = paramString.equals(localObject1);
            if (!bool1)
            {
              localObject1 = "neft";
              bool1 = paramString.equals(localObject1);
              if (!bool1)
              {
                localObject1 = "imps";
                bool1 = paramString.equals(localObject1);
                if (!bool1)
                {
                  localObject1 = "upi";
                  bool1 = paramString.equals(localObject1);
                  if (!bool1)
                  {
                    localObject1 = "aeps";
                    bool1 = paramString.equals(localObject1);
                    if (!bool1)
                    {
                      localObject1 = "rtgs";
                      bool1 = paramString.equals(localObject1);
                      if (!bool1) {
                        return;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    localObject1 = "trx_subcategory";
    bool1 = parama.containsKey((String)localObject1);
    if (bool1)
    {
      localObject1 = getTrxSubCategScore();
      localObject2 = parama.get("trx_subcategory");
      boolean bool2 = ((HashMap)localObject1).containsKey(localObject2);
      if (bool2)
      {
        localObject2 = (Integer)((HashMap)localObject1).get(paramString);
        int j = ((Integer)localObject2).intValue();
        String str = parama.get("trx_subcategory");
        localObject1 = (Integer)((HashMap)localObject1).get(str);
        int i = ((Integer)localObject1).intValue();
        if (j <= i) {
          return;
        }
      }
    }
    localObject1 = "trx_subcategory";
    parama.put((String)localObject1, paramString);
  }
  
  static void valueCompareWithinCondensationAdd(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2)
  {
    String str = "CROSS";
    paramGrammarDataObject1 = label;
    boolean bool = str.equals(paramGrammarDataObject1);
    if (bool)
    {
      paramGrammarDataObject1 = "DUE";
      str = label;
      bool = paramGrammarDataObject1.equals(str);
      if (bool)
      {
        paramGrammarDataObject1 = values;
        paramGrammarDataObject2 = "due";
        str = "overdue";
        paramGrammarDataObject1.put(paramGrammarDataObject2, str);
      }
    }
  }
  
  static void valueConflictWithinCondensationContext(String paramString1, String paramString2, GrammarDataObject paramGrammarDataObject)
  {
    Object localObject1 = "trx_type";
    boolean bool1 = paramString1.equals(localObject1);
    Object localObject2;
    Object localObject3;
    if (bool1)
    {
      localObject1 = paramGrammarDataObject.getChild("TRANSINTENT");
      if (localObject1 != null)
      {
        localObject2 = paramGrammarDataObject.getChild("TRX");
        if (localObject2 != null)
        {
          paramString2 = getTrxTypeScore();
          localObject3 = ((GrammarDataObject)localObject1).getChild("TRANS");
          int i = -1;
          localObject3 = str.toLowerCase();
          String str1 = str.toLowerCase();
          Object localObject4 = values;
          String str2 = "type";
          localObject4 = ((a)localObject4).get(str2);
          boolean bool2 = paramString2.containsKey(localObject3);
          if (bool2)
          {
            i = ((Integer)paramString2.get(localObject3)).intValue();
            localObject1 = values;
            localObject3 = "trx_type";
            localObject4 = ((a)localObject1).get((String)localObject3);
          }
          bool1 = paramString2.containsKey(str1);
          if (bool1)
          {
            paramString2 = (Integer)paramString2.get(str1);
            int j = paramString2.intValue();
            if (j > i)
            {
              paramString2 = values;
              localObject1 = "type";
              localObject4 = paramString2.get((String)localObject1);
            }
          }
          paramGrammarDataObject.addValues(paramString1, (String)localObject4);
          return;
        }
      }
    }
    localObject1 = "trx_type";
    bool1 = paramString1.equals(localObject1);
    if (bool1)
    {
      localObject1 = paramGrammarDataObject.getChild("TRANSINTENT");
      if (localObject1 != null)
      {
        localObject2 = paramGrammarDataObject.getChild("TRANS");
        if (localObject2 != null)
        {
          localObject1 = values;
          localObject2 = "trx_type";
          localObject3 = "credit";
          ((a)localObject1).put((String)localObject2, (String)localObject3);
          break label501;
        }
      }
    }
    localObject1 = "trx_type";
    bool1 = paramString1.equals(localObject1);
    if (bool1)
    {
      localObject1 = paramGrammarDataObject.getChild("INTENT");
      if (localObject1 != null)
      {
        localObject1 = paramGrammarDataObject.getChild("CSHDPST");
        if (localObject1 != null)
        {
          localObject1 = "trx_subcategory";
          localObject2 = "deposit";
          paramGrammarDataObject.addValues((String)localObject1, (String)localObject2);
          break label501;
        }
      }
    }
    localObject1 = "incrdlmt_amt";
    bool1 = paramString1.equals(localObject1);
    if (bool1)
    {
      localObject1 = paramGrammarDataObject.getChild("CRDLIMT");
      if (localObject1 != null)
      {
        localObject2 = paramGrammarDataObject.getChild("AMT");
        if (localObject2 != null)
        {
          paramString1 = getChild"AMT"values.get("amt");
          paramGrammarDataObject.addValues("incrdlmt_amt_old", paramString1);
          paramGrammarDataObject.addValues("incrdlmt_amt_new", paramString2);
          values.remove("incrdlmt_amt");
          return;
        }
      }
    }
    localObject1 = "billprcs_type";
    bool1 = paramString1.equals(localObject1);
    if (bool1)
    {
      localObject1 = paramGrammarDataObject.getChild("TRANSINTENT");
      if (localObject1 != null)
      {
        localObject1 = paramGrammarDataObject.getChild("BILLPRCS");
        if (localObject1 != null)
        {
          localObject2 = values;
          boolean bool3 = ((a)localObject2).containsKey(paramString1);
          if (bool3)
          {
            paramString2 = values.get(paramString1);
            paramGrammarDataObject.addValues(paramString1, paramString2);
            return;
          }
        }
      }
    }
    label501:
    paramGrammarDataObject.addValues(paramString1, paramString2);
  }
  
  public static void valueOverrideAfterFinalise(Finalise paramFinalise, ArrayList paramArrayList)
  {
    paramArrayList = paramFinalise.getValueMap();
    Object localObject1 = paramFinalise.getCategory();
    Object localObject2 = paramFinalise.getAddress();
    Object localObject3 = "IPAYTM";
    boolean bool1 = ((String)localObject2).equals(localObject3);
    if (bool1)
    {
      localObject2 = paramFinalise.getCategory();
      localObject3 = "GRM_BANK";
      bool1 = ((String)localObject2).equals(localObject3);
      if (bool1)
      {
        localObject2 = "acc_num";
        bool1 = paramArrayList.containsKey((String)localObject2);
        if (bool1)
        {
          paramArrayList.put("aux_instr", "mob");
          localObject2 = "aux_instrval";
          localObject3 = paramArrayList.remove("acc_num");
          paramArrayList.put((String)localObject2, (String)localObject3);
        }
        localObject2 = "waladd_amt";
        bool1 = paramArrayList.containsKey((String)localObject2);
        if (bool1)
        {
          localObject3 = paramArrayList.remove("waladd_amt");
          paramArrayList.put("trx_amt", (String)localObject3);
          paramArrayList.put("trx_type", "credit");
          localObject2 = "trx_currency";
          localObject3 = paramArrayList.remove("waladd_currency");
          paramArrayList.put((String)localObject2, (String)localObject3);
        }
        localObject2 = "trx_amt";
        bool1 = paramArrayList.containsKey((String)localObject2);
        if (bool1)
        {
          paramArrayList.put("acc_num", "PYTM");
          localObject2 = "acc_type";
          localObject3 = "wallet";
          paramArrayList.put((String)localObject2, (String)localObject3);
        }
      }
    }
    localObject2 = "GRM_BILL";
    bool1 = ((String)localObject1).equals(localObject2);
    if (!bool1)
    {
      localObject2 = "GRM_BANK";
      bool1 = ((String)localObject1).equals(localObject2);
      if (!bool1) {}
    }
    else
    {
      localObject2 = "status";
      bool1 = paramArrayList.containsKey((String)localObject2);
      if (bool1)
      {
        localObject2 = paramArrayList.get("status");
        localObject3 = "st_promo";
        bool1 = ((String)localObject2).equals(localObject3);
        if (bool1)
        {
          localObject1 = "GRM_OFFERS";
          localObject2 = "GRM_OFFERS";
          paramFinalise.setCategory((String)localObject2);
        }
      }
    }
    int i = ((String)localObject1).hashCode();
    int k = 4;
    float f1 = 5.6E-45F;
    int i1 = 5;
    int i2 = -1;
    int i3 = 2;
    int i4 = 1;
    boolean bool7;
    switch (i)
    {
    default: 
      break;
    case 1240557924: 
      localObject2 = "GRM_BILL";
      boolean bool6 = ((String)localObject1).equals(localObject2);
      if (bool6)
      {
        int i5 = 2;
        f2 = 2.8E-45F;
      }
      break;
    case 1240550297: 
      localObject2 = "GRM_BANK";
      bool7 = ((String)localObject1).equals(localObject2);
      if (bool7)
      {
        bool7 = false;
        f2 = 0.0F;
        localObject1 = null;
      }
      break;
    case 1009862158: 
      localObject2 = "GRM_OTP";
      bool7 = ((String)localObject1).equals(localObject2);
      if (bool7)
      {
        int i6 = 6;
        f2 = 8.4E-45F;
      }
      break;
    case -186141357: 
      localObject2 = "GRM_NOTIF";
      boolean bool8 = ((String)localObject1).equals(localObject2);
      if (bool8)
      {
        int i7 = 4;
        f2 = 5.6E-45F;
      }
      break;
    case -1296211247: 
      localObject2 = "GRM_DELIVERY";
      boolean bool9 = ((String)localObject1).equals(localObject2);
      if (bool9)
      {
        int i8 = 5;
        f2 = 7.0E-45F;
      }
      break;
    case -1301422793: 
      localObject2 = "GRM_TRAVEL";
      boolean bool10 = ((String)localObject1).equals(localObject2);
      if (bool10)
      {
        int i9 = 3;
        f2 = 4.2E-45F;
      }
      break;
    case -1455517772: 
      localObject2 = "GRM_OFFERS";
      boolean bool11 = ((String)localObject1).equals(localObject2);
      if (bool11)
      {
        bool11 = true;
        f2 = Float.MIN_VALUE;
      }
      break;
    }
    int i10 = -1;
    float f2 = 0.0F / 0.0F;
    i = 0;
    localObject2 = null;
    boolean bool12;
    Object localObject4;
    label2189:
    String str2;
    label4248:
    Object localObject5;
    String str3;
    switch (i10)
    {
    default: 
      break;
    case 6: 
      localObject1 = "cab_amt";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        paramFinalise.setCategory("GRM_BILL");
        paramArrayList.put("bill_category", "commute");
        localObject2 = "booking";
        paramArrayList.put("bill_type", (String)localObject2);
        localObject1 = "cab_type";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (bool12)
        {
          localObject1 = "cab_type";
          paramArrayList.remove((String)localObject1);
        }
        localObject2 = paramArrayList.remove("cab_amt");
        paramArrayList.put("due_amt", (String)localObject2);
        localObject1 = "dueamt_currency";
        localObject2 = paramArrayList.remove("cab_currency");
        paramArrayList.put((String)localObject1, (String)localObject2);
      }
      else
      {
        localObject1 = "fare_amt";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (bool12)
        {
          localObject1 = paramFinalise.getAddress();
          localObject2 = "OLACAB";
          bool12 = ((String)localObject1).equals(localObject2);
          if (bool12)
          {
            paramFinalise.setCategory("GRM_BILL");
            paramArrayList.put("bill_category", "commute");
            paramArrayList.put("bill_type", "booking");
            localObject2 = paramArrayList.remove("fare_amt");
            paramArrayList.put("due_amt", (String)localObject2);
            localObject1 = "dueamt_currency";
            localObject2 = paramArrayList.remove("fare_currency");
            paramArrayList.put((String)localObject1, (String)localObject2);
          }
        }
      }
      break;
    case 5: 
      localObject1 = "status";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = "status";
        paramArrayList.remove((String)localObject1);
      }
      localObject1 = "status1";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = "status1";
        paramArrayList.remove((String)localObject1);
      }
      localObject1 = "item";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = "item";
        paramArrayList.remove((String)localObject1);
      }
      localObject1 = "ordnotif_status";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject2 = paramArrayList.remove("ordnotif_status");
        paramArrayList.put("order_status", (String)localObject2);
        localObject1 = "ordnotif_num";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (bool12)
        {
          localObject1 = "tracking_id";
          localObject2 = paramArrayList.remove("ordnotif_num");
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
      }
      localObject1 = "tracking_status";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = "order_status";
        localObject2 = paramArrayList.remove("tracking_status");
        paramArrayList.put((String)localObject1, (String)localObject2);
      }
      localObject1 = "order_num";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = "order_id";
        localObject2 = paramArrayList.remove("order_num");
        paramArrayList.put((String)localObject1, (String)localObject2);
      }
      localObject1 = "tracking_num";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = "tracking_id";
        localObject2 = paramArrayList.remove("tracking_num");
        paramArrayList.put((String)localObject1, (String)localObject2);
      }
      localObject1 = paramArrayList.get("order_status");
      if (localObject1 != null)
      {
        localObject1 = paramArrayList.get("order_status");
        i = ((String)localObject1).hashCode();
        k = -1274064209;
        f1 = -1.3347993E-7F;
        if (i != k)
        {
          k = -1273769907;
          f1 = -1.3766221E-7F;
          if (i != k)
          {
            k = -733821516;
            f1 = -3.34616225E12F;
            if (i == k)
            {
              localObject2 = "arrvtdy";
              bool12 = ((String)localObject1).equals(localObject2);
              if (bool12) {
                i2 = 1;
              }
            }
          }
          else
          {
            localObject2 = "arrvontm";
            bool12 = ((String)localObject1).equals(localObject2);
            if (bool12) {
              i2 = 2;
            }
          }
        }
        else
        {
          localObject2 = "arrverly";
          bool12 = ((String)localObject1).equals(localObject2);
          if (bool12)
          {
            i2 = 0;
            localObject4 = null;
          }
        }
        switch (i2)
        {
        default: 
          break;
        case 2: 
          paramArrayList.put("order_status", "transit");
          localObject1 = "order_substatus";
          localObject2 = "arrvontm";
          paramArrayList.put((String)localObject1, (String)localObject2);
          break;
        case 1: 
          paramArrayList.put("order_status", "transit");
          localObject1 = "order_substatus";
          localObject2 = "arrvtdy";
          paramArrayList.put((String)localObject1, (String)localObject2);
          break;
        case 0: 
          paramArrayList.put("order_status", "transit");
          localObject1 = "order_substatus";
          localObject2 = "arrverly";
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
        localObject1 = "order_amt";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (!bool12)
        {
          localObject1 = "amt";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            localObject1 = "order_amt";
            localObject2 = paramArrayList.remove("amt");
            paramArrayList.put((String)localObject1, (String)localObject2);
          }
          else
          {
            localObject1 = "trx_amt";
            bool12 = paramArrayList.containsKey((String)localObject1);
            if (bool12)
            {
              localObject1 = "order_amt";
              localObject2 = paramArrayList.remove("trx_amt");
              paramArrayList.put((String)localObject1, (String)localObject2);
            }
          }
        }
        localObject1 = "order_date";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (bool12)
        {
          localObject1 = "date";
          localObject2 = paramArrayList.remove("order_date");
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
        localObject1 = "order_reason";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (bool12)
        {
          localObject1 = paramArrayList.get("order_status");
          localObject2 = "ordrcv";
          bool12 = ((String)localObject1).equals(localObject2);
          if (bool12)
          {
            localObject1 = "order_status";
            localObject2 = "ordcancl";
            paramArrayList.put((String)localObject1, (String)localObject2);
          }
        }
        localObject1 = "tracking_item";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (bool12)
        {
          localObject1 = "order_item";
          localObject2 = paramArrayList.remove("tracking_item");
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
      }
      break;
    case 4: 
      localObject1 = "decline_amt";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (!bool12)
      {
        localObject1 = "trx_amt";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (!bool12) {}
      }
      else
      {
        localObject1 = "chqclr_num";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (!bool12)
        {
          paramArrayList.put("notif_category", "trx_decline");
          localObject2 = "decline_amt";
          paramArrayList.put("notif_type1", (String)localObject2);
          localObject1 = "decline_amt";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            localObject1 = "notif_val1";
            localObject2 = paramArrayList.get("decline_amt");
            paramArrayList.put((String)localObject1, (String)localObject2);
            break label2189;
          }
          localObject2 = paramArrayList.get("trx_amt");
          paramArrayList.put("notif_val1", (String)localObject2);
          localObject1 = "decline_amt";
          localObject2 = paramArrayList.remove("trx_amt");
          paramArrayList.put((String)localObject1, (String)localObject2);
          break label2189;
        }
      }
      localObject1 = "decline_reason";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (!bool12)
      {
        localObject1 = "decline_type";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (!bool12)
        {
          localObject1 = "acc_status";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            localObject1 = "notif_category";
            localObject2 = "acc_decline";
            paramArrayList.put((String)localObject1, (String)localObject2);
            break label2189;
          }
          localObject1 = "chqclr_num";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            localObject1 = "notif_category";
            localObject2 = "cheque_status";
            paramArrayList.put((String)localObject1, (String)localObject2);
            break label2189;
          }
          localObject1 = "conv_amt";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            paramArrayList.put("notif_category", "convert_emi");
            localObject2 = paramArrayList.get("conv_amt");
            paramArrayList.put("notif_val1", (String)localObject2);
            localObject1 = "notif_type1";
            localObject2 = "convert_amt";
            paramArrayList.put((String)localObject1, (String)localObject2);
            break label2189;
          }
          localObject1 = "overdrawn_amt";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (!bool12) {
            break label2189;
          }
          paramArrayList.put("notif_category", "acc_overdraw");
          localObject2 = paramArrayList.get("overdrawn_amt");
          paramArrayList.put("notif_val1", (String)localObject2);
          localObject1 = "notif_type1";
          localObject2 = "overdrawn_amt";
          paramArrayList.put((String)localObject1, (String)localObject2);
          break label2189;
        }
      }
      localObject1 = "notif_category";
      localObject2 = "trx_decline";
      paramArrayList.put((String)localObject1, (String)localObject2);
      localObject1 = "notif_val1";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (!bool12)
      {
        localObject1 = "acc_amt";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (bool12)
        {
          localObject2 = paramArrayList.get("acc_amt");
          paramArrayList.put("notif_val1", (String)localObject2);
          localObject1 = "notif_type1";
          localObject2 = "decline_amt";
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
      }
      localObject1 = "notif_category";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = paramArrayList.get("notif_category");
        localObject2 = "trx_decline";
        bool12 = ((String)localObject1).equals(localObject2);
        if (bool12)
        {
          localObject1 = "acc_num";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            paramArrayList.put("notif_type2", "acc_num");
            localObject2 = paramArrayList.remove("acc_num");
            paramArrayList.put("notif_val2", (String)localObject2);
            localObject1 = "debit";
            localObject2 = paramArrayList.get("acc_type");
            bool12 = ((String)localObject1).equals(localObject2);
            if (!bool12)
            {
              localObject1 = "credit";
              localObject2 = paramArrayList.get("acc_type");
              bool12 = ((String)localObject1).equals(localObject2);
              if (!bool12)
              {
                paramArrayList.put("notif_type4", "acc_type");
                localObject1 = "notif_val4";
                localObject2 = paramArrayList.remove("acc_type");
                paramArrayList.put((String)localObject1, (String)localObject2);
              }
            }
          }
          localObject1 = "decline_num";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            localObject1 = "notif_type2";
            bool12 = paramArrayList.containsKey((String)localObject1);
            if (!bool12)
            {
              paramArrayList.put("notif_type2", "acc_num");
              localObject2 = paramArrayList.remove("decline_num");
              paramArrayList.put("notif_val2", (String)localObject2);
              paramArrayList.put("notif_type4", "acc_type");
              localObject1 = "notif_val4";
              localObject2 = paramArrayList.remove("decline_type");
              paramArrayList.put((String)localObject1, (String)localObject2);
            }
            else
            {
              paramArrayList.put("notif_type3", "auxacc_num");
              localObject2 = paramArrayList.remove("decline_num");
              paramArrayList.put("notif_val3", (String)localObject2);
              localObject1 = "debit";
              localObject2 = paramArrayList.get("decline_type");
              bool12 = ((String)localObject1).equals(localObject2);
              if (!bool12)
              {
                localObject1 = "credit";
                localObject2 = paramArrayList.get("decline_type");
                bool12 = ((String)localObject1).equals(localObject2);
                if (!bool12)
                {
                  paramArrayList.put("notif_type5", "auxacc_type");
                  localObject1 = "notif_val5";
                  localObject2 = paramArrayList.remove("decline_type");
                  paramArrayList.put((String)localObject1, (String)localObject2);
                }
              }
            }
          }
          localObject1 = "decline_reason";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            localObject2 = paramArrayList.get("decline_reason");
            paramArrayList.put("notif_subcateg", (String)localObject2);
            localObject1 = "acc_status";
            bool12 = paramArrayList.containsKey((String)localObject1);
            if (bool12)
            {
              localObject1 = "decline_status";
              localObject2 = paramArrayList.remove("acc_status");
              paramArrayList.put((String)localObject1, (String)localObject2);
            }
          }
          else
          {
            localObject1 = "decline_type";
            bool12 = paramArrayList.containsKey((String)localObject1);
            if (bool12)
            {
              localObject1 = "debit";
              localObject2 = paramArrayList.get("decline_type");
              bool12 = ((String)localObject1).equals(localObject2);
              if (!bool12)
              {
                localObject1 = "credit";
                localObject2 = paramArrayList.get("decline_type");
                bool12 = ((String)localObject1).equals(localObject2);
                if (!bool12)
                {
                  localObject2 = paramArrayList.get("decline_type");
                  paramArrayList.put("notif_subcateg", (String)localObject2);
                  localObject2 = paramArrayList.remove("decline_type");
                  paramArrayList.put("decline_reason", (String)localObject2);
                  localObject1 = "acc_status";
                  bool12 = paramArrayList.containsKey((String)localObject1);
                  if (bool12)
                  {
                    localObject1 = "decline_status";
                    localObject2 = paramArrayList.remove("acc_status");
                    paramArrayList.put((String)localObject1, (String)localObject2);
                  }
                }
              }
            }
          }
        }
      }
      localObject1 = "notif_category";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = paramArrayList.get("notif_category");
        localObject2 = "acc_decline";
        bool12 = ((String)localObject1).equals(localObject2);
        if (bool12)
        {
          localObject1 = "acc_num";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            paramArrayList.put("notif_type2", "acc_num");
            localObject2 = paramArrayList.remove("acc_num");
            paramArrayList.put("notif_val2", (String)localObject2);
            paramArrayList.put("notif_type4", "acc_type");
            localObject1 = "notif_val4";
            localObject2 = paramArrayList.remove("acc_type");
            paramArrayList.put((String)localObject1, (String)localObject2);
          }
          localObject1 = "acc_num1";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            paramArrayList.put("notif_type3", "auxacc_num");
            localObject2 = paramArrayList.remove("acc_num1");
            paramArrayList.put("notif_val3", (String)localObject2);
            paramArrayList.put("notif_type5", "auxacc_type");
            localObject1 = "notif_val5";
            localObject2 = paramArrayList.remove("acc_type1");
            paramArrayList.put((String)localObject1, (String)localObject2);
          }
          localObject2 = paramArrayList.get("acc_status");
          paramArrayList.put("notif_subcateg", (String)localObject2);
          localObject1 = "decline_status";
          localObject2 = paramArrayList.remove("acc_status");
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
      }
      localObject1 = "notif_category";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = paramArrayList.get("notif_category");
        localObject2 = "cheque_status";
        bool12 = ((String)localObject1).equals(localObject2);
        if (bool12)
        {
          localObject1 = "chqclr_num";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            paramArrayList.put("notif_type2", "acc_num");
            localObject2 = paramArrayList.remove("chqclr_num");
            paramArrayList.put("notif_val2", (String)localObject2);
            paramArrayList.put("notif_type4", "acc_type");
            localObject2 = "cheque";
            paramArrayList.put("notif_val4", (String)localObject2);
            localObject1 = "chqclr_type";
            paramArrayList.remove((String)localObject1);
          }
          localObject1 = "acc_num";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            paramArrayList.put("notif_type3", "auxacc_num");
            localObject2 = paramArrayList.remove("acc_num");
            paramArrayList.put("notif_val3", (String)localObject2);
            paramArrayList.put("notif_type5", "auxacc_type");
            localObject1 = "notif_val5";
            localObject2 = paramArrayList.remove("acc_type");
            paramArrayList.put((String)localObject1, (String)localObject2);
          }
          localObject1 = "chqclr_amt";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            paramArrayList.put("notif_type1", "trx_amt");
            localObject2 = paramArrayList.remove("chqclr_amt");
            paramArrayList.put("notif_val1", (String)localObject2);
            localObject1 = "chqclr_currency";
            paramArrayList.remove((String)localObject1);
          }
          localObject1 = "notif_subcateg";
          localObject2 = "clearing";
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
      }
      localObject1 = "notif_category";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = paramArrayList.get("notif_category");
        localObject2 = "trx_decline";
        bool12 = ((String)localObject1).equals(localObject2);
        if (!bool12)
        {
          localObject1 = paramArrayList.get("notif_category");
          localObject2 = "acc_decline";
          bool12 = ((String)localObject1).equals(localObject2);
          if (!bool12) {}
        }
        else
        {
          localObject1 = "cheque";
          localObject2 = paramArrayList.get("notif_val4");
          bool12 = ((String)localObject1).equals(localObject2);
          if (!bool12)
          {
            localObject1 = "cheque";
            localObject2 = paramArrayList.get("notif_val5");
            bool12 = ((String)localObject1).equals(localObject2);
            if (!bool12) {}
          }
          else
          {
            paramArrayList.put("notif_category", "cheque_status");
            localObject1 = "notif_subcateg";
            localObject2 = paramArrayList.get("decline_status");
            paramArrayList.put((String)localObject1, (String)localObject2);
          }
        }
      }
      break;
    case 3: 
      localObject1 = "flight_name";
      bool12 = paramArrayList.containsKey((String)localObject1);
      String str1;
      if (bool12)
      {
        localObject4 = "Flight";
        paramArrayList.put("travel_category", (String)localObject4);
        localObject1 = "flight_id";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (bool12)
        {
          localObject1 = Pattern.compile(Pattern.quote(paramArrayList.get("flight_name")), i3);
          localObject4 = paramArrayList.get("flight_id");
          str1 = " ";
          str2 = "";
          localObject4 = ((String)localObject4).replaceAll(str1, str2);
          localObject1 = ((Pattern)localObject1).matcher((CharSequence)localObject4);
          bool12 = ((Matcher)localObject1).find();
          if (bool12)
          {
            localObject1 = "travel_vendor";
            localObject4 = paramArrayList.get("flight_id");
            paramArrayList.put((String)localObject1, (String)localObject4);
          }
          else
          {
            localObject1 = "travel_vendor";
            localObject4 = new java/lang/StringBuilder;
            ((StringBuilder)localObject4).<init>();
            str1 = paramArrayList.get("flight_name");
            ((StringBuilder)localObject4).append(str1);
            ((StringBuilder)localObject4).append(" ");
            str1 = paramArrayList.get("flight_id");
            ((StringBuilder)localObject4).append(str1);
            localObject4 = ((StringBuilder)localObject4).toString();
            paramArrayList.put((String)localObject1, (String)localObject4);
          }
        }
        else
        {
          localObject1 = "travel_vendor";
          localObject4 = paramArrayList.get("flight_name");
          paramArrayList.put((String)localObject1, (String)localObject4);
        }
      }
      else
      {
        localObject1 = "train_num";
        bool12 = paramArrayList.containsKey((String)localObject1);
        if (bool12)
        {
          localObject4 = "Train";
          paramArrayList.put("travel_category", (String)localObject4);
          localObject1 = "vendor";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            localObject1 = "vendor";
            localObject4 = "IRCTC";
            paramArrayList.put((String)localObject1, (String)localObject4);
          }
          localObject1 = "travel_vendor";
          localObject4 = "IRCTC";
          paramArrayList.put((String)localObject1, (String)localObject4);
        }
        else
        {
          localObject4 = "Bus";
          paramArrayList.put("travel_category", (String)localObject4);
          localObject1 = "vendor";
          bool12 = paramArrayList.containsKey((String)localObject1);
          if (bool12)
          {
            localObject1 = "travel_vendor";
            localObject4 = paramArrayList.get("vendor");
            paramArrayList.put((String)localObject1, (String)localObject4);
          }
        }
      }
      localObject1 = "dept_time";
      bool12 = paramArrayList.containsKey((String)localObject1);
      if (bool12)
      {
        localObject1 = paramArrayList.get("dept_time");
        int i11 = ((String)localObject1).length();
        if (i11 == k)
        {
          localObject1 = paramArrayList.get("dept_time");
          localObject4 = ":";
          bool13 = ((String)localObject1).contains((CharSequence)localObject4);
          if (!bool13)
          {
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>();
            localObject4 = paramArrayList.get("dept_time").substring(0, i3);
            ((StringBuilder)localObject1).append((String)localObject4);
            ((StringBuilder)localObject1).append(":");
            localObject4 = paramArrayList.get("dept_time");
            localObject3 = ((String)localObject4).substring(i3, k);
            ((StringBuilder)localObject1).append((String)localObject3);
            localObject1 = ((StringBuilder)localObject1).toString();
            break label4248;
          }
        }
        localObject1 = paramArrayList.get("dept_time");
      }
      else
      {
        localObject1 = "time";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          localObject1 = paramArrayList.get("time");
        }
        else
        {
          bool13 = false;
          localObject1 = null;
          f2 = 0.0F;
        }
      }
      localObject3 = "dept_date";
      boolean bool3 = paramArrayList.containsKey((String)localObject3);
      if (bool3)
      {
        localObject2 = paramArrayList.get("dept_date");
      }
      else
      {
        localObject3 = "date";
        bool3 = paramArrayList.containsKey((String)localObject3);
        if (bool3) {
          localObject2 = paramArrayList.get("date");
        }
      }
      if ((localObject2 != null) && (localObject1 != null))
      {
        int m = ((String)localObject1).length();
        if (m > i1) {
          localObject1 = ((String)localObject1).substring(0, i1);
        }
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject5 = ((String)localObject2).substring(0, 10);
        ((StringBuilder)localObject3).append((String)localObject5);
        i1 = 16;
        i2 = 11;
        str3 = ((String)localObject2).substring(i2, i1);
        str1 = "00:00";
        boolean bool5 = str3.equals(str1);
        if (bool5)
        {
          localObject2 = new java/lang/StringBuilder;
          localObject5 = " ";
          ((StringBuilder)localObject2).<init>((String)localObject5);
          ((StringBuilder)localObject2).append((String)localObject1);
          ((StringBuilder)localObject2).append(":00");
          localObject1 = ((StringBuilder)localObject2).toString();
        }
        else
        {
          localObject1 = new java/lang/StringBuilder;
          str3 = " ";
          ((StringBuilder)localObject1).<init>(str3);
          localObject2 = ((String)localObject2).substring(i2, i1);
          ((StringBuilder)localObject1).append((String)localObject2);
          localObject2 = ":00";
          ((StringBuilder)localObject1).append((String)localObject2);
          localObject1 = ((StringBuilder)localObject1).toString();
        }
        ((StringBuilder)localObject3).append((String)localObject1);
        localObject2 = ((StringBuilder)localObject3).toString();
      }
      if (localObject2 != null)
      {
        paramArrayList.put("date", (String)localObject2);
        localObject1 = "dept_date";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          localObject1 = "dept_date";
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
      }
      localObject1 = "ticket_id";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        localObject1 = "trip_id";
        localObject2 = paramArrayList.get("ticket_id");
        paramArrayList.put((String)localObject1, (String)localObject2);
      }
      localObject1 = "pnr_num";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        localObject1 = "pnr_id";
        localObject2 = paramArrayList.remove("pnr_num");
        paramArrayList.put((String)localObject1, (String)localObject2);
      }
      localObject1 = "fare_num";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        localObject1 = "fare_amt";
        localObject2 = paramArrayList.remove("fare_num");
        paramArrayList.put((String)localObject1, (String)localObject2);
      }
      localObject1 = "from_loc";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        localObject1 = "to_loc";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13) {}
      }
      else
      {
        localObject1 = "Train";
        localObject2 = paramArrayList.get("travel_category");
        bool13 = ((String)localObject1).equals(localObject2);
        if (bool13)
        {
          localObject1 = "train_num";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (bool13) {}
        }
        else
        {
          localObject1 = "travel_category";
          localObject2 = "Notif";
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
      }
      localObject1 = "train_num";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        localObject1 = "trip_id";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (!bool13)
        {
          localObject1 = "trip_id";
          localObject2 = paramArrayList.get("train_num");
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
      }
      localObject1 = "fltalert_id";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        localObject1 = "fltalert_type";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          paramArrayList.put("travel_category", "Alert");
          localObject2 = paramArrayList.remove("fltalert_type");
          paramArrayList.put("alert_type", (String)localObject2);
          localObject1 = "travel_vendor";
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          localObject3 = paramArrayList.remove("fltalert_name");
          ((StringBuilder)localObject2).append((String)localObject3);
          ((StringBuilder)localObject2).append(" ");
          localObject3 = paramArrayList.remove("fltalert_id");
          ((StringBuilder)localObject2).append((String)localObject3);
          localObject2 = ((StringBuilder)localObject2).toString();
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
      }
      localObject1 = "alert_id";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (!bool13)
      {
        localObject1 = "alert_num";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (!bool13) {}
      }
      else
      {
        localObject1 = "alert_type";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          localObject2 = "Alert";
          paramArrayList.put("travel_category", (String)localObject2);
          localObject1 = "alert_id";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (bool13)
          {
            localObject1 = "pnr_id";
            localObject2 = paramArrayList.remove("alert_id");
            paramArrayList.put((String)localObject1, (String)localObject2);
          }
          else
          {
            localObject1 = "pnr_id";
            localObject2 = paramArrayList.remove("alert_num");
            paramArrayList.put((String)localObject1, (String)localObject2);
          }
        }
      }
      break;
    case 2: 
      localObject1 = "discacc_type";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        paramFinalise.setCategory("GRM_NOTIF");
        paramArrayList.put("notif_category", "discacc");
        localObject2 = paramArrayList.get("discacc_type");
        paramArrayList.put("notif_subcateg", (String)localObject2);
        paramArrayList.put("notif_type1", "discacc_num");
        localObject2 = paramArrayList.get("discacc_num");
        paramArrayList.put("notif_val1", (String)localObject2);
        localObject2 = "bill_amt";
        paramArrayList.put("notif_type2", (String)localObject2);
        localObject1 = "bill_amt";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          localObject1 = "notif_val2";
          localObject2 = paramArrayList.get("bill_amt");
          paramArrayList.put((String)localObject1, (String)localObject2);
        }
        else
        {
          localObject1 = "amt";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (bool13)
          {
            localObject1 = "notif_val2";
            localObject2 = paramArrayList.get("amt");
            paramArrayList.put((String)localObject1, (String)localObject2);
          }
        }
      }
      else
      {
        localObject1 = "rechrg_amt";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          localObject1 = "bal_amt";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (!bool13)
          {
            localObject1 = "ref_id";
            bool13 = paramArrayList.containsKey((String)localObject1);
            if (!bool13) {}
          }
          else
          {
            paramArrayList.put("bill_category", "prepaid_bill");
            paramArrayList.put("bill_type", "recharge");
            localObject3 = paramArrayList.remove("rechrg_amt");
            paramArrayList.put("due_amt", (String)localObject3);
            localObject1 = "dueamt_currency";
            localObject3 = paramArrayList.remove("rechrg_currency");
            paramArrayList.put((String)localObject1, (String)localObject3);
            break label5596;
          }
        }
        localObject1 = "emi_amt";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          paramArrayList.put("bill_category", "payment_due");
          localObject3 = paramArrayList.remove("emi_due");
          paramArrayList.put("bill_type", (String)localObject3);
          localObject3 = paramArrayList.remove("emi_amt");
          paramArrayList.put("due_amt", (String)localObject3);
          localObject3 = paramArrayList.remove("emi_currency");
          paramArrayList.put("dueamt_currency", (String)localObject3);
          localObject1 = "emi_num";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (bool13)
          {
            localObject3 = paramArrayList.remove("emi_num");
            paramArrayList.put("dueins_num", (String)localObject3);
            localObject1 = "dueins_type";
            localObject3 = paramArrayList.remove("emi_type");
            paramArrayList.put((String)localObject1, (String)localObject3);
          }
        }
        label5596:
        localObject1 = "emiacc_type";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          paramArrayList.put("bill_category", "payment_due");
          localObject3 = paramArrayList.remove("emiacc_due");
          paramArrayList.put("bill_type", (String)localObject3);
          localObject1 = "emiacc_num";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (bool13)
          {
            localObject1 = "dueins_num";
            localObject3 = paramArrayList.remove("emiacc_num");
            paramArrayList.put((String)localObject1, (String)localObject3);
          }
          localObject1 = "dueins_type";
          localObject3 = paramArrayList.remove("emiacc_type");
          paramArrayList.put((String)localObject1, (String)localObject3);
        }
        else
        {
          localObject1 = "amtrcv_amt";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (bool13)
          {
            paramArrayList.put("bill_category", "bill_status");
            paramArrayList.put("bill_type", "payment_received");
            localObject3 = paramArrayList.remove("amtrcv_amt");
            paramArrayList.put("due_amt", (String)localObject3);
            localObject3 = paramArrayList.remove("amtrcv_currency");
            paramArrayList.put("dueamt_currency", (String)localObject3);
            paramArrayList.remove("amtrcv_due");
            localObject1 = "amtrcv_type";
            bool13 = paramArrayList.containsKey((String)localObject1);
            if (bool13)
            {
              localObject1 = "amtrcv_type";
              paramArrayList.remove((String)localObject1);
            }
            localObject1 = "amtrcv_num";
            bool13 = paramArrayList.containsKey((String)localObject1);
            if (bool13)
            {
              paramArrayList.put("dueins_type", "acc");
              localObject1 = "dueins_num";
              localObject3 = paramArrayList.remove("amtrcv_num");
              paramArrayList.put((String)localObject1, (String)localObject3);
            }
          }
          else
          {
            localObject1 = "policy_amt";
            bool13 = paramArrayList.containsKey((String)localObject1);
            if (!bool13)
            {
              localObject1 = "policy_due";
              bool13 = paramArrayList.containsKey((String)localObject1);
              if (!bool13)
              {
                localObject1 = "billresche_amt";
                bool13 = paramArrayList.containsKey((String)localObject1);
                if (bool13)
                {
                  localObject3 = "bill_status";
                  paramArrayList.put("bill_category", (String)localObject3);
                  localObject1 = "billresche_due";
                  bool13 = paramArrayList.containsKey((String)localObject1);
                  if (bool13)
                  {
                    localObject1 = "bill_type";
                    localObject3 = paramArrayList.remove("billresche_due");
                    paramArrayList.put((String)localObject1, (String)localObject3);
                  }
                  else
                  {
                    localObject1 = "billresche_type";
                    bool13 = paramArrayList.containsKey((String)localObject1);
                    if (bool13)
                    {
                      localObject1 = "bill_type";
                      localObject3 = paramArrayList.remove("billresche_type");
                      paramArrayList.put((String)localObject1, (String)localObject3);
                    }
                  }
                  localObject3 = paramArrayList.remove("billresche_amt");
                  paramArrayList.put("due_amt", (String)localObject3);
                  localObject1 = "dueamt_currency";
                  localObject3 = paramArrayList.remove("billresche_currency");
                  paramArrayList.put((String)localObject1, (String)localObject3);
                  break label7199;
                }
                localObject1 = "preclosure_amt";
                bool13 = paramArrayList.containsKey((String)localObject1);
                if (bool13)
                {
                  paramArrayList.put("bill_category", "loan_status");
                  paramArrayList.put("bill_type", "preclosure");
                  localObject3 = paramArrayList.remove("preclosure_amt");
                  paramArrayList.put("due_amt", (String)localObject3);
                  localObject1 = "dueamt_currency";
                  localObject3 = paramArrayList.remove("preclosure_currency");
                  paramArrayList.put((String)localObject1, (String)localObject3);
                  break label7199;
                }
                localObject1 = "billprcs_num";
                bool13 = paramArrayList.containsKey((String)localObject1);
                if (!bool13)
                {
                  localObject1 = "billprcs_amt";
                  bool13 = paramArrayList.containsKey((String)localObject1);
                  if (!bool13)
                  {
                    localObject1 = "approvedloan_amt";
                    bool13 = paramArrayList.containsKey((String)localObject1);
                    if (bool13)
                    {
                      paramArrayList.put("bill_category", "loan_status");
                      paramArrayList.put("bill_type", "approved");
                      localObject3 = paramArrayList.remove("approvedloan_num");
                      paramArrayList.put("dueins_num", (String)localObject3);
                      localObject3 = paramArrayList.remove("approvedloan_type");
                      paramArrayList.put("dueins_type", (String)localObject3);
                      localObject3 = paramArrayList.remove("approvedloan_amt");
                      paramArrayList.put("due_amt", (String)localObject3);
                      localObject1 = "dueamt_currency";
                      localObject3 = paramArrayList.remove("approvedloan_currency");
                      paramArrayList.put((String)localObject1, (String)localObject3);
                      break label7199;
                    }
                    localObject1 = "due_num";
                    bool13 = paramArrayList.containsKey((String)localObject1);
                    if (bool13)
                    {
                      paramArrayList.put("bill_category", "payment_due");
                      localObject3 = paramArrayList.remove("due_due");
                      paramArrayList.put("bill_type", (String)localObject3);
                      localObject1 = "due_amt";
                      localObject3 = paramArrayList.remove("due_num");
                      paramArrayList.put((String)localObject1, (String)localObject3);
                      break label7199;
                    }
                    localObject1 = "bill_due";
                    bool13 = paramArrayList.containsKey((String)localObject1);
                    if (bool13)
                    {
                      localObject3 = "payment_due";
                      paramArrayList.put("bill_category", (String)localObject3);
                      localObject1 = "bill_num";
                      bool13 = paramArrayList.containsKey((String)localObject1);
                      if (bool13)
                      {
                        localObject3 = paramArrayList.get("bill_type");
                        paramArrayList.put("dueins_type", (String)localObject3);
                        localObject1 = "dueins_num";
                        localObject3 = paramArrayList.remove("bill_num");
                        paramArrayList.put((String)localObject1, (String)localObject3);
                      }
                      localObject1 = "bill_type";
                      localObject3 = paramArrayList.remove("bill_due");
                      paramArrayList.put((String)localObject1, (String)localObject3);
                      break label7199;
                    }
                    localObject1 = "cab_amt";
                    bool13 = paramArrayList.containsKey((String)localObject1);
                    if (bool13)
                    {
                      paramArrayList.put("bill_category", "commute");
                      localObject3 = "booking";
                      paramArrayList.put("bill_type", (String)localObject3);
                      localObject1 = "cab_type";
                      bool13 = paramArrayList.containsKey((String)localObject1);
                      if (bool13)
                      {
                        localObject1 = "cab_type";
                        paramArrayList.remove((String)localObject1);
                      }
                      localObject3 = paramArrayList.remove("cab_amt");
                      paramArrayList.put("due_amt", (String)localObject3);
                      localObject1 = "dueamt_currency";
                      localObject3 = paramArrayList.remove("cab_currency");
                      paramArrayList.put((String)localObject1, (String)localObject3);
                      break label7199;
                    }
                    localObject1 = "cabcancel_amt";
                    bool13 = paramArrayList.containsKey((String)localObject1);
                    if (bool13)
                    {
                      paramArrayList.put("bill_category", "commute");
                      paramArrayList.put("bill_type", "cancel");
                      localObject3 = paramArrayList.remove("cabcancel_amt");
                      paramArrayList.put("due_amt", (String)localObject3);
                      localObject1 = "dueamt_currency";
                      localObject3 = paramArrayList.remove("cabcancel_currency");
                      paramArrayList.put((String)localObject1, (String)localObject3);
                      break label7199;
                    }
                    localObject1 = "bill_category";
                    bool13 = paramArrayList.containsKey((String)localObject1);
                    if (bool13) {
                      break label7199;
                    }
                    localObject1 = "bill_category";
                    localObject3 = "payment_notif";
                    paramArrayList.put((String)localObject1, (String)localObject3);
                    break label7199;
                  }
                }
                paramArrayList.put("bill_category", "bill_status");
                localObject3 = paramArrayList.remove("billprcs_type");
                paramArrayList.put("bill_type", (String)localObject3);
                localObject1 = "billprcs_num";
                bool13 = paramArrayList.containsKey((String)localObject1);
                if (bool13)
                {
                  localObject3 = paramArrayList.remove("billprcs_num");
                  paramArrayList.put("dueins_num", (String)localObject3);
                  localObject1 = "dueins_type";
                  localObject3 = "acc";
                  paramArrayList.put((String)localObject1, (String)localObject3);
                }
                localObject1 = "billprcs_amt";
                bool13 = paramArrayList.containsKey((String)localObject1);
                if (!bool13) {
                  break label7199;
                }
                localObject3 = paramArrayList.remove("billprcs_amt");
                paramArrayList.put("due_amt", (String)localObject3);
                localObject1 = "dueamt_currency";
                localObject3 = paramArrayList.remove("billprcs_currency");
                paramArrayList.put((String)localObject1, (String)localObject3);
                break label7199;
              }
            }
            paramArrayList.put("bill_category", "payment_due");
            localObject3 = paramArrayList.remove("policy_due");
            paramArrayList.put("bill_type", (String)localObject3);
            localObject1 = "policy_amt";
            bool13 = paramArrayList.containsKey((String)localObject1);
            if (bool13)
            {
              localObject3 = paramArrayList.remove("policy_amt");
              paramArrayList.put("due_amt", (String)localObject3);
              localObject1 = "dueamt_currency";
              localObject3 = paramArrayList.remove("policy_currency");
              paramArrayList.put((String)localObject1, (String)localObject3);
            }
            localObject3 = "policy";
            paramArrayList.put("dueins_type", (String)localObject3);
            localObject1 = "policy_num";
            bool13 = paramArrayList.containsKey((String)localObject1);
            if (bool13)
            {
              localObject1 = "dueins_num";
              localObject3 = paramArrayList.remove("policy_num");
              paramArrayList.put((String)localObject1, (String)localObject3);
            }
          }
        }
        label7199:
        localObject1 = paramFinalise.getAddress();
        localObject3 = "OLACAB";
        bool13 = ((String)localObject1).equals(localObject3);
        if (bool13)
        {
          localObject3 = "commute";
          paramArrayList.put("bill_category", (String)localObject3);
          localObject1 = "bill_type";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (!bool13)
          {
            localObject1 = "bill_type";
            localObject3 = "booking";
            paramArrayList.put((String)localObject1, (String)localObject3);
          }
        }
        localObject1 = "trx_amt";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          localObject1 = "due_amt";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (!bool13)
          {
            localObject3 = paramArrayList.remove("trx_amt");
            paramArrayList.put("due_amt", (String)localObject3);
            localObject3 = paramArrayList.remove("trx_currency");
            paramArrayList.put("dueamt_currency", (String)localObject3);
            localObject1 = "trx_type";
            paramArrayList.remove((String)localObject1);
          }
        }
        else
        {
          localObject1 = "bill_amt";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (bool13)
          {
            localObject3 = paramArrayList.remove("bill_amt");
            paramArrayList.put("due_amt", (String)localObject3);
            localObject3 = paramArrayList.remove("bill_currency");
            paramArrayList.put("dueamt_currency", (String)localObject3);
            paramArrayList.remove("bill_due");
            localObject1 = "bill_category";
            bool13 = paramArrayList.containsKey((String)localObject1);
            if (bool13)
            {
              localObject1 = "dueins_num";
              bool13 = paramArrayList.containsKey((String)localObject1);
              if (!bool13)
              {
                localObject1 = "acc_num";
                bool13 = paramArrayList.containsKey((String)localObject1);
                if (!bool13) {}
              }
              else
              {
                localObject1 = "bill_type";
                bool13 = paramArrayList.containsKey((String)localObject1);
                if (!bool13)
                {
                  localObject1 = paramArrayList.get("bill_category");
                  localObject3 = "payment_notif";
                  bool13 = ((String)localObject1).equals(localObject3);
                  if (bool13)
                  {
                    localObject1 = "bill_category";
                    localObject3 = "payment_due";
                    paramArrayList.put((String)localObject1, (String)localObject3);
                  }
                }
              }
            }
            localObject1 = "min_amt";
            bool13 = paramArrayList.containsKey((String)localObject1);
            if (bool13)
            {
              localObject3 = "payment_due";
              paramArrayList.put("bill_category", (String)localObject3);
              localObject1 = "min_amt";
            }
          }
        }
      }
      break;
    }
    try
    {
      localObject1 = paramArrayList.get((String)localObject1);
      f2 = Float.parseFloat((String)localObject1);
      localObject3 = "due_amt";
      localObject3 = paramArrayList.get((String)localObject3);
      f1 = Float.parseFloat((String)localObject3);
      bool13 = f2 < f1;
      if (bool13)
      {
        localObject1 = "aux_amt";
        localObject3 = "due_amt";
        localObject3 = paramArrayList.get((String)localObject3);
        paramArrayList.put((String)localObject1, (String)localObject3);
        localObject1 = "due_amt";
        localObject3 = "min_amt";
        localObject3 = paramArrayList.remove((String)localObject3);
        paramArrayList.put((String)localObject1, (String)localObject3);
      }
      else
      {
        localObject1 = "aux_amt";
        localObject3 = "min_amt";
        localObject3 = paramArrayList.remove((String)localObject3);
        paramArrayList.put((String)localObject1, (String)localObject3);
      }
    }
    catch (Exception localException1)
    {
      for (;;) {}
    }
    paramArrayList.put("aux_type", "min");
    paramArrayList.remove("min_due");
    localObject1 = "aux_currency";
    localObject3 = paramArrayList.remove("min_currency");
    paramArrayList.put((String)localObject1, (String)localObject3);
    localObject1 = "bal_amt";
    boolean bool13 = paramArrayList.containsKey((String)localObject1);
    if (bool13)
    {
      localObject3 = paramArrayList.remove("bal_amt");
      paramArrayList.put("aux_amt", (String)localObject3);
      paramArrayList.put("aux_type", "bal");
      localObject1 = "aux_currency";
      localObject3 = paramArrayList.remove("bal_currency");
      paramArrayList.put((String)localObject1, (String)localObject3);
    }
    else
    {
      localObject1 = "min_amt";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        localObject1 = "due_amt";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (!bool13)
        {
          localObject3 = paramArrayList.remove("min_amt");
          paramArrayList.put("due_amt", (String)localObject3);
          paramArrayList.put("due_type", "min");
          paramArrayList.remove("min_due");
          localObject1 = "due_currency";
          localObject3 = paramArrayList.remove("min_currency");
          paramArrayList.put((String)localObject1, (String)localObject3);
        }
        else
        {
          localObject1 = "min_amt";
        }
      }
    }
    try
    {
      localObject1 = paramArrayList.get((String)localObject1);
      f2 = Float.parseFloat((String)localObject1);
      localObject3 = "due_amt";
      localObject3 = paramArrayList.get((String)localObject3);
      f1 = Float.parseFloat((String)localObject3);
      bool13 = f2 < f1;
      if (bool13)
      {
        localObject1 = "aux_amt";
        localObject3 = "due_amt";
        localObject3 = paramArrayList.get((String)localObject3);
        paramArrayList.put((String)localObject1, (String)localObject3);
        localObject1 = "due_amt";
        localObject3 = "min_amt";
        localObject3 = paramArrayList.remove((String)localObject3);
        paramArrayList.put((String)localObject1, (String)localObject3);
      }
      else
      {
        localObject1 = "aux_amt";
        localObject3 = "min_amt";
        localObject3 = paramArrayList.remove((String)localObject3);
        paramArrayList.put((String)localObject1, (String)localObject3);
      }
    }
    catch (Exception localException2)
    {
      boolean bool4;
      int i12;
      boolean bool14;
      int i13;
      for (;;) {}
    }
    paramArrayList.put("aux_type", "min");
    paramArrayList.remove("min_due");
    localObject1 = "aux_currency";
    localObject3 = paramArrayList.remove("min_currency");
    paramArrayList.put((String)localObject1, (String)localObject3);
    localObject1 = "bal_type";
    bool13 = paramArrayList.containsKey((String)localObject1);
    if (bool13)
    {
      localObject1 = "bal_type";
      paramArrayList.remove((String)localObject1);
    }
    localObject1 = "mobile_num";
    bool13 = paramArrayList.containsKey((String)localObject1);
    if (bool13)
    {
      localObject1 = "bill_date";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (!bool13)
      {
        localObject1 = "bill_status";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (!bool13) {}
      }
      else
      {
        localObject1 = "bill_status";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (!bool13)
        {
          localObject1 = "bill_category";
          localObject3 = "payment_due";
          paramArrayList.put((String)localObject1, (String)localObject3);
        }
        paramArrayList.put("dueins_type", "mobile");
        localObject1 = "dueins_num";
        localObject3 = paramArrayList.remove("mobile_num");
        paramArrayList.put((String)localObject1, (String)localObject3);
        break label8841;
      }
    }
    localObject1 = "billno_num";
    bool13 = paramArrayList.containsKey((String)localObject1);
    if (bool13)
    {
      localObject1 = "billno_type";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        localObject1 = "dueins_type";
        localObject3 = paramArrayList.remove("billno_type");
        paramArrayList.put((String)localObject1, (String)localObject3);
      }
      else
      {
        localObject1 = "dueins_type";
        localObject3 = "acc";
        paramArrayList.put((String)localObject1, (String)localObject3);
      }
      localObject1 = "dueins_num";
      localObject3 = paramArrayList.remove("billno_num");
      paramArrayList.put((String)localObject1, (String)localObject3);
    }
    else
    {
      localObject1 = "consumer_num";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        localObject1 = "rechrg_amt";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          localObject1 = "due_amt";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (!bool13)
          {
            paramArrayList.put("bill_category", "prepaid_bill");
            paramArrayList.put("bill_type", "recharge");
            localObject3 = paramArrayList.remove("rechrg_amt");
            paramArrayList.put("due_amt", (String)localObject3);
            localObject1 = "dueamt_currency";
            localObject3 = paramArrayList.remove("rechrg_currency");
            paramArrayList.put((String)localObject1, (String)localObject3);
          }
        }
        localObject1 = "dueins_type";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (!bool13)
        {
          paramArrayList.put("dueins_type", "consumer");
          localObject1 = "dueins_num";
          localObject3 = paramArrayList.remove("consumer_num");
          paramArrayList.put((String)localObject1, (String)localObject3);
        }
      }
      else
      {
        localObject1 = "acc_num";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          localObject1 = "dueins_num";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (bool13)
          {
            localObject3 = paramArrayList.remove("acc_type");
            paramArrayList.put("aux_instr", (String)localObject3);
            localObject1 = "aux_instrval";
            localObject3 = paramArrayList.remove("acc_num");
            paramArrayList.put((String)localObject1, (String)localObject3);
          }
          else
          {
            localObject1 = "acc_num1";
            bool13 = paramArrayList.containsKey((String)localObject1);
            if (bool13)
            {
              localObject3 = paramArrayList.remove("acc_type1");
              paramArrayList.put("aux_instr", (String)localObject3);
              localObject1 = "aux_instrval";
              localObject3 = paramArrayList.remove("acc_num1");
              paramArrayList.put((String)localObject1, (String)localObject3);
            }
            localObject1 = paramArrayList.remove("acc_type");
            localObject3 = "dueins_type";
            if (localObject1 == null) {
              localObject1 = "acc";
            }
            paramArrayList.put((String)localObject3, (String)localObject1);
            localObject1 = "dueins_num";
            localObject3 = paramArrayList.remove("acc_num");
            paramArrayList.put((String)localObject1, (String)localObject3);
          }
        }
      }
    }
    label8841:
    localObject1 = "mobile_num";
    bool13 = paramArrayList.containsKey((String)localObject1);
    if (bool13)
    {
      localObject1 = "rechrg_amt";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        paramArrayList.put("bill_category", "prepaid_bill");
        paramArrayList.put("bill_type", "recharge");
        localObject3 = paramArrayList.remove("rechrg_amt");
        paramArrayList.put("due_amt", (String)localObject3);
        localObject1 = "dueamt_currency";
        localObject3 = paramArrayList.remove("rechrg_currency");
        paramArrayList.put((String)localObject1, (String)localObject3);
      }
      localObject1 = "dueins_type";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (!bool13)
      {
        paramArrayList.put("dueins_type", "mobile");
        localObject1 = "dueins_num";
        localObject3 = paramArrayList.remove("mobile_num");
        paramArrayList.put((String)localObject1, (String)localObject3);
      }
    }
    else
    {
      localObject1 = "mrp_amt";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (!bool13)
      {
        localObject1 = "rechrg_amt";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (!bool13) {}
      }
      else
      {
        localObject1 = "rechrg_type";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          localObject1 = paramArrayList.get("rechrg_type");
          localObject3 = "success";
          bool13 = ((String)localObject1).equals(localObject3);
          if (bool13)
          {
            paramArrayList.put("bill_category", "prepaid_bill");
            localObject3 = "recharge";
            paramArrayList.put("bill_type", (String)localObject3);
            localObject1 = "mrp_amt";
            bool13 = paramArrayList.containsKey((String)localObject1);
            if (bool13)
            {
              localObject3 = paramArrayList.remove("mrp_amt");
              paramArrayList.put("due_amt", (String)localObject3);
              localObject1 = "dueamt_currency";
              localObject3 = paramArrayList.remove("mrp_currency");
              paramArrayList.put((String)localObject1, (String)localObject3);
            }
            else
            {
              localObject3 = paramArrayList.remove("rechrg_amt");
              paramArrayList.put("due_amt", (String)localObject3);
              localObject1 = "dueamt_currency";
              localObject3 = paramArrayList.remove("rechrg_currency");
              paramArrayList.put((String)localObject1, (String)localObject3);
            }
          }
        }
      }
    }
    localObject1 = "bill_date";
    bool13 = paramArrayList.containsKey((String)localObject1);
    if (bool13)
    {
      localObject1 = "date";
      localObject3 = paramArrayList.remove("bill_date");
      paramArrayList.put((String)localObject1, (String)localObject3);
    }
    else
    {
      localObject1 = "due_date";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (bool13)
      {
        localObject3 = paramArrayList.remove("due_date");
        paramArrayList.put("date", (String)localObject3);
        localObject1 = "due_due";
        paramArrayList.remove((String)localObject1);
      }
      else
      {
        localObject1 = "emi_date";
        bool13 = paramArrayList.containsKey((String)localObject1);
        if (bool13)
        {
          localObject1 = "date";
          localObject3 = paramArrayList.get("emi_date");
          paramArrayList.put((String)localObject1, (String)localObject3);
        }
        else
        {
          localObject1 = "policy_date";
          bool13 = paramArrayList.containsKey((String)localObject1);
          if (bool13)
          {
            localObject1 = "date";
            localObject3 = paramArrayList.remove("policy_date");
            paramArrayList.put((String)localObject1, (String)localObject3);
          }
        }
      }
    }
    localObject1 = "dueins_type";
    bool13 = paramArrayList.containsKey((String)localObject1);
    if (bool13)
    {
      localObject1 = "card";
      localObject3 = paramArrayList.get("dueins_type");
      bool13 = ((String)localObject1).equals(localObject3);
      if (bool13)
      {
        localObject1 = "dueins_type";
        localObject3 = "creditcard";
        paramArrayList.put((String)localObject1, (String)localObject3);
      }
    }
    localObject1 = "status";
    bool13 = paramArrayList.containsKey((String)localObject1);
    if (bool13)
    {
      localObject1 = paramArrayList.get("status");
      localObject3 = "st_";
      bool13 = ((String)localObject1).startsWith((String)localObject3);
      if (!bool13)
      {
        localObject1 = "status";
        paramArrayList.remove((String)localObject1);
      }
    }
    localObject1 = "trx_subcategory";
    bool13 = paramArrayList.containsKey((String)localObject1);
    if (bool13)
    {
      localObject1 = "trx_subcategory";
      paramArrayList.remove((String)localObject1);
    }
    localObject1 = "bill_category";
    bool13 = paramArrayList.containsKey((String)localObject1);
    if (bool13)
    {
      localObject1 = "due_amt";
      bool13 = paramArrayList.containsKey((String)localObject1);
      if (!bool13)
      {
        localObject1 = "bill_category";
        localObject3 = "notif";
        paramArrayList.put((String)localObject1, (String)localObject3);
      }
    }
    localObject1 = Controller.getInstance();
    localObject3 = paramFinalise.getAddress();
    localObject1 = ((Compiler)localObject1).verifyAddress((String)localObject3);
    localObject3 = "due_amt";
    bool4 = paramArrayList.containsKey((String)localObject3);
    if (!bool4)
    {
      localObject3 = "bill_category";
      bool4 = paramArrayList.containsKey((String)localObject3);
      if (bool4)
      {
        localObject3 = paramArrayList.get("bill_category");
        localObject5 = "notif";
        bool4 = ((String)localObject3).equals(localObject5);
        if (!bool4) {
          break label12364;
        }
        if (localObject1 != null)
        {
          i12 = ((ArrayList)localObject1).size();
          if (i12 != 0) {
            break label12364;
          }
        }
      }
      paramFinalise.setCategory(null);
      break label12364;
      localObject1 = "offer_mode";
      bool14 = paramArrayList.containsKey((String)localObject1);
      if (bool14)
      {
        localObject1 = "mult_offer_mode";
        bool14 = paramArrayList.containsKey((String)localObject1);
        if (bool14)
        {
          localObject1 = paramArrayList.get("mult_offer_mode");
          i13 = Integer.parseInt((String)localObject1);
        }
        else
        {
          i13 = 1;
          f2 = Float.MIN_VALUE;
        }
        i = 0;
        localObject2 = null;
        while (i < i13)
        {
          if (i == 0) {
            localObject3 = "";
          } else {
            localObject3 = String.valueOf(i);
          }
          localObject4 = String.valueOf(localObject3);
          localObject5 = "offer_mode".concat((String)localObject4);
          localObject5 = paramArrayList.get((String)localObject5).split("_");
          localObject4 = localObject5[0];
          localObject5 = localObject5[i4];
          str2 = String.valueOf(localObject3);
          str3 = "offer_type".concat(str2);
          paramArrayList.put(str3, (String)localObject4);
          str3 = String.valueOf(localObject3);
          localObject4 = "offer_ins".concat(str3);
          paramArrayList.put((String)localObject4, (String)localObject5);
          localObject4 = String.valueOf(localObject3);
          localObject5 = "offer_val".concat((String)localObject4);
          localObject4 = "offer_mode";
          localObject3 = String.valueOf(localObject3);
          localObject3 = ((String)localObject4).concat((String)localObject3);
          localObject3 = paramArrayList.get((String)localObject3);
          localObject3 = paramArrayList.remove((String)localObject3);
          paramArrayList.put((String)localObject5, (String)localObject3);
          i += 1;
          continue;
          localObject1 = "upi_user";
          boolean bool15 = paramArrayList.containsKey((String)localObject1);
          if (bool15)
          {
            localObject1 = "upi_num";
            bool15 = paramArrayList.containsKey((String)localObject1);
            if (bool15)
            {
              paramArrayList.put("trx_subcategory", "upi");
              localObject3 = paramArrayList.remove("upi_num");
              paramArrayList.put("ref_id", (String)localObject3);
              paramArrayList.put("aux_instr", "user");
              localObject1 = "aux_instrval";
              localObject3 = paramArrayList.remove("upi_user");
              paramArrayList.put((String)localObject1, (String)localObject3);
              break label10964;
            }
          }
          localObject1 = "upi_user_from";
          bool15 = paramArrayList.containsKey((String)localObject1);
          if (bool15)
          {
            localObject1 = "upi_user_to";
            bool15 = paramArrayList.containsKey((String)localObject1);
            if (bool15)
            {
              localObject1 = "upi_num";
              bool15 = paramArrayList.containsKey((String)localObject1);
              if (bool15)
              {
                paramArrayList.put("trx_subcategory", "upi");
                localObject3 = paramArrayList.remove("upi_num");
                paramArrayList.put("ref_id", (String)localObject3);
                paramArrayList.put("aux_instr", "user");
                localObject1 = "aux_instrval";
                localObject3 = "trx_type";
                bool4 = paramArrayList.containsKey((String)localObject3);
                if (bool4)
                {
                  localObject3 = paramArrayList.get("trx_type");
                  localObject5 = "credit";
                  bool4 = ((String)localObject3).equals(localObject5);
                  if (bool4)
                  {
                    localObject3 = "upi_user_from";
                    break label10292;
                  }
                }
                localObject3 = "upi_user_to";
                label10292:
                localObject3 = paramArrayList.get((String)localObject3);
                paramArrayList.put((String)localObject1, (String)localObject3);
                paramArrayList.remove("upi_user_from");
                localObject1 = "upi_user_to";
                paramArrayList.remove((String)localObject1);
                break label10964;
              }
            }
          }
          localObject1 = "imps_num";
          bool15 = paramArrayList.containsKey((String)localObject1);
          if (bool15)
          {
            paramArrayList.put("trx_subcategory", "imps");
            localObject1 = "ref_id";
            localObject3 = paramArrayList.remove("imps_num");
            paramArrayList.put((String)localObject1, (String)localObject3);
          }
          else
          {
            localObject1 = "neft_num";
            bool15 = paramArrayList.containsKey((String)localObject1);
            if (bool15)
            {
              paramArrayList.put("trx_subcategory", "neft");
              localObject1 = "ref_id";
              localObject3 = paramArrayList.remove("neft_num");
              paramArrayList.put((String)localObject1, (String)localObject3);
            }
            else
            {
              localObject1 = "trx_subcategory";
              bool15 = paramArrayList.containsKey((String)localObject1);
              if (bool15)
              {
                localObject1 = paramArrayList.get("trx_subcategory");
                localObject3 = "imps";
                bool15 = ((String)localObject1).equals(localObject3);
                if (bool15)
                {
                  localObject1 = "mobile_num";
                  bool15 = paramArrayList.containsKey((String)localObject1);
                  if (bool15)
                  {
                    paramArrayList.put("aux_instr", "mob");
                    localObject1 = "aux_instrval";
                    localObject3 = paramArrayList.remove("mobile_num");
                    paramArrayList.put((String)localObject1, (String)localObject3);
                    break label10964;
                  }
                }
              }
              localObject1 = "trx_subcategory";
              bool15 = paramArrayList.containsKey((String)localObject1);
              if (bool15)
              {
                localObject1 = paramArrayList.get("trx_subcategory");
                localObject3 = "benef";
                bool15 = ((String)localObject1).equals(localObject3);
                if (bool15)
                {
                  localObject1 = "ref_id";
                  bool15 = paramArrayList.containsKey((String)localObject1);
                  if (bool15)
                  {
                    localObject1 = "ref_id";
                    localObject3 = paramArrayList.remove("ref_id");
                    paramArrayList.put((String)localObject1, (String)localObject3);
                    break label10964;
                  }
                }
              }
              localObject1 = "trx_subcategory";
              bool15 = paramArrayList.containsKey((String)localObject1);
              if (!bool15)
              {
                localObject1 = "trx_amt";
                bool15 = paramArrayList.containsKey((String)localObject1);
                if (!bool15)
                {
                  localObject1 = "bal_amt";
                  bool15 = paramArrayList.containsKey((String)localObject1);
                  if (bool15)
                  {
                    localObject1 = "acc_num";
                    bool15 = paramArrayList.containsKey((String)localObject1);
                    if (bool15)
                    {
                      localObject1 = "trx_subcategory";
                      localObject3 = "balenq";
                      paramArrayList.put((String)localObject1, (String)localObject3);
                      break label10964;
                    }
                  }
                }
              }
              localObject1 = "trx_subcategory";
              bool15 = paramArrayList.containsKey((String)localObject1);
              if (!bool15)
              {
                localObject1 = "trx_amt";
                bool15 = paramArrayList.containsKey((String)localObject1);
                if (bool15)
                {
                  localObject1 = "trx_subcategory";
                  localObject3 = "trx";
                  paramArrayList.put((String)localObject1, (String)localObject3);
                  break label10964;
                }
              }
              localObject1 = "trx_subcategory";
              bool15 = paramArrayList.containsKey((String)localObject1);
              if (!bool15)
              {
                localObject1 = "futtrx_amt";
                bool15 = paramArrayList.containsKey((String)localObject1);
                if (bool15)
                {
                  paramArrayList.put("trx_subcategory", "futexpense");
                  localObject3 = paramArrayList.get("futtrx_amt");
                  paramArrayList.put("trx_amt", (String)localObject3);
                  localObject3 = paramArrayList.get("futtrx_currency");
                  paramArrayList.put("trx_currency", (String)localObject3);
                  localObject1 = "futtrx_type";
                  bool15 = paramArrayList.containsKey((String)localObject1);
                  if (bool15)
                  {
                    localObject3 = paramArrayList.get("futtrx_type");
                    paramArrayList.put("trx_type", (String)localObject3);
                    localObject1 = paramArrayList.get("futtrx_type");
                    localObject3 = "credit";
                    bool15 = ((String)localObject1).equals(localObject3);
                    if (bool15)
                    {
                      localObject1 = "trx_subcategory";
                      localObject3 = "futincome";
                      paramArrayList.put((String)localObject1, (String)localObject3);
                    }
                  }
                }
              }
            }
          }
          label10964:
          localObject1 = "mult_acc_num";
          bool15 = paramArrayList.containsKey((String)localObject1);
          if (bool15)
          {
            localObject1 = "type";
            bool15 = paramArrayList.containsKey((String)localObject1);
            if (bool15)
            {
              localObject1 = paramArrayList.get("type");
              localObject3 = "autdbt";
              bool15 = ((String)localObject1).equals(localObject3);
              if (bool15)
              {
                localObject1 = "trx_subcategory";
                localObject3 = paramArrayList.remove("type");
                paramArrayList.put((String)localObject1, (String)localObject3);
              }
            }
            localObject1 = "trx_subcategory";
            bool15 = paramArrayList.containsKey((String)localObject1);
            if (bool15)
            {
              localObject1 = sanitizeAccNum(paramArrayList.get("acc_num1"));
              if (localObject1 == null)
              {
                localObject1 = "acc_num1";
                paramArrayList.remove((String)localObject1);
              }
              else
              {
                localObject1 = "acc_type1";
                bool15 = paramArrayList.containsKey((String)localObject1);
                if (bool15)
                {
                  localObject3 = paramArrayList.remove("acc_type1");
                  paramArrayList.put("aux_instr", (String)localObject3);
                  localObject1 = "mult_acc_type";
                  paramArrayList.remove((String)localObject1);
                }
                else
                {
                  localObject1 = "aux_instr";
                  localObject3 = "acc";
                  paramArrayList.put((String)localObject1, (String)localObject3);
                }
                localObject3 = paramArrayList.remove("acc_num1");
                paramArrayList.put("aux_instrval", (String)localObject3);
                localObject1 = "mult_acc_num";
                paramArrayList.remove((String)localObject1);
              }
            }
          }
          localObject1 = "acc_num";
          bool15 = paramArrayList.containsKey((String)localObject1);
          if (bool15)
          {
            localObject1 = sanitizeAccNum(paramArrayList.get("acc_num"));
            if (localObject1 != null)
            {
              localObject3 = "acc_num";
              paramArrayList.put((String)localObject3, (String)localObject1);
            }
            else
            {
              localObject1 = "acc_num";
              paramArrayList.remove((String)localObject1);
            }
          }
          localObject1 = "bal_type";
          bool15 = paramArrayList.containsKey((String)localObject1);
          if (bool15)
          {
            localObject1 = "bal_type";
            paramArrayList.remove((String)localObject1);
          }
          localObject1 = "trx_subcategory";
          bool15 = paramArrayList.containsKey((String)localObject1);
          if (bool15)
          {
            localObject1 = "acc_type";
            bool15 = paramArrayList.containsKey((String)localObject1);
            if (!bool15)
            {
              localObject1 = "acc_type";
              localObject3 = "acc";
              paramArrayList.put((String)localObject1, (String)localObject3);
            }
            localObject1 = paramArrayList.get("trx_subcategory");
            localObject3 = "trx";
            bool15 = ((String)localObject1).equals(localObject3);
            if (bool15)
            {
              localObject1 = "trx_category";
              localObject2 = "credit";
              localObject3 = paramArrayList.get("trx_type");
              boolean bool2 = ((String)localObject2).equals(localObject3);
              if (bool2) {
                localObject2 = "income";
              } else {
                localObject2 = "expense";
              }
              paramArrayList.put((String)localObject1, (String)localObject2);
            }
            else
            {
              localObject1 = getTrxCateg();
              localObject3 = paramArrayList.get("trx_subcategory");
              bool4 = ((HashMap)localObject1).containsKey(localObject3);
              if (bool4)
              {
                localObject3 = paramArrayList.get("trx_subcategory");
                localObject1 = (String)((HashMap)localObject1).get(localObject3);
                localObject3 = "transfer";
                bool4 = ((String)localObject1).equals(localObject3);
                if (bool4)
                {
                  localObject3 = "trx_type";
                  bool4 = paramArrayList.containsKey((String)localObject3);
                  if (bool4)
                  {
                    localObject3 = "credit";
                    localObject5 = paramArrayList.get("trx_type");
                    bool4 = ((String)localObject3).equals(localObject5);
                    if (bool4)
                    {
                      localObject1 = "trx_category";
                      localObject3 = "income";
                      paramArrayList.put((String)localObject1, (String)localObject3);
                      break label11578;
                    }
                  }
                }
                localObject3 = "trx_category";
                paramArrayList.put((String)localObject3, (String)localObject1);
              }
              label11578:
              localObject1 = "autdbt";
              localObject3 = paramArrayList.get("trx_subcategory");
              bool15 = ((String)localObject1).equals(localObject3);
              if (bool15)
              {
                localObject1 = "credit";
                localObject3 = paramArrayList.get("trx_type");
                bool15 = ((String)localObject1).equals(localObject3);
                if (bool15)
                {
                  localObject1 = "aux_instr";
                  bool15 = paramArrayList.containsKey((String)localObject1);
                  if (bool15)
                  {
                    localObject1 = "trx_category";
                    localObject3 = "income";
                    paramArrayList.put((String)localObject1, (String)localObject3);
                    break label11686;
                  }
                }
                localObject1 = "trx_type";
                localObject3 = "debit";
                paramArrayList.put((String)localObject1, (String)localObject3);
              }
              label11686:
              localObject1 = paramArrayList.get("trx_category");
              localObject3 = "notif";
              bool15 = ((String)localObject1).equals(localObject3);
              if (bool15)
              {
                paramFinalise.setCategory("GRM_NOTIF");
                localObject1 = paramArrayList.get("trx_subcategory");
                int j = ((String)localObject1).hashCode();
                int n = -1396410949;
                f1 = -5.5837258E-12F;
                if (j != n)
                {
                  n = 93622892;
                  f1 = 1.397164E-35F;
                  if (j != n)
                  {
                    n = 95302275;
                    f1 = 1.6381436E-35F;
                    if (j == n)
                    {
                      localObject2 = "incrdlmt";
                      bool15 = ((String)localObject1).equals(localObject2);
                      if (bool15)
                      {
                        i2 = 0;
                        localObject4 = null;
                      }
                    }
                  }
                  else
                  {
                    localObject2 = "benef";
                    bool15 = ((String)localObject1).equals(localObject2);
                    if (bool15) {
                      i2 = 1;
                    }
                  }
                }
                else
                {
                  localObject2 = "balenq";
                  bool15 = ((String)localObject1).equals(localObject2);
                  if (bool15) {
                    i2 = 2;
                  }
                }
                switch (i2)
                {
                default: 
                  break;
                case 2: 
                  paramArrayList.put("notif_category", "balenq");
                  paramArrayList.put("notif_type1", "acc_num");
                  localObject2 = paramArrayList.get("acc_num");
                  paramArrayList.put("notif_val1", (String)localObject2);
                  paramArrayList.put("notif_type2", "bal_amt");
                  localObject2 = paramArrayList.get("bal_amt");
                  paramArrayList.put("notif_val2", (String)localObject2);
                  localObject1 = "aux_instrval";
                  bool15 = paramArrayList.containsKey((String)localObject1);
                  if (!bool15) {
                    break;
                  }
                  paramArrayList.put("notif_type3", "acc_num");
                  localObject2 = paramArrayList.get("aux_instrval");
                  paramArrayList.put("notif_val3", (String)localObject2);
                  paramArrayList.put("notif_type4", "bal_amt");
                  localObject1 = "notif_val4";
                  localObject2 = paramArrayList.get("bal_amt1");
                  paramArrayList.put((String)localObject1, (String)localObject2);
                  break;
                case 1: 
                  paramArrayList.put("notif_category", "benef");
                  paramArrayList.put("notif_type1", "trx_amt");
                  localObject2 = paramArrayList.get("trx_amt");
                  paramArrayList.put("notif_val1", (String)localObject2);
                  paramArrayList.put("notif_type2", "ref_id");
                  localObject1 = "notif_val2";
                  localObject2 = paramArrayList.get("ref_id");
                  paramArrayList.put((String)localObject1, (String)localObject2);
                  break;
                case 0: 
                  paramArrayList.put("notif_category", "incrdlmt");
                  paramArrayList.put("notif_type1", "incrdlmt_amt_new");
                  localObject2 = paramArrayList.get("incrdlmt_amt_new");
                  paramArrayList.put("notif_val1", (String)localObject2);
                  paramArrayList.put("notif_type2", "incrdlmt_amt_old");
                  localObject1 = "notif_val2";
                  localObject2 = paramArrayList.get("incrdlmt_amt_old");
                  paramArrayList.put((String)localObject1, (String)localObject2);
                  break;
                }
              }
              else
              {
                localObject1 = "trx_amt";
                bool15 = paramArrayList.containsKey((String)localObject1);
                if (!bool15)
                {
                  localObject1 = "futtrx_amt";
                  bool15 = paramArrayList.containsKey((String)localObject1);
                  if (!bool15) {
                    paramFinalise.setCategory(null);
                  }
                }
              }
            }
          }
          else
          {
            paramFinalise.setCategory(null);
          }
          localObject1 = "trx_category";
          bool15 = paramArrayList.containsKey((String)localObject1);
          if (bool15)
          {
            localObject1 = paramArrayList.get("trx_category");
            localObject2 = "transfer";
            bool15 = ((String)localObject1).equals(localObject2);
            if (bool15)
            {
              localObject1 = "vendor";
              bool15 = paramArrayList.containsKey((String)localObject1);
              if (bool15)
              {
                localObject1 = "aux_instr";
                bool15 = paramArrayList.containsKey((String)localObject1);
                if (bool15)
                {
                  localObject1 = "vendor";
                  paramArrayList.remove((String)localObject1);
                }
              }
            }
          }
        }
      }
    }
    label12364:
    cleanup(paramArrayList);
    paramFinalise.setValueMap(paramArrayList);
  }
  
  public static a valueOverrideBeforeFinalise(GrammarDataLinkedListObject paramGrammarDataLinkedListObject, Finalise paramFinalise)
  {
    ValueMap localValueMap = new com/twelfthmile/malana/compiler/types/ValueMap;
    localValueMap.<init>();
    String str1 = paramFinalise.getCategory();
    String str2;
    boolean bool1;
    if (str1 != null)
    {
      str1 = paramFinalise.getCategory();
      str2 = "GRM_OFFERS";
      bool1 = str1.equals(str2);
      if (bool1)
      {
        str1 = paramFinalise.getAddress();
        str2 = "IPAYTM";
        bool1 = str1.equals(str2);
        if (!bool1) {}
      }
    }
    else
    {
      str1 = paramFinalise.getAddress();
      bool1 = str1.equals("INSIDR");
      int j = 0;
      str2 = null;
      if (bool1)
      {
        paramFinalise.setCategory("GRM_EVENT");
        return null;
      }
      paramGrammarDataLinkedListObject = next;
      bool1 = false;
      str1 = null;
      int i;
      while (paramGrammarDataLinkedListObject != null)
      {
        Object localObject = grammarDataObject;
        if (localObject != null)
        {
          localObject = grammarDataObject;
          boolean bool3 = lock;
          if (!bool3)
          {
            localObject = grammarDataObject;
            bool3 = ((GrammarDataObject)localObject).isNonDet();
            if (!bool3)
            {
              localObject = grammarDataObject.values;
              String str3 = "upi_num";
              bool3 = ((a)localObject).containsKey(str3);
              if (bool3)
              {
                paramFinalise.setCategory("GRM_BANK");
                return null;
              }
              localObject = grammarDataObject.values;
              str3 = "flight_id";
              bool3 = ((a)localObject).containsKey(str3);
              if (!bool3)
              {
                localObject = grammarDataObject.values;
                str3 = "to_loc";
                bool3 = ((a)localObject).containsKey(str3);
                if (!bool3)
                {
                  localObject = grammarDataObject.values;
                  str3 = "dept_date";
                  bool3 = ((a)localObject).containsKey(str3);
                  if (!bool3) {
                    break label293;
                  }
                }
              }
              bool1 += true;
              label293:
              localObject = grammarDataObject.values;
              addValues(localValueMap, (a)localObject);
            }
          }
        }
        paramGrammarDataLinkedListObject = next;
      }
      int k = 2;
      if (i >= k)
      {
        paramGrammarDataLinkedListObject = "GRM_TRAVEL";
        paramFinalise.setCategory(paramGrammarDataLinkedListObject);
      }
      paramGrammarDataLinkedListObject = paramFinalise.getAddress();
      str1 = "IPAYTM";
      boolean bool4 = paramGrammarDataLinkedListObject.equals(str1);
      if (bool4)
      {
        paramGrammarDataLinkedListObject = "trx_amt";
        bool4 = localValueMap.containsKey(paramGrammarDataLinkedListObject);
        if (!bool4)
        {
          paramGrammarDataLinkedListObject = "waladd_amt";
          bool4 = localValueMap.containsKey(paramGrammarDataLinkedListObject);
          if (!bool4) {}
        }
        else
        {
          paramGrammarDataLinkedListObject = Pattern.compile("(?:Rs|INR)[\\ \\.]*([0-9\\.]*)[\\ ]*(?:Cashback|cashback|Off|off)");
          str1 = paramFinalise.getMessage();
          paramGrammarDataLinkedListObject = paramGrammarDataLinkedListObject.matcher(str1);
          boolean bool2 = paramGrammarDataLinkedListObject.find();
          if (bool2)
          {
            str1 = "trx_amt";
            bool2 = localValueMap.containsKey(str1);
            if (bool2) {
              str1 = "trx_amt";
            } else {
              str1 = "waladd_amt";
            }
            str1 = localValueMap.get(str1);
            j = 1;
            paramGrammarDataLinkedListObject = paramGrammarDataLinkedListObject.group(j);
            bool4 = paramGrammarDataLinkedListObject.equals(str1);
            if (bool4)
            {
              paramGrammarDataLinkedListObject = Pattern.compile("Paytm has added (?:Rs|INR)[\\ \\.]*([0-9\\.]*)[\\ ]*(?:Cashback|cashback)");
              str1 = paramFinalise.getMessage();
              paramGrammarDataLinkedListObject = paramGrammarDataLinkedListObject.matcher(str1);
              bool4 = paramGrammarDataLinkedListObject.find();
              if (!bool4)
              {
                paramGrammarDataLinkedListObject = "GRM_OFFERS";
                paramFinalise.setCategory(paramGrammarDataLinkedListObject);
              }
              else
              {
                paramGrammarDataLinkedListObject = "GRM_BANK";
                paramFinalise.setCategory(paramGrammarDataLinkedListObject);
              }
            }
            else
            {
              paramGrammarDataLinkedListObject = "GRM_BANK";
              paramFinalise.setCategory(paramGrammarDataLinkedListObject);
            }
          }
          else
          {
            paramGrammarDataLinkedListObject = "GRM_BANK";
            paramFinalise.setCategory(paramGrammarDataLinkedListObject);
          }
        }
      }
    }
    return localValueMap;
  }
  
  static String valueOverrideInMult(String paramString1, String paramString2, String paramString3, a parama)
  {
    String str = "acc_type";
    boolean bool1 = paramString1.equals(str);
    boolean bool2;
    if (bool1)
    {
      str = "acc_num";
      bool2 = parama.containsKey(str);
      if (!bool2)
      {
        paramString1 = getAccTypeValScore();
        break label72;
      }
    }
    parama = "trx_subcategory";
    boolean bool3 = paramString1.equals(parama);
    if (bool3)
    {
      paramString1 = getTrxSubCategScore();
    }
    else
    {
      bool3 = false;
      paramString1 = null;
    }
    label72:
    if (paramString1 != null)
    {
      bool2 = paramString1.containsKey(paramString2);
      if (bool2)
      {
        parama = (Integer)paramString1.get(paramString2);
        int i = parama.intValue();
        bool1 = paramString1.containsKey(paramString3);
        if (bool1)
        {
          paramString1 = (Integer)paramString1.get(paramString3);
          int j = paramString1.intValue();
          if (j > i) {
            paramString2 = paramString3;
          }
        }
        return paramString2;
      }
    }
    return null;
  }
  
  static a valueOverridePostCondensation(a parama)
  {
    String str1 = "trx_num";
    boolean bool = parama.containsKey(str1);
    String str2;
    if (bool)
    {
      str1 = "trx_type";
      bool = parama.containsKey(str1);
      if (bool)
      {
        str2 = parama.remove("trx_num");
        parama.put("acc_num", str2);
        str2 = parama.remove("trx_type");
        parama.put("acc_type", str2);
        str1 = "cheque";
        str2 = parama.get("acc_type");
        bool = str1.equals(str2);
        if (bool) {
          break label277;
        }
        str1 = "trx_type";
        str2 = "debit";
        parama.put(str1, str2);
        break label277;
      }
    }
    str1 = "ref_num";
    bool = parama.containsKey(str1);
    if (bool)
    {
      str1 = "ref_id";
      str2 = parama.remove("ref_num");
      parama.put(str1, str2);
    }
    else
    {
      str1 = "bentrx_amt";
      bool = parama.containsKey(str1);
      if (bool)
      {
        parama.put("trx_subcategory", "benef");
        str2 = parama.remove("bentrx_amt");
        parama.put("trx_amt", str2);
        str1 = "aux_instr";
        str2 = parama.remove("bentrx_type");
        parama.put(str1, str2);
      }
      else
      {
        str1 = "incrdlmt_amt_old";
        bool = parama.containsKey(str1);
        if (bool)
        {
          str1 = "incrdlmt_amt_new";
          bool = parama.containsKey(str1);
          if (bool)
          {
            str1 = "trx_subcategory";
            str2 = "incrdlmt";
            parama.put(str1, str2);
          }
        }
      }
    }
    label277:
    str1 = "trxcatg_id";
    bool = parama.containsKey(str1);
    if (bool)
    {
      str1 = "ref_id";
      str2 = parama.remove("trxcatg_id");
      parama.put(str1, str2);
    }
    str1 = "trxcatg_type";
    bool = parama.containsKey(str1);
    if (bool)
    {
      str1 = "trx_subcategory";
      str2 = parama.remove("trxcatg_type");
      parama.put(str1, str2);
    }
    str1 = "consumer_id";
    bool = parama.containsKey(str1);
    if (bool)
    {
      str1 = "consumer_num";
      str2 = parama.remove("consumer_id");
      parama.put(str1, str2);
    }
    parama.containsKey("amtrcv_id");
    str1 = "acc_id";
    bool = parama.containsKey(str1);
    if (bool)
    {
      str1 = "acc_num";
      str2 = parama.remove("acc_id");
      parama.put(str1, str2);
    }
    return parama;
  }
  
  static HashMap valueOverrideWithinCondensationAdd(a parama1, a parama2)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    parama2 = parama2.entrySet().iterator();
    for (;;)
    {
      boolean bool1 = parama2.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Map.Entry)parama2.next();
      Object localObject2 = (String)((Map.Entry)localObject1).getKey();
      Object localObject3 = "num_class";
      boolean bool2 = ((String)localObject2).equals(localObject3);
      if (!bool2)
      {
        localObject2 = (String)((Map.Entry)localObject1).getKey();
        localObject3 = "type";
        bool2 = ((String)localObject2).equals(localObject3);
        if (bool2)
        {
          localObject2 = (String)((Map.Entry)localObject1).getKey();
          bool2 = parama1.containsKey((String)localObject2);
          if (bool2)
          {
            localObject2 = (String)((Map.Entry)localObject1).getKey();
            localObject2 = parama1.get((String)localObject2);
            localObject3 = ((Map.Entry)localObject1).getValue();
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2)
            {
              localObject2 = (String)((Map.Entry)localObject1).getValue();
              localObject3 = (String)((Map.Entry)localObject1).getKey();
              localObject3 = parama1.get((String)localObject3);
              String str = "neft";
              boolean bool3 = ((String)localObject2).equals(str);
              if (!bool3)
              {
                str = "imps";
                bool3 = ((String)localObject2).equals(str);
                if (!bool3)
                {
                  str = "upi";
                  bool3 = ((String)localObject2).equals(str);
                  if (!bool3)
                  {
                    str = "aeps";
                    bool3 = ((String)localObject2).equals(str);
                    if (!bool3)
                    {
                      str = "rtgs";
                      bool3 = ((String)localObject2).equals(str);
                      if (!bool3) {
                        break label337;
                      }
                    }
                  }
                }
              }
              str = "debit";
              bool3 = ((String)localObject3).equals(str);
              if (bool3)
              {
                localObject1 = ((Map.Entry)localObject1).getKey();
                localHashMap.put(localObject1, localObject2);
                continue;
              }
              label337:
              str = "neft";
              bool3 = ((String)localObject3).equals(str);
              if (!bool3)
              {
                str = "imps";
                bool3 = ((String)localObject3).equals(str);
                if (!bool3)
                {
                  str = "upi";
                  bool3 = ((String)localObject3).equals(str);
                  if (!bool3)
                  {
                    str = "aeps";
                    bool3 = ((String)localObject3).equals(str);
                    if (!bool3)
                    {
                      str = "rtgs";
                      bool3 = ((String)localObject3).equals(str);
                      if (!bool3) {
                        continue;
                      }
                    }
                  }
                }
              }
              str = "debit";
              bool2 = ((String)localObject2).equals(str);
              if (!bool2) {
                continue;
              }
              localObject1 = ((Map.Entry)localObject1).getKey();
              localHashMap.put(localObject1, localObject3);
              continue;
            }
          }
        }
        localObject2 = (String)((Map.Entry)localObject1).getKey();
        localObject3 = "loc";
        bool2 = ((String)localObject2).equals(localObject3);
        if (bool2)
        {
          localObject2 = (String)((Map.Entry)localObject1).getKey();
          bool2 = parama1.containsKey((String)localObject2);
          if (bool2)
          {
            localObject2 = (String)((Map.Entry)localObject1).getKey();
            localObject2 = parama1.get((String)localObject2);
            localObject3 = ((Map.Entry)localObject1).getValue();
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2)
            {
              localObject3 = (String)((Map.Entry)localObject1).getKey();
              localObject3 = parama1.remove((String)localObject3);
              localHashMap.put("from_loc", localObject3);
              localObject2 = "to_loc";
              localObject1 = ((Map.Entry)localObject1).getValue();
              localHashMap.put(localObject2, localObject1);
              continue;
            }
          }
        }
        localObject2 = (String)((Map.Entry)localObject1).getKey();
        localObject3 = "airport";
        bool2 = ((String)localObject2).equals(localObject3);
        if (bool2)
        {
          localObject2 = (String)((Map.Entry)localObject1).getKey();
          bool2 = parama1.containsKey((String)localObject2);
          if (bool2)
          {
            localObject2 = (String)((Map.Entry)localObject1).getKey();
            localObject2 = parama1.get((String)localObject2);
            localObject3 = ((Map.Entry)localObject1).getValue();
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2)
            {
              localObject3 = (String)((Map.Entry)localObject1).getKey();
              localObject3 = parama1.remove((String)localObject3);
              localHashMap.put("from_loc", localObject3);
              localObject2 = "to_loc";
              localObject1 = ((Map.Entry)localObject1).getValue();
              localHashMap.put(localObject2, localObject1);
              continue;
            }
          }
        }
        localObject2 = ((Map.Entry)localObject1).getKey();
        localObject1 = ((Map.Entry)localObject1).getValue();
        localHashMap.put(localObject2, localObject1);
      }
    }
    return localHashMap;
  }
  
  static void valueOverrideWithinCondensationContext(String paramString1, String paramString2, GrammarDataObject paramGrammarDataObject)
  {
    Object localObject = "discount_pct";
    boolean bool1 = paramString1.equals(localObject);
    if (!bool1)
    {
      localObject = "discount_amt";
      bool1 = paramString1.equals(localObject);
      if (!bool1)
      {
        localObject = "cashback_amt";
        bool1 = paramString1.equals(localObject);
        if (!bool1)
        {
          localObject = "cashback_pct";
          bool1 = paramString1.equals(localObject);
          if (!bool1)
          {
            localObject = "save_amt";
            bool1 = paramString1.equals(localObject);
            if (!bool1)
            {
              localObject = "save_pct";
              bool1 = paramString1.equals(localObject);
              if (!bool1)
              {
                localObject = "trx_amt";
                bool1 = paramString1.equals(localObject);
                String str1;
                String str2;
                boolean bool2;
                if (bool1)
                {
                  localObject = paramGrammarDataObject.getChild("TRX");
                  if (localObject != null)
                  {
                    str1 = str;
                    str2 = "withdrawn";
                    bool2 = str1.equalsIgnoreCase(str2);
                    if (!bool2)
                    {
                      str1 = str;
                      str2 = "withdrawal";
                      bool2 = str1.equalsIgnoreCase(str2);
                      if (!bool2)
                      {
                        localObject = str;
                        str1 = "w/d";
                        bool1 = ((String)localObject).equalsIgnoreCase(str1);
                        if (!bool1) {
                          break label217;
                        }
                      }
                    }
                    localObject = "trx_subcategory";
                    str1 = "withdraw";
                    paramGrammarDataObject.addValues((String)localObject, str1);
                    break label440;
                  }
                }
                label217:
                localObject = "trx_amt";
                bool1 = paramString1.equals(localObject);
                if (bool1)
                {
                  localObject = paramGrammarDataObject.getChild("TRX");
                  if (localObject != null)
                  {
                    str1 = str;
                    str2 = "refund";
                    bool2 = str1.equalsIgnoreCase(str2);
                    if (!bool2)
                    {
                      str1 = str;
                      str2 = "refunded";
                      bool2 = str1.equalsIgnoreCase(str2);
                      if (!bool2)
                      {
                        localObject = str;
                        str1 = "reversed";
                        bool1 = ((String)localObject).equalsIgnoreCase(str1);
                        if (!bool1) {
                          break label332;
                        }
                      }
                    }
                    localObject = "trx_subcategory";
                    str1 = "refund";
                    paramGrammarDataObject.addValues((String)localObject, str1);
                    break label440;
                  }
                }
                label332:
                localObject = "rechrg_num";
                bool1 = paramString1.equals(localObject);
                if (bool1)
                {
                  localObject = paramGrammarDataObject.getChild("TRANSINTENT");
                  if (localObject != null)
                  {
                    localObject = paramGrammarDataObject.getChild("CONSUMERNUM");
                    if (localObject != null)
                    {
                      paramGrammarDataObject.addValues("consumer_num", paramString2);
                      return;
                    }
                  }
                }
                localObject = "rechrg_num";
                bool1 = paramString1.equals(localObject);
                if (!bool1) {
                  break label440;
                }
                localObject = paramGrammarDataObject.getChild("RCHRGSUCC");
                if (localObject == null) {
                  break label440;
                }
                localObject = paramGrammarDataObject.getChild("NUM");
                if (localObject == null) {
                  break label440;
                }
                paramGrammarDataObject.addValues("mobile_num", paramString2);
                return;
              }
            }
          }
        }
      }
    }
    localObject = "offer_mode";
    paramGrammarDataObject.addValues((String)localObject, paramString1);
    label440:
    paramGrammarDataObject.addValues(paramString1, paramString2);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.branch.BranchRules
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */