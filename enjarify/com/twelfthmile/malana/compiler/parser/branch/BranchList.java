package com.twelfthmile.malana.compiler.parser.branch;

import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import java.util.ArrayList;
import java.util.Iterator;

public class BranchList
{
  ArrayList buffer;
  boolean gaveList = false;
  ArrayList list;
  boolean lock = false;
  
  public void add(Branch paramBranch)
  {
    boolean bool = lock;
    if (!bool)
    {
      localArrayList = list;
      if (localArrayList == null)
      {
        localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
        list = localArrayList;
      }
      list.add(paramBranch);
      return;
    }
    ArrayList localArrayList = buffer;
    if (localArrayList == null)
    {
      localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      buffer = localArrayList;
    }
    buffer.add(paramBranch);
  }
  
  public void clear()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Iterator localIterator = list.iterator();
    localIterator.next();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Branch localBranch = (Branch)localIterator.next();
      boolean bool2 = condensationFlag;
      if (!bool2)
      {
        GrammarDataObject localGrammarDataObject = condensedGdo;
        if (localGrammarDataObject != null)
        {
          localGrammarDataObject = condensedGdo;
          bool2 = localGrammarDataObject.hasLockedChildren();
          if (!bool2) {}
        }
      }
      else
      {
        localArrayList.add(localBranch);
      }
    }
    list.removeAll(localArrayList);
  }
  
  public void clearAll()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Iterator localIterator = list.iterator();
    localIterator.next();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Branch localBranch = (Branch)localIterator.next();
      localArrayList.add(localBranch);
    }
    list.removeAll(localArrayList);
  }
  
  public ArrayList getList()
  {
    boolean bool = gaveList;
    if (!bool)
    {
      bool = true;
      lock = bool;
      gaveList = bool;
      return list;
    }
    ArrayList localArrayList1 = buffer;
    if (localArrayList1 != null)
    {
      int i = localArrayList1.size();
      if (i != 0)
      {
        localArrayList1 = list;
        ArrayList localArrayList2 = buffer;
        localArrayList1.addAll(localArrayList2);
        localArrayList1 = new java/util/ArrayList;
        localArrayList2 = buffer;
        localArrayList1.<init>(localArrayList2);
        localArrayList2 = new java/util/ArrayList;
        localArrayList2.<init>();
        buffer = localArrayList2;
        return localArrayList1;
      }
    }
    gaveList = false;
    lock = false;
    return null;
  }
  
  public int getSize()
  {
    return list.size();
  }
  
  public Branch main()
  {
    return (Branch)list.get(0);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.branch.BranchList
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */