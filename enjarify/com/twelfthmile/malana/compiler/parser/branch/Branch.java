package com.twelfthmile.malana.compiler.parser.branch;

import com.twelfthmile.b.a.a;
import com.twelfthmile.malana.compiler.datastructure.GrammarTrie;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataLinkedListObject;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.types.GrammarCallback;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Branch
{
  int branchIndex;
  int branchSize;
  private GrammarCallback callback;
  boolean condensationFlag = false;
  GrammarDataObject condensedGdo;
  private GrammarDataObject gdoForCondense;
  GrammarDataLinkedListObject gdoListHead;
  GrammarDataLinkedListObject gdoListParent;
  GrammarDataLinkedListObject gdoListRoot;
  GrammarTrie grammarHead;
  GrammarTrie grammarRoot;
  private GrammarTrie prevGrammarHead;
  private GrammarDataObject prevPeekedGdo;
  boolean reCondensed = false;
  
  Branch(GrammarTrie paramGrammarTrie, GrammarDataLinkedListObject paramGrammarDataLinkedListObject1, GrammarDataLinkedListObject paramGrammarDataLinkedListObject2, GrammarCallback paramGrammarCallback, int paramInt)
  {
    grammarRoot = paramGrammarTrie;
    grammarHead = paramGrammarTrie;
    gdoListRoot = paramGrammarDataLinkedListObject2;
    gdoListHead = paramGrammarDataLinkedListObject2;
    gdoListParent = paramGrammarDataLinkedListObject1;
    callback = paramGrammarCallback;
    branchIndex = paramInt;
    branchSize = 0;
    gdoListRoot.next = null;
  }
  
  private void doPeek(GrammarDataObject paramGrammarDataObject)
  {
    boolean bool = isTraversing();
    int i;
    int j;
    Object localObject2;
    Object localObject3;
    if (bool)
    {
      localObject1 = prevGrammarHead;
      if (localObject1 != null)
      {
        i = ((GrammarTrie)localObject1).children();
        j = 1;
        if (i > j)
        {
          localObject1 = callback;
          localObject2 = prevPeekedGdo;
          localObject3 = label;
          int k = branchSize;
          ((GrammarCallback)localObject1).add((GrammarDataObject)localObject2, (String)localObject3, k);
        }
      }
    }
    prevPeekedGdo = paramGrammarDataObject;
    Object localObject1 = grammarHead.getNext(paramGrammarDataObject);
    prevGrammarHead = ((GrammarTrie)localObject1);
    localObject1 = grammarHead.getNext(paramGrammarDataObject);
    grammarHead = ((GrammarTrie)localObject1);
    localObject1 = condensedGdo;
    if (localObject1 == null)
    {
      localObject1 = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObject;
      ((GrammarDataObject)localObject1).<init>();
      condensedGdo = ((GrammarDataObject)localObject1);
      localObject1 = condensedGdo;
      j = index;
      index = j;
    }
    localObject1 = values;
    if (localObject1 != null)
    {
      localObject1 = values;
      i = ((a)localObject1).size();
      if (i > 0)
      {
        localObject1 = condensedGdo;
        localObject2 = values;
        localObject3 = values;
        localObject2 = BranchRules.valueOverrideWithinCondensationAdd((a)localObject2, (a)localObject3);
        ((GrammarDataObject)localObject1).addValues((HashMap)localObject2);
      }
    }
    condensedGdo.children.add(paramGrammarDataObject);
  }
  
  private boolean isMainBranch()
  {
    int i = branchIndex;
    return i == 0;
  }
  
  private boolean isTraversing()
  {
    GrammarTrie localGrammarTrie1 = grammarHead;
    GrammarTrie localGrammarTrie2 = grammarRoot;
    return localGrammarTrie1 != localGrammarTrie2;
  }
  
  private boolean onPeek(GrammarDataObject paramGrammarDataObject)
  {
    Object localObject = condensedGdo;
    if (localObject != null)
    {
      localObject = ((GrammarDataObject)localObject).getLastChild();
    }
    else
    {
      bool1 = false;
      localObject = null;
    }
    if (localObject != null)
    {
      GrammarCallback localGrammarCallback = callback;
      GrammarDataObject localGrammarDataObject = condensedGdo;
      bool2 = localGrammarCallback.shouldCondense((GrammarDataObject)localObject, paramGrammarDataObject, localGrammarDataObject);
      if (!bool2) {
        return false;
      }
    }
    if (localObject != null) {
      BranchRules.valueCompareWithinCondensationAdd((GrammarDataObject)localObject, paramGrammarDataObject);
    }
    localObject = grammarHead.getNext(paramGrammarDataObject);
    boolean bool1 = leaf;
    boolean bool2 = true;
    if (bool1)
    {
      gdoForCondense = paramGrammarDataObject;
      condensationFlag = bool2;
      return false;
    }
    doPeek(paramGrammarDataObject);
    return bool2;
  }
  
  public boolean add(GrammarDataObject paramGrammarDataObject)
  {
    Object localObject = gdoListHead;
    GrammarDataLinkedListObject localGrammarDataLinkedListObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataLinkedListObject;
    localGrammarDataLinkedListObject.<init>(paramGrammarDataObject, null);
    next = localGrammarDataLinkedListObject;
    localObject = gdoListRoot.next;
    if (localObject == null)
    {
      localObject = gdoListRoot;
      localGrammarDataLinkedListObject = gdoListHead.next;
      next = localGrammarDataLinkedListObject;
    }
    boolean bool1 = isMainBranch();
    int i = 1;
    boolean bool2;
    if (bool1)
    {
      localObject = grammarRoot;
      bool1 = ((GrammarTrie)localObject).canPeek(paramGrammarDataObject);
      if (bool1)
      {
        localObject = callback;
        ((GrammarCallback)localObject).add(paramGrammarDataObject);
      }
      bool2 = true;
    }
    else
    {
      localObject = grammarHead;
      bool1 = ((GrammarTrie)localObject).canPeek(paramGrammarDataObject);
      if (!bool1) {
        break label157;
      }
      bool2 = onPeek(paramGrammarDataObject);
    }
    if (bool2)
    {
      paramGrammarDataObject = gdoListHead.next;
      gdoListHead = paramGrammarDataObject;
    }
    else
    {
      paramGrammarDataObject = gdoListHead;
      next = null;
    }
    return i;
    label157:
    localObject = condensedGdo;
    if (localObject != null)
    {
      localObject = infectedChildren;
      ((List)localObject).add(paramGrammarDataObject);
    }
    int j = branchSize + i;
    branchSize = j;
    gdoListHead.next = null;
    return false;
  }
  
  void annulCondense()
  {
    gdoForCondense = null;
    condensationFlag = false;
  }
  
  void doCondense()
  {
    Object localObject1 = gdoForCondense;
    doPeek((GrammarDataObject)localObject1);
    localObject1 = condensedGdo;
    String str = grammarHead.grammar_name;
    boolean bool1 = ((GrammarDataObject)localObject1).hasChild(str);
    boolean bool2 = true;
    if (bool1) {
      reCondensed = bool2;
    }
    localObject1 = grammarHead.grammar_context;
    boolean bool3;
    Object localObject3;
    if (localObject1 != null)
    {
      localObject1 = grammarHead.grammar_context;
      localObject2 = "";
      bool1 = ((String)localObject1).equals(localObject2);
      if (!bool1)
      {
        localObject1 = new java/util/HashSet;
        localObject2 = condensedGdo.values.keySet();
        ((HashSet)localObject1).<init>((Collection)localObject2);
        localObject1 = ((Set)localObject1).iterator();
        for (;;)
        {
          bool3 = ((Iterator)localObject1).hasNext();
          if (!bool3) {
            break;
          }
          localObject2 = (String)((Iterator)localObject1).next();
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          Object localObject4 = grammarHead.grammar_context;
          ((StringBuilder)localObject3).append((String)localObject4);
          ((StringBuilder)localObject3).append("_");
          localObject4 = "_";
          boolean bool4 = ((String)localObject2).contains((CharSequence)localObject4);
          if (bool4) {
            localObject4 = localObject2.split("_")[bool2];
          } else {
            localObject4 = localObject2;
          }
          ((StringBuilder)localObject3).append((String)localObject4);
          localObject3 = ((StringBuilder)localObject3).toString();
          localObject4 = condensedGdo.values.remove((String)localObject2);
          bool3 = ((String)localObject2).equals(localObject3);
          if (!bool3)
          {
            localObject2 = condensedGdo.values;
            bool3 = ((a)localObject2).containsKey((String)localObject3);
            if (bool3)
            {
              localObject2 = condensedGdo;
              BranchRules.valueConflictWithinCondensationContext((String)localObject3, (String)localObject4, (GrammarDataObject)localObject2);
              continue;
            }
          }
          localObject2 = condensedGdo;
          BranchRules.valueOverrideWithinCondensationContext((String)localObject3, (String)localObject4, (GrammarDataObject)localObject2);
        }
        localObject1 = condensedGdo;
        localObject2 = BranchRules.valueOverridePostCondensation(values);
        values = ((a)localObject2);
      }
    }
    localObject1 = condensedGdo.children.iterator();
    for (;;)
    {
      bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = (GrammarDataObject)((Iterator)localObject1).next();
      lock = bool2;
      localObject3 = condensedGdo;
      parent = ((GrammarDataObject)localObject3);
    }
    localObject1 = condensedGdo.infectedChildren.iterator();
    for (;;)
    {
      bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = (GrammarDataObject)((Iterator)localObject1).next();
      boolean bool5 = ((GrammarDataObject)localObject2).isNonDet();
      if (!bool5)
      {
        localObject3 = values;
        int i = ((a)localObject3).size();
        if (i == 0) {
          lock = bool2;
        }
      }
    }
    localObject1 = condensedGdo;
    Object localObject2 = grammarHead.grammar_name;
    ((GrammarDataObject)localObject1).setLabel((String)localObject2);
    localObject1 = condensedGdo;
    localObject2 = grammarHead.grammar_name;
    ((GrammarDataObject)localObject1).setLabelAux((String)localObject2);
    localObject1 = condensedGdo;
    mature = bool2;
    condensed = bool2;
    callback.condensation((GrammarDataObject)localObject1);
    annulCondense();
  }
  
  public int getBranchLength(int paramInt)
  {
    int i = getStartIndex();
    return paramInt - i;
  }
  
  public int getEndIndex()
  {
    Object localObject = gdoListRoot;
    int i = 0;
    GrammarDataObject localGrammarDataObject = null;
    while (localObject != null)
    {
      GrammarDataLinkedListObject localGrammarDataLinkedListObject = next;
      if (localGrammarDataLinkedListObject == null) {
        break;
      }
      localGrammarDataObject = next.grammarDataObject;
      localObject = next;
    }
    if (localGrammarDataObject != null)
    {
      localObject = children;
      if (localObject != null)
      {
        localObject = children;
        j = ((List)localObject).size();
        if (j > 0)
        {
          localObject = children;
          i = children.size() + -1;
          localObject = (GrammarDataObject)((List)localObject).get(i);
          i = index;
          j = str.length();
          return i + j;
        }
      }
      int j = index;
      i = str.length();
      return j + i;
    }
    return 0;
  }
  
  public int getStartIndex()
  {
    Object localObject = gdoListRoot.next;
    if (localObject != null)
    {
      localObject = gdoListRoot.next.grammarDataObject;
      if (localObject != null) {
        return gdoListRoot.next.grammarDataObject.index;
      }
    }
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.branch.Branch
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */