package com.twelfthmile.malana.compiler.parser.branch;

import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;

class BranchController$ParallelIntent
{
  GrammarDataObject gdo;
  Branch gdoContext;
  
  BranchController$ParallelIntent(Branch paramBranch, GrammarDataObject paramGrammarDataObject)
  {
    gdoContext = paramBranch;
    gdo = paramGrammarDataObject;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.branch.BranchController.ParallelIntent
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */