package com.twelfthmile.malana.compiler.parser.context;

import com.twelfthmile.e.b.c;
import com.twelfthmile.malana.compiler.parser.confidence.ConfidenceModel;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataLinkedListObject;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObjectList;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObjectPosition;
import com.twelfthmile.malana.compiler.types.GDOPos;
import com.twelfthmile.malana.compiler.types.Pair;
import com.twelfthmile.malana.compiler.types.SemanticSeed;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ContextController
{
  private GDOPos GDOPosition;
  private HashMap confMap;
  private List exclusionList;
  private List indexList;
  private GrammarDataObjectList master;
  private GrammarDataObjectList masterWoNonDet;
  private GrammarDataObjectList stack;
  private List tokenExclusionList;
  
  public ContextController(SemanticSeed paramSemanticSeed, GrammarDataLinkedListObject paramGrammarDataLinkedListObject)
  {
    Object localObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectList;
    ((GrammarDataObjectList)localObject).<init>();
    master = ((GrammarDataObjectList)localObject);
    localObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectList;
    ((GrammarDataObjectList)localObject).<init>();
    masterWoNonDet = ((GrammarDataObjectList)localObject);
    localObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectList;
    ((GrammarDataObjectList)localObject).<init>();
    stack = ((GrammarDataObjectList)localObject);
    localObject = paramSemanticSeed.getContextBreak();
    exclusionList = ((List)localObject);
    localObject = createTokenExclusionList();
    tokenExclusionList = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    indexList = ((List)localObject);
    localObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectPosition;
    ((GrammarDataObjectPosition)localObject).<init>(paramGrammarDataLinkedListObject);
    GDOPosition = ((GDOPos)localObject);
    paramSemanticSeed = paramSemanticSeed.getConfidenceMap();
    confMap = paramSemanticSeed;
  }
  
  private List createTokenExclusionList()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localArrayList.add("SEATNUMB");
    return localArrayList;
  }
  
  public void add(GrammarDataObject paramGrammarDataObject)
  {
    stack.add(paramGrammarDataObject);
    GrammarDataObjectList localGrammarDataObjectList = master;
    localGrammarDataObjectList.add(paramGrammarDataObject);
    boolean bool = paramGrammarDataObject.isNonDet();
    if (!bool)
    {
      localGrammarDataObjectList = masterWoNonDet;
      localGrammarDataObjectList.add(paramGrammarDataObject);
    }
  }
  
  public void end(int paramInt)
  {
    if (paramInt > 0)
    {
      List localList = indexList;
      localObject = Integer.valueOf(paramInt);
      localList.add(localObject);
    }
    Object localObject = new com/twelfthmile/malana/compiler/parser/gdo/GrammarDataObjectList;
    ((GrammarDataObjectList)localObject).<init>();
    stack = ((GrammarDataObjectList)localObject);
  }
  
  public List getGdoList()
  {
    return masterWoNonDet.getGdoList();
  }
  
  public List getIndexList()
  {
    return indexList;
  }
  
  public Pair isNotEllipsis(String paramString, int paramInt)
  {
    int i = paramInt + 3;
    int k = paramString.length();
    int m = -1;
    if (i < k)
    {
      localObject = paramString.substring(paramInt, i);
      if (localObject != null)
      {
        String str = "com";
        boolean bool = ((String)localObject).equalsIgnoreCase(str);
        if (bool)
        {
          paramString = new com/twelfthmile/malana/compiler/types/Pair;
          localBoolean = Boolean.FALSE;
          localObject = Integer.valueOf(m);
          paramString.<init>(localBoolean, localObject);
          return paramString;
        }
      }
    }
    int j = paramInt + -2;
    k = paramString.charAt(j);
    int n = 32;
    int i1 = 46;
    int i2 = 1;
    if (k != i1)
    {
      j = paramString.charAt(j);
      if (j != n) {
        j = -1;
      } else {
        j = 1;
      }
    }
    else
    {
      j = 0;
      localObject = null;
    }
    k = 2;
    if (j == 0) {
      i3 = 2;
    } else {
      i3 = 1;
    }
    int i4 = i3;
    int i3 = paramInt;
    for (;;)
    {
      int i5 = paramString.length();
      if (i3 >= i5) {
        break label421;
      }
      i5 = paramString.charAt(i3);
      if (i5 != i1) {
        break;
      }
      i4 += 1;
      i3 += 1;
    }
    if (i4 >= k)
    {
      paramInt = c.e(paramString.charAt(i3));
      if (paramInt == 0)
      {
        paramInt = c.a(paramString.charAt(i3));
        if (paramInt == 0)
        {
          i6 = paramString.charAt(i3);
          paramInt = 43;
          if (i6 != paramInt) {
            break label255;
          }
        }
      }
      i3 += -1;
      label255:
      paramString = new com/twelfthmile/malana/compiler/types/Pair;
      localBoolean = Boolean.FALSE;
      localObject = Integer.valueOf(i3);
      paramString.<init>(localBoolean, localObject);
      return paramString;
    }
    paramInt = paramString.charAt(paramInt);
    if (paramInt != i1)
    {
      paramString = new com/twelfthmile/malana/compiler/types/Pair;
      localBoolean = Boolean.TRUE;
      localObject = Integer.valueOf(m);
      paramString.<init>(localBoolean, localObject);
      return paramString;
    }
    int i6 = paramString.charAt(i3);
    if (i6 == n)
    {
      if (j == i2)
      {
        paramString = new com/twelfthmile/malana/compiler/types/Pair;
        localBoolean = Boolean.TRUE;
        localObject = Integer.valueOf(i3);
        paramString.<init>(localBoolean, localObject);
        return paramString;
      }
      paramString = new com/twelfthmile/malana/compiler/types/Pair;
      localBoolean = Boolean.FALSE;
      localObject = Integer.valueOf(i3);
      paramString.<init>(localBoolean, localObject);
      return paramString;
    }
    paramString = new com/twelfthmile/malana/compiler/types/Pair;
    Boolean localBoolean = Boolean.FALSE;
    Object localObject = Integer.valueOf(i3 - i2);
    paramString.<init>(localBoolean, localObject);
    return paramString;
    label421:
    paramString = new com/twelfthmile/malana/compiler/types/Pair;
    localBoolean = Boolean.TRUE;
    localObject = Integer.valueOf(m);
    paramString.<init>(localBoolean, localObject);
    return paramString;
  }
  
  public boolean isValid(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2)
  {
    GrammarDataObjectList localGrammarDataObjectList = stack;
    boolean bool = localGrammarDataObjectList.containsGdo(paramGrammarDataObject1);
    if (bool)
    {
      paramGrammarDataObject1 = stack;
      bool = paramGrammarDataObject1.containsGdo(paramGrammarDataObject2);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public boolean shouldCondense(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2, GrammarDataObject paramGrammarDataObject3)
  {
    boolean bool = isValid(paramGrammarDataObject1, paramGrammarDataObject2);
    if (bool)
    {
      GDOPos localGDOPos = GDOPosition;
      HashMap localHashMap = confMap;
      int i = ConfidenceModel.checkConfidence(paramGrammarDataObject1, paramGrammarDataObject2, paramGrammarDataObject3, localGDOPos, localHashMap);
      int j = 50;
      if (i > j) {
        return true;
      }
    }
    return false;
  }
  
  public boolean shouldPop(String paramString)
  {
    String str = stack.getLastToken();
    if (str != null)
    {
      List localList = tokenExclusionList;
      boolean bool = localList.contains(str);
      if (bool) {
        return false;
      }
    }
    return shouldPopH(paramString);
  }
  
  public boolean shouldPopH(String paramString)
  {
    int i = paramString.length();
    int j = 1;
    i -= j;
    while (i >= 0)
    {
      int k = paramString.charAt(i);
      int m = 32;
      if (k != m)
      {
        k = paramString.charAt(i);
        m = 46;
        if (k != m)
        {
          i += -1;
          continue;
        }
      }
      List localList = exclusionList;
      i += j;
      m = paramString.length();
      paramString = paramString.substring(i, m).toLowerCase();
      boolean bool = localList.contains(paramString);
      if (!bool) {
        return j;
      }
      return false;
    }
    return false;
  }
  
  public boolean shouldPopWord(String paramString)
  {
    List localList = exclusionList;
    paramString = paramString.toLowerCase();
    boolean bool = localList.contains(paramString);
    return !bool;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.context.ContextController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */