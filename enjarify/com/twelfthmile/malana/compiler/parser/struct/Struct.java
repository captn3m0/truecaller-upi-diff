package com.twelfthmile.malana.compiler.parser.struct;

import com.twelfthmile.b.a.a;
import com.twelfthmile.malana.compiler.datastructure.GrammarTrie;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.types.ValueMap;
import java.util.Map;

public class Struct
{
  private int indexPrev;
  private boolean mature;
  private a structCandidateMap;
  private GrammarTrie structHead;
  private a structMap;
  
  public Struct(GrammarTrie paramGrammarTrie)
  {
    structHead = paramGrammarTrie;
    paramGrammarTrie = new com/twelfthmile/malana/compiler/types/ValueMap;
    paramGrammarTrie.<init>();
    structMap = paramGrammarTrie;
    paramGrammarTrie = new com/twelfthmile/malana/compiler/types/ValueMap;
    paramGrammarTrie.<init>();
    structCandidateMap = paramGrammarTrie;
  }
  
  private void transfer()
  {
    mature = true;
    Object localObject = structMap;
    Map localMap = structCandidateMap.getAll();
    ((a)localObject).putAll(localMap);
    localObject = new com/twelfthmile/malana/compiler/types/ValueMap;
    ((ValueMap)localObject).<init>();
    structCandidateMap = ((a)localObject);
  }
  
  boolean canPeekWithoutOrigin(String paramString)
  {
    return structHead.canPeekWithoutOrigin(paramString);
  }
  
  public int getIndexPrev()
  {
    return indexPrev;
  }
  
  a getStructMap()
  {
    return structMap;
  }
  
  boolean hasValue()
  {
    return structHead.value;
  }
  
  void insert(String paramString)
  {
    a locala = structMap;
    String str = structHead.grammar_name;
    boolean bool1 = locala.containsKey(str);
    if (!bool1)
    {
      locala = structCandidateMap;
      str = structHead.grammar_name;
      bool1 = locala.containsKey(str);
      if (!bool1)
      {
        locala = structCandidateMap;
        str = structHead.grammar_name;
        locala.put(str, paramString);
        paramString = structHead;
        boolean bool2 = leaf;
        if (bool2) {
          transfer();
        }
      }
    }
  }
  
  boolean isMature()
  {
    return mature;
  }
  
  public void notMature()
  {
    mature = false;
  }
  
  boolean peek(GrammarDataObject paramGrammarDataObject)
  {
    GrammarTrie localGrammarTrie = structHead;
    String str = label;
    boolean bool = localGrammarTrie.canPeek(str);
    if (bool)
    {
      int i = index;
      indexPrev = i;
      localGrammarTrie = structHead;
      paramGrammarDataObject = label;
      paramGrammarDataObject = localGrammarTrie.getNext(paramGrammarDataObject);
      structHead = paramGrammarDataObject;
      return true;
    }
    return false;
  }
  
  boolean peekWithoutOrigin(GrammarDataObject paramGrammarDataObject)
  {
    GrammarTrie localGrammarTrie = structHead;
    String str = label;
    boolean bool = localGrammarTrie.canPeekWithoutOrigin(str);
    if (bool)
    {
      int i = index;
      indexPrev = i;
      localGrammarTrie = structHead;
      paramGrammarDataObject = label;
      paramGrammarDataObject = localGrammarTrie.getNextWithoutOrigin(paramGrammarDataObject);
      structHead = paramGrammarDataObject;
      paramGrammarDataObject = structCandidateMap;
      int j = paramGrammarDataObject.size();
      if (j > 0) {
        transfer();
      }
      return true;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.struct.Struct
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */