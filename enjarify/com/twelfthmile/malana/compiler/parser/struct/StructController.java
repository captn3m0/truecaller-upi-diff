package com.twelfthmile.malana.compiler.parser.struct;

import com.twelfthmile.b.a.a;
import com.twelfthmile.malana.compiler.datastructure.GrammarTrie;
import com.twelfthmile.malana.compiler.lex.Tokenizer;
import com.twelfthmile.malana.compiler.parser.branch.BranchRules;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataLinkedListObject;
import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import com.twelfthmile.malana.compiler.types.Finalise;
import com.twelfthmile.malana.compiler.types.Pair;
import com.twelfthmile.malana.compiler.types.ParserListener;
import com.twelfthmile.malana.compiler.types.Req;
import com.twelfthmile.malana.compiler.types.ValueMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class StructController
{
  private boolean mature;
  private ParserListener parserListener;
  private ArrayList structList;
  private GrammarTrie structRoot;
  
  public StructController(GrammarTrie paramGrammarTrie, ParserListener paramParserListener)
  {
    structRoot = paramGrammarTrie;
    paramGrammarTrie = new java/util/ArrayList;
    paramGrammarTrie.<init>();
    structList = paramGrammarTrie;
    parserListener = paramParserListener;
  }
  
  private void checkMaturity()
  {
    boolean bool1 = mature;
    if (bool1) {
      return;
    }
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    int i = 0;
    Object localObject2 = null;
    int j = 0;
    ArrayList localArrayList = null;
    for (;;)
    {
      Object localObject3 = structList;
      k = ((ArrayList)localObject3).size();
      if (j >= k) {
        break;
      }
      localObject3 = (Struct)structList.get(j);
      boolean bool3 = ((Struct)localObject3).isMature();
      if (bool3) {
        ((ArrayList)localObject1).add(localObject3);
      }
      j += 1;
    }
    j = ((ArrayList)localObject1).size();
    int k = 1;
    if (j == k)
    {
      mature = k;
      localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      structList = localArrayList;
      localArrayList = structList;
      localObject1 = ((ArrayList)localObject1).get(0);
      localArrayList.add(localObject1);
      return;
    }
    i = ((ArrayList)localObject1).size();
    if (i > k)
    {
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      structList = ((ArrayList)localObject2);
      localObject2 = structList;
      ((ArrayList)localObject2).addAll((Collection)localObject1);
      localObject1 = ((ArrayList)localObject1).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = (Struct)((Iterator)localObject1).next();
        ((Struct)localObject2).notMature();
      }
    }
  }
  
  private boolean isOutOfContext(Struct paramStruct, int paramInt)
  {
    Iterator localIterator = parserListener.contextIndexList().iterator();
    int i;
    int j;
    do
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Integer localInteger = (Integer)localIterator.next();
      i = localInteger.intValue();
      j = paramStruct.getIndexPrev();
    } while (j >= i);
    return paramInt > i;
    return false;
  }
  
  private boolean sanitizeAndAddToNodeList(ArrayList paramArrayList, String paramString)
  {
    Object localObject = "[-/@#$%^&_+=()]+";
    boolean bool = paramString.matches((String)localObject);
    if (!bool)
    {
      int i = paramString.length();
      if (i > 0)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("\"#");
        ((StringBuilder)localObject).append(paramString);
        ((StringBuilder)localObject).append("\"");
        paramString = ((StringBuilder)localObject).toString();
        paramArrayList.add(paramString);
        return true;
      }
    }
    return false;
  }
  
  private boolean shouldUnload(String paramString, GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2)
  {
    int i = index;
    int j = str.length();
    i += j;
    j = 0;
    int k = i;
    int m = 0;
    int n = 0;
    int i3;
    for (;;)
    {
      int i1 = paramString.length();
      i3 = 1;
      if (k >= i1) {
        break;
      }
      i1 = paramString.charAt(k);
      boolean bool2 = Tokenizer.skipCharacter(paramString, k, i1);
      if (!bool2) {
        break;
      }
      int i2 = paramString.charAt(k);
      int i4 = 46;
      if (i2 == i4)
      {
        Object localObject = parserListener;
        String str = paramString.substring(0, k);
        int i5 = -1;
        int i6 = 2;
        localObject = (Boolean)((ParserListener)localObject).shouldEnd(str, i5, i6).getA();
        boolean bool3 = ((Boolean)localObject).booleanValue();
        if (bool3) {
          n = 1;
        }
      }
      m += 1;
      k += 1;
    }
    int i7 = index;
    i += m;
    if (i7 > i)
    {
      i7 = 1;
    }
    else
    {
      i7 = 0;
      paramString = null;
    }
    boolean bool4;
    if ((i7 == 0) && (n != 0))
    {
      boolean bool1 = paramGrammarDataObject2.startWithCaps();
      if (bool1)
      {
        paramGrammarDataObject2 = str;
        boolean bool5 = tokenNotExtension(paramGrammarDataObject2);
        if (bool5)
        {
          if (m <= i3)
          {
            bool4 = paramGrammarDataObject1.allCaps();
            if (bool4)
            {
              bool4 = false;
              paramString = null;
              break label266;
            }
          }
          bool4 = true;
        }
      }
    }
    label266:
    return bool4;
  }
  
  private boolean tokenNotExtension(String paramString)
  {
    boolean bool1 = true;
    if (paramString == null) {
      return bool1;
    }
    paramString = paramString.toLowerCase();
    String str = "com";
    boolean bool2 = paramString.equals(str);
    if (!bool2)
    {
      str = "in";
      boolean bool3 = paramString.equals(str);
      if (!bool3) {
        return bool1;
      }
    }
    return false;
  }
  
  private boolean tokenNotValidforBlockEnd(GrammarDataObject paramGrammarDataObject)
  {
    String str1 = label;
    String str2 = "SAL";
    boolean bool1 = str1.equals(str2);
    if (!bool1)
    {
      str1 = label;
      str2 = "NAME";
      bool1 = str1.equals(str2);
      if (!bool1)
      {
        str1 = label;
        str2 = "DET";
        bool1 = str1.equals(str2);
        if (!bool1)
        {
          str1 = label;
          str2 = "ART";
          bool1 = str1.equals(str2);
          if (!bool1)
          {
            str1 = label;
            str2 = "USE";
            bool1 = str1.equals(str2);
            if (!bool1)
            {
              str1 = label;
              str2 = "AUX";
              bool1 = str1.equals(str2);
              if (!bool1)
              {
                str1 = label;
                str2 = "PREP";
                bool1 = str1.equals(str2);
                if (!bool1)
                {
                  str1 = label;
                  str2 = "PREPL";
                  bool1 = str1.equals(str2);
                  if (!bool1)
                  {
                    str1 = label;
                    str2 = "PREPV";
                    bool1 = str1.equals(str2);
                    if (!bool1)
                    {
                      str1 = label;
                      str2 = "ORDERID";
                      bool1 = str1.equals(str2);
                      if (!bool1)
                      {
                        str1 = label;
                        str2 = "TRANSID";
                        bool1 = str1.equals(str2);
                        if (!bool1)
                        {
                          str1 = label;
                          str2 = "PNR";
                          bool1 = str1.equals(str2);
                          if (!bool1)
                          {
                            str1 = label;
                            str2 = "MIN";
                            bool1 = str1.equals(str2);
                            if (!bool1)
                            {
                              str1 = label;
                              str2 = "AMNT";
                              bool1 = str1.equals(str2);
                              if (!bool1)
                              {
                                str1 = label;
                                str2 = "AVBL";
                                bool1 = str1.equals(str2);
                                if (!bool1)
                                {
                                  str1 = label;
                                  str2 = "CONJ";
                                  bool1 = str1.equals(str2);
                                  if (!bool1)
                                  {
                                    str1 = label;
                                    str2 = "DECLINE";
                                    bool1 = str1.equals(str2);
                                    if (!bool1)
                                    {
                                      paramGrammarDataObject = label;
                                      str1 = "OFFER";
                                      boolean bool2 = paramGrammarDataObject.equals(str1);
                                      if (!bool2) {
                                        return false;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return true;
  }
  
  public boolean canPeek(String paramString)
  {
    Iterator localIterator = structList.iterator();
    boolean bool;
    do
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Struct localStruct = (Struct)localIterator.next();
      bool = localStruct.canPeekWithoutOrigin(paramString);
    } while (!bool);
    return true;
    return structRoot.canPeek(paramString);
  }
  
  public void finalise(Req paramReq, ArrayList paramArrayList, Finalise paramFinalise, GrammarDataLinkedListObject paramGrammarDataLinkedListObject)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    paramGrammarDataLinkedListObject = next;
    int i = 0;
    int j = 0;
    Object localObject2 = null;
    int n = 0;
    Object localObject3 = null;
    int i3;
    int i14;
    label726:
    label1039:
    int m;
    int i2;
    for (;;)
    {
      int i1 = -1;
      i3 = 1;
      if (paramGrammarDataLinkedListObject == null) {
        break;
      }
      Object localObject4 = grammarDataObject;
      if (localObject4 != null)
      {
        localObject4 = grammarDataObject;
        boolean bool3 = lock;
        if (!bool3)
        {
          int i4 = ((ArrayList)localObject1).size();
          Object localObject5;
          int i12;
          if (i4 > 0)
          {
            localObject4 = message;
            int i10 = ((ArrayList)localObject1).size() - i3;
            localObject5 = (GrammarDataObject)((ArrayList)localObject1).get(i10);
            Object localObject6 = grammarDataObject;
            int i5 = shouldUnload((String)localObject4, (GrammarDataObject)localObject5, (GrammarDataObject)localObject6);
            if (i5 != 0)
            {
              localObject3 = new com/twelfthmile/malana/compiler/types/Pair;
              localObject4 = Integer.valueOf(i1);
              localObject5 = Integer.valueOf(i1);
              ((Pair)localObject3).<init>(localObject4, localObject5);
              i5 = 0;
              localObject4 = null;
              int i6;
              for (;;)
              {
                i10 = ((ArrayList)localObject1).size();
                if (i5 >= i10) {
                  break;
                }
                if (i5 == 0)
                {
                  i11 = getindex;
                  localObject5 = Integer.valueOf(i11);
                  ((Pair)localObject3).setA(localObject5);
                }
                int i11 = ((ArrayList)localObject1).size() - i3;
                if (i5 == i11)
                {
                  i12 = getindex;
                  localObject6 = getstr;
                  i13 = ((String)localObject6).length();
                  i12 += i13;
                  localObject5 = Integer.valueOf(i12);
                  ((Pair)localObject3).setB(localObject5);
                }
                localObject5 = (Integer)((Pair)localObject3).getB();
                i12 = ((Integer)localObject5).intValue();
                localObject6 = message;
                int i13 = ((String)localObject6).length();
                if (i12 >= i13)
                {
                  i12 = message.length();
                  localObject5 = Integer.valueOf(i12);
                  ((Pair)localObject3).setB(localObject5);
                }
                i5 += 1;
              }
              localObject1 = (Integer)((Pair)localObject3).getA();
              i14 = ((Integer)localObject1).intValue();
              if (i14 >= 0)
              {
                localObject1 = (Integer)((Pair)localObject3).getB();
                i14 = ((Integer)localObject1).intValue();
                if (i14 > 0)
                {
                  localObject1 = message;
                  i6 = ((Integer)((Pair)localObject3).getA()).intValue();
                  i12 = ((Integer)((Pair)localObject3).getB()).intValue();
                  localObject1 = ((String)localObject1).substring(i6, i12);
                  localObject4 = "\"";
                  localObject5 = "\\\"";
                  localObject1 = ((String)localObject1).replace((CharSequence)localObject4, (CharSequence)localObject5);
                  bool4 = sanitizeAndAddToNodeList(paramArrayList, (String)localObject1);
                  if (bool4)
                  {
                    localObject3 = (Integer)((Pair)localObject3).getA();
                    n = ((Integer)localObject3).intValue();
                    insert(n, (String)localObject1);
                  }
                }
              }
              localObject1 = new java/util/ArrayList;
              ((ArrayList)localObject1).<init>();
              n = 0;
              localObject3 = null;
            }
          }
          localObject4 = grammarDataObject;
          boolean bool4 = ((GrammarDataObject)localObject4).isNonDet();
          if (bool4)
          {
            if (j == 0)
            {
              n = 0;
              localObject3 = null;
            }
            localObject2 = grammarDataObject;
            ((ArrayList)localObject1).add(localObject2);
            j = 1;
          }
          else
          {
            int i7 = ((ArrayList)localObject1).size();
            if (i7 > 0)
            {
              j = 2;
              if (n < j)
              {
                localObject4 = grammarDataObject;
                boolean bool5 = mature;
                if (!bool5)
                {
                  localObject4 = grammarDataObject.label;
                  bool5 = canPeek((String)localObject4);
                  if (!bool5)
                  {
                    localObject2 = grammarDataObject;
                    ((ArrayList)localObject1).add(localObject2);
                    j = n + 1;
                    n = j;
                    break label1514;
                  }
                }
              }
              if (n < j)
              {
                j = ((ArrayList)localObject1).size() - i3;
                localObject2 = (GrammarDataObject)((ArrayList)localObject1).get(j);
                bool1 = tokenNotValidforBlockEnd((GrammarDataObject)localObject2);
                if (!bool1)
                {
                  bool1 = false;
                  localObject2 = null;
                  break label726;
                }
              }
              boolean bool1 = true;
              n = ((ArrayList)localObject1).size() - i3;
              if (bool1)
              {
                int k = ((ArrayList)localObject1).size() - i3;
                n = k;
                while (n >= 0)
                {
                  localObject2 = (GrammarDataObject)((ArrayList)localObject1).get(n);
                  boolean bool2 = ((GrammarDataObject)localObject2).isNonDet();
                  if (bool2) {
                    break;
                  }
                  n += -1;
                }
              }
              localObject2 = new com/twelfthmile/malana/compiler/types/Pair;
              localObject4 = Integer.valueOf(i1);
              localObject7 = Integer.valueOf(i1);
              ((Pair)localObject2).<init>(localObject4, localObject7);
              i1 = 0;
              localObject7 = null;
              int i9;
              while (i1 <= n)
              {
                if (i1 == 0)
                {
                  int i8 = getindex;
                  localObject4 = Integer.valueOf(i8);
                  ((Pair)localObject2).setA(localObject4);
                }
                if (i1 == n)
                {
                  if (i1 > 0)
                  {
                    localObject4 = "(";
                    localObject5 = getstr;
                    boolean bool6 = ((String)localObject4).equals(localObject5);
                    if (bool6)
                    {
                      i9 = i1 + -1;
                      localObject5 = (GrammarDataObject)((ArrayList)localObject1).get(i9);
                      i12 = index;
                      i9 = getstr.length();
                      i12 += i9;
                      localObject4 = Integer.valueOf(i12);
                      ((Pair)localObject2).setB(localObject4);
                      break label1039;
                    }
                  }
                  i9 = getindex;
                  localObject5 = getstr;
                  i12 = ((String)localObject5).length();
                  i9 += i12;
                  localObject4 = Integer.valueOf(i9);
                  ((Pair)localObject2).setB(localObject4);
                }
                localObject4 = (Integer)((Pair)localObject2).getB();
                i9 = ((Integer)localObject4).intValue();
                localObject5 = message;
                i12 = ((String)localObject5).length();
                if (i9 >= i12)
                {
                  i9 = message.length();
                  localObject4 = Integer.valueOf(i9);
                  ((Pair)localObject2).setB(localObject4);
                }
                i1 += 1;
              }
              localObject7 = (Integer)((Pair)localObject2).getA();
              i1 = ((Integer)localObject7).intValue();
              if (i1 >= 0)
              {
                localObject7 = (Integer)((Pair)localObject2).getB();
                i1 = ((Integer)localObject7).intValue();
                if (i1 > 0)
                {
                  localObject7 = message;
                  i9 = ((Integer)((Pair)localObject2).getA()).intValue();
                  i12 = ((Integer)((Pair)localObject2).getB()).intValue();
                  localObject7 = ((String)localObject7).substring(i9, i12);
                  localObject4 = "\"";
                  localObject5 = "\\\"";
                  localObject7 = ((String)localObject7).replace((CharSequence)localObject4, (CharSequence)localObject5);
                  boolean bool7 = sanitizeAndAddToNodeList(paramArrayList, (String)localObject7);
                  if (bool7)
                  {
                    localObject2 = (Integer)((Pair)localObject2).getA();
                    m = ((Integer)localObject2).intValue();
                    insert(m, (String)localObject7);
                  }
                }
              }
              for (;;)
              {
                n += i3;
                m = ((ArrayList)localObject1).size();
                if (n >= m) {
                  break;
                }
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>("\"");
                localObject7 = ((GrammarDataObject)((ArrayList)localObject1).get(n)).printLabel();
                ((StringBuilder)localObject2).append((String)localObject7);
                localObject7 = "\"";
                ((StringBuilder)localObject2).append((String)localObject7);
                localObject2 = ((StringBuilder)localObject2).toString();
                paramArrayList.add(localObject2);
              }
              localObject1 = new java/util/ArrayList;
              ((ArrayList)localObject1).<init>();
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>("\"");
              localObject3 = grammarDataObject.printLabel();
              ((StringBuilder)localObject2).append((String)localObject3);
              ((StringBuilder)localObject2).append("\"");
              localObject2 = ((StringBuilder)localObject2).toString();
              paramArrayList.add(localObject2);
              n = 0;
              localObject3 = null;
            }
            else
            {
              localObject7 = grammarDataObject;
              i2 = ((GrammarDataObject)localObject7).hasNonMatureValues();
              if (i2 != 0)
              {
                if (m == 0)
                {
                  n = 0;
                  localObject3 = null;
                }
                localObject2 = grammarDataObject;
                ((ArrayList)localObject1).add(localObject2);
              }
              else
              {
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>("\"");
                localObject7 = grammarDataObject.printLabel();
                ((StringBuilder)localObject2).append((String)localObject7);
                localObject7 = "\"";
                ((StringBuilder)localObject2).append((String)localObject7);
                localObject2 = ((StringBuilder)localObject2).toString();
                paramArrayList.add(localObject2);
              }
            }
            label1514:
            localObject2 = grammarDataObject;
            peek((GrammarDataObject)localObject2);
            localObject2 = paramFinalise.getValueMap();
            Object localObject7 = grammarDataObject.values;
            BranchRules.addValues((a)localObject2, (a)localObject7);
            m = 0;
            localObject2 = null;
          }
        }
      }
      paramGrammarDataLinkedListObject = next;
    }
    int i15 = ((ArrayList)localObject1).size();
    if (i15 > 0)
    {
      paramFinalise = new com/twelfthmile/malana/compiler/types/Pair;
      paramGrammarDataLinkedListObject = Integer.valueOf(i2);
      localObject2 = Integer.valueOf(i2);
      paramFinalise.<init>(paramGrammarDataLinkedListObject, localObject2);
      for (;;)
      {
        i16 = ((ArrayList)localObject1).size();
        if (i >= i16) {
          break;
        }
        if (i == 0)
        {
          i16 = getindex;
          paramGrammarDataLinkedListObject = Integer.valueOf(i16);
          paramFinalise.setA(paramGrammarDataLinkedListObject);
        }
        i16 = ((ArrayList)localObject1).size() - i3;
        if (i == i16)
        {
          i16 = getindex;
          localObject2 = getstr;
          m = ((String)localObject2).length();
          i16 += m;
          paramGrammarDataLinkedListObject = Integer.valueOf(i16);
          paramFinalise.setB(paramGrammarDataLinkedListObject);
        }
        paramGrammarDataLinkedListObject = (Integer)paramFinalise.getB();
        i16 = paramGrammarDataLinkedListObject.intValue();
        localObject2 = message;
        m = ((String)localObject2).length();
        if (i16 >= m)
        {
          i16 = message.length();
          paramGrammarDataLinkedListObject = Integer.valueOf(i16);
          paramFinalise.setB(paramGrammarDataLinkedListObject);
        }
        i += 1;
      }
      paramGrammarDataLinkedListObject = (Integer)paramFinalise.getA();
      int i16 = paramGrammarDataLinkedListObject.intValue();
      if (i16 >= 0)
      {
        paramGrammarDataLinkedListObject = (Integer)paramFinalise.getB();
        i16 = paramGrammarDataLinkedListObject.intValue();
        if (i16 > 0)
        {
          paramReq = message;
          i16 = ((Integer)paramFinalise.getA()).intValue();
          i14 = ((Integer)paramFinalise.getB()).intValue();
          paramReq = paramReq.substring(i16, i14);
          paramGrammarDataLinkedListObject = "\"";
          localObject1 = "\\\"";
          paramReq = paramReq.replace(paramGrammarDataLinkedListObject, (CharSequence)localObject1);
          boolean bool8 = sanitizeAndAddToNodeList(paramArrayList, paramReq);
          if (bool8)
          {
            paramArrayList = (Integer)paramFinalise.getA();
            int i17 = paramArrayList.intValue();
            insert(i17, paramReq);
          }
        }
      }
    }
  }
  
  public a getValues()
  {
    boolean bool = mature;
    if (bool)
    {
      localObject = structList;
      int i = ((ArrayList)localObject).size();
      int j = 1;
      if (i == j) {
        return ((Struct)structList.get(0)).getStructMap();
      }
    }
    Object localObject = new com/twelfthmile/malana/compiler/types/ValueMap;
    ((ValueMap)localObject).<init>();
    return (a)localObject;
  }
  
  public void insert(int paramInt, String paramString)
  {
    Iterator localIterator = structList.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Struct localStruct = (Struct)localIterator.next();
      boolean bool2 = isOutOfContext(localStruct, paramInt);
      if (!bool2)
      {
        bool2 = localStruct.hasValue();
        if (bool2) {
          localStruct.insert(paramString);
        }
      }
    }
    checkMaturity();
  }
  
  public boolean peek(GrammarDataObject paramGrammarDataObject)
  {
    checkMaturity();
    Object localObject = structList.iterator();
    GrammarTrie localGrammarTrie = null;
    int i = 0;
    String str = null;
    boolean bool2;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      bool2 = true;
      if (!bool1) {
        break;
      }
      Struct localStruct = (Struct)((Iterator)localObject).next();
      bool1 = localStruct.peekWithoutOrigin(paramGrammarDataObject);
      if (bool1) {
        i = 1;
      }
    }
    if (i != 0) {
      return bool2;
    }
    boolean bool3 = mature;
    if (!bool3)
    {
      localObject = structRoot;
      str = label;
      bool3 = ((GrammarTrie)localObject).canPeek(str);
      if (bool3)
      {
        localObject = new com/twelfthmile/malana/compiler/parser/struct/Struct;
        localGrammarTrie = structRoot;
        ((Struct)localObject).<init>(localGrammarTrie);
        ((Struct)localObject).peek(paramGrammarDataObject);
        structList.add(localObject);
        return bool2;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.parser.struct.StructController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */