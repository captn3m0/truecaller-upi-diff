package com.twelfthmile.malana.compiler;

import com.twelfthmile.malana.compiler.datastructure.AddressTrie;
import com.twelfthmile.malana.compiler.datastructure.GrammarTrie;
import com.twelfthmile.malana.compiler.datastructure.RootTrie;
import com.twelfthmile.malana.compiler.lex.Tokenizer;
import com.twelfthmile.malana.compiler.parser.Parser;
import com.twelfthmile.malana.compiler.parser.score.Score;
import com.twelfthmile.malana.compiler.types.MalanaSeed;
import com.twelfthmile.malana.compiler.types.Request;
import com.twelfthmile.malana.compiler.types.Response;
import com.twelfthmile.malana.compiler.types.SemanticSeed;
import com.twelfthmile.malana.compiler.util.L;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class Compiler
{
  private String addrJson;
  private Seeder seeder;
  
  public Compiler(MalanaSeed paramMalanaSeed)
  {
    try
    {
      Object localObject1 = new com/twelfthmile/malana/compiler/Seeder;
      ((Seeder)localObject1).<init>();
      seeder = ((Seeder)localObject1);
      localObject1 = seeder;
      Object localObject2 = new org/json/JSONObject;
      Object localObject3 = paramMalanaSeed.getMalanaSyntax();
      ((JSONObject)localObject2).<init>((String)localObject3);
      ((Seeder)localObject1).setSeedJsonObj((JSONObject)localObject2);
      localObject1 = seeder;
      ((Seeder)localObject1).tokenSeeding();
      localObject1 = seeder;
      ((Seeder)localObject1).classifierSeeding();
      localObject1 = seeder;
      int i = ((Seeder)localObject1).grammarSeeding();
      localObject2 = seeder;
      localObject3 = new com/twelfthmile/malana/compiler/parser/score/Score;
      ((Score)localObject3).<init>(i);
      score = ((Score)localObject3);
      localObject1 = seeder;
      localObject2 = new org/json/JSONObject;
      localObject3 = paramMalanaSeed.getMalanaOffers();
      ((JSONObject)localObject2).<init>((String)localObject3);
      ((Seeder)localObject1).seedOffers((JSONObject)localObject2);
      localObject1 = seeder;
      localObject2 = new org/json/JSONArray;
      localObject3 = paramMalanaSeed.getMalanaLocation();
      ((JSONArray)localObject2).<init>((String)localObject3);
      ((Seeder)localObject1).seedLocation((JSONArray)localObject2);
      localObject1 = seeder;
      localObject2 = new org/json/JSONObject;
      localObject3 = paramMalanaSeed.getMalanaAirport();
      ((JSONObject)localObject2).<init>((String)localObject3);
      ((Seeder)localObject1).seedAirport((JSONObject)localObject2);
      localObject1 = seeder;
      localObject2 = new org/json/JSONObject;
      localObject3 = paramMalanaSeed.getMalanaBank();
      ((JSONObject)localObject2).<init>((String)localObject3);
      ((Seeder)localObject1).seedBank((JSONObject)localObject2);
      localObject1 = paramMalanaSeed.getMalanaAddr();
      addrJson = ((String)localObject1);
      localObject1 = seeder;
      localObject2 = new org/json/JSONObject;
      localObject3 = addrJson;
      ((JSONObject)localObject2).<init>((String)localObject3);
      ((Seeder)localObject1).seedAddr((JSONObject)localObject2);
      localObject1 = seeder;
      localObject2 = new org/json/JSONObject;
      paramMalanaSeed = paramMalanaSeed.getMalanaSemantic();
      ((JSONObject)localObject2).<init>(paramMalanaSeed);
      ((Seeder)localObject1).semanticSeeding((JSONObject)localObject2);
      paramMalanaSeed = seeder;
      paramMalanaSeed.seedAllGrammars();
      return;
    }
    catch (Exception localException)
    {
      L.error(localException;
    }
  }
  
  private RootTrie onDemandSeeding(ArrayList paramArrayList)
  {
    RootTrie localRootTrie = new com/twelfthmile/malana/compiler/datastructure/RootTrie;
    localRootTrie.<init>();
    Object localObject1 = new com/twelfthmile/malana/compiler/datastructure/GrammarTrie;
    ((GrammarTrie)localObject1).<init>();
    grammar = ((GrammarTrie)localObject1);
    localObject1 = new com/twelfthmile/malana/compiler/datastructure/GrammarTrie;
    ((GrammarTrie)localObject1).<init>();
    struct = ((GrammarTrie)localObject1);
    paramArrayList = paramArrayList.iterator();
    for (;;)
    {
      boolean bool1 = paramArrayList.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (String)paramArrayList.next();
      Object localObject2 = (RootTrie)seeder.getGrammarRootMap().get(localObject1);
      if (localObject2 != null)
      {
        GrammarTrie localGrammarTrie1 = grammar;
        if (localGrammarTrie1 != null)
        {
          localGrammarTrie1 = grammar;
          GrammarTrie localGrammarTrie2 = grammar;
          localGrammarTrie1.putNext((String)localObject1, localGrammarTrie2);
          localGrammarTrie1 = grammar;
          boolean bool2 = true;
          origin = bool2;
          localGrammarTrie1 = struct;
          localObject2 = struct;
          localGrammarTrie1.putNext((String)localObject1, (GrammarTrie)localObject2);
          localObject1 = struct;
          origin = bool2;
        }
      }
    }
    return localRootTrie;
  }
  
  public Seeder getSeedData()
  {
    return seeder;
  }
  
  public String getSemantics(Request paramRequest, ArrayList paramArrayList)
  {
    Object localObject1 = address.toUpperCase();
    address = ((String)localObject1);
    if (paramArrayList != null)
    {
      paramArrayList = onDemandSeeding(paramArrayList);
      if (paramArrayList != null)
      {
        localObject1 = new com/twelfthmile/malana/compiler/parser/score/Score;
        Object localObject2 = seeder.score.auxMap;
        Object localObject3 = seeder.score.grammarList;
        int i = seeder.score.auxGrammarBaseSize;
        ((Score)localObject1).<init>((HashMap)localObject2, (ArrayList)localObject3, i);
        localObject2 = new com/twelfthmile/malana/compiler/parser/Parser;
        localObject3 = grammar;
        paramArrayList = struct;
        SemanticSeed localSemanticSeed = seeder.getSemanticSeed();
        ((Parser)localObject2).<init>((GrammarTrie)localObject3, paramArrayList, (Score)localObject1, localSemanticSeed);
        parser = ((Parser)localObject2);
        return Tokenizer.getSemantics(paramRequest);
      }
    }
    return null;
  }
  
  public String getTokens(Request paramRequest, ArrayList paramArrayList, List paramList)
  {
    Object localObject1 = address.toUpperCase();
    address = ((String)localObject1);
    if (paramArrayList != null)
    {
      paramArrayList = onDemandSeeding(paramArrayList);
      if (paramArrayList != null)
      {
        localObject1 = new com/twelfthmile/malana/compiler/parser/score/Score;
        Object localObject2 = seeder.score.auxMap;
        Object localObject3 = seeder.score.grammarList;
        int i = seeder.score.auxGrammarBaseSize;
        ((Score)localObject1).<init>((HashMap)localObject2, (ArrayList)localObject3, i);
        localObject2 = new com/twelfthmile/malana/compiler/parser/Parser;
        localObject3 = grammar;
        paramArrayList = struct;
        SemanticSeed localSemanticSeed = seeder.getSemanticSeed();
        ((Parser)localObject2).<init>((GrammarTrie)localObject3, paramArrayList, (Score)localObject1, localSemanticSeed);
        parser = ((Parser)localObject2);
        return Tokenizer.getTokens(paramRequest, paramList);
      }
    }
    return null;
  }
  
  public String getYugaTokens(Request paramRequest, ArrayList paramArrayList, List paramList)
  {
    Object localObject1 = address.toUpperCase();
    address = ((String)localObject1);
    if (paramArrayList != null)
    {
      paramArrayList = onDemandSeeding(paramArrayList);
      if (paramArrayList != null)
      {
        localObject1 = new com/twelfthmile/malana/compiler/parser/score/Score;
        Object localObject2 = seeder.score.auxMap;
        Object localObject3 = seeder.score.grammarList;
        int i = seeder.score.auxGrammarBaseSize;
        ((Score)localObject1).<init>((HashMap)localObject2, (ArrayList)localObject3, i);
        localObject2 = new com/twelfthmile/malana/compiler/parser/Parser;
        localObject3 = grammar;
        paramArrayList = struct;
        SemanticSeed localSemanticSeed = seeder.getSemanticSeed();
        ((Parser)localObject2).<init>((GrammarTrie)localObject3, paramArrayList, (Score)localObject1, localSemanticSeed);
        parser = ((Parser)localObject2);
        return Tokenizer.getYugaTokens(paramRequest, paramList);
      }
    }
    return null;
  }
  
  public String grammarOutput(Request paramRequest, ArrayList paramArrayList, List paramList)
  {
    Object localObject1 = address.toUpperCase();
    address = ((String)localObject1);
    if (paramArrayList != null)
    {
      paramArrayList = onDemandSeeding(paramArrayList);
      if (paramArrayList != null)
      {
        localObject1 = new com/twelfthmile/malana/compiler/parser/score/Score;
        Object localObject2 = seeder.score.auxMap;
        Object localObject3 = seeder.score.grammarList;
        int i = seeder.score.auxGrammarBaseSize;
        ((Score)localObject1).<init>((HashMap)localObject2, (ArrayList)localObject3, i);
        localObject2 = new com/twelfthmile/malana/compiler/parser/Parser;
        localObject3 = grammar;
        paramArrayList = struct;
        SemanticSeed localSemanticSeed = seeder.getSemanticSeed();
        ((Parser)localObject2).<init>((GrammarTrie)localObject3, paramArrayList, (Score)localObject1, localSemanticSeed);
        parser = ((Parser)localObject2);
        return Tokenizer.grammarOutput(paramRequest, paramList);
      }
    }
    return null;
  }
  
  public boolean grammarPresent(Request paramRequest, ArrayList paramArrayList, String paramString, String... paramVarArgs)
  {
    Object localObject1 = address.toUpperCase();
    address = ((String)localObject1);
    if (paramArrayList != null)
    {
      paramArrayList = onDemandSeeding(paramArrayList);
      if (paramArrayList != null)
      {
        localObject1 = new com/twelfthmile/malana/compiler/parser/score/Score;
        Object localObject2 = seeder.score.auxMap;
        Object localObject3 = seeder.score.grammarList;
        int i = seeder.score.auxGrammarBaseSize;
        ((Score)localObject1).<init>((HashMap)localObject2, (ArrayList)localObject3, i);
        localObject2 = new com/twelfthmile/malana/compiler/parser/Parser;
        localObject3 = grammar;
        paramArrayList = struct;
        SemanticSeed localSemanticSeed = seeder.getSemanticSeed();
        ((Parser)localObject2).<init>((GrammarTrie)localObject3, paramArrayList, (Score)localObject1, localSemanticSeed);
        parser = ((Parser)localObject2);
        return Tokenizer.grammarPresent(paramRequest, paramString, paramVarArgs);
      }
    }
    return false;
  }
  
  public Response parse(Request paramRequest, ArrayList paramArrayList)
  {
    Object localObject1 = address.toUpperCase();
    address = ((String)localObject1);
    if (paramArrayList != null)
    {
      paramArrayList = onDemandSeeding(paramArrayList);
      if (paramArrayList != null)
      {
        localObject1 = new com/twelfthmile/malana/compiler/parser/score/Score;
        Object localObject2 = seeder.score.auxMap;
        Object localObject3 = seeder.score.grammarList;
        int i = seeder.score.auxGrammarBaseSize;
        ((Score)localObject1).<init>((HashMap)localObject2, (ArrayList)localObject3, i);
        localObject2 = new com/twelfthmile/malana/compiler/parser/Parser;
        localObject3 = grammar;
        paramArrayList = struct;
        SemanticSeed localSemanticSeed = seeder.getSemanticSeed();
        ((Parser)localObject2).<init>((GrammarTrie)localObject3, paramArrayList, (Score)localObject1, localSemanticSeed);
        parser = ((Parser)localObject2);
        return Tokenizer.parse(paramRequest);
      }
    }
    return null;
  }
  
  public Response parse(Request paramRequest, boolean paramBoolean)
  {
    Object localObject1 = address.toUpperCase();
    address = ((String)localObject1);
    localObject1 = address;
    localObject1 = verifyAddress((String)localObject1);
    Object localObject2;
    if (paramBoolean)
    {
      if (localObject1 == null)
      {
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
      }
      localObject2 = "GRM_OFFERS";
      paramBoolean = ((ArrayList)localObject1).contains(localObject2);
      if (!paramBoolean)
      {
        localObject2 = "GRM_OFFERS";
        ((ArrayList)localObject1).add(localObject2);
      }
      ((ArrayList)localObject1).add("GRM_VOID");
      ((ArrayList)localObject1).add("GRM_OTP");
      localObject2 = "GRM_NOTIF";
      ((ArrayList)localObject1).add(localObject2);
    }
    else if (localObject1 != null)
    {
      localObject2 = "GRM_OFFERS";
      paramBoolean = ((ArrayList)localObject1).contains(localObject2);
      if (!paramBoolean)
      {
        localObject2 = "GRM_OFFERS";
        ((ArrayList)localObject1).add(localObject2);
      }
      ((ArrayList)localObject1).add("GRM_VOID");
      localObject2 = "GRM_BANK";
      paramBoolean = ((ArrayList)localObject1).contains(localObject2);
      if (paramBoolean)
      {
        ((ArrayList)localObject1).add("GRM_OTP");
        localObject2 = "GRM_NOTIF";
        ((ArrayList)localObject1).add(localObject2);
      }
    }
    if (localObject1 != null)
    {
      localObject2 = onDemandSeeding((ArrayList)localObject1);
      if (localObject2 != null)
      {
        localObject1 = new com/twelfthmile/malana/compiler/parser/score/Score;
        Object localObject3 = seeder.score.auxMap;
        Object localObject4 = seeder.score.grammarList;
        int i = seeder.score.auxGrammarBaseSize;
        ((Score)localObject1).<init>((HashMap)localObject3, (ArrayList)localObject4, i);
        localObject3 = new com/twelfthmile/malana/compiler/parser/Parser;
        localObject4 = grammar;
        localObject2 = struct;
        SemanticSeed localSemanticSeed = seeder.getSemanticSeed();
        ((Parser)localObject3).<init>((GrammarTrie)localObject4, (GrammarTrie)localObject2, (Score)localObject1, localSemanticSeed);
        parser = ((Parser)localObject3);
        return Tokenizer.parse(paramRequest);
      }
    }
    return null;
  }
  
  public ArrayList verifyAddress(String paramString)
  {
    Object localObject1 = getSeedData().getAddressTrie();
    boolean bool1 = child;
    Object localObject3;
    if (!bool1)
    {
      localObject1 = seeder;
      localObject2 = new org/json/JSONObject;
      localObject3 = addrJson;
      ((JSONObject)localObject2).<init>((String)localObject3);
      ((Seeder)localObject1).seedAddr((JSONObject)localObject2);
    }
    localObject1 = getSeedData().getAddressTrie();
    int i = 0;
    Object localObject2 = null;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      boolean bool2 = child;
      if (!bool2) {
        break;
      }
      Object localObject4 = next;
      Character localCharacter = Character.valueOf(j);
      bool2 = ((HashMap)localObject4).containsKey(localCharacter);
      if (!bool2) {
        break;
      }
      localObject4 = next;
      localCharacter = Character.valueOf(j);
      localObject4 = (AddressTrie)((HashMap)localObject4).get(localCharacter);
      bool2 = leaf;
      if (bool2)
      {
        int k = i + 1;
        int m = paramString.length();
        if (k == m)
        {
          paramString = new java/util/ArrayList;
          localObject1 = next;
          localObject2 = Character.valueOf(j);
          localObject1 = getgrammarList;
          paramString.<init>((Collection)localObject1);
          return paramString;
        }
      }
      localObject1 = next;
      localObject3 = Character.valueOf(j);
      localObject1 = (AddressTrie)((HashMap)localObject1).get(localObject3);
      i += 1;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.Compiler
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */