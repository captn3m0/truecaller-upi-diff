package com.twelfthmile.malana.compiler.types;

public class ConfType
{
  private int n;
  private String[] strArr;
  
  public ConfType(int paramInt)
  {
    n = paramInt;
  }
  
  public ConfType(int paramInt, String[] paramArrayOfString)
  {
    n = paramInt;
    strArr = paramArrayOfString;
  }
  
  public int getN()
  {
    return n;
  }
  
  public String[] getStrArr()
  {
    return strArr;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.ConfType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */