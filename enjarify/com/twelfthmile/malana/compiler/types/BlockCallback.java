package com.twelfthmile.malana.compiler.types;

public abstract interface BlockCallback
{
  public abstract void setValue(String paramString);
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.BlockCallback
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */