package com.twelfthmile.malana.compiler.types;

import java.util.Date;

public class Req
{
  public String address;
  public String message;
  public Date messageDate;
  
  public Req(Request paramRequest)
  {
    String str = message;
    message = str;
    str = address;
    address = str;
    paramRequest = messageDate;
    messageDate = paramRequest;
  }
  
  public Req(String paramString1, String paramString2, Date paramDate)
  {
    message = paramString1;
    address = paramString2;
    messageDate = paramDate;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.Req
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */