package com.twelfthmile.malana.compiler.types;

import com.twelfthmile.malana.compiler.parser.Parser;
import java.util.Date;

public class Request
{
  public String address;
  public String message;
  public Date messageDate;
  public Parser parser;
  
  public Request(String paramString1, String paramString2, Date paramDate)
  {
    message = paramString1;
    address = paramString2;
    messageDate = paramDate;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.Request
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */