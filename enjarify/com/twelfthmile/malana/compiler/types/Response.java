package com.twelfthmile.malana.compiler.types;

import com.twelfthmile.b.a.a;

public class Response
{
  private String category;
  private String condensedList;
  private String nodeList;
  private String score;
  private String tokenList;
  private a valMap;
  private boolean valid = false;
  
  public String getCategory()
  {
    return category;
  }
  
  public String getCondensedList()
  {
    return condensedList;
  }
  
  public String getNodeList()
  {
    return nodeList;
  }
  
  public String getScore()
  {
    return score;
  }
  
  public String getTokenList()
  {
    return tokenList;
  }
  
  public a getValMap()
  {
    return valMap;
  }
  
  public boolean isValid()
  {
    return valid;
  }
  
  public String print()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{ category: ");
    Object localObject = category;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" nodeList: ");
    localObject = nodeList;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" score:");
    localObject = score;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" tokenList:");
    localObject = tokenList;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" condensedList:");
    localObject = condensedList;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" valMap:");
    localObject = valMap;
    localStringBuilder.append(localObject);
    localStringBuilder.append(" }");
    return localStringBuilder.toString();
  }
  
  public void setCategory(String paramString)
  {
    valid = true;
    category = paramString;
  }
  
  public void setCondensedList(String paramString)
  {
    condensedList = paramString;
  }
  
  public void setNodeList(String paramString)
  {
    nodeList = paramString;
  }
  
  public void setScore(String paramString)
  {
    score = paramString;
  }
  
  public void setTokenList(String paramString)
  {
    tokenList = paramString;
  }
  
  public void setValMap(a parama)
  {
    valMap = parama;
  }
  
  public void setValid(boolean paramBoolean)
  {
    valid = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.Response
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */