package com.twelfthmile.malana.compiler.types;

import com.twelfthmile.malana.compiler.datastructure.TokenTrie;

public class TokenizerType
{
  public int index;
  public int nextIndex;
  public TokenTrie possibleToken;
  public int possibleTokenIndex;
  public int start;
  
  public TokenizerType(int paramInt1, int paramInt2, int paramInt3, TokenTrie paramTokenTrie, int paramInt4)
  {
    start = paramInt1;
    index = paramInt2;
    nextIndex = paramInt3;
    possibleToken = paramTokenTrie;
    possibleTokenIndex = paramInt4;
  }
  
  public TokenizerType getClone()
  {
    TokenizerType localTokenizerType = new com/twelfthmile/malana/compiler/types/TokenizerType;
    int i = index;
    int j = i + 1;
    int k = i + 1;
    int m = i + 1;
    localTokenizerType.<init>(j, k, m, null, -1);
    return localTokenizerType;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.TokenizerType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */