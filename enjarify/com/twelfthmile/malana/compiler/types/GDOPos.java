package com.twelfthmile.malana.compiler.types;

import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;

public abstract interface GDOPos
{
  public abstract Pair getGDOsAfter(GrammarDataObject paramGrammarDataObject);
  
  public abstract Pair getGDOsBefore(GrammarDataObject paramGrammarDataObject);
  
  public abstract Pair getGDOsBetween(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2);
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.GDOPos
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */