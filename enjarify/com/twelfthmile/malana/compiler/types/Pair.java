package com.twelfthmile.malana.compiler.types;

public class Pair
{
  Object a;
  Object b;
  
  public Pair(Object paramObject1, Object paramObject2)
  {
    a = paramObject1;
    b = paramObject2;
  }
  
  public Object getA()
  {
    return a;
  }
  
  public Object getB()
  {
    return b;
  }
  
  public void setA(Object paramObject)
  {
    a = paramObject;
  }
  
  public void setB(Object paramObject)
  {
    b = paramObject;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.Pair
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */