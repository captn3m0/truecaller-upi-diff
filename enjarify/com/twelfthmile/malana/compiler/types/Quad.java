package com.twelfthmile.malana.compiler.types;

public class Quad
{
  Object a;
  Object b;
  Object c;
  Object d;
  
  public Quad(Object paramObject1, Object paramObject2, Object paramObject3, Object paramObject4)
  {
    a = paramObject1;
    b = paramObject2;
    c = paramObject3;
    d = paramObject4;
  }
  
  public Object getA()
  {
    return a;
  }
  
  public Object getB()
  {
    return b;
  }
  
  public Object getC()
  {
    return c;
  }
  
  public Object getD()
  {
    return d;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.Quad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */