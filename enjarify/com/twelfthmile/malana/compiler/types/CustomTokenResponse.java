package com.twelfthmile.malana.compiler.types;

public class CustomTokenResponse
{
  int index;
  String token;
  String tokenTypeName;
  String tokenTypeValue;
  
  public CustomTokenResponse(int paramInt, String paramString1, String paramString2, String paramString3)
  {
    index = paramInt;
    token = paramString1;
    tokenTypeName = paramString2;
    tokenTypeValue = paramString3;
  }
  
  public int getIndex()
  {
    return index;
  }
  
  public String getToken()
  {
    return token;
  }
  
  public String getTokenTypeName()
  {
    return tokenTypeName;
  }
  
  public String getTokenTypeValue()
  {
    return tokenTypeValue;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.CustomTokenResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */