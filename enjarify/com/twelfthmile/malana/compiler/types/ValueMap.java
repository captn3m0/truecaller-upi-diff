package com.twelfthmile.malana.compiler.types;

import com.twelfthmile.b.a.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ValueMap
  implements a
{
  private List valList;
  private HashMap valMap;
  
  public ValueMap()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    valList = ((List)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    valMap = ((HashMap)localObject);
  }
  
  public void add(int paramInt1, int paramInt2)
  {
    List localList = valList;
    int i = localList.size();
    if (paramInt1 >= i) {
      return;
    }
    valList.remove(paramInt1);
    localList = valList;
    Integer localInteger = Integer.valueOf(paramInt2);
    localList.add(paramInt1, localInteger);
  }
  
  public boolean containsKey(String paramString)
  {
    return valMap.containsKey(paramString);
  }
  
  public boolean containsValue(String paramString)
  {
    return valMap.containsValue(paramString);
  }
  
  public Set entrySet()
  {
    return valMap.entrySet();
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    HashMap localHashMap = null;
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (ValueMap)paramObject;
        localObject1 = valList;
        localObject2 = valList;
        boolean bool = ((List)localObject1).equals(localObject2);
        if (!bool) {
          return false;
        }
        localHashMap = valMap;
        paramObject = valMap;
        return localHashMap.equals(paramObject);
      }
    }
    return false;
  }
  
  public int get(int paramInt)
  {
    List localList = valList;
    int i = localList.size();
    if (paramInt >= i) {
      return 0;
    }
    return ((Integer)valList.get(paramInt)).intValue();
  }
  
  public String get(String paramString)
  {
    return (String)valMap.get(paramString);
  }
  
  public Map getAll()
  {
    return valMap;
  }
  
  public int hashCode()
  {
    int i = valList.hashCode() * 31;
    int j = valMap.hashCode();
    return i + j;
  }
  
  public void initList(int paramInt)
  {
    int i = 0;
    while (i < paramInt)
    {
      List localList = valList;
      Integer localInteger = Integer.valueOf(0);
      localList.add(i, localInteger);
      i += 1;
    }
  }
  
  public Set keySet()
  {
    return valMap.keySet();
  }
  
  public String put(String paramString1, String paramString2)
  {
    paramString1.hashCode();
    return (String)valMap.put(paramString1, paramString2);
  }
  
  public void putAll(Map paramMap)
  {
    valMap.putAll(paramMap);
  }
  
  public String remove(String paramString)
  {
    return (String)valMap.remove(paramString);
  }
  
  public int size()
  {
    return valMap.size();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    localStringBuilder1.<init>("");
    Iterator localIterator = valList.iterator();
    StringBuilder localStringBuilder2;
    Object localObject;
    String str;
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      int i = ((Integer)localIterator.next()).intValue();
      localStringBuilder2 = new java/lang/StringBuilder;
      localObject = "[";
      localStringBuilder2.<init>((String)localObject);
      localStringBuilder2.append(i);
      localStringBuilder2.append("]");
      str = localStringBuilder2.toString();
      localStringBuilder1.append(str);
    }
    localStringBuilder1.append(" ");
    localIterator = valMap.keySet().iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      str = (String)localIterator.next();
      localStringBuilder2 = new java/lang/StringBuilder;
      localStringBuilder2.<init>("{");
      localStringBuilder2.append(str);
      localStringBuilder2.append(",");
      localObject = valMap;
      str = (String)((HashMap)localObject).get(str);
      localStringBuilder2.append(str);
      localStringBuilder2.append("}");
      str = localStringBuilder2.toString();
      localStringBuilder1.append(str);
    }
    return localStringBuilder1.toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.ValueMap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */