package com.twelfthmile.malana.compiler.types;

import com.twelfthmile.b.a.a;

public class Finalise
{
  private String category;
  private Req request;
  private a valueMap;
  
  public Finalise(String paramString, Req paramReq)
  {
    category = paramString;
    request = paramReq;
    paramString = new com/twelfthmile/malana/compiler/types/ValueMap;
    paramString.<init>();
    valueMap = paramString;
  }
  
  public String getAddress()
  {
    return request.address;
  }
  
  public String getCategory()
  {
    return category;
  }
  
  public String getMessage()
  {
    return request.message;
  }
  
  public a getValueMap()
  {
    return valueMap;
  }
  
  public void setCategory(String paramString)
  {
    category = paramString;
  }
  
  public void setValueMap(a parama)
  {
    valueMap = parama;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.Finalise
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */