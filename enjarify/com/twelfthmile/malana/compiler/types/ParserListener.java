package com.twelfthmile.malana.compiler.types;

import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;
import java.util.List;

public abstract interface ParserListener
{
  public abstract void add(GrammarDataObject paramGrammarDataObject);
  
  public abstract void condensation(GrammarDataObject paramGrammarDataObject);
  
  public abstract List contextIndexList();
  
  public abstract void end(int paramInt);
  
  public abstract void score(GrammarDataObject paramGrammarDataObject);
  
  public abstract boolean shouldCondense(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2, GrammarDataObject paramGrammarDataObject3);
  
  public abstract Pair shouldEnd(String paramString, int paramInt1, int paramInt2);
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.ParserListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */