package com.twelfthmile.malana.compiler.types;

import com.twelfthmile.malana.compiler.parser.semantic.SemanticSeedConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SemanticSeed
{
  private HashMap mConfidenceMap;
  private List mContextBreak;
  private SemanticSeedConstants mSemanticConstants;
  private HashMap mSemanticMap;
  
  public SemanticSeed()
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    mSemanticMap = ((HashMap)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    mConfidenceMap = ((HashMap)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    mContextBreak = ((List)localObject);
    localObject = new com/twelfthmile/malana/compiler/parser/semantic/SemanticSeedConstants;
    ((SemanticSeedConstants)localObject).<init>();
    mSemanticConstants = ((SemanticSeedConstants)localObject);
  }
  
  public HashMap getConfidenceMap()
  {
    return mConfidenceMap;
  }
  
  public List getContextBreak()
  {
    return mContextBreak;
  }
  
  public SemanticSeedConstants getSemanticConstants()
  {
    return mSemanticConstants;
  }
  
  public HashMap getSemanticMap()
  {
    return mSemanticMap;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.SemanticSeed
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */