package com.twelfthmile.malana.compiler.types;

public class Triplet
{
  Object a;
  Object b;
  Object c;
  
  public Triplet(Object paramObject1, Object paramObject2, Object paramObject3)
  {
    a = paramObject1;
    b = paramObject2;
    c = paramObject3;
  }
  
  public Object getA()
  {
    return a;
  }
  
  public Object getB()
  {
    return b;
  }
  
  public Object getC()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.Triplet
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */