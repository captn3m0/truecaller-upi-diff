package com.twelfthmile.malana.compiler.types;

import com.twelfthmile.malana.compiler.parser.gdo.GrammarDataObject;

public abstract interface GrammarCallback
{
  public abstract void add(GrammarDataObject paramGrammarDataObject);
  
  public abstract void add(GrammarDataObject paramGrammarDataObject, String paramString, int paramInt);
  
  public abstract void condensation(GrammarDataObject paramGrammarDataObject);
  
  public abstract boolean shouldCondense(GrammarDataObject paramGrammarDataObject1, GrammarDataObject paramGrammarDataObject2, GrammarDataObject paramGrammarDataObject3);
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.GrammarCallback
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */