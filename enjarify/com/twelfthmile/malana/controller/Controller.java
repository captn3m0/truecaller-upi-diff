package com.twelfthmile.malana.controller;

import com.twelfthmile.e.a.b;
import com.twelfthmile.malana.compiler.Compiler;
import com.twelfthmile.malana.compiler.parser.branch.BranchDebug;
import com.twelfthmile.malana.compiler.types.MalanaSeed;
import com.twelfthmile.malana.compiler.types.Pair;
import com.twelfthmile.malana.compiler.types.Request;
import com.twelfthmile.malana.compiler.types.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class Controller
{
  static Compiler INSTANCE;
  static MalanaSeed sMalanaSeed;
  
  public static ArrayList getGRMs()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localArrayList.add("GRM_BANK");
    localArrayList.add("GRM_BILL");
    localArrayList.add("GRM_EVENT");
    localArrayList.add("GRM_TRAVEL");
    localArrayList.add("GRM_OFFERS");
    localArrayList.add("GRM_OTP");
    localArrayList.add("GRM_NOTIF");
    localArrayList.add("GRM_DELIVERY");
    localArrayList.add("GRM_VOID");
    return localArrayList;
  }
  
  public static Compiler getInstance()
  {
    Object localObject = sMalanaSeed;
    if (localObject != null)
    {
      localObject = INSTANCE;
      if (localObject != null) {
        return (Compiler)localObject;
      }
      return Controller.LazyHolder.INSTANCE;
    }
    localObject = new java/lang/RuntimeException;
    ((RuntimeException)localObject).<init>("Malana not inited");
    throw ((Throwable)localObject);
  }
  
  public static String getSemantics(String paramString)
  {
    Compiler localCompiler = getInstance();
    Request localRequest = new com/twelfthmile/malana/compiler/types/Request;
    Date localDate = new java/util/Date;
    localDate.<init>();
    localRequest.<init>(paramString, "", localDate);
    paramString = getGRMs();
    return localCompiler.getSemantics(localRequest, paramString);
  }
  
  public static String getTokens(String paramString)
  {
    Compiler localCompiler = getInstance();
    Request localRequest = new com/twelfthmile/malana/compiler/types/Request;
    Date localDate = new java/util/Date;
    localDate.<init>();
    localRequest.<init>(paramString, "", localDate);
    paramString = getGRMs();
    return localCompiler.getTokens(localRequest, paramString, null);
  }
  
  public static String getTokens(String paramString, List paramList1, List paramList2)
  {
    JSONArray localJSONArray = new org/json/JSONArray;
    paramString = getTokensnc(paramString, paramList1);
    localJSONArray.<init>(paramString);
    paramString = new java/util/ArrayList;
    paramString.<init>();
    int i = 0;
    paramList1 = null;
    for (;;)
    {
      int j = localJSONArray.length();
      if (i >= j) {
        break;
      }
      Object localObject1 = localJSONArray.getJSONObject(i);
      String str = "GDO_NONDET";
      Object localObject2 = ((JSONObject)localObject1).get("token");
      boolean bool1 = str.equals(localObject2);
      if (!bool1)
      {
        str = ((JSONObject)localObject1).getString("token");
        boolean bool2 = paramList2.contains(str);
        if (bool2)
        {
          paramString.add(str);
        }
        else
        {
          str = "str";
          localObject1 = ((JSONObject)localObject1).getString(str);
          paramString.add(localObject1);
        }
      }
      else
      {
        str = "str";
        localObject1 = ((JSONObject)localObject1).getString(str);
        paramString.add(localObject1);
      }
      i += 1;
    }
    paramList1 = new java/lang/StringBuilder;
    paramList1.<init>();
    paramString = paramString.iterator();
    for (;;)
    {
      boolean bool3 = paramString.hasNext();
      if (!bool3) {
        break;
      }
      paramList2 = (String)paramString.next();
      paramList1.append(paramList2);
      paramList2 = " ";
      paramList1.append(paramList2);
    }
    return paramList1.toString();
  }
  
  public static String getTokensnc(String paramString, List paramList)
  {
    Compiler localCompiler = getInstance();
    Request localRequest = new com/twelfthmile/malana/compiler/types/Request;
    Date localDate = new java/util/Date;
    localDate.<init>();
    localRequest.<init>(paramString, "", localDate);
    paramString = getGRMs();
    return localCompiler.getTokens(localRequest, paramString, paramList);
  }
  
  public static String getYugaTokens(String paramString, List paramList)
  {
    Compiler localCompiler = getInstance();
    Request localRequest = new com/twelfthmile/malana/compiler/types/Request;
    Date localDate = new java/util/Date;
    localDate.<init>();
    localRequest.<init>(paramString, "", localDate);
    paramString = getGRMs();
    return localCompiler.getYugaTokens(localRequest, paramString, paramList);
  }
  
  public static String grammarOutput(String paramString1, String paramString2, Date paramDate, List paramList)
  {
    Compiler localCompiler = getInstance();
    Request localRequest = new com/twelfthmile/malana/compiler/types/Request;
    localRequest.<init>(paramString1, paramString2, paramDate);
    paramString1 = getGRMs();
    return localCompiler.grammarOutput(localRequest, paramString1, paramList);
  }
  
  public static boolean grammarPresent(String paramString1, String paramString2, String... paramVarArgs)
  {
    Compiler localCompiler = getInstance();
    Request localRequest = new com/twelfthmile/malana/compiler/types/Request;
    Date localDate = new java/util/Date;
    localDate.<init>();
    localRequest.<init>(paramString1, "", localDate);
    paramString1 = getGRMs();
    return localCompiler.grammarPresent(localRequest, paramString1, paramString2, paramVarArgs);
  }
  
  public static void init(MalanaSeed paramMalanaSeed)
  {
    init(paramMalanaSeed, false);
  }
  
  public static void init(MalanaSeed paramMalanaSeed, boolean paramBoolean)
  {
    sMalanaSeed = paramMalanaSeed;
    if (paramBoolean)
    {
      paramMalanaSeed = new com/twelfthmile/malana/compiler/Compiler;
      MalanaSeed localMalanaSeed = sMalanaSeed;
      paramMalanaSeed.<init>(localMalanaSeed);
      INSTANCE = paramMalanaSeed;
    }
  }
  
  public static Response parse(String paramString1, String paramString2, Date paramDate)
  {
    Compiler localCompiler = getInstance();
    Request localRequest = new com/twelfthmile/malana/compiler/types/Request;
    localRequest.<init>(paramString1, paramString2, paramDate);
    return localCompiler.parse(localRequest, true);
  }
  
  public static Response parse(String paramString1, String paramString2, Date paramDate, ArrayList paramArrayList)
  {
    Compiler localCompiler = getInstance();
    Request localRequest = new com/twelfthmile/malana/compiler/types/Request;
    localRequest.<init>(paramString1, paramString2, paramDate);
    return localCompiler.parse(localRequest, paramArrayList);
  }
  
  public static Response parse(String paramString1, String paramString2, Date paramDate, boolean paramBoolean)
  {
    Compiler localCompiler = getInstance();
    Request localRequest = new com/twelfthmile/malana/compiler/types/Request;
    localRequest.<init>(paramString1, paramString2, paramDate);
    return localCompiler.parse(localRequest, paramBoolean);
  }
  
  public static String parse(String paramString1, String paramString2, String paramString3)
  {
    Object localObject1;
    if (paramString2 != null) {
      localObject1 = "";
    }
    try
    {
      boolean bool1 = paramString2.equals(localObject1);
      if (!bool1)
      {
        localObject1 = new java/util/ArrayList;
        str1 = ",";
        paramString2 = paramString2.split(str1);
        paramString2 = Arrays.asList(paramString2);
        ((ArrayList)localObject1).<init>(paramString2);
        paramString2 = new java/util/ArrayList;
        str1 = ",";
        paramString3 = paramString3.split(str1);
        paramString3 = Arrays.asList(paramString3);
        paramString2.<init>(paramString3);
      }
      else
      {
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        paramString2 = new java/util/ArrayList;
        paramString2.<init>();
        paramString3 = "GRM_BANK";
        ((ArrayList)localObject1).add(paramString3);
        paramString3 = "GRM_BILL";
        ((ArrayList)localObject1).add(paramString3);
        paramString3 = "GRM_BANK";
        paramString2.add(paramString3);
        paramString3 = "GRM_BILL";
        paramString2.add(paramString3);
      }
      paramString3 = "GRM_VOID";
      ((ArrayList)localObject1).add(paramString3);
      paramString3 = new org/json/JSONArray;
      paramString3.<init>(paramString1);
      paramString1 = new java/lang/StringBuilder;
      String str1 = "[";
      paramString1.<init>(str1);
      str1 = null;
      int i = 0;
      for (;;)
      {
        int j = paramString3.length();
        if (i >= j) {
          break;
        }
        Object localObject2 = paramString3.getJSONObject(i);
        Object localObject3 = "date";
        localObject3 = ((JSONObject)localObject2).getString((String)localObject3);
        localObject3 = com.twelfthmile.e.a.a((String)localObject3);
        if (localObject3 != null)
        {
          localObject3 = ((b)localObject3).b();
          localObject3 = (Date)localObject3;
        }
        else
        {
          localObject3 = new java/util/Date;
          ((Date)localObject3).<init>();
        }
        Object localObject4 = "body";
        try
        {
          localObject4 = ((JSONObject)localObject2).getString((String)localObject4);
          String str2 = "\n";
          String str3 = " ";
          localObject4 = ((String)localObject4).replace(str2, str3);
          str2 = "\r";
          str3 = " ";
          localObject4 = ((String)localObject4).replace(str2, str3);
          str2 = "addr";
          localObject2 = ((JSONObject)localObject2).getString(str2);
          localObject2 = parse((String)localObject4, (String)localObject2, (Date)localObject3, (ArrayList)localObject1);
          if (localObject2 != null)
          {
            localObject3 = ((Response)localObject2).getCategory();
            boolean bool2 = paramString2.contains(localObject3);
            if (bool2)
            {
              localObject3 = ((Response)localObject2).getValMap();
              localObject4 = ((Response)localObject2).getNodeList();
              localObject3 = Interface.getTagsAndBlocks((com.twelfthmile.b.a.a)localObject3, (String)localObject4);
              localObject4 = new java/lang/StringBuilder;
              str2 = "{\"status\":\"success\",\"data\":{ \"category\":\"";
              ((StringBuilder)localObject4).<init>(str2);
              str2 = ((Response)localObject2).getCategory();
              str2 = Interface.getCategory(str2);
              ((StringBuilder)localObject4).append(str2);
              str2 = "\",\"values\":";
              ((StringBuilder)localObject4).append(str2);
              localObject2 = ((Response)localObject2).getValMap();
              localObject2 = BranchDebug.valuePrint((com.twelfthmile.b.a.a)localObject2);
              ((StringBuilder)localObject4).append((String)localObject2);
              localObject2 = ",\"tags\":";
              ((StringBuilder)localObject4).append((String)localObject2);
              localObject2 = ((Pair)localObject3).getA();
              localObject2 = (HashMap)localObject2;
              localObject2 = BranchDebug.valuePrint((HashMap)localObject2);
              ((StringBuilder)localObject4).append((String)localObject2);
              localObject2 = ",\"unclassified\":";
              ((StringBuilder)localObject4).append((String)localObject2);
              localObject2 = ((Pair)localObject3).getB();
              ((StringBuilder)localObject4).append(localObject2);
              localObject2 = "}}";
              ((StringBuilder)localObject4).append((String)localObject2);
              localObject2 = ((StringBuilder)localObject4).toString();
              paramString1.append((String)localObject2);
              localObject2 = ",";
              paramString1.append((String)localObject2);
            }
            else
            {
              localObject3 = new java/lang/StringBuilder;
              localObject4 = "{\"status\":\"unsupported\",\"data\":{ \"category\":\"";
              ((StringBuilder)localObject3).<init>((String)localObject4);
              localObject2 = ((Response)localObject2).getCategory();
              localObject2 = Interface.getCategory((String)localObject2);
              ((StringBuilder)localObject3).append((String)localObject2);
              localObject2 = "\"}}";
              ((StringBuilder)localObject3).append((String)localObject2);
              localObject2 = ((StringBuilder)localObject3).toString();
              paramString1.append((String)localObject2);
              localObject2 = ",";
              paramString1.append((String)localObject2);
            }
          }
          else
          {
            localObject2 = "{\"status\":\"fail\"},";
            paramString1.append((String)localObject2);
          }
        }
        catch (Exception localException1)
        {
          localObject2 = "{\"status\":\"error\"},";
          paramString1.append((String)localObject2);
        }
        i += 1;
      }
      int k = paramString1.length();
      int m = 1;
      if (k > m)
      {
        k = paramString1.length() - m;
        paramString1.setLength(k);
      }
      paramString2 = "]";
      paramString1.append(paramString2);
      paramString2 = paramString1.toString();
      paramString1.setLength(0);
      paramString1.trimToSize();
      paramString1 = new org/json/JSONArray;
      paramString1.<init>(paramString2);
      return paramString1.toString();
    }
    catch (Exception localException2) {}
    return "[{\"status\":\"error\"}]";
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.Controller
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */