package com.twelfthmile.malana.controller;

import com.twelfthmile.malana.compiler.Compiler;
import com.twelfthmile.malana.compiler.types.MalanaSeed;

class Controller$LazyHolder
{
  static Compiler INSTANCE;
  
  static
  {
    Compiler localCompiler = new com/twelfthmile/malana/compiler/Compiler;
    MalanaSeed localMalanaSeed = Controller.sMalanaSeed;
    localCompiler.<init>(localMalanaSeed);
    INSTANCE = localCompiler;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.Controller.LazyHolder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */