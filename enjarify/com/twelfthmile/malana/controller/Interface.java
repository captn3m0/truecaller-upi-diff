package com.twelfthmile.malana.controller;

import com.twelfthmile.b.a.a;
import com.twelfthmile.malana.compiler.types.Pair;
import com.twelfthmile.malana.compiler.util.StringEscapeUtils;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;

public class Interface
{
  static String getCategory(String paramString)
  {
    int i = paramString.hashCode();
    String str;
    boolean bool2;
    boolean bool4;
    switch (i)
    {
    default: 
      break;
    case 1240557924: 
      str = "GRM_BILL";
      boolean bool1 = paramString.equals(str);
      if (bool1) {
        int j = 3;
      }
      break;
    case 1240550297: 
      str = "GRM_BANK";
      bool2 = paramString.equals(str);
      if (bool2)
      {
        bool2 = false;
        paramString = null;
      }
      break;
    case 1009862158: 
      str = "GRM_OTP";
      bool2 = paramString.equals(str);
      if (bool2) {
        int k = 5;
      }
      break;
    case -186141357: 
      str = "GRM_NOTIF";
      boolean bool3 = paramString.equals(str);
      if (bool3) {
        int m = 6;
      }
      break;
    case -194258755: 
      str = "GRM_EVENT";
      bool4 = paramString.equals(str);
      if (bool4) {
        bool4 = true;
      }
      break;
    case -1296211247: 
      str = "GRM_DELIVERY";
      bool4 = paramString.equals(str);
      if (bool4) {
        int n = 7;
      }
      break;
    case -1301422793: 
      str = "GRM_TRAVEL";
      boolean bool5 = paramString.equals(str);
      if (bool5) {
        int i1 = 2;
      }
      break;
    case -1455517772: 
      str = "GRM_OFFERS";
      boolean bool6 = paramString.equals(str);
      if (bool6) {
        i2 = 4;
      }
      break;
    }
    int i2 = -1;
    switch (i2)
    {
    default: 
      return "UnClassified";
    case 7: 
      return "Delivery";
    case 6: 
      return "Notification";
    case 5: 
      return "OTP";
    case 4: 
      return "Offers";
    case 3: 
      return "BillReminder";
    case 2: 
      return "Travel";
    case 1: 
      return "Movie";
    }
    return "Finance";
  }
  
  static Pair getTagsAndBlocks(a parama, String paramString)
  {
    Object localObject1 = new org/json/JSONArray;
    ((JSONArray)localObject1).<init>(paramString);
    paramString = new java/util/ArrayList;
    paramString.<init>();
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i = 0;
    String str1 = null;
    for (;;)
    {
      int j = ((JSONArray)localObject1).length();
      if (i >= j) {
        break;
      }
      String str2 = ((JSONArray)localObject1).getString(i);
      Object localObject2 = "#";
      boolean bool1 = str2.startsWith((String)localObject2);
      if (bool1)
      {
        localObject2 = str2.substring(1);
        bool1 = parama.containsValue((String)localObject2);
        if (!bool1)
        {
          localObject2 = new java/lang/StringBuilder;
          String str3 = "\"";
          ((StringBuilder)localObject2).<init>(str3);
          str2 = StringEscapeUtils.escapeJava(str2);
          ((StringBuilder)localObject2).append(str2);
          ((StringBuilder)localObject2).append("\"");
          str2 = ((StringBuilder)localObject2).toString();
          paramString.add(str2);
        }
      }
      i += 1;
    }
    localObject1 = "trx_category";
    boolean bool2 = parama.containsKey((String)localObject1);
    if (bool2)
    {
      localObject1 = "trx_category";
      str1 = parama.remove("trx_category");
      localHashMap.put(localObject1, str1);
    }
    localObject1 = "trx_subcategory";
    bool2 = parama.containsKey((String)localObject1);
    if (bool2)
    {
      localObject1 = "trx_subcategory";
      str1 = "trx_subcategory";
      parama = parama.remove(str1);
      localHashMap.put(localObject1, parama);
    }
    parama = new com/twelfthmile/malana/compiler/types/Pair;
    parama.<init>(localHashMap, paramString);
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.Interface
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */