package com.twelfthmile.malana.controller.task;

import com.twelfthmile.malana.compiler.types.Request;
import com.twelfthmile.malana.compiler.types.Response;
import com.twelfthmile.malana.controller.Controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Callable;
import org.json.JSONObject;

class TaskMaster$4
  implements Callable
{
  TaskMaster$4(TaskMaster paramTaskMaster, JSONObject paramJSONObject, Date paramDate) {}
  
  public TaskResponse call()
  {
    Object localObject1 = val$jsonObj.getString("body").replace("\n", ". ").replace("\r", ". ");
    Object localObject2 = val$jsonObj.getString("addr");
    Object localObject3 = val$parsedDateFromYuga;
    Object localObject4 = Controller.getGRMs();
    localObject1 = Controller.parse((String)localObject1, (String)localObject2, (Date)localObject3, (ArrayList)localObject4);
    localObject2 = new com/twelfthmile/malana/controller/task/TaskResponse;
    localObject3 = new com/twelfthmile/malana/compiler/types/Request;
    localObject4 = val$jsonObj.getString("body").replace("\n", ". ").replace("\r", ". ");
    String str = val$jsonObj.getString("addr");
    Date localDate = val$parsedDateFromYuga;
    ((Request)localObject3).<init>((String)localObject4, str, localDate);
    ((TaskResponse)localObject2).<init>((Response)localObject1, (Request)localObject3);
    return (TaskResponse)localObject2;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.task.TaskMaster.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */