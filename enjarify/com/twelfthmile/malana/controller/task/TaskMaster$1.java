package com.twelfthmile.malana.controller.task;

import com.twelfthmile.malana.compiler.Compiler;
import com.twelfthmile.malana.compiler.types.Request;
import com.twelfthmile.malana.compiler.types.Response;
import com.twelfthmile.malana.controller.Controller;
import java.util.Date;
import java.util.concurrent.Callable;

class TaskMaster$1
  implements Callable
{
  TaskMaster$1(TaskMaster paramTaskMaster, String paramString1, String paramString2, Date paramDate, boolean paramBoolean, long paramLong) {}
  
  public TaskResponse call()
  {
    try
    {
      Request localRequest = new com/twelfthmile/malana/compiler/types/Request;
      Object localObject1 = val$message;
      Object localObject2 = val$address;
      Date localDate = val$messageDate;
      localRequest.<init>((String)localObject1, (String)localObject2, localDate);
      localObject1 = Controller.getInstance();
      boolean bool = val$live;
      localObject1 = ((Compiler)localObject1).parse(localRequest, bool);
      if (localObject1 == null) {
        return null;
      }
      parser = null;
      localObject2 = new com/twelfthmile/malana/controller/task/TaskResponse;
      long l = val$id;
      ((TaskResponse)localObject2).<init>((Response)localObject1, localRequest, l);
      return (TaskResponse)localObject2;
    }
    catch (Exception localException) {}
    return null;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.task.TaskMaster.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */