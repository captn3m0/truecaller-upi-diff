package com.twelfthmile.malana.controller.task;

import com.twelfthmile.malana.compiler.parser.branch.BranchDebug;
import com.twelfthmile.malana.compiler.types.Request;
import com.twelfthmile.malana.compiler.types.Response;
import com.twelfthmile.malana.compiler.util.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;

class TaskMaster$3
  implements TaskCallback
{
  TaskMaster$3(TaskMaster paramTaskMaster, JSONArray paramJSONArray) {}
  
  public void bind(TaskResponse paramTaskResponse)
  {
    Object localObject1 = new org/json/JSONObject;
    ((JSONObject)localObject1).<init>();
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    if (paramTaskResponse != null)
    {
      Object localObject2 = paramTaskResponse.getResponse();
      paramTaskResponse = paramTaskResponse.getRequest();
      if (localObject2 != null)
      {
        localJSONObject.put("status", "success");
        paramTaskResponse = StringEscapeUtils.escapeJava(message);
        localJSONObject.put("message", paramTaskResponse);
        Object localObject3 = ((Response)localObject2).getCategory();
        ((JSONObject)localObject1).put("category", localObject3);
        localObject3 = new org/json/JSONArray;
        String str = ((Response)localObject2).getNodeList();
        ((JSONArray)localObject3).<init>(str);
        ((JSONObject)localObject1).put("nodes", localObject3);
        localObject3 = new org/json/JSONObject;
        localObject2 = BranchDebug.valuePrint(((Response)localObject2).getValMap());
        ((JSONObject)localObject3).<init>((String)localObject2);
        ((JSONObject)localObject1).put("values", localObject3);
        paramTaskResponse = "data";
        localJSONObject.put(paramTaskResponse, localObject1);
      }
      else
      {
        paramTaskResponse = "status";
        localObject1 = "fail";
        localJSONObject.put(paramTaskResponse, localObject1);
      }
      paramTaskResponse = val$retArr;
      paramTaskResponse.put(localJSONObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.task.TaskMaster.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */