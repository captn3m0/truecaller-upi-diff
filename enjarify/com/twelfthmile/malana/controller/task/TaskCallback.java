package com.twelfthmile.malana.controller.task;

public abstract interface TaskCallback
{
  public abstract void bind(TaskResponse paramTaskResponse);
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.task.TaskCallback
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */