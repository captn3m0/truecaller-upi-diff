package com.twelfthmile.malana.controller.task;

import com.twelfthmile.e.a;
import com.twelfthmile.e.a.b;
import com.twelfthmile.malana.controller.Controller;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

public class TaskMaster
{
  private boolean inited = false;
  private TaskManager mTaskManager;
  
  public void execute(String paramString1, String paramString2, Date paramDate, ArrayList paramArrayList, long paramLong)
  {
    boolean bool = inited;
    if (bool)
    {
      TaskManager localTaskManager = mTaskManager;
      TaskMaster.2 local2 = new com/twelfthmile/malana/controller/task/TaskMaster$2;
      localObject = local2;
      local2.<init>(this, paramString1, paramString2, paramDate, paramArrayList, paramLong);
      localTaskManager.execute(local2);
      return;
    }
    Object localObject = new java/lang/RuntimeException;
    ((RuntimeException)localObject).<init>("Not inited!");
    throw ((Throwable)localObject);
  }
  
  public void execute(String paramString1, String paramString2, Date paramDate, boolean paramBoolean, long paramLong)
  {
    boolean bool = inited;
    if (bool)
    {
      TaskManager localTaskManager = mTaskManager;
      TaskMaster.1 local1 = new com/twelfthmile/malana/controller/task/TaskMaster$1;
      localObject = local1;
      local1.<init>(this, paramString1, paramString2, paramDate, paramBoolean, paramLong);
      localTaskManager.execute(local1);
      return;
    }
    Object localObject = new java/lang/RuntimeException;
    ((RuntimeException)localObject).<init>("Not inited!");
    throw ((Throwable)localObject);
  }
  
  public void exit()
  {
    mTaskManager.finish();
    mTaskManager = null;
    inited = false;
  }
  
  public void init(TaskCallback paramTaskCallback)
  {
    a.a();
    Controller.getInstance();
    TaskManager localTaskManager = new com/twelfthmile/malana/controller/task/TaskManager;
    localTaskManager.<init>(paramTaskCallback);
    mTaskManager = localTaskManager;
    inited = true;
  }
  
  public String run(JSONArray paramJSONArray)
  {
    JSONArray localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>();
    TaskMaster.3 local3 = new com/twelfthmile/malana/controller/task/TaskMaster$3;
    local3.<init>(this, localJSONArray);
    init(local3);
    int i = 0;
    local3 = null;
    for (;;)
    {
      int j = paramJSONArray.length();
      if (i >= j) {
        break;
      }
      try
      {
        JSONObject localJSONObject = paramJSONArray.getJSONObject(i);
        Object localObject = "date";
        localObject = localJSONObject.getString((String)localObject);
        localObject = a.a((String)localObject);
        localObject = ((b)localObject).b();
        localObject = (Date)localObject;
        TaskManager localTaskManager = mTaskManager;
        TaskMaster.4 local4 = new com/twelfthmile/malana/controller/task/TaskMaster$4;
        local4.<init>(this, localJSONObject, (Date)localObject);
        localTaskManager.execute(local4);
      }
      catch (Exception localException)
      {
        for (;;) {}
      }
      i += 1;
    }
    exit();
    return localJSONArray.toString(2);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.task.TaskMaster
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */