package com.twelfthmile.malana.controller.task;

import java.util.concurrent.ThreadFactory;

class TaskManager$BackgroundThreadFactory
  implements ThreadFactory
{
  public Thread newThread(Runnable paramRunnable)
  {
    Thread localThread = new java/lang/Thread;
    localThread.<init>(paramRunnable);
    localThread.setName("malanaCustomThread");
    localThread.setPriority(5);
    paramRunnable = new com/twelfthmile/malana/controller/task/TaskManager$BackgroundThreadFactory$1;
    paramRunnable.<init>(this);
    localThread.setUncaughtExceptionHandler(paramRunnable);
    return localThread;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.task.TaskManager.BackgroundThreadFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */