package com.twelfthmile.malana.controller.task;

import com.twelfthmile.malana.compiler.types.Request;
import com.twelfthmile.malana.compiler.types.Response;

public class TaskResponse
{
  long id;
  Request request;
  Response response;
  
  public TaskResponse(Response paramResponse, Request paramRequest)
  {
    response = paramResponse;
    request = paramRequest;
  }
  
  public TaskResponse(Response paramResponse, Request paramRequest, long paramLong)
  {
    this(paramResponse, paramRequest);
    id = paramLong;
  }
  
  public long getId()
  {
    return id;
  }
  
  public Request getRequest()
  {
    return request;
  }
  
  public Response getResponse()
  {
    return response;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.task.TaskResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */