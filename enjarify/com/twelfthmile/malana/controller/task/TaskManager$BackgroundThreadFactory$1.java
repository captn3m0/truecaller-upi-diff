package com.twelfthmile.malana.controller.task;

import com.twelfthmile.malana.compiler.util.L;

class TaskManager$BackgroundThreadFactory$1
  implements Thread.UncaughtExceptionHandler
{
  TaskManager$BackgroundThreadFactory$1(TaskManager.BackgroundThreadFactory paramBackgroundThreadFactory) {}
  
  public void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramThread = paramThread.getName();
    localStringBuilder.append(paramThread);
    localStringBuilder.append(" encountered an error: ");
    paramThread = paramThrowable.getMessage();
    localStringBuilder.append(paramThread);
    L.msg(localStringBuilder.toString());
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.task.TaskManager.BackgroundThreadFactory.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */