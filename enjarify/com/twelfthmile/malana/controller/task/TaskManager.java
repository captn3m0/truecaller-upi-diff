package com.twelfthmile.malana.controller.task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskManager
{
  private static final int QUEUE_SIZE = 10;
  private int KEEP_ALIVE_TIME;
  private TimeUnit KEEP_ALIVE_TIME_UNIT;
  private int NUMBER_OF_CORES;
  private List bufferList;
  private CompletionService mCompletionService;
  private volatile boolean mExecuted;
  private ExecutorService mExecutorLooper;
  private ExecutorService mExecutorPool;
  private BlockingQueue mTaskBlockingQueue;
  private AtomicInteger parity;
  
  public TaskManager(TaskCallback paramTaskCallback)
  {
    Object localObject1 = Runtime.getRuntime();
    int i = ((Runtime)localObject1).availableProcessors();
    NUMBER_OF_CORES = i;
    i = 1;
    KEEP_ALIVE_TIME = i;
    Object localObject2 = TimeUnit.SECONDS;
    KEEP_ALIVE_TIME_UNIT = ((TimeUnit)localObject2);
    mExecuted = false;
    AtomicInteger localAtomicInteger = new java/util/concurrent/atomic/AtomicInteger;
    localAtomicInteger.<init>(0);
    parity = localAtomicInteger;
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    bufferList = ((List)localObject2);
    localObject2 = new java/util/concurrent/LinkedBlockingQueue;
    ((LinkedBlockingQueue)localObject2).<init>(10);
    mTaskBlockingQueue = ((BlockingQueue)localObject2);
    localObject2 = new java/util/concurrent/ThreadPoolExecutor;
    int j = NUMBER_OF_CORES;
    int k;
    if (j > i)
    {
      j -= i;
      k = j;
    }
    else
    {
      k = 1;
    }
    int m = NUMBER_OF_CORES * 2 + -1;
    long l = KEEP_ALIVE_TIME;
    TimeUnit localTimeUnit = KEEP_ALIVE_TIME_UNIT;
    BlockingQueue localBlockingQueue = mTaskBlockingQueue;
    TaskManager.BackgroundThreadFactory localBackgroundThreadFactory = new com/twelfthmile/malana/controller/task/TaskManager$BackgroundThreadFactory;
    localBackgroundThreadFactory.<init>(null);
    ThreadPoolExecutor.CallerRunsPolicy localCallerRunsPolicy = new java/util/concurrent/ThreadPoolExecutor$CallerRunsPolicy;
    localCallerRunsPolicy.<init>();
    ((ThreadPoolExecutor)localObject2).<init>(k, m, l, localTimeUnit, localBlockingQueue, localBackgroundThreadFactory, localCallerRunsPolicy);
    mExecutorPool = ((ExecutorService)localObject2);
    localObject1 = new java/util/concurrent/ExecutorCompletionService;
    localObject2 = mExecutorPool;
    ((ExecutorCompletionService)localObject1).<init>((Executor)localObject2);
    mCompletionService = ((CompletionService)localObject1);
    localObject1 = Executors.newSingleThreadExecutor();
    mExecutorLooper = ((ExecutorService)localObject1);
    localObject1 = mExecutorLooper;
    localObject2 = new com/twelfthmile/malana/controller/task/TaskManager$1;
    ((TaskManager.1)localObject2).<init>(this, paramTaskCallback);
    ((ExecutorService)localObject1).submit((Runnable)localObject2);
  }
  
  public void execute(Callable paramCallable)
  {
    BlockingQueue localBlockingQueue = mTaskBlockingQueue;
    float f1 = localBlockingQueue.size();
    float f2 = 8.0F;
    boolean bool = f1 < f2;
    if (!bool)
    {
      bufferList.add(paramCallable);
      return;
    }
    mExecuted = true;
    mCompletionService.submit(paramCallable);
    parity.incrementAndGet();
  }
  
  public void finish()
  {
    Object localObject1 = bufferList.iterator();
    Object localObject3;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (Callable)((Iterator)localObject1).next();
      bool2 = true;
      mExecuted = bool2;
      localObject3 = mCompletionService;
      ((CompletionService)localObject3).submit((Callable)localObject2);
      localObject2 = parity;
      ((AtomicInteger)localObject2).incrementAndGet();
    }
    bufferList.clear();
    localObject1 = mExecutorPool;
    ((ExecutorService)localObject1).shutdown();
    long l = 10;
    try
    {
      localObject3 = mExecutorPool;
      localTimeUnit = TimeUnit.SECONDS;
      ((ExecutorService)localObject3).awaitTermination(l, localTimeUnit);
    }
    catch (Exception localException1)
    {
      TimeUnit localTimeUnit;
      for (;;) {}
    }
    boolean bool2 = mExecuted;
    if (bool2)
    {
      localObject3 = mExecutorLooper;
      ((ExecutorService)localObject3).shutdown();
    }
    try
    {
      localObject3 = mExecutorLooper;
      localTimeUnit = TimeUnit.SECONDS;
      ((ExecutorService)localObject3).awaitTermination(l, localTimeUnit);
    }
    catch (Exception localException2)
    {
      for (;;) {}
    }
    localObject1 = mExecutorLooper;
    ((ExecutorService)localObject1).shutdownNow();
    mTaskBlockingQueue.clear();
    mCompletionService = null;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.controller.task.TaskManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */