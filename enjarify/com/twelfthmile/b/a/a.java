package com.twelfthmile.b.a;

import java.util.Map;
import java.util.Set;

public abstract interface a
{
  public abstract void add(int paramInt1, int paramInt2);
  
  public abstract boolean containsKey(String paramString);
  
  public abstract boolean containsValue(String paramString);
  
  public abstract Set entrySet();
  
  public abstract int get(int paramInt);
  
  public abstract String get(String paramString);
  
  public abstract Map getAll();
  
  public abstract void initList(int paramInt);
  
  public abstract Set keySet();
  
  public abstract String put(String paramString1, String paramString2);
  
  public abstract void putAll(Map paramMap);
  
  public abstract String remove(String paramString);
  
  public abstract int size();
}

/* Location:
 * Qualified Name:     com.twelfthmile.b.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */