package com.twelfthmile.e;

import com.twelfthmile.e.a.d;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class a
{
  private static int a(String paramString, int paramInt)
  {
    int j;
    for (;;)
    {
      int i = paramString.length();
      j = -1;
      if (paramInt >= i) {
        break;
      }
      i = paramString.charAt(paramInt);
      int k = 46;
      if (i != k)
      {
        int m = 42;
        if (i != m)
        {
          m = 88;
          if (i != m)
          {
            m = 120;
            if (i != m)
            {
              boolean bool = com.twelfthmile.e.b.c.a(i);
              if (!bool) {
                return j;
              }
            }
          }
        }
        return paramInt;
      }
      paramInt += 1;
    }
    return j;
  }
  
  private static int a(String paramString, int paramInt, com.twelfthmile.e.b.b paramb, Map paramMap)
  {
    char c1 = paramString.charAt(paramInt);
    String str1 = paramString.substring(paramInt);
    int i = 46;
    boolean bool;
    Object localObject1;
    if (c1 == i)
    {
      if (paramInt == 0)
      {
        paramString = "YUGA_SOURCE_CONTEXT";
        bool = paramMap.containsKey(paramString);
        if (bool)
        {
          paramString = (String)paramMap.get("YUGA_SOURCE_CONTEXT");
          localObject1 = "YUGA_SC_CURR";
          bool = paramString.equals(localObject1);
          if (bool)
          {
            paramString = "AMT";
            localObject1 = "AMT";
            paramb.b(paramString, (String)localObject1);
          }
        }
      }
      paramb.a(c1);
      return 10;
    }
    i = 42;
    char c2 = 'X';
    int k;
    if ((c1 != i) && (c1 != c2))
    {
      i = 120;
      if (c1 != i)
      {
        i = 44;
        if (c1 == i) {
          return 12;
        }
        i = 37;
        k = -1;
        if (c1 != i)
        {
          char c3 = ' ';
          if (c1 == c3)
          {
            int m = paramInt + 1;
            int n = paramString.length();
            if (m < n)
            {
              m = paramString.charAt(m);
              if (m == i) {
                break label502;
              }
            }
          }
          i = 43;
          if (c1 == i)
          {
            paramString = "YUGA_SOURCE_CONTEXT";
            bool = paramMap.containsKey(paramString);
            if (bool)
            {
              paramString = (String)paramMap.get("YUGA_SOURCE_CONTEXT");
              localObject1 = "YUGA_SC_CURR";
              bool = paramString.equals(localObject1);
              if (bool) {
                return k;
              }
            }
            paramb.b("STR", "STR");
            return 36;
          }
          int i1 = 38;
          Object localObject2;
          String str2;
          if (paramInt > 0)
          {
            localObject2 = a.b.a;
            str2 = "FSA_AMT";
            localObject2 = com.twelfthmile.e.b.c.a((d)localObject2, str2, str1);
            if (localObject2 != null)
            {
              int j = ((Integer)a).intValue();
              paramb.a(j);
              paramb.b("AMT", "AMT");
              paramString = d((String)b);
              paramb.c(paramString);
              return i1;
            }
          }
          if (paramInt > 0)
          {
            localObject2 = a.b.a;
            str2 = "FSA_TIMES";
            localObject2 = com.twelfthmile.e.b.c.a((d)localObject2, str2, str1);
            if (localObject2 != null)
            {
              int i2 = ((Integer)a).intValue() + paramInt;
              paramb.a(i2);
              i = 0;
              str2 = null;
              paramb.b("TIME", null);
              i2 = 0;
              str1 = null;
              paramString = paramString.substring(0, paramInt);
              localObject1 = (String)b;
              localObject2 = "mins";
              paramInt = ((String)localObject1).equals(localObject2);
              if (paramInt != 0)
              {
                localObject1 = "00";
                paramString = String.valueOf(paramString);
                paramString = ((String)localObject1).concat(paramString);
              }
              localObject1 = a;
              paramb = new String[0];
              a(paramString, (Map)localObject1, paramb);
              return i1;
            }
          }
          return k;
        }
        label502:
        paramb.b("PCT", "PCT");
        return k;
      }
    }
    paramb.b("INSTRNO", "INSTRNO");
    paramb.a(k);
    return 11;
  }
  
  private static int a(String paramString, com.twelfthmile.e.b.b paramb)
  {
    int i = 1;
    int j = 1;
    int i1 = 0;
    String str1 = null;
    int i2;
    int i4;
    int n;
    for (;;)
    {
      i2 = 4;
      int i3 = -1;
      if (j <= 0) {
        break;
      }
      int i5 = paramString.length();
      if (i1 >= i5) {
        break;
      }
      i5 = paramString.charAt(i1);
      int i6 = 32;
      int m;
      switch (j)
      {
      default: 
        break;
      case 5: 
        j = i1 + 4;
        String str2 = paramString.substring(i1, j);
        i2 = i1 + 3;
        i3 = paramString.length();
        if (i2 < i3)
        {
          i4 = com.twelfthmile.e.b.c.a(str2);
          if (i4 != 0)
          {
            str1 = "yyyy";
            paramb.a(str1, str2);
            i1 = i2;
          }
        }
        j = -2;
        break;
      case 4: 
        if (i5 == i6) {
          j = 5;
        } else {
          j = -2;
        }
        break;
      case 3: 
        boolean bool2 = com.twelfthmile.e.b.c.a(i5);
        int k;
        if (bool2) {
          k = 4;
        } else {
          k = -1;
        }
        break;
      case 2: 
        boolean bool3 = com.twelfthmile.e.b.c.a(i5);
        if (bool3) {
          m = 3;
        } else {
          m = -1;
        }
        break;
      case 1: 
        if (i5 != i6)
        {
          m = 43;
          if (i5 != m)
          {
            boolean bool4 = com.twelfthmile.e.b.c.a(i5);
            if (!bool4)
            {
              n = 58;
              if (i5 == n)
              {
                n = 2;
                break;
              }
              n = -1;
              break;
            }
          }
        }
        n = 1;
      }
      i1 += i;
    }
    paramString = paramString.substring(0, i1).trim();
    if (n == i)
    {
      i = paramString.length();
      if (i == i2)
      {
        boolean bool1 = com.twelfthmile.e.b.c.a(paramString);
        if (bool1)
        {
          String str3 = "yyyy";
          paramb.a(str3, paramString);
        }
      }
    }
    if (n == i4) {
      return 0;
    }
    return i1;
  }
  
  public static com.twelfthmile.e.a.b a(String paramString)
  {
    Object localObject = b();
    paramString = c(paramString, (Map)localObject);
    com.twelfthmile.e.a.b localb = null;
    if (paramString == null) {
      return null;
    }
    com.twelfthmile.e.b.b localb1 = (com.twelfthmile.e.b.b)b;
    localObject = localb1.a((Map)localObject);
    if (localObject == null) {
      return null;
    }
    localb = new com/twelfthmile/e/a/b;
    paramString = (Integer)a;
    localb.<init>(paramString, localObject);
    return localb;
  }
  
  private static com.twelfthmile.e.a.b a(String paramString, com.twelfthmile.e.a.b paramb, Map paramMap)
  {
    Object localObject = (Integer)a;
    int i = ((Integer)localObject).intValue();
    com.twelfthmile.e.b.b localb = (com.twelfthmile.e.b.b)b;
    String str = localb.b();
    boolean bool1 = str.equals("DATE");
    if (bool1)
    {
      str = "MMM";
      bool1 = localb.a(str);
      if (bool1)
      {
        int j = localb.a();
        int k = 3;
        if (j < k)
        {
          paramb = new com/twelfthmile/e/a/b;
          paramString = paramString.substring(0, i);
          paramb.<init>("STR", paramString);
          return paramb;
        }
      }
      str = "HH";
      boolean bool2 = localb.a(str);
      if (bool2)
      {
        str = "mm";
        bool2 = localb.a(str);
        if (bool2)
        {
          str = "d";
          bool2 = localb.a(str);
          if (!bool2)
          {
            str = "dd";
            bool2 = localb.a(str);
            if (!bool2)
            {
              str = "MM";
              bool2 = localb.a(str);
              if (!bool2)
              {
                str = "MMM";
                bool2 = localb.a(str);
                if (!bool2)
                {
                  str = "yy";
                  bool2 = localb.a(str);
                  if (!bool2)
                  {
                    str = "yyyy";
                    bool2 = localb.a(str);
                    if (!bool2)
                    {
                      localb.b("TIME", null);
                      paramMap = new java/lang/StringBuilder;
                      paramMap.<init>();
                      str = localb.e("HH");
                      paramMap.append(str);
                      paramMap.append(":");
                      str = localb.e("mm");
                      paramMap.append(str);
                      paramMap = paramMap.toString();
                      localb.c("time", paramMap);
                      paramb = new com/twelfthmile/e/a/b;
                      paramString = paramString.substring(0, i);
                      paramb.<init>("TIME", paramString);
                      return paramb;
                    }
                  }
                }
              }
            }
          }
        }
      }
      paramMap = localb.a(paramMap);
      if (paramMap != null)
      {
        paramString = new com/twelfthmile/e/a/b;
        paramb = ((com.twelfthmile.e.b.b)b).b();
        paramString.<init>(paramb, paramMap);
        return paramString;
      }
      paramb = new com/twelfthmile/e/a/b;
      paramString = paramString.substring(0, i);
      paramb.<init>("STR", paramString);
      return paramb;
    }
    str = localb.b();
    str = localb.e(str);
    if (str != null)
    {
      paramString = localb.b();
      localObject = "INSTRNO";
      boolean bool3 = paramString.equals(localObject);
      if (bool3)
      {
        paramString = "YUGA_SOURCE_CONTEXT";
        bool3 = paramMap.containsKey(paramString);
        if (bool3)
        {
          paramString = (String)paramMap.get("YUGA_SOURCE_CONTEXT");
          paramMap = "YUGA_SC_CURR";
          bool3 = paramString.equals(paramMap);
          if (bool3)
          {
            paramString = new com/twelfthmile/e/a/b;
            paramMap = localb.b();
            paramMap = localb.e(paramMap).replaceAll("X", "");
            paramString.<init>("AMT", paramMap);
            return paramString;
          }
        }
      }
      paramString = new com/twelfthmile/e/a/b;
      paramb = ((com.twelfthmile.e.b.b)b).b();
      paramMap = localb.b();
      paramMap = localb.e(paramMap);
      paramString.<init>(paramb, paramMap);
      return paramString;
    }
    paramMap = new com/twelfthmile/e/a/b;
    paramb = ((com.twelfthmile.e.b.b)b).b();
    paramString = paramString.substring(0, i);
    paramMap.<init>(paramb, paramString);
    return paramMap;
  }
  
  public static com.twelfthmile.e.a.b a(String paramString1, String paramString2)
  {
    return com.twelfthmile.e.b.c.a(a.b.a, paramString1, paramString2);
  }
  
  public static com.twelfthmile.e.a.c a(String paramString, Map paramMap)
  {
    return b(paramString, paramMap);
  }
  
  public static void a() {}
  
  static void a(String paramString, com.twelfthmile.e.a.a parama)
  {
    String str1 = ",";
    paramString = paramString.split(str1);
    int i = paramString.length;
    int j = 0;
    while (j < i)
    {
      Object localObject1 = paramString[j];
      int k = ((String)localObject1).length();
      Object localObject2 = parama;
      int m = 0;
      String str2 = null;
      while (m < k)
      {
        char c = ((String)localObject1).charAt(m);
        int i1 = 1;
        b = i1;
        Object localObject3 = c;
        Character localCharacter = Character.valueOf(c);
        boolean bool = ((Map)localObject3).containsKey(localCharacter);
        if (!bool)
        {
          localObject3 = c;
          localCharacter = Character.valueOf(c);
          com.twelfthmile.e.a.a locala = new com/twelfthmile/e/a/a;
          locala.<init>();
          ((Map)localObject3).put(localCharacter, locala);
        }
        localObject2 = c;
        Object localObject4 = Character.valueOf(c);
        localObject2 = (com.twelfthmile.e.a.a)((Map)localObject2).get(localObject4);
        int n = k + -1;
        if (m == n)
        {
          a = i1;
          localObject3 = "";
          localObject4 = ((String)localObject1).replace(";", (CharSequence)localObject3);
          d = ((String)localObject4);
        }
        else if (m < n)
        {
          n = m + 1;
          int i2 = ((String)localObject1).charAt(n);
          int i3 = 59;
          if (i2 == i3)
          {
            a = i1;
            localObject3 = "";
            str2 = ((String)localObject1).replace(";", (CharSequence)localObject3);
            d = str2;
            m = n;
          }
        }
        m += i1;
      }
      j += 1;
    }
  }
  
  private static void a(String paramString, Map paramMap, String... paramVarArgs)
  {
    Object localObject1 = "";
    int i = paramVarArgs.length;
    if (i > 0)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      i = 0;
      paramVarArgs = paramVarArgs[0];
      ((StringBuilder)localObject1).append(paramVarArgs);
      paramVarArgs = "_";
      ((StringBuilder)localObject1).append(paramVarArgs);
      localObject1 = ((StringBuilder)localObject1).toString();
    }
    paramVarArgs = Pattern.compile("([0-9]{2})([0-9]{2})?([0-9]{2})?");
    paramString = paramVarArgs.matcher(paramString);
    boolean bool = paramString.find();
    if (bool)
    {
      paramVarArgs = new java/lang/StringBuilder;
      paramVarArgs.<init>();
      paramVarArgs.append((String)localObject1);
      paramVarArgs.append("time");
      paramVarArgs = paramVarArgs.toString();
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      i = 1;
      Object localObject2 = paramString.group(i);
      ((StringBuilder)localObject1).append((String)localObject2);
      int j = paramString.groupCount();
      if (j > i)
      {
        i = 2;
        localObject2 = paramString.group(i);
        if (localObject2 != null)
        {
          localObject2 = new java/lang/StringBuilder;
          String str = ":";
          ((StringBuilder)localObject2).<init>(str);
          paramString = paramString.group(i);
          ((StringBuilder)localObject2).append(paramString);
          paramString = ((StringBuilder)localObject2).toString();
          break label194;
        }
      }
      paramString = ":00";
      label194:
      ((StringBuilder)localObject1).append(paramString);
      paramString = ((StringBuilder)localObject1).toString();
      paramMap.put(paramVarArgs, paramString);
    }
  }
  
  public static com.twelfthmile.e.a.c b(String paramString)
  {
    Map localMap = b();
    return b(paramString, localMap);
  }
  
  private static com.twelfthmile.e.a.c b(String paramString, Map paramMap)
  {
    com.twelfthmile.e.a.b localb = c(paramString, paramMap);
    if (localb == null) {
      return null;
    }
    paramString = a(paramString, localb, paramMap);
    paramMap = new com/twelfthmile/e/a/c;
    String str = (String)a;
    Map localMap = b).a;
    paramString = b;
    int i = ((Integer)a).intValue();
    paramMap.<init>(str, localMap, paramString, i);
    return paramMap;
  }
  
  private static Map b()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject = com.twelfthmile.e.b.a.a();
    Date localDate = new java/util/Date;
    localDate.<init>();
    localObject = ((SimpleDateFormat)localObject).format(localDate);
    localHashMap.put("YUGA_CONF_DATE", localObject);
    return localHashMap;
  }
  
  private static int c(String paramString)
  {
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int k = 32;
      if (j != k)
      {
        j = paramString.charAt(i);
        k = 44;
        if (j != k)
        {
          j = paramString.charAt(i);
          k = 40;
          if (j != k)
          {
            j = paramString.charAt(i);
            k = 58;
            if (j != k) {
              break;
            }
          }
        }
      }
      i += 1;
    }
    return i;
  }
  
  private static com.twelfthmile.e.a.b c(String paramString, Map paramMap)
  {
    Object localObject1 = paramMap;
    com.twelfthmile.e.b.b localb = new com/twelfthmile/e/b/b;
    localb.<init>();
    Object localObject2 = new com/twelfthmile/e/a$a;
    ((a.a)localObject2).<init>();
    Object localObject3 = paramString.toLowerCase();
    int i = 1;
    int j = 1;
    int i19 = 0;
    String str1 = null;
    int i21 = 0;
    String str2 = null;
    int i24;
    int i25;
    Object localObject5;
    label585:
    label4003:
    label4639:
    label4815:
    label5011:
    label5674:
    label6094:
    int i116;
    int i16;
    label7055:
    int i20;
    for (;;)
    {
      boolean bool21 = false;
      String str3 = null;
      i24 = 12;
      i25 = 32;
      if (j <= 0) {
        break;
      }
      int i26 = ((String)localObject3).length();
      if (i19 >= i26) {
        break;
      }
      int i27 = ((String)localObject3).charAt(i19);
      int i28 = 42;
      int i35 = 46;
      int i36 = 45;
      int i117 = 44;
      boolean bool24;
      int i37;
      boolean bool23;
      int i42;
      String str4;
      int k;
      int i29;
      int m;
      boolean bool29;
      int i46;
      int n;
      int i47;
      boolean bool31;
      boolean bool33;
      int i51;
      char c1;
      Object localObject6;
      int i4;
      boolean bool38;
      int i62;
      int i7;
      int i64;
      int i9;
      int i67;
      int i23;
      int i30;
      int i10;
      int i11;
      int i81;
      int i31;
      int i83;
      int i89;
      boolean bool16;
      switch (j)
      {
      case 23: 
      default: 
        break;
      case 44: 
        bool24 = com.twelfthmile.e.b.c.g(i27);
        if (!bool24)
        {
          bool24 = com.twelfthmile.e.b.c.a(i27);
          if ((!bool24) && (i27 != i35)) {}
        }
        else
        {
          localb.a(i27);
          j = 44;
        }
        break;
      case 43: 
        bool24 = com.twelfthmile.e.b.c.g(i27);
        if (!bool24)
        {
          bool24 = com.twelfthmile.e.b.c.a(i27);
          if (!bool24)
          {
            j = -1;
            break label7055;
          }
        }
        localObject4 = "VPD";
        localObject5 = "VPD";
        localb.b((String)localObject4, (String)localObject5);
        i37 = ((a.a)localObject2).a();
        localb.a(i37);
        localb.a(i27);
        j = 44;
        break;
      case 42: 
        bool23 = com.twelfthmile.e.b.c.a(i27);
        if (bool23)
        {
          localb.a(i27);
          break label7055;
        }
        if (i27 == i37)
        {
          int i38 = i19 + 1;
          j = ((String)localObject3).length();
          if (i38 < j)
          {
            boolean bool25 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i38));
            if (bool25)
            {
              int i39 = 39;
              j = 39;
              break label7055;
            }
          }
        }
        i19 += -1;
        break;
      case 41: 
        boolean bool26 = com.twelfthmile.e.b.c.a(i27);
        if (bool26)
        {
          localb.a(i27);
        }
        else
        {
          int i40;
          if (i27 == i25)
          {
            i40 = 41;
            j = 41;
          }
          else
          {
            i40 = i19 + -1;
            if (i40 > 0)
            {
              j = ((String)localObject3).charAt(i40);
              if (j == i25)
              {
                i19 += -2;
                break label585;
              }
            }
            i19 = i40;
            j = -1;
          }
        }
        break;
      case 40: 
        boolean bool27 = com.twelfthmile.e.b.c.a(i27);
        int i41;
        if (bool27)
        {
          localObject4 = "yy";
          localb.a((String)localObject4, i27);
          i41 = 20;
          j = 20;
        }
        else if ((i27 != i25) && (i27 != i117))
        {
          localObject4 = "DATE";
          localb.b((String)localObject4);
          i19 += -1;
        }
        else
        {
          i41 = 40;
          j = 40;
        }
        break;
      case 39: 
        i42 = com.twelfthmile.e.b.c.a(i27);
        if (i42 != 0)
        {
          localb.a(i27);
          break label7055;
        }
        localObject4 = "INSTRNO";
        localObject5 = "INSTRNO";
        localb.b((String)localObject4, (String)localObject5);
        break;
      case 38: 
        i19 = localb.c();
        break;
      case 37: 
        boolean bool1 = com.twelfthmile.e.b.c.a(i27);
        if (bool1)
        {
          str4 = "AMT";
          localb.b("AMT", str4);
          localObject5 = "AMT";
          localb.a((String)localObject5, i42);
          localb.a(i27);
          k = 12;
        }
        else if (i27 == i35)
        {
          localObject5 = "AMT";
          localb.a((String)localObject5, i42);
          localb.a(i27);
          k = 10;
        }
        else
        {
          k = -1;
        }
        break;
      case 36: 
        bool23 = com.twelfthmile.e.b.c.a(i27);
        if (bool23)
        {
          localb.a(i27);
          i21 += 1;
        }
        else
        {
          if (i27 == i35)
          {
            k = i19 + 1;
            i29 = ((String)localObject3).length();
            if (k < i29)
            {
              boolean bool2 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(k));
              if (bool2)
              {
                localb.a(i27);
                m = 10;
                break label7055;
              }
            }
          }
          if (i27 == i42)
          {
            int i43 = i19 + 1;
            m = ((String)localObject3).length();
            if (i43 < m)
            {
              boolean bool28 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i43));
              if (bool28)
              {
                ((a.a)localObject2).a(i27);
                localb.a(i27);
                int i44 = 16;
                m = 16;
                break label7055;
              }
            }
          }
          if (i21 != i24)
          {
            localObject4 = ((String)localObject3).substring(i, i19);
            bool29 = com.twelfthmile.e.b.c.a((String)localObject4);
            if (!bool29) {
              return null;
            }
          }
          localObject4 = "NUM";
          localObject5 = "NUM";
          localb.b((String)localObject4, (String)localObject5);
          m = -1;
        }
        break;
      case 35: 
        bool29 = com.twelfthmile.e.b.c.a(i27);
        int i45;
        if (bool29)
        {
          localObject4 = "d";
          localObject5 = "yyyy";
          localb.d((String)localObject4, (String)localObject5);
          localb.a(i27);
          i45 = 20;
          m = 20;
        }
        else if ((i27 != i25) && (i27 != i117))
        {
          localObject4 = "DATE";
          localb.b((String)localObject4);
          i19 += -1;
        }
        else
        {
          i45 = 40;
          m = 40;
        }
        break;
      case 34: 
        boolean bool30 = com.twelfthmile.e.b.c.a(i27);
        if (bool30)
        {
          localb.a(i27);
          i46 = 35;
          m = 35;
        }
        else if ((i27 != i25) && (i27 != i117))
        {
          localObject4 = "DATE";
          localb.b((String)localObject4);
          i19 += -1;
        }
        else
        {
          i46 = 35;
          m = 35;
        }
        break;
      case 33: 
        boolean bool3 = com.twelfthmile.e.b.c.a(i27);
        if (bool3)
        {
          localObject4 = "d";
          localb.a((String)localObject4, i27);
          i46 = 34;
          n = 34;
        }
        else if ((i27 != i25) && (i27 != i117) && (i27 != i46))
        {
          localObject4 = "DATE";
          localb.b((String)localObject4);
          i19 += -1;
        }
        else
        {
          i47 = 33;
          n = 33;
        }
        break;
      case 32: 
        localObject4 = a.b.a;
        localObject5 = "FSA_MONTHS";
        str4 = ((String)localObject3).substring(i19);
        localObject4 = com.twelfthmile.e.b.c.a((d)localObject4, (String)localObject5, str4);
        if (localObject4 != null)
        {
          localObject5 = "MMM";
          str4 = (String)b;
          localb.a((String)localObject5, str4);
          localObject4 = (Integer)a;
          i47 = ((Integer)localObject4).intValue();
          i19 += i47;
          n = 24;
        }
        else if ((i27 != i117) && (i27 != i25))
        {
          localObject4 = a.b.a;
          localObject5 = "FSA_DAYSFFX";
          str4 = ((String)localObject3).substring(i19);
          localObject4 = com.twelfthmile.e.b.c.a((d)localObject4, (String)localObject5, str4);
          if (localObject4 != null)
          {
            localObject4 = (Integer)a;
            int i48 = ((Integer)localObject4).intValue();
            i19 += i48;
            n = 32;
          }
          else
          {
            for (;;)
            {
              bool31 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i19));
              if (bool31) {
                break;
              }
              i19 += -1;
            }
          }
        }
        else
        {
          n = 32;
        }
        break;
      case 31: 
        bool31 = com.twelfthmile.e.b.c.a(i27);
        if (bool31)
        {
          localb.a(i27);
          n = 32;
        }
        else
        {
          localObject4 = a.b.a;
          localObject5 = "FSA_MONTHS";
          str4 = ((String)localObject3).substring(i19);
          localObject4 = com.twelfthmile.e.b.c.a((d)localObject4, (String)localObject5, str4);
          if (localObject4 != null)
          {
            localObject5 = "MMM";
            str4 = (String)b;
            localb.a((String)localObject5, str4);
            localObject4 = (Integer)a;
            int i49 = ((Integer)localObject4).intValue();
            i19 += i49;
            n = 24;
          }
          else if ((i27 != i117) && (i27 != i25))
          {
            i19 += -1;
          }
          else
          {
            n = 32;
          }
        }
        break;
      case 30: 
        int i50;
        if ((i27 != i117) && (i27 != i25))
        {
          boolean bool32 = com.twelfthmile.e.b.c.a(i27);
          if (bool32)
          {
            localObject4 = "d";
            localb.a((String)localObject4, i27);
            i50 = 31;
            n = 31;
            break label7055;
          }
          localObject4 = "DATE";
          localb.b((String)localObject4);
          i19 += -1;
        }
        else
        {
          i50 = 30;
          n = 30;
        }
        break;
      case 29: 
        bool33 = com.twelfthmile.e.b.c.a(i27);
        if (bool33) {
          localb.a(i27);
        } else {
          i19 += -1;
        }
        n = -1;
        break;
      case 28: 
        bool33 = com.twelfthmile.e.b.c.a(i27);
        if (bool33)
        {
          localObject4 = "d";
          localb.a((String)localObject4, i27);
          i51 = 29;
          n = 29;
          break label7055;
        }
        localObject4 = "STR";
        localObject5 = "STR";
        localb.b((String)localObject4, (String)localObject5);
        i19 += -2;
        break;
      case 27: 
        boolean bool4 = com.twelfthmile.e.b.c.b(i27);
        if (bool4)
        {
          ((a.a)localObject2).a(i27);
          i51 = 28;
          int i1 = 28;
        }
        else
        {
          boolean bool5 = com.twelfthmile.e.b.c.a(i27);
          int i52;
          if (bool5)
          {
            localObject5 = localb.b();
            str4 = "DATE";
            bool5 = ((String)localObject5).equals(str4);
            if (bool5)
            {
              localObject5 = "NUM";
              str4 = "NUM";
              localb.b((String)localObject5, str4);
            }
            localb.a(i27);
            int i2 = ((a.a)localObject2).a();
            i29 = 47;
            if (i2 != i29)
            {
              i2 = ((a.a)localObject2).a();
              if (i2 != i51) {}
            }
            else
            {
              i51 = i19 + 1;
              i2 = ((String)localObject3).length();
              if (i51 < i2)
              {
                boolean bool6 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i51));
                if (bool6)
                {
                  int i3 = i19 + 2;
                  i29 = ((String)localObject3).length();
                  if (i3 != i29)
                  {
                    boolean bool7 = com.twelfthmile.e.b.c.c(((String)localObject3).charAt(i3));
                    if (!bool7) {}
                  }
                  else
                  {
                    localObject5 = "TIMES";
                    str1 = "TIMES";
                    localb.b((String)localObject5, str1);
                    c1 = ((String)localObject3).charAt(i51);
                    localb.a(c1);
                    i19 = i51;
                    break label6094;
                  }
                }
              }
            }
            i51 = ((a.a)localObject2).a();
            if (i51 == i25)
            {
              i52 = 41;
              c1 = ')';
              break label7055;
            }
            c1 = '\f';
            break label7055;
          }
          else
          {
            if (i27 != i29)
            {
              i52 = 88;
              if (i27 != i52)
              {
                int i53 = 120;
                if (i27 != i53)
                {
                  localObject4 = "STR";
                  localObject5 = "STR";
                  localb.b((String)localObject4, (String)localObject5);
                  i19 += -1;
                  break label6094;
                }
              }
            }
            localObject4 = "INSTRNO";
            localObject5 = "INSTRNO";
            localb.b((String)localObject4, (String)localObject5);
            localb.a('X');
            int i54 = 11;
            c1 = '\013';
          }
        }
        break;
      case 26: 
        boolean bool34 = com.twelfthmile.e.b.c.a(i27);
        if (bool34)
        {
          localb.a(i27);
          int i55 = 27;
          c1 = '\033';
          break label7055;
        }
        localObject4 = "STR";
        localObject5 = "STR";
        localb.b((String)localObject4, (String)localObject5);
        i19 += -1;
        break;
      case 25: 
        boolean bool35 = com.twelfthmile.e.b.c.a(i27);
        int i56;
        if (bool35)
        {
          localObject5 = "yyyy";
          localb.b("DATE", (String)localObject5);
          localObject4 = "MM";
          localb.a((String)localObject4, i27);
          i56 = 26;
          c1 = '\032';
          break label7055;
        }
        if (i19 > 0)
        {
          localObject4 = a.b.a;
          localObject5 = "FSA_TIMES";
          str4 = ((String)localObject3).substring(i19);
          localObject4 = com.twelfthmile.e.b.c.a((d)localObject4, (String)localObject5, str4);
          if (localObject4 != null)
          {
            localb.b("TIME", null);
            str4 = ((String)localObject3).substring(0, i19);
            localObject5 = (String)b;
            str3 = "mins";
            boolean bool8 = ((String)localObject5).equals(str3);
            if (bool8)
            {
              localObject5 = "00";
              str4 = String.valueOf(str4);
              str4 = ((String)localObject5).concat(str4);
            }
            localObject5 = a;
            bool21 = false;
            str3 = null;
            localObject6 = new String[0];
            a(str4, (Map)localObject5, (String[])localObject6);
            localObject4 = (Integer)a;
            i56 = ((Integer)localObject4).intValue();
            i19 += i56;
            break label6094;
          }
        }
        i19 += -2;
        break;
      case 24: 
        boolean bool36 = com.twelfthmile.e.b.c.b(i27);
        if ((!bool36) && (i27 != i117))
        {
          bool36 = com.twelfthmile.e.b.c.a(i27);
          if (bool36)
          {
            localb.b(i27);
            i57 = 20;
            i4 = 20;
            break label7055;
          }
          int i57 = 39;
          if (i27 == i57)
          {
            int i58 = i19 + 1;
            i4 = ((String)localObject3).length();
            if (i58 < i4)
            {
              boolean bool37 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i58));
              if (bool37)
              {
                i4 = 24;
                break label7055;
              }
            }
          }
          int i59 = 124;
          if (i27 == i59)
          {
            i4 = 24;
            break label7055;
          }
          i19 += -1;
        }
        else
        {
          ((a.a)localObject2).a(i27);
          i4 = 24;
        }
        break;
      case 22: 
        bool38 = com.twelfthmile.e.b.c.a(i27);
        if (bool38)
        {
          localb.a(i27);
        }
        else
        {
          localObject4 = "yyyy";
          localb.d((String)localObject4);
          i19 += -1;
        }
        break;
      case 21: 
        bool38 = com.twelfthmile.e.b.c.a(i27);
        if (bool38)
        {
          localb.b(i27);
          i60 = 22;
          i4 = 22;
          break label7055;
        }
        int i60 = 58;
        if (i27 == i60)
        {
          localObject4 = "yy";
          localObject5 = "HH";
          localb.d((String)localObject4, (String)localObject5);
          i4 = 4;
          break label7055;
        }
        i19 += -1;
        break;
      case 20: 
        boolean bool39 = com.twelfthmile.e.b.c.a(i27);
        if (bool39)
        {
          localb.a(i27);
          i61 = 21;
          i4 = 21;
          break label7055;
        }
        int i61 = 58;
        if (i27 == i61)
        {
          localObject4 = "yy";
          localObject5 = "HH";
          localb.d((String)localObject4, (String)localObject5);
          i4 = 4;
          break label7055;
        }
        localObject4 = "yy";
        localb.d((String)localObject4);
        i19 += -1;
        break;
      case 19: 
        boolean bool40 = com.twelfthmile.e.b.c.a(i27);
        if (bool40)
        {
          localb.b(i27);
          i62 = 20;
          i4 = 20;
          break label7055;
        }
        i19 += -2;
        break;
      case 18: 
        boolean bool9 = com.twelfthmile.e.b.c.b(i27);
        if (bool9)
        {
          ((a.a)localObject2).a(i27);
          i62 = 19;
          int i5 = 19;
          break label7055;
        }
        boolean bool10 = com.twelfthmile.e.b.c.a(i27);
        if (bool10)
        {
          int i6 = ((a.a)localObject2).a();
          if (i6 == i117)
          {
            localObject4 = "NUM";
            localObject5 = "NUM";
            localb.b((String)localObject4, (String)localObject5);
            localb.a(i27);
            i6 = 12;
            break label7055;
          }
        }
        boolean bool11 = com.twelfthmile.e.b.c.a(i27);
        if (bool11)
        {
          i7 = ((a.a)localObject2).a();
          if (i7 == i62)
          {
            localObject4 = "NUM";
            localObject5 = "NUM";
            localb.b((String)localObject4, (String)localObject5);
            localb.a(i27);
            i7 = 42;
            break label7055;
          }
        }
        if (i27 == i117)
        {
          i62 = ((a.a)localObject2).a();
          if (i62 == i117)
          {
            localObject4 = "NUM";
            localObject5 = "NUM";
            localb.b((String)localObject4, (String)localObject5);
            i7 = 12;
            break label7055;
          }
        }
        if (i27 == i35)
        {
          i62 = ((a.a)localObject2).a();
          if (i62 == i117)
          {
            localObject4 = "NUM";
            localObject5 = "NUM";
            localb.b((String)localObject4, (String)localObject5);
            localb.a(i27);
            i7 = 10;
            break label7055;
          }
        }
        localObject4 = "STR";
        localObject5 = "STR";
        localb.b((String)localObject4, (String)localObject5);
        i19 += -1;
        break;
      case 17: 
        boolean bool41 = com.twelfthmile.e.b.c.a(i27);
        if (bool41)
        {
          localb.a(i27);
          int i63 = 18;
          i7 = 18;
          break label7055;
        }
        boolean bool42 = com.twelfthmile.e.b.c.b(i27);
        if (bool42)
        {
          ((a.a)localObject2).a(i27);
          i64 = 19;
          i7 = 19;
          break label7055;
        }
        if (i27 == i117)
        {
          i64 = ((a.a)localObject2).a();
          if (i64 == i117)
          {
            localObject4 = "NUM";
            localObject5 = "NUM";
            localb.b((String)localObject4, (String)localObject5);
            i7 = 12;
            break label7055;
          }
        }
        if (i27 == i35)
        {
          i64 = ((a.a)localObject2).a();
          if (i64 == i117)
          {
            localObject4 = "NUM";
            localObject5 = "NUM";
            localb.b((String)localObject4, (String)localObject5);
            localb.a(i27);
            i7 = 10;
            break label7055;
          }
        }
        localObject4 = "STR";
        localObject5 = "STR";
        localb.b((String)localObject4, (String)localObject5);
        i19 += -1;
        break;
      case 16: 
        boolean bool12 = com.twelfthmile.e.b.c.a(i27);
        int i8;
        if (bool12)
        {
          localb.b(i27);
          i64 = 17;
          i8 = 17;
        }
        else if ((i27 != i25) && (i27 != i117))
        {
          localObject5 = a.b.a;
          str4 = "FSA_MONTHS";
          String str5 = ((String)localObject3).substring(i19);
          localObject5 = com.twelfthmile.e.b.c.a((d)localObject5, str4, str5);
          if (localObject5 != null)
          {
            str4 = (String)b;
            localb.a("MMM", str4);
            localObject4 = (Integer)a;
            i64 = ((Integer)localObject4).intValue();
            i19 += i64;
            i8 = 24;
            break label7055;
          }
          if (i27 == i35)
          {
            localObject4 = "NUM";
            localObject5 = "NUM";
            localb.b((String)localObject4, (String)localObject5);
            localb.a(i27);
            i8 = 10;
            break label7055;
          }
          int i65;
          if (i19 > 0)
          {
            localObject5 = a.b.a;
            str4 = "FSA_TIMES";
            localObject6 = ((String)localObject3).substring(i19);
            localObject5 = com.twelfthmile.e.b.c.a((d)localObject5, str4, (String)localObject6);
            if (localObject5 != null)
            {
              localb.b("TIME", null);
              str4 = ((String)localObject3).substring(0, i19);
              localObject4 = (String)b;
              str3 = "mins";
              boolean bool43 = ((String)localObject4).equals(str3);
              if (!bool43)
              {
                localObject4 = (String)b;
                str3 = "minutes";
                bool43 = ((String)localObject4).equals(str3);
                if (!bool43) {}
              }
              else
              {
                localObject4 = "00";
                str4 = String.valueOf(str4);
                str4 = ((String)localObject4).concat(str4);
              }
              localObject4 = a;
              bool21 = false;
              str3 = null;
              localObject6 = new String[0];
              a(str4, (Map)localObject4, (String[])localObject6);
              localObject4 = (Integer)a;
              i65 = ((Integer)localObject4).intValue();
              i19 += i65;
              break label6094;
            }
          }
          i8 = ((a.a)localObject2).a();
          if ((i8 == i25) && (i27 == i65))
          {
            int i66 = i19 + 1;
            i8 = ((String)localObject3).length();
            if (i66 < i8)
            {
              boolean bool13 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i66));
              if (!bool13)
              {
                localObject5 = a.b.a;
                str4 = "FSA_MONTHS";
                localObject4 = ((String)localObject3).substring(i66);
                localObject4 = com.twelfthmile.e.b.c.a((d)localObject5, str4, (String)localObject4);
                if (localObject4 == null) {}
              }
              else
              {
                i66 = 16;
                i9 = 16;
                break label7055;
              }
            }
          }
          localObject4 = "NUM";
          localObject5 = "NUM";
          localb.b((String)localObject4, (String)localObject5);
          for (;;)
          {
            boolean bool44 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i19));
            if (bool44) {
              break;
            }
            i19 += -1;
          }
        }
        else
        {
          i67 = 16;
          i9 = 16;
        }
        break;
      case 15: 
        bool21 = com.twelfthmile.e.b.c.a(i27);
        if (bool21)
        {
          i21 += 1;
          localb.a(i27);
          break label7055;
        }
        if (i27 == i117)
        {
          i9 = 12;
          break label7055;
        }
        if (i27 == i35)
        {
          localb.a(i27);
          i9 = 10;
          break label7055;
        }
        if (i27 != i29)
        {
          i9 = 88;
          if (i27 != i9)
          {
            i9 = 120;
            if (i27 != i9) {
              break label4003;
            }
          }
        }
        i9 = i19 + 1;
        int i22 = ((String)localObject3).length();
        int i71;
        if (i9 < i22)
        {
          boolean bool22 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i9));
          if (!bool22)
          {
            i23 = ((String)localObject3).charAt(i9);
            if (i23 != i67)
            {
              int i68 = ((String)localObject3).charAt(i9);
              if (i68 != i29)
              {
                int i69 = ((String)localObject3).charAt(i9);
                i30 = 88;
                if (i69 != i30)
                {
                  int i70 = ((String)localObject3).charAt(i9);
                  i9 = 120;
                  if (i70 != i9) {
                    break label4003;
                  }
                }
              }
            }
          }
          localObject4 = "INSTRNO";
          localObject5 = "INSTRNO";
          localb.b((String)localObject4, (String)localObject5);
          localb.a('X');
          i71 = 11;
          i9 = 11;
          break label7055;
        }
        if (i27 == i25)
        {
          i71 = i19 + 2;
          i9 = ((String)localObject3).length();
          if (i71 < i9)
          {
            i9 = i19 + 1;
            boolean bool14 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i9));
            if (bool14)
            {
              boolean bool45 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i71));
              if (bool45)
              {
                i72 = 41;
                i10 = 41;
                break label7055;
              }
            }
          }
        }
        int i72 = 64;
        if (i27 == i72)
        {
          int i73 = 10;
          if (i19 == i73)
          {
            ((a.a)localObject2).a(i27);
            int i74 = 43;
            i10 = 43;
            break label7055;
          }
        }
        i19 += -1;
        break;
      case 14: 
        boolean bool46 = com.twelfthmile.e.b.c.a(i27);
        if (bool46)
        {
          localb.a(i27);
          break label7055;
        }
        int i75 = 37;
        if (i27 == i75)
        {
          localObject4 = "PCT";
          localObject5 = "PCT";
          localb.b((String)localObject4, (String)localObject5);
        }
        else
        {
          int i76 = 107;
          int i77;
          if (i27 == i76)
          {
            i77 = i19 + 1;
            i10 = ((String)localObject3).length();
            if (i77 < i10)
            {
              i10 = ((String)localObject3).charAt(i77);
              i30 = 109;
              if (i10 == i30)
              {
                localObject5 = "DST";
                str1 = "DST";
                localb.b((String)localObject5, str1);
                i19 = i77;
                break label6094;
              }
            }
          }
          if (i27 == i35)
          {
            i77 = i19 + 1;
            i10 = ((String)localObject3).length();
            if (i77 < i10)
            {
              boolean bool47 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i77));
              if (bool47)
              {
                localObject4 = localb.b();
                localObject4 = localb.e((String)localObject4);
                localObject5 = ".";
                boolean bool15 = ((String)localObject4).contains((CharSequence)localObject5);
                if (bool15)
                {
                  localObject5 = "\\.";
                  localObject4 = ((String)localObject4).split((String)localObject5);
                  i11 = localObject4.length;
                  i30 = 2;
                  if (i11 == i30)
                  {
                    localb.b("DATE");
                    i30 = 0;
                    str4 = null;
                    str3 = localObject4[0];
                    localb.a("d", str3);
                    localObject5 = "MM";
                    localObject4 = localObject4[i];
                    localb.a((String)localObject5, (String)localObject4);
                    int i78 = 19;
                    i11 = 19;
                    break label7055;
                  }
                }
              }
            }
          }
          i19 += -1;
        }
        break;
      case 13: 
        boolean bool48 = com.twelfthmile.e.b.c.a(i27);
        if (bool48)
        {
          localb.a(i27);
        }
        else
        {
          if (i27 != i30)
          {
            int i79 = 88;
            if (i27 == i79) {
              break label4639;
            }
            int i80 = 120;
            if (i27 != i80)
            {
              if (i27 == i35)
              {
                localObject4 = "YUGA_SOURCE_CONTEXT";
                boolean bool49 = ((Map)localObject1).containsKey(localObject4);
                if (bool49)
                {
                  localObject4 = (String)((Map)localObject1).get("YUGA_SOURCE_CONTEXT");
                  str4 = "YUGA_SC_CURR";
                  bool49 = ((String)localObject4).equals(str4);
                  if (bool49)
                  {
                    localb.b("AMT", "AMT");
                    localObject4 = "AMT";
                    localObject5 = localb.e("AMT");
                    str4 = "X";
                    str3 = "";
                    localObject5 = ((String)localObject5).replaceAll(str4, str3);
                    localb.a((String)localObject4, (String)localObject5);
                    localb.a(i27);
                    i11 = 10;
                    break label7055;
                  }
                }
              }
              if (i27 == i35)
              {
                i81 = a((String)localObject3, i19);
                if (i81 > 0)
                {
                  i19 = i81;
                  break label7055;
                }
              }
              i19 += -1;
              break label6094;
            }
          }
          i81 = 88;
          localb.a(i81);
        }
        break;
      case 12: 
        i31 = com.twelfthmile.e.b.c.a(i27);
        if (i31 != 0)
        {
          localObject4 = "AMT";
          str4 = "AMT";
          localb.b((String)localObject4, str4);
          localb.a(i27);
        }
        else if (i27 == i117)
        {
          i11 = 12;
        }
        else if (i27 == i35)
        {
          localb.a(i27);
          i11 = 10;
        }
        else
        {
          if (i27 == i81)
          {
            int i82 = i19 + 1;
            i11 = ((String)localObject3).length();
            if (i82 < i11)
            {
              boolean bool50 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i82));
              if (bool50)
              {
                i83 = 39;
                i11 = 39;
                break label7055;
              }
            }
          }
          i83 = i19 + -1;
          if (i83 > 0)
          {
            i11 = ((String)localObject3).charAt(i83);
            if (i11 == i117)
            {
              i19 += -2;
              break label4815;
            }
          }
          i19 = i83;
          i11 = -1;
        }
        break;
      case 11: 
        if (i27 != i31)
        {
          i23 = 88;
          if (i27 != i23)
          {
            i23 = 120;
            if (i27 != i23)
            {
              if (i27 == i83)
              {
                int i84 = 11;
                i11 = 11;
                break label7055;
              }
              boolean bool51 = com.twelfthmile.e.b.c.a(i27);
              int i85;
              if (bool51)
              {
                localb.a(i27);
                i85 = 13;
                i11 = 13;
                break label7055;
              }
              if (i27 == i25)
              {
                i85 = i19 + 1;
                i23 = ((String)localObject3).length();
                if (i85 < i23)
                {
                  i23 = ((String)localObject3).charAt(i85);
                  if (i23 != i31)
                  {
                    int i32 = ((String)localObject3).charAt(i85);
                    i23 = 88;
                    if (i32 != i23)
                    {
                      int i33 = ((String)localObject3).charAt(i85);
                      i23 = 120;
                      if (i33 != i23)
                      {
                        boolean bool52 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i85));
                        if (!bool52) {
                          break label5011;
                        }
                      }
                    }
                  }
                  i86 = 11;
                  i11 = 11;
                  break label7055;
                }
              }
              if (i27 == i35)
              {
                i86 = a((String)localObject3, i19);
                if (i86 > 0)
                {
                  i19 = i86;
                  break label7055;
                }
              }
              i19 += -1;
              break label6094;
            }
          }
        }
        int i86 = 88;
        localb.a(i86);
        break;
      case 10: 
        boolean bool53 = com.twelfthmile.e.b.c.a(i27);
        if (bool53)
        {
          localb.a(i27);
          localObject4 = "AMT";
          localObject5 = "AMT";
          localb.b((String)localObject4, (String)localObject5);
          int i87 = 14;
          i11 = 14;
          break label7055;
        }
        localb.d();
        i19 += -2;
        break;
      case 9: 
        boolean bool54 = com.twelfthmile.e.b.c.b(i27);
        if (bool54)
        {
          ((a.a)localObject2).a(i27);
          int i88 = 25;
          i11 = 25;
        }
        else
        {
          boolean bool55 = com.twelfthmile.e.b.c.a(i27);
          if (bool55)
          {
            localb.a(i27);
            i89 = 5;
            i11 = 15;
            i21 = 5;
          }
          else
          {
            i89 = a((String)localObject3, i19, localb, (Map)localObject1);
            i11 = -1;
            if (i89 == i11)
            {
              localObject5 = localb.b();
              str4 = "PCT";
              bool16 = ((String)localObject5).equals(str4);
              if (!bool16)
              {
                i19 += -1;
                bool16 = i89;
              }
            }
          }
        }
        break;
      }
      do
      {
        int i15;
        do
        {
          do
          {
            int i13;
            do
            {
              int i109;
              do
              {
                int i12;
                do
                {
                  bool16 = i89;
                  break label7055;
                  bool16 = com.twelfthmile.e.b.c.a(i27);
                  if (bool16)
                  {
                    localb.a(i27);
                    i89 = 9;
                    i12 = 9;
                    break label7055;
                  }
                  i12 = a((String)localObject3, i19, localb, (Map)localObject1);
                  if ((i27 != i25) && (i27 != i89)) {}
                  int i34;
                  int i91;
                  do
                  {
                    int i90;
                    do
                    {
                      i90 = -1;
                      break;
                      i90 = -1;
                      if (i12 != i90) {
                        break;
                      }
                      i90 = i19 + 1;
                      i34 = ((String)localObject3).length();
                    } while (i90 >= i34);
                    i91 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i90));
                  } while (i91 == 0);
                  i12 = 12;
                  break label7055;
                  if (i12 != i91) {
                    break label7055;
                  }
                  localObject4 = localb.b();
                  str4 = "PCT";
                  boolean bool56 = ((String)localObject4).equals(str4);
                  if (bool56) {
                    break label7055;
                  }
                  i19 += -1;
                  break label7055;
                  int i92 = 97;
                  if (i27 == i92)
                  {
                    int i93 = i19 + 1;
                    i12 = ((String)localObject3).length();
                    if (i93 < i12)
                    {
                      i12 = ((String)localObject3).charAt(i93);
                      i34 = 109;
                      if (i12 == i34)
                      {
                        localObject5 = localb.e("HH");
                        i12 = Integer.parseInt((String)localObject5);
                        if (i12 == i24)
                        {
                          localObject5 = "HH";
                          str1 = "0";
                          localb.a((String)localObject5, str1);
                        }
                        i19 = i93;
                        break label5674;
                      }
                    }
                  }
                  int i94 = 112;
                  if (i27 == i94)
                  {
                    int i95 = i19 + 1;
                    i12 = ((String)localObject3).length();
                    if (i95 < i12)
                    {
                      i12 = ((String)localObject3).charAt(i95);
                      i34 = 109;
                      if (i12 == i34)
                      {
                        localObject5 = localb.e("HH");
                        i12 = Integer.parseInt((String)localObject5);
                        if (i12 != i24)
                        {
                          str1 = "HH";
                          i12 += 12;
                          localObject5 = String.valueOf(i12);
                          localb.a(str1, (String)localObject5);
                        }
                        i19 = i95;
                        break label5674;
                      }
                    }
                  }
                  localObject4 = a.b.a;
                  localObject5 = "FSA_TIMES";
                  str4 = ((String)localObject3).substring(i19);
                  localObject4 = com.twelfthmile.e.b.c.a((d)localObject4, (String)localObject5, str4);
                  if (localObject4 != null)
                  {
                    localObject4 = (Integer)a;
                    int i96 = ((Integer)localObject4).intValue();
                    i19 += i96;
                  }
                  else
                  {
                    i19 += -2;
                  }
                  i12 = -1;
                  break label7055;
                  boolean bool57 = com.twelfthmile.e.b.c.a(i27);
                  if (bool57)
                  {
                    localb.b(i27);
                    i19 += 1;
                    int i97 = ((String)localObject3).length();
                    if (i19 < i97)
                    {
                      boolean bool58 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i19));
                      if (bool58)
                      {
                        char c2 = ((String)localObject3).charAt(i19);
                        localb.a(c2);
                      }
                    }
                  }
                  else
                  {
                    i12 = -1;
                    break label7055;
                    boolean bool59 = com.twelfthmile.e.b.c.a(i27);
                    if (bool59)
                    {
                      localb.a(i27);
                      i98 = 5;
                      i12 = 5;
                      break label7055;
                    }
                    int i98 = 58;
                    if (i27 == i98)
                    {
                      i99 = 6;
                      i12 = 6;
                      break label7055;
                    }
                    int i99 = 97;
                    if (i27 == i99)
                    {
                      int i100 = i19 + 1;
                      i12 = ((String)localObject3).length();
                      if (i100 < i12)
                      {
                        i12 = ((String)localObject3).charAt(i100);
                        i34 = 109;
                        if (i12 == i34)
                        {
                          i19 = i100;
                          break label6094;
                        }
                      }
                    }
                    int i101 = 112;
                    if (i27 == i101)
                    {
                      int i102 = i19 + 1;
                      i12 = ((String)localObject3).length();
                      if (i102 < i12)
                      {
                        i12 = ((String)localObject3).charAt(i102);
                        i34 = 109;
                        if (i12 == i34)
                        {
                          localObject5 = "HH";
                          str1 = String.valueOf(Integer.parseInt(localb.e("HH")) + i24);
                          localb.a((String)localObject5, str1);
                          i19 = i102;
                          break label6094;
                        }
                      }
                    }
                    localObject4 = a.b.a;
                    localObject5 = "FSA_TIMES";
                    str4 = ((String)localObject3).substring(i19);
                    localObject4 = com.twelfthmile.e.b.c.a((d)localObject4, (String)localObject5, str4);
                    if (localObject4 != null)
                    {
                      localObject4 = (Integer)a;
                      int i103 = ((Integer)localObject4).intValue();
                      i19 += i103;
                    }
                    else
                    {
                      int i104 = 7;
                      i12 = 7;
                      break label7055;
                      boolean bool60 = com.twelfthmile.e.b.c.a(i27);
                      if (bool60)
                      {
                        localb.b(i27);
                        int i105 = 5;
                        i12 = 5;
                        break label7055;
                      }
                      localObject4 = "MMM";
                      bool61 = localb.a((String)localObject4);
                      if (!bool61)
                      {
                        localObject4 = "NUM";
                        localObject5 = "NUM";
                        localb.b((String)localObject4, (String)localObject5);
                      }
                      i19 += -2;
                    }
                  }
                  i12 = -1;
                  break label7055;
                  boolean bool61 = com.twelfthmile.e.b.c.a(i27);
                  if (bool61)
                  {
                    localb.a(i27);
                    int i106 = 8;
                    i12 = 8;
                    break label7055;
                  }
                  boolean bool62 = com.twelfthmile.e.b.c.d(i27);
                  if (bool62)
                  {
                    ((a.a)localObject2).a(i27);
                    localObject4 = "DATE";
                    localObject5 = "HH";
                    localb.b((String)localObject4, (String)localObject5);
                    i12 = 4;
                    break label7055;
                  }
                  bool62 = com.twelfthmile.e.b.c.b(i27);
                  if ((bool62) || (i27 == i117)) {
                    break;
                  }
                  localObject4 = a.b.a;
                  localObject5 = "FSA_MONTHS";
                  str4 = ((String)localObject3).substring(i19);
                  localObject4 = com.twelfthmile.e.b.c.a((d)localObject4, (String)localObject5, str4);
                  if (localObject4 != null)
                  {
                    localb.b("DATE", "d");
                    localObject5 = "MMM";
                    str4 = (String)b;
                    localb.a((String)localObject5, str4);
                    localObject4 = (Integer)a;
                    int i107 = ((Integer)localObject4).intValue();
                    i19 += i107;
                    i12 = 24;
                    break label7055;
                  }
                  localObject4 = a.b.a;
                  localObject5 = "FSA_DAYSFFX";
                  str4 = ((String)localObject3).substring(i19);
                  localObject4 = com.twelfthmile.e.b.c.a((d)localObject4, (String)localObject5, str4);
                  if (localObject4 != null)
                  {
                    localObject5 = "DATE";
                    str4 = "d";
                    localb.b((String)localObject5, str4);
                    localObject4 = (Integer)a;
                    int i108 = ((Integer)localObject4).intValue();
                    i19 += i108;
                    i12 = 32;
                    break label7055;
                  }
                  i109 = a((String)localObject3, i19, localb, (Map)localObject1);
                  i12 = -1;
                } while (i109 != i12);
                localObject5 = localb.b();
                str4 = "PCT";
                bool17 = ((String)localObject5).equals(str4);
              } while (bool17);
              i19 += -1;
              boolean bool17 = i109;
              break label7055;
              ((a.a)localObject2).a(i27);
              localObject4 = "DATE";
              localObject5 = "d";
              localb.b((String)localObject4, (String)localObject5);
              int i110 = 16;
              i13 = 16;
              break label7055;
              boolean bool63 = com.twelfthmile.e.b.c.a(i27);
              if (bool63)
              {
                localb.a(i27);
                int i111 = 3;
                i13 = 3;
                break label7055;
              }
              boolean bool64 = com.twelfthmile.e.b.c.d(i27);
              if (bool64)
              {
                ((a.a)localObject2).a(i27);
                localObject4 = "DATE";
                localObject5 = "HH";
                localb.b((String)localObject4, (String)localObject5);
                i13 = 4;
                break label7055;
              }
              bool64 = com.twelfthmile.e.b.c.b(i27);
              if ((bool64) || (i27 == i117)) {
                break;
              }
              localObject4 = a.b.a;
              localObject5 = "FSA_MONTHS";
              str4 = ((String)localObject3).substring(i19);
              localObject4 = com.twelfthmile.e.b.c.a((d)localObject4, (String)localObject5, str4);
              if (localObject4 != null)
              {
                localb.b("DATE", "d");
                localObject5 = "MMM";
                str4 = (String)b;
                localb.a((String)localObject5, str4);
                localObject4 = (Integer)a;
                int i112 = ((Integer)localObject4).intValue();
                i19 += i112;
                i13 = 24;
                break label7055;
              }
              i113 = a((String)localObject3, i19, localb, (Map)localObject1);
              i13 = -1;
            } while (i113 != i13);
            localObject5 = localb.b();
            str4 = "PCT";
            bool18 = ((String)localObject5).equals(str4);
          } while (bool18);
          i19 += -1;
          boolean bool18 = i113;
          break;
          ((a.a)localObject2).a(i27);
          localObject4 = "DATE";
          localObject5 = "d";
          localb.b((String)localObject4, (String)localObject5);
          int i113 = 16;
          int i14 = 16;
          break;
          boolean bool19 = com.twelfthmile.e.b.c.a(i27);
          if (bool19)
          {
            localb.b("NUM", null);
            localObject4 = "NUM";
            localb.a((String)localObject4, i27);
            i15 = 2;
            break;
          }
          localObject5 = a.b.a;
          str4 = "FSA_MONTHS";
          localObject6 = ((String)localObject3).substring(i19);
          localObject5 = com.twelfthmile.e.b.c.a((d)localObject5, str4, (String)localObject6);
          int i114;
          if (localObject5 != null)
          {
            localb.b("DATE", null);
            str4 = (String)b;
            localb.a("MMM", str4);
            localObject4 = (Integer)a;
            i113 = ((Integer)localObject4).intValue();
            i19 += i113;
            i114 = 33;
            i15 = 33;
            break;
          }
          localObject5 = a.b.a;
          str4 = "FSA_DAYS";
          localObject6 = ((String)localObject3).substring(i19);
          localObject5 = com.twelfthmile.e.b.c.a((d)localObject5, str4, (String)localObject6);
          int i115;
          if (localObject5 != null)
          {
            localb.b("DATE", null);
            str4 = (String)b;
            localb.a("dd", str4);
            localObject4 = (Integer)a;
            i114 = ((Integer)localObject4).intValue();
            i19 += i114;
            i115 = 30;
            i15 = 30;
            break;
          }
          if (i27 == i115)
          {
            i116 = 37;
            i15 = 37;
            break;
          }
          i116 = a((String)localObject3, i19, localb, (Map)localObject1);
          localObject5 = localb.b();
          if (localObject5 == null) {
            return null;
          }
          i15 = -1;
        } while (i116 != i15);
        localObject5 = localb.b();
        str4 = "PCT";
        i16 = ((String)localObject5).equals(str4);
      } while (i16 != 0);
      i19 += -1;
      i16 = i116;
      i19 += i;
    }
    localObject2 = localb.b();
    if (localObject2 == null) {
      return null;
    }
    int i118 = 10;
    if (i16 == i118)
    {
      localb.d();
      i20 += -1;
    }
    else
    {
      int i119 = 36;
      if (i16 == i119)
      {
        if (i21 != i24)
        {
          localObject2 = ((String)localObject3).substring(i, i20);
          bool66 = com.twelfthmile.e.b.c.a((String)localObject2);
          if (!bool66) {
            return null;
          }
        }
        localObject2 = "NUM";
        localObject4 = "NUM";
        localb.b((String)localObject2, (String)localObject4);
      }
    }
    localObject2 = localb.b();
    Object localObject4 = "AMT";
    boolean bool66 = ((String)localObject2).equals(localObject4);
    if (bool66)
    {
      localObject2 = localb.b();
      bool66 = localb.a((String)localObject2);
      if (bool66)
      {
        localObject2 = localb.b();
        localObject2 = localb.e((String)localObject2);
        localObject4 = ".";
        bool66 = ((String)localObject2).contains((CharSequence)localObject4);
        if (bool66)
        {
          localObject2 = localb.b();
          localObject2 = localb.e((String)localObject2).split("\\.");
          localObject4 = null;
          localObject2 = localObject2[0];
          int i120 = ((String)localObject2).length();
          i116 = 8;
          if (i120 > i116) {}
        }
        else
        {
          localObject2 = localb.b();
          localObject2 = localb.e((String)localObject2);
          localObject4 = ".";
          boolean bool67 = ((String)localObject2).contains((CharSequence)localObject4);
          if (bool67) {
            break label7372;
          }
          localObject2 = localb.b();
          localObject2 = localb.e((String)localObject2);
          int i121 = ((String)localObject2).length();
          i116 = 8;
          if (i121 <= i116) {
            break label7372;
          }
        }
      }
      localObject2 = "NUM";
      localObject4 = "NUM";
      localb.b((String)localObject2, (String)localObject4);
    }
    label7372:
    localObject2 = localb.b();
    localObject4 = "NUM";
    boolean bool68 = ((String)localObject2).equals(localObject4);
    int i135;
    label7643:
    int i132;
    if (bool68)
    {
      int i122 = ((String)localObject3).length();
      if (i20 < i122)
      {
        boolean bool69 = Character.isAlphabetic(((String)localObject3).charAt(i20));
        if (bool69)
        {
          localObject2 = "YUGA_SOURCE_CONTEXT";
          boolean bool71 = ((Map)localObject1).containsKey(localObject2);
          if (!bool71)
          {
            for (;;)
            {
              int i129 = ((String)localObject3).length();
              if (i20 >= i129) {
                break;
              }
              i129 = ((String)localObject3).charAt(i20);
              if (i129 == i25) {
                break;
              }
              i20 += 1;
            }
            localObject1 = "STR";
            localObject2 = "STR";
            localb.b((String)localObject1, (String)localObject2);
            break label8724;
          }
        }
      }
      localObject1 = localb.e("NUM");
      if (localObject1 != null)
      {
        localObject1 = localb.e("NUM");
        int i130 = ((String)localObject1).length();
        int i123 = 10;
        if (i130 == i123)
        {
          localObject1 = localb.e("NUM");
          i123 = 0;
          localObject2 = null;
          i130 = ((String)localObject1).charAt(0);
          i135 = 57;
          if (i130 != i135)
          {
            localObject1 = localb.e("NUM");
            i130 = ((String)localObject1).charAt(0);
            i135 = 56;
            if (i130 != i135)
            {
              localObject1 = localb.e("NUM");
              i130 = ((String)localObject1).charAt(0);
              i123 = 55;
              if (i130 != i123) {
                break label7643;
              }
            }
          }
          localObject1 = "num_class";
          localObject2 = "PHN";
          localb.c((String)localObject1, (String)localObject2);
          break label8724;
        }
        localObject1 = localb.e("NUM");
        i130 = ((String)localObject1).length();
        if (i130 == i24)
        {
          localObject1 = localb.e("NUM");
          localObject2 = "91";
          boolean bool72 = ((String)localObject1).startsWith((String)localObject2);
          if (bool72)
          {
            localObject1 = "num_class";
            localObject2 = "PHN";
            localb.c((String)localObject1, (String)localObject2);
            break label8724;
          }
        }
        localObject1 = localb.e("NUM");
        int i131 = ((String)localObject1).length();
        i123 = 11;
        if (i131 == i123)
        {
          localObject1 = localb.e("NUM");
          localObject2 = "18";
          boolean bool73 = ((String)localObject1).startsWith((String)localObject2);
          if (bool73)
          {
            localObject1 = "num_class";
            localObject2 = "PHN";
            localb.c((String)localObject1, (String)localObject2);
            break label8724;
          }
        }
        localObject1 = localb.e("NUM");
        i132 = ((String)localObject1).length();
        i123 = 11;
        if (i132 == i123)
        {
          localObject1 = localb.e("NUM");
          localObject2 = null;
          i132 = ((String)localObject1).charAt(0);
          i123 = 48;
          if (i132 == i123)
          {
            localObject1 = "num_class";
            localObject2 = "PHN";
            localb.c((String)localObject1, (String)localObject2);
            break label8724;
          }
        }
        localObject1 = "num_class";
        localObject2 = "NUM";
        localb.c((String)localObject1, (String)localObject2);
      }
    }
    else
    {
      localObject2 = localb.b();
      localObject4 = "DATE";
      boolean bool70 = ((String)localObject2).equals(localObject4);
      Object localObject7;
      int i128;
      if (bool70)
      {
        int i124 = i20 + 1;
        i116 = ((String)localObject3).length();
        if (i124 < i116)
        {
          localObject2 = ((String)localObject3).substring(i20);
          int i125 = c((String)localObject2) + i20;
          localObject4 = ((String)localObject3).substring(i125);
          int i17 = ((String)localObject3).length();
          if (i125 >= i17) {
            break label8724;
          }
          boolean bool20 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i125));
          if (!bool20)
          {
            localObject5 = a.b.a;
            str2 = "FSA_MONTHS";
            localObject5 = com.twelfthmile.e.b.c.a((d)localObject5, str2, (String)localObject4);
            if (localObject5 == null)
            {
              localObject5 = a.b.a;
              str2 = "FSA_DAYS";
              localObject5 = com.twelfthmile.e.b.c.a((d)localObject5, str2, (String)localObject4);
              if (localObject5 == null)
              {
                localObject5 = a.b.a;
                str2 = "FSA_TIMEPRFX";
                localObject5 = com.twelfthmile.e.b.c.a((d)localObject5, str2, (String)localObject4);
                int i127;
                if (localObject5 != null)
                {
                  localObject4 = (Integer)a;
                  i116 = ((Integer)localObject4).intValue() + i125 + i;
                  localObject5 = (Integer)a;
                  int i18 = ((Integer)localObject5).intValue();
                  i125 = i125 + i18 + i;
                  localObject2 = ((String)localObject3).substring(i125);
                  i125 = c((String)localObject2);
                  i116 += i125;
                  int i126 = ((String)localObject3).length();
                  if (i116 >= i126) {
                    break label8724;
                  }
                  i127 = com.twelfthmile.e.b.c.a(((String)localObject3).charAt(i116));
                  if (i127 == 0)
                  {
                    localObject2 = a.b.a;
                    localObject7 = "FSA_DAYS";
                    localObject5 = ((String)localObject3).substring(i116);
                    localObject2 = com.twelfthmile.e.b.c.a((d)localObject2, (String)localObject7, (String)localObject5);
                    if (localObject2 == null) {
                      break label8724;
                    }
                  }
                  localObject2 = ((String)localObject3).substring(i116);
                  localObject1 = c((String)localObject2, (Map)localObject1);
                  if (localObject1 == null) {
                    break label8724;
                  }
                  localObject2 = ((com.twelfthmile.e.b.b)b).b();
                  localObject3 = "DATE";
                  i127 = ((String)localObject2).equals(localObject3);
                  if (i127 == 0) {
                    break label8724;
                  }
                  localObject2 = (com.twelfthmile.e.b.b)b;
                  localb.a((com.twelfthmile.e.b.b)localObject2);
                  localObject1 = (Integer)a;
                  i132 = ((Integer)localObject1).intValue();
                  i116 += i132;
                  i20 = i116;
                  break label8724;
                }
                localObject1 = a.b.a;
                localObject5 = "FSA_TZ";
                localObject1 = com.twelfthmile.e.b.c.a((d)localObject1, (String)localObject5, (String)localObject4);
                if (localObject1 != null)
                {
                  localObject4 = (Integer)a;
                  i116 = ((Integer)localObject4).intValue() + i127 + i;
                  localObject3 = ((String)localObject3).substring(i116);
                  i135 = a((String)localObject3, localb);
                  localObject1 = (Integer)a;
                  int i133 = ((Integer)localObject1).intValue();
                  i128 = i127 + i133 + i;
                  i20 = i128 + i135;
                  break label8724;
                }
                localObject1 = ((String)localObject4).toLowerCase();
                localObject3 = "pm";
                boolean bool74 = ((String)localObject1).startsWith((String)localObject3);
                if (!bool74)
                {
                  localObject1 = ((String)localObject4).toLowerCase();
                  localObject3 = "am";
                  bool74 = ((String)localObject1).startsWith((String)localObject3);
                  if (!bool74) {
                    break label8724;
                  }
                }
                i134 = 2;
                i128 += i134;
                i20 = i128;
                break label8724;
              }
            }
          }
          localObject1 = c((String)localObject4, (Map)localObject1);
          if (localObject1 == null) {
            break label8724;
          }
          localObject3 = ((com.twelfthmile.e.b.b)b).b();
          localObject4 = "DATE";
          boolean bool76 = ((String)localObject3).equals(localObject4);
          if (!bool76) {
            break label8724;
          }
          localObject3 = (com.twelfthmile.e.b.b)b;
          localb.a((com.twelfthmile.e.b.b)localObject3);
          localObject1 = (Integer)a;
          int i134 = ((Integer)localObject1).intValue();
          i20 = i128 + i134;
          break label8724;
        }
      }
      localObject1 = localb.b();
      localObject2 = "TIMES";
      boolean bool75 = ((String)localObject1).equals(localObject2);
      if (bool75)
      {
        localObject1 = localb.b();
        localObject1 = localb.e((String)localObject1);
        if (localObject1 != null)
        {
          i128 = ((String)localObject1).length();
          int i136 = 8;
          if (i128 == i136)
          {
            i128 = 0;
            localObject2 = null;
            i136 = ((String)localObject1).charAt(0);
            i116 = ((String)localObject1).charAt(i);
            boolean bool77 = com.twelfthmile.e.b.c.a(i136, i116);
            if (bool77)
            {
              int i137 = 4;
              char c3 = ((String)localObject1).charAt(i137);
              i = ((String)localObject1).charAt(5);
              boolean bool65 = com.twelfthmile.e.b.c.a(c3, i);
              if (bool65)
              {
                localObject2 = ((String)localObject1).substring(0, i137);
                localObject4 = a;
                localObject7 = new String[] { "dept" };
                a((String)localObject2, (Map)localObject4, (String[])localObject7);
                i128 = 8;
                localObject1 = ((String)localObject1).substring(i137, i128);
                localObject2 = a;
                localObject3 = new String[] { "arrv" };
                a((String)localObject1, (Map)localObject2, (String[])localObject3);
              }
            }
          }
        }
      }
    }
    label8724:
    localObject1 = new com/twelfthmile/e/a/b;
    localObject2 = Integer.valueOf(i20);
    ((com.twelfthmile.e.a.b)localObject1).<init>(localObject2, localb);
    return (com.twelfthmile.e.a.b)localObject1;
  }
  
  private static String d(String paramString)
  {
    int i = paramString.hashCode();
    int j = 107;
    String str;
    boolean bool;
    if (i != j)
    {
      j = 106894;
      if (i != j)
      {
        j = 3314066;
        if (i == j)
        {
          str = "lakh";
          bool = paramString.equals(str);
          if (bool)
          {
            bool = false;
            paramString = null;
            break label105;
          }
        }
      }
      else
      {
        str = "lac";
        bool = paramString.equals(str);
        if (bool)
        {
          bool = true;
          break label105;
        }
      }
    }
    else
    {
      str = "k";
      bool = paramString.equals(str);
      if (bool)
      {
        k = 2;
        break label105;
      }
    }
    int k = -1;
    switch (k)
    {
    default: 
      return "";
    case 2: 
      label105:
      return "000";
    }
    return "00000";
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */