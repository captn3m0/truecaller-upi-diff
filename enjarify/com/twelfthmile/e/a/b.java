package com.twelfthmile.e.a;

import java.util.Objects;

public final class b
{
  public final Object a;
  public final Object b;
  
  public b(Object paramObject1, Object paramObject2)
  {
    a = paramObject1;
    b = paramObject2;
  }
  
  public final Object a()
  {
    return a;
  }
  
  public final Object b()
  {
    return b;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof b;
    if (!bool2) {
      return false;
    }
    paramObject = (b)paramObject;
    Object localObject1 = a;
    Object localObject2 = a;
    bool2 = localObject1.equals(localObject2);
    if (bool2)
    {
      localObject1 = b;
      paramObject = b;
      boolean bool3 = localObject1.equals(paramObject);
      if (bool3) {
        return bool1;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    Object[] arrayOfObject = new Object[2];
    Object localObject = a;
    arrayOfObject[0] = localObject;
    localObject = b;
    arrayOfObject[1] = localObject;
    return Objects.hash(arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.e.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */