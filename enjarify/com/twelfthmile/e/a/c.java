package com.twelfthmile.e.a;

import com.twelfthmile.e.b.a;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

public class c
{
  private String a;
  private Map b;
  private String c;
  private int d;
  private Date e;
  
  private c() {}
  
  public c(String paramString, Map paramMap, Object paramObject, int paramInt)
  {
    a = paramString;
    b = paramMap;
    boolean bool = paramObject instanceof String;
    if (bool)
    {
      paramObject = (String)paramObject;
      c = ((String)paramObject);
    }
    else
    {
      paramObject = (Date)paramObject;
      e = ((Date)paramObject);
      paramString = a.a();
      paramMap = e;
      paramString = paramString.format(paramMap);
      c = paramString;
    }
    d = paramInt;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final void a(String paramString)
  {
    c = paramString;
  }
  
  public final String b()
  {
    return c;
  }
  
  public final int c()
  {
    return d;
  }
  
  public final Map d()
  {
    return b;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof c;
    if (!bool2) {
      return false;
    }
    paramObject = (c)paramObject;
    int i = d;
    int j = d;
    if (i == j)
    {
      Object localObject1 = a;
      Object localObject2 = a;
      boolean bool3 = ((String)localObject1).equals(localObject2);
      if (bool3)
      {
        localObject1 = b;
        localObject2 = b;
        bool3 = ((Map)localObject1).equals(localObject2);
        if (bool3)
        {
          localObject1 = c;
          paramObject = c;
          boolean bool4 = ((String)localObject1).equals(paramObject);
          if (bool4) {
            return bool1;
          }
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    Object[] arrayOfObject = new Object[4];
    Object localObject = a;
    arrayOfObject[0] = localObject;
    localObject = b;
    arrayOfObject[1] = localObject;
    localObject = c;
    arrayOfObject[2] = localObject;
    localObject = Integer.valueOf(d);
    arrayOfObject[3] = localObject;
    return Objects.hash(arrayOfObject);
  }
  
  public String toString()
  {
    StringJoiner localStringJoiner = new java/util/StringJoiner;
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    String str = c.class.getSimpleName();
    ((StringBuilder)localObject1).append(str);
    ((StringBuilder)localObject1).append("[");
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringJoiner.<init>(", ", (CharSequence)localObject1, "]");
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("type='");
    localObject1 = a;
    ((StringBuilder)localObject2).append((String)localObject1);
    ((StringBuilder)localObject2).append("'");
    localObject2 = ((StringBuilder)localObject2).toString();
    localStringJoiner = localStringJoiner.add((CharSequence)localObject2);
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("valMap=");
    localObject1 = b;
    ((StringBuilder)localObject2).append(localObject1);
    localObject2 = ((StringBuilder)localObject2).toString();
    localStringJoiner = localStringJoiner.add((CharSequence)localObject2);
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("str='");
    localObject1 = c;
    ((StringBuilder)localObject2).append((String)localObject1);
    ((StringBuilder)localObject2).append("'");
    localObject2 = ((StringBuilder)localObject2).toString();
    localStringJoiner = localStringJoiner.add((CharSequence)localObject2);
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("index=");
    int i = d;
    ((StringBuilder)localObject2).append(i);
    localObject2 = ((StringBuilder)localObject2).toString();
    localStringJoiner = localStringJoiner.add((CharSequence)localObject2);
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("date=");
    localObject1 = e;
    ((StringBuilder)localObject2).append(localObject1);
    localObject2 = ((StringBuilder)localObject2).toString();
    return localStringJoiner.add((CharSequence)localObject2).toString();
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.e.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */