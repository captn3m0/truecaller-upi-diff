package com.twelfthmile.e;

import com.twelfthmile.e.a.d;
import java.util.Map;

final class a$b
{
  static d a;
  
  static
  {
    d locald = new com/twelfthmile/e/a/d;
    locald.<init>();
    Map localMap = a;
    com.twelfthmile.e.a.a locala1 = new com/twelfthmile/e/a/a;
    locala1.<init>();
    localMap.put("FSA_MONTHS", locala1);
    localMap = a;
    locala1 = new com/twelfthmile/e/a/a;
    locala1.<init>();
    localMap.put("FSA_DAYS", locala1);
    localMap = a;
    locala1 = new com/twelfthmile/e/a/a;
    locala1.<init>();
    localMap.put("FSA_TIMEPRFX", locala1);
    localMap = a;
    locala1 = new com/twelfthmile/e/a/a;
    locala1.<init>();
    localMap.put("FSA_AMT", locala1);
    localMap = a;
    locala1 = new com/twelfthmile/e/a/a;
    locala1.<init>();
    localMap.put("FSA_TIMES", locala1);
    localMap = a;
    locala1 = new com/twelfthmile/e/a/a;
    locala1.<init>();
    localMap.put("FSA_TZ", locala1);
    localMap = a;
    locala1 = new com/twelfthmile/e/a/a;
    locala1.<init>();
    localMap.put("FSA_DAYSFFX", locala1);
    localMap = a;
    locala1 = new com/twelfthmile/e/a/a;
    locala1.<init>();
    localMap.put("FSA_UPI", locala1);
    com.twelfthmile.e.a.a locala2 = (com.twelfthmile.e.a.a)a.get("FSA_MONTHS");
    a.a("jan;uary,feb;ruary,mar;ch,apr;il,may,jun;e,jul;y,aug;ust,sep;t;ember,oct;ober,nov;ember,dec;ember", locala2);
    locala2 = (com.twelfthmile.e.a.a)a.get("FSA_DAYS");
    a.a("sun;day,mon;day,tue;sday,wed;nesday,thu;rsday,thur;sday,fri;day,sat;urday", locala2);
    locala2 = (com.twelfthmile.e.a.a)a.get("FSA_TIMEPRFX");
    a.a("at,on,before,by", locala2);
    locala2 = (com.twelfthmile.e.a.a)a.get("FSA_AMT");
    a.a("lac,lakh,k", locala2);
    locala2 = (com.twelfthmile.e.a.a)a.get("FSA_TIMES");
    a.a("hours,hrs,hr,mins,minutes", locala2);
    locala2 = (com.twelfthmile.e.a.a)a.get("FSA_TZ");
    a.a("gmt,ist", locala2);
    locala2 = (com.twelfthmile.e.a.a)a.get("FSA_DAYSFFX");
    a.a("st,nd,rd,th", locala2);
    locala2 = (com.twelfthmile.e.a.a)a.get("FSA_UPI");
    a.a("UPI,MMT,NEFT", locala2);
    a = locald;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.e.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */