package com.twelfthmile.e.b;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class b
{
  public final Map a;
  private final Map b;
  private String c;
  private List d;
  
  public b()
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = ((Map)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    a = ((Map)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    d = ((List)localObject);
  }
  
  private void f(String paramString)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = d.iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      String str = (String)((Iterator)localObject2).next();
      Map localMap = b;
      str = (String)localMap.remove(str);
      ((StringBuilder)localObject1).append(str);
    }
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    d = ((List)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    a(paramString, (String)localObject1);
  }
  
  public final int a()
  {
    return b.keySet().size();
  }
  
  public final Date a(Map paramMap)
  {
    b localb = this;
    Object localObject1 = paramMap;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    Object localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    try
    {
      Object localObject5 = b;
      localObject5 = ((Map)localObject5).entrySet();
      localObject5 = ((Set)localObject5).iterator();
      int i = 1;
      int j = 0;
      int k = 0;
      int m = 0;
      int i1;
      int i2;
      label281:
      int n;
      boolean bool4;
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject5).hasNext();
        i1 = 2;
        i2 = 3;
        if (!bool1) {
          break;
        }
        localObject6 = ((Iterator)localObject5).next();
        localObject6 = (Map.Entry)localObject6;
        Object localObject7 = ((Map.Entry)localObject6).getKey();
        localObject7 = (String)localObject7;
        localObject8 = "d";
        boolean bool2 = ((String)localObject7).equals(localObject8);
        if (!bool2)
        {
          localObject8 = "MM";
          bool2 = ((String)localObject7).equals(localObject8);
          if (!bool2)
          {
            localObject8 = "MMM";
            bool2 = ((String)localObject7).equals(localObject8);
            if (!bool2)
            {
              localObject8 = "yy";
              bool2 = ((String)localObject7).equals(localObject8);
              if (!bool2)
              {
                localObject8 = "yyyy";
                bool2 = ((String)localObject7).equals(localObject8);
                if (!bool2)
                {
                  localObject8 = "HH";
                  bool2 = ((String)localObject7).equals(localObject8);
                  if (!bool2)
                  {
                    localObject8 = "mm";
                    bool2 = ((String)localObject7).equals(localObject8);
                    if (!bool2)
                    {
                      localObject8 = "ss";
                      bool2 = ((String)localObject7).equals(localObject8);
                      if (!bool2)
                      {
                        bool2 = false;
                        localObject8 = null;
                        break label281;
                      }
                    }
                  }
                }
              }
            }
          }
        }
        bool2 = true;
        if (bool2)
        {
          ((StringBuilder)localObject2).append((String)localObject7);
          localObject8 = " ";
          ((StringBuilder)localObject2).append((String)localObject8);
          localObject8 = ((Map.Entry)localObject6).getValue();
          localObject8 = (String)localObject8;
          ((StringBuilder)localObject3).append((String)localObject8);
          localObject8 = " ";
          ((StringBuilder)localObject3).append((String)localObject8);
          n = ((String)localObject7).hashCode();
          int i3 = 100;
          if (n != i3)
          {
            i3 = 2464;
            if (n != i3)
            {
              i3 = 3872;
              if (n != i3)
              {
                i3 = 76461;
                boolean bool3;
                if (n != i3)
                {
                  i3 = 3724864;
                  if (n == i3)
                  {
                    localObject8 = "yyyy";
                    bool3 = ((String)localObject7).equals(localObject8);
                    if (bool3)
                    {
                      i2 = 1;
                      break label532;
                    }
                  }
                }
                else
                {
                  localObject8 = "MMM";
                  bool3 = ((String)localObject7).equals(localObject8);
                  if (bool3)
                  {
                    int i4 = 4;
                    i2 = 4;
                    break label532;
                  }
                }
              }
              else
              {
                localObject8 = "yy";
                bool4 = ((String)localObject7).equals(localObject8);
                if (bool4)
                {
                  i2 = 0;
                  break label532;
                }
              }
            }
            else
            {
              localObject8 = "MM";
              bool4 = ((String)localObject7).equals(localObject8);
              if (bool4) {
                break label532;
              }
            }
          }
          else
          {
            localObject8 = "d";
            bool4 = ((String)localObject7).equals(localObject8);
            if (bool4)
            {
              i2 = 2;
              break label532;
            }
          }
          i2 = -1;
          switch (i2)
          {
          default: 
            break;
          case 3: 
          case 4: 
            k = 1;
            break;
          case 2: 
            m = 1;
            break;
          case 0: 
          case 1: 
            label532:
            j = 1;
          }
        }
      }
      if (j == 0)
      {
        localObject8 = "YUGA_CONF_DATE";
        bool4 = ((Map)localObject1).containsKey(localObject8);
        if (bool4)
        {
          localObject8 = "yyyy ";
          ((StringBuilder)localObject2).append((String)localObject8);
          localObject8 = "YUGA_CONF_DATE";
          localObject8 = ((Map)localObject1).get(localObject8);
          localObject8 = (String)localObject8;
          localObject5 = "-";
          localObject8 = ((String)localObject8).split((String)localObject5);
          localObject8 = localObject8[0];
          ((StringBuilder)localObject3).append((String)localObject8);
          localObject8 = " ";
          ((StringBuilder)localObject3).append((String)localObject8);
          break label874;
        }
      }
      Object localObject8 = Calendar.getInstance();
      int i5 = ((Calendar)localObject8).get(i);
      localObject5 = b;
      Object localObject6 = "yy";
      boolean bool7 = ((Map)localObject5).containsKey(localObject6);
      int i8;
      if (bool7)
      {
        localObject5 = b;
        localObject6 = "yy";
        localObject5 = ((Map)localObject5).get(localObject6);
        localObject5 = (String)localObject5;
        i8 = Integer.parseInt((String)localObject5);
        if (i8 > 0)
        {
          i5 = i5 % 1000 + i2;
          if (i8 < i5) {}
        }
        else
        {
          localObject8 = "yy";
          ((ArrayList)localObject4).add(localObject8);
        }
      }
      else
      {
        localObject5 = b;
        localObject6 = "yyyy";
        localObject5 = ((Map)localObject5).get(localObject6);
        localObject5 = (String)localObject5;
        i8 = Integer.parseInt((String)localObject5);
        n = 2000;
        if (i8 > n)
        {
          i5 += i2;
          if (i8 < i5) {}
        }
        else
        {
          localObject8 = "yyyy";
          ((ArrayList)localObject4).add(localObject8);
        }
      }
      label874:
      if (k == 0)
      {
        localObject8 = "YUGA_CONF_DATE";
        bool5 = ((Map)localObject1).containsKey(localObject8);
        if (bool5)
        {
          localObject8 = "MM ";
          ((StringBuilder)localObject2).append((String)localObject8);
          localObject8 = "YUGA_CONF_DATE";
          localObject8 = ((Map)localObject1).get(localObject8);
          localObject8 = (String)localObject8;
          localObject5 = "-";
          localObject8 = ((String)localObject8).split((String)localObject5);
          localObject8 = localObject8[i];
          ((StringBuilder)localObject3).append((String)localObject8);
          localObject8 = " ";
          ((StringBuilder)localObject3).append((String)localObject8);
          break label1070;
        }
      }
      localObject8 = b;
      localObject5 = "MM";
      boolean bool5 = ((Map)localObject8).containsKey(localObject5);
      if (bool5)
      {
        localObject8 = b;
        localObject5 = "MM";
        localObject8 = ((Map)localObject8).get(localObject5);
        localObject8 = (String)localObject8;
        localObject8 = Integer.valueOf((String)localObject8);
        int i6 = ((Integer)localObject8).intValue();
        if (i6 >= 0)
        {
          i8 = 12;
          if (i6 <= i8) {}
        }
        else
        {
          localObject8 = "MM";
          ((ArrayList)localObject4).add(localObject8);
        }
      }
      label1070:
      if (m == 0)
      {
        localObject8 = "YUGA_CONF_DATE";
        boolean bool6 = ((Map)localObject1).containsKey(localObject8);
        if (bool6)
        {
          localObject8 = "dd ";
          ((StringBuilder)localObject2).append((String)localObject8);
          localObject8 = "YUGA_CONF_DATE";
          localObject1 = ((Map)localObject1).get(localObject8);
          localObject1 = (String)localObject1;
          localObject8 = "-";
          localObject1 = ((String)localObject1).split((String)localObject8);
          localObject1 = localObject1[i1];
          localObject8 = " ";
          localObject1 = ((String)localObject1).split((String)localObject8);
          localObject1 = localObject1[0];
          ((StringBuilder)localObject3).append((String)localObject1);
          localObject1 = " ";
          ((StringBuilder)localObject3).append((String)localObject1);
          break label1259;
        }
      }
      localObject1 = b;
      localObject8 = "d";
      boolean bool8 = ((Map)localObject1).containsKey(localObject8);
      if (bool8)
      {
        localObject1 = b;
        localObject8 = "d";
        localObject1 = ((Map)localObject1).get(localObject8);
        localObject1 = (String)localObject1;
        localObject1 = Integer.valueOf((String)localObject1);
        i9 = ((Integer)localObject1).intValue();
        if (i9 >= 0)
        {
          int i7 = 31;
          if (i9 <= i7) {}
        }
        else
        {
          localObject1 = "d";
          ((ArrayList)localObject4).add(localObject1);
        }
      }
      label1259:
      int i9 = ((ArrayList)localObject4).size();
      if (i9 > 0)
      {
        i9 = ((ArrayList)localObject4).size();
        if (i9 == i)
        {
          localObject1 = ((ArrayList)localObject4).get(0);
          localObject1 = (String)localObject1;
          localObject2 = "MM";
          boolean bool9 = ((String)localObject1).equals(localObject2);
          if ((bool9) && (m != 0) && (j != 0))
          {
            localObject1 = new java/text/SimpleDateFormat;
            localObject2 = new java/lang/StringBuilder;
            localObject3 = "d/MM/";
            ((StringBuilder)localObject2).<init>((String)localObject3);
            localObject3 = b;
            localObject4 = "yy";
            boolean bool10 = ((Map)localObject3).containsKey(localObject4);
            if (bool10) {
              localObject3 = "yy";
            } else {
              localObject3 = "yyyy";
            }
            ((StringBuilder)localObject2).append((String)localObject3);
            localObject2 = ((StringBuilder)localObject2).toString();
            ((SimpleDateFormat)localObject1).<init>((String)localObject2);
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            localObject3 = b;
            localObject4 = "MM";
            localObject3 = ((Map)localObject3).get(localObject4);
            localObject3 = (String)localObject3;
            ((StringBuilder)localObject2).append((String)localObject3);
            localObject3 = "/";
            ((StringBuilder)localObject2).append((String)localObject3);
            localObject3 = b;
            localObject4 = "d";
            localObject3 = ((Map)localObject3).get(localObject4);
            localObject3 = (String)localObject3;
            ((StringBuilder)localObject2).append((String)localObject3);
            localObject3 = "/";
            ((StringBuilder)localObject2).append((String)localObject3);
            localObject3 = b;
            localObject4 = "yy";
            bool10 = ((Map)localObject3).containsKey(localObject4);
            if (bool10)
            {
              localObject3 = b;
              localObject4 = "yy";
            }
            for (localObject3 = ((Map)localObject3).get(localObject4);; localObject3 = ((Map)localObject3).get(localObject4))
            {
              localObject3 = (String)localObject3;
              break;
              localObject3 = b;
              localObject4 = "yyyy";
            }
            ((StringBuilder)localObject2).append((String)localObject3);
            localObject2 = ((StringBuilder)localObject2).toString();
            return ((DateFormat)localObject1).parse((String)localObject2);
          }
        }
        return null;
      }
      localObject1 = new java/text/SimpleDateFormat;
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject4 = Locale.ENGLISH;
      ((SimpleDateFormat)localObject1).<init>((String)localObject2, (Locale)localObject4);
      localObject2 = ((StringBuilder)localObject3).toString();
      return ((DateFormat)localObject1).parse((String)localObject2);
    }
    catch (Exception localException) {}
    return null;
  }
  
  public final void a(char paramChar)
  {
    Object localObject = b;
    String str1 = c;
    localObject = (String)((Map)localObject).get(str1);
    str1 = c;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(paramChar);
    String str2 = localStringBuilder.toString();
    a(str1, str2);
  }
  
  public final void a(int paramInt)
  {
    Map localMap = b;
    String str = String.valueOf(paramInt);
    localMap.put("INDEX", str);
  }
  
  public final void a(b paramb)
  {
    Map localMap = b;
    paramb = b;
    localMap.putAll(paramb);
  }
  
  public final void a(String paramString, char paramChar)
  {
    Object localObject = d;
    boolean bool = ((List)localObject).contains(paramString);
    if (!bool)
    {
      localObject = d;
      ((List)localObject).add(paramString);
    }
    localObject = b;
    String str = Character.toString(paramChar);
    ((Map)localObject).put(paramString, str);
    c = paramString;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    List localList = d;
    boolean bool = localList.contains(paramString1);
    if (!bool)
    {
      localList = d;
      localList.add(paramString1);
    }
    b.put(paramString1, paramString2);
    c = paramString1;
  }
  
  public final boolean a(String paramString)
  {
    return b.containsKey(paramString);
  }
  
  public final String b()
  {
    return (String)b.get("TYP");
  }
  
  public final void b(char paramChar)
  {
    String str1 = c;
    int i = str1.hashCode();
    int j = 100;
    Object localObject1;
    boolean bool4;
    if (i != j)
    {
      j = 2304;
      if (i != j)
      {
        j = 2464;
        boolean bool3;
        if (i != j)
        {
          j = 3488;
          if (i != j)
          {
            j = 3872;
            if (i != j)
            {
              j = 76461;
              if (i == j)
              {
                localObject1 = "MMM";
                boolean bool1 = str1.equals(localObject1);
                if (bool1)
                {
                  int k = 4;
                  break label220;
                }
              }
            }
            else
            {
              localObject1 = "yy";
              boolean bool2 = str1.equals(localObject1);
              if (bool2)
              {
                int m = 5;
                break label220;
              }
            }
          }
          else
          {
            localObject1 = "mm";
            bool3 = str1.equals(localObject1);
            if (bool3)
            {
              bool3 = true;
              break label220;
            }
          }
        }
        else
        {
          localObject1 = "MM";
          bool3 = str1.equals(localObject1);
          if (bool3)
          {
            int n = 3;
            break label220;
          }
        }
      }
      else
      {
        localObject1 = "HH";
        bool4 = str1.equals(localObject1);
        if (bool4)
        {
          bool4 = false;
          str1 = null;
          break label220;
        }
      }
    }
    else
    {
      localObject1 = "d";
      bool4 = str1.equals(localObject1);
      if (bool4)
      {
        i1 = 2;
        break label220;
      }
    }
    int i1 = -1;
    switch (i1)
    {
    default: 
      break;
    case 5: 
      str1 = "yyyy";
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      Object localObject2 = b;
      String str2 = "yy";
      localObject2 = (String)((Map)localObject2).remove(str2);
      ((StringBuilder)localObject1).append((String)localObject2);
      ((StringBuilder)localObject1).append(paramChar);
      String str3 = ((StringBuilder)localObject1).toString();
      a(str1, str3);
      str3 = "yyyy";
      c = str3;
      break;
    case 3: 
    case 4: 
      a("yy", paramChar);
      c = "yy";
      return;
    case 2: 
      a("MM", paramChar);
      c = "MM";
      return;
    case 1: 
      a("ss", paramChar);
      c = "ss";
      return;
    case 0: 
      label220:
      a("mm", paramChar);
      c = "mm";
      return;
    }
  }
  
  public final void b(String paramString)
  {
    b.put("TYP", paramString);
  }
  
  public final void b(String paramString1, String paramString2)
  {
    Map localMap = b;
    String str = "TYP";
    localMap.put(str, paramString1);
    if (paramString2 != null) {
      f(paramString2);
    }
  }
  
  public final int c()
  {
    return Integer.parseInt((String)b.get("INDEX"));
  }
  
  public final void c(String paramString)
  {
    Object localObject = b;
    String str = c;
    localObject = (String)((Map)localObject).get(str);
    str = c;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(paramString);
    paramString = localStringBuilder.toString();
    a(str, paramString);
  }
  
  public final void c(String paramString1, String paramString2)
  {
    a.put(paramString1, paramString2);
  }
  
  public final void d()
  {
    Object localObject = b;
    String str = c;
    localObject = (String)((Map)localObject).get(str);
    str = c;
    int i = ((String)localObject).length() + -1;
    localObject = ((String)localObject).substring(0, i);
    a(str, (String)localObject);
  }
  
  public final void d(String paramString)
  {
    b.remove(paramString);
  }
  
  public final void d(String paramString1, String paramString2)
  {
    Object localObject1 = b;
    boolean bool = ((Map)localObject1).containsKey(paramString1);
    if (bool)
    {
      localObject1 = b;
      bool = ((Map)localObject1).containsKey(paramString2);
      if (!bool)
      {
        localObject1 = b;
        paramString1 = (String)((Map)localObject1).remove(paramString1);
        a(paramString2, paramString1);
      }
      else
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        Object localObject2 = (String)b.get(paramString2);
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = b;
        paramString1 = (String)((Map)localObject2).remove(paramString1);
        ((StringBuilder)localObject1).append(paramString1);
        paramString1 = ((StringBuilder)localObject1).toString();
        a(paramString2, paramString1);
      }
      c = paramString2;
    }
  }
  
  public final String e(String paramString)
  {
    return (String)b.get(paramString);
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.e.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */