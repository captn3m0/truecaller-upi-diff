package com.truecaller.callrecording;

public abstract interface CallRecorder
{
  public abstract String getOutputFile();
  
  public abstract CallRecorder.RecordingState getRecordingState();
  
  public abstract boolean isRecording();
  
  public abstract void prepare();
  
  public abstract void release();
  
  public abstract void reset();
  
  public abstract void setErrorListener(CallRecorder.a parama);
  
  public abstract void setOutputFile(String paramString);
  
  public abstract void start();
  
  public abstract void stop();
}

/* Location:
 * Qualified Name:     com.truecaller.callrecording.CallRecorder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */