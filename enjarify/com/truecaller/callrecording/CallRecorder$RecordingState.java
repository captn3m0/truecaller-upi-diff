package com.truecaller.callrecording;

public enum CallRecorder$RecordingState
{
  static
  {
    RecordingState[] arrayOfRecordingState = new RecordingState[6];
    RecordingState localRecordingState = new com/truecaller/callrecording/CallRecorder$RecordingState;
    localRecordingState.<init>("INITIALIZING", 0);
    INITIALIZING = localRecordingState;
    arrayOfRecordingState[0] = localRecordingState;
    localRecordingState = new com/truecaller/callrecording/CallRecorder$RecordingState;
    int i = 1;
    localRecordingState.<init>("READY", i);
    READY = localRecordingState;
    arrayOfRecordingState[i] = localRecordingState;
    localRecordingState = new com/truecaller/callrecording/CallRecorder$RecordingState;
    i = 2;
    localRecordingState.<init>("RECORDING", i);
    RECORDING = localRecordingState;
    arrayOfRecordingState[i] = localRecordingState;
    localRecordingState = new com/truecaller/callrecording/CallRecorder$RecordingState;
    i = 3;
    localRecordingState.<init>("ERROR", i);
    ERROR = localRecordingState;
    arrayOfRecordingState[i] = localRecordingState;
    localRecordingState = new com/truecaller/callrecording/CallRecorder$RecordingState;
    i = 4;
    localRecordingState.<init>("STOPPED", i);
    STOPPED = localRecordingState;
    arrayOfRecordingState[i] = localRecordingState;
    localRecordingState = new com/truecaller/callrecording/CallRecorder$RecordingState;
    i = 5;
    localRecordingState.<init>("PAUSED", i);
    PAUSED = localRecordingState;
    arrayOfRecordingState[i] = localRecordingState;
    $VALUES = arrayOfRecordingState;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callrecording.CallRecorder.RecordingState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */