package com.truecaller.callrecording;

import android.media.MediaRecorder;
import android.os.Environment;
import java.io.File;
import java.io.IOException;

public final class b
  implements CallRecorder
{
  private final MediaRecorder a;
  private String b;
  private CallRecorder.RecordingState c;
  private CallRecorder.a d;
  
  public b()
  {
    Object localObject = new android/media/MediaRecorder;
    ((MediaRecorder)localObject).<init>();
    a = ((MediaRecorder)localObject);
    localObject = CallRecorder.RecordingState.INITIALIZING;
    c = ((CallRecorder.RecordingState)localObject);
  }
  
  public final String getOutputFile()
  {
    return b;
  }
  
  public final CallRecorder.RecordingState getRecordingState()
  {
    return c;
  }
  
  public final boolean isRecording()
  {
    CallRecorder.RecordingState localRecordingState1 = c;
    CallRecorder.RecordingState localRecordingState2 = CallRecorder.RecordingState.RECORDING;
    return localRecordingState1 == localRecordingState2;
  }
  
  public final void prepare()
  {
    new String[1][0] = "prepare:: ";
    CallRecorder.RecordingState localRecordingState = CallRecorder.RecordingState.READY;
    c = localRecordingState;
  }
  
  public final void release() {}
  
  public final void reset() {}
  
  public final void setErrorListener(CallRecorder.a parama)
  {
    d = parama;
  }
  
  public final void setOutputFile(String paramString)
  {
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("setOutputFile() called with: filePath = [");
    ((StringBuilder)localObject2).append(paramString);
    ((StringBuilder)localObject2).append("]");
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    if (paramString != null)
    {
      localObject1 = ".m4a";
      localObject2 = ".3gp";
      paramString = paramString.replace((CharSequence)localObject1, (CharSequence)localObject2);
    }
    String[] arrayOfString = new String[i];
    localObject2 = String.valueOf(paramString);
    localObject1 = "setOutputFile:: New file: ".concat((String)localObject2);
    arrayOfString[0] = localObject1;
    b = paramString;
  }
  
  public final void start()
  {
    Object localObject1 = Environment.getExternalStorageState();
    Object localObject2 = "mounted";
    int i = ((String)localObject1).equals(localObject2);
    if (i != 0)
    {
      localObject1 = new java/io/File;
      localObject2 = b;
      ((File)localObject1).<init>((String)localObject2);
      localObject1 = ((File)localObject1).getParentFile();
      i = ((File)localObject1).exists();
      if (i == 0)
      {
        boolean bool = ((File)localObject1).mkdirs();
        if (!bool)
        {
          localObject1 = new java/io/IOException;
          ((IOException)localObject1).<init>("Path to file could not be created.");
          throw ((Throwable)localObject1);
        }
      }
      try
      {
        localObject1 = a;
        i = 1;
        ((MediaRecorder)localObject1).setAudioSource(i);
        localObject1 = a;
        ((MediaRecorder)localObject1).setOutputFormat(i);
        localObject1 = a;
        ((MediaRecorder)localObject1).setAudioEncoder(i);
        localObject1 = a;
        localObject2 = b;
        ((MediaRecorder)localObject1).setOutputFile((String)localObject2);
        localObject1 = a;
        ((MediaRecorder)localObject1).prepare();
        localObject1 = a;
        ((MediaRecorder)localObject1).start();
        localObject1 = CallRecorder.RecordingState.RECORDING;
        c = ((CallRecorder.RecordingState)localObject1);
        return;
      }
      catch (Exception localException)
      {
        d.onError(localException);
        return;
      }
    }
    localObject2 = new java/io/IOException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SD Card is not mounted.  It is ");
    localStringBuilder.append(localException);
    localStringBuilder.append(".");
    String str = localStringBuilder.toString();
    ((IOException)localObject2).<init>(str);
    throw ((Throwable)localObject2);
  }
  
  public final void stop()
  {
    new String[1][0] = "stop:: ";
    a.stop();
    a.release();
    CallRecorder.RecordingState localRecordingState = CallRecorder.RecordingState.STOPPED;
    c = localRecordingState;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callrecording.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */