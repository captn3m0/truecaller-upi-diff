package com.truecaller.filters;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final h a;
  private final Provider b;
  private final Provider c;
  
  private m(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramh;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static m a(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    m localm = new com/truecaller/filters/m;
    localm.<init>(paramh, paramProvider1, paramProvider2);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */