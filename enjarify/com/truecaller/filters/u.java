package com.truecaller.filters;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.truecaller.androidactors.ab;
import com.truecaller.androidactors.w;
import com.truecaller.common.c.b.b;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.content.TruecallerContract.Filters;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.filters.a.a;
import com.truecaller.filters.a.c;
import com.truecaller.filters.sync.FilterUploadWorker;
import com.truecaller.log.AssertionUtil;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class u
  implements s
{
  private final Context a;
  private final FilterManager b;
  
  u(Context paramContext, FilterManager paramFilterManager)
  {
    a = paramContext;
    b = paramFilterManager;
  }
  
  private static ContentValues a(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt1, int paramInt2, int paramInt3, TruecallerContract.Filters.EntityType paramEntityType)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    localContentValues.put("value", paramString1);
    localContentValues.put("label", paramString3);
    paramString3 = Integer.valueOf(paramInt1);
    localContentValues.put("rule", paramString3);
    paramString3 = Integer.valueOf(paramInt2);
    localContentValues.put("wildcard_type", paramString3);
    paramString3 = Integer.valueOf(paramInt3);
    localContentValues.put("sync_state", paramString3);
    localContentValues.put("tracking_type", paramString2);
    localContentValues.put("tracking_source", paramString4);
    paramString2 = Integer.valueOf(value);
    localContentValues.put("entity_type", paramString2);
    return localContentValues;
  }
  
  private static ContentValues a(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt, TruecallerContract.Filters.EntityType paramEntityType)
  {
    int i = NONEtype;
    return a(paramString1, paramString2, paramString3, paramString4, paramInt, i, 1, paramEntityType);
  }
  
  private void a(List paramList1, List paramList2, List paramList3, String paramString1, String paramString2, boolean paramBoolean, int paramInt, TruecallerContract.Filters.EntityType paramEntityType)
  {
    u localu = this;
    int i = paramList1.size();
    ContentValues[] arrayOfContentValues = new ContentValues[i];
    int j = 0;
    ContentResolver localContentResolver = null;
    for (;;)
    {
      int k = paramList1.size();
      if (j >= k) {
        break;
      }
      localObject1 = paramList1;
      String str = (String)paramList1.get(j);
      Object localObject2 = paramList2.get(j);
      Object localObject3 = localObject2;
      localObject3 = (String)localObject2;
      localObject2 = paramList3.get(j);
      Object localObject4 = localObject2;
      localObject4 = (String)localObject2;
      localObject2 = str;
      localObject2 = a(str, (String)localObject3, (String)localObject4, paramString2, paramInt, paramEntityType);
      arrayOfContentValues[j] = localObject2;
      localObject2 = b;
      localObject4 = localObject3;
      ((FilterManager)localObject2).a(str, (String)localObject3, paramString1, paramString2, paramBoolean);
      j += 1;
    }
    localContentResolver = a.getContentResolver();
    Object localObject1 = TruecallerContract.Filters.a();
    localContentResolver.bulkInsert((Uri)localObject1, arrayOfContentValues);
  }
  
  public final w a()
  {
    String[] arrayOfString = { "0", "2" };
    ContentResolver localContentResolver = a.getContentResolver();
    Uri localUri = TruecallerContract.Filters.a();
    Object localObject = localContentResolver.query(localUri, null, "rule=? AND sync_state!=?", arrayOfString, "_id DESC");
    c localc = new com/truecaller/filters/a/c;
    localc.<init>((Cursor)localObject);
    localObject = -..Lambda.l5vNz0Dnc5PqEuCdBo9UGFYxfYw.INSTANCE;
    return w.a(localc, (ab)localObject);
  }
  
  public final w a(CountryListDto.a parama, String paramString)
  {
    parama = c;
    TruecallerContract.Filters.EntityType localEntityType = TruecallerContract.Filters.EntityType.UNKNOWN;
    Object localObject1 = parama;
    localObject1 = a(parama, "COUNTRY_CODE", null, paramString, 0, localEntityType);
    Object localObject2 = Integer.valueOf(0);
    ((ContentValues)localObject1).put("sync_state", (Integer)localObject2);
    Object localObject3 = a.getContentResolver();
    localObject2 = TruecallerContract.Filters.a();
    ((ContentResolver)localObject3).insert((Uri)localObject2, (ContentValues)localObject1);
    localObject1 = b;
    localObject3 = parama;
    ((FilterManager)localObject1).a(parama, "COUNTRY_CODE", "block", paramString, false);
    FilterUploadWorker.b();
    return w.b(Boolean.TRUE);
  }
  
  public final w a(a parama, String paramString, boolean paramBoolean)
  {
    Object localObject1 = e;
    Object localObject2 = f;
    Object localObject3 = d;
    int i = b;
    int j = h.type;
    int k = c;
    Object localObject4 = TruecallerContract.Filters.EntityType.UNKNOWN;
    Object localObject5 = paramString;
    localObject1 = a((String)localObject1, (String)localObject2, (String)localObject3, paramString, i, j, k, (TruecallerContract.Filters.EntityType)localObject4);
    boolean bool = parama.b();
    int m = 1;
    if (bool)
    {
      localObject1 = a.getContentResolver();
      localObject2 = TruecallerContract.Filters.a();
      localObject3 = new String[m];
      str1 = String.valueOf(a);
      localObject3[0] = str1;
      ((ContentResolver)localObject1).delete((Uri)localObject2, "_id = ?", (String[])localObject3);
      FilterManager localFilterManager = b;
      localObject4 = e;
      localFilterManager.a((String)localObject4, "COUNTRY_CODE", "unblock", paramString, paramBoolean);
      return w.b(Boolean.TRUE);
    }
    bool = parama.a();
    if (!bool)
    {
      localObject5 = Integer.valueOf(m);
      ((ContentValues)localObject1).put("rule", (Integer)localObject5);
      localObject2 = "sync_state";
      localObject3 = Integer.valueOf(m);
      ((ContentValues)localObject1).put((String)localObject2, (Integer)localObject3);
    }
    else
    {
      localObject2 = "sync_state";
      m = 2;
      localObject3 = Integer.valueOf(m);
      ((ContentValues)localObject1).put((String)localObject2, (Integer)localObject3);
    }
    localObject2 = a.getContentResolver();
    localObject3 = TruecallerContract.Filters.a();
    ((ContentResolver)localObject2).insert((Uri)localObject3, (ContentValues)localObject1);
    localObject5 = b;
    String str2 = e;
    String str1 = f;
    localObject4 = paramString;
    ((FilterManager)localObject5).a(str2, str1, "unblock", paramString, paramBoolean);
    FilterUploadWorker.b();
    return w.b(Boolean.TRUE);
  }
  
  public final w a(String paramString1, String paramString2, TruecallerContract.Filters.WildCardType paramWildCardType, String paramString3)
  {
    paramString1 = paramWildCardType.formatPattern(paramString1);
    try
    {
      Pattern.compile(paramString1);
      TruecallerContract.Filters.EntityType localEntityType = TruecallerContract.Filters.EntityType.UNKNOWN;
      Object localObject = paramString1;
      paramString2 = a(paramString1, "REG_EXP", paramString2, paramString3, 0, localEntityType);
      paramWildCardType = Integer.valueOf(type);
      paramString2.put("wildcard_type", paramWildCardType);
      paramWildCardType = a.getContentResolver();
      localObject = TruecallerContract.Filters.a();
      paramWildCardType.insert((Uri)localObject, paramString2);
      b.a(paramString1, "REG_EXP", "block", paramString3, false);
      FilterUploadWorker.b();
      return w.b(Boolean.TRUE);
    }
    catch (PatternSyntaxException paramString1)
    {
      paramString2 = new String[] { "Could not compile wildcard pattern" };
      AssertionUtil.shouldNeverHappen(paramString1, paramString2);
    }
    return w.b(Boolean.FALSE);
  }
  
  public final w a(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, TruecallerContract.Filters.EntityType paramEntityType)
  {
    Object localObject = paramString1;
    paramString3 = a(paramString1, paramString2, paramString3, paramString4, 0, paramEntityType);
    paramEntityType = a.getContentResolver();
    localObject = TruecallerContract.Filters.a();
    paramEntityType.insert((Uri)localObject, paramString3);
    FilterUploadWorker.b();
    b.a(paramString1, paramString2, "block", paramString4, paramBoolean);
    return w.b(Boolean.TRUE);
  }
  
  public final w a(List paramList1, List paramList2, List paramList3, String paramString1, String paramString2, boolean paramBoolean)
  {
    TruecallerContract.Filters.EntityType localEntityType = TruecallerContract.Filters.EntityType.UNKNOWN;
    a(paramList1, paramList2, paramList3, paramString1, paramString2, paramBoolean, 1, localEntityType);
    FilterUploadWorker.b();
    return w.b(Boolean.TRUE);
  }
  
  public final w a(List paramList1, List paramList2, List paramList3, String paramString, boolean paramBoolean, TruecallerContract.Filters.EntityType paramEntityType)
  {
    a(paramList1, paramList2, paramList3, "block", paramString, paramBoolean, 0, paramEntityType);
    FilterUploadWorker.b();
    return w.b(Boolean.TRUE);
  }
  
  public final w b()
  {
    String[] arrayOfString = { "0", "2" };
    ContentResolver localContentResolver = a.getContentResolver();
    Uri localUri = TruecallerContract.Filters.a();
    String str = "rule=? AND sync_state!=?";
    int i = b.a(localContentResolver, localUri, str, arrayOfString);
    int j = -1;
    if (i == j)
    {
      i = 0;
      arrayOfString = null;
    }
    return w.b(Integer.valueOf(i));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */