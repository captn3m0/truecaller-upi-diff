package com.truecaller.filters;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import c.d.f;
import c.g.b.k;
import com.truecaller.common.background.b;
import com.truecaller.common.f.c;
import com.truecaller.common.h.an;
import com.truecaller.common.h.q;
import com.truecaller.content.TruecallerContract.ao;
import com.truecaller.filters.sync.TopSpammer;
import com.truecaller.filters.sync.c.d;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import e.r;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class w
  implements v
{
  private final p a;
  private final ContentResolver b;
  private final f c;
  private final b d;
  private final com.truecaller.filters.sync.e e;
  private final com.truecaller.common.h.u f;
  private final an g;
  private final c h;
  
  public w(p paramp, ContentResolver paramContentResolver, f paramf, b paramb, com.truecaller.filters.sync.e parame, com.truecaller.common.h.u paramu, an paraman, c paramc)
  {
    a = paramp;
    b = paramContentResolver;
    c = paramf;
    d = paramb;
    e = parame;
    f = paramu;
    g = paraman;
    h = paramc;
  }
  
  private static TopSpammer a(Cursor paramCursor)
  {
    TopSpammer localTopSpammer;
    try
    {
      localTopSpammer = new com/truecaller/filters/sync/TopSpammer;
      String str1 = "label";
      int i = paramCursor.getColumnIndexOrThrow(str1);
      str1 = paramCursor.getString(i);
      String str2 = "value";
      int j = paramCursor.getColumnIndexOrThrow(str2);
      str2 = paramCursor.getString(j);
      String str3 = "count";
      int k = paramCursor.getColumnIndexOrThrow(str3);
      int m = paramCursor.getInt(k);
      paramCursor = Integer.valueOf(m);
      localTopSpammer.<init>(str2, str1, paramCursor);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      paramCursor = (Throwable)localIllegalAccessException;
      d.a(paramCursor, "could not read top spammer from db");
      localTopSpammer = null;
    }
    return localTopSpammer;
  }
  
  private static Collection a(Collection paramCollection)
  {
    paramCollection = (Iterable)paramCollection;
    Object localObject1 = new java/util/ArrayList;
    int i = c.a.m.a(paramCollection, 10);
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool1 = paramCollection.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (TopSpammer)paramCollection.next();
      ContentValues localContentValues = new android/content/ContentValues;
      localContentValues.<init>();
      String str1 = ((TopSpammer)localObject2).getValue();
      localContentValues.put("value", str1);
      str1 = ((TopSpammer)localObject2).getLabel();
      localContentValues.put("label", str1);
      String str2 = "count";
      localObject2 = ((TopSpammer)localObject2).getReports();
      localContentValues.put(str2, (Integer)localObject2);
      bool1 = TextUtils.isEmpty((CharSequence)localContentValues.getAsString("value"));
      boolean bool2 = true;
      bool1 ^= bool2;
      str1 = null;
      String[] arrayOfString = new String[0];
      AssertionUtil.isTrue(bool1, arrayOfString);
      localObject2 = localContentValues.getAsLong("count");
      long l1 = ((Long)localObject2).longValue();
      long l2 = 0L;
      bool1 = l1 < l2;
      if (bool1)
      {
        bool2 = false;
        str2 = null;
      }
      localObject2 = new String[0];
      AssertionUtil.isTrue(bool2, (String[])localObject2);
      ((Collection)localObject1).add(localContentValues);
    }
    return (Collection)localObject1;
  }
  
  private final void b(Collection paramCollection)
  {
    ContentResolver localContentResolver = b;
    Uri localUri = TruecallerContract.ao.a();
    if (paramCollection != null)
    {
      Object localObject = new ContentValues[0];
      localObject = paramCollection.toArray((Object[])localObject);
      if (localObject != null)
      {
        localObject = (ContentValues[])localObject;
        int i = localContentResolver.bulkInsert(localUri, (ContentValues[])localObject);
        int j = paramCollection.size();
        int k = 1;
        if (i == j)
        {
          j = 1;
        }
        else
        {
          j = 0;
          localUri = null;
        }
        localObject = new String[k];
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("Unexpected # of spammers added, got ");
        int m = paramCollection.size();
        localStringBuilder.append(m);
        localStringBuilder.append(", added ");
        localStringBuilder.append(i);
        paramCollection = localStringBuilder.toString();
        localObject[0] = paramCollection;
        AssertionUtil.OnlyInDebug.isTrue(j, (String[])localObject);
        return;
      }
      paramCollection = new c/u;
      paramCollection.<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw paramCollection;
    }
    paramCollection = new c/u;
    paramCollection.<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw paramCollection;
  }
  
  /* Error */
  public final TopSpammer a(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -12
    //   3: invokestatic 29	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_0
    //   7: getfield 51	com/truecaller/filters/w:b	Landroid/content/ContentResolver;
    //   10: astore_2
    //   11: invokestatic 197	com/truecaller/content/TruecallerContract$ao:a	()Landroid/net/Uri;
    //   14: astore_3
    //   15: ldc -10
    //   17: astore 4
    //   19: iconst_1
    //   20: anewarray 170	java/lang/String
    //   23: astore 5
    //   25: aload 5
    //   27: iconst_0
    //   28: aload_1
    //   29: aastore
    //   30: aload_2
    //   31: aload_3
    //   32: aconst_null
    //   33: aload 4
    //   35: aload 5
    //   37: aconst_null
    //   38: invokevirtual 250	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   41: astore_1
    //   42: aconst_null
    //   43: astore 6
    //   45: aload_1
    //   46: ifnull +69 -> 115
    //   49: aload_1
    //   50: checkcast 252	java/io/Closeable
    //   53: astore_1
    //   54: aload_1
    //   55: astore_2
    //   56: aload_1
    //   57: checkcast 69	android/database/Cursor
    //   60: astore_2
    //   61: aload_2
    //   62: invokeinterface 255 1 0
    //   67: istore 7
    //   69: iload 7
    //   71: ifeq +15 -> 86
    //   74: aload_2
    //   75: invokestatic 258	com/truecaller/filters/w:a	(Landroid/database/Cursor;)Lcom/truecaller/filters/sync/TopSpammer;
    //   78: astore_2
    //   79: aload_1
    //   80: aconst_null
    //   81: invokestatic 263	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   84: aload_2
    //   85: areturn
    //   86: getstatic 268	c/x:a	Lc/x;
    //   89: astore_2
    //   90: aload_1
    //   91: aconst_null
    //   92: invokestatic 263	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   95: goto +20 -> 115
    //   98: astore_2
    //   99: goto +8 -> 107
    //   102: astore 6
    //   104: aload 6
    //   106: athrow
    //   107: aload_1
    //   108: aload 6
    //   110: invokestatic 263	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   113: aload_2
    //   114: athrow
    //   115: aconst_null
    //   116: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	117	0	this	w
    //   0	117	1	paramString	String
    //   10	80	2	localObject1	Object
    //   98	16	2	localObject2	Object
    //   14	18	3	localUri	Uri
    //   17	17	4	str	String
    //   23	13	5	arrayOfString	String[]
    //   43	1	6	localObject3	Object
    //   102	7	6	localThrowable	Throwable
    //   67	3	7	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   104	107	98	finally
    //   56	60	102	finally
    //   61	67	102	finally
    //   74	78	102	finally
    //   86	89	102	finally
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString2, "number");
    paramString2 = f.b(paramString2);
    TopSpammer localTopSpammer = new com/truecaller/filters/sync/TopSpammer;
    Integer localInteger = Integer.valueOf(999);
    localTopSpammer.<init>(paramString2, paramString1, localInteger);
    paramString1 = a((Collection)c.a.m.a(localTopSpammer));
    b(paramString1);
  }
  
  public final boolean a()
  {
    Object localObject1 = h;
    boolean bool1 = ((c)localObject1).d();
    int i;
    if (bool1)
    {
      localObject1 = a;
      i = ((p)localObject1).m();
    }
    else
    {
      localObject1 = a;
      i = ((p)localObject1).l();
    }
    Object localObject2 = q.a(e.a(i, "caller"));
    int j = 0;
    if (localObject2 != null)
    {
      boolean bool2 = ((r)localObject2).d();
      int k;
      if (!bool2)
      {
        k = 0;
        localObject2 = null;
      }
      if (localObject2 != null)
      {
        localObject2 = (c.d)((r)localObject2).e();
        if (localObject2 != null)
        {
          localObject2 = a;
          if (localObject2 != null)
          {
            Object localObject3 = e;
            String str = "sms";
            localObject1 = q.a(((com.truecaller.filters.sync.e)localObject3).a(i, str));
            if (localObject1 != null)
            {
              bool2 = ((r)localObject1).d();
              if (!bool2)
              {
                i = 0;
                localObject1 = null;
              }
              if (localObject1 != null)
              {
                localObject1 = (c.d)((r)localObject1).e();
                if (localObject1 != null)
                {
                  localObject1 = a;
                  if (localObject1 != null)
                  {
                    localObject2 = (Collection)localObject2;
                    localObject1 = (Iterable)localObject1;
                    localObject1 = a((Collection)c.a.m.a((Iterable)c.a.m.c((Collection)localObject2, (Iterable)localObject1)));
                    localObject2 = b;
                    localObject3 = TruecallerContract.ao.a();
                    ((ContentResolver)localObject2).delete((Uri)localObject3, null, null);
                    b((Collection)localObject1);
                    localObject1 = a;
                    long l1 = ((p)localObject1).i();
                    long l2 = 0L;
                    boolean bool3 = true;
                    boolean bool4 = l1 < l2;
                    if (!bool4) {
                      j = 1;
                    }
                    localObject1 = a;
                    localObject2 = g;
                    l2 = ((an)localObject2).a();
                    ((p)localObject1).b(l2);
                    if (j != 0)
                    {
                      localObject1 = d;
                      k = 10004;
                      ((b)localObject1).a(k);
                    }
                    return bool3;
                  }
                }
              }
            }
            return false;
          }
        }
      }
    }
    return false;
  }
  
  public final void b()
  {
    ag localag = (ag)bg.a;
    f localf = c;
    Object localObject = new com/truecaller/filters/w$a;
    ((w.a)localObject).<init>(this, null);
    localObject = (c.g.a.m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (c.g.a.m)localObject, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */