package com.truecaller.filters;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.filters.a.a;
import java.util.List;

public final class t
  implements s
{
  private final v a;
  
  public t(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return s.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    t.f localf = new com/truecaller/filters/t$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, (byte)0);
    return w.a(localv, localf);
  }
  
  public final w a(CountryListDto.a parama, String paramString)
  {
    v localv = a;
    t.c localc = new com/truecaller/filters/t$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, parama, paramString, (byte)0);
    return w.a(localv, localc);
  }
  
  public final w a(a parama, String paramString, boolean paramBoolean)
  {
    v localv = a;
    t.h localh = new com/truecaller/filters/t$h;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localh.<init>(locale, parama, paramString, paramBoolean, (byte)0);
    return w.a(localv, localh);
  }
  
  public final w a(String paramString1, String paramString2, TruecallerContract.Filters.WildCardType paramWildCardType, String paramString3)
  {
    v localv = a;
    t.d locald = new com/truecaller/filters/t$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramString1, paramString2, paramWildCardType, paramString3, (byte)0);
    return w.a(localv, locald);
  }
  
  public final w a(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, TruecallerContract.Filters.EntityType paramEntityType)
  {
    v localv = a;
    t.a locala = new com/truecaller/filters/t$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramString1, paramString2, paramString3, paramString4, paramBoolean, paramEntityType, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w a(List paramList1, List paramList2, List paramList3, String paramString1, String paramString2, boolean paramBoolean)
  {
    v localv = a;
    t.g localg = new com/truecaller/filters/t$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, paramList1, paramList2, paramList3, paramString1, paramString2, paramBoolean, (byte)0);
    return w.a(localv, localg);
  }
  
  public final w a(List paramList1, List paramList2, List paramList3, String paramString, boolean paramBoolean, TruecallerContract.Filters.EntityType paramEntityType)
  {
    v localv = a;
    t.b localb = new com/truecaller/filters/t$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramList1, paramList2, paramList3, paramString, paramBoolean, paramEntityType, (byte)0);
    return w.a(localv, localb);
  }
  
  public final w b()
  {
    v localv = a;
    t.e locale = new com/truecaller/filters/t$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, (byte)0);
    return w.a(localv, locale);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */