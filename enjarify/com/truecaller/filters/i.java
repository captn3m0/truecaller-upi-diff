package com.truecaller.filters;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final h a;
  private final Provider b;
  private final Provider c;
  
  private i(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramh;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static i a(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    i locali = new com/truecaller/filters/i;
    locali.<init>(paramh, paramProvider1, paramProvider2);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */