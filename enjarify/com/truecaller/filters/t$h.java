package com.truecaller.filters;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.filters.a.a;

final class t$h
  extends u
{
  private final a b;
  private final String c;
  private final boolean d;
  
  private t$h(e parame, a parama, String paramString, boolean paramBoolean)
  {
    super(parame);
    b = parama;
    c = paramString;
    d = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".whitelistFilter(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = c;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(Boolean.valueOf(d), i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.t.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */