package com.truecaller.filters;

import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d
{
  private final h a;
  private final Provider b;
  private final Provider c;
  
  private n(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramh;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static n a(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    n localn = new com/truecaller/filters/n;
    localn.<init>(paramh, paramProvider1, paramProvider2);
    return localn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */