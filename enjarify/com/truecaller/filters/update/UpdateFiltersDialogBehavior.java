package com.truecaller.filters.update;

import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.view.MotionEvent;
import android.view.View;
import c.g.b.k;

public final class UpdateFiltersDialogBehavior
  extends BottomSheetBehavior
{
  public UpdateFiltersDialogBehavior()
  {
    a(-1 >>> 1);
  }
  
  public final void a(CoordinatorLayout paramCoordinatorLayout, View paramView1, View paramView2, int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
  {
    k.b(paramCoordinatorLayout, "coordinatorLayout");
    k.b(paramView2, "target");
    k.b(paramArrayOfInt, "consumed");
  }
  
  public final void a(CoordinatorLayout paramCoordinatorLayout, View paramView, int[] paramArrayOfInt)
  {
    k.b(paramCoordinatorLayout, "coordinatorLayout");
    k.b(paramView, "target");
    k.b(paramArrayOfInt, "consumed");
  }
  
  public final boolean a(CoordinatorLayout paramCoordinatorLayout, View paramView, MotionEvent paramMotionEvent)
  {
    k.b(paramCoordinatorLayout, "parent");
    k.b(paramMotionEvent, "event");
    return false;
  }
  
  public final boolean a(CoordinatorLayout paramCoordinatorLayout, View paramView1, View paramView2, float paramFloat1, float paramFloat2)
  {
    k.b(paramCoordinatorLayout, "coordinatorLayout");
    k.b(paramView2, "target");
    return false;
  }
  
  public final boolean a(CoordinatorLayout paramCoordinatorLayout, View paramView1, View paramView2, int paramInt)
  {
    k.b(paramCoordinatorLayout, "coordinatorLayout");
    k.b(paramView1, "directTargetChild");
    k.b(paramView2, "target");
    return false;
  }
  
  public final boolean a(CoordinatorLayout paramCoordinatorLayout, View paramView1, View paramView2, View paramView3, int paramInt1, int paramInt2)
  {
    k.b(paramCoordinatorLayout, "coordinatorLayout");
    k.b(paramView2, "directTargetChild");
    k.b(paramView3, "target");
    return false;
  }
  
  public final void b(CoordinatorLayout paramCoordinatorLayout, View paramView1, View paramView2, int paramInt)
  {
    k.b(paramCoordinatorLayout, "coordinatorLayout");
    k.b(paramView2, "target");
  }
  
  public final boolean b(CoordinatorLayout paramCoordinatorLayout, View paramView, MotionEvent paramMotionEvent)
  {
    k.b(paramCoordinatorLayout, "parent");
    k.b(paramMotionEvent, "event");
    return false;
  }
  
  public final void c(CoordinatorLayout paramCoordinatorLayout, View paramView)
  {
    k.b(paramCoordinatorLayout, "coordinatorLayout");
    k.b(paramView, "target");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.update.UpdateFiltersDialogBehavior
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */