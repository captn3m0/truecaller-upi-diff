package com.truecaller.filters.update;

import com.truecaller.ads.k;
import com.truecaller.ads.provider.f;
import com.truecaller.ads.provider.holders.e;
import com.truecaller.analytics.b;
import com.truecaller.filters.p;
import com.truecaller.filters.v;
import com.truecaller.util.af;
import com.truecaller.utils.n;

final class h
  extends g
  implements com.truecaller.ads.g
{
  private final p a;
  private final af c;
  private final com.truecaller.utils.i d;
  private final n e;
  private final f f;
  private final k g;
  private final b h;
  private final v i;
  private boolean j = false;
  private e k = null;
  
  h(p paramp, af paramaf, com.truecaller.utils.i parami, b paramb, n paramn, f paramf, k paramk, v paramv)
  {
    a = paramp;
    c = paramaf;
    d = parami;
    h = paramb;
    e = paramn;
    f = paramf;
    g = paramk;
    i = paramv;
  }
  
  private void e()
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = f;
      Object localObject2 = g;
      localObject1 = ((f)localObject1).a((k)localObject2, 0);
      if (localObject1 != null)
      {
        localObject2 = (i)b;
        ((i)localObject2).a((e)localObject1);
        k = ((e)localObject1);
      }
    }
  }
  
  public final void a()
  {
    boolean bool = j;
    if (bool)
    {
      e locale = k;
      if (locale == null) {
        e();
      }
    }
  }
  
  public final void a(int paramInt) {}
  
  public final void a(e parame, int paramInt) {}
  
  final void b()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = d;
      boolean bool = ((com.truecaller.utils.i)localObject).a();
      if (!bool)
      {
        ((i)b).c();
        return;
      }
      ((i)b).a();
      i.b();
      localObject = a;
      ((p)localObject).h(false);
    }
  }
  
  final void c()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (i)b;
      ((i)localObject).b();
      e();
    }
    j = true;
  }
  
  public final void y_()
  {
    super.y_();
    Object localObject = f;
    k localk = g;
    ((f)localObject).b(localk, this);
    localObject = k;
    if (localObject != null) {
      ((e)localObject).d();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.update.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */