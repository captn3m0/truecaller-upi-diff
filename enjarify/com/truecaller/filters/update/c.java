package com.truecaller.filters.update;

import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.design.widget.CoordinatorLayout.b;
import android.support.design.widget.CoordinatorLayout.e;
import android.support.transition.p;
import android.support.v4.app.f;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.truecaller.ads.AdLayoutType;
import com.truecaller.ads.provider.holders.e;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.util.at;
import java.util.Random;

public final class c
  extends android.support.design.widget.b
  implements i
{
  g a;
  public c.a b;
  private ConstraintLayout c;
  private ImageView d;
  private TextView e;
  private TextView f;
  private ProgressBar g;
  private Button h;
  private FrameLayout i;
  private Group j;
  private View k;
  private ValueAnimator l;
  
  public static c a(boolean paramBoolean)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putBoolean("forceUpdate", paramBoolean);
    c localc = new com/truecaller/filters/update/c;
    localc.<init>();
    localc.setArguments(localBundle);
    return localc;
  }
  
  public final void a()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    ImageView localImageView = d;
    int m = com.truecaller.utils.ui.b.d(localContext, 2130968674);
    at.a(localImageView, m);
    at.a(e, 2131887290);
    at.a(h, false);
    at.a(f, false, false);
    at.a(g, true);
    l.start();
  }
  
  public final void a(e parame)
  {
    j.setVisibility(0);
    f localf = getActivity();
    AdLayoutType localAdLayoutType = AdLayoutType.MEGA_VIDEO;
    parame = com.truecaller.ads.d.a(localf, localAdLayoutType, parame);
    i.removeAllViews();
    i.addView(parame);
    at.a(h, false);
  }
  
  public final void a(String paramString)
  {
    at.b(f, paramString);
  }
  
  public final void b()
  {
    p.a(c);
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    ImageView localImageView = d;
    int m = com.truecaller.utils.ui.b.d(localContext, 2130968675);
    at.a(localImageView, m);
    at.a(e, 2131887289);
    at.a(g, false, false);
  }
  
  public final void c()
  {
    p.a(c);
    at.a(d, 2131234305);
    at.a(e, 2131887284);
    at.a(h, 2131887288);
    at.a(f, false, false);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = new com/truecaller/filters/update/a$a;
    paramBundle.<init>((byte)0);
    Object localObject = (bp)dagger.a.g.a(((bk)getContext().getApplicationContext()).a());
    b = ((bp)localObject);
    localObject = new com/truecaller/filters/update/d;
    ((d)localObject).<init>();
    localObject = (d)dagger.a.g.a(localObject);
    a = ((d)localObject);
    localObject = a;
    if (localObject == null)
    {
      localObject = new com/truecaller/filters/update/d;
      ((d)localObject).<init>();
      a = ((d)localObject);
    }
    dagger.a.g.a(b, bp.class);
    localObject = new com/truecaller/filters/update/a;
    d locald = a;
    paramBundle = b;
    ((a)localObject).<init>(locald, paramBundle, (byte)0);
    ((b)localObject).a(this);
  }
  
  public final void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    paramDialogInterface = b;
    if (paramDialogInterface != null) {
      paramDialogInterface.w();
    }
    l.cancel();
    a.y_();
  }
  
  public final void setupDialog(Dialog paramDialog, int paramInt)
  {
    super.setupDialog(paramDialog, paramInt);
    paramInt = 2;
    Object localObject1 = new int[paramInt];
    Object tmp13_12 = localObject1;
    tmp13_12[0] = 0;
    tmp13_12[1] = 100;
    localObject1 = ValueAnimator.ofInt((int[])localObject1);
    l = ((ValueAnimator)localObject1);
    localObject1 = l;
    Object localObject2 = new java/util/Random;
    ((Random)localObject2).<init>();
    int m = 2000;
    long l1 = ((Random)localObject2).nextInt(m) + m;
    ((ValueAnimator)localObject1).setDuration(l1);
    localObject1 = l;
    localObject2 = new com/truecaller/filters/update/-$$Lambda$c$NH8YyACNzWISuCo8ov_FMh0dt4k;
    ((-..Lambda.c.NH8YyACNzWISuCo8ov_FMh0dt4k)localObject2).<init>(this);
    ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    localObject1 = l;
    localObject2 = new android/support/v4/view/b/b;
    ((android.support.v4.view.b.b)localObject2).<init>();
    ((ValueAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
    localObject1 = l;
    localObject2 = new com/truecaller/filters/update/c$1;
    ((c.1)localObject2).<init>(this);
    ((ValueAnimator)localObject1).addListener((Animator.AnimatorListener)localObject2);
    localObject1 = getContext();
    m = 0;
    localObject1 = View.inflate((Context)localObject1, 2131558601, null);
    localObject2 = localObject1;
    localObject2 = (ConstraintLayout)localObject1;
    c = ((ConstraintLayout)localObject2);
    localObject2 = (ImageView)((View)localObject1).findViewById(2131363323);
    d = ((ImageView)localObject2);
    localObject2 = (TextView)((View)localObject1).findViewById(2131364884);
    e = ((TextView)localObject2);
    localObject2 = (TextView)((View)localObject1).findViewById(2131364609);
    f = ((TextView)localObject2);
    localObject2 = (ProgressBar)((View)localObject1).findViewById(2131364008);
    g = ((ProgressBar)localObject2);
    localObject2 = (Button)((View)localObject1).findViewById(2131362276);
    h = ((Button)localObject2);
    localObject2 = (FrameLayout)((View)localObject1).findViewById(2131361950);
    i = ((FrameLayout)localObject2);
    localObject2 = (Group)((View)localObject1).findViewById(2131361957);
    j = ((Group)localObject2);
    localObject2 = ((View)localObject1).findViewById(2131364938);
    k = ((View)localObject2);
    localObject2 = h;
    Object localObject3 = new com/truecaller/filters/update/-$$Lambda$c$MWkboyrrAvtosdlePr4rGhasbu4;
    ((-..Lambda.c.MWkboyrrAvtosdlePr4rGhasbu4)localObject3).<init>(this);
    ((Button)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    int n = 2131362507;
    localObject2 = ((View)localObject1).findViewById(n);
    localObject3 = new com/truecaller/filters/update/-$$Lambda$c$6Q1eZxjJiyQ3WBVo-Lz20P3IiLY;
    ((-..Lambda.c.6Q1eZxjJiyQ3WBVo-Lz20P3IiLY)localObject3).<init>(this);
    ((View)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    localObject2 = k;
    localObject3 = new com/truecaller/filters/update/-$$Lambda$c$7hA9hN6P3EQPziiYUuXx5qIIIEw;
    ((-..Lambda.c.7hA9hN6P3EQPziiYUuXx5qIIIEw)localObject3).<init>(this);
    ((View)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    paramDialog.setContentView((View)localObject1);
    a.a(this);
    localObject1 = getArguments();
    if (localObject1 != null)
    {
      localObject2 = "forceUpdate";
      paramInt = ((Bundle)localObject1).getBoolean((String)localObject2);
      if (paramInt != 0)
      {
        localObject1 = a;
        ((g)localObject1).b();
      }
    }
    paramDialog = paramDialog.findViewById(2131362815);
    localObject1 = paramDialog.getLayoutParams();
    paramInt = localObject1 instanceof CoordinatorLayout.e;
    if (paramInt != 0)
    {
      localObject1 = (CoordinatorLayout.e)paramDialog.getLayoutParams();
      localObject2 = new com/truecaller/filters/update/UpdateFiltersDialogBehavior;
      ((UpdateFiltersDialogBehavior)localObject2).<init>();
      ((CoordinatorLayout.e)localObject1).a((CoordinatorLayout.b)localObject2);
      paramDialog.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    }
    paramDialog.setBackgroundResource(17170445);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.update.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */