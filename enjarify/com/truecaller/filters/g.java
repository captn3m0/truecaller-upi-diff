package com.truecaller.filters;

import android.text.TextUtils;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import org.c.a.a.a.a.a;
import org.c.a.a.a.a.c;
import org.c.a.a.a.a.d;

public final class g
{
  static final g a;
  static final g b;
  static final g c;
  static final g d;
  static final g e;
  static final g f;
  static final g g;
  public final FilterManager.FilterAction h;
  public final long i;
  public final FilterManager.ActionSource j;
  public final String k;
  public final int l;
  public final int m;
  public final TruecallerContract.Filters.WildCardType n;
  
  static
  {
    g localg = new com/truecaller/filters/g;
    FilterManager.FilterAction localFilterAction = FilterManager.FilterAction.NONE_FOUND;
    FilterManager.ActionSource localActionSource = FilterManager.ActionSource.NONE;
    localg.<init>(localFilterAction, localActionSource);
    a = localg;
    localg = new com/truecaller/filters/g;
    localFilterAction = FilterManager.FilterAction.FILTER_BLACKLISTED;
    localActionSource = FilterManager.ActionSource.UNKNOWN;
    localg.<init>(localFilterAction, localActionSource);
    b = localg;
    localg = new com/truecaller/filters/g;
    localFilterAction = FilterManager.FilterAction.FILTER_DISABLED;
    localActionSource = FilterManager.ActionSource.UNKNOWN;
    localg.<init>(localFilterAction, localActionSource);
    c = localg;
    localg = new com/truecaller/filters/g;
    localFilterAction = FilterManager.FilterAction.FILTER_BLACKLISTED;
    localActionSource = FilterManager.ActionSource.NON_PHONEBOOK;
    localg.<init>(localFilterAction, localActionSource);
    d = localg;
    localg = new com/truecaller/filters/g;
    localFilterAction = FilterManager.FilterAction.FILTER_BLACKLISTED;
    localActionSource = FilterManager.ActionSource.FOREIGN;
    localg.<init>(localFilterAction, localActionSource);
    e = localg;
    localg = new com/truecaller/filters/g;
    localFilterAction = FilterManager.FilterAction.FILTER_BLACKLISTED;
    localActionSource = FilterManager.ActionSource.NEIGHBOUR_SPOOFING;
    localg.<init>(localFilterAction, localActionSource);
    f = localg;
    localg = new com/truecaller/filters/g;
    localFilterAction = FilterManager.FilterAction.FILTER_BLACKLISTED;
    localActionSource = FilterManager.ActionSource.INDIAN_REGISTERED_TELEMARKETER;
    localg.<init>(localFilterAction, localActionSource);
    g = localg;
  }
  
  public g(long paramLong, FilterManager.FilterAction paramFilterAction, FilterManager.ActionSource paramActionSource, String paramString, int paramInt1, int paramInt2, TruecallerContract.Filters.WildCardType paramWildCardType)
  {
    i = paramLong;
    h = paramFilterAction;
    j = paramActionSource;
    k = paramString;
    l = paramInt1;
    m = paramInt2;
    n = paramWildCardType;
  }
  
  private g(FilterManager.FilterAction paramFilterAction, FilterManager.ActionSource paramActionSource)
  {
    i = -1;
    h = paramFilterAction;
    j = paramActionSource;
    k = null;
    l = 0;
    m = 0;
    paramFilterAction = TruecallerContract.Filters.WildCardType.NONE;
    n = paramFilterAction;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof g;
    if (bool1)
    {
      bool1 = true;
      if (paramObject == this) {
        return bool1;
      }
      paramObject = (g)paramObject;
      long l1 = i;
      long l2 = i;
      boolean bool2 = l1 < l2;
      if (!bool2)
      {
        int i1 = l;
        int i2 = l;
        if (i1 == i2)
        {
          i1 = m;
          i2 = m;
          if (i1 == i2)
          {
            Object localObject1 = h;
            Object localObject2 = h;
            if (localObject1 == localObject2)
            {
              localObject1 = j;
              localObject2 = j;
              if (localObject1 == localObject2)
              {
                localObject1 = n;
                localObject2 = n;
                if (localObject1 == localObject2)
                {
                  localObject1 = k;
                  paramObject = k;
                  boolean bool3 = TextUtils.equals((CharSequence)localObject1, (CharSequence)paramObject);
                  if (bool3) {
                    return bool1;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    a locala = new org/c/a/a/a/a/a;
    locala.<init>();
    long l1 = i;
    locala = locala.a(l1);
    Object localObject = h;
    locala = locala.a(localObject);
    localObject = j;
    locala = locala.a(localObject);
    localObject = k;
    locala = locala.a(localObject);
    int i1 = l;
    locala = locala.a(i1);
    i1 = m;
    locala = locala.a(i1);
    localObject = n;
    return aa;
  }
  
  public final String toString()
  {
    d locald = d.g;
    return c.b(this, locald);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */