package com.truecaller.filters;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import java.util.List;

final class t$b
  extends u
{
  private final List b;
  private final List c;
  private final List d;
  private final String e;
  private final boolean f;
  private final TruecallerContract.Filters.EntityType g;
  
  private t$b(e parame, List paramList1, List paramList2, List paramList3, String paramString, boolean paramBoolean, TruecallerContract.Filters.EntityType paramEntityType)
  {
    super(parame);
    b = paramList1;
    c = paramList2;
    d = paramList3;
    e = paramString;
    f = paramBoolean;
    g = paramEntityType;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".blacklistAddresses(");
    Object localObject = b;
    int i = 1;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = c;
    int j = 2;
    localObject = a(localObject, j);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(e, j);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(f), j);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(g, j);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.t.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */