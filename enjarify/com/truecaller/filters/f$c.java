package com.truecaller.filters;

import com.google.c.a.k;
import com.truecaller.common.b.a;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.ab;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.log.AssertionUtil;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class f$c
{
  private final Pattern a;
  
  f$c(String paramString, int paramInt)
  {
    TruecallerContract.Filters.WildCardType localWildCardType = TruecallerContract.Filters.WildCardType.valueOfType(paramInt);
    int i = 1;
    Pattern localPattern;
    try
    {
      localPattern = localWildCardType.compilePattern(paramString);
      try
      {
        localObject1 = TruecallerContract.Filters.WildCardType.START;
        if (localWildCardType != localObject1) {
          break label334;
        }
        localObject1 = localWildCardType.stripPattern(paramString);
        localObject2 = "+";
        boolean bool1 = ((String)localObject1).startsWith((String)localObject2);
        if (bool1) {
          break label334;
        }
        localObject2 = k.a();
        Object localObject3 = a.F();
        localObject3 = ((a)localObject3).H();
        localObject3 = ((String)localObject3).toUpperCase();
        int j = ((k)localObject2).c((String)localObject3);
        boolean bool2 = ab.a(j);
        String str = "00";
        boolean bool3 = ((String)localObject1).startsWith(str);
        int k;
        if (bool3)
        {
          localObject2 = new java/lang/StringBuilder;
          localObject3 = "+";
          ((StringBuilder)localObject2).<init>((String)localObject3);
          k = 2;
          localObject1 = ((String)localObject1).substring(k);
          ((StringBuilder)localObject2).append((String)localObject1);
          localObject1 = ((StringBuilder)localObject2).toString();
        }
        else
        {
          str = "0";
          bool3 = ((String)localObject1).startsWith(str);
          if ((bool3) && (k == 0))
          {
            localObject1 = ((String)localObject1).substring(i);
            localObject3 = new java/lang/StringBuilder;
            str = "+";
            ((StringBuilder)localObject3).<init>(str);
            ((StringBuilder)localObject3).append(j);
            ((StringBuilder)localObject3).append((String)localObject1);
            localObject1 = ((StringBuilder)localObject3).toString();
          }
          else
          {
            localObject3 = new java/lang/StringBuilder;
            str = "+";
            ((StringBuilder)localObject3).<init>(str);
            ((StringBuilder)localObject3).append(j);
            ((StringBuilder)localObject3).append((String)localObject1);
            localObject1 = ((StringBuilder)localObject3).toString();
          }
        }
        localObject1 = localWildCardType.formatPattern((String)localObject1);
        localPattern = localWildCardType.compilePattern((String)localObject1);
      }
      catch (PatternSyntaxException localPatternSyntaxException1) {}
      arrayOfString = new String[i];
    }
    catch (PatternSyntaxException localPatternSyntaxException2)
    {
      localPattern = null;
    }
    String[] arrayOfString;
    Object localObject1 = null;
    Object localObject2 = "Could not parse ";
    paramString = String.valueOf(paramString);
    paramString = ((String)localObject2).concat(paramString);
    arrayOfString[0] = paramString;
    AssertionUtil.reportThrowableButNeverCrash(localPatternSyntaxException2);
    label334:
    a = localPattern;
  }
  
  final boolean a(String paramString)
  {
    Object localObject = a;
    if (localObject != null)
    {
      localObject = aa.b(paramString);
      if (localObject != null)
      {
        Pattern localPattern = a;
        localObject = localPattern.matcher((CharSequence)localObject);
        boolean bool1 = ((Matcher)localObject).matches();
        if (bool1) {}
      }
      else
      {
        localObject = a;
        paramString = ((Pattern)localObject).matcher(paramString);
        boolean bool2 = paramString.matches();
        if (!bool2) {
          break label64;
        }
      }
      return true;
    }
    label64:
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.f.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */