package com.truecaller.filters.blockedevents;

public enum BlockDialogActivity$DialogType
{
  static
  {
    Object localObject = new com/truecaller/filters/blockedevents/BlockDialogActivity$DialogType;
    ((DialogType)localObject).<init>("NAME", 0);
    NAME = (DialogType)localObject;
    localObject = new com/truecaller/filters/blockedevents/BlockDialogActivity$DialogType;
    int i = 1;
    ((DialogType)localObject).<init>("ADVANCED", i);
    ADVANCED = (DialogType)localObject;
    localObject = new com/truecaller/filters/blockedevents/BlockDialogActivity$DialogType;
    int j = 2;
    ((DialogType)localObject).<init>("NUMBER", j);
    NUMBER = (DialogType)localObject;
    localObject = new com/truecaller/filters/blockedevents/BlockDialogActivity$DialogType;
    int k = 3;
    ((DialogType)localObject).<init>("COUNTRY", k);
    COUNTRY = (DialogType)localObject;
    localObject = new DialogType[4];
    DialogType localDialogType = NAME;
    localObject[0] = localDialogType;
    localDialogType = ADVANCED;
    localObject[i] = localDialogType;
    localDialogType = NUMBER;
    localObject[j] = localDialogType;
    localDialogType = COUNTRY;
    localObject[k] = localDialogType;
    $VALUES = (DialogType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.BlockDialogActivity.DialogType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */