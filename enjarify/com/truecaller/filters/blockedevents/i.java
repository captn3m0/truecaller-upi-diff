package com.truecaller.filters.blockedevents;

import com.google.android.gms.ads.AdSize;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.ads.k;
import com.truecaller.ads.k.a;
import com.truecaller.ads.k.b;
import com.truecaller.ads.k.c;
import com.truecaller.i.a;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class i
  implements d
{
  private final Provider a;
  
  public static k a(a parama)
  {
    boolean bool = parama.b("adsFeatureUnifiedAdsBlock");
    int i = 1;
    Object localObject;
    CustomTemplate[] arrayOfCustomTemplate;
    if (bool)
    {
      parama = k.a().a("/43067329/A*Block*Unified*GPS").b("BLOCK").c("blockView");
      localObject = new AdSize[i];
      AdSize localAdSize = AdSize.a;
      localObject[0] = localAdSize;
      parama = parama.a((AdSize[])localObject);
      arrayOfCustomTemplate = new CustomTemplate[i];
      localObject = CustomTemplate.NATIVE_CONTENT_DUAL_TRACKER;
      arrayOfCustomTemplate[0] = localObject;
      parama = parama.a(arrayOfCustomTemplate).e();
    }
    else
    {
      parama = k.a().a("/43067329/A*Block*Native*GPS").b("BLOCK").c("blockView");
      arrayOfCustomTemplate = new CustomTemplate[i];
      localObject = CustomTemplate.NATIVE_CONTENT_DUAL_TRACKER;
      arrayOfCustomTemplate[0] = localObject;
      parama = parama.a(arrayOfCustomTemplate).e();
    }
    return (k)g.a(parama, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */