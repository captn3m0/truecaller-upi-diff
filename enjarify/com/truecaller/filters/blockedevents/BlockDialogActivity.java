package com.truecaller.filters.blockedevents;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public class BlockDialogActivity
  extends AppCompatActivity
{
  public static void a(Context paramContext, BlockDialogActivity.DialogType paramDialogType)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, BlockDialogActivity.class);
    localIntent.putExtra("type", paramDialogType);
    paramContext.startActivity(localIntent);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    Object localObject = ThemeManager.a();
    int i = resId;
    setTheme(i);
    super.onCreate(paramBundle);
    if (paramBundle == null)
    {
      paramBundle = (BlockDialogActivity.DialogType)getIntent().getSerializableExtra("type");
      localObject = BlockDialogActivity.1.a;
      int j = paramBundle.ordinal();
      j = localObject[j];
      switch (j)
      {
      default: 
        AssertionUtil.OnlyInDebug.fail(new String[] { "No dialog type specified" });
        return;
      case 4: 
        paramBundle = new com/truecaller/filters/blockedevents/a/b;
        paramBundle.<init>();
        break;
      case 3: 
        paramBundle = new com/truecaller/filters/blockedevents/c/b;
        paramBundle.<init>();
        break;
      case 2: 
        paramBundle = new com/truecaller/filters/blockedevents/blockadvanced/b;
        paramBundle.<init>();
        break;
      case 1: 
        paramBundle = new com/truecaller/filters/blockedevents/b/b;
        paramBundle.<init>();
      }
      localObject = getSupportFragmentManager().a();
      int k = 16908290;
      paramBundle = ((o)localObject).b(k, paramBundle);
      paramBundle.c();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.BlockDialogActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */