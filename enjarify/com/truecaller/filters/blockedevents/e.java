package com.truecaller.filters.blockedevents;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.ads.AdLayoutType;
import com.truecaller.ads.ui.AdsSwitchView;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.filters.blockedlist.BlockedListActivity;
import com.truecaller.filters.update.c;
import com.truecaller.filters.update.c.a;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.ui.RequiredPermissionsActivity;
import com.truecaller.ui.SettingsFragment;
import com.truecaller.ui.SettingsFragment.SettingsViewType;
import com.truecaller.ui.components.FloatingActionButton.a;
import com.truecaller.ui.components.FloatingActionButton.c;
import com.truecaller.ui.components.i;
import com.truecaller.util.at;
import java.util.List;

public final class e
  extends com.truecaller.filters.b
  implements l, c.a, FloatingActionButton.c
{
  j c;
  br d;
  private AdsSwitchView e;
  private TextView f;
  private ImageView g;
  private TextView h;
  private TextView i;
  private p j;
  private View k;
  private RecyclerView l;
  private View m;
  private p n;
  private View o;
  private RecyclerView p;
  private View q;
  
  public final void a(int paramInt)
  {
    c.c(paramInt);
  }
  
  public final void a(com.truecaller.ads.provider.holders.e parame)
  {
    AdsSwitchView localAdsSwitchView = e;
    AdLayoutType localAdLayoutType = AdLayoutType.SMALL;
    localAdsSwitchView.a(parame, localAdLayoutType);
    e.setVisibility(0);
  }
  
  public final void a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    br.a(requireActivity(), paramLaunchContext, "premiumAdvancedBlocking");
  }
  
  public final void a(Integer paramInteger, String paramString)
  {
    paramInteger = a.a(paramInteger, paramString);
    paramString = getFragmentManager();
    paramInteger.show(paramString, null);
  }
  
  public final void a(String paramString)
  {
    f.setText(paramString);
  }
  
  public final void a(List paramList1, List paramList2)
  {
    j.a(paramList1);
    n.a(paramList2);
  }
  
  public final void a(boolean paramBoolean)
  {
    TextView localTextView = h;
    int i1 = 0;
    int i2;
    if (paramBoolean) {
      i2 = 0;
    } else {
      i2 = 8;
    }
    localTextView.setVisibility(i2);
    localTextView = i;
    if (paramBoolean) {
      i1 = 8;
    }
    localTextView.setVisibility(i1);
  }
  
  public final void a(boolean paramBoolean, int paramInt)
  {
    View localView1 = m;
    at.a(localView1, paramBoolean);
    if (paramBoolean)
    {
      View localView2 = m;
      localView2.setBackgroundResource(paramInt);
    }
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    at.a(k, paramBoolean1);
    at.a(o, paramBoolean2);
  }
  
  public final void b(boolean paramBoolean)
  {
    View localView = q;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public final void c(int paramInt)
  {
    g.setImageResource(paramInt);
  }
  
  public final void c(boolean paramBoolean)
  {
    c localc = c.a(paramBoolean);
    b = this;
    android.support.v4.app.j localj = getFragmentManager();
    localc.show(localj, null);
  }
  
  public final void h()
  {
    Context localContext = getContext();
    BlockDialogActivity.DialogType localDialogType = BlockDialogActivity.DialogType.NAME;
    BlockDialogActivity.a(localContext, localDialogType);
  }
  
  public final int i()
  {
    return 2131233881;
  }
  
  public final i[] j()
  {
    return d();
  }
  
  public final FloatingActionButton.a k()
  {
    return this;
  }
  
  public final boolean l()
  {
    return true;
  }
  
  public final void m()
  {
    Context localContext = getContext();
    BlockDialogActivity.DialogType localDialogType = BlockDialogActivity.DialogType.ADVANCED;
    BlockDialogActivity.a(localContext, localDialogType);
  }
  
  public final void n()
  {
    Context localContext = getContext();
    BlockDialogActivity.DialogType localDialogType = BlockDialogActivity.DialogType.NUMBER;
    BlockDialogActivity.a(localContext, localDialogType);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = r.a();
    Object localObject = ((bk)requireContext().getApplicationContext()).a();
    paramBundle = paramBundle.a((bp)localObject);
    localObject = new com/truecaller/filters/blockedevents/f;
    ((f)localObject).<init>();
    paramBundle.a((f)localObject).a().a(this);
    setHasOptionsMenu(true);
    paramBundle = new com/truecaller/filters/blockedevents/p;
    localObject = c;
    paramBundle.<init>((o)localObject);
    j = paramBundle;
    paramBundle = new com/truecaller/filters/blockedevents/p;
    localObject = c;
    paramBundle.<init>((o)localObject);
    n = paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131558663, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    c.y_();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i1 = paramMenuItem.getItemId();
    int i2 = 16908332;
    if (i1 == i2)
    {
      c.e();
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public final void onResume()
  {
    super.onResume();
    c.b();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (AdsSwitchView)paramView.findViewById(2131362587);
    e = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131365463);
    f = paramBundle;
    paramBundle = (ImageView)paramView.findViewById(2131364561);
    g = paramBundle;
    paramBundle = paramView.findViewById(2131362438);
    k = paramBundle;
    paramBundle = (RecyclerView)paramView.findViewById(2131363663);
    l = paramBundle;
    paramBundle = paramView.findViewById(2131362302);
    m = paramBundle;
    paramBundle = paramView.findViewById(2131362437);
    o = paramBundle;
    paramBundle = (RecyclerView)paramView.findViewById(2131363661);
    p = paramBundle;
    paramBundle = paramView.findViewById(2131362359);
    q = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131365403);
    h = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131362063);
    i = paramBundle;
    paramBundle = h;
    Object localObject = new com/truecaller/filters/blockedevents/-$$Lambda$e$Hp17lcXmckFDLoKgqMimZOZ_WBY;
    ((-..Lambda.e.Hp17lcXmckFDLoKgqMimZOZ_WBY)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    paramBundle = requireContext();
    int i1 = android.support.v4.content.b.c(requireContext(), 2131100601);
    paramBundle = at.a(paramBundle, 2131234567, i1);
    h.setCompoundDrawablesWithIntrinsicBounds(paramBundle, null, null, null);
    i.setCompoundDrawablesWithIntrinsicBounds(paramBundle, null, null, null);
    paramBundle = f;
    localObject = new com/truecaller/filters/blockedevents/-$$Lambda$e$0UwtzGkZkEgxsa29mUne-FGrsI4;
    ((-..Lambda.e.0UwtzGkZkEgxsa29mUne-FGrsI4)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    paramBundle = paramView.findViewById(2131363720);
    localObject = new com/truecaller/filters/blockedevents/-$$Lambda$e$rgleBVaK6tfdYpPxhwGyPXsNsqY;
    ((-..Lambda.e.rgleBVaK6tfdYpPxhwGyPXsNsqY)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    paramView = paramView.findViewById(2131362357);
    paramBundle = new com/truecaller/filters/blockedevents/-$$Lambda$e$tE4NWf8MFsPFznPQ3I6DYQJrcs4;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = TrueApp.y();
    boolean bool2 = paramView.isTcPayEnabled();
    localObject = paramView.f().ap();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    int i3 = 1;
    if (bool1)
    {
      bool3 = paramView.p();
      if (bool3)
      {
        bool3 = true;
        break label357;
      }
    }
    boolean bool3 = false;
    paramView = null;
    label357:
    if ((bool3) && (!bool2))
    {
      bool3 = false;
      paramView = null;
    }
    else
    {
      bool3 = true;
    }
    if (bool3)
    {
      int i4 = 2131886203;
      b(i4);
      b();
    }
    else
    {
      paramView = b;
      int i2 = 8;
      paramView.setVisibility(i2);
      c();
    }
    paramView = getContext();
    if (paramView != null)
    {
      l.setNestedScrollingEnabled(false);
      paramBundle = l;
      localObject = new android/support/v7/widget/LinearLayoutManager;
      ((LinearLayoutManager)localObject).<init>(paramView, i3, false);
      paramBundle.setLayoutManager((RecyclerView.LayoutManager)localObject);
      paramBundle = l;
      localObject = j;
      paramBundle.setAdapter((RecyclerView.Adapter)localObject);
      p.setNestedScrollingEnabled(false);
      paramBundle = p;
      localObject = new android/support/v7/widget/LinearLayoutManager;
      ((LinearLayoutManager)localObject).<init>(paramView, i3, false);
      paramBundle.setLayoutManager((RecyclerView.LayoutManager)localObject);
      paramView = p;
      paramBundle = n;
      paramView.setAdapter(paramBundle);
    }
    paramView = m;
    paramBundle = new com/truecaller/filters/blockedevents/-$$Lambda$e$uPH8d7_CCFyDE8TS5Yg7woCTvUE;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    c.a(this);
  }
  
  public final boolean q()
  {
    return c.c();
  }
  
  public final void r()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    localBuilder.<init>(localContext);
    localBuilder.setTitle(2131886166).setMessage(2131886165).setPositiveButton(2131886164, null).show();
  }
  
  public final void s()
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = getContext();
    localIntent.<init>(localContext, BlockedListActivity.class);
    startActivity(localIntent);
  }
  
  public final void t()
  {
    Context localContext = getContext();
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_BLOCK;
    SettingsFragment.c(localContext, localSettingsViewType);
  }
  
  public final void u()
  {
    RequiredPermissionsActivity.a(getContext());
  }
  
  public final void v()
  {
    getActivity().finish();
  }
  
  public final void w()
  {
    c.i();
  }
  
  public final void x()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf == null) {
      return;
    }
    Intent localIntent = com.truecaller.common.h.o.a(localf);
    com.truecaller.common.h.o.a(localf, localIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */