package com.truecaller.filters.blockedevents.a;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.bk;
import java.util.HashMap;

public final class b
  extends Fragment
  implements g
{
  public e a;
  private HashMap b;
  
  private View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final e a()
  {
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locale;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "countryName");
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject).<init>(localContext);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    paramString = (CharSequence)getString(2131886129, arrayOfObject);
    paramString = ((AlertDialog.Builder)localObject).setMessage(paramString);
    localObject = new com/truecaller/filters/blockedevents/a/b$c;
    ((b.c)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    paramString = paramString.setPositiveButton(2131886127, (DialogInterface.OnClickListener)localObject);
    localObject = (DialogInterface.OnClickListener)b.d.a;
    paramString.setNegativeButton(2131886387, (DialogInterface.OnClickListener)localObject).create().show();
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.block_button;
    Button localButton = (Button)a(i);
    k.a(localButton, "block_button");
    localButton.setEnabled(paramBoolean);
  }
  
  public final void b()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void c()
  {
    Toast.makeText(getContext(), 2131886138, 0).show();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null)
    {
      k.a(paramBundle, "context ?: throw Runtime…ountryFragment onCreate\")");
      h.a locala = h.a();
      paramBundle = paramBundle.getApplicationContext();
      if (paramBundle != null)
      {
        paramBundle = ((bk)paramBundle).a();
        locala.a(paramBundle).a().a(this);
        return;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
      throw paramBundle;
    }
    paramBundle = new java/lang/RuntimeException;
    paramBundle.<init>("Null context in BlockCountryFragment onCreate");
    throw ((Throwable)paramBundle);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558660, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.y_();
    super.onDestroy();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      paramBundle = (AppCompatActivity)paramBundle;
      int i = 2131364907;
      paramView = paramView.findViewById(i);
      if (paramView != null)
      {
        paramView = (Toolbar)paramView;
        Object localObject = paramBundle;
        localObject = (Context)paramBundle;
        int j = 2131233794;
        int k = 2130969592;
        localObject = com.truecaller.utils.ui.b.a((Context)localObject, j, k);
        paramView.setNavigationIcon((Drawable)localObject);
        paramBundle.setSupportActionBar(paramView);
        paramView = paramBundle.getSupportActionBar();
        if (paramView != null)
        {
          paramView.setTitle(2131886128);
          boolean bool = true;
          paramView.setDisplayHomeAsUpEnabled(bool);
        }
        paramView = a;
        if (paramView == null)
        {
          paramBundle = "presenter";
          k.a(paramBundle);
        }
        paramView.a(this);
        int m = R.id.country_spinner;
        paramView = (Spinner)a(m);
        k.a(paramView, "country_spinner");
        paramBundle = new com/truecaller/filters/blockedevents/c/h;
        localObject = a;
        if (localObject == null)
        {
          String str = "presenter";
          k.a(str);
        }
        localObject = (com.truecaller.b)localObject;
        paramBundle.<init>((com.truecaller.b)localObject);
        paramBundle = (SpinnerAdapter)paramBundle;
        paramView.setAdapter(paramBundle);
        m = R.id.country_spinner;
        paramView = (Spinner)a(m);
        k.a(paramView, "country_spinner");
        paramBundle = new com/truecaller/filters/blockedevents/a/b$a;
        paramBundle.<init>(this);
        paramBundle = (AdapterView.OnItemSelectedListener)paramBundle;
        paramView.setOnItemSelectedListener(paramBundle);
        m = R.id.block_button;
        paramView = (Button)a(m);
        paramBundle = new com/truecaller/filters/blockedevents/a/b$b;
        paramBundle.<init>(this);
        paramBundle = (View.OnClickListener)paramBundle;
        paramView.setOnClickListener(paramBundle);
        return;
      }
      paramView = new c/u;
      paramView.<init>("null cannot be cast to non-null type android.support.v7.widget.Toolbar");
      throw paramView;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */