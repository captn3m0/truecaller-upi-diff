package com.truecaller.filters.blockedevents.a;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

final class b$d
  implements DialogInterface.OnClickListener
{
  public static final d a;
  
  static
  {
    d locald = new com/truecaller/filters/blockedevents/a/b$d;
    locald.<init>();
    a = locald;
  }
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    paramDialogInterface.dismiss();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.a.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */