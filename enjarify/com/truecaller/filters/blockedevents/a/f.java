package com.truecaller.filters.blockedevents.a;

import c.g.b.k;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.filters.s;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.ad;
import com.truecaller.utils.n;
import java.util.List;

public final class f
  extends e
{
  private final List a;
  private CountryListDto.a c;
  private final i d;
  private final com.truecaller.androidactors.f e;
  private final n f;
  
  public f(i parami, ad paramad, com.truecaller.androidactors.f paramf, n paramn)
  {
    d = parami;
    e = paramf;
    f = paramn;
    parami = paramad.a();
    k.a(parami, "countryManager.allCountries");
    a = parami;
  }
  
  public final int a()
  {
    return a.size() + 1;
  }
  
  public final int a(int paramInt)
  {
    return 0;
  }
  
  public final long b(int paramInt)
  {
    return 0L;
  }
  
  public final void b()
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "Country had no name";
      AssertionUtil.reportWeirdnessButNeverCrash((String)localObject2);
    }
    Object localObject2 = (g)b;
    if (localObject2 != null)
    {
      if (localObject1 == null) {
        localObject1 = "";
      }
      ((g)localObject2).a((String)localObject1);
      return;
    }
  }
  
  public final void c()
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    localObject1 = ((s)e.a()).a((CountryListDto.a)localObject1, "blockView");
    i locali = d;
    Object localObject2 = new com/truecaller/filters/blockedevents/a/f$a;
    ((f.a)localObject2).<init>(this);
    localObject2 = (ac)localObject2;
    ((w)localObject1).a(locali, (ac)localObject2);
  }
  
  public final void c(int paramInt)
  {
    if (paramInt == 0)
    {
      paramInt = 0;
      c = null;
      localObject = (g)b;
      if (localObject != null)
      {
        localList = null;
        ((g)localObject).a(false);
      }
      return;
    }
    List localList = a;
    int i = 1;
    paramInt -= i;
    Object localObject = (CountryListDto.a)localList.get(paramInt);
    c = ((CountryListDto.a)localObject);
    localObject = (g)b;
    if (localObject != null)
    {
      ((g)localObject).a(i);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */