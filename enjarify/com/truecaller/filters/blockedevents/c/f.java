package com.truecaller.filters.blockedevents.c;

import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.u;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.filters.s;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.ad;
import java.util.List;
import org.c.a.a.a.k;

final class f
  extends e
{
  private final i a;
  private final com.truecaller.androidactors.f c;
  private final ad d;
  private final u e;
  private final List f;
  
  f(i parami, com.truecaller.androidactors.f paramf, ad paramad, u paramu)
  {
    a = parami;
    c = paramf;
    d = paramad;
    e = paramu;
    parami = d.a();
    f = parami;
  }
  
  private void a(CountryListDto.a parama)
  {
    Object localObject = b;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localObject, arrayOfString);
    localObject = f;
    int i = ((List)localObject).indexOf(parama);
    if (i >= 0)
    {
      localObject = (g)b;
      ((g)localObject).a(i);
    }
  }
  
  public final int a()
  {
    return f.size();
  }
  
  public final int a(int paramInt)
  {
    return 0;
  }
  
  final void a(String paramString)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    boolean bool = k.b(paramString);
    if (bool)
    {
      paramString = (g)b;
      localObject = paramString;
    }
    else
    {
      localObject = d.a(paramString);
      a((CountryListDto.a)localObject);
      localObject = (g)b;
      i = paramString.length();
      int j = 3;
      if (i >= j)
      {
        i = 1;
        break label83;
      }
    }
    int i = 0;
    paramString = null;
    label83:
    ((g)localObject).a(i);
  }
  
  final void a(boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject != null)
    {
      ((g)b).f();
      localObject = (g)b;
      ((g)localObject).g();
    }
  }
  
  public final long b(int paramInt)
  {
    return 0L;
  }
  
  final void b()
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    ((g)b).e();
    localObject1 = (g)b;
    Object localObject2 = null;
    ((g)localObject1).a(false);
    localObject1 = (g)b;
    int i = ((g)localObject1).a();
    if (i >= 0)
    {
      localObject2 = f;
      localObject1 = getc;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    localObject2 = ((g)b).b();
    Object localObject3 = e;
    Object localObject4 = ((u)localObject3).a();
    localObject1 = ((u)localObject3).a((String)localObject2, (String)localObject4, (String)localObject1);
    Object localObject5;
    if (localObject1 == null)
    {
      localObject1 = "OTHER";
      localObject5 = localObject1;
      localObject4 = localObject2;
    }
    else
    {
      localObject2 = "PHONE_NUMBER";
      localObject4 = localObject1;
      localObject5 = localObject2;
    }
    localObject1 = c.a();
    localObject3 = localObject1;
    localObject3 = (s)localObject1;
    String str = ((g)b).c();
    TruecallerContract.Filters.EntityType localEntityType = ((g)b).d();
    localObject1 = ((s)localObject3).a((String)localObject4, (String)localObject5, str, "blockView", false, localEntityType);
    localObject2 = a;
    localObject3 = new com/truecaller/filters/blockedevents/c/-$$Lambda$dZEsZMgp0dXUXtWFPMeW9BlcuzA;
    ((-..Lambda.dZEsZMgp0dXUXtWFPMeW9BlcuzA)localObject3).<init>(this);
    ((w)localObject1).a((i)localObject2, (ac)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.c.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */