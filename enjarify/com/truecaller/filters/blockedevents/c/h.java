package com.truecaller.filters.blockedevents.c;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.truecaller.b;

public final class h
  extends BaseAdapter
{
  private final b a;
  
  public h(b paramb)
  {
    a = paramb;
  }
  
  public final int getCount()
  {
    return a.a();
  }
  
  public final Object getItem(int paramInt)
  {
    return null;
  }
  
  public final long getItemId(int paramInt)
  {
    return a.b(paramInt);
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null)
    {
      paramView = LayoutInflater.from(paramViewGroup.getContext());
      int i = 17367049;
      paramView = paramView.inflate(i, paramViewGroup, false);
      paramViewGroup = new com/truecaller/filters/blockedevents/c/h$a;
      paramViewGroup.<init>(this, paramView);
      paramView.setTag(paramViewGroup);
    }
    else
    {
      paramViewGroup = (h.a)paramView.getTag();
    }
    a.a(paramViewGroup, paramInt);
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.c.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */