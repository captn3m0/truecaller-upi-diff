package com.truecaller.filters.blockedevents.c;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.content.TruecallerContract.Filters.EntityType;

public final class b
  extends Fragment
  implements g
{
  e a;
  private Spinner b;
  private EditText c;
  private EditText d;
  private View e;
  private RadioGroup f;
  
  public final int a()
  {
    return b.getSelectedItemPosition();
  }
  
  public final void a(int paramInt)
  {
    b.setSelection(paramInt);
  }
  
  public final void a(boolean paramBoolean)
  {
    e.setEnabled(paramBoolean);
  }
  
  public final String b()
  {
    return c.getText().toString();
  }
  
  public final String c()
  {
    return d.getText().toString();
  }
  
  public final TruecallerContract.Filters.EntityType d()
  {
    RadioGroup localRadioGroup = f;
    int i = localRadioGroup.getCheckedRadioButtonId();
    int j = 2131362274;
    if (i == j) {
      return TruecallerContract.Filters.EntityType.BUSINESS;
    }
    return TruecallerContract.Filters.EntityType.PERSON;
  }
  
  public final void e()
  {
    b.setEnabled(false);
    c.setEnabled(false);
    d.setEnabled(false);
  }
  
  public final void f()
  {
    Toast.makeText(getContext(), 2131886138, 0).show();
  }
  
  public final void g()
  {
    getActivity().finish();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = new com/truecaller/filters/blockedevents/c/j$a;
    paramBundle.<init>((byte)0);
    Object localObject = (bp)dagger.a.g.a(((bk)getContext().getApplicationContext()).a());
    b = ((bp)localObject);
    localObject = a;
    if (localObject == null)
    {
      localObject = new com/truecaller/filters/blockedevents/c/c;
      ((c)localObject).<init>();
      a = ((c)localObject);
    }
    dagger.a.g.a(b, bp.class);
    localObject = new com/truecaller/filters/blockedevents/c/j;
    c localc = a;
    paramBundle = b;
    ((j)localObject).<init>(localc, paramBundle, (byte)0);
    ((a)localObject).a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131558662, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    a.y_();
    super.onDestroy();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (AppCompatActivity)getActivity();
    int i = 2131364907;
    Object localObject1 = (Toolbar)paramView.findViewById(i);
    Object localObject2 = getContext();
    int j = 2131233794;
    int k = 2130969592;
    localObject2 = com.truecaller.utils.ui.b.a((Context)localObject2, j, k);
    ((Toolbar)localObject1).setNavigationIcon((Drawable)localObject2);
    paramBundle.setSupportActionBar((Toolbar)localObject1);
    paramBundle = paramBundle.getSupportActionBar();
    if (paramBundle != null)
    {
      paramBundle.setTitle(2131886137);
      i = 1;
      paramBundle.setDisplayHomeAsUpEnabled(i);
    }
    paramBundle = (Spinner)paramView.findViewById(2131362613);
    b = paramBundle;
    paramBundle = (EditText)paramView.findViewById(2131363828);
    c = paramBundle;
    paramBundle = (EditText)paramView.findViewById(2131363791);
    d = paramBundle;
    paramBundle = paramView.findViewById(2131362123);
    e = paramBundle;
    paramView = (RadioGroup)paramView.findViewById(2131364061);
    f = paramView;
    paramView = b;
    paramBundle = new com/truecaller/filters/blockedevents/c/h;
    localObject1 = a;
    paramBundle.<init>((com.truecaller.b)localObject1);
    paramView.setAdapter(paramBundle);
    a.a(this);
    paramView = e;
    paramBundle = new com/truecaller/filters/blockedevents/c/-$$Lambda$b$4MubrOdpi93OKxq3hbZKU2ZLqaA;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = c;
    paramBundle = new com/truecaller/filters/blockedevents/c/b$1;
    paramBundle.<init>(this);
    paramView.addTextChangedListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */