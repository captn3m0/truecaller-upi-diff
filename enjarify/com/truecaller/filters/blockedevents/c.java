package com.truecaller.filters.blockedevents;

import c.g.b.k;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bb;
import com.truecaller.filters.r;
import com.truecaller.filters.sync.FilterSettingsUploadWorker;
import com.truecaller.filters.sync.FilterSettingsUploadWorker.a;

public final class c
  extends bb
  implements b
{
  private final com.truecaller.filters.p a;
  private final r c;
  private final com.truecaller.analytics.b d;
  private final androidx.work.p e;
  
  public c(com.truecaller.filters.p paramp, r paramr, com.truecaller.analytics.b paramb, androidx.work.p paramp1)
  {
    a = paramp;
    c = paramr;
    d = paramb;
    e = paramp1;
  }
  
  public final void a(int paramInt)
  {
    int i = c.b() + paramInt;
    a.a(i);
    a.g(true);
    Object localObject = FilterSettingsUploadWorker.d;
    FilterSettingsUploadWorker.a.a(e);
    localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("BLOCKSETTINGS_BlockNeighbourSpoofing");
    ((e.a)localObject).a("NbMatchingDigits", paramInt);
    com.truecaller.analytics.b localb = d;
    localObject = ((e.a)localObject).a();
    k.a(localObject, "event.build()");
    localb.b((e)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */