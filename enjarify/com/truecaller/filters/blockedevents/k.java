package com.truecaller.filters.blockedevents;

import c.g.a.q;
import com.truecaller.ads.g;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.am;
import com.truecaller.filters.r;
import com.truecaller.filters.s;
import com.truecaller.filters.sync.FilterSettingsUploadWorker;
import com.truecaller.filters.sync.FilterSettingsUploadWorker.a;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.utils.d;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class k
  extends j
  implements g
{
  a a;
  final n c;
  private com.truecaller.ads.provider.holders.e d;
  private boolean e;
  private final com.truecaller.androidactors.k f;
  private final com.truecaller.androidactors.f g;
  private final com.truecaller.filters.p h;
  private final com.truecaller.tcpermissions.l i;
  private final com.truecaller.analytics.b j;
  private final com.truecaller.ads.provider.f k;
  private final com.truecaller.ads.k l;
  private final com.truecaller.common.f.c m;
  private final com.truecaller.featuretoggles.e n;
  private final androidx.work.p o;
  private final r p;
  private final com.truecaller.abtest.c q;
  private final d r;
  
  public k(com.truecaller.androidactors.k paramk, com.truecaller.androidactors.f paramf, com.truecaller.filters.p paramp, com.truecaller.tcpermissions.l paraml, com.truecaller.analytics.b paramb, com.truecaller.ads.provider.f paramf1, com.truecaller.ads.k paramk1, n paramn, com.truecaller.common.f.c paramc, com.truecaller.featuretoggles.e parame, androidx.work.p paramp1, r paramr, com.truecaller.abtest.c paramc1, d paramd)
  {
    f = paramk;
    g = paramf;
    h = paramp;
    i = paraml;
    j = paramb;
    k = paramf1;
    l = paramk1;
    c = paramn;
    m = paramc;
    n = parame;
    o = paramp1;
    p = paramr;
    q = paramc1;
    r = paramd;
  }
  
  private final void a(boolean paramBoolean, String paramString)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>(paramString);
    paramString = "BlocktabSettings_Action";
    if (paramBoolean) {
      localObject = "Enabled";
    } else {
      localObject = "Disabled";
    }
    locala.a(paramString, (String)localObject);
    Object localObject = j;
    paramString = locala.a();
    c.g.b.k.a(paramString, "event.build()");
    ((com.truecaller.analytics.b)localObject).b(paramString);
  }
  
  private final boolean l()
  {
    com.truecaller.common.f.c localc = m;
    boolean bool = localc.d();
    if (!bool)
    {
      h.a(null);
      return false;
    }
    return com.truecaller.utils.extensions.c.a(h.k());
  }
  
  private final void m()
  {
    l locall = (l)b;
    if (locall == null) {
      return;
    }
    boolean bool1 = l();
    boolean bool2 = true;
    if (!bool1)
    {
      bool3 = e;
      if (!bool3)
      {
        bool3 = false;
        break label47;
      }
    }
    boolean bool3 = true;
    label47:
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (List)localObject1;
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (List)localObject2;
    Object localObject3 = new com/truecaller/filters/blockedevents/k$b;
    ((k.b)localObject3).<init>((List)localObject1, (List)localObject2);
    localObject3 = (q)localObject3;
    Object localObject4 = m.f.iterator();
    for (;;)
    {
      boolean bool5 = ((Iterator)localObject4).hasNext();
      if (!bool5) {
        break;
      }
      m localm = (m)((Iterator)localObject4).next();
      Object localObject5 = m.a.h;
      boolean bool6 = c.g.b.k.a(localm, localObject5);
      Boolean localBoolean;
      if (bool6)
      {
        localObject5 = Boolean.valueOf(bool1);
        localBoolean = Boolean.TRUE;
        ((q)localObject3).a(localm, localObject5, localBoolean);
      }
      else
      {
        localObject5 = m.h.h;
        bool6 = c.g.b.k.a(localm, localObject5);
        boolean bool7;
        if (bool6)
        {
          bool6 = h.g();
          localObject5 = Boolean.valueOf(bool6);
          bool7 = n.L().a();
          localBoolean = Boolean.valueOf(bool7);
          ((q)localObject3).a(localm, localObject5, localBoolean);
        }
        else
        {
          localObject5 = m.g.h;
          bool6 = c.g.b.k.a(localm, localObject5);
          if (bool6)
          {
            localObject5 = n.M();
            bool6 = ((com.truecaller.featuretoggles.b)localObject5).a();
            if (bool6)
            {
              bool6 = h.b();
              localObject5 = Boolean.valueOf(bool6);
              bool7 = n.N().a();
              localBoolean = Boolean.valueOf(bool7);
              ((q)localObject3).a(localm, localObject5, localBoolean);
            }
          }
          else
          {
            localObject5 = m.d.h;
            bool6 = c.g.b.k.a(localm, localObject5);
            if (bool6)
            {
              localObject5 = n.O();
              bool6 = ((com.truecaller.featuretoggles.b)localObject5).a();
              if (bool6)
              {
                bool6 = h.c();
                localObject5 = Boolean.valueOf(bool6);
                bool7 = n.P().a();
                localBoolean = Boolean.valueOf(bool7);
                ((q)localObject3).a(localm, localObject5, localBoolean);
              }
            }
            else
            {
              localObject5 = m.f.h;
              bool6 = c.g.b.k.a(localm, localObject5);
              if (bool6)
              {
                localObject5 = n.Q();
                bool6 = ((com.truecaller.featuretoggles.b)localObject5).a();
                if (bool6)
                {
                  bool6 = h.d();
                  localObject5 = Boolean.valueOf(bool6);
                  bool7 = n.R().a();
                  localBoolean = Boolean.valueOf(bool7);
                  ((q)localObject3).a(localm, localObject5, localBoolean);
                }
              }
              else
              {
                localObject5 = m.i.h;
                bool6 = c.g.b.k.a(localm, localObject5);
                if (bool6)
                {
                  bool6 = h.a();
                  localObject5 = Boolean.valueOf(bool6);
                  bool7 = n.K().a();
                  localBoolean = Boolean.valueOf(bool7);
                  ((q)localObject3).a(localm, localObject5, localBoolean);
                }
                else
                {
                  localObject5 = m.e.h;
                  bool6 = c.g.b.k.a(localm, localObject5);
                  if (bool6)
                  {
                    localObject5 = n.S();
                    bool6 = ((com.truecaller.featuretoggles.b)localObject5).a();
                    if (bool6)
                    {
                      bool6 = h.e();
                      localObject5 = Boolean.valueOf(bool6);
                      localBoolean = Boolean.TRUE;
                      ((q)localObject3).a(localm, localObject5, localBoolean);
                    }
                  }
                  else
                  {
                    localObject5 = m.c.h;
                    bool6 = c.g.b.k.a(localm, localObject5);
                    if (bool6)
                    {
                      bool6 = m.d();
                      localObject5 = Boolean.valueOf(bool6);
                      localBoolean = Boolean.TRUE;
                      ((q)localObject3).a(localm, localObject5, localBoolean);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    localObject3 = localObject1;
    localObject3 = (Collection)localObject1;
    boolean bool8 = ((Collection)localObject3).isEmpty() ^ bool2;
    localObject4 = localObject2;
    localObject4 = (Collection)localObject2;
    boolean bool9 = ((Collection)localObject4).isEmpty() ^ bool2;
    locall.a(bool8, bool9);
    locall.a((List)localObject1, (List)localObject2);
    int i1;
    if (bool3) {
      i1 = 2131233893;
    } else {
      i1 = 2131234312;
    }
    locall.c(i1);
    boolean bool4 = m.d() ^ bool2;
    int i2 = o();
    locall.a(bool4, i2);
    bool1 ^= bool2;
    locall.a(bool1);
    bool1 = p();
    locall.b(bool1);
  }
  
  private void n()
  {
    Object localObject1 = a;
    if (localObject1 != null) {
      ((a)localObject1).a();
    }
    localObject1 = g.a();
    c.g.b.k.a(localObject1, "spamManager.tell()");
    localObject1 = ((s)localObject1).b();
    i locali = f.a();
    Object localObject2 = new com/truecaller/filters/blockedevents/k$a;
    ((k.a)localObject2).<init>(this);
    localObject2 = (ac)localObject2;
    localObject1 = ((w)localObject1).a(locali, (ac)localObject2);
    a = ((a)localObject1);
  }
  
  private final int o()
  {
    CharSequence localCharSequence1 = (CharSequence)q.a("premiumButtonColors_16430");
    CharSequence localCharSequence2 = (CharSequence)"orange";
    boolean bool1 = am.b(localCharSequence1, localCharSequence2);
    if (bool1) {
      return 2131230907;
    }
    localCharSequence2 = (CharSequence)"blue";
    boolean bool2 = am.b(localCharSequence1, localCharSequence2);
    if (bool2) {
      return 2131230906;
    }
    return 2131230905;
  }
  
  private final boolean p()
  {
    d locald1 = r;
    int i1 = locald1.h();
    boolean bool1 = true;
    int i2 = 24;
    if (i1 >= i2)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      locald1 = null;
    }
    d locald2 = r;
    boolean bool2 = locald2.c();
    if ((i1 != 0) && (!bool2)) {
      return bool1;
    }
    return false;
  }
  
  public final void a()
  {
    Object localObject1 = (l)b;
    if (localObject1 != null)
    {
      Object localObject2 = k;
      Object localObject3 = l;
      com.truecaller.ads.k localk = null;
      localObject2 = ((com.truecaller.ads.provider.f)localObject2).a((com.truecaller.ads.k)localObject3, 0);
      if (localObject2 != null)
      {
        localObject3 = k;
        localk = l;
        Object localObject4 = this;
        localObject4 = (g)this;
        ((com.truecaller.ads.provider.f)localObject3).b(localk, (g)localObject4);
        ((l)localObject1).a((com.truecaller.ads.provider.holders.e)localObject2);
        localObject1 = d;
        if (localObject1 != null) {
          ((com.truecaller.ads.provider.holders.e)localObject1).d();
        }
        d = ((com.truecaller.ads.provider.holders.e)localObject2);
        return;
      }
      return;
    }
  }
  
  public final void a(int paramInt) {}
  
  public final void a(com.truecaller.ads.provider.holders.e parame, int paramInt)
  {
    c.g.b.k.b(parame, "ad");
  }
  
  public final void a(m paramm)
  {
    c.g.b.k.b(paramm, "blockingSwitch");
    m.f localf = m.f.h;
    boolean bool = c.g.b.k.a(paramm, localf);
    if (bool)
    {
      paramm = (l)b;
      if (paramm != null)
      {
        paramm.r();
        return;
      }
    }
  }
  
  public final void a(m paramm, boolean paramBoolean)
  {
    c.g.b.k.b(paramm, "blockingSwitch");
    Object localObject1 = m.a.h;
    boolean bool1 = c.g.b.k.a(paramm, localObject1);
    Object localObject2;
    if (bool1)
    {
      paramm = m;
      bool2 = paramm.d();
      if (!bool2)
      {
        paramm = (l)b;
        if (paramm != null)
        {
          localObject2 = PremiumPresenterView.LaunchContext.TOPSPAMMER_UPDATE;
          paramm.a((PremiumPresenterView.LaunchContext)localObject2);
        }
      }
      else
      {
        paramm = h;
        localObject2 = Boolean.valueOf(paramBoolean);
        paramm.a((Boolean)localObject2);
      }
      m();
      return;
    }
    localObject1 = m.h.h;
    bool1 = c.g.b.k.a(paramm, localObject1);
    boolean bool3 = true;
    if (bool1)
    {
      paramm = n.L();
      bool2 = paramm.a();
      if (bool2)
      {
        paramm = m;
        bool2 = paramm.d();
        if ((!bool2) && (paramBoolean))
        {
          paramm = (l)b;
          if (paramm != null)
          {
            localObject2 = PremiumPresenterView.LaunchContext.BLOCK_TOP_SPAMMERS;
            paramm.a((PremiumPresenterView.LaunchContext)localObject2);
          }
          m();
          return;
        }
      }
      h.f(paramBoolean);
      h.g(bool3);
      paramm = FilterSettingsUploadWorker.d;
      FilterSettingsUploadWorker.a.a(o);
      n();
      a(paramBoolean, "BLOCKSETTINGS_BlockSpammers");
      return;
    }
    localObject1 = m.g.h;
    bool1 = c.g.b.k.a(paramm, localObject1);
    if (bool1)
    {
      paramm = n.N();
      bool2 = paramm.a();
      if (bool2)
      {
        paramm = m;
        bool2 = paramm.d();
        if ((!bool2) && (paramBoolean))
        {
          paramm = (l)b;
          if (paramm != null)
          {
            localObject2 = PremiumPresenterView.LaunchContext.BLOCK_NON_PHONEBOOK;
            paramm.a((PremiumPresenterView.LaunchContext)localObject2);
          }
          m();
          return;
        }
      }
      h.b(paramBoolean);
      h.g(bool3);
      paramm = FilterSettingsUploadWorker.d;
      FilterSettingsUploadWorker.a.a(o);
      a(paramBoolean, "BLOCKSETTINGS_BlockNonPhonebook");
      return;
    }
    localObject1 = m.d.h;
    bool1 = c.g.b.k.a(paramm, localObject1);
    if (bool1)
    {
      paramm = n.P();
      bool2 = paramm.a();
      if (bool2)
      {
        paramm = m;
        bool2 = paramm.d();
        if ((!bool2) && (paramBoolean))
        {
          paramm = (l)b;
          if (paramm != null)
          {
            localObject2 = PremiumPresenterView.LaunchContext.BLOCK_FOREIGN_NUMBERS;
            paramm.a((PremiumPresenterView.LaunchContext)localObject2);
          }
          m();
          return;
        }
      }
      h.c(paramBoolean);
      h.g(bool3);
      paramm = FilterSettingsUploadWorker.d;
      FilterSettingsUploadWorker.a.a(o);
      a(paramBoolean, "BLOCKSETTINGS_BlockForeignNumbers");
      return;
    }
    localObject1 = m.f.h;
    bool1 = c.g.b.k.a(paramm, localObject1);
    if (bool1)
    {
      paramm = n.R();
      bool2 = paramm.a();
      if (bool2)
      {
        paramm = m;
        bool2 = paramm.d();
        if ((!bool2) && (paramBoolean))
        {
          paramm = (l)b;
          if (paramm != null)
          {
            localObject2 = PremiumPresenterView.LaunchContext.BLOCK_NEIGHBOUR_SPOOFING;
            paramm.a((PremiumPresenterView.LaunchContext)localObject2);
          }
          m();
          return;
        }
      }
      h.d(paramBoolean);
      h.g(bool3);
      paramm = FilterSettingsUploadWorker.d;
      FilterSettingsUploadWorker.a.a(o);
      a(paramBoolean, "BLOCKSETTINGS_BlockNeighbourSpoofing");
      return;
    }
    localObject1 = m.i.h;
    bool1 = c.g.b.k.a(paramm, localObject1);
    if (bool1)
    {
      paramm = n.K();
      bool2 = paramm.a();
      if (bool2)
      {
        paramm = m;
        bool2 = paramm.d();
        if ((!bool2) && (paramBoolean))
        {
          paramm = (l)b;
          if (paramm != null)
          {
            localObject2 = PremiumPresenterView.LaunchContext.BLOCK_HIDDEN_NUMBERS;
            paramm.a((PremiumPresenterView.LaunchContext)localObject2);
          }
          m();
          return;
        }
      }
      h.a(paramBoolean);
      h.g(bool3);
      paramm = FilterSettingsUploadWorker.d;
      FilterSettingsUploadWorker.a.a(o);
      a(paramBoolean, "BLOCKSETTINGS_BlockHiddenNumbers");
      return;
    }
    localObject1 = m.e.h;
    bool1 = c.g.b.k.a(paramm, localObject1);
    if (bool1)
    {
      paramm = n.S();
      bool2 = paramm.a();
      if (bool2)
      {
        paramm = m;
        bool2 = paramm.d();
        if ((!bool2) && (paramBoolean))
        {
          paramm = (l)b;
          if (paramm != null)
          {
            localObject2 = PremiumPresenterView.LaunchContext.BLOCK_INDIAN_REGISTERED_TELEMARKETERS;
            paramm.a((PremiumPresenterView.LaunchContext)localObject2);
          }
          m();
          return;
        }
      }
      h.e(paramBoolean);
      h.g(bool3);
      paramm = FilterSettingsUploadWorker.d;
      FilterSettingsUploadWorker.a.a(o);
      a(paramBoolean, "BLOCKSETTINGS_BlockIndianTelemarketers");
      return;
    }
    localObject1 = m.c.h;
    boolean bool2 = c.g.b.k.a(paramm, localObject1);
    if (bool2)
    {
      paramm = m;
      bool2 = paramm.d();
      if ((!bool2) && (paramBoolean))
      {
        paramm = (l)b;
        if (paramm != null)
        {
          localObject2 = PremiumPresenterView.LaunchContext.BLOCK_EXTENDED_TOP_SPAMMER_LIST;
          paramm.a((PremiumPresenterView.LaunchContext)localObject2);
        }
      }
      m();
    }
  }
  
  public final void b()
  {
    Object localObject = i;
    boolean bool = ((com.truecaller.tcpermissions.l)localObject).f();
    if (!bool)
    {
      localObject = b;
      if (localObject != null)
      {
        localObject = (l)b;
        if (localObject != null) {
          ((l)localObject).u();
        }
        localObject = (l)b;
        if (localObject != null)
        {
          ((l)localObject).v();
          return;
        }
        return;
      }
    }
    n();
    m();
  }
  
  public final void b(m paramm)
  {
    c.g.b.k.b(paramm, "blockingSwitch");
    Object localObject = m.f.h;
    boolean bool = c.g.b.k.a(paramm, localObject);
    if (bool)
    {
      paramm = h.f();
      String str;
      int i1;
      if (paramm != null)
      {
        localObject = p;
        str = "it";
        c.g.b.k.a(paramm, str);
        i1 = paramm.intValue();
        int i2 = ((r)localObject).b();
        i1 -= i2;
        paramm = Integer.valueOf(i1);
      }
      else
      {
        i1 = 0;
        paramm = null;
      }
      localObject = (l)b;
      if (localObject != null)
      {
        str = p.a();
        ((l)localObject).a(paramm, str);
        return;
      }
    }
  }
  
  protected final void j()
  {
    l locall = (l)b;
    if (locall != null)
    {
      PremiumPresenterView.LaunchContext localLaunchContext = PremiumPresenterView.LaunchContext.BLOCK;
      locall.a(localLaunchContext);
      return;
    }
  }
  
  public final void k()
  {
    Object localObject1 = r;
    boolean bool = ((d)localObject1).c();
    if (bool) {
      return;
    }
    localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("PermissionChanged");
    Object localObject2 = ((e.a)localObject1).a("Context", "blockView").a("Permission", "DialerApp");
    String str1 = "Asked";
    ((e.a)localObject2).a("State", str1);
    localObject2 = j;
    localObject1 = ((e.a)localObject1).a();
    String str2 = "event.build()";
    c.g.b.k.a(localObject1, str2);
    ((com.truecaller.analytics.b)localObject2).b((com.truecaller.analytics.e)localObject1);
    localObject1 = (l)b;
    if (localObject1 != null)
    {
      ((l)localObject1).x();
      return;
    }
  }
  
  public final void y_()
  {
    a locala = a;
    if (locala != null) {
      locala.a();
    }
    locala = null;
    a = null;
    Object localObject1 = k;
    com.truecaller.ads.k localk = l;
    Object localObject2 = this;
    localObject2 = (g)this;
    ((com.truecaller.ads.provider.f)localObject1).b(localk, (g)localObject2);
    localObject1 = d;
    if (localObject1 != null) {
      ((com.truecaller.ads.provider.holders.e)localObject1).d();
    }
    d = null;
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */