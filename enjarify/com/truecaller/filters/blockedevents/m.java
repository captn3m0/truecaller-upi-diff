package com.truecaller.filters.blockedevents;

import java.util.List;

public abstract class m
{
  public static final List f;
  public static final m.b g;
  final int a;
  final int b;
  final int c;
  final boolean d;
  final boolean e;
  
  static
  {
    Object localObject = new com/truecaller/filters/blockedevents/m$b;
    ((m.b)localObject).<init>((byte)0);
    g = (m.b)localObject;
    localObject = new m[8];
    m localm1 = (m)m.a.h;
    localObject[0] = localm1;
    m localm2 = (m)m.c.h;
    localObject[1] = localm2;
    localm2 = (m)m.h.h;
    localObject[2] = localm2;
    localm2 = (m)m.i.h;
    localObject[3] = localm2;
    localm2 = (m)m.d.h;
    localObject[4] = localm2;
    localm2 = (m)m.g.h;
    localObject[5] = localm2;
    localm2 = (m)m.f.h;
    localObject[6] = localm2;
    localm2 = (m)m.e.h;
    localObject[7] = localm2;
    f = c.a.m.b((Object[])localObject);
  }
  
  private m(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramInt3;
    d = paramBoolean1;
    e = paramBoolean2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */