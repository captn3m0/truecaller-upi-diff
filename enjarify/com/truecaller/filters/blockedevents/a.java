package com.truecaller.filters.blockedevents;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import c.a.ae;
import c.g.b.k;
import c.k.i;
import c.u;
import com.truecaller.R.id;
import com.truecaller.bk;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public final class a
  extends AppCompatDialogFragment
  implements View.OnClickListener, RadioGroup.OnCheckedChangeListener
{
  public static final a.a b;
  public b a;
  private int c;
  private String d;
  private HashMap e;
  
  static
  {
    a.a locala = new com/truecaller/filters/blockedevents/a$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final a a(Integer paramInteger, String paramString)
  {
    k.b(paramString, "phoneNumber");
    a locala = new com/truecaller/filters/blockedevents/a;
    locala.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    if (paramInteger != null)
    {
      int i = ((Number)paramInteger).intValue();
      String str = "matching_digits";
      paramInteger = (Serializable)Integer.valueOf(i);
      localBundle.putSerializable(str, paramInteger);
    }
    paramString = (Serializable)paramString;
    localBundle.putSerializable("phone_number", paramString);
    locala.setArguments(localBundle);
    return locala;
  }
  
  private final void a()
  {
    int i = R.id.titleTextView;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "titleTextView");
    Object localObject = new Object[1];
    String str = String.valueOf(c());
    localObject[0] = str;
    localObject = (CharSequence)getString(2131886163, (Object[])localObject);
    localTextView.setText((CharSequence)localObject);
  }
  
  private final void b()
  {
    int i = c();
    Object localObject1 = d;
    if (localObject1 != null)
    {
      localObject1 = (Iterable)c.n.m.g((CharSequence)localObject1);
      Object localObject2 = (Collection)c.a.m.d((Iterable)localObject1, i);
      localObject1 = (Iterable)c.a.m.c((Iterable)localObject1, i);
      Object localObject3 = new java/util/ArrayList;
      int j = c.a.m.a((Iterable)localObject1, 10);
      ((ArrayList)localObject3).<init>(j);
      localObject3 = (Collection)localObject3;
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        ((Character)((Iterator)localObject1).next()).charValue();
        localObject4 = "*";
        ((Collection)localObject3).add(localObject4);
      }
      localObject3 = (Iterable)localObject3;
      localObject1 = c.a.m.c((Collection)localObject2, (Iterable)localObject3);
      localObject2 = localObject1;
      localObject2 = (Iterable)localObject1;
      localObject3 = (CharSequence)" ";
      boolean bool1 = false;
      Object localObject4 = null;
      int k = 0;
      int m = 0;
      int n = 62;
      localObject1 = c.a.m.a((Iterable)localObject2, (CharSequence)localObject3, null, null, 0, null, null, n);
      localObject2 = new android/text/SpannableString;
      localObject1 = (CharSequence)localObject1;
      ((SpannableString)localObject2).<init>((CharSequence)localObject1);
      localObject1 = null;
      localObject5 = ((Iterable)i.b(0, i)).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject5).hasNext();
        if (!bool2) {
          break;
        }
        localObject3 = localObject5;
        localObject3 = (ae)localObject5;
        int i1 = ((ae)localObject3).a() * 2;
        localObject4 = new android/text/style/UnderlineSpan;
        ((UnderlineSpan)localObject4).<init>();
        k = i1 + 1;
        m = 17;
        ((SpannableString)localObject2).setSpan(localObject4, i1, k, m);
      }
      i = R.id.phoneNumberTextView;
      localObject5 = (TextView)a(i);
      k.a(localObject5, "phoneNumberTextView");
      ((TextView)localObject5).setVisibility(0);
      i = R.id.phoneNumberTextView;
      localObject5 = (TextView)a(i);
      k.a(localObject5, "phoneNumberTextView");
      localObject2 = (CharSequence)localObject2;
      ((TextView)localObject5).setText((CharSequence)localObject2);
      return;
    }
    i = R.id.phoneNumberTextView;
    Object localObject5 = (TextView)a(i);
    k.a(localObject5, "phoneNumberTextView");
    ((TextView)localObject5).setVisibility(8);
  }
  
  private final int c()
  {
    int i = R.id.radioGroup;
    Object localObject1 = (RadioGroup)a(i);
    Object localObject2 = "radioGroup";
    k.a(localObject1, (String)localObject2);
    i = ((RadioGroup)localObject1).getCheckedRadioButtonId();
    switch (i)
    {
    default: 
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Checked radio button id ");
      int j = R.id.radioGroup;
      localObject2 = (RadioGroup)a(j);
      k.a(localObject2, "radioGroup");
      j = ((RadioGroup)localObject2).getCheckedRadioButtonId();
      ((StringBuilder)localObject1).append(j);
      ((StringBuilder)localObject1).append(" has no corresponding matching digits option.");
      localObject1 = ((StringBuilder)localObject1).toString();
      localObject2 = new java/lang/IllegalStateException;
      localObject1 = localObject1.toString();
      ((IllegalStateException)localObject2).<init>((String)localObject1);
      throw ((Throwable)localObject2);
    case 2131364058: 
      return 5;
    case 2131364057: 
      return 6;
    }
    return 4;
  }
  
  public final void onCheckedChanged(RadioGroup paramRadioGroup, int paramInt)
  {
    a();
    b();
  }
  
  public final void onClick(View paramView)
  {
    Object localObject = "v";
    k.b(paramView, (String)localObject);
    int i = paramView.getId();
    int j = 2131362890;
    if (i == j)
    {
      i = c();
      j = c;
      if (i != j)
      {
        localObject = a;
        if (localObject == null)
        {
          String str = "presenter";
          k.a(str);
        }
        ((b)localObject).a(i);
      }
    }
    dismiss();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getArguments();
    int i = 5;
    if (paramBundle != null)
    {
      String str1 = "matching_digits";
      i = paramBundle.getInt(str1, i);
    }
    c = i;
    paramBundle = getArguments();
    if (paramBundle != null)
    {
      String str2 = "phone_number";
      paramBundle = paramBundle.getString(str2);
    }
    else
    {
      paramBundle = null;
    }
    d = paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = 2131558561;
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    paramViewGroup = getDialog();
    if (paramViewGroup != null)
    {
      paramViewGroup = paramViewGroup.getWindow();
      if (paramViewGroup != null)
      {
        Object localObject = new android/graphics/drawable/ColorDrawable;
        ((ColorDrawable)localObject).<init>(0);
        localObject = (Drawable)localObject;
        paramViewGroup.setBackgroundDrawable((Drawable)localObject);
      }
    }
    return paramLayoutInflater;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    int i = R.id.radioButtonOptionOne;
    paramView = (RadioButton)a(i);
    k.a(paramView, "radioButtonOptionOne");
    paramBundle = (CharSequence)"4";
    paramView.setText(paramBundle);
    i = R.id.radioButtonOptionTwo;
    paramView = (RadioButton)a(i);
    k.a(paramView, "radioButtonOptionTwo");
    paramBundle = (CharSequence)"5";
    paramView.setText(paramBundle);
    i = R.id.radioButtonOptionThree;
    paramView = (RadioButton)a(i);
    k.a(paramView, "radioButtonOptionThree");
    paramBundle = (CharSequence)"6";
    paramView.setText(paramBundle);
    i = R.id.radioGroup;
    paramView = (RadioGroup)a(i);
    paramBundle = this;
    paramBundle = (RadioGroup.OnCheckedChangeListener)this;
    paramView.setOnCheckedChangeListener(paramBundle);
    i = R.id.doneTextView;
    paramView = (TextView)a(i);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    paramView.setOnClickListener(paramBundle);
    i = R.id.cancelTextView;
    paramView = (TextView)a(i);
    paramView.setOnClickListener(paramBundle);
    i = c;
    int j;
    switch (i)
    {
    default: 
      paramView = new java/lang/StringBuilder;
      paramView.<init>("Current matching digits ");
      j = c;
      paramView.append(j);
      paramView.append(" has no corresponding radio button option to check.");
      paramView = paramView.toString();
      paramBundle = new java/lang/IllegalStateException;
      paramView = paramView.toString();
      paramBundle.<init>(paramView);
      throw ((Throwable)paramBundle);
    case 6: 
      i = R.id.radioGroup;
      paramView = (RadioGroup)a(i);
      j = 2131364057;
      paramView.check(j);
      break;
    case 5: 
      i = R.id.radioGroup;
      paramView = (RadioGroup)a(i);
      j = 2131364058;
      paramView.check(j);
      break;
    case 4: 
      i = R.id.radioGroup;
      paramView = (RadioGroup)a(i);
      j = 2131364056;
      paramView.check(j);
    }
    a();
    b();
    paramView = r.a();
    paramBundle = requireContext();
    String str = "requireContext()";
    k.a(paramBundle, str);
    paramBundle = paramBundle.getApplicationContext();
    if (paramBundle != null)
    {
      paramBundle = ((bk)paramBundle).a();
      paramView = paramView.a(paramBundle);
      paramBundle = new com/truecaller/filters/blockedevents/f;
      paramBundle.<init>();
      paramView.a(paramBundle).a().a(this);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */