package com.truecaller.filters.blockedevents;

import com.truecaller.bp;
import dagger.a.g;

public final class r$a
{
  private f a;
  private bp b;
  
  public final d a()
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = new com/truecaller/filters/blockedevents/f;
      ((f)localObject).<init>();
      a = ((f)localObject);
    }
    g.a(b, bp.class);
    localObject = new com/truecaller/filters/blockedevents/r;
    f localf = a;
    bp localbp = b;
    ((r)localObject).<init>(localf, localbp, (byte)0);
    return (d)localObject;
  }
  
  public final a a(bp parambp)
  {
    parambp = (bp)g.a(parambp);
    b = parambp;
    return this;
  }
  
  public final a a(f paramf)
  {
    paramf = (f)g.a(paramf);
    a = paramf;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.r.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */