package com.truecaller.filters.blockedevents.b;

import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.filters.s;
import org.c.a.a.a.k;

final class f
  extends e
{
  private final i a;
  private final com.truecaller.androidactors.f c;
  
  f(i parami, com.truecaller.androidactors.f paramf)
  {
    a = parami;
    c = paramf;
  }
  
  final void a()
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    ((g)b).b();
    ((g)b).a(false);
    String str = ((g)b).a();
    localObject1 = c.a();
    Object localObject2 = localObject1;
    localObject2 = (s)localObject1;
    TruecallerContract.Filters.EntityType localEntityType = TruecallerContract.Filters.EntityType.UNKNOWN;
    Object localObject3 = str;
    localObject1 = ((s)localObject2).a(str, "OTHER", str, "blockView", false, localEntityType);
    localObject2 = a;
    localObject3 = new com/truecaller/filters/blockedevents/b/-$$Lambda$JXHYtM7D9gumGNzDwdZPnCsvOAg;
    ((-..Lambda.JXHYtM7D9gumGNzDwdZPnCsvOAg)localObject3).<init>(this);
    ((w)localObject1).a((i)localObject2, (ac)localObject3);
  }
  
  final void a(String paramString)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (g)b;
      boolean bool = k.b(paramString) ^ true;
      ((g)localObject).a(bool);
    }
  }
  
  final void a(boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject != null)
    {
      ((g)b).c();
      localObject = (g)b;
      ((g)localObject).d();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */