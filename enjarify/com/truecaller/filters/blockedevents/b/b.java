package com.truecaller.filters.blockedevents.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import com.truecaller.bk;
import com.truecaller.bp;

public final class b
  extends Fragment
  implements g
{
  e a;
  private EditText b;
  private View c;
  
  public final String a()
  {
    return b.getText().toString();
  }
  
  public final void a(boolean paramBoolean)
  {
    c.setEnabled(paramBoolean);
  }
  
  public final void b()
  {
    b.setEnabled(false);
  }
  
  public final void c()
  {
    Toast.makeText(getContext(), 2131886138, 0).show();
  }
  
  public final void d()
  {
    getActivity().finish();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = new com/truecaller/filters/blockedevents/b/h$a;
    paramBundle.<init>((byte)0);
    Object localObject = (bp)dagger.a.g.a(((bk)getContext().getApplicationContext()).a());
    b = ((bp)localObject);
    localObject = a;
    if (localObject == null)
    {
      localObject = new com/truecaller/filters/blockedevents/b/c;
      ((c)localObject).<init>();
      a = ((c)localObject);
    }
    dagger.a.g.a(b, bp.class);
    localObject = new com/truecaller/filters/blockedevents/b/h;
    c localc = a;
    paramBundle = b;
    ((h)localObject).<init>(localc, paramBundle, (byte)0);
    ((a)localObject).a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131558661, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    a.y_();
    super.onDestroy();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (AppCompatActivity)getActivity();
    int i = 2131364907;
    Toolbar localToolbar = (Toolbar)paramView.findViewById(i);
    Object localObject = getContext();
    int j = 2131233794;
    int k = 2130969592;
    localObject = com.truecaller.utils.ui.b.a((Context)localObject, j, k);
    localToolbar.setNavigationIcon((Drawable)localObject);
    paramBundle.setSupportActionBar(localToolbar);
    paramBundle = paramBundle.getSupportActionBar();
    if (paramBundle != null)
    {
      paramBundle.setTitle(2131886133);
      i = 1;
      paramBundle.setDisplayHomeAsUpEnabled(i);
    }
    paramBundle = (EditText)paramView.findViewById(2131363791);
    b = paramBundle;
    paramView = paramView.findViewById(2131362123);
    c = paramView;
    a.a(this);
    paramView = c;
    paramBundle = new com/truecaller/filters/blockedevents/b/-$$Lambda$b$SAcZSjJ5rLY82U3NTmSZuJs5Fjg;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = b;
    paramBundle = new com/truecaller/filters/blockedevents/b/b$1;
    paramBundle.<init>(this);
    paramView.addTextChangedListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */