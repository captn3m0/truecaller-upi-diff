package com.truecaller.filters.blockedevents;

import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.R.id;

final class p$b
  implements View.OnClickListener
{
  p$b(q paramq) {}
  
  public final void onClick(View paramView)
  {
    paramView = a;
    int i = R.id.itemSwitch;
    ((SwitchCompat)paramView.a(i)).performClick();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.p.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */