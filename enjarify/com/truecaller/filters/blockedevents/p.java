package com.truecaller.filters.blockedevents;

import android.support.v7.widget.RecyclerView.Adapter;
import c.a.y;
import c.g.b.k;
import c.g.b.n;
import c.g.b.w;
import c.i.a;
import c.i.d;
import c.l.b;
import c.l.g;
import java.util.List;

public final class p
  extends RecyclerView.Adapter
{
  private final d b;
  private final o c;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/o;
    b localb = w.a(p.class);
    ((c.g.b.o)localObject).<init>(localb, "switches", "getSwitches()Ljava/util/List;");
    localObject = (g)w.a((n)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public p(o paramo)
  {
    c = paramo;
    paramo = a.a;
    paramo = (List)y.a;
    Object localObject = new com/truecaller/filters/blockedevents/p$a;
    ((p.a)localObject).<init>(paramo, paramo, this);
    localObject = (d)localObject;
    b = ((d)localObject);
  }
  
  private List a()
  {
    d locald = b;
    g localg = a[0];
    return (List)locald.a(localg);
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "<set-?>");
    d locald = b;
    g localg = a[0];
    locald.a(localg, paramList);
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */