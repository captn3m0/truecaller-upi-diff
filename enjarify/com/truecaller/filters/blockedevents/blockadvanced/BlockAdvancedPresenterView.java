package com.truecaller.filters.blockedevents.blockadvanced;

abstract interface BlockAdvancedPresenterView
{
  public abstract String a();
  
  public abstract void a(boolean paramBoolean);
  
  public abstract BlockAdvancedPresenterView.AdvancedType b();
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.blockadvanced.BlockAdvancedPresenterView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */