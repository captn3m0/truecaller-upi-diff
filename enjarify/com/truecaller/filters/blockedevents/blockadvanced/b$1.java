package com.truecaller.filters.blockedevents.blockadvanced;

import android.text.Editable;
import android.text.TextWatcher;

final class b$1
  implements TextWatcher
{
  b$1(b paramb) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    e locale = a.a;
    paramEditable = paramEditable.toString();
    locale.a(paramEditable);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.blockadvanced.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */