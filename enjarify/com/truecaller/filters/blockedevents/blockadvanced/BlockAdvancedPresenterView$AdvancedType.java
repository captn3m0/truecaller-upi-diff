package com.truecaller.filters.blockedevents.blockadvanced;

public enum BlockAdvancedPresenterView$AdvancedType
{
  static
  {
    Object localObject = new com/truecaller/filters/blockedevents/blockadvanced/BlockAdvancedPresenterView$AdvancedType;
    ((AdvancedType)localObject).<init>("STARTS_WITH", 0);
    STARTS_WITH = (AdvancedType)localObject;
    localObject = new com/truecaller/filters/blockedevents/blockadvanced/BlockAdvancedPresenterView$AdvancedType;
    int i = 1;
    ((AdvancedType)localObject).<init>("CONTAINS", i);
    CONTAINS = (AdvancedType)localObject;
    localObject = new com/truecaller/filters/blockedevents/blockadvanced/BlockAdvancedPresenterView$AdvancedType;
    int j = 2;
    ((AdvancedType)localObject).<init>("ENDS_WITH", j);
    ENDS_WITH = (AdvancedType)localObject;
    localObject = new AdvancedType[3];
    AdvancedType localAdvancedType = STARTS_WITH;
    localObject[0] = localAdvancedType;
    localAdvancedType = CONTAINS;
    localObject[i] = localAdvancedType;
    localAdvancedType = ENDS_WITH;
    localObject[j] = localAdvancedType;
    $VALUES = (AdvancedType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.blockadvanced.BlockAdvancedPresenterView.AdvancedType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */