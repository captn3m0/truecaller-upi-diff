package com.truecaller.filters.blockedevents.blockadvanced;

import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.filters.s;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import org.c.a.a.a.k;

final class f
  extends e
{
  private final i a;
  private final com.truecaller.androidactors.f c;
  
  f(i parami, com.truecaller.androidactors.f paramf)
  {
    a = parami;
    c = paramf;
  }
  
  final void a()
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    ((BlockAdvancedPresenterView)b).c();
    ((BlockAdvancedPresenterView)b).a(false);
    localObject1 = ((BlockAdvancedPresenterView)b).a();
    Object localObject2 = f.1.a;
    Object localObject3 = ((BlockAdvancedPresenterView)b).b();
    int i = ((BlockAdvancedPresenterView.AdvancedType)localObject3).ordinal();
    int j = localObject2[i];
    switch (j)
    {
    default: 
      AssertionUtil.AlwaysFatal.fail(new String[] { "Unknown wildcard type" });
      return;
    case 3: 
      localObject2 = TruecallerContract.Filters.WildCardType.END;
      break;
    case 2: 
      localObject2 = TruecallerContract.Filters.WildCardType.CONTAIN;
      break;
    case 1: 
      localObject2 = TruecallerContract.Filters.WildCardType.START;
    }
    localObject1 = ((s)c.a()).a((String)localObject1, null, (TruecallerContract.Filters.WildCardType)localObject2, "blockView");
    localObject2 = a;
    localObject3 = new com/truecaller/filters/blockedevents/blockadvanced/-$$Lambda$e6QrfRx5hG-oVdVuB9kl8pnz7dM;
    ((-..Lambda.e6QrfRx5hG-oVdVuB9kl8pnz7dM)localObject3).<init>(this);
    ((w)localObject1).a((i)localObject2, (ac)localObject3);
  }
  
  final void a(String paramString)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (BlockAdvancedPresenterView)b;
      boolean bool = k.b(paramString) ^ true;
      ((BlockAdvancedPresenterView)localObject).a(bool);
    }
  }
  
  final void a(boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject != null)
    {
      ((BlockAdvancedPresenterView)b).d();
      localObject = (BlockAdvancedPresenterView)b;
      ((BlockAdvancedPresenterView)localObject).e();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.blockadvanced.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */