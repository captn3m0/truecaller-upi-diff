package com.truecaller.filters.blockedevents.blockadvanced;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.truecaller.bk;
import com.truecaller.bp;

public final class b
  extends Fragment
  implements BlockAdvancedPresenterView
{
  e a;
  private Spinner b;
  private EditText c;
  private View d;
  
  public final String a()
  {
    return c.getText().toString();
  }
  
  public final void a(boolean paramBoolean)
  {
    d.setEnabled(paramBoolean);
  }
  
  public final BlockAdvancedPresenterView.AdvancedType b()
  {
    BlockAdvancedPresenterView.AdvancedType[] arrayOfAdvancedType = BlockAdvancedPresenterView.AdvancedType.values();
    int i = b.getSelectedItemPosition();
    return arrayOfAdvancedType[i];
  }
  
  public final void c()
  {
    b.setEnabled(false);
    c.setEnabled(false);
  }
  
  public final void d()
  {
    Toast.makeText(getContext(), 2131886138, 0).show();
  }
  
  public final void e()
  {
    getActivity().finish();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = new com/truecaller/filters/blockedevents/blockadvanced/g$a;
    paramBundle.<init>((byte)0);
    Object localObject = (bp)dagger.a.g.a(((bk)getContext().getApplicationContext()).a());
    b = ((bp)localObject);
    localObject = a;
    if (localObject == null)
    {
      localObject = new com/truecaller/filters/blockedevents/blockadvanced/c;
      ((c)localObject).<init>();
      a = ((c)localObject);
    }
    dagger.a.g.a(b, bp.class);
    localObject = new com/truecaller/filters/blockedevents/blockadvanced/g;
    c localc = a;
    paramBundle = b;
    ((g)localObject).<init>(localc, paramBundle, (byte)0);
    ((a)localObject).a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131558659, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    a.y_();
    super.onDestroy();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (AppCompatActivity)getActivity();
    int i = 2131364907;
    Object localObject1 = (Toolbar)paramView.findViewById(i);
    Object localObject2 = getContext();
    int j = 2131233794;
    int k = 2130969592;
    localObject2 = com.truecaller.utils.ui.b.a((Context)localObject2, j, k);
    ((Toolbar)localObject1).setNavigationIcon((Drawable)localObject2);
    paramBundle.setSupportActionBar((Toolbar)localObject1);
    paramBundle = paramBundle.getSupportActionBar();
    if (paramBundle != null)
    {
      paramBundle.setTitle(2131886134);
      i = 1;
      paramBundle.setDisplayHomeAsUpEnabled(i);
    }
    paramBundle = (Spinner)paramView.findViewById(2131365391);
    b = paramBundle;
    paramBundle = (EditText)paramView.findViewById(2131363828);
    c = paramBundle;
    paramView = paramView.findViewById(2131362123);
    d = paramView;
    paramView = b;
    paramBundle = new android/widget/ArrayAdapter;
    localObject1 = getContext();
    String[] arrayOfString = getResources().getStringArray(2130903040);
    paramBundle.<init>((Context)localObject1, 17367049, arrayOfString);
    paramView.setAdapter(paramBundle);
    a.a(this);
    paramView = d;
    paramBundle = new com/truecaller/filters/blockedevents/blockadvanced/-$$Lambda$b$ZEO5duG9yEiiOzUTGJYCoSinkFE;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = c;
    paramBundle = new com/truecaller/filters/blockedevents/blockadvanced/b$1;
    paramBundle.<init>(this);
    paramView.addTextChangedListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedevents.blockadvanced.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */