package com.truecaller.filters;

import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d
{
  private final h a;
  private final Provider b;
  private final Provider c;
  
  private l(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramh;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static l a(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    l locall = new com/truecaller/filters/l;
    locall.<init>(paramh, paramProvider1, paramProvider2);
    return locall;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */