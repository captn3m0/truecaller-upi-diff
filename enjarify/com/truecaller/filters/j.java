package com.truecaller.filters;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final h a;
  private final Provider b;
  private final Provider c;
  
  private j(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramh;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static j a(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    j localj = new com/truecaller/filters/j;
    localj.<init>(paramh, paramProvider1, paramProvider2);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */