package com.truecaller.filters;

public enum FilterManager$FilterAction
{
  static
  {
    Object localObject = new com/truecaller/filters/FilterManager$FilterAction;
    ((FilterAction)localObject).<init>("NONE_FOUND", 0);
    NONE_FOUND = (FilterAction)localObject;
    localObject = new com/truecaller/filters/FilterManager$FilterAction;
    int i = 1;
    ((FilterAction)localObject).<init>("ALLOW_WHITELISTED", i);
    ALLOW_WHITELISTED = (FilterAction)localObject;
    localObject = new com/truecaller/filters/FilterManager$FilterAction;
    int j = 2;
    ((FilterAction)localObject).<init>("FILTER_BLACKLISTED", j);
    FILTER_BLACKLISTED = (FilterAction)localObject;
    localObject = new com/truecaller/filters/FilterManager$FilterAction;
    int k = 3;
    ((FilterAction)localObject).<init>("FILTER_DISABLED", k);
    FILTER_DISABLED = (FilterAction)localObject;
    localObject = new FilterAction[4];
    FilterAction localFilterAction = NONE_FOUND;
    localObject[0] = localFilterAction;
    localFilterAction = ALLOW_WHITELISTED;
    localObject[i] = localFilterAction;
    localFilterAction = FILTER_BLACKLISTED;
    localObject[j] = localFilterAction;
    localFilterAction = FILTER_DISABLED;
    localObject[k] = localFilterAction;
    $VALUES = (FilterAction[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.FilterManager.FilterAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */