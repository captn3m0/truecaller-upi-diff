package com.truecaller.filters;

import android.os.Bundle;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.log.AssertionUtil;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public abstract class a
  extends AppCompatActivity
{
  protected b a;
  
  protected final void a(b paramb)
  {
    a = paramb;
    paramb = getSupportFragmentManager().a();
    b localb = a;
    paramb.b(16908290, localb).c();
  }
  
  public void onBackPressed()
  {
    b localb = a;
    boolean bool = localb.q();
    if (!bool) {
      super.onBackPressed();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    boolean bool = ((com.truecaller.common.b.a)getApplication()).p();
    String[] arrayOfString = { "Users that have not signed-in are not supposed to have access to this activity" };
    AssertionUtil.isTrue(bool, arrayOfString);
    int i = aresId;
    setTheme(i);
    super.onCreate(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */