package com.truecaller.filters;

public enum FilterManager$ActionSource
{
  static
  {
    Object localObject = new com/truecaller/filters/FilterManager$ActionSource;
    ((ActionSource)localObject).<init>("NONE", 0);
    NONE = (ActionSource)localObject;
    localObject = new com/truecaller/filters/FilterManager$ActionSource;
    int i = 1;
    ((ActionSource)localObject).<init>("UNKNOWN", i);
    UNKNOWN = (ActionSource)localObject;
    localObject = new com/truecaller/filters/FilterManager$ActionSource;
    int j = 2;
    ((ActionSource)localObject).<init>("NON_PHONEBOOK", j);
    NON_PHONEBOOK = (ActionSource)localObject;
    localObject = new com/truecaller/filters/FilterManager$ActionSource;
    int k = 3;
    ((ActionSource)localObject).<init>("FOREIGN", k);
    FOREIGN = (ActionSource)localObject;
    localObject = new com/truecaller/filters/FilterManager$ActionSource;
    int m = 4;
    ((ActionSource)localObject).<init>("NEIGHBOUR_SPOOFING", m);
    NEIGHBOUR_SPOOFING = (ActionSource)localObject;
    localObject = new com/truecaller/filters/FilterManager$ActionSource;
    int n = 5;
    ((ActionSource)localObject).<init>("INDIAN_REGISTERED_TELEMARKETER", n);
    INDIAN_REGISTERED_TELEMARKETER = (ActionSource)localObject;
    localObject = new com/truecaller/filters/FilterManager$ActionSource;
    int i1 = 6;
    ((ActionSource)localObject).<init>("TOP_SPAMMER", i1);
    TOP_SPAMMER = (ActionSource)localObject;
    localObject = new com/truecaller/filters/FilterManager$ActionSource;
    int i2 = 7;
    ((ActionSource)localObject).<init>("CUSTOM_BLACKLIST", i2);
    CUSTOM_BLACKLIST = (ActionSource)localObject;
    localObject = new com/truecaller/filters/FilterManager$ActionSource;
    int i3 = 8;
    ((ActionSource)localObject).<init>("CUSTOM_WHITELIST", i3);
    CUSTOM_WHITELIST = (ActionSource)localObject;
    localObject = new com/truecaller/filters/FilterManager$ActionSource;
    int i4 = 9;
    ((ActionSource)localObject).<init>("OTHER", i4);
    OTHER = (ActionSource)localObject;
    localObject = new ActionSource[10];
    ActionSource localActionSource = NONE;
    localObject[0] = localActionSource;
    localActionSource = UNKNOWN;
    localObject[i] = localActionSource;
    localActionSource = NON_PHONEBOOK;
    localObject[j] = localActionSource;
    localActionSource = FOREIGN;
    localObject[k] = localActionSource;
    localActionSource = NEIGHBOUR_SPOOFING;
    localObject[m] = localActionSource;
    localActionSource = INDIAN_REGISTERED_TELEMARKETER;
    localObject[n] = localActionSource;
    localActionSource = TOP_SPAMMER;
    localObject[i1] = localActionSource;
    localActionSource = CUSTOM_BLACKLIST;
    localObject[i2] = localActionSource;
    localActionSource = CUSTOM_WHITELIST;
    localObject[i3] = localActionSource;
    localActionSource = OTHER;
    localObject[i4] = localActionSource;
    $VALUES = (ActionSource[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.FilterManager.ActionSource
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */