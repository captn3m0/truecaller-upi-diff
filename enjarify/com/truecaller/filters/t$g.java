package com.truecaller.filters;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.List;

final class t$g
  extends u
{
  private final List b;
  private final List c;
  private final List d;
  private final String e;
  private final String f;
  private final boolean g;
  
  private t$g(e parame, List paramList1, List paramList2, List paramList3, String paramString1, String paramString2, boolean paramBoolean)
  {
    super(parame);
    b = paramList1;
    c = paramList2;
    d = paramList3;
    e = paramString1;
    f = paramString2;
    g = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".whitelistAddresses(");
    Object localObject = b;
    int i = 1;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = c;
    int j = 2;
    localObject = a(localObject, j);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(e, j);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(f, j);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(g), j);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.t.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */