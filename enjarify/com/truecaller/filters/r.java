package com.truecaller.filters;

import c.n.k;
import com.truecaller.common.h.u;

public final class r
{
  private final com.truecaller.common.account.r a;
  private final u b;
  
  public r(com.truecaller.common.account.r paramr, u paramu)
  {
    a = paramr;
    b = paramu;
  }
  
  private final String c()
  {
    Object localObject = a.b();
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new java/lang/IllegalArgumentException;
    String str = "Account normalized number should not be null (because block options require a valid account)".toString();
    ((IllegalArgumentException)localObject).<init>(str);
    throw ((Throwable)localObject);
  }
  
  public final String a()
  {
    Object localObject1 = c();
    Object localObject2 = b;
    Object localObject3 = null;
    localObject1 = ((u)localObject2).a((String)localObject1, null);
    if (localObject1 != null)
    {
      localObject1 = (CharSequence)localObject1;
      localObject3 = new c/n/k;
      ((k)localObject3).<init>("[^\\d]");
      localObject2 = "";
      localObject3 = ((k)localObject3).a((CharSequence)localObject1, (String)localObject2);
    }
    if (localObject3 != null) {
      return (String)localObject3;
    }
    localObject1 = new java/lang/IllegalArgumentException;
    localObject2 = "Parsing of normalized account number to national failed".toString();
    ((IllegalArgumentException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final int b()
  {
    int i = c().length();
    int j = a().length();
    return i - j;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */