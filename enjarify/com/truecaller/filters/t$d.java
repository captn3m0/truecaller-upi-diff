package com.truecaller.filters;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;

final class t$d
  extends u
{
  private final String b;
  private final String c;
  private final TruecallerContract.Filters.WildCardType d;
  private final String e;
  
  private t$d(e parame, String paramString1, String paramString2, TruecallerContract.Filters.WildCardType paramWildCardType, String paramString3)
  {
    super(parame);
    b = paramString1;
    c = paramString2;
    d = paramWildCardType;
    e = paramString3;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".blacklistWildcard(");
    Object localObject = b;
    int i = 1;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = d;
    i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(e, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.t.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */