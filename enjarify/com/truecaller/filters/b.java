package com.truecaller.filters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.filters.blockedevents.BlockDialogActivity;
import com.truecaller.filters.blockedevents.BlockDialogActivity.DialogType;
import com.truecaller.ui.components.FloatingActionButton;
import com.truecaller.ui.components.FloatingActionButton.a;
import com.truecaller.ui.components.i;

public abstract class b
  extends Fragment
  implements e, FloatingActionButton.a
{
  protected FloatingActionButton a;
  protected Toolbar b;
  
  public final void a() {}
  
  public void a(int paramInt) {}
  
  public final void b()
  {
    FloatingActionButton localFloatingActionButton = a;
    Object localObject = com.truecaller.utils.ui.b.a(getContext(), 2131233881, 2130968965);
    localFloatingActionButton.setDrawable((Drawable)localObject);
    localFloatingActionButton = a;
    int i = com.truecaller.utils.ui.b.a(getContext(), 2130968964);
    localFloatingActionButton.setBackgroundColor(i);
    a.setFabActionListener(this);
    localFloatingActionButton = a;
    localObject = d();
    localFloatingActionButton.setMenuItems((i[])localObject);
    a.a(true);
  }
  
  protected final void b(int paramInt)
  {
    Object localObject = (AppCompatActivity)getActivity();
    AppCompatActivity localAppCompatActivity = (AppCompatActivity)getActivity();
    Toolbar localToolbar = b;
    localAppCompatActivity.setSupportActionBar(localToolbar);
    localObject = ((AppCompatActivity)localObject).getSupportActionBar();
    if (localObject != null)
    {
      boolean bool = true;
      ((ActionBar)localObject).setDisplayHomeAsUpEnabled(bool);
      ((ActionBar)localObject).setTitle(paramInt);
    }
  }
  
  public final void c()
  {
    a.a(false);
  }
  
  public final i[] d()
  {
    Object localObject = ((bk)getContext().getApplicationContext()).a().aF().u();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    int j = 2131886137;
    int k = 2131233888;
    int m = 2131886134;
    int n = 2131233882;
    int i1 = 2131886133;
    int i2 = 2131234560;
    int i3 = 3;
    int i4 = 2;
    int i5 = 1;
    i locali2;
    i locali3;
    if (bool)
    {
      int i = 4;
      localObject = new i[i];
      i locali1 = new com/truecaller/ui/components/i;
      int i6 = 2131234136;
      int i7 = 2131886128;
      locali1.<init>(i3, i6, i7);
      localObject[0] = locali1;
      locali1 = new com/truecaller/ui/components/i;
      locali1.<init>(0, i2, i1);
      localObject[i5] = locali1;
      locali2 = new com/truecaller/ui/components/i;
      locali2.<init>(i5, n, m);
      localObject[i4] = locali2;
      locali3 = new com/truecaller/ui/components/i;
      locali3.<init>(i4, k, j);
      localObject[i3] = locali3;
    }
    else
    {
      localObject = new i[i3];
      i locali4 = new com/truecaller/ui/components/i;
      locali4.<init>(0, i2, i1);
      localObject[0] = locali4;
      locali2 = new com/truecaller/ui/components/i;
      locali2.<init>(i5, n, m);
      localObject[i5] = locali2;
      locali3 = new com/truecaller/ui/components/i;
      locali3.<init>(i4, k, j);
      localObject[i4] = locali3;
    }
    return (i[])localObject;
  }
  
  public final void e()
  {
    getActivity().finish();
  }
  
  public final void f()
  {
    a.c();
  }
  
  public final boolean g()
  {
    return a.a;
  }
  
  public void h()
  {
    Context localContext = getContext();
    BlockDialogActivity.DialogType localDialogType = BlockDialogActivity.DialogType.NAME;
    BlockDialogActivity.a(localContext, localDialogType);
  }
  
  public void m()
  {
    Context localContext = getContext();
    BlockDialogActivity.DialogType localDialogType = BlockDialogActivity.DialogType.ADVANCED;
    BlockDialogActivity.a(localContext, localDialogType);
  }
  
  public void n()
  {
    Context localContext = getContext();
    BlockDialogActivity.DialogType localDialogType = BlockDialogActivity.DialogType.NUMBER;
    BlockDialogActivity.a(localContext, localDialogType);
  }
  
  public final void o()
  {
    Context localContext = getContext();
    BlockDialogActivity.DialogType localDialogType = BlockDialogActivity.DialogType.COUNTRY;
    BlockDialogActivity.a(localContext, localDialogType);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (FloatingActionButton)paramView.findViewById(2131363121);
    a = paramBundle;
    paramView = (Toolbar)paramView.findViewById(2131364907);
    b = paramView;
  }
  
  public final void p() {}
  
  public boolean q()
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */