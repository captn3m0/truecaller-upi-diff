package com.truecaller.filters;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.common.network.country.CountryListDto.a;

final class t$c
  extends u
{
  private final CountryListDto.a b;
  private final String c;
  
  private t$c(e parame, CountryListDto.a parama, String paramString)
  {
    super(parame);
    b = parama;
    c = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".blacklistCountry(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(c, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.t.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */