package com.truecaller.filters;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

final class q
  implements p
{
  private final SharedPreferences a;
  private final r b;
  
  q(SharedPreferences paramSharedPreferences, r paramr)
  {
    a = paramSharedPreferences;
    b = paramr;
  }
  
  public final void a(int paramInt)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putInt("filter_filteringNeighbourSpoofingMatchingDigits", paramInt);
    localEditor.apply();
  }
  
  public final void a(long paramLong)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putLong("filter_filterSyncLastUpdateTimestamp", paramLong);
    localEditor.apply();
  }
  
  public final void a(Boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    if (paramBoolean == null)
    {
      localEditor.remove("filter_autoUpdateTopSpammers").apply();
      return;
    }
    boolean bool = paramBoolean.booleanValue();
    localEditor.putBoolean("filter_autoUpdateTopSpammers", bool).apply();
  }
  
  public final void a(Integer paramInteger)
  {
    SharedPreferences.Editor localEditor = a.edit();
    if (paramInteger == null)
    {
      paramInteger = "filter_topSpammersMaxSize";
      localEditor.remove(paramInteger);
    }
    else
    {
      String str = "filter_topSpammersMaxSize";
      int i = paramInteger.intValue();
      localEditor.putInt(str, i);
    }
    localEditor.apply();
  }
  
  public final void a(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filteringUnknown", paramBoolean);
    localEditor.apply();
  }
  
  public final boolean a()
  {
    return a.getBoolean("filter_filteringUnknown", false);
  }
  
  public final void b(long paramLong)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putLong("filter_topSpammersSyncLastUpdateTimestamp", paramLong);
    localEditor.apply();
  }
  
  public final void b(Integer paramInteger)
  {
    SharedPreferences.Editor localEditor = a.edit();
    if (paramInteger == null)
    {
      paramInteger = "filter_topSpammersPremiumMaxSize";
      localEditor.remove(paramInteger);
    }
    else
    {
      String str = "filter_topSpammersPremiumMaxSize";
      int i = paramInteger.intValue();
      localEditor.putInt(str, i);
    }
    localEditor.apply();
  }
  
  public final void b(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filteringNonPhonebook", paramBoolean);
    localEditor.apply();
  }
  
  public final boolean b()
  {
    return a.getBoolean("filter_filteringNonPhonebook", false);
  }
  
  public final void c(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filteringForeignNumbers", paramBoolean);
    localEditor.apply();
  }
  
  public final boolean c()
  {
    return a.getBoolean("filter_filteringForeignNumbers", false);
  }
  
  public final void d(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    String str = "filter_filteringNeighbourSpoofing";
    localEditor.putBoolean(str, paramBoolean);
    localEditor.apply();
    if (paramBoolean)
    {
      Object localObject = f();
      if (localObject == null)
      {
        localObject = b;
        paramBoolean = ((r)localObject).b() + 5;
        a(paramBoolean);
      }
    }
  }
  
  public final boolean d()
  {
    return a.getBoolean("filter_filteringNeighbourSpoofing", false);
  }
  
  public final void e(boolean paramBoolean)
  {
    a.edit().putBoolean("filter_filteringIndianRegisteredTelemarketers", paramBoolean).apply();
  }
  
  public final boolean e()
  {
    return a.getBoolean("filter_filteringIndianRegisteredTelemarketers", false);
  }
  
  public final Integer f()
  {
    SharedPreferences localSharedPreferences = a;
    String str = "filter_filteringNeighbourSpoofingMatchingDigits";
    boolean bool = localSharedPreferences.contains(str);
    if (bool) {
      return Integer.valueOf(a.getInt("filter_filteringNeighbourSpoofingMatchingDigits", -1 << -1));
    }
    return null;
  }
  
  public final void f(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filteringTopSpammers", paramBoolean);
    localEditor.apply();
  }
  
  public final void g(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filterSettingsNeedsUpload", paramBoolean);
    localEditor.apply();
  }
  
  public final boolean g()
  {
    return a.getBoolean("filter_filteringTopSpammers", false);
  }
  
  public final void h(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_updateNeeded", paramBoolean);
    localEditor.apply();
  }
  
  public final boolean h()
  {
    return a.getBoolean("filter_filterSettingsNeedsUpload", false);
  }
  
  public final long i()
  {
    return a.getLong("filter_topSpammersSyncLastUpdateTimestamp", 0L);
  }
  
  public final boolean j()
  {
    long l1 = i();
    long l2 = System.currentTimeMillis() - l1;
    boolean bool1 = true;
    long l3 = 1209600000L;
    boolean bool2 = l2 < l3;
    int i;
    if (bool2) {
      i = 1;
    } else {
      i = 0;
    }
    SharedPreferences localSharedPreferences = a;
    String str = "filter_updateNeeded";
    boolean bool3 = localSharedPreferences.getBoolean(str, false);
    if ((!bool3) && (i != 0)) {
      return false;
    }
    return bool1;
  }
  
  public final Boolean k()
  {
    SharedPreferences localSharedPreferences = a;
    String str = "filter_autoUpdateTopSpammers";
    boolean bool = localSharedPreferences.contains(str);
    if (!bool) {
      return null;
    }
    return Boolean.valueOf(a.getBoolean("filter_autoUpdateTopSpammers", false));
  }
  
  public final int l()
  {
    return a.getInt("filter_topSpammersMaxSize", 2000);
  }
  
  public final int m()
  {
    return a.getInt("filter_topSpammersPremiumMaxSize", 2000);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */