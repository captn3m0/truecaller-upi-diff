package com.truecaller.filters;

import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import java.util.Collection;
import java.util.List;

public abstract interface FilterManager
{
  public abstract int a(List paramList, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean);
  
  public abstract int a(List paramList, String paramString1, String paramString2, String paramString3, boolean paramBoolean, TruecallerContract.Filters.WildCardType paramWildCardType, TruecallerContract.Filters.EntityType paramEntityType);
  
  public abstract int a(List paramList, String paramString1, String paramString2, boolean paramBoolean, TruecallerContract.Filters.WildCardType paramWildCardType, TruecallerContract.Filters.EntityType paramEntityType);
  
  public abstract g a(String paramString);
  
  public abstract g a(String paramString1, String paramString2, String paramString3, boolean paramBoolean);
  
  public abstract Collection a(String paramString1, String paramString2, boolean paramBoolean);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean);
  
  public abstract boolean a();
  
  public abstract g b(String paramString);
  
  public abstract boolean b();
  
  public abstract boolean c();
  
  public abstract void d();
}

/* Location:
 * Qualified Name:     com.truecaller.filters.FilterManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */