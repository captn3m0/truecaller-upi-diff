package com.truecaller.filters;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.content.TruecallerContract.Filters.EntityType;

final class t$a
  extends u
{
  private final String b;
  private final String c;
  private final String d;
  private final String e;
  private final boolean f;
  private final TruecallerContract.Filters.EntityType g;
  
  private t$a(e parame, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, TruecallerContract.Filters.EntityType paramEntityType)
  {
    super(parame);
    b = paramString1;
    c = paramString2;
    d = paramString3;
    e = paramString4;
    f = paramBoolean;
    g = paramEntityType;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".blacklistAddress(");
    String str = b;
    int i = 1;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = c;
    int j = 2;
    str = a(str, j);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(d, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(e, j);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(Boolean.valueOf(f), j);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(g, j);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.t.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */