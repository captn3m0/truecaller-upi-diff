package com.truecaller.filters.a;

import android.database.Cursor;
import android.database.CursorWrapper;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;

public final class c
  extends CursorWrapper
  implements b
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  
  public c(Cursor paramCursor)
  {
    super(paramCursor);
    int j = paramCursor.getColumnIndexOrThrow("_id");
    a = j;
    j = paramCursor.getColumnIndexOrThrow("rule");
    b = j;
    j = paramCursor.getColumnIndexOrThrow("sync_state");
    c = j;
    j = paramCursor.getColumnIndexOrThrow("wildcard_type");
    d = j;
    j = paramCursor.getColumnIndexOrThrow("label");
    e = j;
    j = paramCursor.getColumnIndexOrThrow("value");
    f = j;
    j = paramCursor.getColumnIndexOrThrow("tracking_type");
    g = j;
    j = paramCursor.getColumnIndexOrThrow("tracking_source");
    h = j;
    int k = paramCursor.getColumnIndexOrThrow("entity_type");
    i = k;
  }
  
  public final a a()
  {
    a.a locala = new com/truecaller/filters/a/a$a;
    locala.<init>();
    int j = a;
    long l = getLong(j);
    a = l;
    j = b;
    j = getInt(j);
    b = j;
    j = c;
    j = getInt(j);
    c = j;
    j = d;
    Object localObject = TruecallerContract.Filters.WildCardType.valueOfType(getInt(j));
    i = ((TruecallerContract.Filters.WildCardType)localObject);
    j = e;
    localObject = getString(j);
    d = ((String)localObject);
    j = f;
    localObject = getString(j);
    e = ((String)localObject);
    j = g;
    localObject = getString(j);
    f = ((String)localObject);
    j = h;
    localObject = getString(j);
    g = ((String)localObject);
    j = i;
    j = getInt(j);
    h = j;
    localObject = new com/truecaller/filters/a/a;
    ((a)localObject).<init>(locala, (byte)0);
    return (a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */