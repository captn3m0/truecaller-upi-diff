package com.truecaller.filters;

import android.support.v4.f.j;
import java.util.Iterator;

final class f$b
{
  private final String a;
  private StringBuilder b;
  
  f$b(String paramString)
  {
    a = paramString;
  }
  
  final String a(Iterable paramIterable)
  {
    boolean bool = false;
    Object localObject = null;
    b = null;
    paramIterable = paramIterable.iterator();
    for (;;)
    {
      bool = paramIterable.hasNext();
      if (!bool) {
        break;
      }
      localObject = (j)paramIterable.next();
      StringBuilder localStringBuilder = b;
      if (localStringBuilder != null)
      {
        String str = a;
        localStringBuilder.append(str);
      }
      else
      {
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        b = localStringBuilder;
      }
      localStringBuilder = b;
      localObject = b.toString();
      localStringBuilder.append((String)localObject);
    }
    paramIterable = b;
    if (paramIterable != null) {
      return paramIterable.toString();
    }
    return "";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */