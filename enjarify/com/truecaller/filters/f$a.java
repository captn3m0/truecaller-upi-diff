package com.truecaller.filters;

import android.database.DatabaseUtils;
import android.text.TextUtils;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.List;

final class f$a
{
  public final String a;
  public final String[] b;
  
  private f$a(String paramString, String[] paramArrayOfString)
  {
    a = paramString;
    b = paramArrayOfString;
  }
  
  public static a a(TruecallerContract.Filters.WildCardType paramWildCardType, String paramString1, String paramString2, int paramInt, boolean paramBoolean)
  {
    boolean bool1 = TextUtils.isEmpty(paramString1);
    ArrayList localArrayList = null;
    if (bool1)
    {
      bool1 = TextUtils.isEmpty(paramString2);
      if (bool1)
      {
        paramString1 = new String[0];
        AssertionUtil.isTrue(false, paramString1);
        return null;
      }
    }
    if ((paramString2 != null) && (paramString1 != null))
    {
      bool1 = paramString1.equals(paramString2);
      if (bool1) {
        paramString2 = null;
      }
    }
    Object localObject = "";
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    if (paramWildCardType != null)
    {
      localObject = "wildcard_type=? AND ";
      int i = type;
      paramWildCardType = String.valueOf(i);
      localArrayList.add(paramWildCardType);
    }
    if (paramInt >= 0)
    {
      paramWildCardType = new java/lang/StringBuilder;
      paramWildCardType.<init>();
      paramWildCardType.append((String)localObject);
      paramWildCardType.append("rule=? AND ");
      localObject = paramWildCardType.toString();
      paramWildCardType = String.valueOf(paramInt);
      localArrayList.add(paramWildCardType);
    }
    paramWildCardType = new java/lang/StringBuilder;
    paramWildCardType.<init>();
    paramWildCardType.append((String)localObject);
    localObject = "(";
    paramWildCardType.append((String)localObject);
    paramWildCardType = paramWildCardType.toString();
    bool1 = TextUtils.isEmpty(paramString1);
    if (!bool1)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(paramWildCardType);
      ((StringBuilder)localObject).append("value=? COLLATE NOCASE");
      paramWildCardType = ((StringBuilder)localObject).toString();
      localArrayList.add(paramString1);
    }
    bool1 = TextUtils.isEmpty(paramString2);
    if (!bool1)
    {
      boolean bool2 = TextUtils.isEmpty(paramString1);
      if (!bool2)
      {
        paramString1 = new java/lang/StringBuilder;
        paramString1.<init>();
        paramString1.append(paramWildCardType);
        paramString1.append(" OR ");
        paramWildCardType = paramString1.toString();
      }
      paramString1 = new java/lang/StringBuilder;
      paramString1.<init>();
      paramString1.append(paramWildCardType);
      paramString1.append("value=?");
      paramWildCardType = paramString1.toString();
      localArrayList.add(paramString2);
    }
    paramString1 = new java/lang/StringBuilder;
    paramString1.<init>();
    paramString1.append(paramWildCardType);
    paramString1.append(")");
    paramWildCardType = paramString1.toString();
    if ((paramInt >= 0) && (!paramBoolean))
    {
      paramWildCardType = DatabaseUtils.concatenateWhere(paramWildCardType, "sync_state!=?");
      paramString1 = "2";
      localArrayList.add(paramString1);
    }
    paramString1 = new com/truecaller/filters/f$a;
    paramString2 = new String[localArrayList.size()];
    paramString2 = (String[])localArrayList.toArray(paramString2);
    paramString1.<init>(paramWildCardType, paramString2);
    return paramString1;
  }
  
  public final String toString()
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = getClass().getSimpleName();
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(": selection: ");
    localObject2 = a;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject2 = b;
    int i = localObject2.length;
    int j = 0;
    while (j < i)
    {
      String str = localObject2[j];
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append((String)localObject1);
      localStringBuilder.append(", ");
      localStringBuilder.append(str);
      localObject1 = localStringBuilder.toString();
      j += 1;
    }
    return (String)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */