package com.truecaller.filters.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.c;
import androidx.work.c.a;
import androidx.work.g;
import androidx.work.j;
import androidx.work.k.a;
import androidx.work.p;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.filters.FilterManager;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;

public final class FilterUploadWorker
  extends Worker
{
  public static final FilterUploadWorker.a d;
  public r b;
  public FilterManager c;
  
  static
  {
    FilterUploadWorker.a locala = new com/truecaller/filters/sync/FilterUploadWorker$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public FilterUploadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    c.g.b.k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public static final void b()
  {
    p localp = p.a();
    g localg = g.a;
    Object localObject1 = new androidx/work/k$a;
    ((k.a)localObject1).<init>(FilterUploadWorker.class);
    Object localObject2 = new androidx/work/c$a;
    ((c.a)localObject2).<init>();
    j localj = j.b;
    localObject2 = ((c.a)localObject2).a(localj).a();
    localObject1 = (androidx.work.k)((k.a)((k.a)localObject1).a((c)localObject2)).c();
    localp.a("FilterUploadWorker", localg, (androidx.work.k)localObject1);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject1 = b;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "accountManager";
      c.g.b.k.a((String)localObject2);
    }
    boolean bool = ((r)localObject1).c();
    if (!bool)
    {
      localObject1 = ListenableWorker.a.a();
      c.g.b.k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    try
    {
      localObject1 = c;
      if (localObject1 == null)
      {
        localObject2 = "filterManager";
        c.g.b.k.a((String)localObject2);
      }
      bool = ((FilterManager)localObject1).a();
      if (!bool)
      {
        localObject1 = ListenableWorker.a.a();
        localObject2 = "Result.success()";
        c.g.b.k.a(localObject1, (String)localObject2);
        return (ListenableWorker.a)localObject1;
      }
    }
    catch (Exception localException)
    {
      localObject1 = (Throwable)localException;
      localObject2 = new String[0];
      AssertionUtil.shouldNeverHappen((Throwable)localObject1, (String[])localObject2);
    }
    catch (IOException localIOException)
    {
      ListenableWorker.a.b();
    }
    catch (RuntimeException localRuntimeException)
    {
      ListenableWorker.a.b();
    }
    localObject1 = ListenableWorker.a.c();
    c.g.b.k.a(localObject1, "Result.failure()");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.sync.FilterUploadWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */