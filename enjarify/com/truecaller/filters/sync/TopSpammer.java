package com.truecaller.filters.sync;

import c.g.b.k;
import com.truecaller.common.h.am;

public final class TopSpammer
  implements Comparable
{
  private final String label;
  private final Integer reports;
  private final String value;
  
  public TopSpammer()
  {
    this(null, null, null, 7, null);
  }
  
  public TopSpammer(String paramString1, String paramString2, Integer paramInteger)
  {
    value = paramString1;
    label = paramString2;
    reports = paramInteger;
  }
  
  public final int compareTo(TopSpammer paramTopSpammer)
  {
    k.b(paramTopSpammer, "other");
    String str = value;
    paramTopSpammer = value;
    return am.a(str, paramTopSpammer, false);
  }
  
  public final String getLabel()
  {
    return label;
  }
  
  public final Integer getReports()
  {
    return reports;
  }
  
  public final String getValue()
  {
    return value;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Spammer{reports=");
    Integer localInteger = reports;
    localStringBuilder.append(localInteger);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.sync.TopSpammer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */