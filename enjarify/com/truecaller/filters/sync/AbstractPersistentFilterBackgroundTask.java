package com.truecaller.filters.sync;

import com.truecaller.common.background.PersistentBackgroundTask;

abstract class AbstractPersistentFilterBackgroundTask
  extends PersistentBackgroundTask
{}

/* Location:
 * Qualified Name:     com.truecaller.filters.sync.AbstractPersistentFilterBackgroundTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */