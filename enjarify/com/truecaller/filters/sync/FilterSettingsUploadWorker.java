package com.truecaller.filters.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.filters.FilterManager;
import java.io.IOException;

public final class FilterSettingsUploadWorker
  extends Worker
{
  public static final FilterSettingsUploadWorker.a d;
  public r b;
  public FilterManager c;
  
  static
  {
    FilterSettingsUploadWorker.a locala = new com/truecaller/filters/sync/FilterSettingsUploadWorker$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public FilterSettingsUploadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public static final void b()
  {
    FilterSettingsUploadWorker.a.a(null);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject = b;
    String str;
    if (localObject == null)
    {
      str = "accountManager";
      k.a(str);
    }
    boolean bool = ((r)localObject).c();
    if (!bool)
    {
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    try
    {
      localObject = c;
      if (localObject == null)
      {
        str = "filterManager";
        k.a(str);
      }
      bool = ((FilterManager)localObject).b();
      if (!bool)
      {
        localObject = ListenableWorker.a.a();
        str = "Result.success()";
        k.a(localObject, str);
        return (ListenableWorker.a)localObject;
      }
    }
    catch (IOException localIOException)
    {
      ListenableWorker.a.b();
    }
    catch (RuntimeException localRuntimeException)
    {
      ListenableWorker.a.b();
    }
    localObject = ListenableWorker.a.c();
    k.a(localObject, "Result.failure()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.sync.FilterSettingsUploadWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */