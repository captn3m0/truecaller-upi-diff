package com.truecaller.filters.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.filters.v;
import com.truecaller.log.d;

public final class TopSpammersSyncRecurringWorker
  extends TrackedWorker
{
  public static final TopSpammersSyncRecurringWorker.a e;
  public b b;
  public r c;
  public v d;
  private final Context f;
  
  static
  {
    TopSpammersSyncRecurringWorker.a locala = new com/truecaller/filters/sync/TopSpammersSyncRecurringWorker$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public TopSpammersSyncRecurringWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    f = paramContext;
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = c;
    if (localr == null)
    {
      String str = "accountManager";
      k.a(str);
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject;
    try
    {
      localObject = d;
      String str;
      if (localObject == null)
      {
        str = "topSpammerRepository";
        k.a(str);
      }
      boolean bool = ((v)localObject).a();
      if (bool)
      {
        localObject = ListenableWorker.a.a();
        str = "Result.success()";
        k.a(localObject, str);
        return (ListenableWorker.a)localObject;
      }
    }
    catch (Exception localException)
    {
      localObject = (Throwable)localException;
      d.a((Throwable)localObject);
      localObject = ListenableWorker.a.b();
      k.a(localObject, "Result.retry()");
    }
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.sync.TopSpammersSyncRecurringWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */