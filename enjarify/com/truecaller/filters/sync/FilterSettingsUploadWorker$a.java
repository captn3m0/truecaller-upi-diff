package com.truecaller.filters.sync;

import androidx.work.c;
import androidx.work.c.a;
import androidx.work.g;
import androidx.work.j;
import androidx.work.k.a;
import androidx.work.p;

public final class FilterSettingsUploadWorker$a
{
  public static void a(p paramp)
  {
    if (paramp == null)
    {
      paramp = p.a();
      String str = "WorkManager.getInstance()";
      c.g.b.k.a(paramp, str);
    }
    g localg = g.a;
    Object localObject1 = new androidx/work/k$a;
    ((k.a)localObject1).<init>(FilterSettingsUploadWorker.class);
    Object localObject2 = new androidx/work/c$a;
    ((c.a)localObject2).<init>();
    j localj = j.b;
    localObject2 = ((c.a)localObject2).a(localj).a();
    localObject1 = (androidx.work.k)((k.a)((k.a)localObject1).a((c)localObject2)).c();
    paramp.a("FilterSettingsUploadWorker", localg, (androidx.work.k)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.sync.FilterSettingsUploadWorker.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */