package com.truecaller.filters.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.c;
import androidx.work.c.a;
import androidx.work.g;
import androidx.work.j;
import androidx.work.k.a;
import androidx.work.p;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.filters.FilterManager;
import com.truecaller.log.d;

public final class FilterRestoreWorker
  extends Worker
{
  public static final FilterRestoreWorker.a d;
  public r b;
  public FilterManager c;
  
  static
  {
    FilterRestoreWorker.a locala = new com/truecaller/filters/sync/FilterRestoreWorker$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public FilterRestoreWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    c.g.b.k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public static final void b()
  {
    p localp = p.a();
    g localg = g.a;
    Object localObject1 = new androidx/work/k$a;
    ((k.a)localObject1).<init>(FilterRestoreWorker.class);
    Object localObject2 = new androidx/work/c$a;
    ((c.a)localObject2).<init>();
    j localj = j.b;
    localObject2 = ((c.a)localObject2).a(localj).a();
    localObject1 = (androidx.work.k)((k.a)((k.a)localObject1).a((c)localObject2)).c();
    localp.a("FilterRestoreWorker", localg, (androidx.work.k)localObject1);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject = b;
    String str;
    if (localObject == null)
    {
      str = "accountManager";
      c.g.b.k.a(str);
    }
    boolean bool = ((r)localObject).c();
    if (!bool)
    {
      localObject = ListenableWorker.a.a();
      c.g.b.k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    try
    {
      localObject = c;
      if (localObject == null)
      {
        str = "filterManager";
        c.g.b.k.a(str);
      }
      bool = ((FilterManager)localObject).c();
      if (bool)
      {
        localObject = "Filter restore success!";
        new String[1][0] = localObject;
        localObject = ListenableWorker.a.a();
        str = "Result.success()";
        c.g.b.k.a(localObject, str);
        return (ListenableWorker.a)localObject;
      }
    }
    catch (Exception localException)
    {
      localObject = (Throwable)localException;
      d.a((Throwable)localObject);
      new String[1][0] = "Filter restore failed, will retry";
      localObject = ListenableWorker.a.b();
      c.g.b.k.a(localObject, "Result.retry()");
    }
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.sync.FilterRestoreWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */