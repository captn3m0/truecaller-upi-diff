package com.truecaller.filters.blockedlist;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.am;
import com.truecaller.common.h.u;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.filters.s;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.ad;
import com.truecaller.utils.n;

final class h
  extends g
{
  private final k a;
  private final f c;
  private final u d;
  private final n e;
  private final com.truecaller.analytics.b f;
  private final ad g;
  private com.truecaller.androidactors.a h;
  private com.truecaller.filters.a.b i;
  private com.truecaller.filters.a.a j;
  
  h(k paramk, ad paramad, f paramf, u paramu, n paramn, com.truecaller.analytics.b paramb)
  {
    a = paramk;
    g = paramad;
    c = paramf;
    d = paramu;
    e = paramn;
    f = paramb;
  }
  
  private String a(TruecallerContract.Filters.WildCardType paramWildCardType)
  {
    Object localObject = h.1.a;
    int k = paramWildCardType.ordinal();
    k = localObject[k];
    localObject = null;
    switch (k)
    {
    default: 
      return "";
    case 3: 
      paramWildCardType = e;
      localObject = new Object[0];
      return paramWildCardType.a(2131886140, (Object[])localObject);
    case 2: 
      paramWildCardType = e;
      localObject = new Object[0];
      return paramWildCardType.a(2131886139, (Object[])localObject);
    }
    paramWildCardType = e;
    localObject = new Object[0];
    return paramWildCardType.a(2131886141, (Object[])localObject);
  }
  
  private void g()
  {
    Object localObject = h;
    if (localObject != null) {
      ((com.truecaller.androidactors.a)localObject).a();
    }
    localObject = ((s)c.a()).a();
    com.truecaller.androidactors.i locali = a.a();
    -..Lambda.po7pe45gyP7zMkDKo3hdk-l1h-g localpo7pe45gyP7zMkDKo3hdk-l1h-g = new com/truecaller/filters/blockedlist/-$$Lambda$po7pe45gyP7zMkDKo3hdk-l1h-g;
    localpo7pe45gyP7zMkDKo3hdk-l1h-g.<init>(this);
    localObject = ((w)localObject).a(locali, localpo7pe45gyP7zMkDKo3hdk-l1h-g);
    h = ((com.truecaller.androidactors.a)localObject);
  }
  
  public final int a()
  {
    com.truecaller.filters.a.b localb = i;
    if (localb == null) {
      return 0;
    }
    return localb.getCount();
  }
  
  public final int a(int paramInt)
  {
    return 0;
  }
  
  final void a(com.truecaller.filters.a.b paramb)
  {
    h = null;
    Object localObject = b;
    if (localObject == null)
    {
      if (paramb != null) {
        paramb.close();
      }
      return;
    }
    i = paramb;
    int k = a();
    if (k > 0)
    {
      paramb = (i)b;
      paramb.k();
    }
    else
    {
      paramb = (i)b;
      paramb.j();
    }
    ((i)b).i();
  }
  
  public final long b(int paramInt)
  {
    return 0L;
  }
  
  public final void b()
  {
    g();
  }
  
  public final void d(int paramInt)
  {
    Object localObject1 = i;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        i.moveToPosition(paramInt);
        Object localObject2 = i.a();
        localObject1 = (i)b;
        String str = d;
        localObject2 = e;
        ((i)localObject1).a(str, (String)localObject2);
      }
    }
  }
  
  final void e(int paramInt)
  {
    Object localObject1 = i;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = i;
        ((com.truecaller.filters.a.b)localObject1).moveToPosition(paramInt);
        Object localObject2 = i.a();
        j = ((com.truecaller.filters.a.a)localObject2);
        boolean bool1 = ((com.truecaller.filters.a.a)localObject2).b();
        int k = 1;
        Object localObject3;
        Object localObject4;
        int m;
        if (bool1)
        {
          localObject1 = g;
          localObject3 = e;
          localObject1 = ((ad)localObject1).c((String)localObject3);
          if (localObject1 == null)
          {
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>("Country was not found by iso ");
            localObject4 = e;
            ((StringBuilder)localObject1).append((String)localObject4);
            localObject1 = ((StringBuilder)localObject1).toString();
            AssertionUtil.reportWeirdnessButNeverCrash((String)localObject1);
            localObject2 = e;
          }
          else
          {
            localObject2 = e;
            m = 2131886193;
            localObject4 = new Object[k];
            localObject1 = b;
            localObject4[0] = localObject1;
            localObject2 = ((n)localObject2).a(m, (Object[])localObject4);
          }
        }
        else
        {
          bool1 = ((com.truecaller.filters.a.a)localObject2).a();
          if (bool1)
          {
            localObject1 = h;
            localObject1 = a((TruecallerContract.Filters.WildCardType)localObject1);
            localObject3 = h;
            localObject2 = e;
            localObject2 = ((TruecallerContract.Filters.WildCardType)localObject3).stripPattern((String)localObject2);
            localObject3 = e;
            int i1 = 2131886196;
            int i2 = 2;
            Object[] arrayOfObject = new Object[i2];
            arrayOfObject[0] = localObject1;
            arrayOfObject[k] = localObject2;
            localObject2 = ((n)localObject3).a(i1, arrayOfObject);
          }
          else
          {
            localObject1 = d;
            localObject3 = e;
            localObject1 = ((u)localObject1).b((String)localObject3);
            if (localObject1 != null)
            {
              m = 1;
            }
            else
            {
              m = 0;
              localObject3 = null;
            }
            int n;
            if (m != 0)
            {
              localObject3 = d;
              boolean bool2 = am.b((CharSequence)localObject3);
              if (!bool2) {
                localObject1 = d;
              }
              localObject2 = e;
              n = 2131886195;
              localObject4 = new Object[k];
              localObject4[0] = localObject1;
              localObject2 = ((n)localObject2).a(n, (Object[])localObject4);
            }
            else
            {
              localObject1 = d;
              bool1 = am.b((CharSequence)localObject1);
              if (bool1) {
                localObject2 = e;
              } else {
                localObject2 = d;
              }
              localObject1 = e;
              n = 2131886197;
              localObject4 = new Object[k];
              localObject4[0] = localObject2;
              localObject2 = ((n)localObject1).a(n, (Object[])localObject4);
            }
          }
        }
        localObject1 = (i)b;
        ((i)localObject1).a((String)localObject2);
      }
    }
  }
  
  final void f()
  {
    Object localObject1 = (s)c.a();
    Object localObject2 = j;
    localObject1 = ((s)localObject1).a((com.truecaller.filters.a.a)localObject2, "blockViewList", false);
    localObject2 = a.a();
    -..Lambda.h.h6jgvdzGeP_YB2QE0KWGqyTMELo localh6jgvdzGeP_YB2QE0KWGqyTMELo = new com/truecaller/filters/blockedlist/-$$Lambda$h$h6jgvdzGeP_YB2QE0KWGqyTMELo;
    localh6jgvdzGeP_YB2QE0KWGqyTMELo.<init>(this);
    ((w)localObject1).a((com.truecaller.androidactors.i)localObject2, localh6jgvdzGeP_YB2QE0KWGqyTMELo);
  }
  
  public final void y_()
  {
    Object localObject = i;
    if (localObject != null)
    {
      ((com.truecaller.filters.a.b)localObject).close();
      i = null;
    }
    localObject = h;
    if (localObject != null)
    {
      ((com.truecaller.androidactors.a)localObject).a();
      h = null;
    }
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedlist.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */