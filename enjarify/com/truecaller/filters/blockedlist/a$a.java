package com.truecaller.filters.blockedlist;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.ui.components.d.c;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.util.at;

final class a$a
  extends d.c
  implements d
{
  private final ContactPhoto a;
  private final TextView c;
  private final TextView d;
  private final ImageView e;
  private final g f;
  
  a$a(View paramView, g paramg)
  {
    super(paramView);
    f = paramg;
    paramg = (ContactPhoto)paramView.findViewById(2131362554);
    a = paramg;
    paramg = (TextView)paramView.findViewById(2131364884);
    c = paramg;
    paramg = (TextView)paramView.findViewById(2131364609);
    d = paramg;
    paramView = (ImageView)paramView.findViewById(2131364111);
    e = paramView;
    a.setIsSpam(true);
    paramView = e;
    paramg = new com/truecaller/filters/blockedlist/-$$Lambda$a$a$uM1UDkOdNeYTxsOr_4x4pr3AJ60;
    paramg.<init>(this);
    paramView.setOnClickListener(paramg);
  }
  
  public final void a()
  {
    a.setDrawableRes(2131233882);
  }
  
  public final void a(String paramString)
  {
    at.a(c, paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    itemView.setEnabled(paramBoolean);
  }
  
  public final void b()
  {
    a.setDrawableRes(2131233888);
  }
  
  public final void b(String paramString)
  {
    at.a(d, paramString);
  }
  
  public final void c()
  {
    a.setDrawableRes(2131234560);
  }
  
  public final void d()
  {
    a.setDrawableRes(2131234136);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedlist.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */