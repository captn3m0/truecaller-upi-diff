package com.truecaller.filters.blockedlist;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.filters.blockedevents.BlockDialogActivity;
import com.truecaller.filters.blockedevents.BlockDialogActivity.DialogType;
import com.truecaller.ui.components.d.a;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.at;

public final class c
  extends com.truecaller.filters.b
  implements i
{
  g c;
  private RecyclerView d;
  private View e;
  private a f;
  
  public final void a(int paramInt)
  {
    c.c(paramInt);
  }
  
  public final void a(String paramString)
  {
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = getContext();
    ((AlertDialog.Builder)localObject).<init>(localContext);
    paramString = ((AlertDialog.Builder)localObject).setMessage(paramString);
    localObject = new com/truecaller/filters/blockedlist/-$$Lambda$c$s26Xmqj2ucpcIqMucuHoINcWGq8;
    ((-..Lambda.c.s26Xmqj2ucpcIqMucuHoINcWGq8)localObject).<init>(this);
    paramString.setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject).setNegativeButton(2131887214, null).show();
  }
  
  public final void a(String paramString1, String paramString2)
  {
    f localf = getActivity();
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.SpammersList;
    DetailsFragment.a(localf, null, paramString1, null, paramString2, null, localSourceType, true, true);
  }
  
  public final void h()
  {
    Context localContext = getContext();
    BlockDialogActivity.DialogType localDialogType = BlockDialogActivity.DialogType.NAME;
    BlockDialogActivity.a(localContext, localDialogType);
  }
  
  public final void i()
  {
    f.notifyDataSetChanged();
  }
  
  public final void j()
  {
    at.a(e, true);
    at.a(d, false);
  }
  
  public final void k()
  {
    at.a(e, false);
    at.a(d, true);
  }
  
  public final void m()
  {
    Context localContext = getContext();
    BlockDialogActivity.DialogType localDialogType = BlockDialogActivity.DialogType.ADVANCED;
    BlockDialogActivity.a(localContext, localDialogType);
  }
  
  public final void n()
  {
    Context localContext = getContext();
    BlockDialogActivity.DialogType localDialogType = BlockDialogActivity.DialogType.NUMBER;
    BlockDialogActivity.a(localContext, localDialogType);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = new com/truecaller/filters/blockedlist/j$a;
    Object localObject1 = null;
    paramBundle.<init>((byte)0);
    Object localObject2 = (bp)dagger.a.g.a(((bk)getContext().getApplicationContext()).a());
    b = ((bp)localObject2);
    localObject2 = new com/truecaller/filters/blockedlist/e;
    ((e)localObject2).<init>();
    localObject2 = (e)dagger.a.g.a(localObject2);
    a = ((e)localObject2);
    localObject2 = a;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/filters/blockedlist/e;
      ((e)localObject2).<init>();
      a = ((e)localObject2);
    }
    dagger.a.g.a(b, bp.class);
    localObject2 = new com/truecaller/filters/blockedlist/j;
    e locale = a;
    paramBundle = b;
    ((j)localObject2).<init>(locale, paramBundle, (byte)0);
    ((b)localObject2).a(this);
    paramBundle = new com/truecaller/filters/blockedlist/a;
    localObject1 = c;
    paramBundle.<init>((g)localObject1);
    f = paramBundle;
    paramBundle = f;
    localObject1 = new com/truecaller/filters/blockedlist/-$$Lambda$c$If5G1mOjLbQ0IYaWF-_2crTuT0c;
    ((-..Lambda.c.If5G1mOjLbQ0IYaWF-_2crTuT0c)localObject1).<init>(this);
    a = ((d.a)localObject1);
    setHasOptionsMenu(true);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131558664, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    c.y_();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    int j = 16908332;
    if (i != j) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    c.e();
    return true;
  }
  
  public final void onResume()
  {
    super.onResume();
    c.b();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = paramView.findViewById(2131362976);
    e = paramBundle;
    paramView = (RecyclerView)paramView.findViewById(2131364093);
    d = paramView;
    d.setItemAnimator(null);
    paramView = d;
    paramBundle = new android/support/v7/widget/LinearLayoutManager;
    Context localContext = getContext();
    paramBundle.<init>(localContext);
    paramView.setLayoutManager(paramBundle);
    paramView = d;
    paramBundle = f;
    paramView.setAdapter(paramBundle);
    b(2131886191);
    c.a(this);
    b();
  }
  
  public final boolean q()
  {
    return c.c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.blockedlist.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */