package com.truecaller.filters;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import c.n;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.common.h.u;
import com.truecaller.content.TruecallerContract.Filters;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.content.TruecallerContract.ao;
import com.truecaller.data.entity.Contact;
import com.truecaller.featuretoggles.e;
import com.truecaller.filters.sync.FilterUploadWorker;
import com.truecaller.filters.sync.c.b;
import com.truecaller.filters.sync.c.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.tracking.events.at;
import com.truecaller.tracking.events.at.a;
import com.truecaller.utils.l;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.ad;
import org.c.a.a.a.k;

final class f
  implements FilterManager
{
  private static final Pattern a = Pattern.compile("91140\\d{7}$");
  private final Context b;
  private final p c;
  private final com.truecaller.androidactors.f d;
  private final com.truecaller.analytics.b e;
  private final u f;
  private final e g;
  private final com.truecaller.common.account.r h;
  private final TelephonyManager i;
  private final l j;
  private final com.truecaller.common.f.c k;
  
  f(Context paramContext, p paramp, com.truecaller.androidactors.f paramf, com.truecaller.analytics.b paramb, u paramu, e parame, com.truecaller.common.account.r paramr, TelephonyManager paramTelephonyManager, l paraml, com.truecaller.common.f.c paramc)
  {
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
    c = paramp;
    d = paramf;
    e = paramb;
    f = paramu;
    g = parame;
    h = paramr;
    i = paramTelephonyManager;
    j = paraml;
    k = paramc;
  }
  
  private static ContentValues a(String paramString1, String paramString2, String paramString3, int paramInt, String paramString4, TruecallerContract.Filters.EntityType paramEntityType)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    localContentValues.put("value", paramString1);
    localContentValues.put("label", paramString3);
    paramString3 = Integer.valueOf(paramInt);
    localContentValues.put("rule", paramString3);
    paramString3 = Integer.valueOf(NONEtype);
    localContentValues.put("wildcard_type", paramString3);
    paramString3 = Integer.valueOf(1);
    localContentValues.put("sync_state", paramString3);
    localContentValues.put("tracking_type", paramString2);
    localContentValues.put("tracking_source", paramString4);
    paramString2 = Integer.valueOf(value);
    localContentValues.put("entity_type", paramString2);
    return localContentValues;
  }
  
  private g a(g paramg, boolean paramBoolean)
  {
    if ((paramg != null) && (paramBoolean))
    {
      Object localObject1 = h;
      Object localObject2 = FilterManager.FilterAction.FILTER_BLACKLISTED;
      if (localObject1 == localObject2)
      {
        localObject1 = j;
        localObject2 = FilterManager.ActionSource.TOP_SPAMMER;
        if (localObject1 == localObject2)
        {
          localObject1 = c;
          paramBoolean = ((p)localObject1).g();
          if (!paramBoolean)
          {
            localObject1 = new com/truecaller/filters/g;
            long l = i;
            FilterManager.FilterAction localFilterAction = FilterManager.FilterAction.FILTER_DISABLED;
            FilterManager.ActionSource localActionSource = j;
            String str = k;
            int m = l;
            int n = m;
            TruecallerContract.Filters.WildCardType localWildCardType = n;
            localObject2 = localObject1;
            ((g)localObject1).<init>(l, localFilterAction, localActionSource, str, m, n, localWildCardType);
            return (g)localObject1;
          }
        }
      }
    }
    return paramg;
  }
  
  private g a(String paramString1, String paramString2)
  {
    int m = 1;
    paramString1 = c(paramString1, paramString2, m);
    boolean bool = paramString1.isEmpty();
    if (bool) {
      return null;
    }
    return (g)paramString1.get(0);
  }
  
  private static com.truecaller.filters.sync.c.a a(ContentValues paramContentValues, List paramList)
  {
    paramContentValues = paramContentValues.getAsString("value");
    if (paramContentValues == null) {
      return null;
    }
    paramList = paramList.iterator();
    com.truecaller.filters.sync.c.a locala;
    boolean bool2;
    do
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      locala = (com.truecaller.filters.sync.c.a)paramList.next();
      String str = b;
      bool2 = paramContentValues.equals(str);
    } while (!bool2);
    return locala;
    return null;
  }
  
  private String a(String... paramVarArgs)
  {
    int m = paramVarArgs.length;
    int n = 0;
    while (n < m)
    {
      String str = paramVarArgs[n];
      boolean bool = TextUtils.isEmpty(str);
      if (!bool)
      {
        u localu = f;
        str = localu.e(str);
        if (str != null) {
          return str;
        }
      }
      n += 1;
    }
    return null;
  }
  
  private List a(String paramString1, String paramString2, int paramInt)
  {
    String str1 = paramString1;
    String str2 = paramString2;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject2 = TruecallerContract.Filters.WildCardType.NONE;
    int m = 1;
    localObject2 = f.a.a((TruecallerContract.Filters.WildCardType)localObject2, paramString1, paramString2, m, m);
    if (localObject2 == null) {
      return localArrayList;
    }
    Object localObject3 = b;
    Object localObject4 = ((Context)localObject3).getContentResolver();
    Object localObject5 = TruecallerContract.Filters.a();
    int n = 0;
    String str3 = null;
    Object localObject6 = a;
    String[] arrayOfString = b;
    boolean bool1 = false;
    localObject2 = ((ContentResolver)localObject4).query((Uri)localObject5, null, (String)localObject6, arrayOfString, null);
    if (localObject2 != null)
    {
      localObject3 = "_id";
      try
      {
        int i1 = ((Cursor)localObject2).getColumnIndexOrThrow((String)localObject3);
        localObject4 = "value";
        int i2 = ((Cursor)localObject2).getColumnIndex((String)localObject4);
        localObject5 = "label";
        int i3 = ((Cursor)localObject2).getColumnIndex((String)localObject5);
        str3 = "sync_state";
        n = ((Cursor)localObject2).getColumnIndex(str3);
        for (;;)
        {
          boolean bool2 = ((Cursor)localObject2).moveToNext();
          if (!bool2) {
            break;
          }
          int i4 = localArrayList.size();
          if (i4 >= paramInt) {
            break;
          }
          long l = ((Cursor)localObject2).getLong(i1);
          localObject6 = ((Cursor)localObject2).getString(i2);
          String str4 = ((Cursor)localObject2).getString(i3);
          int i5 = ((Cursor)localObject2).getInt(n);
          bool1 = ((String)localObject6).equals(str1);
          if (!bool1)
          {
            boolean bool3 = ((String)localObject6).equals(str2);
            if (!bool3) {}
          }
          else
          {
            localObject6 = new com/truecaller/filters/g;
            FilterManager.FilterAction localFilterAction = FilterManager.FilterAction.ALLOW_WHITELISTED;
            FilterManager.ActionSource localActionSource = FilterManager.ActionSource.CUSTOM_WHITELIST;
            TruecallerContract.Filters.WildCardType localWildCardType = TruecallerContract.Filters.WildCardType.NONE;
            ((g)localObject6).<init>(l, localFilterAction, localActionSource, str4, i5, 0, localWildCardType);
            localArrayList.add(localObject6);
          }
        }
      }
      finally
      {
        ((Cursor)localObject2).close();
      }
    }
    return localArrayList;
  }
  
  private static void a(Collection paramCollection)
  {
    a(paramCollection, true);
    FilterUploadWorker.b();
  }
  
  private static void a(Collection paramCollection, boolean paramBoolean)
  {
    int m = 1;
    if (paramBoolean)
    {
      localObject1 = paramCollection.iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (ContentValues)((Iterator)localObject1).next();
        localObject3 = "sync_state";
        localObject4 = Integer.valueOf(m);
        ((ContentValues)localObject2).put((String)localObject3, (Integer)localObject4);
      }
    }
    Object localObject1 = com.truecaller.common.b.a.F().getContentResolver();
    Object localObject2 = TruecallerContract.Filters.a();
    Object localObject3 = new ContentValues[paramCollection.size()];
    localObject3 = (ContentValues[])paramCollection.toArray((Object[])localObject3);
    paramBoolean = ((ContentResolver)localObject1).bulkInsert((Uri)localObject2, (ContentValues[])localObject3);
    boolean bool2 = paramCollection.size();
    localObject3 = null;
    if (paramBoolean == bool2)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject2 = null;
    }
    String[] arrayOfString = new String[m];
    Object localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>("Unexpected # of filters added, got ");
    int n = paramCollection.size();
    ((StringBuilder)localObject4).append(n);
    ((StringBuilder)localObject4).append(", added ");
    ((StringBuilder)localObject4).append(paramBoolean);
    paramCollection = ((StringBuilder)localObject4).toString();
    arrayOfString[0] = paramCollection;
    AssertionUtil.OnlyInDebug.isTrue(bool2, arrayOfString);
  }
  
  private static void a(List paramList)
  {
    boolean bool = paramList.isEmpty();
    if (bool) {
      return;
    }
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    ((StringBuilder)localObject1).append("_id IN (?");
    Object localObject3 = String.valueOf(paramList.get(0));
    ((ArrayList)localObject2).add(localObject3);
    int m = 1;
    for (;;)
    {
      int n = paramList.size();
      if (m >= n) {
        break;
      }
      ((StringBuilder)localObject1).append(",?");
      localObject4 = String.valueOf(paramList.get(m));
      ((ArrayList)localObject2).add(localObject4);
      m += 1;
    }
    ((StringBuilder)localObject1).append(")");
    paramList = com.truecaller.common.b.a.F().getContentResolver();
    localObject3 = TruecallerContract.Filters.a();
    localObject1 = ((StringBuilder)localObject1).toString();
    Object localObject4 = new String[((ArrayList)localObject2).size()];
    localObject2 = (String[])((ArrayList)localObject2).toArray((Object[])localObject4);
    paramList.delete((Uri)localObject3, (String)localObject1, (String[])localObject2);
  }
  
  private boolean a(c.f paramf)
  {
    Object localObject = g.M();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool1)
    {
      localObject = c;
      bool1 = ((p)localObject).b();
      if (bool1)
      {
        paramf = (Boolean)paramf.b();
        boolean bool2 = paramf.booleanValue();
        return !bool2;
      }
    }
    return false;
  }
  
  private boolean a(String paramString, c.f paramf)
  {
    int m = 1;
    Object localObject;
    boolean bool1;
    if (paramString != null)
    {
      localObject = "+";
      bool1 = paramString.startsWith((String)localObject);
      if (bool1) {
        paramString = paramString.substring(m);
      }
    }
    if (paramString != null)
    {
      localObject = g.S();
      bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
      if (bool1)
      {
        localObject = c;
        bool1 = ((p)localObject).e();
        if (bool1)
        {
          localObject = a;
          paramString = ((Pattern)localObject).matcher(paramString);
          boolean bool2 = paramString.matches();
          if (bool2)
          {
            paramString = (Boolean)paramf.b();
            bool2 = paramString.booleanValue();
            if (!bool2) {
              return m;
            }
          }
        }
      }
    }
    return false;
  }
  
  private static boolean a(String paramString, String... paramVarArgs)
  {
    int m = 0;
    for (;;)
    {
      int n = 3;
      if (m >= n) {
        break;
      }
      String str = paramVarArgs[m];
      if (str != null)
      {
        boolean bool = str.equalsIgnoreCase(paramString);
        if (bool) {
          return true;
        }
      }
      m += 1;
    }
    return false;
  }
  
  private g b(String paramString1, String paramString2)
  {
    int m = 1;
    paramString1 = a(paramString1, paramString2, m);
    boolean bool = paramString1.isEmpty();
    if (bool) {
      return null;
    }
    return (g)paramString1.get(0);
  }
  
  private g b(String paramString1, String paramString2, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramBoolean = ab.a(paramString1);
      if (paramBoolean)
      {
        paramBoolean = ab.a(paramString2);
        if (paramBoolean)
        {
          paramString1 = c;
          bool = paramString1.a();
          if (bool) {
            return g.b;
          }
          return g.c;
        }
      }
    }
    boolean bool = TextUtils.isEmpty(paramString1);
    if (bool)
    {
      bool = TextUtils.isEmpty(paramString2);
      if (bool) {
        return g.a;
      }
    }
    return null;
  }
  
  private List b(String paramString1, String paramString2, int paramInt)
  {
    f localf = this;
    String str1 = paramString1;
    String str3 = paramString2;
    int m = paramInt;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject2 = TruecallerContract.Filters.WildCardType.NONE;
    int n = 0;
    Object localObject3 = null;
    localObject2 = f.a.a((TruecallerContract.Filters.WildCardType)localObject2, paramString1, paramString2, 0, false);
    if (localObject2 == null) {
      return localArrayList;
    }
    Object localObject4 = b;
    Object localObject5 = ((Context)localObject4).getContentResolver();
    Object localObject6 = TruecallerContract.Filters.a();
    int i1 = 0;
    Object localObject7 = null;
    Object localObject8 = a;
    Object localObject9 = b;
    localObject2 = ((ContentResolver)localObject5).query((Uri)localObject6, null, (String)localObject8, (String[])localObject9, null);
    int i4;
    boolean bool4;
    String str4;
    int i7;
    Object localObject10;
    Object localObject11;
    TruecallerContract.Filters.WildCardType localWildCardType1;
    if (localObject2 != null)
    {
      localObject4 = "_id";
      try
      {
        i3 = ((Cursor)localObject2).getColumnIndexOrThrow((String)localObject4);
        localObject5 = "value";
        i4 = ((Cursor)localObject2).getColumnIndex((String)localObject5);
        localObject6 = "label";
        i5 = ((Cursor)localObject2).getColumnIndexOrThrow((String)localObject6);
        localObject7 = "sync_state";
        i1 = ((Cursor)localObject2).getColumnIndexOrThrow((String)localObject7);
        for (;;)
        {
          boolean bool3 = ((Cursor)localObject2).moveToNext();
          if (!bool3) {
            break;
          }
          int i6 = localArrayList.size();
          if (i6 >= m) {
            break;
          }
          long l1 = ((Cursor)localObject2).getLong(i3);
          localObject8 = ((Cursor)localObject2).getString(i4);
          bool5 = ((String)localObject8).equals(str1);
          if (!bool5)
          {
            bool4 = ((String)localObject8).equals(str3);
            if (!bool4) {}
          }
          else
          {
            str4 = ((Cursor)localObject2).getString(i5);
            i7 = ((Cursor)localObject2).getInt(i1);
            localObject8 = new com/truecaller/filters/g;
            localObject10 = FilterManager.FilterAction.FILTER_BLACKLISTED;
            localObject11 = FilterManager.ActionSource.CUSTOM_BLACKLIST;
            localWildCardType1 = null;
            TruecallerContract.Filters.WildCardType localWildCardType2 = TruecallerContract.Filters.WildCardType.NONE;
            localObject9 = localObject8;
            ((g)localObject8).<init>(l1, (FilterManager.FilterAction)localObject10, (FilterManager.ActionSource)localObject11, str4, i7, 0, localWildCardType2);
            localArrayList.add(localObject8);
          }
        }
      }
      finally
      {
        ((Cursor)localObject2).close();
      }
    }
    int i8 = localArrayList.size();
    if (i8 >= m) {
      return localArrayList;
    }
    i8 = 0;
    int i3 = -1;
    localObject2 = f.a.a(null, str2, str3, i3, false);
    if (localObject2 == null) {
      return localArrayList;
    }
    localObject3 = b;
    localObject4 = ((Context)localObject3).getContentResolver();
    localObject5 = TruecallerContract.ao.a();
    int i5 = 0;
    localObject6 = null;
    localObject7 = a;
    localObject8 = b;
    boolean bool5 = false;
    localObject9 = null;
    localObject2 = ((ContentResolver)localObject4).query((Uri)localObject5, null, (String)localObject7, (String[])localObject8, null);
    if (localObject2 != null)
    {
      localObject3 = "_id";
      try
      {
        n = ((Cursor)localObject2).getColumnIndexOrThrow((String)localObject3);
        localObject4 = "value";
        i3 = ((Cursor)localObject2).getColumnIndex((String)localObject4);
        localObject5 = "label";
        i4 = ((Cursor)localObject2).getColumnIndexOrThrow((String)localObject5);
        localObject6 = "count";
        i5 = ((Cursor)localObject2).getColumnIndexOrThrow((String)localObject6);
        for (;;)
        {
          boolean bool1 = ((Cursor)localObject2).moveToNext();
          if (!bool1) {
            break;
          }
          int i2 = localArrayList.size();
          if (i2 >= m) {
            break;
          }
          long l2 = ((Cursor)localObject2).getLong(n);
          localObject7 = ((Cursor)localObject2).getString(i3);
          bool4 = ((String)localObject7).equalsIgnoreCase(str2);
          if (!bool4)
          {
            boolean bool2 = ((String)localObject7).equalsIgnoreCase(str3);
            if (!bool2) {}
          }
          else
          {
            localObject11 = ((Cursor)localObject2).getString(i4);
            i7 = ((Cursor)localObject2).getInt(i5);
            localObject7 = new com/truecaller/filters/g;
            FilterManager.FilterAction localFilterAction = FilterManager.FilterAction.FILTER_BLACKLISTED;
            localObject10 = FilterManager.ActionSource.TOP_SPAMMER;
            str4 = null;
            localWildCardType1 = TruecallerContract.Filters.WildCardType.NONE;
            localObject8 = localObject7;
            ((g)localObject7).<init>(l2, localFilterAction, (FilterManager.ActionSource)localObject10, (String)localObject11, 0, i7, localWildCardType1);
            localArrayList.add(localObject7);
          }
        }
      }
      finally
      {
        ((Cursor)localObject2).close();
      }
    }
    return localArrayList;
  }
  
  private boolean b(String paramString, c.f paramf)
  {
    String str = h.b();
    com.truecaller.featuretoggles.b localb = g.Q();
    boolean bool1 = localb.a();
    p localp = c;
    boolean bool2 = localp.d();
    Integer localInteger = c.f();
    if ((str != null) && (paramString != null) && (localInteger != null) && (bool1) && (bool2))
    {
      paramf = (Boolean)paramf.b();
      boolean bool3 = paramf.booleanValue();
      if (!bool3)
      {
        int n = localInteger.intValue();
        paramf = str.substring(0, n);
        int i1 = paramf.length();
        int m = 3;
        if (i1 < m)
        {
          AssertionUtil.reportWeirdnessButNeverCrash("Invalid neighbour spoofing filter (too short)");
          return false;
        }
        return paramString.startsWith(paramf);
      }
    }
    return false;
  }
  
  private g c(String paramString)
  {
    Object localObject1 = b.getContentResolver();
    Object localObject2 = TruecallerContract.Filters.a();
    Object localObject3 = "value = ? AND tracking_type = ? AND rule = ?";
    String[] arrayOfString = new String[3];
    Object localObject4 = Locale.getDefault();
    paramString = paramString.toLowerCase((Locale)localObject4);
    localObject4 = null;
    arrayOfString[0] = paramString;
    arrayOfString[1] = "COUNTRY_CODE";
    int m = 2;
    arrayOfString[m] = "0";
    boolean bool = false;
    paramString = ((ContentResolver)localObject1).query((Uri)localObject2, null, (String)localObject3, arrayOfString, null);
    if (paramString != null)
    {
      localObject4 = "_id";
      try
      {
        m = paramString.getColumnIndexOrThrow((String)localObject4);
        localObject1 = "value";
        int n = paramString.getColumnIndex((String)localObject1);
        localObject2 = "sync_state";
        int i1 = paramString.getColumnIndexOrThrow((String)localObject2);
        bool = paramString.moveToFirst();
        if (bool)
        {
          long l = paramString.getLong(m);
          String str = paramString.getString(n);
          int i2 = paramString.getInt(i1);
          localObject4 = new com/truecaller/filters/g;
          FilterManager.FilterAction localFilterAction = FilterManager.FilterAction.FILTER_BLACKLISTED;
          FilterManager.ActionSource localActionSource = FilterManager.ActionSource.CUSTOM_BLACKLIST;
          TruecallerContract.Filters.WildCardType localWildCardType = TruecallerContract.Filters.WildCardType.NONE;
          localObject3 = localObject4;
          ((g)localObject4).<init>(l, localFilterAction, localActionSource, str, i2, 0, localWildCardType);
          return (g)localObject4;
        }
      }
      finally
      {
        paramString.close();
      }
    }
    return null;
  }
  
  private g c(String paramString1, String paramString2)
  {
    int m = 1;
    paramString1 = b(paramString1, paramString2, m);
    boolean bool = paramString1.isEmpty();
    if (bool) {
      return null;
    }
    return (g)paramString1.get(0);
  }
  
  private g c(String paramString1, String paramString2, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramString1 = b(paramString1, paramString2, -1 >>> 1);
      boolean bool1 = paramString1.size();
      paramBoolean = true;
      if (bool1 > paramBoolean)
      {
        paramString2 = paramString1.iterator();
        g localg;
        Object localObject;
        FilterManager.FilterAction localFilterAction;
        do
        {
          do
          {
            boolean bool2 = paramString2.hasNext();
            if (!bool2) {
              break;
            }
            localg = (g)paramString2.next();
            localObject = a(localg, paramBoolean);
          } while (localObject == null);
          localObject = h;
          localFilterAction = FilterManager.FilterAction.FILTER_BLACKLISTED;
        } while (localObject != localFilterAction);
        return localg;
      }
      bool1 = paramString1.isEmpty();
      if (bool1)
      {
        paramString1 = null;
      }
      else
      {
        bool1 = false;
        paramString2 = null;
        paramString1 = (g)paramString1.get(0);
      }
      return a(paramString1, paramBoolean);
    }
    return c(paramString1, paramString2);
  }
  
  private List c(String paramString1, String paramString2, int paramInt)
  {
    int m = 1;
    Object localObject1 = new String[m];
    int n = NONEtype;
    Object localObject2 = String.valueOf(n);
    int i1 = 0;
    localObject1[0] = localObject2;
    String str1 = DatabaseUtils.concatenateWhere("wildcard_type !=?", "sync_state!=?");
    Object localObject3 = { "2" };
    Object localObject4 = DatabaseUtils.appendSelectionArgs((String[])localObject1, (String[])localObject3);
    localObject1 = this;
    Object localObject5 = b.getContentResolver();
    Object localObject6 = TruecallerContract.Filters.a();
    int i2 = 0;
    String str2 = null;
    boolean bool1 = false;
    Object localObject7 = null;
    localObject2 = ((ContentResolver)localObject5).query((Uri)localObject6, null, str1, (String[])localObject4, null);
    localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    if (localObject2 != null)
    {
      localObject5 = "_id";
      try
      {
        i1 = ((Cursor)localObject2).getColumnIndexOrThrow((String)localObject5);
        localObject6 = "value";
        int i4 = ((Cursor)localObject2).getColumnIndex((String)localObject6);
        str2 = "wildcard_type";
        i2 = ((Cursor)localObject2).getColumnIndex(str2);
        str1 = "label";
        int i5 = ((Cursor)localObject2).getColumnIndexOrThrow(str1);
        localObject4 = "sync_state";
        int i6 = ((Cursor)localObject2).getColumnIndexOrThrow((String)localObject4);
        for (;;)
        {
          bool1 = ((Cursor)localObject2).moveToNext();
          if (!bool1) {
            break;
          }
          int i3 = ((List)localObject3).size();
          if (i3 >= paramInt) {
            break;
          }
          long l = ((Cursor)localObject2).getLong(i1);
          localObject7 = ((Cursor)localObject2).getString(i4);
          int i7 = ((Cursor)localObject2).getInt(i2);
          String str3 = ((Cursor)localObject2).getString(i5);
          int i8 = ((Cursor)localObject2).getInt(i6);
          TruecallerContract.Filters.WildCardType localWildCardType = TruecallerContract.Filters.WildCardType.valueOfType(i7);
          Object localObject8 = TruecallerContract.Filters.WildCardType.CONTAIN;
          int i9 = type;
          Object localObject9;
          FilterManager.FilterAction localFilterAction;
          if (i7 == i9)
          {
            i9 = 0;
            localObject8 = null;
            boolean bool4 = TextUtils.isEmpty(null);
            if (!bool4)
            {
              bool3 = d(null, (String)localObject7, i7);
              if (bool3)
              {
                localObject7 = new com/truecaller/filters/g;
                localObject8 = FilterManager.FilterAction.FILTER_BLACKLISTED;
                localObject9 = FilterManager.ActionSource.CUSTOM_BLACKLIST;
                localFilterAction = null;
                ((g)localObject7).<init>(l, (FilterManager.FilterAction)localObject8, (FilterManager.ActionSource)localObject9, str3, i8, 0, localWildCardType);
                ((List)localObject3).add(localObject7);
                continue;
              }
            }
          }
          boolean bool3 = TextUtils.isEmpty(paramString1);
          if (!bool3)
          {
            localObject9 = paramString1;
            bool3 = d(paramString1, (String)localObject7, i7);
            if (bool3)
            {
              localObject8 = paramString2;
              break label461;
            }
          }
          else
          {
            localObject9 = paramString1;
          }
          bool3 = TextUtils.isEmpty(paramString2);
          if (!bool3)
          {
            localObject8 = paramString2;
            boolean bool2 = d(paramString2, (String)localObject7, i7);
            if (bool2)
            {
              label461:
              localObject7 = new com/truecaller/filters/g;
              localFilterAction = FilterManager.FilterAction.FILTER_BLACKLISTED;
              FilterManager.ActionSource localActionSource = FilterManager.ActionSource.CUSTOM_BLACKLIST;
              localObject8 = localFilterAction;
              localObject9 = localActionSource;
              localFilterAction = null;
              ((g)localObject7).<init>(l, (FilterManager.FilterAction)localObject8, localActionSource, str3, i8, 0, localWildCardType);
              ((List)localObject3).add(localObject7);
            }
          }
        }
      }
      finally
      {
        ((Cursor)localObject2).close();
      }
    }
    return localList;
  }
  
  private boolean c(String paramString, c.f paramf)
  {
    Object localObject = g.O();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool1)
    {
      localObject = c;
      bool1 = ((p)localObject).c();
      if (bool1)
      {
        paramf = (Boolean)paramf.b();
        boolean bool2 = paramf.booleanValue();
        if (!bool2)
        {
          paramf = h.a();
          localObject = i.getNetworkCountryIso();
          String str = i.getSimCountryIso();
          int n = 3;
          String[] arrayOfString = new String[n];
          arrayOfString[0] = paramf;
          bool2 = true;
          arrayOfString[bool2] = localObject;
          int m = 2;
          arrayOfString[m] = str;
          boolean bool3 = a(paramString, arrayOfString);
          if (!bool3) {
            return bool2;
          }
          return false;
        }
      }
    }
    return false;
  }
  
  private static boolean d(String paramString1, String paramString2, int paramInt)
  {
    f.c localc = new com/truecaller/filters/f$c;
    localc.<init>(paramString2, paramInt);
    return localc.a(paramString1);
  }
  
  public final int a(List paramList, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      String str1 = (String)paramList.next();
      boolean bool2 = TextUtils.isEmpty(str1);
      if (!bool2)
      {
        String str2 = am.n(paramString2);
        int m = 1;
        TruecallerContract.Filters.EntityType localEntityType = TruecallerContract.Filters.EntityType.UNKNOWN;
        Object localObject = str1;
        localObject = a(str1, paramString1, str2, m, paramString3, localEntityType);
        localArrayList.add(localObject);
        localObject = this;
        str2 = paramString1;
        a(str1, paramString1, paramString4, paramString3, paramBoolean);
      }
    }
    boolean bool3 = localArrayList.isEmpty();
    if (!bool3) {
      a(localArrayList);
    }
    return localArrayList.size();
  }
  
  public final int a(List paramList, String paramString1, String paramString2, String paramString3, boolean paramBoolean, TruecallerContract.Filters.WildCardType paramWildCardType, TruecallerContract.Filters.EntityType paramEntityType)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (String)paramList.next();
      localObject2 = new c/n;
      ((n)localObject2).<init>(localObject1, paramString2);
      localArrayList.add(localObject2);
    }
    Object localObject1 = this;
    Object localObject2 = paramString1;
    return a(localArrayList, paramString1, paramString3, paramBoolean, paramWildCardType, paramEntityType);
  }
  
  public final int a(List paramList, String paramString1, String paramString2, boolean paramBoolean, TruecallerContract.Filters.WildCardType paramWildCardType, TruecallerContract.Filters.EntityType paramEntityType)
  {
    TruecallerContract.Filters.WildCardType localWildCardType = paramWildCardType;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Iterator localIterator = paramList.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (n)localIterator.next();
      Object localObject2 = (CharSequence)a;
      boolean bool2 = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool2)
      {
        localObject2 = TruecallerContract.Filters.WildCardType.NONE;
        if (localWildCardType == localObject2)
        {
          localObject2 = a;
          localObject3 = localObject2;
          localObject3 = (String)localObject2;
          str1 = am.n((String)b);
          str2 = null;
          localObject4 = paramString1;
          localObject2 = a((String)localObject3, paramString1, str1, 0, paramString2, paramEntityType);
          localArrayList.add(localObject2);
        }
        else
        {
          localObject2 = (String)a;
          str2 = am.n((String)b);
          localObject4 = localWildCardType.formatPattern((String)localObject2);
          Pattern.compile((String)localObject4);
          str1 = "REG_EXP";
          TruecallerContract.Filters.EntityType localEntityType = TruecallerContract.Filters.EntityType.UNKNOWN;
          localObject2 = a((String)localObject4, str1, str2, 0, paramString2, localEntityType);
          localObject3 = "wildcard_type";
          int m = type;
          localObject4 = Integer.valueOf(m);
          ((ContentValues)localObject2).put((String)localObject3, (Integer)localObject4);
          localArrayList.add(localObject2);
        }
        localObject1 = a;
        Object localObject3 = localObject1;
        localObject3 = (String)localObject1;
        String str1 = "block";
        localObject2 = this;
        Object localObject4 = paramString1;
        String str2 = paramString2;
        a((String)localObject3, paramString1, str1, paramString2, paramBoolean);
      }
    }
    boolean bool3 = localArrayList.isEmpty();
    if (!bool3) {
      a(localArrayList);
    }
    return localArrayList.size();
  }
  
  public final g a(String paramString)
  {
    return a(paramString, null, null, false);
  }
  
  public final g a(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    Object localObject1 = b(paramString1, paramString2, paramBoolean);
    if (localObject1 != null) {
      return (g)localObject1;
    }
    localObject1 = new com/truecaller/filters/-$$Lambda$f$gwA-od1EsMZv1k1JzBjqllBVpOc;
    ((-..Lambda.f.gwA-od1EsMZv1k1JzBjqllBVpOc)localObject1).<init>(this, paramString1);
    localObject1 = c.g.a((c.g.a.a)localObject1);
    if (paramBoolean)
    {
      boolean bool1 = a((c.f)localObject1);
      if (bool1) {
        return g.d;
      }
    }
    paramString1 = am.n(paramString1);
    paramString2 = am.n(paramString2);
    paramString3 = aa.c(paramString3);
    String str1;
    if (paramString3 == null) {
      str1 = paramString1;
    } else {
      str1 = aa.b(paramString1, paramString3);
    }
    if (paramString3 == null) {
      paramString3 = paramString2;
    } else {
      paramString3 = aa.b(paramString2, paramString3);
    }
    boolean bool2 = TextUtils.equals(paramString1, str1);
    int m = 0;
    String str2 = null;
    boolean bool3 = true;
    if (bool2)
    {
      bool2 = TextUtils.equals(paramString2, paramString3);
      if (bool2)
      {
        bool2 = false;
        break label152;
      }
    }
    bool2 = true;
    label152:
    Object localObject2 = b(str1, paramString3);
    if (localObject2 != null) {
      return (g)localObject2;
    }
    if (bool2)
    {
      localObject2 = b(paramString1, paramString2);
      if (localObject2 != null) {
        return (g)localObject2;
      }
    }
    int n = 4;
    localObject2 = new String[n];
    localObject2[0] = str1;
    localObject2[bool3] = paramString3;
    localObject2[2] = paramString1;
    m = 3;
    localObject2[m] = paramString2;
    str2 = a((String[])localObject2);
    if (paramBoolean)
    {
      bool3 = c(str2, (c.f)localObject1);
      if (bool3) {
        return g.e;
      }
    }
    if (paramBoolean)
    {
      bool3 = b(str1, (c.f)localObject1);
      if (bool3) {
        return g.f;
      }
    }
    if (paramBoolean)
    {
      boolean bool4 = a(str1, (c.f)localObject1);
      if (bool4) {
        return g.g;
      }
    }
    if (str2 != null)
    {
      localObject1 = c(str2);
      if (localObject1 != null) {
        return (g)localObject1;
      }
    }
    paramString3 = c(str1, paramString3, paramBoolean);
    if (paramString3 != null) {
      return paramString3;
    }
    if (bool2)
    {
      paramString3 = c(paramString1, paramString2, paramBoolean);
      if (paramString3 != null) {
        return paramString3;
      }
    }
    paramString1 = a(paramString1, paramString2);
    if (paramString1 != null) {
      return paramString1;
    }
    return g.a;
  }
  
  public final Collection a(String paramString1, String paramString2, boolean paramBoolean)
  {
    LinkedHashSet localLinkedHashSet = new java/util/LinkedHashSet;
    localLinkedHashSet.<init>();
    g localg1 = b(paramString1, paramString2, paramBoolean);
    if (localg1 != null) {
      localLinkedHashSet.add(localg1);
    }
    int m = -1 >>> 1;
    Object localObject1 = a(paramString1, paramString2, m);
    localLinkedHashSet.addAll((Collection)localObject1);
    localObject1 = b(paramString1, paramString2, m).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      g localg2 = (g)((Iterator)localObject1).next();
      localg2 = a(localg2, paramBoolean);
      localLinkedHashSet.add(localg2);
    }
    paramBoolean = true;
    Object localObject2 = new String[paramBoolean];
    localObject1 = null;
    localObject2[0] = paramString2;
    int n = 1;
    localObject2[n] = paramString1;
    localObject2 = a((String[])localObject2);
    if (localObject2 != null)
    {
      localObject2 = c((String)localObject2);
      if (localObject2 != null) {
        localLinkedHashSet.add(localObject2);
      }
    }
    paramString1 = c(paramString1, paramString2, m);
    localLinkedHashSet.addAll(paramString1);
    return localLinkedHashSet;
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    com.truecaller.tracking.events.c.a locala = com.truecaller.tracking.events.c.b().a(paramString1).d(paramString3).e(paramString4);
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("ViewAction");
    Object localObject2 = "Action";
    paramString3 = ((e.a)localObject1).a((String)localObject2, paramString3).a("Context", paramString4);
    paramString4 = "REG_EXP";
    boolean bool1 = paramString2.equals(paramString4);
    localObject1 = null;
    if (!bool1)
    {
      paramString4 = "COUNTRY_CODE";
      bool1 = paramString2.equals(paramString4);
      if (!bool1)
      {
        paramString2 = at.b();
        paramString4 = a(paramString1);
        localObject2 = h;
        Object localObject3 = FilterManager.FilterAction.ALLOW_WHITELISTED;
        int m = 1;
        if (localObject2 == localObject3)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localObject2 = null;
        }
        paramString2.c(bool2);
        localObject2 = h;
        localObject3 = FilterManager.FilterAction.FILTER_BLACKLISTED;
        if (localObject2 != localObject3)
        {
          localObject2 = h;
          localObject3 = FilterManager.FilterAction.FILTER_DISABLED;
          if (localObject2 != localObject3)
          {
            paramString2.b(false);
            paramString2.a(false);
            break label279;
          }
        }
        localObject2 = j;
        localObject3 = FilterManager.ActionSource.CUSTOM_BLACKLIST;
        if (localObject2 == localObject3)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localObject2 = null;
        }
        paramString2.b(bool2);
        paramString4 = j;
        localObject2 = FilterManager.ActionSource.TOP_SPAMMER;
        if (paramString4 == localObject2)
        {
          bool1 = true;
        }
        else
        {
          bool1 = false;
          paramString4 = null;
        }
        paramString2.a(bool1);
        label279:
        boolean bool3;
        try
        {
          paramString1 = aa.a(paramString1);
        }
        catch (com.google.c.a.g localg)
        {
          bool3 = false;
          paramString1 = null;
        }
        if (paramString1 == null)
        {
          bool1 = false;
          paramString4 = null;
        }
        else
        {
          paramString4 = new com/truecaller/data/access/c;
          localObject2 = b;
          paramString4.<init>((Context)localObject2);
          paramString4 = paramString4.b(paramString1);
        }
        if (paramString4 != null)
        {
          bool2 = paramString4.U();
          if (bool2)
          {
            bool2 = true;
            break label361;
          }
        }
        boolean bool2 = false;
        localObject2 = null;
        label361:
        paramString2.d(bool2);
        localObject2 = "+";
        bool2 = k.a(paramString1, (CharSequence)localObject2, false);
        if (bool2)
        {
          paramString1 = paramString1.substring(m);
          localObject2 = "SubAction";
          localObject3 = "numeric";
          paramString3.a((String)localObject2, (String)localObject3);
        }
        else
        {
          localObject2 = "SubAction";
          localObject3 = "alphanumeric";
          paramString3.a((String)localObject2, (String)localObject3);
        }
        localObject2 = "address";
        locala.b((CharSequence)localObject2);
        locala.c(paramString1);
        paramString1 = paramString2.a();
        locala.a(paramString1);
        if ((paramBoolean) && (paramString4 != null))
        {
          bool3 = paramString4.U();
          if (bool3)
          {
            int n = paramString4.I();
            localObject1 = Integer.valueOf(n);
          }
        }
        locala.a((Integer)localObject1);
        break label588;
      }
    }
    paramString1 = "COUNTRY_CODE";
    boolean bool4 = paramString2.equals(paramString1);
    if (bool4)
    {
      paramString1 = "country";
      paramString2 = "country";
    }
    else
    {
      paramString1 = "wildcard";
      paramString2 = "wildcard";
    }
    locala.b(paramString1);
    locala.c(null);
    locala.a(null);
    locala.a(null);
    paramString1 = "SubAction";
    paramString3.a(paramString1, paramString2);
    try
    {
      label588:
      paramString1 = d;
      paramString1 = paramString1.a();
      paramString1 = (ae)paramString1;
      paramString2 = locala.a();
      paramString1.a(paramString2);
    }
    catch (org.apache.a.a paramString1)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramString1);
    }
    paramString1 = e;
    paramString2 = paramString3.a();
    paramString1.a(paramString2);
  }
  
  /* Error */
  public final boolean a()
  {
    // Byte code:
    //   0: iconst_1
    //   1: anewarray 194	java/lang/String
    //   4: dup
    //   5: iconst_0
    //   6: ldc_w 773
    //   9: aastore
    //   10: invokestatic 777	com/truecaller/log/AssertionUtil:notOnMainThread	([Ljava/lang/String;)V
    //   13: aload_0
    //   14: getfield 50	com/truecaller/filters/f:b	Landroid/content/Context;
    //   17: invokevirtual 219	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   20: astore_1
    //   21: invokestatic 224	com/truecaller/content/TruecallerContract$Filters:a	()Landroid/net/Uri;
    //   24: astore_2
    //   25: ldc_w 779
    //   28: astore_3
    //   29: iconst_2
    //   30: anewarray 194	java/lang/String
    //   33: dup
    //   34: iconst_0
    //   35: ldc_w 781
    //   38: aastore
    //   39: dup
    //   40: iconst_1
    //   41: ldc_w 489
    //   44: aastore
    //   45: astore 4
    //   47: iconst_0
    //   48: istore 5
    //   50: aconst_null
    //   51: astore 6
    //   53: aconst_null
    //   54: astore 7
    //   56: aload_1
    //   57: aload_2
    //   58: aconst_null
    //   59: aload_3
    //   60: aload 4
    //   62: aconst_null
    //   63: invokevirtual 235	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   66: astore 8
    //   68: iconst_0
    //   69: istore 9
    //   71: aconst_null
    //   72: astore_1
    //   73: aload 8
    //   75: ifnull +1092 -> 1167
    //   78: iconst_1
    //   79: istore 10
    //   81: aload 8
    //   83: invokeinterface 784 1 0
    //   88: istore 5
    //   90: iload 5
    //   92: ifne +6 -> 98
    //   95: goto +1072 -> 1167
    //   98: new 209	java/util/ArrayList
    //   101: astore 6
    //   103: aload 6
    //   105: invokespecial 210	java/util/ArrayList:<init>	()V
    //   108: aload 8
    //   110: aload 6
    //   112: invokestatic 789	com/truecaller/common/c/b/a:a	(Landroid/database/Cursor;Ljava/util/Collection;)V
    //   115: iload 10
    //   117: anewarray 194	java/lang/String
    //   120: astore_3
    //   121: new 307	java/lang/StringBuilder
    //   124: astore 4
    //   126: ldc_w 791
    //   129: astore 7
    //   131: aload 4
    //   133: aload 7
    //   135: invokespecial 312	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   138: aload 4
    //   140: aload 6
    //   142: invokevirtual 794	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: ldc_w 796
    //   149: astore 7
    //   151: aload 4
    //   153: aload 7
    //   155: invokevirtual 321	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   158: pop
    //   159: aload 4
    //   161: invokevirtual 325	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   164: astore 4
    //   166: aload_3
    //   167: iconst_0
    //   168: aload 4
    //   170: aastore
    //   171: new 209	java/util/ArrayList
    //   174: astore_3
    //   175: aload_3
    //   176: invokespecial 210	java/util/ArrayList:<init>	()V
    //   179: new 209	java/util/ArrayList
    //   182: astore 4
    //   184: aload 4
    //   186: invokespecial 210	java/util/ArrayList:<init>	()V
    //   189: aload 6
    //   191: invokeinterface 287 1 0
    //   196: astore 7
    //   198: aload 7
    //   200: invokeinterface 184 1 0
    //   205: istore 11
    //   207: iload 11
    //   209: ifeq +498 -> 707
    //   212: aload 7
    //   214: invokeinterface 188 1 0
    //   219: astore 12
    //   221: aload 12
    //   223: checkcast 70	android/content/ContentValues
    //   226: astore 12
    //   228: ldc 73
    //   230: astore 13
    //   232: aload 12
    //   234: aload 13
    //   236: invokevirtual 175	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   239: astore 13
    //   241: ldc 107
    //   243: astore 14
    //   245: aload 12
    //   247: aload 14
    //   249: invokevirtual 175	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   252: astore 14
    //   254: aload 14
    //   256: invokestatic 203	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   259: istore 15
    //   261: iload 15
    //   263: ifeq +8 -> 271
    //   266: ldc_w 798
    //   269: astore 14
    //   271: ldc 111
    //   273: astore 16
    //   275: aload 12
    //   277: aload 16
    //   279: invokevirtual 802	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   282: astore 16
    //   284: aload 16
    //   286: invokevirtual 434	java/lang/Integer:intValue	()I
    //   289: istore 15
    //   291: iload 15
    //   293: invokestatic 806	com/truecaller/content/TruecallerContract$Filters$EntityType:fromValue	(I)Lcom/truecaller/content/TruecallerContract$Filters$EntityType;
    //   296: astore 16
    //   298: ldc 104
    //   300: astore 17
    //   302: aload 12
    //   304: aload 17
    //   306: invokevirtual 802	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   309: astore 17
    //   311: aload 17
    //   313: invokevirtual 434	java/lang/Integer:intValue	()I
    //   316: istore 18
    //   318: iload 18
    //   320: iload 10
    //   322: if_icmpne +274 -> 596
    //   325: new 190	com/truecaller/filters/sync/c$a
    //   328: astore 17
    //   330: aload 17
    //   332: invokespecial 807	com/truecaller/filters/sync/c$a:<init>	()V
    //   335: ldc_w 809
    //   338: astore 19
    //   340: aload 12
    //   342: aload 19
    //   344: invokevirtual 175	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   347: astore 19
    //   349: aload 17
    //   351: aload 19
    //   353: putfield 810	com/truecaller/filters/sync/c$a:a	Ljava/lang/String;
    //   356: aload 17
    //   358: aload 13
    //   360: putfield 192	com/truecaller/filters/sync/c$a:b	Ljava/lang/String;
    //   363: ldc 79
    //   365: astore 13
    //   367: aload 12
    //   369: aload 13
    //   371: invokevirtual 175	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   374: astore 13
    //   376: aload 17
    //   378: aload 13
    //   380: putfield 812	com/truecaller/filters/sync/c$a:c	Ljava/lang/String;
    //   383: ldc 81
    //   385: astore 13
    //   387: aload 12
    //   389: aload 13
    //   391: invokevirtual 802	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   394: astore 13
    //   396: iconst_m1
    //   397: istore 20
    //   399: iload 20
    //   401: invokestatic 87	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   404: astore 19
    //   406: aload 13
    //   408: aload 19
    //   410: invokestatic 817	org/c/a/a/a/h:a	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   413: astore 13
    //   415: aload 13
    //   417: checkcast 83	java/lang/Integer
    //   420: astore 13
    //   422: aload 13
    //   424: invokevirtual 434	java/lang/Integer:intValue	()I
    //   427: istore 20
    //   429: iload 20
    //   431: tableswitch	default:+21->452, 0:+46->477, 1:+31->462
    //   452: iload 10
    //   454: anewarray 194	java/lang/String
    //   457: astore 12
    //   459: goto +102 -> 561
    //   462: ldc_w 819
    //   465: astore 13
    //   467: aload 17
    //   469: aload 13
    //   471: putfield 821	com/truecaller/filters/sync/c$a:d	Ljava/lang/String;
    //   474: goto +15 -> 489
    //   477: ldc_w 823
    //   480: astore 13
    //   482: aload 17
    //   484: aload 13
    //   486: putfield 821	com/truecaller/filters/sync/c$a:d	Ljava/lang/String;
    //   489: aload 17
    //   491: aload 14
    //   493: putfield 825	com/truecaller/filters/sync/c$a:e	Ljava/lang/String;
    //   496: ldc 109
    //   498: astore 13
    //   500: aload 12
    //   502: aload 13
    //   504: invokevirtual 175	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   507: astore 12
    //   509: aload 17
    //   511: aload 12
    //   513: putfield 827	com/truecaller/filters/sync/c$a:f	Ljava/lang/String;
    //   516: getstatic 557	com/truecaller/content/TruecallerContract$Filters$EntityType:UNKNOWN	Lcom/truecaller/content/TruecallerContract$Filters$EntityType;
    //   519: astore 12
    //   521: aload 16
    //   523: aload 12
    //   525: if_acmpeq +24 -> 549
    //   528: aload 16
    //   530: getfield 115	com/truecaller/content/TruecallerContract$Filters$EntityType:value	I
    //   533: istore 11
    //   535: iload 11
    //   537: invokestatic 87	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   540: astore 12
    //   542: aload 17
    //   544: aload 12
    //   546: putfield 830	com/truecaller/filters/sync/c$a:g	Ljava/lang/Integer;
    //   549: aload_3
    //   550: aload 17
    //   552: invokeinterface 274 2 0
    //   557: pop
    //   558: goto -360 -> 198
    //   561: ldc_w 832
    //   564: astore 14
    //   566: aload 13
    //   568: invokestatic 337	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   571: astore 13
    //   573: aload 14
    //   575: aload 13
    //   577: invokevirtual 835	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   580: astore 13
    //   582: aload 12
    //   584: iconst_0
    //   585: aload 13
    //   587: aastore
    //   588: aload 12
    //   590: invokestatic 838	com/truecaller/log/AssertionUtil:report	([Ljava/lang/String;)V
    //   593: goto -395 -> 198
    //   596: ldc_w 809
    //   599: astore 13
    //   601: aload 12
    //   603: aload 13
    //   605: invokevirtual 175	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   608: astore 13
    //   610: ldc -19
    //   612: astore 14
    //   614: aload 12
    //   616: aload 14
    //   618: invokevirtual 842	android/content/ContentValues:getAsLong	(Ljava/lang/String;)Ljava/lang/Long;
    //   621: astore 12
    //   623: aload 12
    //   625: invokevirtual 848	java/lang/Long:longValue	()J
    //   628: lstore 21
    //   630: aload 13
    //   632: invokestatic 203	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   635: istore 11
    //   637: iload 11
    //   639: ifne +46 -> 685
    //   642: lload 21
    //   644: invokestatic 851	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   647: astore 12
    //   649: ldc_w 853
    //   652: astore 14
    //   654: aload 13
    //   656: aload 14
    //   658: invokestatic 858	java/net/URLEncoder:encode	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   661: astore 13
    //   663: aload 12
    //   665: aload 13
    //   667: invokestatic 863	android/support/v4/f/j:a	(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/f/j;
    //   670: astore 12
    //   672: aload 4
    //   674: aload 12
    //   676: invokeinterface 274 2 0
    //   681: pop
    //   682: goto -484 -> 198
    //   685: lload 21
    //   687: invokestatic 851	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   690: astore 12
    //   692: aload 12
    //   694: invokestatic 869	java/util/Collections:singletonList	(Ljava/lang/Object;)Ljava/util/List;
    //   697: astore 12
    //   699: aload 12
    //   701: invokestatic 872	com/truecaller/filters/f:a	(Ljava/util/List;)V
    //   704: goto -506 -> 198
    //   707: invokestatic 877	com/truecaller/filters/sync/a:a	()Lcom/truecaller/filters/sync/b;
    //   710: astore 7
    //   712: aload_3
    //   713: invokeinterface 167 1 0
    //   718: istore 11
    //   720: iload 11
    //   722: ifne +223 -> 945
    //   725: aload 7
    //   727: aload_3
    //   728: invokeinterface 882 2 0
    //   733: astore_3
    //   734: aload_3
    //   735: invokeinterface 887 1 0
    //   740: astore_3
    //   741: aload_3
    //   742: getfield 890	e/r:b	Ljava/lang/Object;
    //   745: astore_3
    //   746: aload_3
    //   747: checkcast 892	com/truecaller/filters/sync/c$b
    //   750: astore_3
    //   751: aload_3
    //   752: ifnull +190 -> 942
    //   755: aload_3
    //   756: getfield 895	com/truecaller/filters/sync/c$b:a	Ljava/util/List;
    //   759: astore 12
    //   761: aload 12
    //   763: ifnull +179 -> 942
    //   766: aload_3
    //   767: getfield 895	com/truecaller/filters/sync/c$b:a	Ljava/util/List;
    //   770: astore 12
    //   772: aload 12
    //   774: invokeinterface 167 1 0
    //   779: istore 11
    //   781: iload 11
    //   783: ifne +159 -> 942
    //   786: aload 6
    //   788: invokeinterface 287 1 0
    //   793: astore 6
    //   795: iconst_0
    //   796: istore 11
    //   798: fconst_0
    //   799: fstore 23
    //   801: aconst_null
    //   802: astore 12
    //   804: aload 6
    //   806: invokeinterface 184 1 0
    //   811: istore 24
    //   813: iload 24
    //   815: ifeq +120 -> 935
    //   818: aload 6
    //   820: invokeinterface 188 1 0
    //   825: astore 13
    //   827: aload 13
    //   829: checkcast 70	android/content/ContentValues
    //   832: astore 13
    //   834: aload_3
    //   835: getfield 895	com/truecaller/filters/sync/c$b:a	Ljava/util/List;
    //   838: astore 14
    //   840: aload 13
    //   842: aload 14
    //   844: invokestatic 898	com/truecaller/filters/f:a	(Landroid/content/ContentValues;Ljava/util/List;)Lcom/truecaller/filters/sync/c$a;
    //   847: astore 14
    //   849: aload 14
    //   851: ifnull +74 -> 925
    //   854: ldc 104
    //   856: astore 16
    //   858: iconst_0
    //   859: invokestatic 87	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   862: astore 17
    //   864: aload 13
    //   866: aload 16
    //   868: aload 17
    //   870: invokevirtual 90	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   873: ldc_w 809
    //   876: astore 16
    //   878: aload 14
    //   880: getfield 810	com/truecaller/filters/sync/c$a:a	Ljava/lang/String;
    //   883: astore 14
    //   885: aload 13
    //   887: aload 16
    //   889: aload 14
    //   891: invokevirtual 77	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   894: aload_0
    //   895: getfield 50	com/truecaller/filters/f:b	Landroid/content/Context;
    //   898: astore 14
    //   900: aload 14
    //   902: invokevirtual 219	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   905: astore 14
    //   907: invokestatic 224	com/truecaller/content/TruecallerContract$Filters:a	()Landroid/net/Uri;
    //   910: astore 16
    //   912: aload 14
    //   914: aload 16
    //   916: aload 13
    //   918: invokevirtual 902	android/content/ContentResolver:insert	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   921: pop
    //   922: goto -118 -> 804
    //   925: iconst_1
    //   926: istore 11
    //   928: ldc 105
    //   930: fstore 23
    //   932: goto -128 -> 804
    //   935: iload 11
    //   937: istore 9
    //   939: goto +6 -> 945
    //   942: iconst_1
    //   943: istore 9
    //   945: ldc_w 904
    //   948: astore 6
    //   950: new 906	com/truecaller/filters/f$b
    //   953: astore_3
    //   954: aload 6
    //   956: invokeinterface 907 1 0
    //   961: astore 6
    //   963: aload_3
    //   964: aload 6
    //   966: invokespecial 908	com/truecaller/filters/f$b:<init>	(Ljava/lang/String;)V
    //   969: aload_3
    //   970: aload 4
    //   972: invokevirtual 911	com/truecaller/filters/f$b:a	(Ljava/lang/Iterable;)Ljava/lang/String;
    //   975: astore 6
    //   977: aload 6
    //   979: invokestatic 913	com/truecaller/common/h/am:b	(Ljava/lang/CharSequence;)Z
    //   982: istore 25
    //   984: iload 25
    //   986: ifne +124 -> 1110
    //   989: aload 7
    //   991: aload 6
    //   993: invokeinterface 916 2 0
    //   998: astore 6
    //   1000: aload 6
    //   1002: invokeinterface 887 1 0
    //   1007: astore 6
    //   1009: aload 6
    //   1011: getfield 919	e/r:a	Lokhttp3/ad;
    //   1014: astore 6
    //   1016: aload 6
    //   1018: invokevirtual 922	okhttp3/ad:c	()Z
    //   1021: istore 5
    //   1023: iload 5
    //   1025: ifeq +89 -> 1114
    //   1028: new 209	java/util/ArrayList
    //   1031: astore 6
    //   1033: aload 4
    //   1035: invokeinterface 253 1 0
    //   1040: istore 25
    //   1042: aload 6
    //   1044: iload 25
    //   1046: invokespecial 925	java/util/ArrayList:<init>	(I)V
    //   1049: aload 4
    //   1051: invokeinterface 179 1 0
    //   1056: astore_3
    //   1057: aload_3
    //   1058: invokeinterface 184 1 0
    //   1063: istore 26
    //   1065: iload 26
    //   1067: ifeq +38 -> 1105
    //   1070: aload_3
    //   1071: invokeinterface 188 1 0
    //   1076: astore 4
    //   1078: aload 4
    //   1080: checkcast 860	android/support/v4/f/j
    //   1083: astore 4
    //   1085: aload 4
    //   1087: getfield 926	android/support/v4/f/j:a	Ljava/lang/Object;
    //   1090: astore 4
    //   1092: aload 6
    //   1094: aload 4
    //   1096: invokeinterface 274 2 0
    //   1101: pop
    //   1102: goto -45 -> 1057
    //   1105: aload 6
    //   1107: invokestatic 872	com/truecaller/filters/f:a	(Ljava/util/List;)V
    //   1110: iload 9
    //   1112: istore 10
    //   1114: aload 8
    //   1116: ifnull +34 -> 1150
    //   1119: aload 8
    //   1121: invokeinterface 277 1 0
    //   1126: goto +24 -> 1150
    //   1129: astore_1
    //   1130: goto +23 -> 1153
    //   1133: astore_1
    //   1134: goto +4 -> 1138
    //   1137: astore_1
    //   1138: aload_1
    //   1139: invokestatic 759	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1142: aload 8
    //   1144: ifnull +6 -> 1150
    //   1147: goto -28 -> 1119
    //   1150: iload 10
    //   1152: ireturn
    //   1153: aload 8
    //   1155: ifnull +10 -> 1165
    //   1158: aload 8
    //   1160: invokeinterface 277 1 0
    //   1165: aload_1
    //   1166: athrow
    //   1167: aload 8
    //   1169: ifnull +10 -> 1179
    //   1172: aload 8
    //   1174: invokeinterface 277 1 0
    //   1179: iconst_0
    //   1180: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1181	0	this	f
    //   20	53	1	localContentResolver	ContentResolver
    //   1129	1	1	localObject1	Object
    //   1133	1	1	localRuntimeException	RuntimeException
    //   1137	29	1	localIOException	IOException
    //   24	34	2	localUri	Uri
    //   28	1043	3	localObject2	Object
    //   45	1050	4	localObject3	Object
    //   48	43	5	m	int
    //   1021	3	5	bool1	boolean
    //   51	1055	6	localObject4	Object
    //   54	936	7	localObject5	Object
    //   66	1107	8	localCursor	Cursor
    //   69	1042	9	n	int
    //   79	1072	10	i1	int
    //   205	3	11	bool2	boolean
    //   533	3	11	i2	int
    //   635	301	11	i3	int
    //   219	584	12	localObject6	Object
    //   230	687	13	localObject7	Object
    //   243	670	14	localObject8	Object
    //   259	3	15	bool3	boolean
    //   289	3	15	i4	int
    //   273	642	16	localObject9	Object
    //   300	569	17	localObject10	Object
    //   316	7	18	i5	int
    //   338	71	19	localObject11	Object
    //   397	33	20	i6	int
    //   628	58	21	l	long
    //   799	132	23	f1	float
    //   811	3	24	bool4	boolean
    //   982	3	25	bool5	boolean
    //   1040	5	25	i7	int
    //   1063	3	26	bool6	boolean
    // Exception table:
    //   from	to	target	type
    //   81	88	1129	finally
    //   98	101	1129	finally
    //   103	108	1129	finally
    //   110	115	1129	finally
    //   115	120	1129	finally
    //   121	124	1129	finally
    //   133	138	1129	finally
    //   140	146	1129	finally
    //   153	159	1129	finally
    //   159	164	1129	finally
    //   168	171	1129	finally
    //   171	174	1129	finally
    //   175	179	1129	finally
    //   179	182	1129	finally
    //   184	189	1129	finally
    //   189	196	1129	finally
    //   198	205	1129	finally
    //   212	219	1129	finally
    //   221	226	1129	finally
    //   234	239	1129	finally
    //   247	252	1129	finally
    //   254	259	1129	finally
    //   277	282	1129	finally
    //   284	289	1129	finally
    //   291	296	1129	finally
    //   304	309	1129	finally
    //   311	316	1129	finally
    //   325	328	1129	finally
    //   330	335	1129	finally
    //   342	347	1129	finally
    //   351	356	1129	finally
    //   358	363	1129	finally
    //   369	374	1129	finally
    //   378	383	1129	finally
    //   389	394	1129	finally
    //   399	404	1129	finally
    //   408	413	1129	finally
    //   415	420	1129	finally
    //   422	427	1129	finally
    //   452	457	1129	finally
    //   469	474	1129	finally
    //   484	489	1129	finally
    //   491	496	1129	finally
    //   502	507	1129	finally
    //   511	516	1129	finally
    //   516	519	1129	finally
    //   528	533	1129	finally
    //   535	540	1129	finally
    //   544	549	1129	finally
    //   550	558	1129	finally
    //   566	571	1129	finally
    //   575	580	1129	finally
    //   585	588	1129	finally
    //   588	593	1129	finally
    //   603	608	1129	finally
    //   616	621	1129	finally
    //   623	628	1129	finally
    //   630	635	1129	finally
    //   642	647	1129	finally
    //   656	661	1129	finally
    //   665	670	1129	finally
    //   674	682	1129	finally
    //   685	690	1129	finally
    //   692	697	1129	finally
    //   699	704	1129	finally
    //   707	710	1129	finally
    //   712	718	1129	finally
    //   727	733	1129	finally
    //   734	740	1129	finally
    //   741	745	1129	finally
    //   746	750	1129	finally
    //   755	759	1129	finally
    //   766	770	1129	finally
    //   772	779	1129	finally
    //   786	793	1129	finally
    //   804	811	1129	finally
    //   818	825	1129	finally
    //   827	832	1129	finally
    //   834	838	1129	finally
    //   842	847	1129	finally
    //   858	862	1129	finally
    //   868	873	1129	finally
    //   878	883	1129	finally
    //   889	894	1129	finally
    //   894	898	1129	finally
    //   900	905	1129	finally
    //   907	910	1129	finally
    //   916	922	1129	finally
    //   950	953	1129	finally
    //   954	961	1129	finally
    //   964	969	1129	finally
    //   970	975	1129	finally
    //   977	982	1129	finally
    //   991	998	1129	finally
    //   1000	1007	1129	finally
    //   1009	1014	1129	finally
    //   1016	1021	1129	finally
    //   1028	1031	1129	finally
    //   1033	1040	1129	finally
    //   1044	1049	1129	finally
    //   1049	1056	1129	finally
    //   1057	1063	1129	finally
    //   1070	1076	1129	finally
    //   1078	1083	1129	finally
    //   1085	1090	1129	finally
    //   1094	1102	1129	finally
    //   1105	1110	1129	finally
    //   1138	1142	1129	finally
    //   81	88	1133	java/lang/RuntimeException
    //   98	101	1133	java/lang/RuntimeException
    //   103	108	1133	java/lang/RuntimeException
    //   110	115	1133	java/lang/RuntimeException
    //   115	120	1133	java/lang/RuntimeException
    //   121	124	1133	java/lang/RuntimeException
    //   133	138	1133	java/lang/RuntimeException
    //   140	146	1133	java/lang/RuntimeException
    //   153	159	1133	java/lang/RuntimeException
    //   159	164	1133	java/lang/RuntimeException
    //   168	171	1133	java/lang/RuntimeException
    //   171	174	1133	java/lang/RuntimeException
    //   175	179	1133	java/lang/RuntimeException
    //   179	182	1133	java/lang/RuntimeException
    //   184	189	1133	java/lang/RuntimeException
    //   189	196	1133	java/lang/RuntimeException
    //   198	205	1133	java/lang/RuntimeException
    //   212	219	1133	java/lang/RuntimeException
    //   221	226	1133	java/lang/RuntimeException
    //   234	239	1133	java/lang/RuntimeException
    //   247	252	1133	java/lang/RuntimeException
    //   254	259	1133	java/lang/RuntimeException
    //   277	282	1133	java/lang/RuntimeException
    //   284	289	1133	java/lang/RuntimeException
    //   291	296	1133	java/lang/RuntimeException
    //   304	309	1133	java/lang/RuntimeException
    //   311	316	1133	java/lang/RuntimeException
    //   325	328	1133	java/lang/RuntimeException
    //   330	335	1133	java/lang/RuntimeException
    //   342	347	1133	java/lang/RuntimeException
    //   351	356	1133	java/lang/RuntimeException
    //   358	363	1133	java/lang/RuntimeException
    //   369	374	1133	java/lang/RuntimeException
    //   378	383	1133	java/lang/RuntimeException
    //   389	394	1133	java/lang/RuntimeException
    //   399	404	1133	java/lang/RuntimeException
    //   408	413	1133	java/lang/RuntimeException
    //   415	420	1133	java/lang/RuntimeException
    //   422	427	1133	java/lang/RuntimeException
    //   452	457	1133	java/lang/RuntimeException
    //   469	474	1133	java/lang/RuntimeException
    //   484	489	1133	java/lang/RuntimeException
    //   491	496	1133	java/lang/RuntimeException
    //   502	507	1133	java/lang/RuntimeException
    //   511	516	1133	java/lang/RuntimeException
    //   516	519	1133	java/lang/RuntimeException
    //   528	533	1133	java/lang/RuntimeException
    //   535	540	1133	java/lang/RuntimeException
    //   544	549	1133	java/lang/RuntimeException
    //   550	558	1133	java/lang/RuntimeException
    //   566	571	1133	java/lang/RuntimeException
    //   575	580	1133	java/lang/RuntimeException
    //   585	588	1133	java/lang/RuntimeException
    //   588	593	1133	java/lang/RuntimeException
    //   603	608	1133	java/lang/RuntimeException
    //   616	621	1133	java/lang/RuntimeException
    //   623	628	1133	java/lang/RuntimeException
    //   630	635	1133	java/lang/RuntimeException
    //   642	647	1133	java/lang/RuntimeException
    //   656	661	1133	java/lang/RuntimeException
    //   665	670	1133	java/lang/RuntimeException
    //   674	682	1133	java/lang/RuntimeException
    //   685	690	1133	java/lang/RuntimeException
    //   692	697	1133	java/lang/RuntimeException
    //   699	704	1133	java/lang/RuntimeException
    //   707	710	1133	java/lang/RuntimeException
    //   712	718	1133	java/lang/RuntimeException
    //   727	733	1133	java/lang/RuntimeException
    //   734	740	1133	java/lang/RuntimeException
    //   741	745	1133	java/lang/RuntimeException
    //   746	750	1133	java/lang/RuntimeException
    //   755	759	1133	java/lang/RuntimeException
    //   766	770	1133	java/lang/RuntimeException
    //   772	779	1133	java/lang/RuntimeException
    //   786	793	1133	java/lang/RuntimeException
    //   804	811	1133	java/lang/RuntimeException
    //   818	825	1133	java/lang/RuntimeException
    //   827	832	1133	java/lang/RuntimeException
    //   834	838	1133	java/lang/RuntimeException
    //   842	847	1133	java/lang/RuntimeException
    //   858	862	1133	java/lang/RuntimeException
    //   868	873	1133	java/lang/RuntimeException
    //   878	883	1133	java/lang/RuntimeException
    //   889	894	1133	java/lang/RuntimeException
    //   894	898	1133	java/lang/RuntimeException
    //   900	905	1133	java/lang/RuntimeException
    //   907	910	1133	java/lang/RuntimeException
    //   916	922	1133	java/lang/RuntimeException
    //   950	953	1133	java/lang/RuntimeException
    //   954	961	1133	java/lang/RuntimeException
    //   964	969	1133	java/lang/RuntimeException
    //   970	975	1133	java/lang/RuntimeException
    //   977	982	1133	java/lang/RuntimeException
    //   991	998	1133	java/lang/RuntimeException
    //   1000	1007	1133	java/lang/RuntimeException
    //   1009	1014	1133	java/lang/RuntimeException
    //   1016	1021	1133	java/lang/RuntimeException
    //   1028	1031	1133	java/lang/RuntimeException
    //   1033	1040	1133	java/lang/RuntimeException
    //   1044	1049	1133	java/lang/RuntimeException
    //   1049	1056	1133	java/lang/RuntimeException
    //   1057	1063	1133	java/lang/RuntimeException
    //   1070	1076	1133	java/lang/RuntimeException
    //   1078	1083	1133	java/lang/RuntimeException
    //   1085	1090	1133	java/lang/RuntimeException
    //   1094	1102	1133	java/lang/RuntimeException
    //   1105	1110	1133	java/lang/RuntimeException
    //   81	88	1137	java/io/IOException
    //   98	101	1137	java/io/IOException
    //   103	108	1137	java/io/IOException
    //   110	115	1137	java/io/IOException
    //   115	120	1137	java/io/IOException
    //   121	124	1137	java/io/IOException
    //   133	138	1137	java/io/IOException
    //   140	146	1137	java/io/IOException
    //   153	159	1137	java/io/IOException
    //   159	164	1137	java/io/IOException
    //   168	171	1137	java/io/IOException
    //   171	174	1137	java/io/IOException
    //   175	179	1137	java/io/IOException
    //   179	182	1137	java/io/IOException
    //   184	189	1137	java/io/IOException
    //   189	196	1137	java/io/IOException
    //   198	205	1137	java/io/IOException
    //   212	219	1137	java/io/IOException
    //   221	226	1137	java/io/IOException
    //   234	239	1137	java/io/IOException
    //   247	252	1137	java/io/IOException
    //   254	259	1137	java/io/IOException
    //   277	282	1137	java/io/IOException
    //   284	289	1137	java/io/IOException
    //   291	296	1137	java/io/IOException
    //   304	309	1137	java/io/IOException
    //   311	316	1137	java/io/IOException
    //   325	328	1137	java/io/IOException
    //   330	335	1137	java/io/IOException
    //   342	347	1137	java/io/IOException
    //   351	356	1137	java/io/IOException
    //   358	363	1137	java/io/IOException
    //   369	374	1137	java/io/IOException
    //   378	383	1137	java/io/IOException
    //   389	394	1137	java/io/IOException
    //   399	404	1137	java/io/IOException
    //   408	413	1137	java/io/IOException
    //   415	420	1137	java/io/IOException
    //   422	427	1137	java/io/IOException
    //   452	457	1137	java/io/IOException
    //   469	474	1137	java/io/IOException
    //   484	489	1137	java/io/IOException
    //   491	496	1137	java/io/IOException
    //   502	507	1137	java/io/IOException
    //   511	516	1137	java/io/IOException
    //   516	519	1137	java/io/IOException
    //   528	533	1137	java/io/IOException
    //   535	540	1137	java/io/IOException
    //   544	549	1137	java/io/IOException
    //   550	558	1137	java/io/IOException
    //   566	571	1137	java/io/IOException
    //   575	580	1137	java/io/IOException
    //   585	588	1137	java/io/IOException
    //   588	593	1137	java/io/IOException
    //   603	608	1137	java/io/IOException
    //   616	621	1137	java/io/IOException
    //   623	628	1137	java/io/IOException
    //   630	635	1137	java/io/IOException
    //   642	647	1137	java/io/IOException
    //   656	661	1137	java/io/IOException
    //   665	670	1137	java/io/IOException
    //   674	682	1137	java/io/IOException
    //   685	690	1137	java/io/IOException
    //   692	697	1137	java/io/IOException
    //   699	704	1137	java/io/IOException
    //   707	710	1137	java/io/IOException
    //   712	718	1137	java/io/IOException
    //   727	733	1137	java/io/IOException
    //   734	740	1137	java/io/IOException
    //   741	745	1137	java/io/IOException
    //   746	750	1137	java/io/IOException
    //   755	759	1137	java/io/IOException
    //   766	770	1137	java/io/IOException
    //   772	779	1137	java/io/IOException
    //   786	793	1137	java/io/IOException
    //   804	811	1137	java/io/IOException
    //   818	825	1137	java/io/IOException
    //   827	832	1137	java/io/IOException
    //   834	838	1137	java/io/IOException
    //   842	847	1137	java/io/IOException
    //   858	862	1137	java/io/IOException
    //   868	873	1137	java/io/IOException
    //   878	883	1137	java/io/IOException
    //   889	894	1137	java/io/IOException
    //   894	898	1137	java/io/IOException
    //   900	905	1137	java/io/IOException
    //   907	910	1137	java/io/IOException
    //   916	922	1137	java/io/IOException
    //   950	953	1137	java/io/IOException
    //   954	961	1137	java/io/IOException
    //   964	969	1137	java/io/IOException
    //   970	975	1137	java/io/IOException
    //   977	982	1137	java/io/IOException
    //   991	998	1137	java/io/IOException
    //   1000	1007	1137	java/io/IOException
    //   1009	1014	1137	java/io/IOException
    //   1016	1021	1137	java/io/IOException
    //   1028	1031	1137	java/io/IOException
    //   1033	1040	1137	java/io/IOException
    //   1044	1049	1137	java/io/IOException
    //   1049	1056	1137	java/io/IOException
    //   1057	1063	1137	java/io/IOException
    //   1070	1076	1137	java/io/IOException
    //   1078	1083	1137	java/io/IOException
    //   1085	1090	1137	java/io/IOException
    //   1094	1102	1137	java/io/IOException
    //   1105	1110	1137	java/io/IOException
  }
  
  public final g b(String paramString)
  {
    return a(paramString, null, null, true);
  }
  
  public final boolean b()
  {
    Object localObject1 = c;
    boolean bool = ((p)localObject1).h();
    if (bool)
    {
      localObject1 = new com/truecaller/filters/sync/c$c;
      ((c.c)localObject1).<init>();
      int m = c.g();
      a = m;
      m = c.a();
      b = m;
      m = c.b();
      c = m;
      m = c.c();
      d = m;
      m = c.d();
      e = m;
      m = c.e();
      g = m;
      Object localObject2 = c.f();
      if (localObject2 != null)
      {
        int n = ((Integer)localObject2).intValue();
        f = n;
      }
      try
      {
        localObject2 = com.truecaller.filters.sync.a.a();
        localObject1 = ((com.truecaller.filters.sync.b)localObject2).a((c.c)localObject1);
        localObject1 = ((e.b)localObject1).c();
        localObject1 = a;
        bool = ((ad)localObject1).c();
        if (bool)
        {
          localObject1 = c;
          ((p)localObject1).g(false);
        }
      }
      catch (IOException localIOException)
      {
        return true;
      }
    }
    return false;
  }
  
  public final boolean c()
  {
    Object localObject1 = b.getContentResolver();
    Uri localUri = TruecallerContract.Filters.a();
    Object localObject2 = null;
    int m = com.truecaller.common.c.b.b.a((ContentResolver)localObject1, localUri, null, null);
    boolean bool1 = true;
    if (m > 0)
    {
      AssertionUtil.report(new String[] { "Filters are already present" });
      return bool1;
    }
    localObject1 = com.truecaller.filters.sync.a.a();
    localObject2 = ((com.truecaller.filters.sync.b)localObject1).b().c();
    Object localObject3 = a;
    boolean bool2 = ((ad)localObject3).c();
    boolean bool4 = false;
    if (bool2)
    {
      localObject2 = (c.b)b;
      if (localObject2 != null)
      {
        localObject3 = a;
        if (localObject3 != null)
        {
          localObject3 = a;
          bool2 = ((List)localObject3).isEmpty();
          if (!bool2)
          {
            localObject3 = new java/util/ArrayList;
            ((ArrayList)localObject3).<init>();
            localObject2 = a.iterator();
            for (;;)
            {
              boolean bool5 = ((Iterator)localObject2).hasNext();
              if (!bool5) {
                break;
              }
              Object localObject4 = (com.truecaller.filters.sync.c.a)((Iterator)localObject2).next();
              try
              {
                Object localObject5 = b;
                boolean bool6 = TextUtils.isEmpty((CharSequence)localObject5);
                if (!bool6)
                {
                  localObject5 = new android/content/ContentValues;
                  ((ContentValues)localObject5).<init>();
                  Object localObject6 = "server_id";
                  Object localObject7 = a;
                  ((ContentValues)localObject5).put((String)localObject6, (String)localObject7);
                  localObject6 = "value";
                  localObject7 = b;
                  ((ContentValues)localObject5).put((String)localObject6, (String)localObject7);
                  localObject6 = "label";
                  localObject7 = c;
                  ((ContentValues)localObject5).put((String)localObject6, (String)localObject7);
                  localObject6 = "BLACKLIST";
                  localObject7 = d;
                  int i2 = ((String)localObject6).equals(localObject7);
                  if (i2 != 0)
                  {
                    i2 = 0;
                    localObject6 = null;
                  }
                  else
                  {
                    localObject6 = "WHITELIST";
                    localObject7 = d;
                    i2 = ((String)localObject6).equals(localObject7);
                    if (i2 == 0) {
                      break label619;
                    }
                    i2 = 1;
                  }
                  localObject7 = "rule";
                  localObject6 = Integer.valueOf(i2);
                  ((ContentValues)localObject5).put((String)localObject7, (Integer)localObject6);
                  localObject6 = b;
                  localObject6 = TruecallerContract.Filters.WildCardType.valueOfPattern((String)localObject6);
                  localObject7 = "wildcard_type";
                  int i3 = ((TruecallerContract.Filters.WildCardType)localObject6).getType();
                  localObject6 = Integer.valueOf(i3);
                  ((ContentValues)localObject5).put((String)localObject7, (Integer)localObject6);
                  localObject6 = "sync_state";
                  localObject7 = Integer.valueOf(0);
                  ((ContentValues)localObject5).put((String)localObject6, (Integer)localObject7);
                  localObject6 = "tracking_source";
                  localObject7 = f;
                  ((ContentValues)localObject5).put((String)localObject6, (String)localObject7);
                  localObject6 = "tracking_type";
                  localObject4 = e;
                  ((ContentValues)localObject5).put((String)localObject6, (String)localObject4);
                  localObject4 = "server_id";
                  localObject4 = ((ContentValues)localObject5).getAsString((String)localObject4);
                  bool5 = TextUtils.isEmpty((CharSequence)localObject4);
                  if (!bool5)
                  {
                    bool5 = true;
                  }
                  else
                  {
                    bool5 = false;
                    localObject4 = null;
                  }
                  localObject6 = new String[0];
                  AssertionUtil.isTrue(bool5, (String[])localObject6);
                  localObject4 = "value";
                  localObject4 = ((ContentValues)localObject5).getAsString((String)localObject4);
                  bool5 = TextUtils.isEmpty((CharSequence)localObject4);
                  if (!bool5)
                  {
                    bool5 = true;
                  }
                  else
                  {
                    bool5 = false;
                    localObject4 = null;
                  }
                  localObject6 = new String[0];
                  AssertionUtil.isTrue(bool5, (String[])localObject6);
                  localObject4 = "wildcard_type";
                  localObject4 = ((ContentValues)localObject5).getAsLong((String)localObject4);
                  long l1 = ((Long)localObject4).longValue();
                  long l2 = 0L;
                  bool5 = l1 < l2;
                  if (!bool5)
                  {
                    bool5 = true;
                  }
                  else
                  {
                    bool5 = false;
                    localObject4 = null;
                  }
                  localObject6 = new String[0];
                  AssertionUtil.isTrue(bool5, (String[])localObject6);
                  ((Collection)localObject3).add(localObject5);
                  continue;
                  label619:
                  localObject5 = new java/lang/IllegalArgumentException;
                  localObject6 = new java/lang/StringBuilder;
                  localObject7 = "Unknown backend filter rule ";
                  ((StringBuilder)localObject6).<init>((String)localObject7);
                  localObject4 = d;
                  ((StringBuilder)localObject6).append((String)localObject4);
                  localObject4 = ((StringBuilder)localObject6).toString();
                  ((IllegalArgumentException)localObject5).<init>((String)localObject4);
                  throw ((Throwable)localObject5);
                }
                else
                {
                  localObject4 = new java/lang/IllegalArgumentException;
                  localObject5 = "Filter value is empty";
                  ((IllegalArgumentException)localObject4).<init>((String)localObject5);
                  throw ((Throwable)localObject4);
                }
              }
              catch (IllegalArgumentException localIllegalArgumentException)
              {
                AssertionUtil.reportThrowableButNeverCrash(localIllegalArgumentException);
              }
            }
            a((Collection)localObject3, false);
          }
        }
      }
      localObject1 = (c.c)acb;
      if (localObject1 != null)
      {
        localObject2 = c;
        int n = a;
        if (n > 0)
        {
          n = 1;
        }
        else
        {
          n = 0;
          localObject3 = null;
        }
        ((p)localObject2).f(n);
        localObject2 = c;
        int i1 = b;
        if (i1 > 0)
        {
          i1 = 1;
        }
        else
        {
          i1 = 0;
          localObject3 = null;
        }
        ((p)localObject2).a(i1);
        localObject2 = c;
        localObject3 = g.S();
        boolean bool3 = ((com.truecaller.featuretoggles.b)localObject3).a();
        if (bool3)
        {
          localObject3 = k;
          bool3 = ((com.truecaller.common.f.c)localObject3).d();
          if (bool3)
          {
            m = g;
            if (m > 0) {
              bool4 = true;
            }
          }
        }
        ((p)localObject2).e(bool4);
      }
      localObject1 = c;
      long l3 = System.currentTimeMillis();
      ((p)localObject1).a(l3);
      return bool1;
    }
    AssertionUtil.reportWeirdnessButNeverCrash("Could not restore filters");
    return false;
  }
  
  public final void d()
  {
    ContentResolver localContentResolver = b.getContentResolver();
    Uri localUri = TruecallerContract.Filters.a();
    localContentResolver.delete(localUri, "rule=1", null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */