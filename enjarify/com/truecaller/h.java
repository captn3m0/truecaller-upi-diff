package com.truecaller;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  
  private h(c paramc, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static h a(c paramc, Provider paramProvider1, Provider paramProvider2)
  {
    h localh = new com/truecaller/h;
    localh.<init>(paramc, paramProvider1, paramProvider2);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */