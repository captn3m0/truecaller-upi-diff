package com.truecaller;

import com.truecaller.swish.SwishInputActivity;
import com.truecaller.swish.SwishResultActivity;
import com.truecaller.swish.f;
import com.truecaller.swish.g;
import com.truecaller.swish.h.a;
import com.truecaller.swish.j;
import com.truecaller.swish.k;
import com.truecaller.swish.m;
import com.truecaller.swish.o;
import com.truecaller.swish.q.a;
import com.truecaller.swish.s;
import dagger.a.c;
import javax.inject.Provider;

final class be$r
  implements g
{
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  
  private be$r(be parambe)
  {
    parambe = f.a(be.o(a));
    b = parambe;
    parambe = c.a(b);
    c = parambe;
    parambe = c.a(o.a());
    d = parambe;
    parambe = be.a(a);
    Provider localProvider1 = c;
    Provider localProvider2 = be.C(a);
    Provider localProvider3 = d;
    Provider localProvider4 = be.r(a);
    parambe = m.a(parambe, localProvider1, localProvider2, localProvider3, localProvider4);
    e = parambe;
    parambe = c.a(e);
    f = parambe;
    parambe = f;
    localProvider1 = c;
    localProvider2 = be.S(a);
    localProvider3 = be.W(a);
    parambe = j.a(parambe, localProvider1, localProvider2, localProvider3);
    g = parambe;
    parambe = c.a(g);
    h = parambe;
    localProvider1 = be.l(a);
    localProvider2 = f;
    localProvider3 = be.at(a);
    localProvider4 = be.ay(a);
    Provider localProvider5 = be.R(a);
    Provider localProvider6 = be.r(a);
    Provider localProvider7 = be.S(a);
    Provider localProvider8 = be.W(a);
    parambe = s.a(localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7, localProvider8);
    i = parambe;
    parambe = c.a(i);
    j = parambe;
  }
  
  public final k a()
  {
    return (k)f.get();
  }
  
  public final void a(SwishInputActivity paramSwishInputActivity)
  {
    h.a locala = (h.a)h.get();
    a = locala;
  }
  
  public final void a(SwishResultActivity paramSwishResultActivity)
  {
    q.a locala = (q.a)j.get();
    a = locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */