package com.truecaller;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private m(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static m a(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    m localm = new com/truecaller/m;
    localm.<init>(paramc, paramProvider1, paramProvider2, paramProvider3);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */