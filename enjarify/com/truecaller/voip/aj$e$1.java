package com.truecaller.voip;

import android.support.v4.app.f;
import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.c.c.a;
import com.truecaller.data.entity.Contact;
import java.util.List;
import kotlinx.coroutines.ag;

final class aj$e$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  aj$e$1(aj.e parame, List paramList, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/voip/aj$e$1;
    aj.e locale = b;
    List localList = c;
    local1.<init>(locale, localList, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = com.truecaller.calling.c.c.e;
        localObject = b.g;
        Contact localContact = b.e;
        List localList = c;
        String str = b.f;
        c.a.a((f)localObject, localContact, localList, false, false, false, true, str, 112);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.aj.e.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */