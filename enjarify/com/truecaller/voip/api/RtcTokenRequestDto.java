package com.truecaller.voip.api;

import c.g.b.k;

public final class RtcTokenRequestDto
{
  private final String channelId;
  
  public RtcTokenRequestDto(String paramString)
  {
    channelId = paramString;
  }
  
  public final String component1()
  {
    return channelId;
  }
  
  public final RtcTokenRequestDto copy(String paramString)
  {
    k.b(paramString, "channelId");
    RtcTokenRequestDto localRtcTokenRequestDto = new com/truecaller/voip/api/RtcTokenRequestDto;
    localRtcTokenRequestDto.<init>(paramString);
    return localRtcTokenRequestDto;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RtcTokenRequestDto;
      if (bool1)
      {
        paramObject = (RtcTokenRequestDto)paramObject;
        String str = channelId;
        paramObject = channelId;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getChannelId()
  {
    return channelId;
  }
  
  public final int hashCode()
  {
    String str = channelId;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RtcTokenRequestDto(channelId=");
    String str = channelId;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.api.RtcTokenRequestDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */