package com.truecaller.voip.api;

public final class VoipNumberDto
{
  private final long expiryEpochSeconds;
  private final long phone;
  
  public VoipNumberDto(long paramLong1, long paramLong2)
  {
    phone = paramLong1;
    expiryEpochSeconds = paramLong2;
  }
  
  public final long component1()
  {
    return phone;
  }
  
  public final long component2()
  {
    return expiryEpochSeconds;
  }
  
  public final VoipNumberDto copy(long paramLong1, long paramLong2)
  {
    VoipNumberDto localVoipNumberDto = new com/truecaller/voip/api/VoipNumberDto;
    localVoipNumberDto.<init>(paramLong1, paramLong2);
    return localVoipNumberDto;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof VoipNumberDto;
      if (bool2)
      {
        paramObject = (VoipNumberDto)paramObject;
        long l1 = phone;
        long l2 = phone;
        bool2 = l1 < l2;
        if (!bool2) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        if (bool2)
        {
          l1 = expiryEpochSeconds;
          l2 = expiryEpochSeconds;
          boolean bool3 = l1 < l2;
          if (!bool3)
          {
            bool3 = true;
          }
          else
          {
            bool3 = false;
            paramObject = null;
          }
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final long getExpiryEpochSeconds()
  {
    return expiryEpochSeconds;
  }
  
  public final long getPhone()
  {
    return phone;
  }
  
  public final int hashCode()
  {
    long l1 = phone;
    int i = 32;
    long l2 = l1 >>> i;
    int j = (int)(l1 ^ l2) * 31;
    l2 = expiryEpochSeconds;
    long l3 = l2 >>> i;
    int k = (int)(l2 ^ l3);
    return j + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipNumberDto(phone=");
    long l = phone;
    localStringBuilder.append(l);
    localStringBuilder.append(", expiryEpochSeconds=");
    l = expiryEpochSeconds;
    localStringBuilder.append(l);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.api.VoipNumberDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */