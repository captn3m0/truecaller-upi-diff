package com.truecaller.voip.api;

import c.g.b.k;

public final class VoipIdDto
{
  private final long expiryEpochSeconds;
  private final String id;
  
  public VoipIdDto(String paramString, long paramLong)
  {
    id = paramString;
    expiryEpochSeconds = paramLong;
  }
  
  public final String component1()
  {
    return id;
  }
  
  public final long component2()
  {
    return expiryEpochSeconds;
  }
  
  public final VoipIdDto copy(String paramString, long paramLong)
  {
    k.b(paramString, "id");
    VoipIdDto localVoipIdDto = new com/truecaller/voip/api/VoipIdDto;
    localVoipIdDto.<init>(paramString, paramLong);
    return localVoipIdDto;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof VoipIdDto;
      if (bool2)
      {
        paramObject = (VoipIdDto)paramObject;
        String str1 = id;
        String str2 = id;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          long l1 = expiryEpochSeconds;
          long l2 = expiryEpochSeconds;
          boolean bool3 = l1 < l2;
          if (!bool3)
          {
            bool3 = true;
          }
          else
          {
            bool3 = false;
            paramObject = null;
          }
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final long getExpiryEpochSeconds()
  {
    return expiryEpochSeconds;
  }
  
  public final String getId()
  {
    return id;
  }
  
  public final int hashCode()
  {
    String str = id;
    int i;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    i *= 31;
    long l1 = expiryEpochSeconds;
    long l2 = l1 >>> 32;
    int j = (int)(l1 ^ l2);
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipIdDto(id=");
    String str = id;
    localStringBuilder.append(str);
    localStringBuilder.append(", expiryEpochSeconds=");
    long l = expiryEpochSeconds;
    localStringBuilder.append(l);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.api.VoipIdDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */