package com.truecaller.voip.api;

import c.g.b.k;

public final class WakeUpDto
{
  private final String action;
  private final long callee;
  private final String channel;
  
  public WakeUpDto(long paramLong, String paramString1, String paramString2)
  {
    callee = paramLong;
    channel = paramString1;
    action = paramString2;
  }
  
  public final long component1()
  {
    return callee;
  }
  
  public final String component2()
  {
    return channel;
  }
  
  public final String component3()
  {
    return action;
  }
  
  public final WakeUpDto copy(long paramLong, String paramString1, String paramString2)
  {
    k.b(paramString1, "channel");
    k.b(paramString2, "action");
    WakeUpDto localWakeUpDto = new com/truecaller/voip/api/WakeUpDto;
    localWakeUpDto.<init>(paramLong, paramString1, paramString2);
    return localWakeUpDto;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof WakeUpDto;
      if (bool2)
      {
        paramObject = (WakeUpDto)paramObject;
        long l1 = callee;
        long l2 = callee;
        bool2 = l1 < l2;
        String str1;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          str1 = null;
        }
        if (bool2)
        {
          str1 = channel;
          String str2 = channel;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            str1 = action;
            paramObject = action;
            boolean bool3 = k.a(str1, paramObject);
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getAction()
  {
    return action;
  }
  
  public final long getCallee()
  {
    return callee;
  }
  
  public final String getChannel()
  {
    return channel;
  }
  
  public final int hashCode()
  {
    long l1 = callee;
    long l2 = l1 >>> 32;
    l1 ^= l2;
    int i = (int)l1 * 31;
    String str = channel;
    int j = 0;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = action;
    if (str != null) {
      j = str.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("WakeUpDto(callee=");
    long l = callee;
    localStringBuilder.append(l);
    localStringBuilder.append(", channel=");
    String str = channel;
    localStringBuilder.append(str);
    localStringBuilder.append(", action=");
    str = action;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.api.WakeUpDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */