package com.truecaller.voip.api;

import c.g.b.k;

public final class RtcTokenDto
{
  private final String token;
  private final int uid;
  
  public RtcTokenDto(String paramString, int paramInt)
  {
    token = paramString;
    uid = paramInt;
  }
  
  public final String component1()
  {
    return token;
  }
  
  public final int component2()
  {
    return uid;
  }
  
  public final RtcTokenDto copy(String paramString, int paramInt)
  {
    k.b(paramString, "token");
    RtcTokenDto localRtcTokenDto = new com/truecaller/voip/api/RtcTokenDto;
    localRtcTokenDto.<init>(paramString, paramInt);
    return localRtcTokenDto;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof RtcTokenDto;
      if (bool2)
      {
        paramObject = (RtcTokenDto)paramObject;
        String str1 = token;
        String str2 = token;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          int i = uid;
          int j = uid;
          if (i == j)
          {
            j = 1;
          }
          else
          {
            j = 0;
            paramObject = null;
          }
          if (j != 0) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getToken()
  {
    return token;
  }
  
  public final int getUid()
  {
    return uid;
  }
  
  public final int hashCode()
  {
    String str = token;
    int i;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    i *= 31;
    int j = uid;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RtcTokenDto(token=");
    String str = token;
    localStringBuilder.append(str);
    localStringBuilder.append(", uid=");
    int i = uid;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.api.RtcTokenDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */