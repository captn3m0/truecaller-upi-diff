package com.truecaller.voip.api;

import c.g.b.k;

public final class RtmTokenDto
{
  private final String token;
  
  public RtmTokenDto(String paramString)
  {
    token = paramString;
  }
  
  public final String component1()
  {
    return token;
  }
  
  public final RtmTokenDto copy(String paramString)
  {
    k.b(paramString, "token");
    RtmTokenDto localRtmTokenDto = new com/truecaller/voip/api/RtmTokenDto;
    localRtmTokenDto.<init>(paramString);
    return localRtmTokenDto;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RtmTokenDto;
      if (bool1)
      {
        paramObject = (RtmTokenDto)paramObject;
        String str = token;
        paramObject = token;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getToken()
  {
    return token;
  }
  
  public final int hashCode()
  {
    String str = token;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RtmTokenDto(token=");
    String str = token;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.api.RtmTokenDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */