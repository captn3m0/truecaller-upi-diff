package com.truecaller.voip;

import c.g.b.k;

public final class ag
{
  public final VoipState a;
  public final VoipStateReason b;
  public final ConnectionState c;
  public final int d;
  public final String e;
  private final int f;
  private final boolean g;
  private final boolean h;
  
  public ag()
  {
    this(null, null, 0, 0, false, null, false, 255);
  }
  
  private ag(VoipState paramVoipState, VoipStateReason paramVoipStateReason, ConnectionState paramConnectionState, int paramInt1, int paramInt2, boolean paramBoolean1, String paramString, boolean paramBoolean2)
  {
    a = paramVoipState;
    b = paramVoipStateReason;
    c = paramConnectionState;
    d = paramInt1;
    f = paramInt2;
    g = paramBoolean1;
    e = paramString;
    h = paramBoolean2;
  }
  
  public final int a()
  {
    Integer localInteger = c.getStatusId();
    if (localInteger != null) {
      return localInteger.intValue();
    }
    return d;
  }
  
  public final boolean b()
  {
    Boolean localBoolean = c.getStartTimer();
    if (localBoolean != null) {
      return localBoolean.booleanValue();
    }
    return h;
  }
  
  public final int c()
  {
    Integer localInteger = c.getCallStatusColor();
    if (localInteger != null) {
      return localInteger.intValue();
    }
    return f;
  }
  
  public final boolean d()
  {
    Boolean localBoolean = c.getShowAvatarRing();
    if (localBoolean != null) {
      return localBoolean.booleanValue();
    }
    return g;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ag;
      if (bool2)
      {
        paramObject = (ag)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              int i = d;
              int j = d;
              if (i == j)
              {
                i = 1;
              }
              else
              {
                i = 0;
                localObject1 = null;
              }
              if (i != 0)
              {
                i = f;
                j = f;
                if (i == j)
                {
                  i = 1;
                }
                else
                {
                  i = 0;
                  localObject1 = null;
                }
                if (i != 0)
                {
                  boolean bool3 = g;
                  boolean bool4 = g;
                  if (bool3 == bool4)
                  {
                    bool3 = true;
                  }
                  else
                  {
                    bool3 = false;
                    localObject1 = null;
                  }
                  if (bool3)
                  {
                    localObject1 = e;
                    localObject2 = e;
                    bool3 = k.a(localObject1, localObject2);
                    if (bool3)
                    {
                      bool3 = h;
                      boolean bool5 = h;
                      if (bool3 == bool5)
                      {
                        bool5 = true;
                      }
                      else
                      {
                        bool5 = false;
                        paramObject = null;
                      }
                      if (bool5) {
                        return bool1;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    VoipState localVoipState = a;
    int i = 0;
    if (localVoipState != null)
    {
      k = localVoipState.hashCode();
    }
    else
    {
      k = 0;
      localVoipState = null;
    }
    k *= 31;
    Object localObject = b;
    int m;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    int k = (k + m) * 31;
    localObject = c;
    int n;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    k = (k + n) * 31;
    int i1 = d;
    k = (k + i1) * 31;
    int i2 = f;
    k = (k + i2) * 31;
    int i3 = g;
    if (i3 != 0) {
      i3 = 1;
    }
    k = (k + i3) * 31;
    localObject = e;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    k = (k + i) * 31;
    int j = h;
    if (j != 0) {
      j = 1;
    }
    return k + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipServiceState(state=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", stateReason=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", connectionState=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", statusId=");
    int i = d;
    localStringBuilder.append(i);
    localStringBuilder.append(", callStatusColor=");
    i = f;
    localStringBuilder.append(i);
    localStringBuilder.append(", showAvatarRing=");
    boolean bool = g;
    localStringBuilder.append(bool);
    localStringBuilder.append(", logMessage=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", startTimer=");
    bool = h;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */