package com.truecaller.voip;

import c.o.b;
import c.x;
import com.truecaller.api.services.presence.v1.models.i;
import com.truecaller.voip.db.VoipAvailability;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class aj$d
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  aj$d(aj paramaj, ArrayList paramArrayList, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/voip/aj$d;
    aj localaj = b;
    ArrayList localArrayList = c;
    locald.<init>(localaj, localArrayList, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = b.b;
        localObject1 = (Iterable)c;
        Object localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        localObject2 = (Collection)localObject2;
        localObject1 = ((Iterable)localObject1).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          Object localObject3 = (com.truecaller.presence.a)((Iterator)localObject1).next();
          i locali = f;
          if (locali == null)
          {
            bool2 = false;
            localObject3 = null;
          }
          else
          {
            VoipAvailability localVoipAvailability = new com/truecaller/voip/db/VoipAvailability;
            localObject3 = a;
            String str1 = "+";
            String str2 = "";
            localObject3 = c.n.m.a((String)localObject3, str1, str2);
            int j = locali.a() ^ true;
            int k = locali.b();
            localVoipAvailability.<init>((String)localObject3, j, k);
            localObject3 = localVoipAvailability;
          }
          if (localObject3 != null) {
            ((Collection)localObject2).add(localObject3);
          }
        }
        localObject2 = (List)localObject2;
        ((com.truecaller.voip.db.c)paramObject).a((List)localObject2);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.aj.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */