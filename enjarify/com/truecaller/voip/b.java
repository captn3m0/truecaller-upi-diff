package com.truecaller.voip;

import android.content.Context;
import c.g.b.k;
import c.u;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.presence.c;
import com.truecaller.voip.util.l;

public final class b
  implements l
{
  private final Context a;
  
  public b(Context paramContext)
  {
    a = paramContext;
  }
  
  public final boolean a()
  {
    Object localObject = a.getApplicationContext();
    if (localObject != null)
    {
      localObject = ((bk)localObject).a();
      k.a(localObject, "(context.applicationCont…GraphHolder).objectsGraph");
      localObject = ((bp)localObject).ae();
      String str = "graph.presenceManager()";
      k.a(localObject, str);
      localObject = (Boolean)((c)((f)localObject).a()).a().d();
      if (localObject == null) {
        localObject = Boolean.FALSE;
      }
      k.a(localObject, "presenceManager.tell().r…Settings().get() ?: false");
      return ((Boolean)localObject).booleanValue();
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */