package com.truecaller.voip.a;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import c.g.b.k;
import c.u;

final class a$d
  implements ValueAnimator.AnimatorUpdateListener
{
  a$d(a parama) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    String str = "it";
    k.a(paramValueAnimator, str);
    paramValueAnimator = paramValueAnimator.getAnimatedValue();
    if (paramValueAnimator != null)
    {
      float f = ((Float)paramValueAnimator).floatValue();
      a.a(a, f);
      return;
    }
    paramValueAnimator = new c/u;
    paramValueAnimator.<init>("null cannot be cast to non-null type kotlin.Float");
    throw paramValueAnimator;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.a.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */