package com.truecaller.voip.a;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import c.g.b.k;
import c.u;

final class a$a
  implements ValueAnimator.AnimatorUpdateListener
{
  a$a(a parama) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    Object localObject = "it";
    k.a(paramValueAnimator, (String)localObject);
    paramValueAnimator = paramValueAnimator.getAnimatedValue();
    if (paramValueAnimator != null)
    {
      paramValueAnimator = (Float)paramValueAnimator;
      float f1 = paramValueAnimator.floatValue();
      localObject = a;
      a.a((a)localObject, f1);
      float f2 = 0.5F;
      boolean bool = f1 < f2;
      if (!bool)
      {
        a locala = a;
        f1 -= f2;
        a.b(locala, f1);
        paramValueAnimator = a;
        a.a(paramValueAnimator);
      }
      return;
    }
    paramValueAnimator = new c/u;
    paramValueAnimator.<init>("null cannot be cast to non-null type kotlin.Float");
    throw paramValueAnimator;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */