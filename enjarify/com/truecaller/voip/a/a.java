package com.truecaller.voip.a;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PathDashPathEffect;
import android.graphics.PathDashPathEffect.Style;
import android.graphics.PathEffect;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Xfermode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.support.v4.content.b;
import android.view.animation.OvershootInterpolator;
import c.g.b.k;
import c.k.i;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.dimen;

public final class a
  extends Drawable
{
  public final Path a;
  public boolean b;
  public final AnimatorSet c;
  public final AnimatorSet d;
  public final Context e;
  private final Resources f;
  private final float g;
  private final float h;
  private final Path i;
  private final PathMeasure j;
  private final Path k;
  private final Paint l;
  private final Paint m;
  private float n;
  private float o;
  
  public a(Context paramContext)
  {
    e = paramContext;
    paramContext = e.getResources();
    f = paramContext;
    paramContext = f;
    int i1 = R.dimen.voip_call_state_avatar_ring_width;
    float f1 = paramContext.getDimension(i1);
    g = f1;
    paramContext = f;
    i1 = R.dimen.voip_call_state_avatar_ring_dot_radius;
    f1 = paramContext.getDimension(i1);
    h = f1;
    paramContext = new android/graphics/Path;
    paramContext.<init>();
    i = paramContext;
    paramContext = new android/graphics/PathMeasure;
    paramContext.<init>();
    j = paramContext;
    paramContext = new android/graphics/Path;
    paramContext.<init>();
    float f2 = h;
    Object localObject1 = Path.Direction.CW;
    paramContext.addCircle(0.0F, 0.0F, f2, (Path.Direction)localObject1);
    k = paramContext;
    paramContext = new android/graphics/Path;
    paramContext.<init>();
    a = paramContext;
    paramContext = new android/graphics/Paint;
    i1 = 1;
    paramContext.<init>(i1);
    localObject1 = Paint.Style.STROKE;
    paramContext.setStyle((Paint.Style)localObject1);
    float f3 = g;
    paramContext.setStrokeWidth(f3);
    paramContext.setColor(0);
    Object localObject2 = new android/graphics/PorterDuffXfermode;
    Object localObject3 = PorterDuff.Mode.SRC_OUT;
    ((PorterDuffXfermode)localObject2).<init>((PorterDuff.Mode)localObject3);
    localObject2 = (Xfermode)localObject2;
    paramContext.setXfermode((Xfermode)localObject2);
    l = paramContext;
    paramContext = new android/graphics/Paint;
    paramContext.<init>(i1);
    paramContext.setColor(0);
    localObject2 = Paint.Style.FILL_AND_STROKE;
    paramContext.setStyle((Paint.Style)localObject2);
    m = paramContext;
    paramContext = new android/animation/AnimatorSet;
    paramContext.<init>();
    int i2 = 2;
    localObject3 = new float[i2];
    Object tmp265_263 = localObject3;
    tmp265_263[0] = 0.0F;
    tmp265_263[1] = 0.5F;
    localObject3 = ValueAnimator.ofFloat((float[])localObject3);
    long l1 = 800L;
    ((ValueAnimator)localObject3).setDuration(l1);
    Object localObject4 = new android/view/animation/OvershootInterpolator;
    ((OvershootInterpolator)localObject4).<init>();
    localObject4 = (TimeInterpolator)localObject4;
    ((ValueAnimator)localObject3).setInterpolator((TimeInterpolator)localObject4);
    localObject4 = new com/truecaller/voip/a/a$a;
    ((a.a)localObject4).<init>(this);
    localObject4 = (ValueAnimator.AnimatorUpdateListener)localObject4;
    ((ValueAnimator)localObject3).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject4);
    localObject4 = new float[i2];
    Object tmp350_348 = localObject4;
    tmp350_348[0] = 0.5F;
    tmp350_348[1] = 0.0F;
    localObject4 = ValueAnimator.ofFloat((float[])localObject4);
    ((ValueAnimator)localObject4).setDuration(l1);
    Object localObject5 = new android/view/animation/OvershootInterpolator;
    ((OvershootInterpolator)localObject5).<init>();
    localObject5 = (TimeInterpolator)localObject5;
    ((ValueAnimator)localObject4).setInterpolator((TimeInterpolator)localObject5);
    localObject5 = new com/truecaller/voip/a/a$b;
    ((a.b)localObject5).<init>(this);
    localObject5 = (ValueAnimator.AnimatorUpdateListener)localObject5;
    ((ValueAnimator)localObject4).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject5);
    localObject5 = new Animator[i2];
    localObject3 = (Animator)localObject3;
    localObject5[0] = localObject3;
    localObject4 = (Animator)localObject4;
    localObject5[i1] = localObject4;
    paramContext.playSequentially((Animator[])localObject5);
    c = paramContext;
    paramContext = new android/animation/AnimatorSet;
    paramContext.<init>();
    localObject3 = new float[i2];
    Object tmp482_480 = localObject3;
    tmp482_480[0] = 0.0F;
    tmp482_480[1] = 0.5F;
    localObject3 = ValueAnimator.ofFloat((float[])localObject3);
    ((ValueAnimator)localObject3).setDuration(l1);
    localObject4 = new android/view/animation/OvershootInterpolator;
    ((OvershootInterpolator)localObject4).<init>();
    localObject4 = (TimeInterpolator)localObject4;
    ((ValueAnimator)localObject3).setInterpolator((TimeInterpolator)localObject4);
    localObject4 = new com/truecaller/voip/a/a$c;
    ((a.c)localObject4).<init>(this);
    localObject4 = (ValueAnimator.AnimatorUpdateListener)localObject4;
    ((ValueAnimator)localObject3).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject4);
    localObject4 = new float[i2];
    Object tmp562_560 = localObject4;
    tmp562_560[0] = 0.5F;
    tmp562_560[1] = 0.0F;
    localObject4 = ValueAnimator.ofFloat((float[])localObject4);
    ((ValueAnimator)localObject4).setDuration(l1);
    Object localObject6 = new android/view/animation/OvershootInterpolator;
    ((OvershootInterpolator)localObject6).<init>();
    localObject6 = (TimeInterpolator)localObject6;
    ((ValueAnimator)localObject4).setInterpolator((TimeInterpolator)localObject6);
    localObject6 = new com/truecaller/voip/a/a$d;
    ((a.d)localObject6).<init>(this);
    localObject6 = (ValueAnimator.AnimatorUpdateListener)localObject6;
    ((ValueAnimator)localObject4).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject6);
    localObject2 = new Animator[i2];
    localObject3 = (Animator)localObject3;
    localObject2[0] = localObject3;
    localObject4 = (Animator)localObject4;
    localObject2[i1] = localObject4;
    paramContext.playSequentially((Animator[])localObject2);
    d = paramContext;
  }
  
  private final void a(float paramFloat)
  {
    paramFloat = i.a(paramFloat);
    n = paramFloat;
    Drawable.Callback localCallback = getCallback();
    if (localCallback != null)
    {
      Object localObject = this;
      localObject = (Drawable)this;
      localCallback.invalidateDrawable((Drawable)localObject);
      return;
    }
  }
  
  private final void b(float paramFloat)
  {
    paramFloat = i.a(paramFloat);
    o = paramFloat;
    Drawable.Callback localCallback = getCallback();
    if (localCallback != null)
    {
      Object localObject = this;
      localObject = (Drawable)this;
      localCallback.invalidateDrawable((Drawable)localObject);
      return;
    }
  }
  
  private final void d()
  {
    i.reset();
    Object localObject = i;
    int i1 = getBoundsbottom;
    int i2 = getBoundstop;
    float f1 = (i1 - i2) / 2.0F;
    float f2 = h;
    f2 = f1 - f2;
    Path.Direction localDirection = Path.Direction.CW;
    ((Path)localObject).addCircle(f1, f1, f2, localDirection);
    localObject = j;
    Path localPath = i;
    ((PathMeasure)localObject).setPath(localPath, false);
  }
  
  private final void e()
  {
    Object localObject1 = a;
    boolean bool = ((Path)localObject1).isEmpty();
    if (bool) {
      return;
    }
    a.reset();
    localObject1 = getCallback();
    if (localObject1 != null)
    {
      Object localObject2 = this;
      localObject2 = (Drawable)this;
      ((Drawable.Callback)localObject1).invalidateDrawable((Drawable)localObject2);
      return;
    }
  }
  
  public final void a()
  {
    Object localObject1 = a;
    boolean bool = ((Path)localObject1).isEmpty();
    if (!bool) {
      return;
    }
    localObject1 = a;
    float f1 = h;
    Path.Direction localDirection = Path.Direction.CW;
    ((Path)localObject1).addCircle(0.0F, 0.0F, f1, localDirection);
    localObject1 = getCallback();
    if (localObject1 != null)
    {
      Object localObject2 = this;
      localObject2 = (Drawable)this;
      ((Drawable.Callback)localObject1).invalidateDrawable((Drawable)localObject2);
      return;
    }
  }
  
  public final void a(int paramInt)
  {
    l.setColor(paramInt);
    Object localObject = m;
    ((Paint)localObject).setColor(paramInt);
    Drawable.Callback localCallback = getCallback();
    if (localCallback != null)
    {
      localObject = this;
      localObject = (Drawable)this;
      localCallback.invalidateDrawable((Drawable)localObject);
      return;
    }
  }
  
  public final void b()
  {
    c.cancel();
    d.cancel();
    e();
    a(0.0F);
    b(0.0F);
  }
  
  public final void c()
  {
    Context localContext = e;
    int i1 = R.color.voip_call_status_warning_color;
    int i2 = b.c(localContext, i1);
    a(i2);
    boolean bool = b;
    if (!bool)
    {
      bool = true;
      b = bool;
    }
  }
  
  public final void draw(Canvas paramCanvas)
  {
    k.b(paramCanvas, "canvas");
    Object localObject1 = j;
    float f1 = ((PathMeasure)localObject1).getLength();
    float f2 = n;
    float f3 = 1.0F;
    f2 = (f3 - f2 + 0.125F) % f3 * f1;
    float f4 = o;
    f4 = f3 - f4;
    float f5 = 0.625F;
    f4 = (f4 + f5) % f3 * f1;
    Object localObject2 = i;
    Object localObject3 = l;
    paramCanvas.drawPath((Path)localObject2, (Paint)localObject3);
    localObject2 = k;
    boolean bool1 = ((Path)localObject2).isEmpty();
    Object localObject4;
    if (!bool1)
    {
      localObject2 = m;
      localObject3 = new android/graphics/PathDashPathEffect;
      localObject4 = k;
      PathDashPathEffect.Style localStyle = PathDashPathEffect.Style.TRANSLATE;
      ((PathDashPathEffect)localObject3).<init>((Path)localObject4, f1, f2, localStyle);
      localObject3 = (PathEffect)localObject3;
      ((Paint)localObject2).setPathEffect((PathEffect)localObject3);
      localObject5 = i;
      localObject2 = m;
      paramCanvas.drawPath((Path)localObject5, (Paint)localObject2);
    }
    Object localObject5 = a;
    boolean bool2 = ((Path)localObject5).isEmpty();
    if (!bool2)
    {
      localObject5 = m;
      localObject2 = new android/graphics/PathDashPathEffect;
      localObject3 = a;
      localObject4 = PathDashPathEffect.Style.TRANSLATE;
      ((PathDashPathEffect)localObject2).<init>((Path)localObject3, f1, f4, (PathDashPathEffect.Style)localObject4);
      localObject2 = (PathEffect)localObject2;
      ((Paint)localObject5).setPathEffect((PathEffect)localObject2);
      localObject1 = i;
      localObject5 = m;
      paramCanvas.drawPath((Path)localObject1, (Paint)localObject5);
    }
  }
  
  public final int getOpacity()
  {
    return -3;
  }
  
  public final void setAlpha(int paramInt)
  {
    m.setAlpha(paramInt);
    l.setAlpha(paramInt);
  }
  
  public final void setBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
    d();
  }
  
  public final void setBounds(Rect paramRect)
  {
    k.b(paramRect, "bounds");
    super.setBounds(paramRect);
    d();
  }
  
  public final void setColorFilter(ColorFilter paramColorFilter)
  {
    m.setColorFilter(paramColorFilter);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */