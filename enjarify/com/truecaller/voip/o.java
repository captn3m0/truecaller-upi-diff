package com.truecaller.voip;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.d;
import com.truecaller.tcpermissions.l;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker.a;
import com.truecaller.voip.manager.h;
import com.truecaller.voip.util.VoipAnalyticsCallDirection;
import com.truecaller.voip.util.VoipAnalyticsState;
import com.truecaller.voip.util.ac;
import com.truecaller.voip.util.an;
import com.truecaller.voip.util.n;
import dagger.a;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

public final class o
  implements d, ag
{
  private final f b;
  private final f c;
  private final Context d;
  private final l e;
  private final com.truecaller.tcpermissions.o f;
  private final k g;
  private final h h;
  private final a i;
  private final a j;
  private final a k;
  private final a l;
  private final a m;
  private final com.truecaller.voip.util.o n;
  private final ac o;
  private final an p;
  
  public o(f paramf1, f paramf2, Context paramContext, l paraml, com.truecaller.tcpermissions.o paramo, k paramk, h paramh, a parama1, a parama2, a parama3, a parama4, a parama5, com.truecaller.voip.util.o paramo1, ac paramac, an paraman)
  {
    b = paramf1;
    c = paramf2;
    d = paramContext;
    e = paraml;
    f = paramo;
    g = paramk;
    h = paramh;
    i = parama1;
    j = parama2;
    k = parama3;
    l = parama4;
    m = parama5;
    n = paramo1;
    o = paramac;
    p = paraman;
  }
  
  public final f V_()
  {
    return b;
  }
  
  public final void a(af paramaf)
  {
    c.g.b.k.b(paramaf, "notification");
    Object localObject1 = new String[1];
    Object localObject2 = String.valueOf(paramaf);
    Object localObject3 = "New voip push notification is received. Sender id ".concat((String)localObject2);
    int i1 = 0;
    localObject2 = null;
    localObject1[0] = localObject3;
    boolean bool = a();
    if (!bool) {
      return;
    }
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/log/UnmutedException$d;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Invalid voip notification. Sender id is null. Action: ");
      paramaf = b;
      ((StringBuilder)localObject3).append(paramaf);
      ((StringBuilder)localObject3).append('.');
      paramaf = ((StringBuilder)localObject3).toString();
      ((UnmutedException.d)localObject1).<init>(paramaf);
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
      return;
    }
    localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    int i2 = ((String)localObject1).hashCode();
    i1 = 3625376;
    if (i2 == i1)
    {
      localObject3 = "voip";
      bool = ((String)localObject1).equals(localObject3);
      if (bool)
      {
        localObject1 = h;
        i2 = 0;
        localObject3 = null;
        if (localObject1 != null)
        {
          localObject2 = n;
          n localn = new com/truecaller/voip/util/n;
          VoipAnalyticsCallDirection localVoipAnalyticsCallDirection = VoipAnalyticsCallDirection.INCOMING;
          int i3 = 60;
          localn.<init>(localVoipAnalyticsCallDirection, (String)localObject1, null, i3);
          localObject1 = VoipAnalyticsState.WAKE_UP_RECEIVED;
          com.truecaller.voip.util.o.a.a((com.truecaller.voip.util.o)localObject2, localn, (VoipAnalyticsState)localObject1);
        }
        localObject1 = c;
        localObject2 = new com/truecaller/voip/o$a;
        ((o.a)localObject2).<init>(this, paramaf, null);
        localObject2 = (m)localObject2;
        int i4 = 2;
        e.b(this, (f)localObject1, (m)localObject2, i4);
      }
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "number");
    Object localObject = "analyticsContext";
    c.g.b.k.b(paramString2, (String)localObject);
    boolean bool = a();
    if (!bool) {
      return;
    }
    localObject = new com/truecaller/voip/o$c;
    ((o.c)localObject).<init>(this, paramString2, paramString1, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
  }
  
  public final boolean a()
  {
    return g.a();
  }
  
  public final boolean a(String paramString)
  {
    return p.c(paramString);
  }
  
  public final void b()
  {
    h.a();
  }
  
  public final void b(String paramString)
  {
    h.b();
    Object localObject = new com/truecaller/voip/o$b;
    ((o.b)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
  }
  
  public final void d()
  {
    boolean bool = a();
    if (!bool) {
      return;
    }
    MissedVoipCallsWorker.a.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */