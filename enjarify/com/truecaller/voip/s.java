package com.truecaller.voip;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.notifications.MissedCallsNotificationActionReceiver;
import com.truecaller.voip.util.af;

public final class s
  implements af
{
  private final Context a;
  
  public s(Context paramContext)
  {
    a = paramContext;
  }
  
  public final PendingIntent a()
  {
    Object localObject = new android/content/Intent;
    Context localContext = a;
    ((Intent)localObject).<init>(localContext, MissedCallsNotificationActionReceiver.class);
    localObject = ((Intent)localObject).setAction("com.truecaller.OPEN_APP");
    localObject = PendingIntent.getBroadcast(a, 2131364149, (Intent)localObject, 134217728);
    k.a(localObject, "PendingIntent.getBroadca…CURRENT\n                )");
    k.a(localObject, "Intent(context, MissedCa…          )\n            }");
    return (PendingIntent)localObject;
  }
  
  public final PendingIntent a(long paramLong)
  {
    Intent localIntent = new android/content/Intent;
    Object localObject = a;
    Class localClass = MissedCallsNotificationActionReceiver.class;
    localIntent.<init>((Context)localObject, localClass);
    localIntent = localIntent.setAction("com.truecaller.CLEAR_MISSED_CALLS");
    localObject = "lastTimestamp";
    localIntent = localIntent.putExtra((String)localObject, paramLong);
    long l = 0L;
    boolean bool = paramLong < l;
    if (bool)
    {
      localObject = "lastTimestamp";
      localIntent.putExtra((String)localObject, paramLong);
    }
    PendingIntent localPendingIntent = PendingIntent.getBroadcast(a, 2131364147, localIntent, 134217728);
    k.a(localPendingIntent, "PendingIntent.getBroadca…CURRENT\n                )");
    k.a(localPendingIntent, "Intent(context, MissedCa…          )\n            }");
    return localPendingIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */