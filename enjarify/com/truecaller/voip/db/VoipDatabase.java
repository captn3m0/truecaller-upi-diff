package com.truecaller.voip.db;

import android.arch.persistence.room.f;

public abstract class VoipDatabase
  extends f
{
  public static final VoipDatabase.a g;
  private static VoipDatabase h;
  private static final android.arch.persistence.room.a.a i;
  
  static
  {
    Object localObject = new com/truecaller/voip/db/VoipDatabase$a;
    ((VoipDatabase.a)localObject).<init>((byte)0);
    g = (VoipDatabase.a)localObject;
    localObject = new com/truecaller/voip/db/VoipDatabase$b;
    ((VoipDatabase.b)localObject).<init>();
    i = (android.arch.persistence.room.a.a)localObject;
  }
  
  public abstract a h();
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.VoipDatabase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */