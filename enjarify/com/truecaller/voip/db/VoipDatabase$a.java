package com.truecaller.voip.db;

import android.arch.persistence.room.a.a;
import android.arch.persistence.room.e;
import android.arch.persistence.room.f.a;
import android.content.Context;
import c.g.b.k;

public final class VoipDatabase$a
{
  public final VoipDatabase a(Context paramContext)
  {
    Object localObject = "context";
    try
    {
      k.b(paramContext, (String)localObject);
      localObject = VoipDatabase.j();
      if (localObject == null)
      {
        paramContext = paramContext.getApplicationContext();
        localObject = VoipDatabase.class;
        String str = "voipDb";
        paramContext = e.a(paramContext, (Class)localObject, str);
        int i = 1;
        localObject = new a[i];
        str = null;
        a locala = VoipDatabase.i();
        localObject[0] = locala;
        paramContext = paramContext.a((a[])localObject);
        paramContext = paramContext.a();
        paramContext = paramContext.b();
        paramContext = (VoipDatabase)paramContext;
        VoipDatabase.a(paramContext);
      }
      paramContext = VoipDatabase.j();
      return paramContext;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.VoipDatabase.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */