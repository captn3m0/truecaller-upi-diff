package com.truecaller.voip.db;

import android.arch.persistence.db.c;
import android.arch.persistence.db.c.a;
import android.arch.persistence.db.c.b;
import android.arch.persistence.db.c.b.a;
import android.arch.persistence.db.c.c;
import android.arch.persistence.room.d;
import android.arch.persistence.room.h;
import android.arch.persistence.room.h.a;

public class VoipDatabase_Impl
  extends VoipDatabase
{
  private volatile a h;
  
  public final d a()
  {
    d locald = new android/arch/persistence/room/d;
    String[] arrayOfString = { "voip_availability", "voip_id_cache" };
    locald.<init>(this, arrayOfString);
    return locald;
  }
  
  public final c b(android.arch.persistence.room.a parama)
  {
    Object localObject1 = new android/arch/persistence/room/h;
    Object localObject2 = new com/truecaller/voip/db/VoipDatabase_Impl$1;
    ((VoipDatabase_Impl.1)localObject2).<init>(this);
    ((h)localObject1).<init>(parama, (h.a)localObject2, "347a6a5a76bf916aae5582781ebc7fb7", "f6b354aba1750c9569d822d2120a818d");
    localObject2 = c.b.a(b);
    String str = c;
    b = str;
    c = ((c.a)localObject1);
    localObject1 = ((c.b.a)localObject2).a();
    return a.a((c.b)localObject1);
  }
  
  public final a h()
  {
    Object localObject1 = h;
    if (localObject1 != null) {
      return h;
    }
    try
    {
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/voip/db/b;
        ((b)localObject1).<init>(this);
        h = ((a)localObject1);
      }
      localObject1 = h;
      return (a)localObject1;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.VoipDatabase_Impl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */