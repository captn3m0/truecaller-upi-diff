package com.truecaller.voip.db;

public final class DBContract
{
  public static final DBContract INSTANCE;
  
  static
  {
    DBContract localDBContract = new com/truecaller/voip/db/DBContract;
    localDBContract.<init>();
    INSTANCE = localDBContract;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.DBContract
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */