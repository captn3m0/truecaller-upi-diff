package com.truecaller.voip.db;

import android.arch.persistence.db.b;
import android.arch.persistence.room.a.a;
import c.g.b.k;

public final class VoipDatabase$b
  extends a
{
  VoipDatabase$b()
  {
    super(1, 2);
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "database");
    paramb.c("CREATE TABLE IF NOT EXISTS `voip_id_cache` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `voip_id` TEXT NOT NULL, `number` TEXT NOT NULL, `expiry_epoch_seconds` INTEGER NOT NULL)");
    paramb.c("CREATE UNIQUE INDEX `index_voip_id_cache_voip_id` ON `voip_id_cache` (`voip_id`)");
    paramb.c("CREATE UNIQUE INDEX `index_voip_id_cache_number` ON `voip_id_cache` (`number`)");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.VoipDatabase.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */