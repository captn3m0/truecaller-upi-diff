package com.truecaller.voip.db;

import android.arch.persistence.room.b.b.a;
import android.arch.persistence.room.b.b.d;
import android.arch.persistence.room.f.b;
import android.arch.persistence.room.h.a;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class VoipDatabase_Impl$1
  extends h.a
{
  VoipDatabase_Impl$1(VoipDatabase_Impl paramVoipDatabase_Impl)
  {
    super(2);
  }
  
  public final void a(android.arch.persistence.db.b paramb)
  {
    paramb.c("DROP TABLE IF EXISTS `voip_availability`");
    paramb.c("DROP TABLE IF EXISTS `voip_id_cache`");
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    paramb.c("CREATE TABLE IF NOT EXISTS `voip_availability` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `phone` TEXT NOT NULL, `voip_enabled` INTEGER NOT NULL, `version` INTEGER NOT NULL)");
    paramb.c("CREATE UNIQUE INDEX `index_voip_availability_phone` ON `voip_availability` (`phone`)");
    paramb.c("CREATE TABLE IF NOT EXISTS `voip_id_cache` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, `voip_id` TEXT NOT NULL, `number` TEXT NOT NULL, `expiry_epoch_seconds` INTEGER NOT NULL)");
    paramb.c("CREATE UNIQUE INDEX `index_voip_id_cache_voip_id` ON `voip_id_cache` (`voip_id`)");
    paramb.c("CREATE UNIQUE INDEX `index_voip_id_cache_number` ON `voip_id_cache` (`number`)");
    paramb.c("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    paramb.c("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"347a6a5a76bf916aae5582781ebc7fb7\")");
  }
  
  public final void c(android.arch.persistence.db.b paramb)
  {
    VoipDatabase_Impl.a(b, paramb);
    VoipDatabase_Impl.b(b, paramb);
    List localList1 = VoipDatabase_Impl.d(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = VoipDatabase_Impl.e(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)VoipDatabase_Impl.f(b).get(i);
        localb.b(paramb);
        i += 1;
      }
    }
  }
  
  public final void d(android.arch.persistence.db.b paramb)
  {
    List localList1 = VoipDatabase_Impl.a(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = VoipDatabase_Impl.b(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)VoipDatabase_Impl.c(b).get(i);
        localb.a(paramb);
        i += 1;
      }
    }
  }
  
  public final void e(android.arch.persistence.db.b paramb)
  {
    Object localObject1 = new java/util/HashMap;
    int i = 4;
    ((HashMap)localObject1).<init>(i);
    Object localObject2 = new android/arch/persistence/room/b/b$a;
    int j = 1;
    ((b.a)localObject2).<init>("_id", "INTEGER", false, j);
    ((HashMap)localObject1).put("_id", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("phone", "TEXT", j, 0);
    ((HashMap)localObject1).put("phone", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("voip_enabled", "INTEGER", j, 0);
    ((HashMap)localObject1).put("voip_enabled", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("version", "INTEGER", j, 0);
    ((HashMap)localObject1).put("version", localObject2);
    Object localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>(0);
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>(j);
    Object localObject4 = new android/arch/persistence/room/b/b$d;
    List localList = Arrays.asList(new String[] { "phone" });
    ((b.d)localObject4).<init>("index_voip_availability_phone", j, localList);
    ((HashSet)localObject2).add(localObject4);
    localObject4 = new android/arch/persistence/room/b/b;
    Object localObject5 = "voip_availability";
    ((android.arch.persistence.room.b.b)localObject4).<init>((String)localObject5, (Map)localObject1, (Set)localObject3, (Set)localObject2);
    localObject1 = android.arch.persistence.room.b.b.a(paramb, "voip_availability");
    boolean bool1 = ((android.arch.persistence.room.b.b)localObject4).equals(localObject1);
    if (bool1)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>(i);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("_id", "INTEGER", false, j);
      ((HashMap)localObject1).put("_id", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("voip_id", "TEXT", j, 0);
      ((HashMap)localObject1).put("voip_id", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("number", "TEXT", j, 0);
      ((HashMap)localObject1).put("number", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("expiry_epoch_seconds", "INTEGER", j, 0);
      ((HashMap)localObject1).put("expiry_epoch_seconds", localObject3);
      localObject6 = new java/util/HashSet;
      ((HashSet)localObject6).<init>(0);
      localObject3 = new java/util/HashSet;
      int k = 2;
      ((HashSet)localObject3).<init>(k);
      localObject2 = new android/arch/persistence/room/b/b$d;
      localObject5 = Arrays.asList(new String[] { "voip_id" });
      ((b.d)localObject2).<init>("index_voip_id_cache_voip_id", j, (List)localObject5);
      ((HashSet)localObject3).add(localObject2);
      localObject2 = new android/arch/persistence/room/b/b$d;
      localObject5 = Arrays.asList(new String[] { "number" });
      ((b.d)localObject2).<init>("index_voip_id_cache_number", j, (List)localObject5);
      ((HashSet)localObject3).add(localObject2);
      localObject2 = new android/arch/persistence/room/b/b;
      localObject4 = "voip_id_cache";
      ((android.arch.persistence.room.b.b)localObject2).<init>((String)localObject4, (Map)localObject1, (Set)localObject6, (Set)localObject3);
      localObject1 = "voip_id_cache";
      paramb = android.arch.persistence.room.b.b.a(paramb, (String)localObject1);
      boolean bool2 = ((android.arch.persistence.room.b.b)localObject2).equals(paramb);
      if (bool2) {
        return;
      }
      localObject1 = new java/lang/IllegalStateException;
      localObject6 = new java/lang/StringBuilder;
      ((StringBuilder)localObject6).<init>("Migration didn't properly handle voip_id_cache(com.truecaller.voip.db.VoipIdCache).\n Expected:\n");
      ((StringBuilder)localObject6).append(localObject2);
      ((StringBuilder)localObject6).append("\n Found:\n");
      ((StringBuilder)localObject6).append(paramb);
      paramb = ((StringBuilder)localObject6).toString();
      ((IllegalStateException)localObject1).<init>(paramb);
      throw ((Throwable)localObject1);
    }
    paramb = new java/lang/IllegalStateException;
    Object localObject6 = new java/lang/StringBuilder;
    ((StringBuilder)localObject6).<init>("Migration didn't properly handle voip_availability(com.truecaller.voip.db.VoipAvailability).\n Expected:\n");
    ((StringBuilder)localObject6).append(localObject4);
    ((StringBuilder)localObject6).append("\n Found:\n");
    ((StringBuilder)localObject6).append(localObject1);
    localObject1 = ((StringBuilder)localObject6).toString();
    paramb.<init>((String)localObject1);
    throw paramb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.VoipDatabase_Impl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */