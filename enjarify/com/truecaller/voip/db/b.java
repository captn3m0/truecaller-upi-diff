package com.truecaller.voip.db;

import android.arch.persistence.db.e;
import android.arch.persistence.room.c;
import android.arch.persistence.room.f;
import android.arch.persistence.room.i;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

public final class b
  implements a
{
  private final f a;
  private final c b;
  private final c c;
  private final android.arch.persistence.room.b d;
  
  public b(f paramf)
  {
    a = paramf;
    Object localObject = new com/truecaller/voip/db/b$1;
    ((b.1)localObject).<init>(this, paramf);
    b = ((c)localObject);
    localObject = new com/truecaller/voip/db/b$2;
    ((b.2)localObject).<init>(this, paramf);
    c = ((c)localObject);
    localObject = new com/truecaller/voip/db/b$3;
    ((b.3)localObject).<init>(this, paramf);
    d = ((android.arch.persistence.room.b)localObject);
  }
  
  public final VoipIdCache a(String paramString)
  {
    int i = 1;
    i locali = i.a("SELECT * FROM voip_id_cache WHERE voip_id=?", i);
    if (paramString == null)
    {
      paramString = e;
      paramString[i] = i;
    }
    else
    {
      locali.a(i, paramString);
    }
    paramString = a.a(locali);
    String str1 = "_id";
    try
    {
      i = paramString.getColumnIndexOrThrow(str1);
      String str2 = "voip_id";
      int j = paramString.getColumnIndexOrThrow(str2);
      String str3 = "number";
      int k = paramString.getColumnIndexOrThrow(str3);
      String str4 = "expiry_epoch_seconds";
      int m = paramString.getColumnIndexOrThrow(str4);
      boolean bool2 = paramString.moveToFirst();
      Object localObject2 = null;
      if (bool2)
      {
        str2 = paramString.getString(j);
        str3 = paramString.getString(k);
        long l1 = paramString.getLong(m);
        VoipIdCache localVoipIdCache = new com/truecaller/voip/db/VoipIdCache;
        localVoipIdCache.<init>(str2, str3, l1);
        boolean bool1 = paramString.isNull(i);
        if (!bool1)
        {
          long l2 = paramString.getLong(i);
          localObject2 = Long.valueOf(l2);
        }
        localVoipIdCache.setId((Long)localObject2);
        localObject2 = localVoipIdCache;
      }
      return (VoipIdCache)localObject2;
    }
    finally
    {
      paramString.close();
      locali.b();
    }
  }
  
  public final List a(String[] paramArrayOfString)
  {
    Object localObject1 = android.arch.persistence.room.b.a.a();
    String str1 = "SELECT * FROM voip_availability WHERE phone IN (";
    ((StringBuilder)localObject1).append(str1);
    int i = paramArrayOfString.length;
    android.arch.persistence.room.b.a.a((StringBuilder)localObject1, i);
    ((StringBuilder)localObject1).append(") AND voip_enabled = 1");
    localObject1 = ((StringBuilder)localObject1).toString();
    int j = 0;
    String str2 = null;
    i += 0;
    localObject1 = i.a((String)localObject1, i);
    i = paramArrayOfString.length;
    int k = 1;
    int m = 1;
    Object localObject3;
    while (j < i)
    {
      localObject3 = paramArrayOfString[j];
      if (localObject3 == null)
      {
        localObject3 = e;
        localObject3[m] = k;
      }
      else
      {
        ((i)localObject1).a(m, (String)localObject3);
      }
      m += 1;
      j += 1;
    }
    paramArrayOfString = a.a((e)localObject1);
    str1 = "_id";
    try
    {
      i = paramArrayOfString.getColumnIndexOrThrow(str1);
      str2 = "phone";
      j = paramArrayOfString.getColumnIndexOrThrow(str2);
      String str3 = "voip_enabled";
      k = paramArrayOfString.getColumnIndexOrThrow(str3);
      String str4 = "version";
      m = paramArrayOfString.getColumnIndexOrThrow(str4);
      localObject3 = new java/util/ArrayList;
      int n = paramArrayOfString.getCount();
      ((ArrayList)localObject3).<init>(n);
      for (;;)
      {
        boolean bool = paramArrayOfString.moveToNext();
        if (!bool) {
          break;
        }
        Object localObject4 = paramArrayOfString.getString(j);
        int i1 = paramArrayOfString.getInt(k);
        int i2 = paramArrayOfString.getInt(m);
        VoipAvailability localVoipAvailability = new com/truecaller/voip/db/VoipAvailability;
        localVoipAvailability.<init>((String)localObject4, i1, i2);
        bool = paramArrayOfString.isNull(i);
        if (bool)
        {
          bool = false;
          localObject4 = null;
        }
        else
        {
          long l = paramArrayOfString.getLong(i);
          localObject4 = Long.valueOf(l);
        }
        localVoipAvailability.setId((Long)localObject4);
        ((List)localObject3).add(localVoipAvailability);
      }
      return (List)localObject3;
    }
    finally
    {
      paramArrayOfString.close();
      ((i)localObject1).b();
    }
  }
  
  public final void a(VoipIdCache paramVoipIdCache)
  {
    Object localObject = a;
    ((f)localObject).d();
    try
    {
      localObject = c;
      ((c)localObject).a(paramVoipIdCache);
      paramVoipIdCache = a;
      paramVoipIdCache.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
  
  public final void a(List paramList)
  {
    Object localObject = a;
    ((f)localObject).d();
    try
    {
      localObject = b;
      ((c)localObject).a(paramList);
      paramList = a;
      paramList.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
  
  public final VoipIdCache b(String paramString)
  {
    int i = 1;
    i locali = i.a("SELECT * FROM voip_id_cache WHERE number=?", i);
    if (paramString == null)
    {
      paramString = e;
      paramString[i] = i;
    }
    else
    {
      locali.a(i, paramString);
    }
    paramString = a.a(locali);
    String str1 = "_id";
    try
    {
      i = paramString.getColumnIndexOrThrow(str1);
      String str2 = "voip_id";
      int j = paramString.getColumnIndexOrThrow(str2);
      String str3 = "number";
      int k = paramString.getColumnIndexOrThrow(str3);
      String str4 = "expiry_epoch_seconds";
      int m = paramString.getColumnIndexOrThrow(str4);
      boolean bool2 = paramString.moveToFirst();
      Object localObject2 = null;
      if (bool2)
      {
        str2 = paramString.getString(j);
        str3 = paramString.getString(k);
        long l1 = paramString.getLong(m);
        VoipIdCache localVoipIdCache = new com/truecaller/voip/db/VoipIdCache;
        localVoipIdCache.<init>(str2, str3, l1);
        boolean bool1 = paramString.isNull(i);
        if (!bool1)
        {
          long l2 = paramString.getLong(i);
          localObject2 = Long.valueOf(l2);
        }
        localVoipIdCache.setId((Long)localObject2);
        localObject2 = localVoipIdCache;
      }
      return (VoipIdCache)localObject2;
    }
    finally
    {
      paramString.close();
      locali.b();
    }
  }
  
  public final void b(VoipIdCache paramVoipIdCache)
  {
    Object localObject = a;
    ((f)localObject).d();
    try
    {
      localObject = d;
      ((android.arch.persistence.room.b)localObject).a(paramVoipIdCache);
      paramVoipIdCache = a;
      paramVoipIdCache.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */