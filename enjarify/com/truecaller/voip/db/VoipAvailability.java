package com.truecaller.voip.db;

import c.g.b.k;

public final class VoipAvailability
{
  private final int enabled;
  private Long id;
  private final String phone;
  private final int version;
  
  public VoipAvailability(String paramString, int paramInt1, int paramInt2)
  {
    phone = paramString;
    enabled = paramInt1;
    version = paramInt2;
  }
  
  public final String component1()
  {
    return phone;
  }
  
  public final int component2()
  {
    return enabled;
  }
  
  public final int component3()
  {
    return version;
  }
  
  public final VoipAvailability copy(String paramString, int paramInt1, int paramInt2)
  {
    k.b(paramString, "phone");
    VoipAvailability localVoipAvailability = new com/truecaller/voip/db/VoipAvailability;
    localVoipAvailability.<init>(paramString, paramInt1, paramInt2);
    return localVoipAvailability;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof VoipAvailability;
      if (bool2)
      {
        paramObject = (VoipAvailability)paramObject;
        String str1 = phone;
        String str2 = phone;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          int i = enabled;
          int j = enabled;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            str1 = null;
          }
          if (i != 0)
          {
            i = version;
            int k = version;
            if (i == k)
            {
              k = 1;
            }
            else
            {
              k = 0;
              paramObject = null;
            }
            if (k != 0) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int getEnabled()
  {
    return enabled;
  }
  
  public final Long getId()
  {
    return id;
  }
  
  public final String getPhone()
  {
    return phone;
  }
  
  public final int getVersion()
  {
    return version;
  }
  
  public final int hashCode()
  {
    String str = phone;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    i *= 31;
    int j = enabled;
    int i = (i + j) * 31;
    j = version;
    return i + j;
  }
  
  public final void setId(Long paramLong)
  {
    id = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipAvailability(phone=");
    String str = phone;
    localStringBuilder.append(str);
    localStringBuilder.append(", enabled=");
    int i = enabled;
    localStringBuilder.append(i);
    localStringBuilder.append(", version=");
    i = version;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.VoipAvailability
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */