package com.truecaller.voip.db;

import c.g.b.k;

public final class VoipIdCache
{
  private final long expiryEpochSeconds;
  private Long id;
  private final String number;
  private final String voipId;
  
  public VoipIdCache(String paramString1, String paramString2, long paramLong)
  {
    voipId = paramString1;
    number = paramString2;
    expiryEpochSeconds = paramLong;
  }
  
  public final String component1()
  {
    return voipId;
  }
  
  public final String component2()
  {
    return number;
  }
  
  public final long component3()
  {
    return expiryEpochSeconds;
  }
  
  public final VoipIdCache copy(String paramString1, String paramString2, long paramLong)
  {
    k.b(paramString1, "voipId");
    k.b(paramString2, "number");
    VoipIdCache localVoipIdCache = new com/truecaller/voip/db/VoipIdCache;
    localVoipIdCache.<init>(paramString1, paramString2, paramLong);
    return localVoipIdCache;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof VoipIdCache;
      if (bool2)
      {
        paramObject = (VoipIdCache)paramObject;
        String str1 = voipId;
        String str2 = voipId;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          str1 = number;
          str2 = number;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            long l1 = expiryEpochSeconds;
            long l2 = expiryEpochSeconds;
            boolean bool3 = l1 < l2;
            if (!bool3)
            {
              bool3 = true;
            }
            else
            {
              bool3 = false;
              paramObject = null;
            }
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final long getExpiryEpochSeconds()
  {
    return expiryEpochSeconds;
  }
  
  public final Long getId()
  {
    return id;
  }
  
  public final String getNumber()
  {
    return number;
  }
  
  public final String getVoipId()
  {
    return voipId;
  }
  
  public final int hashCode()
  {
    String str1 = voipId;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = number;
    if (str2 != null) {
      i = str2.hashCode();
    }
    int j = (j + i) * 31;
    long l1 = expiryEpochSeconds;
    long l2 = l1 >>> 32;
    int k = (int)(l1 ^ l2);
    return j + k;
  }
  
  public final void setId(Long paramLong)
  {
    id = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipIdCache(voipId=");
    String str = voipId;
    localStringBuilder.append(str);
    localStringBuilder.append(", number=");
    str = number;
    localStringBuilder.append(str);
    localStringBuilder.append(", expiryEpochSeconds=");
    long l = expiryEpochSeconds;
    localStringBuilder.append(l);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.VoipIdCache
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */