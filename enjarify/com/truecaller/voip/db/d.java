package com.truecaller.voip.db;

import android.content.Context;
import c.g.b.k;
import java.util.List;

public final class d
  implements c
{
  private final Context a;
  
  public d(Context paramContext)
  {
    a = paramContext;
  }
  
  private final a a()
  {
    Object localObject = VoipDatabase.g;
    Context localContext = a;
    localObject = ((VoipDatabase.a)localObject).a(localContext);
    if (localObject != null) {
      return ((VoipDatabase)localObject).h();
    }
    return null;
  }
  
  public final List a(String[] paramArrayOfString)
  {
    k.b(paramArrayOfString, "phoneNumbers");
    a locala = a();
    if (locala != null) {
      return locala.a(paramArrayOfString);
    }
    return null;
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "availabilities");
    a locala = a();
    if (locala != null)
    {
      locala.a(paramList);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */