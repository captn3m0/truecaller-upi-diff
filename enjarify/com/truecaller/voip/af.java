package com.truecaller.voip;

import c.g.b.k;

public final class af
{
  final long a;
  final String b;
  final String c;
  final String d;
  final String e;
  final String f;
  final Integer g;
  final String h;
  final Long i;
  
  public af(long paramLong, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, Integer paramInteger, String paramString6, Long paramLong1)
  {
    a = paramLong;
    b = paramString1;
    c = paramString2;
    d = paramString3;
    e = paramString4;
    f = paramString5;
    g = paramInteger;
    h = paramString6;
    i = paramLong1;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof af;
      if (bool2)
      {
        paramObject = (af)paramObject;
        long l1 = a;
        long l2 = a;
        bool2 = l1 < l2;
        Object localObject1;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localObject1 = null;
        }
        if (bool2)
        {
          localObject1 = b;
          Object localObject2 = b;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              localObject1 = d;
              localObject2 = d;
              bool2 = k.a(localObject1, localObject2);
              if (bool2)
              {
                localObject1 = e;
                localObject2 = e;
                bool2 = k.a(localObject1, localObject2);
                if (bool2)
                {
                  localObject1 = f;
                  localObject2 = f;
                  bool2 = k.a(localObject1, localObject2);
                  if (bool2)
                  {
                    localObject1 = g;
                    localObject2 = g;
                    bool2 = k.a(localObject1, localObject2);
                    if (bool2)
                    {
                      localObject1 = h;
                      localObject2 = h;
                      bool2 = k.a(localObject1, localObject2);
                      if (bool2)
                      {
                        localObject1 = i;
                        paramObject = i;
                        boolean bool3 = k.a(localObject1, paramObject);
                        if (bool3) {
                          return bool1;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    long l2 = l1 >>> 32;
    l1 ^= l2;
    int j = (int)l1 * 31;
    Object localObject = b;
    int k = 0;
    int m;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = c;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = d;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = e;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = f;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = g;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = h;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = i;
    if (localObject != null) {
      k = localObject.hashCode();
    }
    return j + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipPushNotification(sentTime=");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(", action=");
    Object localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", senderId=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", senderNumber=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", rtmToken=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", rtcToken=");
    localObject = f;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", rtcTokenUid=");
    localObject = g;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", channelId=");
    localObject = h;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", idExpiryEpochSeconds=");
    localObject = i;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */