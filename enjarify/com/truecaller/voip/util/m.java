package com.truecaller.voip.util;

import c.g.b.k;

public final class m
{
  public final d a;
  
  public m(d paramd)
  {
    a = paramd;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof m;
      if (bool1)
      {
        paramObject = (m)paramObject;
        d locald = a;
        paramObject = a;
        boolean bool2 = k.a(locald, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    d locald = a;
    if (locald != null) {
      return locald.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RouteSupport(bluetooth=");
    d locald = a;
    localStringBuilder.append(locald);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */