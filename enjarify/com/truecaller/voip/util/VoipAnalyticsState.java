package com.truecaller.voip.util;

public enum VoipAnalyticsState
{
  private final String value;
  
  static
  {
    VoipAnalyticsState[] arrayOfVoipAnalyticsState = new VoipAnalyticsState[18];
    VoipAnalyticsState localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    localVoipAnalyticsState.<init>("INITIATED", 0, "Initiated");
    INITIATED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[0] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    int i = 1;
    localVoipAnalyticsState.<init>("WAKE_UP_SENT", i, "WakeUpSent");
    WAKE_UP_SENT = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 2;
    localVoipAnalyticsState.<init>("WAKE_UP_RECEIVED", i, "WakeUpReceived");
    WAKE_UP_RECEIVED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 3;
    localVoipAnalyticsState.<init>("INIT_FAILED", i, "InitFailed");
    INIT_FAILED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 4;
    localVoipAnalyticsState.<init>("INVITED", i, "Invited");
    INVITED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 5;
    localVoipAnalyticsState.<init>("BUSY", i, "Busy");
    BUSY = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 6;
    localVoipAnalyticsState.<init>("BLOCKED", i, "Blocked");
    BLOCKED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 7;
    localVoipAnalyticsState.<init>("RINGING", i, "Ringing");
    RINGING = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 8;
    localVoipAnalyticsState.<init>("FAILED", i, "Failed");
    FAILED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 9;
    localVoipAnalyticsState.<init>("ACCEPTED", i, "Accepted");
    ACCEPTED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 10;
    localVoipAnalyticsState.<init>("REJECTED", i, "Rejected");
    REJECTED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 11;
    localVoipAnalyticsState.<init>("ON_HOLD", i, "OnHold");
    ON_HOLD = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 12;
    localVoipAnalyticsState.<init>("RESUMED", i, "Resumed");
    RESUMED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 13;
    localVoipAnalyticsState.<init>("CANCELLED", i, "Cancelled");
    CANCELLED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    i = 14;
    localVoipAnalyticsState.<init>("END", i, "End");
    END = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[i] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    localVoipAnalyticsState.<init>("RECONNECTING", 15, "Reconnecting");
    RECONNECTING = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[15] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    localVoipAnalyticsState.<init>("RECONNECTED", 16, "Reconnected");
    RECONNECTED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[16] = localVoipAnalyticsState;
    localVoipAnalyticsState = new com/truecaller/voip/util/VoipAnalyticsState;
    localVoipAnalyticsState.<init>("DISCONNECTED", 17, "Disconnected");
    DISCONNECTED = localVoipAnalyticsState;
    arrayOfVoipAnalyticsState[17] = localVoipAnalyticsState;
    $VALUES = arrayOfVoipAnalyticsState;
  }
  
  private VoipAnalyticsState(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipAnalyticsState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */