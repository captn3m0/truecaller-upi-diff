package com.truecaller.voip.util;

import c.d.c;
import c.g.a.b;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.db.VoipIdCache;
import kotlinx.coroutines.ag;

final class ad$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  ad$c(ad paramad, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/voip/util/ad$c;
    ad localad = b;
    String str = c;
    localc.<init>(localad, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.c;
        localObject = new com/truecaller/voip/util/ad$c$1;
        ((ad.c.1)localObject).<init>(this);
        localObject = (b)localObject;
        paramObject = (VoipIdCache)ad.a((com.truecaller.voip.db.a)paramObject, (b)localObject);
        if (paramObject != null)
        {
          localObject = b.c;
          ((com.truecaller.voip.db.a)localObject).b((VoipIdCache)paramObject);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ad.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */