package com.truecaller.voip.util;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.log.AssertionUtil;
import com.truecaller.voip.api.RtcTokenDto;
import com.truecaller.voip.api.a;
import e.b;
import e.r;
import kotlinx.coroutines.g;

public final class ar
  implements aq
{
  String a;
  n b;
  final a c;
  private final f d;
  
  public ar(f paramf, a parama)
  {
    d = paramf;
    c = parama;
  }
  
  static Object a(b paramb)
  {
    b localb = null;
    try
    {
      paramb = paramb.c();
      if (paramb != null)
      {
        paramb = paramb.e();
        localb = paramb;
      }
    }
    catch (Exception localException)
    {
      paramb = (Throwable)localException;
      AssertionUtil.reportThrowableButNeverCrash(paramb);
    }
    return localb;
  }
  
  public final Object a(c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/voip/util/ar$b;
    ((ar.b)localObject).<init>(this, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object a(String paramString, c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/voip/util/ar$a;
    ((ar.a)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final void a()
  {
    a = null;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "token");
    a = paramString;
  }
  
  public final void a(String paramString, RtcTokenDto paramRtcTokenDto)
  {
    k.b(paramString, "channelId");
    k.b(paramRtcTokenDto, "token");
    paramString = t.a(paramString, paramRtcTokenDto);
    b = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */