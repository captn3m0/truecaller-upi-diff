package com.truecaller.voip.util;

import android.content.Context;
import c.g.b.k;
import com.truecaller.utils.a.a;

public final class ai
  extends a
  implements ah
{
  private final int b = 1;
  private final String c = "voip_settings";
  
  public ai(Context paramContext)
  {
    super(paramContext);
  }
  
  public final int a()
  {
    return b;
  }
  
  public final void a(int paramInt, Context paramContext)
  {
    String str1 = "context";
    k.b(paramContext, str1);
    if (paramInt <= 0)
    {
      String str2 = "ownVoipId";
      d(str2);
    }
  }
  
  public final String b()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */