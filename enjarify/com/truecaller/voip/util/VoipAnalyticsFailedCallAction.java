package com.truecaller.voip.util;

public enum VoipAnalyticsFailedCallAction
{
  private final String value;
  
  static
  {
    VoipAnalyticsFailedCallAction[] arrayOfVoipAnalyticsFailedCallAction = new VoipAnalyticsFailedCallAction[3];
    VoipAnalyticsFailedCallAction localVoipAnalyticsFailedCallAction = new com/truecaller/voip/util/VoipAnalyticsFailedCallAction;
    localVoipAnalyticsFailedCallAction.<init>("OFFLINE", 0, "VoipCallOffline");
    OFFLINE = localVoipAnalyticsFailedCallAction;
    arrayOfVoipAnalyticsFailedCallAction[0] = localVoipAnalyticsFailedCallAction;
    localVoipAnalyticsFailedCallAction = new com/truecaller/voip/util/VoipAnalyticsFailedCallAction;
    int i = 1;
    localVoipAnalyticsFailedCallAction.<init>("NO_MIC_PERMISSION", i, "VoipCallNoMicPermission");
    NO_MIC_PERMISSION = localVoipAnalyticsFailedCallAction;
    arrayOfVoipAnalyticsFailedCallAction[i] = localVoipAnalyticsFailedCallAction;
    localVoipAnalyticsFailedCallAction = new com/truecaller/voip/util/VoipAnalyticsFailedCallAction;
    i = 2;
    localVoipAnalyticsFailedCallAction.<init>("CALLEE_NOT_CAPABLE", i, "VoipCallCalleeNotCapable");
    CALLEE_NOT_CAPABLE = localVoipAnalyticsFailedCallAction;
    arrayOfVoipAnalyticsFailedCallAction[i] = localVoipAnalyticsFailedCallAction;
    $VALUES = arrayOfVoipAnalyticsFailedCallAction;
  }
  
  private VoipAnalyticsFailedCallAction(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipAnalyticsFailedCallAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */