package com.truecaller.voip.util;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import c.g.a.b;
import kotlinx.coroutines.a.y;

public final class k
  implements j
{
  final Context a;
  final an b;
  
  public k(Context paramContext, an paraman)
  {
    a = paramContext;
    b = paraman;
  }
  
  static i a(int paramInt, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 2: 
      localObject = new com/truecaller/voip/util/i$b;
      ((i.b)localObject).<init>(paramBoolean);
      return (i)localObject;
    case 1: 
      localObject = new com/truecaller/voip/util/i$c;
      ((i.c)localObject).<init>(paramBoolean);
      return (i)localObject;
    }
    Object localObject = new com/truecaller/voip/util/i$a;
    ((i.a)localObject).<init>(paramBoolean);
    return (i)localObject;
  }
  
  public final i a()
  {
    an localan = b;
    boolean bool = localan.c(null);
    int i = com.truecaller.utils.extensions.i.b(a).getCallState();
    Object localObject = a(i, bool);
    if (localObject == null)
    {
      localObject = new com/truecaller/voip/util/i$a;
      ((i.a)localObject).<init>(bool);
      localObject = (i)localObject;
    }
    return (i)localObject;
  }
  
  public final void a(y paramy)
  {
    c.g.b.k.b(paramy, "channel");
    Object localObject1 = new com/truecaller/voip/util/k$b;
    ((k.b)localObject1).<init>(this, paramy);
    Object localObject2 = new com/truecaller/voip/util/k$a;
    ((k.a)localObject2).<init>(this, (k.b)localObject1);
    localObject2 = (b)localObject2;
    paramy.a((b)localObject2);
    new String[1][0] = "Listening call states";
    paramy = com.truecaller.utils.extensions.i.b(a);
    localObject1 = (PhoneStateListener)localObject1;
    paramy.listen((PhoneStateListener)localObject1, 32);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */