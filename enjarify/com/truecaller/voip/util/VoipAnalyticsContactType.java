package com.truecaller.voip.util;

public enum VoipAnalyticsContactType
{
  private final String value;
  
  static
  {
    VoipAnalyticsContactType[] arrayOfVoipAnalyticsContactType = new VoipAnalyticsContactType[3];
    VoipAnalyticsContactType localVoipAnalyticsContactType = new com/truecaller/voip/util/VoipAnalyticsContactType;
    localVoipAnalyticsContactType.<init>("PHONEBOOK", 0, "Phonebook");
    PHONEBOOK = localVoipAnalyticsContactType;
    arrayOfVoipAnalyticsContactType[0] = localVoipAnalyticsContactType;
    localVoipAnalyticsContactType = new com/truecaller/voip/util/VoipAnalyticsContactType;
    int i = 1;
    localVoipAnalyticsContactType.<init>("SPAM", i, "Spam");
    SPAM = localVoipAnalyticsContactType;
    arrayOfVoipAnalyticsContactType[i] = localVoipAnalyticsContactType;
    localVoipAnalyticsContactType = new com/truecaller/voip/util/VoipAnalyticsContactType;
    i = 2;
    localVoipAnalyticsContactType.<init>("OTHER", i, "Other");
    OTHER = localVoipAnalyticsContactType;
    arrayOfVoipAnalyticsContactType[i] = localVoipAnalyticsContactType;
    $VALUES = arrayOfVoipAnalyticsContactType;
  }
  
  private VoipAnalyticsContactType(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipAnalyticsContactType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */