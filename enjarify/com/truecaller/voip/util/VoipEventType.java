package com.truecaller.voip.util;

public enum VoipEventType
{
  static
  {
    VoipEventType[] arrayOfVoipEventType = new VoipEventType[4];
    VoipEventType localVoipEventType = new com/truecaller/voip/util/VoipEventType;
    localVoipEventType.<init>("INCOMING", 0);
    INCOMING = localVoipEventType;
    arrayOfVoipEventType[0] = localVoipEventType;
    localVoipEventType = new com/truecaller/voip/util/VoipEventType;
    int i = 1;
    localVoipEventType.<init>("OUTGOING", i);
    OUTGOING = localVoipEventType;
    arrayOfVoipEventType[i] = localVoipEventType;
    localVoipEventType = new com/truecaller/voip/util/VoipEventType;
    i = 2;
    localVoipEventType.<init>("MISSED", i);
    MISSED = localVoipEventType;
    arrayOfVoipEventType[i] = localVoipEventType;
    localVoipEventType = new com/truecaller/voip/util/VoipEventType;
    i = 3;
    localVoipEventType.<init>("BLOCKED", i);
    BLOCKED = localVoipEventType;
    arrayOfVoipEventType[i] = localVoipEventType;
    $VALUES = arrayOfVoipEventType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipEventType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */