package com.truecaller.voip.util;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.tracking.events.af;
import com.truecaller.tracking.events.af.a;
import org.apache.a.d.d;

final class p$e
  extends c.d.b.a.k
  implements m
{
  int a;
  private kotlinx.coroutines.ag e;
  
  p$e(p paramp, String paramString1, String paramString2, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/voip/util/p$e;
    p localp = b;
    String str1 = c;
    String str2 = d;
    locale.<init>(localp, str1, str2, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    e = ((kotlinx.coroutines.ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label331;
      }
      paramObject = b.c;
      localObject2 = c;
      int j = 1;
      a = j;
      paramObject = ((aa)paramObject).a((String)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (ag)paramObject;
    if (paramObject == null)
    {
      new String[1][0] = "Search is failed. Cannot log voip call attempt.";
      return x.a;
    }
    boolean bool2 = f;
    if (bool2)
    {
      paramObject = VoipAnalyticsContactType.PHONEBOOK;
    }
    else
    {
      paramObject = e;
      if (paramObject != null) {
        paramObject = VoipAnalyticsContactType.SPAM;
      } else {
        paramObject = VoipAnalyticsContactType.OTHER;
      }
    }
    localObject1 = b.a;
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("ViewAction");
    String str = d;
    localObject2 = ((e.a)localObject2).a("Context", str);
    str = VoipAnalyticsViewAction.VOIP_CALL.getValue();
    localObject2 = ((e.a)localObject2).a("Action", str);
    paramObject = ((VoipAnalyticsContactType)paramObject).getValue();
    paramObject = ((e.a)localObject2).a("SubAction", (String)paramObject).a();
    c.g.b.k.a(paramObject, "AnalyticsEvent.Builder(E…                 .build()");
    ((b)localObject1).b((e)paramObject);
    paramObject = (ae)b.b.a();
    localObject1 = af.b();
    localObject2 = (CharSequence)d;
    localObject1 = ((af.a)localObject1).a((CharSequence)localObject2);
    localObject2 = (CharSequence)VoipAnalyticsViewAction.VOIP_CALL.getValue();
    localObject1 = (d)((af.a)localObject1).b((CharSequence)localObject2).a();
    ((ae)paramObject).a((d)localObject1);
    return x.a;
    label331:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.p.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */