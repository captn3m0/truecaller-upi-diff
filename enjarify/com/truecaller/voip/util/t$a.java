package com.truecaller.voip.util;

import android.media.AudioFocusRequest;
import android.media.AudioManager;
import com.truecaller.utils.extensions.i;

final class t$a
  implements e
{
  private final AudioFocusRequest b;
  
  public t$a(t paramt, AudioFocusRequest paramAudioFocusRequest)
  {
    b = paramAudioFocusRequest;
  }
  
  public final void a()
  {
    AudioManager localAudioManager = i.i(a.b);
    AudioFocusRequest localAudioFocusRequest = b;
    localAudioManager.abandonAudioFocusRequest(localAudioFocusRequest);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.t.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */