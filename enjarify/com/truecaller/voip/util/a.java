package com.truecaller.voip.util;

import c.g.b.k;

public final class a
{
  public final AudioRoute a;
  public final m b;
  
  public a(AudioRoute paramAudioRoute, m paramm)
  {
    a = paramAudioRoute;
    b = paramm;
  }
  
  private static a a(AudioRoute paramAudioRoute, m paramm)
  {
    k.b(paramAudioRoute, "audioRoute");
    k.b(paramm, "routeSupport");
    a locala = new com/truecaller/voip/util/a;
    locala.<init>(paramAudioRoute, paramm);
    return locala;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        Object localObject = a;
        AudioRoute localAudioRoute = a;
        bool1 = k.a(localObject, localAudioRoute);
        if (bool1)
        {
          localObject = b;
          paramObject = b;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    AudioRoute localAudioRoute = a;
    int i = 0;
    int j;
    if (localAudioRoute != null)
    {
      j = localAudioRoute.hashCode();
    }
    else
    {
      j = 0;
      localAudioRoute = null;
    }
    j *= 31;
    m localm = b;
    if (localm != null) {
      i = localm.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AudioRouteSetting(audioRoute=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", routeSupport=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */