package com.truecaller.voip.util;

public enum VoipAnalyticsInCallUiAction
{
  private final String value;
  
  static
  {
    VoipAnalyticsInCallUiAction[] arrayOfVoipAnalyticsInCallUiAction = new VoipAnalyticsInCallUiAction[9];
    VoipAnalyticsInCallUiAction localVoipAnalyticsInCallUiAction = new com/truecaller/voip/util/VoipAnalyticsInCallUiAction;
    localVoipAnalyticsInCallUiAction.<init>("BACK", 0, "Back");
    BACK = localVoipAnalyticsInCallUiAction;
    arrayOfVoipAnalyticsInCallUiAction[0] = localVoipAnalyticsInCallUiAction;
    localVoipAnalyticsInCallUiAction = new com/truecaller/voip/util/VoipAnalyticsInCallUiAction;
    int i = 1;
    localVoipAnalyticsInCallUiAction.<init>("MIC_ON", i, "MicOn");
    MIC_ON = localVoipAnalyticsInCallUiAction;
    arrayOfVoipAnalyticsInCallUiAction[i] = localVoipAnalyticsInCallUiAction;
    localVoipAnalyticsInCallUiAction = new com/truecaller/voip/util/VoipAnalyticsInCallUiAction;
    i = 2;
    localVoipAnalyticsInCallUiAction.<init>("MIC_OFF", i, "MicOff");
    MIC_OFF = localVoipAnalyticsInCallUiAction;
    arrayOfVoipAnalyticsInCallUiAction[i] = localVoipAnalyticsInCallUiAction;
    localVoipAnalyticsInCallUiAction = new com/truecaller/voip/util/VoipAnalyticsInCallUiAction;
    i = 3;
    localVoipAnalyticsInCallUiAction.<init>("SPEAKER_ON", i, "SpeakerOn");
    SPEAKER_ON = localVoipAnalyticsInCallUiAction;
    arrayOfVoipAnalyticsInCallUiAction[i] = localVoipAnalyticsInCallUiAction;
    localVoipAnalyticsInCallUiAction = new com/truecaller/voip/util/VoipAnalyticsInCallUiAction;
    i = 4;
    localVoipAnalyticsInCallUiAction.<init>("SPEAKER_OFF", i, "SpeakerOff");
    SPEAKER_OFF = localVoipAnalyticsInCallUiAction;
    arrayOfVoipAnalyticsInCallUiAction[i] = localVoipAnalyticsInCallUiAction;
    localVoipAnalyticsInCallUiAction = new com/truecaller/voip/util/VoipAnalyticsInCallUiAction;
    i = 5;
    localVoipAnalyticsInCallUiAction.<init>("ACCEPT", i, "Accept");
    ACCEPT = localVoipAnalyticsInCallUiAction;
    arrayOfVoipAnalyticsInCallUiAction[i] = localVoipAnalyticsInCallUiAction;
    localVoipAnalyticsInCallUiAction = new com/truecaller/voip/util/VoipAnalyticsInCallUiAction;
    i = 6;
    localVoipAnalyticsInCallUiAction.<init>("REJECT", i, "Reject");
    REJECT = localVoipAnalyticsInCallUiAction;
    arrayOfVoipAnalyticsInCallUiAction[i] = localVoipAnalyticsInCallUiAction;
    localVoipAnalyticsInCallUiAction = new com/truecaller/voip/util/VoipAnalyticsInCallUiAction;
    i = 7;
    localVoipAnalyticsInCallUiAction.<init>("REJECT_WITH_MESSAGE", i, "RejectWithMessage");
    REJECT_WITH_MESSAGE = localVoipAnalyticsInCallUiAction;
    arrayOfVoipAnalyticsInCallUiAction[i] = localVoipAnalyticsInCallUiAction;
    localVoipAnalyticsInCallUiAction = new com/truecaller/voip/util/VoipAnalyticsInCallUiAction;
    i = 8;
    localVoipAnalyticsInCallUiAction.<init>("DISMISS", i, "Dismiss");
    DISMISS = localVoipAnalyticsInCallUiAction;
    arrayOfVoipAnalyticsInCallUiAction[i] = localVoipAnalyticsInCallUiAction;
    $VALUES = arrayOfVoipAnalyticsInCallUiAction;
  }
  
  private VoipAnalyticsInCallUiAction(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipAnalyticsInCallUiAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */