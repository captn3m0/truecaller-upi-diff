package com.truecaller.voip.util;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.t;
import c.x;
import com.truecaller.voip.ConnectionState;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import kotlinx.coroutines.a.u;

final class p$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private kotlinx.coroutines.ag f;
  
  p$c(p paramp, u paramu, c.g.a.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/voip/util/p$c;
    p localp = c;
    u localu = d;
    c.g.a.a locala = e;
    localc.<init>(localp, localu, locala, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    f = ((kotlinx.coroutines.ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = b;
    Object localObject2;
    int j;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      localObject2 = (kotlinx.coroutines.a.l)a;
      j = paramObject instanceof o.b;
      if (j == 0)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 1: 
      localObject2 = (kotlinx.coroutines.a.l)a;
      j = paramObject instanceof o.b;
      if (j == 0)
      {
        localObject3 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label1149;
      }
      paramObject = d.f();
      localObject2 = localObject1;
      localObject1 = this;
    }
    for (;;)
    {
      a = paramObject;
      j = 1;
      b = j;
      localObject3 = ((kotlinx.coroutines.a.l)paramObject).a((c)localObject1);
      if (localObject3 == localObject2) {
        return localObject2;
      }
      Object localObject4 = localObject2;
      localObject2 = paramObject;
      paramObject = localObject3;
      localObject3 = localObject4;
      paramObject = (Boolean)paramObject;
      boolean bool2 = ((Boolean)paramObject).booleanValue();
      if (!bool2) {
        break;
      }
      a = localObject2;
      int k = 2;
      b = k;
      paramObject = ((kotlinx.coroutines.a.l)localObject2).b((c)localObject1);
      if (paramObject == localObject3) {
        return localObject3;
      }
      localObject4 = localObject3;
      localObject3 = localObject2;
      localObject2 = localObject4;
      paramObject = (com.truecaller.voip.ag)paramObject;
      n localn = (n)e.invoke();
      Object localObject5 = b;
      Object localObject6 = VoipStateReason.CONNECTION_STATE_CHANGED;
      if (localObject5 == localObject6)
      {
        localObject5 = c;
        paramObject = c;
        localObject6 = q.b;
        k = ((ConnectionState)paramObject).ordinal();
        k = localObject6[k];
        switch (k)
        {
        default: 
          paramObject = new c/l;
          ((c.l)paramObject).<init>();
          throw ((Throwable)paramObject);
        case 3: 
          paramObject = VoipAnalyticsState.DISCONNECTED;
          break;
        case 2: 
          paramObject = VoipAnalyticsState.RECONNECTING;
          break;
        case 1: 
          paramObject = VoipAnalyticsState.RECONNECTED;
        }
        o.a.a((o)localObject5, localn, (VoipAnalyticsState)paramObject);
      }
      else
      {
        localObject5 = c;
        localObject6 = a;
        Object localObject7 = q.a;
        int m = ((VoipState)localObject6).ordinal();
        m = localObject7[m];
        localObject7 = null;
        switch (m)
        {
        default: 
          paramObject = new c/l;
          ((c.l)paramObject).<init>();
          throw ((Throwable)paramObject);
        case 10: 
        case 11: 
          paramObject = b;
          if (paramObject != null)
          {
            localObject6 = a;
            int[] arrayOfInt = q.c;
            k = ((VoipStateReason)paramObject).ordinal();
            k = arrayOfInt[k];
            switch (k)
            {
            default: 
              paramObject = new c/l;
              ((c.l)paramObject).<init>();
              throw ((Throwable)paramObject);
            case 15: 
              paramObject = VoipAnalyticsState.CANCELLED;
              localObject7 = t.a(paramObject, null);
              break;
            case 14: 
              paramObject = VoipAnalyticsState.END;
              localObject6 = VoipAnalyticsStateReason.POLL_FAILED;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 13: 
              paramObject = VoipAnalyticsState.END;
              localObject6 = VoipAnalyticsStateReason.REMOTE_LEFT_CHANNEL;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 12: 
              paramObject = VoipAnalyticsState.END;
              localObject6 = VoipAnalyticsStateReason.RECEIVED_END;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 11: 
              paramObject = VoipAnalyticsState.END;
              localObject6 = VoipAnalyticsStateReason.PRESSED_END;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 10: 
              paramObject = VoipAnalyticsState.END;
              localObject6 = VoipAnalyticsStateReason.INVITE_TIMEOUT;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 9: 
              paramObject = p.a((VoipAnalyticsCallDirection)localObject6);
              localObject6 = VoipAnalyticsStateReason.WAKE_UP_TIMEOUT;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 8: 
              paramObject = p.a((VoipAnalyticsCallDirection)localObject6);
              localObject6 = VoipAnalyticsStateReason.GET_PHONE_FAILED;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 7: 
              paramObject = p.a((VoipAnalyticsCallDirection)localObject6);
              localObject6 = VoipAnalyticsStateReason.WAKE_UP_SENT_FAILED;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 6: 
              paramObject = p.a((VoipAnalyticsCallDirection)localObject6);
              localObject6 = VoipAnalyticsStateReason.SEARCH_FAILED;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 5: 
              paramObject = p.a((VoipAnalyticsCallDirection)localObject6);
              localObject6 = VoipAnalyticsStateReason.JOIN_RTC_FAILED;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 4: 
              paramObject = p.a((VoipAnalyticsCallDirection)localObject6);
              localObject6 = VoipAnalyticsStateReason.JOIN_RTM_FAILED;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 3: 
              paramObject = p.a((VoipAnalyticsCallDirection)localObject6);
              localObject6 = VoipAnalyticsStateReason.GET_RTC_FAILED;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 2: 
              paramObject = p.a((VoipAnalyticsCallDirection)localObject6);
              localObject6 = VoipAnalyticsStateReason.GET_RTM_FAILED;
              localObject7 = t.a(paramObject, localObject6);
              break;
            case 1: 
              paramObject = p.a((VoipAnalyticsCallDirection)localObject6);
              localObject6 = VoipAnalyticsStateReason.GET_VOIP_ID_FAILED;
              localObject7 = t.a(paramObject, localObject6);
            }
          }
          break;
        case 9: 
          paramObject = VoipAnalyticsState.BLOCKED;
          localObject7 = t.a(paramObject, null);
          break;
        case 8: 
          paramObject = a;
          localObject6 = VoipAnalyticsCallDirection.OUTGOING;
          if (paramObject == localObject6)
          {
            paramObject = VoipAnalyticsState.ACCEPTED;
            localObject7 = t.a(paramObject, null);
          }
          break;
        case 7: 
          paramObject = VoipAnalyticsState.END;
          localObject6 = VoipAnalyticsStateReason.RING_TIMEOUT;
          localObject7 = t.a(paramObject, localObject6);
          break;
        case 6: 
          paramObject = VoipAnalyticsState.REJECTED;
          localObject7 = t.a(paramObject, null);
          break;
        case 5: 
          paramObject = VoipAnalyticsState.ACCEPTED;
          localObject7 = t.a(paramObject, null);
          break;
        case 4: 
          paramObject = VoipAnalyticsState.RINGING;
          localObject7 = t.a(paramObject, null);
          break;
        case 3: 
          paramObject = VoipAnalyticsState.BUSY;
          localObject7 = t.a(paramObject, null);
          break;
        case 2: 
          paramObject = VoipAnalyticsState.INVITED;
          localObject7 = t.a(paramObject, null);
          break;
        case 1: 
          paramObject = a;
          localObject6 = VoipAnalyticsCallDirection.OUTGOING;
          if (paramObject == localObject6)
          {
            paramObject = VoipAnalyticsState.INITIATED;
            localObject7 = t.a(paramObject, null);
          }
          break;
        }
        if (localObject7 != null)
        {
          paramObject = (VoipAnalyticsState)a;
          localObject6 = (VoipAnalyticsStateReason)b;
          ((p)localObject5).a(localn, (VoipAnalyticsState)paramObject, (VoipAnalyticsStateReason)localObject6);
        }
      }
      paramObject = localObject3;
    }
    return x.a;
    label1149:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.p.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */