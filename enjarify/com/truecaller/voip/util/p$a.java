package com.truecaller.voip.util;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.ag;

final class p$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag f;
  
  p$a(p paramp, u paramu, c.g.a.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/voip/util/p$a;
    p localp = c;
    u localu = d;
    c.g.a.a locala1 = e;
    locala.<init>(localp, localu, locala1, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = b;
    int j = 1;
    Object localObject2;
    boolean bool2;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      localObject2 = (kotlinx.coroutines.a.l)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 1: 
      localObject2 = (kotlinx.coroutines.a.l)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject3 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label347;
      }
      paramObject = d.f();
      localObject2 = localObject1;
      localObject1 = this;
    }
    for (;;)
    {
      a = paramObject;
      b = j;
      localObject3 = ((kotlinx.coroutines.a.l)paramObject).a((c)localObject1);
      if (localObject3 == localObject2) {
        return localObject2;
      }
      Object localObject4 = localObject2;
      localObject2 = paramObject;
      paramObject = localObject3;
      localObject3 = localObject4;
      paramObject = (Boolean)paramObject;
      boolean bool3 = ((Boolean)paramObject).booleanValue();
      if (!bool3) {
        break label343;
      }
      a = localObject2;
      int k = 2;
      b = k;
      paramObject = ((kotlinx.coroutines.a.l)localObject2).b((c)localObject1);
      if (paramObject == localObject3) {
        return localObject3;
      }
      localObject4 = localObject3;
      localObject3 = localObject2;
      localObject2 = localObject4;
      paramObject = (Boolean)paramObject;
      boolean bool4 = ((Boolean)paramObject).booleanValue();
      p localp = c;
      n localn = (n)e.invoke();
      if (bool4 == j)
      {
        paramObject = VoipAnalyticsState.ON_HOLD;
        o.a.a(localp, localn, (VoipAnalyticsState)paramObject);
      }
      else
      {
        if (bool4) {
          break;
        }
        paramObject = VoipAnalyticsState.RESUMED;
        o.a.a(localp, localn, (VoipAnalyticsState)paramObject);
      }
      paramObject = localObject3;
    }
    paramObject = new c/l;
    ((c.l)paramObject).<init>();
    throw ((Throwable)paramObject);
    label343:
    return x.a;
    label347:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.p.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */