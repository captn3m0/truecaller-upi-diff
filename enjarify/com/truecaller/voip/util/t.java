package com.truecaller.voip.util;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioAttributes.Builder;
import android.media.AudioFocusRequest.Builder;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build.VERSION;
import c.g.a.m;
import c.g.b.k;
import c.l;
import c.x;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.d;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.callconnection.f;
import kotlinx.coroutines.a.y;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;

public final class t
  implements s
{
  MediaPlayer a;
  final Context b;
  final f c;
  private e d;
  private c e;
  private final d f;
  
  public t(Context paramContext, d paramd, f paramf)
  {
    b = paramContext;
    f = paramd;
    c = paramf;
    paramContext = e;
    if (paramContext == null)
    {
      paramContext = new com/truecaller/voip/util/c;
      paramContext.<init>();
      paramd = BluetoothAdapter.getDefaultAdapter();
      if (paramd != null)
      {
        boolean bool = paramd.isEnabled();
        if (!bool) {
          paramd = null;
        }
        if (paramd != null)
        {
          paramf = b;
          localObject = paramContext;
          localObject = (BluetoothProfile.ServiceListener)paramContext;
          int i = 1;
          paramd.getProfileProxy(paramf, (BluetoothProfile.ServiceListener)localObject, i);
        }
      }
      e = paramContext;
    }
  }
  
  static x a(MediaPlayer paramMediaPlayer)
  {
    x localx = null;
    if (paramMediaPlayer != null) {
      try
      {
        boolean bool = paramMediaPlayer.isPlaying() ^ true;
        if (!bool) {
          paramMediaPlayer = null;
        }
        if (paramMediaPlayer != null)
        {
          paramMediaPlayer.start();
          localx = x.a;
        }
      }
      catch (IllegalStateException localIllegalStateException)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalStateException);
        return x.a;
      }
    }
    paramMediaPlayer = localx;
    return paramMediaPlayer;
  }
  
  private final e a(b paramb, AudioManager.OnAudioFocusChangeListener paramOnAudioFocusChangeListener)
  {
    Object localObject = "Requesting audio focus...";
    new String[1][0] = localObject;
    int i = Build.VERSION.SDK_INT;
    int j = 4;
    int k = 26;
    if (i >= k)
    {
      localObject = u.a;
      m = paramb.ordinal();
      m = localObject[m];
      switch (m)
      {
      default: 
        paramb = new c/l;
        paramb.<init>();
        throw paramb;
      case 2: 
        paramb = i();
        break;
      case 1: 
        paramb = h();
      }
      localObject = new android/media/AudioFocusRequest$Builder;
      ((AudioFocusRequest.Builder)localObject).<init>(j);
      paramb = ((AudioFocusRequest.Builder)localObject).setOnAudioFocusChangeListener(paramOnAudioFocusChangeListener).setAudioAttributes(paramb).build();
      i.i(b).requestAudioFocus(paramb);
      paramOnAudioFocusChangeListener = new com/truecaller/voip/util/t$a;
      k.a(paramb, "focusRequest");
      paramOnAudioFocusChangeListener.<init>(this, paramb);
      return (e)paramOnAudioFocusChangeListener;
    }
    localObject = u.b;
    int m = paramb.ordinal();
    m = localObject[m];
    switch (m)
    {
    default: 
      paramb = new c/l;
      paramb.<init>();
      throw paramb;
    case 2: 
      m = 0;
      paramb = null;
      break;
    case 1: 
      m = 2;
    }
    i.i(b).requestAudioFocus(paramOnAudioFocusChangeListener, m, j);
    paramb = new com/truecaller/voip/util/t$b;
    paramb.<init>(this, paramOnAudioFocusChangeListener);
    return (e)paramb;
  }
  
  static x b(MediaPlayer paramMediaPlayer)
  {
    x localx = null;
    if (paramMediaPlayer != null) {
      try
      {
        boolean bool = paramMediaPlayer.isPlaying();
        if (!bool) {
          paramMediaPlayer = null;
        }
        if (paramMediaPlayer != null)
        {
          paramMediaPlayer.pause();
          localx = x.a;
        }
      }
      catch (IllegalStateException localIllegalStateException)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalStateException);
        return x.a;
      }
    }
    paramMediaPlayer = localx;
    return paramMediaPlayer;
  }
  
  private static x c(MediaPlayer paramMediaPlayer)
  {
    if (paramMediaPlayer != null) {
      try
      {
        paramMediaPlayer.stop();
        paramMediaPlayer = x.a;
      }
      catch (IllegalStateException localIllegalStateException)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalStateException);
        paramMediaPlayer = x.a;
      }
    } else {
      paramMediaPlayer = null;
    }
    return paramMediaPlayer;
  }
  
  private final MediaPlayer g()
  {
    MediaPlayer localMediaPlayer;
    try
    {
      localMediaPlayer = new android/media/MediaPlayer;
      localMediaPlayer.<init>();
      int i = Build.VERSION.SDK_INT;
      int j = 21;
      if (i >= j)
      {
        localObject1 = h();
        localMediaPlayer.setAudioAttributes((AudioAttributes)localObject1);
      }
      else
      {
        i = 2;
        localMediaPlayer.setAudioStreamType(i);
      }
      i = 1;
      localMediaPlayer.setLooping(i);
      Object localObject1 = b;
      Object localObject2 = new java/lang/StringBuilder;
      Object localObject3 = "android.resource://";
      ((StringBuilder)localObject2).<init>((String)localObject3);
      localObject3 = f;
      localObject3 = ((d)localObject3).n();
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject3 = v.a();
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject2 = Uri.parse((String)localObject2);
      localObject3 = "Uri.parse(\"android.resou…geName()}$VOIP_TONE_URI\")";
      k.a(localObject2, (String)localObject3);
      localMediaPlayer.setDataSource((Context)localObject1, (Uri)localObject2);
      localMediaPlayer.prepare();
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localException);
      localMediaPlayer = null;
    }
    return localMediaPlayer;
  }
  
  private static AudioAttributes h()
  {
    AudioAttributes.Builder localBuilder = new android/media/AudioAttributes$Builder;
    localBuilder.<init>();
    return localBuilder.setContentType(2).setUsage(6).build();
  }
  
  private static AudioAttributes i()
  {
    AudioAttributes.Builder localBuilder = new android/media/AudioAttributes$Builder;
    localBuilder.<init>();
    return localBuilder.setContentType(1).setUsage(2).build();
  }
  
  public final void a()
  {
    MediaPlayer localMediaPlayer = a;
    if (localMediaPlayer == null)
    {
      localMediaPlayer = g();
      a = localMediaPlayer;
    }
    if (localMediaPlayer == null) {
      return;
    }
    Object localObject = d;
    if (localObject == null)
    {
      localObject = new com/truecaller/voip/util/t$e;
      ((t.e)localObject).<init>(this);
      localObject = (AudioManager.OnAudioFocusChangeListener)localObject;
      b localb = b.a;
      localObject = a(localb, (AudioManager.OnAudioFocusChangeListener)localObject);
      d = ((e)localObject);
    }
    a(localMediaPlayer);
  }
  
  public final void a(String paramString, y paramy)
  {
    k.b(paramString, "number");
    k.b(paramy, "channel");
    Object localObject1 = c;
    Object localObject2 = new com/truecaller/voip/util/t$c;
    ((t.c)localObject2).<init>(this, paramy);
    localObject2 = (c.g.a.b)localObject2;
    ((f)localObject1).a(paramString, (c.g.a.b)localObject2);
    localObject1 = new com/truecaller/voip/util/t$d;
    ((t.d)localObject1).<init>(this, paramString);
    localObject1 = (c.g.a.b)localObject1;
    paramy.a((c.g.a.b)localObject1);
  }
  
  public final void a(ag paramag)
  {
    k.b(paramag, "scope");
    Object localObject1 = (AudioManager.OnAudioFocusChangeListener)t.h.a;
    Object localObject2 = b.b;
    localObject1 = a((b)localObject2, (AudioManager.OnAudioFocusChangeListener)localObject1);
    localObject2 = new com/truecaller/voip/util/t$f;
    ((t.f)localObject2).<init>(null);
    localObject2 = (m)localObject2;
    paramag = kotlinx.coroutines.e.b(paramag, null, (m)localObject2, 3);
    localObject2 = new com/truecaller/voip/util/t$g;
    ((t.g)localObject2).<init>((e)localObject1);
    localObject2 = (c.g.a.b)localObject2;
    paramag.a_((c.g.a.b)localObject2);
  }
  
  public final void b()
  {
    Object localObject = a;
    if (localObject == null) {
      return;
    }
    c((MediaPlayer)localObject);
    localObject = d;
    if (localObject != null) {
      ((e)localObject).a();
    }
    d = null;
  }
  
  public final void c()
  {
    i.i(b).setMode(3);
  }
  
  public final void d()
  {
    i.i(b).setMode(0);
  }
  
  public final void e()
  {
    i.i(b).setSpeakerphoneOn(false);
  }
  
  public final void f()
  {
    b();
    Object localObject = a;
    if (localObject != null)
    {
      ((MediaPlayer)localObject).release();
      localObject = x.a;
    }
    localObject = null;
    a = null;
    c localc = e;
    if (localc != null) {
      localc.a();
    }
    e = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */