package com.truecaller.voip.util;

import dagger.a.d;
import javax.inject.Provider;

public final class ae
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private ae(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
  }
  
  public static ae a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    ae localae = new com/truecaller/voip/util/ae;
    localae.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localae;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */