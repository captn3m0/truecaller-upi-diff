package com.truecaller.voip.util;

import c.g.b.k;

public final class ab
{
  public final String a;
  public final VoipEventType b;
  public final long c;
  public final Long d;
  
  private ab(String paramString, VoipEventType paramVoipEventType, long paramLong, Long paramLong1)
  {
    a = paramString;
    b = paramVoipEventType;
    c = paramLong;
    d = paramLong1;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ab;
      if (bool2)
      {
        paramObject = (ab)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            long l1 = c;
            long l2 = c;
            bool2 = l1 < l2;
            if (!bool2)
            {
              bool2 = true;
            }
            else
            {
              bool2 = false;
              localObject1 = null;
            }
            if (bool2)
            {
              localObject1 = d;
              paramObject = d;
              boolean bool3 = k.a(localObject1, paramObject);
              if (bool3) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = b;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    long l1 = c;
    int m = 32;
    long l2 = l1 >>> m;
    l1 ^= l2;
    int n = (int)l1;
    j = (j + n) * 31;
    localObject = d;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipHistoryEvent(number=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", type=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", duration=");
    long l = c;
    localStringBuilder.append(l);
    localStringBuilder.append(", timestamp=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */