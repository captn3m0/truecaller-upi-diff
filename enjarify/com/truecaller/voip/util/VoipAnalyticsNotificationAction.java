package com.truecaller.voip.util;

public enum VoipAnalyticsNotificationAction
{
  private final String value;
  
  static
  {
    VoipAnalyticsNotificationAction[] arrayOfVoipAnalyticsNotificationAction = new VoipAnalyticsNotificationAction[4];
    VoipAnalyticsNotificationAction localVoipAnalyticsNotificationAction = new com/truecaller/voip/util/VoipAnalyticsNotificationAction;
    localVoipAnalyticsNotificationAction.<init>("ANSWERED", 0, "VoipAnswered");
    ANSWERED = localVoipAnalyticsNotificationAction;
    arrayOfVoipAnalyticsNotificationAction[0] = localVoipAnalyticsNotificationAction;
    localVoipAnalyticsNotificationAction = new com/truecaller/voip/util/VoipAnalyticsNotificationAction;
    int i = 1;
    localVoipAnalyticsNotificationAction.<init>("REJECTED", i, "VoipRejected");
    REJECTED = localVoipAnalyticsNotificationAction;
    arrayOfVoipAnalyticsNotificationAction[i] = localVoipAnalyticsNotificationAction;
    localVoipAnalyticsNotificationAction = new com/truecaller/voip/util/VoipAnalyticsNotificationAction;
    i = 2;
    localVoipAnalyticsNotificationAction.<init>("CALL_BACK", i, "VoipCallBack");
    CALL_BACK = localVoipAnalyticsNotificationAction;
    arrayOfVoipAnalyticsNotificationAction[i] = localVoipAnalyticsNotificationAction;
    localVoipAnalyticsNotificationAction = new com/truecaller/voip/util/VoipAnalyticsNotificationAction;
    i = 3;
    localVoipAnalyticsNotificationAction.<init>("HANG_UP", i, "VoipHangUp");
    HANG_UP = localVoipAnalyticsNotificationAction;
    arrayOfVoipAnalyticsNotificationAction[i] = localVoipAnalyticsNotificationAction;
    $VALUES = arrayOfVoipAnalyticsNotificationAction;
  }
  
  private VoipAnalyticsNotificationAction(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipAnalyticsNotificationAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */