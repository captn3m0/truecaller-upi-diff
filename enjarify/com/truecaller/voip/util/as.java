package com.truecaller.voip.util;

import dagger.a.d;
import javax.inject.Provider;

public final class as
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private as(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static as a(Provider paramProvider1, Provider paramProvider2)
  {
    as localas = new com/truecaller/voip/util/as;
    localas.<init>(paramProvider1, paramProvider2);
    return localas;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.as
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */