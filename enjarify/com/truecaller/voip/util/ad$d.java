package com.truecaller.voip.util;

import c.d.c;
import c.g.a.b;
import c.o.b;
import c.x;
import com.truecaller.voip.api.VoipIdDto;
import com.truecaller.voip.db.VoipIdCache;
import kotlinx.coroutines.ag;

final class ad$d
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  ad$d(ad paramad, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/voip/util/ad$d;
    ad localad = b;
    String str = c;
    locald.<init>(localad, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        int j = 1;
        localObject1 = new String[j];
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("Fetching voip id for number:");
        String str1 = c;
        ((StringBuilder)localObject2).append(str1);
        localObject2 = ((StringBuilder)localObject2).toString();
        str1 = null;
        localObject1[0] = localObject2;
        localObject1 = b.c;
        localObject2 = new com/truecaller/voip/util/ad$d$a;
        ((ad.d.a)localObject2).<init>(this);
        localObject2 = (b)localObject2;
        localObject1 = (VoipIdCache)ad.a((com.truecaller.voip.db.a)localObject1, (b)localObject2);
        localObject2 = null;
        if (localObject1 != null)
        {
          localObject3 = b;
          localObject1 = ad.a((ad)localObject3, (VoipIdCache)localObject1);
        }
        else
        {
          bool = false;
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          paramObject = new String[j];
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("Returning cached voip id:");
          localObject3 = ((VoipIdCache)localObject1).getVoipId();
          ((StringBuilder)localObject2).append((String)localObject3);
          localObject2 = ((StringBuilder)localObject2).toString();
          paramObject[0] = localObject2;
          return ((VoipIdCache)localObject1).getVoipId();
        }
        localObject1 = b.b;
        Object localObject3 = c;
        String str2 = "+";
        String str3 = "";
        localObject3 = c.n.m.a((String)localObject3, str2, str3);
        localObject1 = (VoipIdDto)ad.a(((com.truecaller.voip.api.a)localObject1).a((String)localObject3));
        if (localObject1 != null)
        {
          localObject3 = b;
          str2 = c;
          localObject1 = ad.a((ad)localObject3, (VoipIdDto)localObject1, str2);
        }
        else
        {
          bool = false;
          localObject1 = null;
        }
        paramObject = new String[j];
        str2 = String.valueOf(localObject1);
        localObject3 = "Fetched voip id is ".concat(str2);
        paramObject[0] = localObject3;
        if (localObject1 != null) {
          return ((VoipIdCache)localObject1).getVoipId();
        }
        return null;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ad.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */