package com.truecaller.voip.util;

public enum AudioRoute
{
  static
  {
    AudioRoute[] arrayOfAudioRoute = new AudioRoute[4];
    AudioRoute localAudioRoute = new com/truecaller/voip/util/AudioRoute;
    localAudioRoute.<init>("EARPIECE", 0);
    EARPIECE = localAudioRoute;
    arrayOfAudioRoute[0] = localAudioRoute;
    localAudioRoute = new com/truecaller/voip/util/AudioRoute;
    int i = 1;
    localAudioRoute.<init>("BLUETOOTH", i);
    BLUETOOTH = localAudioRoute;
    arrayOfAudioRoute[i] = localAudioRoute;
    localAudioRoute = new com/truecaller/voip/util/AudioRoute;
    i = 2;
    localAudioRoute.<init>("WIRED_HEADSET", i);
    WIRED_HEADSET = localAudioRoute;
    arrayOfAudioRoute[i] = localAudioRoute;
    localAudioRoute = new com/truecaller/voip/util/AudioRoute;
    i = 3;
    localAudioRoute.<init>("SPEAKER", i);
    SPEAKER = localAudioRoute;
    arrayOfAudioRoute[i] = localAudioRoute;
    $VALUES = arrayOfAudioRoute;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.AudioRoute
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */