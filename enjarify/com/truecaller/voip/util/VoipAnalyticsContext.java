package com.truecaller.voip.util;

public enum VoipAnalyticsContext
{
  private final String value;
  
  static
  {
    VoipAnalyticsContext[] arrayOfVoipAnalyticsContext = new VoipAnalyticsContext[3];
    VoipAnalyticsContext localVoipAnalyticsContext = new com/truecaller/voip/util/VoipAnalyticsContext;
    localVoipAnalyticsContext.<init>("NOTIFICATION", 0, "notification");
    NOTIFICATION = localVoipAnalyticsContext;
    arrayOfVoipAnalyticsContext[0] = localVoipAnalyticsContext;
    localVoipAnalyticsContext = new com/truecaller/voip/util/VoipAnalyticsContext;
    int i = 1;
    localVoipAnalyticsContext.<init>("VOIP_IN_CALL_UI", i, "VoipInCallUI");
    VOIP_IN_CALL_UI = localVoipAnalyticsContext;
    arrayOfVoipAnalyticsContext[i] = localVoipAnalyticsContext;
    localVoipAnalyticsContext = new com/truecaller/voip/util/VoipAnalyticsContext;
    i = 2;
    localVoipAnalyticsContext.<init>("MISSED_CALL_NOTIFICATION", i, "missedCallNotification");
    MISSED_CALL_NOTIFICATION = localVoipAnalyticsContext;
    arrayOfVoipAnalyticsContext[i] = localVoipAnalyticsContext;
    $VALUES = arrayOfVoipAnalyticsContext;
  }
  
  private VoipAnalyticsContext(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipAnalyticsContext
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */