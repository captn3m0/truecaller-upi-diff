package com.truecaller.voip.util;

import android.database.sqlite.SQLiteException;
import c.d.c;
import c.d.f;
import c.g.a.m;
import com.truecaller.log.AssertionUtil;
import kotlinx.coroutines.g;

public final class ad
  implements ac
{
  final com.truecaller.common.account.r a;
  final com.truecaller.voip.api.a b;
  final com.truecaller.voip.db.a c;
  private final f d;
  private final com.truecaller.utils.a e;
  
  public ad(f paramf, com.truecaller.common.account.r paramr, com.truecaller.voip.api.a parama, com.truecaller.voip.db.a parama1, com.truecaller.utils.a parama2)
  {
    d = paramf;
    a = paramr;
    b = parama;
    c = parama1;
    e = parama2;
  }
  
  static Object a(com.truecaller.voip.db.a parama, c.g.a.b paramb)
  {
    try
    {
      parama = paramb.invoke(parama);
    }
    catch (SQLiteException localSQLiteException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException);
      parama = null;
    }
    return parama;
  }
  
  static Object a(e.b paramb)
  {
    e.b localb = null;
    try
    {
      paramb = paramb.c();
      if (paramb != null)
      {
        paramb = paramb.e();
        localb = paramb;
      }
    }
    catch (Exception localException)
    {
      paramb = (Throwable)localException;
      AssertionUtil.reportThrowableButNeverCrash(paramb);
    }
    return localb;
  }
  
  public final Object a(c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/voip/util/ad$f;
    ((ad.f)localObject).<init>(this, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object a(String paramString, c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/voip/util/ad$d;
    ((ad.d)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object b(String paramString, c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/voip/util/ad$e;
    ((ad.e)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object c(String paramString, c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/voip/util/ad$c;
    ((ad.c)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */