package com.truecaller.voip.util;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;

final class c
  implements BluetoothProfile.ServiceListener
{
  final int[] a;
  BluetoothProfile b;
  private boolean c;
  
  public c()
  {
    int[] arrayOfInt = new int[2];
    int[] tmp9_8 = arrayOfInt;
    tmp9_8[0] = 2;
    tmp9_8[1] = 1;
    a = arrayOfInt;
  }
  
  public final void a()
  {
    int i = 1;
    c = i;
    BluetoothProfile localBluetoothProfile = b;
    if (localBluetoothProfile != null)
    {
      BluetoothAdapter localBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
      if (localBluetoothAdapter != null)
      {
        boolean bool = localBluetoothAdapter.isEnabled();
        if (!bool) {
          localBluetoothAdapter = null;
        }
        if (localBluetoothAdapter != null)
        {
          localBluetoothAdapter.closeProfileProxy(i, localBluetoothProfile);
          return;
        }
      }
      return;
    }
  }
  
  public final void onServiceConnected(int paramInt, BluetoothProfile paramBluetoothProfile)
  {
    int i = 1;
    String[] arrayOfString = new String[i];
    String str = String.valueOf(paramInt);
    str = "Bluetooth profile proxy is received. Profile: ".concat(str);
    arrayOfString[0] = str;
    b = paramBluetoothProfile;
    paramInt = c;
    if (paramInt != 0) {
      a();
    }
  }
  
  public final void onServiceDisconnected(int paramInt)
  {
    String[] arrayOfString = new String[1];
    String str = String.valueOf(paramInt);
    str = "Bluetooth headset listener disconnected. Profile: ".concat(str);
    arrayOfString[0] = str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */