package com.truecaller.voip.util;

import c.g.b.k;

public final class d$a
  extends d
{
  public final String a;
  
  public d$a(String paramString)
  {
    super((byte)0);
    a = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        String str = a;
        paramObject = a;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Connected(deviceName=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */