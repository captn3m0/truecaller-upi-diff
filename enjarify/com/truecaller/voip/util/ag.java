package com.truecaller.voip.util;

import c.g.b.k;

public final class ag
{
  public final String a;
  final String b;
  final String c;
  final boolean d;
  final Integer e;
  final boolean f;
  
  public ag(String paramString1, String paramString2, String paramString3, boolean paramBoolean1, Integer paramInteger, boolean paramBoolean2)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramBoolean1;
    e = paramInteger;
    f = paramBoolean2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ag;
      if (bool2)
      {
        paramObject = (ag)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              bool2 = d;
              boolean bool3 = d;
              if (bool2 == bool3)
              {
                bool2 = true;
              }
              else
              {
                bool2 = false;
                localObject1 = null;
              }
              if (bool2)
              {
                localObject1 = e;
                localObject2 = e;
                bool2 = k.a(localObject1, localObject2);
                if (bool2)
                {
                  bool2 = f;
                  boolean bool4 = f;
                  if (bool2 == bool4)
                  {
                    bool4 = true;
                  }
                  else
                  {
                    bool4 = false;
                    paramObject = null;
                  }
                  if (bool4) {
                    return bool1;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    k *= 31;
    Object localObject = b;
    int m;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    int k = (k + m) * 31;
    localObject = c;
    int n;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    k = (k + n) * 31;
    int i1 = d;
    if (i1 != 0) {
      i1 = 1;
    }
    k = (k + i1) * 31;
    localObject = e;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    k = (k + i) * 31;
    int j = f;
    if (j != 0) {
      j = 1;
    }
    return k + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipSearchResult(profileName=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", profilePictureUrl=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", phoneNumber=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", blocked=");
    boolean bool = d;
    localStringBuilder.append(bool);
    localStringBuilder.append(", spamScore=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", isPhonebookContact=");
    bool = f;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */