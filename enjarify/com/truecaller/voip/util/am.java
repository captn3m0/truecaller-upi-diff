package com.truecaller.voip.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import com.getkeepsafe.taptargetview.b;
import com.getkeepsafe.taptargetview.c;
import com.getkeepsafe.taptargetview.c.a;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.string;

public final class am
  implements ak
{
  c a;
  private final ah b;
  private final com.truecaller.voip.k c;
  
  public am(ah paramah, com.truecaller.voip.k paramk)
  {
    b = paramah;
    c = paramk;
  }
  
  public final void a(Activity paramActivity, View paramView, al paramal)
  {
    c.g.b.k.b(paramActivity, "activity");
    c.g.b.k.b(paramView, "view");
    c.g.b.k.b(paramal, "callback");
    Object localObject1 = c;
    boolean bool1 = ((com.truecaller.voip.k)localObject1).a();
    if (bool1)
    {
      localObject1 = b;
      Object localObject2 = "showCaseDisplayed";
      bool1 = ((ah)localObject1).b((String)localObject2);
      if (!bool1)
      {
        localObject1 = b;
        localObject2 = "showCaseDisplayed";
        boolean bool2 = true;
        ((ah)localObject1).b((String)localObject2, bool2);
        localObject1 = paramActivity;
        localObject1 = (Context)paramActivity;
        float f1 = paramView.getWidth() / 2;
        localObject1 = ((Context)localObject1).getResources();
        String str1 = "context.resources";
        c.g.b.k.a(localObject1, str1);
        localObject1 = ((Resources)localObject1).getDisplayMetrics();
        float f2 = densityDpi;
        float f3 = 160.0F;
        f2 /= f3;
        int i = (int)(f1 / f2);
        int j = 30;
        f1 = 4.2E-44F;
        if (i <= j)
        {
          i = 30;
          f2 = 4.2E-44F;
        }
        localObject2 = new com/truecaller/voip/util/am$a;
        ((am.a)localObject2).<init>(this, paramal);
        boolean bool3 = a();
        f3 = 0.0F;
        str1 = null;
        if (bool3)
        {
          paramal = a;
          if (paramal != null) {
            paramal.a(false);
          }
        }
        int k = R.string.voip_showcase_title;
        Object[] arrayOfObject = new Object[bool2];
        int m = R.string.voip_text;
        String str2 = paramActivity.getString(m);
        arrayOfObject[0] = str2;
        paramal = (CharSequence)paramActivity.getString(k, arrayOfObject);
        int n = R.string.voip_showcase_message;
        Object localObject3 = new Object[bool2];
        m = R.string.voip_text;
        str2 = paramActivity.getString(m);
        localObject3[0] = str2;
        localObject3 = (CharSequence)paramActivity.getString(n, (Object[])localObject3);
        paramView = b.a(paramView, paramal, (CharSequence)localObject3);
        k = R.color.voip_showcase_color;
        paramView = paramView.a(k).b();
        k = R.color.voip_showcase_color;
        paramView = paramView.b(k).c().d();
        k = R.color.voip_showcase_text_color;
        paramView = paramView.c(k);
        paramal = Typeface.SANS_SERIF;
        paramView = paramView.a(paramal);
        k = R.color.voip_showcase_color;
        paramView = paramView.d(k).e().f().g().a().e(i);
        localObject2 = (c.a)localObject2;
        paramActivity = c.a(paramActivity, paramView, (c.a)localObject2);
        a = paramActivity;
        return;
      }
    }
  }
  
  public final boolean a()
  {
    c localc = a;
    if (localc != null) {
      return localc.a();
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */