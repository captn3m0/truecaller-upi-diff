package com.truecaller.voip.util;

import c.g.a.a;
import c.g.a.m;
import c.g.b.k;
import c.g.b.v.c;
import c.l;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.tracking.events.af;
import com.truecaller.tracking.events.af.a;
import com.truecaller.tracking.events.ag.a;
import com.truecaller.tracking.events.ah;
import com.truecaller.tracking.events.ah.a;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import java.util.HashMap;
import java.util.Map;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.bn;
import org.apache.a.d.d;

public final class p
  implements o, kotlinx.coroutines.ag
{
  final com.truecaller.analytics.b a;
  final com.truecaller.androidactors.f b;
  final aa c;
  private final c.d.f d;
  
  public p(c.d.f paramf, com.truecaller.analytics.b paramb, com.truecaller.androidactors.f paramf1, aa paramaa)
  {
    d = paramf;
    a = paramb;
    b = paramf1;
    c = paramaa;
  }
  
  static VoipAnalyticsState a(VoipAnalyticsCallDirection paramVoipAnalyticsCallDirection)
  {
    int[] arrayOfInt = q.d;
    int i = paramVoipAnalyticsCallDirection.ordinal();
    i = arrayOfInt[i];
    switch (i)
    {
    default: 
      paramVoipAnalyticsCallDirection = new c/l;
      paramVoipAnalyticsCallDirection.<init>();
      throw paramVoipAnalyticsCallDirection;
    case 2: 
      return VoipAnalyticsState.INIT_FAILED;
    }
    return VoipAnalyticsState.FAILED;
  }
  
  public final c.d.f V_()
  {
    return d;
  }
  
  public final void a(VoipAnalyticsInCallUiAction paramVoipAnalyticsInCallUiAction)
  {
    k.b(paramVoipAnalyticsInCallUiAction, "action");
    Object localObject1 = a;
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("ViewAction");
    String str = paramVoipAnalyticsInCallUiAction.getValue();
    localObject2 = ((e.a)localObject2).a("Action", str);
    str = VoipAnalyticsContext.VOIP_IN_CALL_UI.getValue();
    localObject2 = ((e.a)localObject2).a("Context", str).a();
    k.a(localObject2, "AnalyticsEvent.Builder(E…\n                .build()");
    ((com.truecaller.analytics.b)localObject1).b((com.truecaller.analytics.e)localObject2);
    localObject1 = (ae)b.a();
    localObject2 = af.b();
    paramVoipAnalyticsInCallUiAction = (CharSequence)paramVoipAnalyticsInCallUiAction.getValue();
    paramVoipAnalyticsInCallUiAction = ((af.a)localObject2).b(paramVoipAnalyticsInCallUiAction);
    localObject2 = (CharSequence)VoipAnalyticsContext.VOIP_IN_CALL_UI.getValue();
    paramVoipAnalyticsInCallUiAction = (d)paramVoipAnalyticsInCallUiAction.a((CharSequence)localObject2).a();
    ((ae)localObject1).a(paramVoipAnalyticsInCallUiAction);
  }
  
  public final void a(n paramn, VoipAnalyticsState paramVoipAnalyticsState, VoipAnalyticsStateReason paramVoipAnalyticsStateReason)
  {
    k.b(paramn, "callInfo");
    k.b(paramVoipAnalyticsState, "state");
    Object localObject1 = a;
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("VoipStateChanged");
    Object localObject3 = a.getValue();
    localObject2 = ((e.a)localObject2).a("Direction", (String)localObject3);
    localObject3 = paramVoipAnalyticsState.getValue();
    localObject2 = ((e.a)localObject2).a("State", (String)localObject3);
    k.a(localObject2, "AnalyticsEvent.Builder(E…PARAM_STATE, state.value)");
    String str1 = "Reason";
    localObject3 = null;
    String str2;
    if (paramVoipAnalyticsStateReason != null) {
      str2 = paramVoipAnalyticsStateReason.getValue();
    } else {
      str2 = null;
    }
    if (str2 != null) {
      ((e.a)localObject2).a(str1, str2);
    }
    localObject2 = ((e.a)localObject2).a();
    str1 = "AnalyticsEvent.Builder(E…\n                .build()";
    k.a(localObject2, str1);
    ((com.truecaller.analytics.b)localObject1).b((com.truecaller.analytics.e)localObject2);
    localObject1 = ah.b();
    localObject2 = (CharSequence)a.getValue();
    localObject1 = ((ah.a)localObject1).a((CharSequence)localObject2);
    paramVoipAnalyticsState = (CharSequence)paramVoipAnalyticsState.getValue();
    paramVoipAnalyticsState = ((ah.a)localObject1).b(paramVoipAnalyticsState);
    localObject1 = (CharSequence)b;
    paramVoipAnalyticsState = paramVoipAnalyticsState.d((CharSequence)localObject1);
    if (paramVoipAnalyticsStateReason != null) {
      localObject3 = paramVoipAnalyticsStateReason.getValue();
    }
    localObject3 = (CharSequence)localObject3;
    paramVoipAnalyticsState = paramVoipAnalyticsState.c((CharSequence)localObject3);
    paramVoipAnalyticsStateReason = (CharSequence)c;
    paramVoipAnalyticsState = paramVoipAnalyticsState.e(paramVoipAnalyticsStateReason);
    paramVoipAnalyticsStateReason = (CharSequence)e;
    paramVoipAnalyticsState = paramVoipAnalyticsState.f(paramVoipAnalyticsStateReason);
    paramVoipAnalyticsStateReason = d;
    paramVoipAnalyticsState = paramVoipAnalyticsState.a(paramVoipAnalyticsStateReason);
    paramn = f;
    paramn = paramVoipAnalyticsState.b(paramn);
    paramVoipAnalyticsState = (ae)b.a();
    paramn = (d)paramn.a();
    paramVoipAnalyticsState.a(paramn);
  }
  
  public final void a(String paramString, long paramLong)
  {
    k.b(paramString, "channelId");
    Object localObject1 = a;
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("VoipCallFinished");
    Double localDouble = Double.valueOf(paramLong);
    localObject2 = ((e.a)localObject2).a(localDouble).a();
    k.a(localObject2, "AnalyticsEvent.Builder(E…\n                .build()");
    ((com.truecaller.analytics.b)localObject1).b((com.truecaller.analytics.e)localObject2);
    localObject1 = (ae)b.a();
    localObject2 = com.truecaller.tracking.events.ag.b();
    paramString = (CharSequence)paramString;
    paramString = (d)((ag.a)localObject2).a(paramString).a(paramLong).a();
    ((ae)localObject1).a(paramString);
  }
  
  public final void a(String paramString, VoipAnalyticsFailedCallAction paramVoipAnalyticsFailedCallAction)
  {
    k.b(paramString, "analyticsContext");
    k.b(paramVoipAnalyticsFailedCallAction, "action");
    Object localObject1 = a;
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("ViewAction");
    localObject2 = ((e.a)localObject2).a("Context", paramString);
    String str = paramVoipAnalyticsFailedCallAction.getValue();
    localObject2 = ((e.a)localObject2).a("Action", str).a();
    k.a(localObject2, "AnalyticsEvent.Builder(E…\n                .build()");
    ((com.truecaller.analytics.b)localObject1).b((com.truecaller.analytics.e)localObject2);
    localObject1 = (ae)b.a();
    localObject2 = af.b();
    paramString = (CharSequence)paramString;
    paramString = ((af.a)localObject2).a(paramString);
    paramVoipAnalyticsFailedCallAction = (CharSequence)paramVoipAnalyticsFailedCallAction.getValue();
    paramString = (d)paramString.b(paramVoipAnalyticsFailedCallAction).a();
    ((ae)localObject1).a(paramString);
  }
  
  public final void a(String paramString, VoipAnalyticsNotificationAction paramVoipAnalyticsNotificationAction)
  {
    k.b(paramString, "analyticsContext");
    k.b(paramVoipAnalyticsNotificationAction, "action");
    Object localObject1 = a;
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("ViewAction");
    localObject2 = ((e.a)localObject2).a("Context", paramString);
    String str = paramVoipAnalyticsNotificationAction.getValue();
    localObject2 = ((e.a)localObject2).a("Action", str).a();
    k.a(localObject2, "AnalyticsEvent.Builder(E…\n                .build()");
    ((com.truecaller.analytics.b)localObject1).b((com.truecaller.analytics.e)localObject2);
    localObject1 = (ae)b.a();
    localObject2 = af.b();
    paramString = (CharSequence)paramString;
    paramString = ((af.a)localObject2).a(paramString);
    paramVoipAnalyticsNotificationAction = (CharSequence)paramVoipAnalyticsNotificationAction.getValue();
    paramString = (d)paramString.b(paramVoipAnalyticsNotificationAction).a();
    ((ae)localObject1).a(paramString);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "analyticsContext");
    k.b(paramString2, "number");
    Object localObject = new com/truecaller/voip/util/p$e;
    ((p.e)localObject).<init>(this, paramString2, paramString1, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
  
  public final void a(String paramString1, String paramString2, int paramInt, boolean paramBoolean)
  {
    k.b(paramString1, "voipId");
    k.b(paramString2, "token");
    Object localObject1 = a;
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("VoipRTMLoginError");
    localObject2 = ((e.a)localObject2).a("ErrorCode", paramInt).a("IsRetryAttempt", paramBoolean).a();
    k.a(localObject2, "AnalyticsEvent.Builder(E…\n                .build()");
    ((com.truecaller.analytics.b)localObject1).b((com.truecaller.analytics.e)localObject2);
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    localObject1 = (Map)localObject1;
    String str1 = String.valueOf(paramInt);
    ((Map)localObject1).put("ErrorCode", str1);
    String str2 = String.valueOf(paramBoolean);
    ((Map)localObject1).put("IsRetryAttempt", str2);
    ((Map)localObject1).put("VoipId", paramString1);
    ((Map)localObject1).put("Token", paramString2);
    paramString1 = ao.b();
    paramString2 = (CharSequence)"VoipRTMLoginError";
    paramString1 = paramString1.a(paramString2).a((Map)localObject1).a();
    paramString2 = (ae)b.a();
    paramString1 = (d)paramString1;
    paramString2.a(paramString1);
  }
  
  public final void a(kotlinx.coroutines.ag paramag, VoipAnalyticsCallDirection paramVoipAnalyticsCallDirection, String paramString, a parama1, a parama2, u paramu1, u paramu2, u paramu3)
  {
    Object localObject1 = paramag;
    Object localObject2 = paramu1;
    k.b(paramag, "scope");
    k.b(paramVoipAnalyticsCallDirection, "direction");
    k.b(paramString, "channelId");
    k.b(paramu1, "statesChannel");
    k.b(paramu2, "usersChannel");
    Object localObject3 = new c/g/b/v$c;
    ((v.c)localObject3).<init>();
    a = null;
    Object localObject4 = new com/truecaller/voip/util/p$f;
    Object localObject5 = localObject4;
    ((p.f)localObject4).<init>((v.c)localObject3, paramu2, paramVoipAnalyticsCallDirection, paramString, parama1, parama2);
    localObject4 = (a)localObject4;
    localObject5 = d;
    localObject3 = new com/truecaller/voip/util/p$c;
    ((p.c)localObject3).<init>(this, paramu1, (a)localObject4, null);
    localObject3 = (m)localObject3;
    int i = 2;
    localObject5 = kotlinx.coroutines.e.b(paramag, (c.d.f)localObject5, (m)localObject3, i);
    localObject3 = new com/truecaller/voip/util/p$d;
    ((p.d)localObject3).<init>(paramu1);
    localObject3 = (c.g.a.b)localObject3;
    ((bn)localObject5).a_((c.g.a.b)localObject3);
    if (paramu3 != null)
    {
      localObject2 = d;
      localObject5 = new com/truecaller/voip/util/p$a;
      ((p.a)localObject5).<init>(this, paramu3, (a)localObject4, null);
      localObject5 = (m)localObject5;
      localObject1 = kotlinx.coroutines.e.b(paramag, (c.d.f)localObject2, (m)localObject5, i);
      localObject2 = new com/truecaller/voip/util/p$b;
      ((p.b)localObject2).<init>(paramu3);
      localObject2 = (c.g.a.b)localObject2;
      ((bn)localObject1).a_((c.g.a.b)localObject2);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */