package com.truecaller.voip.util;

import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import com.truecaller.utils.extensions.i;

final class t$b
  implements e
{
  private final AudioManager.OnAudioFocusChangeListener b;
  
  public t$b(t paramt, AudioManager.OnAudioFocusChangeListener paramOnAudioFocusChangeListener)
  {
    b = paramOnAudioFocusChangeListener;
  }
  
  public final void a()
  {
    AudioManager localAudioManager = i.i(a.b);
    AudioManager.OnAudioFocusChangeListener localOnAudioFocusChangeListener = b;
    localAudioManager.abandonAudioFocus(localOnAudioFocusChangeListener);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.t.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */