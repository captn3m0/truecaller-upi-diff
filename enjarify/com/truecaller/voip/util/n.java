package com.truecaller.voip.util;

import c.g.b.k;

public final class n
{
  final VoipAnalyticsCallDirection a;
  final String b;
  final String c;
  final Integer d;
  final String e;
  final Integer f;
  
  public n(VoipAnalyticsCallDirection paramVoipAnalyticsCallDirection, String paramString1, String paramString2, Integer paramInteger1, String paramString3, Integer paramInteger2)
  {
    a = paramVoipAnalyticsCallDirection;
    b = paramString1;
    c = paramString2;
    d = paramInteger1;
    e = paramString3;
    f = paramInteger2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof n;
      if (bool1)
      {
        paramObject = (n)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = c;
            localObject2 = c;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = d;
              localObject2 = d;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = e;
                localObject2 = e;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = f;
                  paramObject = f;
                  boolean bool2 = k.a(localObject1, paramObject);
                  if (bool2) {
                    break label156;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label156:
    return true;
  }
  
  public final int hashCode()
  {
    VoipAnalyticsCallDirection localVoipAnalyticsCallDirection = a;
    int i = 0;
    if (localVoipAnalyticsCallDirection != null)
    {
      j = localVoipAnalyticsCallDirection.hashCode();
    }
    else
    {
      j = 0;
      localVoipAnalyticsCallDirection = null;
    }
    j *= 31;
    Object localObject = b;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = d;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = e;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = f;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipAnalyticsCallInfo(direction=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", channelId=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", voipId=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", rtcUid=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", peerVoipId=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", peerRtcUid=");
    localObject = f;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */