package com.truecaller.voip.util;

import c.d.c;
import c.g.a.b;
import c.o.b;
import c.x;
import com.truecaller.common.account.r;
import com.truecaller.voip.api.VoipIdDto;
import com.truecaller.voip.db.VoipIdCache;
import kotlinx.coroutines.ag;

final class ad$f
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag c;
  
  ad$f(ad paramad, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/voip/util/ad$f;
    ad localad = b;
    localf.<init>(localad, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        new String[1][0] = "Fetching own voip id.";
        paramObject = b.a.b();
        bool1 = false;
        Object localObject1 = null;
        if (paramObject != null)
        {
          localObject2 = "+";
          bool2 = c.n.m.b((String)paramObject, (String)localObject2, false);
          if (bool2) {}
        }
        else
        {
          paramObject = null;
        }
        boolean bool2 = true;
        Object localObject3 = new String[bool2];
        String str = String.valueOf(paramObject);
        Object localObject4 = "Own phone number is ".concat(str);
        localObject3[0] = localObject4;
        if (paramObject == null) {
          return null;
        }
        localObject3 = b.c;
        localObject4 = new com/truecaller/voip/util/ad$f$a;
        ((ad.f.a)localObject4).<init>((String)paramObject);
        localObject4 = (b)localObject4;
        localObject3 = (VoipIdCache)ad.a((com.truecaller.voip.db.a)localObject3, (b)localObject4);
        if (localObject3 != null)
        {
          localObject4 = b;
          localObject3 = ad.a((ad)localObject4, (VoipIdCache)localObject3);
        }
        else
        {
          localObject3 = null;
        }
        if (localObject3 != null)
        {
          paramObject = new String[bool2];
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>("Returning cached voip id:");
          localObject2 = ((VoipIdCache)localObject3).getVoipId();
          ((StringBuilder)localObject1).append((String)localObject2);
          localObject1 = ((StringBuilder)localObject1).toString();
          paramObject[0] = localObject1;
          return ((VoipIdCache)localObject3).getVoipId();
        }
        new String[1][0] = "Own id is not registered. Registering...";
        localObject3 = (VoipIdDto)ad.a(b.b.a());
        if (localObject3 != null)
        {
          localObject4 = b;
          paramObject = ad.a((ad)localObject4, (VoipIdDto)localObject3, (String)paramObject);
        }
        else
        {
          paramObject = null;
        }
        Object localObject2 = new String[bool2];
        localObject4 = String.valueOf(paramObject);
        localObject3 = "Fetched own voip id is ".concat((String)localObject4);
        localObject2[0] = localObject3;
        if (paramObject != null) {
          return ((VoipIdCache)paramObject).getVoipId();
        }
        return null;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ad.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */