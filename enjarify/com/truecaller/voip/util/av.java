package com.truecaller.voip.util;

import c.d.a.a;
import c.d.c;
import c.o.b;

public final class av
  implements au
{
  private final ac a;
  private final aa b;
  
  public av(ac paramac, aa paramaa)
  {
    a = paramac;
    b = paramaa;
  }
  
  public final Object a(ag paramag, c paramc)
  {
    boolean bool1 = paramc instanceof av.b;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (av.b)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label77;
      }
    }
    Object localObject1 = new com/truecaller/voip/util/av$b;
    ((av.b)localObject1).<init>(this, (c)paramc);
    label77:
    paramc = a;
    a locala = a.a;
    int j = b;
    int m = 1;
    switch (j)
    {
    default: 
      paramag = new java/lang/IllegalStateException;
      paramag.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramag;
    case 1: 
      paramag = (ag)e;
      bool1 = paramc instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label327;
      }
      paramc = new String[m];
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Fetching voip user from number:");
      String str = c;
      ((StringBuilder)localObject2).append(str);
      localObject2 = ((StringBuilder)localObject2).toString();
      paramc[0] = localObject2;
      paramc = a;
      localObject2 = c;
      d = this;
      e = paramag;
      b = m;
      paramc = paramc.a((String)localObject2, (c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      break;
    }
    paramc = (String)paramc;
    if (paramc == null)
    {
      paramc = new String[m];
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Cannot fetch voip id from number:");
      paramag = c;
      ((StringBuilder)localObject1).append(paramag);
      paramag = ((StringBuilder)localObject1).toString();
      paramc[0] = paramag;
      return null;
    }
    return aw.a(paramag, paramc);
    label327:
    throw a;
  }
  
  public final Object a(String paramString, c paramc)
  {
    boolean bool1 = paramc instanceof av.a;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (av.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label77;
      }
    }
    Object localObject1 = new com/truecaller/voip/util/av$a;
    ((av.a)localObject1).<init>(this, (c)paramc);
    label77:
    paramc = a;
    a locala = a.a;
    int j = b;
    int m = 0;
    int n = 1;
    Object localObject2;
    switch (j)
    {
    default: 
      paramString = new java/lang/IllegalStateException;
      paramString.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramString;
    case 2: 
      boolean bool3 = paramc instanceof o.b;
      if (!bool3) {
        break label367;
      }
      throw a;
    case 1: 
      paramString = (String)e;
      localObject2 = (av)d;
      boolean bool4 = paramc instanceof o.b;
      if (bool4) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label369;
      }
      paramc = new String[n];
      String str = String.valueOf(paramString);
      localObject2 = "Fetching voip user from voip id:".concat(str);
      paramc[0] = localObject2;
      paramc = a;
      d = this;
      e = paramString;
      b = n;
      paramc = paramc.b(paramString, (c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      localObject2 = this;
    }
    paramc = (String)paramc;
    if (paramc == null)
    {
      paramc = new String[n];
      paramString = String.valueOf(paramString);
      paramString = "Cannot fetch number from voip id:".concat(paramString);
      paramc[0] = paramString;
      return null;
    }
    d = localObject2;
    e = paramString;
    f = paramc;
    m = 2;
    b = m;
    paramc = ((av)localObject2).a(paramString, paramc, (c)localObject1);
    if (paramc == locala) {
      return locala;
    }
    label367:
    return paramc;
    label369:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.av
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */