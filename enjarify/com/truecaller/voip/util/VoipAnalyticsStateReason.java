package com.truecaller.voip.util;

public enum VoipAnalyticsStateReason
{
  private final String value;
  
  static
  {
    VoipAnalyticsStateReason[] arrayOfVoipAnalyticsStateReason = new VoipAnalyticsStateReason[18];
    VoipAnalyticsStateReason localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    localVoipAnalyticsStateReason.<init>("OFFLINE", 0, "Offline");
    OFFLINE = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[0] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    int i = 1;
    localVoipAnalyticsStateReason.<init>("MIC_PERMISSION_DENIED", i, "MicPermissionDenied");
    MIC_PERMISSION_DENIED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 2;
    localVoipAnalyticsStateReason.<init>("GET_PHONE_FAILED", i, "GetPhoneFailed");
    GET_PHONE_FAILED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 3;
    localVoipAnalyticsStateReason.<init>("WAKE_UP_SENT_FAILED", i, "WakeUpSendFailed");
    WAKE_UP_SENT_FAILED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 4;
    localVoipAnalyticsStateReason.<init>("WAKE_UP_TIMEOUT", i, "WakeUpTimeout");
    WAKE_UP_TIMEOUT = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 5;
    localVoipAnalyticsStateReason.<init>("SEARCH_FAILED", i, "SearchFailed");
    SEARCH_FAILED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 6;
    localVoipAnalyticsStateReason.<init>("GET_VOIP_ID_FAILED", i, "GetVoipIdFailed");
    GET_VOIP_ID_FAILED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 7;
    localVoipAnalyticsStateReason.<init>("GET_RTM_FAILED", i, "GetRtmFailed");
    GET_RTM_FAILED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 8;
    localVoipAnalyticsStateReason.<init>("GET_RTC_FAILED", i, "GetRtcFailed");
    GET_RTC_FAILED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 9;
    localVoipAnalyticsStateReason.<init>("JOIN_RTC_FAILED", i, "JoinRtcFailed");
    JOIN_RTC_FAILED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 10;
    localVoipAnalyticsStateReason.<init>("JOIN_RTM_FAILED", i, "JoinRtmFailed");
    JOIN_RTM_FAILED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 11;
    localVoipAnalyticsStateReason.<init>("RECEIVED_END", i, "ReceivedEnd");
    RECEIVED_END = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 12;
    localVoipAnalyticsStateReason.<init>("RECEIVED_FAILED", i, "ReceivedFailed");
    RECEIVED_FAILED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 13;
    localVoipAnalyticsStateReason.<init>("PRESSED_END", i, "PressedEnd");
    PRESSED_END = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    i = 14;
    localVoipAnalyticsStateReason.<init>("REMOTE_LEFT_CHANNEL", i, "RemoteLeftChannel");
    REMOTE_LEFT_CHANNEL = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[i] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    localVoipAnalyticsStateReason.<init>("POLL_FAILED", 15, "PollFailed");
    POLL_FAILED = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[15] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    localVoipAnalyticsStateReason.<init>("INVITE_TIMEOUT", 16, "InviteTimeout");
    INVITE_TIMEOUT = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[16] = localVoipAnalyticsStateReason;
    localVoipAnalyticsStateReason = new com/truecaller/voip/util/VoipAnalyticsStateReason;
    localVoipAnalyticsStateReason.<init>("RING_TIMEOUT", 17, "RingTimeout");
    RING_TIMEOUT = localVoipAnalyticsStateReason;
    arrayOfVoipAnalyticsStateReason[17] = localVoipAnalyticsStateReason;
    $VALUES = arrayOfVoipAnalyticsStateReason;
  }
  
  private VoipAnalyticsStateReason(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipAnalyticsStateReason
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */