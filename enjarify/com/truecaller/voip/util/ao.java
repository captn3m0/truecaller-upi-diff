package com.truecaller.voip.util;

import android.content.ComponentName;
import android.content.Context;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccount.Builder;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import c.l.g;
import c.n.m;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.d;
import com.truecaller.utils.extensions.i;
import com.truecaller.utils.extensions.r;
import com.truecaller.voip.R.string;
import com.truecaller.voip.callconnection.VoipCallConnectionService;
import java.util.Iterator;
import java.util.List;

public final class ao
  implements an
{
  private final e a;
  private final d b;
  private final Context c;
  private final com.truecaller.voip.k d;
  private final com.truecaller.voip.callconnection.f e;
  
  public ao(e parame, d paramd, Context paramContext, com.truecaller.voip.k paramk, com.truecaller.voip.callconnection.f paramf)
  {
    a = parame;
    b = paramd;
    c = paramContext;
    d = paramk;
    e = paramf;
  }
  
  private boolean a()
  {
    Object localObject1 = d;
    boolean bool1 = ((com.truecaller.voip.k)localObject1).a();
    if (bool1)
    {
      int i = Build.VERSION.SDK_INT;
      int j = 26;
      if (i >= j) {
        try
        {
          localObject1 = c;
          localObject1 = i.c((Context)localObject1);
          Object localObject2 = b();
          Object localObject3 = ((TelecomManager)localObject1).getPhoneAccount((PhoneAccountHandle)localObject2);
          Object localObject4 = a;
          Object localObject5 = P;
          Object localObject6 = e.a;
          int k = 112;
          localObject6 = localObject6[k];
          localObject4 = ((e.a)localObject5).a((e)localObject4, (g)localObject6);
          localObject4 = (com.truecaller.featuretoggles.f)localObject4;
          localObject4 = ((com.truecaller.featuretoggles.f)localObject4).e();
          localObject5 = localObject4;
          localObject5 = (CharSequence)localObject4;
          boolean bool2 = m.a((CharSequence)localObject5);
          int n = 1;
          bool2 ^= n;
          k = 0;
          Object localObject7 = null;
          if (!bool2)
          {
            i1 = 0;
            localObject4 = null;
          }
          if (localObject4 != null)
          {
            localObject4 = (CharSequence)localObject4;
            localObject5 = ",";
            localObject5 = new String[] { localObject5 };
            int i2 = 6;
            localObject4 = m.c((CharSequence)localObject4, (String[])localObject5, false, i2);
            if (localObject4 != null)
            {
              int m = ((List)localObject4).size();
              boolean bool3;
              if (m == n)
              {
                localObject5 = ((List)localObject4).get(0);
                localObject5 = (String)localObject5;
                localObject8 = "AllModels";
                bool3 = c.g.b.k.a(localObject5, localObject8);
                if (bool3)
                {
                  i1 = 1;
                  break label412;
                }
              }
              localObject5 = b;
              localObject5 = ((d)localObject5).l();
              Object localObject8 = localObject5;
              localObject8 = (CharSequence)localObject5;
              boolean bool4 = m.a((CharSequence)localObject8) ^ n;
              if (!bool4)
              {
                bool3 = false;
                localObject5 = null;
              }
              if (localObject5 != null)
              {
                localObject4 = (Iterable)localObject4;
                localObject4 = ((Iterable)localObject4).iterator();
                boolean bool5;
                do
                {
                  bool4 = ((Iterator)localObject4).hasNext();
                  if (!bool4) {
                    break;
                  }
                  localObject8 = ((Iterator)localObject4).next();
                  Object localObject9 = localObject8;
                  localObject9 = (String)localObject8;
                  bool5 = m.a((String)localObject5, (String)localObject9, n);
                } while (!bool5);
                localObject7 = localObject8;
                if (localObject7 != null)
                {
                  i1 = 1;
                  break label412;
                }
              }
            }
          }
          int i1 = 0;
          localObject4 = null;
          label412:
          if (localObject3 != null)
          {
            if (i1 != 0)
            {
              ((TelecomManager)localObject1).unregisterPhoneAccount((PhoneAccountHandle)localObject2);
              return false;
            }
            localObject1 = "Phone account is already registered.";
            new String[1][0] = localObject1;
            return n;
          }
          if (i1 != 0) {
            return false;
          }
          localObject3 = c;
          i1 = R.string.voip_text;
          localObject3 = ((Context)localObject3).getString(i1);
          localObject3 = (CharSequence)localObject3;
          localObject2 = PhoneAccount.builder((PhoneAccountHandle)localObject2, (CharSequence)localObject3);
          int i3 = 2048;
          localObject2 = ((PhoneAccount.Builder)localObject2).setCapabilities(i3);
          localObject3 = "tel";
          localObject2 = ((PhoneAccount.Builder)localObject2).addSupportedUriScheme((String)localObject3);
          localObject2 = ((PhoneAccount.Builder)localObject2).build();
          ((TelecomManager)localObject1).registerPhoneAccount((PhoneAccount)localObject2);
          localObject1 = "New phone account is registered.";
          new String[1][0] = localObject1;
          return n;
        }
        catch (SecurityException localSecurityException)
        {
          new String[1][0] = "Cannot register phone account.";
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
          return false;
        }
      }
    }
    return false;
  }
  
  private final PhoneAccountHandle b()
  {
    ComponentName localComponentName = new android/content/ComponentName;
    Object localObject = c;
    localComponentName.<init>((Context)localObject, VoipCallConnectionService.class);
    localObject = new android/telecom/PhoneAccountHandle;
    ((PhoneAccountHandle)localObject).<init>(localComponentName, "TruecallerVoipAccount");
    return (PhoneAccountHandle)localObject;
  }
  
  public final boolean a(String paramString)
  {
    Object localObject1 = "number";
    c.g.b.k.b(paramString, (String)localObject1);
    boolean bool1 = a();
    boolean bool2 = false;
    if (!bool1) {
      return false;
    }
    try
    {
      localObject1 = c;
      localObject1 = i.c((Context)localObject1);
      Uri localUri = r.a(paramString);
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      String str = "android.telecom.extra.PHONE_ACCOUNT_HANDLE";
      Object localObject2 = b();
      localObject2 = (Parcelable)localObject2;
      localBundle.putParcelable(str, (Parcelable)localObject2);
      ((TelecomManager)localObject1).placeCall(localUri, localBundle);
      localObject1 = e;
      ((com.truecaller.voip.callconnection.f)localObject1).d(paramString);
      paramString = "New voip call is placed.";
      new String[1][0] = paramString;
      bool2 = true;
    }
    catch (SecurityException paramString)
    {
      localObject1 = "Cannot place a new voip call.";
      new String[1][0] = localObject1;
      paramString = (Throwable)paramString;
      AssertionUtil.reportThrowableButNeverCrash(paramString);
    }
    return bool2;
  }
  
  public final boolean b(String paramString)
  {
    Object localObject1 = "number";
    c.g.b.k.b(paramString, (String)localObject1);
    boolean bool1 = a();
    boolean bool2 = false;
    if (!bool1) {
      return false;
    }
    try
    {
      localObject1 = b();
      Object localObject2 = c;
      localObject2 = i.c((Context)localObject2);
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      String str = "android.telecom.extra.PHONE_ACCOUNT_HANDLE";
      Object localObject3 = localObject1;
      localObject3 = (Parcelable)localObject1;
      localBundle.putParcelable(str, (Parcelable)localObject3);
      str = "android.telecom.extra.INCOMING_CALL_ADDRESS";
      paramString = r.a(paramString);
      paramString = (Parcelable)paramString;
      localBundle.putParcelable(str, paramString);
      ((TelecomManager)localObject2).addNewIncomingCall((PhoneAccountHandle)localObject1, localBundle);
      paramString = "New incoming call is added.";
      new String[1][0] = paramString;
      bool2 = true;
    }
    catch (SecurityException paramString)
    {
      localObject1 = "Cannot add new incoming call.";
      new String[1][0] = localObject1;
      paramString = (Throwable)paramString;
      AssertionUtil.reportThrowableButNeverCrash(paramString);
    }
    return bool2;
  }
  
  public final boolean c(String paramString)
  {
    int i = 1;
    String[] arrayOfString = new String[i];
    String str1 = String.valueOf(paramString);
    String str2 = "Checking if there is voip call for number: ".concat(str1);
    str1 = null;
    arrayOfString[0] = str2;
    if (paramString == null) {
      return e.a();
    }
    return e.a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */