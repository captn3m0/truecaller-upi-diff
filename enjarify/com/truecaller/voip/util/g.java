package com.truecaller.voip.util;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioAttributes.Builder;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build.VERSION;
import android.os.VibrationEffect;
import android.os.Vibrator;
import c.l;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.ConnectionState;
import com.truecaller.voip.VoipState;

public final class g
  implements f
{
  private final ToneGenerator a;
  private final Vibrator b;
  private final AudioManager c;
  
  public g(Context paramContext)
  {
    Vibrator localVibrator = i.h(paramContext);
    b = localVibrator;
    paramContext = i.i(paramContext);
    c = paramContext;
    try
    {
      paramContext = new android/media/ToneGenerator;
      localVibrator = null;
      int i = 100;
      paramContext.<init>(0, i);
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeException);
      paramContext = null;
    }
    a = paramContext;
  }
  
  private static AudioAttributes e()
  {
    AudioAttributes.Builder localBuilder = new android/media/AudioAttributes$Builder;
    localBuilder.<init>();
    return localBuilder.setContentType(4).setUsage(6).build();
  }
  
  public final void a()
  {
    Object localObject = b;
    boolean bool = ((Vibrator)localObject).hasVibrator();
    if (!bool) {
      return;
    }
    localObject = c;
    int i = ((AudioManager)localObject).getRingerMode();
    if (i == 0) {
      return;
    }
    i = Build.VERSION.SDK_INT;
    int j = 26;
    long l = 400L;
    if (i >= j)
    {
      localObject = b;
      VibrationEffect localVibrationEffect = VibrationEffect.createOneShot(l, -1);
      ((Vibrator)localObject).vibrate(localVibrationEffect);
      return;
    }
    b.vibrate(l);
  }
  
  public final void a(VoipState paramVoipState, ConnectionState paramConnectionState, com.truecaller.voip.manager.k paramk1, com.truecaller.voip.manager.k paramk2)
  {
    c.g.b.k.b(paramVoipState, "voipState");
    c.g.b.k.b(paramConnectionState, "connectionState");
    c.g.b.k.b(paramk1, "serviceSetting");
    c.g.b.k.b(paramk2, "peerServiceSetting");
    boolean bool1 = b;
    Integer localInteger = null;
    int k;
    if (!bool1)
    {
      bool1 = b;
      int j = 22;
      if (bool1)
      {
        localInteger = Integer.valueOf(j);
      }
      else
      {
        paramk1 = h.a;
        k = paramConnectionState.ordinal();
        k = paramk1[k];
        int i = 96;
        switch (k)
        {
        default: 
          paramVoipState = new c/l;
          paramVoipState.<init>();
          throw paramVoipState;
        case 3: 
          localInteger = Integer.valueOf(i);
          break;
        case 2: 
          localInteger = Integer.valueOf(j);
          break;
        case 1: 
          paramConnectionState = h.b;
          int m = paramVoipState.ordinal();
          m = paramConnectionState[m];
          int n;
          switch (m)
          {
          default: 
            paramVoipState = new c/l;
            paramVoipState.<init>();
            throw paramVoipState;
          case 12: 
            boolean bool2 = a;
            if (bool2)
            {
              n = 19;
              localInteger = Integer.valueOf(n);
            }
            break;
          case 6: 
          case 7: 
          case 8: 
          case 9: 
          case 10: 
          case 11: 
            localInteger = Integer.valueOf(i);
            break;
          case 5: 
            n = 23;
            localInteger = Integer.valueOf(n);
            break;
          case 1: 
          case 2: 
          case 3: 
          case 4: 
            localInteger = Integer.valueOf(j);
          }
          break;
        }
      }
    }
    if (localInteger == null)
    {
      paramVoipState = a;
      if (paramVoipState != null) {
        paramVoipState.stopTone();
      }
      return;
    }
    paramVoipState = a;
    if (paramVoipState != null)
    {
      k = localInteger.intValue();
      paramVoipState.startTone(k);
      return;
    }
  }
  
  public final void b()
  {
    Object localObject = b;
    boolean bool = ((Vibrator)localObject).hasVibrator();
    if (!bool) {
      return;
    }
    localObject = c;
    int i = ((AudioManager)localObject).getRingerMode();
    if (i == 0) {
      return;
    }
    i = 2;
    localObject = new long[i];
    Object tmp37_36 = localObject;
    tmp37_36[0] = 1000L;
    tmp37_36[1] = 1000L;
    int j = Build.VERSION.SDK_INT;
    int k = 26;
    Vibrator localVibrator;
    AudioAttributes localAudioAttributes;
    if (j >= k)
    {
      localVibrator = b;
      localObject = VibrationEffect.createWaveform((long[])localObject, 0);
      localAudioAttributes = e();
      localVibrator.vibrate((VibrationEffect)localObject, localAudioAttributes);
      return;
    }
    j = Build.VERSION.SDK_INT;
    k = 21;
    if (j >= k)
    {
      localVibrator = b;
      localAudioAttributes = e();
      localVibrator.vibrate((long[])localObject, 0, localAudioAttributes);
      return;
    }
    b.vibrate((long[])localObject, 0);
  }
  
  public final void c()
  {
    Vibrator localVibrator = b;
    boolean bool = localVibrator.hasVibrator();
    if (!bool) {
      return;
    }
    b.cancel();
  }
  
  public final void d()
  {
    ToneGenerator localToneGenerator = a;
    if (localToneGenerator != null) {
      localToneGenerator.stopTone();
    }
    localToneGenerator = a;
    if (localToneGenerator != null) {
      localToneGenerator.release();
    }
    c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */