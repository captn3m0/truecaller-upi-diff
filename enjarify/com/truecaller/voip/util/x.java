package com.truecaller.voip.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.b;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.ai;
import com.truecaller.common.h.aq.d;
import com.truecaller.voip.R.dimen;
import java.io.IOException;

public final class x
  implements w
{
  private final Context a;
  
  public x(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Bitmap a(int paramInt)
  {
    Object localObject = a;
    Drawable localDrawable = b.a((Context)localObject, paramInt);
    if (localDrawable == null) {
      return null;
    }
    k.a(localDrawable, "ContextCompat.getDrawabl…awableRes) ?: return null");
    int i = localDrawable.getIntrinsicWidth();
    int j = localDrawable.getIntrinsicHeight();
    Bitmap.Config localConfig = Bitmap.Config.ARGB_8888;
    localObject = Bitmap.createBitmap(i, j, localConfig);
    Canvas localCanvas = new android/graphics/Canvas;
    localCanvas.<init>((Bitmap)localObject);
    int k = localCanvas.getWidth();
    int m = localCanvas.getHeight();
    localDrawable.setBounds(0, 0, k, m);
    localDrawable.draw(localCanvas);
    return (Bitmap)localObject;
  }
  
  public final Bitmap a(String paramString)
  {
    k.b(paramString, "url");
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    int i = ((CharSequence)localObject).length();
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    Bitmap localBitmap = null;
    if (i != 0) {
      return null;
    }
    try
    {
      localObject = a;
      localObject = com.d.b.w.a((Context)localObject);
      paramString = ((com.d.b.w)localObject).a(paramString);
      i = R.dimen.notification_avatar_image_size;
      paramString = paramString.a(i, i);
      localObject = aq.d.b();
      localObject = (ai)localObject;
      paramString = paramString.a((ai)localObject);
      localBitmap = paramString.d();
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    return localBitmap;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */