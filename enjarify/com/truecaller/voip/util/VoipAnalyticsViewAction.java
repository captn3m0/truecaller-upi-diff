package com.truecaller.voip.util;

public enum VoipAnalyticsViewAction
{
  private final String value;
  
  static
  {
    VoipAnalyticsViewAction[] arrayOfVoipAnalyticsViewAction = new VoipAnalyticsViewAction[1];
    VoipAnalyticsViewAction localVoipAnalyticsViewAction = new com/truecaller/voip/util/VoipAnalyticsViewAction;
    localVoipAnalyticsViewAction.<init>("VOIP_CALL", 0, "VoipCall");
    VOIP_CALL = localVoipAnalyticsViewAction;
    arrayOfVoipAnalyticsViewAction[0] = localVoipAnalyticsViewAction;
    $VALUES = arrayOfVoipAnalyticsViewAction;
  }
  
  private VoipAnalyticsViewAction(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipAnalyticsViewAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */