package com.truecaller.voip.util;

import c.g.a.a;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.ag;

public abstract interface o
{
  public abstract void a(VoipAnalyticsInCallUiAction paramVoipAnalyticsInCallUiAction);
  
  public abstract void a(n paramn, VoipAnalyticsState paramVoipAnalyticsState, VoipAnalyticsStateReason paramVoipAnalyticsStateReason);
  
  public abstract void a(String paramString, long paramLong);
  
  public abstract void a(String paramString, VoipAnalyticsFailedCallAction paramVoipAnalyticsFailedCallAction);
  
  public abstract void a(String paramString, VoipAnalyticsNotificationAction paramVoipAnalyticsNotificationAction);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(String paramString1, String paramString2, int paramInt, boolean paramBoolean);
  
  public abstract void a(ag paramag, VoipAnalyticsCallDirection paramVoipAnalyticsCallDirection, String paramString, a parama1, a parama2, u paramu1, u paramu2, u paramu3);
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */