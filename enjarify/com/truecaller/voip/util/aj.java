package com.truecaller.voip.util;

import dagger.a.d;
import javax.inject.Provider;

public final class aj
  implements d
{
  private final Provider a;
  
  private aj(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static aj a(Provider paramProvider)
  {
    aj localaj = new com/truecaller/voip/util/aj;
    localaj.<init>(paramProvider);
    return localaj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */