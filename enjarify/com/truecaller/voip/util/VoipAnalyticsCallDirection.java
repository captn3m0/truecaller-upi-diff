package com.truecaller.voip.util;

public enum VoipAnalyticsCallDirection
{
  private final String value;
  
  static
  {
    VoipAnalyticsCallDirection[] arrayOfVoipAnalyticsCallDirection = new VoipAnalyticsCallDirection[2];
    VoipAnalyticsCallDirection localVoipAnalyticsCallDirection = new com/truecaller/voip/util/VoipAnalyticsCallDirection;
    localVoipAnalyticsCallDirection.<init>("INCOMING", 0, "Incoming");
    INCOMING = localVoipAnalyticsCallDirection;
    arrayOfVoipAnalyticsCallDirection[0] = localVoipAnalyticsCallDirection;
    localVoipAnalyticsCallDirection = new com/truecaller/voip/util/VoipAnalyticsCallDirection;
    int i = 1;
    localVoipAnalyticsCallDirection.<init>("OUTGOING", i, "Outgoing");
    OUTGOING = localVoipAnalyticsCallDirection;
    arrayOfVoipAnalyticsCallDirection[i] = localVoipAnalyticsCallDirection;
    $VALUES = arrayOfVoipAnalyticsCallDirection;
  }
  
  private VoipAnalyticsCallDirection(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipAnalyticsCallDirection
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */