package com.truecaller.voip.util;

import c.d.c;
import c.g.a.b;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.api.VoipNumberDto;
import com.truecaller.voip.db.VoipIdCache;
import kotlinx.coroutines.ag;

final class ad$e
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  ad$e(ad paramad, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/voip/util/ad$e;
    ad localad = b;
    String str = c;
    locale.<init>(localad, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        int j = 1;
        localObject1 = new String[j];
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("Fetching number for voip id:");
        String str1 = c;
        ((StringBuilder)localObject2).append(str1);
        localObject2 = ((StringBuilder)localObject2).toString();
        str1 = null;
        localObject1[0] = localObject2;
        localObject1 = b.c;
        localObject2 = new com/truecaller/voip/util/ad$e$a;
        ((ad.e.a)localObject2).<init>(this);
        localObject2 = (b)localObject2;
        localObject1 = (VoipIdCache)ad.a((com.truecaller.voip.db.a)localObject1, (b)localObject2);
        localObject2 = null;
        if (localObject1 != null)
        {
          localObject3 = b;
          localObject1 = ad.a((ad)localObject3, (VoipIdCache)localObject1);
        }
        else
        {
          bool = false;
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          paramObject = new String[j];
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("Returning cached number:");
          localObject3 = ((VoipIdCache)localObject1).getNumber();
          ((StringBuilder)localObject2).append((String)localObject3);
          localObject2 = ((StringBuilder)localObject2).toString();
          paramObject[0] = localObject2;
          return ((VoipIdCache)localObject1).getNumber();
        }
        localObject1 = b.b;
        Object localObject3 = c;
        localObject1 = (VoipNumberDto)ad.a(((com.truecaller.voip.api.a)localObject1).b((String)localObject3));
        VoipIdCache localVoipIdCache;
        if (localObject1 != null)
        {
          localObject3 = b;
          String str2 = c;
          localVoipIdCache = new com/truecaller/voip/db/VoipIdCache;
          Object localObject4 = new java/lang/StringBuilder;
          String str3 = "+";
          ((StringBuilder)localObject4).<init>(str3);
          long l = ((VoipNumberDto)localObject1).getPhone();
          ((StringBuilder)localObject4).append(l);
          localObject4 = ((StringBuilder)localObject4).toString();
          l = ((VoipNumberDto)localObject1).getExpiryEpochSeconds();
          localVoipIdCache.<init>(str2, (String)localObject4, l);
          localObject1 = c;
          localObject3 = new com/truecaller/voip/util/ad$b;
          ((ad.b)localObject3).<init>(localVoipIdCache);
          localObject3 = (b)localObject3;
          ad.a((com.truecaller.voip.db.a)localObject1, (b)localObject3);
        }
        else
        {
          localVoipIdCache = null;
        }
        paramObject = new String[j];
        localObject3 = String.valueOf(localVoipIdCache);
        localObject1 = "Fetched number is ".concat((String)localObject3);
        paramObject[0] = localObject1;
        if (localVoipIdCache != null) {
          return localVoipIdCache.getNumber();
        }
        return null;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ad.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */