package com.truecaller.voip;

import android.content.Context;
import com.truecaller.tcpermissions.l;
import com.truecaller.voip.callconnection.VoipCallConnectionService;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.ui.VoipInAppNotificationView;
import com.truecaller.voip.incoming.IncomingVoipService;
import com.truecaller.voip.incoming.b.c;
import com.truecaller.voip.incoming.blocked.VoipBlockedCallsWorker;
import com.truecaller.voip.incoming.blocked.b.b;
import com.truecaller.voip.incoming.missed.MissedVoipCallMessageBroadcast;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker;
import com.truecaller.voip.incoming.missed.c.c;
import com.truecaller.voip.incoming.ui.b.a;
import com.truecaller.voip.manager.rtm.i;
import com.truecaller.voip.util.ae;
import com.truecaller.voip.util.af;
import com.truecaller.voip.util.ai;
import com.truecaller.voip.util.aj;
import com.truecaller.voip.util.ak;
import com.truecaller.voip.util.am;
import com.truecaller.voip.util.an;
import com.truecaller.voip.util.ao;
import com.truecaller.voip.util.ap;
import com.truecaller.voip.util.aq;
import com.truecaller.voip.util.as;
import com.truecaller.voip.util.at;
import com.truecaller.voip.util.au;
import com.truecaller.voip.util.av;
import com.truecaller.voip.util.o;
import com.truecaller.voip.util.p;
import com.truecaller.voip.util.r;
import javax.inject.Provider;

public final class a
  implements c
{
  private Provider A;
  private Provider B;
  private Provider C;
  private Provider D;
  private Provider E;
  private Provider F;
  private Provider G;
  private Provider H;
  private Provider I;
  private Provider J;
  private Provider K;
  private Provider L;
  private Provider M;
  private Provider N;
  private final com.truecaller.common.a b;
  private final com.truecaller.utils.t c;
  private final com.truecaller.analytics.d d;
  private final com.truecaller.notificationchannels.n e;
  private final com.truecaller.tcpermissions.e f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  private Provider w;
  private Provider x;
  private Provider y;
  private Provider z;
  
  private a(com.truecaller.common.a parama, com.truecaller.utils.t paramt, com.truecaller.notificationchannels.n paramn, com.truecaller.tcpermissions.e parame, com.truecaller.analytics.d paramd)
  {
    b = parama;
    c = paramt;
    d = paramd;
    Object localObject5 = paramn;
    e = paramn;
    f = parame;
    localObject5 = new com/truecaller/voip/a$i;
    ((a.i)localObject5).<init>(parama);
    g = ((Provider)localObject5);
    localObject5 = new com/truecaller/voip/a$f;
    ((a.f)localObject5).<init>(parama);
    h = ((Provider)localObject5);
    localObject5 = new com/truecaller/voip/a$d;
    ((a.d)localObject5).<init>(parama);
    i = ((Provider)localObject5);
    localObject5 = new com/truecaller/voip/a$j;
    ((a.j)localObject5).<init>(parame);
    j = ((Provider)localObject5);
    localObject5 = new com/truecaller/voip/a$k;
    ((a.k)localObject5).<init>(parame);
    k = ((Provider)localObject5);
    localObject3 = new com/truecaller/voip/a$e;
    ((a.e)localObject3).<init>(parama);
    l = ((Provider)localObject3);
    localObject3 = new com/truecaller/voip/a$h;
    ((a.h)localObject3).<init>(parama);
    m = ((Provider)localObject3);
    localObject3 = new com/truecaller/voip/a$m;
    ((a.m)localObject3).<init>(paramt);
    n = ((Provider)localObject3);
    localObject3 = l;
    localObject5 = m;
    Object localObject6 = n;
    localObject3 = m.a((Provider)localObject3, (Provider)localObject5, (Provider)localObject6);
    o = ((Provider)localObject3);
    localObject3 = dagger.a.c.a(o);
    p = ((Provider)localObject3);
    localObject3 = aj.a(i);
    q = ((Provider)localObject3);
    localObject3 = ab.a(i);
    r = ((Provider)localObject3);
    localObject3 = new com/truecaller/voip/a$l;
    ((a.l)localObject3).<init>(paramt);
    s = ((Provider)localObject3);
    localObject2 = h;
    localObject3 = m;
    localObject5 = ad.a();
    localObject6 = r;
    Provider localProvider1 = s;
    localObject2 = ae.a((Provider)localObject2, (Provider)localObject3, (Provider)localObject5, (Provider)localObject6, localProvider1);
    t = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(t);
    u = ((Provider)localObject2);
    localObject2 = h;
    localObject3 = p;
    localObject5 = q;
    localObject6 = z.a();
    localProvider1 = u;
    localObject2 = com.truecaller.voip.manager.j.a((Provider)localObject2, (Provider)localObject3, (Provider)localObject5, (Provider)localObject6, localProvider1);
    v = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(v);
    w = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(w.a());
    x = ((Provider)localObject2);
    localObject2 = new com/truecaller/voip/a$b;
    ((a.b)localObject2).<init>(paramd);
    y = ((Provider)localObject2);
    localObject2 = new com/truecaller/voip/a$c;
    ((a.c)localObject2).<init>(paramd);
    z = ((Provider)localObject2);
    localObject2 = h;
    localObject3 = y;
    localObject4 = z;
    localObject5 = v.a();
    localObject2 = r.a((Provider)localObject2, (Provider)localObject3, (Provider)localObject4, (Provider)localObject5);
    A = ((Provider)localObject2);
    localObject3 = g;
    localObject4 = h;
    localObject5 = l;
    localObject6 = i;
    localProvider1 = x;
    Object localObject7 = ad.a();
    Provider localProvider2 = A;
    localObject2 = dagger.a.c.a(com.truecaller.voip.manager.rtm.e.a((Provider)localObject3, (Provider)localObject4, (Provider)localObject5, (Provider)localObject6, localProvider1, (Provider)localObject7, localProvider2));
    B = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(u.a(B));
    C = ((Provider)localObject2);
    localObject2 = h;
    localObject3 = ad.a();
    localObject2 = as.a((Provider)localObject2, (Provider)localObject3);
    D = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(D);
    E = ((Provider)localObject2);
    localObject3 = h;
    localObject4 = C;
    localObject5 = u;
    localObject6 = E;
    localProvider1 = B;
    localObject7 = A;
    localObject2 = com.truecaller.voip.manager.rtm.b.a((Provider)localObject3, (Provider)localObject4, (Provider)localObject5, (Provider)localObject6, localProvider1, (Provider)localObject7);
    F = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(F);
    G = ((Provider)localObject2);
    localObject2 = g;
    localObject3 = i;
    localObject2 = com.truecaller.voip.callconnection.e.a((Provider)localObject2, (Provider)localObject3);
    H = ((Provider)localObject2);
    localObject2 = new com/truecaller/voip/a$g;
    ((a.g)localObject2).<init>(parama);
    I = ((Provider)localObject2);
    localObject1 = p;
    localObject2 = H;
    localObject3 = I;
    localObject1 = com.truecaller.voip.callconnection.h.a((Provider)localObject1, (Provider)localObject2, (Provider)localObject3);
    J = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(J);
    K = ((Provider)localObject1);
    localObject1 = l;
    localObject2 = n;
    localObject3 = i;
    localObject4 = p;
    localObject5 = K;
    localObject1 = ap.a((Provider)localObject1, (Provider)localObject2, (Provider)localObject3, (Provider)localObject4, (Provider)localObject5);
    L = ((Provider)localObject1);
    localObject2 = g;
    localObject3 = h;
    localObject4 = i;
    localObject5 = j;
    localObject6 = k;
    localProvider1 = p;
    localObject7 = w;
    localProvider2 = G;
    Provider localProvider3 = B;
    x localx = x.a();
    Provider localProvider4 = r;
    Provider localProvider5 = E;
    Provider localProvider6 = A;
    Provider localProvider7 = u;
    localObject1 = L;
    localObject1 = q.a((Provider)localObject2, (Provider)localObject3, (Provider)localObject4, (Provider)localObject5, (Provider)localObject6, localProvider1, (Provider)localObject7, localProvider2, localProvider3, localx, localProvider4, localProvider5, localProvider6, localProvider7, (Provider)localObject1);
    M = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(M);
    N = ((Provider)localObject1);
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/voip/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  private p f()
  {
    p localp = new com/truecaller/voip/util/p;
    c.d.f localf = (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.analytics.b localb = (com.truecaller.analytics.b)dagger.a.g.a(d.c(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.androidactors.f localf1 = (com.truecaller.androidactors.f)dagger.a.g.a(d.f(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.voip.util.aa localaa = v.b();
    localp.<init>(localf, localb, localf1, localaa);
    return localp;
  }
  
  private av g()
  {
    av localav = new com/truecaller/voip/util/av;
    com.truecaller.voip.util.ac localac = (com.truecaller.voip.util.ac)u.get();
    com.truecaller.voip.util.aa localaa = v.b();
    localav.<init>(localac, localaa);
    return localav;
  }
  
  private com.truecaller.voip.util.x h()
  {
    com.truecaller.voip.util.x localx = new com/truecaller/voip/util/x;
    Context localContext = (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    localx.<init>(localContext);
    return localx;
  }
  
  private ao i()
  {
    ao localao = new com/truecaller/voip/util/ao;
    Object localObject1 = dagger.a.g.a(b.v(), "Cannot return null from a non-@Nullable component method");
    Object localObject2 = localObject1;
    localObject2 = (com.truecaller.featuretoggles.e)localObject1;
    localObject1 = dagger.a.g.a(c.c(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject1;
    localObject3 = (com.truecaller.utils.d)localObject1;
    localObject1 = dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    Object localObject4 = localObject1;
    localObject4 = (Context)localObject1;
    localObject1 = p.get();
    Object localObject5 = localObject1;
    localObject5 = (k)localObject1;
    localObject1 = K.get();
    Object localObject6 = localObject1;
    localObject6 = (com.truecaller.voip.callconnection.f)localObject1;
    localObject1 = localao;
    localao.<init>((com.truecaller.featuretoggles.e)localObject2, (com.truecaller.utils.d)localObject3, (Context)localObject4, (k)localObject5, (com.truecaller.voip.callconnection.f)localObject6);
    return localao;
  }
  
  private com.truecaller.voip.util.g j()
  {
    com.truecaller.voip.util.g localg = new com/truecaller/voip/util/g;
    Context localContext = (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    localg.<init>(localContext);
    return localg;
  }
  
  private com.truecaller.voip.util.t k()
  {
    com.truecaller.voip.util.t localt = new com/truecaller/voip/util/t;
    Context localContext = (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.utils.d locald = (com.truecaller.utils.d)dagger.a.g.a(c.c(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.voip.callconnection.f localf = (com.truecaller.voip.callconnection.f)K.get();
    localt.<init>(localContext, locald, localf);
    return localt;
  }
  
  public final void a(VoipCallConnectionService paramVoipCallConnectionService)
  {
    com.truecaller.voip.callconnection.f localf = (com.truecaller.voip.callconnection.f)K.get();
    a = localf;
  }
  
  public final void a(VoipService paramVoipService)
  {
    Object localObject1 = paramVoipService;
    Object localObject2 = (c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method");
    a = ((c.d.f)localObject2);
    localObject2 = (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method");
    b = ((c.d.f)localObject2);
    com.truecaller.voip.incall.c localc = new com/truecaller/voip/incall/c;
    c.d.f localf1 = (c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method");
    c.d.f localf2 = (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = new com/truecaller/voip/manager/a;
    Object localObject4 = localObject3;
    Object localObject5 = (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method");
    Object localObject7 = (aq)E.get();
    ((com.truecaller.voip.manager.a)localObject3).<init>((Context)localObject5, (c.d.f)localObject6, (aq)localObject7);
    localObject3 = (i)B.get();
    localObject5 = v.b();
    localObject6 = g();
    localObject7 = (com.truecaller.notificationchannels.b)dagger.a.g.a(e.d(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.utils.a locala = (com.truecaller.utils.a)dagger.a.g.a(c.d(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.voip.util.x localx = h();
    com.truecaller.voip.util.y localy = x.b();
    Object localObject8 = new com/truecaller/voip/util/k;
    Object localObject9 = localObject8;
    localObject1 = (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    localObject2 = i();
    ((com.truecaller.voip.util.k)localObject8).<init>((Context)localObject1, (an)localObject2);
    localObject8 = j();
    localObject1 = dagger.a.g.a(c.g(), "Cannot return null from a non-@Nullable component method");
    Object localObject10 = localObject1;
    localObject10 = (com.truecaller.utils.n)localObject1;
    com.truecaller.voip.util.t localt = k();
    localObject1 = G.get();
    Object localObject11 = localObject1;
    localObject11 = (com.truecaller.voip.manager.rtm.h)localObject1;
    p localp = f();
    ao localao = i();
    localc.<init>(localf1, localf2, (com.truecaller.voip.manager.g)localObject4, (i)localObject3, (com.truecaller.voip.util.aa)localObject5, (au)localObject6, (com.truecaller.notificationchannels.b)localObject7, locala, localx, localy, (com.truecaller.voip.util.j)localObject9, (com.truecaller.voip.util.f)localObject8, (com.truecaller.utils.n)localObject10, localt, (com.truecaller.voip.manager.rtm.h)localObject11, localp, localao);
    localObject2 = localc;
    localObject1 = paramVoipService;
    c = localc;
    localObject2 = h();
    d = ((com.truecaller.voip.util.w)localObject2);
  }
  
  public final void a(VoipInAppNotificationView paramVoipInAppNotificationView)
  {
    com.truecaller.voip.incall.ui.d locald = new com/truecaller/voip/incall/ui/d;
    c.d.f localf = (c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.utils.a locala = (com.truecaller.utils.a)dagger.a.g.a(c.d(), "Cannot return null from a non-@Nullable component method");
    locald.<init>(localf, locala);
    l = locald;
  }
  
  public final void a(com.truecaller.voip.incall.ui.a parama)
  {
    com.truecaller.voip.incall.ui.g localg = new com/truecaller/voip/incall/ui/g;
    c.d.f localf = (c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.utils.a locala = (com.truecaller.utils.a)dagger.a.g.a(c.d(), "Cannot return null from a non-@Nullable component method");
    at localat = aa.a();
    p localp = f();
    localg.<init>(localf, locala, localat, localp);
    a = localg;
  }
  
  public final void a(IncomingVoipService paramIncomingVoipService)
  {
    Object localObject1 = (c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method");
    a = ((c.d.f)localObject1);
    localObject1 = (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method");
    b = ((c.d.f)localObject1);
    localObject1 = new com/truecaller/voip/incoming/c;
    Object localObject2 = dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject2;
    localObject3 = (c.d.f)localObject2;
    localObject2 = dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method");
    Object localObject4 = localObject2;
    localObject4 = (c.d.f)localObject2;
    localObject2 = B.get();
    Object localObject5 = localObject2;
    localObject5 = (i)localObject2;
    av localav = g();
    localObject2 = dagger.a.g.a(e.d(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject2;
    localObject6 = (com.truecaller.notificationchannels.b)localObject2;
    com.truecaller.voip.util.x localx = h();
    com.truecaller.voip.util.y localy = x.b();
    localObject2 = dagger.a.g.a(c.g(), "Cannot return null from a non-@Nullable component method");
    Object localObject7 = localObject2;
    localObject7 = (com.truecaller.utils.n)localObject2;
    com.truecaller.voip.util.t localt = k();
    localObject2 = G.get();
    Object localObject8 = localObject2;
    localObject8 = (com.truecaller.voip.manager.rtm.h)localObject2;
    com.truecaller.voip.util.g localg = j();
    p localp = f();
    ao localao = i();
    localObject2 = localObject1;
    ((com.truecaller.voip.incoming.c)localObject1).<init>((c.d.f)localObject3, (c.d.f)localObject4, (i)localObject5, localav, (com.truecaller.notificationchannels.b)localObject6, localx, localy, (com.truecaller.utils.n)localObject7, localt, (com.truecaller.voip.manager.rtm.h)localObject8, localg, localp, localao);
    c = ((b.c)localObject1);
    localObject1 = h();
    d = ((com.truecaller.voip.util.w)localObject1);
  }
  
  public final void a(VoipBlockedCallsWorker paramVoipBlockedCallsWorker)
  {
    Object localObject1 = new com/truecaller/voip/incoming/blocked/c;
    com.truecaller.voip.incoming.blocked.d locald = new com/truecaller/voip/incoming/blocked/d;
    Object localObject2 = (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    locald.<init>((Context)localObject2);
    localObject2 = (c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method");
    c.d.f localf = (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method");
    ((com.truecaller.voip.incoming.blocked.c)localObject1).<init>(locald, (c.d.f)localObject2, localf);
    b = ((b.b)localObject1);
    localObject1 = (com.truecaller.notificationchannels.b)dagger.a.g.a(e.d(), "Cannot return null from a non-@Nullable component method");
    c = ((com.truecaller.notificationchannels.b)localObject1);
    localObject1 = y.a();
    d = ((af)localObject1);
  }
  
  public final void a(MissedVoipCallMessageBroadcast paramMissedVoipCallMessageBroadcast)
  {
    com.truecaller.voip.util.z localz = ac.a();
    a = localz;
  }
  
  public final void a(MissedVoipCallsWorker paramMissedVoipCallsWorker)
  {
    Object localObject1 = new com/truecaller/voip/incoming/missed/d;
    c.d.f localf1 = (c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method");
    c.d.f localf2 = (c.d.f)dagger.a.g.a(b.s(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.voip.incoming.missed.b localb = new com/truecaller/voip/incoming/missed/b;
    Object localObject2 = (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    localb.<init>((Context)localObject2);
    localObject2 = h();
    ((com.truecaller.voip.incoming.missed.d)localObject1).<init>(localf1, localf2, localb, (com.truecaller.voip.util.w)localObject2);
    b = ((c.c)localObject1);
    localObject1 = (com.truecaller.notificationchannels.b)dagger.a.g.a(e.d(), "Cannot return null from a non-@Nullable component method");
    c = ((com.truecaller.notificationchannels.b)localObject1);
    localObject1 = y.a();
    d = ((af)localObject1);
  }
  
  public final void a(com.truecaller.voip.incoming.ui.a parama)
  {
    Object localObject = new com/truecaller/voip/incoming/ui/c;
    c.d.f localf = (c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.voip.util.z localz = ac.a();
    at localat = aa.a();
    p localp = f();
    ((com.truecaller.voip.incoming.ui.c)localObject).<init>(localf, localz, localat, localp);
    a = ((b.a)localObject);
    localObject = (l)dagger.a.g.a(f.c(), "Cannot return null from a non-@Nullable component method");
    b = ((l)localObject);
  }
  
  public final d b()
  {
    return (d)N.get();
  }
  
  public final com.truecaller.voip.db.c c()
  {
    com.truecaller.voip.db.d locald = new com/truecaller/voip/db/d;
    Context localContext = (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    locald.<init>(localContext);
    return locald;
  }
  
  public final ak d()
  {
    am localam = new com/truecaller/voip/util/am;
    ai localai = new com/truecaller/voip/util/ai;
    Object localObject = (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    localai.<init>((Context)localObject);
    localObject = (k)p.get();
    localam.<init>(localai, (k)localObject);
    return localam;
  }
  
  public final o e()
  {
    return f();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */