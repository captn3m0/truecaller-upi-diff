package com.truecaller.voip;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class VoipUser$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    VoipUser localVoipUser = new com/truecaller/voip/VoipUser;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    int i = paramParcel.readInt();
    boolean bool;
    if (i != 0)
    {
      i = 1;
      bool = true;
    }
    else
    {
      i = 0;
      localObject1 = null;
      bool = false;
    }
    i = paramParcel.readInt();
    Object localObject2 = null;
    if (i != 0)
    {
      i = paramParcel.readInt();
      localObject1 = Integer.valueOf(i);
      localObject3 = localObject1;
    }
    else
    {
      localObject3 = null;
    }
    i = paramParcel.readInt();
    int j;
    if (i != 0)
    {
      j = paramParcel.readInt();
      paramParcel = Integer.valueOf(j);
    }
    else
    {
      j = 0;
      paramParcel = null;
    }
    Object localObject1 = localVoipUser;
    localObject2 = localObject3;
    Object localObject3 = paramParcel;
    localVoipUser.<init>(str1, str2, str3, str4, bool, (Integer)localObject2, paramParcel);
    return localVoipUser;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new VoipUser[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.VoipUser.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */