package com.truecaller.voip;

import c.d.a.a;
import c.d.c;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.voip.db.VoipAvailability;
import com.truecaller.voip.util.VoipAnalyticsFailedCallAction;
import com.truecaller.voip.util.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class aj$e
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  int c;
  private ag h;
  
  aj$e(aj paramaj, Contact paramContact, String paramString, android.support.v4.app.f paramf, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/voip/aj$e;
    aj localaj = d;
    Contact localContact = e;
    String str = f;
    android.support.v4.app.f localf = g;
    locale.<init>(localaj, localContact, str, localf, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = c;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label640;
      }
      paramObject = d;
      Object localObject2 = e.A();
      c.g.b.k.a(localObject2, "contact.numbers");
      localObject2 = (Iterable)localObject2;
      Object localObject3 = new java/util/ArrayList;
      int j = c.a.m.a((Iterable)localObject2, 10);
      ((ArrayList)localObject3).<init>(j);
      localObject3 = (Collection)localObject3;
      localObject2 = ((Iterable)localObject2).iterator();
      for (;;)
      {
        k = ((Iterator)localObject2).hasNext();
        if (k == 0) {
          break;
        }
        Object localObject4 = (Number)((Iterator)localObject2).next();
        localObject5 = "it";
        c.g.b.k.a(localObject4, (String)localObject5);
        localObject4 = ((Number)localObject4).a();
        ((Collection)localObject3).add(localObject4);
      }
      localObject3 = (List)localObject3;
      paramObject = aj.b((aj)paramObject, (List)localObject3);
      localObject2 = paramObject;
      localObject2 = (Collection)paramObject;
      localObject3 = null;
      int k = 1;
      if (localObject2 != null)
      {
        bool1 = ((Collection)localObject2).isEmpty();
        if (!bool1)
        {
          bool1 = false;
          localObject2 = null;
          break label256;
        }
      }
      bool1 = true;
      label256:
      if (bool1)
      {
        paramObject = d;
        localObject1 = f;
        localObject2 = e;
        ((aj)paramObject).a((String)localObject1, (Contact)localObject2);
        paramObject = d.c;
        localObject1 = f;
        localObject2 = VoipAnalyticsFailedCallAction.CALLEE_NOT_CAPABLE;
        ((o)paramObject).a((String)localObject1, (VoipAnalyticsFailedCallAction)localObject2);
        return x.a;
      }
      localObject2 = e.A();
      c.g.b.k.a(localObject2, "contact.numbers");
      localObject2 = (Iterable)localObject2;
      Object localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>();
      localObject5 = (Collection)localObject5;
      localObject2 = ((Iterable)localObject2).iterator();
      for (;;)
      {
        bool3 = ((Iterator)localObject2).hasNext();
        if (!bool3) {
          break;
        }
        localObject6 = ((Iterator)localObject2).next();
        Object localObject7 = localObject6;
        localObject7 = (Number)localObject6;
        Object localObject8 = paramObject;
        localObject8 = (Iterable)paramObject;
        boolean bool4 = localObject8 instanceof Collection;
        Object localObject9;
        if (bool4)
        {
          localObject9 = localObject8;
          localObject9 = (Collection)localObject8;
          bool4 = ((Collection)localObject9).isEmpty();
          if (bool4) {}
        }
        else
        {
          localObject8 = ((Iterable)localObject8).iterator();
          do
          {
            bool4 = ((Iterator)localObject8).hasNext();
            if (!bool4) {
              break;
            }
            localObject9 = ((VoipAvailability)((Iterator)localObject8).next()).getPhone();
            c.g.b.k.a(localObject7, "number");
            String str1 = ((Number)localObject7).a();
            String str2 = "number.normalizedNumber";
            c.g.b.k.a(str1, str2);
            str1 = al.a(str1);
            bool4 = c.g.b.k.a(localObject9, str1);
          } while (!bool4);
          m = 1;
          break label542;
        }
        int m = 0;
        localObject7 = null;
        label542:
        if (m != 0) {
          ((Collection)localObject5).add(localObject6);
        }
      }
      localObject5 = (List)localObject5;
      localObject2 = d.a;
      localObject3 = new com/truecaller/voip/aj$e$1;
      boolean bool3 = false;
      Object localObject6 = null;
      ((aj.e.1)localObject3).<init>(this, (List)localObject5, null);
      localObject3 = (c.g.a.m)localObject3;
      a = paramObject;
      b = localObject5;
      c = k;
      paramObject = g.a((c.d.f)localObject2, (c.g.a.m)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    return x.a;
    label640:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.aj.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */