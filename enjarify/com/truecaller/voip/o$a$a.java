package com.truecaller.voip;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.manager.rtm.RtmMsg;
import com.truecaller.voip.manager.rtm.RtmMsgAction;
import com.truecaller.voip.manager.rtm.i;
import kotlinx.coroutines.a.l;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.ag;

final class o$a$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag f;
  
  o$a$a(o.a parama, String paramString, u paramu, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/voip/o$a$a;
    o.a locala1 = c;
    String str = d;
    u localu = e;
    locala.<init>(locala1, str, localu, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = b;
    boolean bool2;
    Object localObject3;
    boolean bool1;
    Object localObject4;
    String str;
    int k;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      localObject2 = (l)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = this;
        break label356;
      }
      throw a;
    case 2: 
      localObject2 = (l)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject3 = localObject1;
        localObject1 = this;
        break label295;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label423;
      }
      paramObject = (i)o.h(c.f).get();
      localObject2 = d;
      localObject3 = new com/truecaller/voip/manager/rtm/RtmMsg;
      localObject4 = RtmMsgAction.ONLINE;
      str = "";
      ((RtmMsg)localObject3).<init>((RtmMsgAction)localObject4, str);
      k = 1;
      b = k;
      paramObject = ((i)paramObject).a((String)localObject2, (RtmMsg)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = e.f();
    Object localObject2 = localObject1;
    localObject1 = this;
    for (;;)
    {
      a = paramObject;
      int j = 2;
      b = j;
      localObject3 = ((l)paramObject).a((c)localObject1);
      if (localObject3 == localObject2) {
        return localObject2;
      }
      Object localObject5 = localObject2;
      localObject2 = paramObject;
      paramObject = localObject3;
      localObject3 = localObject5;
      label295:
      paramObject = (Boolean)paramObject;
      boolean bool3 = ((Boolean)paramObject).booleanValue();
      if (!bool3) {
        break;
      }
      a = localObject2;
      int m = 3;
      b = m;
      paramObject = ((l)localObject2).b((c)localObject1);
      if (paramObject == localObject3) {
        return localObject3;
      }
      localObject5 = localObject3;
      localObject3 = localObject2;
      localObject2 = localObject5;
      label356:
      paramObject = (RtmMsg)paramObject;
      localObject4 = ((RtmMsg)paramObject).getSenderId();
      str = c.g.c;
      k = c.g.b.k.a(localObject4, str);
      if (k != 0)
      {
        paramObject = ((RtmMsg)paramObject).getAction();
        localObject4 = RtmMsgAction.INVITE;
        if (paramObject == localObject4) {
          return Boolean.TRUE;
        }
      }
      paramObject = localObject3;
    }
    return Boolean.FALSE;
    label423:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.o.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */