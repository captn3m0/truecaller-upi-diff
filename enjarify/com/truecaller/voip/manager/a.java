package com.truecaller.voip.manager;

import android.content.Context;
import c.g.a.m;
import c.g.b.k;
import c.o.b;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.j;
import com.truecaller.voip.R.string;
import com.truecaller.voip.api.RtcTokenDto;
import com.truecaller.voip.util.aq;
import io.agora.rtc.Constants.AudioProfile;
import io.agora.rtc.Constants.AudioScenario;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.a.i;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class a
  implements g, ag
{
  final h a;
  final RtcEngine b;
  e c;
  final aq d;
  private final a.d e;
  private final c.d.f f;
  
  public a(Context paramContext, c.d.f paramf, aq paramaq)
  {
    f = paramf;
    d = paramaq;
    int i = 10;
    paramf = i.a(i);
    a = paramf;
    paramf = new com/truecaller/voip/manager/a$d;
    paramf.<init>(this);
    e = paramf;
    try
    {
      i = R.string.voip_agora_app_id;
      paramf = paramContext.getString(i);
      paramaq = e;
      paramaq = (IRtcEngineEventHandler)paramaq;
      paramContext = RtcEngine.create(paramContext, paramf, paramaq);
      i = 0;
      paramf = null;
      paramContext.setDefaultAudioRoutetoSpeakerphone(false);
      paramContext.setChannelProfile(0);
      paramf = Constants.AudioProfile.SPEECH_STANDARD;
      i = Constants.AudioProfile.getValue(paramf);
      paramaq = Constants.AudioScenario.DEFAULT;
      int j = Constants.AudioScenario.getValue(paramaq);
      paramContext.setAudioProfile(i, j);
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localException);
      paramContext = null;
    }
    b = paramContext;
  }
  
  public final c.d.f V_()
  {
    return f;
  }
  
  public final e a()
  {
    return c;
  }
  
  public final Object a(String paramString, c.d.c paramc)
  {
    boolean bool1 = paramc instanceof a.a;
    int k;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (a.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        k = b - j;
        b = k;
        break label77;
      }
    }
    Object localObject1 = new com/truecaller/voip/manager/a$a;
    ((a.a)localObject1).<init>(this, (c.d.c)paramc);
    label77:
    paramc = a;
    Object localObject2 = c.d.a.a.a;
    int j = b;
    int m = 1;
    Object localObject3;
    Object localObject4;
    switch (j)
    {
    default: 
      paramString = new java/lang/IllegalStateException;
      paramString.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramString;
    case 2: 
      paramString = (u)g;
      localObject2 = (String)e;
      localObject1 = (a)d;
      bool2 = paramc instanceof o.b;
      if (!bool2)
      {
        localObject3 = paramString;
        paramString = (String)localObject2;
        break label595;
      }
      try
      {
        paramc = (o.b)paramc;
        paramc = a;
        throw paramc;
      }
      finally
      {
        localObject3 = paramString;
        break label681;
      }
    case 1: 
      paramString = (String)e;
      localObject4 = (a)d;
      boolean bool4 = paramc instanceof o.b;
      if (!bool4) {
        break label332;
      }
      throw a;
    }
    boolean bool2 = paramc instanceof o.b;
    if (!bool2)
    {
      paramc = new String[m];
      localObject3 = String.valueOf(paramString);
      localObject4 = "Joining channel ".concat((String)localObject3);
      paramc[0] = localObject4;
      paramc = d;
      d = this;
      e = paramString;
      b = m;
      paramc = paramc.a(paramString, (c.d.c)localObject1);
      if (paramc == localObject2) {
        return localObject2;
      }
      localObject4 = this;
      label332:
      paramc = (RtcTokenDto)paramc;
      if (paramc == null)
      {
        paramc = new String[m];
        paramString = String.valueOf(paramString);
        paramString = "Cannot fetch rtc token for channel:".concat(paramString);
        paramc[0] = paramString;
        paramString = new com/truecaller/voip/manager/d;
        paramc = FailedChannelJoinReason.GET_TOKEN_FAILED;
        paramString.<init>(paramc);
        return paramString;
      }
      localObject3 = a.a();
      Object localObject5 = b;
      int i1;
      if (localObject5 != null)
      {
        String str = paramc.getToken();
        int n = paramc.getUid();
        i1 = ((RtcEngine)localObject5).joinChannel(str, paramString, null, n);
        localObject5 = Integer.valueOf(i1);
      }
      else
      {
        i1 = 0;
        localObject5 = null;
      }
      if (localObject5 != null)
      {
        int i2 = ((Integer)localObject5).intValue();
        if (i2 == 0) {}
      }
      else
      {
        paramc = new String[m];
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("Cannot join to channel ");
        ((StringBuilder)localObject1).append(paramString);
        ((StringBuilder)localObject1).append(", returning false. Return code ");
        ((StringBuilder)localObject1).append(localObject5);
        paramString = ((StringBuilder)localObject1).toString();
        paramc[0] = paramString;
        paramString = new com/truecaller/voip/manager/d;
        paramc = FailedChannelJoinReason.RTC_JOIN_FAILED;
        paramString.<init>(paramc);
        return paramString;
      }
      try
      {
        d = localObject4;
        e = paramString;
        f = paramc;
        g = localObject3;
        h = localObject5;
        k = 2;
        b = k;
        paramc = ((a)localObject4).a((u)localObject3, (c.d.c)localObject1);
        if (paramc == localObject2) {
          return localObject2;
        }
        localObject1 = localObject4;
        label595:
        paramc = (Boolean)paramc;
        boolean bool3 = paramc.booleanValue();
        if (bool3)
        {
          paramc = new com/truecaller/voip/manager/a$b;
          paramc.<init>((a)localObject1, paramString, null);
          paramc = (m)paramc;
          int i3 = 3;
          kotlinx.coroutines.e.b((ag)localObject1, null, paramc, i3);
          paramString = f.a;
          paramString = (c)paramString;
        }
        else
        {
          paramString = new com/truecaller/voip/manager/d;
          paramc = FailedChannelJoinReason.RTC_JOIN_FAILED;
          paramString.<init>(paramc);
          paramString = (c)paramString;
        }
        ((u)localObject3).n();
        return paramString;
      }
      finally {}
      label681:
      ((u)localObject3).n();
      throw paramc;
    }
    throw a;
  }
  
  public final void a(ag paramag, m paramm)
  {
    k.b(paramag, "scope");
    k.b(paramm, "block");
    h localh = a;
    j.a(paramag, localh, paramm);
  }
  
  public final void a(boolean paramBoolean)
  {
    RtcEngine localRtcEngine = b;
    if (localRtcEngine != null)
    {
      localRtcEngine.setEnableSpeakerphone(paramBoolean);
      return;
    }
  }
  
  public final void b()
  {
    RtcEngine localRtcEngine = b;
    if (localRtcEngine != null)
    {
      localRtcEngine.leaveChannel();
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    RtcEngine localRtcEngine = b;
    if (localRtcEngine != null)
    {
      localRtcEngine.muteLocalAudioStream(paramBoolean);
      return;
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localRtcEngine = b;
      if (localRtcEngine != null) {
        localRtcEngine.enableAudio();
      }
      return;
    }
    RtcEngine localRtcEngine = b;
    if (localRtcEngine != null)
    {
      localRtcEngine.disableAudio();
      return;
    }
  }
  
  public final void d()
  {
    new String[1][0] = "Leaving channel and stopping manager";
    Object localObject1 = b;
    if (localObject1 != null) {
      ((RtcEngine)localObject1).leaveChannel();
    }
    localObject1 = (ag)bg.a;
    c.d.f localf = f;
    Object localObject2 = new com/truecaller/voip/manager/a$e;
    ((a.e)localObject2).<init>(null);
    localObject2 = (m)localObject2;
    kotlinx.coroutines.e.b((ag)localObject1, localf, (m)localObject2, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */