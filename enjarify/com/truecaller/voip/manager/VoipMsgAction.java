package com.truecaller.voip.manager;

public enum VoipMsgAction
{
  static
  {
    VoipMsgAction[] arrayOfVoipMsgAction = new VoipMsgAction[12];
    VoipMsgAction localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    localVoipMsgAction.<init>("NONE", 0);
    NONE = localVoipMsgAction;
    arrayOfVoipMsgAction[0] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    int i = 1;
    localVoipMsgAction.<init>("USER_JOINED", i);
    USER_JOINED = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    i = 2;
    localVoipMsgAction.<init>("INTERRUPTED", i);
    INTERRUPTED = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    i = 3;
    localVoipMsgAction.<init>("REJOINED", i);
    REJOINED = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    i = 4;
    localVoipMsgAction.<init>("LOST", i);
    LOST = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    i = 5;
    localVoipMsgAction.<init>("USER_MUTE_CHANGED", i);
    USER_MUTE_CHANGED = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    i = 6;
    localVoipMsgAction.<init>("OFFLINE", i);
    OFFLINE = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    i = 7;
    localVoipMsgAction.<init>("STATS_RECEIVED", i);
    STATS_RECEIVED = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    i = 8;
    localVoipMsgAction.<init>("JOINED", i);
    JOINED = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    i = 9;
    localVoipMsgAction.<init>("ERROR", i);
    ERROR = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    i = 10;
    localVoipMsgAction.<init>("LEFT_CHANNEL", i);
    LEFT_CHANNEL = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    localVoipMsgAction = new com/truecaller/voip/manager/VoipMsgAction;
    i = 11;
    localVoipMsgAction.<init>("NEW_TOKEN_REQUIRED", i);
    NEW_TOKEN_REQUIRED = localVoipMsgAction;
    arrayOfVoipMsgAction[i] = localVoipMsgAction;
    $VALUES = arrayOfVoipMsgAction;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.VoipMsgAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */