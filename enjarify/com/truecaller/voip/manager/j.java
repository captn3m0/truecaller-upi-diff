package com.truecaller.voip.manager;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private j(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
  }
  
  public static j a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    j localj = new com/truecaller/voip/manager/j;
    localj.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */