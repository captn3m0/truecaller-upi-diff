package com.truecaller.voip.manager;

import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.IRtcEngineEventHandler.RtcStats;
import kotlinx.coroutines.a.h;

public final class a$d
  extends IRtcEngineEventHandler
{
  a$d(a parama) {}
  
  public final void onConnectionInterrupted()
  {
    super.onConnectionInterrupted();
    new String[1][0] = "Connection interrupted.";
    h localh = a.a;
    VoipMsg localVoipMsg = new com/truecaller/voip/manager/VoipMsg;
    VoipMsgAction localVoipMsgAction = VoipMsgAction.INTERRUPTED;
    localVoipMsg.<init>(localVoipMsgAction, null, 2, null);
    localh.d_(localVoipMsg);
  }
  
  public final void onConnectionLost()
  {
    super.onConnectionLost();
    new String[1][0] = "Connection lost.";
    h localh = a.a;
    VoipMsg localVoipMsg = new com/truecaller/voip/manager/VoipMsg;
    VoipMsgAction localVoipMsgAction = VoipMsgAction.LOST;
    localVoipMsg.<init>(localVoipMsgAction, null, 2, null);
    localh.d_(localVoipMsg);
  }
  
  public final void onError(int paramInt)
  {
    super.onError(paramInt);
    Object localObject1 = new String[1];
    Object localObject2 = String.valueOf(paramInt);
    Object localObject3 = "Generic error: ".concat((String)localObject2);
    localObject1[0] = localObject3;
    localObject1 = a.a;
    localObject3 = new com/truecaller/voip/manager/VoipMsg;
    localObject2 = VoipMsgAction.ERROR;
    VoipMsgExtras localVoipMsgExtras = new com/truecaller/voip/manager/VoipMsgExtras;
    localVoipMsgExtras.<init>(null, null, false, 0, paramInt, 15, null);
    ((VoipMsg)localObject3).<init>((VoipMsgAction)localObject2, localVoipMsgExtras);
    ((h)localObject1).d_(localObject3);
  }
  
  public final void onJoinChannelSuccess(String paramString, int paramInt1, int paramInt2)
  {
    super.onJoinChannelSuccess(paramString, paramInt1, paramInt2);
    if (paramString == null) {
      return;
    }
    Object localObject1 = new String[1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Joining channel is successful. Channel:");
    ((StringBuilder)localObject2).append(paramString);
    ((StringBuilder)localObject2).append(", uid:");
    ((StringBuilder)localObject2).append(paramInt1);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = a;
    Object localObject3 = new com/truecaller/voip/manager/e;
    ((e)localObject3).<init>(paramString, paramInt1);
    c = ((e)localObject3);
    h localh = a.a;
    localObject1 = new com/truecaller/voip/manager/VoipMsg;
    localObject3 = VoipMsgAction.JOINED;
    VoipMsgExtras localVoipMsgExtras = new com/truecaller/voip/manager/VoipMsgExtras;
    localObject2 = localVoipMsgExtras;
    localVoipMsgExtras.<init>(null, paramString, false, 0, 0, 29, null);
    ((VoipMsg)localObject1).<init>((VoipMsgAction)localObject3, localVoipMsgExtras);
    localh.d_(localObject1);
  }
  
  public final void onLeaveChannel(IRtcEngineEventHandler.RtcStats paramRtcStats)
  {
    super.onLeaveChannel(paramRtcStats);
    paramRtcStats = a;
    c = null;
    paramRtcStats = a;
    VoipMsg localVoipMsg = new com/truecaller/voip/manager/VoipMsg;
    VoipMsgAction localVoipMsgAction = VoipMsgAction.LEFT_CHANNEL;
    localVoipMsg.<init>(localVoipMsgAction, null, 2, null);
    paramRtcStats.d_(localVoipMsg);
  }
  
  public final void onRejoinChannelSuccess(String paramString, int paramInt1, int paramInt2)
  {
    super.onRejoinChannelSuccess(paramString, paramInt1, paramInt2);
    Object localObject1 = new String[1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Channel rejoined ");
    ((StringBuilder)localObject2).append(paramString);
    ((StringBuilder)localObject2).append(' ');
    ((StringBuilder)localObject2).append(paramInt1);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = a.a;
    localObject2 = new com/truecaller/voip/manager/VoipMsg;
    VoipMsgAction localVoipMsgAction = VoipMsgAction.REJOINED;
    VoipMsgExtras localVoipMsgExtras = new com/truecaller/voip/manager/VoipMsgExtras;
    Integer localInteger = Integer.valueOf(paramInt1);
    localVoipMsgExtras.<init>(localInteger, paramString, false, 0, 0, 28, null);
    ((VoipMsg)localObject2).<init>(localVoipMsgAction, localVoipMsgExtras);
    ((h)localObject1).d_(localObject2);
  }
  
  public final void onRequestToken()
  {
    super.onRequestToken();
    new String[1][0] = "On request token";
    h localh = a.a;
    VoipMsg localVoipMsg = new com/truecaller/voip/manager/VoipMsg;
    VoipMsgAction localVoipMsgAction = VoipMsgAction.NEW_TOKEN_REQUIRED;
    localVoipMsg.<init>(localVoipMsgAction, null, 2, null);
    localh.d_(localVoipMsg);
  }
  
  public final void onRtcStats(IRtcEngineEventHandler.RtcStats paramRtcStats)
  {
    super.onRtcStats(paramRtcStats);
    if (paramRtcStats == null) {
      return;
    }
    Object localObject1 = new String[1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Stats has changed: Users:");
    int i = users;
    ((StringBuilder)localObject2).append(i);
    ((StringBuilder)localObject2).append(", duration:");
    i = totalDuration;
    ((StringBuilder)localObject2).append(i);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = a.a;
    VoipMsg localVoipMsg = new com/truecaller/voip/manager/VoipMsg;
    localObject2 = VoipMsgAction.STATS_RECEIVED;
    VoipMsgExtras localVoipMsgExtras = new com/truecaller/voip/manager/VoipMsgExtras;
    int j = users;
    localVoipMsgExtras.<init>(null, null, false, j, 0, 23, null);
    localVoipMsg.<init>((VoipMsgAction)localObject2, localVoipMsgExtras);
    ((h)localObject1).d_(localVoipMsg);
  }
  
  public final void onTokenPrivilegeWillExpire(String paramString)
  {
    super.onTokenPrivilegeWillExpire(paramString);
    Object localObject = new String[1];
    paramString = String.valueOf(paramString);
    paramString = "On token privilege will expire. Token:".concat(paramString);
    localObject[0] = paramString;
    paramString = a.a;
    localObject = new com/truecaller/voip/manager/VoipMsg;
    VoipMsgAction localVoipMsgAction = VoipMsgAction.NEW_TOKEN_REQUIRED;
    ((VoipMsg)localObject).<init>(localVoipMsgAction, null, 2, null);
    paramString.d_(localObject);
  }
  
  public final void onUserJoined(int paramInt1, int paramInt2)
  {
    super.onUserJoined(paramInt1, paramInt2);
    Object localObject1 = new String[1];
    Object localObject2 = String.valueOf(paramInt1);
    Object localObject3 = "User joined ".concat((String)localObject2);
    localObject1[0] = localObject3;
    localObject1 = a.a;
    localObject3 = new com/truecaller/voip/manager/VoipMsg;
    localObject2 = VoipMsgAction.USER_JOINED;
    VoipMsgExtras localVoipMsgExtras = new com/truecaller/voip/manager/VoipMsgExtras;
    Integer localInteger = Integer.valueOf(paramInt1);
    localVoipMsgExtras.<init>(localInteger, null, false, 0, 0, 30, null);
    ((VoipMsg)localObject3).<init>((VoipMsgAction)localObject2, localVoipMsgExtras);
    ((h)localObject1).d_(localObject3);
  }
  
  public final void onUserMuteAudio(int paramInt, boolean paramBoolean)
  {
    super.onUserMuteAudio(paramInt, paramBoolean);
    Object localObject1 = new String[1];
    Object localObject2 = String.valueOf(paramBoolean);
    Object localObject3 = "Mute toggled. Muted: ".concat((String)localObject2);
    localObject1[0] = localObject3;
    localObject1 = a.a;
    localObject3 = new com/truecaller/voip/manager/VoipMsg;
    localObject2 = VoipMsgAction.USER_MUTE_CHANGED;
    VoipMsgExtras localVoipMsgExtras = new com/truecaller/voip/manager/VoipMsgExtras;
    localVoipMsgExtras.<init>(null, null, paramBoolean, 0, 0, 27, null);
    ((VoipMsg)localObject3).<init>((VoipMsgAction)localObject2, localVoipMsgExtras);
    ((h)localObject1).d_(localObject3);
  }
  
  public final void onUserOffline(int paramInt1, int paramInt2)
  {
    super.onUserOffline(paramInt1, paramInt2);
    Object localObject1 = new String[1];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("User is offline. Uid: ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(", Reason: ");
    localStringBuilder.append(paramInt2);
    Object localObject2 = localStringBuilder.toString();
    localObject1[0] = localObject2;
    localObject2 = a.a;
    VoipMsg localVoipMsg = new com/truecaller/voip/manager/VoipMsg;
    localObject1 = VoipMsgAction.OFFLINE;
    localVoipMsg.<init>((VoipMsgAction)localObject1, null, 2, null);
    ((h)localObject2).d_(localVoipMsg);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */