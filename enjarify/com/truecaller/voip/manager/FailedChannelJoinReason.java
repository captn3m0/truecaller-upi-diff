package com.truecaller.voip.manager;

public enum FailedChannelJoinReason
{
  static
  {
    FailedChannelJoinReason[] arrayOfFailedChannelJoinReason = new FailedChannelJoinReason[2];
    FailedChannelJoinReason localFailedChannelJoinReason = new com/truecaller/voip/manager/FailedChannelJoinReason;
    localFailedChannelJoinReason.<init>("GET_TOKEN_FAILED", 0);
    GET_TOKEN_FAILED = localFailedChannelJoinReason;
    arrayOfFailedChannelJoinReason[0] = localFailedChannelJoinReason;
    localFailedChannelJoinReason = new com/truecaller/voip/manager/FailedChannelJoinReason;
    int i = 1;
    localFailedChannelJoinReason.<init>("RTC_JOIN_FAILED", i);
    RTC_JOIN_FAILED = localFailedChannelJoinReason;
    arrayOfFailedChannelJoinReason[i] = localFailedChannelJoinReason;
    $VALUES = arrayOfFailedChannelJoinReason;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.FailedChannelJoinReason
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */