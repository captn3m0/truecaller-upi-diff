package com.truecaller.voip.manager;

import com.truecaller.voip.util.a;

public final class k
{
  public final boolean a;
  public final boolean b;
  public final a c;
  
  private k(boolean paramBoolean1, boolean paramBoolean2, a parama)
  {
    a = paramBoolean1;
    b = paramBoolean2;
    c = parama;
  }
  
  private static k a(boolean paramBoolean1, boolean paramBoolean2, a parama)
  {
    c.g.b.k.b(parama, "audioRouteSetting");
    k localk = new com/truecaller/voip/manager/k;
    localk.<init>(paramBoolean1, paramBoolean2, parama);
    return localk;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof k;
      if (bool2)
      {
        paramObject = (k)paramObject;
        bool2 = a;
        boolean bool3 = a;
        a locala;
        if (bool2 == bool3)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          locala = null;
        }
        if (bool2)
        {
          bool2 = b;
          bool3 = b;
          if (bool2 == bool3)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            locala = null;
          }
          if (bool2)
          {
            locala = c;
            paramObject = c;
            boolean bool4 = c.g.b.k.a(locala, paramObject);
            if (bool4) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    boolean bool = a;
    int j = 1;
    if (bool) {
      bool = true;
    }
    bool *= true;
    int k = b;
    if (k == 0) {
      j = k;
    }
    int i = (i + j) * 31;
    a locala = c;
    if (locala != null)
    {
      j = locala.hashCode();
    }
    else
    {
      j = 0;
      locala = null;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipServiceSetting(muted=");
    boolean bool = a;
    localStringBuilder.append(bool);
    localStringBuilder.append(", onHold=");
    bool = b;
    localStringBuilder.append(bool);
    localStringBuilder.append(", audioRouteSetting=");
    a locala = c;
    localStringBuilder.append(locala);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */