package com.truecaller.voip.manager;

import c.g.b.k;

public final class VoipMsgExtras
{
  private final String channelId;
  private final int errorCode;
  private final boolean muted;
  private final Integer uid;
  private final int userCount;
  
  public VoipMsgExtras()
  {
    this(null, null, false, 0, 0, 31, null);
  }
  
  public VoipMsgExtras(Integer paramInteger, String paramString, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    uid = paramInteger;
    channelId = paramString;
    muted = paramBoolean;
    userCount = paramInt1;
    errorCode = paramInt2;
  }
  
  public final Integer component1()
  {
    return uid;
  }
  
  public final String component2()
  {
    return channelId;
  }
  
  public final boolean component3()
  {
    return muted;
  }
  
  public final int component4()
  {
    return userCount;
  }
  
  public final int component5()
  {
    return errorCode;
  }
  
  public final VoipMsgExtras copy(Integer paramInteger, String paramString, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    VoipMsgExtras localVoipMsgExtras = new com/truecaller/voip/manager/VoipMsgExtras;
    localVoipMsgExtras.<init>(paramInteger, paramString, paramBoolean, paramInt1, paramInt2);
    return localVoipMsgExtras;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof VoipMsgExtras;
      if (bool2)
      {
        paramObject = (VoipMsgExtras)paramObject;
        Object localObject1 = uid;
        Object localObject2 = uid;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = channelId;
          localObject2 = channelId;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            bool2 = muted;
            boolean bool3 = muted;
            if (bool2 == bool3)
            {
              bool2 = true;
            }
            else
            {
              bool2 = false;
              localObject1 = null;
            }
            if (bool2)
            {
              int i = userCount;
              int j = userCount;
              if (i == j)
              {
                i = 1;
              }
              else
              {
                i = 0;
                localObject1 = null;
              }
              if (i != 0)
              {
                i = errorCode;
                int k = errorCode;
                if (i == k)
                {
                  k = 1;
                }
                else
                {
                  k = 0;
                  paramObject = null;
                }
                if (k != 0) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getChannelId()
  {
    return channelId;
  }
  
  public final int getErrorCode()
  {
    return errorCode;
  }
  
  public final boolean getMuted()
  {
    return muted;
  }
  
  public final Integer getUid()
  {
    return uid;
  }
  
  public final int getUserCount()
  {
    return userCount;
  }
  
  public final int hashCode()
  {
    Integer localInteger = uid;
    int i = 0;
    if (localInteger != null)
    {
      m = localInteger.hashCode();
    }
    else
    {
      m = 0;
      localInteger = null;
    }
    m *= 31;
    String str = channelId;
    if (str != null) {
      i = str.hashCode();
    }
    int m = (m + i) * 31;
    int j = muted;
    if (j != 0) {
      j = 1;
    }
    int n = (m + j) * 31;
    int k = userCount;
    n = (n + k) * 31;
    k = errorCode;
    return n + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipMsgExtras(uid=");
    Object localObject = uid;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", channelId=");
    localObject = channelId;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", muted=");
    boolean bool = muted;
    localStringBuilder.append(bool);
    localStringBuilder.append(", userCount=");
    int i = userCount;
    localStringBuilder.append(i);
    localStringBuilder.append(", errorCode=");
    i = errorCode;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.VoipMsgExtras
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */