package com.truecaller.voip.manager;

import c.g.b.k;

public final class VoipMsg
{
  private final VoipMsgAction action;
  private final VoipMsgExtras extras;
  
  public VoipMsg()
  {
    this(null, null, 3, null);
  }
  
  public VoipMsg(VoipMsgAction paramVoipMsgAction, VoipMsgExtras paramVoipMsgExtras)
  {
    action = paramVoipMsgAction;
    extras = paramVoipMsgExtras;
  }
  
  public final VoipMsgAction component1()
  {
    return action;
  }
  
  public final VoipMsgExtras component2()
  {
    return extras;
  }
  
  public final VoipMsg copy(VoipMsgAction paramVoipMsgAction, VoipMsgExtras paramVoipMsgExtras)
  {
    k.b(paramVoipMsgAction, "action");
    k.b(paramVoipMsgExtras, "extras");
    VoipMsg localVoipMsg = new com/truecaller/voip/manager/VoipMsg;
    localVoipMsg.<init>(paramVoipMsgAction, paramVoipMsgExtras);
    return localVoipMsg;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof VoipMsg;
      if (bool1)
      {
        paramObject = (VoipMsg)paramObject;
        Object localObject = action;
        VoipMsgAction localVoipMsgAction = action;
        bool1 = k.a(localObject, localVoipMsgAction);
        if (bool1)
        {
          localObject = extras;
          paramObject = extras;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final VoipMsgAction getAction()
  {
    return action;
  }
  
  public final VoipMsgExtras getExtras()
  {
    return extras;
  }
  
  public final int hashCode()
  {
    VoipMsgAction localVoipMsgAction = action;
    int i = 0;
    int j;
    if (localVoipMsgAction != null)
    {
      j = localVoipMsgAction.hashCode();
    }
    else
    {
      j = 0;
      localVoipMsgAction = null;
    }
    j *= 31;
    VoipMsgExtras localVoipMsgExtras = extras;
    if (localVoipMsgExtras != null) {
      i = localVoipMsgExtras.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipMsg(action=");
    Object localObject = action;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", extras=");
    localObject = extras;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.VoipMsg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */