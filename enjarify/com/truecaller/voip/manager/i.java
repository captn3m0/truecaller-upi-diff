package com.truecaller.voip.manager;

import c.d.f;
import c.g.a.b;
import c.g.a.m;
import com.truecaller.utils.extensions.c;
import com.truecaller.voip.k;
import com.truecaller.voip.util.ac;
import com.truecaller.voip.util.ah;
import com.truecaller.voip.util.l;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class i
  implements h, ag
{
  volatile bn a;
  final k b;
  final ah c;
  final l d;
  final ac e;
  private final f f;
  
  public i(f paramf, k paramk, ah paramah, l paraml, ac paramac)
  {
    f = paramf;
    b = paramk;
    c = paramah;
    d = paraml;
    e = paramac;
  }
  
  public final f V_()
  {
    return f;
  }
  
  public final void a()
  {
    try
    {
      Object localObject1 = a;
      Object localObject3 = null;
      if (localObject1 != null)
      {
        localObject1 = a;
        if (localObject1 != null)
        {
          bool = ((bn)localObject1).av_();
          localObject1 = Boolean.valueOf(bool);
        }
        else
        {
          bool = false;
          localObject1 = null;
        }
        boolean bool = c.a((Boolean)localObject1);
        if (bool)
        {
          localObject1 = "Already reporting presence. Ignoring...";
          new String[1][0] = localObject1;
          return;
        }
      }
      localObject1 = new com/truecaller/voip/manager/i$a;
      ((i.a)localObject1).<init>(this, null);
      localObject1 = (m)localObject1;
      int i = 3;
      localObject1 = e.b(this, null, (m)localObject1, i);
      a = ((bn)localObject1);
      localObject1 = a;
      if (localObject1 != null)
      {
        localObject3 = new com/truecaller/voip/manager/i$b;
        ((i.b)localObject3).<init>(this);
        localObject3 = (b)localObject3;
        ((bn)localObject1).a_((b)localObject3);
        return;
      }
      return;
    }
    finally {}
  }
  
  public final void b()
  {
    c.d("reportedVoipState");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */