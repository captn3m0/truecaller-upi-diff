package com.truecaller.voip.manager;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.util.ac;
import com.truecaller.voip.util.ah;
import com.truecaller.voip.util.l;
import kotlinx.coroutines.ag;

final class i$a
  extends c.d.b.a.k
  implements m
{
  boolean a;
  int b;
  private ag d;
  
  i$a(i parami, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/voip/manager/i$a;
    i locali = c;
    locala.<init>(locali, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    boolean bool1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = a;
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label274;
      }
      paramObject = c.b;
      bool3 = ((com.truecaller.voip.k)paramObject).a();
      if (!bool3) {
        break label169;
      }
      localObject2 = c.e;
      a = bool3;
      int j = 1;
      b = j;
      localObject2 = ((ac)localObject2).a(this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      bool2 = bool3;
      paramObject = localObject2;
    }
    if (paramObject == null)
    {
      new String[1][0] = "Cannot register own voip id. Not reporting presence.";
      return x.a;
    }
    boolean bool3 = bool2;
    label169:
    localObject1 = c.c;
    Object localObject2 = "reportedVoipState";
    boolean bool2 = ((ah)localObject1).b((String)localObject2);
    if (bool2 != bool3)
    {
      new String[1][0] = "Voip state is changed. Reporting presence...";
      localObject1 = c.d;
      bool2 = ((l)localObject1).a();
      if (bool2)
      {
        new String[1][0] = "Presence reported for voip. Saving to settings...";
        localObject1 = c.c;
        localObject2 = "reportedVoipState";
        ((ah)localObject1).b((String)localObject2, bool3);
      }
      else
      {
        paramObject = "Reporting presence is failed";
        new String[1][0] = paramObject;
      }
    }
    return x.a;
    label274:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */