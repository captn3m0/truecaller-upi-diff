package com.truecaller.voip.manager;

import c.g.b.k;

public final class e
{
  public final int a;
  private final String b;
  
  public e(String paramString, int paramInt)
  {
    b = paramString;
    a = paramInt;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof e;
      if (bool2)
      {
        paramObject = (e)paramObject;
        String str1 = b;
        String str2 = b;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          int i = a;
          int j = a;
          if (i == j)
          {
            j = 1;
          }
          else
          {
            j = 0;
            paramObject = null;
          }
          if (j != 0) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = b;
    int i;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    i *= 31;
    int j = a;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("JoinedChannel(channelId=");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", uid=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */