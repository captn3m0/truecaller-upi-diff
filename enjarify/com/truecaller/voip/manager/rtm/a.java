package com.truecaller.voip.manager.rtm;

import c.d.c;
import c.g.a.m;
import c.o.b;
import com.truecaller.voip.util.ac;
import com.truecaller.voip.util.aq;
import com.truecaller.voip.util.o;
import io.agora.rtm.RtmClient;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.cn;
import kotlinx.coroutines.e;
import kotlinx.coroutines.e.b;
import kotlinx.coroutines.e.d;

public final class a
  implements h, ag
{
  boolean a;
  final b b;
  String c;
  final RtmClient d;
  final ac e;
  final aq f;
  final o g;
  private final c.d.f h;
  private final i i;
  
  public a(c.d.f paramf, RtmClient paramRtmClient, ac paramac, aq paramaq, i parami, o paramo)
  {
    h = paramf;
    d = paramRtmClient;
    e = paramac;
    f = paramaq;
    i = parami;
    g = paramo;
    paramf = d.a();
    b = paramf;
    paramf = i;
    paramRtmClient = this;
    paramRtmClient = (ag)this;
    paramac = new com/truecaller/voip/manager/rtm/a$b;
    paramac.<init>(this, null);
    paramac = (m)paramac;
    paramf.b(paramRtmClient, paramac);
  }
  
  public final c.d.f V_()
  {
    return h;
  }
  
  public final Object a(long paramLong, c paramc)
  {
    boolean bool1 = paramc instanceof a.c;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (a.c)paramc;
      int j = b;
      k = -1 << -1;
      j &= k;
      if (j != 0)
      {
        int n = b - k;
        b = n;
        break label79;
      }
    }
    Object localObject1 = new com/truecaller/voip/manager/rtm/a$c;
    ((a.c)localObject1).<init>(this, (c)paramc);
    label79:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    int k = b;
    Object localObject2;
    switch (k)
    {
    default: 
      localObject2 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject2).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject2);
    case 1: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      int m = paramc instanceof o.b;
      if (m != 0) {
        break label259;
      }
      paramc = new com/truecaller/voip/manager/rtm/a$d;
      paramc.<init>(this, null);
      paramc = (m)paramc;
      d = this;
      e = paramLong;
      m = 1;
      b = m;
      paramc = cn.a(paramLong, paramc, (c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      break;
    }
    paramc = (g)paramc;
    if (paramc == null)
    {
      localObject2 = new com/truecaller/voip/manager/rtm/f;
      FailedRtmLoginReason localFailedRtmLoginReason = FailedRtmLoginReason.TIMED_OUT;
      ((f)localObject2).<init>(localFailedRtmLoginReason);
      paramc = (c)localObject2;
      paramc = (g)localObject2;
    }
    return paramc;
    label259:
    throw a;
  }
  
  public final String a()
  {
    return c;
  }
  
  public final Object b(long paramLong, c paramc)
  {
    Object localObject = new com/truecaller/voip/manager/rtm/a$h;
    ((a.h)localObject).<init>(this, null);
    localObject = (m)localObject;
    return cn.a(paramLong, (m)localObject, paramc);
  }
  
  public final void b()
  {
    Object localObject = new com/truecaller/voip/manager/rtm/a$i;
    ((a.i)localObject).<init>(this, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */