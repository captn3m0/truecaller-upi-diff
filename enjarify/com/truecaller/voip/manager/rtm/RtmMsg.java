package com.truecaller.voip.manager.rtm;

import c.g.b.k;

public final class RtmMsg
{
  private final RtmMsgAction action;
  private final String channelId;
  private transient String senderId;
  
  public RtmMsg(RtmMsgAction paramRtmMsgAction, String paramString)
  {
    action = paramRtmMsgAction;
    channelId = paramString;
    senderId = "";
  }
  
  public final RtmMsgAction component1()
  {
    return action;
  }
  
  public final String component2()
  {
    return channelId;
  }
  
  public final RtmMsg copy(RtmMsgAction paramRtmMsgAction, String paramString)
  {
    k.b(paramRtmMsgAction, "action");
    k.b(paramString, "channelId");
    RtmMsg localRtmMsg = new com/truecaller/voip/manager/rtm/RtmMsg;
    localRtmMsg.<init>(paramRtmMsgAction, paramString);
    return localRtmMsg;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RtmMsg;
      if (bool1)
      {
        paramObject = (RtmMsg)paramObject;
        Object localObject = action;
        RtmMsgAction localRtmMsgAction = action;
        bool1 = k.a(localObject, localRtmMsgAction);
        if (bool1)
        {
          localObject = channelId;
          paramObject = channelId;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final RtmMsgAction getAction()
  {
    return action;
  }
  
  public final String getChannelId()
  {
    return channelId;
  }
  
  public final String getSenderId()
  {
    return senderId;
  }
  
  public final int hashCode()
  {
    RtmMsgAction localRtmMsgAction = action;
    int i = 0;
    int j;
    if (localRtmMsgAction != null)
    {
      j = localRtmMsgAction.hashCode();
    }
    else
    {
      j = 0;
      localRtmMsgAction = null;
    }
    j *= 31;
    String str = channelId;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final void setSenderId(String paramString)
  {
    k.b(paramString, "<set-?>");
    senderId = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RtmMsg(action=");
    Object localObject = action;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", channelId=");
    localObject = channelId;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.RtmMsg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */