package com.truecaller.voip.manager.rtm;

import c.d.a.b;
import c.d.c;
import c.g.a.m;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.voip.util.aq;
import io.agora.rtm.IResultCallback;
import io.agora.rtm.RtmClient;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ah;

final class a$e
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private ag f;
  
  a$e(a parama, boolean paramBoolean, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/voip/manager/rtm/a$e;
    a locala = d;
    boolean bool = e;
    locale.<init>(locala, bool, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = c;
    int k = 1;
    boolean bool1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label575;
      }
      throw a;
    case 2: 
      localObject2 = (String)b;
      localObject3 = (String)a;
      boolean bool3 = paramObject instanceof o.b;
      if (!bool3)
      {
        localObject4 = localObject3;
        localObject3 = localObject2;
        localObject2 = localObject4;
        break label434;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label605;
      }
      paramObject = new com/truecaller/voip/manager/rtm/a$e$1;
      bool1 = false;
      localObject2 = null;
      ((a.e.1)paramObject).<init>(this, null);
      paramObject = (m)paramObject;
      c = k;
      paramObject = ah.a((m)paramObject, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (n)paramObject;
    Object localObject2 = (String)a;
    paramObject = (String)b;
    if (localObject2 == null)
    {
      new String[1][0] = "Voip id is null. Cannot login...";
      paramObject = new com/truecaller/voip/manager/rtm/f;
      localObject1 = FailedRtmLoginReason.GET_OWN_VOIP_ID_FAILED;
      ((f)paramObject).<init>((FailedRtmLoginReason)localObject1);
      return paramObject;
    }
    if (paramObject == null)
    {
      new String[1][0] = "Cannot fetch token. Login will fail";
      paramObject = new com/truecaller/voip/manager/rtm/f;
      localObject1 = FailedRtmLoginReason.GET_RTM_TOKEN_FAILED;
      ((f)paramObject).<init>((FailedRtmLoginReason)localObject1);
      return paramObject;
    }
    a locala = d;
    boolean bool4 = e ^ true;
    a = localObject2;
    b = paramObject;
    int m = 2;
    c = m;
    kotlinx.coroutines.k localk = new kotlinx/coroutines/k;
    Object localObject5 = b.a(this);
    localk.<init>((c)localObject5, k);
    Object localObject6 = localk;
    localObject6 = (kotlinx.coroutines.j)localk;
    Object localObject3 = d;
    Object localObject7 = new com/truecaller/voip/manager/rtm/a$f;
    localObject5 = localObject7;
    ((a.f)localObject7).<init>((kotlinx.coroutines.j)localObject6, locala, (String)paramObject, (String)localObject2, bool4);
    localObject7 = (IResultCallback)localObject7;
    ((RtmClient)localObject3).login((String)paramObject, (String)localObject2, (IResultCallback)localObject7);
    localObject3 = localk.h();
    localObject5 = c.d.a.a.a;
    if (localObject3 == localObject5)
    {
      localObject5 = "frame";
      c.g.b.k.b(this, (String)localObject5);
    }
    if (localObject3 == localObject1) {
      return localObject1;
    }
    Object localObject4 = localObject3;
    localObject3 = paramObject;
    paramObject = localObject4;
    label434:
    paramObject = (Number)paramObject;
    int n = ((Number)paramObject).intValue();
    if (n != 0)
    {
      switch (n)
      {
      default: 
        paramObject = new com/truecaller/voip/manager/rtm/f;
        localObject1 = FailedRtmLoginReason.LOGIN_TO_RTM_FAILED;
        ((f)paramObject).<init>((FailedRtmLoginReason)localObject1);
        return (g)paramObject;
      }
      paramObject = d.f;
      ((aq)paramObject).a();
      boolean bool5 = e;
      if (bool5)
      {
        new String[1][0] = "Rtm token is expired or invalid. Retrying login again.";
        paramObject = d;
        m = 0;
        localObject5 = null;
        a = localObject2;
        b = localObject3;
        int j = 3;
        c = j;
        paramObject = ((a)paramObject).a(false, this);
        if (paramObject == localObject1) {
          return localObject1;
        }
        label575:
        return (g)paramObject;
      }
      paramObject = new com/truecaller/voip/manager/rtm/f;
      localObject1 = FailedRtmLoginReason.LOGIN_TO_RTM_FAILED;
      ((f)paramObject).<init>((FailedRtmLoginReason)localObject1);
      return (g)paramObject;
    }
    return (g)j.a;
    label605:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */