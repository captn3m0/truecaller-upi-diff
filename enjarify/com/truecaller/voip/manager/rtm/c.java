package com.truecaller.voip.manager.rtm;

import android.content.Context;
import c.d.a.b;
import c.g.a.m;
import c.o.b;
import com.truecaller.log.AssertionUtil;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.util.o;
import io.agora.rtm.IStateListener;
import io.agora.rtm.RtmClient;
import io.agora.rtm.RtmClientListener;
import io.agora.rtm.RtmMessage;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

public final class c
  implements i, ag
{
  final h a;
  final h b;
  public final RtmClient c;
  final com.truecaller.featuretoggles.e d;
  final Context e;
  final com.truecaller.voip.api.a f;
  final o g;
  private final c.d.f h;
  private final c.d.f i;
  private final com.google.gson.f j;
  
  public c(c.d.f paramf1, c.d.f paramf2, com.truecaller.featuretoggles.e parame, Context paramContext, com.google.gson.f paramf, com.truecaller.voip.api.a parama, o paramo)
  {
    h = paramf1;
    i = paramf2;
    d = parame;
    e = paramContext;
    j = paramf;
    f = parama;
    g = paramo;
    int k = 10;
    paramf2 = kotlinx.coroutines.a.i.a(k);
    a = paramf2;
    paramf1 = kotlinx.coroutines.a.i.a(k);
    b = paramf1;
    paramf1 = e;
    int m = R.string.voip_agora_app_id;
    paramf2 = paramf1.getString(m);
    parame = new com/truecaller/voip/manager/rtm/c$c;
    parame.<init>(this);
    parame = (RtmClientListener)parame;
    paramf1 = RtmClient.createInstance(paramf1, paramf2, parame);
    c.g.b.k.a(paramf1, "RtmClient.createInstance…        }\n        }\n    )");
    c = paramf1;
  }
  
  public final c.d.f V_()
  {
    return h;
  }
  
  final RtmMsg a(String paramString1, String paramString2)
  {
    try
    {
      localObject = j;
      Class localClass = RtmMsg.class;
      paramString2 = ((com.google.gson.f)localObject).a(paramString2, localClass);
      paramString2 = (RtmMsg)paramString2;
      localObject = paramString2.getAction();
      ((RtmMsgAction)localObject).name();
      localObject = paramString2.getChannelId();
      ((String)localObject).length();
      paramString2.setSenderId(paramString1);
    }
    catch (Exception paramString2)
    {
      new String[1][0] = "Rtm message is invalid. Sending UNSUPPORTED message to the caller";
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramString2);
      paramString2 = i;
      Object localObject = new com/truecaller/voip/manager/rtm/c$a;
      ((c.a)localObject).<init>(this, paramString1, null);
      localObject = (m)localObject;
      int k = 2;
      kotlinx.coroutines.e.b(this, paramString2, (m)localObject, k);
      paramString2 = null;
    }
    return paramString2;
  }
  
  public final Object a(VoipUser paramVoipUser, RtmMsg paramRtmMsg, boolean paramBoolean, c.g.a.a parama, c.d.c paramc)
  {
    boolean bool1 = paramc instanceof c.e;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (c.e)paramc;
      int k = b;
      m = -1 << -1;
      k &= m;
      if (k != 0)
      {
        int i1 = b - m;
        b = i1;
        break label83;
      }
    }
    Object localObject1 = new com/truecaller/voip/manager/rtm/c$e;
    ((c.e)localObject1).<init>(this, (c.d.c)paramc);
    label83:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    int m = b;
    Object localObject2;
    VoipUser localVoipUser;
    RtmMsg localRtmMsg;
    c.g.a.a locala1;
    Object localObject3;
    switch (m)
    {
    default: 
      paramVoipUser = new java/lang/IllegalStateException;
      paramVoipUser.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramVoipUser;
    case 2: 
      boolean bool2 = paramc instanceof o.b;
      if (!bool2) {
        break label484;
      }
      throw a;
    case 1: 
      paramVoipUser = g;
      parama = paramVoipUser;
      parama = (c.g.a.a)paramVoipUser;
      paramBoolean = h;
      paramVoipUser = f;
      paramRtmMsg = paramVoipUser;
      paramRtmMsg = (RtmMsg)paramVoipUser;
      paramVoipUser = (VoipUser)e;
      localObject2 = (c)d;
      boolean bool3 = paramc instanceof o.b;
      if (!bool3)
      {
        localVoipUser = paramVoipUser;
        localRtmMsg = paramRtmMsg;
        locala1 = parama;
        localObject3 = localObject2;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      int n = paramc instanceof o.b;
      if (n != 0) {
        break label493;
      }
      paramc = a;
      d = this;
      e = paramVoipUser;
      f = paramRtmMsg;
      h = paramBoolean;
      g = parama;
      n = 1;
      b = n;
      paramc = a(paramc, paramRtmMsg, (c.d.c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      localObject3 = this;
      localVoipUser = paramVoipUser;
      localRtmMsg = paramRtmMsg;
      locala1 = parama;
    }
    paramc = (Number)paramc;
    int i2 = paramc.intValue();
    if (paramBoolean)
    {
      int i3 = 3;
      if (i2 != i3)
      {
        d = localObject3;
        e = localVoipUser;
        f = localRtmMsg;
        h = paramBoolean;
        g = locala1;
        i = i2;
        i2 = 2;
        b = i2;
        paramVoipUser = i;
        paramRtmMsg = new com/truecaller/voip/manager/rtm/c$b;
        localObject2 = paramRtmMsg;
        paramRtmMsg.<init>((c)localObject3, localVoipUser, localRtmMsg, locala1, null);
        paramRtmMsg = (m)paramRtmMsg;
        paramc = g.a(paramVoipUser, paramRtmMsg, (c.d.c)localObject1);
        if (paramc == locala) {
          return locala;
        }
        label484:
        return paramc;
      }
    }
    return Integer.valueOf(i2);
    label493:
    throw a;
  }
  
  public final Object a(String paramString, RtmMsg paramRtmMsg, c.d.c paramc)
  {
    kotlinx.coroutines.k localk = new kotlinx/coroutines/k;
    Object localObject1 = b.a(paramc);
    int k = 1;
    localk.<init>((c.d.c)localObject1, k);
    localObject1 = localk;
    localObject1 = (kotlinx.coroutines.j)localk;
    RtmMessage localRtmMessage = RtmMessage.createMessage();
    paramRtmMsg = j.b(paramRtmMsg);
    localRtmMessage.setText(paramRtmMsg);
    paramRtmMsg = new String[k];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Sending RTM message to user ");
    ((StringBuilder)localObject2).append(paramString);
    ((StringBuilder)localObject2).append(". Message is:\n");
    c.g.b.k.a(localRtmMessage, "rtmMessage");
    String str = localRtmMessage.getText();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    str = null;
    paramRtmMsg[0] = localObject2;
    paramRtmMsg = c;
    localObject2 = new com/truecaller/voip/manager/rtm/c$d;
    ((c.d)localObject2).<init>((kotlinx.coroutines.j)localObject1);
    localObject2 = (IStateListener)localObject2;
    paramRtmMsg.sendMessageToPeer(paramString, localRtmMessage, (IStateListener)localObject2);
    paramString = localk.h();
    paramRtmMsg = c.d.a.a.a;
    if (paramString == paramRtmMsg)
    {
      paramRtmMsg = "frame";
      c.g.b.k.b(paramc, paramRtmMsg);
    }
    return paramString;
  }
  
  public final h a()
  {
    return a;
  }
  
  public final void a(ag paramag, m paramm)
  {
    c.g.b.k.b(paramag, "scope");
    c.g.b.k.b(paramm, "block");
    h localh = a;
    com.truecaller.utils.extensions.j.a(paramag, localh, paramm);
  }
  
  public final void b(ag paramag, m paramm)
  {
    c.g.b.k.b(paramag, "scope");
    c.g.b.k.b(paramm, "block");
    h localh = b;
    com.truecaller.utils.extensions.j.a(paramag, localh, paramm);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */