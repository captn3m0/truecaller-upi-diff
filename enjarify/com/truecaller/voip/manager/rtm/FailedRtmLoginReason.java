package com.truecaller.voip.manager.rtm;

public enum FailedRtmLoginReason
{
  static
  {
    FailedRtmLoginReason[] arrayOfFailedRtmLoginReason = new FailedRtmLoginReason[4];
    FailedRtmLoginReason localFailedRtmLoginReason = new com/truecaller/voip/manager/rtm/FailedRtmLoginReason;
    localFailedRtmLoginReason.<init>("GET_OWN_VOIP_ID_FAILED", 0);
    GET_OWN_VOIP_ID_FAILED = localFailedRtmLoginReason;
    arrayOfFailedRtmLoginReason[0] = localFailedRtmLoginReason;
    localFailedRtmLoginReason = new com/truecaller/voip/manager/rtm/FailedRtmLoginReason;
    int i = 1;
    localFailedRtmLoginReason.<init>("GET_RTM_TOKEN_FAILED", i);
    GET_RTM_TOKEN_FAILED = localFailedRtmLoginReason;
    arrayOfFailedRtmLoginReason[i] = localFailedRtmLoginReason;
    localFailedRtmLoginReason = new com/truecaller/voip/manager/rtm/FailedRtmLoginReason;
    i = 2;
    localFailedRtmLoginReason.<init>("LOGIN_TO_RTM_FAILED", i);
    LOGIN_TO_RTM_FAILED = localFailedRtmLoginReason;
    arrayOfFailedRtmLoginReason[i] = localFailedRtmLoginReason;
    localFailedRtmLoginReason = new com/truecaller/voip/manager/rtm/FailedRtmLoginReason;
    i = 3;
    localFailedRtmLoginReason.<init>("TIMED_OUT", i);
    TIMED_OUT = localFailedRtmLoginReason;
    arrayOfFailedRtmLoginReason[i] = localFailedRtmLoginReason;
    $VALUES = arrayOfFailedRtmLoginReason;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.FailedRtmLoginReason
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */