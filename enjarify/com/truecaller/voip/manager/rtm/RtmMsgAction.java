package com.truecaller.voip.manager.rtm;

public enum RtmMsgAction
{
  static
  {
    RtmMsgAction[] arrayOfRtmMsgAction = new RtmMsgAction[9];
    RtmMsgAction localRtmMsgAction = new com/truecaller/voip/manager/rtm/RtmMsgAction;
    localRtmMsgAction.<init>("INVITE", 0);
    INVITE = localRtmMsgAction;
    arrayOfRtmMsgAction[0] = localRtmMsgAction;
    localRtmMsgAction = new com/truecaller/voip/manager/rtm/RtmMsgAction;
    int i = 1;
    localRtmMsgAction.<init>("REJECT", i);
    REJECT = localRtmMsgAction;
    arrayOfRtmMsgAction[i] = localRtmMsgAction;
    localRtmMsgAction = new com/truecaller/voip/manager/rtm/RtmMsgAction;
    i = 2;
    localRtmMsgAction.<init>("BUSY", i);
    BUSY = localRtmMsgAction;
    arrayOfRtmMsgAction[i] = localRtmMsgAction;
    localRtmMsgAction = new com/truecaller/voip/manager/rtm/RtmMsgAction;
    i = 3;
    localRtmMsgAction.<init>("ON_HOLD", i);
    ON_HOLD = localRtmMsgAction;
    arrayOfRtmMsgAction[i] = localRtmMsgAction;
    localRtmMsgAction = new com/truecaller/voip/manager/rtm/RtmMsgAction;
    i = 4;
    localRtmMsgAction.<init>("RESUMED", i);
    RESUMED = localRtmMsgAction;
    arrayOfRtmMsgAction[i] = localRtmMsgAction;
    localRtmMsgAction = new com/truecaller/voip/manager/rtm/RtmMsgAction;
    i = 5;
    localRtmMsgAction.<init>("END", i);
    END = localRtmMsgAction;
    arrayOfRtmMsgAction[i] = localRtmMsgAction;
    localRtmMsgAction = new com/truecaller/voip/manager/rtm/RtmMsgAction;
    i = 6;
    localRtmMsgAction.<init>("UNSUPPORTED", i);
    UNSUPPORTED = localRtmMsgAction;
    arrayOfRtmMsgAction[i] = localRtmMsgAction;
    localRtmMsgAction = new com/truecaller/voip/manager/rtm/RtmMsgAction;
    i = 7;
    localRtmMsgAction.<init>("ONLINE", i);
    ONLINE = localRtmMsgAction;
    arrayOfRtmMsgAction[i] = localRtmMsgAction;
    localRtmMsgAction = new com/truecaller/voip/manager/rtm/RtmMsgAction;
    i = 8;
    localRtmMsgAction.<init>("RINGING", i);
    RINGING = localRtmMsgAction;
    arrayOfRtmMsgAction[i] = localRtmMsgAction;
    $VALUES = arrayOfRtmMsgAction;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.RtmMsgAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */