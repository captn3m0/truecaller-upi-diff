package com.truecaller.voip.manager.rtm;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import c.n.m;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.d;
import com.truecaller.voip.incoming.IncomingVoipService;
import com.truecaller.voip.util.VoipAnalyticsCallDirection;
import com.truecaller.voip.util.VoipAnalyticsState;
import com.truecaller.voip.util.n;
import com.truecaller.voip.util.o;
import com.truecaller.voip.util.o.a;
import io.agora.rtm.RtmClientListener;
import io.agora.rtm.RtmMessage;
import kotlinx.coroutines.a.h;

public final class c$c
  implements RtmClientListener
{
  c$c(c paramc) {}
  
  public final void onConnectionStateChanged(int paramInt)
  {
    Object localObject = new String[1];
    String str1 = String.valueOf(paramInt);
    String str2 = "Connection state is changed. State: ".concat(str1);
    localObject[0] = str2;
    localObject = a.b;
    Integer localInteger = Integer.valueOf(paramInt);
    ((h)localObject).d_(localInteger);
  }
  
  public final void onMessageReceived(RtmMessage paramRtmMessage, String paramString)
  {
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("New message is received from ");
    ((StringBuilder)localObject2).append(paramString);
    ((StringBuilder)localObject2).append(". Message: ");
    Object localObject3 = null;
    if (paramRtmMessage != null) {
      localObject4 = paramRtmMessage.getText();
    } else {
      localObject4 = null;
    }
    ((StringBuilder)localObject2).append((String)localObject4);
    localObject2 = ((StringBuilder)localObject2).toString();
    Object localObject4 = null;
    localObject1[0] = localObject2;
    localObject1 = a.d.B();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (!bool1) {
      return;
    }
    label154:
    String str;
    if ((paramRtmMessage != null) && (paramString != null))
    {
      localObject1 = (CharSequence)paramRtmMessage.getText();
      if (localObject1 != null)
      {
        bool1 = m.a((CharSequence)localObject1);
        if (!bool1)
        {
          bool1 = false;
          localObject1 = null;
          break label154;
        }
      }
      bool1 = true;
      if (!bool1)
      {
        localObject1 = a;
        paramRtmMessage = paramRtmMessage.getText();
        localObject2 = "rtmMessage.text";
        k.a(paramRtmMessage, (String)localObject2);
        paramRtmMessage = ((c)localObject1).a(paramString, paramRtmMessage);
        if (paramRtmMessage == null) {
          return;
        }
        a.a.d_(paramRtmMessage);
        paramString = paramRtmMessage.getAction();
        localObject1 = d.a;
        int j = paramString.ordinal();
        j = localObject1[j];
        if (j == i)
        {
          paramString = a;
          str = paramRtmMessage.getSenderId();
          paramRtmMessage = paramRtmMessage.getChannelId();
          localObject1 = g;
          localObject2 = new com/truecaller/voip/util/n;
          localObject4 = VoipAnalyticsCallDirection.INCOMING;
          int k = 60;
          ((n)localObject2).<init>((VoipAnalyticsCallDirection)localObject4, paramRtmMessage, null, k);
          localObject3 = VoipAnalyticsState.INVITED;
          o.a.a((o)localObject1, (n)localObject2, (VoipAnalyticsState)localObject3);
          localObject1 = e;
          localObject2 = IncomingVoipService.e;
          paramString = e;
          k.b(paramString, "context");
          k.b(str, "voipId");
          k.b(paramRtmMessage, "channelId");
          localObject2 = new android/content/Intent;
          localObject3 = IncomingVoipService.class;
          ((Intent)localObject2).<init>(paramString, (Class)localObject3);
          ((Intent)localObject2).putExtra("com.truecaller.voip.extra.EXTRA_VOIP_ID", str);
          paramString = "com.truecaller.voip.extra.EXTRA_CHANNEL_ID";
          ((Intent)localObject2).putExtra(paramString, paramRtmMessage);
          android.support.v4.content.b.a((Context)localObject1, (Intent)localObject2);
        }
        return;
      }
    }
    localObject1 = new java/lang/StringBuilder;
    localObject2 = "Invalid voip Rtm message. Rtm message(null = ";
    ((StringBuilder)localObject1).<init>((String)localObject2);
    boolean bool3;
    if (paramRtmMessage == null)
    {
      bool3 = true;
    }
    else
    {
      bool3 = false;
      localObject2 = null;
    }
    ((StringBuilder)localObject1).append(bool3);
    localObject2 = ") User id(null = ";
    ((StringBuilder)localObject1).append((String)localObject2);
    boolean bool2;
    if (paramString == null)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      paramString = null;
    }
    ((StringBuilder)localObject1).append(bool2);
    paramString = ") Rtm message text(blank = ";
    ((StringBuilder)localObject1).append(paramString);
    if (paramRtmMessage != null) {
      localObject3 = paramRtmMessage.getText();
    }
    localObject3 = (CharSequence)localObject3;
    if (localObject3 != null)
    {
      boolean bool4 = m.a((CharSequence)localObject3);
      if (!bool4)
      {
        i = 0;
        str = null;
      }
    }
    ((StringBuilder)localObject1).append(i);
    ((StringBuilder)localObject1).append(')');
    paramRtmMessage = ((StringBuilder)localObject1).toString();
    paramString = new com/truecaller/log/UnmutedException$d;
    paramString.<init>(paramRtmMessage);
    AssertionUtil.reportThrowableButNeverCrash((Throwable)paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */