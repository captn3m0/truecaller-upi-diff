package com.truecaller.voip;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class aj$b
  extends c.d.b.a.k
  implements c.g.a.m
{
  boolean a;
  int b;
  private ag f;
  
  aj$b(aj paramaj, Contact paramContact, e parame, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/voip/aj$b;
    aj localaj = c;
    Contact localContact = d;
    e locale = e;
    localb.<init>(localaj, localContact, locale, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label283;
      }
      paramObject = c;
      Object localObject1 = d.A();
      c.g.b.k.a(localObject1, "contact.numbers");
      localObject1 = (Iterable)localObject1;
      Object localObject2 = new java/util/ArrayList;
      int j = c.a.m.a((Iterable)localObject1, 10);
      ((ArrayList)localObject2).<init>(j);
      localObject2 = (Collection)localObject2;
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        bool3 = ((Iterator)localObject1).hasNext();
        if (!bool3) {
          break;
        }
        localObject3 = (Number)((Iterator)localObject1).next();
        String str = "it";
        c.g.b.k.a(localObject3, str);
        localObject3 = ((Number)localObject3).a();
        ((Collection)localObject2).add(localObject3);
      }
      localObject2 = (List)localObject2;
      int k = aj.a((aj)paramObject, (List)localObject2);
      localObject1 = c.a;
      localObject2 = new com/truecaller/voip/aj$b$1;
      boolean bool3 = false;
      Object localObject3 = null;
      ((aj.b.1)localObject2).<init>(this, k, null);
      localObject2 = (c.g.a.m)localObject2;
      a = k;
      k = 1;
      b = k;
      paramObject = g.a((f)localObject1, (c.g.a.m)localObject2, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return x.a;
    label283:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.aj.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */