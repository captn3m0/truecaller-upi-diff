package com.truecaller.voip;

import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.search.j;
import com.truecaller.network.search.l;
import com.truecaller.network.search.n;
import java.io.IOException;
import java.util.UUID;

final class i$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private kotlinx.coroutines.ag d;
  
  i$a(i parami, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/voip/i$a;
    i locali = b;
    String str = c;
    locala.<init>(locali, str, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    d = ((kotlinx.coroutines.ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = null;
        try
        {
          localObject1 = b;
          localObject1 = ((i)localObject1).a();
          localObject1 = ((bp)localObject1).u();
          Object localObject2 = "graph.searchManager()";
          c.g.b.k.a(localObject1, (String)localObject2);
          localObject2 = UUID.randomUUID();
          String str1 = "UUID.randomUUID()";
          c.g.b.k.a(localObject2, str1);
          str1 = "voip";
          localObject1 = ((l)localObject1).a((UUID)localObject2, str1);
          localObject1 = ((j)localObject1).b();
          localObject2 = c;
          localObject1 = ((j)localObject1).a((String)localObject2);
          localObject1 = ((j)localObject1).a();
          int j = 4;
          localObject1 = ((j)localObject1).a(j);
          localObject1 = ((j)localObject1).f();
          if (localObject1 != null)
          {
            localObject1 = ((n)localObject1).a();
            if (localObject1 != null)
            {
              com.truecaller.voip.util.ag localag = new com/truecaller/voip/util/ag;
              localObject2 = "contact";
              c.g.b.k.a(localObject1, (String)localObject2);
              str1 = ((Contact)localObject1).s();
              localObject2 = "contact.displayNameOrNumber";
              c.g.b.k.a(str1, (String)localObject2);
              j = 0;
              localObject2 = null;
              localObject2 = ((Contact)localObject1).a(false);
              Object localObject3;
              if (localObject2 != null)
              {
                localObject2 = ((Uri)localObject2).toString();
                localObject3 = localObject2;
              }
              else
              {
                localObject3 = null;
              }
              String str2 = c;
              localObject2 = b;
              boolean bool2 = i.a((i)localObject2, (Contact)localObject1);
              localObject2 = b;
              Integer localInteger = i.b((i)localObject2, (Contact)localObject1);
              boolean bool3 = ((Contact)localObject1).Z();
              localObject2 = localag;
              localag.<init>(str1, (String)localObject3, str2, bool2, localInteger, bool3);
              paramObject = localag;
            }
          }
        }
        catch (IOException localIOException)
        {
          localObject1 = (Throwable)localIOException;
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
        }
        return paramObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */