package com.truecaller.voip.incoming;

import android.graphics.Bitmap;
import com.truecaller.voip.VoipUser;

public abstract interface b$d
{
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(Bitmap paramBitmap);
  
  public abstract void a(VoipUser paramVoipUser, String paramString);
  
  public abstract void a(String paramString);
  
  public abstract void b(String paramString);
  
  public abstract boolean b();
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract void f();
  
  public abstract void g();
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */