package com.truecaller.voip.incoming;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.util.an;
import kotlinx.coroutines.ao;

final class c$e
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private kotlinx.coroutines.ag g;
  
  c$e(c paramc, String paramString1, String paramString2, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/voip/incoming/c$e;
    c localc = d;
    String str1 = e;
    String str2 = f;
    locale.<init>(localc, str1, str2, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    g = ((kotlinx.coroutines.ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = c;
    Object localObject2;
    Object localObject3;
    boolean bool3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label536;
      }
      throw a;
    case 2: 
      localObject2 = (ao)b;
      localObject3 = (ao)a;
      bool3 = paramObject instanceof o.b;
      if (!bool3) {
        break label425;
      }
      throw a;
    case 1: 
      localObject2 = (ao)b;
      localObject3 = (ao)a;
      bool3 = paramObject instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label605;
      }
      paramObject = bd).a;
      localObject2 = VoipState.INITIAL;
      if (paramObject != localObject2)
      {
        paramObject = d;
        localObject1 = e;
        localObject2 = f;
        c.a((c)paramObject, (String)localObject1, (String)localObject2);
        return x.a;
      }
      paramObject = d;
      localObject2 = e;
      localObject3 = f;
      bool4 = c.b((c)paramObject, (String)localObject2, (String)localObject3);
      if (!bool4)
      {
        paramObject = c.c(d);
        if (paramObject != null) {
          ((b.d)paramObject).g();
        }
        return x.a;
      }
      c.d(d);
      c.e(d);
      paramObject = d;
      localObject2 = VoipState.CONNECTING;
      c.a((c)paramObject, (VoipState)localObject2);
      paramObject = c.f(d);
      localObject2 = d;
      localObject3 = e;
      localObject2 = c.a((c)localObject2, (String)localObject3);
      a = paramObject;
      b = localObject2;
      int m = 1;
      c = m;
      localObject3 = ((ao)paramObject).a(this);
      if (localObject3 == localObject1) {
        return localObject1;
      }
      Object localObject4 = localObject3;
      localObject3 = paramObject;
      paramObject = localObject4;
    }
    paramObject = (Boolean)paramObject;
    boolean bool4 = ((Boolean)paramObject).booleanValue();
    if (bool4)
    {
      a = localObject3;
      b = localObject2;
      int k = 2;
      c = k;
      paramObject = ((ao)localObject2).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label425:
      paramObject = (Boolean)paramObject;
      boolean bool5 = ((Boolean)paramObject).booleanValue();
      if (bool5)
      {
        paramObject = d;
        bool5 = c.g((c)paramObject);
        if (!bool5)
        {
          paramObject = c.c(d);
          if (paramObject != null)
          {
            String str = hd).c;
            ((b.d)paramObject).a(str);
          }
          c.i(d);
          paramObject = d;
          a = localObject3;
          b = localObject2;
          int j = 3;
          c = j;
          paramObject = ((c)paramObject).a(this);
          if (paramObject == localObject1) {
            return localObject1;
          }
          label536:
          paramObject = (Boolean)paramObject;
          bool5 = ((Boolean)paramObject).booleanValue();
          if (!bool5) {
            return x.a;
          }
          paramObject = c.j(d);
          localObject1 = hd).b;
          ((an)paramObject).b((String)localObject1);
          paramObject = d;
          localObject1 = VoipState.RINGING;
          c.a((c)paramObject, (VoipState)localObject1);
          return x.a;
        }
      }
    }
    return x.a;
    label605:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.c.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */