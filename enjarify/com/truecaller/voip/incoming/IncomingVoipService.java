package com.truecaller.voip.incoming;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.app.z.a;
import android.support.v4.app.z.a.a;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.telephony.TelephonyManager;
import androidx.work.g;
import androidx.work.k.a;
import androidx.work.p;
import c.d.f;
import c.g.a.m;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incoming.blocked.VoipBlockedCallsWorker;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker.a;
import com.truecaller.voip.incoming.ui.IncomingVoipActivity;
import com.truecaller.voip.incoming.ui.IncomingVoipActivity.a;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.w;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

public final class IncomingVoipService
  extends Service
  implements b.d, ag
{
  public static final IncomingVoipService.a e;
  private static boolean h;
  public f a;
  public f b;
  public b.c c;
  public w d;
  private BroadcastReceiver f;
  private z.d g;
  
  static
  {
    IncomingVoipService.a locala = new com/truecaller/voip/incoming/IncomingVoipService$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  private final void i()
  {
    Object localObject = g;
    if (localObject != null)
    {
      int i = R.id.voip_incoming_service_foreground_notification;
      localObject = ((z.d)localObject).h();
      startForeground(i, (Notification)localObject);
      return;
    }
  }
  
  public final f V_()
  {
    f localf = a;
    if (localf == null)
    {
      String str = "uiContext";
      c.g.b.k.a(str);
    }
    return localf;
  }
  
  public final void a()
  {
    Object localObject = IncomingVoipActivity.a;
    localObject = this;
    localObject = IncomingVoipActivity.a.a((Context)this, false, false);
    startActivity((Intent)localObject);
  }
  
  public final void a(int paramInt)
  {
    z.d locald = g;
    if (locald == null) {
      return;
    }
    f localf = b;
    if (localf == null)
    {
      localObject = "asyncContext";
      c.g.b.k.a((String)localObject);
    }
    Object localObject = new com/truecaller/voip/incoming/IncomingVoipService$c;
    ((IncomingVoipService.c)localObject).<init>(this, paramInt, locald, null);
    localObject = (m)localObject;
    e.b(this, localf, (m)localObject, 2);
  }
  
  public final void a(Bitmap paramBitmap)
  {
    c.g.b.k.b(paramBitmap, "icon");
    z.d locald = g;
    if (locald != null) {
      locald.a(paramBitmap);
    }
    i();
  }
  
  public final void a(VoipUser paramVoipUser, String paramString)
  {
    c.g.b.k.b(paramVoipUser, "voipUser");
    c.g.b.k.b(paramString, "channelId");
    Object localObject = VoipService.e;
    localObject = this;
    localObject = (Context)this;
    c.g.b.k.b(localObject, "context");
    c.g.b.k.b(paramVoipUser, "voipUser");
    c.g.b.k.b(paramString, "channelId");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>((Context)localObject, VoipService.class);
    paramVoipUser = (Parcelable)paramVoipUser;
    localIntent.putExtra("com.truecaller.voip.extra.EXTRA_USER_ID", paramVoipUser);
    localIntent.putExtra("com.truecaller.voip.extra.EXTRA_CHANNEL_ID", paramString);
    b.a((Context)localObject, localIntent);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "title");
    z.d locald = g;
    if (locald != null)
    {
      paramString = (CharSequence)paramString;
      locald.a(paramString);
    }
    i();
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "channelId");
    Object localObject1 = new android/support/v4/app/z$a$a;
    int i = R.string.voip_button_notification_answer;
    Object localObject2 = (CharSequence)getString(i);
    Object localObject3 = IncomingVoipActivity.a;
    localObject3 = this;
    localObject3 = (Context)this;
    int j = 1;
    Object localObject4 = IncomingVoipActivity.a.a((Context)localObject3, j, j);
    int k = R.id.voip_incoming_notification_action_answer;
    int m = 134217728;
    localObject4 = PendingIntent.getActivity((Context)localObject3, k, (Intent)localObject4, m);
    c.g.b.k.a(localObject4, "PendingIntent.getActivit…tent.FLAG_UPDATE_CURRENT)");
    ((z.a.a)localObject1).<init>(0, (CharSequence)localObject2, (PendingIntent)localObject4);
    localObject1 = ((z.a.a)localObject1).a();
    localObject2 = new android/support/v4/app/z$a$a;
    int n = R.string.voip_button_notification_decline;
    localObject4 = (CharSequence)getString(n);
    c.g.b.k.b(localObject3, "context");
    Object localObject5 = new android/content/Intent;
    ((Intent)localObject5).<init>((Context)localObject3, IncomingVoipService.class);
    ((Intent)localObject5).setAction("com.truecaller.voip.incoming.ACTION_NOTIFICATION");
    ((Intent)localObject5).putExtra("com.truecaller.voip.incoming.EXTRA_ACTION_REJECT_CALL", j);
    int i1 = R.id.voip_incoming_notification_action_decline;
    Object localObject6 = PendingIntent.getService((Context)localObject3, i1, (Intent)localObject5, m);
    c.g.b.k.a(localObject6, "PendingIntent.getService…tent.FLAG_UPDATE_CURRENT)");
    ((z.a.a)localObject2).<init>(0, (CharSequence)localObject4, (PendingIntent)localObject6);
    localObject2 = ((z.a.a)localObject2).a();
    localObject4 = IncomingVoipActivity.a;
    localObject4 = IncomingVoipActivity.a.a((Context)localObject3, false, false);
    localObject4 = PendingIntent.getActivity((Context)localObject3, 0, (Intent)localObject4, 0);
    localObject6 = new android/support/v4/app/z$d;
    ((z.d)localObject6).<init>((Context)localObject3, paramString);
    int i2 = R.drawable.ic_voip_notification;
    paramString = ((z.d)localObject6).a(i2).b().d();
    m = R.color.voip_header_color;
    int i3 = b.c((Context)localObject3, m);
    paramString = paramString.f(i3);
    i3 = R.string.voip_status_incoming_audio_call;
    localObject6 = new Object[j];
    int i4 = R.string.voip_text;
    localObject5 = getString(i4);
    localObject6[0] = localObject5;
    localObject3 = (CharSequence)getString(i3, (Object[])localObject6);
    paramString = paramString.b((CharSequence)localObject3).a((z.a)localObject2).a((z.a)localObject1).a((PendingIntent)localObject4).e(j);
    g = paramString;
    i();
  }
  
  public final boolean b()
  {
    Object localObject = VoipService.e;
    boolean bool = VoipService.p();
    if (!bool)
    {
      localObject = i.b(this);
      int i = ((TelephonyManager)localObject).getCallState();
      if (i == 0) {
        return false;
      }
    }
    return true;
  }
  
  public final void d()
  {
    MissedVoipCallsWorker.a.a();
  }
  
  public final void e()
  {
    Object localObject = VoipBlockedCallsWorker.e;
    localObject = new androidx/work/k$a;
    ((k.a)localObject).<init>(VoipBlockedCallsWorker.class);
    localObject = ((k.a)localObject).c();
    c.g.b.k.a(localObject, "OneTimeWorkRequest.Build…\n                .build()");
    localObject = (androidx.work.k)localObject;
    p localp = p.a();
    g localg = g.a;
    localp.a("com.truecaller.voip.incoming.blocked.BlockedVoipCallsWorker", localg, (androidx.work.k)localObject);
  }
  
  public final void f()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.CLOSE_SYSTEM_DIALOGS");
    sendBroadcast(localIntent);
  }
  
  public final void g()
  {
    stopForeground(true);
    stopSelf();
  }
  
  public final void onCreate()
  {
    super.onCreate();
    h = true;
    Object localObject = j.a;
    j.a.a().a(this);
    localObject = new com/truecaller/voip/incoming/IncomingVoipService$b;
    ((IncomingVoipService.b)localObject).<init>(this);
    localObject = (BroadcastReceiver)localObject;
    f = ((BroadcastReceiver)localObject);
    localObject = new android/content/IntentFilter;
    ((IntentFilter)localObject).<init>();
    ((IntentFilter)localObject).addAction("android.intent.action.SCREEN_OFF");
    ((IntentFilter)localObject).addAction("android.media.VOLUME_CHANGED_ACTION");
    BroadcastReceiver localBroadcastReceiver = f;
    registerReceiver(localBroadcastReceiver, (IntentFilter)localObject);
  }
  
  public final void onDestroy()
  {
    h = false;
    Object localObject = f;
    unregisterReceiver((BroadcastReceiver)localObject);
    localObject = c;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    ((b.c)localObject).y_();
    super.onDestroy();
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    String str1 = null;
    String str2;
    if (paramIntent != null) {
      str2 = paramIntent.getAction();
    } else {
      str2 = null;
    }
    String str3;
    if (paramIntent != null) {
      str3 = paramIntent.getStringExtra("com.truecaller.voip.extra.EXTRA_VOIP_ID");
    } else {
      str3 = null;
    }
    if (paramIntent != null) {
      str1 = paramIntent.getStringExtra("com.truecaller.voip.extra.EXTRA_CHANNEL_ID");
    }
    b.c localc = c;
    if (localc == null)
    {
      String str4 = "presenter";
      c.g.b.k.a(str4);
    }
    localc.a(this);
    if (str2 == null)
    {
      paramIntent = c;
      if (paramIntent == null)
      {
        str2 = "presenter";
        c.g.b.k.a(str2);
      }
      paramIntent.a(str3, str1);
    }
    else
    {
      str1 = "com.truecaller.voip.incoming.EXTRA_ACTION_REJECT_CALL";
      str2 = null;
      boolean bool = paramIntent.getBooleanExtra(str1, false);
      if (bool)
      {
        paramIntent = c;
        if (paramIntent == null)
        {
          str1 = "presenter";
          c.g.b.k.a(str1);
        }
        paramIntent.h();
      }
    }
    return 2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.IncomingVoipService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */