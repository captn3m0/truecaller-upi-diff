package com.truecaller.voip.incoming.missed;

import androidx.work.ListenableWorker.a;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class d$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  d$c(d paramd, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/voip/incoming/missed/d$c;
    d locald = b;
    localc.<init>(locald, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    int j = 1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label144;
      }
      paramObject = b;
      a = j;
      paramObject = ((d)paramObject).a(this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = (Boolean)paramObject;
    boolean bool3 = ((Boolean)paramObject).booleanValue();
    if (bool3 == j) {
      return ListenableWorker.a.a();
    }
    if (!bool3) {
      return ListenableWorker.a.b();
    }
    paramObject = new c/l;
    ((l)paramObject).<init>();
    throw ((Throwable)paramObject);
    label144:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */