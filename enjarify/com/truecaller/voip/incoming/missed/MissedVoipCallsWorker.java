package com.truecaller.voip.incoming.missed;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.v4.app.z.d;
import android.support.v4.app.z.f;
import android.support.v4.app.z.g;
import android.text.format.DateUtils;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import c.a.m;
import c.g.b.k;
import c.l;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.plurals;
import com.truecaller.voip.R.string;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.VoipService.a;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.af;
import java.util.Iterator;
import java.util.List;

public final class MissedVoipCallsWorker
  extends Worker
  implements c.d
{
  public static final MissedVoipCallsWorker.a e;
  public c.c b;
  public com.truecaller.notificationchannels.b c;
  public af d;
  private final Context f;
  
  static
  {
    MissedVoipCallsWorker.a locala = new com/truecaller/voip/incoming/missed/MissedVoipCallsWorker$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public MissedVoipCallsWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    f = paramContext;
  }
  
  private final z.d c()
  {
    z.d locald = new android/support/v4/app/z$d;
    Context localContext = f;
    Object localObject = c;
    if (localObject == null)
    {
      String str = "callingNotificationChannelProvider";
      k.a(str);
    }
    localObject = ((com.truecaller.notificationchannels.b)localObject).X_();
    locald.<init>(localContext, (String)localObject);
    locald = locald.c(4);
    localContext = f;
    int i = R.color.truecaller_blue_all_themes;
    int j = android.support.v4.content.b.c(localContext, i);
    locald = locald.f(j);
    j = R.drawable.ic_notification_call_missed;
    return locald.a(j).e();
  }
  
  public final ListenableWorker.a a()
  {
    boolean bool = isStopped();
    if (bool)
    {
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    Object localObject1 = com.truecaller.voip.j.a;
    j.a.a().a(this);
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((c.c)localObject1).a(this);
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    localObject1 = ((c.c)localObject1).a();
    Object localObject2 = b;
    if (localObject2 == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((c.c)localObject2).y_();
    return (ListenableWorker.a)localObject1;
  }
  
  public final void a(c.a parama, Bitmap paramBitmap)
  {
    k.b(parama, "missedCall");
    Object localObject1 = VoipService.e;
    localObject1 = f;
    Object localObject2 = b;
    localObject1 = VoipService.a.a((Context)localObject1, (String)localObject2);
    int i = 1;
    ((Intent)localObject1).putExtra("com.truecaller.voip.incoming.EXTRA_FROM_MISSED_CALL", i);
    localObject2 = f;
    int j = R.id.voip_notification_missed_action_call_back;
    k.b(localObject2, "receiver$0");
    Object localObject3 = "intent";
    k.b(localObject1, (String)localObject3);
    int k = Build.VERSION.SDK_INT;
    int m = 134217728;
    int n = 26;
    if (k >= n)
    {
      localObject1 = PendingIntent.getForegroundService((Context)localObject2, j, (Intent)localObject1, m);
      localObject2 = "PendingIntent.getForegro…questCode, intent, flags)";
      k.a(localObject1, (String)localObject2);
    }
    else
    {
      localObject1 = PendingIntent.getService((Context)localObject2, j, (Intent)localObject1, m);
      localObject2 = "PendingIntent.getService…questCode, intent, flags)";
      k.a(localObject1, (String)localObject2);
    }
    localObject2 = f;
    Object localObject4 = MissedVoipCallMessageBroadcast.b;
    localObject4 = f;
    localObject3 = b;
    k.b(localObject4, "context");
    k.b(localObject3, "number");
    Object localObject5 = new android/content/Intent;
    Class localClass = MissedVoipCallMessageBroadcast.class;
    ((Intent)localObject5).<init>((Context)localObject4, localClass);
    ((Intent)localObject5).putExtra("com.truecaller.voip.extra.EXTRA_NUMBER", (String)localObject3);
    j = 0;
    localObject4 = null;
    localObject2 = PendingIntent.getBroadcast((Context)localObject2, 0, (Intent)localObject5, m);
    localObject3 = c();
    long l1 = d;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      l1 = d;
      ((z.d)localObject3).a(l1);
    }
    m = R.drawable.ic_notification_call;
    localObject5 = f;
    int i1 = R.string.voip_button_notification_call_back;
    localObject5 = (CharSequence)((Context)localObject5).getString(i1);
    localObject1 = ((z.d)localObject3).a(m, (CharSequence)localObject5, (PendingIntent)localObject1);
    k = R.drawable.ic_sms;
    Object localObject6 = f;
    n = R.string.voip_button_notification_message;
    localObject6 = (CharSequence)((Context)localObject6).getString(n);
    localObject1 = ((z.d)localObject1).a(k, (CharSequence)localObject6, (PendingIntent)localObject2);
    if (paramBitmap != null) {
      ((z.d)localObject1).a(paramBitmap);
    }
    paramBitmap = f.getResources();
    int i2 = R.plurals.voip_notification_missed_grouped_title;
    localObject3 = new Object[i];
    localObject6 = f;
    n = R.string.voip_text;
    localObject6 = ((Context)localObject6).getString(n);
    localObject3[0] = localObject6;
    paramBitmap = (CharSequence)paramBitmap.getQuantityString(i2, i, (Object[])localObject3);
    paramBitmap = ((z.d)localObject1).a(paramBitmap);
    localObject1 = (CharSequence)a;
    paramBitmap = paramBitmap.b((CharSequence)localObject1);
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject2 = "missedCallIntentProvider";
      k.a((String)localObject2);
    }
    localObject1 = ((af)localObject1).a();
    paramBitmap = paramBitmap.a((PendingIntent)localObject1);
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject2 = "missedCallIntentProvider";
      k.a((String)localObject2);
    }
    long l3 = d;
    parama = ((af)localObject1).a(l3);
    parama = paramBitmap.b(parama).h();
    paramBitmap = i.f(f);
    int i3 = R.id.voip_incoming_service_missed_call_notification;
    paramBitmap.notify(i3, parama);
  }
  
  public final void a(List paramList, int paramInt)
  {
    k.b(paramList, "missedCallsToShow");
    Object localObject1 = f.getResources();
    int i = R.plurals.voip_notification_missed_grouped_title;
    boolean bool1 = true;
    Object localObject2 = new Object[bool1];
    Object localObject3 = f;
    int j = R.string.voip_text;
    localObject3 = ((Context)localObject3).getString(j);
    j = 0;
    localObject2[0] = localObject3;
    localObject1 = ((Resources)localObject1).getQuantityString(i, paramInt, (Object[])localObject2);
    Object localObject4 = f;
    int k = R.string.voip_notification_missed_grouped_message;
    int m = 2;
    Object localObject5 = new Object[m];
    int n = 99;
    if (paramInt > n)
    {
      localObject6 = new java/lang/StringBuilder;
      ((StringBuilder)localObject6).<init>();
      ((StringBuilder)localObject6).append(paramInt);
      char c1 = '+';
      ((StringBuilder)localObject6).append(c1);
      localObject6 = ((StringBuilder)localObject6).toString();
    }
    else
    {
      localObject6 = Integer.valueOf(paramInt);
    }
    localObject5[0] = localObject6;
    Object localObject6 = f;
    int i2 = R.string.voip_text;
    localObject6 = ((Context)localObject6).getString(i2);
    localObject5[bool1] = localObject6;
    localObject4 = ((Context)localObject4).getString(k, (Object[])localObject5);
    localObject2 = new android/support/v4/app/z$f;
    ((z.f)localObject2).<init>();
    localObject4 = (CharSequence)localObject4;
    ((z.f)localObject2).a((CharSequence)localObject4);
    localObject5 = paramList;
    localObject5 = ((Iterable)paramList).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject5).hasNext();
      if (!bool2) {
        break label408;
      }
      localObject6 = (c.a)((Iterator)localObject5).next();
      long l1 = d;
      boolean bool3 = DateUtils.isToday(l1);
      Object localObject7;
      long l2;
      if (bool3 == bool1)
      {
        localObject7 = f;
        l2 = d;
        localObject7 = com.truecaller.common.h.j.f((Context)localObject7, l2);
      }
      else
      {
        if (bool3) {
          break;
        }
        localObject7 = f;
        l2 = d;
        localObject7 = com.truecaller.common.h.j.e((Context)localObject7, l2);
      }
      k.a(localObject7, "when (DateUtils.isToday(….timestamp)\n            }");
      Context localContext = f;
      int i3 = R.string.voip_notification_missed_grouped_time_and_caller;
      Object[] arrayOfObject = new Object[m];
      arrayOfObject[0] = localObject7;
      localObject6 = a;
      arrayOfObject[bool1] = localObject6;
      localObject6 = (CharSequence)localContext.getString(i3, arrayOfObject);
      ((z.f)localObject2).c((CharSequence)localObject6);
    }
    paramList = new c/l;
    paramList.<init>();
    throw paramList;
    label408:
    m = paramList.size();
    if (paramInt > m)
    {
      localObject3 = f;
      int i4 = R.string.voip_notification_missed_grouped_more;
      localObject8 = new Object[bool1];
      int i1 = paramList.size();
      paramInt -= i1;
      localObject9 = Integer.valueOf(paramInt);
      localObject8[0] = localObject9;
      localObject9 = (CharSequence)((Context)localObject3).getString(i4, (Object[])localObject8);
      ((z.f)localObject2).c((CharSequence)localObject9);
    }
    paramList = (c.a)m.d(paramList);
    long l3 = d;
    Object localObject8 = c();
    localObject1 = (CharSequence)localObject1;
    localObject1 = ((z.d)localObject8).a((CharSequence)localObject1).b((CharSequence)localObject4);
    localObject4 = d;
    if (localObject4 == null)
    {
      localObject8 = "missedCallIntentProvider";
      k.a((String)localObject8);
    }
    localObject4 = ((af)localObject4).a();
    localObject1 = ((z.d)localObject1).a((PendingIntent)localObject4);
    localObject4 = d;
    if (localObject4 == null)
    {
      localObject8 = "missedCallIntentProvider";
      k.a((String)localObject8);
    }
    paramList = ((af)localObject4).a(l3);
    paramList = ((z.d)localObject1).b(paramList).a();
    localObject2 = (z.g)localObject2;
    paramList = paramList.a((z.g)localObject2).h();
    Object localObject9 = i.f(f);
    int i5 = R.id.voip_incoming_service_missed_call_notification;
    ((NotificationManager)localObject9).notify(i5, paramList);
  }
  
  public final void b()
  {
    NotificationManager localNotificationManager = i.f(f);
    int i = R.id.voip_incoming_service_missed_call_notification;
    localNotificationManager.cancel(i);
  }
  
  public final void onStopped()
  {
    super.onStopped();
    Object localObject = this;
    localObject = b;
    if (localObject != null)
    {
      localObject = b;
      if (localObject == null)
      {
        String str = "presenter";
        k.a(str);
      }
      ((c.c)localObject).y_();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.MissedVoipCallsWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */