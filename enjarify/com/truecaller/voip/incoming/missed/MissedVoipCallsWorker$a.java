package com.truecaller.voip.incoming.missed;

import androidx.work.g;
import androidx.work.k.a;
import androidx.work.p;
import java.util.concurrent.TimeUnit;

public final class MissedVoipCallsWorker$a
{
  public static void a()
  {
    Object localObject1 = new androidx/work/k$a;
    ((k.a)localObject1).<init>(MissedVoipCallsWorker.class);
    Object localObject2 = TimeUnit.MILLISECONDS;
    localObject1 = ((k.a)localObject1).a(300L, (TimeUnit)localObject2).c();
    c.g.b.k.a(localObject1, "OneTimeWorkRequest.Build…\n                .build()");
    localObject1 = (androidx.work.k)localObject1;
    localObject2 = p.a();
    c.g.b.k.a(localObject2, "WorkManager.getInstance()");
    g localg = g.a;
    ((p)localObject2).a("com.truecaller.voip.incoming.missed.MissedVoipCallsWorker", localg, (androidx.work.k)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.MissedVoipCallsWorker.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */