package com.truecaller.voip.incoming.missed;

import android.graphics.Bitmap;
import java.util.List;

public abstract interface c$d
{
  public abstract void a(c.a parama, Bitmap paramBitmap);
  
  public abstract void a(List paramList, int paramInt);
  
  public abstract void b();
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */