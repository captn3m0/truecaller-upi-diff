package com.truecaller.voip.incoming.missed;

import c.g.b.k;

public final class c$a
{
  final String a;
  final String b;
  final String c;
  final long d;
  
  public c$a(String paramString1, String paramString2, String paramString3, long paramLong)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramLong;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof a;
      if (bool2)
      {
        paramObject = (a)paramObject;
        String str1 = a;
        String str2 = a;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          str1 = b;
          str2 = b;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            str1 = c;
            str2 = c;
            bool2 = k.a(str1, str2);
            if (bool2)
            {
              long l1 = d;
              long l2 = d;
              boolean bool3 = l1 < l2;
              if (!bool3)
              {
                bool3 = true;
              }
              else
              {
                bool3 = false;
                paramObject = null;
              }
              if (bool3) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str1 = a;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = b;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = c;
    if (str2 != null) {
      i = str2.hashCode();
    }
    j = (j + i) * 31;
    long l1 = d;
    long l2 = l1 >>> 32;
    int k = (int)(l1 ^ l2);
    return j + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("MissedVoipCall(name=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", number=");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", pictureUrl=");
    str = c;
    localStringBuilder.append(str);
    localStringBuilder.append(", timestamp=");
    long l = d;
    localStringBuilder.append(l);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */