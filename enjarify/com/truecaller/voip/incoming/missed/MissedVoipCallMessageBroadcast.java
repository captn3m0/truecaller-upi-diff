package com.truecaller.voip.incoming.missed;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.string;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.z;

public final class MissedVoipCallMessageBroadcast
  extends BroadcastReceiver
{
  public static final MissedVoipCallMessageBroadcast.a b;
  public z a;
  
  static
  {
    MissedVoipCallMessageBroadcast.a locala = new com/truecaller/voip/incoming/missed/MissedVoipCallMessageBroadcast$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if ((paramContext != null) && (paramIntent != null))
    {
      Object localObject = j.a;
      j.a.a().a(this);
      localObject = i.f(paramContext);
      int i = R.id.voip_incoming_service_missed_call_notification;
      ((NotificationManager)localObject).cancel(i);
      localObject = new android/content/Intent;
      String str = "android.intent.action.CLOSE_SYSTEM_DIALOGS";
      ((Intent)localObject).<init>(str);
      paramContext.sendBroadcast((Intent)localObject);
      paramContext = "com.truecaller.voip.extra.EXTRA_NUMBER";
      boolean bool = paramIntent.hasExtra(paramContext);
      if (bool)
      {
        paramContext = a;
        if (paramContext == null)
        {
          localObject = "voipCallMessage";
          k.a((String)localObject);
        }
        paramIntent = paramIntent.getStringExtra("com.truecaller.voip.extra.EXTRA_NUMBER");
        k.a(paramIntent, "intent.getStringExtra(EXTRA_NUMBER)");
        int j = R.string.voip_empty;
        localObject = Integer.valueOf(j);
        paramContext.a(paramIntent, (Integer)localObject);
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.MissedVoipCallMessageBroadcast
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */