package com.truecaller.voip.incoming.missed;

import androidx.work.ListenableWorker.a;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.voip.util.w;
import java.util.concurrent.CancellationException;

public final class d
  extends ba
  implements c.c
{
  private final c.d.f c;
  private final c.b d;
  private final w e;
  
  public d(c.d.f paramf1, c.d.f paramf2, c.b paramb, w paramw)
  {
    super(paramf1);
    c = paramf2;
    d = paramb;
    e = paramw;
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject1 = "Checking missed voip calls";
    try
    {
      new String[1][0] = localObject1;
      localObject1 = V_();
      Object localObject2 = new com/truecaller/voip/incoming/missed/d$c;
      ((d.c)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      localObject1 = kotlinx.coroutines.f.a((c.d.f)localObject1, (m)localObject2);
      localObject1 = (ListenableWorker.a)localObject1;
    }
    catch (CancellationException localCancellationException)
    {
      localObject1 = ListenableWorker.a.a();
    }
    k.a(localObject1, "try {\n        TLog.d(\"Ch…   Result.success()\n    }");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */