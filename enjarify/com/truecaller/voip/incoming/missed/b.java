package com.truecaller.voip.incoming.missed;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import c.d.c;
import c.g.b.k;
import c.o.b;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.h;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class b
  implements c.b
{
  private final Context a;
  
  public b(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Object a(c paramc)
  {
    boolean bool1 = paramc instanceof b.a;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (b.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/voip/incoming/missed/b$a;
    ((b.a)localObject1).<init>(this, (c)paramc);
    label70:
    paramc = a;
    Object localObject4 = c.d.a.a.a;
    int j = b;
    int m = 1;
    c localc = null;
    boolean bool2;
    Object localObject5;
    Object localObject6;
    Object localObject7;
    switch (j)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 1: 
      bool1 = paramc instanceof o.b;
      if (bool1) {
        try
        {
          paramc = (o.b)paramc;
          paramc = a;
          throw paramc;
        }
        catch (SQLException paramc) {}
      }
      break;
    case 0: 
      bool2 = paramc instanceof o.b;
      if (bool2) {
        break label613;
      }
      paramc = a;
      paramc = paramc.getContentResolver();
      localObject5 = "context.contentResolver";
      k.a(paramc, (String)localObject5);
      localObject5 = TruecallerContract.n.d();
      localObject6 = "HistoryTable.getContentWithAggregatedContactUri()";
      k.a(localObject5, (String)localObject6);
      localObject6 = "subscription_component_name='com.truecaller.voip.manager.VOIP' AND type=3 AND new=1 AND normalized_number IS NOT NULL";
      localObject7 = "timestamp DESC LIMIT 100";
      d = this;
      b = m;
      paramc = h.b(paramc, (Uri)localObject5, (String)localObject6, (String)localObject7, (c)localObject1);
      if (paramc == localObject4) {
        return localObject4;
      }
      break;
    }
    paramc = (Cursor)paramc;
    if (paramc != null)
    {
      localObject1 = new com/truecaller/voip/incoming/missed/a;
      ((a)localObject1).<init>(paramc);
      localObject1 = (Cursor)localObject1;
      paramc = (c)localObject1;
      paramc = (Closeable)localObject1;
      try
      {
        localObject4 = new java/util/ArrayList;
        ((ArrayList)localObject4).<init>();
        localObject4 = (Collection)localObject4;
        for (;;)
        {
          bool2 = ((Cursor)localObject1).moveToNext();
          if (!bool2) {
            break;
          }
          localObject5 = localObject1;
          localObject5 = (a)localObject1;
          localObject6 = ((a)localObject5).a();
          localObject7 = ((a)localObject5).a();
          localObject7 = (CharSequence)localObject7;
          if (localObject7 != null)
          {
            bool3 = c.n.m.a((CharSequence)localObject7);
            if (!bool3)
            {
              bool3 = false;
              localObject7 = null;
              break label365;
            }
          }
          boolean bool3 = true;
          label365:
          bool3 ^= m;
          if (!bool3) {
            localObject6 = null;
          }
          if (localObject6 == null) {
            localObject6 = ((a)localObject5).b();
          }
          localObject7 = localObject6;
          c.a locala = new com/truecaller/voip/incoming/missed/c$a;
          String str = ((a)localObject5).b();
          localObject6 = b;
          Object localObject8 = localObject5;
          localObject8 = (Cursor)localObject5;
          Object localObject9 = a.a;
          int n = 2;
          localObject9 = localObject9[n];
          localObject6 = ((com.truecaller.utils.extensions.g)localObject6).a((Cursor)localObject8, (c.l.g)localObject9);
          localObject8 = localObject6;
          localObject8 = (String)localObject6;
          localObject6 = c;
          localObject5 = (Cursor)localObject5;
          localObject9 = a.a;
          n = 3;
          localObject9 = localObject9[n];
          localObject5 = ((com.truecaller.utils.extensions.g)localObject6).a((Cursor)localObject5, (c.l.g)localObject9);
          localObject5 = (Number)localObject5;
          long l = ((Number)localObject5).longValue();
          localObject6 = locala;
          locala.<init>((String)localObject7, str, (String)localObject8, l);
          ((Collection)localObject4).add(locala);
        }
        localObject4 = (List)localObject4;
        c.f.b.a(paramc, null);
        localObject4 = (Iterable)localObject4;
        paramc = c.a.m.g((Iterable)localObject4);
        localc = paramc;
      }
      finally
      {
        try
        {
          throw ((Throwable)localObject2);
        }
        finally
        {
          localObject4 = localObject2;
          Object localObject3 = localObject10;
          c.f.b.a(paramc, (Throwable)localObject4);
        }
      }
      paramc = (Throwable)paramc;
      AssertionUtil.reportThrowableButNeverCrash(paramc);
    }
    return localc;
    label613:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */