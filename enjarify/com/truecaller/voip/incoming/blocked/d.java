package com.truecaller.voip.incoming.blocked;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import c.d.c;
import c.f.b;
import c.g.b.k;
import c.o.b;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.h;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class d
  implements b.c
{
  private final Context a;
  
  public d(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Object a(c paramc)
  {
    boolean bool1 = paramc instanceof d.a;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (d.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/voip/incoming/blocked/d$a;
    ((d.a)localObject1).<init>(this, (c)paramc);
    label70:
    paramc = a;
    Object localObject4 = c.d.a.a.a;
    int j = b;
    int m = 1;
    c localc = null;
    boolean bool2;
    Object localObject5;
    String str1;
    Object localObject6;
    switch (j)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 1: 
      bool1 = paramc instanceof o.b;
      if (bool1) {
        try
        {
          paramc = (o.b)paramc;
          paramc = a;
          throw paramc;
        }
        catch (SQLException paramc) {}
      }
      break;
    case 0: 
      bool2 = paramc instanceof o.b;
      if (bool2) {
        break label548;
      }
      paramc = a;
      paramc = paramc.getContentResolver();
      localObject5 = "context.contentResolver";
      k.a(paramc, (String)localObject5);
      localObject5 = TruecallerContract.n.d();
      str1 = "HistoryTable.getContentWithAggregatedContactUri()";
      k.a(localObject5, str1);
      str1 = "subscription_component_name='com.truecaller.voip.manager.VOIP' AND type=3 AND action=1 AND new=1 AND normalized_number IS NOT NULL";
      localObject6 = "timestamp DESC LIMIT 100";
      d = this;
      b = m;
      paramc = h.b(paramc, (Uri)localObject5, str1, (String)localObject6, (c)localObject1);
      if (paramc == localObject4) {
        return localObject4;
      }
      break;
    }
    paramc = (Cursor)paramc;
    if (paramc != null)
    {
      localObject1 = new com/truecaller/voip/incoming/blocked/a;
      ((a)localObject1).<init>(paramc);
      localObject1 = (Cursor)localObject1;
      paramc = (c)localObject1;
      paramc = (Closeable)localObject1;
      try
      {
        localObject4 = new java/util/ArrayList;
        ((ArrayList)localObject4).<init>();
        localObject4 = (Collection)localObject4;
        for (;;)
        {
          bool2 = ((Cursor)localObject1).moveToNext();
          if (!bool2) {
            break;
          }
          localObject5 = localObject1;
          localObject5 = (a)localObject1;
          str1 = ((a)localObject5).a();
          localObject6 = ((a)localObject5).a();
          localObject6 = (CharSequence)localObject6;
          if (localObject6 != null)
          {
            bool3 = c.n.m.a((CharSequence)localObject6);
            if (!bool3)
            {
              bool3 = false;
              localObject6 = null;
              break label365;
            }
          }
          boolean bool3 = true;
          label365:
          bool3 ^= m;
          if (!bool3) {
            str1 = null;
          }
          if (str1 == null) {
            str1 = ((a)localObject5).b();
          }
          localObject6 = new com/truecaller/voip/incoming/blocked/b$a;
          String str2 = ((a)localObject5).b();
          com.truecaller.utils.extensions.g localg = b;
          localObject5 = (Cursor)localObject5;
          Object localObject7 = a.a;
          int n = 2;
          localObject7 = localObject7[n];
          localObject5 = localg.a((Cursor)localObject5, (c.l.g)localObject7);
          localObject5 = (Number)localObject5;
          long l = ((Number)localObject5).longValue();
          ((b.a)localObject6).<init>(str1, str2, l);
          ((Collection)localObject4).add(localObject6);
        }
        localObject4 = (List)localObject4;
        b.a(paramc, null);
        localObject4 = (Iterable)localObject4;
        paramc = c.a.m.g((Iterable)localObject4);
        localc = paramc;
      }
      finally
      {
        try
        {
          throw ((Throwable)localObject2);
        }
        finally
        {
          localObject4 = localObject2;
          Object localObject3 = localObject8;
          b.a(paramc, (Throwable)localObject4);
        }
      }
      paramc = (Throwable)paramc;
      AssertionUtil.reportThrowableButNeverCrash(paramc);
    }
    return localc;
    label548:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.blocked.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */