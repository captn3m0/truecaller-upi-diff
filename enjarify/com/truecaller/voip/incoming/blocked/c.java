package com.truecaller.voip.incoming.blocked;

import androidx.work.ListenableWorker.a;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import java.util.concurrent.CancellationException;

public final class c
  extends ba
  implements b.b
{
  private final b.c c;
  private final c.d.f d;
  private final c.d.f e;
  
  public c(b.c paramc, c.d.f paramf1, c.d.f paramf2)
  {
    super(paramf1);
    c = paramc;
    d = paramf1;
    e = paramf2;
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject;
    try
    {
      localObject = new com/truecaller/voip/incoming/blocked/c$a;
      ((c.a)localObject).<init>(this, null);
      localObject = (m)localObject;
      localObject = kotlinx.coroutines.f.a((m)localObject);
      localObject = (ListenableWorker.a)localObject;
    }
    catch (CancellationException localCancellationException)
    {
      localObject = ListenableWorker.a.a();
    }
    k.a(localObject, "try {\n        runBlockin…   Result.success()\n    }");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.blocked.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */