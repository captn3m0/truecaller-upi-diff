package com.truecaller.voip.incoming.blocked;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;

public final class a
  implements Cursor
{
  final com.truecaller.utils.extensions.g b;
  private final com.truecaller.utils.extensions.g c;
  private final com.truecaller.utils.extensions.g d;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[3];
    Object localObject = new c/g/b/u;
    b localb = w.a(a.class);
    ((u)localObject).<init>(localb, "name", "getName()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "number", "getNumber()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "timestamp", "getTimestamp()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[2] = localObject;
    a = arrayOfg;
  }
  
  public a(Cursor paramCursor)
  {
    e = paramCursor;
    Object localObject1 = new com/truecaller/utils/extensions/g;
    Object localObject2 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject1).<init>("contact_name", (b)localObject2, null);
    c = ((com.truecaller.utils.extensions.g)localObject1);
    localObject1 = new com/truecaller/utils/extensions/g;
    localObject2 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject1).<init>("normalized_number", (b)localObject2, null);
    d = ((com.truecaller.utils.extensions.g)localObject1);
    localObject1 = Long.valueOf(0L);
    localObject2 = new com/truecaller/utils/extensions/g;
    b localb = w.a(Long.class);
    ((com.truecaller.utils.extensions.g)localObject2).<init>("timestamp", localb, localObject1);
    b = ((com.truecaller.utils.extensions.g)localObject2);
  }
  
  public final String a()
  {
    com.truecaller.utils.extensions.g localg = c;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[0];
    return (String)localg.a((Cursor)localObject, localg1);
  }
  
  public final String b()
  {
    com.truecaller.utils.extensions.g localg = d;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[1];
    return (String)localg.a((Cursor)localObject, localg1);
  }
  
  public final void close()
  {
    e.close();
  }
  
  public final void copyStringToBuffer(int paramInt, CharArrayBuffer paramCharArrayBuffer)
  {
    e.copyStringToBuffer(paramInt, paramCharArrayBuffer);
  }
  
  public final void deactivate()
  {
    e.deactivate();
  }
  
  public final byte[] getBlob(int paramInt)
  {
    return e.getBlob(paramInt);
  }
  
  public final int getColumnCount()
  {
    return e.getColumnCount();
  }
  
  public final int getColumnIndex(String paramString)
  {
    return e.getColumnIndex(paramString);
  }
  
  public final int getColumnIndexOrThrow(String paramString)
  {
    return e.getColumnIndexOrThrow(paramString);
  }
  
  public final String getColumnName(int paramInt)
  {
    return e.getColumnName(paramInt);
  }
  
  public final String[] getColumnNames()
  {
    return e.getColumnNames();
  }
  
  public final int getCount()
  {
    return e.getCount();
  }
  
  public final double getDouble(int paramInt)
  {
    return e.getDouble(paramInt);
  }
  
  public final Bundle getExtras()
  {
    return e.getExtras();
  }
  
  public final float getFloat(int paramInt)
  {
    return e.getFloat(paramInt);
  }
  
  public final int getInt(int paramInt)
  {
    return e.getInt(paramInt);
  }
  
  public final long getLong(int paramInt)
  {
    return e.getLong(paramInt);
  }
  
  public final Uri getNotificationUri()
  {
    return e.getNotificationUri();
  }
  
  public final int getPosition()
  {
    return e.getPosition();
  }
  
  public final short getShort(int paramInt)
  {
    return e.getShort(paramInt);
  }
  
  public final String getString(int paramInt)
  {
    return e.getString(paramInt);
  }
  
  public final int getType(int paramInt)
  {
    return e.getType(paramInt);
  }
  
  public final boolean getWantsAllOnMoveCalls()
  {
    return e.getWantsAllOnMoveCalls();
  }
  
  public final boolean isAfterLast()
  {
    return e.isAfterLast();
  }
  
  public final boolean isBeforeFirst()
  {
    return e.isBeforeFirst();
  }
  
  public final boolean isClosed()
  {
    return e.isClosed();
  }
  
  public final boolean isFirst()
  {
    return e.isFirst();
  }
  
  public final boolean isLast()
  {
    return e.isLast();
  }
  
  public final boolean isNull(int paramInt)
  {
    return e.isNull(paramInt);
  }
  
  public final boolean move(int paramInt)
  {
    return e.move(paramInt);
  }
  
  public final boolean moveToFirst()
  {
    return e.moveToFirst();
  }
  
  public final boolean moveToLast()
  {
    return e.moveToLast();
  }
  
  public final boolean moveToNext()
  {
    return e.moveToNext();
  }
  
  public final boolean moveToPosition(int paramInt)
  {
    return e.moveToPosition(paramInt);
  }
  
  public final boolean moveToPrevious()
  {
    return e.moveToPrevious();
  }
  
  public final void registerContentObserver(ContentObserver paramContentObserver)
  {
    e.registerContentObserver(paramContentObserver);
  }
  
  public final void registerDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    e.registerDataSetObserver(paramDataSetObserver);
  }
  
  public final boolean requery()
  {
    return e.requery();
  }
  
  public final Bundle respond(Bundle paramBundle)
  {
    return e.respond(paramBundle);
  }
  
  public final void setExtras(Bundle paramBundle)
  {
    e.setExtras(paramBundle);
  }
  
  public final void setNotificationUri(ContentResolver paramContentResolver, Uri paramUri)
  {
    e.setNotificationUri(paramContentResolver, paramUri);
  }
  
  public final void unregisterContentObserver(ContentObserver paramContentObserver)
  {
    e.unregisterContentObserver(paramContentObserver);
  }
  
  public final void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    e.unregisterDataSetObserver(paramDataSetObserver);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.blocked.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */