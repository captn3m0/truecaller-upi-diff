package com.truecaller.voip.incoming.blocked;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.z.d;
import android.support.v4.app.z.f;
import android.support.v4.app.z.g;
import android.text.format.DateUtils;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import c.a.m;
import c.g.b.k;
import c.g.b.z;
import c.l;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.string;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.af;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public final class VoipBlockedCallsWorker
  extends Worker
  implements b.d
{
  public static final VoipBlockedCallsWorker.a e;
  public b.b b;
  public com.truecaller.notificationchannels.b c;
  public af d;
  private final Context f;
  
  static
  {
    VoipBlockedCallsWorker.a locala = new com/truecaller/voip/incoming/blocked/VoipBlockedCallsWorker$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public VoipBlockedCallsWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    f = paramContext;
  }
  
  private final z.d c()
  {
    z.d locald = new android/support/v4/app/z$d;
    Context localContext = f;
    Object localObject = c;
    if (localObject == null)
    {
      String str = "notificationChannelProvider";
      k.a(str);
    }
    localObject = ((com.truecaller.notificationchannels.b)localObject).g();
    locald.<init>(localContext, (String)localObject);
    locald = locald.c(4);
    localContext = f;
    int i = R.color.truecaller_blue_all_themes;
    int j = android.support.v4.content.b.c(localContext, i);
    locald = locald.f(j);
    j = R.drawable.ic_notification_blocked_call;
    return locald.a(j).e();
  }
  
  public final ListenableWorker.a a()
  {
    boolean bool = isStopped();
    if (bool)
    {
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    Object localObject = com.truecaller.voip.j.a;
    j.a.a().a(this);
    localObject = b;
    String str;
    if (localObject == null)
    {
      str = "presenter";
      k.a(str);
    }
    ((b.b)localObject).a(this);
    localObject = b;
    if (localObject == null)
    {
      str = "presenter";
      k.a(str);
    }
    return ((b.b)localObject).a();
  }
  
  public final void a(b.a parama)
  {
    k.b(parama, "blockedCall");
    Object localObject1 = z.a;
    localObject1 = f;
    int i = R.string.voip_notification_blocked_calls_single_content;
    localObject1 = ((Context)localObject1).getString(i);
    k.a(localObject1, "context.getString(R.stri…ked_calls_single_content)");
    i = 2;
    Object localObject2 = new Object[i];
    String str = a;
    localObject2[0] = str;
    str = b;
    int j = 1;
    localObject2[j] = str;
    Object localObject3 = Arrays.copyOf((Object[])localObject2, i);
    localObject1 = String.format((String)localObject1, (Object[])localObject3);
    k.a(localObject1, "java.lang.String.format(format, *args)");
    localObject3 = c();
    long l1 = c;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      l1 = c;
      ((z.d)localObject3).a(l1);
    }
    localObject2 = f;
    int k = R.string.voip_notification_blocked_calls_single_title;
    localObject2 = (CharSequence)((Context)localObject2).getString(k);
    localObject3 = ((z.d)localObject3).a((CharSequence)localObject2);
    localObject1 = (CharSequence)localObject1;
    localObject1 = ((z.d)localObject3).b((CharSequence)localObject1);
    localObject3 = f.getResources();
    int m = R.drawable.ic_notification_call_blocked_standard;
    localObject3 = BitmapFactory.decodeResource((Resources)localObject3, m);
    localObject1 = ((z.d)localObject1).a((Bitmap)localObject3);
    localObject3 = d;
    if (localObject3 == null)
    {
      localObject2 = "intentProvider";
      k.a((String)localObject2);
    }
    localObject3 = ((af)localObject3).a();
    localObject1 = ((z.d)localObject1).a((PendingIntent)localObject3);
    localObject3 = d;
    if (localObject3 == null)
    {
      localObject2 = "intentProvider";
      k.a((String)localObject2);
    }
    l1 = c;
    parama = ((af)localObject3).a(l1);
    parama = ((z.d)localObject1).b(parama).h();
    localObject1 = i.f(f);
    i = R.id.voip_blocked_call_notification;
    ((NotificationManager)localObject1).notify(i, parama);
  }
  
  public final void a(List paramList, int paramInt)
  {
    k.b(paramList, "blockedCallsToShow");
    Object localObject1 = f;
    int i = R.string.voip_notification_blocked_calls_grouped_content;
    boolean bool1 = true;
    Object localObject2 = new Object[bool1];
    Object localObject3 = Integer.valueOf(paramInt);
    localObject2[0] = localObject3;
    localObject1 = ((Context)localObject1).getString(i, (Object[])localObject2);
    i = paramList.size();
    if (paramInt > i)
    {
      localObject4 = f;
      int j = R.string.voip_notification_blocked_calls_grouped_summary;
      localObject3 = new Object[bool1];
      int m = paramList.size();
      paramInt -= m;
      localObject5 = Integer.valueOf(paramInt);
      localObject3[0] = localObject5;
      localObject5 = ((Context)localObject4).getString(j, (Object[])localObject3);
    }
    else
    {
      localObject5 = "";
    }
    Object localObject4 = new android/support/v4/app/z$f;
    ((z.f)localObject4).<init>();
    localObject1 = (CharSequence)localObject1;
    ((z.f)localObject4).a((CharSequence)localObject1);
    Object localObject5 = (CharSequence)localObject5;
    ((z.f)localObject4).b((CharSequence)localObject5);
    localObject5 = paramList;
    localObject5 = ((Iterable)paramList).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject5).hasNext();
      if (!bool2) {
        break label374;
      }
      localObject2 = (b.a)((Iterator)localObject5).next();
      long l1 = c;
      boolean bool3 = DateUtils.isToday(l1);
      if (bool3 == bool1)
      {
        localObject3 = f;
        l1 = c;
        localObject3 = com.truecaller.common.h.j.f((Context)localObject3, l1);
      }
      else
      {
        if (bool3) {
          break;
        }
        localObject3 = f;
        l1 = c;
        localObject3 = com.truecaller.common.h.j.e((Context)localObject3, l1);
      }
      k.a(localObject3, "when (DateUtils.isToday(….timestamp)\n            }");
      Context localContext = f;
      int i1 = R.string.voip_notification_blocked_calls_grouped_caller;
      int i2 = 3;
      Object[] arrayOfObject = new Object[i2];
      arrayOfObject[0] = localObject3;
      localObject3 = a;
      arrayOfObject[bool1] = localObject3;
      int n = 2;
      localObject2 = b;
      arrayOfObject[n] = localObject2;
      localObject2 = (CharSequence)localContext.getString(i1, arrayOfObject);
      ((z.f)localObject4).c((CharSequence)localObject2);
    }
    paramList = new c/l;
    paramList.<init>();
    throw paramList;
    label374:
    localObject5 = c();
    Object localObject6 = f;
    int k = R.string.voip_notification_blocked_calls_grouped_title;
    localObject6 = (CharSequence)((Context)localObject6).getString(k);
    localObject5 = ((z.d)localObject5).a((CharSequence)localObject6).b((CharSequence)localObject1);
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject6 = "intentProvider";
      k.a((String)localObject6);
    }
    localObject1 = ((af)localObject1).a();
    localObject5 = ((z.d)localObject5).a((PendingIntent)localObject1);
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject6 = "intentProvider";
      k.a((String)localObject6);
    }
    long l2 = dc;
    paramList = ((af)localObject1).a(l2);
    paramList = ((z.d)localObject5).b(paramList).a();
    localObject4 = (z.g)localObject4;
    paramList = paramList.a((z.g)localObject4).h();
    localObject5 = i.f(f);
    int i3 = R.id.voip_blocked_call_notification;
    ((NotificationManager)localObject5).notify(i3, paramList);
  }
  
  public final void b()
  {
    NotificationManager localNotificationManager = i.f(f);
    int i = R.id.voip_blocked_call_notification;
    localNotificationManager.cancel(i);
  }
  
  public final void onStopped()
  {
    super.onStopped();
    Object localObject = this;
    localObject = b;
    if (localObject != null)
    {
      localObject = b;
      if (localObject == null)
      {
        String str = "presenter";
        k.a(str);
      }
      ((b.b)localObject).y_();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.blocked.VoipBlockedCallsWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */