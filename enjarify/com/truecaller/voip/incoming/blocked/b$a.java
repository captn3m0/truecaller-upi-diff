package com.truecaller.voip.incoming.blocked;

import c.g.b.k;

public final class b$a
{
  final String a;
  final String b;
  final long c;
  
  public b$a(String paramString1, String paramString2, long paramLong)
  {
    a = paramString1;
    b = paramString2;
    c = paramLong;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof a;
      if (bool2)
      {
        paramObject = (a)paramObject;
        String str1 = a;
        String str2 = a;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          str1 = b;
          str2 = b;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            long l1 = c;
            long l2 = c;
            boolean bool3 = l1 < l2;
            if (!bool3)
            {
              bool3 = true;
            }
            else
            {
              bool3 = false;
              paramObject = null;
            }
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str1 = a;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = b;
    if (str2 != null) {
      i = str2.hashCode();
    }
    int j = (j + i) * 31;
    long l1 = c;
    long l2 = l1 >>> 32;
    int k = (int)(l1 ^ l2);
    return j + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BlockedVoipCall(name=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", number=");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", timestamp=");
    long l = c;
    localStringBuilder.append(l);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.blocked.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */