package com.truecaller.voip.incoming.ui;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import c.g.b.k;

public final class a$a
  implements ServiceConnection
{
  a$a(a parama) {}
  
  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    k.b(paramIBinder, "binder");
    paramIBinder = (com.truecaller.voip.incoming.a)paramIBinder;
    paramComponentName = a.a();
    paramIBinder = a;
    paramComponentName.a(paramIBinder);
  }
  
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    a.a().h();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.ui.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */