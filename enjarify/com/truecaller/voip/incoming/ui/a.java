package com.truecaller.voip.incoming.ui;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.constraint.motion.MotionLayout;
import android.support.constraint.motion.MotionLayout.d;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.content.b;
import android.support.v4.view.r;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import c.a.m;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.tcpermissions.l;
import com.truecaller.utils.extensions.i;
import com.truecaller.utils.extensions.t;
import com.truecaller.voip.R.anim;
import com.truecaller.voip.R.array;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.layout;
import com.truecaller.voip.R.string;
import com.truecaller.voip.incoming.IncomingVoipService;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import java.util.HashMap;
import java.util.List;

public final class a
  extends Fragment
  implements b.b
{
  public b.a a;
  public l b;
  private MotionLayout c;
  private FloatingActionButton d;
  private FloatingActionButton e;
  private FloatingActionButton f;
  private TextView g;
  private TextView h;
  private TextView i;
  private ImageView j;
  private ImageButton k;
  private TextView l;
  private View m;
  private View n;
  private View o;
  private ImageView p;
  private ImageView q;
  private ServiceConnection r;
  private HashMap s;
  
  public final b.a a()
  {
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(int paramInt)
  {
    ImageView localImageView = q;
    if (localImageView == null)
    {
      String str = "truecallerLogoView";
      k.a(str);
    }
    localImageView.setImageResource(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    String str1 = "context ?: return";
    k.a(localObject1, str1);
    paramInt2 = b.c((Context)localObject1, paramInt2);
    localObject1 = h;
    if (localObject1 == null)
    {
      str1 = "statusTextView";
      k.a(str1);
    }
    ((TextView)localObject1).setText(paramInt1);
    Object localObject2 = h;
    if (localObject2 == null)
    {
      localObject1 = "statusTextView";
      k.a((String)localObject1);
    }
    ((TextView)localObject2).setTextColor(paramInt2);
    localObject2 = p;
    String str2;
    if (localObject2 == null)
    {
      str2 = "callStateRingView";
      k.a(str2);
    }
    localObject2 = ((ImageView)localObject2).getDrawable();
    if (localObject2 != null)
    {
      localObject2 = (com.truecaller.voip.a.a)localObject2;
      if (paramBoolean)
      {
        ((com.truecaller.voip.a.a)localObject2).c();
      }
      else
      {
        paramInt2 = 0;
        str2 = null;
        ((com.truecaller.voip.a.a)localObject2).a(0);
      }
      localObject2 = p;
      if (localObject2 == null)
      {
        str2 = "callStateRingView";
        k.a(str2);
      }
      t.a((View)localObject2, paramBoolean);
      return;
    }
    localObject2 = new c/u;
    ((u)localObject2).<init>("null cannot be cast to non-null type com.truecaller.voip.view.CallStateAvatarRingDrawable");
    throw ((Throwable)localObject2);
  }
  
  final void a(Bundle paramBundle)
  {
    b.a locala = a;
    if (locala == null)
    {
      str1 = "presenter";
      k.a(str1);
    }
    boolean bool1 = false;
    String str1 = null;
    String str2;
    boolean bool2;
    if (paramBundle != null)
    {
      str2 = "com.truecaller.voip.incoming.ui.EXTRA_ACCEPT_CALL";
      bool2 = paramBundle.getBoolean(str2, false);
    }
    else
    {
      bool2 = false;
      str2 = null;
    }
    if (paramBundle != null)
    {
      String str3 = "com.truecaller.voip.incoming.ui.EXTRA_VOIP_NOTIFICATION_ACTION";
      bool1 = paramBundle.getBoolean(str3, false);
    }
    locala.a(bool2, bool1);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "profileName");
    Object localObject = g;
    if (localObject == null)
    {
      String str = "profileNameTextView";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    ((TextView)localObject).setText(paramString);
    paramString = g;
    if (paramString == null)
    {
      localObject = "profileNameTextView";
      k.a((String)localObject);
    }
    paramString.setSelected(true);
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = "activity ?: return";
    k.a(localObject1, (String)localObject2);
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 21;
    if (i1 >= i2)
    {
      localObject2 = ((f)localObject1).getWindow();
      k.a(localObject2, "context.window");
      localObject3 = localObject1;
      localObject3 = (Context)localObject1;
      i3 = R.color.voip_status_bar_spam_color;
      i2 = b.c((Context)localObject3, i3);
      ((Window)localObject2).setStatusBarColor(i2);
    }
    localObject2 = n;
    if (localObject2 == null)
    {
      localObject3 = "headerCoverView";
      k.a((String)localObject3);
    }
    localObject1 = (Context)localObject1;
    i2 = R.color.voip_spam_color;
    int i4 = b.c((Context)localObject1, i2);
    ((View)localObject2).setBackgroundColor(i4);
    localObject1 = o;
    if (localObject1 == null)
    {
      localObject2 = "headerArcView";
      k.a((String)localObject2);
    }
    i1 = R.drawable.background_voip_spam_header_view;
    ((View)localObject1).setBackgroundResource(i1);
    localObject1 = k;
    if (localObject1 == null)
    {
      localObject2 = "minimiseButton";
      k.a((String)localObject2);
    }
    i1 = R.drawable.background_voip_minimise_spam_call;
    ((ImageButton)localObject1).setBackgroundResource(i1);
    localObject1 = j;
    if (localObject1 == null)
    {
      localObject2 = "profilePictureImageView";
      k.a((String)localObject2);
    }
    i1 = 0;
    localObject2 = null;
    ((ImageView)localObject1).setImageDrawable(null);
    localObject1 = j;
    if (localObject1 == null)
    {
      localObject2 = "profilePictureImageView";
      k.a((String)localObject2);
    }
    i1 = R.drawable.ic_avatar_voip_spam;
    ((ImageView)localObject1).setImageResource(i1);
    localObject1 = i;
    if (localObject1 == null)
    {
      localObject2 = "spamScoreTextView";
      k.a((String)localObject2);
    }
    i1 = R.string.voip_spam_reports_score;
    i2 = 1;
    Object localObject3 = new Object[i2];
    int i3 = 0;
    Object localObject4 = Integer.valueOf(paramInt);
    localObject3[0] = localObject4;
    localObject4 = (CharSequence)getString(i1, (Object[])localObject3);
    ((TextView)localObject1).setText((CharSequence)localObject4);
    localObject4 = i;
    if (localObject4 == null)
    {
      localObject1 = "spamScoreTextView";
      k.a((String)localObject1);
    }
    t.a((View)localObject4);
  }
  
  public final void b(String paramString)
  {
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    boolean bool = true;
    if (localObject != null)
    {
      i1 = ((CharSequence)localObject).length();
      if (i1 != 0)
      {
        i1 = 0;
        localObject = null;
        break label40;
      }
    }
    int i1 = 1;
    label40:
    if (i1 != 0) {
      return;
    }
    localObject = requireContext();
    String str1 = "requireContext()";
    k.a(localObject, str1);
    paramString = w.a((Context)localObject).a(paramString);
    i1 = R.drawable.ic_avatar_voip_default;
    paramString = paramString.a(i1);
    localObject = (ai)aq.d.b();
    paramString = paramString.a((ai)localObject);
    c = bool;
    paramString = paramString.b();
    localObject = j;
    if (localObject == null)
    {
      String str2 = "profilePictureImageView";
      k.a(str2);
    }
    paramString.a((ImageView)localObject, null);
  }
  
  public final boolean b()
  {
    l locall = b;
    if (locall == null)
    {
      String str = "tcPermissionsUtil";
      k.a(str);
    }
    return locall.h();
  }
  
  public final void c()
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    Object localObject2 = d;
    if (localObject2 == null)
    {
      String str = "acceptCallButton";
      k.a(str);
    }
    localObject2 = (View)localObject2;
    localObject1 = (Context)localObject1;
    int i1 = R.color.voip_action_end_call_background_color;
    int i3 = b.c((Context)localObject1, i1);
    localObject1 = ColorStateList.valueOf(i3);
    r.a((View)localObject2, (ColorStateList)localObject1);
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject2 = "acceptCallButton";
      k.a((String)localObject2);
    }
    int i4 = R.drawable.ic_button_voip_hangup;
    ((FloatingActionButton)localObject1).setImageResource(i4);
    i3 = R.string.voip_status_connecting;
    i4 = R.color.voip_call_status_warning_color;
    i1 = 1;
    a(i3, i4, i1);
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "motionLayoutView";
      k.a((String)localObject2);
    }
    ((MotionLayout)localObject1).c();
    localObject2 = null;
    ((MotionLayout)localObject1).setTransitionListener(null);
    i4 = R.id.incoming_call_accepted_start_set;
    int i2 = R.id.incoming_call_accepted_end_set;
    ((MotionLayout)localObject1).a(i4, i2);
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "motionLayoutView";
      k.a((String)localObject2);
    }
    ((MotionLayout)localObject1).c();
  }
  
  public final void c(String paramString)
  {
    String str = "message";
    k.b(paramString, str);
    paramString = l;
    if (paramString == null)
    {
      str = "logTextView";
      k.a(str);
    }
    t.b((View)paramString);
  }
  
  public final boolean d()
  {
    Object localObject = b;
    if (localObject == null)
    {
      String str1 = "tcPermissionsUtil";
      k.a(str1);
    }
    localObject = ((l)localObject).e();
    int i1 = localObject.length;
    int i2 = 0;
    while (i2 < i1)
    {
      str2 = localObject[i2];
      boolean bool = shouldShowRequestPermissionRationale(str2);
      if (bool) {
        break label70;
      }
      i2 += 1;
    }
    String str2 = null;
    label70:
    return str2 != null;
  }
  
  public final void e()
  {
    Object localObject = b;
    if (localObject == null)
    {
      String str = "tcPermissionsUtil";
      k.a(str);
    }
    localObject = ((l)localObject).e();
    requestPermissions((String[])localObject, 1000);
  }
  
  public final void f()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    int i1 = R.string.voip_permissions_denied_explanation;
    i.a(localContext, i1, null, 1, 2);
  }
  
  public final void g()
  {
    MotionLayout localMotionLayout = c;
    if (localMotionLayout == null)
    {
      String str = "motionLayoutView";
      k.a(str);
    }
    localMotionLayout.b();
  }
  
  public final void h()
  {
    Object localObject = getContext();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "context ?: return");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.settings.APPLICATION_DETAILS_SETTINGS");
    localObject = ((Context)localObject).getPackageName();
    localObject = Uri.fromParts("package", (String)localObject, null);
    localObject = localIntent.setData((Uri)localObject);
    startActivity((Intent)localObject);
  }
  
  public final void i()
  {
    Object localObject1 = new Integer[4];
    Object localObject2 = Integer.valueOf(R.string.voip_reject_message_first_option);
    localObject1[0] = localObject2;
    localObject2 = Integer.valueOf(R.string.voip_reject_message_second_option);
    localObject1[1] = localObject2;
    localObject2 = Integer.valueOf(R.string.voip_reject_message_third_option);
    localObject1[2] = localObject2;
    localObject2 = Integer.valueOf(R.string.voip_reject_message_custom_option);
    localObject1[3] = localObject2;
    localObject1 = m.b((Object[])localObject1);
    localObject2 = new android/app/AlertDialog$Builder;
    Context localContext = (Context)getActivity();
    ((AlertDialog.Builder)localObject2).<init>(localContext);
    int i1 = R.array.voip_button_message_options;
    Object localObject3 = new com/truecaller/voip/incoming/ui/a$f;
    ((a.f)localObject3).<init>(this, (List)localObject1);
    localObject3 = (DialogInterface.OnClickListener)localObject3;
    ((AlertDialog.Builder)localObject2).setItems(i1, (DialogInterface.OnClickListener)localObject3).show();
  }
  
  public final void j()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void k()
  {
    MotionLayout localMotionLayout = c;
    String str;
    if (localMotionLayout == null)
    {
      str = "motionLayoutView";
      k.a(str);
    }
    int i1 = R.id.incoming_call_ended_start_set;
    int i2 = R.id.incoming_call_ended_end_set;
    localMotionLayout.a(i1, i2);
    localMotionLayout = c;
    if (localMotionLayout == null)
    {
      str = "motionLayoutView";
      k.a(str);
    }
    localMotionLayout.c();
  }
  
  public final void l()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = j.a;
    j.a.a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i1 = R.layout.fragment_voip_incoming;
    paramLayoutInflater = paramLayoutInflater.inflate(i1, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…coming, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    Object localObject1 = a;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((b.a)localObject1).y_();
    localObject1 = r;
    if (localObject1 != null)
    {
      localObject2 = getContext();
      if (localObject2 != null) {
        ((Context)localObject2).unbindService((ServiceConnection)localObject1);
      }
    }
    super.onDestroyView();
    localObject1 = s;
    if (localObject1 != null) {
      ((HashMap)localObject1).clear();
    }
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    String str = "permissions";
    k.b(paramArrayOfString, str);
    paramArrayOfString = "grantResults";
    k.b(paramArrayOfInt, paramArrayOfString);
    int i1 = 1000;
    if (paramInt != i1) {
      return;
    }
    b.a locala = a;
    if (locala == null)
    {
      paramArrayOfString = "presenter";
      k.a(paramArrayOfString);
    }
    paramArrayOfString = b;
    if (paramArrayOfString == null)
    {
      paramArrayOfInt = "tcPermissionsUtil";
      k.a(paramArrayOfInt);
    }
    boolean bool = paramArrayOfString.h();
    locala.a(bool);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i1 = R.id.motion_layout;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.motion_layout)");
    paramBundle = (MotionLayout)paramBundle;
    c = paramBundle;
    i1 = R.id.button_accept_call;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_accept_call)");
    paramBundle = (FloatingActionButton)paramBundle;
    d = paramBundle;
    i1 = R.id.button_reject_call;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_reject_call)");
    paramBundle = (FloatingActionButton)paramBundle;
    e = paramBundle;
    i1 = R.id.button_message;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_message)");
    paramBundle = (FloatingActionButton)paramBundle;
    f = paramBundle;
    i1 = R.id.text_profile_name;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_profile_name)");
    paramBundle = (TextView)paramBundle;
    g = paramBundle;
    i1 = R.id.text_status;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_status)");
    paramBundle = (TextView)paramBundle;
    h = paramBundle;
    i1 = R.id.text_spam_score;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_spam_score)");
    paramBundle = (TextView)paramBundle;
    i = paramBundle;
    i1 = R.id.image_profile_picture;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.image_profile_picture)");
    paramBundle = (ImageView)paramBundle;
    j = paramBundle;
    i1 = R.id.button_minimise;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_minimise)");
    paramBundle = (ImageButton)paramBundle;
    k = paramBundle;
    i1 = R.id.text_log;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_log)");
    paramBundle = (TextView)paramBundle;
    l = paramBundle;
    i1 = R.id.view_answer_arrows;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.view_answer_arrows)");
    m = paramBundle;
    i1 = R.id.view_header_cover;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.view_header_cover)");
    n = paramBundle;
    i1 = R.id.view_header;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.view_header)");
    o = paramBundle;
    i1 = R.id.image_call_state_ring;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.image_call_state_ring)");
    paramBundle = (ImageView)paramBundle;
    p = paramBundle;
    i1 = R.id.image_logo;
    paramBundle = paramView.findViewById(i1);
    Object localObject = "view.findViewById(R.id.image_logo)";
    k.a(paramBundle, (String)localObject);
    paramBundle = (ImageView)paramBundle;
    q = paramBundle;
    paramBundle = p;
    if (paramBundle == null)
    {
      localObject = "callStateRingView";
      k.a((String)localObject);
    }
    localObject = new com/truecaller/voip/a/a;
    Context localContext = paramView.getContext();
    String str = "view.context";
    k.a(localContext, str);
    ((com.truecaller.voip.a.a)localObject).<init>(localContext);
    localObject = (Drawable)localObject;
    paramBundle.setImageDrawable((Drawable)localObject);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    paramBundle.a(this);
    paramBundle = getArguments();
    a(paramBundle);
    paramView = paramView.getContext();
    k.a(paramView, "view.context");
    paramBundle = new com/truecaller/voip/incoming/ui/a$a;
    paramBundle.<init>(this);
    paramBundle = (ServiceConnection)paramBundle;
    r = paramBundle;
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>(paramView, IncomingVoipService.class);
    localContext = null;
    paramView.bindService((Intent)localObject, paramBundle, 0);
    paramView = m;
    if (paramView == null)
    {
      paramBundle = "answerArrowsView";
      k.a(paramBundle);
    }
    paramBundle = requireContext();
    int i2 = R.anim.anim_voip_answer_arrows;
    paramBundle = AnimationUtils.loadAnimation(paramBundle, i2);
    paramView.startAnimation(paramBundle);
    paramView = e;
    if (paramView == null)
    {
      paramBundle = "rejectCallButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/voip/incoming/ui/a$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = f;
    if (paramView == null)
    {
      paramBundle = "rejectMessageButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/voip/incoming/ui/a$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = k;
    if (paramView == null)
    {
      paramBundle = "minimiseButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/voip/incoming/ui/a$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = c;
    if (paramView == null)
    {
      paramBundle = "motionLayoutView";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/voip/incoming/ui/a$e;
    paramBundle.<init>(this);
    paramBundle = (MotionLayout.d)paramBundle;
    paramView.setTransitionListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.ui.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */