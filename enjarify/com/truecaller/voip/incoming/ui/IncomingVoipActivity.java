package com.truecaller.voip.incoming.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v4.content.b;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Window;
import c.g.b.k;
import com.truecaller.voip.R.color;
import java.util.Iterator;
import java.util.List;

public final class IncomingVoipActivity
  extends AppCompatActivity
{
  public static final IncomingVoipActivity.a a;
  
  static
  {
    IncomingVoipActivity.a locala = new com/truecaller/voip/incoming/ui/IncomingVoipActivity$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public final void onBackPressed()
  {
    Object localObject1 = getSupportFragmentManager();
    Object localObject2 = "supportFragmentManager";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((j)localObject1).f().iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Fragment)((Iterator)localObject1).next();
      boolean bool2 = localObject2 instanceof a;
      if (bool2)
      {
        localObject2 = a;
        if (localObject2 == null)
        {
          String str = "presenter";
          k.a(str);
        }
        ((b.a)localObject2).g();
      }
    }
    super.onBackPressed();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this, true);
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      localObject1 = getWindow();
      k.a(localObject1, "window");
      Object localObject2 = this;
      localObject2 = (Context)this;
      int k = R.color.voip_status_bar_color;
      j = b.c((Context)localObject2, k);
      ((Window)localObject1).setStatusBarColor(j);
    }
    if (paramBundle != null) {
      return;
    }
    paramBundle = new com/truecaller/voip/incoming/ui/a;
    paramBundle.<init>();
    Object localObject1 = getIntent();
    if (localObject1 != null)
    {
      localObject1 = ((Intent)localObject1).getExtras();
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    paramBundle.setArguments((Bundle)localObject1);
    localObject1 = getSupportFragmentManager().a();
    paramBundle = (Fragment)paramBundle;
    ((o)localObject1).b(16908290, paramBundle, "IncomingVoipFragment").c();
  }
  
  public final boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    default: 
      return super.onKeyDown(paramInt, paramKeyEvent);
    }
    Object localObject = getSupportFragmentManager();
    paramKeyEvent = "IncomingVoipFragment";
    localObject = (a)((j)localObject).a(paramKeyEvent);
    if (localObject != null)
    {
      localObject = a;
      if (localObject == null)
      {
        paramKeyEvent = "presenter";
        k.a(paramKeyEvent);
      }
      ((b.a)localObject).e();
    }
    return true;
  }
  
  public final boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    default: 
      return super.onKeyUp(paramInt, paramKeyEvent);
    }
    return true;
  }
  
  public final void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    if (paramIntent != null)
    {
      Object localObject = getSupportFragmentManager();
      String str = "IncomingVoipFragment";
      localObject = (a)((j)localObject).a(str);
      if (localObject == null) {
        return;
      }
      k.b(paramIntent, "intent");
      paramIntent = paramIntent.getExtras();
      ((a)localObject).a(paramIntent);
      return;
    }
  }
  
  public final void onPause()
  {
    super.onPause();
    overridePendingTransition(0, 0);
  }
  
  public final void onResume()
  {
    super.onResume();
    setVolumeControlStream(2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.ui.IncomingVoipActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */