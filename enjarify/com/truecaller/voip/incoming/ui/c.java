package com.truecaller.voip.incoming.ui;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.utils.extensions.j;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.ag;
import com.truecaller.voip.util.VoipAnalyticsContext;
import com.truecaller.voip.util.VoipAnalyticsFailedCallAction;
import com.truecaller.voip.util.VoipAnalyticsInCallUiAction;
import com.truecaller.voip.util.VoipAnalyticsNotificationAction;
import com.truecaller.voip.util.at;
import com.truecaller.voip.util.o;
import com.truecaller.voip.util.z;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class c
  extends ba
  implements com.truecaller.voip.incoming.b.b, b.a
{
  private com.truecaller.voip.incoming.b.a c;
  private boolean d;
  private boolean e;
  private final z f;
  private final at g;
  private final o h;
  
  public c(f paramf, z paramz, at paramat, o paramo)
  {
    super(paramf);
    f = paramz;
    g = paramat;
    h = paramo;
  }
  
  private final void i()
  {
    com.truecaller.voip.incoming.b.a locala = c;
    if (locala == null) {
      return;
    }
    boolean bool = e;
    if (bool) {
      j();
    }
  }
  
  private final void j()
  {
    b.b localb = (b.b)b;
    boolean bool1 = true;
    boolean bool2;
    if (localb != null)
    {
      bool2 = localb.b();
      if (bool2 == bool1)
      {
        k();
        return;
      }
    }
    localb = (b.b)b;
    if (localb != null)
    {
      bool2 = localb.d();
      if (!bool2) {}
    }
    else
    {
      bool1 = false;
    }
    d = bool1;
    localb = (b.b)b;
    if (localb != null)
    {
      localb.e();
      return;
    }
  }
  
  private final bn k()
  {
    Object localObject = new com/truecaller/voip/incoming/ui/c$a;
    ((c.a)localObject).<init>(this, null);
    localObject = (m)localObject;
    return e.b(this, null, (m)localObject, 3);
  }
  
  public final void a()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.k();
      return;
    }
  }
  
  public final void a(float paramFloat, int paramInt1, int paramInt2, int paramInt3)
  {
    double d1 = paramFloat;
    double d2 = 0.95D;
    boolean bool = d1 < d2;
    o localo;
    if (bool)
    {
      i = R.id.incoming_call_answer_end_set;
      if (paramInt1 != i)
      {
        i = 0;
        paramFloat = 0.0F;
        localo = null;
        break label52;
      }
    }
    int i = 1;
    paramFloat = Float.MIN_VALUE;
    label52:
    paramInt1 = R.id.incoming_call_answer_start_set;
    if (paramInt2 == paramInt1)
    {
      paramInt1 = R.id.incoming_call_answer_end_set;
      if ((paramInt3 == paramInt1) && (i != 0))
      {
        localo = h;
        VoipAnalyticsInCallUiAction localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.ACCEPT;
        localo.a(localVoipAnalyticsInCallUiAction);
        j();
      }
    }
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = ((com.truecaller.voip.incoming.b.a)localObject1).a();
      if (localObject1 != null)
      {
        localObject1 = (VoipUser)j.a((h)localObject1);
        if (localObject1 != null)
        {
          int i = R.string.voip_reject_message_custom_option;
          if (paramInt != i)
          {
            localObject2 = Integer.valueOf(paramInt);
          }
          else
          {
            paramInt = 0;
            localObject2 = null;
          }
          z localz = f;
          localObject1 = b;
          localz.a((String)localObject1, (Integer)localObject2);
          localObject2 = c;
          if (localObject2 != null) {
            ((com.truecaller.voip.incoming.b.a)localObject2).e();
          }
        }
      }
    }
    Object localObject2 = h;
    localObject1 = VoipAnalyticsInCallUiAction.REJECT_WITH_MESSAGE;
    ((o)localObject2).a((VoipAnalyticsInCallUiAction)localObject1);
  }
  
  public final void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.a(paramInt1, paramInt2, paramBoolean);
      return;
    }
  }
  
  public final void a(com.truecaller.voip.incoming.b.a parama)
  {
    k.b(parama, "binderPresenter");
    c = parama;
    Object localObject = parama.g();
    b.b localb = (b.b)b;
    if (localb != null)
    {
      int i = d;
      int j = ((ag)localObject).c();
      boolean bool = ((ag)localObject).d();
      localb.a(i, j, bool);
    }
    localb = (b.b)b;
    if (localb != null)
    {
      localObject = e;
      localb.c((String)localObject);
    }
    localObject = this;
    localObject = (com.truecaller.voip.incoming.b.b)this;
    parama.a((com.truecaller.voip.incoming.b.b)localObject);
    parama = parama.a();
    localObject = new com/truecaller/voip/incoming/ui/c$b;
    ((c.b)localObject).<init>(this, null);
    localObject = (m)localObject;
    j.a(this, parama, (m)localObject);
    i();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.c(paramString);
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      k();
      return;
    }
    Object localObject = (b.b)b;
    if (localObject != null) {
      ((b.b)localObject).f();
    }
    localObject = (b.b)b;
    if (localObject != null) {
      ((b.b)localObject).g();
    }
    localObject = h;
    String str = VoipAnalyticsContext.VOIP_IN_CALL_UI.getValue();
    VoipAnalyticsFailedCallAction localVoipAnalyticsFailedCallAction = VoipAnalyticsFailedCallAction.NO_MIC_PERMISSION;
    ((o)localObject).a(str, localVoipAnalyticsFailedCallAction);
    paramBoolean = d;
    if (paramBoolean)
    {
      localObject = (b.b)b;
      if (localObject != null)
      {
        paramBoolean = ((b.b)localObject).d();
        if (!paramBoolean)
        {
          localObject = (b.b)b;
          if (localObject != null) {
            ((b.b)localObject).h();
          }
        }
      }
      else {}
    }
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    e = paramBoolean1;
    if ((paramBoolean1) && (paramBoolean2))
    {
      o localo = h;
      String str = VoipAnalyticsContext.NOTIFICATION.getValue();
      VoipAnalyticsNotificationAction localVoipAnalyticsNotificationAction = VoipAnalyticsNotificationAction.ANSWERED;
      localo.a(str, localVoipAnalyticsNotificationAction);
    }
    i();
  }
  
  public final void b()
  {
    Object localObject = c;
    if (localObject != null) {
      ((com.truecaller.voip.incoming.b.a)localObject).e();
    }
    localObject = h;
    VoipAnalyticsInCallUiAction localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.REJECT;
    ((o)localObject).a(localVoipAnalyticsInCallUiAction);
  }
  
  public final void c()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.i();
      return;
    }
  }
  
  public final void e()
  {
    com.truecaller.voip.incoming.b.a locala = c;
    if (locala != null)
    {
      locala.f();
      return;
    }
  }
  
  public final void f()
  {
    Object localObject = (b.b)b;
    if (localObject != null) {
      ((b.b)localObject).j();
    }
    localObject = h;
    VoipAnalyticsInCallUiAction localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.DISMISS;
    ((o)localObject).a(localVoipAnalyticsInCallUiAction);
  }
  
  public final void g()
  {
    o localo = h;
    VoipAnalyticsInCallUiAction localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.BACK;
    localo.a(localVoipAnalyticsInCallUiAction);
  }
  
  public final void h()
  {
    b.b localb = (b.b)b;
    if (localb != null) {
      localb.l();
    }
    c = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.ui.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */