package com.truecaller.voip.incoming.ui;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class IncomingVoipActivity$a
{
  public static Intent a(Context paramContext, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, IncomingVoipActivity.class);
    localIntent.addFlags(268435456);
    localIntent.putExtra("com.truecaller.voip.incoming.ui.EXTRA_ACCEPT_CALL", paramBoolean1);
    localIntent.putExtra("com.truecaller.voip.incoming.ui.EXTRA_VOIP_NOTIFICATION_ACTION", paramBoolean2);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.ui.IncomingVoipActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */