package com.truecaller.voip.incoming;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.manager.rtm.RtmMsg;
import com.truecaller.voip.manager.rtm.RtmMsgAction;
import com.truecaller.voip.manager.rtm.i;
import com.truecaller.voip.manager.rtm.i.a;
import kotlinx.coroutines.ag;

final class c$j
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  c$j(c paramc, RtmMsgAction paramRtmMsgAction, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    j localj = new com/truecaller/voip/incoming/c$j;
    c localc = b;
    RtmMsgAction localRtmMsgAction = c;
    localj.<init>(localc, localRtmMsgAction, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localj;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label151;
      }
      paramObject = c.l(b);
      VoipUser localVoipUser = c.h(b);
      RtmMsg localRtmMsg = new com/truecaller/voip/manager/rtm/RtmMsg;
      RtmMsgAction localRtmMsgAction = c;
      String str = c.x(b);
      localRtmMsg.<init>(localRtmMsgAction, str);
      int j = 1;
      a = j;
      paramObject = i.a.a((i)paramObject, localVoipUser, localRtmMsg, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return x.a;
    label151:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (j)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((j)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.c.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */