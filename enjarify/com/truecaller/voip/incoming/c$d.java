package com.truecaller.voip.incoming;

import c.d.a.a;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.manager.rtm.FailedRtmLoginReason;
import com.truecaller.voip.manager.rtm.f;
import com.truecaller.voip.manager.rtm.g;
import com.truecaller.voip.manager.rtm.h;
import com.truecaller.voip.manager.rtm.h.a;
import com.truecaller.voip.manager.rtm.j;
import kotlinx.coroutines.ag;

final class c$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  c$d(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/voip/incoming/c$d;
    c localc = b;
    locald.<init>(localc, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    int j = 1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label250;
      }
      paramObject = c.m(b);
      a = j;
      paramObject = h.a.a((h)paramObject, this);
      if (paramObject == localObject) {
        return localObject;
      }
      break;
    }
    paramObject = (g)paramObject;
    boolean bool2 = paramObject instanceof j;
    if (!bool2)
    {
      bool2 = paramObject instanceof f;
      if (bool2)
      {
        paramObject = a;
        localObject = d.b;
        int k = ((FailedRtmLoginReason)paramObject).ordinal();
        k = localObject[k];
        switch (k)
        {
        default: 
          k = 0;
          paramObject = null;
          break;
        case 3: 
          paramObject = VoipStateReason.LOGIN_RTM_FAILED;
          break;
        case 2: 
          paramObject = VoipStateReason.GET_RTM_TOKEN_FAILED;
          break;
        case 1: 
          paramObject = VoipStateReason.GET_VOIP_ID_FAILED;
        }
        localObject = b;
        VoipState localVoipState = VoipState.FAILED;
        c.a((c)localObject, localVoipState, (VoipStateReason)paramObject);
        j = 0;
      }
    }
    else
    {
      return Boolean.valueOf(j);
    }
    paramObject = new c/l;
    ((l)paramObject).<init>();
    throw ((Throwable)paramObject);
    label250:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */