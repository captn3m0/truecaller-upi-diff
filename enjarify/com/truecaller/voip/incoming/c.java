package com.truecaller.voip.incoming;

import c.g.a.a;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.notificationchannels.b;
import com.truecaller.utils.n;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.ag;
import com.truecaller.voip.manager.rtm.RtmMsgAction;
import com.truecaller.voip.util.VoipAnalyticsContext;
import com.truecaller.voip.util.VoipAnalyticsNotificationAction;
import com.truecaller.voip.util.VoipEventType;
import com.truecaller.voip.util.ab;
import com.truecaller.voip.util.an;
import com.truecaller.voip.util.au;
import com.truecaller.voip.util.o;
import com.truecaller.voip.util.s;
import com.truecaller.voip.util.w;
import com.truecaller.voip.util.y;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class c
  extends ba
  implements b.c
{
  final kotlinx.coroutines.a.h c;
  private String d;
  private VoipUser e;
  private boolean f;
  private String g;
  private ag h;
  private b.b i;
  private final kotlinx.coroutines.a.h j;
  private final c.d.f k;
  private final com.truecaller.voip.manager.rtm.i l;
  private final au m;
  private final b n;
  private final w o;
  private final y p;
  private final n q;
  private final s r;
  private final com.truecaller.voip.manager.rtm.h s;
  private final com.truecaller.voip.util.f t;
  private final o u;
  private final an v;
  
  public c(c.d.f paramf1, c.d.f paramf2, com.truecaller.voip.manager.rtm.i parami, au paramau, b paramb, w paramw, y paramy, n paramn, s params, com.truecaller.voip.manager.rtm.h paramh, com.truecaller.voip.util.f paramf, o paramo, an paraman)
  {
    super(paramf1);
    k = paramf2;
    l = parami;
    m = paramau;
    n = paramb;
    o = paramw;
    p = paramy;
    q = paramn;
    r = params;
    s = paramh;
    t = paramf;
    u = paramo;
    v = paraman;
    paramh = new com/truecaller/voip/ag;
    paramf1 = paramh;
    paramh.<init>(null, null, 0, 0, false, null, false, 255);
    h = paramh;
    int i1 = -1;
    paramf2 = kotlinx.coroutines.a.i.a(i1);
    c = paramf2;
    paramf1 = kotlinx.coroutines.a.i.a(i1);
    j = paramf1;
  }
  
  private final bn a(RtmMsgAction paramRtmMsgAction)
  {
    Object localObject = new com/truecaller/voip/incoming/c$j;
    ((c.j)localObject).<init>(this, paramRtmMsgAction, null);
    localObject = (m)localObject;
    return e.b(this, null, (m)localObject, 3);
  }
  
  private final void a(VoipState paramVoipState, VoipStateReason paramVoipStateReason)
  {
    c localc = this;
    Object localObject1 = h.a;
    int i1 = 1;
    int i2 = 0;
    Object localObject2 = null;
    Object localObject3 = paramVoipState;
    int i3;
    if (paramVoipState == localObject1)
    {
      i3 = 0;
      localObject1 = null;
    }
    else
    {
      localObject1 = h.a;
      localObject4 = d.d;
      i3 = ((VoipState)localObject1).ordinal();
      i3 = localObject4[i3];
      switch (i3)
      {
      default: 
        i3 = 1;
        break;
      case 1: 
      case 2: 
      case 3: 
      case 4: 
      case 5: 
      case 6: 
        i3 = 0;
        localObject1 = null;
      }
    }
    if (i3 == 0) {
      return;
    }
    localObject1 = new String[i1];
    Object localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>("Setting state: ");
    Object localObject4 = paramVoipState.name();
    ((StringBuilder)localObject5).append((String)localObject4);
    localObject5 = ((StringBuilder)localObject5).toString();
    localObject1[0] = localObject5;
    localObject1 = (a)c.ab.a;
    localObject5 = d.c;
    i2 = paramVoipState.ordinal();
    i1 = localObject5[i2];
    switch (i1)
    {
    default: 
      localObject1 = new com/truecaller/voip/incoming/c$r;
      ((c.r)localObject1).<init>(this);
      localObject1 = (a)localObject1;
      localObject5 = new com/truecaller/voip/incoming/c$s;
      ((c.s)localObject5).<init>(this);
      localObject5 = (a)localObject5;
      localObject2 = new com/truecaller/voip/ag;
      i4 = R.string.voip_status_call_ended;
      i5 = R.color.voip_call_status_error_color;
      bool1 = true;
      str = "Error. Exiting...";
      i6 = 134;
      localObject4 = localObject2;
      localObject3 = paramVoipState;
      ((ag)localObject2).<init>(paramVoipState, null, i4, i5, bool1, str, false, i6);
      break;
    case 7: 
      localObject1 = new com/truecaller/voip/incoming/c$p;
      ((c.p)localObject1).<init>(this);
      localObject1 = (a)localObject1;
      localObject5 = new com/truecaller/voip/incoming/c$q;
      ((c.q)localObject5).<init>(this);
      localObject5 = (a)localObject5;
      localObject2 = new com/truecaller/voip/ag;
      i4 = R.string.voip_status_call_ended;
      i5 = R.color.voip_call_status_error_color;
      bool1 = true;
      str = "Call cancelled. Exiting...";
      i6 = 134;
      localObject4 = localObject2;
      localObject3 = paramVoipState;
      ((ag)localObject2).<init>(paramVoipState, null, i4, i5, bool1, str, false, i6);
      break;
    case 6: 
      localObject1 = new com/truecaller/voip/incoming/c$n;
      ((c.n)localObject1).<init>(this);
      localObject1 = (a)localObject1;
      localObject5 = new com/truecaller/voip/incoming/c$o;
      ((c.o)localObject5).<init>(this);
      localObject5 = (a)localObject5;
      localObject2 = new com/truecaller/voip/ag;
      i4 = 0;
      i5 = 0;
      bool1 = false;
      str = "Call blocked. Exiting...";
      i6 = 190;
      localObject4 = localObject2;
      localObject3 = paramVoipState;
      ((ag)localObject2).<init>(paramVoipState, null, 0, 0, false, str, false, i6);
      break;
    case 5: 
      localObject1 = new com/truecaller/voip/incoming/c$z;
      ((c.z)localObject1).<init>(this);
      localObject1 = (a)localObject1;
      localObject5 = new com/truecaller/voip/incoming/c$aa;
      ((c.aa)localObject5).<init>(this);
      localObject5 = (a)localObject5;
      localObject2 = new com/truecaller/voip/ag;
      i4 = R.string.voip_status_no_answer;
      i5 = R.color.voip_call_status_error_color;
      bool1 = true;
      str = "No answer. Exiting...";
      i6 = 134;
      localObject4 = localObject2;
      localObject3 = paramVoipState;
      ((ag)localObject2).<init>(paramVoipState, null, i4, i5, bool1, str, false, i6);
      break;
    case 4: 
      localObject1 = new com/truecaller/voip/incoming/c$x;
      ((c.x)localObject1).<init>(this);
      localObject1 = (a)localObject1;
      localObject5 = new com/truecaller/voip/incoming/c$y;
      ((c.y)localObject5).<init>(this);
      localObject5 = (a)localObject5;
      localObject2 = new com/truecaller/voip/ag;
      i4 = R.string.voip_status_rejected;
      i5 = R.color.voip_call_status_error_color;
      bool1 = true;
      str = "Incoming call is rejected. Exiting...";
      i6 = 134;
      localObject4 = localObject2;
      localObject3 = paramVoipState;
      ((ag)localObject2).<init>(paramVoipState, null, i4, i5, bool1, str, false, i6);
      break;
    case 3: 
      localObject5 = new com/truecaller/voip/incoming/c$w;
      ((c.w)localObject5).<init>(this);
      localObject5 = (a)localObject5;
      localObject2 = new com/truecaller/voip/ag;
      i4 = R.string.voip_status_connecting;
      i5 = R.color.voip_call_status_warning_color;
      bool1 = true;
      str = "Incoming call is accepted. Opening VoIP screen...";
      i6 = 134;
      localObject4 = localObject2;
      localObject3 = paramVoipState;
      ((ag)localObject2).<init>(paramVoipState, null, i4, i5, bool1, str, false, i6);
      break;
    case 2: 
      localObject1 = new com/truecaller/voip/incoming/c$u;
      ((c.u)localObject1).<init>(this);
      localObject1 = (a)localObject1;
      localObject5 = new com/truecaller/voip/incoming/c$v;
      ((c.v)localObject5).<init>(this);
      localObject5 = (a)localObject5;
      localObject2 = new com/truecaller/voip/ag;
      i4 = R.string.voip_status_incoming;
      i5 = R.color.voip_call_status_neutral_color;
      bool1 = false;
      str = "Incoming call is received.";
      i6 = 134;
      localObject4 = localObject2;
      localObject3 = paramVoipState;
      ((ag)localObject2).<init>(paramVoipState, null, i4, i5, false, str, false, i6);
      break;
    case 1: 
      localObject1 = new com/truecaller/voip/incoming/c$m;
      ((c.m)localObject1).<init>(this);
      localObject1 = (a)localObject1;
      localObject5 = new com/truecaller/voip/incoming/c$t;
      ((c.t)localObject5).<init>(this);
      localObject5 = (a)localObject5;
      localObject2 = new com/truecaller/voip/ag;
      i4 = R.string.voip_status_connecting;
      i5 = R.color.voip_call_status_warning_color;
      bool1 = true;
      str = "Initializing and resolving user details...";
      i6 = 134;
      localObject4 = localObject2;
      localObject3 = paramVoipState;
      ((ag)localObject2).<init>(paramVoipState, null, i4, i5, bool1, str, false, i6);
    }
    h = ((ag)localObject2);
    localObject4 = h;
    boolean bool2 = false;
    localObject3 = null;
    int i4 = 0;
    int i5 = 0;
    boolean bool1 = false;
    String str = null;
    int i6 = 0;
    int i7 = 253;
    localObject2 = ag.a((ag)localObject4, null, paramVoipStateReason, null, 0, 0, false, null, false, i7);
    h = ((ag)localObject2);
    ((a)localObject1).invoke();
    localObject1 = i;
    if (localObject1 != null)
    {
      localObject2 = h;
      i2 = d;
      localObject4 = h;
      int i8 = ((ag)localObject4).c();
      localObject3 = h;
      bool2 = ((ag)localObject3).d();
      ((b.b)localObject1).a(i2, i8, bool2);
    }
    localObject1 = i;
    if (localObject1 != null)
    {
      localObject2 = h.e;
      ((b.b)localObject1).a((String)localObject2);
    }
    localObject1 = j;
    localObject2 = h;
    ((kotlinx.coroutines.a.h)localObject1).d_(localObject2);
    ((a)localObject5).invoke();
  }
  
  private final void a(VoipUser paramVoipUser)
  {
    y localy = p;
    ab localab = new com/truecaller/voip/util/ab;
    String str = b;
    VoipEventType localVoipEventType = VoipEventType.MISSED;
    long l1 = 0L;
    int i1 = 12;
    localab.<init>(str, localVoipEventType, l1, null, i1);
    localy.a(localab);
    paramVoipUser = (b.d)b;
    if (paramVoipUser != null)
    {
      paramVoipUser.d();
      return;
    }
  }
  
  private final void a(String paramString)
  {
    y localy = p;
    ab localab = new com/truecaller/voip/util/ab;
    VoipEventType localVoipEventType = VoipEventType.BLOCKED;
    long l1 = 0L;
    int i1 = 12;
    localab.<init>(paramString, localVoipEventType, l1, null, i1);
    localy.a(localab);
    paramString = (b.d)b;
    if (paramString != null)
    {
      paramString.e();
      return;
    }
  }
  
  private final String j()
  {
    boolean bool = f;
    if (bool)
    {
      VoipUser localVoipUser = e;
      if (localVoipUser == null)
      {
        String str = "voipUser";
        k.a(str);
      }
      return a;
    }
    return g;
  }
  
  public final kotlinx.coroutines.a.h a()
  {
    return c;
  }
  
  public final bn a(String paramString1, String paramString2)
  {
    Object localObject = new com/truecaller/voip/incoming/c$e;
    ((c.e)localObject).<init>(this, paramString1, paramString2, null);
    localObject = (m)localObject;
    return e.b(this, null, (m)localObject, 3);
  }
  
  public final void a(b.b paramb)
  {
    i = paramb;
  }
  
  public final kotlinx.coroutines.a.h b()
  {
    return j;
  }
  
  public final void c()
  {
    boolean bool = f;
    if (bool)
    {
      VoipState localVoipState = VoipState.ACCEPTED;
      a(localVoipState, null);
    }
  }
  
  public final void e()
  {
    VoipState localVoipState = VoipState.REJECTED;
    a(localVoipState, null);
  }
  
  public final void f()
  {
    i();
  }
  
  public final ag g()
  {
    return h;
  }
  
  public final void h()
  {
    Object localObject = u;
    String str = VoipAnalyticsContext.NOTIFICATION.getValue();
    VoipAnalyticsNotificationAction localVoipAnalyticsNotificationAction = VoipAnalyticsNotificationAction.REJECTED;
    ((o)localObject).a(str, localVoipAnalyticsNotificationAction);
    localObject = (b.d)b;
    if (localObject != null) {
      ((b.d)localObject).f();
    }
    e();
  }
  
  public final void i()
  {
    t.c();
    r.b();
  }
  
  public final void y_()
  {
    t.c();
    r.f();
    Object localObject = h.a;
    VoipState localVoipState = VoipState.ACCEPTED;
    if (localObject != localVoipState)
    {
      localObject = s;
      ((com.truecaller.voip.manager.rtm.h)localObject).b();
    }
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */