package com.truecaller.voip.incoming;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.manager.rtm.RtmMsg;
import com.truecaller.voip.manager.rtm.RtmMsgAction;

final class c$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private RtmMsg c;
  
  c$b(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/voip/incoming/c$b;
    c localc = b;
    localb.<init>(localc, paramc);
    paramObject = (RtmMsg)paramObject;
    c = ((RtmMsg)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = c;
        localObject1 = ((RtmMsg)paramObject).getSenderId();
        Object localObject2 = c.n(b);
        bool = c.g.b.k.a(localObject1, localObject2);
        int j = 1;
        bool ^= j;
        if (bool) {
          return x.a;
        }
        paramObject = ((RtmMsg)paramObject).getAction();
        localObject1 = d.a;
        int k = ((RtmMsgAction)paramObject).ordinal();
        k = localObject1[k];
        if (k == j)
        {
          paramObject = b;
          localObject1 = VoipState.ENDED;
          localObject2 = VoipStateReason.RECEIVED_END;
          c.a((c)paramObject, (VoipState)localObject1, (VoipStateReason)localObject2);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */