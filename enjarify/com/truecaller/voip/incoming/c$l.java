package com.truecaller.voip.incoming;

import android.graphics.Bitmap;
import c.d.a.a;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipUser;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class c$l
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  c$l(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    l locall = new com/truecaller/voip/incoming/c$l;
    c localc = c;
    locall.<init>(localc, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locall;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label189;
      }
      paramObject = hc).d;
      if (paramObject == null) {
        return x.a;
      }
      f localf = c.p(c);
      Object localObject2 = new com/truecaller/voip/incoming/c$l$a;
      ((c.l.a)localObject2).<init>(this, (String)paramObject, null);
      localObject2 = (m)localObject2;
      a = paramObject;
      int j = 1;
      b = j;
      paramObject = g.a(localf, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Bitmap)paramObject;
    if (paramObject == null) {
      return x.a;
    }
    localObject1 = c.c(c);
    if (localObject1 != null) {
      ((b.d)localObject1).a((Bitmap)paramObject);
    }
    return x.a;
    label189:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (l)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((l)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.c.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */