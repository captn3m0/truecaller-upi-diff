package com.truecaller.voip.incoming;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.util.au;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.ag;

final class c$g
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  c$g(c paramc, String paramString, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    g localg = new com/truecaller/voip/incoming/c$g;
    c localc = b;
    String str = c;
    localg.<init>(localc, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localg;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    int j = 1;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label254;
      }
      paramObject = c.k(b);
      localObject2 = c;
      if (localObject2 == null) {
        return Boolean.FALSE;
      }
      a = j;
      paramObject = ((au)paramObject).a((String)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (VoipUser)paramObject;
    if (paramObject == null)
    {
      paramObject = new String[j];
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Cannot resolve caller voip id:");
      String str = c;
      ((StringBuilder)localObject2).append(str);
      ((StringBuilder)localObject2).append(". Exiting...");
      localObject2 = ((StringBuilder)localObject2).toString();
      paramObject[0] = localObject2;
      paramObject = b;
      localObject1 = VoipState.FAILED;
      localObject2 = VoipStateReason.GET_PHONE_FAILED;
      c.a((c)paramObject, (VoipState)localObject1, (VoipStateReason)localObject2);
      return Boolean.FALSE;
    }
    c.a(b, (VoipUser)paramObject);
    c.o(b);
    paramObject = b.c;
    localObject1 = c.h(b);
    ((h)paramObject).d_(localObject1);
    return Boolean.TRUE;
    label254:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (g)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((g)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.c.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */