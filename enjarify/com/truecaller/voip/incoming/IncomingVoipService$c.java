package com.truecaller.voip.incoming;

import android.graphics.Bitmap;
import android.support.v4.app.z.d;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.util.w;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class IncomingVoipService$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag f;
  
  IncomingVoipService$c(IncomingVoipService paramIncomingVoipService, int paramInt, z.d paramd, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/voip/incoming/IncomingVoipService$c;
    IncomingVoipService localIncomingVoipService = c;
    int i = d;
    z.d locald = e;
    localc.<init>(localIncomingVoipService, i, locald, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label186;
      }
      paramObject = c.d;
      if (paramObject == null)
      {
        localObject1 = "bitmapUtil";
        c.g.b.k.a((String)localObject1);
      }
      int j = d;
      paramObject = ((w)paramObject).a(j);
      Object localObject1 = c.a;
      if (localObject1 == null)
      {
        localObject2 = "uiContext";
        c.g.b.k.a((String)localObject2);
      }
      Object localObject2 = new com/truecaller/voip/incoming/IncomingVoipService$c$1;
      ((IncomingVoipService.c.1)localObject2).<init>(this, (Bitmap)paramObject, null);
      localObject2 = (m)localObject2;
      a = paramObject;
      int k = 1;
      b = k;
      paramObject = g.a((f)localObject1, (m)localObject2, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return x.a;
    label186:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.IncomingVoipService.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */