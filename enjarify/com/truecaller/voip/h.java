package com.truecaller.voip;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.a.m;
import c.g.b.k;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.o;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.g.d;
import com.truecaller.voip.util.z;
import java.util.Collection;

public final class h
  implements z
{
  final Context a;
  
  public h(Context paramContext)
  {
    a = paramContext;
  }
  
  static void a(Context paramContext, Participant[] paramArrayOfParticipant, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    Object localObject = ConversationActivity.class;
    localIntent.<init>(paramContext, (Class)localObject);
    if (paramArrayOfParticipant != null)
    {
      localObject = "participants";
      paramArrayOfParticipant = (Parcelable[])paramArrayOfParticipant;
      localIntent.putExtra((String)localObject, paramArrayOfParticipant);
    }
    if (paramString != null)
    {
      paramArrayOfParticipant = new android/content/Intent;
      paramArrayOfParticipant.<init>();
      localObject = "android.intent.extra.TEXT";
      paramArrayOfParticipant.putExtra((String)localObject, paramString);
      paramString = "send_intent";
      paramArrayOfParticipant = (Parcelable)paramArrayOfParticipant;
      localIntent.putExtra(paramString, paramArrayOfParticipant);
    }
    localIntent.setFlags(268435456);
    paramContext.startActivity(localIntent);
  }
  
  private final com.truecaller.common.h.u d()
  {
    com.truecaller.common.h.u localu = a().V();
    k.a(localu, "graph.phoneNumberHelper()");
    return localu;
  }
  
  final bp a()
  {
    Object localObject = a.getApplicationContext();
    if (localObject != null)
    {
      localObject = ((bk)localObject).a();
      k.a(localObject, "(context.applicationCont…GraphHolder).objectsGraph");
      return (bp)localObject;
    }
    localObject = new c/u;
    ((c.u)localObject).<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw ((Throwable)localObject);
  }
  
  public final void a(String paramString, Integer paramInteger)
  {
    k.b(paramString, "number");
    Object localObject = d();
    String str = d().a();
    paramString = (Collection)m.a(Participant.a(paramString, (com.truecaller.common.h.u)localObject, str));
    if (paramString != null)
    {
      localObject = new Participant[0];
      paramString = paramString.toArray((Object[])localObject);
      if (paramString != null)
      {
        paramString = (Participant[])paramString;
        if (paramInteger == null)
        {
          a(a, paramString, null);
          return;
        }
        localObject = a().q();
        k.a(localObject, "graph.fetchMessageStorage()");
        paramString = ((o)((f)localObject).a()).a(paramString, 1);
        localObject = new com/truecaller/voip/h$a;
        ((h.a)localObject).<init>(this, paramInteger);
        localObject = (ac)localObject;
        k.a(paramString.a((ac)localObject), "fetchMessageStorage.tell…g(message))\n            }");
        return;
      }
      paramString = new c/u;
      paramString.<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw paramString;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw paramString;
  }
  
  final d b()
  {
    d locald = a().bW();
    k.a(locald, "graph.draftSender()");
    return locald;
  }
  
  final com.truecaller.multisim.h c()
  {
    com.truecaller.multisim.h localh = a().U();
    k.a(localh, "graph.multiSimManager()");
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */