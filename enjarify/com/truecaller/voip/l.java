package com.truecaller.voip;

import c.g.a.a;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.n.m;
import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.utils.d;
import java.util.Iterator;

public final class l
  implements k
{
  String b;
  private final c.f c;
  private final e d;
  private final r e;
  private final d f;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(l.class);
    ((u)localObject).<init>(localb, "isArchitectureSupported", "isArchitectureSupported()Z");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public l(e parame, r paramr, d paramd)
  {
    d = parame;
    e = paramr;
    f = paramd;
    b = "release";
    parame = new com/truecaller/voip/l$a;
    parame.<init>(this);
    parame = c.g.a((a)parame);
    c = parame;
  }
  
  public final boolean a()
  {
    Object localObject1 = d.B();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool1)
    {
      localObject1 = e;
      bool1 = ((r)localObject1).c();
      if (bool1)
      {
        localObject1 = (Boolean)c.b();
        bool1 = ((Boolean)localObject1).booleanValue();
        if (bool1)
        {
          localObject1 = d;
          Object localObject2 = O;
          c.l.g localg = e.a[111];
          localObject1 = ((com.truecaller.featuretoggles.f)((e.a)localObject2).a((e)localObject1, localg)).e();
          localObject2 = localObject1;
          localObject2 = (CharSequence)localObject1;
          boolean bool2 = m.a((CharSequence)localObject2);
          boolean bool3 = true;
          bool2 ^= bool3;
          Object localObject3 = null;
          if (!bool2)
          {
            bool1 = false;
            localObject1 = null;
          }
          if (localObject1 != null)
          {
            localObject1 = (CharSequence)localObject1;
            localObject2 = new String[] { "," };
            int i = 6;
            localObject1 = m.c((CharSequence)localObject1, (String[])localObject2, false, i);
            if (localObject1 != null)
            {
              localObject2 = f.l();
              Object localObject4 = localObject2;
              localObject4 = (CharSequence)localObject2;
              boolean bool4 = m.a((CharSequence)localObject4) ^ bool3;
              if (!bool4)
              {
                bool2 = false;
                localObject2 = null;
              }
              if (localObject2 == null)
              {
                bool1 = true;
                break label299;
              }
              localObject1 = ((Iterable)localObject1).iterator();
              boolean bool5;
              do
              {
                bool4 = ((Iterator)localObject1).hasNext();
                if (!bool4) {
                  break;
                }
                localObject4 = ((Iterator)localObject1).next();
                Object localObject5 = localObject4;
                localObject5 = (String)localObject4;
                bool5 = m.a((String)localObject2, (String)localObject5, bool3);
              } while (!bool5);
              localObject3 = localObject4;
              if (localObject3 == null)
              {
                bool1 = true;
                break label299;
              }
              bool1 = false;
              localObject1 = null;
              break label299;
            }
          }
          bool1 = true;
          label299:
          if (bool1) {
            return bool3;
          }
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */