package com.truecaller.voip;

import android.content.Context;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.filters.FilterManager;
import com.truecaller.voip.util.aa;
import kotlinx.coroutines.g;

public final class i
  implements aa
{
  private final Context a;
  
  public i(Context paramContext)
  {
    a = paramContext;
  }
  
  private final FilterManager b()
  {
    FilterManager localFilterManager = a().P();
    k.a(localFilterManager, "graph.filterManager()");
    return localFilterManager;
  }
  
  final bp a()
  {
    Object localObject = a.getApplicationContext();
    if (localObject != null)
    {
      localObject = ((bk)localObject).a();
      k.a(localObject, "(context.applicationCont…GraphHolder).objectsGraph");
      return (bp)localObject;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw ((Throwable)localObject);
  }
  
  public final Object a(String paramString, c paramc)
  {
    f localf = a().bm();
    k.a(localf, "graph.asyncCoroutineContext()");
    Object localObject = new com/truecaller/voip/i$a;
    ((i.a)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */