package com.truecaller.voip;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.callhistory.a;
import com.truecaller.common.account.r;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.featuretoggles.b;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.voip.db.c;
import com.truecaller.voip.util.VoipAnalyticsFailedCallAction;
import com.truecaller.voip.util.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

public final class aj
  implements ai, ag
{
  final c.d.f a;
  final c b;
  final o c;
  private final c.d.f d;
  private final Context e;
  private final d f;
  private final com.truecaller.utils.i g;
  private final com.truecaller.common.h.u h;
  private final com.truecaller.featuretoggles.e i;
  private final com.truecaller.androidactors.f j;
  private final r k;
  
  public aj(c.d.f paramf1, c.d.f paramf2, Context paramContext, d paramd, c paramc, com.truecaller.utils.i parami, com.truecaller.common.h.u paramu, o paramo, com.truecaller.featuretoggles.e parame, com.truecaller.androidactors.f paramf, r paramr)
  {
    a = paramf1;
    d = paramf2;
    e = paramContext;
    f = paramd;
    b = paramc;
    g = parami;
    h = paramu;
    c = paramo;
    i = parame;
    j = paramf;
    k = paramr;
  }
  
  public final c.d.f V_()
  {
    return a;
  }
  
  final List a(List paramList)
  {
    Object localObject1 = k.b();
    paramList = (Iterable)paramList;
    Object localObject2 = new java/util/ArrayList;
    int m = c.a.m.a(paramList, 10);
    ((ArrayList)localObject2).<init>(m);
    localObject2 = (Collection)localObject2;
    paramList = paramList.iterator();
    boolean bool1;
    Object localObject3;
    for (;;)
    {
      bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = al.a((String)paramList.next());
      ((Collection)localObject2).add(localObject3);
    }
    localObject2 = (Iterable)localObject2;
    paramList = new java/util/ArrayList;
    paramList.<init>();
    paramList = (Collection)paramList;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = ((Iterator)localObject2).next();
      Object localObject4 = localObject3;
      localObject4 = (String)localObject3;
      com.truecaller.common.h.u localu = h;
      localObject4 = localu.b((String)localObject4);
      boolean bool2 = k.a(localObject1, localObject4);
      if (!bool2) {
        paramList.add(localObject3);
      }
    }
    paramList = (Collection)paramList;
    localObject1 = new String[0];
    paramList = paramList.toArray((Object[])localObject1);
    if (paramList != null)
    {
      paramList = (String[])paramList;
      return b.a(paramList);
    }
    paramList = new c/u;
    paramList.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramList;
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    Object localObject1 = f;
    boolean bool = ((d)localObject1).a();
    if (bool)
    {
      localObject1 = "com.truecaller.datamanager.EXTRA_PRESENCE";
      bool = paramIntent.hasExtra((String)localObject1);
      if (bool)
      {
        localObject1 = "com.truecaller.datamanager.EXTRA_PRESENCE";
        paramIntent = paramIntent.getSerializableExtra((String)localObject1);
        if (paramIntent != null)
        {
          paramIntent = (ArrayList)paramIntent;
          localObject1 = d;
          Object localObject2 = new com/truecaller/voip/aj$d;
          ((aj.d)localObject2).<init>(this, paramIntent, null);
          localObject2 = (c.g.a.m)localObject2;
          kotlinx.coroutines.e.b(this, (c.d.f)localObject1, (c.g.a.m)localObject2, 2);
          return;
        }
        paramIntent = new c/u;
        paramIntent.<init>("null cannot be cast to non-null type kotlin.collections.ArrayList<com.truecaller.presence.Presence> /* = java.util.ArrayList<com.truecaller.presence.Presence> */");
        throw paramIntent;
      }
    }
  }
  
  public final void a(Contact paramContact, e parame)
  {
    k.b(paramContact, "contact");
    k.b(parame, "listener");
    Object localObject1 = f;
    boolean bool = ((d)localObject1).a();
    if (!bool)
    {
      parame.onVoipAvailabilityLoaded(false);
      return;
    }
    localObject1 = d;
    Object localObject2 = new com/truecaller/voip/aj$b;
    ((aj.b)localObject2).<init>(this, paramContact, parame, null);
    localObject2 = (c.g.a.m)localObject2;
    kotlinx.coroutines.e.b(this, (c.d.f)localObject1, (c.g.a.m)localObject2, 2);
  }
  
  public final void a(Participant paramParticipant, e parame)
  {
    k.b(paramParticipant, "participant");
    k.b(parame, "listener");
    Object localObject1 = f;
    boolean bool = ((d)localObject1).a();
    if (!bool)
    {
      parame.onVoipAvailabilityLoaded(false);
      return;
    }
    localObject1 = d;
    Object localObject2 = new com/truecaller/voip/aj$c;
    ((aj.c)localObject2).<init>(this, paramParticipant, parame, null);
    localObject2 = (c.g.a.m)localObject2;
    kotlinx.coroutines.e.b(this, (c.d.f)localObject1, (c.g.a.m)localObject2, 2);
  }
  
  public final void a(Notification paramNotification, long paramLong)
  {
    Object localObject1 = paramNotification;
    k.b(paramNotification, "notification");
    Object localObject2 = Settings.b("qa_voip_notification_rtm_token");
    k.a(localObject2, "it");
    Object localObject3 = localObject2;
    localObject3 = (CharSequence)localObject2;
    boolean bool = c.n.m.a((CharSequence)localObject3);
    Long localLong = null;
    if (bool) {
      localObject2 = null;
    }
    if (localObject2 == null) {
      localObject2 = paramNotification.k();
    }
    Object localObject4 = localObject2;
    localObject2 = new com/truecaller/voip/af;
    String str1 = paramNotification.h();
    String str2 = paramNotification.i();
    String str3 = paramNotification.a();
    String str4 = paramNotification.l();
    localObject3 = paramNotification.n();
    Object localObject5;
    if (localObject3 != null)
    {
      localObject3 = c.n.m.b((String)localObject3);
      localObject5 = localObject3;
    }
    else
    {
      localObject5 = null;
    }
    String str5 = paramNotification.j();
    localObject1 = paramNotification.m();
    if (localObject1 != null) {
      localLong = c.n.m.d((String)localObject1);
    }
    ((af)localObject2).<init>(paramLong, str1, str2, str3, (String)localObject4, str4, (Integer)localObject5, str5, localLong);
    localObject1 = this;
    f.a((af)localObject2);
  }
  
  final void a(String paramString, Contact paramContact)
  {
    paramContact = paramContact.A();
    String str = "contact.numbers";
    k.a(paramContact, str);
    paramContact = (Number)c.a.m.e(paramContact);
    if (paramContact != null)
    {
      paramContact = paramContact.a();
      if (paramContact != null)
      {
        str = h.b(paramContact);
        if (str != null) {
          paramContact = str;
        }
        c.a(paramString, paramContact);
        return;
      }
    }
  }
  
  public final void a(List paramList, ae paramae)
  {
    k.b(paramList, "normalizedNumbers");
    k.b(paramae, "listener");
    c.d.f localf = d;
    Object localObject = new com/truecaller/voip/aj$a;
    ((aj.a)localObject).<init>(this, paramList, paramae, null);
    localObject = (c.g.a.m)localObject;
    kotlinx.coroutines.e.b(this, localf, (c.g.a.m)localObject, 2);
  }
  
  public final boolean a(android.support.v4.app.f paramf, Contact paramContact, String paramString)
  {
    k.b(paramString, "analyticsContext");
    c.d.f localf = null;
    if (paramContact == null) {
      return false;
    }
    Object localObject1 = g;
    boolean bool = ((com.truecaller.utils.i)localObject1).a();
    if (!bool)
    {
      com.truecaller.utils.extensions.i.a(e, 2131888998, null, 0, 6);
      a(paramString, paramContact);
      paramf = c;
      paramContact = VoipAnalyticsFailedCallAction.OFFLINE;
      paramf.a(paramString, paramContact);
      return false;
    }
    localf = d;
    Object localObject2 = new com/truecaller/voip/aj$e;
    localObject1 = localObject2;
    ((aj.e)localObject2).<init>(this, paramContact, paramString, paramf, null);
    localObject2 = (c.g.a.m)localObject2;
    kotlinx.coroutines.e.b(this, localf, (c.g.a.m)localObject2, 2);
    return true;
  }
  
  public final boolean a(String paramString1, String paramString2)
  {
    k.b(paramString1, "number");
    k.b(paramString2, "analyticsContext");
    Object localObject = h.b(paramString1);
    if (localObject != null) {
      paramString1 = (String)localObject;
    }
    c.a(paramString2, paramString1);
    localObject = g;
    boolean bool1 = ((com.truecaller.utils.i)localObject).a();
    if (!bool1)
    {
      com.truecaller.utils.extensions.i.a(e, 2131888998, null, 0, 6);
      paramString1 = c;
      localObject = VoipAnalyticsFailedCallAction.OFFLINE;
      paramString1.a(paramString2, (VoipAnalyticsFailedCallAction)localObject);
      return false;
    }
    localObject = k.b();
    bool1 = k.a(localObject, paramString1);
    if (bool1) {
      return false;
    }
    localObject = f;
    ((d)localObject).a(paramString1, paramString2);
    paramString2 = i.e();
    boolean bool2 = paramString2.a();
    if (bool2)
    {
      paramString2 = (a)j.a();
      paramString2.b(paramString1);
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */