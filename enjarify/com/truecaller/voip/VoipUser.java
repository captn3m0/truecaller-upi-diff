package com.truecaller.voip;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class VoipUser
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  public final String b;
  public final String c;
  public final String d;
  public final boolean e;
  public final Integer f;
  public final Integer g;
  
  static
  {
    VoipUser.a locala = new com/truecaller/voip/VoipUser$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public VoipUser(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, Integer paramInteger1, Integer paramInteger2)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramString4;
    e = paramBoolean;
    f = paramInteger1;
    g = paramInteger2;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof VoipUser;
      if (bool2)
      {
        paramObject = (VoipUser)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              localObject1 = d;
              localObject2 = d;
              bool2 = k.a(localObject1, localObject2);
              if (bool2)
              {
                bool2 = e;
                boolean bool3 = e;
                if (bool2 == bool3)
                {
                  bool2 = true;
                }
                else
                {
                  bool2 = false;
                  localObject1 = null;
                }
                if (bool2)
                {
                  localObject1 = f;
                  localObject2 = f;
                  bool2 = k.a(localObject1, localObject2);
                  if (bool2)
                  {
                    localObject1 = g;
                    paramObject = g;
                    boolean bool4 = k.a(localObject1, paramObject);
                    if (bool4) {
                      return bool1;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = b;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = d;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    int m = e;
    if (m != 0) {
      m = 1;
    }
    j = (j + m) * 31;
    localObject = f;
    int n;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    j = (j + n) * 31;
    localObject = g;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VoipUser(id=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", number=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", name=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", pictureUrl=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", blocked=");
    boolean bool = e;
    localStringBuilder.append(bool);
    localStringBuilder.append(", spamScore=");
    localObject = f;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", rtcUid=");
    localObject = g;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = b;
    paramParcel.writeString((String)localObject);
    localObject = c;
    paramParcel.writeString((String)localObject);
    localObject = d;
    paramParcel.writeString((String)localObject);
    paramInt = e;
    paramParcel.writeInt(paramInt);
    localObject = f;
    int i = 1;
    if (localObject != null)
    {
      paramParcel.writeInt(i);
      paramInt = ((Integer)localObject).intValue();
      paramParcel.writeInt(paramInt);
    }
    else
    {
      paramParcel.writeInt(0);
    }
    localObject = g;
    if (localObject != null)
    {
      paramParcel.writeInt(i);
      paramInt = ((Integer)localObject).intValue();
      paramParcel.writeInt(paramInt);
      return;
    }
    paramParcel.writeInt(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.VoipUser
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */