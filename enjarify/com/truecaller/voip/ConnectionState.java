package com.truecaller.voip;

public enum ConnectionState
{
  private final Integer callStatusColor;
  private final Boolean showAvatarRing;
  private final Boolean startTimer;
  private final Integer statusId;
  
  static
  {
    ConnectionState[] arrayOfConnectionState = new ConnectionState[3];
    ConnectionState localConnectionState1 = new com/truecaller/voip/ConnectionState;
    ConnectionState localConnectionState2 = localConnectionState1;
    localConnectionState1.<init>("CONNECTED", 0, null, null, null, null, 15, null);
    CONNECTED = localConnectionState1;
    arrayOfConnectionState[0] = localConnectionState1;
    localConnectionState2 = new com/truecaller/voip/ConnectionState;
    Integer localInteger = Integer.valueOf(R.string.voip_status_reconnecting);
    Object localObject1 = Integer.valueOf(R.color.voip_call_status_warning_color);
    Object localObject2 = Boolean.TRUE;
    Object localObject3 = Boolean.FALSE;
    localConnectionState2.<init>("INTERRUPTED", 1, localInteger, (Boolean)localObject3, (Integer)localObject1, (Boolean)localObject2);
    INTERRUPTED = localConnectionState2;
    arrayOfConnectionState[1] = localConnectionState2;
    localConnectionState2 = new com/truecaller/voip/ConnectionState;
    localObject3 = Integer.valueOf(R.string.voip_status_call_failed);
    localObject2 = Integer.valueOf(R.color.voip_call_status_error_color);
    Boolean localBoolean = Boolean.TRUE;
    localObject1 = Boolean.FALSE;
    localConnectionState2.<init>("DISCONNECTED", 2, (Integer)localObject3, (Boolean)localObject1, (Integer)localObject2, localBoolean);
    DISCONNECTED = localConnectionState2;
    arrayOfConnectionState[2] = localConnectionState2;
    $VALUES = arrayOfConnectionState;
  }
  
  private ConnectionState(Integer paramInteger1, Boolean paramBoolean1, Integer paramInteger2, Boolean paramBoolean2)
  {
    statusId = paramInteger1;
    startTimer = paramBoolean1;
    callStatusColor = paramInteger2;
    showAvatarRing = paramBoolean2;
  }
  
  public final Integer getCallStatusColor()
  {
    return callStatusColor;
  }
  
  public final Boolean getShowAvatarRing()
  {
    return showAvatarRing;
  }
  
  public final Boolean getStartTimer()
  {
    return startTimer;
  }
  
  public final Integer getStatusId()
  {
    return statusId;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.ConnectionState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */