package com.truecaller.voip;

import c.d.b.a.b;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker.a;
import com.truecaller.voip.manager.rtm.FailedRtmLoginReason;
import com.truecaller.voip.manager.rtm.g;
import com.truecaller.voip.manager.rtm.h.a;
import com.truecaller.voip.manager.rtm.i;
import com.truecaller.voip.manager.rtm.j;
import com.truecaller.voip.util.VoipEventType;
import com.truecaller.voip.util.ab;
import com.truecaller.voip.util.y;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.cn;
import kotlinx.coroutines.e;

final class o$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  Object d;
  int e;
  private ag h;
  
  o$a(o paramo, af paramaf, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/voip/o$a;
    o localo = f;
    af localaf = g;
    locala.<init>(localo, localaf, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = e;
    int j = 2;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    int k;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      localObject1 = (u)d;
      localObject2 = (String)c;
      localObject3 = (ag)a;
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label501;
      }
      try
      {
        paramObject = (o.b)paramObject;
        paramObject = a;
        throw ((Throwable)paramObject);
      }
      finally
      {
        break label708;
      }
    case 1: 
      localObject2 = (String)c;
      localObject3 = (String)b;
      localObject4 = (ag)a;
      k = paramObject instanceof o.b;
      if (k == 0) {
        break label318;
      }
      throw a;
    }
    boolean bool1 = paramObject instanceof o.b;
    if (!bool1)
    {
      paramObject = h;
      localObject2 = g;
      localObject3 = c;
      if (localObject3 == null) {
        return x.a;
      }
      localObject2 = g.d;
      if (localObject2 == null) {
        return x.a;
      }
      localObject4 = f;
      Object localObject5 = g;
      o.a((o)localObject4, (af)localObject5);
      localObject4 = f;
      localObject5 = g;
      o.b((o)localObject4, (af)localObject5);
      localObject4 = (com.truecaller.voip.manager.rtm.h)o.g(f).get();
      a = paramObject;
      b = localObject3;
      c = localObject2;
      k = 1;
      e = k;
      localObject4 = h.a.a((com.truecaller.voip.manager.rtm.h)localObject4, this);
      if (localObject4 == localObject1) {
        return localObject1;
      }
      Object localObject6 = localObject4;
      localObject4 = paramObject;
      paramObject = localObject6;
      label318:
      paramObject = (g)paramObject;
      k = paramObject instanceof j;
      if (k == 0)
      {
        k = paramObject instanceof com.truecaller.voip.manager.rtm.f;
        if (k != 0)
        {
          localObject1 = g.h;
          if (localObject1 != null)
          {
            localObject2 = f;
            paramObject = a;
            o.a((o)localObject2, (FailedRtmLoginReason)paramObject, (String)localObject3, (String)localObject1);
          }
          new String[1][0] = "Cannot login to RTM after push.";
          return x.a;
        }
      }
      paramObject = ((i)o.h(f).get()).a().a();
      long l1 = 10000L;
      try
      {
        Object localObject8 = new com/truecaller/voip/o$a$a;
        ((o.a.a)localObject8).<init>(this, (String)localObject3, (u)paramObject, null);
        localObject8 = (m)localObject8;
        a = localObject4;
        b = localObject3;
        c = localObject2;
        d = paramObject;
        e = j;
        localObject3 = cn.a(l1, (m)localObject8, this);
        if (localObject3 == localObject1) {
          return localObject1;
        }
        localObject1 = paramObject;
        paramObject = localObject3;
        localObject3 = localObject4;
        label501:
        paramObject = (Boolean)paramObject;
        boolean bool3;
        if (paramObject != null)
        {
          bool3 = ((Boolean)paramObject).booleanValue();
        }
        else
        {
          bool3 = false;
          paramObject = null;
        }
        if (!bool3)
        {
          paramObject = "Did not receive invite message after 10000 milliseconds. Showing missed call notification.";
          new String[1][0] = paramObject;
          paramObject = f;
          paramObject = o.i((o)paramObject);
          paramObject = ((dagger.a)paramObject).get();
          paramObject = (y)paramObject;
          ab localab = new com/truecaller/voip/util/ab;
          localObject4 = "+";
          localObject2 = String.valueOf(localObject2);
          localObject5 = ((String)localObject4).concat((String)localObject2);
          VoipEventType localVoipEventType = VoipEventType.MISSED;
          long l2 = 0L;
          localObject2 = g;
          long l3 = a;
          Long localLong = b.a(l3);
          int m = 4;
          localObject4 = localab;
          localab.<init>((String)localObject5, localVoipEventType, l2, localLong, m);
          ((y)paramObject).a(localab);
          paramObject = MissedVoipCallsWorker.e;
          MissedVoipCallsWorker.a.a();
          paramObject = f;
          paramObject = o.j((o)paramObject);
          localObject2 = new com/truecaller/voip/o$a$1;
          ((o.a.1)localObject2).<init>(this, null);
          localObject2 = (m)localObject2;
          e.b((ag)localObject3, (c.d.f)paramObject, (m)localObject2, j);
        }
        ((u)localObject1).n();
        return x.a;
      }
      finally
      {
        localObject1 = paramObject;
        paramObject = localObject7;
      }
      label708:
      ((u)localObject1).n();
      throw ((Throwable)paramObject);
    }
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.o.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */