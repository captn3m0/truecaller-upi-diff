package com.truecaller.voip;

import android.content.Context;
import c.g.b.k;
import c.l;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callhistory.a;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.voip.util.VoipEventType;
import com.truecaller.voip.util.ab;
import com.truecaller.voip.util.y;
import java.util.concurrent.TimeUnit;

public final class f
  implements y
{
  private final Context a;
  
  public f(Context paramContext)
  {
    a = paramContext;
  }
  
  public final void a(ab paramab)
  {
    k.b(paramab, "event");
    HistoryEvent localHistoryEvent = new com/truecaller/data/entity/HistoryEvent;
    Object localObject = a;
    localHistoryEvent.<init>((String)localObject);
    k.b(paramab, "receiver$0");
    localObject = b;
    int[] arrayOfInt = g.a;
    int i = ((VoipEventType)localObject).ordinal();
    i = arrayOfInt[i];
    int j = 3;
    int k = 1;
    switch (i)
    {
    default: 
      paramab = new c/l;
      paramab.<init>();
      throw paramab;
    case 2: 
      j = 2;
      break;
    case 1: 
      j = 1;
    }
    localHistoryEvent.a(j);
    k.b(paramab, "receiver$0");
    localObject = b;
    arrayOfInt = g.b;
    i = ((VoipEventType)localObject).ordinal();
    i = arrayOfInt[i];
    if (i != k) {
      k = 0;
    }
    localHistoryEvent.b(k);
    localHistoryEvent.b("com.truecaller.voip.manager.VOIP");
    localObject = TimeUnit.MILLISECONDS;
    long l1 = c;
    long l2 = ((TimeUnit)localObject).toSeconds(l1);
    localHistoryEvent.b(l2);
    paramab = d;
    if (paramab != null)
    {
      paramab = (Number)paramab;
      l2 = paramab.longValue();
      localHistoryEvent.a(l2);
    }
    paramab = a.getApplicationContext();
    if (paramab != null)
    {
      paramab = ((bk)paramab).a();
      k.a(paramab, "(context.applicationCont…GraphHolder).objectsGraph");
      paramab = paramab.ad();
      k.a(paramab, "graph.callHistoryManager()");
      ((a)paramab.a()).a(localHistoryEvent);
      return;
    }
    paramab = new c/u;
    paramab.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramab;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */