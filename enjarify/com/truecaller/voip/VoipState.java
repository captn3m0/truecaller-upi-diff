package com.truecaller.voip;

public enum VoipState
{
  static
  {
    VoipState[] arrayOfVoipState = new VoipState[14];
    VoipState localVoipState = new com/truecaller/voip/VoipState;
    localVoipState.<init>("INITIAL", 0);
    INITIAL = localVoipState;
    arrayOfVoipState[0] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    int i = 1;
    localVoipState.<init>("CONNECTING", i);
    CONNECTING = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 2;
    localVoipState.<init>("INVITING", i);
    INVITING = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 3;
    localVoipState.<init>("INVITED", i);
    INVITED = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 4;
    localVoipState.<init>("OFFLINE", i);
    OFFLINE = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 5;
    localVoipState.<init>("RINGING", i);
    RINGING = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 6;
    localVoipState.<init>("NO_ANSWER", i);
    NO_ANSWER = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 7;
    localVoipState.<init>("ACCEPTED", i);
    ACCEPTED = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 8;
    localVoipState.<init>("REJECTED", i);
    REJECTED = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 9;
    localVoipState.<init>("BUSY", i);
    BUSY = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 10;
    localVoipState.<init>("ONGOING", i);
    ONGOING = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 11;
    localVoipState.<init>("BLOCKED", i);
    BLOCKED = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 12;
    localVoipState.<init>("ENDED", i);
    ENDED = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    localVoipState = new com/truecaller/voip/VoipState;
    i = 13;
    localVoipState.<init>("FAILED", i);
    FAILED = localVoipState;
    arrayOfVoipState[i] = localVoipState;
    $VALUES = arrayOfVoipState;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.VoipState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */