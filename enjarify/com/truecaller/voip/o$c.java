package com.truecaller.voip;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.b;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.tcpermissions.PermissionRequestOptions;
import com.truecaller.tcpermissions.d;
import com.truecaller.tcpermissions.l;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.VoipService.a;
import com.truecaller.voip.util.VoipAnalyticsFailedCallAction;
import com.truecaller.voip.util.an;
import java.util.Arrays;
import kotlinx.coroutines.ag;

final class o$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag f;
  
  o$c(o paramo, String paramString1, String paramString2, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/voip/o$c;
    o localo = c;
    String str1 = d;
    String str2 = e;
    localc.<init>(localo, str1, str2, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label305;
      }
      paramObject = new com/truecaller/tcpermissions/PermissionRequestOptions;
      int j = R.string.voip_permissions_denied_explanation;
      localObject2 = Integer.valueOf(j);
      int k = 3;
      ((PermissionRequestOptions)paramObject).<init>(false, false, (Integer)localObject2, k);
      localObject2 = o.a(c);
      String[] arrayOfString = o.b(c).e();
      int m = arrayOfString.length;
      arrayOfString = (String[])Arrays.copyOf(arrayOfString, m);
      a = paramObject;
      m = 1;
      b = m;
      paramObject = ((com.truecaller.tcpermissions.o)localObject2).a((PermissionRequestOptions)paramObject, arrayOfString, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (d)paramObject;
    boolean bool2 = a;
    if (bool2)
    {
      boolean bool3 = b;
      if (!bool3)
      {
        paramObject = o.d(c);
        localObject1 = e;
        bool3 = ((an)paramObject).a((String)localObject1);
        if (!bool3)
        {
          paramObject = o.e(c);
          localObject1 = VoipService.e;
          localObject1 = o.e(c);
          localObject2 = e;
          localObject1 = VoipService.a.a((Context)localObject1, (String)localObject2);
          b.a((Context)paramObject, (Intent)localObject1);
        }
        return x.a;
      }
    }
    paramObject = o.c(c);
    localObject1 = d;
    Object localObject2 = VoipAnalyticsFailedCallAction.NO_MIC_PERMISSION;
    ((com.truecaller.voip.util.o)paramObject).a((String)localObject1, (VoipAnalyticsFailedCallAction)localObject2);
    return x.a;
    label305:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.o.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */