package com.truecaller.voip;

import android.content.Context;
import c.g.b.k;
import com.google.gson.f;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import com.truecaller.voip.db.VoipDatabase;
import com.truecaller.voip.db.VoipDatabase.a;
import com.truecaller.voip.manager.rtm.c;
import com.truecaller.voip.util.aa;
import com.truecaller.voip.util.af;
import com.truecaller.voip.util.at;
import com.truecaller.voip.util.l;
import com.truecaller.voip.util.y;
import com.truecaller.voip.util.z;
import io.agora.rtm.RtmClient;

public abstract class t
{
  public static final t.a a;
  
  static
  {
    t.a locala = new com/truecaller/voip/t$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final f a()
  {
    f localf = new com/google/gson/f;
    localf.<init>();
    return localf;
  }
  
  public static final com.truecaller.voip.db.a a(Context paramContext)
  {
    k.b(paramContext, "context");
    VoipDatabase.a locala = VoipDatabase.g;
    paramContext = locala.a(paramContext);
    if (paramContext != null)
    {
      paramContext = paramContext.h();
      if (paramContext != null) {
        return paramContext;
      }
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Cannot initialize voip database");
    throw ((Throwable)paramContext);
  }
  
  public static final RtmClient a(c paramc)
  {
    k.b(paramc, "manager");
    return c;
  }
  
  public static final com.truecaller.voip.api.a b()
  {
    return (com.truecaller.voip.api.a)h.a(KnownEndpoints.VOIP, com.truecaller.voip.api.a.class);
  }
  
  public static final y c()
  {
    Object localObject = d.a;
    localObject = d.a.a();
    if (localObject != null) {
      return (y)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Voip history model should be set");
    throw ((Throwable)localObject);
  }
  
  public static final af d()
  {
    Object localObject = d.a;
    localObject = d.a.b();
    if (localObject != null) {
      return (af)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Voip missed call provider should be set");
    throw ((Throwable)localObject);
  }
  
  public static final aa e()
  {
    Object localObject = d.a;
    localObject = d.a.c();
    if (localObject != null) {
      return (aa)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Voip caller info provider should be set");
    throw ((Throwable)localObject);
  }
  
  public static final z f()
  {
    Object localObject = d.a;
    localObject = d.a.d();
    if (localObject != null) {
      return (z)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Voip call message should be set");
    throw ((Throwable)localObject);
  }
  
  public static final l g()
  {
    Object localObject = d.a;
    localObject = d.a.e();
    if (localObject != null) {
      return (l)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Presence reporter should be set");
    throw ((Throwable)localObject);
  }
  
  public static final at h()
  {
    Object localObject = d.a;
    localObject = d.a.f();
    if (localObject != null) {
      return (at)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Logo helper should be set");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */