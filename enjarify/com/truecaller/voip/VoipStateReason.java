package com.truecaller.voip;

public enum VoipStateReason
{
  static
  {
    VoipStateReason[] arrayOfVoipStateReason = new VoipStateReason[16];
    VoipStateReason localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    localVoipStateReason.<init>("SEARCH_FAILED", 0);
    SEARCH_FAILED = localVoipStateReason;
    arrayOfVoipStateReason[0] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    int i = 1;
    localVoipStateReason.<init>("GET_PHONE_FAILED", i);
    GET_PHONE_FAILED = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 2;
    localVoipStateReason.<init>("GET_VOIP_ID_FAILED", i);
    GET_VOIP_ID_FAILED = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 3;
    localVoipStateReason.<init>("GET_RTM_TOKEN_FAILED", i);
    GET_RTM_TOKEN_FAILED = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 4;
    localVoipStateReason.<init>("GET_RTC_TOKEN_FAILED", i);
    GET_RTC_TOKEN_FAILED = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 5;
    localVoipStateReason.<init>("LOGIN_RTM_FAILED", i);
    LOGIN_RTM_FAILED = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 6;
    localVoipStateReason.<init>("JOIN_CHANNEL_FAILED", i);
    JOIN_CHANNEL_FAILED = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 7;
    localVoipStateReason.<init>("PEER_LEFT_CHANNEL", i);
    PEER_LEFT_CHANNEL = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 8;
    localVoipStateReason.<init>("EMPTY_CHANNEL", i);
    EMPTY_CHANNEL = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 9;
    localVoipStateReason.<init>("CONNECTION_STATE_CHANGED", i);
    CONNECTION_STATE_CHANGED = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 10;
    localVoipStateReason.<init>("INVITE_FAILED", i);
    INVITE_FAILED = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 11;
    localVoipStateReason.<init>("INVITE_TIMEOUT", i);
    INVITE_TIMEOUT = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 12;
    localVoipStateReason.<init>("WAKE_UP_TIMEOUT", i);
    WAKE_UP_TIMEOUT = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 13;
    localVoipStateReason.<init>("INVITE_CANCELLED", i);
    INVITE_CANCELLED = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    i = 14;
    localVoipStateReason.<init>("HUNG_UP", i);
    HUNG_UP = localVoipStateReason;
    arrayOfVoipStateReason[i] = localVoipStateReason;
    localVoipStateReason = new com/truecaller/voip/VoipStateReason;
    localVoipStateReason.<init>("RECEIVED_END", 15);
    RECEIVED_END = localVoipStateReason;
    arrayOfVoipStateReason[15] = localVoipStateReason;
    $VALUES = arrayOfVoipStateReason;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.VoipStateReason
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */