package com.truecaller.voip.incall;

import c.g.a.m;
import c.l;
import com.truecaller.ba;
import com.truecaller.notificationchannels.b;
import com.truecaller.utils.n;
import com.truecaller.voip.ConnectionState;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.ag;
import com.truecaller.voip.manager.g;
import com.truecaller.voip.util.AudioRoute;
import com.truecaller.voip.util.VoipAnalyticsCallDirection;
import com.truecaller.voip.util.VoipAnalyticsContext;
import com.truecaller.voip.util.VoipAnalyticsNotificationAction;
import com.truecaller.voip.util.aa;
import com.truecaller.voip.util.an;
import com.truecaller.voip.util.au;
import com.truecaller.voip.util.j;
import com.truecaller.voip.util.o;
import com.truecaller.voip.util.s;
import com.truecaller.voip.util.w;
import com.truecaller.voip.util.y;
import kotlinx.coroutines.a.p;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class c
  extends ba
  implements b.c
{
  private final j A;
  private final com.truecaller.voip.util.f B;
  private final n C;
  private final s D;
  private final com.truecaller.voip.manager.rtm.h E;
  private final o F;
  private final an G;
  b.b c;
  private String d;
  private VoipUser e;
  private boolean f;
  private String g;
  private ag h;
  private com.truecaller.voip.manager.k i;
  private com.truecaller.voip.manager.k j;
  private boolean k;
  private long l;
  private final kotlinx.coroutines.a.h m;
  private final kotlinx.coroutines.a.h n;
  private final kotlinx.coroutines.a.h o;
  private final p p;
  private final c.d.f q;
  private final c.d.f r;
  private final g s;
  private final com.truecaller.voip.manager.rtm.i t;
  private final aa u;
  private final au v;
  private final b w;
  private final com.truecaller.utils.a x;
  private final w y;
  private final y z;
  
  public c(c.d.f paramf1, c.d.f paramf2, g paramg, com.truecaller.voip.manager.rtm.i parami, aa paramaa, au paramau, b paramb, com.truecaller.utils.a parama, w paramw, y paramy, j paramj, com.truecaller.voip.util.f paramf, n paramn, s params, com.truecaller.voip.manager.rtm.h paramh, o paramo, an paraman)
  {
    super(paramf1);
    q = paramf1;
    r = paramf2;
    s = paramg;
    t = parami;
    u = paramaa;
    v = paramau;
    w = paramb;
    x = parama;
    y = paramw;
    z = paramy;
    A = paramj;
    B = paramf;
    C = paramn;
    D = params;
    localObject1 = paramh;
    E = paramh;
    F = paramo;
    localObject1 = paraman;
    G = paraman;
    localObject1 = new com/truecaller/voip/ag;
    paramf1 = (c.d.f)localObject1;
    int i1 = 255;
    ((ag)localObject1).<init>(null, null, 0, 0, false, null, false, i1);
    h = ((ag)localObject1);
    localObject1 = new com/truecaller/voip/manager/k;
    ((com.truecaller.voip.manager.k)localObject1).<init>();
    i = ((com.truecaller.voip.manager.k)localObject1);
    localObject1 = new com/truecaller/voip/manager/k;
    ((com.truecaller.voip.manager.k)localObject1).<init>();
    j = ((com.truecaller.voip.manager.k)localObject1);
    int i2 = -1;
    localObject2 = kotlinx.coroutines.a.i.a(i2);
    m = ((kotlinx.coroutines.a.h)localObject2);
    localObject2 = kotlinx.coroutines.a.i.a(i2);
    n = ((kotlinx.coroutines.a.h)localObject2);
    localObject1 = kotlinx.coroutines.a.i.a(i2);
    o = ((kotlinx.coroutines.a.h)localObject1);
    localObject1 = new kotlinx/coroutines/a/p;
    localObject2 = i;
    ((p)localObject1).<init>(localObject2);
    p = ((p)localObject1);
  }
  
  private final void a(VoipState paramVoipState, VoipStateReason paramVoipStateReason)
  {
    c localc = this;
    boolean bool1 = a(paramVoipState);
    if (!bool1) {
      return;
    }
    bool1 = true;
    Object localObject1 = new String[bool1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Setting state: ");
    Object localObject3 = paramVoipState.name();
    ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = (c.g.a.a)c.ak.a;
    Object localObject4 = (c.g.a.a)c.aj.a;
    localObject2 = d.f;
    int i1 = paramVoipState.ordinal();
    int i2 = localObject2[i1];
    Object localObject5;
    int i3;
    int i4;
    boolean bool2;
    String str1;
    boolean bool3;
    int i5;
    int i6;
    int i7;
    boolean bool4;
    String str2;
    int i8;
    switch (i2)
    {
    default: 
      localObject1 = new c/l;
      ((l)localObject1).<init>();
      throw ((Throwable)localObject1);
    case 14: 
      localObject4 = new com/truecaller/voip/incall/c$aa;
      ((c.aa)localObject4).<init>(this);
      localObject4 = (c.g.a.a)localObject4;
      localObject5 = new com/truecaller/voip/ag;
      i3 = R.string.voip_status_call_failed;
      i4 = R.color.voip_call_status_error_color;
      bool2 = true;
      str1 = "Call failed. Exiting...";
      bool3 = false;
      i5 = 134;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, i3, i4, bool2, str1, false, i5);
      break;
    case 13: 
      localObject4 = new com/truecaller/voip/incall/c$z;
      ((c.z)localObject4).<init>(this);
      localObject4 = (c.g.a.a)localObject4;
      localObject5 = new com/truecaller/voip/ag;
      i3 = R.string.voip_status_call_ended;
      i4 = R.color.voip_call_status_error_color;
      bool2 = true;
      str1 = "Call ended. Exiting...";
      bool3 = false;
      i5 = 134;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, i3, i4, bool2, str1, false, i5);
      break;
    case 12: 
      localObject4 = new com/truecaller/voip/incall/c$y;
      ((c.y)localObject4).<init>(this);
      localObject4 = (c.g.a.a)localObject4;
      localObject5 = new com/truecaller/voip/ag;
      i3 = 0;
      i4 = 0;
      bool2 = false;
      str1 = "Call blocked. Exiting...";
      bool3 = false;
      i5 = 190;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, 0, 0, false, str1, false, i5);
      break;
    case 11: 
      localObject1 = new com/truecaller/voip/incall/c$w;
      ((c.w)localObject1).<init>(this);
      localObject1 = (c.g.a.a)localObject1;
      localObject4 = new com/truecaller/voip/incall/c$x;
      ((c.x)localObject4).<init>(this);
      localObject4 = (c.g.a.a)localObject4;
      localObject5 = new com/truecaller/voip/ag;
      i3 = 0;
      str1 = "Channel joined. Say hello!";
      i4 = R.color.voip_call_status_ok_color;
      bool2 = true;
      bool3 = true;
      i5 = 14;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, 0, i4, bool2, str1, bool3, i5);
      break;
    case 10: 
      localObject1 = new com/truecaller/voip/incall/c$u;
      ((c.u)localObject1).<init>(this);
      localObject1 = (c.g.a.a)localObject1;
      localObject4 = new com/truecaller/voip/incall/c$v;
      ((c.v)localObject4).<init>(this);
      localObject4 = (c.g.a.a)localObject4;
      localObject5 = new com/truecaller/voip/ag;
      i3 = R.string.voip_status_busy;
      i4 = R.color.voip_call_status_warning_color;
      bool2 = true;
      str1 = "User is in another call. Exiting...";
      bool3 = false;
      i5 = 134;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, i3, i4, bool2, str1, false, i5);
      break;
    case 9: 
      localObject1 = new com/truecaller/voip/incall/c$ah;
      ((c.ah)localObject1).<init>(this);
      localObject1 = (c.g.a.a)localObject1;
      localObject4 = new com/truecaller/voip/incall/c$ai;
      ((c.ai)localObject4).<init>(this);
      localObject4 = (c.g.a.a)localObject4;
      localObject5 = new com/truecaller/voip/ag;
      i3 = R.string.voip_status_rejected;
      i4 = R.color.voip_call_status_error_color;
      bool2 = true;
      str1 = "Invite rejected. Exiting...";
      bool3 = false;
      i5 = 134;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, i3, i4, bool2, str1, false, i5);
      break;
    case 8: 
      localObject2 = new com/truecaller/voip/ag;
      i6 = 0;
      i7 = 0;
      bool4 = false;
      str2 = "Invite accepted.";
      i8 = 190;
      ((ag)localObject2).<init>(paramVoipState, null, 0, 0, false, str2, false, i8);
      localObject5 = localObject2;
      break;
    case 7: 
      localObject1 = new com/truecaller/voip/incall/c$af;
      ((c.af)localObject1).<init>(this);
      localObject1 = (c.g.a.a)localObject1;
      localObject4 = new com/truecaller/voip/incall/c$ag;
      ((c.ag)localObject4).<init>(this);
      localObject4 = (c.g.a.a)localObject4;
      localObject5 = new com/truecaller/voip/ag;
      i3 = R.string.voip_status_offline;
      i4 = R.color.voip_call_status_error_color;
      bool2 = true;
      str1 = "Invite failed. User is offline. Exiting...";
      bool3 = false;
      i5 = 134;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, i3, i4, bool2, str1, false, i5);
      break;
    case 6: 
      localObject4 = new com/truecaller/voip/incall/c$ae;
      ((c.ae)localObject4).<init>(this);
      localObject4 = (c.g.a.a)localObject4;
      localObject5 = new com/truecaller/voip/ag;
      i3 = R.string.voip_status_no_answer;
      i4 = R.color.voip_call_status_error_color;
      bool2 = true;
      str1 = "User did not answer. Exiting...";
      bool3 = false;
      i5 = 134;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, i3, i4, bool2, str1, false, i5);
      break;
    case 5: 
      localObject4 = new com/truecaller/voip/incall/c$ad;
      ((c.ad)localObject4).<init>(this);
      localObject4 = (c.g.a.a)localObject4;
      localObject5 = new com/truecaller/voip/ag;
      i3 = R.string.voip_status_ringing;
      i4 = R.color.voip_call_status_ok_color;
      bool2 = true;
      str1 = "Invite received. Ringing...";
      bool3 = false;
      i5 = 134;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, i3, i4, bool2, str1, false, i5);
      break;
    case 4: 
      localObject2 = new com/truecaller/voip/ag;
      i6 = R.string.voip_status_connecting;
      i7 = R.color.voip_call_status_warning_color;
      bool4 = true;
      str2 = "Invite is received by peer. Waiting for ringing message...";
      i8 = 134;
      ((ag)localObject2).<init>(paramVoipState, null, i6, i7, bool4, str2, false, i8);
      localObject5 = localObject2;
      break;
    case 3: 
      localObject1 = new com/truecaller/voip/incall/c$ac;
      ((c.ac)localObject1).<init>(this);
      localObject1 = (c.g.a.a)localObject1;
      localObject5 = new com/truecaller/voip/ag;
      i3 = R.string.voip_status_connecting;
      i4 = R.color.voip_call_status_warning_color;
      bool2 = true;
      str1 = "Inviting user to voip call...";
      bool3 = false;
      i5 = 134;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, i3, i4, bool2, str1, false, i5);
      break;
    case 2: 
      localObject1 = new com/truecaller/voip/incall/c$t;
      ((c.t)localObject1).<init>(this);
      localObject1 = (c.g.a.a)localObject1;
      localObject4 = new com/truecaller/voip/incall/c$ab;
      ((c.ab)localObject4).<init>(this);
      localObject4 = (c.g.a.a)localObject4;
      localObject5 = new com/truecaller/voip/ag;
      i3 = R.string.voip_status_connecting;
      i4 = R.color.voip_call_status_warning_color;
      bool2 = true;
      str1 = "Initializing, resolving the user...";
      bool3 = false;
      i5 = 134;
      localObject2 = localObject5;
      localObject3 = paramVoipState;
      ((ag)localObject5).<init>(paramVoipState, null, i3, i4, bool2, str1, false, i5);
      break;
    case 1: 
      localObject5 = h;
    }
    h = ((ag)localObject5);
    localObject2 = ag.a(h, null, paramVoipStateReason, null, 0, 0, false, null, false, 253);
    h = ((ag)localObject2);
    ((c.g.a.a)localObject1).invoke();
    m();
    ((c.g.a.a)localObject4).invoke();
    localObject1 = n;
    localObject4 = h;
    ((kotlinx.coroutines.a.h)localObject1).d_(localObject4);
  }
  
  private final void a(com.truecaller.voip.manager.k paramk)
  {
    com.truecaller.voip.manager.k localk = i;
    boolean bool = c.g.b.k.a(paramk, localk);
    if (bool) {
      return;
    }
    i = paramk;
    p.d_(paramk);
  }
  
  private final boolean a(VoipState paramVoipState)
  {
    VoipState localVoipState = h.a;
    if (paramVoipState == localVoipState) {
      return false;
    }
    boolean bool = q();
    return !bool;
  }
  
  private final void d(boolean paramBoolean)
  {
    Object localObject = i;
    boolean bool = b;
    if (paramBoolean != bool)
    {
      bool = q();
      if (!bool)
      {
        localObject = com.truecaller.voip.manager.k.a(i, false, paramBoolean, null, 5);
        a((com.truecaller.voip.manager.k)localObject);
        n();
        m();
        k();
        localObject = o;
        Boolean localBoolean = Boolean.valueOf(paramBoolean);
        ((kotlinx.coroutines.a.h)localObject).d_(localBoolean);
        return;
      }
    }
  }
  
  private final bn k()
  {
    Object localObject = new com/truecaller/voip/incall/c$r;
    ((c.r)localObject).<init>(this, null);
    localObject = (m)localObject;
    return e.b(this, null, (m)localObject, 3);
  }
  
  private final VoipAnalyticsCallDirection l()
  {
    boolean bool = k;
    if (bool) {
      return VoipAnalyticsCallDirection.INCOMING;
    }
    return VoipAnalyticsCallDirection.OUTGOING;
  }
  
  private final void m()
  {
    Object localObject1 = p();
    Object localObject2 = (b.d)b;
    int i1;
    if (localObject2 != null)
    {
      i1 = ((ag)localObject1).a();
      ((b.d)localObject2).b(i1);
    }
    localObject2 = c;
    if (localObject2 != null)
    {
      i1 = ((ag)localObject1).a();
      int i2 = ((ag)localObject1).c();
      boolean bool2 = ((ag)localObject1).d();
      ((b.b)localObject2).a(i1, i2, bool2);
    }
    localObject2 = (b.d)b;
    boolean bool1;
    long l1;
    if (localObject2 != null)
    {
      bool1 = ((ag)localObject1).b();
      l1 = l;
      ((b.d)localObject2).a(bool1, l1);
    }
    localObject2 = c;
    if (localObject2 != null)
    {
      bool1 = ((ag)localObject1).b();
      l1 = l;
      ((b.b)localObject2).a(bool1, l1);
    }
    localObject2 = c;
    if (localObject2 != null)
    {
      localObject1 = e;
      ((b.b)localObject2).a((String)localObject1);
    }
    localObject1 = B;
    localObject2 = h.a;
    ConnectionState localConnectionState = h.c;
    com.truecaller.voip.manager.k localk1 = i;
    com.truecaller.voip.manager.k localk2 = j;
    ((com.truecaller.voip.util.f)localObject1).a((VoipState)localObject2, localConnectionState, localk1, localk2);
  }
  
  private final void n()
  {
    g localg = s;
    AudioRoute localAudioRoute1 = i.c.a;
    AudioRoute localAudioRoute2 = AudioRoute.SPEAKER;
    int i1 = 1;
    if (localAudioRoute1 == localAudioRoute2)
    {
      bool = true;
    }
    else
    {
      bool = false;
      localAudioRoute1 = null;
    }
    localg.a(bool);
    localg = s;
    boolean bool = i.b ^ i1;
    localg.c(bool);
    localg = s;
    bool = i.a;
    localg.b(bool);
  }
  
  private final bn o()
  {
    Object localObject = new com/truecaller/voip/incall/c$a;
    ((c.a)localObject).<init>(this, null);
    localObject = (m)localObject;
    return e.b(this, null, (m)localObject, 3);
  }
  
  private final ag p()
  {
    Object localObject1 = i;
    boolean bool = b;
    if (!bool)
    {
      localObject1 = j;
      bool = b;
      if (!bool)
      {
        localObject1 = j;
        bool = a;
        if (bool)
        {
          localObject1 = h.a;
          localObject2 = VoipState.ONGOING;
          if (localObject1 == localObject2)
          {
            ag localag = h;
            int i1 = R.string.voip_status_call_muted;
            return ag.a(localag, null, null, null, i1, 0, false, "Peer has muted the microphone.", false, 55);
          }
        }
        return h;
      }
    }
    localObject1 = new com/truecaller/voip/ag;
    int i2 = R.string.voip_status_on_hold;
    int i3 = R.color.voip_call_status_warning_color;
    Object localObject2 = localObject1;
    ((ag)localObject1).<init>(null, null, i2, i3, true, "Call is on hold...", false, 135);
    return (ag)localObject1;
  }
  
  private final boolean q()
  {
    VoipState localVoipState = h.a;
    int[] arrayOfInt = d.i;
    int i1 = localVoipState.ordinal();
    i1 = arrayOfInt[i1];
    switch (i1)
    {
    default: 
      return false;
    }
    return true;
  }
  
  private final String r()
  {
    boolean bool = f;
    if (bool)
    {
      VoipUser localVoipUser = e;
      if (localVoipUser == null)
      {
        String str = "voipUser";
        c.g.b.k.a(str);
      }
      return b;
    }
    return g;
  }
  
  public final kotlinx.coroutines.a.h a()
  {
    return m;
  }
  
  public final bn a(String paramString1, VoipUser paramVoipUser, String paramString2, boolean paramBoolean)
  {
    Object localObject = new com/truecaller/voip/incall/c$m;
    ((c.m)localObject).<init>(this, paramBoolean, paramString1, paramString2, paramVoipUser, null);
    localObject = (m)localObject;
    return e.b(this, null, (m)localObject, 3);
  }
  
  public final void a(b.b paramb)
  {
    c = paramb;
  }
  
  public final void a(String paramString)
  {
    String str = "action";
    c.g.b.k.b(paramString, str);
    int i1 = paramString.hashCode();
    int i2 = 1547796818;
    if (i1 == i2)
    {
      str = "com.truecaller.voip.incoming.ACTION_NOTIFICATION";
      boolean bool = paramString.equals(str);
      if (bool)
      {
        paramString = F;
        str = VoipAnalyticsContext.NOTIFICATION.getValue();
        VoipAnalyticsNotificationAction localVoipAnalyticsNotificationAction = VoipAnalyticsNotificationAction.HANG_UP;
        paramString.a(str, localVoipAnalyticsNotificationAction);
        h();
        paramString = (b.d)b;
        if (paramString != null)
        {
          paramString.k();
          return;
        }
      }
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean) {
      localObject = AudioRoute.SPEAKER;
    } else {
      localObject = AudioRoute.EARPIECE;
    }
    Object localObject = com.truecaller.voip.util.a.a(i.c, (AudioRoute)localObject);
    localObject = com.truecaller.voip.manager.k.a(i, false, false, (com.truecaller.voip.util.a)localObject, 3);
    a((com.truecaller.voip.manager.k)localObject);
    n();
  }
  
  public final kotlinx.coroutines.a.h b()
  {
    return n;
  }
  
  public final void b(boolean paramBoolean)
  {
    com.truecaller.voip.manager.k localk = com.truecaller.voip.manager.k.a(i, paramBoolean, false, null, 6);
    a(localk);
    n();
  }
  
  public final void c(boolean paramBoolean)
  {
    if (paramBoolean) {
      localObject = AudioRoute.BLUETOOTH;
    } else {
      localObject = AudioRoute.EARPIECE;
    }
    Object localObject = com.truecaller.voip.util.a.a(i.c, (AudioRoute)localObject);
    localObject = com.truecaller.voip.manager.k.a(i, false, false, (com.truecaller.voip.util.a)localObject, 3);
    a((com.truecaller.voip.manager.k)localObject);
  }
  
  public final String e()
  {
    return w.h();
  }
  
  public final ag f()
  {
    return p();
  }
  
  public final long g()
  {
    return l;
  }
  
  public final void h()
  {
    Object localObject1 = new com/truecaller/voip/incall/c$l;
    ((c.l)localObject1).<init>(this, null);
    localObject1 = (m)localObject1;
    int i1 = 3;
    e.b(this, null, (m)localObject1, i1);
    localObject1 = h.a;
    Object localObject2 = d.a;
    int i2 = ((VoipState)localObject1).ordinal();
    i2 = localObject2[i2];
    int i3 = 1;
    if (i2 != i3) {
      localObject1 = VoipStateReason.HUNG_UP;
    } else {
      localObject1 = VoipStateReason.INVITE_CANCELLED;
    }
    localObject2 = VoipState.ENDED;
    a((VoipState)localObject2, (VoipStateReason)localObject1);
  }
  
  public final void i()
  {
    d(true);
  }
  
  public final void j()
  {
    d(false);
  }
  
  public final void y_()
  {
    Object localObject = c;
    if (localObject != null) {
      ((b.b)localObject).a();
    }
    s.d();
    localObject = (b.d)b;
    if (localObject != null) {
      ((b.d)localObject).a(false);
    }
    D.f();
    E.b();
    D.d();
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */