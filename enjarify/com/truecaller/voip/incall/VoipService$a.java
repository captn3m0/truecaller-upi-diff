package com.truecaller.voip.incall;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class VoipService$a
{
  public static Intent a(Context paramContext, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramString, "number");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, VoipService.class);
    localIntent.putExtra("com.truecaller.voip.extra.EXTRA_NUMBER", paramString);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.VoipService.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */