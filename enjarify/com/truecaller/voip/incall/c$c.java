package com.truecaller.voip.incall;

import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.manager.rtm.RtmMsg;
import com.truecaller.voip.manager.rtm.RtmMsgAction;
import com.truecaller.voip.manager.rtm.i;
import kotlinx.coroutines.ag;

final class c$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private ag e;
  
  c$c(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/voip/incall/c$c;
    c localc1 = d;
    localc.<init>(localc1, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = c;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label279;
      }
      RtmMsg localRtmMsg = new com/truecaller/voip/manager/rtm/RtmMsg;
      paramObject = RtmMsgAction.INVITE;
      localObject2 = c.u(d);
      localRtmMsg.<init>((RtmMsgAction)paramObject, (String)localObject2);
      paramObject = new com/truecaller/voip/incall/c$c$a;
      ((c.c.a)paramObject).<init>(this);
      Object localObject3 = paramObject;
      localObject3 = (c.g.a.a)paramObject;
      i locali = c.t(d);
      VoipUser localVoipUser = c.n(d);
      boolean bool3 = true;
      a = localRtmMsg;
      b = localObject3;
      j = 1;
      c = j;
      paramObject = locali.a(localVoipUser, localRtmMsg, bool3, (c.g.a.a)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Number)paramObject;
    int j = ((Number)paramObject).intValue();
    switch (j)
    {
    default: 
      paramObject = d;
      localObject1 = VoipState.FAILED;
      localObject2 = VoipStateReason.INVITE_FAILED;
      c.a((c)paramObject, (VoipState)localObject1, (VoipStateReason)localObject2);
      break;
    case 3: 
      paramObject = d;
      localObject1 = VoipState.INVITED;
      c.a((c)paramObject, (VoipState)localObject1);
      break;
    case 2: 
      paramObject = d;
      localObject1 = VoipState.OFFLINE;
      c.a((c)paramObject, (VoipState)localObject1);
    }
    return x.a;
    label279:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */