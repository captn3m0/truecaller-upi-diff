package com.truecaller.voip.incall;

import c.d.a.a;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.util.VoipAnalyticsContext;
import com.truecaller.voip.util.VoipAnalyticsNotificationAction;
import com.truecaller.voip.util.o;
import kotlinx.coroutines.ao;

final class c$m
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  int d;
  private kotlinx.coroutines.ag j;
  
  c$m(c paramc, boolean paramBoolean, String paramString1, String paramString2, VoipUser paramVoipUser, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    m localm = new com/truecaller/voip/incall/c$m;
    c localc = e;
    boolean bool = f;
    String str1 = g;
    String str2 = h;
    VoipUser localVoipUser = i;
    localm.<init>(localc, bool, str1, str2, localVoipUser, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    j = ((kotlinx.coroutines.ag)paramObject);
    return localm;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int k = d;
    int m = 1;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    boolean bool3;
    switch (k)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label726;
      }
      throw a;
    case 2: 
      localObject2 = (ao)c;
      localObject3 = (ao)b;
      localObject4 = (ao)a;
      bool3 = paramObject instanceof o.b;
      if (!bool3) {
        break label667;
      }
      throw a;
    case 1: 
      localObject2 = (ao)c;
      localObject3 = (ao)b;
      localObject4 = (ao)a;
      bool3 = paramObject instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label883;
      }
      bool4 = f;
      if (bool4)
      {
        paramObject = c.a(e);
        if (paramObject != null) {
          ((b.d)paramObject).h();
        }
        paramObject = c.b(e);
        localObject2 = VoipAnalyticsContext.NOTIFICATION.getValue();
        localObject3 = VoipAnalyticsNotificationAction.CALL_BACK;
        ((o)paramObject).a((String)localObject2, (VoipAnalyticsNotificationAction)localObject3);
        paramObject = g;
        if (paramObject != null)
        {
          localObject2 = c.b(e);
          localObject3 = VoipAnalyticsContext.MISSED_CALL_NOTIFICATION.getValue();
          ((o)localObject2).a((String)localObject3, (String)paramObject);
        }
      }
      paramObject = ce).a;
      localObject2 = VoipState.INITIAL;
      if (paramObject != localObject2)
      {
        new String[1][0] = "VoIP service already running.";
        paramObject = g;
        if (paramObject != null)
        {
          localObject1 = c.d(e);
          bool4 = c.g.b.k.a(paramObject, localObject1);
          if (bool4)
          {
            paramObject = c.a(e);
            if (paramObject == null) {
              break label379;
            }
            ((b.d)paramObject).j();
            break label379;
          }
        }
        paramObject = c.a(e);
        if (paramObject != null) {
          ((b.d)paramObject).l();
        }
        label379:
        return x.a;
      }
      paramObject = c.a(e);
      if (paramObject != null) {
        ((b.d)paramObject).i();
      }
      paramObject = e;
      localObject2 = g;
      localObject3 = h;
      localObject4 = i;
      bool4 = c.a((c)paramObject, (String)localObject2, (String)localObject3, (VoipUser)localObject4);
      if (!bool4)
      {
        paramObject = c.a(e);
        if (paramObject != null) {
          ((b.d)paramObject).e();
        }
        return x.a;
      }
      c.e(e);
      c.f(e);
      c.g(e);
      paramObject = e;
      localObject2 = VoipState.CONNECTING;
      c.a((c)paramObject, (VoipState)localObject2);
      paramObject = c.h(e);
      localObject2 = e;
      localObject3 = g;
      localObject4 = i;
      localObject2 = c.a((c)localObject2, (String)localObject3, (VoipUser)localObject4);
      localObject3 = c.i(e);
      a = paramObject;
      b = localObject2;
      c = localObject3;
      d = m;
      localObject4 = ((ao)paramObject).a(this);
      if (localObject4 == localObject1) {
        return localObject1;
      }
      Object localObject5 = localObject4;
      localObject4 = paramObject;
      paramObject = localObject5;
      Object localObject6 = localObject3;
      localObject3 = localObject2;
      localObject2 = localObject6;
    }
    paramObject = (Boolean)paramObject;
    boolean bool4 = ((Boolean)paramObject).booleanValue();
    if (bool4)
    {
      a = localObject4;
      b = localObject3;
      c = localObject2;
      int n = 2;
      d = n;
      paramObject = ((ao)localObject3).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label667:
      paramObject = (Boolean)paramObject;
      boolean bool5 = ((Boolean)paramObject).booleanValue();
      if (bool5)
      {
        a = localObject4;
        b = localObject3;
        c = localObject2;
        int i1 = 3;
        d = i1;
        paramObject = ((ao)localObject2).a(this);
        if (paramObject == localObject1) {
          return localObject1;
        }
        label726:
        paramObject = (Boolean)paramObject;
        boolean bool6 = ((Boolean)paramObject).booleanValue();
        if (bool6)
        {
          c.j(e);
          c.k(e);
          c.l(e);
          c.m(e);
          paramObject = c.a(e);
          if (paramObject != null)
          {
            localObject1 = ne).c;
            ((b.d)paramObject).a((String)localObject1);
          }
          paramObject = c.a(e);
          if (paramObject != null) {
            ((b.d)paramObject).a(m);
          }
          paramObject = e;
          bool6 = c.o((c)paramObject);
          if (bool6 == m)
          {
            paramObject = VoipState.ONGOING;
          }
          else
          {
            if (bool6) {
              break label869;
            }
            paramObject = VoipState.INVITING;
          }
          c.a(e, (VoipState)paramObject);
          return x.a;
          label869:
          paramObject = new c/l;
          ((l)paramObject).<init>();
          throw ((Throwable)paramObject);
        }
      }
    }
    return x.a;
    label883:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (m)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((m)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.c.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */