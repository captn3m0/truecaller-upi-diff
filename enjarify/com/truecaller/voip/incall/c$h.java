package com.truecaller.voip.incall;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.ConnectionState;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.manager.VoipMsg;
import com.truecaller.voip.manager.VoipMsgAction;
import com.truecaller.voip.manager.VoipMsgExtras;

final class c$h
  extends c.d.b.a.k
  implements m
{
  int a;
  private VoipMsg c;
  
  c$h(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    h localh = new com/truecaller/voip/incall/c$h;
    c localc = b;
    localh.<init>(localc, paramc);
    paramObject = (VoipMsg)paramObject;
    c = ((VoipMsg)paramObject);
    return localh;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = c;
        localObject1 = new String[1];
        String str1 = String.valueOf(paramObject);
        String str2 = "New voip message:".concat(str1);
        localObject1[0] = str2;
        localObject1 = ((VoipMsg)paramObject).getAction();
        Object localObject2 = d.e;
        int j = ((VoipMsgAction)localObject1).ordinal();
        j = localObject2[j];
        boolean bool2;
        switch (j)
        {
        default: 
          break;
        case 7: 
          localObject1 = b;
          c.a((c)localObject1, (VoipMsg)paramObject);
          break;
        case 6: 
          paramObject = b;
          localObject1 = VoipState.ENDED;
          localObject2 = VoipStateReason.PEER_LEFT_CHANNEL;
          c.a((c)paramObject, (VoipState)localObject1, (VoipStateReason)localObject2);
          break;
        case 5: 
          localObject1 = b;
          paramObject = ((VoipMsg)paramObject).getExtras();
          bool2 = ((VoipMsgExtras)paramObject).getMuted();
          c.b((c)localObject1, bool2);
          break;
        case 4: 
          paramObject = b;
          localObject1 = ConnectionState.DISCONNECTED;
          c.a((c)paramObject, (ConnectionState)localObject1);
          break;
        case 3: 
          paramObject = b;
          localObject1 = ConnectionState.CONNECTED;
          c.a((c)paramObject, (ConnectionState)localObject1);
          break;
        case 2: 
          paramObject = b;
          localObject1 = ConnectionState.INTERRUPTED;
          c.a((c)paramObject, (ConnectionState)localObject1);
          break;
        case 1: 
          localObject1 = b;
          localObject2 = c.n((c)localObject1);
          paramObject = ((VoipMsg)paramObject).getExtras().getUid();
          paramObject = VoipUser.a((VoipUser)localObject2, (Integer)paramObject);
          c.a((c)localObject1, (VoipUser)paramObject);
          paramObject = b;
          localObject1 = VoipState.ONGOING;
          c.a((c)paramObject, (VoipState)localObject1);
          paramObject = c.y(b);
          bool2 = b;
          if (bool2)
          {
            paramObject = b;
            c.z((c)paramObject);
          }
          break;
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (h)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((h)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.c.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */