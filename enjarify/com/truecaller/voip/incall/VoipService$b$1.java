package com.truecaller.voip.incall;

import android.graphics.Bitmap;
import android.support.v4.app.z.d;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class VoipService$b$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  VoipService$b$1(VoipService.b paramb, Bitmap paramBitmap, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/voip/incall/VoipService$b$1;
    VoipService.b localb = b;
    Bitmap localBitmap = c;
    local1.<init>(localb, localBitmap, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.e;
        localObject = c;
        ((z.d)paramObject).a((Bitmap)localObject);
        VoipService.a(b.c);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.VoipService.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */