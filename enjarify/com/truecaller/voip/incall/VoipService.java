package com.truecaller.voip.incall;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.z.a;
import android.support.v4.app.z.a.a;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.widget.Toast;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.utils.extensions.i;
import com.truecaller.utils.extensions.n;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.incall.ui.VoipActivity;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.w;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.e;

public final class VoipService
  extends Service
  implements b.d, kotlinx.coroutines.ag
{
  public static final VoipService.a e;
  private static boolean h;
  public f a;
  public f b;
  public b.c c;
  public w d;
  private z.d f;
  private PowerManager.WakeLock g;
  
  static
  {
    VoipService.a locala = new com/truecaller/voip/incall/VoipService$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  private final Intent q()
  {
    Intent localIntent = new android/content/Intent;
    Object localObject = this;
    localObject = (Context)this;
    localIntent.<init>((Context)localObject, VoipActivity.class);
    localIntent.setFlags(268435456);
    return localIntent;
  }
  
  private final void r()
  {
    Object localObject = f;
    if (localObject == null) {
      return;
    }
    int i = R.id.voip_service_foreground_notification;
    localObject = ((z.d)localObject).h();
    startForeground(i, (Notification)localObject);
  }
  
  public final f V_()
  {
    f localf = a;
    if (localf == null)
    {
      String str = "uiContext";
      k.a(str);
    }
    return localf;
  }
  
  public final h a()
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localc.a();
  }
  
  public final void a(int paramInt)
  {
    z.d locald = f;
    if (locald == null) {
      return;
    }
    f localf = b;
    if (localf == null)
    {
      localObject = "asyncContext";
      k.a((String)localObject);
    }
    Object localObject = new com/truecaller/voip/incall/VoipService$b;
    ((VoipService.b)localObject).<init>(this, paramInt, locald, null);
    localObject = (m)localObject;
    e.b(this, localf, (m)localObject, 2);
  }
  
  public final void a(Bitmap paramBitmap)
  {
    k.b(paramBitmap, "icon");
    z.d locald = f;
    if (locald != null) {
      locald.a(paramBitmap);
    }
    r();
  }
  
  public final void a(b.b paramb)
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localc.a(paramb);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    z.d locald = f;
    if (locald != null)
    {
      paramString = (CharSequence)paramString;
      locald.a(paramString);
    }
    r();
  }
  
  public final void a(boolean paramBoolean)
  {
    boolean bool;
    if (paramBoolean)
    {
      localWakeLock = g;
      if (localWakeLock != null)
      {
        bool = localWakeLock.isHeld();
        if (!bool)
        {
          TimeUnit localTimeUnit = TimeUnit.HOURS;
          long l1 = 5;
          long l2 = localTimeUnit.toMillis(l1);
          localWakeLock.acquire(l2);
        }
      }
      return;
    }
    PowerManager.WakeLock localWakeLock = g;
    if (localWakeLock != null)
    {
      bool = localWakeLock.isHeld();
      if (bool) {
        localWakeLock.release();
      }
      return;
    }
  }
  
  public final void a(boolean paramBoolean, long paramLong)
  {
    z.d locald1 = f;
    if (locald1 != null) {
      locald1.a(paramBoolean);
    }
    z.d locald2 = f;
    if (locald2 != null) {
      locald2.a(paramLong);
    }
    r();
  }
  
  public final h b()
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localc.b();
  }
  
  public final void b(int paramInt)
  {
    String str = getString(paramInt);
    k.a(str, "getString(id)");
    b(str);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    z.d locald = f;
    if (locald != null)
    {
      paramString = (CharSequence)paramString;
      locald.b(paramString);
    }
    r();
  }
  
  public final void b(boolean paramBoolean)
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localc.a(paramBoolean);
  }
  
  public final void c(boolean paramBoolean)
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localc.b(paramBoolean);
  }
  
  public final h d()
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localc.c();
  }
  
  public final void d(boolean paramBoolean)
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localc.c(paramBoolean);
  }
  
  public final void e()
  {
    stopForeground(true);
    stopSelf();
  }
  
  public final com.truecaller.voip.ag f()
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localc.f();
  }
  
  public final long g()
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localc.g();
  }
  
  public final void h()
  {
    Object localObject = new android/content/Intent;
    ((Intent)localObject).<init>("android.intent.action.CLOSE_SYSTEM_DIALOGS");
    sendBroadcast((Intent)localObject);
    localObject = i.f(this);
    int i = R.id.voip_incoming_service_missed_call_notification;
    ((NotificationManager)localObject).cancel(i);
  }
  
  public final void i()
  {
    Object localObject1 = q();
    z.d locald = f;
    if (locald == null)
    {
      localObject1 = null;
    }
    else
    {
      Object localObject2 = this;
      localObject2 = (Context)this;
      int i = R.id.voip_incoming_notification_action_hang_up;
      Object localObject3 = new android/content/Intent;
      ((Intent)localObject3).<init>((Context)localObject2, VoipService.class);
      ((Intent)localObject3).setAction("com.truecaller.voip.incoming.ACTION_NOTIFICATION");
      Object localObject4 = PendingIntent.getService((Context)localObject2, i, (Intent)localObject3, 134217728);
      localObject3 = new android/support/v4/app/z$a$a;
      int j = R.string.voip_button_notification_hang_up;
      CharSequence localCharSequence = (CharSequence)getString(j);
      ((z.a.a)localObject3).<init>(0, localCharSequence, (PendingIntent)localObject4);
      localObject4 = ((z.a.a)localObject3).a();
      locald = locald.a((z.a)localObject4);
      localObject1 = PendingIntent.getActivity((Context)localObject2, 0, (Intent)localObject1, 0);
      localObject1 = locald.a((PendingIntent)localObject1).h();
    }
    if (localObject1 == null) {
      return;
    }
    int k = R.id.voip_service_foreground_notification;
    startForeground(k, (Notification)localObject1);
  }
  
  public final void j()
  {
    Intent localIntent = q();
    startActivity(localIntent);
  }
  
  public final void k()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.CLOSE_SYSTEM_DIALOGS");
    sendBroadcast(localIntent);
  }
  
  public final void l()
  {
    Object localObject = this;
    localObject = (Context)this;
    int i = R.string.voip_error_already_in_another_call;
    int j = 1;
    Object[] arrayOfObject = new Object[j];
    int k = R.string.voip_text;
    String str = getString(k);
    arrayOfObject[0] = str;
    CharSequence localCharSequence = (CharSequence)getString(i, arrayOfObject);
    Toast.makeText((Context)localObject, localCharSequence, j).show();
  }
  
  public final void m()
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localc.h();
  }
  
  public final void n()
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localc.i();
  }
  
  public final void o()
  {
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localc.j();
  }
  
  public final void onCreate()
  {
    super.onCreate();
    int i = 1;
    h = i;
    Object localObject1 = j.a;
    j.a.a().a(this);
    localObject1 = new android/support/v4/app/z$d;
    Object localObject2 = this;
    localObject2 = (Context)this;
    Object localObject3 = c;
    if (localObject3 == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localObject3 = ((b.c)localObject3).e();
    ((z.d)localObject1).<init>((Context)localObject2, (String)localObject3);
    int j = R.drawable.ic_voip_notification;
    localObject1 = ((z.d)localObject1).a(j).b().d();
    j = R.color.voip_header_color;
    int k = b.c((Context)localObject2, j);
    Object localObject4 = ((z.d)localObject1).f(k).e(i);
    f = ((z.d)localObject4);
    localObject4 = n.a(i.g(this));
    g = ((PowerManager.WakeLock)localObject4);
  }
  
  public final void onDestroy()
  {
    h = false;
    b.c localc = c;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localc.y_();
    super.onDestroy();
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    String str1 = null;
    String str2;
    if (paramIntent != null) {
      str2 = paramIntent.getAction();
    } else {
      str2 = null;
    }
    String str3;
    if (paramIntent != null) {
      str3 = paramIntent.getStringExtra("com.truecaller.voip.extra.EXTRA_NUMBER");
    } else {
      str3 = null;
    }
    VoipUser localVoipUser;
    if (paramIntent != null) {
      localVoipUser = (VoipUser)paramIntent.getParcelableExtra("com.truecaller.voip.extra.EXTRA_USER_ID");
    } else {
      localVoipUser = null;
    }
    if (paramIntent != null) {
      str1 = paramIntent.getStringExtra("com.truecaller.voip.extra.EXTRA_CHANNEL_ID");
    }
    boolean bool = false;
    String str4;
    if (paramIntent != null)
    {
      str4 = "com.truecaller.voip.incoming.EXTRA_FROM_MISSED_CALL";
      bool = paramIntent.getBooleanExtra(str4, false);
    }
    paramIntent = c;
    if (paramIntent == null)
    {
      str4 = "presenter";
      k.a(str4);
    }
    paramIntent.a(this);
    if (str2 == null)
    {
      paramIntent = c;
      if (paramIntent == null)
      {
        str2 = "presenter";
        k.a(str2);
      }
      paramIntent.a(str3, localVoipUser, str1, bool);
    }
    else
    {
      paramIntent = c;
      if (paramIntent == null)
      {
        str1 = "presenter";
        k.a(str1);
      }
      paramIntent.a(str2);
    }
    return 2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.VoipService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */