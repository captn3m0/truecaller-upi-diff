package com.truecaller.voip.incall;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.manager.rtm.RtmMsg;
import com.truecaller.voip.manager.rtm.RtmMsgAction;

final class c$g
  extends c.d.b.a.k
  implements m
{
  int a;
  private RtmMsg c;
  
  c$g(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    g localg = new com/truecaller/voip/incall/c$g;
    c localc = b;
    localg.<init>(localc, paramc);
    paramObject = (RtmMsg)paramObject;
    c = ((RtmMsg)paramObject);
    return localg;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = c;
        bool1 = true;
        Object localObject2 = new String[bool1];
        String str1 = String.valueOf(paramObject);
        String str2 = "New rtm message:".concat(str1);
        str1 = null;
        localObject2[0] = str2;
        localObject2 = ((RtmMsg)paramObject).getSenderId();
        str2 = nb).a;
        boolean bool2 = c.g.b.k.a(localObject2, str2) ^ bool1;
        if (bool2) {
          return x.a;
        }
        paramObject = ((RtmMsg)paramObject).getAction();
        localObject2 = d.d;
        int j = ((RtmMsgAction)paramObject).ordinal();
        j = localObject2[j];
        switch (j)
        {
        default: 
          break;
        case 6: 
          paramObject = b;
          localObject1 = VoipState.ENDED;
          localObject2 = VoipStateReason.RECEIVED_END;
          c.a((c)paramObject, (VoipState)localObject1, (VoipStateReason)localObject2);
          break;
        case 5: 
          paramObject = b;
          c.a((c)paramObject, false);
          break;
        case 4: 
          paramObject = b;
          c.a((c)paramObject, bool1);
          break;
        case 3: 
          paramObject = b;
          localObject1 = VoipState.BUSY;
          c.a((c)paramObject, (VoipState)localObject1);
          break;
        case 2: 
          paramObject = b;
          localObject1 = VoipState.REJECTED;
          c.a((c)paramObject, (VoipState)localObject1);
          break;
        case 1: 
          paramObject = b;
          c.x((c)paramObject);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (g)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((g)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.c.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */