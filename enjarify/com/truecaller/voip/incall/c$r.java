package com.truecaller.voip.incall;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.manager.rtm.RtmMsg;
import com.truecaller.voip.manager.rtm.RtmMsgAction;
import com.truecaller.voip.manager.rtm.i;
import com.truecaller.voip.manager.rtm.i.a;
import kotlinx.coroutines.ag;

final class c$r
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  c$r(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    r localr = new com/truecaller/voip/incall/c$r;
    c localc = c;
    localr.<init>(localc, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localr;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label181;
      }
      paramObject = c.y(c);
      int j = b;
      if (j != 0) {
        paramObject = RtmMsgAction.ON_HOLD;
      } else {
        paramObject = RtmMsgAction.RESUMED;
      }
      i locali = c.t(c);
      VoipUser localVoipUser = c.n(c);
      RtmMsg localRtmMsg = new com/truecaller/voip/manager/rtm/RtmMsg;
      String str = c.u(c);
      localRtmMsg.<init>((RtmMsgAction)paramObject, str);
      a = paramObject;
      j = 1;
      b = j;
      paramObject = i.a.a(locali, localVoipUser, localRtmMsg, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return x.a;
    label181:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (r)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((r)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.c.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */