package com.truecaller.voip.incall.ui;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v4.content.b;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import c.g.b.k;
import com.truecaller.voip.R.color;
import java.util.Iterator;
import java.util.List;

public final class VoipActivity
  extends AppCompatActivity
{
  public final void onBackPressed()
  {
    Object localObject1 = getSupportFragmentManager();
    Object localObject2 = "supportFragmentManager";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((j)localObject1).f().iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Fragment)((Iterator)localObject1).next();
      boolean bool2 = localObject2 instanceof a;
      if (bool2)
      {
        localObject2 = a;
        if (localObject2 == null)
        {
          String str = "presenter";
          k.a(str);
        }
        ((f.a)localObject2).e();
      }
    }
    super.onBackPressed();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Window localWindow = null;
    com.truecaller.utils.extensions.a.a(this, false);
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      localWindow = getWindow();
      k.a(localWindow, "window");
      localObject = this;
      localObject = (Context)this;
      int k = R.color.voip_status_bar_color;
      j = b.c((Context)localObject, k);
      localWindow.setStatusBarColor(j);
    }
    if (paramBundle != null) {
      return;
    }
    paramBundle = getSupportFragmentManager().a();
    Object localObject = new com/truecaller/voip/incall/ui/a;
    ((a)localObject).<init>();
    localObject = (Fragment)localObject;
    paramBundle.b(16908290, (Fragment)localObject).c();
  }
  
  public final void onResume()
  {
    super.onResume();
    setVolumeControlStream(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.VoipActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */