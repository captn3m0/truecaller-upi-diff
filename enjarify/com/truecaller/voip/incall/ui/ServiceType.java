package com.truecaller.voip.incall.ui;

public enum ServiceType
{
  static
  {
    ServiceType[] arrayOfServiceType = new ServiceType[2];
    ServiceType localServiceType = new com/truecaller/voip/incall/ui/ServiceType;
    localServiceType.<init>("OUTGOING", 0);
    OUTGOING = localServiceType;
    arrayOfServiceType[0] = localServiceType;
    localServiceType = new com/truecaller/voip/incall/ui/ServiceType;
    int i = 1;
    localServiceType.<init>("INCOMING", i);
    INCOMING = localServiceType;
    arrayOfServiceType[i] = localServiceType;
    $VALUES = arrayOfServiceType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.ServiceType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */