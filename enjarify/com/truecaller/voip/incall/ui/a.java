package com.truecaller.voip.incall.ui;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.constraint.motion.MotionLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.text.method.MovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Chronometer;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.utils.extensions.t;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.layout;
import com.truecaller.voip.R.string;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import java.util.HashMap;

public final class a
  extends Fragment
  implements f.b
{
  public f.a a;
  private MotionLayout b;
  private FloatingActionButton c;
  private Chronometer d;
  private ImageView e;
  private TextView f;
  private TextView g;
  private TextView h;
  private TextView i;
  private ToggleButton j;
  private ToggleButton k;
  private ToggleButton l;
  private ImageButton m;
  private View n;
  private View o;
  private ImageView p;
  private ImageView q;
  private TextView r;
  private final a.b s;
  private final m t;
  private final m u;
  private final m v;
  private HashMap w;
  
  public a()
  {
    Object localObject = new com/truecaller/voip/incall/ui/a$b;
    ((a.b)localObject).<init>(this);
    s = ((a.b)localObject);
    localObject = new com/truecaller/voip/incall/ui/a$f;
    ((a.f)localObject).<init>(this);
    localObject = (m)localObject;
    t = ((m)localObject);
    localObject = new com/truecaller/voip/incall/ui/a$c;
    ((a.c)localObject).<init>(this);
    localObject = (m)localObject;
    u = ((m)localObject);
    localObject = new com/truecaller/voip/incall/ui/a$a;
    ((a.a)localObject).<init>(this);
    localObject = (m)localObject;
    v = ((m)localObject);
  }
  
  private static void a(ToggleButton paramToggleButton, boolean paramBoolean, m paramm)
  {
    paramToggleButton.setOnCheckedChangeListener(null);
    paramToggleButton.setChecked(paramBoolean);
    if (paramm != null)
    {
      localObject = new com/truecaller/voip/incall/ui/b;
      ((b)localObject).<init>(paramm);
    }
    else
    {
      localObject = paramm;
    }
    Object localObject = (CompoundButton.OnCheckedChangeListener)localObject;
    paramToggleButton.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject);
  }
  
  public final f.a a()
  {
    f.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(int paramInt)
  {
    ImageView localImageView = q;
    if (localImageView == null)
    {
      String str = "truecallerLogoView";
      k.a(str);
    }
    localImageView.setImageResource(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "context ?: return");
    int i1 = android.support.v4.content.b.c((Context)localObject1, paramInt2);
    Object localObject2 = h;
    if (localObject2 == null)
    {
      String str = "statusTextView";
      k.a(str);
    }
    ((TextView)localObject2).setText(paramInt1);
    Object localObject3 = h;
    if (localObject3 == null)
    {
      localObject2 = "statusTextView";
      k.a((String)localObject2);
    }
    ((TextView)localObject3).setTextColor(i1);
    localObject3 = p;
    if (localObject3 == null)
    {
      localObject1 = "callStateRingView";
      k.a((String)localObject1);
    }
    localObject3 = ((ImageView)localObject3).getDrawable();
    if (localObject3 != null)
    {
      localObject3 = (com.truecaller.voip.a.a)localObject3;
      i1 = R.color.voip_call_status_warning_color;
      Object localObject4;
      if (paramInt2 == i1)
      {
        ((com.truecaller.voip.a.a)localObject3).c();
      }
      else
      {
        i1 = R.color.voip_call_status_ok_color;
        if (paramInt2 == i1)
        {
          localObject4 = e;
          i1 = R.color.voip_call_status_ok_color;
          paramInt2 = android.support.v4.content.b.c((Context)localObject4, i1);
          ((com.truecaller.voip.a.a)localObject3).a(paramInt2);
          localObject4 = a;
          paramInt2 = ((Path)localObject4).isEmpty();
          if (paramInt2 != 0)
          {
            paramInt2 = b;
            if (paramInt2 == 0)
            {
              ((com.truecaller.voip.a.a)localObject3).a();
            }
            else
            {
              ((com.truecaller.voip.a.a)localObject3).b();
              localObject3 = c;
              ((AnimatorSet)localObject3).start();
            }
          }
        }
        else
        {
          i1 = R.color.voip_call_status_error_color;
          if (paramInt2 == i1)
          {
            localObject4 = e;
            i1 = R.color.voip_call_status_error_color;
            paramInt2 = android.support.v4.content.b.c((Context)localObject4, i1);
            ((com.truecaller.voip.a.a)localObject3).a(paramInt2);
            localObject4 = a;
            paramInt2 = ((Path)localObject4).isEmpty();
            if (paramInt2 == 0)
            {
              ((com.truecaller.voip.a.a)localObject3).b();
              ((com.truecaller.voip.a.a)localObject3).a();
              localObject3 = d;
              ((AnimatorSet)localObject3).start();
            }
          }
          else
          {
            localObject4 = e;
            i1 = R.color.voip_call_status_neutral_color;
            paramInt2 = android.support.v4.content.b.c((Context)localObject4, i1);
            ((com.truecaller.voip.a.a)localObject3).a(paramInt2);
          }
        }
      }
      localObject3 = p;
      if (localObject3 == null)
      {
        localObject4 = "callStateRingView";
        k.a((String)localObject4);
      }
      t.a((View)localObject3, paramBoolean);
      return;
    }
    localObject3 = new c/u;
    ((u)localObject3).<init>("null cannot be cast to non-null type com.truecaller.voip.view.CallStateAvatarRingDrawable");
    throw ((Throwable)localObject3);
  }
  
  public final void a(String paramString)
  {
    Object localObject = g;
    if (localObject == null)
    {
      String str = "profileNameTextView";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    ((TextView)localObject).setText(paramString);
    paramString = g;
    if (paramString == null)
    {
      localObject = "profileNameTextView";
      k.a((String)localObject);
    }
    paramString.setSelected(true);
  }
  
  public final void a(boolean paramBoolean)
  {
    ToggleButton localToggleButton = k;
    if (localToggleButton == null)
    {
      localObject = "speakerToggleButton";
      k.a((String)localObject);
    }
    Object localObject = t;
    a(localToggleButton, paramBoolean, (m)localObject);
  }
  
  public final void a(boolean paramBoolean, long paramLong)
  {
    Object localObject = d;
    if (localObject == null)
    {
      String str1 = "chronometer";
      k.a(str1);
    }
    localObject = (View)localObject;
    t.a((View)localObject, paramBoolean);
    String str2;
    if (paramBoolean)
    {
      localChronometer = d;
      if (localChronometer == null)
      {
        localObject = "chronometer";
        k.a((String)localObject);
      }
      localChronometer.setBase(paramLong);
      localChronometer = d;
      if (localChronometer == null)
      {
        str2 = "chronometer";
        k.a(str2);
      }
      localChronometer.start();
      return;
    }
    Chronometer localChronometer = d;
    if (localChronometer == null)
    {
      str2 = "chronometer";
      k.a(str2);
    }
    localChronometer.stop();
  }
  
  public final void a(boolean paramBoolean, String paramString)
  {
    k.b(paramString, "deviceName");
    Object localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "bluetoothToggleButton";
      k.a((String)localObject2);
    }
    t.a((View)localObject1);
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "bluetoothToggleButton";
      k.a((String)localObject2);
    }
    boolean bool = true;
    ((ToggleButton)localObject1).setEnabled(bool);
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "bluetoothToggleButton";
      k.a((String)localObject2);
    }
    Object localObject2 = v;
    a((ToggleButton)localObject1, paramBoolean, (m)localObject2);
    TextView localTextView = r;
    if (localTextView == null)
    {
      localObject1 = "bluetoothTextView";
      k.a((String)localObject1);
    }
    t.a((View)localTextView);
    localTextView = r;
    if (localTextView == null)
    {
      localObject1 = "bluetoothTextView";
      k.a((String)localObject1);
    }
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    localTextView = r;
    if (localTextView == null)
    {
      paramString = "bluetoothTextView";
      k.a(paramString);
    }
    localTextView.setTextColor(-1);
  }
  
  public final void b()
  {
    Object localObject = l;
    String str;
    if (localObject == null)
    {
      str = "bluetoothToggleButton";
      k.a(str);
    }
    t.b((View)localObject);
    localObject = r;
    if (localObject == null)
    {
      str = "bluetoothTextView";
      k.a(str);
    }
    t.b((View)localObject);
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = "activity ?: return";
    k.a(localObject1, (String)localObject2);
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 21;
    if (i1 >= i2)
    {
      localObject2 = ((f)localObject1).getWindow();
      k.a(localObject2, "context.window");
      localObject3 = localObject1;
      localObject3 = (Context)localObject1;
      i3 = R.color.voip_status_bar_spam_color;
      i2 = android.support.v4.content.b.c((Context)localObject3, i3);
      ((Window)localObject2).setStatusBarColor(i2);
    }
    localObject2 = n;
    if (localObject2 == null)
    {
      localObject3 = "headerCoverView";
      k.a((String)localObject3);
    }
    localObject1 = (Context)localObject1;
    i2 = R.color.voip_spam_color;
    int i4 = android.support.v4.content.b.c((Context)localObject1, i2);
    ((View)localObject2).setBackgroundColor(i4);
    localObject1 = o;
    if (localObject1 == null)
    {
      localObject2 = "headerArcView";
      k.a((String)localObject2);
    }
    i1 = R.drawable.background_voip_spam_header_view;
    ((View)localObject1).setBackgroundResource(i1);
    localObject1 = m;
    if (localObject1 == null)
    {
      localObject2 = "minimiseButton";
      k.a((String)localObject2);
    }
    i1 = R.drawable.background_voip_minimise_spam_call;
    ((ImageButton)localObject1).setBackgroundResource(i1);
    localObject1 = e;
    if (localObject1 == null)
    {
      localObject2 = "profilePictureImageView";
      k.a((String)localObject2);
    }
    i1 = 0;
    localObject2 = null;
    ((ImageView)localObject1).setImageDrawable(null);
    localObject1 = e;
    if (localObject1 == null)
    {
      localObject2 = "profilePictureImageView";
      k.a((String)localObject2);
    }
    i1 = R.drawable.ic_avatar_voip_spam;
    ((ImageView)localObject1).setImageResource(i1);
    localObject1 = f;
    if (localObject1 == null)
    {
      localObject2 = "spamScoreTextView";
      k.a((String)localObject2);
    }
    i1 = R.string.voip_spam_reports_score;
    i2 = 1;
    Object localObject3 = new Object[i2];
    int i3 = 0;
    Object localObject4 = Integer.valueOf(paramInt);
    localObject3[0] = localObject4;
    localObject4 = (CharSequence)getString(i1, (Object[])localObject3);
    ((TextView)localObject1).setText((CharSequence)localObject4);
    localObject4 = f;
    if (localObject4 == null)
    {
      localObject1 = "spamScoreTextView";
      k.a((String)localObject1);
    }
    t.a((View)localObject4);
  }
  
  public final void b(String paramString)
  {
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    boolean bool = true;
    if (localObject != null)
    {
      i1 = ((CharSequence)localObject).length();
      if (i1 != 0)
      {
        i1 = 0;
        localObject = null;
        break label40;
      }
    }
    int i1 = 1;
    label40:
    if (i1 != 0) {
      return;
    }
    localObject = requireContext();
    String str1 = "requireContext()";
    k.a(localObject, str1);
    paramString = w.a((Context)localObject).a(paramString);
    i1 = R.drawable.ic_avatar_voip_default;
    paramString = paramString.a(i1);
    localObject = (ai)aq.d.b();
    paramString = paramString.a((ai)localObject);
    c = bool;
    paramString = paramString.b();
    localObject = e;
    if (localObject == null)
    {
      String str2 = "profilePictureImageView";
      k.a(str2);
    }
    paramString.a((ImageView)localObject, null);
  }
  
  public final void b(boolean paramBoolean)
  {
    ToggleButton localToggleButton = j;
    if (localToggleButton == null)
    {
      localObject = "muteToggleButton";
      k.a((String)localObject);
    }
    Object localObject = u;
    a(localToggleButton, paramBoolean, (m)localObject);
  }
  
  public final void c()
  {
    Object localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "bluetoothToggleButton";
      k.a((String)localObject2);
    }
    t.a((View)localObject1);
    localObject1 = r;
    if (localObject1 == null)
    {
      localObject2 = "bluetoothTextView";
      k.a((String)localObject2);
    }
    t.a((View)localObject1);
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "bluetoothToggleButton";
      k.a((String)localObject2);
    }
    int i1 = 0;
    Object localObject2 = null;
    ((ToggleButton)localObject1).setEnabled(false);
    localObject1 = r;
    if (localObject1 == null)
    {
      localObject2 = "bluetoothTextView";
      k.a((String)localObject2);
    }
    i1 = R.string.voip_button_bluetooth;
    ((TextView)localObject1).setText(i1);
    localObject1 = r;
    if (localObject1 == null)
    {
      localObject2 = "bluetoothTextView";
      k.a((String)localObject2);
    }
    localObject2 = requireContext();
    int i2 = R.color.voip_action_text_color_disabled;
    i1 = android.support.v4.content.b.c((Context)localObject2, i2);
    ((TextView)localObject1).setTextColor(i1);
  }
  
  public final void c(String paramString)
  {
    String str = "message";
    k.b(paramString, str);
    paramString = i;
    if (paramString == null)
    {
      str = "logTextView";
      k.a(str);
    }
    t.b((View)paramString);
  }
  
  public final void d()
  {
    Object localObject = c;
    if (localObject == null)
    {
      str1 = "endCallButton";
      k.a(str1);
    }
    String str1 = null;
    ((FloatingActionButton)localObject).setEnabled(false);
    localObject = j;
    String str2;
    if (localObject == null)
    {
      str2 = "muteToggleButton";
      k.a(str2);
    }
    ((ToggleButton)localObject).setEnabled(false);
    localObject = k;
    if (localObject == null)
    {
      str2 = "speakerToggleButton";
      k.a(str2);
    }
    ((ToggleButton)localObject).setEnabled(false);
    localObject = l;
    if (localObject == null)
    {
      str2 = "bluetoothToggleButton";
      k.a(str2);
    }
    ((ToggleButton)localObject).setEnabled(false);
  }
  
  public final void e()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void f()
  {
    MotionLayout localMotionLayout = b;
    String str;
    if (localMotionLayout == null)
    {
      str = "motionLayoutView";
      k.a(str);
    }
    int i1 = R.id.outgoing_call_ended_start_set;
    int i2 = R.id.outgoing_call_ended_end_set;
    localMotionLayout.a(i1, i2);
    localMotionLayout = b;
    if (localMotionLayout == null)
    {
      str = "motionLayoutView";
      k.a(str);
    }
    localMotionLayout.c();
  }
  
  public final void g()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = j.a;
    j.a.a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i1 = R.layout.fragment_voip;
    paramLayoutInflater = paramLayoutInflater.inflate(i1, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…t_voip, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((f.a)localObject).y_();
    localObject = w;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onStart()
  {
    super.onStart();
    Context localContext = requireContext();
    k.a(localContext, "requireContext()");
    Object localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>(localContext, VoipService.class);
    Object localObject2 = (ServiceConnection)s;
    boolean bool = localContext.bindService((Intent)localObject1, (ServiceConnection)localObject2, 0);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((f.a)localObject1).a(bool);
  }
  
  public final void onStop()
  {
    super.onStop();
    Context localContext = requireContext();
    ServiceConnection localServiceConnection = (ServiceConnection)s;
    localContext.unbindService(localServiceConnection);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i1 = R.id.motion_layout;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.motion_layout)");
    paramBundle = (MotionLayout)paramBundle;
    b = paramBundle;
    i1 = R.id.button_end_call;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_end_call)");
    paramBundle = (FloatingActionButton)paramBundle;
    c = paramBundle;
    i1 = R.id.chronometer;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.chronometer)");
    paramBundle = (Chronometer)paramBundle;
    d = paramBundle;
    i1 = R.id.text_profile_name;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_profile_name)");
    paramBundle = (TextView)paramBundle;
    g = paramBundle;
    i1 = R.id.text_status;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_status)");
    paramBundle = (TextView)paramBundle;
    h = paramBundle;
    i1 = R.id.image_profile_picture;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.image_profile_picture)");
    paramBundle = (ImageView)paramBundle;
    e = paramBundle;
    i1 = R.id.text_spam_score;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_spam_score)");
    paramBundle = (TextView)paramBundle;
    f = paramBundle;
    i1 = R.id.text_log;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_log)");
    paramBundle = (TextView)paramBundle;
    i = paramBundle;
    i1 = R.id.toggle_mute;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.toggle_mute)");
    paramBundle = (ToggleButton)paramBundle;
    j = paramBundle;
    i1 = R.id.toggle_speaker;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.toggle_speaker)");
    paramBundle = (ToggleButton)paramBundle;
    k = paramBundle;
    i1 = R.id.toggle_bluetooth;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.toggle_bluetooth)");
    paramBundle = (ToggleButton)paramBundle;
    l = paramBundle;
    i1 = R.id.button_minimise;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_minimise)");
    paramBundle = (ImageButton)paramBundle;
    m = paramBundle;
    i1 = R.id.view_header_cover;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.view_header_cover)");
    n = paramBundle;
    i1 = R.id.view_header;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.view_header)");
    o = paramBundle;
    i1 = R.id.image_call_state_ring;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.image_call_state_ring)");
    paramBundle = (ImageView)paramBundle;
    p = paramBundle;
    i1 = R.id.image_logo;
    paramBundle = paramView.findViewById(i1);
    Object localObject = "view.findViewById(R.id.image_logo)";
    k.a(paramBundle, (String)localObject);
    paramBundle = (ImageView)paramBundle;
    q = paramBundle;
    paramBundle = p;
    if (paramBundle == null)
    {
      localObject = "callStateRingView";
      k.a((String)localObject);
    }
    localObject = new com/truecaller/voip/a/a;
    Context localContext = paramView.getContext();
    String str = "view.context";
    k.a(localContext, str);
    ((com.truecaller.voip.a.a)localObject).<init>(localContext);
    localObject = (Drawable)localObject;
    paramBundle.setImageDrawable((Drawable)localObject);
    i1 = R.id.text_bluetooth;
    paramView = paramView.findViewById(i1);
    paramBundle = "view.findViewById(R.id.text_bluetooth)";
    k.a(paramView, paramBundle);
    paramView = (TextView)paramView;
    r = paramView;
    paramView = i;
    if (paramView == null)
    {
      paramBundle = "logTextView";
      k.a(paramBundle);
    }
    paramBundle = new android/text/method/ScrollingMovementMethod;
    paramBundle.<init>();
    paramBundle = (MovementMethod)paramBundle;
    paramView.setMovementMethod(paramBundle);
    paramView = c;
    if (paramView == null)
    {
      paramBundle = "endCallButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/voip/incall/ui/a$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = k;
    if (paramView == null)
    {
      paramBundle = "speakerToggleButton";
      k.a(paramBundle);
    }
    paramBundle = t;
    if (paramBundle != null)
    {
      localObject = new com/truecaller/voip/incall/ui/b;
      ((b)localObject).<init>(paramBundle);
      paramBundle = (Bundle)localObject;
    }
    paramBundle = (CompoundButton.OnCheckedChangeListener)paramBundle;
    paramView.setOnCheckedChangeListener(paramBundle);
    paramView = j;
    if (paramView == null)
    {
      paramBundle = "muteToggleButton";
      k.a(paramBundle);
    }
    paramBundle = u;
    if (paramBundle != null)
    {
      localObject = new com/truecaller/voip/incall/ui/b;
      ((b)localObject).<init>(paramBundle);
      paramBundle = (Bundle)localObject;
    }
    paramBundle = (CompoundButton.OnCheckedChangeListener)paramBundle;
    paramView.setOnCheckedChangeListener(paramBundle);
    paramView = l;
    if (paramView == null)
    {
      paramBundle = "bluetoothToggleButton";
      k.a(paramBundle);
    }
    paramBundle = v;
    if (paramBundle != null)
    {
      localObject = new com/truecaller/voip/incall/ui/b;
      ((b)localObject).<init>(paramBundle);
      paramBundle = (Bundle)localObject;
    }
    paramBundle = (CompoundButton.OnCheckedChangeListener)paramBundle;
    paramView.setOnCheckedChangeListener(paramBundle);
    paramView = m;
    if (paramView == null)
    {
      paramBundle = "minimiseButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/voip/incall/ui/a$e;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */