package com.truecaller.voip.incall.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build.VERSION;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Chronometer;
import android.widget.TextView;
import c.g.a.a;
import c.g.b.k;
import c.g.b.w;
import com.truecaller.voip.R.attr;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.layout;
import com.truecaller.voip.R.string;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incoming.IncomingVoipService;
import com.truecaller.voip.incoming.ui.IncomingVoipActivity;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;

public final class VoipInAppNotificationView
  extends ConstraintLayout
  implements c.b
{
  public c.a l;
  private final c.f m;
  private final c.f n;
  private ServiceType o;
  private final VoipInAppNotificationView.c p;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[2];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(VoipInAppNotificationView.class);
    ((c.g.b.u)localObject).<init>(localb, "nameTextView", "getNameTextView()Landroid/widget/TextView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(VoipInAppNotificationView.class);
    ((c.g.b.u)localObject).<init>(localb, "chronometer", "getChronometer()Landroid/widget/Chronometer;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    k = arrayOfg;
  }
  
  public VoipInAppNotificationView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private VoipInAppNotificationView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    paramAttributeSet = new com/truecaller/voip/incall/ui/VoipInAppNotificationView$b;
    paramAttributeSet.<init>(this);
    paramAttributeSet = c.g.a((a)paramAttributeSet);
    m = paramAttributeSet;
    paramAttributeSet = new com/truecaller/voip/incall/ui/VoipInAppNotificationView$a;
    paramAttributeSet.<init>(this);
    paramAttributeSet = c.g.a((a)paramAttributeSet);
    n = paramAttributeSet;
    paramAttributeSet = j.a;
    j.a.a().a(this);
    int i = R.layout.view_voip_in_app_notification;
    Object localObject = this;
    localObject = (ViewGroup)this;
    View.inflate(paramContext, i, (ViewGroup)localObject);
    paramContext = paramContext.getResources();
    i = R.color.voip_in_app_notification_background_color;
    int j = android.support.v4.content.b.f.b(paramContext, i, null);
    setBackgroundColor(j);
    com.truecaller.utils.extensions.t.b(this);
    paramContext = new com/truecaller/voip/incall/ui/VoipInAppNotificationView$1;
    paramContext.<init>(this);
    paramContext = (View.OnClickListener)paramContext;
    setOnClickListener(paramContext);
    paramContext = new com/truecaller/voip/incall/ui/VoipInAppNotificationView$c;
    paramContext.<init>(this);
    p = paramContext;
  }
  
  private final void g()
  {
    Context localContext = getContext();
    Intent localIntent = new android/content/Intent;
    Object localObject = getContext();
    localIntent.<init>((Context)localObject, VoipService.class);
    localObject = (ServiceConnection)p;
    localContext.bindService(localIntent, (ServiceConnection)localObject, 0);
    localContext = getContext();
    localIntent = new android/content/Intent;
    localObject = getContext();
    localIntent.<init>((Context)localObject, IncomingVoipService.class);
    localObject = (ServiceConnection)p;
    localContext.bindService(localIntent, (ServiceConnection)localObject, 0);
  }
  
  private final Chronometer getChronometer()
  {
    return (Chronometer)n.b();
  }
  
  private final TextView getNameTextView()
  {
    return (TextView)m.b();
  }
  
  public final void a(String paramString, long paramLong)
  {
    k.b(paramString, "name");
    com.truecaller.utils.extensions.t.a(this);
    f();
    TextView localTextView = getNameTextView();
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    com.truecaller.utils.extensions.t.a((View)getChronometer());
    getChronometer().setBase(paramLong);
    getChronometer().start();
  }
  
  public final void ai_()
  {
    com.truecaller.utils.extensions.t.a(this);
    f();
    com.truecaller.utils.extensions.t.b((View)getChronometer());
    TextView localTextView = getNameTextView();
    Object localObject = getContext();
    int i = R.string.voip_in_app_notification_outgoing_call;
    localObject = (CharSequence)((Context)localObject).getString(i);
    localTextView.setText((CharSequence)localObject);
  }
  
  public final void b()
  {
    com.truecaller.utils.extensions.t.a(this);
    f();
    com.truecaller.utils.extensions.t.b((View)getChronometer());
    TextView localTextView = getNameTextView();
    Object localObject = getContext();
    int i = R.string.voip_in_app_notification_incoming_call;
    localObject = (CharSequence)((Context)localObject).getString(i);
    localTextView.setText((CharSequence)localObject);
  }
  
  public final void c()
  {
    com.truecaller.utils.extensions.t.b(this);
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      Object localObject = getContext();
      boolean bool = localObject instanceof Activity;
      if (bool)
      {
        localObject = getContext();
        if (localObject != null)
        {
          localObject = ((Activity)localObject).getWindow();
          k.a(localObject, "(context as Activity).window");
          Context localContext = getContext();
          int i1 = R.attr.theme_statusBarColor;
          j = com.truecaller.utils.ui.b.a(localContext, i1);
          ((Window)localObject).setStatusBarColor(j);
        }
        else
        {
          localObject = new c/u;
          ((c.u)localObject).<init>("null cannot be cast to non-null type android.app.Activity");
          throw ((Throwable)localObject);
        }
      }
    }
    getChronometer().stop();
  }
  
  public final void d()
  {
    Context localContext1 = getContext();
    Intent localIntent = new android/content/Intent;
    Context localContext2 = getContext();
    localIntent.<init>(localContext2, VoipActivity.class);
    localContext1.startActivity(localIntent);
  }
  
  public final void e()
  {
    Context localContext1 = getContext();
    Intent localIntent = new android/content/Intent;
    Context localContext2 = getContext();
    localIntent.<init>(localContext2, IncomingVoipActivity.class);
    localContext1.startActivity(localIntent);
  }
  
  public final void f()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i < j) {
      return;
    }
    Object localObject = getContext();
    boolean bool = localObject instanceof Activity;
    if (bool)
    {
      localObject = getContext();
      if (localObject != null)
      {
        localObject = ((Activity)localObject).getWindow();
        k.a(localObject, "(context as Activity).window");
        Context localContext = getContext();
        int i1 = R.color.voip_in_app_notification_status_bar_color;
        j = android.support.v4.content.b.c(localContext, i1);
        ((Window)localObject).setStatusBarColor(j);
      }
      else
      {
        localObject = new c/u;
        ((c.u)localObject).<init>("null cannot be cast to non-null type android.app.Activity");
        throw ((Throwable)localObject);
      }
    }
  }
  
  public final c.a getPresenter()
  {
    c.a locala = l;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final ServiceType getServiceType()
  {
    return o;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    g();
    c.a locala = l;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a(this);
  }
  
  protected final void onDetachedFromWindow()
  {
    Object localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((c.a)localObject1).y_();
    localObject1 = getContext();
    Object localObject2 = (ServiceConnection)p;
    ((Context)localObject1).unbindService((ServiceConnection)localObject2);
    super.onDetachedFromWindow();
  }
  
  public final void setPresenter(c.a parama)
  {
    k.b(parama, "<set-?>");
    l = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.VoipInAppNotificationView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */