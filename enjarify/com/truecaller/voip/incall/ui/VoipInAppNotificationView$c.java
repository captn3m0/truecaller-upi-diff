package com.truecaller.voip.incall.ui;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import c.g.b.k;

public final class VoipInAppNotificationView$c
  implements ServiceConnection
{
  private ComponentName b;
  
  VoipInAppNotificationView$c(VoipInAppNotificationView paramVoipInAppNotificationView) {}
  
  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    int i = 1;
    Object localObject1 = new String[i];
    String str1 = String.valueOf(paramComponentName);
    String str2 = "Voip service connected to ".concat(str1);
    str1 = null;
    localObject1[0] = str2;
    if (paramIBinder == null) {
      return;
    }
    localObject1 = b;
    Object localObject2;
    if (localObject1 != null)
    {
      paramComponentName = new String[i];
      paramIBinder = new java/lang/StringBuilder;
      paramIBinder.<init>("Already connected to the service: ");
      localObject2 = b;
      paramIBinder.append(localObject2);
      paramIBinder = paramIBinder.toString();
      paramComponentName[0] = paramIBinder;
      return;
    }
    b = paramComponentName;
    boolean bool = paramIBinder instanceof com.truecaller.voip.incall.a;
    if (bool)
    {
      paramComponentName = a;
      localObject2 = ServiceType.OUTGOING;
      VoipInAppNotificationView.a(paramComponentName, (ServiceType)localObject2);
      paramComponentName = a.getPresenter();
      paramIBinder = a;
      paramComponentName.a(paramIBinder);
      return;
    }
    bool = paramIBinder instanceof com.truecaller.voip.incoming.a;
    if (bool)
    {
      paramComponentName = a;
      localObject2 = ServiceType.INCOMING;
      VoipInAppNotificationView.a(paramComponentName, (ServiceType)localObject2);
      paramComponentName = a.getPresenter();
      paramIBinder = a;
      paramComponentName.a(paramIBinder);
    }
  }
  
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    int i = 1;
    Object localObject = new String[i];
    String str1 = String.valueOf(paramComponentName);
    String str2 = "Voip service is disconnected. Component name: ".concat(str1);
    str1 = null;
    localObject[0] = str2;
    localObject = b;
    boolean bool = k.a(localObject, paramComponentName) ^ i;
    if (bool)
    {
      new String[1][0] = "Ignoring disconnected event from ignored connection.";
      return;
    }
    a.getPresenter().a();
    b = null;
    VoipInAppNotificationView.a(a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.VoipInAppNotificationView.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */