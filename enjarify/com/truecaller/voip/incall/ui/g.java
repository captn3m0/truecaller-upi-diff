package com.truecaller.voip.incall.ui;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.utils.a;
import com.truecaller.utils.extensions.j;
import com.truecaller.voip.ag;
import com.truecaller.voip.incall.b.a;
import com.truecaller.voip.incall.b.b;
import com.truecaller.voip.util.VoipAnalyticsInCallUiAction;
import com.truecaller.voip.util.at;
import com.truecaller.voip.util.o;
import kotlinx.coroutines.a.h;

public final class g
  extends ba
  implements f.a
{
  private b.a c;
  private final g.a d;
  private final f e;
  private final a f;
  private final at g;
  private final o h;
  
  public g(f paramf, a parama, at paramat, o paramo)
  {
    super(paramf);
    e = paramf;
    f = parama;
    g = paramat;
    h = paramo;
    paramf = new com/truecaller/voip/incall/ui/g$a;
    paramf.<init>(this);
    d = paramf;
  }
  
  private final long a(long paramLong)
  {
    long l1 = f.b();
    long l2 = f.a() - paramLong;
    return l1 - l2;
  }
  
  public final void a()
  {
    b.a locala = c;
    if (locala != null) {
      locala.a(null);
    }
    c = null;
  }
  
  public final void a(b.a parama)
  {
    k.b(parama, "binderView");
    Object localObject1 = (b.b)d;
    parama.a((b.b)localObject1);
    localObject1 = parama.a();
    Object localObject2 = new com/truecaller/voip/incall/ui/g$c;
    int i = 0;
    ((g.c)localObject2).<init>(this, null);
    localObject2 = (m)localObject2;
    j.a(this, (h)localObject1, (m)localObject2);
    localObject1 = parama.d();
    localObject2 = new com/truecaller/voip/incall/ui/g$b;
    ((g.b)localObject2).<init>(this, null);
    localObject2 = (m)localObject2;
    j.a(this, (h)localObject1, (m)localObject2);
    localObject1 = parama.f();
    localObject2 = (f.b)b;
    if (localObject2 != null)
    {
      i = ((ag)localObject1).a();
      int j = ((ag)localObject1).c();
      boolean bool2 = ((ag)localObject1).d();
      ((f.b)localObject2).a(i, j, bool2);
    }
    localObject2 = (f.b)b;
    if (localObject2 != null)
    {
      boolean bool1 = ((ag)localObject1).b();
      long l = parama.g();
      l = a(l);
      ((f.b)localObject2).a(bool1, l);
    }
    localObject2 = (f.b)b;
    if (localObject2 != null)
    {
      localObject1 = e;
      ((f.b)localObject2).c((String)localObject1);
    }
    c = parama;
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean) {
      return;
    }
    f.b localb = (f.b)b;
    if (localb != null)
    {
      localb.g();
      return;
    }
  }
  
  public final void b()
  {
    Object localObject = c;
    if (localObject != null) {
      ((b.a)localObject).m();
    }
    localObject = (f.b)b;
    if (localObject != null) {
      ((f.b)localObject).d();
    }
    localObject = h;
    VoipAnalyticsInCallUiAction localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.REJECT;
    ((o)localObject).a(localVoipAnalyticsInCallUiAction);
  }
  
  public final void b(boolean paramBoolean)
  {
    b.a locala = c;
    if (locala != null) {
      locala.b(paramBoolean);
    }
    VoipAnalyticsInCallUiAction localVoipAnalyticsInCallUiAction;
    if (paramBoolean) {
      localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.SPEAKER_ON;
    } else {
      localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.SPEAKER_OFF;
    }
    h.a(localVoipAnalyticsInCallUiAction);
  }
  
  public final void c()
  {
    Object localObject = (f.b)b;
    if (localObject != null) {
      ((f.b)localObject).e();
    }
    localObject = h;
    VoipAnalyticsInCallUiAction localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.DISMISS;
    ((o)localObject).a(localVoipAnalyticsInCallUiAction);
  }
  
  public final void c(boolean paramBoolean)
  {
    b.a locala = c;
    if (locala != null) {
      locala.c(paramBoolean);
    }
    VoipAnalyticsInCallUiAction localVoipAnalyticsInCallUiAction;
    if (paramBoolean) {
      localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.MIC_OFF;
    } else {
      localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.MIC_ON;
    }
    h.a(localVoipAnalyticsInCallUiAction);
  }
  
  public final void d(boolean paramBoolean)
  {
    b.a locala = c;
    if (locala != null)
    {
      locala.d(paramBoolean);
      return;
    }
  }
  
  public final void e()
  {
    o localo = h;
    VoipAnalyticsInCallUiAction localVoipAnalyticsInCallUiAction = VoipAnalyticsInCallUiAction.BACK;
    localo.a(localVoipAnalyticsInCallUiAction);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */