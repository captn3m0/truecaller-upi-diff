package com.truecaller.voip.incall.ui;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import c.g.b.k;

public final class a$b
  implements ServiceConnection
{
  a$b(a parama) {}
  
  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    k.b(paramComponentName, "className");
    k.b(paramIBinder, "binder");
    paramIBinder = (com.truecaller.voip.incall.a)paramIBinder;
    paramComponentName = a.a();
    paramIBinder = a;
    paramComponentName.a(paramIBinder);
  }
  
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    k.b(paramComponentName, "className");
    a.a().a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */