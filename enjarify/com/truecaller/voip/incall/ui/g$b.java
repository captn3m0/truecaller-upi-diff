package com.truecaller.voip.incall.ui;

import c.d.c;
import c.o.b;
import c.x;
import com.truecaller.voip.util.AudioRoute;
import com.truecaller.voip.util.a;
import com.truecaller.voip.util.d.a;
import com.truecaller.voip.util.d.b;
import com.truecaller.voip.util.d.c;

final class g$b
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private com.truecaller.voip.manager.k c;
  
  g$b(g paramg, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/voip/incall/ui/g$b;
    g localg = b;
    localb.<init>(localg, paramc);
    paramObject = (com.truecaller.voip.manager.k)paramObject;
    c = ((com.truecaller.voip.manager.k)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = c;
        bool1 = true;
        Object localObject1 = new String[bool1];
        String str = String.valueOf(paramObject);
        Object localObject2 = "New voip setting is received. Setting: ".concat(str);
        str = null;
        localObject1[0] = localObject2;
        localObject1 = c;
        localObject2 = g.a(b);
        if (localObject2 != null)
        {
          boolean bool2 = a;
          ((f.b)localObject2).b(bool2);
        }
        paramObject = g.a(b);
        if (paramObject != null)
        {
          localObject2 = a;
          AudioRoute localAudioRoute = AudioRoute.SPEAKER;
          if (localObject2 == localAudioRoute)
          {
            bool3 = true;
          }
          else
          {
            bool3 = false;
            localObject2 = null;
          }
          ((f.b)paramObject).a(bool3);
        }
        paramObject = b.a;
        boolean bool3 = paramObject instanceof d.c;
        if (bool3)
        {
          paramObject = g.a(b);
          if (paramObject != null) {
            ((f.b)paramObject).b();
          }
        }
        else
        {
          bool3 = paramObject instanceof d.b;
          if (bool3)
          {
            paramObject = g.a(b);
            if (paramObject != null) {
              ((f.b)paramObject).c();
            }
          }
          else
          {
            bool3 = paramObject instanceof d.a;
            if (bool3)
            {
              localObject1 = a;
              localObject2 = AudioRoute.BLUETOOTH;
              if (localObject1 != localObject2) {
                bool1 = false;
              }
              localObject1 = g.a(b);
              if (localObject1 != null)
              {
                paramObject = a;
                ((f.b)localObject1).a(bool1, (String)paramObject);
              }
            }
          }
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.g.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */