package com.truecaller.voip.incall.ui;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.ag;

final class d$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  d$b(d paramd, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/voip/incall/ui/d$b;
    d locald = b;
    localb.<init>(locald, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = c.a;
        localObject = e.b;
        int j = ((VoipState)paramObject).ordinal();
        j = localObject[j];
        switch (j)
        {
        default: 
          paramObject = d.b(b);
          if (paramObject != null) {
            ((c.b)paramObject).c();
          }
          break;
        case 1: 
        case 2: 
          paramObject = b;
          d.c((d)paramObject);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */