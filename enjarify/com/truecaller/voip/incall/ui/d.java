package com.truecaller.voip.incall.ui;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.l;
import com.truecaller.ba;
import com.truecaller.utils.a;
import com.truecaller.utils.extensions.j;
import kotlinx.coroutines.a.h;

public final class d
  extends ba
  implements c.a
{
  private final f c;
  private final a d;
  
  public d(f paramf, a parama)
  {
    super(paramf);
    c = paramf;
    d = parama;
  }
  
  public final void a()
  {
    c.b localb = (c.b)b;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void a(com.truecaller.voip.incall.b.a parama)
  {
    k.b(parama, "binderView");
    h localh = parama.b();
    Object localObject = new com/truecaller/voip/incall/ui/d$a;
    ((d.a)localObject).<init>(this, parama, null);
    localObject = (m)localObject;
    j.a(this, localh, (m)localObject);
  }
  
  public final void a(com.truecaller.voip.incoming.b.a parama)
  {
    k.b(parama, "binderPresenter");
    parama = parama.b();
    Object localObject = new com/truecaller/voip/incall/ui/d$b;
    ((d.b)localObject).<init>(this, null);
    localObject = (m)localObject;
    j.a(this, parama, (m)localObject);
  }
  
  public final void b()
  {
    Object localObject = (c.b)b;
    if (localObject != null)
    {
      localObject = ((c.b)localObject).getServiceType();
      if (localObject != null)
      {
        int[] arrayOfInt = e.c;
        int i = ((ServiceType)localObject).ordinal();
        i = arrayOfInt[i];
        switch (i)
        {
        default: 
          localObject = new c/l;
          ((l)localObject).<init>();
          throw ((Throwable)localObject);
        case 2: 
          localObject = (c.b)b;
          if (localObject != null) {
            ((c.b)localObject).e();
          }
          return;
        }
        localObject = (c.b)b;
        if (localObject != null) {
          ((c.b)localObject).d();
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */