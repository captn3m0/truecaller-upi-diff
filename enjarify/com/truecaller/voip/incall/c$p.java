package com.truecaller.voip.incall;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import kotlinx.coroutines.ar;

final class c$p
  extends c.d.b.a.k
  implements m
{
  int a;
  private kotlinx.coroutines.ag c;
  
  c$p(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    p localp = new com/truecaller/voip/incall/c$p;
    c localc = b;
    localp.<init>(localc, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    c = ((kotlinx.coroutines.ag)paramObject);
    return localp;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label204;
      }
      long l = 60000L;
      j = 1;
      a = j;
      paramObject = ar.a(l, this);
      if (paramObject == localObject) {
        return localObject;
      }
      break;
    }
    paramObject = cb).a;
    localObject = d.h;
    int j = ((VoipState)paramObject).ordinal();
    j = localObject[j];
    switch (j)
    {
    default: 
      j = 0;
      paramObject = null;
      break;
    case 3: 
      paramObject = VoipStateReason.INVITE_TIMEOUT;
      break;
    case 1: 
    case 2: 
      paramObject = VoipStateReason.WAKE_UP_TIMEOUT;
    }
    if (paramObject != null)
    {
      localObject = b;
      VoipState localVoipState = VoipState.FAILED;
      c.a((c)localObject, localVoipState, (VoipStateReason)paramObject);
    }
    return x.a;
    label204:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (p)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((p)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.c.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */