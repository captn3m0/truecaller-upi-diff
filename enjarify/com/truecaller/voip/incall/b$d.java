package com.truecaller.voip.incall;

import android.graphics.Bitmap;

public abstract interface b$d
  extends b.a
{
  public abstract void a(int paramInt);
  
  public abstract void a(Bitmap paramBitmap);
  
  public abstract void a(String paramString);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean, long paramLong);
  
  public abstract void b(int paramInt);
  
  public abstract void b(String paramString);
  
  public abstract void e();
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */