package com.truecaller.voip.incall;

import c.d.a.a;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.manager.FailedChannelJoinReason;
import com.truecaller.voip.manager.f;
import com.truecaller.voip.manager.g;
import com.truecaller.voip.util.s;
import kotlinx.coroutines.ag;

final class c$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  c$b(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/voip/incall/c$b;
    c localc = b;
    localb.<init>(localc, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    int j = 1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label349;
      }
      paramObject = c.v(b);
      localObject2 = (ag)b;
      ((s)paramObject).a((ag)localObject2);
      paramObject = c.q(b);
      localObject2 = c.u(b);
      a = j;
      paramObject = ((g)paramObject).a((String)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (com.truecaller.voip.manager.c)paramObject;
    boolean bool2 = paramObject instanceof f;
    boolean bool1 = false;
    Object localObject2 = null;
    if (bool2)
    {
      c.w(b);
      paramObject = b;
      c.f((c)paramObject);
      bool1 = true;
    }
    else
    {
      bool2 = paramObject instanceof com.truecaller.voip.manager.d;
      if (!bool2) {
        break label339;
      }
      paramObject = a;
      localObject1 = d.b;
      int k = ((FailedChannelJoinReason)paramObject).ordinal();
      k = localObject1[k];
      switch (k)
      {
      default: 
        paramObject = new c/l;
        ((l)paramObject).<init>();
        throw ((Throwable)paramObject);
      case 2: 
        paramObject = VoipStateReason.JOIN_CHANNEL_FAILED;
        break;
      case 1: 
        paramObject = VoipStateReason.GET_RTC_TOKEN_FAILED;
      }
      localObject1 = b;
      VoipState localVoipState = VoipState.FAILED;
      c.a((c)localObject1, localVoipState, (VoipStateReason)paramObject);
      paramObject = new String[j];
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Cannot join the voip channel ");
      String str = c.u(b);
      ((StringBuilder)localObject1).append(str);
      str = ". Exiting...";
      ((StringBuilder)localObject1).append(str);
      localObject1 = ((StringBuilder)localObject1).toString();
      paramObject[0] = localObject1;
    }
    return Boolean.valueOf(bool1);
    label339:
    paramObject = new c/l;
    ((l)paramObject).<init>();
    throw ((Throwable)paramObject);
    label349:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */