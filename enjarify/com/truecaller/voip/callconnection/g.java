package com.truecaller.voip.callconnection;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import com.truecaller.common.h.u;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.VoipService.a;
import java.util.LinkedHashMap;
import java.util.Map;

public final class g
  implements f
{
  final Map a;
  private int b;
  private String c;
  private final com.truecaller.voip.k d;
  private final c e;
  private final u f;
  
  public g(com.truecaller.voip.k paramk, c paramc, u paramu)
  {
    d = paramk;
    e = paramc;
    f = paramu;
    int i = Build.VERSION.SDK_INT;
    b = i;
    paramk = new java/util/LinkedHashMap;
    paramk.<init>();
    paramk = (Map)paramk;
    a = paramk;
  }
  
  private final a a(String paramString, boolean paramBoolean)
  {
    u localu = f;
    paramString = localu.b(paramString);
    if (paramString == null) {
      return null;
    }
    return e.a(paramString, paramBoolean);
  }
  
  private final void a(a parama)
  {
    Object localObject1 = new String[1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Registering connection with number: ");
    String str = c;
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = a;
    localObject2 = c;
    ((Map)localObject1).put(localObject2, parama);
    localObject1 = new com/truecaller/voip/callconnection/g$a;
    ((g.a)localObject1).<init>(this, parama);
    localObject1 = (c.g.a.a)localObject1;
    parama.a((c.g.a.a)localObject1);
  }
  
  private final boolean c()
  {
    int i = b;
    int j = 26;
    if (i >= j)
    {
      com.truecaller.voip.k localk = d;
      boolean bool = localk.a();
      if (bool) {
        return false;
      }
    }
    return true;
  }
  
  public final void a(String paramString, c.g.a.b paramb)
  {
    Object localObject = "number";
    c.g.b.k.b(paramString, (String)localObject);
    boolean bool = c();
    if (bool) {
      return;
    }
    localObject = a;
    paramString = (a)((Map)localObject).get(paramString);
    if (paramString == null) {
      return;
    }
    a = paramb;
    paramb = paramString.getCallAudioState();
    if (paramb != null)
    {
      paramString = a;
      if (paramString != null)
      {
        paramString.invoke(paramb);
        return;
      }
      return;
    }
  }
  
  public final boolean a()
  {
    boolean bool1 = c();
    if (bool1) {
      return false;
    }
    Object localObject = a;
    bool1 = ((Map)localObject).isEmpty();
    boolean bool2 = true;
    bool1 ^= bool2;
    if (!bool1)
    {
      localObject = c;
      if (localObject == null) {
        return false;
      }
    }
    return bool2;
  }
  
  public final boolean a(String paramString)
  {
    Object localObject = "number";
    c.g.b.k.b(paramString, (String)localObject);
    boolean bool1 = c();
    if (bool1) {
      return false;
    }
    localObject = f;
    paramString = ((u)localObject).b(paramString);
    if (paramString == null) {
      return false;
    }
    localObject = a.get(paramString);
    if (localObject == null)
    {
      localObject = c;
      boolean bool2 = c.g.b.k.a(localObject, paramString);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  public final a b(String paramString)
  {
    String str1 = "number";
    c.g.b.k.b(paramString, str1);
    boolean bool1 = c();
    if (bool1) {
      return e.a();
    }
    bool1 = true;
    a locala = a(paramString, bool1);
    if (locala == null) {
      return e.a();
    }
    Object localObject1 = b;
    Object localObject2 = VoipService.e;
    localObject2 = b;
    String str2 = c;
    localObject2 = VoipService.a.a((Context)localObject2, str2);
    android.support.v4.content.b.a((Context)localObject1, (Intent)localObject2);
    localObject1 = c;
    boolean bool2 = c.g.b.k.a(paramString, localObject1);
    if (bool2)
    {
      bool2 = false;
      paramString = null;
      c = null;
    }
    paramString = a;
    bool2 = paramString.isEmpty() ^ bool1;
    if (bool2) {
      return e.a();
    }
    locala.a();
    a(locala);
    return locala;
  }
  
  public final void b()
  {
    boolean bool = c();
    if (bool) {
      return;
    }
    a.clear();
    c = null;
  }
  
  public final a c(String paramString)
  {
    String str = "number";
    c.g.b.k.b(paramString, str);
    boolean bool = c();
    if (bool) {
      return e.a();
    }
    bool = false;
    str = null;
    paramString = a(paramString, false);
    if (paramString == null) {
      return e.a();
    }
    paramString.a();
    a(paramString);
    return paramString;
  }
  
  public final void d(String paramString)
  {
    String str = "number";
    c.g.b.k.b(paramString, str);
    boolean bool = c();
    if (bool) {
      return;
    }
    c = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */