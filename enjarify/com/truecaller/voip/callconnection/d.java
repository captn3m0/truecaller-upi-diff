package com.truecaller.voip.callconnection;

import android.content.Context;
import c.d.f;
import c.g.b.k;

public final class d
  implements c
{
  private final f a;
  private final Context b;
  
  public d(f paramf, Context paramContext)
  {
    a = paramf;
    b = paramContext;
  }
  
  public final a a()
  {
    a locala = new com/truecaller/voip/callconnection/a;
    f localf = a;
    Context localContext = b;
    locala.<init>(localf, localContext, "", false);
    locala.a(4);
    return locala;
  }
  
  public final a a(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "number");
    a locala = new com/truecaller/voip/callconnection/a;
    f localf = a;
    Context localContext = b;
    locala.<init>(localf, localContext, paramString, paramBoolean);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */