package com.truecaller.voip.callconnection;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.telecom.CallAudioState;
import android.telecom.Connection;
import android.telecom.DisconnectCause;
import c.d.f;
import c.g.a.b;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.r;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incoming.IncomingVoipService;
import com.truecaller.voip.incoming.ui.IncomingVoipActivity;
import com.truecaller.voip.incoming.ui.IncomingVoipActivity.a;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.bs;

public final class a
  extends Connection
  implements ag
{
  b a;
  final Context b;
  final String c;
  final boolean d;
  private bn e;
  private a.a f;
  private com.truecaller.voip.incall.b.a g;
  private com.truecaller.voip.incoming.b.a h;
  private c.g.a.a i;
  private final f j;
  
  public a(f paramf, Context paramContext, String paramString, boolean paramBoolean)
  {
    j = paramf;
    b = paramContext;
    c = paramString;
    d = paramBoolean;
    paramf = bs.a(null);
    e = paramf;
    int k = 1;
    setAudioModeIsVoip(k);
    setConnectionProperties(128);
    setConnectionCapabilities(2);
    setCallerDisplayName("", 3);
    paramContext = r.a(c);
    setAddress(paramContext, k);
  }
  
  private final void a(Class paramClass, b paramb, c.g.a.a parama)
  {
    d();
    a.a locala = new com/truecaller/voip/callconnection/a$a;
    locala.<init>(this, paramb, parama);
    paramb = b;
    parama = new android/content/Intent;
    parama.<init>(paramb, paramClass);
    paramClass = locala;
    paramClass = (ServiceConnection)locala;
    int k = paramb.bindService(parama, paramClass, 0);
    if (k == 0)
    {
      paramClass = "Cannot connect to service. Destroying connection.";
      new String[1][0] = paramClass;
      k = 1;
      b(k);
    }
    f = locala;
  }
  
  private final void b()
  {
    Object localObject1 = new com/truecaller/voip/callconnection/a$d;
    ((a.d)localObject1).<init>(this);
    localObject1 = (b)localObject1;
    Object localObject2 = new com/truecaller/voip/callconnection/a$e;
    ((a.e)localObject2).<init>(this);
    localObject2 = (c.g.a.a)localObject2;
    a(VoipService.class, (b)localObject1, (c.g.a.a)localObject2);
  }
  
  private final void b(int paramInt)
  {
    new String[1][0] = "Destroying connection.";
    d();
    bn localbn = e;
    boolean bool = localbn.j();
    if (!bool)
    {
      localbn = e;
      localbn.n();
    }
    a(paramInt);
    destroy();
  }
  
  private final void d()
  {
    try
    {
      localObject1 = f;
      if (localObject1 != null)
      {
        Object localObject2 = "Unbinding from service.";
        new String[1][0] = localObject2;
        localObject2 = b;
        localObject1 = (ServiceConnection)localObject1;
        ((Context)localObject2).unbindService((ServiceConnection)localObject1);
      }
    }
    catch (Exception localException)
    {
      Object localObject1 = (Throwable)localException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
    }
    f = null;
  }
  
  public final f V_()
  {
    f localf1 = j;
    f localf2 = (f)e;
    return localf1.plus(localf2);
  }
  
  public final void a()
  {
    int k = getState();
    int m = 1;
    if (k != m)
    {
      new String[1][0] = "Initialize is already triggered.";
      return;
    }
    k = getState();
    m = 6;
    if (k == m)
    {
      new String[1][0] = "Connection is disconnected.";
      return;
    }
    setInitializing();
    boolean bool = d;
    if (bool)
    {
      b();
      return;
    }
    Object localObject1 = new com/truecaller/voip/callconnection/a$b;
    ((a.b)localObject1).<init>(this);
    localObject1 = (b)localObject1;
    Object localObject2 = new com/truecaller/voip/callconnection/a$c;
    ((a.c)localObject2).<init>(this);
    localObject2 = (c.g.a.a)localObject2;
    a(IncomingVoipService.class, (b)localObject1, (c.g.a.a)localObject2);
  }
  
  final void a(int paramInt)
  {
    DisconnectCause localDisconnectCause = new android/telecom/DisconnectCause;
    localDisconnectCause.<init>(paramInt);
    setDisconnected(localDisconnectCause);
    c.g.a.a locala = i;
    if (locala != null)
    {
      locala.invoke();
      return;
    }
  }
  
  public final void a(c.g.a.a parama)
  {
    i = parama;
    int k = getState();
    int m = 6;
    if (k == m) {
      parama.invoke();
    }
  }
  
  public final void onCallAudioStateChanged(CallAudioState paramCallAudioState)
  {
    int k = 1;
    Object localObject = new String[k];
    String str1 = String.valueOf(paramCallAudioState);
    String str2 = "Call audio state is changed: ".concat(str1);
    str1 = null;
    localObject[0] = str2;
    if (paramCallAudioState != null)
    {
      localObject = a;
      if (localObject != null)
      {
        ((b)localObject).invoke(paramCallAudioState);
        return;
      }
      return;
    }
  }
  
  public final void onDisconnect()
  {
    new String[1][0] = "On disconnect.";
    Object localObject = g;
    if (localObject != null) {
      ((com.truecaller.voip.incall.b.a)localObject).m();
    }
    localObject = i;
    if (localObject != null)
    {
      ((c.g.a.a)localObject).invoke();
      return;
    }
  }
  
  public final void onHold()
  {
    new String[1][0] = "On hold.";
    com.truecaller.voip.incall.b.a locala = g;
    if (locala != null)
    {
      locala.n();
      return;
    }
  }
  
  public final void onShowIncomingCallUi()
  {
    new String[1][0] = "On show incoming call ui.";
    super.onShowIncomingCallUi();
    Context localContext = b;
    Object localObject = IncomingVoipActivity.a;
    localObject = IncomingVoipActivity.a.a(b, false, false);
    localContext.startActivity((Intent)localObject);
  }
  
  public final void onStateChanged(int paramInt)
  {
    super.onStateChanged(paramInt);
    String[] arrayOfString = new String[1];
    String str = String.valueOf(paramInt);
    str = "State changed ".concat(str);
    arrayOfString[0] = str;
  }
  
  public final void onUnhold()
  {
    new String[1][0] = "On unHold.";
    com.truecaller.voip.incall.b.a locala = g;
    if (locala != null)
    {
      locala.o();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */