package com.truecaller.voip.callconnection;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import c.g.a.b;
import c.g.b.k;
import kotlinx.coroutines.bn;

final class a$a
  implements ServiceConnection
{
  boolean a;
  private final b c;
  private final c.g.a.a d;
  
  public a$a(a parama, b paramb, c.g.a.a parama1)
  {
    c = paramb;
    d = parama1;
    a = true;
  }
  
  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    String str = "className";
    k.b(paramComponentName, str);
    k.b(paramIBinder, "binder");
    new String[1][0] = "Connected to voip service.";
    paramComponentName = a.a(b);
    boolean bool = paramComponentName.j();
    if (bool)
    {
      new String[1][0] = "Job is not active.";
      return;
    }
    c.invoke(paramIBinder);
  }
  
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    String str = "className";
    k.b(paramComponentName, str);
    new String[1][0] = "Disconnected from voip service.";
    a.b(b);
    paramComponentName = a.a(b);
    boolean bool = paramComponentName.j();
    if (bool)
    {
      new String[1][0] = "Job is not active.";
      return;
    }
    paramComponentName = d;
    paramComponentName.invoke();
    bool = a;
    if (bool)
    {
      paramComponentName = b;
      a.c(paramComponentName);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */