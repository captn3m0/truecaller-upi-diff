package com.truecaller.voip.callconnection;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.b;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.ConnectionService;
import android.telecom.PhoneAccountHandle;
import c.g.b.k;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.VoipService.a;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;

public final class VoipCallConnectionService
  extends ConnectionService
{
  public f a;
  
  public final void onCreate()
  {
    super.onCreate();
    j.a.a().a(this);
  }
  
  public final Connection onCreateIncomingConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest)
  {
    int i = 1;
    paramPhoneAccountHandle = new String[i];
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("New incoming connection. Request:");
    Bundle localBundle = null;
    if (paramConnectionRequest != null) {
      localObject2 = paramConnectionRequest.getAddress();
    } else {
      localObject2 = null;
    }
    ((StringBuilder)localObject1).append(localObject2);
    Object localObject2 = " Extras:";
    ((StringBuilder)localObject1).append((String)localObject2);
    if (paramConnectionRequest != null) {
      localBundle = paramConnectionRequest.getExtras();
    }
    ((StringBuilder)localObject1).append(localBundle);
    localObject1 = ((StringBuilder)localObject1).toString();
    localBundle = null;
    paramPhoneAccountHandle[0] = localObject1;
    if (paramConnectionRequest != null)
    {
      paramPhoneAccountHandle = paramConnectionRequest.getAddress();
      if (paramPhoneAccountHandle != null)
      {
        paramPhoneAccountHandle = paramPhoneAccountHandle.getSchemeSpecificPart();
        if (paramPhoneAccountHandle != null)
        {
          paramConnectionRequest = a;
          if (paramConnectionRequest == null)
          {
            localObject1 = "connectionManager";
            k.a((String)localObject1);
          }
          return (Connection)paramConnectionRequest.c(paramPhoneAccountHandle);
        }
      }
    }
    paramPhoneAccountHandle = Connection.createCanceledConnection();
    k.a(paramPhoneAccountHandle, "Connection.createCanceledConnection()");
    return paramPhoneAccountHandle;
  }
  
  public final void onCreateIncomingConnectionFailed(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest)
  {
    int i = 1;
    String[] arrayOfString = new String[i];
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Incoming connection is failed. Request: ");
    Bundle localBundle = null;
    if (paramConnectionRequest != null) {
      localObject2 = paramConnectionRequest.getAddress();
    } else {
      localObject2 = null;
    }
    ((StringBuilder)localObject1).append(localObject2);
    Object localObject2 = " Extras:";
    ((StringBuilder)localObject1).append((String)localObject2);
    if (paramConnectionRequest != null) {
      localBundle = paramConnectionRequest.getExtras();
    }
    ((StringBuilder)localObject1).append(localBundle);
    localObject1 = ((StringBuilder)localObject1).toString();
    arrayOfString[0] = localObject1;
    super.onCreateIncomingConnectionFailed(paramPhoneAccountHandle, paramConnectionRequest);
  }
  
  public final Connection onCreateOutgoingConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest)
  {
    int i = 1;
    paramPhoneAccountHandle = new String[i];
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("New outgoing connection. Request:");
    Bundle localBundle = null;
    if (paramConnectionRequest != null) {
      localObject2 = paramConnectionRequest.getAddress();
    } else {
      localObject2 = null;
    }
    ((StringBuilder)localObject1).append(localObject2);
    Object localObject2 = " Extras:";
    ((StringBuilder)localObject1).append((String)localObject2);
    if (paramConnectionRequest != null) {
      localBundle = paramConnectionRequest.getExtras();
    }
    ((StringBuilder)localObject1).append(localBundle);
    localObject1 = ((StringBuilder)localObject1).toString();
    localBundle = null;
    paramPhoneAccountHandle[0] = localObject1;
    if (paramConnectionRequest != null)
    {
      paramPhoneAccountHandle = paramConnectionRequest.getAddress();
      if (paramPhoneAccountHandle != null)
      {
        paramPhoneAccountHandle = paramPhoneAccountHandle.getSchemeSpecificPart();
        if (paramPhoneAccountHandle != null)
        {
          paramConnectionRequest = a;
          if (paramConnectionRequest == null)
          {
            localObject1 = "connectionManager";
            k.a((String)localObject1);
          }
          return (Connection)paramConnectionRequest.b(paramPhoneAccountHandle);
        }
      }
    }
    paramPhoneAccountHandle = Connection.createCanceledConnection();
    k.a(paramPhoneAccountHandle, "Connection.createCanceledConnection()");
    return paramPhoneAccountHandle;
  }
  
  public final void onCreateOutgoingConnectionFailed(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest)
  {
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Outgoing connection is failed. Request: ");
    Bundle localBundle = null;
    if (paramConnectionRequest != null) {
      localObject3 = paramConnectionRequest.getAddress();
    } else {
      localObject3 = null;
    }
    ((StringBuilder)localObject2).append(localObject3);
    Object localObject3 = " Extras:";
    ((StringBuilder)localObject2).append((String)localObject3);
    if (paramConnectionRequest != null) {
      localBundle = paramConnectionRequest.getExtras();
    }
    ((StringBuilder)localObject2).append(localBundle);
    localObject2 = ((StringBuilder)localObject2).toString();
    localBundle = null;
    localObject1[0] = localObject2;
    super.onCreateOutgoingConnectionFailed(paramPhoneAccountHandle, paramConnectionRequest);
    if (paramConnectionRequest != null)
    {
      paramPhoneAccountHandle = paramConnectionRequest.getAddress();
      if (paramPhoneAccountHandle != null)
      {
        paramPhoneAccountHandle = paramPhoneAccountHandle.getSchemeSpecificPart();
        if (paramPhoneAccountHandle != null)
        {
          new String[1][0] = "Creating outgoing connection is failed. Falling back to default behaviour.";
          paramConnectionRequest = this;
          paramConnectionRequest = (Context)this;
          localObject1 = VoipService.e;
          paramPhoneAccountHandle = VoipService.a.a(paramConnectionRequest, paramPhoneAccountHandle);
          b.a(paramConnectionRequest, paramPhoneAccountHandle);
          return;
        }
      }
    }
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    new String[1][0] = "On destroy";
    f localf = a;
    if (localf == null)
    {
      String str = "connectionManager";
      k.a(str);
    }
    localf.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.VoipCallConnectionService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */