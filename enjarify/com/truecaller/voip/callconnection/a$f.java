package com.truecaller.voip.callconnection;

import android.telecom.CallAudioState;
import c.d.c;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import com.truecaller.voip.util.AudioRoute;

final class a$f
  extends c.d.b.a.k
  implements m
{
  int a;
  private com.truecaller.voip.manager.k c;
  
  a$f(a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/voip/callconnection/a$f;
    a locala = b;
    localf.<init>(locala, paramc);
    paramObject = (com.truecaller.voip.manager.k)paramObject;
    c = ((com.truecaller.voip.manager.k)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = c;
        bool1 = true;
        Object localObject1 = new String[bool1];
        int j = 0;
        String str1 = String.valueOf(paramObject);
        String str2 = "New setting is received ".concat(str1);
        localObject1[0] = str2;
        localObject1 = c.a;
        Object localObject2 = b.b;
        int k = ((AudioRoute)localObject1).ordinal();
        k = localObject2[k];
        switch (k)
        {
        default: 
          paramObject = new c/l;
          ((l)paramObject).<init>();
          throw ((Throwable)paramObject);
        case 4: 
          k = 2;
          break;
        case 3: 
          k = 8;
          break;
        case 1: 
        case 2: 
          k = 5;
        }
        localObject2 = b.getCallAudioState();
        if (localObject2 != null)
        {
          j = ((CallAudioState)localObject2).getRoute();
          if (k == j) {}
        }
        else
        {
          localObject2 = b;
          ((a)localObject2).setAudioRoute(k);
        }
        boolean bool2 = b;
        if (bool2 == bool1)
        {
          paramObject = b;
          ((a)paramObject).setOnHold();
        }
        else if (!bool2)
        {
          paramObject = b;
          ((a)paramObject).setActive();
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */