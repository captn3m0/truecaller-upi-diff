package com.truecaller.voip.callconnection;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.ag;

final class a$g
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  a$g(a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    g localg = new com/truecaller/voip/callconnection/a$g;
    a locala = b;
    localg.<init>(locala, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localg;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = c;
        bool1 = true;
        Object localObject1 = new String[bool1];
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("New state is received ");
        VoipState localVoipState = a;
        ((StringBuilder)localObject2).append(localVoipState);
        localObject2 = ((StringBuilder)localObject2).toString();
        localVoipState = null;
        localObject1[0] = localObject2;
        localObject1 = a;
        localObject2 = b.a;
        int k = ((VoipState)localObject1).ordinal();
        k = localObject2[k];
        switch (k)
        {
        default: 
          break;
        case 3: 
          localObject1 = b;
          ((a)localObject1).setActive();
          break;
        case 2: 
          localObject1 = a.d(b);
          if (localObject1 != null) {
            a = false;
          }
          localObject1 = b;
          a.e((a)localObject1);
          break;
        case 1: 
          localObject1 = b;
          boolean bool2 = d;
          if (!bool2)
          {
            localObject1 = b;
            ((a)localObject1).setRinging();
          }
          break;
        }
        localObject1 = b;
        paramObject = a;
        localObject2 = VoipState.ONGOING;
        int j;
        if (paramObject != localObject2) {
          j = 2;
        }
        ((a)localObject1).setConnectionCapabilities(j);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (g)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((g)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */