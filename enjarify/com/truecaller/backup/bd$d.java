package com.truecaller.backup;

import android.content.Intent;
import android.support.v4.app.Fragment;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import kotlinx.coroutines.ag;

final class bd$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  bd$d(c paramc, bd parambd, Fragment paramFragment, GoogleSignInClient paramGoogleSignInClient)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/backup/bd$d;
    bd localbd = b;
    Fragment localFragment = c;
    GoogleSignInClient localGoogleSignInClient = d;
    locald.<init>(paramc, localbd, localFragment, localGoogleSignInClient);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = c;
        boolean bool2 = ((Fragment)paramObject).isAdded();
        if (!bool2)
        {
          b.c();
          return x.a;
        }
        paramObject = c;
        localObject = d.a();
        ((Fragment)paramObject).startActivityForResult((Intent)localObject, 4321);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bd.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */