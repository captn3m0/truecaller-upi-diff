package com.truecaller.backup;

import android.support.v4.app.Fragment;
import c.g.a.m;
import c.g.b.k;
import c.g.b.r;
import c.g.b.s;
import c.g.b.w;
import c.l.g;
import c.n;
import c.t;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.at;
import com.truecaller.analytics.e.a;
import com.truecaller.bb;
import com.truecaller.common.g.a;
import com.truecaller.messaging.h;
import com.truecaller.tcpermissions.l;
import com.truecaller.tcpermissions.o;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.utils.i;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.bs;
import org.apache.a.d.d;

public final class p
  extends bb
  implements o.a
{
  private final bn c;
  private final c.d.f d;
  private final c.d.f e;
  private final e f;
  private final i g;
  private final a h;
  private final l i;
  private final h j;
  private final com.truecaller.common.background.b k;
  private final com.truecaller.analytics.b l;
  private final com.truecaller.androidactors.f m;
  private final o n;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/s;
    c.l.b localb = w.a(p.class);
    ((s)localObject).<init>(localb, "hasSMSPermissions", "<v#0>");
    localObject = (g)w.a((r)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public p(c.d.f paramf1, c.d.f paramf2, e parame, i parami, a parama, l paraml, h paramh, com.truecaller.common.background.b paramb, com.truecaller.analytics.b paramb1, com.truecaller.androidactors.f paramf, o paramo)
  {
    d = paramf1;
    e = paramf2;
    f = parame;
    g = parami;
    h = parama;
    i = paraml;
    j = paramh;
    k = paramb;
    l = paramb1;
    m = paramf;
    n = paramo;
    paramf1 = bs.a(null);
    c = paramf1;
  }
  
  private final void a(String paramString)
  {
    Object localObject1 = l;
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("StartupDialog");
    localObject2 = ((e.a)localObject2).a("Context", "wizard").a("Type", "Backup").a("Action", paramString).a();
    k.a(localObject2, "AnalyticsEvent.Builder(A…\n                .build()");
    ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject2);
    localObject1 = (ae)m.a();
    localObject2 = com.truecaller.tracking.events.ao.b();
    Object localObject3 = (CharSequence)"Backup_Restore_Dialog";
    localObject2 = ((ao.a)localObject2).a((CharSequence)localObject3);
    localObject3 = new n[3];
    n localn = t.a("Context", "wizard");
    localObject3[0] = localn;
    localn = t.a("Type", "Backup");
    localObject3[1] = localn;
    paramString = t.a("Action", paramString);
    localObject3[2] = paramString;
    paramString = c.a.ag.a((n[])localObject3);
    paramString = (d)((ao.a)localObject2).a(paramString).a();
    ((ae)localObject1).a(paramString);
  }
  
  private final void b(String paramString)
  {
    com.truecaller.analytics.b localb = l;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("StartupDialog");
    paramString = locala.a("Context", "wizard").a("Type", "BackupSmsPermission").a("Action", paramString).a();
    k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.a(paramString);
  }
  
  public final kotlinx.coroutines.ao a(Fragment paramFragment)
  {
    k.b(paramFragment, "fragment");
    kotlinx.coroutines.ag localag = (kotlinx.coroutines.ag)bg.a;
    c.d.f localf = d;
    Object localObject = (c.d.f)c;
    localf = localf.plus((c.d.f)localObject);
    localObject = new com/truecaller/backup/p$a;
    ((p.a)localObject).<init>(this, paramFragment, null);
    localObject = (m)localObject;
    return kotlinx.coroutines.e.a(localag, localf, (m)localObject, 2);
  }
  
  public final void a()
  {
    o.b localb = (o.b)b;
    if (localb != null) {
      localb.dismiss();
    }
    a("NegativeBtnClicked");
  }
  
  public final void a(int paramInt)
  {
    String[] arrayOfString = new String[1];
    String str1 = String.valueOf(paramInt);
    String str2 = "Resolution result: requestCode = ".concat(str1);
    str1 = null;
    arrayOfString[0] = str2;
    int i1 = 4321;
    if (paramInt != i1) {
      return;
    }
    f.b();
  }
  
  public final void a(long paramLong)
  {
    long l1 = TimeUnit.DAYS.toHours(7);
    a locala = h;
    boolean bool = true;
    locala.b("backup_enabled", bool);
    h.b("key_backup_frequency_hours", l1);
    h.b("key_backup_last_success", 0L);
    k.c();
    k.b();
    Object localObject1 = k;
    int[] arrayOfInt = new int[0];
    ((com.truecaller.common.background.b)localObject1).a(paramLong, 10002, arrayOfInt);
    Object localObject2 = l;
    Object localObject3 = new com/truecaller/analytics/e$a;
    ((e.a)localObject3).<init>("SettingChanged");
    localObject3 = ((e.a)localObject3).a("Context", "wizard").a("Setting", "Backup").a("State", "Enabled").a();
    k.a(localObject3, "AnalyticsEvent.Builder(A…\n                .build()");
    ((com.truecaller.analytics.b)localObject2).a((com.truecaller.analytics.e)localObject3);
    localObject2 = (ae)m.a();
    localObject3 = (d)at.a(bool, "wizard");
    ((ae)localObject2).a((d)localObject3);
    localObject2 = (kotlinx.coroutines.ag)bg.a;
    localObject3 = e;
    localObject1 = new com/truecaller/backup/p$c;
    ((p.c)localObject1).<init>(this, null);
    localObject1 = (m)localObject1;
    kotlinx.coroutines.e.b((kotlinx.coroutines.ag)localObject2, (c.d.f)localObject3, (m)localObject1, 2);
  }
  
  public final void b()
  {
    a("DialogCancelled");
  }
  
  public final bn c()
  {
    kotlinx.coroutines.ag localag = (kotlinx.coroutines.ag)bg.a;
    c.d.f localf = e;
    Object localObject = new com/truecaller/backup/p$b;
    ((p.b)localObject).<init>(this, null);
    localObject = (m)localObject;
    return kotlinx.coroutines.e.b(localag, localf, (m)localObject, 2);
  }
  
  public final void e()
  {
    a(0L);
    b("NegativeBtnClicked");
  }
  
  public final void f()
  {
    b("DialogCancelled");
  }
  
  public final void y_()
  {
    o.b localb = (o.b)b;
    if (localb != null) {
      localb.c();
    }
    c.n();
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */