package com.truecaller.backup;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.log.AssertionUtil;
import e.r;
import java.io.IOException;
import kotlinx.coroutines.ag;

final class ah$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  ah$b(ah paramah, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/backup/ah$b;
    ah localah = b;
    localb.<init>(localah, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = null;
        long l;
        try
        {
          localObject = s.a;
          localObject = s.a();
          localObject = ((e.b)localObject).c();
          if (localObject != null)
          {
            localObject = ((r)localObject).e();
            localObject = (BackupDto)localObject;
            if (localObject != null)
            {
              l = a;
              localObject = c.d.b.a.b.a(l);
              break label89;
            }
          }
          bool = false;
          localObject = null;
        }
        catch (IOException localIOException)
        {
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localIOException);
          bool = false;
          localObject = null;
        }
        label89:
        int j = 1;
        String[] arrayOfString = new String[j];
        String str1 = String.valueOf(localObject);
        String str2 = "Backup timestamp is fetched. Timestamp: ".concat(str1);
        arrayOfString[0] = str2;
        if (localObject != null)
        {
          l = ((Number)localObject).longValue();
          b.a.b("key_backup_fetched_timestamp", l);
          return x.a;
        }
        return null;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ah.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */