package com.truecaller.backup;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;

final class as
  implements Cursor
{
  final com.truecaller.utils.extensions.g b;
  final com.truecaller.utils.extensions.g c;
  private final Cursor d;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[2];
    Object localObject = new c/g/b/u;
    b localb = w.a(as.class);
    ((u)localObject).<init>(localb, "id", "getId()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(as.class);
    ((u)localObject).<init>(localb, "timestamp", "getTimestamp()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public as(Cursor paramCursor)
  {
    d = paramCursor;
    long l = 0L;
    Object localObject = Long.valueOf(l);
    com.truecaller.utils.extensions.g localg1 = new com/truecaller/utils/extensions/g;
    b localb = w.a(Long.class);
    localg1.<init>("_id", localb, localObject);
    b = localg1;
    Long localLong = Long.valueOf(l);
    com.truecaller.utils.extensions.g localg2 = new com/truecaller/utils/extensions/g;
    localObject = w.a(Long.class);
    localg2.<init>("date", (b)localObject, localLong);
    c = localg2;
  }
  
  public final void close()
  {
    d.close();
  }
  
  public final void copyStringToBuffer(int paramInt, CharArrayBuffer paramCharArrayBuffer)
  {
    d.copyStringToBuffer(paramInt, paramCharArrayBuffer);
  }
  
  public final void deactivate()
  {
    d.deactivate();
  }
  
  public final byte[] getBlob(int paramInt)
  {
    return d.getBlob(paramInt);
  }
  
  public final int getColumnCount()
  {
    return d.getColumnCount();
  }
  
  public final int getColumnIndex(String paramString)
  {
    return d.getColumnIndex(paramString);
  }
  
  public final int getColumnIndexOrThrow(String paramString)
  {
    return d.getColumnIndexOrThrow(paramString);
  }
  
  public final String getColumnName(int paramInt)
  {
    return d.getColumnName(paramInt);
  }
  
  public final String[] getColumnNames()
  {
    return d.getColumnNames();
  }
  
  public final int getCount()
  {
    return d.getCount();
  }
  
  public final double getDouble(int paramInt)
  {
    return d.getDouble(paramInt);
  }
  
  public final Bundle getExtras()
  {
    return d.getExtras();
  }
  
  public final float getFloat(int paramInt)
  {
    return d.getFloat(paramInt);
  }
  
  public final int getInt(int paramInt)
  {
    return d.getInt(paramInt);
  }
  
  public final long getLong(int paramInt)
  {
    return d.getLong(paramInt);
  }
  
  public final Uri getNotificationUri()
  {
    return d.getNotificationUri();
  }
  
  public final int getPosition()
  {
    return d.getPosition();
  }
  
  public final short getShort(int paramInt)
  {
    return d.getShort(paramInt);
  }
  
  public final String getString(int paramInt)
  {
    return d.getString(paramInt);
  }
  
  public final int getType(int paramInt)
  {
    return d.getType(paramInt);
  }
  
  public final boolean getWantsAllOnMoveCalls()
  {
    return d.getWantsAllOnMoveCalls();
  }
  
  public final boolean isAfterLast()
  {
    return d.isAfterLast();
  }
  
  public final boolean isBeforeFirst()
  {
    return d.isBeforeFirst();
  }
  
  public final boolean isClosed()
  {
    return d.isClosed();
  }
  
  public final boolean isFirst()
  {
    return d.isFirst();
  }
  
  public final boolean isLast()
  {
    return d.isLast();
  }
  
  public final boolean isNull(int paramInt)
  {
    return d.isNull(paramInt);
  }
  
  public final boolean move(int paramInt)
  {
    return d.move(paramInt);
  }
  
  public final boolean moveToFirst()
  {
    return d.moveToFirst();
  }
  
  public final boolean moveToLast()
  {
    return d.moveToLast();
  }
  
  public final boolean moveToNext()
  {
    return d.moveToNext();
  }
  
  public final boolean moveToPosition(int paramInt)
  {
    return d.moveToPosition(paramInt);
  }
  
  public final boolean moveToPrevious()
  {
    return d.moveToPrevious();
  }
  
  public final void registerContentObserver(ContentObserver paramContentObserver)
  {
    d.registerContentObserver(paramContentObserver);
  }
  
  public final void registerDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    d.registerDataSetObserver(paramDataSetObserver);
  }
  
  public final boolean requery()
  {
    return d.requery();
  }
  
  public final Bundle respond(Bundle paramBundle)
  {
    return d.respond(paramBundle);
  }
  
  public final void setExtras(Bundle paramBundle)
  {
    d.setExtras(paramBundle);
  }
  
  public final void setNotificationUri(ContentResolver paramContentResolver, Uri paramUri)
  {
    d.setNotificationUri(paramContentResolver, paramUri);
  }
  
  public final void unregisterContentObserver(ContentObserver paramContentObserver)
  {
    d.unregisterContentObserver(paramContentObserver);
  }
  
  public final void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    d.unregisterDataSetObserver(paramDataSetObserver);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.as
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */