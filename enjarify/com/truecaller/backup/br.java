package com.truecaller.backup;

import android.content.Context;
import android.support.v4.app.Fragment;
import c.g.a.m;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.bb;
import com.truecaller.common.g.a;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.utils.i;
import java.text.DateFormat;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.bs;
import org.apache.a.d.d;

public final class br
  extends bb
  implements bq.a
{
  String a;
  boolean c;
  final Context d;
  final c.d.f e;
  final e f;
  final e g;
  final i h;
  final b i;
  final com.truecaller.androidactors.f j;
  private final bn k;
  private boolean l;
  private final c.d.f m;
  private final a n;
  
  public br(Context paramContext, c.d.f paramf1, c.d.f paramf2, e parame1, e parame2, i parami, a parama, b paramb, com.truecaller.androidactors.f paramf)
  {
    d = paramContext;
    m = paramf1;
    e = paramf2;
    f = parame1;
    g = parame2;
    h = parami;
    n = parama;
    i = paramb;
    j = paramf;
    paramContext = bs.a(null);
    k = paramContext;
  }
  
  public final kotlinx.coroutines.ao a(Fragment paramFragment)
  {
    k.b(paramFragment, "fragment");
    kotlinx.coroutines.ag localag = (kotlinx.coroutines.ag)bg.a;
    c.d.f localf = m;
    Object localObject = (c.d.f)k;
    localf = localf.plus((c.d.f)localObject);
    localObject = new com/truecaller/backup/br$c;
    ((br.c)localObject).<init>(this, paramFragment, null);
    localObject = (m)localObject;
    return kotlinx.coroutines.e.a(localag, localf, (m)localObject, 2);
  }
  
  public final void a()
  {
    l = false;
    bq.b localb = (bq.b)b;
    if (localb != null)
    {
      localb.g();
      return;
    }
  }
  
  public final void a(int paramInt)
  {
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Resolution result: requestCode = ");
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).append(", resultCode = ");
    ((StringBuilder)localObject).append(paramInt);
    localObject = ((StringBuilder)localObject).toString();
    arrayOfString[0] = localObject;
    int i1 = 4321;
    if (paramInt != i1) {
      return;
    }
    f.b();
  }
  
  public final void a(long paramLong)
  {
    long l1 = 0L;
    boolean bool = paramLong < l1;
    Object localObject1;
    if (!bool)
    {
      localObject1 = "";
    }
    else
    {
      Object localObject2 = (bq.b)b;
      String str = null;
      if (localObject2 != null)
      {
        localObject2 = ((bq.b)localObject2).d();
        if (localObject2 != null)
        {
          localObject3 = Long.valueOf(paramLong);
          localObject2 = ((DateFormat)localObject2).format(localObject3);
          break label74;
        }
      }
      int i2 = 0;
      localObject2 = null;
      label74:
      Object localObject3 = (bq.b)b;
      if (localObject3 != null)
      {
        localObject3 = ((bq.b)localObject3).e();
        if (localObject3 != null)
        {
          localObject1 = Long.valueOf(paramLong);
          str = ((DateFormat)localObject3).format(localObject1);
        }
      }
      localObject1 = d;
      int i3 = 2131888701;
      int i1 = 2;
      localObject3 = new Object[i1];
      localObject3[0] = localObject2;
      i2 = 1;
      localObject3[i2] = str;
      localObject1 = ((Context)localObject1).getString(i3, (Object[])localObject3);
    }
    bq.b localb = (bq.b)b;
    if (localb != null)
    {
      k.a(localObject1, "timestamp");
      localb.b((String)localObject1);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    a = paramString;
    b("Shown");
  }
  
  public final void b()
  {
    bq.b localb = (bq.b)b;
    if (localb != null) {
      localb.dismiss();
    }
    boolean bool = l;
    if (bool)
    {
      b("DialogCancelled");
      return;
    }
    b("NegativeBtnClicked");
  }
  
  final void b(String paramString)
  {
    Object localObject1 = a;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = i;
    Object localObject3 = new com/truecaller/analytics/e$a;
    ((e.a)localObject3).<init>("StartupDialog");
    localObject3 = ((e.a)localObject3).a("Context", (String)localObject1).a("Type", "Restore").a("Action", paramString).a();
    k.a(localObject3, "AnalyticsEvent.Builder(A…\n                .build()");
    ((b)localObject2).a((com.truecaller.analytics.e)localObject3);
    localObject2 = (ae)j.a();
    localObject3 = com.truecaller.tracking.events.ao.b();
    Object localObject4 = (CharSequence)"Backup_Restore_Dialog";
    localObject3 = ((ao.a)localObject3).a((CharSequence)localObject4);
    localObject4 = new n[3];
    localObject1 = t.a("Context", localObject1);
    localObject4[0] = localObject1;
    n localn = t.a("Type", "Restore");
    localObject4[1] = localn;
    paramString = t.a("Action", paramString);
    localObject4[2] = paramString;
    paramString = c.a.ag.a((n[])localObject4);
    paramString = (d)((ao.a)localObject3).a(paramString).a();
    ((ae)localObject2).a(paramString);
  }
  
  public final void c()
  {
    boolean bool = true;
    l = bool;
    bq.b localb = (bq.b)b;
    if (localb != null)
    {
      localb.g();
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    bq.b localb = (bq.b)b;
    if (localb != null) {
      localb.i();
    }
    k.n();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.br
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */