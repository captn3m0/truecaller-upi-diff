package com.truecaller.backup;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.messaging.data.t;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

final class bu$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  long c;
  int d;
  private ag f;
  
  bu$a(bu parambu, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/backup/bu$a;
    bu localbu = e;
    locala.<init>(localbu, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = d;
    int j = 1;
    e.a locala = null;
    int k = 2;
    long l2;
    boolean bool3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      long l1 = c;
      localObject2 = (String)a;
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        l2 = l1;
        localObject3 = localObject2;
        break label488;
      }
      throw a;
    case 2: 
      l2 = c;
      localObject3 = (String)a;
      bool3 = paramObject instanceof o.b;
      if (!bool3) {
        break label404;
      }
      throw a;
    case 1: 
      l2 = c;
      localObject3 = (String)a;
      bool3 = paramObject instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label700;
      }
      paramObject = e.f.g();
      localObject3 = (ag)bg.a;
      c.d.f localf = e.a;
      Object localObject4 = new com/truecaller/backup/bu$a$1;
      ((bu.a.1)localObject4).<init>(this, (String)paramObject, null);
      localObject4 = (m)localObject4;
      kotlinx.coroutines.e.b((ag)localObject3, localf, (m)localObject4, k);
      l2 = System.currentTimeMillis();
      localObject3 = e.c;
      a = paramObject;
      c = l2;
      d = j;
      localObject3 = ((e)localObject3).c(this);
      if (localObject3 == localObject1) {
        return localObject1;
      }
      Object localObject5 = localObject3;
      localObject3 = paramObject;
      paramObject = localObject5;
    }
    paramObject = (BackupResult)paramObject;
    Object localObject6 = bv.a;
    int n = ((BackupResult)paramObject).ordinal();
    int m = localObject6[n];
    switch (m)
    {
    default: 
      localObject6 = e.d;
      a = localObject3;
      c = l2;
      b = paramObject;
      d = k;
      paramObject = ((e)localObject6).c(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label404:
      paramObject = (BackupResult)paramObject;
    }
    localObject6 = bv.b;
    n = ((BackupResult)paramObject).ordinal();
    m = localObject6[n];
    if (m == j)
    {
      localObject2 = e.e;
      a = localObject3;
      c = l2;
      b = paramObject;
      int i1 = 3;
      d = i1;
      paramObject = ((bx)localObject2).b(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label488:
      paramObject = (BackupResult)paramObject;
    }
    long l3 = System.currentTimeMillis();
    localObject1 = BackupResult.Success;
    if (paramObject == localObject1)
    {
      ((com.truecaller.callhistory.a)e.i.a()).a();
      localObject1 = (t)e.h.a();
      j = 0;
      localObject2 = null;
      ((t)localObject1).a(false);
    }
    localObject1 = (ag)bg.a;
    Object localObject2 = e.a;
    Object localObject7 = new com/truecaller/backup/bu$a$2;
    ((bu.a.2)localObject7).<init>(this, (BackupResult)paramObject, (String)localObject3, null);
    localObject7 = (m)localObject7;
    kotlinx.coroutines.e.b((ag)localObject1, (c.d.f)localObject2, (m)localObject7, k);
    localObject1 = e;
    l3 -= l2;
    double d1 = TimeUnit.MILLISECONDS.toSeconds(l3);
    localObject1 = g;
    locala = new com/truecaller/analytics/e$a;
    locala.<init>("BackupTask");
    paramObject = ((BackupResult)paramObject).name();
    paramObject = locala.a("Result", (String)paramObject).a("Type", "Restore");
    Object localObject3 = Double.valueOf(d1);
    paramObject = ((e.a)paramObject).a((Double)localObject3).a();
    c.g.b.k.a(paramObject, "AnalyticsEvent.Builder(A…\n                .build()");
    ((b)localObject1).a((com.truecaller.analytics.e)paramObject);
    return x.a;
    label700:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bu.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */