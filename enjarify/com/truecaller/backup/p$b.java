package com.truecaller.backup;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.l.g;
import c.o.b;
import c.x;
import com.truecaller.messaging.h;
import com.truecaller.tcpermissions.d;
import com.truecaller.tcpermissions.l;
import com.truecaller.tcpermissions.o;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.ag;

final class p$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  p$b(p paramp, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/backup/p$b;
    p localp = b;
    localb.<init>(localp, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    int j = 1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label332;
      }
      p.b(b, "PositiveBtnClicked");
      paramObject = p.f(b);
      localObject2 = p.e(b).d();
      int k = 3;
      localObject2 = (String[])Arrays.copyOf((Object[])localObject2, k);
      a = j;
      paramObject = ((o)paramObject).a((String[])localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    localObject1 = p.a;
    Object localObject2 = null;
    localObject1 = localObject1[0];
    paramObject = (d)paramObject;
    Object localObject3 = new String[j];
    Object localObject4 = new java/lang/StringBuilder;
    String str = "Sms permissions request result ";
    ((StringBuilder)localObject4).<init>(str);
    boolean bool3 = ((d)paramObject).a((g)localObject1);
    ((StringBuilder)localObject4).append(bool3);
    localObject4 = ((StringBuilder)localObject4).toString();
    localObject3[0] = localObject4;
    boolean bool1 = ((d)paramObject).a((g)localObject1);
    long l1 = 0L;
    if (bool1)
    {
      localObject2 = p.g(b);
      long l2 = ((h)localObject2).a();
      bool1 = l2 < l1;
      if (!bool1)
      {
        localObject2 = b;
        localObject3 = TimeUnit.MINUTES;
        long l3 = 2;
        l1 = ((TimeUnit)localObject3).toMillis(l3);
        ((p)localObject2).a(l1);
        break label308;
      }
    }
    localObject2 = b;
    ((o.a)localObject2).a(l1);
    label308:
    localObject2 = b;
    boolean bool4 = ((d)paramObject).a((g)localObject1);
    p.a((p)localObject2, bool4);
    return x.a;
    label332:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.p.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */