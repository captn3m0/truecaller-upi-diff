package com.truecaller.backup;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;

final class bf
  implements Cursor
{
  final com.truecaller.utils.extensions.g b;
  final com.truecaller.utils.extensions.g c;
  private final com.truecaller.utils.extensions.g d;
  private final Cursor e;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[3];
    Object localObject = new c/g/b/u;
    b localb = w.a(bf.class);
    ((u)localObject).<init>(localb, "id", "getId()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bf.class);
    ((u)localObject).<init>(localb, "callLogId", "getCallLogId()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bf.class);
    ((u)localObject).<init>(localb, "timestamp", "getTimestamp()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[2] = localObject;
    a = arrayOfg;
  }
  
  public bf(Cursor paramCursor)
  {
    e = paramCursor;
    long l = 0L;
    Object localObject = Long.valueOf(l);
    com.truecaller.utils.extensions.g localg1 = new com/truecaller/utils/extensions/g;
    b localb = w.a(Long.class);
    localg1.<init>("_id", localb, localObject);
    d = localg1;
    localObject = Long.valueOf(l);
    localg1 = new com/truecaller/utils/extensions/g;
    localb = w.a(Long.class);
    localg1.<init>("call_log_id", localb, localObject);
    b = localg1;
    Long localLong = Long.valueOf(l);
    com.truecaller.utils.extensions.g localg2 = new com/truecaller/utils/extensions/g;
    localObject = w.a(Long.class);
    localg2.<init>("timestamp", (b)localObject, localLong);
    c = localg2;
  }
  
  public final long a()
  {
    com.truecaller.utils.extensions.g localg = d;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[0];
    return ((Number)localg.a((Cursor)localObject, localg1)).longValue();
  }
  
  public final void close()
  {
    e.close();
  }
  
  public final void copyStringToBuffer(int paramInt, CharArrayBuffer paramCharArrayBuffer)
  {
    e.copyStringToBuffer(paramInt, paramCharArrayBuffer);
  }
  
  public final void deactivate()
  {
    e.deactivate();
  }
  
  public final byte[] getBlob(int paramInt)
  {
    return e.getBlob(paramInt);
  }
  
  public final int getColumnCount()
  {
    return e.getColumnCount();
  }
  
  public final int getColumnIndex(String paramString)
  {
    return e.getColumnIndex(paramString);
  }
  
  public final int getColumnIndexOrThrow(String paramString)
  {
    return e.getColumnIndexOrThrow(paramString);
  }
  
  public final String getColumnName(int paramInt)
  {
    return e.getColumnName(paramInt);
  }
  
  public final String[] getColumnNames()
  {
    return e.getColumnNames();
  }
  
  public final int getCount()
  {
    return e.getCount();
  }
  
  public final double getDouble(int paramInt)
  {
    return e.getDouble(paramInt);
  }
  
  public final Bundle getExtras()
  {
    return e.getExtras();
  }
  
  public final float getFloat(int paramInt)
  {
    return e.getFloat(paramInt);
  }
  
  public final int getInt(int paramInt)
  {
    return e.getInt(paramInt);
  }
  
  public final long getLong(int paramInt)
  {
    return e.getLong(paramInt);
  }
  
  public final Uri getNotificationUri()
  {
    return e.getNotificationUri();
  }
  
  public final int getPosition()
  {
    return e.getPosition();
  }
  
  public final short getShort(int paramInt)
  {
    return e.getShort(paramInt);
  }
  
  public final String getString(int paramInt)
  {
    return e.getString(paramInt);
  }
  
  public final int getType(int paramInt)
  {
    return e.getType(paramInt);
  }
  
  public final boolean getWantsAllOnMoveCalls()
  {
    return e.getWantsAllOnMoveCalls();
  }
  
  public final boolean isAfterLast()
  {
    return e.isAfterLast();
  }
  
  public final boolean isBeforeFirst()
  {
    return e.isBeforeFirst();
  }
  
  public final boolean isClosed()
  {
    return e.isClosed();
  }
  
  public final boolean isFirst()
  {
    return e.isFirst();
  }
  
  public final boolean isLast()
  {
    return e.isLast();
  }
  
  public final boolean isNull(int paramInt)
  {
    return e.isNull(paramInt);
  }
  
  public final boolean move(int paramInt)
  {
    return e.move(paramInt);
  }
  
  public final boolean moveToFirst()
  {
    return e.moveToFirst();
  }
  
  public final boolean moveToLast()
  {
    return e.moveToLast();
  }
  
  public final boolean moveToNext()
  {
    return e.moveToNext();
  }
  
  public final boolean moveToPosition(int paramInt)
  {
    return e.moveToPosition(paramInt);
  }
  
  public final boolean moveToPrevious()
  {
    return e.moveToPrevious();
  }
  
  public final void registerContentObserver(ContentObserver paramContentObserver)
  {
    e.registerContentObserver(paramContentObserver);
  }
  
  public final void registerDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    e.registerDataSetObserver(paramDataSetObserver);
  }
  
  public final boolean requery()
  {
    return e.requery();
  }
  
  public final Bundle respond(Bundle paramBundle)
  {
    return e.respond(paramBundle);
  }
  
  public final void setExtras(Bundle paramBundle)
  {
    e.setExtras(paramBundle);
  }
  
  public final void setNotificationUri(ContentResolver paramContentResolver, Uri paramUri)
  {
    e.setNotificationUri(paramContentResolver, paramUri);
  }
  
  public final void unregisterContentObserver(ContentObserver paramContentObserver)
  {
    e.unregisterContentObserver(paramContentObserver);
  }
  
  public final void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    e.unregisterDataSetObserver(paramDataSetObserver);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bf
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */