package com.truecaller.backup;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.analytics.b;
import com.truecaller.content.c.ab;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.d;
import java.io.Closeable;
import java.io.File;
import kotlinx.coroutines.g;

public final class ah
  implements ag
{
  final com.truecaller.common.g.a a;
  private final c.n.k b;
  private final f c;
  private final Context d;
  private final b e;
  
  public ah(f paramf, Context paramContext, com.truecaller.common.g.a parama, b paramb)
  {
    c = paramf;
    d = paramContext;
    a = parama;
    e = paramb;
    paramf = new c/n/k;
    paramf.<init>("^\\++");
    b = paramf;
  }
  
  public final File a()
  {
    try
    {
      localObject1 = d;
      Object localObject2 = com.truecaller.content.c.ag.b();
      b localb = e;
      localObject1 = com.truecaller.content.c.ag.a((Context)localObject1, (ab[])localObject2, localb);
      localObject2 = "TruecallerDatabaseHelper…ableHelpers(), analytics)";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject1 = ((com.truecaller.content.c.ag)localObject1).getWritableDatabase();
      localObject2 = "PRAGMA wal_checkpoint(FULL)";
      localb = null;
      localObject1 = ((SQLiteDatabase)localObject1).rawQuery((String)localObject2, null);
      localObject2 = "cursor";
      c.g.b.k.a(localObject1, (String)localObject2);
      ((Cursor)localObject1).getCount();
      localObject1 = (Closeable)localObject1;
      d.a((Closeable)localObject1);
    }
    catch (Exception localException)
    {
      Object localObject1 = (Throwable)localException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
    }
    return d.getDatabasePath("tc.db");
  }
  
  public final Object a(c paramc)
  {
    boolean bool1 = paramc instanceof ah.a;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (ah.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/backup/ah$a;
    ((ah.a)localObject1).<init>(this, (c)paramc);
    label70:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    int j = b;
    switch (j)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 1: 
      bool1 = paramc instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label250;
      }
      paramc = a;
      Object localObject2 = "key_backup_fetched_timestamp";
      boolean bool3 = paramc.e((String)localObject2);
      if (bool3) {
        return x.a;
      }
      new String[1][0] = "Fetching timestamp for backup...";
      paramc = c;
      localObject2 = new com/truecaller/backup/ah$b;
      ((ah.b)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      d = this;
      int m = 1;
      b = m;
      paramc = g.a(paramc, (m)localObject2, (c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      break;
    }
    return x.a;
    label250:
    throw a;
  }
  
  public final String a(BackupFile paramBackupFile)
  {
    c.g.b.k.b(paramBackupFile, "backupFile");
    Object localObject1 = a;
    Object localObject2 = "profileNumber";
    localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject2);
    if (localObject1 == null) {
      return null;
    }
    c.g.b.k.a(localObject1, "coreSettings.getString(C…LE_NUMBER) ?: return null");
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject1 = (CharSequence)localObject1;
    localObject1 = b.a((CharSequence)localObject1, "");
    ((StringBuilder)localObject2).append((String)localObject1);
    paramBackupFile = paramBackupFile.getNameSuffix();
    ((StringBuilder)localObject2).append(paramBackupFile);
    return ((StringBuilder)localObject2).toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */