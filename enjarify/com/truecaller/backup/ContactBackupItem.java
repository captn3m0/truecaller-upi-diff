package com.truecaller.backup;

import c.g.b.k;
import java.util.Comparator;
import java.util.Set;

public final class ContactBackupItem
  implements Comparable
{
  private final Set addresses;
  private final String avatarUrl;
  private final Set emails;
  private final String name;
  private final Set numbers;
  private final int source;
  
  public ContactBackupItem(String paramString1, int paramInt, Set paramSet1, Set paramSet2, Set paramSet3, String paramString2)
  {
    name = paramString1;
    source = paramInt;
    numbers = paramSet1;
    emails = paramSet2;
    addresses = paramSet3;
    avatarUrl = paramString2;
  }
  
  public final int compareTo(ContactBackupItem paramContactBackupItem)
  {
    k.b(paramContactBackupItem, "other");
    Object localObject = new com/truecaller/backup/ContactBackupItem$a;
    ((ContactBackupItem.a)localObject).<init>();
    localObject = (Comparator)localObject;
    ContactBackupItem localContactBackupItem = this;
    localContactBackupItem = (ContactBackupItem)this;
    return ((Comparator)localObject).compare(localContactBackupItem, paramContactBackupItem);
  }
  
  public final String component1()
  {
    return name;
  }
  
  public final int component2()
  {
    return source;
  }
  
  public final Set component3()
  {
    return numbers;
  }
  
  public final Set component4()
  {
    return emails;
  }
  
  public final Set component5()
  {
    return addresses;
  }
  
  public final String component6()
  {
    return avatarUrl;
  }
  
  public final ContactBackupItem copy(String paramString1, int paramInt, Set paramSet1, Set paramSet2, Set paramSet3, String paramString2)
  {
    k.b(paramString1, "name");
    ContactBackupItem localContactBackupItem = new com/truecaller/backup/ContactBackupItem;
    localContactBackupItem.<init>(paramString1, paramInt, paramSet1, paramSet2, paramSet3, paramString2);
    return localContactBackupItem;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ContactBackupItem;
      if (bool2)
      {
        paramObject = (ContactBackupItem)paramObject;
        Object localObject1 = name;
        Object localObject2 = name;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          int i = source;
          int j = source;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            localObject1 = null;
          }
          if (i != 0)
          {
            localObject1 = numbers;
            localObject2 = numbers;
            boolean bool3 = k.a(localObject1, localObject2);
            if (bool3)
            {
              localObject1 = emails;
              localObject2 = emails;
              bool3 = k.a(localObject1, localObject2);
              if (bool3)
              {
                localObject1 = addresses;
                localObject2 = addresses;
                bool3 = k.a(localObject1, localObject2);
                if (bool3)
                {
                  localObject1 = avatarUrl;
                  paramObject = avatarUrl;
                  boolean bool4 = k.a(localObject1, paramObject);
                  if (bool4) {
                    return bool1;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final Set getAddresses()
  {
    return addresses;
  }
  
  public final String getAvatarUrl()
  {
    return avatarUrl;
  }
  
  public final Set getEmails()
  {
    return emails;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final Set getNumbers()
  {
    return numbers;
  }
  
  public final int getSource()
  {
    return source;
  }
  
  public final int hashCode()
  {
    String str = name;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    int k = source;
    int j = (j + k) * 31;
    Object localObject = numbers;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = emails;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = addresses;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = avatarUrl;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ContactBackupItem(name=");
    Object localObject = name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", source=");
    int i = source;
    localStringBuilder.append(i);
    localStringBuilder.append(", numbers=");
    localObject = numbers;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", emails=");
    localObject = emails;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", addresses=");
    localObject = addresses;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", avatarUrl=");
    localObject = avatarUrl;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ContactBackupItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */