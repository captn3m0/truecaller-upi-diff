package com.truecaller.backup;

import dagger.a.d;
import javax.inject.Provider;

public final class cb
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private cb(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static cb a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    cb localcb = new com/truecaller/backup/cb;
    localcb.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localcb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.cb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */