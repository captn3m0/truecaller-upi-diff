package com.truecaller.backup;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class BackupTransportInfo$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    BackupTransportInfo localBackupTransportInfo = new com/truecaller/backup/BackupTransportInfo;
    long l = paramParcel.readLong();
    localBackupTransportInfo.<init>(l);
    return localBackupTransportInfo;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new BackupTransportInfo[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupTransportInfo.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */