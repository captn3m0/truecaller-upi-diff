package com.truecaller.backup;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import c.a.m;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.ah;
import java.util.ArrayList;

public final class ay
  implements ax
{
  private final String a;
  private final Context b;
  
  public ay(Context paramContext)
  {
    b = paramContext;
    a = "(contact_source & 2) = 2";
  }
  
  public final void a()
  {
    Object localObject1 = ContentProviderOperation.newUpdate(TruecallerContract.ah.a());
    int i = 1;
    Object localObject2 = Integer.valueOf(i);
    localObject1 = ((ContentProviderOperation.Builder)localObject1).withValue("tc_flag", localObject2).build();
    Object localObject3 = ContentProviderOperation.newUpdate(TruecallerContract.a.a());
    Object localObject4 = Integer.valueOf(i);
    localObject3 = ((ContentProviderOperation.Builder)localObject3).withValue("tc_flag", localObject4).build();
    localObject2 = b.getContentResolver();
    localObject4 = TruecallerContract.a();
    ContentProviderOperation[] arrayOfContentProviderOperation = new ContentProviderOperation[2];
    arrayOfContentProviderOperation[0] = localObject1;
    arrayOfContentProviderOperation[i] = localObject3;
    localObject1 = m.d(arrayOfContentProviderOperation);
    ((ContentResolver)localObject2).applyBatch((String)localObject4, (ArrayList)localObject1);
  }
  
  public final void b()
  {
    Object localObject1 = ContentProviderOperation.newUpdate(TruecallerContract.ah.a());
    int i = 2;
    Object localObject2 = Integer.valueOf(i);
    localObject1 = ((ContentProviderOperation.Builder)localObject1).withValue("tc_flag", localObject2).build();
    Object localObject3 = ContentProviderOperation.newUpdate(TruecallerContract.a.a());
    Object localObject4 = Integer.valueOf(i);
    localObject3 = ((ContentProviderOperation.Builder)localObject3).withValue("tc_flag", localObject4).build();
    localObject2 = new android/content/ContentValues;
    ((ContentValues)localObject2).<init>();
    Object localObject5 = Integer.valueOf(32);
    ((ContentValues)localObject2).put("contact_source", (Integer)localObject5);
    ((ContentValues)localObject2).putNull("contact_phonebook_id");
    ((ContentValues)localObject2).putNull("contact_phonebook_hash");
    ((ContentValues)localObject2).putNull("contact_phonebook_lookup");
    localObject4 = ContentProviderOperation.newUpdate(TruecallerContract.ah.a());
    localObject5 = a;
    localObject4 = ((ContentProviderOperation.Builder)localObject4).withSelection((String)localObject5, null).withValues((ContentValues)localObject2).build();
    localObject5 = ContentProviderOperation.newUpdate(TruecallerContract.a.a());
    Object localObject6 = a;
    localObject2 = ((ContentProviderOperation.Builder)localObject5).withSelection((String)localObject6, null).withValues((ContentValues)localObject2).build();
    localObject5 = b.getContentResolver();
    String str = TruecallerContract.a();
    localObject6 = new ContentProviderOperation[4];
    localObject6[0] = localObject1;
    localObject6[1] = localObject3;
    localObject6[i] = localObject4;
    localObject6[3] = localObject2;
    localObject1 = m.d((Object[])localObject6);
    ((ContentResolver)localObject5).applyBatch(str, (ArrayList)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ay
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */