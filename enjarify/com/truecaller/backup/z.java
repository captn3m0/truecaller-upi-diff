package com.truecaller.backup;

import android.content.Context;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.common.h.ac;
import com.truecaller.filters.p;
import com.truecaller.utils.d;
import com.truecaller.utils.l;
import java.util.LinkedHashMap;
import java.util.Map;

public final class z
{
  final Map b;
  final c.d.f c;
  private final c.f d;
  private final c.f e;
  private final c.f f;
  private final c.f g;
  private final c.f h;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[5];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(z.class);
    ((u)localObject).<init>(localb, "notificationPermissionIsGranted", "getNotificationPermissionIsGranted()Z");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(z.class);
    ((u)localObject).<init>(localb, "hasSimSlot1Info", "getHasSimSlot1Info()Z");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(z.class);
    ((u)localObject).<init>(localb, "hasSimSlot2Info", "getHasSimSlot2Info()Z");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(z.class);
    ((u)localObject).<init>(localb, "hasSmsPermission", "getHasSmsPermission()Z");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[3] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(z.class);
    ((u)localObject).<init>(localb, "doNotDisturbPermissionIsGranted", "getDoNotDisturbPermissionIsGranted()Z");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[4] = localObject;
    a = arrayOfg;
  }
  
  public z(c.d.f paramf, Context paramContext, com.google.gson.f paramf1, com.truecaller.common.background.b paramb, d paramd, com.truecaller.common.g.a parama, ac paramac, com.truecaller.i.c paramc, p paramp, com.truecaller.messaging.h paramh, com.truecaller.multisim.h paramh1, l paraml, com.truecaller.common.profile.e parame, com.truecaller.common.h.c paramc1, com.truecaller.common.e.e parame1)
  {
    c = paramf;
    localObject1 = new com/truecaller/backup/z$e;
    ((z.e)localObject1).<init>(paraml);
    localObject1 = c.g.a((c.g.a.a)localObject1);
    d = ((c.f)localObject1);
    localObject1 = new com/truecaller/backup/z$b;
    ((z.b)localObject1).<init>(paramh1);
    localObject1 = c.g.a((c.g.a.a)localObject1);
    e = ((c.f)localObject1);
    localObject1 = new com/truecaller/backup/z$c;
    ((z.c)localObject1).<init>(paramh1);
    localObject1 = c.g.a((c.g.a.a)localObject1);
    f = ((c.f)localObject1);
    localObject1 = new com/truecaller/backup/z$d;
    ((z.d)localObject1).<init>(paramd, paraml);
    localObject1 = c.g.a((c.g.a.a)localObject1);
    g = ((c.f)localObject1);
    localObject1 = new com/truecaller/backup/z$a;
    ((z.a)localObject1).<init>(paraml);
    localObject1 = c.g.a((c.g.a.a)localObject1);
    h = ((c.f)localObject1);
    localObject1 = new t[45];
    localObject3 = new com/truecaller/backup/z$f;
    ((z.f)localObject3).<init>(parama, "availability_enabled", parama);
    localObject3 = (t)localObject3;
    localObject1[0] = localObject3;
    localObject3 = new com/truecaller/backup/z$q;
    ((z.q)localObject3).<init>(parama, "flash_enabled", parama);
    localObject3 = (t)localObject3;
    localObject1[1] = localObject3;
    localObject3 = new com/truecaller/backup/ce;
    ((ce)localObject3).<init>("callLogTapBehavior");
    localObject3 = (t)localObject3;
    localObject1[2] = localObject3;
    localObject3 = new com/truecaller/backup/cd;
    ((cd)localObject3).<init>("profileAcceptAuto", parama);
    localObject3 = (t)localObject3;
    localObject1[3] = localObject3;
    localObject3 = new com/truecaller/backup/z$v;
    ((z.v)localObject3).<init>(parame, parama, "profileAcceptAuto", parama);
    localObject3 = (t)localObject3;
    localObject1[4] = localObject3;
    localObject3 = new com/truecaller/backup/al;
    ((al)localObject3).<init>("clipboardSearchEnabled");
    localObject3 = (t)localObject3;
    localObject1[5] = localObject3;
    localObject3 = new com/truecaller/backup/al;
    ((al)localObject3).<init>("enhancedNotificationsEnabled");
    localObject3 = (t)localObject3;
    localObject1[6] = localObject3;
    localObject3 = new com/truecaller/backup/z$w;
    ((z.w)localObject3).<init>(this, "enhancedNotificationsEnabled");
    localObject3 = (t)localObject3;
    localObject1[7] = localObject3;
    localObject3 = new com/truecaller/backup/al;
    ((al)localObject3).<init>("alwaysDownloadImages");
    localObject3 = (t)localObject3;
    localObject1[8] = localObject3;
    localObject3 = new com/truecaller/backup/ce;
    ((ce)localObject3).<init>("dialpad_feedback_index_str");
    localObject3 = (t)localObject3;
    localObject1[9] = localObject3;
    localObject3 = new com/truecaller/backup/z$x;
    ((z.x)localObject3).<init>(this, paramc, "showMissedCallsNotifications", paramc);
    localObject3 = (t)localObject3;
    localObject1[10] = localObject3;
    localObject3 = new com/truecaller/backup/al;
    ((al)localObject3).<init>("showMissedCallReminders");
    localObject3 = (t)localObject3;
    localObject1[11] = localObject3;
    localObject3 = new com/truecaller/backup/z$y;
    ((z.y)localObject3).<init>("t9_lang");
    localObject3 = (t)localObject3;
    localObject1[12] = localObject3;
    localObject3 = new com/truecaller/backup/aj;
    ((aj)localObject3).<init>("enabledCallerIDforPB", paramc);
    localObject3 = (t)localObject3;
    localObject1[13] = localObject3;
    localObject3 = new com/truecaller/backup/aj;
    ((aj)localObject3).<init>("afterCall", paramc);
    localObject3 = (t)localObject3;
    localObject1[14] = localObject3;
    localObject3 = new com/truecaller/backup/cc;
    ((cc)localObject3).<init>("speed_dial_2", paramc);
    localObject3 = (t)localObject3;
    localObject1[15] = localObject3;
    localObject3 = new com/truecaller/backup/cc;
    ((cc)localObject3).<init>("speed_dial_3", paramc);
    localObject3 = (t)localObject3;
    localObject1[16] = localObject3;
    localObject3 = new com/truecaller/backup/cc;
    ((cc)localObject3).<init>("speed_dial_4", paramc);
    localObject3 = (t)localObject3;
    localObject1[17] = localObject3;
    localObject3 = new com/truecaller/backup/cc;
    ((cc)localObject3).<init>("speed_dial_5", paramc);
    localObject3 = (t)localObject3;
    localObject1[18] = localObject3;
    localObject3 = new com/truecaller/backup/cc;
    ((cc)localObject3).<init>("speed_dial_6", paramc);
    localObject3 = (t)localObject3;
    localObject1[19] = localObject3;
    localObject3 = new com/truecaller/backup/cc;
    ((cc)localObject3).<init>("speed_dial_7", paramc);
    localObject3 = (t)localObject3;
    localObject1[20] = localObject3;
    localObject3 = new com/truecaller/backup/cc;
    ((cc)localObject3).<init>("speed_dial_8", paramc);
    localObject3 = (t)localObject3;
    localObject1[21] = localObject3;
    localObject3 = new com/truecaller/backup/cc;
    ((cc)localObject3).<init>("speed_dial_9", paramc);
    localObject3 = (t)localObject3;
    localObject1[22] = localObject3;
    localObject3 = new com/truecaller/backup/z$z;
    ((z.z)localObject3).<init>(paramp);
    localObject3 = (t)localObject3;
    localObject1[23] = localObject3;
    localObject3 = new com/truecaller/backup/z$aa;
    ((z.aa)localObject3).<init>(paramp);
    localObject3 = (t)localObject3;
    localObject1[24] = localObject3;
    localObject3 = new com/truecaller/backup/z$ab;
    ((z.ab)localObject3).<init>(this, paramc, "blockCallMethod", paramc);
    localObject3 = (t)localObject3;
    localObject1[25] = localObject3;
    localObject3 = new com/truecaller/backup/aj;
    ((aj)localObject3).<init>("blockCallNotification", paramc);
    localObject3 = (t)localObject3;
    localObject1[26] = localObject3;
    localObject3 = new com/truecaller/backup/z$g;
    ((z.g)localObject3).<init>(paramh);
    localObject3 = (t)localObject3;
    localObject1[27] = localObject3;
    localObject3 = new com/truecaller/backup/z$h;
    ((z.h)localObject3).<init>(paramc1, paramac, "backup");
    localObject3 = (t)localObject3;
    localObject1[28] = localObject3;
    localObject3 = new com/truecaller/backup/z$i;
    ((z.i)localObject3).<init>(this, paramh);
    localObject3 = (t)localObject3;
    localObject1[29] = localObject3;
    localObject3 = new com/truecaller/backup/z$j;
    ((z.j)localObject3).<init>(this, paramh);
    localObject3 = (t)localObject3;
    localObject1[30] = localObject3;
    localObject3 = new com/truecaller/backup/z$k;
    ((z.k)localObject3).<init>(this, paramh);
    localObject3 = (t)localObject3;
    localObject1[31] = localObject3;
    localObject3 = new com/truecaller/backup/z$l;
    ((z.l)localObject3).<init>(this, paramh);
    localObject3 = (t)localObject3;
    localObject1[32] = localObject3;
    localObject3 = new com/truecaller/backup/z$m;
    ((z.m)localObject3).<init>(this, paramh);
    localObject3 = (t)localObject3;
    localObject1[33] = localObject3;
    localObject3 = new com/truecaller/backup/z$n;
    ((z.n)localObject3).<init>(this, paramh);
    localObject3 = (t)localObject3;
    localObject1[34] = localObject3;
    localObject3 = new com/truecaller/backup/z$o;
    ((z.o)localObject3).<init>(this, paramh);
    localObject3 = (t)localObject3;
    localObject1[35] = localObject3;
    localObject3 = new com/truecaller/backup/z$p;
    ((z.p)localObject3).<init>(this, paramh);
    localObject3 = (t)localObject3;
    localObject1[36] = localObject3;
    localObject3 = new com/truecaller/backup/z$r;
    ((z.r)localObject3).<init>();
    localObject3 = (t)localObject3;
    localObject1[37] = localObject3;
    localObject3 = new com/truecaller/backup/bh;
    ((bh)localObject3).<init>("merge_by");
    localObject3 = (t)localObject3;
    localObject1[38] = localObject3;
    localObject3 = new com/truecaller/backup/al;
    ((al)localObject3).<init>("showFrequentlyCalledContacts");
    localObject3 = (t)localObject3;
    localObject1[39] = localObject3;
    localObject3 = new com/truecaller/backup/z$s;
    ((z.s)localObject3).<init>(this, paramf1, paramContext, parame1);
    localObject3 = (t)localObject3;
    localObject1[40] = localObject3;
    localObject2 = new com/truecaller/backup/bi;
    ((bi)localObject2).<init>("key_backup_frequency_hours", parama);
    localObject2 = (t)localObject2;
    localObject1[41] = localObject2;
    localObject2 = new com/truecaller/backup/z$t;
    ((z.t)localObject2).<init>(paramb, parama, "backup_enabled", parama);
    localObject2 = (t)localObject2;
    localObject1[42] = localObject2;
    localObject2 = new com/truecaller/backup/aj;
    ((aj)localObject2).<init>("madeCallsFromCallLog", paramc);
    localObject2 = (t)localObject2;
    localObject1[43] = localObject2;
    localObject2 = new com/truecaller/backup/z$u;
    ((z.u)localObject2).<init>(this, paramc, "whatsAppCallsEnabled", paramc);
    localObject2 = (t)localObject2;
    localObject1[44] = localObject2;
    localObject1 = a((t[])localObject1);
    b = ((Map)localObject1);
  }
  
  private static Map a(t... paramVarArgs)
  {
    Object localObject = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject).<init>();
    localObject = (Map)localObject;
    int i = 0;
    for (;;)
    {
      int j = 45;
      if (i >= j) {
        break;
      }
      t localt = paramVarArgs[i];
      String str = localt.a();
      ((Map)localObject).put(str, localt);
      i += 1;
    }
    return (Map)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */