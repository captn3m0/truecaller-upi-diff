package com.truecaller.backup;

import android.content.Intent;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.a;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.a.a;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.messaging.transport.l.b;
import com.truecaller.utils.q;
import java.util.List;
import java.util.Set;
import org.a.a.a.g;

public abstract class af
  implements l
{
  private final ad.b a;
  private final h b;
  private final e c;
  
  public af(ad.b paramb, h paramh, e parame)
  {
    a = paramb;
    b = paramh;
    c = parame;
  }
  
  public final long a(long paramLong)
  {
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("Backup transport can not be used to store a message.");
    throw ((Throwable)localIllegalStateException);
  }
  
  public final long a(f paramf, i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set paramSet)
  {
    String str = "threadInfoCache";
    c.g.b.k.b(paramf, str);
    c.g.b.k.b(parami, "participantCache");
    c.g.b.k.b(paramr, "localCursor");
    c.g.b.k.b(paramb1, "timeTo");
    c.g.b.k.b(paramb2, "timeFrom");
    c.g.b.k.b(paramList, "operations");
    c.g.b.k.b(paramq, "trace");
    c.g.b.k.b(paramSet, "messagesToClassify");
    paramf = c.w();
    boolean bool = paramf.a();
    if (bool)
    {
      int i;
      do
      {
        bool = paramr.moveToNext();
        if (!bool) {
          break;
        }
        i = paramr.o();
        if (i == 0)
        {
          long l = paramr.a();
          paramf = Long.valueOf(l);
          paramSet.add(paramf);
        }
        i = paramSet.size();
      } while (i < paramInt);
      return l.b.a(paramr.e());
    }
    return Long.MIN_VALUE;
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramArrayOfParticipant, "recipients");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("Backup transport can not be used to enqueue a message.");
    throw ((Throwable)paramMessage);
  }
  
  public final String a()
  {
    return "backup";
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    return "-1";
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    c.g.b.k.b(paramIntent, "intent");
    paramIntent = new java/lang/IllegalStateException;
    paramIntent.<init>("Backup transport can not be used to handle received messages.");
    throw ((Throwable)paramIntent);
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramString2, "initiatedVia");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("Backup transport does not support sending reactions");
    throw ((Throwable)paramMessage);
  }
  
  public final void a(org.a.a.b paramb)
  {
    c.g.b.k.b(paramb, "time");
    h localh = b;
    long l = a;
    localh.a(4, l);
  }
  
  public final boolean a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("Backup transport can not be used to store a message.");
    throw ((Throwable)paramMessage);
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramEntity, "entity");
    return false;
  }
  
  public final boolean a(Message paramMessage, ad paramad)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramad, "transaction");
    paramMessage = TruecallerContract.aa.a(paramMessage.a());
    paramMessage = paramad.a(paramMessage);
    Integer localInteger = Integer.valueOf(9);
    paramMessage = paramMessage.a("status", localInteger).a();
    paramad.a(paramMessage);
    return true;
  }
  
  public final boolean a(Participant paramParticipant)
  {
    c.g.b.k.b(paramParticipant, "participant");
    return false;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, long paramLong1, long paramLong2, ad paramad)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    paramTransportInfo = TruecallerContract.aa.a(paramTransportInfo.c());
    paramTransportInfo = paramad.a(paramTransportInfo);
    int i = 1;
    Integer localInteger = Integer.valueOf(i);
    paramTransportInfo = paramTransportInfo.a("read", localInteger);
    localInteger = Integer.valueOf(i);
    paramTransportInfo = paramTransportInfo.a("seen", localInteger).a();
    paramad.a(paramTransportInfo);
    return i;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, ad paramad)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    paramTransportInfo = TruecallerContract.aa.a(paramTransportInfo.c());
    paramTransportInfo = paramad.a(paramTransportInfo);
    int i = 1;
    Integer localInteger = Integer.valueOf(i);
    paramTransportInfo = paramTransportInfo.a("seen", localInteger).a();
    paramad.a(paramTransportInfo);
    return i;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, ad paramad, boolean paramBoolean)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    paramTransportInfo = TruecallerContract.aa.a(paramTransportInfo.c());
    paramTransportInfo = paramad.b(paramTransportInfo).a();
    paramad.a(paramTransportInfo);
    return true;
  }
  
  public final boolean a(ad paramad)
  {
    c.g.b.k.b(paramad, "transaction");
    boolean bool1 = false;
    try
    {
      boolean bool2 = paramad.a();
      if (bool2) {
        return false;
      }
      Object localObject = a;
      paramad = ((ad.b)localObject).a(paramad);
      localObject = "transactionExecutor.execute(transaction)";
      c.g.b.k.a(paramad, (String)localObject);
      int i = paramad.length;
      bool2 = true;
      if (i == 0) {
        bool1 = true;
      }
      bool1 ^= bool2;
    }
    catch (Exception localException)
    {
      paramad = (Throwable)localException;
      AssertionUtil.reportThrowableButNeverCrash(paramad);
    }
    return bool1;
  }
  
  public final boolean a(String paramString, a parama)
  {
    c.g.b.k.b(paramString, "text");
    c.g.b.k.b(parama, "result");
    return false;
  }
  
  public final ad b()
  {
    ad localad = new com/truecaller/messaging/transport/ad;
    String str = TruecallerContract.a();
    localad.<init>(str);
    return localad;
  }
  
  public final void b(long paramLong)
  {
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("Backup transport can not retry messages.");
    throw ((Throwable)localIllegalStateException);
  }
  
  public final boolean b(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final boolean b(ad paramad)
  {
    String str = "transaction";
    c.g.b.k.b(paramad, str);
    boolean bool1 = paramad.a();
    if (!bool1)
    {
      paramad = paramad.c();
      str = TruecallerContract.a();
      boolean bool2 = c.g.b.k.a(paramad, str);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final boolean c(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("Backup transport can not be used to send a message.");
    throw ((Throwable)paramMessage);
  }
  
  public final org.a.a.b d()
  {
    org.a.a.b localb = new org/a/a/b;
    long l = b.a(4);
    localb.<init>(l);
    return localb;
  }
  
  public final int e(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return 0;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final boolean f(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */