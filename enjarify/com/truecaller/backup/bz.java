package com.truecaller.backup;

import dagger.a.d;
import javax.inject.Provider;

public final class bz
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private bz(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
  }
  
  public static bz a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    bz localbz = new com/truecaller/backup/bz;
    localbz.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localbz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bz
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */