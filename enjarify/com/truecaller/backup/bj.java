package com.truecaller.backup;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;

final class bj
  implements Cursor
{
  final com.truecaller.utils.extensions.g b;
  private final com.truecaller.utils.extensions.g c;
  private final com.truecaller.utils.extensions.g d;
  private final com.truecaller.utils.extensions.g e;
  private final Cursor f;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[4];
    Object localObject = new c/g/b/u;
    b localb = w.a(bj.class);
    ((u)localObject).<init>(localb, "id", "getId()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bj.class);
    ((u)localObject).<init>(localb, "date", "getDate()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bj.class);
    ((u)localObject).<init>(localb, "participantId", "getParticipantId()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bj.class);
    ((u)localObject).<init>(localb, "status", "getStatus()I");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[3] = localObject;
    a = arrayOfg;
  }
  
  public bj(Cursor paramCursor)
  {
    f = paramCursor;
    long l = 0L;
    Object localObject1 = Long.valueOf(l);
    com.truecaller.utils.extensions.g localg1 = new com/truecaller/utils/extensions/g;
    b localb = w.a(Long.class);
    localg1.<init>("_id", localb, localObject1);
    c = localg1;
    localObject1 = Long.valueOf(l);
    localg1 = new com/truecaller/utils/extensions/g;
    localb = w.a(Long.class);
    localg1.<init>("date", localb, localObject1);
    d = localg1;
    Object localObject2 = Long.valueOf(l);
    com.truecaller.utils.extensions.g localg2 = new com/truecaller/utils/extensions/g;
    localObject1 = w.a(Long.class);
    localg2.<init>("participant_id", (b)localObject1, localObject2);
    e = localg2;
    localObject2 = Integer.valueOf(0);
    localg2 = new com/truecaller/utils/extensions/g;
    localObject1 = w.a(Integer.class);
    localg2.<init>("status", (b)localObject1, localObject2);
    b = localg2;
  }
  
  public final long a()
  {
    com.truecaller.utils.extensions.g localg = c;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[0];
    return ((Number)localg.a((Cursor)localObject, localg1)).longValue();
  }
  
  public final long b()
  {
    com.truecaller.utils.extensions.g localg = d;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[1];
    return ((Number)localg.a((Cursor)localObject, localg1)).longValue();
  }
  
  public final long c()
  {
    com.truecaller.utils.extensions.g localg = e;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[2];
    return ((Number)localg.a((Cursor)localObject, localg1)).longValue();
  }
  
  public final void close()
  {
    f.close();
  }
  
  public final void copyStringToBuffer(int paramInt, CharArrayBuffer paramCharArrayBuffer)
  {
    f.copyStringToBuffer(paramInt, paramCharArrayBuffer);
  }
  
  public final void deactivate()
  {
    f.deactivate();
  }
  
  public final byte[] getBlob(int paramInt)
  {
    return f.getBlob(paramInt);
  }
  
  public final int getColumnCount()
  {
    return f.getColumnCount();
  }
  
  public final int getColumnIndex(String paramString)
  {
    return f.getColumnIndex(paramString);
  }
  
  public final int getColumnIndexOrThrow(String paramString)
  {
    return f.getColumnIndexOrThrow(paramString);
  }
  
  public final String getColumnName(int paramInt)
  {
    return f.getColumnName(paramInt);
  }
  
  public final String[] getColumnNames()
  {
    return f.getColumnNames();
  }
  
  public final int getCount()
  {
    return f.getCount();
  }
  
  public final double getDouble(int paramInt)
  {
    return f.getDouble(paramInt);
  }
  
  public final Bundle getExtras()
  {
    return f.getExtras();
  }
  
  public final float getFloat(int paramInt)
  {
    return f.getFloat(paramInt);
  }
  
  public final int getInt(int paramInt)
  {
    return f.getInt(paramInt);
  }
  
  public final long getLong(int paramInt)
  {
    return f.getLong(paramInt);
  }
  
  public final Uri getNotificationUri()
  {
    return f.getNotificationUri();
  }
  
  public final int getPosition()
  {
    return f.getPosition();
  }
  
  public final short getShort(int paramInt)
  {
    return f.getShort(paramInt);
  }
  
  public final String getString(int paramInt)
  {
    return f.getString(paramInt);
  }
  
  public final int getType(int paramInt)
  {
    return f.getType(paramInt);
  }
  
  public final boolean getWantsAllOnMoveCalls()
  {
    return f.getWantsAllOnMoveCalls();
  }
  
  public final boolean isAfterLast()
  {
    return f.isAfterLast();
  }
  
  public final boolean isBeforeFirst()
  {
    return f.isBeforeFirst();
  }
  
  public final boolean isClosed()
  {
    return f.isClosed();
  }
  
  public final boolean isFirst()
  {
    return f.isFirst();
  }
  
  public final boolean isLast()
  {
    return f.isLast();
  }
  
  public final boolean isNull(int paramInt)
  {
    return f.isNull(paramInt);
  }
  
  public final boolean move(int paramInt)
  {
    return f.move(paramInt);
  }
  
  public final boolean moveToFirst()
  {
    return f.moveToFirst();
  }
  
  public final boolean moveToLast()
  {
    return f.moveToLast();
  }
  
  public final boolean moveToNext()
  {
    return f.moveToNext();
  }
  
  public final boolean moveToPosition(int paramInt)
  {
    return f.moveToPosition(paramInt);
  }
  
  public final boolean moveToPrevious()
  {
    return f.moveToPrevious();
  }
  
  public final void registerContentObserver(ContentObserver paramContentObserver)
  {
    f.registerContentObserver(paramContentObserver);
  }
  
  public final void registerDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    f.registerDataSetObserver(paramDataSetObserver);
  }
  
  public final boolean requery()
  {
    return f.requery();
  }
  
  public final Bundle respond(Bundle paramBundle)
  {
    return f.respond(paramBundle);
  }
  
  public final void setExtras(Bundle paramBundle)
  {
    f.setExtras(paramBundle);
  }
  
  public final void setNotificationUri(ContentResolver paramContentResolver, Uri paramUri)
  {
    f.setNotificationUri(paramContentResolver, paramUri);
  }
  
  public final void unregisterContentObserver(ContentObserver paramContentObserver)
  {
    f.unregisterContentObserver(paramContentObserver);
  }
  
  public final void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    f.unregisterDataSetObserver(paramDataSetObserver);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */