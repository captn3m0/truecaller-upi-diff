package com.truecaller.backup;

import android.support.v4.app.Fragment;
import c.d.c;
import c.g.a.m;
import c.o.b;
import kotlinx.coroutines.ag;

final class x$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  x$d(x paramx, String paramString, Fragment paramFragment, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/backup/x$d;
    x localx = b;
    String str = c;
    Fragment localFragment = d;
    locald.<init>(localx, str, localFragment, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label205;
      }
      paramObject = c;
      String str = x.h(b);
      bool2 = c.g.b.k.a(paramObject, str);
      j = 1;
      bool2 ^= j;
      if (bool2) {
        return c.x.a;
      }
      paramObject = x.c(b);
      Fragment localFragment = d;
      a = j;
      paramObject = ((e)paramObject).b(localFragment, this);
      if (paramObject == localObject) {
        return localObject;
      }
      break;
    }
    paramObject = (Boolean)paramObject;
    boolean bool2 = ((Boolean)paramObject).booleanValue();
    if (bool2)
    {
      x.a(b).d("key_backup_last_success");
      paramObject = b;
      localObject = "backupAccountChange";
      x.a((x)paramObject, (String)localObject);
    }
    x.f(b);
    return c.x.a;
    label205:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = c.x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.x.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */