package com.truecaller.backup;

import c.g.b.k;

public final class BackupSettingItem
{
  private final String key;
  private final Object value;
  
  public BackupSettingItem(String paramString, Object paramObject)
  {
    key = paramString;
    value = paramObject;
  }
  
  public final String component1()
  {
    return key;
  }
  
  public final Object component2()
  {
    return value;
  }
  
  public final BackupSettingItem copy(String paramString, Object paramObject)
  {
    k.b(paramString, "key");
    BackupSettingItem localBackupSettingItem = new com/truecaller/backup/BackupSettingItem;
    localBackupSettingItem.<init>(paramString, paramObject);
    return localBackupSettingItem;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BackupSettingItem;
      if (bool1)
      {
        paramObject = (BackupSettingItem)paramObject;
        Object localObject = key;
        String str = key;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = value;
          paramObject = value;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getKey()
  {
    return key;
  }
  
  public final Object getValue()
  {
    return value;
  }
  
  public final int hashCode()
  {
    String str = key;
    int i = 0;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = value;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BackupSettingItem(key=");
    Object localObject = key;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", value=");
    localObject = value;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupSettingItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */