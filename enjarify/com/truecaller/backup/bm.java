package com.truecaller.backup;

import dagger.a.d;
import javax.inject.Provider;

public final class bm
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private bm(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static bm a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    bm localbm = new com/truecaller/backup/bm;
    localbm.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localbm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bm
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */