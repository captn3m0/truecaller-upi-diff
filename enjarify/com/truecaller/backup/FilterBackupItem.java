package com.truecaller.backup;

import c.g.b.k;
import c.u;

public final class FilterBackupItem
{
  private final int entityType;
  private final String label;
  private final int rule;
  private final String serverId;
  private final int syncState;
  private final String trackingSource;
  private final String trackingType;
  private final String value;
  private final int wildcardType;
  
  public FilterBackupItem(String paramString1, String paramString2, String paramString3, int paramInt1, int paramInt2, int paramInt3, String paramString4, String paramString5, int paramInt4)
  {
    serverId = paramString1;
    value = paramString2;
    label = paramString3;
    rule = paramInt1;
    wildcardType = paramInt2;
    syncState = paramInt3;
    trackingType = paramString4;
    trackingSource = paramString5;
    entityType = paramInt4;
  }
  
  public final String component1()
  {
    return serverId;
  }
  
  public final String component2()
  {
    return value;
  }
  
  public final String component3()
  {
    return label;
  }
  
  public final int component4()
  {
    return rule;
  }
  
  public final int component5()
  {
    return wildcardType;
  }
  
  public final int component6()
  {
    return syncState;
  }
  
  public final String component7()
  {
    return trackingType;
  }
  
  public final String component8()
  {
    return trackingSource;
  }
  
  public final int component9()
  {
    return entityType;
  }
  
  public final FilterBackupItem copy(String paramString1, String paramString2, String paramString3, int paramInt1, int paramInt2, int paramInt3, String paramString4, String paramString5, int paramInt4)
  {
    k.b(paramString2, "value");
    FilterBackupItem localFilterBackupItem = new com/truecaller/backup/FilterBackupItem;
    localFilterBackupItem.<init>(paramString1, paramString2, paramString3, paramInt1, paramInt2, paramInt3, paramString4, paramString5, paramInt4);
    return localFilterBackupItem;
  }
  
  public final boolean equals(Object paramObject)
  {
    Object localObject = this;
    localObject = (FilterBackupItem)this;
    boolean bool1 = true;
    if (localObject == paramObject) {
      return bool1;
    }
    localObject = getClass();
    if (paramObject != null) {
      localClass = paramObject.getClass();
    } else {
      localClass = null;
    }
    boolean bool2 = k.a(localObject, localClass) ^ bool1;
    Class localClass = null;
    if (bool2) {
      return false;
    }
    if (paramObject != null)
    {
      localObject = value;
      paramObject = (FilterBackupItem)paramObject;
      String str = value;
      bool2 = k.a(localObject, str) ^ bool1;
      if (bool2) {
        return false;
      }
      int i = rule;
      int j = rule;
      if (i != j) {
        return false;
      }
      i = wildcardType;
      int k = wildcardType;
      if (i != k) {
        return false;
      }
      return bool1;
    }
    paramObject = new c/u;
    ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.backup.FilterBackupItem");
    throw ((Throwable)paramObject);
  }
  
  public final int getEntityType()
  {
    return entityType;
  }
  
  public final String getLabel()
  {
    return label;
  }
  
  public final int getRule()
  {
    return rule;
  }
  
  public final String getServerId()
  {
    return serverId;
  }
  
  public final int getSyncState()
  {
    return syncState;
  }
  
  public final String getTrackingSource()
  {
    return trackingSource;
  }
  
  public final String getTrackingType()
  {
    return trackingType;
  }
  
  public final String getValue()
  {
    return value;
  }
  
  public final int getWildcardType()
  {
    return wildcardType;
  }
  
  public final int hashCode()
  {
    int i = value.hashCode() * 31;
    int j = rule;
    i = (i + j) * 31;
    j = wildcardType;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FilterBackupItem(serverId=");
    String str = serverId;
    localStringBuilder.append(str);
    localStringBuilder.append(", value=");
    str = value;
    localStringBuilder.append(str);
    localStringBuilder.append(", label=");
    str = label;
    localStringBuilder.append(str);
    localStringBuilder.append(", rule=");
    int i = rule;
    localStringBuilder.append(i);
    localStringBuilder.append(", wildcardType=");
    i = wildcardType;
    localStringBuilder.append(i);
    localStringBuilder.append(", syncState=");
    i = syncState;
    localStringBuilder.append(i);
    localStringBuilder.append(", trackingType=");
    str = trackingType;
    localStringBuilder.append(str);
    localStringBuilder.append(", trackingSource=");
    str = trackingSource;
    localStringBuilder.append(str);
    localStringBuilder.append(", entityType=");
    i = entityType;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.FilterBackupItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */