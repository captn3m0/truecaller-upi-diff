package com.truecaller.backup;

import android.content.Context;
import c.a.y;
import c.g.b.w;
import c.l.b;
import com.truecaller.common.e.e;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.access.d.a;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

public final class z$s
  implements t
{
  private final c.f f;
  private final String g;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(s.class);
    ((c.g.b.u)localObject).<init>(localb, "systemLocale", "getSystemLocale()Ljava/util/Locale;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  z$s(z paramz, com.google.gson.f paramf, Context paramContext, e parame)
  {
    paramz = new com/truecaller/backup/z$s$c;
    paramz.<init>(this);
    paramz = c.g.a((c.g.a.a)paramz);
    f = paramz;
    g = "Language";
  }
  
  private final Locale d()
  {
    return (Locale)f.b();
  }
  
  public final String a()
  {
    return g;
  }
  
  public final boolean b(Object paramObject)
  {
    boolean bool1 = paramObject instanceof String;
    int i = 1;
    if (bool1)
    {
      bool1 = c();
      if (bool1)
      {
        localObject1 = b();
        bool1 = c.g.b.k.a(paramObject, localObject1);
        if (bool1) {}
      }
      else
      {
        a(paramObject);
        bool1 = true;
        break label58;
      }
    }
    bool1 = false;
    Object localObject1 = null;
    label58:
    if (!bool1) {
      return false;
    }
    localObject1 = c;
    if (paramObject != null)
    {
      paramObject = (String)paramObject;
      Object localObject2 = new com/truecaller/backup/z$s$a;
      ((z.s.a)localObject2).<init>();
      localObject2 = b;
      Object localObject3 = "object : TypeToken<T>() {}.type";
      c.g.b.k.a(localObject2, (String)localObject3);
      paramObject = ((com.google.gson.f)localObject1).a((String)paramObject, (Type)localObject2);
      localObject1 = "this.fromJson(json, typeToken<T>())";
      c.g.b.k.a(paramObject, (String)localObject1);
      paramObject = (LanguageBackupItem)paramObject;
      boolean bool2 = ((LanguageBackupItem)paramObject).getAuto();
      if (bool2)
      {
        paramObject = d();
      }
      else
      {
        paramObject = Settings.b("language");
        c.g.b.k.a(paramObject, "Settings.get(Settings.LANGUAGE_ISO)");
        localObject1 = paramObject;
        localObject1 = (CharSequence)paramObject;
        localObject2 = "_";
        localObject3 = new c/n/k;
        ((c.n.k)localObject3).<init>((String)localObject2);
        localObject1 = ((c.n.k)localObject3).a((CharSequence)localObject1, 0);
        boolean bool3 = ((List)localObject1).isEmpty();
        if (!bool3)
        {
          j = ((List)localObject1).size();
          localObject2 = ((List)localObject1).listIterator(j);
          do
          {
            boolean bool4 = ((ListIterator)localObject2).hasPrevious();
            if (!bool4) {
              break;
            }
            localObject3 = (CharSequence)((ListIterator)localObject2).previous();
            k = ((CharSequence)localObject3).length();
            if (k == 0)
            {
              k = 1;
            }
            else
            {
              k = 0;
              localObject3 = null;
            }
          } while (k != 0);
          localObject1 = (Iterable)localObject1;
          j = ((ListIterator)localObject2).nextIndex() + i;
          localObject1 = c.a.m.d((Iterable)localObject1, j);
        }
        else
        {
          localObject1 = (List)y.a;
        }
        localObject1 = (Collection)localObject1;
        if (localObject1 == null) {
          break label532;
        }
        localObject2 = new String[0];
        localObject1 = ((Collection)localObject1).toArray((Object[])localObject2);
        if (localObject1 == null) {
          break label519;
        }
        localObject1 = (String[])localObject1;
        j = localObject1.length;
        int k = 2;
        if (j == k)
        {
          paramObject = new java/util/Locale;
          localObject2 = localObject1[0];
          localObject1 = localObject1[i];
          ((Locale)paramObject).<init>((String)localObject2, (String)localObject1);
        }
        else
        {
          localObject1 = new java/util/Locale;
          ((Locale)localObject1).<init>((String)paramObject);
          paramObject = localObject1;
        }
      }
      localObject1 = b.c;
      Object localObject4 = new com/truecaller/backup/z$s$b;
      int j = 0;
      localObject2 = null;
      ((z.s.b)localObject4).<init>(this, (Locale)paramObject, null);
      localObject4 = (c.g.a.m)localObject4;
      kotlinx.coroutines.f.a((c.d.f)localObject1, (c.g.a.m)localObject4);
      paramObject = new com/truecaller/old/data/access/d$a;
      localObject1 = d.getApplicationContext();
      if (localObject1 != null)
      {
        localObject1 = (com.truecaller.common.b.a)localObject1;
        ((d.a)paramObject).<init>((com.truecaller.common.b.a)localObject1);
        ((d.a)paramObject).run();
        return false;
      }
      paramObject = new c/u;
      ((c.u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
      throw ((Throwable)paramObject);
      label519:
      paramObject = new c/u;
      ((c.u)paramObject).<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw ((Throwable)paramObject);
      label532:
      paramObject = new c/u;
      ((c.u)paramObject).<init>("null cannot be cast to non-null type java.util.Collection<T>");
      throw ((Throwable)paramObject);
    }
    paramObject = new c/u;
    ((c.u)paramObject).<init>("null cannot be cast to non-null type kotlin.String");
    throw ((Throwable)paramObject);
  }
  
  public final boolean c()
  {
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.z.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */