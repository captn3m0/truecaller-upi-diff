package com.truecaller.backup;

import android.content.Context;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.common.e.f;
import com.truecaller.common.h.j;
import java.util.Locale;
import kotlinx.coroutines.ag;

final class z$s$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  z$s$b(z.s params, Locale paramLocale, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/backup/z$s$b;
    z.s locals = b;
    Locale localLocale = c;
    localb.<init>(locals, localLocale, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = c;
        if (paramObject != null)
        {
          paramObject = b.d;
          localObject = c;
          f.a((Context)paramObject, (Locale)localObject);
        }
        j.b();
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.z.s.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */