package com.truecaller.backup;

import c.b.a;
import java.util.Comparator;

public final class ContactBackupItem$a
  implements Comparator
{
  public final int compare(Object paramObject1, Object paramObject2)
  {
    paramObject1 = (Comparable)((ContactBackupItem)paramObject1).getName();
    paramObject2 = (Comparable)((ContactBackupItem)paramObject2).getName();
    return a.a((Comparable)paramObject1, (Comparable)paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ContactBackupItem.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */