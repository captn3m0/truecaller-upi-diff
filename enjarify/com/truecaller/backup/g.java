package com.truecaller.backup;

import android.content.Context;
import android.support.v4.app.Fragment;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.o.b;
import com.truecaller.common.account.r;
import com.truecaller.utils.d;

public final class g
  implements e
{
  private final Context a;
  private final ap b;
  private final ax c;
  private final bk d;
  private final bc e;
  private final f f;
  private final com.truecaller.featuretoggles.e g;
  private final r h;
  private final com.truecaller.common.g.a i;
  private final d j;
  private final ag k;
  
  public g(Context paramContext, ap paramap, ax paramax, bk parambk, bc parambc, f paramf, com.truecaller.featuretoggles.e parame, r paramr, com.truecaller.common.g.a parama, d paramd, ag paramag)
  {
    a = paramContext;
    b = paramap;
    c = paramax;
    d = parambk;
    e = parambc;
    f = paramf;
    g = parame;
    h = paramr;
    i = parama;
    j = paramd;
    k = paramag;
  }
  
  public final Object a(Fragment paramFragment, c paramc)
  {
    return e.a(paramFragment, paramc);
  }
  
  public final Object a(c paramc)
  {
    return e.a(paramc);
  }
  
  public final boolean a()
  {
    Object localObject = g.l();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool1)
    {
      localObject = h;
      bool1 = ((r)localObject).c();
      if (bool1)
      {
        localObject = i;
        String str = "backup_enabled";
        bool1 = ((com.truecaller.common.g.a)localObject).b(str);
        if (bool1)
        {
          localObject = j.l();
          str = "kenzo";
          bool1 = k.a(localObject, str);
          boolean bool2 = true;
          bool1 ^= bool2;
          if (bool1) {
            return bool2;
          }
        }
      }
    }
    return false;
  }
  
  public final Object b(Fragment paramFragment, c paramc)
  {
    return e.b(paramFragment, paramc);
  }
  
  public final Object b(c paramc)
  {
    boolean bool1 = paramc instanceof g.b;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (g.b)paramc;
      int m = b;
      n = -1 << -1;
      m &= n;
      if (m != 0)
      {
        int i3 = b - n;
        b = i3;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/backup/g$b;
    ((g.b)localObject1).<init>(this, (c)paramc);
    label70:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    int n = b;
    g localg;
    switch (n)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 2: 
      bool1 = paramc instanceof o.b;
      if (!bool1) {
        break label308;
      }
      throw a;
    case 1: 
      localg = (g)d;
      boolean bool3 = paramc instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      int i1 = paramc instanceof o.b;
      if (i1 != 0) {
        break label328;
      }
      paramc = e;
      d = this;
      i1 = 1;
      b = i1;
      paramc = paramc.a(null, (c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      localg = this;
    }
    paramc = (Boolean)paramc;
    boolean bool2 = paramc.booleanValue();
    if (!bool2) {
      return BackupResult.ErrorNetwork;
    }
    paramc = f;
    Object localObject2 = new com/truecaller/backup/g$c;
    ((g.c)localObject2).<init>(localg, null);
    localObject2 = (m)localObject2;
    d = localg;
    int i2 = 2;
    b = i2;
    paramc = kotlinx.coroutines.g.a(paramc, (m)localObject2, (c)localObject1);
    if (paramc == locala) {
      return locala;
    }
    label308:
    paramc = (BackupResult)paramc;
    localObject1 = BackupResult.Success;
    if (paramc != localObject1) {
      return paramc;
    }
    return BackupResult.Success;
    label328:
    throw a;
  }
  
  public final void b()
  {
    e.a();
  }
  
  public final Object c()
  {
    Object localObject = k;
    BackupFile localBackupFile = BackupFile.DB;
    localObject = ((ag)localObject).a(localBackupFile);
    if (localObject == null) {
      return c.d.b.a.b.a(0L);
    }
    return e.c((String)localObject);
  }
  
  public final Object c(c paramc)
  {
    boolean bool1 = paramc instanceof g.d;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (g.d)paramc;
      int m = b;
      n = -1 << -1;
      m &= n;
      if (m != 0)
      {
        int i3 = b - n;
        b = i3;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/backup/g$d;
    ((g.d)localObject1).<init>(this, (c)paramc);
    label70:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    int n = b;
    g localg;
    switch (n)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 2: 
      bool1 = paramc instanceof o.b;
      if (!bool1) {
        break label308;
      }
      throw a;
    case 1: 
      localg = (g)d;
      boolean bool3 = paramc instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      int i1 = paramc instanceof o.b;
      if (i1 != 0) {
        break label328;
      }
      paramc = e;
      d = this;
      i1 = 1;
      b = i1;
      paramc = paramc.a(null, (c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      localg = this;
    }
    paramc = (Boolean)paramc;
    boolean bool2 = paramc.booleanValue();
    if (!bool2) {
      return BackupResult.ErrorNetwork;
    }
    paramc = f;
    Object localObject2 = new com/truecaller/backup/g$e;
    ((g.e)localObject2).<init>(localg, null);
    localObject2 = (m)localObject2;
    d = localg;
    int i2 = 2;
    b = i2;
    paramc = kotlinx.coroutines.g.a(paramc, (m)localObject2, (c)localObject1);
    if (paramc == locala) {
      return locala;
    }
    label308:
    paramc = (BackupResult)paramc;
    localObject1 = BackupResult.Success;
    if (paramc != localObject1) {
      return paramc;
    }
    return BackupResult.Success;
    label328:
    throw a;
  }
  
  public final void d() {}
}

/* Location:
 * Qualified Name:     com.truecaller.backup.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */