package com.truecaller.backup;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import c.a.m;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.utils.extensions.d;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

public final class bl
  implements bk
{
  final ContentResolver a;
  private final com.truecaller.common.g.a b;
  private final com.truecaller.utils.a c;
  private final b d;
  
  public bl(Context paramContext, com.truecaller.common.g.a parama, com.truecaller.utils.a parama1, b paramb)
  {
    b = parama;
    c = parama1;
    d = paramb;
    paramContext = paramContext.getContentResolver();
    a = paramContext;
  }
  
  private final bj a(int paramInt)
  {
    Object localObject1 = a;
    Uri localUri = TruecallerContract.aa.a();
    String[] arrayOfString1 = bn.a();
    String str1 = "transport=?";
    int i = 1;
    String[] arrayOfString2 = new String[i];
    Object localObject2 = String.valueOf(paramInt);
    arrayOfString2[0] = localObject2;
    String str2 = "date DESC, participant_id DESC";
    localObject2 = ((ContentResolver)localObject1).query(localUri, arrayOfString1, str1, arrayOfString2, str2);
    if (localObject2 != null)
    {
      localObject1 = new com/truecaller/backup/bj;
      ((bj)localObject1).<init>((Cursor)localObject2);
      return (bj)localObject1;
    }
    return null;
  }
  
  public final void a()
  {
    Object localObject1 = ContentProviderOperation.newUpdate(TruecallerContract.aa.a());
    Object localObject2 = { "0" };
    localObject1 = ((ContentProviderOperation.Builder)localObject1).withSelection("transport=?", (String[])localObject2);
    localObject2 = Integer.valueOf(4);
    localObject1 = ((ContentProviderOperation.Builder)localObject1).withValue("transport", localObject2).build();
    ContentResolver localContentResolver = a;
    localObject2 = TruecallerContract.a();
    boolean bool = true;
    ContentProviderOperation[] arrayOfContentProviderOperation = new ContentProviderOperation[bool];
    arrayOfContentProviderOperation[0] = localObject1;
    localObject1 = m.d(arrayOfContentProviderOperation);
    localContentResolver.applyBatch((String)localObject2, (ArrayList)localObject1);
    b.b("deleteBackupDuplicates", bool);
  }
  
  public final void b()
  {
    bl localbl = this;
    try
    {
      Object localObject1 = b;
      Object localObject3 = "deleteBackupDuplicates";
      int i = 0;
      Object localObject4 = null;
      boolean bool2 = ((com.truecaller.common.g.a)localObject1).a((String)localObject3, false);
      if (!bool2) {
        return;
      }
      localObject1 = c;
      long l1 = ((com.truecaller.utils.a)localObject1).a();
      int k = 4;
      localObject1 = a(k);
      if (localObject1 == null) {
        return;
      }
      localObject3 = a(0);
      if (localObject3 == null) {
        return;
      }
      Object localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>();
      localObject5 = (List)localObject5;
      Object localObject6 = new com/truecaller/backup/bl$a;
      ((bl.a)localObject6).<init>(this, (List)localObject5);
      localObject6 = (c.g.a.a)localObject6;
      int m = ((bj)localObject1).moveToNext();
      boolean bool4 = ((bj)localObject3).moveToNext();
      while ((m != 0) && (bool4))
      {
        long l2 = ((bj)localObject1).b();
        long l3 = ((bj)localObject3).b();
        long l4 = ((bj)localObject1).c();
        long l5 = ((bj)localObject3).c();
        localObject4 = b;
        int i2 = m;
        Object localObject7 = localObject1;
        localObject7 = (Cursor)localObject1;
        c.l.g[] arrayOfg = bj.a;
        int i3 = 3;
        boolean bool5 = bool4;
        Object localObject8 = arrayOfg[i3];
        localObject4 = ((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject7, (c.l.g)localObject8);
        localObject4 = (Number)localObject4;
        i = ((Number)localObject4).intValue() & 0x34;
        if (i != 0)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localObject4 = null;
        }
        int n = 50;
        long l6;
        int i1;
        if (i != 0)
        {
          l6 = ((bj)localObject1).a();
          localObject4 = TruecallerContract.aa.a(l6);
          localObject4 = ContentProviderOperation.newDelete((Uri)localObject4);
          localObject4 = ((ContentProviderOperation.Builder)localObject4).build();
          localObject8 = "ContentProviderOperation…                 .build()";
          k.a(localObject4, (String)localObject8);
          ((List)localObject5).add(localObject4);
          i = ((List)localObject5).size();
          if (i == n) {
            ((c.g.a.a)localObject6).invoke();
          }
          i1 = ((bj)localObject1).moveToNext();
          bool4 = bool5;
          i = 0;
          localObject4 = null;
        }
        else
        {
          boolean bool1 = l2 < l3;
          if (!bool1)
          {
            bool1 = l2 < l3;
            if (bool1)
            {
              bool4 = ((bj)localObject3).moveToNext();
              i1 = i2;
              bool1 = false;
              localObject4 = null;
              continue;
            }
            bool1 = l4 < l5;
            if (!bool1)
            {
              bool1 = l4 < l5;
              if (bool1)
              {
                bool4 = ((bj)localObject3).moveToNext();
                i1 = i2;
                bool1 = false;
                localObject4 = null;
                continue;
              }
              l6 = ((bj)localObject1).a();
              localObject4 = TruecallerContract.aa.a(l6);
              localObject4 = ContentProviderOperation.newDelete((Uri)localObject4);
              localObject4 = ((ContentProviderOperation.Builder)localObject4).build();
              localObject8 = "ContentProviderOperation…                 .build()";
              k.a(localObject4, (String)localObject8);
              ((List)localObject5).add(localObject4);
              j = ((List)localObject5).size();
              if (j == i1) {
                ((c.g.a.a)localObject6).invoke();
              }
            }
          }
          boolean bool3 = ((bj)localObject1).moveToNext();
          bool4 = bool5;
          j = 0;
          localObject4 = null;
        }
      }
      ((c.g.a.a)localObject6).invoke();
      localObject1 = (Closeable)localObject1;
      d.a((Closeable)localObject1);
      localObject3 = (Closeable)localObject3;
      d.a((Closeable)localObject3);
      localObject1 = c;
      long l7 = ((com.truecaller.utils.a)localObject1).a() - l1;
      localObject1 = d;
      e.a locala = new com/truecaller/analytics/e$a;
      String str = "BackupDuration";
      locala.<init>(str);
      str = "Segment";
      localObject5 = "Messages";
      locala = locala.a(str, (String)localObject5);
      double d1 = l7;
      double d2 = 1000.0D;
      Double.isNaN(d1);
      d1 /= d2;
      localObject3 = Double.valueOf(d1);
      localObject3 = locala.a((Double)localObject3);
      localObject3 = ((e.a)localObject3).a();
      localObject4 = "AnalyticsEvent.Builder(B…\n                .build()";
      k.a(localObject3, (String)localObject4);
      ((b)localObject1).b((e)localObject3);
      localObject1 = b;
      localObject3 = "deleteBackupDuplicates";
      int j = 0;
      localObject4 = null;
      ((com.truecaller.common.g.a)localObject1).b((String)localObject3, false);
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */