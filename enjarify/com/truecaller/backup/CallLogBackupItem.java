package com.truecaller.backup;

import c.g.b.k;

public final class CallLogBackupItem
{
  private final int action;
  private final long callLogId;
  private final String componentName;
  private final long duration;
  private final int features;
  private final transient Integer flag;
  private final String number;
  private final long timestamp;
  private final int type;
  
  public CallLogBackupItem(long paramLong1, String paramString1, long paramLong2, long paramLong3, int paramInt1, int paramInt2, int paramInt3, String paramString2, Integer paramInteger)
  {
    callLogId = paramLong1;
    number = paramString1;
    timestamp = paramLong2;
    duration = paramLong3;
    type = paramInt1;
    action = paramInt2;
    features = paramInt3;
    componentName = paramString2;
    flag = paramInteger;
  }
  
  public final long component1()
  {
    return callLogId;
  }
  
  public final String component2()
  {
    return number;
  }
  
  public final long component3()
  {
    return timestamp;
  }
  
  public final long component4()
  {
    return duration;
  }
  
  public final int component5()
  {
    return type;
  }
  
  public final int component6()
  {
    return action;
  }
  
  public final int component7()
  {
    return features;
  }
  
  public final String component8()
  {
    return componentName;
  }
  
  public final Integer component9()
  {
    return flag;
  }
  
  public final CallLogBackupItem copy(long paramLong1, String paramString1, long paramLong2, long paramLong3, int paramInt1, int paramInt2, int paramInt3, String paramString2, Integer paramInteger)
  {
    k.b(paramString1, "number");
    CallLogBackupItem localCallLogBackupItem = new com/truecaller/backup/CallLogBackupItem;
    localCallLogBackupItem.<init>(paramLong1, paramString1, paramLong2, paramLong3, paramInt1, paramInt2, paramInt3, paramString2, paramInteger);
    return localCallLogBackupItem;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof CallLogBackupItem;
    if (bool1)
    {
      long l1 = callLogId;
      paramObject = (CallLogBackupItem)paramObject;
      long l2 = callLogId;
      boolean bool2 = l1 < l2;
      if (!bool2)
      {
        l1 = timestamp;
        l2 = timestamp;
        boolean bool3 = l1 < l2;
        if (!bool3) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final int getAction()
  {
    return action;
  }
  
  public final long getCallLogId()
  {
    return callLogId;
  }
  
  public final String getComponentName()
  {
    return componentName;
  }
  
  public final long getDuration()
  {
    return duration;
  }
  
  public final int getFeatures()
  {
    return features;
  }
  
  public final Integer getFlag()
  {
    return flag;
  }
  
  public final String getNumber()
  {
    return number;
  }
  
  public final long getTimestamp()
  {
    return timestamp;
  }
  
  public final int getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    int i = Long.valueOf(callLogId).hashCode() * 31;
    int j = Long.valueOf(timestamp).hashCode();
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CallLogBackupItem(callLogId=");
    long l = callLogId;
    localStringBuilder.append(l);
    localStringBuilder.append(", number=");
    Object localObject = number;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", timestamp=");
    l = timestamp;
    localStringBuilder.append(l);
    localStringBuilder.append(", duration=");
    l = duration;
    localStringBuilder.append(l);
    localStringBuilder.append(", type=");
    int i = type;
    localStringBuilder.append(i);
    localStringBuilder.append(", action=");
    i = action;
    localStringBuilder.append(i);
    localStringBuilder.append(", features=");
    i = features;
    localStringBuilder.append(i);
    localStringBuilder.append(", componentName=");
    localObject = componentName;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", flag=");
    localObject = flag;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.CallLogBackupItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */