package com.truecaller.backup;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class BackupDto
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public static final BackupDto.a b;
  public final long a;
  
  static
  {
    Object localObject = new com/truecaller/backup/BackupDto$a;
    ((BackupDto.a)localObject).<init>((byte)0);
    b = (BackupDto.a)localObject;
    localObject = new com/truecaller/backup/BackupDto$b;
    ((BackupDto.b)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public BackupDto(long paramLong)
  {
    a = paramLong;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "dest");
    long l = a;
    paramParcel.writeLong(l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */