package com.truecaller.backup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.i;

public final class BackupContactsActivity
  extends AppCompatActivity
{
  public final void onCreate(Bundle paramBundle)
  {
    Object localObject = ThemeManager.a();
    int i = resId;
    setTheme(i);
    super.onCreate(paramBundle);
    setContentView(2131558435);
    int j = 2131364907;
    paramBundle = (Toolbar)findViewById(j);
    setSupportActionBar(paramBundle);
    paramBundle = getSupportActionBar();
    i = 1;
    if (paramBundle != null) {
      paramBundle.setDisplayHomeAsUpEnabled(i);
    }
    paramBundle = getSupportActionBar();
    if (paramBundle != null)
    {
      int k = 2131887467;
      paramBundle.setTitle(k);
    }
    paramBundle = new com/truecaller/ui/i;
    paramBundle.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putBoolean("extraBackedUpContacts", i);
    paramBundle.setArguments(localBundle);
    localObject = getSupportFragmentManager().a();
    paramBundle = (Fragment)paramBundle;
    ((o)localObject).b(2131362590, paramBundle).c();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    finish();
    return super.onOptionsItemSelected(paramMenuItem);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupContactsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */