package com.truecaller.backup;

import android.content.Context;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import java.util.Date;
import kotlinx.coroutines.ag;

final class x$g
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  x$g(x paramx, long paramLong, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    g localg = new com/truecaller/backup/x$g;
    x localx = b;
    long l = c;
    localg.<init>(localx, l, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localg;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        long l1 = c;
        long l2 = 0L;
        boolean bool2 = l1 < l2;
        if (bool2)
        {
          paramObject = new java/util/Date;
          ((Date)paramObject).<init>(l1);
          localObject = android.text.format.DateFormat.getDateFormat(x.t(b)).format((Date)paramObject);
          paramObject = android.text.format.DateFormat.getTimeFormat(x.t(b)).format((Date)paramObject);
          Context localContext = x.t(b);
          int j = 2131887493;
          int k = 2;
          Object[] arrayOfObject = new Object[k];
          arrayOfObject[0] = localObject;
          bool1 = true;
          arrayOfObject[bool1] = paramObject;
          paramObject = localContext.getString(j, arrayOfObject);
          localObject = x.b(b);
          if (localObject != null) {
            ((w.b)localObject).a((String)paramObject);
          }
        }
        else
        {
          paramObject = x.b(b);
          if (paramObject != null)
          {
            bool1 = false;
            localObject = null;
            ((w.b)paramObject).a(null);
          }
        }
        return c.x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (g)a(paramObject1, (c)paramObject2);
    paramObject2 = c.x.a;
    return ((g)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.x.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */