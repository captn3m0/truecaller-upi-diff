package com.truecaller.backup;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class ad$b
  extends c.d.b.a.k
  implements m
{
  long a;
  long b;
  int c;
  private ag e;
  
  ad$b(ad paramad, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/backup/ad$b;
    ad localad = d;
    localb.<init>(localad, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    int i = c;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label209;
      }
      paramObject = ad.a(d);
      String str = "key_backup_last_success";
      long l1 = 0L;
      long l2 = ((com.truecaller.common.g.a)paramObject).a(str, l1);
      long l3 = System.currentTimeMillis() - l2;
      paramObject = d;
      long l4 = ad.b((ad)paramObject);
      int j = 1;
      boolean bool3 = l3 < l4;
      int k;
      if (bool3)
      {
        k = 1;
      }
      else
      {
        k = 0;
        localad = null;
      }
      if (k != 0)
      {
        new String[1][0] = "Not enough time has passed since last backup now, skipping.";
        return BackupResult.Skipped;
      }
      ad localad = d;
      a = l2;
      b = l3;
      c = j;
      paramObject = localad.a(false, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return (BackupResult)paramObject;
    label209:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ad.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */