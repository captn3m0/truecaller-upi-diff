package com.truecaller.backup;

import android.support.v4.app.Fragment;
import c.d.c;
import c.g.a.m;
import c.g.b.k;
import c.o.b;
import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.b;
import com.truecaller.utils.d;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.g;

public final class f
  implements e
{
  final au a;
  final am b;
  private final c.d.f c;
  private final bc d;
  private final com.truecaller.featuretoggles.e e;
  private final r f;
  private final com.truecaller.common.g.a g;
  private final d h;
  
  public f(c.d.f paramf, bc parambc, au paramau, am paramam, com.truecaller.featuretoggles.e parame, r paramr, com.truecaller.common.g.a parama, d paramd)
  {
    c = paramf;
    d = parambc;
    a = paramau;
    b = paramam;
    e = parame;
    f = paramr;
    g = parama;
    h = paramd;
  }
  
  public final Object a(Fragment paramFragment, c paramc)
  {
    return d.a(paramFragment, paramc);
  }
  
  public final Object a(c paramc)
  {
    return d.a(paramc);
  }
  
  public final boolean a()
  {
    Object localObject = e.l();
    boolean bool1 = ((b)localObject).a();
    if (bool1)
    {
      localObject = f;
      bool1 = ((r)localObject).c();
      if (bool1)
      {
        localObject = g;
        String str = "backup_enabled";
        bool1 = ((com.truecaller.common.g.a)localObject).b(str);
        if (bool1)
        {
          localObject = h.l();
          str = "kenzo";
          bool1 = k.a(localObject, str);
          boolean bool2 = true;
          bool1 ^= bool2;
          if (bool1) {
            return bool2;
          }
        }
      }
    }
    return false;
  }
  
  public final Object b(Fragment paramFragment, c paramc)
  {
    return d.b(paramFragment, paramc);
  }
  
  public final Object b(c paramc)
  {
    boolean bool1 = paramc instanceof f.a;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (f.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/backup/f$a;
    ((f.a)localObject1).<init>(this, (c)paramc);
    label70:
    paramc = a;
    Object localObject2 = c.d.a.a.a;
    int j = b;
    int m = 1;
    int n = 0;
    boolean bool2;
    Object localObject3;
    boolean bool5;
    Object localObject4;
    switch (j)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 3: 
      localObject2 = (BackupResult)e;
      localObject1 = (f)d;
      bool2 = paramc instanceof o.b;
      if (!bool2) {
        break label474;
      }
      throw a;
    case 2: 
      bool2 = f;
      localObject3 = (f)d;
      boolean bool4 = paramc instanceof o.b;
      if (!bool4)
      {
        bool5 = bool2;
        localObject4 = localObject3;
        break label390;
      }
      throw a;
    case 1: 
      localObject4 = (f)d;
      bool5 = paramc instanceof o.b;
      if (bool5) {
        throw a;
      }
      break;
    case 0: 
      bool2 = paramc instanceof o.b;
      if (bool2) {
        break label550;
      }
      paramc = d;
      d = this;
      b = m;
      paramc = paramc.a(null, (c)localObject1);
      if (paramc == localObject2) {
        return localObject2;
      }
      localObject4 = this;
    }
    paramc = (Boolean)paramc;
    boolean bool3 = paramc.booleanValue();
    if (bool3)
    {
      localObject3 = c;
      Object localObject5 = new com/truecaller/backup/f$b;
      ((f.b)localObject5).<init>((f)localObject4, null);
      localObject5 = (m)localObject5;
      d = localObject4;
      f = bool3;
      int i1 = 2;
      b = i1;
      localObject3 = g.a((c.d.f)localObject3, (m)localObject5, (c)localObject1);
      if (localObject3 == localObject2) {
        return localObject2;
      }
      bool5 = bool3;
      paramc = (c)localObject3;
      label390:
      paramc = (BackupResult)paramc;
      localObject5 = c;
      Object localObject6 = new com/truecaller/backup/f$c;
      ((f.c)localObject6).<init>((f)localObject4, null);
      localObject6 = (m)localObject6;
      d = localObject4;
      f = bool5;
      e = paramc;
      n = 3;
      b = n;
      localObject1 = g.a((c.d.f)localObject5, (m)localObject6, (c)localObject1);
      if (localObject1 == localObject2) {
        return localObject2;
      }
      localObject2 = paramc;
      paramc = (c)localObject1;
      localObject1 = localObject4;
      label474:
      paramc = (BackupResult)paramc;
      localObject4 = BackupResult.Success;
      if (localObject2 == localObject4)
      {
        localObject2 = BackupResult.Success;
        if (paramc != localObject2) {
          localObject2 = paramc;
        } else {
          localObject2 = BackupResult.Success;
        }
      }
      paramc = BackupResult.Success;
      if (localObject2 == paramc)
      {
        paramc = g;
        localObject1 = "restoreOnboardingShown";
        paramc.b((String)localObject1, m);
      }
      return localObject2;
    }
    return BackupResult.ErrorNetwork;
    label550:
    throw a;
  }
  
  public final void b()
  {
    d.a();
  }
  
  public final Object c()
  {
    return d.b();
  }
  
  public final Object c(c paramc)
  {
    boolean bool1 = paramc instanceof f.d;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (f.d)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int n = b - j;
        b = n;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/backup/f$d;
    ((f.d)localObject1).<init>(this, (c)paramc);
    label70:
    paramc = a;
    Object localObject2 = c.d.a.a.a;
    int j = b;
    Object localObject3;
    switch (j)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 3: 
      localObject1 = (BackupResult)e;
      boolean bool2 = paramc instanceof o.b;
      if (!bool2) {
        break label449;
      }
      throw a;
    case 2: 
      k = f;
      localObject3 = (f)d;
      boolean bool3 = paramc instanceof o.b;
      if (!bool3) {
        break label365;
      }
      throw a;
    case 1: 
      f localf = (f)d;
      boolean bool4 = paramc instanceof o.b;
      if (!bool4) {
        localObject3 = localf;
      } else {
        throw a;
      }
      break;
    case 0: 
      k = paramc instanceof o.b;
      if (k != 0) {
        break label486;
      }
      paramc = d;
      d = this;
      k = 1;
      b = k;
      paramc = paramc.a(null, (c)localObject1);
      if (paramc == localObject2) {
        return localObject2;
      }
      localObject3 = this;
    }
    paramc = (Boolean)paramc;
    int k = paramc.booleanValue();
    if (k != 0)
    {
      paramc = c;
      Object localObject4 = new com/truecaller/backup/f$e;
      ((f.e)localObject4).<init>((f)localObject3, null);
      localObject4 = (m)localObject4;
      d = localObject3;
      f = k;
      int i1 = 2;
      b = i1;
      paramc = g.a(paramc, (m)localObject4, (c)localObject1);
      if (paramc == localObject2) {
        return localObject2;
      }
      label365:
      paramc = (BackupResult)paramc;
      localObject4 = c;
      Object localObject5 = new com/truecaller/backup/f$f;
      ((f.f)localObject5).<init>((f)localObject3, null);
      localObject5 = (m)localObject5;
      d = localObject3;
      f = k;
      e = paramc;
      int m = 3;
      b = m;
      localObject1 = g.a((c.d.f)localObject4, (m)localObject5, (c)localObject1);
      if (localObject1 == localObject2) {
        return localObject2;
      }
      Object localObject6 = localObject1;
      localObject1 = paramc;
      paramc = (c)localObject6;
      label449:
      paramc = (BackupResult)paramc;
      localObject2 = BackupResult.Success;
      if (localObject1 != localObject2) {
        return localObject1;
      }
      localObject1 = BackupResult.Success;
      if (paramc != localObject1) {
        return paramc;
      }
      return BackupResult.Success;
    }
    return BackupResult.ErrorNetwork;
    label486:
    throw a;
  }
  
  public final void d()
  {
    ag localag = (ag)bg.a;
    c.d.f localf = c;
    Object localObject = new com/truecaller/backup/f$g;
    ((f.g)localObject).<init>(this, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.a(localag, localf, (m)localObject, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */