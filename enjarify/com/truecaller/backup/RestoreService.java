package com.truecaller.backup;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.z.a;
import android.support.v4.app.z.a.a;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.support.v4.content.d;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.h.o;
import com.truecaller.notifications.a;
import com.truecaller.ui.TruecallerInit;

public final class RestoreService
  extends Service
  implements bt.b
{
  public bt.a a;
  public a b;
  private final String c = "restoreInProgress";
  private final String d = "restoreSuccess";
  private final String e = "restoreError";
  private final String f = "restoreErrorNotSupportedDb";
  
  private final z.d f(String paramString)
  {
    z.d locald = new android/support/v4/app/z$d;
    Object localObject = this;
    localObject = (Context)this;
    locald.<init>((Context)localObject, paramString);
    return locald;
  }
  
  public final void a()
  {
    Object localObject = this;
    localObject = d.a((Context)this);
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.truecaller.ACTION_FINISH");
    ((d)localObject).a(localIntent);
  }
  
  public final void a(int paramInt)
  {
    Toast.makeText((Context)this, paramInt, 1).show();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "channelId");
    paramString = f(paramString);
    Object localObject = this;
    localObject = (Context)this;
    int i = 2131099685;
    int j = android.support.v4.content.b.c((Context)localObject, i);
    paramString = paramString.f(j).a(17301633);
    localObject = (CharSequence)getString(2131888696);
    paramString = paramString.a((CharSequence)localObject).b();
    j = 0;
    Notification localNotification = paramString.a(0, 0).h();
    int k = 2131364168;
    startForeground(k, localNotification);
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString = "Subtype";
    localObject = c;
    localBundle.putString(paramString, (String)localObject);
    localObject = b;
    if (localObject == null)
    {
      paramString = "notificationManager";
      k.a(paramString);
    }
    k.a(localNotification, "notification");
    ((a)localObject).a(null, 2131364168, localNotification, "notificationBackup", localBundle);
  }
  
  public final void b()
  {
    stopSelf();
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "channelId");
    Object localObject1 = this;
    localObject1 = (Context)this;
    Object localObject2 = TruecallerInit.a((Context)localObject1, "notificationBackup");
    int i = 268435456;
    ((Intent)localObject2).addFlags(i);
    ((Intent)localObject2).addFlags(32768);
    localObject2 = PendingIntent.getActivity((Context)localObject1, 0, (Intent)localObject2, i);
    paramString = f(paramString);
    i = 2131099685;
    int j = android.support.v4.content.b.c((Context)localObject1, i);
    paramString = paramString.f(j).a(2131234004);
    localObject1 = (CharSequence)getString(2131888695);
    paramString = paramString.a((CharSequence)localObject1);
    j = 2131888694;
    localObject1 = (CharSequence)getString(j);
    Notification localNotification = paramString.b((CharSequence)localObject1).a((PendingIntent)localObject2).e().h();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString = "Subtype";
    localObject1 = d;
    localBundle.putString(paramString, (String)localObject1);
    localObject1 = b;
    if (localObject1 == null)
    {
      paramString = "notificationManager";
      k.a(paramString);
    }
    k.a(localNotification, "notification");
    ((a)localObject1).a(null, 2131364166, localNotification, "notificationBackup", localBundle);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "channelId");
    Object localObject1 = new android/content/Intent;
    Object localObject2 = this;
    localObject2 = (Context)this;
    Object localObject3 = RestoreService.class;
    ((Intent)localObject1).<init>((Context)localObject2, (Class)localObject3);
    int i = Build.VERSION.SDK_INT;
    int j = 134217728;
    int k = 0;
    CharSequence localCharSequence = null;
    int m = 26;
    if (i >= m) {
      localObject1 = PendingIntent.getForegroundService((Context)localObject2, 0, (Intent)localObject1, j);
    } else {
      localObject1 = PendingIntent.getService((Context)localObject2, 0, (Intent)localObject1, j);
    }
    localObject3 = new android/support/v4/app/z$a$a;
    j = 2131234170;
    k = 2131887224;
    localCharSequence = (CharSequence)getString(k);
    ((z.a.a)localObject3).<init>(j, localCharSequence, (PendingIntent)localObject1);
    localObject1 = ((z.a.a)localObject3).a();
    paramString = f(paramString);
    i = 2131099685;
    int n = android.support.v4.content.b.c((Context)localObject2, i);
    paramString = paramString.f(n).a(2131234005);
    localObject2 = (CharSequence)getString(2131887494);
    paramString = paramString.a((CharSequence)localObject2);
    n = 2131887471;
    localObject2 = (CharSequence)getString(n);
    Notification localNotification = paramString.b((CharSequence)localObject2).a((z.a)localObject1).e().h();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString = "Subtype";
    localObject1 = e;
    localBundle.putString(paramString, (String)localObject1);
    localObject1 = b;
    if (localObject1 == null)
    {
      paramString = "notificationManager";
      k.a(paramString);
    }
    k.a(localNotification, "notification");
    ((a)localObject1).a(null, 2131364167, localNotification, "notificationBackup", localBundle);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "channelId");
    Object localObject1 = new android/content/Intent;
    Object localObject2 = this;
    localObject2 = (Context)this;
    Object localObject3 = Uri.parse(o.c((Context)localObject2));
    ((Intent)localObject1).<init>("android.intent.action.VIEW", (Uri)localObject3);
    paramString = f(paramString);
    int i = android.support.v4.content.b.c((Context)localObject2, 2131099685);
    paramString = paramString.f(i).a(2131234005);
    CharSequence localCharSequence = (CharSequence)getString(2131887494);
    paramString = paramString.a(localCharSequence);
    i = 2131887472;
    localObject3 = (CharSequence)getString(i);
    paramString = paramString.b((CharSequence)localObject3);
    localObject1 = PendingIntent.getActivity((Context)localObject2, 0, (Intent)localObject1, 0);
    paramString = paramString.a((PendingIntent)localObject1);
    localObject1 = new android/support/v4/app/z$c;
    ((z.c)localObject1).<init>();
    localCharSequence = (CharSequence)getString(i);
    localObject1 = (z.g)((z.c)localObject1).b(localCharSequence);
    localObject3 = paramString.a((z.g)localObject1).e().h();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString = "Subtype";
    localObject1 = f;
    localBundle.putString(paramString, (String)localObject1);
    localObject1 = b;
    if (localObject1 == null)
    {
      paramString = "notificationManager";
      k.a(paramString);
    }
    k.a(localObject3, "notification");
    ((a)localObject1).a(null, 2131364167, (Notification)localObject3, "notificationBackup", localBundle);
  }
  
  public final void e(String paramString)
  {
    String str = "channelId";
    k.b(paramString, str);
    paramString = b;
    if (paramString == null)
    {
      str = "notificationManager";
      k.a(str);
    }
    paramString.a(2131364167);
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject = getApplicationContext();
    if (localObject != null)
    {
      ((TrueApp)localObject).a().cv().a(this);
      localObject = a;
      if (localObject == null)
      {
        String str = "presenter";
        k.a(str);
      }
      ((bt.a)localObject).a(this);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw ((Throwable)localObject);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    bt.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.y_();
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    paramIntent = a;
    if (paramIntent == null)
    {
      String str = "presenter";
      k.a(str);
    }
    paramIntent.a();
    return 3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.RestoreService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */