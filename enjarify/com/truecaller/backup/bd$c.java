package com.truecaller.backup;

import c.d.a.a;
import c.d.c;
import c.g.a.b;
import c.g.a.m;
import c.o.b;
import c.x;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import kotlinx.coroutines.ag;

final class bd$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  bd$c(bd parambd, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/backup/bd$c;
    bd localbd = b;
    localc.<init>(localbd, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = b.a;
        if (paramObject != null)
        {
          paramObject = ((DriveClient)paramObject).requestSync();
          if (paramObject != null)
          {
            localObject = (OnFailureListener)bd.c.1.a;
            paramObject = ((Task)paramObject).a((OnFailureListener)localObject);
            if (paramObject != null)
            {
              localObject = new com/truecaller/backup/bd$c$2;
              ((bd.c.2)localObject).<init>(this);
              localObject = (b)localObject;
              paramObject = cf.b((Task)paramObject, (b)localObject);
              break label88;
            }
          }
        }
        boolean bool2 = false;
        paramObject = null;
        label88:
        if (paramObject != null)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          paramObject = null;
        }
        return Boolean.valueOf(bool2);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bd.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */