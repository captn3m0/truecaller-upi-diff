package com.truecaller.backup;

import android.support.v4.app.Fragment;
import c.d.c;
import c.g.a.m;
import c.o.b;
import kotlinx.coroutines.ag;

final class x$h
  extends c.d.b.a.k
  implements m
{
  boolean a;
  boolean b;
  int c;
  private ag g;
  
  x$h(x paramx, boolean paramBoolean, Fragment paramFragment, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    h localh = new com/truecaller/backup/x$h;
    x localx = d;
    boolean bool = e;
    Fragment localFragment = f;
    localh.<init>(localx, bool, localFragment, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return localh;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = c;
    int j = 1;
    boolean bool1;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label289;
      }
      throw a;
    case 1: 
      bool1 = a;
      boolean bool3 = paramObject instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label544;
      }
      paramObject = x.a(d);
      localObject2 = "backup_enabled";
      bool1 = ((com.truecaller.common.g.a)paramObject).b((String)localObject2);
      bool4 = e;
      if (bool1 == bool4) {
        return c.x.a;
      }
      if (!bool4) {
        break label472;
      }
      paramObject = x.b(d);
      if (paramObject != null) {
        ((w.b)paramObject).c();
      }
      paramObject = x.c(d);
      localObject3 = f;
      a = bool1;
      c = j;
      paramObject = ((e)paramObject).a((Fragment)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Boolean)paramObject;
    boolean bool4 = ((Boolean)paramObject).booleanValue();
    Object localObject3 = x.b(d);
    if (localObject3 != null) {
      ((w.b)localObject3).d();
    }
    if (bool4)
    {
      localObject3 = d;
      a = bool1;
      b = bool4;
      int k = 2;
      c = k;
      paramObject = ((x)localObject3).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label289:
      paramObject = (Boolean)paramObject;
      boolean bool5 = ((Boolean)paramObject).booleanValue();
      if (bool5)
      {
        paramObject = x.b(d);
        if (paramObject != null) {
          ((w.b)paramObject).a(false);
        }
        return c.x.a;
      }
      x.a(d).b("backup_enabled", j);
      paramObject = x.a(d);
      localObject1 = "key_backup_frequency_hours";
      localObject2 = d;
      long l1 = x.d((x)localObject2);
      long l2 = ((com.truecaller.common.g.a)paramObject).a((String)localObject1, l1);
      paramObject = d;
      l1 = x.d((x)paramObject);
      bool5 = l2 < l1;
      if (!bool5)
      {
        paramObject = d;
        l2 = x.e((x)paramObject);
        ((x)paramObject).a(l2);
      }
      x.a(d, j);
      x.b(d, j);
      paramObject = d;
      x.f((x)paramObject);
    }
    else
    {
      paramObject = x.b(d);
      if (paramObject != null)
      {
        ((w.b)paramObject).a(false);
        break label533;
        label472:
        paramObject = x.a(d);
        localObject1 = "backup_enabled";
        ((com.truecaller.common.g.a)paramObject).b((String)localObject1, false);
        x.a(d, false);
        x.b(d, false);
        x.f(d);
        paramObject = x.b(d);
        if (paramObject != null) {
          ((w.b)paramObject).e();
        }
      }
    }
    label533:
    x.g(d);
    return c.x.a;
    label544:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (h)a(paramObject1, (c)paramObject2);
    paramObject2 = c.x.a;
    return ((h)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.x.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */