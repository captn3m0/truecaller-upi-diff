package com.truecaller.backup;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.o;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.ui.dialogs.a;
import java.util.HashMap;

public final class n
  extends com.truecaller.startup_dialogs.fragments.j
  implements o.b
{
  public o.a a;
  private com.truecaller.ui.dialogs.k b;
  private HashMap c;
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final o.a a()
  {
    o.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locala;
  }
  
  public final void a(long paramLong)
  {
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    c.g.b.k.a(localf, "activity ?: return");
    Object localObject = new com/truecaller/backup/bp;
    ((bp)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putLong("last_backup_time", paramLong);
    localBundle.putString("context", "wizard");
    ((bp)localObject).setArguments(localBundle);
    o localo = localf.getSupportFragmentManager().a();
    localObject = (Fragment)localObject;
    String str = bp.class.getSimpleName();
    localo.a((Fragment)localObject, str).d();
  }
  
  public final void b()
  {
    int i = 2131887466;
    com.truecaller.ui.dialogs.k localk = com.truecaller.ui.dialogs.k.c(i);
    b = localk;
    localk = b;
    if (localk != null)
    {
      boolean bool = true;
      localk.setCancelable(bool);
    }
    localk = b;
    if (localk != null)
    {
      f localf = getActivity();
      a.a(localk, localf);
      return;
    }
  }
  
  public final void c()
  {
    try
    {
      com.truecaller.ui.dialogs.k localk = b;
      if (localk != null) {
        localk.dismissAllowingStateLoss();
      }
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    b = null;
  }
  
  public final void d()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    Toast.makeText(getContext(), 2131887944, 0).show();
  }
  
  public final void dismiss()
  {
    dismissAllowingStateLoss();
  }
  
  public final void e()
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = requireContext();
    localBuilder.<init>((Context)localObject);
    localBuilder = localBuilder.setTitle(2131887478).setMessage(2131887475);
    localObject = new com/truecaller/backup/n$d;
    ((n.d)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    localBuilder = localBuilder.setPositiveButton(2131887477, (DialogInterface.OnClickListener)localObject);
    localObject = new com/truecaller/backup/n$e;
    ((n.e)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    localBuilder = localBuilder.setNegativeButton(2131887476, (DialogInterface.OnClickListener)localObject);
    localObject = new com/truecaller/backup/n$f;
    ((n.f)localObject).<init>(this);
    localObject = (DialogInterface.OnCancelListener)localObject;
    localBuilder.setOnCancelListener((DialogInterface.OnCancelListener)localObject).show();
  }
  
  public final void f()
  {
    HashMap localHashMap = c;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    o.a locala = a;
    if (locala == null)
    {
      paramIntent = "presenter";
      c.g.b.k.a(paramIntent);
    }
    locala.a(paramInt1);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if (paramContext != null) {
      paramContext = paramContext.getApplicationContext();
    } else {
      paramContext = null;
    }
    if (paramContext != null)
    {
      ((TrueApp)paramContext).a().cv().a(this);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramContext;
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new com/truecaller/backup/n$a;
    Context localContext = (Context)getActivity();
    paramBundle.<init>(this, localContext);
    return (Dialog)paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558574, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    o.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.y_();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    paramBundle = paramView.findViewById(2131362306);
    Object localObject = new com/truecaller/backup/n$b;
    ((n.b)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    int i = 2131362341;
    paramView = paramView.findViewById(i);
    paramBundle = new com/truecaller/backup/n$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      c.g.b.k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */