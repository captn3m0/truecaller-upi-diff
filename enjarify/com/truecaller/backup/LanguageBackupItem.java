package com.truecaller.backup;

import c.g.b.k;

public final class LanguageBackupItem
{
  private final boolean auto;
  private final String languageISOCode;
  
  public LanguageBackupItem(boolean paramBoolean, String paramString)
  {
    auto = paramBoolean;
    languageISOCode = paramString;
  }
  
  public final boolean component1()
  {
    return auto;
  }
  
  public final String component2()
  {
    return languageISOCode;
  }
  
  public final LanguageBackupItem copy(boolean paramBoolean, String paramString)
  {
    LanguageBackupItem localLanguageBackupItem = new com/truecaller/backup/LanguageBackupItem;
    localLanguageBackupItem.<init>(paramBoolean, paramString);
    return localLanguageBackupItem;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof LanguageBackupItem;
      if (bool2)
      {
        paramObject = (LanguageBackupItem)paramObject;
        bool2 = auto;
        boolean bool3 = auto;
        String str;
        if (bool2 == bool3)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          str = null;
        }
        if (bool2)
        {
          str = languageISOCode;
          paramObject = languageISOCode;
          boolean bool4 = k.a(str, paramObject);
          if (bool4) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final boolean getAuto()
  {
    return auto;
  }
  
  public final String getLanguageISOCode()
  {
    return languageISOCode;
  }
  
  public final int hashCode()
  {
    boolean bool = auto;
    if (bool) {
      bool = true;
    }
    int i;
    bool *= true;
    String str = languageISOCode;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LanguageBackupItem(auto=");
    boolean bool = auto;
    localStringBuilder.append(bool);
    localStringBuilder.append(", languageISOCode=");
    String str = languageISOCode;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.LanguageBackupItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */