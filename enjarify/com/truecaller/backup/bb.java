package com.truecaller.backup;

import android.content.Context;
import c.d.f;
import com.truecaller.analytics.b;
import com.truecaller.analytics.d;
import com.truecaller.utils.t;
import dagger.a.g;
import javax.inject.Provider;

public final class bb
  implements c
{
  private Provider A;
  private final com.truecaller.common.a b;
  private final d c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  private Provider w;
  private Provider x;
  private Provider y;
  private Provider z;
  
  private bb(com.truecaller.common.a parama, t paramt, d paramd)
  {
    b = parama;
    c = paramd;
    Object localObject = new com/truecaller/backup/bb$c;
    ((bb.c)localObject).<init>(parama);
    d = ((Provider)localObject);
    localObject = new com/truecaller/backup/bb$h;
    ((bb.h)localObject).<init>(parama);
    e = ((Provider)localObject);
    localObject = new com/truecaller/backup/bb$m;
    ((bb.m)localObject).<init>(paramt);
    f = ((Provider)localObject);
    localObject = k.a(d);
    g = ((Provider)localObject);
    localObject = new com/truecaller/backup/bb$k;
    ((bb.k)localObject).<init>(paramt);
    h = ((Provider)localObject);
    localObject = new com/truecaller/backup/bb$b;
    ((bb.b)localObject).<init>(paramd);
    i = ((Provider)localObject);
    Provider localProvider1 = d;
    Provider localProvider2 = e;
    Provider localProvider3 = f;
    Provider localProvider4 = g;
    Provider localProvider5 = h;
    Provider localProvider6 = i;
    paramd = ar.a(localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6);
    j = paramd;
    paramd = dagger.a.c.a(j);
    k = paramd;
    paramd = az.a(d);
    l = paramd;
    paramd = dagger.a.c.a(l);
    m = paramd;
    paramd = new com/truecaller/backup/bb$e;
    paramd.<init>(parama);
    n = paramd;
    paramd = d;
    localObject = n;
    localProvider1 = h;
    localProvider2 = i;
    paramd = bm.a(paramd, (Provider)localObject, localProvider1, localProvider2);
    o = paramd;
    paramd = dagger.a.c.a(o);
    p = paramd;
    paramd = new com/truecaller/backup/bb$j;
    paramd.<init>(parama);
    q = paramd;
    paramd = new com/truecaller/backup/bb$d;
    paramd.<init>(parama);
    r = paramd;
    paramd = new com/truecaller/backup/bb$g;
    paramd.<init>(parama);
    s = paramd;
    paramd = s;
    localObject = d;
    localProvider1 = n;
    localProvider2 = i;
    paramd = ai.a(paramd, (Provider)localObject, localProvider1, localProvider2);
    t = paramd;
    paramd = d;
    localObject = n;
    localProvider1 = q;
    localProvider2 = r;
    localProvider3 = t;
    paramd = be.a(paramd, (Provider)localObject, localProvider1, localProvider2, localProvider3);
    u = paramd;
    paramd = dagger.a.c.a(u);
    v = paramd;
    paramd = new com/truecaller/backup/bb$f;
    paramd.<init>(parama);
    w = paramd;
    paramd = new com/truecaller/backup/bb$i;
    paramd.<init>(parama);
    x = paramd;
    parama = new com/truecaller/backup/bb$l;
    parama.<init>(paramt);
    y = parama;
    localObject = d;
    localProvider1 = k;
    localProvider2 = m;
    localProvider3 = p;
    localProvider4 = v;
    localProvider5 = r;
    localProvider6 = w;
    Provider localProvider7 = x;
    Provider localProvider8 = n;
    Provider localProvider9 = y;
    Provider localProvider10 = t;
    parama = h.a((Provider)localObject, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7, localProvider8, localProvider9, localProvider10);
    z = parama;
    parama = dagger.a.c.a(z);
    A = parama;
  }
  
  public static bb.a e()
  {
    bb.a locala = new com/truecaller/backup/bb$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final e a()
  {
    return (e)A.get();
  }
  
  public final bc b()
  {
    return (bc)v.get();
  }
  
  public final ag c()
  {
    ah localah = new com/truecaller/backup/ah;
    f localf = (f)g.a(b.t(), "Cannot return null from a non-@Nullable component method");
    Context localContext = (Context)g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.common.g.a locala = (com.truecaller.common.g.a)g.a(b.c(), "Cannot return null from a non-@Nullable component method");
    b localb = (b)g.a(c.c(), "Cannot return null from a non-@Nullable component method");
    localah.<init>(localf, localContext, locala, localb);
    return localah;
  }
  
  public final bk d()
  {
    return (bk)p.get();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */