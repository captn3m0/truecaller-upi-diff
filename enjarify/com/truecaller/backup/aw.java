package com.truecaller.backup;

import dagger.a.d;
import javax.inject.Provider;

public final class aw
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  
  private aw(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
    f = paramProvider6;
  }
  
  public static aw a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6)
  {
    aw localaw = new com/truecaller/backup/aw;
    localaw.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
    return localaw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.aw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */