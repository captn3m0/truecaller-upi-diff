package com.truecaller.backup;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.v4.app.Fragment;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.at;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.utils.i;
import kotlinx.coroutines.ag;
import org.apache.a.d.d;

final class br$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  boolean c;
  long d;
  int e;
  private ag h;
  
  br$c(br parambr, Fragment paramFragment, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/backup/br$c;
    br localbr = f;
    Fragment localFragment = g;
    localc.<init>(localbr, localFragment, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = e;
    long l1 = 0L;
    int k = 1;
    boolean bool2 = false;
    int n = 2;
    boolean bool1;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 5: 
      boolean bool3 = paramObject instanceof o.b;
      if (!bool3) {
        break label775;
      }
      throw a;
    case 4: 
      bool1 = c;
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label700;
      }
      throw a;
    case 3: 
      bool1 = c;
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label635;
      }
      throw a;
    case 2: 
      localObject2 = (br)b;
      i2 = c;
      localObject3 = (ag)a;
      boolean bool4 = paramObject instanceof o.b;
      if (!bool4) {
        break label533;
      }
      throw a;
    case 1: 
      localObject2 = (ag)a;
      i2 = paramObject instanceof o.b;
      if (i2 == 0) {
        localObject3 = localObject2;
      } else {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label1016;
      }
      paramObject = h;
      localObject2 = f;
      Object localObject4 = "PositiveBtnClicked";
      ((br)localObject2).b((String)localObject4);
      localObject2 = f.h;
      bool1 = ((i)localObject2).a();
      if (!bool1)
      {
        localObject1 = f.e;
        localObject2 = new com/truecaller/backup/br$c$1;
        ((br.c.1)localObject2).<init>(this, null);
        localObject2 = (m)localObject2;
        kotlinx.coroutines.e.b((ag)paramObject, (c.d.f)localObject1, (m)localObject2, n);
        return x.a;
      }
      localObject2 = f.e;
      localObject4 = new com/truecaller/backup/br$c$2;
      ((br.c.2)localObject4).<init>(this, null);
      localObject4 = (m)localObject4;
      kotlinx.coroutines.e.b((ag)paramObject, (c.d.f)localObject2, (m)localObject4, n);
      localObject2 = f.f;
      localObject4 = g;
      a = paramObject;
      e = k;
      localObject2 = ((e)localObject2).a((Fragment)localObject4, this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject3 = paramObject;
      paramObject = localObject2;
    }
    paramObject = (Boolean)paramObject;
    int i2 = ((Boolean)paramObject).booleanValue();
    if (i2 == 0)
    {
      paramObject = f.e;
      localObject1 = new com/truecaller/backup/br$c$3;
      ((br.c.3)localObject1).<init>(this, null);
      localObject1 = (m)localObject1;
      kotlinx.coroutines.e.b((ag)localObject3, (c.d.f)paramObject, (m)localObject1, n);
      return x.a;
    }
    paramObject = f;
    boolean bool5 = c;
    if (!bool5)
    {
      localObject2 = f;
      paramObject = f;
      a = localObject3;
      c = i2;
      b = localObject2;
      e = n;
      paramObject = ((e)paramObject).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label533:
      paramObject = (Boolean)paramObject;
      bool5 = ((Boolean)paramObject).booleanValue();
      c = bool5;
    }
    paramObject = f.e;
    Object localObject2 = new com/truecaller/backup/br$c$4;
    ((br.c.4)localObject2).<init>(this, null);
    localObject2 = (m)localObject2;
    kotlinx.coroutines.e.b((ag)localObject3, (c.d.f)paramObject, (m)localObject2, n);
    new String[1][0] = "Checking user's Drive account if has backup";
    paramObject = f.f;
    c = i2;
    int j = 3;
    e = j;
    paramObject = ((e)paramObject).c();
    if (paramObject == localObject1) {
      return localObject1;
    }
    j = i2;
    label635:
    paramObject = (Number)paramObject;
    long l2 = ((Number)paramObject).longValue();
    bool5 = l2 < l1;
    if (!bool5)
    {
      paramObject = f.g;
      c = j;
      d = l2;
      int m = 4;
      e = m;
      paramObject = ((e)paramObject).c();
      if (paramObject == localObject1) {
        return localObject1;
      }
      label700:
      paramObject = (Number)paramObject;
      l2 = ((Number)paramObject).longValue();
    }
    bool5 = l2 < l1;
    Object localObject5;
    if (!bool5)
    {
      new String[1][0] = "User has no backup at chosen Drive account";
      paramObject = f;
      localObject5 = g;
      c = j;
      d = l2;
      j = 5;
      e = j;
      paramObject = ((br)paramObject).a((Fragment)localObject5, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label775:
      paramObject = (Boolean)paramObject;
      bool5 = ((Boolean)paramObject).booleanValue();
      if (!bool5) {
        return x.a;
      }
      paramObject = "Found account with backup. Performing restore";
      new String[1][0] = paramObject;
    }
    paramObject = new android/content/Intent;
    localObject1 = f.d;
    localObject2 = RestoreService.class;
    ((Intent)paramObject).<init>((Context)localObject1, (Class)localObject2);
    int i1 = Build.VERSION.SDK_INT;
    j = 26;
    if (i1 >= j)
    {
      localObject1 = f.d;
      ((Context)localObject1).startForegroundService((Intent)paramObject);
    }
    else
    {
      localObject1 = f.d;
      ((Context)localObject1).startService((Intent)paramObject);
    }
    paramObject = (bq.b)f.b;
    if (paramObject != null) {
      ((bq.b)paramObject).dismiss();
    }
    paramObject = f;
    localObject1 = a;
    if (localObject1 != null)
    {
      localObject2 = i;
      localObject5 = new com/truecaller/analytics/e$a;
      ((e.a)localObject5).<init>("SettingChanged");
      localObject1 = ((e.a)localObject5).a("Context", (String)localObject1).a("Setting", "Backup");
      String str = "Enabled";
      localObject1 = ((e.a)localObject1).a("State", str).a();
      localObject5 = "AnalyticsEvent.Builder(A…\n                .build()";
      c.g.b.k.a(localObject1, (String)localObject5);
      ((b)localObject2).a((com.truecaller.analytics.e)localObject1);
      localObject1 = at.a(k, "wizard");
      paramObject = (ae)j.a();
      localObject1 = (d)localObject1;
      ((ae)paramObject).a((d)localObject1);
    }
    return x.a;
    label1016:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.br.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */