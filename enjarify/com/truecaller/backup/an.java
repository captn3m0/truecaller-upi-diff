package com.truecaller.backup;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import c.a.m;
import c.d.c;
import c.g.b.k;
import c.o.b;
import c.u;
import c.x;
import com.google.gson.f;
import com.google.gson.p;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class an
  implements am
{
  private final Context a;
  private final bc b;
  private final com.truecaller.callhistory.a c;
  private final f d;
  private final ag e;
  
  public an(Context paramContext, bc parambc, com.truecaller.callhistory.a parama, f paramf, ag paramag)
  {
    a = paramContext;
    b = parambc;
    c = parama;
    d = paramf;
    e = paramag;
  }
  
  /* Error */
  private final Set c()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 41	com/truecaller/backup/an:c	Lcom/truecaller/callhistory/a;
    //   4: invokeinterface 50 1 0
    //   9: invokevirtual 55	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   12: checkcast 57	com/truecaller/callhistory/z
    //   15: astore_1
    //   16: aconst_null
    //   17: astore_2
    //   18: aload_1
    //   19: ifnonnull +5 -> 24
    //   22: aconst_null
    //   23: areturn
    //   24: aload_1
    //   25: ldc 59
    //   27: invokestatic 61	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   30: aload_1
    //   31: checkcast 63	android/database/Cursor
    //   34: astore_1
    //   35: aload_1
    //   36: astore_3
    //   37: aload_1
    //   38: checkcast 65	java/io/Closeable
    //   41: astore_3
    //   42: new 67	java/util/ArrayList
    //   45: astore 4
    //   47: aload 4
    //   49: invokespecial 68	java/util/ArrayList:<init>	()V
    //   52: aload 4
    //   54: checkcast 70	java/util/Collection
    //   57: astore 4
    //   59: aload_1
    //   60: invokeinterface 74 1 0
    //   65: istore 5
    //   67: iconst_0
    //   68: istore 6
    //   70: aconst_null
    //   71: astore 7
    //   73: lconst_0
    //   74: lstore 8
    //   76: iload 5
    //   78: ifeq +350 -> 428
    //   81: aload_1
    //   82: astore 10
    //   84: aload_1
    //   85: checkcast 57	com/truecaller/callhistory/z
    //   88: astore 10
    //   90: new 76	com/truecaller/backup/CallLogBackupItem
    //   93: astore 11
    //   95: aload 10
    //   97: invokeinterface 79 1 0
    //   102: astore 12
    //   104: aload 12
    //   106: ifnull +29 -> 135
    //   109: aload 12
    //   111: invokevirtual 85	com/truecaller/data/entity/HistoryEvent:i	()Ljava/lang/Long;
    //   114: astore 12
    //   116: aload 12
    //   118: ifnull +17 -> 135
    //   121: aload 12
    //   123: invokevirtual 91	java/lang/Long:longValue	()J
    //   126: lstore 13
    //   128: lload 13
    //   130: lstore 15
    //   132: goto +7 -> 139
    //   135: lload 8
    //   137: lstore 15
    //   139: aload 10
    //   141: invokeinterface 79 1 0
    //   146: astore 12
    //   148: aload 12
    //   150: ifnull +15 -> 165
    //   153: aload 12
    //   155: invokevirtual 94	com/truecaller/data/entity/HistoryEvent:a	()Ljava/lang/String;
    //   158: astore 12
    //   160: aload 12
    //   162: ifnonnull +7 -> 169
    //   165: ldc 96
    //   167: astore 12
    //   169: aload 12
    //   171: astore 17
    //   173: aload 10
    //   175: invokeinterface 79 1 0
    //   180: astore 12
    //   182: aload 12
    //   184: ifnull +13 -> 197
    //   187: aload 12
    //   189: invokevirtual 99	com/truecaller/data/entity/HistoryEvent:j	()J
    //   192: lstore 18
    //   194: goto +7 -> 201
    //   197: lload 8
    //   199: lstore 18
    //   201: aload 10
    //   203: invokeinterface 79 1 0
    //   208: astore 12
    //   210: aload 12
    //   212: ifnull +10 -> 222
    //   215: aload 12
    //   217: invokevirtual 102	com/truecaller/data/entity/HistoryEvent:k	()J
    //   220: lstore 8
    //   222: aload 10
    //   224: invokeinterface 79 1 0
    //   229: astore 12
    //   231: aload 12
    //   233: ifnull +17 -> 250
    //   236: aload 12
    //   238: invokevirtual 106	com/truecaller/data/entity/HistoryEvent:f	()I
    //   241: istore 20
    //   243: iload 20
    //   245: istore 21
    //   247: goto +6 -> 253
    //   250: iconst_0
    //   251: istore 21
    //   253: aload 10
    //   255: invokeinterface 79 1 0
    //   260: astore 12
    //   262: aload 12
    //   264: ifnull +17 -> 281
    //   267: aload 12
    //   269: invokevirtual 109	com/truecaller/data/entity/HistoryEvent:h	()I
    //   272: istore 20
    //   274: iload 20
    //   276: istore 22
    //   278: goto +6 -> 284
    //   281: iconst_0
    //   282: istore 22
    //   284: aload 10
    //   286: invokeinterface 79 1 0
    //   291: astore 12
    //   293: aload 12
    //   295: ifnull +17 -> 312
    //   298: aload 12
    //   300: invokevirtual 112	com/truecaller/data/entity/HistoryEvent:m	()I
    //   303: istore 6
    //   305: iload 6
    //   307: istore 23
    //   309: goto +6 -> 315
    //   312: iconst_0
    //   313: istore 23
    //   315: aload 10
    //   317: invokeinterface 79 1 0
    //   322: astore 7
    //   324: aload 7
    //   326: ifnull +17 -> 343
    //   329: aload 7
    //   331: invokevirtual 115	com/truecaller/data/entity/HistoryEvent:q	()Ljava/lang/String;
    //   334: astore 7
    //   336: aload 7
    //   338: astore 24
    //   340: goto +6 -> 346
    //   343: aconst_null
    //   344: astore 24
    //   346: aload 10
    //   348: invokeinterface 79 1 0
    //   353: astore 10
    //   355: aload 10
    //   357: ifnull +24 -> 381
    //   360: aload 10
    //   362: invokevirtual 118	com/truecaller/data/entity/HistoryEvent:r	()I
    //   365: istore 5
    //   367: iload 5
    //   369: invokestatic 124	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   372: astore 10
    //   374: aload 10
    //   376: astore 25
    //   378: goto +6 -> 384
    //   381: aconst_null
    //   382: astore 25
    //   384: aload 11
    //   386: astore 12
    //   388: aload 11
    //   390: astore 10
    //   392: aload 11
    //   394: lload 15
    //   396: aload 17
    //   398: lload 18
    //   400: lload 8
    //   402: iload 21
    //   404: iload 22
    //   406: iload 23
    //   408: aload 24
    //   410: aload 25
    //   412: invokespecial 127	com/truecaller/backup/CallLogBackupItem:<init>	(JLjava/lang/String;JJIIILjava/lang/String;Ljava/lang/Integer;)V
    //   415: aload 4
    //   417: aload 11
    //   419: invokeinterface 131 2 0
    //   424: pop
    //   425: goto -366 -> 59
    //   428: aload 4
    //   430: checkcast 133	java/util/List
    //   433: astore 4
    //   435: aload_3
    //   436: aconst_null
    //   437: invokestatic 138	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   440: aload 4
    //   442: checkcast 140	java/lang/Iterable
    //   445: astore 4
    //   447: new 67	java/util/ArrayList
    //   450: astore_1
    //   451: aload_1
    //   452: invokespecial 68	java/util/ArrayList:<init>	()V
    //   455: aload_1
    //   456: checkcast 70	java/util/Collection
    //   459: astore_1
    //   460: aload 4
    //   462: invokeinterface 144 1 0
    //   467: astore_2
    //   468: aload_2
    //   469: invokeinterface 149 1 0
    //   474: istore 26
    //   476: iload 26
    //   478: ifeq +101 -> 579
    //   481: aload_2
    //   482: invokeinterface 152 1 0
    //   487: astore_3
    //   488: aload_3
    //   489: astore 4
    //   491: aload_3
    //   492: checkcast 76	com/truecaller/backup/CallLogBackupItem
    //   495: astore 4
    //   497: aload 4
    //   499: invokevirtual 155	com/truecaller/backup/CallLogBackupItem:getCallLogId	()J
    //   502: lstore 13
    //   504: lload 13
    //   506: lload 8
    //   508: lcmp
    //   509: istore 5
    //   511: iload 5
    //   513: ifge +47 -> 560
    //   516: aload 4
    //   518: invokevirtual 159	com/truecaller/backup/CallLogBackupItem:getFlag	()Ljava/lang/Integer;
    //   521: astore 4
    //   523: aload 4
    //   525: ifnonnull +6 -> 531
    //   528: goto +23 -> 551
    //   531: aload 4
    //   533: invokevirtual 162	java/lang/Integer:intValue	()I
    //   536: istore 27
    //   538: iconst_2
    //   539: istore 5
    //   541: iload 27
    //   543: iload 5
    //   545: if_icmpne +6 -> 551
    //   548: goto +12 -> 560
    //   551: iconst_0
    //   552: istore 27
    //   554: aconst_null
    //   555: astore 4
    //   557: goto +6 -> 563
    //   560: iconst_1
    //   561: istore 27
    //   563: iload 27
    //   565: ifeq -97 -> 468
    //   568: aload_1
    //   569: aload_3
    //   570: invokeinterface 131 2 0
    //   575: pop
    //   576: goto -108 -> 468
    //   579: aload_1
    //   580: checkcast 133	java/util/List
    //   583: checkcast 140	java/lang/Iterable
    //   586: invokestatic 169	c/a/m:i	(Ljava/lang/Iterable;)Ljava/util/Set;
    //   589: areturn
    //   590: astore_1
    //   591: goto +8 -> 599
    //   594: astore_1
    //   595: aload_1
    //   596: astore_2
    //   597: aload_1
    //   598: athrow
    //   599: aload_3
    //   600: aload_2
    //   601: invokestatic 138	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   604: aload_1
    //   605: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	606	0	this	an
    //   15	565	1	localObject1	Object
    //   590	1	1	localObject2	Object
    //   594	11	1	localObject3	Object
    //   17	584	2	localObject4	Object
    //   36	564	3	localObject5	Object
    //   45	511	4	localObject6	Object
    //   65	12	5	bool1	boolean
    //   365	3	5	i	int
    //   509	3	5	bool2	boolean
    //   539	7	5	j	int
    //   68	238	6	k	int
    //   71	266	7	localObject7	Object
    //   74	433	8	l1	long
    //   82	309	10	localObject8	Object
    //   93	325	11	localCallLogBackupItem	CallLogBackupItem
    //   102	285	12	localObject9	Object
    //   126	379	13	l2	long
    //   130	265	15	l3	long
    //   171	226	17	localObject10	Object
    //   192	207	18	l4	long
    //   241	34	20	m	int
    //   245	158	21	n	int
    //   276	129	22	i1	int
    //   307	100	23	i2	int
    //   338	71	24	localObject11	Object
    //   376	35	25	localObject12	Object
    //   474	3	26	bool3	boolean
    //   536	28	27	i3	int
    // Exception table:
    //   from	to	target	type
    //   597	599	590	finally
    //   42	45	594	finally
    //   47	52	594	finally
    //   52	57	594	finally
    //   59	65	594	finally
    //   84	88	594	finally
    //   90	93	594	finally
    //   95	102	594	finally
    //   109	114	594	finally
    //   121	126	594	finally
    //   139	146	594	finally
    //   153	158	594	finally
    //   173	180	594	finally
    //   187	192	594	finally
    //   201	208	594	finally
    //   215	220	594	finally
    //   222	229	594	finally
    //   236	241	594	finally
    //   253	260	594	finally
    //   267	272	594	finally
    //   284	291	594	finally
    //   298	303	594	finally
    //   315	322	594	finally
    //   329	334	594	finally
    //   346	353	594	finally
    //   360	365	594	finally
    //   367	372	594	finally
    //   410	415	594	finally
    //   417	425	594	finally
    //   428	433	594	finally
  }
  
  private final String d()
  {
    ag localag = e;
    BackupFile localBackupFile = BackupFile.CALL_LOG;
    return localag.a(localBackupFile);
  }
  
  public final Object a()
  {
    Object localObject1 = d();
    if (localObject1 == null) {
      return BackupResult.ErrorFileName;
    }
    Object localObject2 = c();
    if (localObject2 == null) {
      return BackupResult.ErrorDatabase;
    }
    int i = 1;
    Object localObject3 = new String[i];
    Object localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>("Backing up ");
    int j = ((Set)localObject2).size();
    ((StringBuilder)localObject4).append(j);
    String str = " entries from the call log";
    ((StringBuilder)localObject4).append(str);
    localObject4 = ((StringBuilder)localObject4).toString();
    localObject3[0] = localObject4;
    localObject3 = d;
    Object localObject5 = new com/truecaller/backup/an$a;
    ((an.a)localObject5).<init>();
    localObject5 = b;
    localObject4 = "object : TypeToken<T>() {}.type";
    k.a(localObject5, (String)localObject4);
    localObject2 = ((f)localObject3).a(localObject2, (Type)localObject5);
    k.a(localObject2, "this.toJson(src, typeToken<T>())");
    localObject3 = b;
    localObject5 = c.n.d.a;
    if (localObject2 != null)
    {
      localObject2 = ((String)localObject2).getBytes((Charset)localObject5);
      k.a(localObject2, "(this as java.lang.String).getBytes(charset)");
      return ((bc)localObject3).a((String)localObject1, (byte[])localObject2);
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
    throw ((Throwable)localObject1);
  }
  
  public final Object a(c paramc)
  {
    boolean bool1 = paramc instanceof an.d;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (an.d)paramc;
      int i = b;
      k = -1 << -1;
      i &= k;
      if (i != 0)
      {
        int m = b - k;
        b = m;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/backup/an$d;
    ((an.d)localObject1).<init>(this, (c)paramc);
    label70:
    paramc = a;
    Object localObject2 = c.d.a.a.a;
    int k = b;
    int n = 1;
    switch (k)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 1: 
      localObject1 = (an)d;
      boolean bool2 = paramc instanceof o.b;
      if (!bool2) {
        break;
      }
    }
    try
    {
      paramc = (o.b)paramc;
      paramc = a;
      throw paramc;
    }
    catch (p localp)
    {
      boolean bool3;
      Object localObject3;
      Object localObject4;
      Object localObject5;
      int i1;
      int i2;
      int j;
      AssertionUtil.report(new String[] { "Cannot parse call log json file" });
      return BackupResult.ErrorJsonParsing;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      AssertionUtil.report(new String[] { "IllegalStateException during parsing call log json file" });
      return BackupResult.ErrorJsonParsing;
    }
    bool3 = paramc instanceof o.b;
    if (!bool3)
    {
      paramc = d();
      if (paramc == null) {
        return BackupResult.ErrorFileName;
      }
      d = this;
      e = paramc;
      b = n;
      paramc = a(paramc, (c)localObject1);
      if (paramc == localObject2) {
        return localObject2;
      }
      localObject1 = this;
      paramc = (List)paramc;
      if (paramc == null) {
        return BackupResult.ErrorRead;
      }
      localObject2 = ((an)localObject1).c();
      if (localObject2 == null) {
        return BackupResult.ErrorDatabase;
      }
      paramc = (Iterable)paramc;
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject3 = (Collection)localObject3;
      paramc = paramc.iterator();
      for (;;)
      {
        boolean bool4 = paramc.hasNext();
        if (!bool4) {
          break;
        }
        localObject4 = paramc.next();
        localObject5 = localObject4;
        localObject5 = (CallLogBackupItem)localObject4;
        boolean bool6 = ((Set)localObject2).contains(localObject5) ^ n;
        if (bool6) {
          ((Collection)localObject3).add(localObject4);
        }
      }
      localObject3 = (Iterable)localObject3;
      paramc = new java/util/ArrayList;
      i1 = m.a((Iterable)localObject3, 10);
      paramc.<init>(i1);
      paramc = (Collection)paramc;
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        boolean bool5 = ((Iterator)localObject3).hasNext();
        if (!bool5) {
          break;
        }
        localObject4 = (CallLogBackupItem)((Iterator)localObject3).next();
        localObject5 = new com/truecaller/data/entity/HistoryEvent;
        ((HistoryEvent)localObject5).<init>((CallLogBackupItem)localObject4);
        paramc.add(localObject5);
      }
      paramc = (List)paramc;
      localObject3 = new String[n];
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("Adding ");
      i2 = paramc.size();
      ((StringBuilder)localObject4).append(i2);
      ((StringBuilder)localObject4).append(" unique entries to ");
      j = ((Set)localObject2).size();
      ((StringBuilder)localObject4).append(j);
      ((StringBuilder)localObject4).append(" existing ones");
      localObject2 = ((StringBuilder)localObject4).toString();
      localObject3[0] = localObject2;
      paramc = (Integer)c.a(paramc).d();
      localObject1 = new String[n];
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append(paramc);
      ((StringBuilder)localObject2).append(" items inserted");
      paramc = ((StringBuilder)localObject2).toString();
      localObject1[0] = paramc;
      return BackupResult.Success;
    }
    throw a;
  }
  
  public final Object b()
  {
    Object localObject1 = TruecallerContract.n.a();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    String str = "timestamp = ? AND tc_flag = ?";
    Object localObject2 = c();
    if (localObject2 != null)
    {
      localObject2 = (Iterable)localObject2;
      Object localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject3 = (Collection)localObject3;
      localObject2 = ((Iterable)localObject2).iterator();
      int k;
      int m;
      Object localObject4;
      Object localObject5;
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject2).hasNext();
        int j = 0;
        k = 1;
        m = 2;
        if (!bool1) {
          break;
        }
        localObject4 = ((Iterator)localObject2).next();
        localObject5 = localObject4;
        localObject5 = ((CallLogBackupItem)localObject4).getFlag();
        if (localObject5 != null)
        {
          int n = ((Integer)localObject5).intValue();
          if (n == m) {}
        }
        else
        {
          j = 1;
        }
        if (j != 0) {
          ((Collection)localObject3).add(localObject4);
        }
      }
      localObject3 = (Iterable)localObject3;
      localObject2 = new java/util/ArrayList;
      int i = m.a((Iterable)localObject3, 10);
      ((ArrayList)localObject2).<init>(i);
      localObject2 = (Collection)localObject2;
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject3).hasNext();
        if (!bool2) {
          break;
        }
        long l = ((CallLogBackupItem)((Iterator)localObject3).next()).getTimestamp();
        localObject4 = String.valueOf(l);
        ((Collection)localObject2).add(localObject4);
      }
      localObject2 = ((Iterable)localObject2).iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject2).hasNext();
        if (!bool3) {
          break;
        }
        localObject3 = (String)((Iterator)localObject2).next();
        localObject4 = ContentProviderOperation.newDelete((Uri)localObject1);
        localObject5 = new String[m];
        localObject5[0] = localObject3;
        localObject5[k] = "2";
        ((ContentProviderOperation.Builder)localObject4).withSelection(str, (String[])localObject5);
        localObject3 = ((ContentProviderOperation.Builder)localObject4).build();
        localArrayList.add(localObject3);
      }
    }
    try
    {
      localObject1 = a;
      localObject1 = ((Context)localObject1).getContentResolver();
      str = TruecallerContract.a();
      ((ContentResolver)localObject1).applyBatch(str, localArrayList);
    }
    catch (Exception localException)
    {
      localObject1 = (Throwable)localException;
      com.truecaller.log.d.a((Throwable)localObject1);
    }
    return x.a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */