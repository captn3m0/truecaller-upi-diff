package com.truecaller.backup;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import androidx.work.p;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.common.g.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class BackupLogWorker
  extends TrackedWorker
{
  public static final BackupLogWorker.a d;
  public b b;
  public a c;
  
  static
  {
    BackupLogWorker.a locala = new com/truecaller/backup/BackupLogWorker$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public BackupLogWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    c.g.b.k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public static final void e()
  {
    p localp = p.a();
    c.g.b.k.a(localp, "WorkManager.getInstance()");
    androidx.work.g localg = androidx.work.g.a;
    androidx.work.k localk = d.a().b();
    localp.a("BackupLogWorker", localg, localk);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      c.g.b.k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    Object localObject = c;
    if (localObject == null)
    {
      str = "coreSettings";
      c.g.b.k.a(str);
    }
    localObject = ((a)localObject).a("accountAutobackupLogInfo");
    String str = null;
    if (localObject != null)
    {
      localObject = (CharSequence)localObject;
      int i = ((CharSequence)localObject).length();
      int j = 1;
      if (i > 0)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localObject = null;
      }
      if (i == j) {
        return j;
      }
    }
    return false;
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject1 = new com/truecaller/backup/d;
    Object localObject2 = c;
    if (localObject2 == null)
    {
      localObject3 = "coreSettings";
      c.g.b.k.a((String)localObject3);
    }
    ((d)localObject1).<init>((a)localObject2);
    localObject2 = a.a("accountAutobackupLogInfo");
    int i = 0;
    Object localObject3 = null;
    Object localObject4;
    if (localObject2 != null)
    {
      c.g.b.k.a(localObject2, "settings.getString(CoreS…_LOG_INFO) ?: return null");
      localObject1 = a;
      localObject4 = "accountAutobackupLogInfo";
      ((a)localObject1).a((String)localObject4, null);
      localObject2 = (CharSequence)localObject2;
      localObject1 = new String[] { ";" };
      localObject3 = null;
      int j = 6;
      localObject1 = (Iterable)c.n.m.c((CharSequence)localObject2, (String[])localObject1, false, j);
      localObject2 = new java/util/ArrayList;
      i = c.a.m.a((Iterable)localObject1, 10);
      ((ArrayList)localObject2).<init>(i);
      localObject2 = (Collection)localObject2;
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject3 = d.a((String)((Iterator)localObject1).next());
        ((Collection)localObject2).add(localObject3);
      }
      localObject3 = localObject2;
      localObject3 = (List)localObject2;
    }
    if (localObject3 == null)
    {
      localObject1 = ListenableWorker.a.c();
      c.g.b.k.a(localObject1, "Result.failure()");
      return (ListenableWorker.a)localObject1;
    }
    localObject3 = (Iterable)localObject3;
    localObject1 = ((Iterable)localObject3).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (Map)((Iterator)localObject1).next();
      localObject3 = b();
      localObject4 = new com/truecaller/analytics/e$a;
      ((e.a)localObject4).<init>("AccountBackup");
      String str1 = "BackupAction";
      String str2 = (String)((Map)localObject2).get("backup_action_key");
      if (str2 == null) {
        str2 = "";
      }
      localObject4 = ((e.a)localObject4).a(str1, str2);
      str1 = "BackupFileExists";
      str2 = (String)((Map)localObject2).get("backup_file_exists_key");
      if (str2 == null) {
        str2 = "false";
      }
      localObject4 = ((e.a)localObject4).a(str1, str2);
      str1 = "AccountStateValid";
      str2 = "account_state_valid";
      localObject2 = (String)((Map)localObject2).get(str2);
      if (localObject2 == null) {
        localObject2 = "false";
      }
      localObject2 = ((e.a)localObject4).a(str1, (String)localObject2).a();
      localObject4 = "AnalyticsEvent.Builder(A…                 .build()";
      c.g.b.k.a(localObject2, (String)localObject4);
      ((b)localObject3).b((e)localObject2);
    }
    localObject1 = ListenableWorker.a.a();
    c.g.b.k.a(localObject1, "Result.success()");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupLogWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */