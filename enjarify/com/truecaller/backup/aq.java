package com.truecaller.backup;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import c.a.f;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.log.AssertionUtil;
import com.truecaller.multisim.h;
import com.truecaller.utils.a;
import com.truecaller.utils.extensions.d;
import com.truecaller.utils.l;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;

public final class aq
  implements ap
{
  private final ContentResolver a;
  private final h b;
  private final l c;
  private final com.truecaller.callhistory.k d;
  private final a e;
  private final b f;
  
  public aq(Context paramContext, h paramh, l paraml, com.truecaller.callhistory.k paramk, a parama, b paramb)
  {
    b = paramh;
    c = paraml;
    d = paramk;
    e = parama;
    f = paramb;
    paramContext = paramContext.getContentResolver();
    a = paramContext;
  }
  
  private final void a(long paramLong, ArrayList paramArrayList)
  {
    Object localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    Integer localInteger = Integer.valueOf(2);
    ((ContentValues)localObject1).put("tc_flag", localInteger);
    ((ContentValues)localObject1).putNull("call_log_id");
    localObject1 = ContentProviderOperation.newUpdate(TruecallerContract.n.a()).withValues((ContentValues)localObject1);
    Object localObject2 = String.valueOf(paramLong);
    localObject2 = "_id=".concat((String)localObject2);
    localObject2 = ((ContentProviderOperation.Builder)localObject1).withSelection((String)localObject2, null).build();
    paramArrayList.add(localObject2);
    a(paramArrayList, false);
  }
  
  private final void a(ArrayList paramArrayList, boolean paramBoolean)
  {
    int i = paramArrayList.size();
    int j = 50;
    if ((i < j) && (!paramBoolean)) {
      return;
    }
    ContentResolver localContentResolver = a;
    String str = TruecallerContract.a();
    ArrayList localArrayList = new java/util/ArrayList;
    Object localObject = paramArrayList;
    localObject = (Collection)paramArrayList;
    localArrayList.<init>((Collection)localObject);
    localContentResolver.applyBatch(str, localArrayList);
    paramArrayList.clear();
  }
  
  public final void a()
  {
    aq localaq = this;
    Object localObject1 = c;
    Object localObject2 = { "android.permission.READ_CALL_LOG" };
    boolean bool1 = ((l)localObject1).a((String[])localObject2);
    if (bool1)
    {
      localObject1 = c;
      localObject2 = new String[] { "android.permission.READ_PHONE_STATE" };
      bool1 = ((l)localObject1).a((String[])localObject2);
      if (bool1)
      {
        long l1 = e.a();
        localObject1 = d.b();
        Object localObject3 = b.d();
        if (localObject3 != null)
        {
          localObject4 = "projection";
          c.g.b.k.a(localObject1, (String)localObject4);
          localObject3 = (String[])f.a((Object[])localObject1, localObject3);
          if (localObject3 != null)
          {
            localObject5 = localObject3;
            break label137;
          }
        }
        Object localObject5 = localObject1;
        label137:
        localObject1 = "_id";
        Object localObject4 = "timestamp";
        String[] tmp148_145 = new String[3];
        String[] tmp149_148 = tmp148_145;
        String[] tmp149_148 = tmp148_145;
        tmp149_148[0] = localObject1;
        tmp149_148[1] = "call_log_id";
        tmp149_148[2] = localObject4;
        localObject3 = tmp149_148;
        int i;
        try
        {
          localObject4 = a;
          localObject1 = d;
          Uri localUri = ((com.truecaller.callhistory.k)localObject1).a();
          i = 0;
          localObject6 = null;
          localObject7 = null;
          localObject8 = "date DESC, _id DESC";
          localObject1 = ((ContentResolver)localObject4).query(localUri, (String[])localObject5, null, null, (String)localObject8);
          if (localObject1 != null)
          {
            localObject4 = new com/truecaller/backup/as;
            ((as)localObject4).<init>((Cursor)localObject1);
          }
          else
          {
            localObject4 = null;
          }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          localObject1 = (Throwable)localIllegalArgumentException;
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
          localObject4 = null;
        }
        if (localObject4 == null) {
          return;
        }
        Object localObject6 = a;
        Object localObject7 = TruecallerContract.n.a();
        Object localObject9 = "call_log_id>=0";
        Object localObject10 = "timestamp DESC, call_log_id DESC";
        Object localObject8 = localObject3;
        localObject1 = ((ContentResolver)localObject6).query((Uri)localObject7, (String[])localObject3, (String)localObject9, null, (String)localObject10);
        if (localObject1 != null)
        {
          localObject3 = new com/truecaller/backup/bf;
          ((bf)localObject3).<init>((Cursor)localObject1);
          localObject1 = new java/util/ArrayList;
          ((ArrayList)localObject1).<init>();
          boolean bool2 = ((as)localObject4).moveToNext();
          boolean bool3 = ((bf)localObject3).moveToNext();
          for (;;)
          {
            i = 1;
            if ((!bool2) || (!bool3)) {
              break;
            }
            localObject7 = c;
            localObject8 = localObject4;
            localObject8 = (Cursor)localObject4;
            localObject9 = as.a[i];
            long l2 = ((Number)((com.truecaller.utils.extensions.g)localObject7).a((Cursor)localObject8, (c.l.g)localObject9)).longValue();
            localObject7 = c;
            localObject10 = localObject3;
            localObject10 = (Cursor)localObject3;
            Object localObject11 = bf.a;
            int j = 2;
            localObject11 = localObject11[j];
            long l3 = ((Number)((com.truecaller.utils.extensions.g)localObject7).a((Cursor)localObject10, (c.l.g)localObject11)).longValue();
            localObject7 = b;
            localObject6 = as.a[0];
            localObject6 = (Number)((com.truecaller.utils.extensions.g)localObject7).a((Cursor)localObject8, (c.l.g)localObject6);
            long l4 = ((Number)localObject6).longValue();
            localObject8 = b;
            c.l.g[] arrayOfg = bf.a;
            int k = 1;
            c.l.g localg = arrayOfg[k];
            localObject8 = (Number)((com.truecaller.utils.extensions.g)localObject8).a((Cursor)localObject10, localg);
            long l5 = ((Number)localObject8).longValue();
            boolean bool4 = l2 < l3;
            if (bool4)
            {
              bool2 = ((as)localObject4).moveToNext();
            }
            else
            {
              bool4 = l2 < l3;
              long l6;
              if (bool4)
              {
                l6 = ((bf)localObject3).a();
                localaq.a(l6, (ArrayList)localObject1);
                bool3 = ((bf)localObject3).moveToNext();
              }
              else
              {
                bool4 = l4 < l5;
                if (bool4)
                {
                  bool2 = ((as)localObject4).moveToNext();
                }
                else
                {
                  bool3 = l4 < l5;
                  if (bool3)
                  {
                    l6 = ((bf)localObject3).a();
                    localaq.a(l6, (ArrayList)localObject1);
                    bool3 = ((bf)localObject3).moveToNext();
                  }
                  else
                  {
                    l6 = ((bf)localObject3).a();
                    localObject7 = ContentProviderOperation.newDelete(TruecallerContract.n.a());
                    localObject8 = "_id=";
                    localObject5 = String.valueOf(l6);
                    localObject5 = ((String)localObject8).concat((String)localObject5);
                    i = 0;
                    localObject6 = null;
                    localObject5 = ((ContentProviderOperation.Builder)localObject7).withSelection((String)localObject5, null).build();
                    ((ArrayList)localObject1).add(localObject5);
                    localObject5 = null;
                    localaq.a((ArrayList)localObject1, false);
                    bool3 = ((bf)localObject3).moveToNext();
                  }
                }
              }
            }
          }
          while (bool3)
          {
            long l7 = ((bf)localObject3).a();
            localaq.a(l7, (ArrayList)localObject1);
            bool3 = ((bf)localObject3).moveToNext();
          }
          localaq.a((ArrayList)localObject1, true);
          d.a((Closeable)localObject4);
          d.a((Closeable)localObject3);
          long l8 = e.a() - l1;
          localObject1 = f;
          localObject2 = new com/truecaller/analytics/e$a;
          ((e.a)localObject2).<init>("BackupDuration");
          localObject2 = ((e.a)localObject2).a("Segment", "CallLog");
          double d1 = l8;
          Double.isNaN(d1);
          Double localDouble = Double.valueOf(d1 / 1000.0D);
          localObject2 = ((e.a)localObject2).a(localDouble).a();
          c.g.b.k.a(localObject2, "AnalyticsEvent.Builder(B…\n                .build()");
          ((b)localObject1).b((e)localObject2);
          return;
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */