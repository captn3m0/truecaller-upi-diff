package com.truecaller.backup;

import android.support.v4.app.Fragment;
import c.d.c;
import java.io.InputStream;
import java.util.Map;

public abstract interface bc
{
  public abstract Object a(Fragment paramFragment, c paramc);
  
  public abstract Object a(c paramc);
  
  public abstract Object a(String paramString);
  
  public abstract Object a(String paramString, InputStream paramInputStream, Map paramMap);
  
  public abstract Object a(String paramString, byte[] paramArrayOfByte);
  
  public abstract void a();
  
  public abstract Object b();
  
  public abstract Object b(Fragment paramFragment, c paramc);
  
  public abstract Object b(String paramString);
  
  public abstract Object c(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */