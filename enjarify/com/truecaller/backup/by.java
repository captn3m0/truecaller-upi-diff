package com.truecaller.backup;

import android.content.Context;
import c.a.m;
import c.d.c;
import c.g.b.k;
import c.n.d;
import c.o.b;
import c.u;
import com.google.gson.f;
import com.google.gson.p;
import com.truecaller.log.AssertionUtil;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class by
  implements bx
{
  private final Context a;
  private final f b;
  private final bc c;
  private final z d;
  private final ag e;
  
  public by(Context paramContext, f paramf, bc parambc, z paramz, ag paramag)
  {
    a = paramContext;
    b = paramf;
    c = parambc;
    d = paramz;
    e = paramag;
  }
  
  /* Error */
  private final List a()
  {
    // Byte code:
    //   0: ldc 47
    //   2: astore_1
    //   3: iconst_1
    //   4: anewarray 51	java/lang/String
    //   7: dup
    //   8: iconst_0
    //   9: ldc 49
    //   11: aastore
    //   12: astore_2
    //   13: aload_0
    //   14: getfield 37	com/truecaller/backup/by:a	Landroid/content/Context;
    //   17: invokevirtual 57	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   20: astore_3
    //   21: invokestatic 62	com/truecaller/content/TruecallerContract$Filters:a	()Landroid/net/Uri;
    //   24: astore 4
    //   26: aconst_null
    //   27: astore 5
    //   29: aconst_null
    //   30: astore 6
    //   32: aload_3
    //   33: aload 4
    //   35: aconst_null
    //   36: aload_1
    //   37: aload_2
    //   38: aconst_null
    //   39: invokevirtual 68	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   42: astore_3
    //   43: aconst_null
    //   44: astore 4
    //   46: aload_3
    //   47: ifnull +237 -> 284
    //   50: aload_3
    //   51: astore 5
    //   53: aload_3
    //   54: checkcast 70	java/io/Closeable
    //   57: astore 5
    //   59: new 72	java/util/ArrayList
    //   62: astore_1
    //   63: aload_1
    //   64: invokespecial 73	java/util/ArrayList:<init>	()V
    //   67: aload_1
    //   68: checkcast 75	java/util/Collection
    //   71: astore_1
    //   72: aload_3
    //   73: invokeinterface 81 1 0
    //   78: istore 7
    //   80: iload 7
    //   82: ifeq +164 -> 246
    //   85: new 83	com/truecaller/backup/FilterBackupItem
    //   88: astore_2
    //   89: ldc 85
    //   91: astore 6
    //   93: aload_3
    //   94: aload 6
    //   96: invokestatic 90	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   99: astore 8
    //   101: ldc 92
    //   103: astore 6
    //   105: aload_3
    //   106: aload 6
    //   108: invokestatic 90	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   111: astore 6
    //   113: aload 6
    //   115: ifnonnull +7 -> 122
    //   118: ldc 94
    //   120: astore 6
    //   122: aload 6
    //   124: astore 9
    //   126: ldc 96
    //   128: astore 6
    //   130: aload_3
    //   131: aload 6
    //   133: invokestatic 90	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   136: astore 10
    //   138: ldc 98
    //   140: astore 6
    //   142: aload_3
    //   143: aload 6
    //   145: invokestatic 101	com/truecaller/utils/extensions/k:b	(Landroid/database/Cursor;Ljava/lang/String;)I
    //   148: istore 11
    //   150: ldc 103
    //   152: astore 6
    //   154: aload_3
    //   155: aload 6
    //   157: invokestatic 101	com/truecaller/utils/extensions/k:b	(Landroid/database/Cursor;Ljava/lang/String;)I
    //   160: istore 12
    //   162: ldc 105
    //   164: astore 6
    //   166: aload_3
    //   167: aload 6
    //   169: invokestatic 101	com/truecaller/utils/extensions/k:b	(Landroid/database/Cursor;Ljava/lang/String;)I
    //   172: istore 13
    //   174: ldc 107
    //   176: astore 6
    //   178: aload_3
    //   179: aload 6
    //   181: invokestatic 90	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   184: astore 14
    //   186: ldc 109
    //   188: astore 6
    //   190: aload_3
    //   191: aload 6
    //   193: invokestatic 90	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   196: astore 15
    //   198: ldc 111
    //   200: astore 6
    //   202: aload_3
    //   203: aload 6
    //   205: invokestatic 101	com/truecaller/utils/extensions/k:b	(Landroid/database/Cursor;Ljava/lang/String;)I
    //   208: istore 16
    //   210: aload_2
    //   211: astore 6
    //   213: aload_2
    //   214: aload 8
    //   216: aload 9
    //   218: aload 10
    //   220: iload 11
    //   222: iload 12
    //   224: iload 13
    //   226: aload 14
    //   228: aload 15
    //   230: iload 16
    //   232: invokespecial 114	com/truecaller/backup/FilterBackupItem:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;I)V
    //   235: aload_1
    //   236: aload_2
    //   237: invokeinterface 118 2 0
    //   242: pop
    //   243: goto -171 -> 72
    //   246: aload_1
    //   247: checkcast 120	java/util/List
    //   250: astore_1
    //   251: aload 5
    //   253: aconst_null
    //   254: invokestatic 125	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   257: aload_1
    //   258: checkcast 127	java/lang/Iterable
    //   261: invokestatic 133	c/a/m:g	(Ljava/lang/Iterable;)Ljava/util/List;
    //   264: areturn
    //   265: astore_3
    //   266: goto +9 -> 275
    //   269: astore_3
    //   270: aload_3
    //   271: astore 4
    //   273: aload_3
    //   274: athrow
    //   275: aload 5
    //   277: aload 4
    //   279: invokestatic 125	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   282: aload_3
    //   283: athrow
    //   284: aconst_null
    //   285: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	286	0	this	by
    //   2	256	1	localObject1	Object
    //   12	225	2	localObject2	Object
    //   20	183	3	localObject3	Object
    //   265	1	3	localObject4	Object
    //   269	14	3	localObject5	Object
    //   24	254	4	localObject6	Object
    //   27	249	5	localObject7	Object
    //   30	182	6	localObject8	Object
    //   78	3	7	bool	boolean
    //   99	116	8	str1	String
    //   124	93	9	localObject9	Object
    //   136	83	10	str2	String
    //   148	73	11	i	int
    //   160	63	12	j	int
    //   172	53	13	k	int
    //   184	43	14	str3	String
    //   196	33	15	str4	String
    //   208	23	16	m	int
    // Exception table:
    //   from	to	target	type
    //   273	275	265	finally
    //   59	62	269	finally
    //   63	67	269	finally
    //   67	71	269	finally
    //   72	78	269	finally
    //   85	88	269	finally
    //   94	99	269	finally
    //   106	111	269	finally
    //   131	136	269	finally
    //   143	148	269	finally
    //   155	160	269	finally
    //   167	172	269	finally
    //   179	184	269	finally
    //   191	196	269	finally
    //   203	208	269	finally
    //   230	235	269	finally
    //   236	243	269	finally
    //   246	250	269	finally
  }
  
  private final String b()
  {
    ag localag = e;
    BackupFile localBackupFile = BackupFile.SETTINGS;
    return localag.a(localBackupFile);
  }
  
  private final String c()
  {
    ag localag = e;
    BackupFile localBackupFile = BackupFile.BLOCK_LIST;
    return localag.a(localBackupFile);
  }
  
  public final Object a(c paramc)
  {
    boolean bool1 = paramc instanceof by.a;
    int m;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (by.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        m = b - j;
        b = m;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/backup/by$a;
    ((by.a)localObject1).<init>(this, (c)paramc);
    label70:
    paramc = a;
    Object localObject2 = c.d.a.a.a;
    int j = b;
    String str1;
    Object localObject5;
    boolean bool4;
    switch (j)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 2: 
      localObject1 = (BackupResult)h;
      boolean bool2 = paramc instanceof o.b;
      if (!bool2) {
        break label800;
      }
      throw a;
    case 1: 
      localObject3 = (List)g;
      str1 = (String)f;
      localObject4 = (String)e;
      localObject5 = (by)d;
      bool4 = paramc instanceof o.b;
      if (bool4) {
        throw a;
      }
      break;
    case 0: 
      boolean bool3 = paramc instanceof o.b;
      if (bool3) {
        break label872;
      }
      localObject4 = b();
      if (localObject4 == null) {
        return BackupResult.ErrorFileName;
      }
      str1 = c();
      if (str1 == null) {
        return BackupResult.ErrorFileName;
      }
      paramc = (Iterable)m.g((Iterable)d.b.values());
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject3 = (Collection)localObject3;
      paramc = paramc.iterator();
      for (;;)
      {
        boolean bool5 = paramc.hasNext();
        if (!bool5) {
          break;
        }
        localObject5 = paramc.next();
        localObject6 = localObject5;
        localObject6 = (t)localObject5;
        bool4 = ((t)localObject6).c();
        if (bool4) {
          ((Collection)localObject3).add(localObject5);
        }
      }
      localObject3 = (Iterable)localObject3;
      paramc = new java/util/ArrayList;
      int n = m.a((Iterable)localObject3, 10);
      paramc.<init>(n);
      paramc = (Collection)paramc;
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        boolean bool6 = ((Iterator)localObject3).hasNext();
        if (!bool6) {
          break;
        }
        localObject5 = (t)((Iterator)localObject3).next();
        localObject6 = new com/truecaller/backup/BackupSettingItem;
        localObject7 = ((t)localObject5).a();
        localObject5 = ((t)localObject5).b();
        ((BackupSettingItem)localObject6).<init>((String)localObject7, localObject5);
        paramc.add(localObject6);
      }
      localObject3 = paramc;
      localObject3 = (List)paramc;
      m = 1;
      localObject5 = new String[m];
      bool4 = false;
      Object localObject7 = new java/lang/StringBuilder;
      ((StringBuilder)localObject7).<init>("Backing up ");
      int i1 = ((List)localObject3).size();
      ((StringBuilder)localObject7).append(i1);
      String str2 = " settings";
      ((StringBuilder)localObject7).append(str2);
      localObject7 = ((StringBuilder)localObject7).toString();
      localObject5[0] = localObject7;
      localObject5 = c;
      Object localObject6 = b.b(localObject3);
      k.a(localObject6, "gson.toJson(settingsAsPairs)");
      localObject7 = d.a;
      if (localObject6 == null) {
        break label859;
      }
      localObject6 = ((String)localObject6).getBytes((Charset)localObject7);
      localObject7 = "(this as java.lang.String).getBytes(charset)";
      k.a(localObject6, (String)localObject7);
      d = this;
      e = localObject4;
      f = str1;
      g = localObject3;
      b = m;
      paramc = ((bc)localObject5).a((String)localObject4, (byte[])localObject6);
      if (paramc == localObject2) {
        return localObject2;
      }
      localObject5 = this;
    }
    paramc = (BackupResult)paramc;
    d = localObject5;
    e = localObject4;
    f = str1;
    g = localObject3;
    h = paramc;
    int k = 2;
    b = k;
    localObject1 = c;
    Object localObject3 = b;
    Object localObject4 = ((by)localObject5).a();
    localObject3 = ((f)localObject3).b(localObject4);
    k.a(localObject3, "gson.toJson(aggregateFilters())");
    localObject4 = d.a;
    if (localObject3 != null)
    {
      localObject3 = ((String)localObject3).getBytes((Charset)localObject4);
      localObject4 = "(this as java.lang.String).getBytes(charset)";
      k.a(localObject3, (String)localObject4);
      localObject1 = ((bc)localObject1).a(str1, (byte[])localObject3);
      if (localObject1 == localObject2) {
        return localObject2;
      }
      Object localObject8 = localObject1;
      localObject1 = paramc;
      paramc = (c)localObject8;
      label800:
      paramc = (BackupResult)paramc;
      localObject2 = BackupResult.Success;
      if (localObject1 == localObject2)
      {
        localObject2 = BackupResult.Success;
        if (paramc == localObject2) {
          return BackupResult.Success;
        }
      }
      localObject2 = BackupResult.Success;
      if (localObject1 != localObject2) {
        return localObject1;
      }
      return paramc;
    }
    paramc = new c/u;
    paramc.<init>("null cannot be cast to non-null type java.lang.String");
    throw paramc;
    label859:
    paramc = new c/u;
    paramc.<init>("null cannot be cast to non-null type java.lang.String");
    throw paramc;
    label872:
    throw a;
  }
  
  public final Object b(c paramc)
  {
    boolean bool1 = paramc instanceof by.e;
    int k;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (by.e)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        k = b - j;
        b = k;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/backup/by$e;
    ((by.e)localObject1).<init>(this, (c)paramc);
    label70:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    int j = b;
    int m = 1;
    String str1;
    String str2;
    by localby;
    switch (j)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 2: 
      bool1 = paramc instanceof o.b;
      if (!bool1) {
        break label610;
      }
      throw a;
    case 1: 
      str1 = (String)f;
      str2 = (String)e;
      localby = (by)d;
      boolean bool3 = paramc instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label648;
      }
      str2 = b();
      if (str2 == null) {
        return BackupResult.ErrorFileName;
      }
      str1 = c();
      if (str1 == null) {
        return BackupResult.ErrorFileName;
      }
      paramc = c;
      d = this;
      e = str2;
      f = str1;
      b = m;
      paramc = paramc.a(str2);
      if (paramc == locala) {
        return locala;
      }
      localby = this;
    }
    paramc = (InputStream)paramc;
    if (paramc == null) {
      return BackupResult.ErrorRead;
    }
    try
    {
      Object localObject2 = b;
      Object localObject3 = new java/io/InputStreamReader;
      ((InputStreamReader)localObject3).<init>(paramc);
      localObject3 = (Reader)localObject3;
      Object localObject4 = new com/truecaller/backup/by$d;
      ((by.d)localObject4).<init>();
      localObject4 = b;
      Object localObject5 = "object : TypeToken<T>() {}.type";
      k.a(localObject4, (String)localObject5);
      localObject2 = ((f)localObject2).a((Reader)localObject3, (Type)localObject4);
      localObject3 = "this.fromJson(json, typeToken<T>())";
      k.a(localObject2, (String)localObject3);
      localObject2 = (List)localObject2;
      Object localObject6 = new String[m];
      boolean bool4 = false;
      localObject3 = null;
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("Restoring ");
      int n = ((List)localObject2).size();
      ((StringBuilder)localObject4).append(n);
      localObject5 = " settings";
      ((StringBuilder)localObject4).append((String)localObject5);
      localObject4 = ((StringBuilder)localObject4).toString();
      localObject6[0] = localObject4;
      localObject6 = localObject2;
      localObject6 = ((Iterable)localObject2).iterator();
      for (;;)
      {
        bool4 = ((Iterator)localObject6).hasNext();
        if (!bool4) {
          break;
        }
        localObject3 = (BackupSettingItem)((Iterator)localObject6).next();
        localObject4 = ((BackupSettingItem)localObject3).component1();
        localObject3 = ((BackupSettingItem)localObject3).component2();
        localObject5 = d.b;
        localObject4 = (t)((Map)localObject5).get(localObject4);
        if (localObject4 != null) {
          ((t)localObject4).b(localObject3);
        }
      }
      d = localby;
      e = str2;
      f = str1;
      g = paramc;
      h = localObject2;
      k = 2;
      b = k;
      paramc = localby.a(str1, (c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      label610:
      return paramc;
    }
    catch (p localp)
    {
      AssertionUtil.report(new String[] { "Cannot parse settings json file" });
      return BackupResult.ErrorJsonParsing;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      AssertionUtil.report(new String[] { "IllegalStateException during parsing settings json file" });
      return BackupResult.ErrorJsonParsing;
    }
    label648:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.by
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */