package com.truecaller.backup;

import c.a.ag;
import c.g.b.k;
import c.k.i;
import c.n;
import c.t;
import com.truecaller.common.g.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class d
{
  public static final d.a b;
  final a a;
  
  static
  {
    d.a locala = new com/truecaller/backup/d$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public d(a parama)
  {
    a = parama;
  }
  
  private static String a(Map paramMap)
  {
    Object localObject1 = new java/util/ArrayList;
    int i = paramMap.size();
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    paramMap = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool = paramMap.hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Map.Entry)paramMap.next();
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      String str = (String)((Map.Entry)localObject2).getKey();
      ((StringBuilder)localObject3).append(str);
      char c = '=';
      ((StringBuilder)localObject3).append(c);
      localObject2 = (String)((Map.Entry)localObject2).getValue();
      ((StringBuilder)localObject3).append((String)localObject2);
      localObject2 = ((StringBuilder)localObject3).toString();
      ((Collection)localObject1).add(localObject2);
    }
    localObject1 = (List)localObject1;
    Object localObject2 = localObject1;
    localObject2 = (Iterable)localObject1;
    Object localObject3 = (CharSequence)",";
    return c.a.m.a((Iterable)localObject2, (CharSequence)localObject3, null, null, 0, null, null, 62);
  }
  
  static Map a(String paramString)
  {
    paramString = (CharSequence)paramString;
    Object localObject1 = { "," };
    int i = 6;
    paramString = (Iterable)c.n.m.c(paramString, (String[])localObject1, false, i);
    int j = ag.a(c.a.m.a(paramString, 10));
    int k = 16;
    j = i.c(j, k);
    Object localObject2 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject2).<init>(j);
    localObject2 = (Map)localObject2;
    paramString = paramString.iterator();
    for (;;)
    {
      boolean bool = paramString.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (CharSequence)paramString.next();
      Object localObject3 = { "=" };
      localObject1 = c.n.m.c((CharSequence)localObject1, (String[])localObject3, false, i);
      localObject3 = (String)((List)localObject1).get(0);
      int m = 1;
      localObject1 = (String)((List)localObject1).get(m);
      localObject1 = t.a(localObject3, localObject1);
      localObject3 = a;
      localObject1 = b;
      ((Map)localObject2).put(localObject3, localObject1);
    }
    return (Map)localObject2;
  }
  
  public final void a(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramString, "backupAction");
    Object localObject1 = new n[3];
    paramString = t.a("backup_action_key", paramString);
    StringBuilder localStringBuilder = null;
    localObject1[0] = paramString;
    Object localObject2 = String.valueOf(paramBoolean1);
    paramString = t.a("backup_file_exists_key", localObject2);
    paramBoolean1 = true;
    localObject1[paramBoolean1] = paramString;
    Object localObject3 = String.valueOf(paramBoolean2);
    paramString = t.a("account_state_valid", localObject3);
    paramBoolean2 = true;
    localObject1[paramBoolean2] = paramString;
    paramString = a(ag.a((n[])localObject1));
    localObject3 = a;
    String str = "";
    localObject3 = ((a)localObject3).b("accountAutobackupLogInfo", str);
    k.a(localObject3, "it");
    localObject1 = localObject3;
    localObject1 = (CharSequence)localObject3;
    int i = ((CharSequence)localObject1).length();
    if (i <= 0)
    {
      paramBoolean1 = false;
      localObject2 = null;
    }
    if (paramBoolean1)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject3);
      ((StringBuilder)localObject2).append(";");
      localObject3 = ((StringBuilder)localObject2).toString();
    }
    localObject2 = a;
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append((String)localObject3);
    localStringBuilder.append(paramString);
    paramString = localStringBuilder.toString();
    ((a)localObject2).a("accountAutobackupLogInfo", paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */