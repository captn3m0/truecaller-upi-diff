package com.truecaller.backup;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import c.d.c;
import c.d.h;
import c.g.a.m;
import c.g.b.k;
import c.l;
import c.n;
import c.o;
import c.o.b;
import c.x;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions.Builder;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet.Builder;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.SearchableMetadataField;
import com.google.android.gms.drive.metadata.SortableMetadataField;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query.Builder;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.drive.query.SortOrder;
import com.google.android.gms.drive.query.SortOrder.Builder;
import com.google.android.gms.drive.query.SortableField;
import com.google.android.gms.tasks.Task;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;
import kotlinx.coroutines.g;

public final class bd
  implements bc
{
  DriveClient a;
  private DriveResourceClient b;
  private c c;
  private final Context d;
  private final com.truecaller.common.g.a e;
  private final c.d.f f;
  private final c.d.f g;
  private final ag h;
  
  public bd(Context paramContext, com.truecaller.common.g.a parama, c.d.f paramf1, c.d.f paramf2, ag paramag)
  {
    d = paramContext;
    e = parama;
    f = paramf1;
    g = paramf2;
    h = paramag;
  }
  
  private final GoogleSignInClient a(Activity paramActivity)
  {
    Object localObject = new com/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;
    ((GoogleSignInOptions.Builder)localObject).<init>();
    Scope localScope = Drive.c;
    Scope[] arrayOfScope1 = new Scope[0];
    localObject = ((GoogleSignInOptions.Builder)localObject).a(localScope, arrayOfScope1);
    localScope = Drive.b;
    Scope[] arrayOfScope2 = new Scope[0];
    localObject = ((GoogleSignInOptions.Builder)localObject).a(localScope, arrayOfScope2).b().d();
    if (paramActivity == null)
    {
      paramActivity = GoogleSignIn.a(d, (GoogleSignInOptions)localObject);
      k.a(paramActivity, "GoogleSignIn.getClient(context, signInOptions)");
      return paramActivity;
    }
    paramActivity = GoogleSignIn.a(paramActivity, (GoogleSignInOptions)localObject);
    k.a(paramActivity, "GoogleSignIn.getClient(activity, signInOptions)");
    return paramActivity;
  }
  
  private final BackupResult a(String paramString, Map paramMap, c.g.a.b paramb)
  {
    int i = 1;
    Object localObject1 = new String[i];
    String str1 = String.valueOf(paramString);
    Object localObject2 = "Writing to file with title: ".concat(str1);
    str1 = null;
    localObject1[0] = localObject2;
    localObject1 = b;
    if (localObject1 == null) {
      return BackupResult.ErrorClient;
    }
    localObject2 = d(paramString);
    x localx = null;
    Object localObject3;
    Object localObject4;
    if (localObject2 == null)
    {
      localObject2 = new String[i];
      localObject3 = String.valueOf(paramString);
      localObject4 = "Creating file with title: ".concat((String)localObject3);
      localObject2[0] = localObject4;
      localObject2 = d();
      if (localObject2 == null)
      {
        localObject2 = null;
      }
      else
      {
        localObject4 = new com/google/android/gms/drive/MetadataChangeSet$Builder;
        ((MetadataChangeSet.Builder)localObject4).<init>();
        paramString = ((MetadataChangeSet.Builder)localObject4).b(paramString).a("application/json").a();
        localObject4 = b;
        if (localObject4 != null)
        {
          paramString = ((DriveResourceClient)localObject4).createFile((DriveFolder)localObject2, paramString, null);
          if (paramString != null)
          {
            paramString = a(paramString);
            localObject2 = paramString;
            localObject2 = (DriveFile)paramString;
            break label180;
          }
        }
        localObject2 = null;
      }
    }
    label180:
    if (localObject2 == null) {
      return BackupResult.ErrorFile;
    }
    if (paramMap != null)
    {
      paramString = new com/google/android/gms/drive/MetadataChangeSet$Builder;
      paramString.<init>();
      paramMap = ((Iterable)paramMap.entrySet()).iterator();
      for (;;)
      {
        boolean bool = paramMap.hasNext();
        if (!bool) {
          break;
        }
        localObject4 = (Map.Entry)paramMap.next();
        localObject3 = new com/google/android/gms/drive/metadata/CustomPropertyKey;
        String str2 = (String)((Map.Entry)localObject4).getKey();
        ((CustomPropertyKey)localObject3).<init>(str2, i);
        localObject4 = (String)((Map.Entry)localObject4).getValue();
        paramString.a((CustomPropertyKey)localObject3, (String)localObject4);
      }
      paramString = paramString.a();
    }
    else
    {
      paramString = null;
    }
    int j = 536870912;
    paramMap = ((DriveResourceClient)localObject1).openFile((DriveFile)localObject2, j);
    if (paramMap != null)
    {
      paramMap = (DriveContents)a(paramMap);
      if (paramMap != null) {
        try
        {
          localObject2 = paramMap.getOutputStream();
          localObject4 = "driveContents.outputStream";
          k.a(localObject2, (String)localObject4);
          paramb.invoke(localObject2);
          paramString = ((DriveResourceClient)localObject1).commitContents(paramMap, paramString);
          if (paramString != null) {
            localx = b(paramString);
          }
          if (localx == null) {
            i = 0;
          }
          if (i != 0) {
            return BackupResult.Success;
          }
          return BackupResult.ErrorCommit;
        }
        catch (IOException localIOException)
        {
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localIOException);
          return BackupResult.ErrorWrite;
        }
      }
    }
    return BackupResult.ErrorOpen;
  }
  
  private final InputStream a(DriveFile paramDriveFile)
  {
    DriveResourceClient localDriveResourceClient = b;
    if (localDriveResourceClient == null) {
      return null;
    }
    int i = 268435456;
    paramDriveFile = localDriveResourceClient.openFile(paramDriveFile, i);
    if (paramDriveFile != null)
    {
      paramDriveFile = (DriveContents)a(paramDriveFile);
      if (paramDriveFile != null)
      {
        paramDriveFile = paramDriveFile.getInputStream();
        if (paramDriveFile != null) {
          return paramDriveFile;
        }
      }
    }
    return null;
  }
  
  private final Object a(Task paramTask)
  {
    Object localObject = new com/truecaller/backup/bd$a;
    ((bd.a)localObject).<init>(this);
    localObject = (c.g.a.b)localObject;
    return cf.a(paramTask, (c.g.a.b)localObject);
  }
  
  private final boolean a(GoogleSignInAccount paramGoogleSignInAccount, Activity paramActivity)
  {
    Object localObject;
    if (paramActivity != null) {
      localObject = paramActivity;
    }
    label19:
    boolean bool;
    try
    {
      localObject = (Context)paramActivity;
      break label19;
      localObject = d;
      localObject = Drive.a((Context)localObject, paramGoogleSignInAccount);
      a = ((DriveClient)localObject);
      if (paramActivity != null) {
        paramActivity = (Context)paramActivity;
      } else {
        paramActivity = d;
      }
      paramGoogleSignInAccount = Drive.b(paramActivity, paramGoogleSignInAccount);
      b = paramGoogleSignInAccount;
      bool = true;
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localException);
      bool = false;
      paramGoogleSignInAccount = null;
    }
    return bool;
  }
  
  private final x b(Task paramTask)
  {
    Object localObject = new com/truecaller/backup/bd$b;
    ((bd.b)localObject).<init>(this);
    localObject = (c.g.a.b)localObject;
    return cf.b(paramTask, (c.g.a.b)localObject);
  }
  
  private final DriveFile d(String paramString)
  {
    Object localObject1 = d();
    if (localObject1 == null) {
      return null;
    }
    Object localObject2 = new com/google/android/gms/drive/query/SortOrder$Builder;
    ((SortOrder.Builder)localObject2).<init>();
    Object localObject3 = SortableField.c;
    localObject2 = ((SortOrder.Builder)localObject2).a((SortableMetadataField)localObject3).a();
    localObject3 = new com/google/android/gms/drive/query/Query$Builder;
    ((Query.Builder)localObject3).<init>();
    SearchableMetadataField localSearchableMetadataField = SearchableField.a;
    paramString = Filters.a(localSearchableMetadataField, paramString);
    paramString = ((Query.Builder)localObject3).a(paramString).a((SortOrder)localObject2).a();
    localObject2 = b;
    if (localObject2 != null)
    {
      paramString = ((DriveResourceClient)localObject2).queryChildren((DriveFolder)localObject1, paramString);
      if (paramString != null)
      {
        paramString = (MetadataBuffer)a(paramString);
        if (paramString != null)
        {
          int i = paramString.getCount();
          if (i == 0) {
            return null;
          }
          i = 0;
          paramString = paramString.a(0);
          localObject1 = "metadataBuffer[0]";
          k.a(paramString, (String)localObject1);
          paramString = paramString.getDriveId();
          if (paramString != null) {
            return paramString.a();
          }
          return null;
        }
      }
    }
    return null;
  }
  
  private final DriveFolder d()
  {
    Object localObject = e;
    boolean bool1 = ((com.truecaller.common.g.a)localObject).b("backupForceRootFolder");
    boolean bool2 = true;
    if (bool1 == bool2)
    {
      localObject = b;
      if (localObject != null)
      {
        localObject = ((DriveResourceClient)localObject).getRootFolder();
        if (localObject != null) {
          return (DriveFolder)a((Task)localObject);
        }
      }
      return null;
    }
    if (!bool1)
    {
      localObject = b;
      if (localObject != null)
      {
        localObject = ((DriveResourceClient)localObject).getAppFolder();
        if (localObject != null) {
          return (DriveFolder)a((Task)localObject);
        }
      }
      return null;
    }
    localObject = new c/l;
    ((l)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final Object a(Fragment paramFragment, c paramc)
  {
    boolean bool1 = paramc instanceof bd.e;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (bd.e)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label77;
      }
    }
    Object localObject1 = new com/truecaller/backup/bd$e;
    ((bd.e)localObject1).<init>(this, (c)paramc);
    label77:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    int j = b;
    switch (j)
    {
    default: 
      paramFragment = new java/lang/IllegalStateException;
      paramFragment.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramFragment;
    case 1: 
      paramFragment = (android.support.v4.app.f)g;
      localObject1 = (bd)d;
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool3 = paramc instanceof o.b;
      if (bool3) {
        break label652;
      }
      paramc = GoogleSignIn.a(d);
      bool3 = false;
      if (paramFragment == null)
      {
        if (paramc == null) {
          return Boolean.FALSE;
        }
        return Boolean.valueOf(a(paramc, null));
      }
      android.support.v4.app.f localf = paramFragment.getActivity();
      if (localf == null) {
        return Boolean.FALSE;
      }
      k.a(localf, "parentFragment.activity ?: return false");
      Object localObject2 = GoogleApiAvailability.a();
      Object localObject3 = localf;
      localObject3 = (Context)localf;
      int m = ((GoogleApiAvailability)localObject2).a((Context)localObject3);
      int n = 2;
      if (m != 0)
      {
        paramFragment = (kotlinx.coroutines.ag)bg.a;
        paramc = f;
        localObject1 = new com/truecaller/backup/bd$f;
        ((bd.f)localObject1).<init>(localf, m, null);
        localObject1 = (m)localObject1;
        e.b(paramFragment, paramc, (m)localObject1, n);
        return Boolean.FALSE;
      }
      Object localObject4 = localf;
      localObject4 = (Activity)localf;
      localObject4 = a((Activity)localObject4);
      Object localObject5 = e;
      boolean bool4 = ((com.truecaller.common.g.a)localObject5).b("backupSignInRequired");
      Object localObject6 = null;
      int i1 = 1;
      Object localObject7;
      if (!bool4)
      {
        localObject5 = new Scope[i1];
        localObject7 = Drive.c;
        localObject5[0] = localObject7;
        bool4 = GoogleSignIn.a(paramc, (Scope[])localObject5);
        if (bool4)
        {
          bool4 = false;
          localObject5 = null;
          break label416;
        }
      }
      bool4 = true;
      label416:
      if ((paramc == null) || (bool4))
      {
        localObject5 = e;
        localObject7 = "backupSignInRequired";
        ((com.truecaller.common.g.a)localObject5).b((String)localObject7, false);
        d = this;
        e = paramFragment;
        f = paramc;
        g = localf;
        i = m;
        h = localObject4;
        b = i1;
        paramc = new c/d/h;
        localObject2 = c.d.a.b.a((c)localObject1);
        paramc.<init>((c)localObject2);
        localObject2 = paramc;
        localObject2 = (c)paramc;
        c = ((c)localObject2);
        localObject2 = (kotlinx.coroutines.ag)bg.a;
        localObject5 = f;
        localObject6 = new com/truecaller/backup/bd$d;
        ((bd.d)localObject6).<init>(null, this, paramFragment, (GoogleSignInClient)localObject4);
        localObject6 = (m)localObject6;
        e.b((kotlinx.coroutines.ag)localObject2, (c.d.f)localObject5, (m)localObject6, n);
        paramFragment = paramc.b();
        paramc = c.d.a.a.a;
        if (paramFragment == paramc)
        {
          paramc = "frame";
          k.b(localObject1, paramc);
        }
        if (paramFragment == locala) {
          return locala;
        }
      }
      localObject1 = this;
      paramFragment = localf;
    }
    paramc = paramFragment;
    paramc = GoogleSignIn.a((Context)paramFragment);
    if (paramc == null) {
      return Boolean.FALSE;
    }
    k.a(paramc, "GoogleSignIn.getLastSign…activity) ?: return false");
    paramFragment = (Activity)paramFragment;
    return Boolean.valueOf(((bd)localObject1).a(paramc, paramFragment));
    label652:
    throw a;
  }
  
  public final Object a(c paramc)
  {
    c.d.f localf = g;
    Object localObject = new com/truecaller/backup/bd$c;
    ((bd.c)localObject).<init>(this, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object a(String paramString)
  {
    int i = 1;
    String[] arrayOfString = new String[i];
    String str1 = String.valueOf(paramString);
    String str2 = "Reading file with title: ".concat(str1);
    str1 = null;
    arrayOfString[0] = str2;
    paramString = d(paramString);
    if (paramString == null) {
      return null;
    }
    return a(paramString);
  }
  
  public final Object a(String paramString, InputStream paramInputStream, Map paramMap)
  {
    Object localObject = new com/truecaller/backup/bd$h;
    ((bd.h)localObject).<init>(paramInputStream);
    localObject = (c.g.a.b)localObject;
    return a(paramString, paramMap, (c.g.a.b)localObject);
  }
  
  public final Object a(String paramString, byte[] paramArrayOfByte)
  {
    Object localObject = new com/truecaller/backup/bd$g;
    ((bd.g)localObject).<init>(paramArrayOfByte);
    localObject = (c.g.a.b)localObject;
    return a(paramString, null, (c.g.a.b)localObject);
  }
  
  public final void a()
  {
    c();
  }
  
  public final Object b()
  {
    Object localObject = h;
    BackupFile localBackupFile = BackupFile.CALL_LOG;
    localObject = ((ag)localObject).a(localBackupFile);
    if (localObject == null) {
      return c.d.b.a.b.a(0L);
    }
    return c((String)localObject);
  }
  
  public final Object b(Fragment paramFragment, c paramc)
  {
    Object localObject = GoogleSignIn.a(d);
    if (localObject != null)
    {
      localObject = a(null).b();
      if (localObject != null) {
        b((Task)localObject);
      }
    }
    return a(paramFragment, paramc);
  }
  
  public final Object b(String paramString)
  {
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = String.valueOf(paramString);
    Object localObject3 = "Reading file with title: ".concat((String)localObject2);
    int j = 0;
    localObject2 = null;
    localObject1[0] = localObject3;
    localObject1 = b;
    localObject3 = null;
    if (localObject1 == null) {
      return null;
    }
    paramString = d(paramString);
    if (paramString == null) {
      return null;
    }
    localObject2 = paramString;
    localObject2 = (DriveResource)paramString;
    localObject1 = ((DriveResourceClient)localObject1).getMetadata((DriveResource)localObject2);
    localObject2 = "driveResourceClient.getMetadata(driveFile)";
    k.a(localObject1, (String)localObject2);
    localObject1 = (Metadata)a((Task)localObject1);
    if (localObject1 == null) {
      return null;
    }
    paramString = a(paramString);
    if (paramString == null) {
      return null;
    }
    localObject1 = ((Metadata)localObject1).getCustomProperties();
    k.a(localObject1, "metadata.customProperties");
    localObject3 = new java/util/LinkedHashMap;
    j = c.a.ag.a(((Map)localObject1).size());
    ((LinkedHashMap)localObject3).<init>(j);
    localObject3 = (Map)localObject3;
    localObject1 = ((Iterable)((Map)localObject1).entrySet()).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      Object localObject4 = ((Map.Entry)localObject2).getKey();
      String str = "it.key";
      k.a(localObject4, str);
      localObject4 = ((CustomPropertyKey)localObject4).a();
      localObject2 = ((Map.Entry)localObject2).getValue();
      ((Map)localObject3).put(localObject4, localObject2);
    }
    localObject1 = new c/n;
    ((n)localObject1).<init>(paramString, localObject3);
    return localObject1;
  }
  
  public final Object c(String paramString)
  {
    int i = 1;
    Object localObject = new String[i];
    String str1 = String.valueOf(paramString);
    String str2 = "Finding last modified time for: ".concat(str1);
    str1 = null;
    localObject[0] = str2;
    localObject = b;
    long l1 = 0L;
    if (localObject == null) {
      return c.d.b.a.b.a(l1);
    }
    paramString = d(paramString);
    if (paramString == null) {
      return c.d.b.a.b.a(l1);
    }
    paramString = (DriveResource)paramString;
    paramString = ((DriveResourceClient)localObject).getMetadata(paramString);
    if (paramString != null)
    {
      paramString = (Metadata)a(paramString);
      if (paramString != null)
      {
        paramString = paramString.getModifiedDate();
        if (paramString != null)
        {
          long l2 = paramString.getTime();
          paramString = c.d.b.a.b.a(l2);
          l1 = paramString.longValue();
        }
      }
    }
    return c.d.b.a.b.a(l1);
  }
  
  final void c()
  {
    c localc = c;
    if (localc != null)
    {
      Object localObject = x.a;
      localObject = o.d(localObject);
      localc.b(localObject);
    }
    c = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bd
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */