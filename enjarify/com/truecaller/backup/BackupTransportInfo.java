package com.truecaller.backup;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import com.truecaller.messaging.data.types.TransportInfo;
import org.a.a.b;

public final class BackupTransportInfo
  implements TransportInfo
{
  public static final Parcelable.Creator CREATOR;
  private final long a;
  
  static
  {
    BackupTransportInfo.a locala = new com/truecaller/backup/BackupTransportInfo$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public BackupTransportInfo(long paramLong)
  {
    a = paramLong;
  }
  
  public final int a()
  {
    return 0;
  }
  
  public final String a(b paramb)
  {
    k.b(paramb, "date");
    return "";
  }
  
  public final int b()
  {
    return 0;
  }
  
  public final long c()
  {
    return a;
  }
  
  public final long d()
  {
    return 0L;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final long e()
  {
    return 0L;
  }
  
  public final boolean f()
  {
    return false;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    long l = a;
    paramParcel.writeLong(l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupTransportInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */