package com.truecaller.backup;

import android.support.v4.app.Fragment;
import com.truecaller.bm;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bn;

public abstract interface w$a
  extends bm
{
  public abstract ao a(Fragment paramFragment, String paramString);
  
  public abstract ao a(Fragment paramFragment, boolean paramBoolean);
  
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(long paramLong);
  
  public abstract void a(Fragment paramFragment);
  
  public abstract void b();
  
  public abstract void b(int paramInt);
  
  public abstract void c();
  
  public abstract bn e();
}

/* Location:
 * Qualified Name:     com.truecaller.backup.w.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */