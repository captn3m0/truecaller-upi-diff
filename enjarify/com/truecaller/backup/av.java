package com.truecaller.backup;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Patterns;
import c.a.aa;
import c.a.ab;
import c.o.b;
import c.u;
import com.google.gson.f;
import com.google.gson.p;
import com.truecaller.analytics.b;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Link;
import com.truecaller.data.entity.Number;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.z;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class av
  implements au
{
  private final Uri a;
  private final Uri b;
  private final Uri c;
  private final String d;
  private final String e;
  private final String f;
  private final List g;
  private final c.n.k h;
  private final Context i;
  private final com.truecaller.data.access.m j;
  private final bc k;
  private final b l;
  private final f m;
  private final ag n;
  
  public av(Context paramContext, com.truecaller.data.access.m paramm, bc parambc, b paramb, f paramf, ag paramag)
  {
    i = paramContext;
    j = paramm;
    k = parambc;
    l = paramb;
    m = paramf;
    n = paramag;
    paramContext = TruecallerContract.a.a();
    a = paramContext;
    paramContext = TruecallerContract.ah.a();
    b = paramContext;
    paramContext = TruecallerContract.ah.b();
    c = paramContext;
    d = "contact_name NOT NULL AND contact_name NOT IN ('', 'Truecaller Verification') AND contact_default_number NOT NULL";
    e = "tc_id DESC, contact_source DESC";
    f = "contact_name = ? AND tc_flag = ?";
    paramContext = new Integer[3];
    paramm = Integer.valueOf(16);
    paramContext[0] = paramm;
    int i1 = 2;
    parambc = Integer.valueOf(i1);
    paramContext[1] = parambc;
    parambc = Integer.valueOf(32);
    paramContext[i1] = parambc;
    paramContext = c.a.m.b(paramContext);
    g = paramContext;
    paramContext = Patterns.PHONE;
    c.g.b.k.a(paramContext, "Patterns.PHONE");
    paramm = new c/n/k;
    paramm.<init>(paramContext);
    h = paramm;
  }
  
  private final ContentProviderOperation a(Uri paramUri, String paramString, int paramInt)
  {
    paramUri = ContentProviderOperation.newUpdate(paramUri);
    Object localObject = Integer.valueOf(paramInt);
    paramUri.withValue("tc_flag", localObject);
    localObject = f;
    String[] arrayOfString = new String[2];
    arrayOfString[0] = paramString;
    arrayOfString[1] = "0";
    paramUri.withSelection((String)localObject, arrayOfString);
    paramUri = paramUri.build();
    c.g.b.k.a(paramUri, "rawUpdate.build()");
    return paramUri;
  }
  
  private final List a()
  {
    ContentResolver localContentResolver = i.getContentResolver();
    Uri localUri = c;
    String str1 = d;
    String str2 = e;
    List localList = z.a(localContentResolver.query(localUri, null, str1, null, str2));
    boolean bool = localList.isEmpty();
    if (!bool) {
      return localList;
    }
    return null;
  }
  
  private final Set a(List paramList)
  {
    if (paramList == null) {
      return null;
    }
    paramList = (Iterable)paramList;
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramList = paramList.iterator();
    boolean bool1;
    int i2;
    Object localObject2;
    Object localObject3;
    int i4;
    Object localObject4;
    boolean bool3;
    for (;;)
    {
      bool1 = paramList.hasNext();
      int i1 = 0;
      i2 = 1;
      if (!bool1) {
        break;
      }
      localObject2 = paramList.next();
      localObject3 = localObject2;
      localObject3 = (Contact)localObject2;
      int i3 = ((Contact)localObject3).getSource();
      i4 = 2;
      if (i3 != i4)
      {
        boolean bool2 = ((Contact)localObject3).U();
        if (!bool2)
        {
          localObject3 = ((Contact)localObject3).z();
          if (localObject3 != null)
          {
            localObject3 = (CharSequence)localObject3;
            localObject4 = h;
            bool3 = ((c.n.k)localObject4).a((CharSequence)localObject3);
            localObject3 = Boolean.valueOf(bool3);
          }
          else
          {
            bool3 = false;
            localObject3 = null;
          }
          bool3 = com.truecaller.utils.extensions.c.a((Boolean)localObject3);
          if (!bool3) {}
        }
        else
        {
          i1 = 1;
        }
      }
      if (i1 == 0) {
        ((Collection)localObject1).add(localObject2);
      }
    }
    localObject1 = (Iterable)localObject1;
    paramList = new java/util/ArrayList;
    paramList.<init>();
    paramList = (Collection)paramList;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Contact)((Iterator)localObject1).next();
      localObject4 = ((Contact)localObject2).z();
      ContactBackupItem localContactBackupItem;
      if (localObject4 == null)
      {
        localContactBackupItem = null;
      }
      else
      {
        c.g.b.k.a(localObject4, "contact.name ?: return@mapNotNull null");
        localObject3 = ((Contact)localObject2).A();
        c.g.b.k.a(localObject3, "contact.numbers");
        localObject3 = (Iterable)localObject3;
        Object localObject5 = new java/util/ArrayList;
        int i6 = c.a.m.a((Iterable)localObject3, 10);
        ((ArrayList)localObject5).<init>(i6);
        localObject5 = (Collection)localObject5;
        localObject3 = ((Iterable)localObject3).iterator();
        for (;;)
        {
          boolean bool4 = ((Iterator)localObject3).hasNext();
          if (!bool4) {
            break;
          }
          localObject6 = (Number)((Iterator)localObject3).next();
          localObject7 = "it";
          c.g.b.k.a(localObject6, (String)localObject7);
          localObject6 = ((Number)localObject6).a();
          ((Collection)localObject5).add(localObject6);
        }
        localObject3 = c.a.m.i((Iterable)localObject5);
        localObject5 = ((Contact)localObject2).y();
        c.g.b.k.a(localObject5, "contact.links");
        localObject5 = (Iterable)localObject5;
        Object localObject6 = new java/util/ArrayList;
        ((ArrayList)localObject6).<init>();
        localObject6 = (Collection)localObject6;
        localObject5 = ((Iterable)localObject5).iterator();
        for (;;)
        {
          boolean bool5 = ((Iterator)localObject5).hasNext();
          if (!bool5) {
            break;
          }
          localObject7 = ((Iterator)localObject5).next();
          localObject8 = localObject7;
          localObject8 = (Link)localObject7;
          localObject9 = "it";
          c.g.b.k.a(localObject8, (String)localObject9);
          localObject8 = (CharSequence)((Link)localObject8).getInfo();
          if (localObject8 != null)
          {
            i8 = ((CharSequence)localObject8).length();
            if (i8 != 0)
            {
              i8 = 0;
              localObject8 = null;
              break label518;
            }
          }
          int i8 = 1;
          label518:
          i8 ^= i2;
          if (i8 != 0) {
            ((Collection)localObject6).add(localObject7);
          }
        }
        localObject5 = c.a.m.i((Iterable)localObject6);
        localObject6 = ((Contact)localObject2).d();
        c.g.b.k.a(localObject6, "contact.addresses");
        localObject6 = (Iterable)localObject6;
        Object localObject7 = new java/util/ArrayList;
        ((ArrayList)localObject7).<init>();
        localObject7 = (Collection)localObject7;
        localObject6 = ((Iterable)localObject6).iterator();
        Object localObject10;
        int i9;
        for (;;)
        {
          bool6 = ((Iterator)localObject6).hasNext();
          if (!bool6) {
            break;
          }
          localObject8 = ((Iterator)localObject6).next();
          localObject9 = localObject8;
          localObject9 = (Address)localObject8;
          c.g.b.k.a(localObject9, "it");
          localObject9 = ((Address)localObject9).getDisplayableAddress();
          localObject10 = "it.displayableAddress";
          c.g.b.k.a(localObject9, (String)localObject10);
          localObject9 = (CharSequence)localObject9;
          i9 = ((CharSequence)localObject9).length();
          if (i9 > 0)
          {
            i9 = 1;
          }
          else
          {
            i9 = 0;
            localObject9 = null;
          }
          if (i9 != 0) {
            ((Collection)localObject7).add(localObject8);
          }
        }
        localObject7 = (Iterable)localObject7;
        localObject6 = c.a.m.i((Iterable)localObject7);
        int i7 = ((Contact)localObject2).getSource();
        boolean bool6 = ((Set)localObject3).isEmpty();
        if (bool6)
        {
          bool6 = false;
          localObject8 = null;
        }
        else
        {
          localObject8 = localObject3;
        }
        bool3 = ((Set)localObject5).isEmpty();
        if (bool3)
        {
          i9 = 0;
          localObject9 = null;
        }
        else
        {
          localObject9 = localObject5;
        }
        bool3 = ((Set)localObject6).isEmpty();
        if (bool3) {
          localObject10 = null;
        } else {
          localObject10 = localObject6;
        }
        localObject3 = (CharSequence)((Contact)localObject2).v();
        if (localObject3 != null)
        {
          i5 = ((CharSequence)localObject3).length();
          if (i5 != 0)
          {
            i5 = 0;
            localObject3 = null;
            break label861;
          }
        }
        int i5 = 1;
        label861:
        if (i5 != 0)
        {
          bool1 = false;
          localObject2 = null;
        }
        else
        {
          localObject2 = ((Contact)localObject2).v();
        }
        localContactBackupItem = new com/truecaller/backup/ContactBackupItem;
        localObject3 = localContactBackupItem;
        i4 = i7;
        localObject6 = localObject8;
        localObject7 = localObject9;
        Object localObject8 = localObject10;
        Object localObject9 = localObject2;
        localContactBackupItem.<init>((String)localObject4, i7, (Set)localObject6, (Set)localObject7, (Set)localObject10, (String)localObject2);
      }
      if (localContactBackupItem != null) {
        paramList.add(localContactBackupItem);
      }
    }
    return (Set)c.a.m.a((Iterable)paramList);
  }
  
  private final void a(Set paramSet, int paramInt)
  {
    new String[1][0] = "Updating flags in local database.";
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramSet = ((Iterable)paramSet).iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (ContactBackupItem)paramSet.next();
      Object localObject2 = a;
      c.g.b.k.a(localObject2, "aggregatedUri");
      String str = ((ContactBackupItem)localObject1).getName();
      localObject2 = a((Uri)localObject2, str, paramInt);
      localArrayList.add(localObject2);
      localObject2 = b;
      str = "rawUri";
      c.g.b.k.a(localObject2, str);
      localObject1 = ((ContactBackupItem)localObject1).getName();
      localObject1 = a((Uri)localObject2, (String)localObject1, paramInt);
      localArrayList.add(localObject1);
    }
    a(localArrayList);
  }
  
  private final boolean a(ArrayList paramArrayList)
  {
    boolean bool = true;
    try
    {
      Object localObject1 = i;
      localObject1 = ((Context)localObject1).getContentResolver();
      Object localObject2 = TruecallerContract.a();
      paramArrayList = ((ContentResolver)localObject1).applyBatch((String)localObject2, paramArrayList);
      localObject1 = new String[bool];
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      int i1 = paramArrayList.length;
      ((StringBuilder)localObject2).append(i1);
      paramArrayList = " contacts updated";
      ((StringBuilder)localObject2).append(paramArrayList);
      paramArrayList = ((StringBuilder)localObject2).toString();
      localObject1[0] = paramArrayList;
    }
    catch (Exception localException)
    {
      paramArrayList = (Throwable)localException;
      com.truecaller.log.d.a(paramArrayList);
      bool = false;
    }
    return bool;
  }
  
  private final String b()
  {
    ag localag = n;
    BackupFile localBackupFile = BackupFile.CONTACTS;
    return localag.a(localBackupFile);
  }
  
  public final Object a(c.d.c paramc)
  {
    boolean bool1 = paramc instanceof av.b;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (av.b)paramc;
      int i1 = b;
      i2 = -1 << -1;
      i1 &= i2;
      if (i1 != 0)
      {
        int i3 = b - i2;
        b = i3;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/backup/av$b;
    ((av.b)localObject1).<init>(this, (c.d.c)paramc);
    label70:
    paramc = a;
    Object localObject2 = c.d.a.a.a;
    int i2 = b;
    int i4 = 1;
    boolean bool2;
    switch (i2)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 1: 
      localObject2 = (Set)g;
      localObject1 = (av)d;
      bool2 = paramc instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool2 = paramc instanceof o.b;
      if (bool2) {
        break label501;
      }
      new String[1][0] = "Backup contacts started";
      paramc = b();
      if (paramc == null) {
        return BackupResult.ErrorFileName;
      }
      localObject3 = a();
      if (localObject3 == null) {
        return BackupResult.ErrorDatabase;
      }
      Set localSet = a((List)localObject3);
      if (localSet == null) {
        return BackupResult.ErrorDatabase;
      }
      Object localObject4 = new String[i4];
      Object localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>("Backing up ");
      int i5 = localSet.size();
      ((StringBuilder)localObject5).append(i5);
      ((StringBuilder)localObject5).append(" contacts");
      localObject5 = ((StringBuilder)localObject5).toString();
      localObject4[0] = localObject5;
      localObject4 = m;
      localObject5 = new com/truecaller/backup/av$a;
      ((av.a)localObject5).<init>();
      localObject5 = b;
      c.g.b.k.a(localObject5, "object : TypeToken<T>() {}.type");
      localObject4 = ((f)localObject4).a(localSet, (Type)localObject5);
      c.g.b.k.a(localObject4, "this.toJson(src, typeToken<T>())");
      localObject5 = k;
      Object localObject6 = c.n.d.a;
      if (localObject4 == null) {
        break label488;
      }
      localObject6 = ((String)localObject4).getBytes((Charset)localObject6);
      String str = "(this as java.lang.String).getBytes(charset)";
      c.g.b.k.a(localObject6, str);
      d = this;
      e = paramc;
      f = localObject3;
      g = localSet;
      h = localObject4;
      b = i4;
      paramc = ((bc)localObject5).a(paramc, (byte[])localObject6);
      if (paramc == localObject2) {
        return localObject2;
      }
      localObject1 = this;
      localObject2 = localSet;
    }
    paramc = (BackupResult)paramc;
    Object localObject3 = BackupResult.Success;
    if (paramc == localObject3) {
      ((av)localObject1).a((Set)localObject2, i4);
    }
    localObject1 = new String[i4];
    localObject3 = String.valueOf(paramc);
    localObject2 = "Backup done: ".concat((String)localObject3);
    localObject1[0] = localObject2;
    return paramc;
    label488:
    paramc = new c/u;
    paramc.<init>("null cannot be cast to non-null type java.lang.String");
    throw paramc;
    label501:
    throw a;
  }
  
  public final Object b(c.d.c paramc)
  {
    boolean bool1 = paramc instanceof av.e;
    int i1;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (av.e)paramc;
      i1 = b;
      i2 = -1 << -1;
      i1 &= i2;
      if (i1 != 0)
      {
        int i3 = b - i2;
        b = i3;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/backup/av$e;
    ((av.e)localObject1).<init>(this, (c.d.c)paramc);
    label70:
    paramc = a;
    Object localObject2 = c.d.a.a.a;
    int i2 = b;
    int i4 = 1;
    switch (i2)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 1: 
      localObject2 = (Set)f;
      localObject1 = (av)d;
      bool2 = paramc instanceof o.b;
      if (!bool2) {
        break;
      }
    }
    try
    {
      paramc = (o.b)paramc;
      paramc = a;
      throw paramc;
    }
    catch (p localp)
    {
      Object localObject3;
      Object localObject4;
      Object localObject5;
      boolean bool3;
      Object localObject6;
      Object localObject7;
      boolean bool8;
      Object localObject8;
      int i9;
      int i6;
      AssertionUtil.report(new String[] { "Cannot parse contacts json file" });
      return BackupResult.ErrorJsonParsing;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      label1121:
      AssertionUtil.report(new String[] { "IllegalStateException during parsing contacts json file" });
      return BackupResult.ErrorJsonParsing;
    }
    boolean bool2 = paramc instanceof o.b;
    if (!bool2)
    {
      new String[1][0] = "Restore contacts started";
      paramc = b();
      if (paramc == null) {
        return BackupResult.ErrorFileName;
      }
      localObject3 = a();
      localObject3 = a((List)localObject3);
      if (localObject3 == null) {
        localObject3 = (Set)aa.a;
      }
      d = this;
      e = paramc;
      f = localObject3;
      b = i4;
      paramc = a(paramc, (c.d.c)localObject1);
      if (paramc == localObject2) {
        return localObject2;
      }
      localObject1 = this;
      localObject2 = localObject3;
      paramc = (Set)paramc;
      if (paramc == null) {
        return BackupResult.ErrorRead;
      }
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject4 = paramc;
      localObject4 = (Iterable)paramc;
      localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>();
      localObject5 = (Collection)localObject5;
      localObject4 = ((Iterable)localObject4).iterator();
      for (;;)
      {
        bool3 = ((Iterator)localObject4).hasNext();
        if (!bool3) {
          break;
        }
        localObject6 = ((Iterator)localObject4).next();
        localObject7 = localObject6;
        localObject7 = (ContactBackupItem)localObject6;
        boolean bool5 = ((Set)localObject2).contains(localObject7) ^ i4;
        if (bool5) {
          ((Collection)localObject5).add(localObject6);
        }
      }
      localObject5 = (Iterable)localObject5;
      localObject2 = ((Iterable)localObject5).iterator();
      for (;;)
      {
        bool8 = ((Iterator)localObject2).hasNext();
        if (!bool8) {
          break;
        }
        localObject4 = (ContactBackupItem)((Iterator)localObject2).next();
        localObject5 = new com/truecaller/data/entity/Contact;
        ((Contact)localObject5).<init>();
        localObject6 = ((ContactBackupItem)localObject4).getName();
        ((Contact)localObject5).l((String)localObject6);
        localObject6 = ((ContactBackupItem)localObject4).getAvatarUrl();
        ((Contact)localObject5).j((String)localObject6);
        ((Contact)localObject5).ae();
        localObject6 = g;
        int i7 = ((ContactBackupItem)localObject4).getSource();
        localObject7 = Integer.valueOf(i7);
        bool3 = ((List)localObject6).contains(localObject7);
        int i5;
        if (bool3) {
          i5 = 32;
        } else {
          i5 = 1;
        }
        ((Contact)localObject5).setSource(i5);
        localObject6 = ((ContactBackupItem)localObject4).getNumbers();
        boolean bool6;
        if (localObject6 != null)
        {
          localObject6 = ((Iterable)localObject6).iterator();
          for (;;)
          {
            bool6 = ((Iterator)localObject6).hasNext();
            if (!bool6) {
              break;
            }
            localObject7 = (String)((Iterator)localObject6).next();
            localObject8 = new com/truecaller/data/entity/Number;
            ((Number)localObject8).<init>((String)localObject7);
            ((Contact)localObject5).a((Number)localObject8);
          }
        }
        localObject6 = ((ContactBackupItem)localObject4).getEmails();
        if (localObject6 != null)
        {
          localObject6 = ((Iterable)localObject6).iterator();
          for (;;)
          {
            bool6 = ((Iterator)localObject6).hasNext();
            if (!bool6) {
              break;
            }
            localObject7 = (Link)((Iterator)localObject6).next();
            ((Contact)localObject5).a((Link)localObject7);
          }
        }
        localObject4 = ((ContactBackupItem)localObject4).getAddresses();
        if (localObject4 != null)
        {
          localObject4 = ((Iterable)localObject4).iterator();
          for (;;)
          {
            boolean bool4 = ((Iterator)localObject4).hasNext();
            if (!bool4) {
              break;
            }
            localObject6 = (Address)((Iterator)localObject4).next();
            ((Contact)localObject5).a((Address)localObject6);
          }
        }
        ((ArrayList)localObject3).add(localObject5);
      }
      localObject2 = new String[i4];
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("Restoring ");
      i9 = ((ArrayList)localObject3).size();
      ((StringBuilder)localObject4).append(i9);
      ((StringBuilder)localObject4).append(" entries");
      localObject4 = ((StringBuilder)localObject4).toString();
      i9 = 0;
      localObject5 = null;
      localObject2[0] = localObject4;
      localObject2 = c.a.m.j((Iterable)localObject3);
      localObject3 = new java/util/LinkedHashMap;
      ((LinkedHashMap)localObject3).<init>();
      localObject3 = (Map)localObject3;
      localObject2 = ((Iterable)localObject2).iterator();
      for (;;)
      {
        bool8 = ((Iterator)localObject2).hasNext();
        if (!bool8) {
          break;
        }
        localObject4 = ((Iterator)localObject2).next();
        localObject6 = localObject4;
        i6 = a / 100;
        localObject6 = Integer.valueOf(i6);
        localObject7 = ((Map)localObject3).get(localObject6);
        if (localObject7 == null)
        {
          localObject7 = new java/util/ArrayList;
          ((ArrayList)localObject7).<init>();
          ((Map)localObject3).put(localObject6, localObject7);
        }
        localObject7 = (List)localObject7;
        ((List)localObject7).add(localObject4);
      }
      localObject2 = ((Map)localObject3).entrySet().iterator();
      bool2 = true;
      for (;;)
      {
        bool8 = ((Iterator)localObject2).hasNext();
        if (!bool8) {
          break;
        }
        localObject4 = (Iterable)((Map.Entry)((Iterator)localObject2).next()).getValue();
        localObject6 = new java/util/ArrayList;
        int i8 = c.a.m.a((Iterable)localObject4, 10);
        ((ArrayList)localObject6).<init>(i8);
        localObject6 = (Collection)localObject6;
        localObject4 = ((Iterable)localObject4).iterator();
        for (;;)
        {
          boolean bool7 = ((Iterator)localObject4).hasNext();
          if (!bool7) {
            break;
          }
          localObject7 = (Contact)nextb;
          ((Collection)localObject6).add(localObject7);
        }
        localObject6 = (List)localObject6;
        if (bool2)
        {
          localObject3 = j;
          bool2 = ((com.truecaller.data.access.m)localObject3).b((List)localObject6);
          if (bool2)
          {
            bool2 = true;
            break label1121;
          }
        }
        bool2 = false;
        localObject3 = null;
        localObject4 = new String[i4];
        localObject7 = new java/lang/StringBuilder;
        localObject8 = "Inserting ";
        ((StringBuilder)localObject7).<init>((String)localObject8);
        i6 = ((List)localObject6).size();
        ((StringBuilder)localObject7).append(i6);
        ((StringBuilder)localObject7).append(" entries");
        localObject6 = ((StringBuilder)localObject7).toString();
        localObject4[0] = localObject6;
      }
      if (bool2)
      {
        i1 = 2;
        ((av)localObject1).a(paramc, i1);
      }
      paramc = new String[i4];
      localObject2 = String.valueOf(bool2);
      localObject1 = "Restore done: ".concat((String)localObject2);
      paramc[0] = localObject1;
      if (bool2) {
        return BackupResult.Success;
      }
      return BackupResult.ErrorDatabase;
    }
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.av
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */