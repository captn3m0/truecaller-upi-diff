package com.truecaller.backup;

import android.support.v4.app.Fragment;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.tcpermissions.l;
import com.truecaller.utils.i;
import kotlinx.coroutines.ag;

final class p$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  boolean b;
  int c;
  private ag f;
  
  p$a(p paramp, Fragment paramFragment, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/backup/p$a;
    p localp = d;
    Fragment localFragment = e;
    locala.<init>(localp, localFragment, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = c;
    int j = 2;
    boolean bool1;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      localObject1 = (ag)a;
      bool1 = paramObject instanceof o.b;
      if (!bool1) {
        break label407;
      }
      throw a;
    case 1: 
      localObject2 = (ag)a;
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label554;
      }
      paramObject = f;
      localObject2 = d;
      localObject3 = "PositiveBtnClicked";
      p.a((p)localObject2, (String)localObject3);
      localObject2 = p.a(d);
      bool1 = ((i)localObject2).a();
      if (!bool1)
      {
        localObject1 = p.b(d);
        localObject2 = new com/truecaller/backup/p$a$1;
        ((p.a.1)localObject2).<init>(this, null);
        localObject2 = (m)localObject2;
        kotlinx.coroutines.e.b((ag)paramObject, (f)localObject1, (m)localObject2, j);
        return x.a;
      }
      localObject2 = p.b(d);
      localObject3 = new com/truecaller/backup/p$a$2;
      ((p.a.2)localObject3).<init>(this, null);
      localObject3 = (m)localObject3;
      kotlinx.coroutines.e.b((ag)paramObject, (f)localObject2, (m)localObject3, j);
      localObject2 = p.d(d);
      localObject3 = e;
      a = paramObject;
      int k = 1;
      c = k;
      localObject2 = ((e)localObject2).a((Fragment)localObject3, this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      Object localObject4 = localObject2;
      localObject2 = paramObject;
      paramObject = localObject4;
    }
    paramObject = (Boolean)paramObject;
    boolean bool3 = ((Boolean)paramObject).booleanValue();
    Object localObject3 = p.b(d);
    Object localObject5 = new com/truecaller/backup/p$a$3;
    ((p.a.3)localObject5).<init>(this, null);
    localObject5 = (m)localObject5;
    kotlinx.coroutines.e.b((ag)localObject2, (f)localObject3, (m)localObject5, j);
    if (!bool3) {
      return x.a;
    }
    localObject3 = p.d(d);
    a = localObject2;
    b = bool3;
    c = j;
    paramObject = ((e)localObject3).c();
    if (paramObject == localObject1) {
      return localObject1;
    }
    localObject1 = localObject2;
    label407:
    paramObject = (Number)paramObject;
    long l1 = ((Number)paramObject).longValue();
    long l2 = 0L;
    bool3 = l1 < l2;
    if (bool3)
    {
      paramObject = p.b(d);
      localObject2 = new com/truecaller/backup/p$a$4;
      ((p.a.4)localObject2).<init>(this, l1, null);
      localObject2 = (m)localObject2;
      kotlinx.coroutines.e.b((ag)localObject1, (f)paramObject, (m)localObject2, j);
      return x.a;
    }
    paramObject = p.e(d);
    bool3 = ((l)paramObject).g();
    if (bool3)
    {
      paramObject = d;
      ((o.a)paramObject).a(l2);
    }
    else
    {
      paramObject = p.b(d);
      localObject2 = new com/truecaller/backup/p$a$5;
      ((p.a.5)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      kotlinx.coroutines.e.b((ag)localObject1, (f)paramObject, (m)localObject2, j);
    }
    return x.a;
    label554:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.p.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */