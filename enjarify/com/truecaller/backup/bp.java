package com.truecaller.backup;

import android.accounts.Account;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import c.g.a.m;
import c.u;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.truecaller.TrueApp;
import com.truecaller.startup_dialogs.fragments.j;
import com.truecaller.ui.dialogs.a;
import java.util.HashMap;

public final class bp
  extends j
  implements bq.b
{
  public bq.a a;
  public c.d.f b;
  private TextView c;
  private com.truecaller.ui.dialogs.k d;
  private HashMap e;
  
  public final View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final bq.a a()
  {
    bq.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locala;
  }
  
  public final boolean a(String paramString)
  {
    c.g.b.k.b(paramString, "account");
    c.d.f localf = b;
    if (localf == null)
    {
      localObject = "uiContext";
      c.g.b.k.a((String)localObject);
    }
    Object localObject = new com/truecaller/backup/bp$d;
    ((bp.d)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    return ((Boolean)kotlinx.coroutines.f.a(localf, (m)localObject)).booleanValue();
  }
  
  public final void b()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    Toast.makeText(getContext(), 2131887944, 0).show();
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "timestamp");
    TextView localTextView = c;
    if (localTextView == null)
    {
      String str = "timestampText";
      c.g.b.k.a(str);
    }
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final String c()
  {
    Object localObject = getContext();
    if (localObject != null)
    {
      localObject = GoogleSignIn.a((Context)localObject);
      if (localObject != null)
      {
        localObject = ((GoogleSignInAccount)localObject).a();
        if (localObject != null) {
          return name;
        }
      }
    }
    return null;
  }
  
  public final java.text.DateFormat d()
  {
    Context localContext = getContext();
    if (localContext != null) {
      return android.text.format.DateFormat.getDateFormat(localContext);
    }
    return null;
  }
  
  public final void dismiss()
  {
    dismissAllowingStateLoss();
  }
  
  public final java.text.DateFormat e()
  {
    Context localContext = getContext();
    if (localContext != null) {
      return android.text.format.DateFormat.getTimeFormat(localContext);
    }
    return null;
  }
  
  public final void f()
  {
    HashMap localHashMap = e;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void g()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    c.g.b.k.a(localObject1, "context ?: return");
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    localBuilder.<init>((Context)localObject1);
    localObject1 = localBuilder.setTitle(2131888704).setMessage(2131888703);
    Object localObject2 = new com/truecaller/backup/bp$e;
    ((bp.e)localObject2).<init>(this);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton(2131888700, (DialogInterface.OnClickListener)localObject2);
    localObject2 = new com/truecaller/backup/bp$f;
    ((bp.f)localObject2).<init>(this);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    ((AlertDialog.Builder)localObject1).setNegativeButton(2131887228, (DialogInterface.OnClickListener)localObject2).show();
  }
  
  public final void h()
  {
    int i = 2131887466;
    com.truecaller.ui.dialogs.k localk = com.truecaller.ui.dialogs.k.c(i);
    d = localk;
    localk = d;
    if (localk != null)
    {
      boolean bool = true;
      localk.setCancelable(bool);
    }
    localk = d;
    if (localk != null)
    {
      android.support.v4.app.f localf = getActivity();
      a.a(localk, localf);
      return;
    }
  }
  
  public final void i()
  {
    try
    {
      com.truecaller.ui.dialogs.k localk = d;
      if (localk != null) {
        localk.dismissAllowingStateLoss();
      }
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    d = null;
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    bq.a locala = a;
    if (locala == null)
    {
      paramIntent = "presenter";
      c.g.b.k.a(paramIntent);
    }
    locala.a(paramInt1);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if (paramContext != null) {
      paramContext = paramContext.getApplicationContext();
    } else {
      paramContext = null;
    }
    if (paramContext != null)
    {
      ((TrueApp)paramContext).a().cv().a(this);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramContext;
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new com/truecaller/backup/bp$a;
    Context localContext = (Context)getActivity();
    paramBundle.<init>(this, localContext);
    return (Dialog)paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558575, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    bq.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.y_();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    paramBundle = paramView.findViewById(2131364877);
    c.g.b.k.a(paramBundle, "view.findViewById(R.id.timestamp)");
    paramBundle = (TextView)paramBundle;
    c = paramBundle;
    paramBundle = paramView.findViewById(2131362338);
    Object localObject = new com/truecaller/backup/bp$b;
    ((bp.b)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    int i = 2131362341;
    paramView = paramView.findViewById(i);
    paramBundle = new com/truecaller/backup/bp$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      c.g.b.k.a(paramBundle);
    }
    paramView.a(this);
    paramView = getArguments();
    long l = 0L;
    if (paramView != null)
    {
      paramBundle = "last_backup_time";
      l = paramView.getLong(paramBundle, l);
    }
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = "context";
      paramView = paramView.getString(paramBundle);
    }
    else
    {
      paramView = null;
    }
    paramBundle = a;
    if (paramBundle == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    paramBundle.a(l);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      c.g.b.k.a((String)localObject);
    }
    paramBundle.a(paramView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bp
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */