package com.truecaller.backup;

import android.accounts.Account;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v4.content.d;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import c.u;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.truecaller.TrueApp;
import com.truecaller.ui.components.ComboBase;
import com.truecaller.ui.components.ComboBase.a;
import com.truecaller.ui.components.n;
import com.truecaller.ui.dialogs.a;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class v
  extends Fragment
  implements w.b
{
  public w.a a;
  private SwitchCompat b;
  private TextView c;
  private ComboBase d;
  private ComboBase e;
  private ComboBase f;
  private TextView g;
  private View h;
  private CardView i;
  private BroadcastReceiver j;
  private com.truecaller.ui.dialogs.k k;
  private HashMap l;
  
  public final w.a a()
  {
    w.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locala;
  }
  
  public final void a(long paramLong)
  {
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    c.g.b.k.a(localf, "activity ?: return");
    Object localObject = new com/truecaller/backup/bp;
    ((bp)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putLong("last_backup_time", paramLong);
    localBundle.putString("context", "settings_screen");
    ((bp)localObject).setArguments(localBundle);
    o localo = localf.getSupportFragmentManager().a();
    localObject = (Fragment)localObject;
    String str = bp.class.getSimpleName();
    localo.a((Fragment)localObject, str).d();
  }
  
  public final void a(String paramString)
  {
    TextView localTextView = c;
    if (localTextView == null)
    {
      String str = "lastBackupText";
      c.g.b.k.a(str);
    }
    p.a(localTextView, paramString);
  }
  
  public final void a(List paramList, n paramn)
  {
    c.g.b.k.b(paramList, "backupFrequencyValues");
    c.g.b.k.b(paramn, "initialValue");
    Object localObject = d;
    if (localObject == null)
    {
      String str = "frequencyCombo";
      c.g.b.k.a(str);
    }
    ((ComboBase)localObject).setData(paramList);
    paramList = d;
    if (paramList == null)
    {
      localObject = "frequencyCombo";
      c.g.b.k.a((String)localObject);
    }
    paramList.setSelection(paramn);
  }
  
  public final void a(boolean paramBoolean)
  {
    SwitchCompat localSwitchCompat = b;
    if (localSwitchCompat == null)
    {
      String str = "backupSwitch";
      c.g.b.k.a(str);
    }
    localSwitchCompat.setChecked(paramBoolean);
  }
  
  public final void b()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    Toast.makeText(getContext(), 2131887944, 0).show();
  }
  
  public final void b(List paramList, n paramn)
  {
    c.g.b.k.b(paramList, "backupOverValues");
    c.g.b.k.b(paramn, "initialValue");
    Object localObject = e;
    if (localObject == null)
    {
      String str = "backupOverCombo";
      c.g.b.k.a(str);
    }
    ((ComboBase)localObject).setData(paramList);
    paramList = e;
    if (paramList == null)
    {
      localObject = "backupOverCombo";
      c.g.b.k.a((String)localObject);
    }
    paramList.setSelection(paramn);
  }
  
  public final void b(boolean paramBoolean)
  {
    ComboBase localComboBase = e;
    if (localComboBase == null)
    {
      String str = "backupOverCombo";
      c.g.b.k.a(str);
    }
    t.a((View)localComboBase, paramBoolean, 0.5F);
  }
  
  public final void c()
  {
    int m = 2131887466;
    com.truecaller.ui.dialogs.k localk = com.truecaller.ui.dialogs.k.c(m);
    k = localk;
    localk = k;
    if (localk != null)
    {
      boolean bool = true;
      localk.setCancelable(bool);
    }
    localk = k;
    if (localk != null)
    {
      f localf = getActivity();
      a.a(localk, localf);
      return;
    }
  }
  
  public final void c(List paramList, n paramn)
  {
    c.g.b.k.b(paramList, "accountValues");
    c.g.b.k.b(paramn, "initialValue");
    Object localObject = f;
    if (localObject == null)
    {
      String str = "accountCombo";
      c.g.b.k.a(str);
    }
    ((ComboBase)localObject).setData(paramList);
    paramList = f;
    if (paramList == null)
    {
      localObject = "accountCombo";
      c.g.b.k.a((String)localObject);
    }
    paramList.setSelection(paramn);
  }
  
  public final void c(boolean paramBoolean)
  {
    ComboBase localComboBase = d;
    if (localComboBase == null)
    {
      String str = "frequencyCombo";
      c.g.b.k.a(str);
    }
    t.a((View)localComboBase, paramBoolean, 0.5F);
  }
  
  public final void d()
  {
    com.truecaller.ui.dialogs.k localk = k;
    if (localk != null) {
      localk.dismissAllowingStateLoss();
    }
    k = null;
  }
  
  public final void d(boolean paramBoolean)
  {
    ComboBase localComboBase = f;
    if (localComboBase == null)
    {
      String str = "accountCombo";
      c.g.b.k.a(str);
    }
    t.a((View)localComboBase, paramBoolean, 0.5F);
  }
  
  public final void e()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    c.g.b.k.a(localObject1, "context ?: return");
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    localBuilder.<init>((Context)localObject1);
    localObject1 = localBuilder.setTitle(2131887498).setMessage(2131887495);
    Object localObject2 = new com/truecaller/backup/v$i;
    ((v.i)localObject2).<init>(this);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    ((AlertDialog.Builder)localObject1).setPositiveButton(2131887497, (DialogInterface.OnClickListener)localObject2).setNegativeButton(2131887496, null).show();
  }
  
  public final void e(boolean paramBoolean)
  {
    TextView localTextView = g;
    if (localTextView == null)
    {
      String str = "backupNowText";
      c.g.b.k.a(str);
    }
    localTextView.setEnabled(paramBoolean);
  }
  
  public final String f()
  {
    Object localObject = getContext();
    if (localObject != null)
    {
      localObject = GoogleSignIn.a((Context)localObject);
      if (localObject != null)
      {
        localObject = ((GoogleSignInAccount)localObject).a();
        if (localObject != null) {
          return name;
        }
      }
    }
    return null;
  }
  
  public final void f(boolean paramBoolean)
  {
    View localView = h;
    if (localView == null)
    {
      String str = "backupSmsContainer";
      c.g.b.k.a(str);
    }
    t.a(localView, paramBoolean);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    w.a locala = a;
    if (locala == null)
    {
      paramIntent = "presenter";
      c.g.b.k.a(paramIntent);
    }
    locala.a(paramInt1);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject1;
    if (paramContext != null) {
      localObject1 = paramContext.getApplicationContext();
    } else {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      ((TrueApp)localObject1).a().cv().a(this);
      localObject1 = new com/truecaller/backup/v$h;
      ((v.h)localObject1).<init>(this);
      localObject1 = (BroadcastReceiver)localObject1;
      j = ((BroadcastReceiver)localObject1);
      paramContext = d.a(paramContext);
      localObject1 = j;
      if (localObject1 == null)
      {
        localObject2 = "backupBroadcastReceiver";
        c.g.b.k.a((String)localObject2);
      }
      Object localObject2 = new android/content/IntentFilter;
      ((IntentFilter)localObject2).<init>("com.truecaller.backup.BACKUP_DONE");
      paramContext.a((BroadcastReceiver)localObject1, (IntentFilter)localObject2);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramContext;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558785, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    Object localObject1 = getContext();
    Object localObject2;
    if (localObject1 != null)
    {
      c.g.b.k.a(localObject1, "context ?: return");
      localObject1 = d.a((Context)localObject1);
      localObject2 = j;
      if (localObject2 == null)
      {
        String str = "backupBroadcastReceiver";
        c.g.b.k.a(str);
      }
      ((d)localObject1).a((BroadcastReceiver)localObject2);
    }
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    ((w.a)localObject1).y_();
  }
  
  public final void onResume()
  {
    super.onResume();
    w.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.a();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    paramBundle = paramView.findViewById(2131364491);
    c.g.b.k.a(paramBundle, "view.findViewById(R.id.settings_backup_switch)");
    paramBundle = (SwitchCompat)paramBundle;
    b = paramBundle;
    paramBundle = paramView.findViewById(2131364484);
    c.g.b.k.a(paramBundle, "view.findViewById(R.id.settings_backup_last)");
    paramBundle = (TextView)paramBundle;
    c = paramBundle;
    paramBundle = paramView.findViewById(2131364483);
    c.g.b.k.a(paramBundle, "view.findViewById(R.id.settings_backup_frequency)");
    paramBundle = (ComboBase)paramBundle;
    d = paramBundle;
    paramBundle = paramView.findViewById(2131364485);
    c.g.b.k.a(paramBundle, "view.findViewById(R.id.settings_backup_over)");
    paramBundle = (ComboBase)paramBundle;
    e = paramBundle;
    paramBundle = paramView.findViewById(2131364481);
    c.g.b.k.a(paramBundle, "view.findViewById(R.id.settings_backup_account)");
    paramBundle = (ComboBase)paramBundle;
    f = paramBundle;
    paramBundle = paramView.findViewById(2131362307);
    c.g.b.k.a(paramBundle, "view.findViewById(R.id.button_backup_now)");
    paramBundle = (TextView)paramBundle;
    g = paramBundle;
    paramBundle = paramView.findViewById(2131364486);
    c.g.b.k.a(paramBundle, "view.findViewById(R.id.settings_backup_sms)");
    h = paramBundle;
    paramBundle = paramView.findViewById(2131364489);
    String str = "view.findViewById(R.id.s…up_sms_permission_button)";
    c.g.b.k.a(paramBundle, str);
    paramBundle = (CardView)paramBundle;
    i = paramBundle;
    int m = 2131364480;
    paramView = paramView.findViewById(m);
    paramBundle = new com/truecaller/backup/v$a;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = g;
    if (paramView == null)
    {
      paramBundle = "backupNowText";
      c.g.b.k.a(paramBundle);
    }
    paramBundle = new com/truecaller/backup/v$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "backupSwitch";
      c.g.b.k.a(paramBundle);
    }
    paramBundle = new com/truecaller/backup/v$c;
    paramBundle.<init>(this);
    paramBundle = (CompoundButton.OnCheckedChangeListener)paramBundle;
    paramView.setOnCheckedChangeListener(paramBundle);
    paramView = d;
    if (paramView == null)
    {
      paramBundle = "frequencyCombo";
      c.g.b.k.a(paramBundle);
    }
    paramBundle = new com/truecaller/backup/v$d;
    paramBundle.<init>(this);
    paramBundle = (ComboBase.a)paramBundle;
    paramView.a(paramBundle);
    paramView = e;
    if (paramView == null)
    {
      paramBundle = "backupOverCombo";
      c.g.b.k.a(paramBundle);
    }
    paramBundle = new com/truecaller/backup/v$e;
    paramBundle.<init>(this);
    paramBundle = (ComboBase.a)paramBundle;
    paramView.a(paramBundle);
    paramView = f;
    if (paramView == null)
    {
      paramBundle = "accountCombo";
      c.g.b.k.a(paramBundle);
    }
    paramBundle = new com/truecaller/backup/v$f;
    paramBundle.<init>(this);
    paramBundle = (ComboBase.a)paramBundle;
    paramView.a(paramBundle);
    paramView = i;
    if (paramView == null)
    {
      paramBundle = "backupSmsPermissionButton";
      c.g.b.k.a(paramBundle);
    }
    paramBundle = new com/truecaller/backup/v$g;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      c.g.b.k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */