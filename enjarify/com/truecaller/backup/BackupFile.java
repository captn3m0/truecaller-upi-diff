package com.truecaller.backup;

public enum BackupFile
{
  private final String nameSuffix;
  
  static
  {
    BackupFile[] arrayOfBackupFile = new BackupFile[5];
    BackupFile localBackupFile = new com/truecaller/backup/BackupFile;
    localBackupFile.<init>("CONTACTS", 0, "_contacts.json");
    CONTACTS = localBackupFile;
    arrayOfBackupFile[0] = localBackupFile;
    localBackupFile = new com/truecaller/backup/BackupFile;
    int i = 1;
    localBackupFile.<init>("CALL_LOG", i, "_call_log.json");
    CALL_LOG = localBackupFile;
    arrayOfBackupFile[i] = localBackupFile;
    localBackupFile = new com/truecaller/backup/BackupFile;
    i = 2;
    localBackupFile.<init>("SETTINGS", i, "_settings.json");
    SETTINGS = localBackupFile;
    arrayOfBackupFile[i] = localBackupFile;
    localBackupFile = new com/truecaller/backup/BackupFile;
    i = 3;
    localBackupFile.<init>("BLOCK_LIST", i, "_blockList.json");
    BLOCK_LIST = localBackupFile;
    arrayOfBackupFile[i] = localBackupFile;
    localBackupFile = new com/truecaller/backup/BackupFile;
    i = 4;
    localBackupFile.<init>("DB", i, "tc.db");
    DB = localBackupFile;
    arrayOfBackupFile[i] = localBackupFile;
    $VALUES = arrayOfBackupFile;
  }
  
  private BackupFile(String paramString1)
  {
    nameSuffix = paramString1;
  }
  
  public final String getNameSuffix()
  {
    return nameSuffix;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupFile
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */