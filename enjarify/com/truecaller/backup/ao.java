package com.truecaller.backup;

import dagger.a.d;
import javax.inject.Provider;

public final class ao
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private ao(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
  }
  
  public static ao a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    ao localao = new com/truecaller/backup/ao;
    localao.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localao;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */