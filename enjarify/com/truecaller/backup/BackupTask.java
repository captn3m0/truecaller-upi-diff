package com.truecaller.backup;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.z.a;
import android.support.v4.app.z.a.a;
import android.support.v4.app.z.d;
import android.support.v4.content.d;
import android.widget.Toast;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.background.e.a;
import com.truecaller.ui.SettingsFragment;
import com.truecaller.ui.SettingsFragment.SettingsViewType;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.f;

public final class BackupTask
  extends PersistentBackgroundTask
  implements ac.b
{
  public ac.a a;
  public com.truecaller.notifications.a b;
  public Context c;
  private final String d = "backupInProgress";
  private final String e = "backupError";
  
  private final z.d d(String paramString)
  {
    z.d locald = new android/support/v4/app/z$d;
    Context localContext = c;
    if (localContext == null)
    {
      String str = "context";
      k.a(str);
    }
    locald.<init>(localContext, paramString);
    return locald;
  }
  
  private final void f(Context paramContext)
  {
    if (paramContext != null)
    {
      Object localObject = paramContext;
      localObject = ((TrueApp)paramContext).a().cv();
      ((b)localObject).a(this);
      c = paramContext;
      paramContext = a;
      if (paramContext == null)
      {
        localObject = "presenter";
        k.a((String)localObject);
      }
      paramContext.a(this);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramContext;
  }
  
  public final int a()
  {
    return 10002;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    k.b(paramContext, "serviceContext");
    BackupTask.a locala = new com/truecaller/backup/BackupTask$a;
    locala.<init>(this, paramContext, paramBundle, null);
    return (PersistentBackgroundTask.RunResult)f.a((m)locala);
  }
  
  public final void a(int paramInt)
  {
    Context localContext = c;
    if (localContext == null)
    {
      String str = "context";
      k.a(str);
    }
    Toast.makeText(localContext, paramInt, 0).show();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "channelId");
    paramString = d(paramString);
    Object localObject = c;
    String str;
    if (localObject == null)
    {
      str = "context";
      k.a(str);
    }
    int i = 2131099685;
    int j = android.support.v4.content.b.c((Context)localObject, i);
    paramString = paramString.f(j);
    j = 17301640;
    paramString = paramString.a(j);
    localObject = c;
    if (localObject == null)
    {
      str = "context";
      k.a(str);
    }
    i = 2131887470;
    localObject = (CharSequence)((Context)localObject).getString(i);
    paramString = paramString.a((CharSequence)localObject).b();
    j = 0;
    Notification localNotification = paramString.a(0, 0).h();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString = "Subtype";
    localObject = d;
    localBundle.putString(paramString, (String)localObject);
    localObject = b;
    if (localObject == null)
    {
      paramString = "notificationManager";
      k.a(paramString);
    }
    k.a(localNotification, "notification");
    ((com.truecaller.notifications.a)localObject).a(null, 2131362079, localNotification, "notificationBackup", localBundle);
  }
  
  public final boolean a(Context paramContext)
  {
    String str = "serviceContext";
    k.b(paramContext, str);
    f(paramContext);
    paramContext = a;
    if (paramContext == null)
    {
      str = "presenter";
      k.a(str);
    }
    return paramContext.a();
  }
  
  public final e b()
  {
    Object localObject = TrueApp.y();
    k.a(localObject, "TrueApp.getApp()");
    localObject = ((TrueApp)localObject).a().I();
    k.a(localObject, "TrueApp.getApp().objectsGraph.coreSettings()");
    int i = 1;
    int j = ((com.truecaller.common.g.a)localObject).a("backupNetworkType", i);
    e.a locala = new com/truecaller/common/background/e$a;
    locala.<init>(i);
    TimeUnit localTimeUnit = TimeUnit.DAYS;
    localObject = locala.a(1L, localTimeUnit).a(j).a().b();
    k.a(localObject, "TaskConfiguration.Builde…lse)\n            .build()");
    return (e)localObject;
  }
  
  public final void b(String paramString)
  {
    String str = "channelId";
    k.b(paramString, str);
    paramString = b;
    if (paramString == null)
    {
      str = "notificationManager";
      k.a(str);
    }
    paramString.a(2131362079);
  }
  
  public final void c()
  {
    d locald = d.a((Context)TrueApp.y());
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.truecaller.backup.BACKUP_DONE");
    locald.a(localIntent);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "channelId");
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "context";
      k.a((String)localObject2);
    }
    Object localObject2 = SettingsFragment.SettingsViewType.SETTINGS_BACKUP;
    localObject1 = SettingsFragment.a((Context)localObject1, (SettingsFragment.SettingsViewType)localObject2);
    localObject2 = c;
    if (localObject2 == null)
    {
      str1 = "context";
      k.a(str1);
    }
    String str1 = null;
    int i = 134217728;
    localObject1 = PendingIntent.getActivity((Context)localObject2, 0, (Intent)localObject1, i);
    localObject2 = new android/support/v4/app/z$a$a;
    int j = 2131234170;
    Object localObject3 = c;
    if (localObject3 == null)
    {
      String str2 = "context";
      k.a(str2);
    }
    int k = 2131887473;
    localObject3 = (CharSequence)((Context)localObject3).getString(k);
    ((z.a.a)localObject2).<init>(j, (CharSequence)localObject3, (PendingIntent)localObject1);
    localObject1 = ((z.a.a)localObject2).a();
    paramString = d(paramString);
    localObject2 = c;
    if (localObject2 == null)
    {
      str1 = "context";
      k.a(str1);
    }
    j = 2131099685;
    int m = android.support.v4.content.b.c((Context)localObject2, j);
    paramString = paramString.f(m);
    m = 2131234005;
    paramString = paramString.a(m);
    localObject2 = c;
    if (localObject2 == null)
    {
      str1 = "context";
      k.a(str1);
    }
    j = 2131887494;
    localObject2 = (CharSequence)((Context)localObject2).getString(j);
    paramString = paramString.a((CharSequence)localObject2);
    localObject2 = c;
    if (localObject2 == null)
    {
      str1 = "context";
      k.a(str1);
    }
    j = 2131887471;
    localObject2 = (CharSequence)((Context)localObject2).getString(j);
    localObject3 = paramString.b((CharSequence)localObject2).a((z.a)localObject1).e().h();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramString = "Subtype";
    localObject1 = e;
    localBundle.putString(paramString, (String)localObject1);
    localObject1 = b;
    if (localObject1 == null)
    {
      paramString = "notificationManager";
      k.a(paramString);
    }
    k.a(localObject3, "notification");
    ((com.truecaller.notifications.a)localObject1).a(null, 2131362078, (Notification)localObject3, "notificationBackup", localBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */