package com.truecaller.backup;

import android.app.Activity;
import android.support.v4.app.f;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.google.android.gms.common.GoogleApiAvailability;
import kotlinx.coroutines.ag;

final class bd$f
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  bd$f(f paramf, int paramInt, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/backup/bd$f;
    f localf1 = b;
    int i = c;
    localf.<init>(localf1, i, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = GoogleApiAvailability.a();
        localObject = (Activity)b;
        int j = c;
        ((GoogleApiAvailability)paramObject).b((Activity)localObject, j, 0);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bd.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */