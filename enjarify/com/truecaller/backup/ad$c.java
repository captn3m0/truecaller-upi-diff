package com.truecaller.backup;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.ag;

final class ad$c
  extends c.d.b.a.k
  implements m
{
  long a;
  long b;
  int c;
  private ag e;
  
  ad$c(ad paramad, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/backup/ad$c;
    ad localad = d;
    localc.<init>(localad, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    int i = c;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label231;
      }
      paramObject = ad.a(d);
      long l1 = 0L;
      long l2 = ((com.truecaller.common.g.a)paramObject).a("key_backup_frequency_hours", l1);
      paramObject = ad.a(d);
      String str = "key_backup_last_success";
      long l3 = ((com.truecaller.common.g.a)paramObject).a(str, l1);
      long l4 = System.currentTimeMillis() - l3;
      paramObject = TimeUnit.HOURS;
      long l5 = ((TimeUnit)paramObject).toMillis(l2);
      int j = 1;
      boolean bool3 = l4 < l5;
      if (bool3)
      {
        bool3 = true;
      }
      else
      {
        bool3 = false;
        localad = null;
      }
      if (bool3)
      {
        new String[1][0] = "Not enough time has passed since last backup, skipping.";
        return BackupResult.Skipped;
      }
      ad localad = d;
      a = l2;
      b = l3;
      c = j;
      paramObject = localad.a(j, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return (BackupResult)paramObject;
    label231:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ad.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */