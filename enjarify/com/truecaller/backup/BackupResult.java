package com.truecaller.backup;

public enum BackupResult
{
  static
  {
    BackupResult[] arrayOfBackupResult = new BackupResult[18];
    BackupResult localBackupResult = new com/truecaller/backup/BackupResult;
    localBackupResult.<init>("Success", 0);
    Success = localBackupResult;
    arrayOfBackupResult[0] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    int i = 1;
    localBackupResult.<init>("Skipped", i);
    Skipped = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 2;
    localBackupResult.<init>("ErrorClient", i);
    ErrorClient = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 3;
    localBackupResult.<init>("ErrorFile", i);
    ErrorFile = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 4;
    localBackupResult.<init>("ErrorOpen", i);
    ErrorOpen = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 5;
    localBackupResult.<init>("ErrorWrite", i);
    ErrorWrite = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 6;
    localBackupResult.<init>("ErrorCommit", i);
    ErrorCommit = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 7;
    localBackupResult.<init>("ErrorDatabase", i);
    ErrorDatabase = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 8;
    localBackupResult.<init>("ErrorNetwork", i);
    ErrorNetwork = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 9;
    localBackupResult.<init>("ErrorRead", i);
    ErrorRead = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 10;
    localBackupResult.<init>("ErrorFileName", i);
    ErrorFileName = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 11;
    localBackupResult.<init>("ErrorJsonParsing", i);
    ErrorJsonParsing = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 12;
    localBackupResult.<init>("ErrorDBFileNotFound", i);
    ErrorDBFileNotFound = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 13;
    localBackupResult.<init>("ErrorDBDeletion", i);
    ErrorDBDeletion = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    i = 14;
    localBackupResult.<init>("ErrorIO", i);
    ErrorIO = localBackupResult;
    arrayOfBackupResult[i] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    localBackupResult.<init>("ErrorAccount", 15);
    ErrorAccount = localBackupResult;
    arrayOfBackupResult[15] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    localBackupResult.<init>("ErrorNotSupportedDb", 16);
    ErrorNotSupportedDb = localBackupResult;
    arrayOfBackupResult[16] = localBackupResult;
    localBackupResult = new com/truecaller/backup/BackupResult;
    localBackupResult.<init>("ErrorPropertyRead", 17);
    ErrorPropertyRead = localBackupResult;
    arrayOfBackupResult[17] = localBackupResult;
    $VALUES = arrayOfBackupResult;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupResult
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */