package com.truecaller.backup;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.support.v7.app.AlertDialog.Builder;
import c.d.a.a;
import c.d.a.b;
import c.d.c;
import c.d.h;
import c.g.a.m;
import c.g.b.v.a;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class bp$d
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private ag f;
  
  bp$d(bp parambp, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/backup/bp$d;
    bp localbp = d;
    String str = e;
    locald.<init>(localbp, str, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = c;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label324;
      }
      paramObject = d.getContext();
      if (paramObject == null) {
        return Boolean.FALSE;
      }
      c.g.b.k.a(paramObject, "context ?: return@runBlocking false");
      a = paramObject;
      b = this;
      j = 1;
      c = j;
      h localh = new c/d/h;
      Object localObject1 = b.a(this);
      localh.<init>((c)localObject1);
      localObject1 = localh;
      localObject1 = (c)localh;
      v.a locala1 = new c/g/b/v$a;
      locala1.<init>();
      a = false;
      AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
      localBuilder.<init>((Context)paramObject);
      paramObject = d;
      int m = 2131888697;
      Object localObject2 = new Object[j];
      String str = e;
      localObject2[0] = str;
      paramObject = (CharSequence)((bp)paramObject).getString(m, (Object[])localObject2);
      paramObject = localBuilder.setMessage((CharSequence)paramObject);
      Object localObject3 = new com/truecaller/backup/bp$d$a;
      ((bp.d.a)localObject3).<init>(locala1);
      localObject3 = (DialogInterface.OnClickListener)localObject3;
      paramObject = ((AlertDialog.Builder)paramObject).setPositiveButton(2131888698, (DialogInterface.OnClickListener)localObject3);
      int k = 2131887197;
      localObject3 = null;
      paramObject = ((AlertDialog.Builder)paramObject).setNegativeButton(k, null);
      localObject2 = new com/truecaller/backup/bp$d$b;
      ((bp.d.b)localObject2).<init>((c)localObject1, locala1);
      localObject2 = (DialogInterface.OnDismissListener)localObject2;
      ((AlertDialog.Builder)paramObject).setOnDismissListener((DialogInterface.OnDismissListener)localObject2).show();
      paramObject = localh.b();
      localObject2 = a.a;
      if (paramObject == localObject2)
      {
        localObject2 = "frame";
        c.g.b.k.b(this, (String)localObject2);
      }
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return paramObject;
    label324:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bp.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */