package com.truecaller.backup;

import c.d.f;
import c.g.a.m;
import com.truecaller.analytics.b;
import com.truecaller.ba;
import com.truecaller.common.g.a;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.bn;

public final class ad
  extends ba
  implements ac.a
{
  private final long c;
  private final f d;
  private final f e;
  private final e f;
  private final bx g;
  private final a h;
  private final com.truecaller.notificationchannels.e i;
  private final b j;
  
  public ad(f paramf1, f paramf2, e parame, bx parambx, a parama, com.truecaller.notificationchannels.e parame1, b paramb)
  {
    super(paramf2);
    d = paramf1;
    e = paramf2;
    f = parame;
    g = parambx;
    h = parama;
    i = parame1;
    j = paramb;
    long l = TimeUnit.SECONDS.toMillis(15);
    c = l;
  }
  
  private final bn a(int paramInt)
  {
    Object localObject = new com/truecaller/backup/ad$e;
    ((ad.e)localObject).<init>(this, paramInt, null);
    localObject = (m)localObject;
    return kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
  
  public final Object a(boolean paramBoolean)
  {
    int k = 2;
    if (paramBoolean)
    {
      localf = d;
      localObject = new com/truecaller/backup/ad$b;
      ((ad.b)localObject).<init>(this, null);
      localObject = (m)localObject;
      return kotlinx.coroutines.e.a(this, localf, (m)localObject, k);
    }
    f localf = d;
    Object localObject = new com/truecaller/backup/ad$c;
    ((ad.c)localObject).<init>(this, null);
    localObject = (m)localObject;
    return kotlinx.coroutines.e.a(this, localf, (m)localObject, k);
  }
  
  public final boolean a()
  {
    Object localObject = f;
    boolean bool1 = ((e)localObject).a();
    if (bool1)
    {
      localObject = h;
      String str = "key_backup_frequency_hours";
      long l1 = 0L;
      long l2 = ((a)localObject).a(str, l1);
      boolean bool2 = l2 < l1;
      if (bool2) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */