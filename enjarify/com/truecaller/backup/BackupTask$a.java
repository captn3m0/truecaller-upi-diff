package com.truecaller.backup;

import android.content.Context;
import android.os.Bundle;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

final class BackupTask$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  BackupTask$a(BackupTask paramBackupTask, Context paramContext, Bundle paramBundle, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/backup/BackupTask$a;
    BackupTask localBackupTask = b;
    Context localContext = c;
    Bundle localBundle = d;
    locala.<init>(localBackupTask, localContext, localBundle, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    int k = 1;
    boolean bool1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label231;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label267;
      }
      paramObject = b;
      Context localContext = c;
      BackupTask.a((BackupTask)paramObject, localContext);
      paramObject = d;
      bool1 = false;
      localContext = null;
      String str;
      if (paramObject != null)
      {
        str = "backupNow";
        boolean bool3 = ((Bundle)paramObject).getBoolean(str, false);
        paramObject = Boolean.valueOf(bool3);
        if (paramObject != null) {
          bool1 = ((Boolean)paramObject).booleanValue();
        }
      }
      paramObject = b.a;
      if (paramObject == null)
      {
        str = "presenter";
        c.g.b.k.a(str);
      }
      a = k;
      paramObject = ((ac.a)paramObject).a(bool1);
      if (paramObject == localObject) {
        return localObject;
      }
      break;
    }
    paramObject = (ao)paramObject;
    int j = 2;
    a = j;
    paramObject = ((ao)paramObject).a(this);
    if (paramObject == localObject) {
      return localObject;
    }
    label231:
    paramObject = (BackupResult)paramObject;
    localObject = ab.a;
    int m = ((BackupResult)paramObject).ordinal();
    m = localObject[m];
    if (m != k) {
      return PersistentBackgroundTask.RunResult.FailedSkip;
    }
    return PersistentBackgroundTask.RunResult.Success;
    label267:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupTask.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */