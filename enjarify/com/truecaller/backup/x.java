package com.truecaller.backup;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import c.g.b.k;
import c.g.b.r;
import c.g.b.s;
import c.g.b.w;
import c.l.g;
import com.truecaller.analytics.e.a;
import com.truecaller.bb;
import com.truecaller.common.g.a;
import com.truecaller.messaging.h;
import com.truecaller.tcpermissions.l;
import com.truecaller.tcpermissions.o;
import com.truecaller.ui.components.n;
import com.truecaller.utils.i;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.bs;

public final class x
  extends bb
  implements w.a
{
  private final long c;
  private final long d;
  private final long e;
  private final long f;
  private final long g;
  private final String h;
  private final bn i;
  private final List j;
  private final List k;
  private boolean l;
  private final Context m;
  private final c.d.f n;
  private final c.d.f o;
  private final e p;
  private final i q;
  private final a r;
  private final com.truecaller.common.background.b s;
  private final com.truecaller.analytics.b t;
  private final com.truecaller.androidactors.f u;
  private final l v;
  private final h w;
  private final o x;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/s;
    c.l.b localb = w.a(x.class);
    ((s)localObject).<init>(localb, "hasSMSPermissions", "<v#0>");
    localObject = (g)w.a((r)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public x(Context paramContext, c.d.f paramf1, c.d.f paramf2, e parame, i parami, a parama, com.truecaller.common.background.b paramb, com.truecaller.analytics.b paramb1, com.truecaller.androidactors.f paramf, l paraml, h paramh, o paramo)
  {
    m = paramContext;
    n = paramf1;
    o = paramf2;
    p = parame;
    q = parami;
    r = parama;
    s = paramb;
    t = paramb1;
    u = paramf;
    v = paraml;
    w = paramh;
    x = paramo;
    c = -1;
    e = 24;
    long l1 = e;
    long l2 = 7 * l1;
    f = l2;
    l1 *= 30;
    g = l1;
    h = "";
    paramContext = bs.a(null);
    i = paramContext;
    paramContext = new n[4];
    paramf1 = new com/truecaller/ui/components/n;
    paramf2 = Long.valueOf(d);
    paramf1.<init>(2131887491, paramf2);
    paramContext[0] = paramf1;
    paramf1 = new com/truecaller/ui/components/n;
    parame = Long.valueOf(e);
    paramf1.<init>(2131887489, parame);
    int i1 = 1;
    paramContext[i1] = paramf1;
    paramf1 = new com/truecaller/ui/components/n;
    parami = Long.valueOf(f);
    paramf1.<init>(2131887492, parami);
    int i2 = 2;
    paramContext[i2] = paramf1;
    paramf1 = new com/truecaller/ui/components/n;
    parama = Long.valueOf(g);
    paramf1.<init>(2131887490, parama);
    paramContext[3] = paramf1;
    paramContext = c.a.m.b(paramContext);
    j = paramContext;
    paramContext = new n[i2];
    paramf1 = new com/truecaller/ui/components/n;
    parami = Integer.valueOf(i2);
    paramf1.<init>(2131887482, parami);
    paramContext[0] = paramf1;
    paramf1 = new com/truecaller/ui/components/n;
    paramf2 = Integer.valueOf(i1);
    paramf1.<init>(2131887483, paramf2);
    paramContext[i1] = paramf1;
    paramContext = c.a.m.b(paramContext);
    k = paramContext;
  }
  
  private final void a(String paramString)
  {
    com.truecaller.analytics.b localb = t;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("ViewAction");
    paramString = locala.a("Context", "settings_screen").a("Action", paramString).a();
    k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.a(paramString);
  }
  
  private final void a(boolean paramBoolean)
  {
    s.b();
    int[] arrayOfInt = null;
    int i1 = 10002;
    if (paramBoolean)
    {
      s.c();
      localb = s;
      long l1 = TimeUnit.MINUTES.toMillis(2);
      arrayOfInt = new int[0];
      localb.a(l1, i1, arrayOfInt);
      return;
    }
    com.truecaller.common.background.b localb = s;
    arrayOfInt = new int[0];
    localb.a(i1, arrayOfInt);
  }
  
  private final void f()
  {
    w.b localb = (w.b)b;
    if (localb != null)
    {
      Object localObject = r;
      String str = "backup_enabled";
      boolean bool = ((a)localObject).b(str);
      if (bool)
      {
        localObject = v;
        bool = ((l)localObject).g();
        if (!bool)
        {
          bool = true;
          break label64;
        }
      }
      bool = false;
      localObject = null;
      label64:
      localb.f(bool);
      return;
    }
  }
  
  private final bn g()
  {
    ag localag = (ag)bg.a;
    c.d.f localf = o;
    Object localObject = new com/truecaller/backup/x$f;
    ((x.f)localObject).<init>(this, null);
    localObject = (c.g.a.m)localObject;
    return kotlinx.coroutines.e.b(localag, localf, (c.g.a.m)localObject, 2);
  }
  
  public final ao a(Fragment paramFragment, String paramString)
  {
    k.b(paramFragment, "fragment");
    k.b(paramString, "selectedOption");
    ag localag = (ag)bg.a;
    c.d.f localf = n;
    Object localObject = (c.d.f)i;
    localf = localf.plus((c.d.f)localObject);
    localObject = new com/truecaller/backup/x$d;
    ((x.d)localObject).<init>(this, paramString, paramFragment, null);
    localObject = (c.g.a.m)localObject;
    return kotlinx.coroutines.e.a(localag, localf, (c.g.a.m)localObject, 2);
  }
  
  public final ao a(Fragment paramFragment, boolean paramBoolean)
  {
    k.b(paramFragment, "fragment");
    ag localag = (ag)bg.a;
    c.d.f localf = o;
    Object localObject = new com/truecaller/backup/x$h;
    ((x.h)localObject).<init>(this, paramBoolean, paramFragment, null);
    localObject = (c.g.a.m)localObject;
    return kotlinx.coroutines.e.a(localag, localf, (c.g.a.m)localObject, 2);
  }
  
  public final void a()
  {
    f();
  }
  
  public final void a(int paramInt)
  {
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Resolution result: requestCode = ");
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).append(", resultCode = ");
    ((StringBuilder)localObject).append(paramInt);
    localObject = ((StringBuilder)localObject).toString();
    arrayOfString[0] = localObject;
    int i1 = 4321;
    if (paramInt != i1) {
      return;
    }
    p.b();
  }
  
  public final void a(long paramLong)
  {
    Object localObject1 = r;
    Object localObject2 = "key_backup_frequency_hours";
    long l1 = c;
    long l2 = ((a)localObject1).a((String)localObject2, l1);
    boolean bool = l2 < paramLong;
    if (!bool) {
      return;
    }
    r.b("key_backup_frequency_hours", paramLong);
    localObject1 = t;
    localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("SettingChanged");
    com.truecaller.analytics.e locale = ((e.a)localObject2).a("Context", "settings_screen").a("Setting", "BackupFrequency").a("State", paramLong).a();
    k.a(locale, "AnalyticsEvent.Builder(A…\n                .build()");
    ((com.truecaller.analytics.b)localObject1).a(locale);
  }
  
  public final void a(Fragment paramFragment)
  {
    k.b(paramFragment, "fragment");
    a(paramFragment, true);
  }
  
  public final void b()
  {
    a("backupNow");
    Object localObject = q;
    boolean bool = ((i)localObject).a();
    if (!bool)
    {
      localObject = (w.b)b;
      if (localObject != null)
      {
        ((w.b)localObject).b();
        return;
      }
      return;
    }
    localObject = new android/os/Bundle;
    ((Bundle)localObject).<init>();
    ((Bundle)localObject).putBoolean("backupNow", true);
    s.b(10002, (Bundle)localObject);
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = r;
    Object localObject2 = "backupNetworkType";
    int i1 = 1;
    int i2 = ((a)localObject1).a((String)localObject2, i1);
    if (i2 == paramInt) {
      return;
    }
    r.b("backupNetworkType", paramInt);
    l = i1;
    localObject1 = t;
    localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("SettingChanged");
    com.truecaller.analytics.e locale = ((e.a)localObject2).a("Context", "settings_screen").a("Setting", "BackupOver").a("State", paramInt).a();
    k.a(locale, "AnalyticsEvent.Builder(A…\n                .build()");
    ((com.truecaller.analytics.b)localObject1).a(locale);
  }
  
  public final void c()
  {
    g();
  }
  
  public final bn e()
  {
    ag localag = (ag)bg.a;
    Object localObject1 = i;
    Object localObject2 = o;
    localObject1 = ((bn)localObject1).plus((c.d.f)localObject2);
    localObject2 = new com/truecaller/backup/x$e;
    ((x.e)localObject2).<init>(this, null);
    localObject2 = (c.g.a.m)localObject2;
    return kotlinx.coroutines.e.b(localag, (c.d.f)localObject1, (c.g.a.m)localObject2, 2);
  }
  
  public final void y_()
  {
    super.y_();
    Object localObject = (w.b)b;
    if (localObject != null) {
      ((w.b)localObject).d();
    }
    localObject = i;
    ((bn)localObject).n();
    boolean bool = l;
    if (bool)
    {
      bool = false;
      localObject = null;
      a(false);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */