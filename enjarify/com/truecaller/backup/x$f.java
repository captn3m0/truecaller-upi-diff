package com.truecaller.backup;

import c.d.c;
import c.g.a.m;
import c.o.b;
import com.truecaller.ui.components.n;
import java.util.List;
import kotlinx.coroutines.ag;

final class x$f
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  x$f(x paramx, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/backup/x$f;
    x localx = b;
    localf.<init>(localx, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = x.a(b);
        boolean bool2 = ((com.truecaller.common.g.a)paramObject).b("backup_enabled");
        localObject1 = x.b(b);
        if (localObject1 != null) {
          ((w.b)localObject1).a(bool2);
        }
        localObject1 = x.b(b);
        if (localObject1 != null) {
          ((w.b)localObject1).c(bool2);
        }
        localObject1 = x.b(b);
        if (localObject1 != null) {
          ((w.b)localObject1).b(bool2);
        }
        localObject1 = x.b(b);
        if (localObject1 != null) {
          ((w.b)localObject1).d(bool2);
        }
        localObject1 = x.b(b);
        if (localObject1 != null) {
          ((w.b)localObject1).e(bool2);
        }
        paramObject = x.a(b);
        localObject1 = "key_backup_last_success";
        Object localObject2 = b;
        long l1 = x.d((x)localObject2);
        long l2 = ((com.truecaller.common.g.a)paramObject).a((String)localObject1, l1);
        paramObject = b;
        long l3 = x.d((x)paramObject);
        bool2 = l2 < l3;
        if (!bool2)
        {
          paramObject = b;
          x.m((x)paramObject);
        }
        else
        {
          paramObject = b;
          x.a((x)paramObject, l2);
        }
        paramObject = x.a(b);
        localObject1 = "key_backup_frequency_hours";
        localObject2 = b;
        l1 = x.d((x)localObject2);
        l2 = ((com.truecaller.common.g.a)paramObject).a((String)localObject1, l1);
        paramObject = b;
        l3 = x.n((x)paramObject);
        int k = 2;
        int m = 1;
        boolean bool3 = l2 < l3;
        Object localObject3;
        if (!bool3)
        {
          localObject1 = (n)x.o(b).get(0);
        }
        else
        {
          localObject3 = b;
          l3 = x.p((x)localObject3);
          bool3 = l2 < l3;
          if (!bool3)
          {
            localObject1 = (n)x.o(b).get(m);
          }
          else
          {
            localObject3 = b;
            l3 = x.e((x)localObject3);
            bool3 = l2 < l3;
            if (bool3)
            {
              localObject3 = b;
              l3 = x.q((x)localObject3);
              bool3 = l2 < l3;
              if (!bool3)
              {
                localObject1 = x.o(b);
                int n = 3;
                localObject1 = (n)((List)localObject1).get(n);
                break label447;
              }
            }
            localObject1 = (n)x.o(b).get(k);
          }
        }
        label447:
        localObject2 = x.b(b);
        if (localObject2 != null)
        {
          localObject3 = x.o(b);
          ((w.b)localObject2).a((List)localObject3, (n)localObject1);
        }
        localObject1 = x.a(b);
        localObject2 = "backupNetworkType";
        int j = ((com.truecaller.common.g.a)localObject1).a((String)localObject2, m);
        if (j != k) {
          paramObject = (n)x.r(b).get(m);
        } else {
          paramObject = (n)x.r(b).get(0);
        }
        localObject1 = x.b(b);
        if (localObject1 != null)
        {
          localObject2 = x.r(b);
          ((w.b)localObject1).b((List)localObject2, (n)paramObject);
        }
        x.s(b);
        return c.x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = c.x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.x.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */