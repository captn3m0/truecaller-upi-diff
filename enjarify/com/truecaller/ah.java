package com.truecaller;

import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  
  private ah(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static ah a(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6)
  {
    ah localah = new com/truecaller/ah;
    localah.<init>(paramc, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
    return localah;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */