package com.truecaller.update;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.log.AssertionUtil;

public class ForcedUpdateActivity
  extends AppCompatActivity
{
  static void a(Context paramContext, String paramString, boolean paramBoolean)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, ForcedUpdateActivity.class);
    localIntent.addFlags(268533760);
    localIntent.putExtra("updateType", paramString);
    localIntent.putExtra("compactMode", paramBoolean);
    paramContext.startActivity(localIntent);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    if (paramBundle == null)
    {
      paramBundle = getIntent();
      if (paramBundle != null)
      {
        paramBundle = getIntent();
        Object localObject = ForcedUpdate.UpdateType.safeValueOf(paramBundle.getStringExtra("updateType"));
        if (localObject == null)
        {
          AssertionUtil.report(new String[] { "No update type specified" });
          paramBundle = null;
        }
        else
        {
          String str = "compactMode";
          boolean bool1 = paramBundle.getBooleanExtra(str, false);
          if (bool1)
          {
            boolean bool2 = supportsCompactMode;
            if (bool2)
            {
              localObject = a.class.getName();
              paramBundle = paramBundle.getExtras();
              paramBundle = Fragment.instantiate(this, (String)localObject, paramBundle);
              break label124;
            }
          }
          localObject = b.class.getName();
          paramBundle = paramBundle.getExtras();
          paramBundle = Fragment.instantiate(this, (String)localObject, paramBundle);
        }
        label124:
        if (paramBundle != null)
        {
          getSupportFragmentManager().a().b(16908290, paramBundle).c();
          return;
        }
        finish();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.update.ForcedUpdateActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */