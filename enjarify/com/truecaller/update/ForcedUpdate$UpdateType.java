package com.truecaller.update;

public enum ForcedUpdate$UpdateType
{
  public final int action;
  public final int description;
  public final boolean skippable;
  public final boolean supportsCompactMode;
  public final int title;
  
  static
  {
    UpdateType localUpdateType = new com/truecaller/update/ForcedUpdate$UpdateType;
    boolean bool = true;
    Object localObject1 = localUpdateType;
    localUpdateType.<init>("OPTIONAL", 0, false, bool, 2131888143, 2131888142, 2131888148);
    OPTIONAL = localUpdateType;
    localObject1 = new com/truecaller/update/ForcedUpdate$UpdateType;
    ((UpdateType)localObject1).<init>("REQUIRED", 1, true, false, 2131888145, 2131888144, 2131888148);
    REQUIRED = (UpdateType)localObject1;
    localObject1 = new com/truecaller/update/ForcedUpdate$UpdateType;
    Object localObject2 = localObject1;
    ((UpdateType)localObject1).<init>("DISCONTINUED", 2, bool, false, 2131888141, 2131888140, 2131888146);
    DISCONTINUED = (UpdateType)localObject1;
    localObject1 = new UpdateType[3];
    localObject2 = OPTIONAL;
    localObject1[0] = localObject2;
    localObject2 = REQUIRED;
    localObject1[1] = localObject2;
    localObject2 = DISCONTINUED;
    localObject1[2] = localObject2;
    $VALUES = (UpdateType[])localObject1;
  }
  
  private ForcedUpdate$UpdateType(boolean paramBoolean1, boolean paramBoolean2, int paramInt2, int paramInt3, int paramInt4)
  {
    supportsCompactMode = paramBoolean1;
    skippable = paramBoolean2;
    title = paramInt2;
    description = paramInt3;
    action = paramInt4;
  }
  
  public static UpdateType safeValueOf(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    try
    {
      return valueOf(paramString);
    }
    catch (IllegalArgumentException localIllegalArgumentException) {}
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.update.ForcedUpdate.UpdateType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */