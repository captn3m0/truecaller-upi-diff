package com.truecaller.update;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.truecaller.common.b.a;
import com.truecaller.common.b.e;
import com.truecaller.common.network.b.a.c;
import com.truecaller.log.AssertionUtil;

public final class ForcedUpdate
{
  private static int a(Context paramContext)
  {
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      paramContext = paramContext.getPackageName();
      paramContext = localPackageManager.getPackageInfo(paramContext, 0);
      return versionCode;
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localException;
    }
    return 0;
  }
  
  public static ForcedUpdate.UpdateType a()
  {
    return ForcedUpdate.UpdateType.safeValueOf(e.a("forcedUpdate_updateType"));
  }
  
  public static void a(a.c paramc)
  {
    if (paramc != null)
    {
      localObject = a;
      int i = -1;
      int j = ((String)localObject).hashCode();
      int k = -1901282887;
      String str1;
      if (j != k)
      {
        k = -717212158;
        if (j != k)
        {
          k = 703609696;
          if (j == k)
          {
            str1 = "OPTIONAL";
            bool = ((String)localObject).equals(str1);
            if (bool)
            {
              i = 0;
              str2 = null;
            }
          }
        }
        else
        {
          str1 = "RETIRED_VERSION";
          bool = ((String)localObject).equals(str1);
          if (bool) {
            i = 2;
          }
        }
      }
      else
      {
        str1 = "MANDATORY";
        bool = ((String)localObject).equals(str1);
        if (bool) {
          i = 1;
        }
      }
      switch (i)
      {
      default: 
        break;
      case 2: 
        localObject = ForcedUpdate.UpdateType.DISCONTINUED;
        break;
      case 1: 
        localObject = ForcedUpdate.UpdateType.REQUIRED;
        break;
      case 0: 
        localObject = ForcedUpdate.UpdateType.OPTIONAL;
        break;
      }
    }
    boolean bool = false;
    Object localObject = null;
    if (localObject == null)
    {
      e.b("forcedUpdate_updateType");
      e.b("forcedUpdate_link");
      e.b("forcedUpdate_period");
      e.b("forcedUpdate_lastDismissed");
      return;
    }
    localObject = ((ForcedUpdate.UpdateType)localObject).name();
    e.b("forcedUpdate_updateType", (String)localObject);
    String str2 = b;
    e.b("forcedUpdate_link", str2);
    long l = c * 86400000L;
    e.b("forcedUpdate_period", l);
  }
  
  public static boolean a(Context paramContext, boolean paramBoolean)
  {
    String str = b(paramContext, paramBoolean);
    if (str == null) {
      return false;
    }
    ForcedUpdateActivity.a(paramContext, str, paramBoolean);
    return true;
  }
  
  private static String b(Context paramContext, boolean paramBoolean)
  {
    String str = "forcedUpdate_appVersion";
    int i = e.a(str, -1);
    int j = a(paramContext);
    e.b("forcedUpdate_appVersion", j);
    if ((i >= 0) && (j > i))
    {
      a(null);
      return null;
    }
    str = e.a("forcedUpdate_updateType");
    boolean bool1 = TextUtils.isEmpty(str);
    if (bool1) {
      return null;
    }
    Object localObject = ForcedUpdate.UpdateType.valueOf(str);
    if (localObject == null) {
      return null;
    }
    if (paramBoolean)
    {
      paramBoolean = supportsCompactMode;
      if (!paramBoolean) {
        return null;
      }
    }
    paramBoolean = skippable;
    if (paramBoolean)
    {
      long l1 = System.currentTimeMillis();
      long l2 = 0L;
      long l3 = e.a("forcedUpdate_lastDismissed", l2);
      l1 -= l3;
      localObject = "forcedUpdate_period";
      l2 = e.a((String)localObject, l2);
      bool1 = l1 < l2;
      if (bool1) {
        return str;
      }
      return null;
    }
    paramContext = (a)paramContext.getApplicationContext();
    boolean bool2 = paramContext.o();
    if (!bool2) {
      return null;
    }
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.update.ForcedUpdate
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */