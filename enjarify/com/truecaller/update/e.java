package com.truecaller.update;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.widget.Toast;
import c.g.b.k;
import c.l;
import c.n.m;
import com.truecaller.analytics.b;
import com.truecaller.analytics.ba;
import com.truecaller.analytics.e.a;
import com.truecaller.common.h.o;

public final class e
  implements d
{
  private final b a;
  
  public e(b paramb)
  {
    a = paramb;
  }
  
  private static boolean a(Context paramContext, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    String str = "android.intent.action.VIEW";
    paramString = Uri.parse(paramString);
    localIntent.<init>(str, paramString);
    paramString = paramContext.getPackageManager();
    int i = 65536;
    paramString = paramString.resolveActivity(localIntent, i);
    if (paramString != null)
    {
      paramContext.startActivity(localIntent);
      return true;
    }
    return false;
  }
  
  public final void a(Context paramContext, String paramString1, String paramString2)
  {
    k.b(paramContext, "androidContext");
    k.b(paramString2, "callContext");
    Object localObject = paramString1;
    localObject = (CharSequence)paramString1;
    if (localObject != null)
    {
      bool1 = m.a((CharSequence)localObject);
      if (!bool1)
      {
        bool1 = false;
        localObject = null;
        break label53;
      }
    }
    boolean bool1 = true;
    label53:
    if (!bool1)
    {
      if (paramString1 == null) {
        k.a();
      }
      bool1 = a(paramContext, paramString1);
      if (bool1) {
        break label148;
      }
    }
    else
    {
      paramString1 = o.c(paramContext);
      localObject = "IntentUtils.getMarketUri(androidContext)";
      k.a(paramString1, (String)localObject);
      bool1 = a(paramContext, paramString1);
      if (bool1) {
        break label148;
      }
    }
    paramString1 = "https://www.truecaller.com/download";
    boolean bool2 = a(paramContext, paramString1);
    if (bool2)
    {
      paramString1 = "https://www.truecaller.com/download";
    }
    else
    {
      paramContext = Toast.makeText(paramContext, 2131887195, 0);
      paramContext.show();
      bool2 = false;
      paramString1 = null;
    }
    label148:
    if (paramString1 == null) {
      paramString1 = "FAIL";
    }
    paramContext = a;
    localObject = new com/truecaller/analytics/ba;
    ((ba)localObject).<init>(paramString1, paramString2);
    localObject = (com.truecaller.analytics.e)localObject;
    paramContext.b((com.truecaller.analytics.e)localObject);
  }
  
  public final void a(ForcedUpdate.UpdateType paramUpdateType)
  {
    k.b(paramUpdateType, "type");
    b localb = a;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("UpdateShow");
    String str = "Type";
    int[] arrayOfInt = f.a;
    int i = paramUpdateType.ordinal();
    i = arrayOfInt[i];
    switch (i)
    {
    default: 
      paramUpdateType = new c/l;
      paramUpdateType.<init>();
      throw paramUpdateType;
    case 3: 
      paramUpdateType = "RETIRED_VERSION";
      break;
    case 2: 
      paramUpdateType = "MANDATORY";
      break;
    case 1: 
      paramUpdateType = "OPTIONAL";
    }
    paramUpdateType = locala.a(str, paramUpdateType).a();
    k.a(paramUpdateType, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.b(paramUpdateType);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.update.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */