package com.truecaller.update;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class a
  extends b
{
  protected final int a()
  {
    return 17170445;
  }
  
  protected final int b()
  {
    int[] arrayOfInt = a.1.a;
    ForcedUpdate.UpdateType localUpdateType = a;
    int i = localUpdateType.ordinal();
    int j = arrayOfInt[i];
    switch (j)
    {
    default: 
      return 0;
    case 2: 
      return 2131233768;
    }
    return 2131233771;
  }
  
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    int j = 2131364555;
    if (i == j)
    {
      getActivity().finish();
      return;
    }
    super.onClick(paramView);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131558804, paramViewGroup, false);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = a;
    Object localObject = ForcedUpdate.UpdateType.DISCONTINUED;
    if (paramBundle == localObject)
    {
      paramBundle = b;
      localObject = getResources();
      int i = 2131100061;
      int j = ((Resources)localObject).getColor(i);
      paramBundle.setCardBackgroundColor(j);
    }
    paramView.findViewById(2131364555).setOnClickListener(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.update.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */