package com.truecaller.update;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.b.e;
import com.truecaller.log.AssertionUtil;

public class b
  extends Fragment
  implements View.OnClickListener
{
  protected ForcedUpdate.UpdateType a;
  protected CardView b;
  public d c;
  
  protected int a()
  {
    int[] arrayOfInt = b.1.a;
    ForcedUpdate.UpdateType localUpdateType = a;
    int i = localUpdateType.ordinal();
    int j = arrayOfInt[i];
    switch (j)
    {
    default: 
      return 0;
    case 3: 
      return 2131100060;
    case 2: 
      return 2131100063;
    }
    return 2131100059;
  }
  
  protected int b()
  {
    int[] arrayOfInt = b.1.a;
    ForcedUpdate.UpdateType localUpdateType = a;
    int i = localUpdateType.ordinal();
    int j = arrayOfInt[i];
    switch (j)
    {
    default: 
      return 0;
    case 3: 
      return 2131233767;
    case 2: 
      return 2131233770;
    }
    return 2131233769;
  }
  
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    int j = 2131362205;
    Object localObject1;
    if (i == j) {
      try
      {
        paramView = a;
        localObject1 = ForcedUpdate.UpdateType.DISCONTINUED;
        Object localObject2;
        Object localObject3;
        if (paramView == localObject1)
        {
          paramView = new android/content/Intent;
          localObject1 = "android.intent.action.UNINSTALL_PACKAGE";
          localObject2 = "package";
          localObject3 = getContext();
          localObject3 = ((Context)localObject3).getPackageName();
          localObject2 = Uri.fromParts((String)localObject2, (String)localObject3, null);
          paramView.<init>((String)localObject1, (Uri)localObject2);
          startActivity(paramView);
          return;
        }
        paramView = getContext();
        if (paramView != null)
        {
          localObject1 = c;
          localObject2 = "forcedUpdate_link";
          localObject2 = e.a((String)localObject2);
          localObject3 = "forcedUpdate";
          ((d)localObject1).a(paramView, (String)localObject2, (String)localObject3);
        }
        return;
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        com.truecaller.log.d.a(localActivityNotFoundException);
        return;
      }
    }
    int k = paramView.getId();
    i = 2131362220;
    if (k == i)
    {
      long l = System.currentTimeMillis();
      e.b("forcedUpdate_lastDismissed", l);
      paramView = getContext().getPackageManager();
      localObject1 = getContext().getPackageName();
      paramView = paramView.getLaunchIntentForPackage((String)localObject1);
      startActivity(paramView);
      paramView = getActivity();
      paramView.finish();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getArguments();
    if (paramBundle != null)
    {
      String str = "updateType";
      paramBundle = paramBundle.getString(str);
      boolean bool = TextUtils.isEmpty(paramBundle);
      if (!bool)
      {
        paramBundle = ForcedUpdate.UpdateType.safeValueOf(paramBundle);
        a = paramBundle;
      }
    }
    paramBundle = a;
    if (paramBundle == null)
    {
      AssertionUtil.report(new String[] { "Update type not specified" });
      getActivity().finish();
      return;
    }
    TrueApp.y().a().cz().a(this);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131558803, paramViewGroup, false);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    paramBundle = getActivity().getWindow();
    int i = a();
    paramBundle.setBackgroundDrawableResource(i);
    paramBundle = (CardView)paramView.findViewById(2131362205);
    b = paramBundle;
    b.setOnClickListener(this);
    paramBundle = (TextView)paramView.findViewById(2131364884);
    i = a.title;
    paramBundle.setText(i);
    paramBundle = (TextView)paramView.findViewById(2131362810);
    i = a.description;
    paramBundle.setText(i);
    paramBundle = (TextView)paramView.findViewById(2131362206);
    ForcedUpdate.UpdateType localUpdateType = a;
    i = action;
    paramBundle.setText(i);
    paramBundle = (ImageView)paramView.findViewById(2131363323);
    i = b();
    paramBundle.setImageResource(i);
    int j = 2131362220;
    paramView = paramView.findViewById(j);
    if (paramView != null)
    {
      paramBundle = a;
      boolean bool = skippable;
      if (bool)
      {
        paramView.setOnClickListener(this);
      }
      else
      {
        int k = 8;
        paramView.setVisibility(k);
      }
    }
    paramView = c;
    paramBundle = a;
    paramView.a(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.update.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */