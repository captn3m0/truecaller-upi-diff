package com.truecaller.featuretoggles;

public enum FirebaseFlavor
{
  static
  {
    FirebaseFlavor[] arrayOfFirebaseFlavor = new FirebaseFlavor[4];
    FirebaseFlavor localFirebaseFlavor = new com/truecaller/featuretoggles/FirebaseFlavor;
    localFirebaseFlavor.<init>("BOOLEAN", 0);
    BOOLEAN = localFirebaseFlavor;
    arrayOfFirebaseFlavor[0] = localFirebaseFlavor;
    localFirebaseFlavor = new com/truecaller/featuretoggles/FirebaseFlavor;
    int i = 1;
    localFirebaseFlavor.<init>("STRING", i);
    STRING = localFirebaseFlavor;
    arrayOfFirebaseFlavor[i] = localFirebaseFlavor;
    localFirebaseFlavor = new com/truecaller/featuretoggles/FirebaseFlavor;
    i = 2;
    localFirebaseFlavor.<init>("INTEGER", i);
    INTEGER = localFirebaseFlavor;
    arrayOfFirebaseFlavor[i] = localFirebaseFlavor;
    localFirebaseFlavor = new com/truecaller/featuretoggles/FirebaseFlavor;
    i = 3;
    localFirebaseFlavor.<init>("LONG", i);
    LONG = localFirebaseFlavor;
    arrayOfFirebaseFlavor[i] = localFirebaseFlavor;
    $VALUES = arrayOfFirebaseFlavor;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.FirebaseFlavor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */