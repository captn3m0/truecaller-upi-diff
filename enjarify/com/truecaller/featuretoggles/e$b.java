package com.truecaller.featuretoggles;

public final class e$b
  implements b
{
  private final boolean d;
  private final String e;
  private final String f;
  
  e$b(boolean paramBoolean, String paramString1, String paramString2)
  {
    d = paramBoolean;
    e = paramString1;
    f = paramString2;
  }
  
  public final boolean a()
  {
    return d;
  }
  
  public final String b()
  {
    return e;
  }
  
  public final String c()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */