package com.truecaller.featuretoggles;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import c.n.m;

public final class g
  implements b, k
{
  final String a;
  final FirebaseFlavor b;
  private final b c;
  private final p d;
  private final SharedPreferences e;
  
  public g(b paramb, p paramp, String paramString, SharedPreferences paramSharedPreferences, FirebaseFlavor paramFirebaseFlavor)
  {
    c = paramb;
    d = paramp;
    a = paramString;
    e = paramSharedPreferences;
    b = paramFirebaseFlavor;
  }
  
  public final int a(int paramInt)
  {
    Object localObject1 = e;
    String str1 = a;
    Object localObject2 = d;
    c.g.b.k.b(localObject1, "receiver$0");
    c.g.b.k.b(str1, "key");
    String str2 = "valueProvider";
    c.g.b.k.b(localObject2, str2);
    localObject2 = ((p)localObject2).a(str1);
    localObject1 = ((SharedPreferences)localObject1).getString(str1, (String)localObject2);
    if (localObject1 != null)
    {
      localObject1 = m.b((String)localObject1);
      if (localObject1 != null) {
        return ((Integer)localObject1).intValue();
      }
    }
    return paramInt;
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "newValue");
    Object localObject1 = b;
    Object localObject2 = FirebaseFlavor.BOOLEAN;
    if (localObject1 != localObject2)
    {
      localObject1 = e;
      localObject2 = a;
      c.g.b.k.b(localObject1, "receiver$0");
      c.g.b.k.b(localObject2, "key");
      ((SharedPreferences)localObject1).edit().putString((String)localObject2, paramString).apply();
      return;
    }
    paramString = new java/lang/RuntimeException;
    paramString.<init>("Firebase flavor can not be boolean when setting new value!");
    throw ((Throwable)paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject1 = b;
    Object localObject2 = FirebaseFlavor.BOOLEAN;
    if (localObject1 == localObject2)
    {
      localObject1 = e;
      localObject2 = a;
      o.a((SharedPreferences)localObject1, (String)localObject2, paramBoolean);
    }
  }
  
  public final boolean a()
  {
    Object localObject1 = b;
    Object localObject2 = FirebaseFlavor.BOOLEAN;
    if (localObject1 == localObject2)
    {
      localObject1 = e;
      localObject2 = a;
      boolean bool = d.b((String)localObject2);
      return ((SharedPreferences)localObject1).getBoolean((String)localObject2, bool);
    }
    return false;
  }
  
  public final String b()
  {
    return c.b();
  }
  
  public final String c()
  {
    return c.c();
  }
  
  public final String d()
  {
    return a;
  }
  
  public final String e()
  {
    Object localObject1 = b;
    Object localObject2 = FirebaseFlavor.BOOLEAN;
    if (localObject1 != localObject2)
    {
      localObject1 = e;
      localObject2 = a;
      String str = d.a((String)localObject2);
      localObject1 = ((SharedPreferences)localObject1).getString((String)localObject2, str);
      c.g.b.k.a(localObject1, "prefs.getString(firebase…r.getString(firebaseKey))");
      return (String)localObject1;
    }
    return "";
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof g;
      if (bool1)
      {
        paramObject = (g)paramObject;
        Object localObject1 = c;
        Object localObject2 = c;
        bool1 = c.g.b.k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = d;
          localObject2 = d;
          bool1 = c.g.b.k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = a;
            localObject2 = a;
            bool1 = c.g.b.k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = e;
              localObject2 = e;
              bool1 = c.g.b.k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = b;
                paramObject = b;
                boolean bool2 = c.g.b.k.a(localObject1, paramObject);
                if (bool2) {
                  break label134;
                }
              }
            }
          }
        }
      }
      return false;
    }
    label134:
    return true;
  }
  
  public final FirebaseFlavor f()
  {
    return b;
  }
  
  public final long g()
  {
    Object localObject1 = e;
    String str1 = a;
    Object localObject2 = d;
    c.g.b.k.b(localObject1, "receiver$0");
    c.g.b.k.b(str1, "key");
    String str2 = "valueProvider";
    c.g.b.k.b(localObject2, str2);
    localObject2 = ((p)localObject2).a(str1);
    localObject1 = ((SharedPreferences)localObject1).getString(str1, (String)localObject2);
    if (localObject1 != null)
    {
      localObject1 = m.d((String)localObject1);
      if (localObject1 != null) {
        return ((Long)localObject1).longValue();
      }
    }
    return 2;
  }
  
  public final void h()
  {
    SharedPreferences localSharedPreferences = e;
    String str = a;
    c.g.b.k.b(localSharedPreferences, "receiver$0");
    c.g.b.k.b(str, "key");
    localSharedPreferences.edit().remove(str).apply();
  }
  
  public final int hashCode()
  {
    b localb = c;
    int i = 0;
    if (localb != null)
    {
      j = localb.hashCode();
    }
    else
    {
      j = 0;
      localb = null;
    }
    j *= 31;
    Object localObject = d;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = a;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = e;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = b;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FirebaseFeatureImpl(feature=");
    Object localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", valueProvider=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", firebaseKey=");
    localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", prefs=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", firebaseFlavor=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */