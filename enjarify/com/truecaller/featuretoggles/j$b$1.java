package com.truecaller.featuretoggles;

public final class j$b$1
  implements b
{
  private final boolean b;
  private final String c;
  private final String d;
  
  j$b$1(j.b paramb)
  {
    boolean bool = ((g)a.a).a();
    b = bool;
    paramb = a.a.b();
    c = paramb;
    paramb = a.a.c();
    d = paramb;
  }
  
  public final boolean a()
  {
    return b;
  }
  
  public final String b()
  {
    return c;
  }
  
  public final String c()
  {
    return d;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.j.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */