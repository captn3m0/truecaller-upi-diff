package com.truecaller.featuretoggles;

import android.content.SharedPreferences;
import java.util.Iterator;

public final class j
  extends e
{
  private final SharedPreferences U;
  
  public j(a parama, SharedPreferences paramSharedPreferences, d paramd, p paramp)
  {
    super(parama, paramSharedPreferences, paramd, paramp);
    U = paramSharedPreferences;
    parama = ((Iterable)a()).iterator();
    for (;;)
    {
      boolean bool1 = parama.hasNext();
      if (!bool1) {
        break;
      }
      paramSharedPreferences = (b)parama.next();
      boolean bool2 = paramSharedPreferences instanceof n;
      if (bool2)
      {
        paramd = new com/truecaller/featuretoggles/j$a;
        paramd.<init>((b)paramSharedPreferences, this, paramp);
        paramd = (c.g.a.b)paramd;
        a((b)paramSharedPreferences, paramd);
      }
      else
      {
        bool2 = paramSharedPreferences instanceof g;
        if (bool2)
        {
          paramd = new com/truecaller/featuretoggles/j$b;
          paramd.<init>((b)paramSharedPreferences, this, paramp);
          paramd = (c.g.a.b)paramd;
          a((b)paramSharedPreferences, paramd);
        }
        else
        {
          paramd = new com/truecaller/featuretoggles/j$c;
          paramd.<init>(this, paramp);
          paramd = (c.g.a.b)paramd;
          a(paramSharedPreferences, paramd);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */