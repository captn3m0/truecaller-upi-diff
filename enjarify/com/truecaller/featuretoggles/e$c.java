package com.truecaller.featuretoggles;

public final class e$c
  implements b
{
  private final boolean c;
  private final String d;
  private final String e;
  
  e$c(String paramString1, String paramString2)
  {
    d = paramString1;
    e = paramString2;
  }
  
  public final boolean a()
  {
    return c;
  }
  
  public final String b()
  {
    return d;
  }
  
  public final String c()
  {
    return e;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */