package com.truecaller.featuretoggles;

import android.content.SharedPreferences;
import c.g.b.k;

public final class n
  implements b, l
{
  final String a;
  final boolean b;
  final SharedPreferences c;
  private boolean d;
  private final b e;
  
  public n(String paramString, boolean paramBoolean, SharedPreferences paramSharedPreferences, b paramb)
  {
    a = paramString;
    b = paramBoolean;
    c = paramSharedPreferences;
    e = paramb;
  }
  
  private final void a(c.g.a.b paramb)
  {
    Object localObject = e;
    boolean bool = localObject instanceof h;
    if (bool)
    {
      paramb.invoke(localObject);
      return;
    }
    paramb = new java/lang/IllegalStateException;
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Attempted to mutate compile time value in release mode. Feature: ");
    String str = b();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(" + ");
    str = c();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    paramb.<init>((String)localObject);
    throw ((Throwable)paramb);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = new com/truecaller/featuretoggles/n$a;
    ((n.a)localObject).<init>(paramBoolean);
    localObject = (c.g.a.b)localObject;
    a((c.g.a.b)localObject);
  }
  
  public final boolean a()
  {
    b localb = e;
    boolean bool = localb.a();
    if (bool)
    {
      bool = b;
      if (!bool)
      {
        bool = e();
        if (!bool) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  public final String b()
  {
    return e.b();
  }
  
  public final void b(boolean paramBoolean)
  {
    SharedPreferences localSharedPreferences = c;
    String str = a;
    o.a(localSharedPreferences, str, paramBoolean);
  }
  
  public final String c()
  {
    return e.c();
  }
  
  public final String d()
  {
    return a;
  }
  
  public final boolean e()
  {
    SharedPreferences localSharedPreferences = c;
    String str = a;
    boolean bool = d;
    return localSharedPreferences.getBoolean(str, bool);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof n;
      if (bool2)
      {
        paramObject = (n)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          bool2 = b;
          boolean bool3 = b;
          if (bool2 == bool3)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            localObject1 = null;
          }
          if (bool2)
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              localObject1 = e;
              paramObject = e;
              boolean bool4 = k.a(localObject1, paramObject);
              if (bool4) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final boolean f()
  {
    return e.a();
  }
  
  public final boolean g()
  {
    return b;
  }
  
  public final void h()
  {
    c.g.a.b localb = (c.g.a.b)n.b.a;
    a(localb);
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    int k = b;
    if (k != 0) {
      k = 1;
    }
    int j = (j + k) * 31;
    Object localObject = c;
    int m;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = e;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RemoteFeatureImpl(remoteKey=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", ignoreRemote=");
    boolean bool = b;
    localStringBuilder.append(bool);
    localStringBuilder.append(", prefs=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", delegate=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */