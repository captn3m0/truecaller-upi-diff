package com.truecaller.featuretoggles;

import c.g.b.k;
import java.util.LinkedHashMap;
import java.util.Map;

public final class d$a
{
  public final Map a;
  
  public d$a()
  {
    Object localObject = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject).<init>();
    localObject = (Map)localObject;
    a = ((Map)localObject);
  }
  
  public final a a(d.b paramb)
  {
    k.b(paramb, "observer");
    Map localMap = a;
    String str = paramb.a();
    localMap.put(str, paramb);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */