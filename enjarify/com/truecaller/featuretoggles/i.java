package com.truecaller.featuretoggles;

import android.content.SharedPreferences;

public final class i
  implements b, h
{
  private boolean a;
  private final b b;
  private final SharedPreferences c;
  
  public i(b paramb, SharedPreferences paramSharedPreferences)
  {
    b = paramb;
    c = paramSharedPreferences;
    boolean bool = b.a();
    a = bool;
  }
  
  public final void a(boolean paramBoolean)
  {
    SharedPreferences localSharedPreferences = c;
    String str = b();
    o.a(localSharedPreferences, str, paramBoolean);
  }
  
  public final boolean a()
  {
    SharedPreferences localSharedPreferences = c;
    String str = b();
    boolean bool = a;
    return localSharedPreferences.getBoolean(str, bool);
  }
  
  public final String b()
  {
    return b.b();
  }
  
  public final String c()
  {
    return b.c();
  }
  
  public final void h()
  {
    SharedPreferences localSharedPreferences = c;
    String str = b();
    boolean bool = b.a();
    o.a(localSharedPreferences, str, bool);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */