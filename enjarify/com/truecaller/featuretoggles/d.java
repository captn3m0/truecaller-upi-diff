package com.truecaller.featuretoggles;

import c.g.b.k;
import java.util.Map;

public final class d
{
  private final Map a;
  
  private d(Map paramMap)
  {
    a = paramMap;
  }
  
  public final void a(m paramm)
  {
    k.b(paramm, "feature");
    Object localObject = a;
    String str = paramm.d();
    localObject = (d.b)((Map)localObject).get(str);
    if (localObject == null) {
      return;
    }
    boolean bool = paramm.a();
    if (bool)
    {
      ((d.b)localObject).b();
      return;
    }
    ((d.b)localObject).c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */