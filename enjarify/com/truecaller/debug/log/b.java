package com.truecaller.debug.log;

import android.content.Context;
import com.truecaller.utils.extensions.d;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.zip.GZIPOutputStream;

public final class b
{
  final BlockingQueue a;
  private String b;
  private long c;
  private int d;
  private Thread e;
  
  public b()
  {
    LinkedBlockingQueue localLinkedBlockingQueue = new java/util/concurrent/LinkedBlockingQueue;
    localLinkedBlockingQueue.<init>();
    a = localLinkedBlockingQueue;
  }
  
  private File a(int paramInt)
  {
    File localFile = new java/io/File;
    String str1;
    if (paramInt == 0)
    {
      str1 = b;
    }
    else
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      String str2 = b;
      localStringBuilder.append(str2);
      str2 = ".";
      localStringBuilder.append(str2);
      localStringBuilder.append(paramInt);
      str1 = localStringBuilder.toString();
    }
    localFile.<init>(str1);
    return localFile;
  }
  
  private void b()
  {
    int i = 0;
    File localFile1 = null;
    for (;;)
    {
      int j = i + 1;
      try
      {
        File localFile2 = a(j);
        boolean bool = localFile2.exists();
        if (bool)
        {
          i = j;
          continue;
        }
        while (i > 0)
        {
          localObject2 = a(i);
          int k = d;
          if (i >= k)
          {
            ((File)localObject2).delete();
          }
          else
          {
            k = i + 1;
            localFile2 = a(k);
            ((File)localObject2).renameTo(localFile2);
          }
          i += -1;
        }
        localFile1 = new java/io/File;
        Object localObject2 = b;
        localFile1.<init>((String)localObject2);
        j = 1;
        localObject2 = a(j);
        localFile1.renameTo((File)localObject2);
        return;
      }
      finally {}
    }
  }
  
  public final void a(Context paramContext, String paramString)
  {
    try
    {
      Object localObject = e;
      if (localObject == null)
      {
        localObject = new java/io/File;
        paramContext = paramContext.getFilesDir();
        String str = "logs";
        ((File)localObject).<init>(paramContext, str);
        ((File)localObject).mkdirs();
        paramContext = new java/io/File;
        paramContext.<init>((File)localObject, paramString);
        paramContext = paramContext.getAbsolutePath();
        b = paramContext;
        long l = 50000L;
        c = l;
        int i = 2;
        d = i;
        paramContext = new java/lang/Thread;
        paramString = new com/truecaller/debug/log/b$a;
        localObject = null;
        paramString.<init>(this, (byte)0);
        paramContext.<init>(paramString);
        e = paramContext;
        paramContext = e;
        paramContext.start();
      }
      return;
    }
    finally {}
  }
  
  public final void a(String paramString)
  {
    a.add(paramString);
  }
  
  public final byte[] a()
  {
    for (int i = -1;; i = j)
    {
      j = i + 1;
      localObject1 = a(j);
      boolean bool = ((File)localObject1).exists();
      if (!bool) {
        break;
      }
    }
    int j = 8192;
    byte[] arrayOfByte = new byte[j];
    Object localObject1 = new java/io/ByteArrayOutputStream;
    ((ByteArrayOutputStream)localObject1).<init>();
    GZIPOutputStream localGZIPOutputStream = new java/util/zip/GZIPOutputStream;
    localGZIPOutputStream.<init>((OutputStream)localObject1);
    while (i >= 0) {
      try
      {
        File localFile = a(i);
        Object localObject2 = null;
        try
        {
          FileInputStream localFileInputStream = new java/io/FileInputStream;
          localFileInputStream.<init>(localFile);
          try
          {
            for (;;)
            {
              int k = localFileInputStream.read(arrayOfByte);
              if (k < 0) {
                break;
              }
              localObject2 = null;
              localGZIPOutputStream.write(arrayOfByte, 0, k);
            }
            d.a(localFileInputStream);
            i += -1;
            continue;
          }
          finally
          {
            localObject2 = localFileInputStream;
          }
          d.a((Closeable)localObject2);
        }
        finally {}
        throw ((Throwable)localObject4);
      }
      finally
      {
        d.a(localGZIPOutputStream);
      }
    }
    d.a(localGZIPOutputStream);
    return ((ByteArrayOutputStream)localObject1).toByteArray();
  }
  
  /* Error */
  final void b(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 25	java/io/File
    //   5: astore_2
    //   6: aload_0
    //   7: getfield 27	com/truecaller/debug/log/b:b	Ljava/lang/String;
    //   10: astore_3
    //   11: aload_2
    //   12: aload_3
    //   13: invokespecial 46	java/io/File:<init>	(Ljava/lang/String;)V
    //   16: aconst_null
    //   17: astore_3
    //   18: new 141	java/io/FileWriter
    //   21: astore 4
    //   23: iconst_1
    //   24: istore 5
    //   26: aload 4
    //   28: aload_2
    //   29: iload 5
    //   31: invokespecial 144	java/io/FileWriter:<init>	(Ljava/io/File;Z)V
    //   34: aload 4
    //   36: aload_1
    //   37: invokevirtual 147	java/io/FileWriter:append	(Ljava/lang/CharSequence;)Ljava/io/Writer;
    //   40: pop
    //   41: ldc -107
    //   43: astore_1
    //   44: aload 4
    //   46: aload_1
    //   47: invokevirtual 147	java/io/FileWriter:append	(Ljava/lang/CharSequence;)Ljava/io/Writer;
    //   50: pop
    //   51: aload 4
    //   53: invokestatic 135	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   56: goto +36 -> 92
    //   59: astore_1
    //   60: aload 4
    //   62: astore_3
    //   63: goto +60 -> 123
    //   66: astore_1
    //   67: aload 4
    //   69: astore_3
    //   70: goto +8 -> 78
    //   73: astore_1
    //   74: goto +49 -> 123
    //   77: astore_1
    //   78: ldc -105
    //   80: astore 4
    //   82: aload_1
    //   83: aload 4
    //   85: invokestatic 156	com/truecaller/log/d:a	(Ljava/lang/Throwable;Ljava/lang/String;)V
    //   88: aload_3
    //   89: invokestatic 135	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   92: aload_2
    //   93: invokevirtual 160	java/io/File:length	()J
    //   96: lstore 6
    //   98: aload_0
    //   99: getfield 88	com/truecaller/debug/log/b:c	J
    //   102: lstore 8
    //   104: lload 6
    //   106: lload 8
    //   108: lcmp
    //   109: istore 10
    //   111: iload 10
    //   113: ifle +7 -> 120
    //   116: aload_0
    //   117: invokespecial 162	com/truecaller/debug/log/b:b	()V
    //   120: aload_0
    //   121: monitorexit
    //   122: return
    //   123: aload_3
    //   124: invokestatic 135	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   127: aload_1
    //   128: athrow
    //   129: astore_1
    //   130: aload_0
    //   131: monitorexit
    //   132: aload_1
    //   133: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	134	0	this	b
    //   0	134	1	paramString	String
    //   5	88	2	localFile	File
    //   10	114	3	localObject1	Object
    //   21	63	4	localObject2	Object
    //   24	6	5	bool1	boolean
    //   96	9	6	l1	long
    //   102	5	8	l2	long
    //   109	3	10	bool2	boolean
    // Exception table:
    //   from	to	target	type
    //   36	41	59	finally
    //   46	51	59	finally
    //   36	41	66	java/io/IOException
    //   46	51	66	java/io/IOException
    //   18	21	73	finally
    //   29	34	73	finally
    //   83	88	73	finally
    //   18	21	77	java/io/IOException
    //   29	34	77	java/io/IOException
    //   2	5	129	finally
    //   6	10	129	finally
    //   12	16	129	finally
    //   51	56	129	finally
    //   88	92	129	finally
    //   92	96	129	finally
    //   98	102	129	finally
    //   116	120	129	finally
    //   123	127	129	finally
    //   127	129	129	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.debug.log.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */