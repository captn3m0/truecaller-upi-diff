package com.truecaller.debug.log;

public final class R$drawable
{
  public static final int common_full_open_on_phone = 2131231050;
  public static final int common_google_signin_btn_icon_dark = 2131231051;
  public static final int common_google_signin_btn_icon_dark_focused = 2131231052;
  public static final int common_google_signin_btn_icon_dark_normal = 2131231053;
  public static final int common_google_signin_btn_icon_dark_normal_background = 2131231054;
  public static final int common_google_signin_btn_icon_disabled = 2131231055;
  public static final int common_google_signin_btn_icon_light = 2131231056;
  public static final int common_google_signin_btn_icon_light_focused = 2131231057;
  public static final int common_google_signin_btn_icon_light_normal = 2131231058;
  public static final int common_google_signin_btn_icon_light_normal_background = 2131231059;
  public static final int common_google_signin_btn_text_dark = 2131231060;
  public static final int common_google_signin_btn_text_dark_focused = 2131231061;
  public static final int common_google_signin_btn_text_dark_normal = 2131231062;
  public static final int common_google_signin_btn_text_dark_normal_background = 2131231063;
  public static final int common_google_signin_btn_text_disabled = 2131231064;
  public static final int common_google_signin_btn_text_light = 2131231065;
  public static final int common_google_signin_btn_text_light_focused = 2131231066;
  public static final int common_google_signin_btn_text_light_normal = 2131231067;
  public static final int common_google_signin_btn_text_light_normal_background = 2131231068;
  public static final int googleg_disabled_color_18 = 2131233773;
  public static final int googleg_standard_color_18 = 2131233774;
  public static final int notification_action_background = 2131234779;
  public static final int notification_bg = 2131234780;
  public static final int notification_bg_low = 2131234781;
  public static final int notification_bg_low_normal = 2131234782;
  public static final int notification_bg_low_pressed = 2131234783;
  public static final int notification_bg_normal = 2131234784;
  public static final int notification_bg_normal_pressed = 2131234785;
  public static final int notification_icon_background = 2131234786;
  public static final int notification_template_icon_bg = 2131234788;
  public static final int notification_template_icon_low_bg = 2131234789;
  public static final int notification_tile_bg = 2131234790;
  public static final int notify_panel_notification_icon_bg = 2131234791;
}

/* Location:
 * Qualified Name:     com.truecaller.debug.log.R.drawable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */