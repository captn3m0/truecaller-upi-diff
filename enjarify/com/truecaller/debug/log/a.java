package com.truecaller.debug.log;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.utils.extensions.j;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class a
{
  public static final a a;
  private static final SimpleDateFormat b;
  private static String c;
  private static b d;
  
  static
  {
    Object localObject = new com/truecaller/debug/log/a;
    ((a)localObject).<init>();
    a = (a)localObject;
    localObject = new java/text/SimpleDateFormat;
    Locale localLocale = Locale.US;
    ((SimpleDateFormat)localObject).<init>("yyyy-MM-dd'T'HH:mm:ss.SSSZ", localLocale);
    b = (SimpleDateFormat)localObject;
  }
  
  public static final ao a(Context paramContext)
  {
    k.b(paramContext, "context");
    ag localag = (ag)bg.a;
    f localf = (f)j.a();
    Object localObject = new com/truecaller/debug/log/a$a;
    ((a.a)localObject).<init>(paramContext, null);
    localObject = (m)localObject;
    return e.a(localag, localf, (m)localObject, 2);
  }
  
  public static final void a(Context paramContext, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramString, "fileProviderAuthority");
    c = paramString;
    paramString = new com/truecaller/debug/log/b;
    paramString.<init>();
    paramContext = paramContext.getApplicationContext();
    paramString.a(paramContext, "debug.log");
    d = paramString;
  }
  
  public static final void a(Object... paramVarArgs)
  {
    k.b(paramVarArgs, "events");
    Object localObject1 = new java/lang/StringBuilder;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    Object localObject3 = b;
    Object localObject4 = new java/util/Date;
    ((Date)localObject4).<init>();
    localObject3 = ((SimpleDateFormat)localObject3).format((Date)localObject4);
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append(": ");
    localObject2 = ((StringBuilder)localObject2).toString();
    ((StringBuilder)localObject1).<init>((String)localObject2);
    int i = paramVarArgs.length;
    int j = 0;
    localObject3 = null;
    while (j < i)
    {
      localObject4 = paramVarArgs[j];
      ((StringBuilder)localObject1).append(localObject4);
      j += 1;
    }
    paramVarArgs = d;
    if (paramVarArgs == null)
    {
      localObject2 = "logger";
      k.a((String)localObject2);
    }
    localObject1 = ((StringBuilder)localObject1).toString();
    paramVarArgs.a((String)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.debug.log.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */