package com.truecaller.debug.log;

public final class R$style
{
  public static final int TextAppearance_Compat_Notification = 2131952010;
  public static final int TextAppearance_Compat_Notification_Info = 2131952011;
  public static final int TextAppearance_Compat_Notification_Info_Media = 2131952012;
  public static final int TextAppearance_Compat_Notification_Line2 = 2131952013;
  public static final int TextAppearance_Compat_Notification_Line2_Media = 2131952014;
  public static final int TextAppearance_Compat_Notification_Media = 2131952015;
  public static final int TextAppearance_Compat_Notification_Time = 2131952016;
  public static final int TextAppearance_Compat_Notification_Time_Media = 2131952017;
  public static final int TextAppearance_Compat_Notification_Title = 2131952018;
  public static final int TextAppearance_Compat_Notification_Title_Media = 2131952019;
  public static final int Widget_Compat_NotificationActionContainer = 2131952288;
  public static final int Widget_Compat_NotificationActionText = 2131952289;
  public static final int Widget_Support_CoordinatorLayout = 2131952336;
}

/* Location:
 * Qualified Name:     com.truecaller.debug.log.R.style
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */