package com.truecaller.debug.log;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import java.io.File;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ax;
import kotlinx.coroutines.g;

final class a$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  a$a(Context paramContext, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/debug/log/a$a;
    Context localContext = c;
    locala.<init>(localContext, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = b;
    boolean bool;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject1 = (File)a;
      bool = paramObject instanceof o.b;
      if (!bool) {
        paramObject = localObject1;
      } else {
        throw a;
      }
      break;
    case 0: 
      bool = paramObject instanceof o.b;
      if (bool) {
        break label270;
      }
      paramObject = new java/io/File;
      Object localObject2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("truecallerDebugLogs_");
      long l = System.currentTimeMillis();
      ((StringBuilder)localObject3).append(l);
      ((StringBuilder)localObject3).append(".gz");
      localObject3 = ((StringBuilder)localObject3).toString();
      ((File)paramObject).<init>((File)localObject2, (String)localObject3);
      localObject2 = (f)ax.b();
      localObject3 = new com/truecaller/debug/log/a$a$1;
      localObject4 = null;
      ((a.a.1)localObject3).<init>((File)paramObject, null);
      localObject3 = (m)localObject3;
      a = paramObject;
      int j = 1;
      b = j;
      localObject2 = g.a((f)localObject2, (m)localObject3, this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      break;
    }
    localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>("android.intent.action.SEND");
    Object localObject3 = c;
    Object localObject4 = a.a;
    localObject4 = a.b();
    paramObject = (Parcelable)FileProvider.a((Context)localObject3, (String)localObject4, (File)paramObject);
    ((Intent)localObject1).putExtra("android.intent.extra.STREAM", (Parcelable)paramObject);
    ((Intent)localObject1).setType("application/gzip");
    c.startActivity((Intent)localObject1);
    return x.a;
    label270:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.debug.log.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */