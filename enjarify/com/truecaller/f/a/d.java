package com.truecaller.f.a;

import android.support.v4.app.j;
import android.view.View;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.engagementrewards.c;
import com.truecaller.engagementrewards.ui.h;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.util.al;

public final class d
  extends a
{
  private final String d;
  private final int e;
  private final int f;
  private final j g;
  private final c h;
  private final br i;
  
  public d(j paramj, com.truecaller.f.a parama, com.truecaller.featuretoggles.e parame, b paramb, al paramal, com.truecaller.utils.a parama1, c paramc, br parambr)
  {
    super(parama, parame, paramb, paramal, parama1);
    g = paramj;
    h = paramc;
    i = parambr;
    d = "engreward";
    e = 2131234162;
    paramj = h;
    parama = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
    paramj = paramj.a(parama);
    parama = e.a;
    int j = paramj.ordinal();
    j = parama[j];
    switch (j)
    {
    default: 
      j = 0;
      paramj = null;
      break;
    case 2: 
      j = 2131887981;
      break;
    case 1: 
      j = 2131887980;
    }
    f = j;
  }
  
  public final void a(View paramView)
  {
    k.b(paramView, "view");
    super.a(paramView);
    paramView = paramView.getContext();
    Object localObject1 = h;
    Object localObject2 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
    localObject1 = ((c)localObject1).a((EngagementRewardActionType)localObject2);
    localObject2 = e.b;
    int j = ((EngagementRewardState)localObject1).ordinal();
    int k = localObject2[j];
    switch (k)
    {
    default: 
      paramView = new java/lang/IllegalArgumentException;
      localObject1 = String.valueOf(localObject1);
      localObject1 = "Wrong state ".concat((String)localObject1);
      paramView.<init>((String)localObject1);
      throw ((Throwable)paramView);
    case 2: 
      paramView = new com/truecaller/engagementrewards/ui/h;
      paramView.<init>();
      localObject1 = g;
      paramView.show((j)localObject1, "RewardsRedeemCelebrationDialog");
      return;
    }
    k.a(paramView, "this");
    localObject1 = PremiumPresenterView.LaunchContext.CALL_LOG_PROMO;
    br.b(paramView, (PremiumPresenterView.LaunchContext)localObject1);
  }
  
  public final boolean b()
  {
    boolean bool1 = super.b();
    if (bool1)
    {
      Object localObject1 = h;
      bool1 = ((c)localObject1).b();
      if (bool1)
      {
        localObject1 = h;
        Object localObject2 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
        localObject1 = ((c)localObject1).a((EngagementRewardActionType)localObject2);
        localObject2 = EngagementRewardState.COMPLETED;
        boolean bool2 = true;
        if (localObject1 != localObject2)
        {
          localObject2 = EngagementRewardState.REDEEMED;
          if (localObject1 != localObject2)
          {
            j = 0;
            localObject2 = null;
            break label73;
          }
        }
        int j = 1;
        label73:
        if (j == 0)
        {
          bool1 = false;
          localObject1 = null;
        }
        if (localObject1 != null) {
          return bool2;
        }
      }
    }
    return false;
  }
  
  public final String e()
  {
    return d;
  }
  
  public final int f()
  {
    return e;
  }
  
  public final int g()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.f.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */