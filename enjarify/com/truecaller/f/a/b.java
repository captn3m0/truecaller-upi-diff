package com.truecaller.f.a;

import android.support.v4.app.j;
import android.view.View;
import c.g.b.k;
import com.truecaller.common.f.c;
import com.truecaller.featuretoggles.e;
import com.truecaller.startup_dialogs.fragments.f;
import com.truecaller.util.al;
import com.truecaller.utils.d;

public final class b
  extends a
{
  private final String d;
  private final int e;
  private final int f;
  private final j g;
  private final c h;
  private final d i;
  
  public b(j paramj, com.truecaller.f.a parama, e parame, c paramc, com.truecaller.analytics.b paramb, d paramd, al paramal, com.truecaller.utils.a parama1)
  {
    super(parama, parame, paramb, paramal, parama1);
    g = paramj;
    h = paramc;
    i = paramd;
    d = "defaultsms";
    e = 2131234230;
    f = 2131887916;
  }
  
  public final void a(View paramView)
  {
    k.b(paramView, "view");
    super.a(paramView);
    paramView = new com/truecaller/startup_dialogs/fragments/f;
    paramView.<init>();
    Object localObject = "callLogPromo";
    k.b(localObject, "<set-?>");
    a = ((String)localObject);
    localObject = g;
    String str = f.class.getSimpleName();
    paramView.show((j)localObject, str);
  }
  
  public final boolean b()
  {
    boolean bool = super.b();
    if (bool)
    {
      Object localObject = i;
      bool = ((d)localObject).a();
      if (!bool)
      {
        localObject = h;
        bool = ((c)localObject).d();
        if (!bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final String e()
  {
    return d;
  }
  
  public final int f()
  {
    return e;
  }
  
  public final int g()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.f.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */