package com.truecaller.f.a;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.f.c;
import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.util.al;

public final class m
  extends a
{
  private final String d;
  private final int e;
  private final int f;
  private final String g;
  private final TcPaySDKListener h;
  
  public m(TcPaySDKListener paramTcPaySDKListener, com.truecaller.f.a parama, e parame, b paramb, al paramal, com.truecaller.utils.a parama1)
  {
    super(parama, parame, paramb, paramal, parama1);
    h = paramTcPaySDKListener;
    d = "tcpay";
    e = 2131234651;
    f = 2131887260;
    paramTcPaySDKListener = d;
    k.b(paramTcPaySDKListener, "key");
    parama = new java/lang/StringBuilder;
    parama.<init>("Promo");
    paramTcPaySDKListener = c.c(paramTcPaySDKListener);
    parama.append(paramTcPaySDKListener);
    parama.append("ClickedCount");
    paramTcPaySDKListener = parama.toString();
    g = paramTcPaySDKListener;
  }
  
  public final void a(View paramView)
  {
    k.b(paramView, "view");
    super.a(paramView);
    Object localObject1 = a;
    Object localObject2 = g;
    com.truecaller.f.a locala = a;
    String str = g;
    int i = locala.c(str) + 1;
    ((com.truecaller.f.a)localObject1).a((String)localObject2, i);
    localObject1 = Truepay.getInstance();
    localObject2 = "Truepay.getInstance()";
    k.a(localObject1, (String)localObject2);
    boolean bool = ((Truepay)localObject1).isRegistrationComplete();
    if (bool) {
      localObject1 = "truecaller://home/tabs/banking";
    } else {
      localObject1 = "truecaller://send";
    }
    paramView = paramView.getContext();
    localObject2 = new android/content/Intent;
    localObject1 = Uri.parse((String)localObject1);
    ((Intent)localObject2).<init>("android.intent.action.VIEW", (Uri)localObject1);
    paramView.startActivity((Intent)localObject2);
  }
  
  public final boolean b()
  {
    boolean bool1 = super.b();
    if (bool1)
    {
      Object localObject = h;
      bool1 = ((TcPaySDKListener)localObject).isTcPayEnabled();
      if (bool1)
      {
        localObject = a;
        String str = g;
        int i = ((com.truecaller.f.a)localObject).c(str);
        boolean bool2 = true;
        if (i > 0)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localObject = null;
        }
        if (i == 0) {
          return bool2;
        }
      }
    }
    return false;
  }
  
  public final String e()
  {
    return d;
  }
  
  public final int f()
  {
    return e;
  }
  
  public final int g()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.f.a.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */