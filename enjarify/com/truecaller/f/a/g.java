package com.truecaller.f.a;

import android.view.View;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.common.f.c;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.featuretoggles.f;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.premium.data.SubscriptionPromoEventMetaData;
import com.truecaller.util.af;
import com.truecaller.util.al;
import java.util.UUID;

public final class g
  extends a
{
  private final String d;
  private final int e;
  private final int f;
  private final c g;
  private final af h;
  private final br i;
  
  public g(com.truecaller.f.a parama, e parame, c paramc, b paramb, al paramal, com.truecaller.utils.a parama1, af paramaf, br parambr)
  {
    super(parama, parame, paramb, paramal, parama1);
    g = paramc;
    h = paramaf;
    i = parambr;
    d = "buypro";
    e = 2131234407;
    f = 2131886817;
  }
  
  public final void a(View paramView)
  {
    k.b(paramView, "view");
    super.a(paramView);
    paramView = paramView.getContext();
    k.a(paramView, "view.context");
    PremiumPresenterView.LaunchContext localLaunchContext = PremiumPresenterView.LaunchContext.CALL_LOG_PROMO;
    SubscriptionPromoEventMetaData localSubscriptionPromoEventMetaData = new com/truecaller/premium/data/SubscriptionPromoEventMetaData;
    String str = UUID.randomUUID().toString();
    localSubscriptionPromoEventMetaData.<init>(str, null);
    br.a(paramView, null, localLaunchContext, localSubscriptionPromoEventMetaData);
  }
  
  public final boolean b()
  {
    boolean bool = super.b();
    if (bool)
    {
      Object localObject1 = g;
      bool = ((c)localObject1).d();
      if (!bool)
      {
        localObject1 = b;
        Object localObject2 = H;
        Object localObject3 = e.a;
        int k = 94;
        localObject3 = localObject3[k];
        localObject1 = (f)((e.a)localObject2).a((e)localObject1, (c.l.g)localObject3);
        int j = ((f)localObject1).a(0);
        localObject2 = h;
        localObject3 = c;
        long l = ((com.truecaller.utils.a)localObject3).a();
        int m = ((af)localObject2).f(l);
        if (j == m) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final String e()
  {
    return d;
  }
  
  public final int f()
  {
    return e;
  }
  
  public final int g()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.f.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */