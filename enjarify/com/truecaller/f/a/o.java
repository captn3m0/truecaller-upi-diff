package com.truecaller.f.a;

import android.content.Context;
import android.view.View;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.util.af;
import com.truecaller.util.al;
import com.truecaller.whoviewedme.WhoViewedMeActivity;
import com.truecaller.whoviewedme.WhoViewedMeActivity.a;
import com.truecaller.whoviewedme.WhoViewedMeLaunchContext;
import com.truecaller.whoviewedme.w;

public final class o
  extends a
{
  private final String d;
  private final int e;
  private final int f;
  private final int g;
  private final com.truecaller.common.f.c h;
  private final w i;
  private final af j;
  
  public o(com.truecaller.f.a parama, e parame, b paramb, al paramal, com.truecaller.utils.a parama1, com.truecaller.common.f.c paramc, w paramw, af paramaf)
  {
    super(parama, parame, paramb, paramal, parama1);
    h = paramc;
    i = paramw;
    j = paramaf;
    d = "whoviewedme";
    e = 2131234684;
    f = 2131887335;
    g = 24;
  }
  
  public final int a()
  {
    return g;
  }
  
  public final void a(View paramView)
  {
    k.b(paramView, "view");
    super.a(paramView);
    Context localContext = paramView.getContext();
    Object localObject = WhoViewedMeActivity.a;
    paramView = paramView.getContext();
    k.a(paramView, "view.context");
    localObject = WhoViewedMeLaunchContext.CALL_LOG_PROMO;
    paramView = WhoViewedMeActivity.a.a(paramView, (WhoViewedMeLaunchContext)localObject);
    localContext.startActivity(paramView);
  }
  
  public final boolean b()
  {
    boolean bool1 = super.b();
    if (bool1)
    {
      Object localObject1 = j;
      Object localObject2 = a;
      Object localObject3 = com.truecaller.f.c.b(d);
      long l1 = ((com.truecaller.f.a)localObject2).b((String)localObject3);
      int k = ((af)localObject1).e(l1);
      localObject2 = j;
      localObject3 = c;
      long l2 = ((com.truecaller.utils.a)localObject3).a();
      int n = ((af)localObject2).e(l2);
      if (k != n)
      {
        localObject1 = h;
        boolean bool2 = ((com.truecaller.common.f.c)localObject1).d();
        if (!bool2)
        {
          localObject1 = i;
          bool2 = ((w)localObject1).a();
          if (bool2)
          {
            localObject1 = i;
            int m = ((w)localObject1).d();
            if (m > 0) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  public final String e()
  {
    return d;
  }
  
  public final int f()
  {
    return e;
  }
  
  public final int g()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.f.a.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */