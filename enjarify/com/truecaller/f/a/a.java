package com.truecaller.f.a;

import android.view.View;
import c.g.b.k;
import c.l.g;
import com.truecaller.f.c;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.f;
import com.truecaller.util.al;
import java.util.concurrent.TimeUnit;

public abstract class a
  implements i
{
  final com.truecaller.f.a a;
  final e b;
  final com.truecaller.utils.a c;
  private final int d;
  private boolean e;
  private final com.truecaller.analytics.b f;
  private final al g;
  
  protected a(com.truecaller.f.a parama, e parame, com.truecaller.analytics.b paramb, al paramal, com.truecaller.utils.a parama1)
  {
    a = parama;
    b = parame;
    f = paramb;
    g = paramal;
    c = parama1;
    d = 6;
  }
  
  private final void a(String paramString)
  {
    com.truecaller.analytics.b localb = f;
    com.truecaller.analytics.e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("PromoView");
    paramString = locala.a("Context", "CallLog").a("Action", paramString);
    String str = e();
    paramString = paramString.a("Type", str).a();
    k.a(paramString, "AnalyticsEvent.Builder(P…tag)\n            .build()");
    localb.a(paramString);
  }
  
  public int a()
  {
    return d;
  }
  
  public void a(View paramView)
  {
    k.b(paramView, "view");
    a("Clicked");
  }
  
  public boolean b()
  {
    org.a.a.b localb1 = new org/a/a/b;
    long l1 = a.b("KeyCallLogPromoDisabledUntil");
    localb1.<init>(l1);
    l1 = c.a();
    boolean bool1 = localb1.d(l1);
    org.a.a.b localb2 = new org/a/a/b;
    long l2 = a.b("LastCallLogPromoDismissedOn");
    localb2.<init>(l2);
    Object localObject1 = TimeUnit.DAYS;
    long l3 = b.ah().g();
    l2 = ((TimeUnit)localObject1).toMillis(l3);
    localb2 = localb2.b(l2);
    l2 = c.a();
    boolean bool2 = localb2.d(l2);
    localObject1 = new org/a/a/b;
    com.truecaller.f.a locala = a;
    String str1 = "LastCallLogPromoShownOn";
    l3 = locala.b(str1);
    ((org.a.a.b)localObject1).<init>(l3);
    int i = a();
    localObject1 = ((org.a.a.b)localObject1).b(i);
    l3 = c.a();
    boolean bool3 = ((org.a.a.b)localObject1).c(l3);
    i = 0;
    locala = null;
    boolean bool4 = true;
    if (!bool3)
    {
      localObject1 = new org/a/a/b;
      long l4 = a.b("LastCallLogPromoShownOn");
      ((org.a.a.b)localObject1).<init>(l4);
      localObject2 = TimeUnit.DAYS;
      localObject3 = b.ah();
      long l5 = ((f)localObject3).g();
      l4 = ((TimeUnit)localObject2).toMillis(l5);
      localObject1 = ((org.a.a.b)localObject1).b(l4);
      localObject2 = c;
      l4 = ((com.truecaller.utils.a)localObject2).a();
      bool3 = ((org.a.a.b)localObject1).d(l4);
      if (!bool3)
      {
        bool3 = false;
        localObject1 = null;
        break label312;
      }
    }
    bool3 = true;
    label312:
    Object localObject2 = a;
    Object localObject3 = c.a(e());
    int j = ((com.truecaller.f.a)localObject2).c((String)localObject3);
    localObject3 = b;
    Object localObject4 = K;
    Object localObject5 = e.a;
    char c1 = 'c';
    localObject5 = localObject5[c1];
    localObject3 = (f)((com.truecaller.featuretoggles.e.a)localObject4).a((e)localObject3, (g)localObject5);
    int k = 2;
    int m = ((f)localObject3).a(k);
    if (j < m)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject2 = null;
    }
    localObject3 = g;
    boolean bool5 = ((al)localObject3).a();
    localObject4 = new String[bool4];
    localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>("Tag:");
    String str2 = e();
    ((StringBuilder)localObject5).append(str2);
    ((StringBuilder)localObject5).append(" suppressForNewUserPassed:");
    ((StringBuilder)localObject5).append(bool1);
    ((StringBuilder)localObject5).append(" coolOffAfterDismissPassed:");
    ((StringBuilder)localObject5).append(bool2);
    ((StringBuilder)localObject5).append(" coolOffAfterShownPassed:");
    ((StringBuilder)localObject5).append(bool3);
    c1 = ' ';
    ((StringBuilder)localObject5).append(c1);
    ((StringBuilder)localObject5).append("dismissCountPermissible:");
    ((StringBuilder)localObject5).append(j);
    str2 = " hasValidAccountState:";
    ((StringBuilder)localObject5).append(str2);
    ((StringBuilder)localObject5).append(bool5);
    localObject5 = ((StringBuilder)localObject5).toString();
    localObject4[0] = localObject5;
    if ((bool1) && (bool2) && (j != 0) && (bool3) && (bool5)) {
      return bool4;
    }
    return false;
  }
  
  public final void c()
  {
    Object localObject1 = a;
    long l = c.a();
    ((com.truecaller.f.a)localObject1).a("LastCallLogPromoDismissedOn", l);
    localObject1 = c.a(e());
    Object localObject2 = a;
    int i = ((com.truecaller.f.a)localObject2).c((String)localObject1) + 1;
    ((com.truecaller.f.a)localObject2).a((String)localObject1, i);
    localObject1 = a;
    localObject2 = c.b(e());
    l = c.a();
    ((com.truecaller.f.a)localObject1).a((String)localObject2, l);
    a("Dismissed");
  }
  
  public final void d()
  {
    boolean bool = e;
    if (!bool)
    {
      Object localObject1 = new org/a/a/b;
      Object localObject2 = a;
      String str = "LastCallLogPromoShownOn";
      long l1 = ((com.truecaller.f.a)localObject2).b(str);
      ((org.a.a.b)localObject1).<init>(l1);
      int i = a();
      localObject1 = ((org.a.a.b)localObject1).b(i);
      localObject2 = c;
      l1 = ((com.truecaller.utils.a)localObject2).a();
      bool = ((org.a.a.b)localObject1).c(l1);
      if (!bool)
      {
        localObject1 = a;
        localObject2 = "LastCallLogPromoShownOn";
        long l2 = System.currentTimeMillis();
        ((com.truecaller.f.a)localObject1).a((String)localObject2, l2);
      }
      localObject1 = "Shown";
      a((String)localObject1);
      bool = true;
      e = bool;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.f.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */