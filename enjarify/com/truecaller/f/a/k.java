package com.truecaller.f.a;

import android.view.View;
import com.truecaller.analytics.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.ReferralManager.ReferralLaunchContext;
import com.truecaller.util.al;
import com.truecaller.utils.extensions.c;

public final class k
  extends a
{
  private final String d;
  private final int e;
  private final int f;
  private final ReferralManager g;
  
  public k(ReferralManager paramReferralManager, com.truecaller.f.a parama, e parame, b paramb, al paramal, com.truecaller.utils.a parama1)
  {
    super(parama, parame, paramb, paramal, parama1);
    g = paramReferralManager;
    d = "referral";
    e = 2131234442;
    f = 2131888633;
  }
  
  public final void a(View paramView)
  {
    Object localObject = "view";
    c.g.b.k.b(paramView, (String)localObject);
    super.a(paramView);
    paramView = g;
    if (paramView != null)
    {
      localObject = ReferralManager.ReferralLaunchContext.HOME_SCREEN;
      paramView.a((ReferralManager.ReferralLaunchContext)localObject);
      return;
    }
  }
  
  public final boolean b()
  {
    boolean bool = super.b();
    if (bool)
    {
      Object localObject = g;
      if (localObject != null)
      {
        ReferralManager.ReferralLaunchContext localReferralLaunchContext = ReferralManager.ReferralLaunchContext.HOME_SCREEN;
        bool = ((ReferralManager)localObject).c(localReferralLaunchContext);
        localObject = Boolean.valueOf(bool);
      }
      else
      {
        bool = false;
        localObject = null;
      }
      bool = c.a((Boolean)localObject);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final String e()
  {
    return d;
  }
  
  public final int f()
  {
    return e;
  }
  
  public final int g()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.f.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */