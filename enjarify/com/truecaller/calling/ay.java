package com.truecaller.calling;

import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.g;

public final class ay
  implements ax
{
  private final f b;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(ay.class);
    ((u)localObject).<init>(localb, "spamDetailsView", "getSpamDetailsView()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public ay(View paramView)
  {
    Object localObject = com.truecaller.utils.extensions.t.a(paramView, 2131364280);
    b = ((f)localObject);
    localObject = a();
    paramView = com.truecaller.utils.ui.b.b(paramView.getContext(), 2130969585);
    ((TextView)localObject).setTextColor(paramView);
  }
  
  private final TextView a()
  {
    return (TextView)b.b();
  }
  
  public final void b(String paramString)
  {
    TextView localTextView = a();
    if (paramString != null)
    {
      com.truecaller.utils.extensions.t.a((View)localTextView);
      paramString = (CharSequence)paramString;
      localTextView.setText(paramString);
      return;
    }
    k.a(localTextView, "this");
    com.truecaller.utils.extensions.t.b((View)localTextView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ay
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */