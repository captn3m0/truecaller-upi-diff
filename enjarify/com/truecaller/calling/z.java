package com.truecaller.calling;

import android.view.View;
import android.view.View.OnClickListener;
import c.a.y;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.l.b;
import c.l.g;
import com.truecaller.flashsdk.ui.CompoundFlashButton;
import java.util.List;

public final class z
  implements w
{
  private final f b;
  private final f c;
  
  static
  {
    g[] arrayOfg = new g[2];
    Object localObject = new c/g/b/u;
    b localb = c.g.b.w.a(z.class);
    ((u)localObject).<init>(localb, "flashButton", "getFlashButton()Lcom/truecaller/flashsdk/ui/CompoundFlashButton;");
    localObject = (g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = c.g.b.w.a(z.class);
    ((u)localObject).<init>(localb, "actionOneClickArea", "getActionOneClickArea()Landroid/view/View;");
    localObject = (g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public z(View paramView)
  {
    Object localObject = com.truecaller.utils.extensions.t.a(paramView, 2131363109);
    b = ((f)localObject);
    paramView = com.truecaller.utils.extensions.t.a(paramView, 2131361842);
    c = paramView;
    paramView = b();
    localObject = new com/truecaller/calling/z$1;
    ((z.1)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  private final CompoundFlashButton a()
  {
    return (CompoundFlashButton)b.b();
  }
  
  private final View b()
  {
    return (View)c.b();
  }
  
  public final void a(x paramx)
  {
    Object localObject1 = a();
    if (paramx != null)
    {
      localObject2 = a;
      if (localObject2 != null) {}
    }
    else
    {
      localObject2 = (List)y.a;
    }
    String str1;
    if (paramx != null)
    {
      str1 = ae.a(paramx);
      if (str1 != null) {}
    }
    else
    {
      str1 = "";
    }
    String str2;
    if (paramx != null)
    {
      str2 = c;
      if (str2 != null) {}
    }
    else
    {
      str2 = "";
    }
    ((CompoundFlashButton)localObject1).a((List)localObject2, str1, str2);
    localObject1 = b();
    Object localObject2 = "actionOneClickArea";
    k.a(localObject1, (String)localObject2);
    boolean bool;
    if (paramx != null)
    {
      bool = true;
    }
    else
    {
      bool = false;
      paramx = null;
    }
    com.truecaller.utils.extensions.t.a((View)localObject1, bool);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */