package com.truecaller.calling.blocking;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.telecom.Call.Details;
import android.telecom.CallScreeningService;
import android.telecom.CallScreeningService.CallResponse;
import android.telecom.CallScreeningService.CallResponse.Builder;
import c.l.g;
import c.n.m;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.callerid.CallerIdService;
import com.truecaller.calling.aj;
import com.truecaller.calling.aj.b;
import com.truecaller.calling.aj.e;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.h.c;

public final class DefaultPhoneAppBlockService
  extends CallScreeningService
{
  private final void a(Call.Details paramDetails)
  {
    Object localObject = new android/telecom/CallScreeningService$CallResponse$Builder;
    ((CallScreeningService.CallResponse.Builder)localObject).<init>();
    localObject = ((CallScreeningService.CallResponse.Builder)localObject).setDisallowCall(false).setSkipNotification(false).build();
    respondToCall(paramDetails, (CallScreeningService.CallResponse)localObject);
  }
  
  public final void onScreenCall(Call.Details paramDetails)
  {
    c.g.b.k.b(paramDetails, "details");
    Object localObject1 = paramDetails.getIntentExtras();
    boolean bool1 = false;
    Object localObject2 = null;
    int i;
    if (localObject1 != null)
    {
      String str = "android.telecom.extra.INCOMING_CALL_ADDRESS";
      localObject1 = (Uri)((Bundle)localObject1).getParcelable(str);
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    if (localObject1 != null) {
      localObject2 = ((Uri)localObject1).getSchemeSpecificPart();
    }
    localObject1 = Uri.decode((String)localObject2);
    localObject2 = localObject1;
    localObject2 = (CharSequence)localObject1;
    int j = 1;
    if (localObject2 != null)
    {
      bool1 = m.a((CharSequence)localObject2);
      if (!bool1)
      {
        bool1 = false;
        localObject2 = null;
        break label98;
      }
    }
    bool1 = true;
    label98:
    if (bool1) {
      return;
    }
    localObject2 = getApplicationContext();
    if (localObject2 != null)
    {
      localObject2 = ((TrueApp)localObject2).a();
      c.g.b.k.a(localObject2, "(applicationContext as TrueApp).objectsGraph");
      Object localObject3 = ((bp)localObject2).aF();
      c.g.b.k.a(localObject3, "trueGraph.featuresRegistry()");
      Object localObject4 = f;
      Object localObject5 = e.a;
      int m = 17;
      localObject5 = localObject5[m];
      localObject3 = ((e.a)localObject4).a((e)localObject3, (g)localObject5);
      boolean bool3 = ((b)localObject3).a();
      if (bool3)
      {
        localObject3 = ((bp)localObject2).ac();
        c.g.b.k.a(localObject3, "trueGraph.callProcessor()");
        localObject4 = new com/truecaller/calling/aj$e;
        long l1 = System.currentTimeMillis();
        ((aj.e)localObject4).<init>((String)localObject1, l1);
        localObject5 = this;
        localObject5 = (Context)this;
        localObject4 = (aj)localObject4;
        localObject4 = ((com.truecaller.calling.k)localObject3).a((Context)localObject5, (aj)localObject4);
        if (localObject4 == null)
        {
          a(paramDetails);
          return;
        }
        boolean bool2 = localObject4 instanceof aj.e;
        if (bool2)
        {
          Bundle localBundle = ((aj)localObject4).a();
          CallerIdService.a((Context)localObject5, localBundle);
          localObject4 = e;
          if (localObject4 != null)
          {
            int n = ((Integer)localObject4).intValue();
            if (n == j)
            {
              localObject2 = new android/telecom/CallScreeningService$CallResponse$Builder;
              ((CallScreeningService.CallResponse.Builder)localObject2).<init>();
              localObject2 = ((CallScreeningService.CallResponse.Builder)localObject2).setDisallowCall(j).setSkipNotification(j).build();
              respondToCall(paramDetails, (CallScreeningService.CallResponse)localObject2);
              paramDetails = new com/truecaller/calling/aj$b;
              long l2 = System.currentTimeMillis();
              paramDetails.<init>((String)localObject1, l2);
              paramDetails = (aj)paramDetails;
              paramDetails = ((com.truecaller.calling.k)localObject3).a((Context)localObject5, paramDetails);
              if (paramDetails != null)
              {
                paramDetails = paramDetails.a();
                CallerIdService.a((Context)localObject5, paramDetails);
              }
              return;
            }
          }
          if (localObject4 != null)
          {
            i = ((Integer)localObject4).intValue();
            int k = 3;
            if (i == k)
            {
              localObject1 = ((c)((bp)localObject2).aN().a()).a();
              ((w)localObject1).c();
              break label471;
            }
          }
          a(paramDetails);
          return;
        }
        label471:
        a(paramDetails);
      }
      return;
    }
    paramDetails = new c/u;
    paramDetails.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramDetails;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.blocking.DefaultPhoneAppBlockService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */