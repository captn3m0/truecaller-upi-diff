package com.truecaller.calling;

import android.content.Context;
import android.telephony.TelephonyManager;
import c.g.b.w;
import com.truecaller.aftercall.AfterCallPromotionActivity;
import com.truecaller.aftercall.PromotionType;
import com.truecaller.androidactors.f;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.common.account.r;
import com.truecaller.common.h.am;
import com.truecaller.common.h.an;
import com.truecaller.featuretoggles.e;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.FilterAction;
import com.truecaller.filters.g;
import com.truecaller.i.c;
import com.truecaller.multisim.h;
import com.truecaller.util.cn;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

public final class l
  implements k
{
  private final LinkedList a;
  private final Stack b;
  private final boolean c;
  private final com.truecaller.utils.d d;
  private final c e;
  private final an f;
  private final FilterManager g;
  private final h h;
  private final r i;
  private final com.truecaller.common.h.u j;
  private final cn k;
  private final com.truecaller.utils.a l;
  private final com.truecaller.utils.l m;
  private final e n;
  private final f o;
  private final com.truecaller.voip.d p;
  
  public l(com.truecaller.utils.d paramd, c paramc, an paraman, FilterManager paramFilterManager, h paramh, r paramr, com.truecaller.common.h.u paramu, cn paramcn, com.truecaller.utils.a parama, com.truecaller.utils.l paraml, CallRecordingManager paramCallRecordingManager, e parame, f paramf, com.truecaller.voip.d paramd1)
  {
    d = paramd;
    e = paramc;
    f = paraman;
    g = paramFilterManager;
    h = paramh;
    i = paramr;
    j = paramu;
    k = paramcn;
    l = parama;
    m = paraml;
    n = parame;
    o = paramf;
    p = paramd1;
    paramd = new java/util/LinkedList;
    paramd.<init>();
    a = paramd;
    paramd = new java/util/Stack;
    paramd.<init>();
    b = paramd;
    boolean bool = paramCallRecordingManager.a();
    c = bool;
  }
  
  private final boolean a(String paramString)
  {
    if (paramString == null) {
      return false;
    }
    Object localObject1 = b;
    boolean bool1 = ((Stack)localObject1).contains(paramString);
    if (bool1) {
      return true;
    }
    localObject1 = (Iterable)b;
    Object localObject2 = new java/util/ArrayList;
    int i1 = c.a.m.a((Iterable)localObject1, 10);
    ((ArrayList)localObject2).<init>(i1);
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      String str1 = (String)((Iterator)localObject1).next();
      com.truecaller.common.h.u localu = j;
      String str2 = "it";
      c.g.b.k.a(str1, str2);
      str1 = localu.b(str1);
      ((Collection)localObject2).add(str1);
    }
    localObject2 = (List)localObject2;
    paramString = j.b(paramString);
    return ((List)localObject2).contains(paramString);
  }
  
  private static boolean b(String paramString)
  {
    int i2;
    if (paramString != null)
    {
      paramString = (CharSequence)paramString;
      char c1 = '#';
      int i1 = 6;
      i2 = c.n.m.a(paramString, c1, 0, i1);
    }
    else
    {
      i2 = -1;
    }
    return i2 >= 0;
  }
  
  private final void c(String paramString)
  {
    Iterator localIterator = a.iterator();
    Object localObject = "lastStates.iterator()";
    c.g.b.k.a(localIterator, (String)localObject);
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject = localIterator.next();
      String str = "it.next()";
      c.g.b.k.a(localObject, str);
      localObject = a;
      bool = c.g.b.k.a(paramString, localObject);
      if (bool) {
        localIterator.remove();
      }
    }
  }
  
  public final aj a(Context paramContext, aj paramaj)
  {
    c.g.b.k.b(paramContext, "context");
    Object localObject1 = "newState";
    c.g.b.k.b(paramaj, (String)localObject1);
    int i1 = 1;
    Object localObject2 = new String[i1];
    String str = String.valueOf(paramaj);
    Object localObject3 = "Receiver received call state change to ".concat(str);
    int i2 = 0;
    str = null;
    localObject2[0] = localObject3;
    localObject2 = d;
    int i3 = ((com.truecaller.utils.d)localObject2).h();
    int i7 = 26;
    if (i3 < i7)
    {
      i3 = 1;
    }
    else
    {
      i3 = 0;
      localObject2 = null;
    }
    localObject3 = d;
    i7 = ((com.truecaller.utils.d)localObject3).h();
    int i9 = 28;
    if (i7 >= i9)
    {
      i7 = 1;
    }
    else
    {
      i7 = 0;
      localObject3 = null;
    }
    Object localObject4 = d;
    boolean bool7 = ((com.truecaller.utils.d)localObject4).c();
    Integer localInteger = null;
    Object localObject5;
    Object localObject6;
    if (i7 != 0)
    {
      localObject5 = (aj)c.a.m.g((List)a);
      if (localObject5 != null)
      {
        localObject6 = w.a(localObject5.getClass());
        c.l.b localb = w.a(paramaj.getClass());
        bool8 = c.g.b.k.a(localObject6, localb) ^ i1;
        if (!bool8)
        {
          bool8 = false;
          localObject6 = null;
          break label221;
        }
      }
      boolean bool8 = true;
      label221:
      if (bool8)
      {
        localObject6 = a;
        if (localObject6 == null)
        {
          a.add(paramaj);
          localObject2 = new String[i1];
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>("Dropping state ");
          ((StringBuilder)localObject3).append(paramaj);
          localObject4 = " as it is a new state change with null number";
          ((StringBuilder)localObject3).append((String)localObject4);
          localObject3 = ((StringBuilder)localObject3).toString();
          localObject2[0] = localObject3;
          i3 = 1;
          break label624;
        }
      }
      if (localObject5 != null) {
        localObject5 = a;
      } else {
        localObject5 = null;
      }
      if (localObject5 != null)
      {
        localObject5 = a;
        if (localObject5 == null)
        {
          localObject2 = new String[i1];
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>("Dropping state ");
          ((StringBuilder)localObject3).append(paramaj);
          localObject4 = " as it's not a new state change, previous state had a number and this one doesn't";
          ((StringBuilder)localObject3).append((String)localObject4);
          localObject3 = ((StringBuilder)localObject3).toString();
          localObject2[0] = localObject3;
          i3 = 1;
          break label624;
        }
      }
    }
    if ((i3 == 0) && (!bool7))
    {
      i3 = 0;
      localObject2 = null;
    }
    else
    {
      i3 = 1;
    }
    if (i7 != 0)
    {
      localObject3 = a;
      if (localObject3 == null)
      {
        i7 = 1;
        break label447;
      }
    }
    i7 = 0;
    localObject3 = null;
    label447:
    if (i3 != 0)
    {
      localObject2 = a;
      bool2 = ((LinkedList)localObject2).contains(paramaj);
      if ((bool2) && (i7 == 0))
      {
        localObject2 = new String[i1];
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("Dropping state ");
        ((StringBuilder)localObject3).append(paramaj);
        localObject4 = " because it's a duplicate";
        ((StringBuilder)localObject3).append((String)localObject4);
        localObject3 = ((StringBuilder)localObject3).toString();
        localObject2[0] = localObject3;
        localObject2 = a;
        i7 = ((LinkedList)localObject2).indexOf(paramaj);
        localObject2 = ((LinkedList)localObject2).get(i7);
        bool6 = localObject2 instanceof aj.e;
        if (!bool6)
        {
          bool2 = false;
          localObject2 = null;
        }
        localObject2 = (aj.e)localObject2;
        bool6 = paramaj instanceof aj.e;
        if (bool6)
        {
          if (localObject2 != null)
          {
            localObject2 = e;
          }
          else
          {
            bool2 = false;
            localObject2 = null;
          }
          if (localObject2 != null) {}
        }
        else
        {
          bool2 = true;
          break label624;
        }
      }
    }
    boolean bool2 = false;
    localObject2 = null;
    label624:
    if (bool2) {
      return null;
    }
    localObject2 = p;
    localObject3 = a;
    bool2 = ((com.truecaller.voip.d)localObject2).a((String)localObject3);
    if (bool2) {
      return null;
    }
    localObject2 = a;
    localObject3 = localObject2;
    localObject3 = (CharSequence)localObject2;
    if (localObject3 != null)
    {
      bool6 = c.n.m.a((CharSequence)localObject3);
      if (!bool6)
      {
        bool6 = false;
        localObject3 = null;
        break label716;
      }
    }
    boolean bool6 = true;
    label716:
    if (bool6)
    {
      localObject2 = b;
      bool2 = ((Stack)localObject2).isEmpty();
      if (!bool2)
      {
        bool2 = paramaj instanceof aj.c;
        if (bool2)
        {
          localObject2 = (String)b.peek();
          a = ((String)localObject2);
        }
        else
        {
          bool2 = paramaj instanceof aj.b;
          if (bool2)
          {
            localObject2 = (String)b.pop();
            a = ((String)localObject2);
          }
        }
      }
    }
    else
    {
      bool2 = b((String)localObject2);
      if (bool2)
      {
        bool2 = c;
        if (!bool2) {
          return null;
        }
      }
      bool2 = paramaj instanceof aj.e;
      if (bool2)
      {
        com.truecaller.util.a.a(paramContext);
        localObject2 = b;
        localObject3 = a;
        ((Stack)localObject2).push(localObject3);
      }
      else
      {
        bool2 = paramaj instanceof aj.b;
        if (bool2)
        {
          localObject2 = b;
          bool2 = ((Stack)localObject2).isEmpty();
          if (!bool2)
          {
            localObject2 = b;
            ((Stack)localObject2).pop();
          }
        }
      }
    }
    bool2 = paramaj instanceof aj.e;
    if (bool2)
    {
      localObject3 = a;
      if (localObject3 != null) {
        c((String)localObject3);
      }
    }
    label1261:
    label1348:
    label1358:
    label1371:
    label1384:
    label1397:
    boolean bool4;
    if (bool2)
    {
      localObject2 = e;
      localObject3 = "truecaller.call_in_progress";
      bool2 = ((c)localObject2).b((String)localObject3);
      if (!bool2)
      {
        localObject2 = a;
        localObject3 = i;
        bool6 = ((r)localObject3).c();
        if (!bool6)
        {
          localObject3 = new String[i1];
          localObject4 = "Account not created, not inspecting block options for ";
          localObject2 = String.valueOf(localObject2);
          localObject2 = ((String)localObject4).concat((String)localObject2);
          localObject3[0] = localObject2;
        }
        else
        {
          localObject3 = paramContext.getSystemService("phone");
          if (localObject3 == null) {
            break label1384;
          }
          localObject3 = (TelephonyManager)localObject3;
          localObject4 = ((TelephonyManager)localObject3).getNetworkCountryIso();
          c.g.b.k.a(localObject4, "telephonyManager.networkCountryIso");
          localObject5 = Locale.ENGLISH;
          localObject6 = "Locale.ENGLISH";
          c.g.b.k.a(localObject5, (String)localObject6);
          if (localObject4 == null) {
            break label1371;
          }
          localObject4 = ((String)localObject4).toUpperCase((Locale)localObject5);
          c.g.b.k.a(localObject4, "(this as java.lang.String).toUpperCase(locale)");
          localObject3 = ((TelephonyManager)localObject3).getSimCountryIso();
          c.g.b.k.a(localObject3, "telephonyManager.simCountryIso");
          localObject5 = Locale.ENGLISH;
          localObject6 = "Locale.ENGLISH";
          c.g.b.k.a(localObject5, (String)localObject6);
          if (localObject3 == null) {
            break label1358;
          }
          localObject3 = ((String)localObject3).toUpperCase((Locale)localObject5);
          c.g.b.k.a(localObject3, "(this as java.lang.String).toUpperCase(locale)");
          localObject5 = g;
          localObject4 = (CharSequence)localObject4;
          localObject3 = (CharSequence)localObject3;
          localObject3 = (String)am.e((CharSequence)localObject4, (CharSequence)localObject3);
          localObject2 = ((FilterManager)localObject5).a((String)localObject2, null, (String)localObject3, i1);
          c.g.b.k.a(localObject2, "filterManager.findFilter…o), false, true\n        )");
          localObject2 = h;
          localObject3 = FilterManager.FilterAction.FILTER_BLACKLISTED;
          if (localObject2 == localObject3)
          {
            bool2 = true;
            break label1261;
          }
        }
        bool2 = false;
        localObject2 = null;
        if (bool2)
        {
          localObject2 = e;
          localObject3 = "blockCallMethod";
          int i4 = ((c)localObject2).a((String)localObject3, 0) & 0x8;
          if (i4 == 0) {
            i2 = 1;
          }
          if (i2 != 0)
          {
            localObject2 = m;
            localObject3 = new String[] { "android.permission.CALL_PHONE" };
            boolean bool3 = ((com.truecaller.utils.l)localObject2).a((String[])localObject3);
            if (bool3)
            {
              bool3 = true;
              break label1348;
            }
          }
          int i5 = 3;
          localInteger = Integer.valueOf(i5);
          break label1397;
          paramContext = new c/u;
          paramContext.<init>("null cannot be cast to non-null type java.lang.String");
          throw paramContext;
          paramContext = new c/u;
          paramContext.<init>("null cannot be cast to non-null type java.lang.String");
          throw paramContext;
          paramContext = new c/u;
          paramContext.<init>("null cannot be cast to non-null type android.telephony.TelephonyManager");
          throw paramContext;
        }
      }
      localObject2 = h;
      bool4 = ((h)localObject2).j();
      int i10;
      if (bool4)
      {
        paramContext = com.truecaller.callhistory.k.a(paramContext);
        i10 = paramContext.a(i1);
      }
      else
      {
        i10 = -1;
      }
      localObject5 = new com/truecaller/calling/aj$e;
      localObject2 = a;
      long l1 = b;
      localObject4 = Integer.valueOf(i10);
      localObject1 = localObject5;
      ((aj.e)localObject5).<init>((String)localObject2, l1, (Integer)localObject4, localInteger);
      paramaj = (aj)localObject5;
      paramaj = (aj)localObject5;
    }
    else
    {
      bool4 = paramaj instanceof aj.c;
      if (bool4)
      {
        paramContext = e;
        localObject2 = "truecaller.call_in_progress";
        paramContext.b((String)localObject2, i1);
        paramContext = a;
        if (paramContext != null)
        {
          paramContext = a;
          boolean bool9 = a(paramContext);
          if (!bool9)
          {
            paramContext = new com/truecaller/calling/aj$d;
            localObject1 = a;
            long l2 = b;
            paramContext.<init>((String)localObject1, l2);
            paramaj = paramContext;
            paramaj = (aj)paramContext;
          }
        }
      }
      else
      {
        bool4 = paramaj instanceof aj.b;
        if (bool4)
        {
          localObject2 = e;
          localObject3 = "truecaller.call_in_progress";
          ((c)localObject2).b((String)localObject3, false);
          localObject2 = a;
          int i6 = ((LinkedList)localObject2).size();
          boolean bool1;
          if (i6 > 0)
          {
            localObject2 = a;
            int i8 = ((LinkedList)localObject2).size() - i1;
            localObject2 = ((LinkedList)localObject2).get(i8);
            boolean bool5 = localObject2 instanceof aj.e;
            if (bool5) {}
          }
          else
          {
            bool1 = false;
            localObject1 = null;
          }
          if (bool1)
          {
            localObject1 = i;
            bool1 = ((r)localObject1).c();
            if (!bool1)
            {
              localObject1 = k;
              bool1 = ((cn)localObject1).b();
              if (!bool1)
              {
                localObject1 = PromotionType.SIGN_UP;
                AfterCallPromotionActivity.a(paramContext, (PromotionType)localObject1);
              }
            }
          }
        }
        else
        {
          paramaj = null;
        }
      }
    }
    if (paramaj != null)
    {
      paramContext = a;
      paramContext.add(paramaj);
    }
    return (aj)paramaj;
  }
  
  public final aj a(aj.d paramd)
  {
    c.g.b.k.b(paramd, "phoneState");
    paramd = a;
    aj.d locald = null;
    if (paramd != null)
    {
      boolean bool1 = b(paramd);
      if (!bool1)
      {
        Object localObject1 = p;
        bool1 = ((com.truecaller.voip.d)localObject1).a(paramd);
        if (bool1) {
          return null;
        }
        localObject1 = n.e();
        bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
        if (bool1)
        {
          localObject1 = j.b(paramd);
          if (localObject1 != null)
          {
            localObject2 = (com.truecaller.callhistory.a)o.a();
            ((com.truecaller.callhistory.a)localObject2).b((String)localObject1);
          }
        }
        b.add(paramd);
        c(paramd);
        localObject1 = f;
        Object localObject2 = e;
        String str = "featureOutgoingSearch";
        long l1 = ((c)localObject2).a(str, 0L);
        long l2 = 86400000L;
        bool1 = ((an)localObject1).a(l1, l2);
        boolean bool2 = am.b(paramd);
        if ((bool2) && (bool1))
        {
          locald = new com/truecaller/calling/aj$d;
          long l3 = l.a();
          locald.<init>(paramd, l3);
          return (aj)locald;
        }
        return null;
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */