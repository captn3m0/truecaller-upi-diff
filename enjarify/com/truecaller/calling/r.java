package com.truecaller.calling;

import c.g.b.k;
import c.n.m;
import com.truecaller.calling.dialer.ax;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.flashsdk.core.i;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class r
{
  public static final x a(Contact paramContact, i parami, String paramString)
  {
    k.b(paramContact, "receiver$0");
    k.b(parami, "flashPoint");
    Object localObject1 = "analyticsContext";
    k.b(paramString, (String)localObject1);
    boolean bool1 = ae.a(parami);
    if (!bool1) {
      return null;
    }
    localObject1 = paramContact.A();
    k.a(localObject1, "numbers");
    localObject1 = (Iterable)localObject1;
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool2;
    Object localObject3;
    Object localObject4;
    for (;;)
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (Number)((Iterator)localObject1).next();
      localObject4 = "it";
      k.a(localObject3, (String)localObject4);
      localObject3 = ((Number)localObject3).a();
      if (localObject3 != null) {
        ((Collection)localObject2).add(localObject3);
      }
    }
    localObject2 = (Iterable)localObject2;
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = ((Iterator)localObject2).next();
      localObject4 = localObject3;
      localObject4 = (String)localObject3;
      boolean bool3 = parami.b((String)localObject4);
      if (bool3) {
        ((Collection)localObject1).add(localObject3);
      }
    }
    localObject1 = (Iterable)localObject1;
    parami = new java/util/ArrayList;
    parami.<init>();
    parami = (Collection)parami;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool4 = ((Iterator)localObject1).hasNext();
      if (!bool4) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      localObject3 = "+";
      localObject4 = "";
      localObject2 = m.d(m.a((String)localObject2, (String)localObject3, (String)localObject4));
      if (localObject2 != null) {
        parami.add(localObject2);
      }
    }
    parami = (List)parami;
    localObject1 = parami;
    localObject1 = (Collection)parami;
    bool1 = ((Collection)localObject1).isEmpty() ^ true;
    if (!bool1) {
      parami = null;
    }
    if (parami != null)
    {
      localObject1 = new com/truecaller/calling/x;
      paramContact = paramContact.s();
      k.a(paramContact, "displayNameOrNumber");
      ((x)localObject1).<init>(parami, paramContact, paramString);
      return (x)localObject1;
    }
    return null;
  }
  
  public static final String a(Contact paramContact, String paramString, n paramn, ax paramax)
  {
    k.b(paramString, "normalizedNumber");
    k.b(paramn, "resourceProvider");
    k.b(paramax, "numberTypeLabelProvider");
    if (paramContact != null)
    {
      paramContact = paramContact.A();
      if (paramContact != null)
      {
        paramContact = ((Iterable)paramContact).iterator();
        boolean bool2;
        do
        {
          bool1 = paramContact.hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = paramContact.next();
          Object localObject2 = localObject1;
          localObject2 = (Number)localObject1;
          String str = "it";
          k.a(localObject2, str);
          localObject2 = ((Number)localObject2).a();
          bool2 = k.a(localObject2, paramString);
        } while (!bool2);
        break label113;
        boolean bool1 = false;
        Object localObject1 = null;
        label113:
        localObject1 = (Number)localObject1;
        if (localObject1 != null) {
          return ah.a((Number)localObject1, paramn, paramax);
        }
      }
    }
    return null;
  }
  
  public static final boolean a(Contact paramContact)
  {
    if (paramContact != null)
    {
      int i = paramContact.getSource() & 0xD;
      if (i != 0) {
        return false;
      }
    }
    return true;
  }
  
  public static final boolean b(Contact paramContact)
  {
    if (paramContact != null)
    {
      paramContact = paramContact.getId();
      if (paramContact != null) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */