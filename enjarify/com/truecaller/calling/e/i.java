package com.truecaller.calling.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import c.g.b.k;
import c.n.m;
import c.u;
import c.x;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.e;
import com.truecaller.util.w;
import com.truecaller.utils.extensions.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public final class i
{
  public static final void a(Activity paramActivity, Contact paramContact, String paramString1, String paramString2, String paramString3)
  {
    k.b(paramActivity, "activity");
    k.b(paramString1, "fallBackNumber");
    k.b(paramString2, "callType");
    k.b(paramString3, "analyticsContext");
    paramString3 = paramActivity;
    paramString3 = (Context)paramActivity;
    boolean bool1;
    if (paramContact != null)
    {
      paramContact = paramContact.E();
    }
    else
    {
      bool1 = false;
      paramContact = null;
    }
    paramContact = w.a(paramString3, paramContact);
    k.a(paramContact, "ContactUtil.getExternalA…ty, contact?.phonebookId)");
    paramContact = (Iterable)paramContact;
    paramString3 = new java/util/ArrayList;
    paramString3.<init>();
    paramString3 = (Collection)paramString3;
    paramContact = paramContact.iterator();
    boolean bool2;
    Object localObject1;
    Object localObject2;
    for (;;)
    {
      bool2 = paramContact.hasNext();
      if (!bool2) {
        break;
      }
      localObject1 = paramContact.next();
      localObject2 = localObject1;
      localObject2 = d;
      k.a(localObject2, "it.packageName");
      localObject2 = (CharSequence)localObject2;
      CharSequence localCharSequence = (CharSequence)"com.whatsapp";
      boolean bool3 = m.b((CharSequence)localObject2, localCharSequence);
      if (bool3) {
        paramString3.add(localObject1);
      }
    }
    paramString3 = (Iterable)paramString3;
    paramContact = paramString3.iterator();
    do
    {
      bool4 = paramContact.hasNext();
      if (!bool4) {
        break;
      }
      paramString3 = paramContact.next();
      localObject1 = paramString3;
      localObject1 = c;
      localObject2 = "it.actionIntent";
      k.a(localObject1, (String)localObject2);
      localObject1 = ((Intent)localObject1).getType();
      if (localObject1 != null)
      {
        localObject1 = (CharSequence)localObject1;
        localObject2 = paramString2;
        localObject2 = (CharSequence)paramString2;
        boolean bool5 = true;
        bool2 = m.a((CharSequence)localObject1, (CharSequence)localObject2, bool5);
        localObject1 = Boolean.valueOf(bool2);
      }
      else
      {
        bool2 = false;
        localObject1 = null;
      }
      bool2 = c.a((Boolean)localObject1);
    } while (!bool2);
    break label319;
    boolean bool4 = false;
    paramString3 = null;
    label319:
    paramString3 = (e)paramString3;
    if (paramString3 != null)
    {
      paramContact = c;
      paramActivity.startActivity(paramContact);
      paramContact = x.a;
      paramContact = "call";
      bool1 = k.a(paramString2, paramContact);
      if (bool1)
      {
        a(paramActivity, "Audio");
        return;
      }
      a(paramActivity, "Video");
      return;
    }
    paramString1 = String.valueOf(paramString1);
    paramContact = Uri.parse("https://api.whatsapp.com/send?phone=".concat(paramString1));
    paramString1 = new android/content/Intent;
    paramString1.<init>("android.intent.action.VIEW", paramContact);
    paramActivity.startActivity(paramString1);
    paramContact = x.a;
    a(paramActivity, "AppOpen");
  }
  
  private static final void a(Activity paramActivity, String paramString)
  {
    paramActivity = paramActivity.getApplication();
    if (paramActivity != null)
    {
      paramActivity = ((TrueApp)paramActivity).s();
      k.a(paramActivity, "(activity.application as TrueApp).analytics");
      e.a locala = new com/truecaller/analytics/e$a;
      locala.<init>("ViewAction");
      paramString = locala.a("Context", "callLog").a("Action", "WhatsApp").a("SubAction", paramString).a();
      k.a(paramString, "eventBuilder.build()");
      paramActivity.a(paramString);
      return;
    }
    paramActivity = new c/u;
    paramActivity.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramActivity;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */