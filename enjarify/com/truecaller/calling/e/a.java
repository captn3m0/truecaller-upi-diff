package com.truecaller.calling.e;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.notifications.g;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.update.c;
import com.truecaller.wizard.utils.PermissionPoller;
import com.truecaller.wizard.utils.PermissionPoller.Permission;
import java.util.HashMap;

public final class a
  extends h
{
  public g a;
  private final int b = 2131887232;
  private final int c = 2131887215;
  private final String d = "WhatsAppEnable";
  private HashMap e;
  
  public final int a()
  {
    return b;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final int b()
  {
    return c;
  }
  
  public final String c()
  {
    return d;
  }
  
  public final void d()
  {
    Object localObject1 = getActivity();
    boolean bool = localObject1 instanceof TruecallerInit;
    if (!bool) {
      localObject1 = null;
    }
    localObject1 = (TruecallerInit)localObject1;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = a;
    if (localObject2 == null)
    {
      localObject3 = "notificationAccessRequester";
      k.a((String)localObject3);
    }
    Object localObject3 = localObject1;
    localObject3 = (Context)localObject1;
    int i = 2131887137;
    bool = ((g)localObject2).a((Context)localObject3, i);
    if (bool)
    {
      localObject1 = ((TruecallerInit)localObject1).e();
      localObject2 = PermissionPoller.Permission.NOTIFICATION_ACCESS;
      ((PermissionPoller)localObject1).a((PermissionPoller.Permission)localObject2);
    }
    super.d();
  }
  
  public final void f()
  {
    HashMap localHashMap = e;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = TrueApp.y();
    k.a(paramBundle, "TrueApp.getApp()");
    paramBundle.a().cz().a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */