package com.truecaller.calling.e;

import c.g.b.k;
import com.truecaller.callhistory.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;

public final class d
  implements c
{
  private final a a;
  private final com.truecaller.i.c b;
  
  public d(a parama, com.truecaller.i.c paramc)
  {
    a = parama;
    b = paramc;
  }
  
  public final void a(int paramInt, boolean paramBoolean, Contact paramContact)
  {
    k.b(paramContact, "contact");
    HistoryEvent localHistoryEvent = new com/truecaller/data/entity/HistoryEvent;
    localHistoryEvent.<init>(paramContact, paramInt);
    if (paramBoolean) {
      localHistoryEvent.n();
    }
    localHistoryEvent.b("com.whatsapp");
    String str = localHistoryEvent.a();
    localHistoryEvent.a(str);
    a.a(localHistoryEvent);
    b.b("whatsAppCallsDetected", true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */