package com.truecaller.calling.e;

import c.l.g;
import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.i.c;
import com.truecaller.utils.d;
import com.truecaller.utils.l;

public final class f
  implements e
{
  private final c a;
  private final com.truecaller.featuretoggles.e b;
  private final d c;
  private final l d;
  private final r e;
  
  public f(c paramc, com.truecaller.featuretoggles.e parame, d paramd, l paraml, r paramr)
  {
    a = paramc;
    b = parame;
    c = paramd;
    d = paraml;
    e = paramr;
  }
  
  public final boolean a()
  {
    Object localObject1 = c;
    int i = ((d)localObject1).h();
    int j = 21;
    if (i < j) {
      return false;
    }
    localObject1 = b;
    Object localObject2 = g;
    Object localObject3 = com.truecaller.featuretoggles.e.a;
    int k = 19;
    localObject3 = localObject3[k];
    localObject1 = ((e.a)localObject2).a((com.truecaller.featuretoggles.e)localObject1, (g)localObject3);
    boolean bool = ((b)localObject1).a();
    if (!bool) {
      return false;
    }
    localObject1 = c;
    localObject2 = "com.whatsapp";
    bool = ((d)localObject1).c((String)localObject2);
    if (!bool) {
      return false;
    }
    localObject1 = e;
    bool = ((r)localObject1).c();
    return bool;
  }
  
  public final boolean b()
  {
    boolean bool = a();
    if (!bool) {
      return false;
    }
    l locall = d;
    bool = locall.d();
    if (!bool) {
      return false;
    }
    return a.a("whatsAppCallsEnabled", true);
  }
  
  public final boolean c()
  {
    return a.b("whatsAppCallsDetected");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */