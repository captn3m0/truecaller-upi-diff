package com.truecaller.calling.e;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.view.View;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.ui.SettingsFragment;
import com.truecaller.ui.SettingsFragment.SettingsViewType;
import java.util.HashMap;

public final class b
  extends h
{
  private final int a = 2131887219;
  private final int b = 2131887231;
  private final String c = "WhatsAppAvailable";
  private HashMap d;
  
  public final int a()
  {
    return a;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final int b()
  {
    return b;
  }
  
  public final String c()
  {
    return c;
  }
  
  public final void e()
  {
    f localf = getActivity();
    if (localf != null)
    {
      Object localObject = localf;
      localObject = (Context)localf;
      SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_GENERAL;
      localObject = SettingsFragment.a((Context)localObject, localSettingsViewType);
      localf.startActivity((Intent)localObject);
    }
    super.e();
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = TrueApp.y();
    k.a(paramBundle, "TrueApp.getApp()");
    paramBundle.a().cz();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */