package com.truecaller.calling.e;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.fragments.a;
import java.util.HashMap;

public abstract class h
  extends a
  implements View.OnClickListener
{
  private HashMap a;
  
  public abstract int a();
  
  public View a(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      a = ((HashMap)localObject1);
    }
    localObject1 = a;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = a;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public abstract int b();
  
  public void e()
  {
    Bundle localBundle = getArguments();
    if (localBundle == null)
    {
      localBundle = new android/os/Bundle;
      localBundle.<init>();
    }
    String str = StartupDialogDismissReason.USER_PRESSED_DISMISS_BUTTON.name();
    localBundle.putString("StartupDialogDismissReason", str);
    setArguments(localBundle);
    super.e();
  }
  
  public void f()
  {
    HashMap localHashMap = a;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = super.onCreateDialog(paramBundle);
    k.a(paramBundle, "super.onCreateDialog(savedInstanceState)");
    Window localWindow = paramBundle.getWindow();
    if (localWindow != null)
    {
      int i = 1;
      localWindow.requestFeature(i);
    }
    return paramBundle;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(2131558580, paramViewGroup, false);
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(2131362303);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    paramViewGroup.setOnClickListener(paramBundle);
    int i = a();
    paramViewGroup.setText(i);
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(2131362326);
    paramViewGroup.setOnClickListener(paramBundle);
    int j = b();
    paramViewGroup.setText(j);
    ((TextView)paramLayoutInflater.findViewById(2131363718)).setText(2131887324);
    ((ImageView)paramLayoutInflater.findViewById(2131363301)).setImageResource(2131234708);
    return paramLayoutInflater;
  }
  
  public void onStart()
  {
    super.onStart();
    Object localObject1 = getDialog();
    if (localObject1 != null)
    {
      localObject1 = ((Dialog)localObject1).getWindow();
      if (localObject1 != null)
      {
        Object localObject2 = new android/graphics/drawable/ColorDrawable;
        ((ColorDrawable)localObject2).<init>(0);
        localObject2 = (Drawable)localObject2;
        ((Window)localObject1).setBackgroundDrawable((Drawable)localObject2);
        ((Window)localObject1).setDimAmount(0.0F);
        localObject2 = ((Window)localObject1).getAttributes();
        width = -1;
        height = -2;
        gravity = 80;
        windowAnimations = 2131951952;
        ((Window)localObject1).setAttributes((WindowManager.LayoutParams)localObject2);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */