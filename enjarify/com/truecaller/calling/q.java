package com.truecaller.calling;

import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.utils.extensions.p;

public final class q
  implements v
{
  private final f b;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(q.class);
    ((u)localObject).<init>(localb, "subtitle", "getSubtitle()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public q(View paramView)
  {
    paramView = com.truecaller.utils.extensions.t.a(paramView, 2131363509);
    b = paramView;
  }
  
  public final void e_(String paramString)
  {
    TextView localTextView = (TextView)b.b();
    k.a(localTextView, "subtitle");
    p.a(localTextView, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */