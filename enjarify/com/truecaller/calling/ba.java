package com.truecaller.calling;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.m;
import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.a.a;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;

public final class ba
  implements az, bb, bc, bd
{
  private final f b;
  private final ColorStateList c;
  private final ColorStateList d;
  private final f e;
  private final az f;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[2];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(ba.class);
    ((u)localObject).<init>(localb, "truecallerBadgeIcon", "getTruecallerBadgeIcon()Landroid/graphics/drawable/Drawable;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(ba.class);
    ((u)localObject).<init>(localb, "titleTextView", "getTitleTextView()Landroid/widget/TextView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public ba(View paramView, az paramaz)
  {
    f = paramaz;
    paramaz = new com/truecaller/calling/ba$a;
    paramaz.<init>(paramView);
    paramaz = c.g.a((a)paramaz);
    b = paramaz;
    paramaz = com.truecaller.utils.ui.b.b(paramView.getContext(), 2130969585);
    c = paramaz;
    paramaz = com.truecaller.utils.ui.b.b(paramView.getContext(), 2130969591);
    d = paramaz;
    paramView = com.truecaller.utils.extensions.t.a(paramView, 2131363510);
    e = paramView;
  }
  
  private final TextView a()
  {
    return (TextView)e.b();
  }
  
  public final void a(boolean paramBoolean)
  {
    TextView localTextView = a();
    Drawable localDrawable;
    if (paramBoolean)
    {
      localDrawable = (Drawable)b.b();
    }
    else
    {
      paramBoolean = false;
      localDrawable = null;
    }
    m.a(localTextView, null, localDrawable);
  }
  
  public final void a_(int paramInt1, int paramInt2)
  {
    TextView localTextView = a();
    k.a(localTextView, "titleTextView");
    ae.a(localTextView, paramInt1, paramInt2);
  }
  
  public final void b_(String paramString)
  {
    TextView localTextView = a();
    k.a(localTextView, "titleTextView");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b_(boolean paramBoolean)
  {
    Object localObject = a();
    ColorStateList localColorStateList;
    if (paramBoolean) {
      localColorStateList = c;
    } else {
      localColorStateList = d;
    }
    ((TextView)localObject).setTextColor(localColorStateList);
    localObject = f;
    if (localObject != null)
    {
      ((az)localObject).b_(paramBoolean);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ba
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */