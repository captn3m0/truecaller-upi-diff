package com.truecaller.calling;

import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import com.truecaller.callerid.CallerIdService;
import com.truecaller.callerid.a.d;
import com.truecaller.common.h.u;
import com.truecaller.log.AssertionUtil;
import com.truecaller.service.CallStateService;
import com.truecaller.utils.l;

public final class al
  implements ak
{
  private final k a;
  private final f b;
  private final l c;
  private final com.truecaller.utils.a d;
  private final d e;
  private final u f;
  
  public al(k paramk, f paramf, l paraml, com.truecaller.utils.a parama, d paramd, u paramu)
  {
    a = paramk;
    b = paramf;
    c = paraml;
    d = parama;
    e = paramd;
    f = paramu;
  }
  
  public final void a(Context paramContext, Intent paramIntent)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramIntent, "intent");
    Object localObject1 = c;
    String str1 = "android.permission.READ_CONTACTS";
    String str2 = "android.permission.WRITE_CONTACTS";
    String[] tmp29_26 = new String[3];
    String[] tmp30_29 = tmp29_26;
    String[] tmp30_29 = tmp29_26;
    tmp30_29[0] = "android.permission.READ_PHONE_STATE";
    tmp30_29[1] = str1;
    tmp30_29[2] = str2;
    Object localObject2 = tmp30_29;
    boolean bool1 = ((l)localObject1).a((String[])localObject2);
    if (!bool1) {
      return;
    }
    localObject1 = aj.c;
    localObject1 = d;
    c.g.b.k.b(paramIntent, "intent");
    c.g.b.k.b(localObject1, "clock");
    localObject2 = paramIntent.getStringExtra("state");
    paramIntent = paramIntent.getStringExtra("incoming_number");
    str1 = TelephonyManager.EXTRA_STATE_IDLE;
    boolean bool2 = c.g.b.k.a(localObject2, str1);
    long l;
    if (bool2)
    {
      localObject2 = new com/truecaller/calling/aj$b;
      l = ((com.truecaller.utils.a)localObject1).a();
      ((aj.b)localObject2).<init>(paramIntent, l);
      localObject2 = (aj)localObject2;
    }
    else
    {
      str1 = TelephonyManager.EXTRA_STATE_RINGING;
      bool2 = c.g.b.k.a(localObject2, str1);
      if (bool2)
      {
        localObject2 = new com/truecaller/calling/aj$e;
        l = ((com.truecaller.utils.a)localObject1).a();
        ((aj.e)localObject2).<init>(paramIntent, l);
        localObject2 = (aj)localObject2;
      }
      else
      {
        str1 = TelephonyManager.EXTRA_STATE_OFFHOOK;
        bool2 = c.g.b.k.a(localObject2, str1);
        if (bool2)
        {
          localObject2 = new com/truecaller/calling/aj$c;
          l = ((com.truecaller.utils.a)localObject1).a();
          ((aj.c)localObject2).<init>(paramIntent, l);
          localObject2 = (aj)localObject2;
        }
        else
        {
          localObject1 = String.valueOf(localObject2);
          paramIntent = "Unknown state ".concat((String)localObject1);
          AssertionUtil.reportWeirdnessButNeverCrash(paramIntent);
          localObject2 = null;
        }
      }
    }
    if (localObject2 == null) {
      return;
    }
    paramIntent = a;
    if (paramIntent != null)
    {
      localObject1 = f;
      paramIntent = ((u)localObject1).b(paramIntent);
      if (paramIntent != null)
      {
        localObject1 = e;
        str1 = "normalizedNumber";
        c.g.b.k.b(paramIntent, str1);
        localObject1 = b;
        int i = 2131365509;
        ((com.truecaller.notifications.a)localObject1).a(paramIntent, i);
      }
    }
    paramIntent = a.a(paramContext, (aj)localObject2);
    if (paramIntent == null) {
      return;
    }
    localObject1 = b;
    paramIntent = ((f)localObject1).a(paramIntent).a();
    CallerIdService.a(paramContext, paramIntent);
    boolean bool3 = CallStateService.a();
    if (!bool3) {
      CallStateService.a(paramContext);
    }
  }
  
  public final void b(Context paramContext, Intent paramIntent)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramIntent, "intent");
    Object localObject1 = c;
    Object localObject2 = "android.permission.READ_CONTACTS";
    String str1 = "android.permission.WRITE_CONTACTS";
    String str2 = "android.permission.PROCESS_OUTGOING_CALLS";
    String[] tmp33_30 = new String[4];
    String[] tmp34_33 = tmp33_30;
    String[] tmp34_33 = tmp33_30;
    tmp34_33[0] = "android.permission.READ_PHONE_STATE";
    tmp34_33[1] = localObject2;
    tmp34_33[2] = str1;
    String[] tmp47_34 = tmp34_33;
    tmp47_34[3] = str2;
    Object localObject3 = tmp47_34;
    boolean bool = ((l)localObject1).a((String[])localObject3);
    if (!bool) {
      return;
    }
    com.truecaller.util.a.a(paramContext);
    localObject1 = a;
    localObject3 = aj.c;
    localObject3 = d;
    c.g.b.k.b(paramIntent, "intent");
    c.g.b.k.b(localObject3, "clock");
    paramIntent = paramIntent.getStringExtra("android.intent.extra.PHONE_NUMBER");
    localObject2 = new com/truecaller/calling/aj$d;
    long l = ((com.truecaller.utils.a)localObject3).a();
    ((aj.d)localObject2).<init>(paramIntent, l);
    paramIntent = ((k)localObject1).a((aj.d)localObject2);
    if (paramIntent == null) {
      return;
    }
    paramIntent = paramIntent.a();
    CallerIdService.a(paramContext, paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */