package com.truecaller.calling;

import android.os.Bundle;

public final class aj$e
  extends aj
{
  final Integer d;
  public final Integer e;
  
  public aj$e(String paramString, long paramLong, Integer paramInteger1, Integer paramInteger2)
  {
    super(paramString, paramLong, (byte)0);
    d = paramInteger1;
    e = paramInteger2;
  }
  
  public final Bundle a()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    int i = 1;
    localBundle.putInt("CALL_STATE", i);
    Object localObject = e;
    int j;
    String str;
    if (localObject != null)
    {
      localObject = (Number)localObject;
      j = ((Number)localObject).intValue();
      str = "ACTION";
      localBundle.putInt(str, j);
    }
    localObject = a;
    if (localObject != null)
    {
      localObject = "NUMBER";
      str = a;
      localBundle.putString((String)localObject, str);
    }
    localObject = d;
    if (localObject != null)
    {
      localObject = (Number)localObject;
      j = ((Number)localObject).intValue();
      str = "SIM_SLOT_INDEX";
      localBundle.putInt(str, j);
    }
    return localBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.aj.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */