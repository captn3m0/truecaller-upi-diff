package com.truecaller.calling.c;

import c.d.f;
import c.g.b.k;
import c.l.g;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.w;
import com.truecaller.bd;
import com.truecaller.callhistory.a;
import com.truecaller.calling.ar;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.multisim.h;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class i
  extends bd
  implements h.e
{
  Contact a;
  b d;
  final com.truecaller.data.access.m e;
  private String f;
  private String g;
  private boolean h;
  private boolean i;
  private boolean j;
  private boolean k;
  private boolean l;
  private int m;
  private boolean n;
  private final ar o;
  private final h p;
  private final a q;
  private final n r;
  private final f s;
  private final f t;
  
  public i(ar paramar, h paramh, a parama, n paramn, com.truecaller.data.access.m paramm, f paramf1, f paramf2)
  {
    o = paramar;
    p = paramh;
    q = parama;
    r = paramn;
    e = paramm;
    s = paramf1;
    t = paramf2;
    f = "";
    g = "";
    int i1 = o.c();
    m = i1;
    paramar = new com/truecaller/calling/c/b;
    paramh = paramar;
    paramar.<init>(false, false, false, null, 15);
    d = paramar;
  }
  
  private final void a(Number paramNumber)
  {
    boolean bool = n;
    if (!bool) {
      return;
    }
    paramNumber = paramNumber.getId();
    if (paramNumber != null)
    {
      long l1 = paramNumber.longValue();
      paramNumber = String.valueOf(l1);
      if (paramNumber != null)
      {
        ag localag = (ag)bg.a;
        f localf = s;
        Object localObject = new com/truecaller/calling/c/i$c;
        ((i.c)localObject).<init>(this, paramNumber, null);
        localObject = (c.g.a.m)localObject;
        e.b(localag, localf, (c.g.a.m)localObject, 2);
        return;
      }
    }
  }
  
  public final b a(h.b paramb, g paramg)
  {
    k.b(paramb, "itemPresenter");
    k.b(paramg, "property");
    return d;
  }
  
  public final void a(Contact paramContact, ArrayList paramArrayList, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, String paramString)
  {
    Object localObject1 = "analyticsContext";
    k.b(paramString, (String)localObject1);
    if (paramContact == null)
    {
      paramContact = (h.f)c;
      if (paramContact != null)
      {
        paramContact.a();
        return;
      }
      return;
    }
    localObject1 = paramArrayList;
    localObject1 = (Collection)paramArrayList;
    boolean bool1 = true;
    if (localObject1 != null)
    {
      bool2 = ((Collection)localObject1).isEmpty();
      if (!bool2)
      {
        bool2 = false;
        localObject1 = null;
        break label81;
      }
    }
    boolean bool2 = true;
    label81:
    if (bool2)
    {
      paramContact = (h.f)c;
      if (paramContact != null)
      {
        paramContact.a();
        return;
      }
      return;
    }
    a = paramContact;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject3;
    if (paramBoolean4)
    {
      localObject2 = r;
      i1 = 2131888349;
      localObject3 = new Object[0];
      localObject2 = ((n)localObject2).a(i1, (Object[])localObject3);
    }
    else if (paramBoolean5)
    {
      localObject2 = r;
      i1 = 2131888352;
      localObject3 = new Object[0];
      localObject2 = ((n)localObject2).a(i1, (Object[])localObject3);
    }
    else
    {
      localObject2 = r;
      i1 = 2131888340;
      localObject3 = new Object[0];
      localObject2 = ((n)localObject2).a(i1, (Object[])localObject3);
    }
    ((StringBuilder)localObject1).append((String)localObject2);
    Object localObject2 = paramContact.t();
    int i1 = 0;
    if (localObject2 != null)
    {
      localObject3 = " - ";
      localObject2 = String.valueOf(localObject2);
      localObject2 = ((String)localObject3).concat((String)localObject2);
    }
    else
    {
      localObject2 = null;
    }
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    f = ((String)localObject1);
    g = paramString;
    j = paramBoolean1;
    k = paramBoolean4;
    i = paramBoolean3;
    l = paramBoolean5;
    h = paramBoolean2;
    paramBoolean2 = paramArrayList.size();
    if (paramBoolean2 == bool1)
    {
      paramArrayList = (Number)c.a.m.d((List)paramArrayList);
      paramContact = paramContact.s();
      a(paramArrayList, paramContact);
      paramContact = (h.f)c;
      if (paramContact != null)
      {
        paramContact.a();
        return;
      }
      return;
    }
    paramArrayList = (Iterable)paramArrayList;
    Object localObject4 = paramArrayList.iterator();
    boolean bool3;
    do
    {
      paramBoolean3 = ((Iterator)localObject4).hasNext();
      if (!paramBoolean3) {
        break;
      }
      localObject5 = ((Iterator)localObject4).next();
      paramString = (String)localObject5;
      paramString = (Number)localObject5;
      if (paramBoolean1)
      {
        bool3 = paramString.isPrimary();
        if (bool3)
        {
          bool3 = true;
          continue;
        }
      }
      bool3 = false;
      paramString = null;
    } while (!bool3);
    break label472;
    paramBoolean3 = false;
    Object localObject5 = null;
    label472:
    localObject5 = (Number)localObject5;
    if (localObject5 != null)
    {
      paramContact = paramContact.s();
      a((Number)localObject5, paramContact);
      paramContact = (h.f)c;
      if (paramContact != null)
      {
        paramContact.a();
        return;
      }
      return;
    }
    paramContact = new com/truecaller/calling/c/b;
    Object localObject6 = p;
    bool1 = ((h)localObject6).j();
    i1 = 0;
    int i2 = 8;
    localObject1 = paramContact;
    paramContact.<init>(bool1, paramBoolean4, paramBoolean5, null, i2);
    d = paramContact;
    paramContact = paramArrayList.iterator();
    for (;;)
    {
      boolean bool4 = paramContact.hasNext();
      if (!bool4) {
        break;
      }
      paramArrayList = (Number)paramContact.next();
      localObject6 = q;
      localObject4 = paramArrayList.a();
      localObject6 = ((a)localObject6).c((String)localObject4);
      localObject4 = new com/truecaller/calling/c/i$b;
      ((i.b)localObject4).<init>(paramArrayList, this);
      localObject4 = (ac)localObject4;
      ((w)localObject6).a((ac)localObject4);
    }
  }
  
  public final void a(Number paramNumber, String paramString)
  {
    k.b(paramNumber, "number");
    Object localObject = paramNumber.e();
    if (localObject == null)
    {
      paramNumber = (h.f)c;
      if (paramNumber != null)
      {
        paramNumber.a();
        return;
      }
      return;
    }
    a(paramNumber);
    boolean bool1 = h;
    if (bool1)
    {
      paramNumber = (h.f)c;
      if (paramNumber != null)
      {
        boolean bool2 = i;
        String str = g;
        paramNumber.a((String)localObject, paramString, bool2, str);
      }
    }
    else
    {
      bool1 = k;
      if (bool1)
      {
        paramNumber = (h.f)c;
        if (paramNumber != null)
        {
          paramString = g;
          paramNumber.a((String)localObject, paramString);
        }
      }
      else
      {
        bool1 = l;
        if (bool1)
        {
          paramNumber = (h.f)c;
          if (paramNumber != null)
          {
            paramString = g;
            paramNumber.b((String)localObject, paramString);
          }
        }
      }
    }
    paramNumber = (ag)bg.a;
    paramString = t;
    localObject = new com/truecaller/calling/c/i$a;
    ((i.a)localObject).<init>(this, null);
    localObject = (c.g.a.m)localObject;
    e.b(paramNumber, paramString, (c.g.a.m)localObject, 2);
  }
  
  public final void a(boolean paramBoolean)
  {
    n = paramBoolean;
  }
  
  public final boolean b()
  {
    return j;
  }
  
  public final String c()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */