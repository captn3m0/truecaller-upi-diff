package com.truecaller.calling.c;

import c.g.b.k;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;

public final class d
{
  final Number a;
  final HistoryEvent b;
  
  public d(Number paramNumber, HistoryEvent paramHistoryEvent)
  {
    a = paramNumber;
    b = paramHistoryEvent;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof d;
      if (bool1)
      {
        paramObject = (d)paramObject;
        Object localObject = a;
        Number localNumber = a;
        bool1 = k.a(localObject, localNumber);
        if (bool1)
        {
          localObject = b;
          paramObject = b;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    Number localNumber = a;
    int i = 0;
    int j;
    if (localNumber != null)
    {
      j = localNumber.hashCode();
    }
    else
    {
      j = 0;
      localNumber = null;
    }
    j *= 31;
    HistoryEvent localHistoryEvent = b;
    if (localHistoryEvent != null) {
      i = localHistoryEvent.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SelectNumberItem(number=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", historyEvent=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */