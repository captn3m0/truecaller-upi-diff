package com.truecaller.calling.c;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import com.truecaller.adapter_delegates.h;
import com.truecaller.calling.dialer.ax;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.multisim.ae;
import com.truecaller.util.af;
import com.truecaller.util.cf;
import com.truecaller.utils.n;
import java.util.ArrayList;

public final class e
  extends com.truecaller.adapter_delegates.c
  implements h.b
{
  private final h.d c;
  private final h.a d;
  private final af e;
  private final ae f;
  private final com.truecaller.search.local.model.c g;
  private final n h;
  private final ax i;
  private final cf j;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(e.class);
    ((u)localObject).<init>(localb, "data", "getData()Lcom/truecaller/calling/select_number/SelectNumberData;");
    localObject = (g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public e(h.d paramd, h.a parama, af paramaf, ae paramae, com.truecaller.search.local.model.c paramc, n paramn, ax paramax, cf paramcf)
  {
    d = parama;
    e = paramaf;
    f = paramae;
    g = paramc;
    h = paramn;
    i = paramax;
    j = paramcf;
    c = paramd;
  }
  
  private final b a()
  {
    h.d locald = c;
    Object localObject = this;
    localObject = (h.b)this;
    g localg = b[0];
    return locald.a((h.b)localObject, localg);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject1 = ad;
    int k = b;
    localObject1 = ((ArrayList)localObject1).get(k);
    k.a(localObject1, "data.items[event.position]");
    localObject1 = (d)localObject1;
    Object localObject2 = b;
    if (localObject2 != null)
    {
      localObject2 = ((HistoryEvent)localObject2).s();
      if (localObject2 != null)
      {
        localObject2 = ((Contact)localObject2).s();
        break label76;
      }
    }
    k = 0;
    localObject2 = null;
    label76:
    k.a(a, "ItemEvent.ACTION_SIM_TWO_CLICKED");
    paramh = d;
    localObject1 = a;
    paramh.a((Number)localObject1, (String)localObject2);
    return true;
  }
  
  public final int getItemCount()
  {
    return ad.size();
  }
  
  public final long getItemId(int paramInt)
  {
    return -1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */