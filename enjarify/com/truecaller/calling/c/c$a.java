package com.truecaller.calling.c;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.f;
import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.dialogs.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class c$a
{
  public static void a(f paramf, Contact paramContact, List paramList, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, String paramString)
  {
    k.b(paramString, "analyticsContext");
    c localc = new com/truecaller/calling/c/c;
    localc.<init>();
    Object localObject1 = localc.getArguments();
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = "contact";
      paramContact = (Parcelable)paramContact;
      ((Bundle)localObject1).putParcelable((String)localObject2, paramContact);
    }
    paramContact = localc.getArguments();
    if (paramContact != null)
    {
      localObject1 = "numbers";
      localObject2 = new java/util/ArrayList;
      paramList = (Collection)paramList;
      ((ArrayList)localObject2).<init>(paramList);
      paramContact.putParcelableArrayList((String)localObject1, (ArrayList)localObject2);
    }
    paramContact = localc.getArguments();
    if (paramContact != null)
    {
      paramList = "consider_primary";
      paramContact.putBoolean(paramList, paramBoolean1);
    }
    paramContact = localc.getArguments();
    if (paramContact != null)
    {
      paramList = "call";
      paramContact.putBoolean(paramList, paramBoolean2);
    }
    paramContact = localc.getArguments();
    if (paramContact != null)
    {
      paramList = "video_call";
      paramContact.putBoolean(paramList, paramBoolean3);
    }
    paramContact = localc.getArguments();
    if (paramContact != null)
    {
      paramList = "sms";
      paramContact.putBoolean(paramList, paramBoolean4);
    }
    paramContact = localc.getArguments();
    if (paramContact != null)
    {
      paramList = "voip_call";
      paramContact.putBoolean(paramList, paramBoolean5);
    }
    paramContact = localc.getArguments();
    if (paramContact != null)
    {
      paramList = "analytics_context";
      paramContact.putString(paramList, paramString);
    }
    a.a(localc, paramf);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */