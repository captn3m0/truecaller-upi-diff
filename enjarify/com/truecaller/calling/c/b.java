package com.truecaller.calling.c;

import c.g.b.k;
import java.util.ArrayList;

public final class b
{
  final boolean a;
  final boolean b;
  final boolean c;
  final ArrayList d;
  
  public b()
  {
    this(false, false, false, null, 15);
  }
  
  private b(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, ArrayList paramArrayList)
  {
    a = paramBoolean1;
    b = paramBoolean2;
    c = paramBoolean3;
    d = paramArrayList;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof b;
      if (bool2)
      {
        paramObject = (b)paramObject;
        bool2 = a;
        boolean bool3 = a;
        ArrayList localArrayList;
        if (bool2 == bool3)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localArrayList = null;
        }
        if (bool2)
        {
          bool2 = b;
          bool3 = b;
          if (bool2 == bool3)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            localArrayList = null;
          }
          if (bool2)
          {
            bool2 = c;
            bool3 = c;
            if (bool2 == bool3)
            {
              bool2 = true;
            }
            else
            {
              bool2 = false;
              localArrayList = null;
            }
            if (bool2)
            {
              localArrayList = d;
              paramObject = d;
              boolean bool4 = k.a(localArrayList, paramObject);
              if (bool4) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    boolean bool = a;
    int j = 1;
    if (bool) {
      bool = true;
    }
    bool *= true;
    int k = b;
    if (k != 0) {
      k = 1;
    }
    int i = (i + k) * 31;
    int m = c;
    if (m == 0) {
      j = m;
    }
    i = (i + j) * 31;
    ArrayList localArrayList = d;
    if (localArrayList != null)
    {
      j = localArrayList.hashCode();
    }
    else
    {
      j = 0;
      localArrayList = null;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SelectNumberData(multiSim=");
    boolean bool = a;
    localStringBuilder.append(bool);
    localStringBuilder.append(", sms=");
    bool = b;
    localStringBuilder.append(bool);
    localStringBuilder.append(", voip=");
    bool = c;
    localStringBuilder.append(bool);
    localStringBuilder.append(", items=");
    ArrayList localArrayList = d;
    localStringBuilder.append(localArrayList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */