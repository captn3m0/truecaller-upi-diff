package com.truecaller.calling.c;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.p;

public final class k
  implements h.g
{
  private final c.f b;
  private final c.f c;
  private final p d;
  private final com.truecaller.adapter_delegates.f e;
  private final View f;
  private final h.g.a g;
  
  static
  {
    g[] arrayOfg = new g[2];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(k.class);
    ((u)localObject).<init>(localb, "recycler", "getRecycler()Landroid/support/v7/widget/RecyclerView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(k.class);
    ((u)localObject).<init>(localb, "checkbox", "getCheckbox()Landroid/widget/CheckBox;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public k(View paramView, h.g.a parama, h.b paramb, boolean paramBoolean)
  {
    f = paramView;
    g = parama;
    paramView = com.truecaller.utils.extensions.t.a(f, 2131364093);
    b = paramView;
    paramView = com.truecaller.utils.extensions.t.a(f, 2131364308);
    c = paramView;
    paramView = new com/truecaller/adapter_delegates/p;
    paramb = (com.truecaller.adapter_delegates.b)paramb;
    parama = new com/truecaller/calling/c/k$b;
    parama.<init>(this);
    parama = (c.g.a.b)parama;
    c.g.a.b localb = (c.g.a.b)k.c.a;
    paramView.<init>(paramb, 2131559018, parama, localb);
    d = paramView;
    paramView = new com/truecaller/adapter_delegates/f;
    parama = (a)d;
    paramView.<init>(parama);
    e = paramView;
    paramView = (RecyclerView)b.b();
    parama = (RecyclerView.Adapter)e;
    paramView.setAdapter(parama);
    parama = new android/support/v7/widget/LinearLayoutManager;
    paramb = f.getContext();
    parama.<init>(paramb);
    parama = (RecyclerView.LayoutManager)parama;
    paramView.setLayoutManager(parama);
    paramView.setItemAnimator(null);
    paramView = (CheckBox)c.b();
    parama = paramView;
    com.truecaller.utils.extensions.t.a((View)paramView, paramBoolean);
    parama = new com/truecaller/calling/c/k$a;
    parama.<init>(this, paramBoolean);
    parama = (CompoundButton.OnCheckedChangeListener)parama;
    paramView.setOnCheckedChangeListener(parama);
  }
  
  public final void a(int paramInt)
  {
    e.notifyItemInserted(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */