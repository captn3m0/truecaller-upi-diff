package com.truecaller.calling.c;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import c.f;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import com.truecaller.adapter_delegates.i;
import com.truecaller.adapter_delegates.k;
import com.truecaller.calling.ag;
import com.truecaller.calling.ap;
import com.truecaller.calling.ba;
import com.truecaller.calling.bc;
import com.truecaller.calling.c;
import com.truecaller.calling.d;
import com.truecaller.calling.dialer.CallIconType;
import com.truecaller.search.local.model.c.a;

public final class g
  extends RecyclerView.ViewHolder
  implements ag, bc, c, h.c
{
  private final f b;
  private final f c;
  private final f d;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[3];
    Object localObject = new c/g/b/u;
    b localb = w.a(g.class);
    ((u)localObject).<init>(localb, "actionOneIcon", "getActionOneIcon()Landroid/widget/ImageView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(g.class);
    ((u)localObject).<init>(localb, "actionTwoIcon", "getActionTwoIcon()Landroid/view/View;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(g.class);
    ((u)localObject).<init>(localb, "actionTwo", "getActionTwo()Landroid/view/View;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    a = arrayOfg;
  }
  
  public g(View paramView, k paramk)
  {
    super(paramView);
    Object localObject = new com/truecaller/calling/ba;
    ((ba)localObject).<init>(paramView);
    e = ((ba)localObject);
    localObject = new com/truecaller/calling/ap;
    ((ap)localObject).<init>(paramView);
    f = ((ap)localObject);
    localObject = new com/truecaller/calling/d;
    ((d)localObject).<init>(paramView);
    g = ((d)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131361918);
    b = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131361939);
    c = ((f)localObject);
    int i = 2131361843;
    localObject = com.truecaller.utils.extensions.t.a(paramView, i);
    d = ((f)localObject);
    localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    String str = null;
    int j = 12;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, j);
    paramView = a();
    if (paramView != null)
    {
      str = "ItemEvent.ACTION_SIM_TWO_CLICKED";
      j = 8;
      i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, str, j);
    }
  }
  
  private final View a()
  {
    return (View)d.b();
  }
  
  public final void a(int paramInt)
  {
    ImageView localImageView = (ImageView)b.b();
    if (localImageView != null)
    {
      localImageView.setImageResource(paramInt);
      return;
    }
  }
  
  public final void a(CallIconType paramCallIconType)
  {
    f.a(paramCallIconType);
  }
  
  public final void a(c.a parama)
  {
    g.a(parama);
  }
  
  public final void a(Integer paramInteger)
  {
    f.a(paramInteger);
  }
  
  public final void a(Long paramLong)
  {
    f.a(paramLong);
  }
  
  public final void a(boolean paramBoolean)
  {
    f.a(paramBoolean);
  }
  
  public final void b(String paramString)
  {
    f.b(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    f.b(paramBoolean);
  }
  
  public final void b_(String paramString)
  {
    e.b_(paramString);
  }
  
  public final void c(boolean paramBoolean)
  {
    View localView = (View)c.b();
    if (localView != null) {
      com.truecaller.utils.extensions.t.a(localView, paramBoolean);
    }
    localView = a();
    if (localView != null)
    {
      com.truecaller.utils.extensions.t.a(localView, paramBoolean);
      return;
    }
  }
  
  public final void e_(String paramString)
  {
    f.e_(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */