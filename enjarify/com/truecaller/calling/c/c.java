package com.truecaller.calling.c;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog.Builder;
import android.view.View;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.initiate_call.b;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.util.bj;
import com.truecaller.voip.ai;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class c
  extends com.truecaller.ui.dialogs.a
  implements h.f
{
  public static final c.a e;
  public h.e a;
  public h.b b;
  public b c;
  public ai d;
  private HashMap f;
  
  static
  {
    c.a locala = new com/truecaller/calling/c/c$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public static final void a(f paramf, Contact paramContact, List paramList, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString)
  {
    c.a.a(paramf, paramContact, paramList, true, paramBoolean1, paramBoolean2, paramBoolean3, false, paramString);
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    dismissAllowingStateLoss();
  }
  
  public final void a(String paramString1, String paramString2)
  {
    String str = "analyticsContext";
    c.g.b.k.b(paramString2, str);
    paramString2 = getContext();
    if (paramString2 != null)
    {
      bj.a(paramString2, paramString1);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean, String paramString3)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString3, "analyticsContext");
    b.a.a locala = new com/truecaller/calling/initiate_call/b$a$a;
    locala.<init>(paramString1, paramString3);
    paramString1 = locala.a(paramString2).a(paramBoolean);
    paramString2 = c;
    if (paramString2 == null)
    {
      String str = "initiateCallHelper";
      c.g.b.k.a(str);
    }
    paramString1 = paramString1.a();
    paramString2.a(paramString1);
  }
  
  public final void b()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void b(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "analyticsContext");
    ai localai = d;
    if (localai == null)
    {
      String str = "voipUtil";
      c.g.b.k.a(str);
    }
    localai.a(paramString1, paramString2);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    Object localObject1 = null;
    boolean bool1;
    if (paramBundle != null)
    {
      paramBundle = paramBundle.getApplicationContext();
    }
    else
    {
      bool1 = false;
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().bs().a().a(this);
      paramBundle = a;
      String str;
      if (paramBundle == null)
      {
        str = "presenter";
        c.g.b.k.a(str);
      }
      paramBundle.b(this);
      h.e locale = a;
      if (locale == null)
      {
        paramBundle = "presenter";
        c.g.b.k.a(paramBundle);
      }
      paramBundle = getArguments();
      Bundle localBundle;
      if (paramBundle != null)
      {
        str = "contact";
        paramBundle = (Contact)paramBundle.getParcelable(str);
        localBundle = paramBundle;
      }
      else
      {
        localBundle = null;
      }
      paramBundle = getArguments();
      if (paramBundle != null) {
        localObject1 = paramBundle.getParcelableArrayList("numbers");
      }
      Object localObject2 = localObject1;
      paramBundle = getArguments();
      boolean bool2;
      if (paramBundle != null)
      {
        localObject1 = "consider_primary";
        bool1 = paramBundle.getBoolean((String)localObject1);
        bool2 = bool1;
      }
      else
      {
        bool1 = true;
        bool2 = true;
      }
      paramBundle = getArguments();
      localObject1 = null;
      boolean bool3;
      if (paramBundle != null)
      {
        str = "call";
        bool1 = paramBundle.getBoolean(str);
        bool3 = bool1;
      }
      else
      {
        bool3 = false;
      }
      paramBundle = getArguments();
      boolean bool4;
      if (paramBundle != null)
      {
        str = "video_call";
        bool1 = paramBundle.getBoolean(str);
        bool4 = bool1;
      }
      else
      {
        bool4 = false;
      }
      paramBundle = getArguments();
      boolean bool5;
      if (paramBundle != null)
      {
        str = "sms";
        bool1 = paramBundle.getBoolean(str);
        bool5 = bool1;
      }
      else
      {
        bool5 = false;
      }
      paramBundle = getArguments();
      boolean bool6;
      if (paramBundle != null)
      {
        localObject1 = "voip_call";
        bool1 = paramBundle.getBoolean((String)localObject1);
        bool6 = bool1;
      }
      else
      {
        bool6 = false;
      }
      paramBundle = getArguments();
      if (paramBundle != null)
      {
        localObject1 = "analytics_context";
        paramBundle = paramBundle.getString((String)localObject1);
        if (paramBundle != null) {}
      }
      else
      {
        paramBundle = "";
      }
      locale.a(localBundle, (ArrayList)localObject2, bool2, bool3, bool4, bool5, bool6, paramBundle);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    Object localObject1 = getActivity();
    if (localObject1 == null)
    {
      paramBundle = super.onCreateDialog(paramBundle);
      c.g.b.k.a(paramBundle, "super.onCreateDialog(savedInstanceState)");
      return paramBundle;
    }
    c.g.b.k.a(localObject1, "activity ?: return super…ialog(savedInstanceState)");
    localObject1 = (Context)localObject1;
    int i = 2131558595;
    paramBundle = View.inflate((Context)localObject1, i, null);
    Object localObject2 = new com/truecaller/calling/c/k;
    c.g.b.k.a(paramBundle, "view");
    Object localObject3 = a;
    if (localObject3 == null)
    {
      localObject4 = "presenter";
      c.g.b.k.a((String)localObject4);
    }
    localObject3 = (h.g.a)localObject3;
    Object localObject4 = b;
    if (localObject4 == null)
    {
      localObject5 = "itemPresenter";
      c.g.b.k.a((String)localObject5);
    }
    Object localObject5 = a;
    if (localObject5 == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    boolean bool = ((h.e)localObject5).b();
    ((k)localObject2).<init>(paramBundle, (h.g.a)localObject3, (h.b)localObject4, bool);
    localObject3 = a;
    if (localObject3 == null)
    {
      localObject4 = "presenter";
      c.g.b.k.a((String)localObject4);
    }
    ((h.e)localObject3).a(localObject2);
    localObject2 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject3 = "presenter";
      c.g.b.k.a((String)localObject3);
    }
    localObject1 = (CharSequence)((h.e)localObject1).c();
    paramBundle = ((AlertDialog.Builder)localObject2).setTitle((CharSequence)localObject1).setView(paramBundle).create();
    c.g.b.k.a(paramBundle, "AlertDialog.Builder(acti…ew)\n            .create()");
    return (Dialog)paramBundle;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    h.e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locale.x_();
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    h.e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locale.y_();
    b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */