package com.truecaller.calling;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import c.f;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.adapter_delegates.i;
import com.truecaller.ui.view.ContactPhoto;

public final class o
  implements an, ao, az, e, n, p
{
  private final f b;
  private final f c;
  private final Object d;
  private final az e;
  
  static
  {
    g[] arrayOfg = new g[2];
    Object localObject = new c/g/b/u;
    b localb = w.a(o.class);
    ((u)localObject).<init>(localb, "photo", "getPhoto()Lcom/truecaller/ui/view/ContactPhoto;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(o.class);
    ((u)localObject).<init>(localb, "progress", "getProgress()Landroid/view/View;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public o(View paramView, Object paramObject, az paramaz)
  {
    d = paramObject;
    e = paramaz;
    paramObject = com.truecaller.utils.extensions.t.a(paramView, 2131362554);
    b = ((f)paramObject);
    paramView = com.truecaller.utils.extensions.t.a(paramView, 2131362547);
    c = paramView;
  }
  
  private final ContactPhoto a()
  {
    return (ContactPhoto)b.b();
  }
  
  public final void a(int paramInt)
  {
    a().setContactBadgeDrawable(paramInt);
  }
  
  public final void a(com.truecaller.adapter_delegates.k paramk, RecyclerView.ViewHolder paramViewHolder)
  {
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramViewHolder, "holder");
    Object localObject = a();
    c.g.b.k.a(localObject, "photo");
    localObject = (View)localObject;
    String str = ActionType.PROFILE.getEventAction();
    i.a((View)localObject, paramk, paramViewHolder, str, "avatar");
  }
  
  public final void a(Object paramObject)
  {
    ContactPhoto localContactPhoto = a();
    Object localObject = d;
    localContactPhoto.a(paramObject, localObject);
  }
  
  public final void a_(boolean paramBoolean)
  {
    View localView = (View)c.b();
    if (localView != null)
    {
      com.truecaller.utils.extensions.t.a(localView, paramBoolean);
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    a().setBackupBadge(paramBoolean);
  }
  
  public final void b_(boolean paramBoolean)
  {
    a().setIsSpam(paramBoolean);
    az localaz = e;
    if (localaz != null)
    {
      localaz.b_(paramBoolean);
      return;
    }
  }
  
  public final void d(boolean paramBoolean)
  {
    ContactPhoto localContactPhoto = a();
    c.g.b.k.a(localContactPhoto, "photo");
    localContactPhoto.setClickable(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */