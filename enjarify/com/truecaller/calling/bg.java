package com.truecaller.calling;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import c.f;
import c.g.b.u;
import c.g.b.w;
import c.l.b;

public final class bg
  implements a
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private ActionType f;
  private final View g;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[4];
    Object localObject = new c/g/b/u;
    b localb = w.a(bg.class);
    ((u)localObject).<init>(localb, "actionOneView", "getActionOneView()Landroid/widget/ImageView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bg.class);
    ((u)localObject).<init>(localb, "actionOneClickArea", "getActionOneClickArea()Landroid/view/View;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bg.class);
    ((u)localObject).<init>(localb, "icSms", "getIcSms()Landroid/graphics/drawable/Drawable;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bg.class);
    ((u)localObject).<init>(localb, "icVoip", "getIcVoip()Landroid/graphics/drawable/Drawable;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[3] = localObject;
    a = arrayOfg;
  }
  
  public bg(View paramView)
  {
    g = paramView;
    paramView = com.truecaller.utils.extensions.t.a(g, 2131361918);
    b = paramView;
    paramView = com.truecaller.utils.extensions.t.a(g, 2131361842);
    c = paramView;
    paramView = new com/truecaller/calling/bg$a;
    paramView.<init>(this);
    paramView = c.g.a((c.g.a.a)paramView);
    d = paramView;
    paramView = new com/truecaller/calling/bg$b;
    paramView.<init>(this);
    paramView = c.g.a((c.g.a.a)paramView);
    e = paramView;
  }
  
  public final ImageView a()
  {
    return (ImageView)b.b();
  }
  
  public final void a(ActionType paramActionType)
  {
    f = paramActionType;
    if (paramActionType != null)
    {
      int[] arrayOfInt = bh.a;
      i = paramActionType.ordinal();
      i = arrayOfInt[i];
      switch (i)
      {
      default: 
        break;
      case 2: 
        paramActionType = (Drawable)e.b();
        break;
      case 1: 
        paramActionType = (Drawable)d.b();
        break;
      }
    }
    int i = 0;
    paramActionType = null;
    if (paramActionType != null)
    {
      a().setImageDrawable(paramActionType);
      return;
    }
  }
  
  public final View b()
  {
    return (View)c.b();
  }
  
  public final void b(boolean paramBoolean)
  {
    Object localObject = f;
    if (localObject == null)
    {
      com.truecaller.utils.extensions.t.b((View)a());
      com.truecaller.utils.extensions.t.b(b());
      return;
    }
    localObject = a();
    if (paramBoolean)
    {
      localObject = (View)localObject;
      com.truecaller.utils.extensions.t.a((View)localObject);
    }
    else
    {
      localObject = (View)localObject;
      com.truecaller.utils.extensions.t.b((View)localObject);
    }
    localObject = b();
    if (paramBoolean)
    {
      com.truecaller.utils.extensions.t.a((View)localObject);
      return;
    }
    com.truecaller.utils.extensions.t.c((View)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.bg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */