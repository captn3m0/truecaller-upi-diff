package com.truecaller.calling.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.common.b.a;
import com.truecaller.common.h.o;
import com.truecaller.wizard.utils.i;

public final class CallingNotificationsBroadcastReceiver
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if ((paramContext != null) && (paramIntent != null))
    {
      Object localObject = paramIntent.getAction();
      if (localObject != null)
      {
        int i = ((String)localObject).hashCode();
        int j = -2104750529;
        if (i != j)
        {
          int k = -720889926;
          if (i == k)
          {
            paramContext = "com.truecaller.request_set_as_default_phone_app";
            boolean bool1 = ((String)localObject).equals(paramContext);
            if (bool1)
            {
              paramContext = a.F();
              k.a(paramContext, "ApplicationBase.getAppBase()");
              paramContext = (Context)paramContext;
              paramIntent = o.a(paramContext).addFlags(268435456);
              o.a(paramContext, paramIntent);
            }
          }
        }
        else
        {
          String str = "com.truecaller.request_allow_draw_over_other_apps";
          boolean bool2 = ((String)localObject).equals(str);
          if (bool2)
          {
            i.b(paramContext);
            Toast.makeText(paramContext, 2131886810, 1).show();
            return;
          }
        }
      }
      paramContext = new java/lang/RuntimeException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("Unknown action ");
      paramIntent = paramIntent.getAction();
      ((StringBuilder)localObject).append(paramIntent);
      ((StringBuilder)localObject).append(" in onReceive");
      paramIntent = ((StringBuilder)localObject).toString();
      paramContext.<init>(paramIntent);
      throw ((Throwable)paramContext);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.notifications.CallingNotificationsBroadcastReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */