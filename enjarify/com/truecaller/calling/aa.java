package com.truecaller.calling;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import c.a.y;
import c.f;
import c.g.a.a;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import com.truecaller.adapter_delegates.i;
import com.truecaller.flashsdk.ui.CompoundFlashButton;
import java.util.List;

public final class aa
  implements ac
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final f f;
  private boolean g;
  private ActionType h;
  private final View i;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[5];
    Object localObject = new c/g/b/u;
    b localb = w.a(aa.class);
    ((u)localObject).<init>(localb, "actionOneView", "getActionOneView()Landroid/widget/ImageView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(aa.class);
    ((u)localObject).<init>(localb, "actionOneClickArea", "getActionOneClickArea()Landroid/view/View;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(aa.class);
    ((u)localObject).<init>(localb, "flashButton", "getFlashButton()Lcom/truecaller/flashsdk/ui/CompoundFlashButton;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(aa.class);
    ((u)localObject).<init>(localb, "icSms", "getIcSms()Landroid/graphics/drawable/Drawable;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[3] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(aa.class);
    ((u)localObject).<init>(localb, "icCall", "getIcCall()Landroid/graphics/drawable/Drawable;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[4] = localObject;
    a = arrayOfg;
  }
  
  public aa(View paramView)
  {
    i = paramView;
    paramView = com.truecaller.utils.extensions.t.a(i, 2131361918);
    b = paramView;
    paramView = com.truecaller.utils.extensions.t.a(i, 2131361842);
    c = paramView;
    paramView = com.truecaller.utils.extensions.t.a(i, 2131363109);
    d = paramView;
    paramView = new com/truecaller/calling/aa$b;
    paramView.<init>(this);
    paramView = c.g.a((a)paramView);
    e = paramView;
    paramView = new com/truecaller/calling/aa$a;
    paramView.<init>(this);
    paramView = c.g.a((a)paramView);
    f = paramView;
  }
  
  private final ImageView a()
  {
    return (ImageView)b.b();
  }
  
  private final View b()
  {
    return (View)c.b();
  }
  
  private final CompoundFlashButton c()
  {
    return (CompoundFlashButton)d.b();
  }
  
  public final void a(com.truecaller.adapter_delegates.k paramk, RecyclerView.ViewHolder paramViewHolder)
  {
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramViewHolder, "holder");
    Object localObject1 = a();
    c.g.b.k.a(localObject1, "actionOneView");
    localObject1 = (View)localObject1;
    Object localObject2 = new com/truecaller/calling/aa$c;
    ((aa.c)localObject2).<init>(this);
    localObject2 = (a)localObject2;
    i.a((View)localObject1, paramk, paramViewHolder, (a)localObject2, "button");
    paramk = b();
    paramViewHolder = new com/truecaller/calling/aa$d;
    paramViewHolder.<init>(this);
    paramViewHolder = (View.OnClickListener)paramViewHolder;
    paramk.setOnClickListener(paramViewHolder);
  }
  
  public final void a(ActionType paramActionType)
  {
    if (paramActionType != null)
    {
      localObject = ab.a;
      int j = paramActionType.ordinal();
      k = localObject[j];
      switch (k)
      {
      default: 
        break;
      case 2: 
      case 3: 
        localObject = (Drawable)f.b();
        break;
      case 1: 
        localObject = (Drawable)e.b();
        break;
      }
    }
    int k = 0;
    Object localObject = null;
    boolean bool;
    if (localObject != null)
    {
      h = paramActionType;
      paramActionType = a();
      com.truecaller.utils.extensions.t.a((View)paramActionType);
      paramActionType.setImageDrawable((Drawable)localObject);
      bool = g;
      b(bool);
      return;
    }
    localObject = ActionType.FLASH;
    if (paramActionType == localObject)
    {
      h = paramActionType;
      paramActionType = a();
      c.g.b.k.a(paramActionType, "actionOneView");
      com.truecaller.utils.extensions.t.b((View)paramActionType);
      bool = g;
      b(bool);
      return;
    }
    h = null;
    paramActionType = a();
    c.g.b.k.a(paramActionType, "actionOneView");
    com.truecaller.utils.extensions.t.b((View)paramActionType);
    paramActionType = b();
    c.g.b.k.a(paramActionType, "actionOneClickArea");
    com.truecaller.utils.extensions.t.b(paramActionType);
  }
  
  public final void a(x paramx)
  {
    CompoundFlashButton localCompoundFlashButton = c();
    List localList;
    if (paramx != null)
    {
      localList = a;
      if (localList != null) {}
    }
    else
    {
      localList = (List)y.a;
    }
    String str;
    if (paramx != null)
    {
      str = ae.a(paramx);
      if (str != null) {}
    }
    else
    {
      str = "";
    }
    if (paramx != null)
    {
      paramx = c;
      if (paramx != null) {}
    }
    else
    {
      paramx = "";
    }
    localCompoundFlashButton.a(localList, str, paramx);
  }
  
  public final void b(boolean paramBoolean)
  {
    g = paramBoolean;
    Object localObject = h;
    if (localObject != null)
    {
      localObject = b();
      if (paramBoolean)
      {
        com.truecaller.utils.extensions.t.a((View)localObject);
        return;
      }
      com.truecaller.utils.extensions.t.c((View)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */