package com.truecaller.calling;

import android.view.View;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.view.VoipTintedImageView;

public final class bf
  implements be
{
  private final f b;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(bf.class);
    ((u)localObject).<init>(localb, "voipTintedImageView", "getVoipTintedImageView()Lcom/truecaller/ui/view/VoipTintedImageView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public bf(View paramView)
  {
    paramView = com.truecaller.utils.extensions.t.a(paramView, 2131361918);
    b = paramView;
  }
  
  public final void a(com.truecaller.calling.contacts_list.data.g paramg, Contact paramContact, c.g.a.b paramb)
  {
    k.b(paramg, "voipAvailabilityCache");
    k.b(paramContact, "contact");
    k.b(paramb, "callback");
    ((VoipTintedImageView)b.b()).a(paramg, paramContact, paramb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.bf
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */