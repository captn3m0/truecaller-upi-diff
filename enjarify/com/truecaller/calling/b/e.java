package com.truecaller.calling.b;

import android.graphics.drawable.Drawable;
import c.a.m;
import c.g.b.k;
import com.truecaller.calling.initiate_call.h.a;
import com.truecaller.multisim.SimInfo;
import com.truecaller.utils.n;

public final class e
  implements d
{
  private final com.truecaller.multisim.h a;
  private final n b;
  
  public e(com.truecaller.multisim.h paramh, n paramn)
  {
    a = paramh;
    b = paramn;
  }
  
  public final com.truecaller.calling.initiate_call.h a(int paramInt)
  {
    Object localObject1 = a.a(paramInt);
    int i = 0;
    Object localObject2 = null;
    if (localObject1 == null) {
      return null;
    }
    Object localObject3 = "multiSimManager.getSimIn…ndex(slot) ?: return null";
    k.a(localObject1, (String)localObject3);
    int j;
    if (paramInt == 0) {
      j = 2131233935;
    } else {
      j = 2131233936;
    }
    localObject3 = b.c(j);
    k.a(localObject3, "resourceProvider.getDrawable(drawableRes)");
    Object localObject4 = b.a(2130903061)[paramInt];
    int k = 3;
    Object localObject5 = new String[k];
    Object localObject6 = d;
    Object[] arrayOfObject = null;
    localObject5[0] = localObject6;
    String str = c;
    localObject5[1] = str;
    int m = 2;
    boolean bool = j;
    if (bool)
    {
      localObject1 = b;
      i = 2131887947;
      arrayOfObject = new Object[0];
      localObject2 = ((n)localObject1).a(i, arrayOfObject);
    }
    localObject5[m] = localObject2;
    localObject1 = m.e((Object[])localObject5);
    localObject5 = localObject1;
    localObject5 = (Iterable)localObject1;
    localObject6 = (CharSequence)", ";
    localObject1 = m.a((Iterable)localObject5, (CharSequence)localObject6, null, null, 0, null, null, 62);
    localObject2 = new com/truecaller/calling/initiate_call/h$a;
    k.a(localObject4, "title");
    localObject4 = (CharSequence)localObject4;
    localObject1 = (CharSequence)localObject1;
    ((h.a)localObject2).<init>((CharSequence)localObject4, (CharSequence)localObject1, (Drawable)localObject3, paramInt);
    return (com.truecaller.calling.initiate_call.h)localObject2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */