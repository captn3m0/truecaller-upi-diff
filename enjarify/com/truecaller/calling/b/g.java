package com.truecaller.calling.b;

import android.telecom.TelecomManager;
import c.a.m;
import c.g.a.b;
import c.g.b.k;
import c.m.i;
import c.m.l;
import java.util.List;

public final class g
  implements f
{
  final TelecomManager a;
  
  public g(TelecomManager paramTelecomManager)
  {
    a = paramTelecomManager;
  }
  
  public final List a()
  {
    Object localObject1 = a.getCallCapablePhoneAccounts();
    k.a(localObject1, "telecomManager.callCapablePhoneAccounts");
    localObject1 = m.n((Iterable)localObject1);
    Object localObject2 = new com/truecaller/calling/b/g$a;
    ((g.a)localObject2).<init>(this);
    localObject2 = (b)localObject2;
    localObject1 = l.d((i)localObject1, (b)localObject2);
    localObject2 = (b)g.b.a;
    return l.d(l.c((i)localObject1, (b)localObject2));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */