package com.truecaller.calling;

import android.content.Context;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;

public final class am
{
  public static final ak a(Context paramContext)
  {
    String str = "context";
    k.b(paramContext, str);
    paramContext = paramContext.getApplicationContext();
    if (paramContext != null)
    {
      paramContext = ((TrueApp)paramContext).a().ab();
      k.a(paramContext, "trueApp.objectsGraph.phoneStateHandler()");
      return paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */