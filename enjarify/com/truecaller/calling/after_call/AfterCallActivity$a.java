package com.truecaller.calling.after_call;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import com.truecaller.filters.FilterManager;
import com.truecaller.log.d;
import com.truecaller.old.a.c;
import com.truecaller.ui.dialogs.g;
import com.truecaller.ui.f;

class AfterCallActivity$a
  extends f
  implements c
{
  private final Activity a;
  private Dialog c;
  
  AfterCallActivity$a(Activity paramActivity, FilterManager paramFilterManager)
  {
    super(paramFilterManager);
    a = paramActivity;
  }
  
  public final void ac_()
  {
    a(2131886537);
  }
  
  public final Context c()
  {
    return a;
  }
  
  public final void d_(boolean paramBoolean)
  {
    boolean bool = isFinishing();
    if (bool) {
      return;
    }
    try
    {
      Object localObject = c;
      if (localObject == null)
      {
        localObject = new com/truecaller/ui/dialogs/g;
        Activity localActivity = a;
        ((g)localObject).<init>(localActivity, paramBoolean);
        c = ((Dialog)localObject);
      }
      Dialog localDialog = c;
      localDialog.show();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      d.a(localRuntimeException, "RuntimeException while showing loading dialog");
    }
  }
  
  public final void e()
  {
    boolean bool = isFinishing();
    if (bool) {
      return;
    }
    try
    {
      Dialog localDialog = c;
      if (localDialog != null)
      {
        localDialog = c;
        localDialog.dismiss();
      }
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      d.a(localRuntimeException, "RuntimeException while dismissing loading dialog");
    }
  }
  
  public boolean isFinishing()
  {
    return a.isFinishing();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */