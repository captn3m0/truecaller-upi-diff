package com.truecaller.calling.after_call;

import android.app.Activity;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.g;
import com.truecaller.ui.f;

final class AfterCallActivity$5
  extends AfterCallActivity.a
{
  AfterCallActivity$5(AfterCallActivity paramAfterCallActivity, Activity paramActivity, FilterManager paramFilterManager)
  {
    super(paramActivity, paramFilterManager);
  }
  
  public final void a()
  {
    AfterCallActivity.d locald = new com/truecaller/calling/after_call/AfterCallActivity$d;
    AfterCallActivity localAfterCallActivity = a;
    locald.<init>(localAfterCallActivity, (byte)0);
  }
  
  public final void a(String paramString)
  {
    AfterCallActivity localAfterCallActivity = a;
    boolean bool = AfterCallActivity.h(localAfterCallActivity);
    if (bool)
    {
      AfterCallActivity.i(a);
      AfterCallActivity.j(a);
      return;
    }
    localAfterCallActivity = a;
    bool = AfterCallActivity.k(localAfterCallActivity);
    if (bool)
    {
      AfterCallActivity.l(a);
      AfterCallActivity.a(a, paramString);
      return;
    }
    localAfterCallActivity = a;
    bool = AfterCallActivity.m(localAfterCallActivity);
    if (bool)
    {
      AfterCallActivity.n(a);
      localAfterCallActivity = a;
      localAfterCallActivity.a(paramString);
    }
  }
  
  public final void b()
  {
    Object localObject1 = a.a.b;
    Object localObject2 = AfterCallActivity.e(a);
    Object localObject3 = AfterCallActivity.f(a);
    Object localObject4 = AfterCallActivity.g(a);
    boolean bool1 = false;
    localObject1 = ((FilterManager)localObject1).a((String)localObject2, (String)localObject3, (String)localObject4, false);
    localObject2 = a;
    localObject3 = j;
    localObject4 = FilterManager.ActionSource.CUSTOM_BLACKLIST;
    boolean bool2;
    if (localObject3 == localObject4)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject3 = null;
    }
    AfterCallActivity.a((AfterCallActivity)localObject2, bool2);
    localObject2 = a;
    localObject3 = j;
    localObject4 = FilterManager.ActionSource.CUSTOM_WHITELIST;
    if (localObject3 == localObject4)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject3 = null;
    }
    AfterCallActivity.b((AfterCallActivity)localObject2, bool2);
    localObject2 = a;
    localObject1 = j;
    localObject3 = FilterManager.ActionSource.TOP_SPAMMER;
    if (localObject1 == localObject3) {
      bool1 = true;
    }
    AfterCallActivity.c((AfterCallActivity)localObject2, bool1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */