package com.truecaller.calling.after_call;

import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.util.af;
import com.truecaller.utils.extensions.c;

public final class h
  implements g
{
  private final com.truecaller.utils.a a;
  private final af b;
  private final com.truecaller.common.g.a c;
  
  public h(com.truecaller.utils.a parama, af paramaf, com.truecaller.common.g.a parama1)
  {
    a = parama;
    b = paramaf;
    c = parama1;
  }
  
  public final boolean a()
  {
    Object localObject1 = c;
    Object localObject2 = "featureBusinessSuggestionMaxCount";
    long l1 = 0L;
    long l2 = ((com.truecaller.common.g.a)localObject1).a((String)localObject2, l1);
    boolean bool1 = l2 < l1;
    if (!bool1) {
      return false;
    }
    localObject1 = b;
    localObject2 = c;
    String str1 = "businessSuggestionShownTimestanp";
    long l3 = ((com.truecaller.common.g.a)localObject2).a(str1, l1);
    boolean bool2 = ((af)localObject1).a(l3);
    boolean bool3 = true;
    if (bool2)
    {
      localObject1 = c;
      str1 = "businessSuggestionShownCount";
      l3 = ((com.truecaller.common.g.a)localObject1).a(str1, l1);
      localObject1 = c;
      String str2 = "featureBusinessSuggestionMaxCount";
      l1 = ((com.truecaller.common.g.a)localObject1).a(str2, l1);
      bool2 = l3 < l1;
      if (bool2) {
        return bool3;
      }
      return false;
    }
    return bool3;
  }
  
  public final boolean a(Contact paramContact, HistoryEvent paramHistoryEvent)
  {
    k.b(paramContact, "contact");
    String str = "event";
    k.b(paramHistoryEvent, str);
    int i = paramHistoryEvent.f();
    int j = 1;
    int k;
    if (i != j)
    {
      k = paramHistoryEvent.f();
      i = 2;
      if (k != i) {
        return false;
      }
    }
    paramHistoryEvent = paramContact.t();
    if (paramHistoryEvent != null)
    {
      paramHistoryEvent = (CharSequence)paramHistoryEvent;
      k = paramHistoryEvent.length();
      if (k > 0)
      {
        k = 1;
      }
      else
      {
        k = 0;
        paramHistoryEvent = null;
      }
      paramHistoryEvent = Boolean.valueOf(k);
    }
    else
    {
      bool = false;
      paramHistoryEvent = null;
    }
    boolean bool = c.a(paramHistoryEvent);
    if (bool) {
      return false;
    }
    bool = paramContact.Z();
    if (bool) {
      return false;
    }
    int n = paramContact.getSource();
    int m = 16;
    n &= m;
    if (n == m)
    {
      n = 1;
    }
    else
    {
      n = 0;
      paramContact = null;
    }
    if (n != 0) {
      return false;
    }
    return j;
  }
  
  public final void b()
  {
    com.truecaller.common.g.a locala1 = c;
    String str = "businessSuggestionShownTimestanp";
    long l1 = 0L;
    long l2 = locala1.a(str, l1);
    af localaf = b;
    boolean bool = localaf.a(l2) ^ true;
    long l3 = 1L;
    if (bool)
    {
      locala1 = c;
      l1 = a.a();
      locala1.b("businessSuggestionShownTimestanp", l1);
      c.b("businessSuggestionShownCount", l3);
      return;
    }
    l2 = c.a("businessSuggestionShownCount", l1);
    com.truecaller.common.g.a locala2 = c;
    l2 += l3;
    locala2.b("businessSuggestionShownCount", l2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */