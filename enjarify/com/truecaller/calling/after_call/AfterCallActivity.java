package com.truecaller.calling.after_call;

import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.database.ContentObserver;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.Toast;
import c.g.b.k;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.internal.ads.zzly;
import com.google.common.base.Strings;
import com.truecaller.TrueApp;
import com.truecaller.aftercall.PromoBadgeView;
import com.truecaller.analytics.bb;
import com.truecaller.analytics.bc;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.calling.initiate_call.b.a;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.common.h.u;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.p;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.network.search.j.c;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.ShineView;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.ReferralManager.ReferralLaunchContext;
import com.truecaller.service.SyncPhoneBookService;
import com.truecaller.tag.TagPickActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.WizardActivity;
import com.truecaller.ui.components.AfterCallHeaderView;
import com.truecaller.ui.components.AfterCallHeaderView.a;
import com.truecaller.ui.components.FeedbackItemView;
import com.truecaller.ui.components.FeedbackItemView.DisplaySource;
import com.truecaller.ui.components.FeedbackItemView.FeedbackItem;
import com.truecaller.ui.components.FeedbackItemView.a;
import com.truecaller.ui.components.q;
import com.truecaller.ui.components.q.a;
import com.truecaller.ui.components.q.b;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.dialogs.d.c;
import com.truecaller.ui.view.AfterCallButtons;
import com.truecaller.ui.view.AfterCallButtons.a;
import com.truecaller.update.ForcedUpdate;
import com.truecaller.util.af;
import com.truecaller.util.b.as;
import com.truecaller.util.b.j.b;
import com.truecaller.util.br.a;
import com.truecaller.util.co;
import com.truecaller.utils.i;
import com.truecaller.utils.l;
import com.truecaller.voip.ai;
import com.truecaller.whoviewedme.ProfileViewService;
import java.util.Collections;
import java.util.List;

public class AfterCallActivity
  extends com.truecaller.ui.n
  implements View.OnClickListener, com.truecaller.old.a.c, FeedbackItemView.a, AfterCallButtons.a
{
  private static final String[] m = { "com.truecaller.EVENT_AFTER_CALL_START" };
  private ValueAnimator A;
  private ValueAnimator B;
  private final ColorDrawable C;
  private Contact D;
  private HistoryEvent E;
  private int F;
  private String G;
  private String H;
  private String I;
  private String J;
  private int K;
  private boolean L;
  private boolean M;
  private boolean N;
  private boolean O;
  private boolean P;
  private boolean Q;
  private boolean R;
  private boolean S;
  private boolean T;
  private boolean U;
  private boolean V;
  private boolean W;
  private boolean X;
  private boolean Y;
  private boolean Z;
  com.truecaller.ui.f a;
  private final j.c aA;
  private ai aa;
  private com.truecaller.analytics.b ab;
  private FeedbackItemView ac;
  private com.truecaller.ui.dialogs.c ad;
  private com.truecaller.i.c ae;
  private g af;
  private AfterCallActivity.c ag;
  private AfterCallActivity.e ah;
  private final AfterCallActivity.g ai;
  private boolean aj;
  private ContentObserver ak;
  private com.truecaller.featuretoggles.e al;
  private com.truecaller.messaging.j am;
  private ReferralManager an;
  private boolean ao;
  private boolean ap;
  private final Handler aq;
  private final Runnable ar;
  private CallRecordingManager as;
  private i at;
  private com.truecaller.calling.initiate_call.b au;
  private com.truecaller.androidactors.f av;
  private a aw;
  private d ax;
  private final Runnable ay;
  private boolean az;
  FilterManager b;
  p c;
  com.truecaller.androidactors.f d;
  private bp i;
  private boolean j;
  private com.truecaller.premium.br k;
  private com.truecaller.common.f.c l;
  private AfterCallHeaderView n;
  private ViewGroup o;
  private ViewGroup p;
  private ViewGroup q;
  private AfterCallButtons r;
  private View s;
  private View t;
  private View u;
  private View v;
  private CallRecordingFloatingButton w;
  private View x;
  private ShineView y;
  private q z;
  
  public AfterCallActivity()
  {
    Object localObject = new android/graphics/drawable/ColorDrawable;
    int i1 = Color.argb(178, 0, 0, 0);
    ((ColorDrawable)localObject).<init>(i1);
    C = ((ColorDrawable)localObject);
    K = 999;
    W = false;
    X = false;
    Y = false;
    Z = false;
    localObject = new com/truecaller/calling/after_call/AfterCallActivity$g;
    ((AfterCallActivity.g)localObject).<init>(this, (byte)0);
    ai = ((AfterCallActivity.g)localObject);
    ap = false;
    localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    aq = ((Handler)localObject);
    localObject = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$l5TrabNa37HosSKBtSfwmiJ7Fm8;
    ((-..Lambda.AfterCallActivity.l5TrabNa37HosSKBtSfwmiJ7Fm8)localObject).<init>(this);
    ar = ((Runnable)localObject);
    localObject = new com/truecaller/calling/after_call/AfterCallActivity$1;
    ((AfterCallActivity.1)localObject).<init>(this);
    ay = ((Runnable)localObject);
    az = true;
    localObject = new com/truecaller/calling/after_call/AfterCallActivity$4;
    ((AfterCallActivity.4)localObject).<init>(this);
    aA = ((j.c)localObject);
  }
  
  public static Intent a(Context paramContext, HistoryEvent paramHistoryEvent, int paramInt)
  {
    int i1 = com.truecaller.util.n.a(o);
    Object localObject = new android/content/Intent;
    Class localClass = AfterCallActivity.class;
    ((Intent)localObject).<init>(paramContext, localClass);
    int i2 = 268533760;
    paramContext = ((Intent)localObject).addFlags(i2);
    localObject = "ARG_SOURCE";
    paramContext = paramContext.putExtra((String)localObject, paramInt).putExtra("ARG_CALL_TYPE", i1);
    Parcel localParcel = Parcel.obtain();
    i1 = 0;
    byte[] arrayOfByte = null;
    try
    {
      paramHistoryEvent.writeToParcel(localParcel, 0);
      localParcel.setDataPosition(0);
      paramHistoryEvent = "ARG_EVENT";
      arrayOfByte = localParcel.marshall();
      paramContext.putExtra(paramHistoryEvent, arrayOfByte);
      return paramContext;
    }
    finally
    {
      localParcel.recycle();
    }
  }
  
  private static HistoryEvent a(Intent paramIntent)
  {
    Object localObject = "ARG_EVENT";
    paramIntent = paramIntent.getByteArrayExtra((String)localObject);
    if (paramIntent == null) {
      return null;
    }
    localObject = Parcel.obtain();
    try
    {
      int i1 = paramIntent.length;
      ((Parcel)localObject).unmarshall(paramIntent, 0, i1);
      ((Parcel)localObject).setDataPosition(0);
      paramIntent = HistoryEvent.CREATOR;
      paramIntent = paramIntent.createFromParcel((Parcel)localObject);
      paramIntent = (HistoryEvent)paramIntent;
      return paramIntent;
    }
    finally
    {
      ((Parcel)localObject).recycle();
    }
  }
  
  private void a(String paramString1, String paramString2)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("ViewAction");
    String str1 = "afterCall";
    localObject = ((e.a)localObject).a("Context", str1);
    String str2 = "Action";
    paramString1 = ((e.a)localObject).a(str2, paramString1);
    boolean bool = Strings.isNullOrEmpty(paramString2);
    if (!bool)
    {
      localObject = "SubAction";
      paramString1.a((String)localObject, paramString2);
    }
    paramString2 = ab;
    paramString1 = paramString1.a();
    paramString2.a(paramString1);
  }
  
  public static void b(Context paramContext, HistoryEvent paramHistoryEvent, int paramInt)
  {
    boolean bool = ForcedUpdate.a(paramContext, true);
    if (!bool) {
      try
      {
        paramHistoryEvent = a(paramContext, paramHistoryEvent, paramInt);
        paramContext.startActivity(paramHistoryEvent);
        return;
      }
      catch (NullPointerException paramContext)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramContext);
      }
    }
  }
  
  private void b(String paramString)
  {
    ah.a(paramString);
    paramString = FeedbackItemView.a(FeedbackItemView.DisplaySource.AFTERCALL, this);
    ((com.truecaller.presence.c)i.ae().a()).b();
    Object localObject1 = D;
    boolean bool1 = ((Contact)localObject1).Z();
    if (!bool1)
    {
      localObject1 = D;
      bool1 = ((Contact)localObject1).P();
      if (bool1)
      {
        bool1 = true;
        Object localObject2;
        if (paramString != null)
        {
          localObject2 = new com/truecaller/ui/dialogs/d$c;
          ((d.c)localObject2).<init>(this);
          i = 2131558567;
          localObject2 = com.truecaller.ui.dialogs.d.a((d.c)localObject2);
          ad = ((com.truecaller.ui.dialogs.c)localObject2);
          ad.a();
          ad.e.setCancelable(bool1);
          ad.e.setCanceledOnTouchOutside(bool1);
          localObject2 = ad.e;
          -..Lambda.AfterCallActivity.ArRaFUtNtSq0xofoyfVj-7D3kdY localArRaFUtNtSq0xofoyfVj-7D3kdY = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$ArRaFUtNtSq0xofoyfVj-7D3kdY;
          localArRaFUtNtSq0xofoyfVj-7D3kdY.<init>(this);
          ((Dialog)localObject2).setOnCancelListener(localArRaFUtNtSq0xofoyfVj-7D3kdY);
          localObject2 = (FeedbackItemView)ad.f;
          ((FeedbackItemView)localObject2).setFeedbackItem(paramString);
          ((FeedbackItemView)localObject2).setFeedbackItemListener(this);
          ((FeedbackItemView)localObject2).setDialogStyle(bool1);
          Settings.g("GOOGLE_REVIEW_ASK_TIMESTAMP");
          return;
        }
        boolean bool2 = ap;
        if (!bool2)
        {
          paramString = an;
          if (paramString != null)
          {
            localObject2 = ReferralManager.ReferralLaunchContext.AFTER_CALL_PROMO;
            bool2 = paramString.c((ReferralManager.ReferralLaunchContext)localObject2);
            if (bool2)
            {
              ap = bool1;
              paramString = an;
              localObject1 = ReferralManager.ReferralLaunchContext.AFTER_CALL_PROMO;
              paramString.a((ReferralManager.ReferralLaunchContext)localObject1);
              return;
            }
          }
        }
        paramString = an;
        if (paramString != null)
        {
          localObject1 = ReferralManager.ReferralLaunchContext.AFTER_CALL_PROMO;
          paramString.b((ReferralManager.ReferralLaunchContext)localObject1);
        }
        finish();
        return;
      }
    }
    finish();
  }
  
  private void b(boolean paramBoolean)
  {
    Object localObject = u;
    Runnable localRunnable = ay;
    ((View)localObject).removeCallbacks(localRunnable);
    aj = paramBoolean;
    if (paramBoolean)
    {
      View localView = u;
      localObject = ay;
      localView.postDelayed((Runnable)localObject, 200L);
      return;
    }
    u.setVisibility(8);
  }
  
  private boolean b(Intent paramIntent)
  {
    if (paramIntent == null) {
      return false;
    }
    try
    {
      Object localObject = a(paramIntent);
      E = ((HistoryEvent)localObject);
      localObject = "ARG_SOURCE";
      int i1 = paramIntent.getIntExtra((String)localObject, 0);
      F = i1;
      i1 = F;
      int i2 = 1;
      if (i1 == i2)
      {
        localObject = d;
        String str1 = "widget";
        String str2 = "listItemClicked";
        bb.a((com.truecaller.androidactors.f)localObject, str1, str2);
      }
      localObject = E;
      localObject = f;
      D = ((Contact)localObject);
      localObject = E;
      localObject = b;
      G = ((String)localObject);
      localObject = E;
      localObject = c;
      H = ((String)localObject);
      localObject = E;
      localObject = d;
      I = ((String)localObject);
      localObject = "ARG_CALL_TYPE";
      int i3 = 999;
      int i4 = paramIntent.getIntExtra((String)localObject, i3);
      K = i4;
      paramIntent = H;
      localObject = G;
      paramIntent = am.e(paramIntent, (CharSequence)localObject);
      paramIntent = (String)paramIntent;
      boolean bool = ab.d(paramIntent);
      P = bool;
      paramIntent = E;
      if (paramIntent != null)
      {
        paramIntent = D;
        if (paramIntent != null) {
          return i2;
        }
      }
      return false;
    }
    catch (RuntimeException localRuntimeException)
    {
      com.truecaller.log.d.a(localRuntimeException;
    }
    return false;
  }
  
  private void c(String paramString)
  {
    Q = false;
    bb.a(d, "afterCall", "unblocked");
    a("unblockQuery", null);
    com.truecaller.ui.f localf = a;
    Object localObject1 = G;
    String str = H;
    localObject1 = am.e((CharSequence)localObject1, str);
    Object localObject2 = localObject1;
    localObject2 = (String)localObject1;
    boolean bool = W;
    localf.a((String)localObject2, "PHONE_NUMBER", "afterCall", paramString, bool);
  }
  
  private boolean c(boolean paramBoolean)
  {
    boolean bool = M;
    if (!bool)
    {
      bool = L;
      if (!bool)
      {
        bool = O;
        if ((!bool) || (!paramBoolean)) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  private void h()
  {
    ah.c = null;
    Contact localContact = D;
    if (localContact != null)
    {
      String str1 = localContact.getTcId();
      localContact = D;
      String str2 = localContact.t();
      String str3 = G;
      String str4 = H;
      String str5 = I;
      DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.AfterCall;
      boolean bool = true;
      DetailsFragment.a(this, str1, str2, str3, str4, str5, localSourceType, false, bool);
    }
    finish();
  }
  
  private void i()
  {
    Object localObject1 = D;
    if (localObject1 != null)
    {
      localObject1 = E;
      if (localObject1 != null)
      {
        localObject1 = b;
        if (localObject1 != null)
        {
          localObject1 = af;
          Object localObject2 = D;
          HistoryEvent localHistoryEvent = E;
          boolean bool1 = ((g)localObject1).a((Contact)localObject2, localHistoryEvent);
          boolean bool2 = V;
          if (!bool2)
          {
            bool2 = j;
            if ((bool2) && (bool1))
            {
              V = true;
              af.b();
              localObject1 = ab;
              localObject2 = new com/truecaller/analytics/e$a;
              ((e.a)localObject2).<init>("ViewAction");
              localObject2 = ((e.a)localObject2).a("Context", "afterCall").a("Action", "businessSuggestion").a("SubAction", "shown").a();
              ((com.truecaller.analytics.b)localObject1).b((com.truecaller.analytics.e)localObject2);
              localObject1 = n;
              localObject2 = new com/truecaller/calling/after_call/AfterCallActivity$7;
              ((AfterCallActivity.7)localObject2).<init>(this);
              ((AfterCallHeaderView)localObject1).a((com.truecaller.ui.components.a)localObject2);
              return;
            }
          }
          if (!bool1)
          {
            localObject1 = n;
            ((AfterCallHeaderView)localObject1).e();
          }
          return;
        }
      }
    }
  }
  
  private void j()
  {
    boolean bool = ao;
    if (bool)
    {
      ReferralManager localReferralManager = an;
      if (localReferralManager != null)
      {
        Object localObject = D;
        bool = localReferralManager.b((Contact)localObject);
        if (bool)
        {
          localObject = r;
          int i1 = 2131888619;
          String str = getString(i1);
          ((AfterCallButtons)localObject).setReferralButtonLabel(str);
        }
        r.a(bool);
        return;
      }
    }
    r.a(false);
  }
  
  private boolean k()
  {
    Contact localContact = D;
    if (localContact != null)
    {
      int i1 = 32;
      boolean bool = localContact.a(i1);
      if (bool) {
        return false;
      }
    }
    return Settings.h();
  }
  
  private void l()
  {
    boolean bool = k();
    if (!bool)
    {
      o.setVisibility(8);
      return;
    }
    Object localObject1 = z;
    if (localObject1 != null) {
      return;
    }
    localObject1 = ah;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/calling/after_call/AfterCallActivity$e;
      int i1 = F;
      int i2 = K;
      com.truecaller.analytics.b localb = ab;
      ((AfterCallActivity.e)localObject1).<init>(i1, i2, localb);
      ah = ((AfterCallActivity.e)localObject1);
    }
    bool = m();
    if (bool)
    {
      bool = isFinishing();
      if (!bool)
      {
        localObject1 = new com/truecaller/ui/components/q;
        ((q)localObject1).<init>(this, "AFTERCALL");
        z = ((q)localObject1);
        localObject1 = z;
        Object localObject2 = D;
        HistoryEvent localHistoryEvent = E;
        ((q)localObject1).a((Contact)localObject2, localHistoryEvent);
        localObject1 = z;
        localObject2 = new com/truecaller/calling/after_call/AfterCallActivity$8;
        ((AfterCallActivity.8)localObject2).<init>(this);
        ((q)localObject1).setAdListener((q.a)localObject2);
        localObject1 = q;
        localObject2 = z;
        ((ViewGroup)localObject1).addView((View)localObject2);
        return;
      }
    }
  }
  
  private boolean m()
  {
    boolean bool = N;
    int i1 = 1;
    if (bool) {
      return i1;
    }
    Contact localContact = D;
    bool = localContact.V();
    if (!bool)
    {
      localContact = D;
      bool = localContact.c(i1);
      if (!bool)
      {
        localContact = D;
        int i2 = 4;
        bool = localContact.c(i2);
        if (!bool) {}
      }
      else
      {
        bool = false;
        localContact = null;
        break label73;
      }
    }
    bool = true;
    label73:
    if (!bool) {
      return i1;
    }
    return false;
  }
  
  private void n()
  {
    AfterCallActivity.c localc = ag;
    if (localc != null)
    {
      unregisterReceiver(localc);
      localc = null;
      ag = null;
    }
  }
  
  private void o()
  {
    Object localObject1 = d;
    String str = "savedContact";
    bb.a((com.truecaller.androidactors.f)localObject1, "afterCall", str);
    localObject1 = i.bR();
    Object localObject2 = D;
    ((b)localObject1).a((Contact)localObject2);
    try
    {
      localObject1 = D;
      localObject2 = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$kNElqoiETHoY1xDvF8h9oAcI_xU;
      ((-..Lambda.AfterCallActivity.kNElqoiETHoY1xDvF8h9oAcI_xU)localObject2).<init>(this);
      localObject1 = com.truecaller.util.br.a((Contact)localObject1, (br.a)localObject2);
      localObject2 = getSupportFragmentManager();
      str = com.truecaller.util.br.a;
      ((com.truecaller.util.br)localObject1).show((android.support.v4.app.j)localObject2, str);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localIllegalStateException;
    }
  }
  
  private boolean p()
  {
    boolean bool = q();
    if (!bool)
    {
      bool = r();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  private boolean q()
  {
    boolean bool = M;
    if (!bool)
    {
      bool = L;
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private boolean r()
  {
    boolean bool = M;
    if (!bool)
    {
      Contact localContact = D;
      bool = localContact.U();
      if (!bool)
      {
        bool = O;
        if (!bool) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  private void s()
  {
    boolean bool1 = Y;
    if (bool1)
    {
      Object localObject = D;
      if (localObject != null)
      {
        int i2 = 32;
        bool1 = ((Contact)localObject).a(i2);
        if (bool1)
        {
          bool1 = true;
          break label40;
        }
      }
      bool1 = false;
      localObject = null;
      label40:
      AfterCallButtons localAfterCallButtons = r;
      int i1;
      if (bool1) {
        i1 = android.support.v4.content.b.c(this, 2131100368);
      } else {
        i1 = com.truecaller.utils.ui.b.a(this, 2130968621);
      }
      boolean bool2 = X;
      PromoBadgeView localPromoBadgeView = b;
      localPromoBadgeView.setBorder(i1);
      localObject = b;
      ((PromoBadgeView)localObject).a(bool2);
    }
  }
  
  private void t()
  {
    boolean bool1 = Y;
    if (bool1)
    {
      Object localObject = D;
      if (localObject != null)
      {
        int i2 = 32;
        bool1 = ((Contact)localObject).a(i2);
        if (bool1)
        {
          bool1 = true;
          break label40;
        }
      }
      bool1 = false;
      localObject = null;
      label40:
      AfterCallButtons localAfterCallButtons = r;
      int i1;
      if (bool1) {
        i1 = android.support.v4.content.b.c(this, 2131100368);
      } else {
        i1 = com.truecaller.utils.ui.b.a(this, 2130968621);
      }
      boolean bool2 = Z;
      PromoBadgeView localPromoBadgeView = c;
      localPromoBadgeView.setBorder(i1);
      localObject = c;
      ((PromoBadgeView)localObject).a(bool2);
    }
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = E.f;
    int i1 = 0;
    Object localObject2 = null;
    boolean bool1;
    if (localObject1 != null)
    {
      localObject1 = ((Contact)localObject1).t();
    }
    else
    {
      bool1 = false;
      localObject1 = null;
    }
    boolean bool2 = false;
    com.truecaller.ui.f localf = null;
    boolean bool3 = true;
    Object localObject3;
    Object localObject4;
    switch (paramInt)
    {
    default: 
      break;
    case 7: 
      localObject3 = D;
      if (localObject3 != null)
      {
        aa.a(this, (Contact)localObject3, "afterCall");
        return;
      }
      break;
    case 6: 
      ah.c = null;
      a("pay", null);
      localObject3 = i.bE();
      localObject2 = G;
      localObject3 = ((com.truecaller.payments.a)localObject3).a((String)localObject2);
      if (localObject3 != null) {
        TransactionActivity.startForSend(this, (String)localObject3, (String)localObject1);
      }
      break;
    case 5: 
      localObject3 = an;
      if (localObject3 != null)
      {
        localObject1 = ReferralManager.ReferralLaunchContext.AFTER_CALL;
        localObject2 = D;
        ((ReferralManager)localObject3).a((ReferralManager.ReferralLaunchContext)localObject1, (Contact)localObject2);
        return;
      }
      break;
    case 4: 
      ah.c = null;
      a("message", null);
      localObject3 = aw;
      bool1 = a;
      if (bool1)
      {
        localObject3 = b;
        localObject1 = new com/truecaller/analytics/e$a;
        ((e.a)localObject1).<init>("AfterCallImNudgeClicked");
        localObject1 = ((e.a)localObject1).a();
        localObject2 = "AnalyticsEvent.Builder(A…udgeClicked.NAME).build()";
        k.a(localObject1, (String)localObject2);
        ((com.truecaller.analytics.b)localObject3).b((com.truecaller.analytics.e)localObject1);
      }
      localObject3 = E.b;
      localObject1 = i.V();
      localObject3 = Participant.b((String)localObject3, (u)localObject1, "-1");
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>(this, ConversationActivity.class);
      localObject4 = new Participant[bool3];
      localObject4[0] = localObject3;
      ((Intent)localObject1).putExtra("participants", (Parcelable[])localObject4);
      ((Intent)localObject1).putExtra("launch_source", "afterCall");
      startActivity((Intent)localObject1);
      return;
    case 3: 
      paramInt = P;
      if (paramInt == 0)
      {
        localObject3 = new String[bool3];
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("Number was not valid for caller: ");
        localObject2 = H;
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append("/");
        localObject2 = G;
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        localObject3[0] = localObject1;
        return;
      }
      localObject3 = c;
      paramInt = ((p)localObject3).g();
      bool1 = c(paramInt);
      if (bool1)
      {
        c("unblock");
        return;
      }
      bool1 = O;
      if ((bool1) && (paramInt == 0)) {
        S = bool3;
      }
      localObject3 = am;
      paramInt = ((com.truecaller.messaging.j)localObject3).a();
      if (paramInt != 0) {
        T = bool3;
      }
      localObject3 = ae;
      localObject1 = "afterCallWarnFriends";
      paramInt = ((com.truecaller.i.c)localObject3).a((String)localObject1, 0);
      int i2 = 3;
      if (paramInt < i2)
      {
        paramInt = 1;
      }
      else
      {
        paramInt = 0;
        localObject3 = null;
      }
      if (paramInt != 0)
      {
        localObject3 = "mounted";
        localObject1 = Environment.getExternalStorageState();
        paramInt = ((String)localObject3).equals(localObject1);
        if (paramInt != 0) {
          bool2 = true;
        }
      }
      Q = bool2;
      bb.a(d, "afterCall", "blocked");
      a("blockQuery", null);
      localf = a;
      localObject3 = G;
      localObject1 = H;
      localObject3 = (String)am.e((CharSequence)localObject3, (CharSequence)localObject1);
      Contact localContact = D;
      boolean bool4 = W;
      localObject4 = Collections.singletonList(localObject3);
      localf.a((List)localObject4, "OTHER", localContact, "afterCall", null, bool4);
      return;
    case 2: 
      paramInt = P;
      if (paramInt == 0)
      {
        localObject3 = new String[bool3];
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("Number was not valid for caller: ");
        localObject2 = H;
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append("/");
        localObject2 = G;
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        localObject3[0] = localObject1;
        return;
      }
      c("notspam");
      return;
    case 1: 
      localObject3 = ah;
      c = null;
      paramInt = N;
      if (paramInt != 0)
      {
        if (paramInt != 0)
        {
          localObject3 = d;
          localObject2 = "editedContact";
          bb.a((com.truecaller.androidactors.f)localObject3, "afterCall", (String)localObject2);
          localObject3 = i.bw();
          localObject1 = new String[] { "android.permission.WRITE_CONTACTS" };
          paramInt = ((l)localObject3).a((String[])localObject1);
          if (paramInt != 0)
          {
            com.truecaller.common.h.n.a(D.a(bool3), this);
            com.truecaller.common.h.n.a(D.a(false), this);
            localObject3 = D;
            localObject3 = com.truecaller.util.d.a(this, (Contact)localObject3);
            localObject1 = this;
            localObject1 = (Activity)this;
            i1 = 21;
            com.truecaller.common.h.o.a((Activity)localObject1, (Intent)localObject3, i1);
          }
        }
      }
      else
      {
        localObject3 = "save";
        a((String)localObject3, null);
        o();
      }
      paramInt = N;
      if (paramInt != 0) {
        localObject3 = "edit";
      } else {
        localObject3 = "save";
      }
      localObject1 = ab;
      localObject2 = new com/truecaller/analytics/e$a;
      ((e.a)localObject2).<init>("ViewAction");
      localObject3 = ((e.a)localObject2).a("Action", (String)localObject3).a("Context", "afterCall").a();
      ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject3);
      return;
    case 0: 
      ah.c = null;
      a("call", null);
      localObject3 = H;
      if (localObject3 == null) {
        localObject3 = G;
      }
      if (localObject3 != null)
      {
        localObject2 = new com/truecaller/calling/initiate_call/b$a$a;
        ((b.a.a)localObject2).<init>((String)localObject3, "afterCall");
        localObject3 = ((b.a.a)localObject2).a((String)localObject1);
        localObject1 = au;
        localObject3 = ((b.a.a)localObject3).a();
        ((com.truecaller.calling.initiate_call.b)localObject1).a((b.a)localObject3);
        return;
      }
      break;
    }
  }
  
  public final void a(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    bb.a(d, "afterCall", "positiveButton");
  }
  
  public final void a(FeedbackItemView paramFeedbackItemView)
  {
    ac = paramFeedbackItemView;
  }
  
  final void a(String paramString)
  {
    boolean bool = g;
    if (!bool)
    {
      bool = TextUtils.isEmpty(paramString);
      if (bool) {
        paramString = D.t();
      }
      com.truecaller.ui.dialogs.o localo = new com/truecaller/ui/dialogs/o;
      String str = D.q();
      localo.<init>(this, paramString, str, null);
      paramString = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$m6hApptDfSLPTiExIdexyvUTPmY;
      paramString.<init>(this);
      localo.setOnDismissListener(paramString);
      localo.show();
    }
  }
  
  public final void ac_()
  {
    Toast.makeText(this, 2131886537, 0).show();
  }
  
  public final void b(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    bb.a(d, "afterCall", "negativeButton");
  }
  
  public final boolean b()
  {
    b("NativeBackButton");
    return true;
  }
  
  public final void c()
  {
    super.c();
    ah.d = true;
  }
  
  public final void c(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    paramFeedbackItem = ad;
    if (paramFeedbackItem != null)
    {
      boolean bool = g;
      if (!bool)
      {
        bool = isFinishing();
        if (!bool)
        {
          paramFeedbackItem = (FeedbackItemView)ad.f;
          if (paramFeedbackItem != null)
          {
            bool = paramFeedbackItem.c();
            if (bool) {}
          }
          else
          {
            paramFeedbackItem = ad;
            paramFeedbackItem.b();
            finish();
          }
        }
      }
    }
  }
  
  public final void d()
  {
    boolean bool1 = g;
    if (bool1) {
      return;
    }
    bool1 = az;
    if (!bool1) {
      l();
    }
    Object localObject1 = aq;
    Object localObject2 = ar;
    ((Handler)localObject1).removeCallbacks((Runnable)localObject2);
    localObject1 = aq;
    localObject2 = ar;
    long l1 = 500L;
    ((Handler)localObject1).postDelayed((Runnable)localObject2, l1);
    localObject1 = r;
    localObject2 = H;
    boolean bool3 = true;
    Object localObject3 = null;
    if (localObject2 == null)
    {
      localObject2 = G;
      if (localObject2 == null)
      {
        bool4 = false;
        localObject2 = null;
        break label104;
      }
    }
    boolean bool4 = true;
    label104:
    ((AfterCallButtons)localObject1).setCallButtonAvailable(bool4);
    localObject1 = (com.truecaller.common.b.a)getApplication();
    bool1 = ((com.truecaller.common.b.a)localObject1).p();
    localObject2 = D;
    bool4 = ((Contact)localObject2).ac();
    if ((bool4) && (bool1))
    {
      bool1 = true;
    }
    else
    {
      bool1 = false;
      localObject1 = null;
    }
    W = bool1;
    localObject1 = D;
    int i2 = 32;
    bool1 = ((Contact)localObject1).a(i2);
    if (bool1)
    {
      bool1 = p();
      if (!bool1)
      {
        bool1 = true;
        break label195;
      }
    }
    bool1 = false;
    localObject1 = null;
    label195:
    Object localObject4 = x;
    if (bool1) {
      i4 = 2131230835;
    } else {
      i4 = com.truecaller.utils.ui.b.d(this, 2130968624);
    }
    ((View)localObject4).setBackgroundResource(i4);
    localObject4 = y;
    int i4 = 8;
    int i5;
    if (bool1)
    {
      i5 = 0;
      localObject5 = null;
    }
    else
    {
      i5 = 8;
    }
    ((ShineView)localObject4).setVisibility(i5);
    int i7 = 2131363192;
    localObject4 = findViewById(i7);
    if (bool1)
    {
      i5 = 0;
      localObject5 = null;
    }
    else
    {
      i5 = 8;
    }
    ((View)localObject4).setVisibility(i5);
    if (bool1)
    {
      localObject4 = p;
      i5 = 2131100588;
      ((ViewGroup)localObject4).setBackgroundResource(i5);
    }
    else
    {
      localObject4 = p;
      i5 = com.truecaller.utils.ui.b.a(this, 2130968621);
      com.truecaller.utils.ui.b.b((View)localObject4, i5);
    }
    AfterCallHeaderView localAfterCallHeaderView = n;
    Contact localContact = D;
    HistoryEvent localHistoryEvent = E;
    boolean bool9 = N;
    boolean bool10 = p();
    int i8 = F;
    boolean bool11 = W;
    localAfterCallHeaderView.a(localContact, localHistoryEvent, bool9, bool10, i8, bool11);
    localObject4 = r;
    Object localObject5 = D;
    boolean bool5 = ((Contact)localObject5).a(i2);
    boolean bool7 = p();
    ((AfterCallButtons)localObject4).a(bool5, bool7);
    localObject2 = D;
    if (localObject2 != null)
    {
      localObject4 = aa;
      localObject5 = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$ci4_7UXelTzhrGxhjtq80vzFvRE;
      ((-..Lambda.AfterCallActivity.ci4_7UXelTzhrGxhjtq80vzFvRE)localObject5).<init>(this);
      ((ai)localObject4).a((Contact)localObject2, (com.truecaller.voip.e)localObject5);
    }
    localObject2 = i.bw();
    localObject4 = new String[] { "android.permission.WRITE_CONTACTS" };
    bool5 = ((l)localObject2).a((String[])localObject4);
    localObject4 = r;
    ((AfterCallButtons)localObject4).setPhoneBookAvailable(bool5);
    if (bool5)
    {
      bool5 = N;
      int i6;
      if (bool5)
      {
        localObject2 = r;
        i7 = 2131234069;
        i6 = 2131886097;
        ((AfterCallButtons)localObject2).a(i7, i6);
      }
      else
      {
        localObject2 = r;
        i7 = 2131234478;
        i6 = 2131886111;
        ((AfterCallButtons)localObject2).a(i7, i6);
      }
    }
    bool5 = N;
    if (bool5)
    {
      localObject1 = r;
      ((AfterCallButtons)localObject1).setBlockButtonAvailable(false);
    }
    else
    {
      localObject2 = c;
      bool5 = ((p)localObject2).g();
      bool5 = c(bool5);
      int i1;
      if (bool5)
      {
        i1 = com.truecaller.utils.ui.b.a(this, 2130969585);
        localObject2 = r;
        i7 = 2131886115;
        ((AfterCallButtons)localObject2).a(i1, i1, i7, bool3);
      }
      else
      {
        int i3;
        if (i1 != 0)
        {
          i1 = android.support.v4.content.b.c(this, 2131100374);
          i3 = android.support.v4.content.b.c(this, 2131100383);
          i7 = 1;
        }
        else
        {
          i1 = 2130968620;
          i3 = com.truecaller.utils.ui.b.a(this, i1);
          i1 = com.truecaller.utils.ui.b.a(this, i1);
          i7 = 0;
          localObject4 = null;
          int i9 = i3;
          i3 = i1;
          i1 = i9;
        }
        localObject5 = r;
        int i10 = 2131886094;
        ((AfterCallButtons)localObject5).a(i1, i3, i10, i7);
      }
      localObject1 = r;
      ((AfterCallButtons)localObject1).setBlockButtonAvailable(bool3);
    }
    localObject1 = r;
    boolean bool6 = r();
    ((AfterCallButtons)localObject1).setSpam(bool6);
    localObject1 = r;
    bool6 = p();
    ((AfterCallButtons)localObject1).setSaveToPhoneBookButton(bool6);
    localObject1 = E.m;
    localObject2 = w;
    if (localObject2 != null)
    {
      if (localObject1 != null) {
        i4 = 0;
      }
      ((CallRecordingFloatingButton)localObject2).setVisibility(i4);
      if (localObject1 != null)
      {
        w.setCallRecording((CallRecording)localObject1);
        localObject2 = w;
        localObject4 = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$RnkobC6Zjtkp6LPCYEwQGiFohME;
        ((-..Lambda.AfterCallActivity.RnkobC6Zjtkp6LPCYEwQGiFohME)localObject4).<init>(this, (CallRecording)localObject1);
        ((CallRecordingFloatingButton)localObject2).setOnClickListener((View.OnClickListener)localObject4);
      }
    }
    i();
    localObject1 = D;
    boolean bool2 = com.truecaller.data.access.c.b((Contact)localObject1);
    if (bool2) {
      localObject1 = D.getId();
    } else {
      localObject1 = D.k();
    }
    localObject2 = i.bk();
    bool6 = ((com.truecaller.whoviewedme.w)localObject2).k();
    if ((bool6) && (localObject1 != null))
    {
      long l2 = ((Long)localObject1).longValue();
      localObject4 = D;
      boolean bool8 = ((Contact)localObject4).Z();
      i4 = K;
      ProfileViewService.a(this, l2, bool8, i4);
    }
    localObject1 = TrueApp.y();
    bool2 = ((TrueApp)localObject1).isTcPayEnabled();
    if (bool2)
    {
      localObject1 = D;
      bool2 = ((Contact)localObject1).Z();
      if (bool2)
      {
        localObject1 = G;
        if (localObject1 != null)
        {
          localObject1 = r;
          ((AfterCallButtons)localObject1).setPayButtonAvailable(bool3);
          break label1024;
        }
      }
    }
    localObject1 = r;
    ((AfterCallButtons)localObject1).setPayButtonAvailable(false);
    label1024:
    localObject1 = E.b;
    if (localObject1 != null)
    {
      localObject2 = aw;
      bool3 = p();
      localObject3 = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$X7oANySkn7thwvKu0tgSeFObVaI;
      ((-..Lambda.AfterCallActivity.X7oANySkn7thwvKu0tgSeFObVaI)localObject3).<init>(this);
      ((a)localObject2).a((String)localObject1, bool3, (a.a)localObject3);
    }
    localObject1 = ax;
    localObject2 = D;
    bool3 = p();
    localObject3 = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$54zwBE5-a0lkglAepdA4JOlHoIA;
    ((-..Lambda.AfterCallActivity.54zwBE5-a0lkglAepdA4JOlHoIA)localObject3).<init>(this);
    ((d)localObject1).a((Contact)localObject2, bool3, (c)localObject3);
  }
  
  public final void d_(boolean paramBoolean) {}
  
  public final void e() {}
  
  public void finish()
  {
    ValueAnimator localValueAnimator1 = B;
    boolean bool = localValueAnimator1.isRunning();
    if (!bool)
    {
      int i1 = s.getTop();
      float f1 = i1;
      float f2 = 1.5F;
      f1 *= f2;
      ValueAnimator localValueAnimator2 = B;
      int i2 = 2;
      float[] arrayOfFloat = new float[i2];
      arrayOfFloat[0] = 0.0F;
      int i3 = 1;
      arrayOfFloat[i3] = f1;
      localValueAnimator2.setFloatValues(arrayOfFloat);
      localValueAnimator1 = B;
      localValueAnimator1.start();
    }
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int i1 = -1;
    int i2 = 31;
    Object localObject1;
    if (paramInt1 == i2)
    {
      if ((paramInt2 == i1) && (paramIntent != null))
      {
        localObject1 = (Contact)paramIntent.getParcelableExtra("contact");
        if (localObject1 != null)
        {
          D = ((Contact)localObject1);
          d();
        }
      }
    }
    else
    {
      i2 = 41;
      if (paramInt1 == i2)
      {
        boolean bool = Q;
        if (bool)
        {
          Q = false;
          localObject1 = J;
          a((String)localObject1);
          return;
        }
      }
      Object localObject2 = ad;
      if (localObject2 != null)
      {
        localObject1 = (FeedbackItemView)f;
        if (localObject1 != null)
        {
          paramInt1 = ((FeedbackItemView)localObject1).c();
          if (paramInt1 == 0) {}
        }
        else
        {
          localObject1 = ad;
          ((com.truecaller.ui.dialogs.c)localObject1).b();
          finish();
        }
        return;
      }
      int i3 = 21;
      if (paramInt1 == i3)
      {
        if (paramIntent != null)
        {
          localObject1 = paramIntent.getData();
          if (localObject1 != null)
          {
            if (paramInt2 != i1) {
              break label252;
            }
            localObject1 = i.bw();
            localObject2 = new String[] { "android.permission.WRITE_CONTACTS" };
            paramInt1 = ((l)localObject1).a((String[])localObject2);
            if (paramInt1 == 0) {
              break label252;
            }
            localObject1 = paramIntent.getData();
            paramIntent = new com/truecaller/calling/after_call/AfterCallActivity$6;
            localObject2 = D;
            paramIntent.<init>(this, (Contact)localObject2, (Uri)localObject1);
            break label252;
          }
        }
        SyncPhoneBookService.a(this);
        label252:
        if (paramInt2 == i1)
        {
          localObject1 = an;
          if (localObject1 != null)
          {
            Object localObject3 = D;
            paramInt1 = ((ReferralManager)localObject1).b((Contact)localObject3);
            if (paramInt1 != 0)
            {
              localObject1 = an;
              localObject3 = D;
              paramInt1 = ((ReferralManager)localObject1).a((Contact)localObject3);
              if (paramInt1 == 0)
              {
                localObject1 = an;
                localObject3 = ReferralManager.ReferralLaunchContext.AFTER_CALL_SAVE_CONTACT;
                paramIntent = D;
                ((ReferralManager)localObject1).a((ReferralManager.ReferralLaunchContext)localObject3, paramIntent);
              }
            }
          }
        }
      }
    }
  }
  
  public void onClick(View paramView)
  {
    int i1 = paramView.getId();
    switch (i1)
    {
    default: 
      break;
    case 2131364653: 
      String str = "button";
      a("tag", str);
      paramView = D;
      int i2 = K;
      paramView = TagPickActivity.a(this, paramView, 1, i2);
      int i3 = 31;
      startActivityForResult(paramView, i3);
      break;
    case 2131363248: 
      a("details", "header");
      h();
      return;
    case 2131362594: 
      b("EmptySpace");
      return;
    case 2131362507: 
      b("CloseButton");
      return;
    case 2131362343: 
      a("details", "button");
      h();
      return;
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    paramBundle = getTheme();
    Object localObject1 = ThemeManager.a();
    int i1 = resId;
    int i2 = 0;
    Object localObject2 = null;
    paramBundle.applyStyle(i1, false);
    paramBundle = getIntent();
    if (paramBundle != null)
    {
      localObject1 = "widgetClick";
      boolean bool1 = paramBundle.hasExtra((String)localObject1);
      if (bool1)
      {
        paramBundle = (com.truecaller.common.b.a)getApplicationContext();
        bool1 = paramBundle.p();
        if (bool1)
        {
          paramBundle = "wizard_FullyCompleted";
          bool1 = com.truecaller.common.b.e.a(paramBundle, false);
          if (bool1) {}
        }
        else
        {
          com.truecaller.wizard.b.c.a(this, WizardActivity.class, "widget");
          super.finish();
          return;
        }
      }
    }
    int i3 = 2131558433;
    setContentView(i3);
    paramBundle = TrueApp.y().a();
    i = paramBundle;
    paramBundle = i.c();
    ab = paramBundle;
    paramBundle = i.bF();
    k = paramBundle;
    paramBundle = i.ai();
    l = paramBundle;
    paramBundle = i.cc();
    aa = paramBundle;
    paramBundle = com.truecaller.referral.w.a(this, "ReferralManagerImpl");
    an = paramBundle;
    paramBundle = an;
    i1 = 1;
    if (paramBundle != null)
    {
      localObject3 = ReferralManager.ReferralLaunchContext.AFTER_CALL;
      bool2 = paramBundle.c((ReferralManager.ReferralLaunchContext)localObject3);
      if (bool2)
      {
        bool2 = true;
        break label248;
      }
    }
    boolean bool2 = false;
    paramBundle = null;
    label248:
    ao = bool2;
    paramBundle = (AfterCallHeaderView)findViewById(2131363248);
    n = paramBundle;
    paramBundle = (ViewGroup)findViewById(2131362003);
    p = paramBundle;
    paramBundle = (ViewGroup)findViewById(2131362006);
    o = paramBundle;
    paramBundle = (ViewGroup)findViewById(2131362005);
    q = paramBundle;
    paramBundle = (AfterCallButtons)findViewById(2131362007);
    r = paramBundle;
    paramBundle = findViewById(2131363885);
    s = paramBundle;
    paramBundle = findViewById(2131362594);
    t = paramBundle;
    paramBundle = findViewById(2131362071);
    u = paramBundle;
    paramBundle = (CallRecordingFloatingButton)findViewById(2131362363);
    w = paramBundle;
    paramBundle = findViewById(2131362008);
    x = paramBundle;
    paramBundle = (ShineView)findViewById(2131363196);
    y = paramBundle;
    y.setLifecycleOwner(this);
    paramBundle = getWindow();
    Object localObject3 = C;
    paramBundle.setBackgroundDrawable((Drawable)localObject3);
    int i4 = 2;
    localObject3 = new float[i4];
    Object tmp454_452 = localObject3;
    tmp454_452[0] = 0.0F;
    tmp454_452[1] = 1.0F;
    localObject3 = ValueAnimator.ofFloat((float[])localObject3);
    A = ((ValueAnimator)localObject3);
    localObject3 = A;
    int i5 = getResources().getInteger(17694722);
    long l1 = i5;
    ((ValueAnimator)localObject3).setDuration(l1);
    localObject3 = A;
    Object localObject4 = new android/view/animation/DecelerateInterpolator;
    int i6 = 1077936128;
    float f = 3.0F;
    ((DecelerateInterpolator)localObject4).<init>(f);
    ((ValueAnimator)localObject3).setInterpolator((TimeInterpolator)localObject4);
    localObject3 = A;
    localObject4 = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$cTgRIkRUHpbebIpFu1m5mpfFvYo;
    ((-..Lambda.AfterCallActivity.cTgRIkRUHpbebIpFu1m5mpfFvYo)localObject4).<init>(this);
    ((ValueAnimator)localObject3).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject4);
    localObject3 = A;
    localObject4 = new com/truecaller/calling/after_call/AfterCallActivity$10;
    ((AfterCallActivity.10)localObject4).<init>(this);
    ((ValueAnimator)localObject3).addListener((Animator.AnimatorListener)localObject4);
    paramBundle = new float[i4];
    Bundle tmp596_595 = paramBundle;
    tmp596_595[0] = 0.0F;
    tmp596_595[1] = 1.0F;
    paramBundle = ValueAnimator.ofFloat(paramBundle);
    B = paramBundle;
    paramBundle = B;
    long l2 = 300L;
    paramBundle.setDuration(l2);
    paramBundle = B;
    localObject3 = new android/view/animation/AccelerateInterpolator;
    ((AccelerateInterpolator)localObject3).<init>(f);
    paramBundle.setInterpolator((TimeInterpolator)localObject3);
    paramBundle = B;
    localObject3 = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$f0spCMFl8A5dy_SflNN8xSVth98;
    ((-..Lambda.AfterCallActivity.f0spCMFl8A5dy_SflNN8xSVth98)localObject3).<init>(this);
    paramBundle.addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject3);
    paramBundle = B;
    localObject3 = new com/truecaller/calling/after_call/AfterCallActivity$11;
    ((AfterCallActivity.11)localObject3).<init>(this);
    paramBundle.addListener((Animator.AnimatorListener)localObject3);
    paramBundle = i.aH();
    av = paramBundle;
    paramBundle = new com/truecaller/calling/after_call/h;
    localObject3 = new com/truecaller/utils/b;
    ((com.truecaller.utils.b)localObject3).<init>();
    localObject4 = i.aJ();
    Object localObject5 = i.I();
    paramBundle.<init>((com.truecaller.utils.a)localObject3, (af)localObject4, (com.truecaller.common.g.a)localObject5);
    af = paramBundle;
    paramBundle = i.P();
    b = paramBundle;
    paramBundle = i.f();
    d = paramBundle;
    paramBundle = i.D();
    ae = paramBundle;
    paramBundle = i.bg();
    as = paramBundle;
    paramBundle = i.aF();
    al = paramBundle;
    paramBundle = i.v();
    at = paramBundle;
    paramBundle = i.bM();
    au = paramBundle;
    paramBundle = i.bS();
    aw = paramBundle;
    paramBundle = i.bT();
    ax = paramBundle;
    paramBundle = i.bV();
    am = paramBundle;
    paramBundle = new com/truecaller/calling/after_call/AfterCallActivity$5;
    localObject3 = b;
    paramBundle.<init>(this, this, (FilterManager)localObject3);
    a = paramBundle;
    paramBundle = ((TrueApp)getApplicationContext()).a().R();
    c = paramBundle;
    boolean bool3 = af.a();
    j = bool3;
    paramBundle = getIntent();
    bool3 = b(paramBundle);
    if (bool3)
    {
      paramBundle = s;
      localObject3 = com.truecaller.util.b.at.a(this);
      localObject4 = ((com.truecaller.util.b.j)localObject3).a(this);
      i6 = 2131363194;
      f = 1.834619E38F;
      localObject5 = paramBundle.findViewById(i6);
      int i7 = 2131363886;
      ImageView localImageView = (ImageView)paramBundle.findViewById(i7);
      Contact localContact = D;
      boolean bool4 = localContact.a(32);
      int i8 = 8;
      if (bool4)
      {
        bool4 = p();
        if (!bool4)
        {
          boolean bool5 = localObject3 instanceof as;
          if (bool5)
          {
            ((View)localObject5).setVisibility(0);
            localImageView.setVisibility(i8);
            i2 = 2131363193;
            localObject2 = (ImageView)((View)localObject5).findViewById(i2);
            bool5 = co.c(this);
            int i9;
            if (bool5) {
              i9 = 2131234628;
            } else {
              i9 = 2131234627;
            }
            com.truecaller.util.at.a((ImageView)localObject2, i9);
            localObject2 = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$oFa_uSkK3FShAaZz4xYZKmut5Yk;
            ((-..Lambda.AfterCallActivity.oFa_uSkK3FShAaZz4xYZKmut5Yk)localObject2).<init>(this);
            ((View)localObject5).setOnClickListener((View.OnClickListener)localObject2);
            break label1202;
          }
        }
      }
      ((View)localObject5).setVisibility(i8);
      localImageView.setVisibility(0);
      i2 = d;
      com.truecaller.util.at.a(localImageView, i2);
      label1202:
      boolean bool6 = ((j.b)localObject4).a();
      com.truecaller.util.at.b(paramBundle, 2131363968, bool6);
      i2 = 2131364985;
      bool6 = ((j.b)localObject4).a();
      com.truecaller.util.at.b(paramBundle, i2, bool6);
      d();
      b(i1);
    }
    else
    {
      finish();
    }
    t.setOnClickListener(this);
    s.setOnClickListener(this);
    n.setOnClickListener(this);
    r.setOnButtonClickListener(this);
    findViewById(2131362507).setOnClickListener(this);
    findViewById(2131362343).setOnClickListener(this);
    n.setOnTagClickListener(this);
    paramBundle = n;
    localObject1 = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$XgPT8qdEfnGhnCYpB-RIUokz90s;
    ((-..Lambda.AfterCallActivity.XgPT8qdEfnGhnCYpB-RIUokz90s)localObject1).<init>(this);
    paramBundle.setOnSuggestNameClickListener((View.OnClickListener)localObject1);
    paramBundle = findViewById(2131362004);
    v = paramBundle;
    paramBundle = v;
    localObject1 = new com/truecaller/calling/after_call/-$$Lambda$AfterCallActivity$omB2M_7t6QYr1igp_0slyB42R1Y;
    ((-..Lambda.AfterCallActivity.omB2M_7t6QYr1igp_0slyB42R1Y)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    Object localObject1 = z;
    if (localObject1 != null)
    {
      localObject2 = b;
      if (localObject2 != null)
      {
        b.a();
        localObject2 = null;
        b = null;
      }
    }
    localObject1 = aq;
    Object localObject2 = ar;
    ((Handler)localObject1).removeCallbacks((Runnable)localObject2);
  }
  
  public void onPause()
  {
    super.onPause();
    n();
  }
  
  public void onResume()
  {
    super.onResume();
    n();
    Object localObject1 = new android/content/IntentFilter;
    ((IntentFilter)localObject1).<init>();
    Object localObject2 = m;
    int i1 = localObject2.length;
    int i2 = 0;
    com.truecaller.ui.f localf = null;
    Object localObject3;
    while (i2 < i1)
    {
      localObject3 = localObject2[i2];
      ((IntentFilter)localObject1).addAction((String)localObject3);
      i2 += 1;
    }
    localObject2 = new com/truecaller/calling/after_call/AfterCallActivity$c;
    ((AfterCallActivity.c)localObject2).<init>(this, (byte)0);
    ag = ((AfterCallActivity.c)localObject2);
    registerReceiver((BroadcastReceiver)localObject2, (IntentFilter)localObject1);
    localObject1 = ac;
    if (localObject1 != null)
    {
      ((FeedbackItemView)localObject1).b();
      localObject1 = null;
      ac = null;
    }
    localObject1 = getIntent();
    localObject2 = AfterCallActivity.AfterCallActionType.STORE.name();
    boolean bool1;
    if (localObject1 != null)
    {
      bool1 = ((Intent)localObject1).hasExtra((String)localObject2);
      if (bool1)
      {
        bool1 = ((Intent)localObject1).getBooleanExtra((String)localObject2, false);
        if (bool1) {
          o();
        }
        ((Intent)localObject1).removeExtra((String)localObject2);
      }
    }
    localObject1 = getIntent();
    localObject2 = AfterCallActivity.AfterCallActionType.BLOCK.name();
    if (localObject1 != null)
    {
      bool1 = ((Intent)localObject1).hasExtra((String)localObject2);
      if (bool1)
      {
        bool1 = ((Intent)localObject1).getBooleanExtra((String)localObject2, false);
        if (bool1)
        {
          localf = a;
          String str1 = G;
          localObject3 = H;
          str1 = (String)am.e(str1, (CharSequence)localObject3);
          String str2 = "OTHER";
          String str3 = D.t();
          String str4 = "afterCall";
          TruecallerContract.Filters.WildCardType localWildCardType = TruecallerContract.Filters.WildCardType.NONE;
          TruecallerContract.Filters.EntityType localEntityType = TruecallerContract.Filters.EntityType.UNKNOWN;
          localObject3 = Collections.singletonList(str1);
          boolean bool2 = true;
          localf.a((List)localObject3, str2, str3, str4, bool2, localWildCardType, localEntityType);
        }
        ((Intent)localObject1).removeExtra((String)localObject2);
      }
    }
    new AfterCallActivity.d(this, (byte)0);
  }
  
  public void onStart()
  {
    super.onStart();
    Object localObject1 = z;
    if (localObject1 != null)
    {
      localObject1 = ((q)localObject1).getParent();
      if (localObject1 == null)
      {
        localObject1 = q;
        localObject2 = z;
        ((ViewGroup)localObject1).addView((View)localObject2);
      }
      localObject1 = z;
      localObject2 = b;
      if (localObject2 != null)
      {
        localObject1 = b;
        localObject2 = a;
        if (localObject2 != null)
        {
          localObject2 = a.a;
          ((zzly)localObject2).resume();
        }
        localObject2 = b;
        boolean bool1 = localObject2 instanceof PublisherAdView;
        if (bool1)
        {
          localObject1 = b).a;
          ((zzly)localObject1).resume();
        }
      }
      localObject1 = z;
      localObject2 = b;
      if (localObject2 != null)
      {
        localObject1 = b;
        ((q.b)localObject1).b();
      }
    }
    localObject1 = t.getViewTreeObserver();
    Object localObject2 = new com/truecaller/calling/after_call/AfterCallActivity$2;
    ((AfterCallActivity.2)localObject2).<init>(this);
    ((ViewTreeObserver)localObject1).addOnPreDrawListener((ViewTreeObserver.OnPreDrawListener)localObject2);
    localObject1 = n;
    long l1 = a;
    long l2 = -1;
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      localObject1 = b;
      ((AfterCallHeaderView.a)localObject1).run();
    }
    localObject1 = ak;
    Object localObject3;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/calling/after_call/AfterCallActivity$b;
      ((AfterCallActivity.b)localObject1).<init>(this);
      ak = ((ContentObserver)localObject1);
      localObject1 = getContentResolver();
      localObject2 = TruecallerContract.b;
      boolean bool3 = true;
      localObject3 = ak;
      ((ContentResolver)localObject1).registerContentObserver((Uri)localObject2, bool3, (ContentObserver)localObject3);
    }
    localObject1 = ah;
    if (localObject1 != null)
    {
      boolean bool4 = d;
      if (!bool4) {}
    }
    else
    {
      localObject1 = new com/truecaller/calling/after_call/AfterCallActivity$e;
      int i1 = F;
      int i2 = K;
      localObject3 = ab;
      ((AfterCallActivity.e)localObject1).<init>(i1, i2, (com.truecaller.analytics.b)localObject3);
      ah = ((AfterCallActivity.e)localObject1);
    }
  }
  
  public void onStop()
  {
    Object localObject1 = n;
    Object localObject2 = b;
    ((AfterCallHeaderView)localObject1).removeCallbacks((Runnable)localObject2);
    super.onStop();
    localObject1 = z;
    if (localObject1 != null)
    {
      localObject2 = b;
      if (localObject2 != null)
      {
        localObject1 = b;
        localObject2 = a;
        if (localObject2 != null)
        {
          localObject2 = a.a;
          ((zzly)localObject2).pause();
        }
        localObject2 = b;
        boolean bool1 = localObject2 instanceof PublisherAdView;
        if (bool1)
        {
          localObject1 = b).a;
          ((zzly)localObject1).pause();
        }
      }
      localObject1 = q;
      localObject2 = z;
      ((ViewGroup)localObject1).removeView((View)localObject2);
      localObject1 = v;
      int i1 = 8;
      ((View)localObject1).setVisibility(i1);
    }
    localObject1 = ak;
    if (localObject1 != null)
    {
      localObject1 = getContentResolver();
      localObject2 = ak;
      ((ContentResolver)localObject1).unregisterContentObserver((ContentObserver)localObject2);
      localObject1 = null;
      ak = null;
    }
    localObject1 = ah;
    boolean bool2 = d;
    if (!bool2)
    {
      localObject2 = b;
      Object localObject3 = new com/truecaller/analytics/bc;
      String str1 = "afterCall";
      String str2 = a;
      ((bc)localObject3).<init>(str1, str2);
      ((com.truecaller.analytics.b)localObject2).a((com.truecaller.analytics.e)localObject3);
      localObject2 = c;
      if (localObject2 != null)
      {
        localObject2 = b;
        localObject3 = new com/truecaller/analytics/e$a;
        ((e.a)localObject3).<init>("AFTERCALL_Dismissed");
        str1 = "Dismiss_Type";
        str2 = c;
        localObject3 = ((e.a)localObject3).a(str1, str2).a();
        ((com.truecaller.analytics.b)localObject2).a((com.truecaller.analytics.e)localObject3);
      }
      bool2 = true;
      d = bool2;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */