package com.truecaller.calling.after_call;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;

final class AfterCallActivity$2
  implements ViewTreeObserver.OnPreDrawListener
{
  AfterCallActivity$2(AfterCallActivity paramAfterCallActivity) {}
  
  public final boolean onPreDraw()
  {
    AfterCallActivity.e(a, false);
    float f = AfterCallActivity.x(a).getTop() * 1.5F;
    AfterCallActivity.y(a).setTranslationY(f);
    ValueAnimator localValueAnimator = AfterCallActivity.z(a);
    float[] arrayOfFloat = new float[2];
    arrayOfFloat[0] = f;
    arrayOfFloat[1] = 0.0F;
    localValueAnimator.setFloatValues(arrayOfFloat);
    AfterCallActivity.z(a).start();
    AfterCallActivity.y(a).getViewTreeObserver().removeOnPreDrawListener(this);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */