package com.truecaller.calling.after_call;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class AfterCallActivity$c
  extends BroadcastReceiver
{
  private AfterCallActivity$c(AfterCallActivity paramAfterCallActivity) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = a;
    paramIntent = paramIntent.getAction();
    AfterCallActivity.b(paramContext, paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */