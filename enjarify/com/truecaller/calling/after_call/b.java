package com.truecaller.calling.after_call;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.data.entity.Contact;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class b
{
  final Context a;
  final com.truecaller.analytics.b b;
  private final f c;
  
  public b(Context paramContext, com.truecaller.analytics.b paramb, f paramf)
  {
    a = paramContext;
    b = paramb;
    c = paramf;
  }
  
  public final void a(Contact paramContact)
  {
    k.b(paramContact, "contact");
    ag localag = (ag)bg.a;
    f localf = c;
    Object localObject = new com/truecaller/calling/after_call/b$a;
    ((b.a)localObject).<init>(this, paramContact, null);
    localObject = (m)localObject;
    e.b(localag, localf, (m)localObject, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */