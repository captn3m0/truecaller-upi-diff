package com.truecaller.calling.after_call;

import com.truecaller.common.h.an;
import java.util.concurrent.TimeUnit;

public final class e$a
  implements com.truecaller.voip.e
{
  e$a(e parame, boolean paramBoolean, c paramc) {}
  
  public final void onVoipAvailabilityLoaded(boolean paramBoolean)
  {
    String str = null;
    if (paramBoolean)
    {
      paramBoolean = b;
      if (!paramBoolean)
      {
        Object localObject1 = a;
        Object localObject2 = ((e)localObject1).a();
        if (localObject2 != null)
        {
          long l1 = ((Long)localObject2).longValue();
          localObject2 = b;
          int i = 5;
          int j = ((com.truecaller.i.e)localObject2).a("feature_voip_promo_after_call_period_days", i);
          an localan = a;
          long l2 = j;
          TimeUnit localTimeUnit = TimeUnit.DAYS;
          paramBoolean = localan.a(l1, l2, localTimeUnit);
        }
        else
        {
          paramBoolean = false;
          localObject1 = null;
        }
        if (!paramBoolean)
        {
          localObject1 = a.a();
          if (localObject1 == null)
          {
            localObject1 = a.b;
            str = "feature_voip_promo_after_call_first_timestamp";
            long l3 = System.currentTimeMillis();
            ((com.truecaller.i.e)localObject1).b(str, l3);
          }
          c.shouldPromoteVoip(true);
          return;
        }
      }
    }
    c.shouldPromoteVoip(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */