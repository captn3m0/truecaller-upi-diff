package com.truecaller.calling.after_call;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import com.truecaller.common.h.am;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.g;
import com.truecaller.old.a.a;
import com.truecaller.old.a.b;
import java.util.Collection;
import java.util.Iterator;

final class AfterCallActivity$d
  extends a
{
  private AfterCallActivity$d(AfterCallActivity paramAfterCallActivity) {}
  
  public final void a(Object paramObject)
  {
    if (paramObject != null)
    {
      localObject = a;
      paramObject = (Contact)paramObject;
      AfterCallActivity.a((AfterCallActivity)localObject, (Contact)paramObject);
    }
    AfterCallActivity.B(a);
    a.d();
    paramObject = a;
    boolean bool1 = AfterCallActivity.C((AfterCallActivity)paramObject);
    if (!bool1)
    {
      AfterCallActivity.D(a);
      return;
    }
    AfterCallActivity.c(a);
    paramObject = AfterCallActivity.f(a);
    Object localObject = AfterCallActivity.e(a);
    paramObject = (String)am.e((CharSequence)paramObject, (CharSequence)localObject);
    localObject = AfterCallActivity.o(a);
    if (localObject != null)
    {
      bool2 = TextUtils.isEmpty((CharSequence)paramObject);
      if (!bool2)
      {
        localObject = a;
        bool2 = AfterCallActivity.E((AfterCallActivity)localObject);
        if (!bool2)
        {
          AfterCallActivity.F(a);
          Ga).a = true;
          localObject = new com/truecaller/calling/after_call/AfterCallActivity$f;
          AfterCallActivity localAfterCallActivity = a;
          Contact localContact = AfterCallActivity.o(localAfterCallActivity);
          ((AfterCallActivity.f)localObject).<init>(localAfterCallActivity, localContact, (String)paramObject);
          paramObject = new Object[0];
          b.b((AsyncTask)localObject, (Object[])paramObject);
          return;
        }
      }
    }
    paramObject = AfterCallActivity.G(a);
    localObject = a;
    boolean bool2 = AfterCallActivity.H((AfterCallActivity)localObject);
    if (bool2) {
      localObject = "inPhonebook";
    } else {
      localObject = "validCacheResult";
    }
    ((AfterCallActivity.g)paramObject).a((String)localObject);
  }
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = AfterCallActivity.o(a);
    if (paramVarArgs == null) {
      return null;
    }
    paramVarArgs = new com/truecaller/data/access/c;
    Object localObject1 = a.getApplicationContext();
    paramVarArgs.<init>((Context)localObject1);
    localObject1 = AfterCallActivity.o(a).getTcId();
    paramVarArgs = paramVarArgs.a((String)localObject1);
    localObject1 = a.a.b;
    Object localObject2 = AfterCallActivity.e(a);
    Object localObject3 = AfterCallActivity.f(a);
    localObject1 = ((FilterManager)localObject1).a((String)localObject2, (String)localObject3, false);
    AfterCallActivity.a(a, false);
    AfterCallActivity.b(a, false);
    localObject2 = a;
    AfterCallActivity.c((AfterCallActivity)localObject2, false);
    localObject1 = ((Collection)localObject1).iterator();
    boolean bool1;
    label200:
    label259:
    do
    {
      do
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (g)((Iterator)localObject1).next();
        localObject3 = a;
        boolean bool2 = AfterCallActivity.I((AfterCallActivity)localObject3);
        boolean bool3 = true;
        FilterManager.ActionSource localActionSource1;
        FilterManager.ActionSource localActionSource2;
        if (!bool2)
        {
          localActionSource1 = j;
          localActionSource2 = FilterManager.ActionSource.CUSTOM_BLACKLIST;
          if (localActionSource1 != localActionSource2)
          {
            bool2 = false;
            localActionSource1 = null;
            break label200;
          }
        }
        bool2 = true;
        AfterCallActivity.a((AfterCallActivity)localObject3, bool2);
        localObject3 = a;
        bool2 = AfterCallActivity.J((AfterCallActivity)localObject3);
        if (!bool2)
        {
          localActionSource1 = j;
          localActionSource2 = FilterManager.ActionSource.CUSTOM_WHITELIST;
          if (localActionSource1 != localActionSource2)
          {
            bool2 = false;
            localActionSource1 = null;
            break label259;
          }
        }
        bool2 = true;
        AfterCallActivity.b((AfterCallActivity)localObject3, bool2);
        localObject3 = a;
        bool2 = AfterCallActivity.K((AfterCallActivity)localObject3);
        if (!bool2)
        {
          localObject2 = j;
          localActionSource1 = FilterManager.ActionSource.TOP_SPAMMER;
          if (localObject2 != localActionSource1) {
            bool3 = false;
          }
        }
        AfterCallActivity.c((AfterCallActivity)localObject3, bool3);
        localObject2 = a;
        bool1 = AfterCallActivity.J((AfterCallActivity)localObject2);
        if (bool1) {
          break;
        }
        localObject2 = a;
        bool1 = AfterCallActivity.I((AfterCallActivity)localObject2);
      } while (!bool1);
      localObject2 = a;
      bool1 = AfterCallActivity.K((AfterCallActivity)localObject2);
    } while (!bool1);
    if (paramVarArgs != null)
    {
      localObject1 = a;
      bool1 = com.truecaller.search.f.a((Context)localObject1, paramVarArgs);
      AfterCallActivity.d((AfterCallActivity)localObject1, bool1);
    }
    return paramVarArgs;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */