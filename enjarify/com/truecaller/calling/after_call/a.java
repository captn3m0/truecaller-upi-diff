package com.truecaller.calling.after_call;

import com.truecaller.analytics.b;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.transport.im.bp;
import java.util.concurrent.TimeUnit;

public final class a
{
  boolean a;
  final b b;
  final e c;
  private com.truecaller.androidactors.k d;
  private final f e;
  private final bw f;
  private final an g;
  
  public a(com.truecaller.androidactors.k paramk, f paramf, bw parambw, b paramb, e parame, an paraman)
  {
    d = paramk;
    e = paramf;
    f = parambw;
    b = paramb;
    c = parame;
    g = paraman;
  }
  
  private final boolean a()
  {
    e locale = c;
    String str = "feature_im_promo_after_call_first_timestamp";
    long l1 = 0L;
    long l2 = locale.a(str, l1);
    boolean bool = l2 < l1;
    if (!bool) {
      return false;
    }
    int i = c.a("feature_im_promo_after_call_period_days", 5);
    an localan = g;
    long l3 = i;
    TimeUnit localTimeUnit = TimeUnit.DAYS;
    return localan.a(l2, l3, localTimeUnit);
  }
  
  public final void a(String paramString, boolean paramBoolean, a.a parama)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    c.g.b.k.b(parama, "callback");
    Object localObject = f;
    boolean bool = ((bw)localObject).a();
    if ((bool) && (!paramBoolean))
    {
      paramBoolean = a();
      if (!paramBoolean)
      {
        paramString = ((bp)e.a()).b(paramString);
        i locali = d.a();
        localObject = new com/truecaller/calling/after_call/a$b;
        ((a.b)localObject).<init>(this, parama);
        localObject = (ac)localObject;
        paramString.a(locali, (ac)localObject);
        return;
      }
    }
    a = false;
    parama.onResult(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */