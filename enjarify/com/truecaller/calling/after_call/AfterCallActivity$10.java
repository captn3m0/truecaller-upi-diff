package com.truecaller.calling.after_call;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class AfterCallActivity$10
  extends AnimatorListenerAdapter
{
  AfterCallActivity$10(AfterCallActivity paramAfterCallActivity) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    AfterCallActivity.e(a, true);
    AfterCallActivity.u(a);
    AfterCallActivity.v(a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.10
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */