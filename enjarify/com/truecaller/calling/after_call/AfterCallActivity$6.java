package com.truecaller.calling.after_call;

import android.net.Uri;
import com.truecaller.data.entity.Contact;
import com.truecaller.util.w.a;

final class AfterCallActivity$6
  extends w.a
{
  AfterCallActivity$6(AfterCallActivity paramAfterCallActivity, Contact paramContact, Uri paramUri)
  {
    super(paramContact, paramUri);
  }
  
  public final void a(Object paramObject)
  {
    if (paramObject != null)
    {
      AfterCallActivity localAfterCallActivity = a;
      paramObject = (Contact)paramObject;
      AfterCallActivity.a(localAfterCallActivity, (Contact)paramObject);
      paramObject = a;
      boolean bool = true;
      AfterCallActivity.d((AfterCallActivity)paramObject, bool);
      paramObject = a;
      ((AfterCallActivity)paramObject).d();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */