package com.truecaller.calling.after_call;

import android.widget.Toast;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.data.entity.Contact;
import com.truecaller.tag.c;
import com.truecaller.ui.components.AfterCallHeaderView;
import com.truecaller.ui.components.a;

final class AfterCallActivity$7
  implements a
{
  AfterCallActivity$7(AfterCallActivity paramAfterCallActivity) {}
  
  public final void a()
  {
    AfterCallActivity.r(a).e();
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = 1;
    if (paramBoolean)
    {
      paramBoolean = true;
      localObject1 = "yes";
    }
    else
    {
      localObject1 = "no";
      paramBoolean = true;
    }
    Object localObject2 = (c)AfterCallActivity.p(a).a();
    Contact localContact = AfterCallActivity.o(a);
    ((c)localObject2).a(localContact, paramBoolean);
    b localb = AfterCallActivity.q(a);
    localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("ViewAction");
    Object localObject1 = ((e.a)localObject2).a("Context", "afterCall").a("Action", "businessSuggestion").a("SubAction", (String)localObject1).a();
    localb.b((e)localObject1);
    AfterCallActivity.r(a).e();
    Toast.makeText(a, 2131887241, i).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */