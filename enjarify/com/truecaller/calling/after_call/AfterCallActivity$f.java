package com.truecaller.calling.after_call;

import com.truecaller.androidactors.f;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.FilterManager;
import com.truecaller.search.b;
import com.truecaller.search.h;
import java.util.UUID;

final class AfterCallActivity$f
  extends h
{
  AfterCallActivity$f(AfterCallActivity paramAfterCallActivity, Contact paramContact, String paramString)
  {
    super(paramAfterCallActivity, paramAfterCallActivity, localFilterManager, localf, paramContact, paramString, "afterCall", localUUID, localb);
  }
  
  public final void a(Object paramObject)
  {
    boolean bool = paramObject instanceof Contact;
    if (bool)
    {
      AfterCallActivity localAfterCallActivity = a;
      paramObject = (Contact)paramObject;
      AfterCallActivity.a(localAfterCallActivity, (Contact)paramObject);
      paramObject = a;
      ((AfterCallActivity)paramObject).d();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */