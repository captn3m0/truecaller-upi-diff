package com.truecaller.calling.after_call;

import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.am;
import com.truecaller.common.tag.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.al;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ay;
import com.truecaller.tracking.events.ay.a;
import com.truecaller.tracking.events.bc;
import com.truecaller.tracking.events.bc.a;
import com.truecaller.tracking.events.z;
import com.truecaller.tracking.events.z.a;
import com.truecaller.util.ce;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.apache.a.a;
import org.apache.a.d.d;

final class AfterCallActivity$g
{
  boolean a = false;
  
  private AfterCallActivity$g(AfterCallActivity paramAfterCallActivity) {}
  
  final void a(String paramString)
  {
    int i = a;
    if (i != 0) {
      return;
    }
    i = 1;
    a = i;
    z.a locala = z.b();
    Object localObject1 = UUID.randomUUID().toString();
    localObject1 = locala.a((CharSequence)localObject1);
    Object localObject2 = b;
    int j = AfterCallActivity.M((AfterCallActivity)localObject2);
    if (j == i) {
      localObject2 = "widget";
    } else {
      localObject2 = "afterCall";
    }
    localObject1 = ((z.a)localObject1).d((CharSequence)localObject2);
    localObject2 = String.valueOf(AfterCallActivity.L(b));
    ((z.a)localObject1).c((CharSequence)localObject2);
    localObject1 = null;
    locala.b(null);
    boolean bool1 = false;
    localObject2 = null;
    locala.a(false);
    locala.b(false);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject3 = AfterCallActivity.o(b);
    if (localObject3 != null)
    {
      localObject3 = al.b();
      boolean bool2 = am.b(AfterCallActivity.o(b).z()) ^ i;
      localObject3 = ((al.a)localObject3).b(bool2);
      bool2 = AfterCallActivity.H(b);
      localObject3 = ((al.a)localObject3).a(bool2);
      int k = AfterCallActivity.o(b).I();
      localObject4 = Integer.valueOf(Math.max(0, k));
      localObject3 = ((al.a)localObject3).a((Integer)localObject4);
      localObject4 = Boolean.valueOf(AfterCallActivity.o(b).U());
      localObject3 = ((al.a)localObject3).d((Boolean)localObject4);
      localObject4 = Boolean.valueOf(AfterCallActivity.I(b));
      localObject3 = ((al.a)localObject3).a((Boolean)localObject4);
      localObject4 = Boolean.valueOf(AfterCallActivity.J(b));
      localObject3 = ((al.a)localObject3).c((Boolean)localObject4);
      localObject4 = Boolean.valueOf(AfterCallActivity.K(b));
      localObject3 = ((al.a)localObject3).b((Boolean)localObject4);
      localObject4 = AfterCallActivity.o(b);
      k = ((Contact)localObject4).getSource() & 0x40;
      if (k != 0) {
        bool1 = true;
      }
      localObject2 = Boolean.valueOf(bool1);
      localObject2 = ((al.a)localObject3).e((Boolean)localObject2).a();
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject4 = new java/util/ArrayList;
      ((ArrayList)localObject4).<init>();
      localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>();
      localObject6 = AfterCallActivity.o(b).J().iterator();
      for (;;)
      {
        bool4 = ((Iterator)localObject6).hasNext();
        if (!bool4) {
          break;
        }
        Object localObject7 = (Tag)((Iterator)localObject6).next();
        int n = ((Tag)localObject7).getSource();
        if (n == i)
        {
          localObject7 = ((Tag)localObject7).a();
          ((List)localObject3).add(localObject7);
        }
        else
        {
          localObject7 = ((Tag)localObject7).a();
          ((List)localObject4).add(localObject7);
        }
      }
      localObject6 = ce.a(AfterCallActivity.o(b));
      if (localObject6 != null)
      {
        long l = a;
        localObject6 = String.valueOf(l);
        ((List)localObject5).add(localObject6);
      }
      localObject6 = bc.b();
      boolean bool4 = ((List)localObject3).isEmpty();
      if (bool4) {
        localObject3 = null;
      }
      localObject3 = ((bc.a)localObject6).a((List)localObject3);
      boolean bool5 = ((List)localObject4).isEmpty();
      if (bool5)
      {
        k = 0;
        localObject4 = null;
      }
      localObject3 = ((bc.a)localObject3).b((List)localObject4);
      boolean bool3 = ((List)localObject5).isEmpty();
      if (bool3) {
        localObject5 = null;
      }
      localObject3 = ((bc.a)localObject3).c((List)localObject5).a();
      localObject4 = AfterCallActivity.o(b).A().iterator();
      localObject5 = null;
      for (;;)
      {
        bool5 = ((Iterator)localObject4).hasNext();
        if (!bool5) {
          break;
        }
        localObject6 = (Number)((Iterator)localObject4).next();
        int m = ((Number)localObject6).getSource() & i;
        if (m != 0) {
          localObject5 = ((Number)localObject6).b();
        }
      }
    }
    Object localObject8 = al.b().b(false).a(false);
    localObject2 = Integer.valueOf(0);
    localObject8 = ((al.a)localObject8).a((Integer)localObject2);
    localObject2 = Boolean.FALSE;
    localObject8 = ((al.a)localObject8).d((Boolean)localObject2);
    localObject2 = Boolean.FALSE;
    localObject8 = ((al.a)localObject8).a((Boolean)localObject2);
    localObject2 = Boolean.FALSE;
    localObject8 = ((al.a)localObject8).c((Boolean)localObject2);
    localObject2 = Boolean.FALSE;
    localObject8 = ((al.a)localObject8).b((Boolean)localObject2);
    localObject2 = Boolean.FALSE;
    localObject8 = ((al.a)localObject8).e((Boolean)localObject2);
    localObject2 = ((al.a)localObject8).a();
    localObject3 = null;
    Object localObject5 = null;
    localObject8 = ay.b();
    Object localObject4 = AfterCallActivity.e(b);
    Object localObject6 = AfterCallActivity.f(b);
    localObject4 = am.e((CharSequence)localObject4, (CharSequence)localObject6);
    localObject8 = ((ay.a)localObject8).a((CharSequence)localObject4).a((bc)localObject3).a((al)localObject2);
    paramString = ((ay.a)localObject8).b(paramString).c((CharSequence)localObject5).a();
    localArrayList.add(paramString);
    locala.a(localArrayList);
    locala.b(null);
    try
    {
      paramString = b;
      paramString = d;
      paramString = paramString.a();
      paramString = (ae)paramString;
      localObject8 = locala.a();
      paramString.a((d)localObject8);
      return;
    }
    catch (a locala1)
    {
      AssertionUtil.reportThrowableButNeverCrash(locala1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */