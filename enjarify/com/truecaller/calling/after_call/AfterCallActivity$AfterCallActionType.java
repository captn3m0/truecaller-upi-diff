package com.truecaller.calling.after_call;

public enum AfterCallActivity$AfterCallActionType
{
  public final int requestCode;
  
  static
  {
    Object localObject = new com/truecaller/calling/after_call/AfterCallActivity$AfterCallActionType;
    ((AfterCallActionType)localObject).<init>("STORE", 0, 2131364121);
    STORE = (AfterCallActionType)localObject;
    localObject = new com/truecaller/calling/after_call/AfterCallActivity$AfterCallActionType;
    int i = 1;
    ((AfterCallActionType)localObject).<init>("BLOCK", i, 2131364120);
    BLOCK = (AfterCallActionType)localObject;
    localObject = new AfterCallActionType[2];
    AfterCallActionType localAfterCallActionType = STORE;
    localObject[0] = localAfterCallActionType;
    localAfterCallActionType = BLOCK;
    localObject[i] = localAfterCallActionType;
    $VALUES = (AfterCallActionType[])localObject;
  }
  
  private AfterCallActivity$AfterCallActionType(int paramInt1)
  {
    requestCode = paramInt1;
  }
  
  public final int getRequestCode()
  {
    return requestCode;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.AfterCallActionType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */