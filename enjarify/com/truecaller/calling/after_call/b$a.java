package com.truecaller.calling.after_call;

import android.content.Context;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.analytics.e.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.util.w;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import kotlinx.coroutines.ag;

final class b$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  b$a(b paramb, Contact paramContact, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/calling/after_call/b$a;
    b localb = b;
    Contact localContact = c;
    locala.<init>(localb, localContact, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = b.a;
        localObject1 = c.E();
        paramObject = w.a((Context)paramObject, (Long)localObject1);
        c.g.b.k.a(paramObject, "ContactUtil.getExternalA…ext, contact.phonebookId)");
        paramObject = (Iterable)paramObject;
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        localObject1 = (Collection)localObject1;
        paramObject = ((Iterable)paramObject).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)paramObject).hasNext();
          if (!bool2) {
            break;
          }
          Object localObject2 = ((Iterator)paramObject).next();
          Object localObject3 = localObject2;
          localObject3 = d;
          String str = "com.whatsapp";
          bool3 = c.g.b.k.a(localObject3, str);
          if (bool3) {
            ((Collection)localObject1).add(localObject2);
          }
        }
        paramObject = b.b;
        localObject1 = new com/truecaller/analytics/e$a;
        ((e.a)localObject1).<init>("AfterCallSaveContact");
        boolean bool3 = c.L();
        localObject1 = ((e.a)localObject1).a("hasTCProfile", bool3).a("hasWhatsAppProfile", true);
        bool3 = c.M();
        localObject1 = ((e.a)localObject1).a("isGoldUser", bool3);
        bool3 = c.a(4);
        localObject1 = ((e.a)localObject1).a("isProUser", bool3).a();
        c.g.b.k.a(localObject1, "AnalyticsEvent.Builder(A…                 .build()");
        ((com.truecaller.analytics.b)paramObject).b((com.truecaller.analytics.e)localObject1);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */