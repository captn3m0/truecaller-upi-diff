package com.truecaller.calling.after_call;

import android.view.View;
import com.d.b.e;
import java.lang.ref.WeakReference;

final class AfterCallActivity$9
  implements e
{
  AfterCallActivity$9(AfterCallActivity paramAfterCallActivity, WeakReference paramWeakReference) {}
  
  public final void onError() {}
  
  public final void onSuccess()
  {
    View localView = (View)a.get();
    if (localView != null) {
      localView.setVisibility(0);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.9
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */