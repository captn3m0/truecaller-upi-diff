package com.truecaller.calling;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import c.f;
import c.g.a.a;
import c.g.b.u;
import c.g.b.w;

public final class i
  implements b
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final f f;
  private boolean g;
  private ActionType h;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[5];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(i.class);
    ((u)localObject).<init>(localb, "actionTwoView", "getActionTwoView()Landroid/widget/ImageView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(i.class);
    ((u)localObject).<init>(localb, "actionTwoClickArea", "getActionTwoClickArea()Landroid/view/View;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(i.class);
    ((u)localObject).<init>(localb, "icViewProfile", "getIcViewProfile()Landroid/graphics/drawable/Drawable;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(i.class);
    ((u)localObject).<init>(localb, "icCall", "getIcCall()Landroid/graphics/drawable/Drawable;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[3] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(i.class);
    ((u)localObject).<init>(localb, "icWhatsApp", "getIcWhatsApp()Landroid/graphics/drawable/Drawable;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[4] = localObject;
    a = arrayOfg;
  }
  
  public i(View paramView)
  {
    Object localObject = com.truecaller.utils.extensions.t.a(paramView, 2131361939);
    b = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131361843);
    c = ((f)localObject);
    localObject = new com/truecaller/calling/i$b;
    ((i.b)localObject).<init>(paramView);
    localObject = c.g.a((a)localObject);
    d = ((f)localObject);
    localObject = new com/truecaller/calling/i$a;
    ((i.a)localObject).<init>(paramView);
    localObject = c.g.a((a)localObject);
    e = ((f)localObject);
    localObject = new com/truecaller/calling/i$c;
    ((i.c)localObject).<init>(paramView);
    paramView = c.g.a((a)localObject);
    f = paramView;
  }
  
  private final ImageView a()
  {
    return (ImageView)b.b();
  }
  
  private final View b()
  {
    return (View)c.b();
  }
  
  public final void a(com.truecaller.adapter_delegates.k paramk, RecyclerView.ViewHolder paramViewHolder)
  {
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramViewHolder, "holder");
    View localView = b();
    c.g.b.k.a(localView, "actionTwoClickArea");
    Object localObject = new com/truecaller/calling/i$d;
    ((i.d)localObject).<init>(this);
    localObject = (a)localObject;
    com.truecaller.adapter_delegates.i.a(localView, paramk, paramViewHolder, (a)localObject, "button");
  }
  
  public final void b(ActionType paramActionType)
  {
    Object localObject1 = null;
    if (paramActionType != null)
    {
      localObject2 = j.a;
      int i = paramActionType.ordinal();
      j = localObject2[i];
      switch (j)
      {
      default: 
        break;
      case 4: 
      case 5: 
        localObject2 = (Drawable)f.b();
        break;
      case 2: 
      case 3: 
        localObject2 = (Drawable)e.b();
        break;
      case 1: 
        localObject2 = (Drawable)d.b();
        break;
      }
    }
    int j = 0;
    Object localObject2 = null;
    if (localObject2 != null)
    {
      h = paramActionType;
      paramActionType = a();
      localObject1 = paramActionType;
      com.truecaller.utils.extensions.t.a((View)paramActionType);
      paramActionType.setImageDrawable((Drawable)localObject2);
      boolean bool = g;
      c(bool);
      return;
    }
    paramActionType = this;
    paramActionType = (i)this;
    h = null;
    localObject1 = paramActionType.a();
    c.g.b.k.a(localObject1, "actionTwoView");
    com.truecaller.utils.extensions.t.b((View)localObject1);
    paramActionType = paramActionType.b();
    c.g.b.k.a(paramActionType, "actionTwoClickArea");
    com.truecaller.utils.extensions.t.b(paramActionType);
  }
  
  public final void c(boolean paramBoolean)
  {
    g = paramBoolean;
    Object localObject = h;
    if (localObject != null)
    {
      localObject = b();
      if (paramBoolean)
      {
        com.truecaller.utils.extensions.t.a((View)localObject);
        return;
      }
      com.truecaller.utils.extensions.t.c((View)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */