package com.truecaller.calling;

import c.g.b.k;
import java.util.List;

public final class x
{
  final List a;
  final String b;
  final String c;
  
  public x(List paramList, String paramString1, String paramString2)
  {
    a = paramList;
    b = paramString1;
    c = paramString2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof x;
      if (bool1)
      {
        paramObject = (x)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = c;
            paramObject = c;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final int hashCode()
  {
    List localList = a;
    int i = 0;
    if (localList != null)
    {
      j = localList.hashCode();
    }
    else
    {
      j = 0;
      localList = null;
    }
    j *= 31;
    String str = b;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    int j = (j + k) * 31;
    str = c;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FlashBinding(phoneNumber=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", name=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", analyticsContext=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */