package com.truecaller.calling;

import android.os.Bundle;
import c.g.b.k;
import c.u;
import java.util.concurrent.TimeUnit;

public abstract class aj
{
  public static final aj.a c;
  String a;
  final long b;
  
  static
  {
    aj.a locala = new com/truecaller/calling/aj$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private aj(String paramString, long paramLong)
  {
    a = paramString;
    b = paramLong;
  }
  
  public abstract Bundle a();
  
  public boolean equals(Object paramObject)
  {
    Object localObject = this;
    localObject = (aj)this;
    boolean bool1 = true;
    if (localObject == paramObject) {
      return bool1;
    }
    localObject = getClass();
    if (paramObject != null) {
      localClass = paramObject.getClass();
    } else {
      localClass = null;
    }
    boolean bool2 = k.a(localObject, localClass) ^ bool1;
    Class localClass = null;
    if (bool2) {
      return false;
    }
    if (paramObject != null)
    {
      localObject = a;
      paramObject = (aj)paramObject;
      String str = a;
      bool2 = k.a(localObject, str) ^ bool1;
      if (bool2) {
        return false;
      }
      long l1 = b;
      long l2 = b;
      l1 = Math.abs(l1 - l2);
      paramObject = TimeUnit.SECONDS;
      l2 = ((TimeUnit)paramObject).toMillis(15);
      boolean bool3 = l1 < l2;
      if (bool3) {
        return false;
      }
      return bool1;
    }
    paramObject = new c/u;
    ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.calling.PhoneState");
    throw ((Throwable)paramObject);
  }
  
  public int hashCode()
  {
    String str = a;
    int i;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    int j = getClass().hashCode();
    return i + j;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = getClass().getSimpleName();
    localStringBuilder.append(str);
    localStringBuilder.append(", number: ");
    str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", time: ");
    long l = b;
    localStringBuilder.append(l);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */