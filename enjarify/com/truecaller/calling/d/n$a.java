package com.truecaller.calling.d;

import android.os.Bundle;

public final class n$a
{
  public static n a(int paramInt, String paramString, boolean paramBoolean)
  {
    n localn = new com/truecaller/calling/d/n;
    localn.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putInt("speed_dial_key", paramInt);
    localBundle.putString("speed_dial_value", paramString);
    localBundle.putBoolean("show_options", paramBoolean);
    localn.setArguments(localBundle);
    localn.setStyle(1, 0);
    return localn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.n.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */