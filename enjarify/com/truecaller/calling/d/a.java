package com.truecaller.calling.d;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import c.g.b.k;
import c.n.m;
import com.truecaller.common.h.ab;
import com.truecaller.ui.dialogs.e;
import java.io.Serializable;
import java.util.HashMap;

public final class a
  extends e
{
  public static final a.a a;
  private HashMap c;
  
  static
  {
    a.a locala = new com/truecaller/calling/d/a$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    HashMap localHashMap = c;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void b(int paramInt)
  {
    super.b(paramInt);
    int i = -1;
    if (paramInt == i)
    {
      String str1 = PhoneNumberUtils.stripSeparators(a());
      Object localObject1 = str1;
      localObject1 = (CharSequence)str1;
      boolean bool1 = ab.a((CharSequence)localObject1);
      boolean bool2 = true;
      bool1 ^= bool2;
      int j = 0;
      Object localObject2 = null;
      if (bool1)
      {
        paramInt = 0;
        str1 = null;
      }
      if (str1 != null)
      {
        localObject1 = "N";
        Object localObject3 = "";
        str1 = m.a(str1, (String)localObject1, (String)localObject3);
        if (str1 != null)
        {
          localObject1 = str1;
          localObject1 = (CharSequence)str1;
          bool1 = m.a((CharSequence)localObject1) ^ bool2;
          String str2;
          if (bool1)
          {
            localObject1 = getArguments();
            if (localObject1 != null)
            {
              localObject3 = "initial_text";
              localObject1 = ((Bundle)localObject1).getString((String)localObject3);
            }
            else
            {
              bool1 = false;
              localObject1 = null;
            }
            bool1 = k.a(str1, localObject1) ^ bool2;
            if (bool1) {}
          }
          else
          {
            bool2 = false;
            str2 = null;
          }
          if (!bool2)
          {
            paramInt = 0;
            str1 = null;
          }
          if (str1 != null)
          {
            localObject1 = new android/content/Intent;
            ((Intent)localObject1).<init>();
            str2 = "speed_dial_key";
            localObject3 = getArguments();
            if (localObject3 != null)
            {
              j = ((Bundle)localObject3).getInt("speed_dial_key");
              localObject2 = Integer.valueOf(j);
            }
            localObject2 = (Serializable)localObject2;
            ((Intent)localObject1).putExtra(str2, (Serializable)localObject2);
            ((Intent)localObject1).putExtra("speed_dial_value", str1);
            a(i, (Intent)localObject1);
            return;
          }
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */