package com.truecaller.calling.d;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.calling.dialer.ax;
import com.truecaller.utils.n;
import java.util.List;

public final class j
  extends c
  implements i.b
{
  private final i.a c;
  private final n d;
  private final b e;
  private final ax f;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(j.class);
    ((u)localObject).<init>(localb, "speedDialModels", "getSpeedDialModels()Ljava/util/List;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public j(n paramn, b paramb, i.a parama, ax paramax)
  {
    d = paramn;
    e = paramb;
    f = paramax;
    c = parama;
  }
  
  private final List a()
  {
    i.a locala = c;
    Object localObject = this;
    localObject = (i.b)this;
    c.l.g localg = b[0];
    return locala.a((i.b)localObject, localg);
  }
  
  public final boolean a(h paramh)
  {
    Object localObject = "event";
    k.b(paramh, (String)localObject);
    int i = b;
    paramh = a;
    String str = "ItemEvent.CLICKED";
    boolean bool = k.a(paramh, str);
    if (bool)
    {
      paramh = e;
      int j = i + 1;
      List localList = a();
      localObject = (g)localList.get(i);
      if (localObject != null)
      {
        localObject = a;
      }
      else
      {
        i = 0;
        localObject = null;
      }
      paramh.a(j, (String)localObject);
      return true;
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */