package com.truecaller.calling.d;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.c;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class n
  extends com.truecaller.ui.dialogs.a
  implements View.OnClickListener
{
  public static final n.a b;
  final c.d.f a;
  private final bp c;
  private int d;
  private String e;
  private final q f;
  private final c.d.f g;
  private final Intent h;
  private HashMap i;
  
  static
  {
    n.a locala = new com/truecaller/calling/d/n$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public n()
  {
    Object localObject = TrueApp.y();
    k.a(localObject, "TrueApp.getApp()");
    localObject = ((TrueApp)localObject).a();
    k.a(localObject, "TrueApp.getApp().objectsGraph");
    c = ((bp)localObject);
    localObject = c.G();
    k.a(localObject, "graph.speedDialSettings()");
    f = ((q)localObject);
    localObject = c.bl();
    k.a(localObject, "graph.uiCoroutineContext()");
    g = ((c.d.f)localObject);
    localObject = c.bm();
    k.a(localObject, "graph.asyncCoroutineContext()");
    a = ((c.d.f)localObject);
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>();
    h = ((Intent)localObject);
  }
  
  private final void a(String paramString)
  {
    q localq = f;
    int j = d;
    localq.a(j, paramString);
    Toast.makeText(getContext(), 2131887166, 0).show();
    paramString = h;
    a(-1, paramString);
    dismissAllowingStateLoss();
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = i;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      i = ((HashMap)localObject1);
    }
    localObject1 = i;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = i;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    HashMap localHashMap = i;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int j = 1003;
    if (paramInt1 == j)
    {
      ag localag = (ag)bg.a;
      c.d.f localf = g;
      Object localObject = new com/truecaller/calling/d/n$c;
      ((n.c)localObject).<init>(this, paramIntent, paramInt2, null);
      localObject = (c.g.a.m)localObject;
      int k = 2;
      e.b(localag, localf, (c.g.a.m)localObject, k);
    }
    j = 1002;
    if (paramInt1 == j)
    {
      paramInt1 = -1;
      String str;
      if (paramInt2 == paramInt1)
      {
        paramInt1 = 1;
      }
      else
      {
        paramInt1 = 0;
        str = null;
      }
      if (paramInt1 == 0) {
        paramIntent = null;
      }
      if (paramIntent != null)
      {
        str = paramIntent.getStringExtra("speed_dial_value");
        if (str != null)
        {
          a(str);
          return;
        }
      }
    }
  }
  
  public final void onClick(View paramView)
  {
    Object localObject1 = "v";
    k.b(paramView, (String)localObject1);
    int j = paramView.getId();
    int k = 2131361983;
    if (j != k)
    {
      k = 2131361987;
      int m = 0;
      Object localObject2 = null;
      if (j != k)
      {
        k = 2131362935;
        if (j != k)
        {
          k = 2131364111;
          if (j == k)
          {
            paramView = f;
            k = d;
            paramView.a(k, null);
            localObject1 = h;
            a(-1, (Intent)localObject1);
            dismissAllowingStateLoss();
          }
        }
        else
        {
          m.a((Activity)getActivity());
          dismissAllowingStateLoss();
        }
      }
      else
      {
        paramView = this;
        paramView = (Fragment)this;
        k = d;
        String str = e;
        k.b(paramView, "fragment");
        android.support.v4.app.f localf = paramView.getActivity();
        if (localf == null) {
          return;
        }
        k.a(localf, "fragment.activity ?: return");
        Object localObject3 = a.a;
        localObject3 = new com/truecaller/calling/d/a;
        ((a)localObject3).<init>();
        Bundle localBundle = new android/os/Bundle;
        localBundle.<init>();
        Object localObject4 = "speed_dial_key";
        Integer localInteger = Integer.valueOf(k);
        Object localObject5 = localInteger;
        localObject5 = (Number)localInteger;
        int n = ((Number)localObject5).intValue();
        int i1 = 9;
        int i2 = 2;
        int i3 = 1;
        if ((i2 <= n) && (i1 >= n))
        {
          n = 1;
        }
        else
        {
          n = 0;
          localObject5 = null;
        }
        if (n != 0) {
          localObject2 = localInteger;
        }
        if (localObject2 != null)
        {
          m = ((Integer)localObject2).intValue();
          localBundle.putInt((String)localObject4, m);
          ((a)localObject3).setArguments(localBundle);
          localObject2 = TrueApp.x();
          localObject4 = new Object[i3];
          localObject1 = Integer.valueOf(k);
          localObject4[0] = localObject1;
          localObject1 = ((Context)localObject2).getString(2131887164, (Object[])localObject4);
          a.a((a)localObject3, (String)localObject1, str);
          ((a)localObject3).setTargetFragment(paramView, 1002);
          com.truecaller.ui.dialogs.a.a((com.truecaller.ui.dialogs.a)localObject3, localf);
          return;
        }
        paramView = new java/lang/IllegalArgumentException;
        paramView.<init>();
        throw ((Throwable)paramView);
      }
    }
    else
    {
      paramView = this;
      paramView = (Fragment)this;
      at.a(paramView);
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getArguments();
    if (paramBundle != null)
    {
      int j = paramBundle.getInt("speed_dial_key");
      paramBundle = Integer.valueOf(j);
      Object localObject = paramBundle;
      localObject = (Number)paramBundle;
      int k = ((Number)localObject).intValue();
      int m = 9;
      int n = 2;
      if ((n <= k) && (m >= k))
      {
        k = 1;
      }
      else
      {
        k = 0;
        localObject = null;
      }
      m = 0;
      String str = null;
      if (k == 0)
      {
        j = 0;
        paramBundle = null;
      }
      if (paramBundle != null)
      {
        j = paramBundle.intValue();
        d = j;
        paramBundle = getArguments();
        if (paramBundle != null)
        {
          localObject = "speed_dial_value";
          str = paramBundle.getString((String)localObject);
        }
        e = str;
        paramBundle = h;
        m = d;
        paramBundle.putExtra("speed_dial_key", m);
        return;
      }
    }
    paramBundle = new java/lang/IllegalArgumentException;
    paramBundle.<init>();
    throw ((Throwable)paramBundle);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558597, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    int j = R.id.title;
    paramView = (TextView)a(j);
    paramBundle = "title";
    k.a(paramView, paramBundle);
    boolean bool1 = true;
    Object localObject1 = new Object[bool1];
    Object localObject2 = Integer.valueOf(d);
    boolean bool2 = false;
    localObject1[0] = localObject2;
    int k = 2131887164;
    localObject1 = (CharSequence)getString(k, (Object[])localObject1);
    paramView.setText((CharSequence)localObject1);
    j = R.id.edit_speed_dial;
    paramView = (Button)a(j);
    k.a(paramView, "edit_speed_dial");
    paramView = (View)paramView;
    localObject1 = getArguments();
    if (localObject1 != null)
    {
      localObject2 = "show_options";
      bool3 = ((Bundle)localObject1).getBoolean((String)localObject2);
      localObject1 = Boolean.valueOf(bool3);
    }
    else
    {
      bool3 = false;
      localObject1 = null;
    }
    boolean bool3 = c.a((Boolean)localObject1);
    t.a(paramView, bool3);
    j = R.id.remove;
    paramView = (Button)a(j);
    k.a(paramView, "remove");
    paramView = (View)paramView;
    localObject1 = (CharSequence)e;
    if (localObject1 != null)
    {
      int m = ((CharSequence)localObject1).length();
      if (m != 0) {}
    }
    else
    {
      bool2 = true;
    }
    bool1 ^= bool2;
    t.a(paramView, bool1);
    j = R.id.edit_speed_dial;
    paramView = (Button)a(j);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    paramView.setOnClickListener(paramBundle);
    j = R.id.remove;
    ((Button)a(j)).setOnClickListener(paramBundle);
    j = R.id.add_phone_number;
    ((Button)a(j)).setOnClickListener(paramBundle);
    j = R.id.add_contact;
    ((Button)a(j)).setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */