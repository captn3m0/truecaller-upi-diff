package com.truecaller.calling.d;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.support.v4.view.r;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import c.g.b.k;
import com.truecaller.util.at;
import com.truecaller.utils.ui.b;

public final class e
  extends RecyclerView.ItemDecoration
{
  private final Paint a;
  private final Paint b;
  private final Rect c;
  private final int d;
  private final int e;
  
  public e(Context paramContext)
  {
    Object localObject1 = new android/graphics/Paint;
    int i = 1;
    ((Paint)localObject1).<init>(i);
    a = ((Paint)localObject1);
    localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>(i);
    b = ((Paint)localObject1);
    localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    c = ((Rect)localObject1);
    localObject1 = a;
    Object localObject2 = Paint.Style.STROKE;
    ((Paint)localObject1).setStyle((Paint.Style)localObject2);
    localObject1 = a;
    float f1 = at.a(paramContext, 1.0F);
    ((Paint)localObject1).setStrokeWidth(f1);
    a.setColor(-7693147);
    localObject1 = b;
    localObject2 = Paint.Align.CENTER;
    ((Paint)localObject1).setTextAlign((Paint.Align)localObject2);
    localObject1 = b;
    f1 = 20.0F;
    float f2 = at.a(paramContext, f1);
    ((Paint)localObject1).setTextSize(f2);
    localObject1 = b;
    int j = b.a(paramContext, 2130969592);
    ((Paint)localObject1).setColor(j);
    int k = at.a(paramContext, f1);
    d = k;
    int m = paramContext.getResources().getDimensionPixelSize(2131165483);
    e = m;
  }
  
  public final void onDrawOver(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    k.b(paramCanvas, "c");
    k.b(paramRecyclerView, "parent");
    k.b(paramState, "state");
    int i = paramRecyclerView.getChildCount();
    int j = 0;
    while (j < i)
    {
      Object localObject1 = paramRecyclerView.getChildAt(j);
      int k = paramRecyclerView.getChildAdapterPosition((View)localObject1);
      int m = 1;
      float f1 = Float.MIN_VALUE;
      k += m;
      Object localObject2 = paramRecyclerView;
      localObject2 = (View)paramRecyclerView;
      int n = r.g((View)localObject2);
      if (n == m)
      {
        n = 1;
        f2 = Float.MIN_VALUE;
      }
      else
      {
        n = 0;
        f2 = 0.0F;
        localObject2 = null;
      }
      int i1;
      int i2;
      if (n != 0)
      {
        localObject3 = "child";
        k.a(localObject1, (String)localObject3);
        i1 = ((View)localObject1).getRight();
        i2 = e / 2;
        i1 -= i2;
      }
      else
      {
        localObject3 = "child";
        k.a(localObject1, (String)localObject3);
        i1 = ((View)localObject1).getLeft();
        i2 = e / 2;
        i1 += i2;
      }
      float f3 = i1;
      float f4 = ((View)localObject1).getY();
      float f5 = ((View)localObject1).getHeight() / 2;
      f4 += f5;
      int i3 = d;
      f5 = i3;
      Paint localPaint = a;
      paramCanvas.drawCircle(f3, f4, f5, localPaint);
      String str = String.valueOf(k);
      Object localObject3 = b;
      Rect localRect = c;
      ((Paint)localObject3).getTextBounds(str, 0, m, localRect);
      if (n != 0)
      {
        m = ((View)localObject1).getRight();
        n = e / 2;
        m -= n;
      }
      else
      {
        m = ((View)localObject1).getLeft();
        n = e / 2;
        m += n;
      }
      f1 = m;
      float f2 = ((View)localObject1).getY();
      float f6 = ((View)localObject1).getHeight() / 2;
      f2 += f6;
      int i4 = c.height() / 2;
      f6 = i4;
      f2 += f6;
      localObject1 = b;
      paramCanvas.drawText(str, f1, f2, (Paint)localObject1);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */