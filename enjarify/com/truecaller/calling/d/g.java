package com.truecaller.calling.d;

import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;

public final class g
{
  final String a;
  final Number b;
  final Contact c;
  
  public g(String paramString, Number paramNumber, Contact paramContact)
  {
    a = paramString;
    b = paramNumber;
    c = paramContact;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof g;
    if (bool1)
    {
      String str = a;
      paramObject = a;
      boolean bool2 = k.a(str, paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SpeedDialItem(originalValue=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", number=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", contact=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */