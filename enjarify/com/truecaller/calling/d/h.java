package com.truecaller.calling.d;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import com.truecaller.adapter_delegates.i;

public final class h
  extends RecyclerView.ViewHolder
  implements i.c
{
  private final f b;
  private final f c;
  private final f d;
  private final int e;
  private final int f;
  private final View g;
  
  static
  {
    g[] arrayOfg = new g[3];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(h.class);
    ((u)localObject).<init>(localb, "title", "getTitle()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(h.class);
    ((u)localObject).<init>(localb, "label", "getLabel()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(h.class);
    ((u)localObject).<init>(localb, "editIcon", "getEditIcon()Landroid/view/View;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    a = arrayOfg;
  }
  
  public h(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    g = paramView;
    paramView = com.truecaller.utils.extensions.t.a(g, 2131364884);
    b = paramView;
    paramView = com.truecaller.utils.extensions.t.a(g, 2131363578);
    c = paramView;
    paramView = com.truecaller.utils.extensions.t.a(g, 2131362932);
    d = paramView;
    int i = com.truecaller.utils.ui.b.a(g.getContext(), 2130969591);
    e = i;
    i = com.truecaller.utils.ui.b.a(g.getContext(), 2130969592);
    f = i;
    paramView = g;
    Object localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, 12);
  }
  
  private final TextView a()
  {
    return (TextView)b.b();
  }
  
  private final TextView b()
  {
    return (TextView)c.b();
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "title");
    TextView localTextView = a();
    c.g.b.k.a(localTextView, "this.title");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    g.setClickable(paramBoolean);
    View localView = (View)d.b();
    c.g.b.k.a(localView, "this.editIcon");
    com.truecaller.utils.extensions.t.a(localView, paramBoolean);
  }
  
  public final void b(String paramString)
  {
    if (paramString != null)
    {
      TextView localTextView = b();
      c.g.b.k.a(localTextView, "this.label");
      paramString = (CharSequence)paramString;
      localTextView.setText(paramString);
      paramString = b();
      c.g.b.k.a(paramString, "this.label");
      com.truecaller.utils.extensions.t.a((View)paramString);
      return;
    }
    paramString = this;
    paramString = ((h)this).b();
    c.g.b.k.a(paramString, "this.label");
    com.truecaller.utils.extensions.t.b((View)paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    TextView localTextView = a();
    if (paramBoolean) {
      paramBoolean = f;
    } else {
      paramBoolean = e;
    }
    localTextView.setTextColor(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */