package com.truecaller.calling.d;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import c.g.a.b;
import c.g.b.k;
import c.k.h;
import c.x;
import com.truecaller.ui.SingleActivity;
import com.truecaller.ui.SingleActivity.FragmentSingle;
import com.truecaller.ui.dialogs.a;

public final class m
{
  public static final x a(Activity paramActivity)
  {
    if (paramActivity != null)
    {
      Object localObject = paramActivity;
      localObject = (Context)paramActivity;
      SingleActivity.FragmentSingle localFragmentSingle = SingleActivity.FragmentSingle.SPEED_DIAL;
      localObject = SingleActivity.a((Context)localObject, localFragmentSingle);
      paramActivity.startActivity((Intent)localObject);
      return x.a;
    }
    return null;
  }
  
  public static final void a(Fragment paramFragment, int paramInt, String paramString, boolean paramBoolean)
  {
    k.b(paramFragment, "fragment");
    f localf = paramFragment.getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "fragment.activity ?: return");
    n localn = n.a.a(paramInt, paramString, paramBoolean);
    localn.setTargetFragment(paramFragment, 1001);
    a.a(localn, localf);
  }
  
  public static final boolean a(int paramInt1, int paramInt2, Intent paramIntent, b paramb)
  {
    int i = 0;
    int j = 1001;
    if (paramInt1 == j)
    {
      paramInt1 = -1;
      j = 1;
      Object localObject;
      if (paramInt2 == paramInt1)
      {
        paramInt1 = 1;
      }
      else
      {
        paramInt1 = 0;
        localObject = null;
      }
      paramInt2 = 0;
      boolean bool;
      if (paramInt1 == 0)
      {
        bool = false;
        paramIntent = null;
      }
      if (paramIntent != null)
      {
        paramInt1 = paramIntent.getIntExtra("speed_dial_key", 0);
        localObject = Integer.valueOf(paramInt1);
      }
      else
      {
        paramInt1 = 0;
        localObject = null;
      }
      paramIntent = new c/k/h;
      int k = 2;
      int m = 9;
      paramIntent.<init>(k, m);
      if (localObject != null)
      {
        k = ((Integer)localObject).intValue();
        bool = paramIntent.a(k);
        if (bool) {
          i = 1;
        }
      }
      if (i == 0)
      {
        paramInt1 = 0;
        localObject = null;
      }
      if (localObject != null)
      {
        localObject = (Number)localObject;
        paramInt1 = ((Number)localObject).intValue();
        if (paramb != null)
        {
          localObject = Integer.valueOf(paramInt1);
          paramb.invoke(localObject);
        }
      }
      return j;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */