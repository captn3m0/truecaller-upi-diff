package com.truecaller.calling.d;

import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private k(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static k a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    k localk = new com/truecaller/calling/d/k;
    localk.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */