package com.truecaller.calling.d;

public final class c
  implements q
{
  private final com.truecaller.i.c a;
  
  public c(com.truecaller.i.c paramc)
  {
    a = paramc;
  }
  
  private static String b(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>();
      throw ((Throwable)localIllegalArgumentException);
    case 9: 
      return "speed_dial_9";
    case 8: 
      return "speed_dial_8";
    case 7: 
      return "speed_dial_7";
    case 6: 
      return "speed_dial_6";
    case 5: 
      return "speed_dial_5";
    case 4: 
      return "speed_dial_4";
    case 3: 
      return "speed_dial_3";
    }
    return "speed_dial_2";
  }
  
  public final String a(int paramInt)
  {
    com.truecaller.i.c localc = a;
    String str = b(paramInt);
    return localc.a(str);
  }
  
  public final void a(int paramInt, String paramString)
  {
    com.truecaller.i.c localc = a;
    String str = b(paramInt);
    localc.a(str, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */