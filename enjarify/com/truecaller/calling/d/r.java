package com.truecaller.calling.d;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.p;

public final class r
  implements l.c
{
  private final c.f b;
  private final p c;
  private final com.truecaller.adapter_delegates.f d;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(r.class);
    ((u)localObject).<init>(localb, "recycler", "getRecycler()Landroid/support/v7/widget/RecyclerView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public r(i.b paramb, View paramView)
  {
    Object localObject1 = com.truecaller.utils.extensions.t.a(paramView, 2131364092);
    b = ((c.f)localObject1);
    localObject1 = new com/truecaller/adapter_delegates/p;
    paramb = (com.truecaller.adapter_delegates.b)paramb;
    Object localObject2 = new com/truecaller/calling/d/r$a;
    ((r.a)localObject2).<init>(this);
    localObject2 = (c.g.a.b)localObject2;
    c.g.a.b localb = (c.g.a.b)r.b.a;
    int i = 2131559037;
    ((p)localObject1).<init>(paramb, i, (c.g.a.b)localObject2, localb);
    c = ((p)localObject1);
    paramb = new com/truecaller/adapter_delegates/f;
    localObject1 = (a)c;
    paramb.<init>((a)localObject1);
    boolean bool1 = true;
    paramb.setHasStableIds(bool1);
    d = paramb;
    paramb = (RecyclerView)b.b();
    localObject1 = (RecyclerView.Adapter)d;
    paramb.setAdapter((RecyclerView.Adapter)localObject1);
    localObject1 = new android/support/v7/widget/LinearLayoutManager;
    localObject2 = paramView.getContext();
    ((LinearLayoutManager)localObject1).<init>((Context)localObject2);
    localObject1 = (RecyclerView.LayoutManager)localObject1;
    paramb.setLayoutManager((RecyclerView.LayoutManager)localObject1);
    localObject1 = paramb.getItemAnimator();
    boolean bool2 = localObject1 instanceof SimpleItemAnimator;
    if (!bool2)
    {
      bool1 = false;
      localObject1 = null;
    }
    localObject1 = (SimpleItemAnimator)localObject1;
    if (localObject1 != null)
    {
      bool2 = false;
      localObject2 = null;
      ((SimpleItemAnimator)localObject1).setSupportsChangeAnimations(false);
    }
    localObject1 = new com/truecaller/calling/d/e;
    paramView = paramView.getContext();
    k.a(paramView, "view.context");
    ((e)localObject1).<init>(paramView);
    localObject1 = (RecyclerView.ItemDecoration)localObject1;
    paramb.addItemDecoration((RecyclerView.ItemDecoration)localObject1);
  }
  
  public final void a(int paramInt)
  {
    com.truecaller.adapter_delegates.f localf = d;
    paramInt = c.a_(paramInt);
    localf.notifyItemChanged(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */