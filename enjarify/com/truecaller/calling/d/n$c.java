package com.truecaller.calling.d;

import android.content.Intent;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class n$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag f;
  
  n$c(n paramn, Intent paramIntent, int paramInt, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/calling/d/n$c;
    n localn = c;
    Intent localIntent = d;
    int i = e;
    localc.<init>(localn, localIntent, i, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label210;
      }
      paramObject = d;
      int j = e;
      int k = -1;
      int m = 1;
      if (j == k)
      {
        j = 1;
      }
      else
      {
        j = 0;
        localObject2 = null;
      }
      k = 0;
      if (j == 0) {
        paramObject = null;
      }
      if (paramObject == null) {
        break label206;
      }
      Object localObject2 = c;
      Uri localUri = ((Intent)paramObject).getData();
      a = paramObject;
      b = m;
      paramObject = a;
      localObject2 = new com/truecaller/calling/d/n$b;
      ((n.b)localObject2).<init>(localUri, null);
      localObject2 = (m)localObject2;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (String)paramObject;
    if (paramObject != null)
    {
      localObject1 = c;
      n.a((n)localObject1, (String)paramObject);
    }
    label206:
    return x.a;
    label210:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.n.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */