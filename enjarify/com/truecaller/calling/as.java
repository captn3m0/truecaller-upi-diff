package com.truecaller.calling;

import c.a.f;
import c.g.b.k;
import com.truecaller.i.c;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.utils.n;
import java.util.Iterator;
import java.util.List;

public final class as
  implements ar
{
  private final c a;
  private final h b;
  private final n c;
  
  public as(c paramc, h paramh, n paramn)
  {
    a = paramc;
    b = paramh;
    c = paramn;
  }
  
  private void a(String paramString)
  {
    k.b(paramString, "simToken");
    a.a("selectedCallSimToken", paramString);
    b.a(paramString);
  }
  
  public final void a()
  {
    int i = c();
    int j = -1;
    Object localObject;
    switch (i)
    {
    default: 
      i = 0;
      localObject = null;
      break;
    case 1: 
      i = -1;
      break;
    case 0: 
      i = 1;
    }
    if (i != j)
    {
      h localh = b;
      localObject = localh.a(i);
      if (localObject == null) {
        return;
      }
      k.a(localObject, "multiSimManager.getSimIn…ex(currentSlot) ?: return");
      localObject = b;
      k.a(localObject, "simInfo.simToken");
      a((String)localObject);
      return;
    }
    a("-1");
  }
  
  public final String b()
  {
    int i = c();
    Object localObject1 = null;
    int j = -1;
    Object localObject5;
    if (i != j)
    {
      Object localObject2 = b.a(i);
      if (localObject2 == null) {
        return null;
      }
      k.a(localObject2, "multiSimManager.getSimIn…rrentSlot) ?: return null");
      Object localObject3 = c.a(2130903061);
      Object localObject4 = "resourceProvider.getStri…ref_items_multi_sim_slot)";
      k.a(localObject3, (String)localObject4);
      localObject5 = (String)f.g((Object[])localObject3).get(i);
      localObject3 = (CharSequence)d;
      int k = 1;
      if (localObject3 != null)
      {
        m = ((CharSequence)localObject3).length();
        if (m != 0)
        {
          m = 0;
          localObject3 = null;
          break label127;
        }
      }
      int m = 1;
      label127:
      if (m == 0)
      {
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append((String)localObject5);
        ((StringBuilder)localObject3).append(" - ");
        localObject5 = d;
        ((StringBuilder)localObject3).append((String)localObject5);
        localObject5 = ((StringBuilder)localObject3).toString();
      }
      localObject2 = c;
      m = 2131888869;
      localObject4 = new Object[k];
      localObject4[0] = localObject5;
      localObject5 = ((n)localObject2).a(m, (Object[])localObject4);
      localObject1 = "resourceProvider.getStri…switched_to_sim, simName)";
      k.a(localObject5, (String)localObject1);
    }
    else
    {
      localObject5 = c;
      j = 2131888388;
      localObject1 = new Object[0];
      localObject5 = ((n)localObject5).a(j, (Object[])localObject1);
      localObject1 = "resourceProvider.getStri…ing.multi_sim_always_ask)";
      k.a(localObject5, (String)localObject1);
    }
    return (String)localObject5;
  }
  
  public final int c()
  {
    Object localObject1 = e();
    Object localObject2 = "-1";
    boolean bool = k.a(localObject2, localObject1);
    int i = -1;
    if (bool) {
      return i;
    }
    localObject2 = b;
    localObject1 = ((h)localObject2).b((String)localObject1);
    if (localObject1 == null) {
      return i;
    }
    k.a(localObject1, "multiSimManager.getSimIn…mManager.SIM_SLOT_UNKNOWN");
    return a;
  }
  
  public final int d()
  {
    int i = c();
    switch (i)
    {
    default: 
      return 2131234556;
    case 1: 
      return 2131234555;
    }
    return 2131234554;
  }
  
  public final String e()
  {
    Object localObject1 = b.g();
    k.a(localObject1, "multiSimManager.selectedCallSimToken");
    String str = "-1";
    boolean bool1 = k.a(localObject1, str);
    if (bool1)
    {
      str = a.b("selectedCallSimToken", "-1");
      k.a(str, "callingSettings.getStrin…KEN_UNKNOWN\n            )");
      Object localObject2 = b.h();
      Object localObject3 = "multiSimManager.allSimInfos";
      k.a(localObject2, (String)localObject3);
      localObject2 = ((Iterable)localObject2).iterator();
      boolean bool3;
      do
      {
        bool2 = ((Iterator)localObject2).hasNext();
        if (!bool2) {
          break;
        }
        localObject3 = ((Iterator)localObject2).next();
        Object localObject4 = localObject3;
        localObject4 = b;
        bool3 = k.a(localObject4, str);
      } while (!bool3);
      break label142;
      boolean bool2 = false;
      localObject3 = null;
      label142:
      localObject3 = (SimInfo)localObject3;
      if (localObject3 != null)
      {
        str = b;
        if (str != null) {
          localObject1 = str;
        }
      }
      return (String)localObject1;
    }
    return (String)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.as
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */