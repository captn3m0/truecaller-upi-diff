package com.truecaller.calling.a;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import c.g.b.k;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.e;
import java.util.ArrayList;

public final class b
  implements a
{
  public static final b.a a;
  private final ContentResolver b;
  
  static
  {
    b.a locala = new com/truecaller/calling/a/b$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public b(ContentResolver paramContentResolver)
  {
    b = paramContentResolver;
  }
  
  /* Error */
  public final com.truecaller.calling.a.a.a a(String paramString)
  {
    // Byte code:
    //   0: ldc 32
    //   2: astore_2
    //   3: aload_1
    //   4: aload_2
    //   5: invokestatic 25	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   8: invokestatic 38	java/lang/System:currentTimeMillis	()J
    //   11: lstore_3
    //   12: aload_1
    //   13: astore 5
    //   15: aload_1
    //   16: checkcast 40	java/lang/CharSequence
    //   19: astore 5
    //   21: aload 5
    //   23: invokestatic 45	c/n/m:a	(Ljava/lang/CharSequence;)Z
    //   26: istore 6
    //   28: aconst_null
    //   29: astore 7
    //   31: iload 6
    //   33: ifeq +5 -> 38
    //   36: aconst_null
    //   37: areturn
    //   38: aload_0
    //   39: getfield 30	com/truecaller/calling/a/b:b	Landroid/content/ContentResolver;
    //   42: astore 8
    //   44: invokestatic 50	com/truecaller/content/TruecallerContract$e:a	()Landroid/net/Uri;
    //   47: astore 9
    //   49: iconst_0
    //   50: istore 10
    //   52: aconst_null
    //   53: astore 11
    //   55: ldc 52
    //   57: astore 12
    //   59: iconst_1
    //   60: istore 6
    //   62: iload 6
    //   64: anewarray 55	java/lang/String
    //   67: astore 13
    //   69: aload 13
    //   71: iconst_0
    //   72: aload_1
    //   73: aastore
    //   74: aload 8
    //   76: aload 9
    //   78: aconst_null
    //   79: aload 12
    //   81: aload 13
    //   83: aconst_null
    //   84: invokevirtual 61	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   87: astore_1
    //   88: aload_1
    //   89: ifnull +171 -> 260
    //   92: aload_1
    //   93: checkcast 63	java/io/Closeable
    //   96: astore_1
    //   97: aload_1
    //   98: astore 8
    //   100: aload_1
    //   101: checkcast 65	android/database/Cursor
    //   104: astore 8
    //   106: new 67	com/truecaller/calling/a/d
    //   109: astore 9
    //   111: aload 9
    //   113: aload 8
    //   115: invokespecial 70	com/truecaller/calling/a/d:<init>	(Landroid/database/Cursor;)V
    //   118: aload 8
    //   120: invokeinterface 74 1 0
    //   125: istore 10
    //   127: iload 10
    //   129: ifeq +95 -> 224
    //   132: ldc 76
    //   134: astore 11
    //   136: aload 8
    //   138: aload 11
    //   140: invokestatic 25	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   143: new 78	com/truecaller/calling/a/a/a
    //   146: astore 11
    //   148: aload 9
    //   150: getfield 81	com/truecaller/calling/a/d:a	I
    //   153: istore 14
    //   155: aload 8
    //   157: iload 14
    //   159: invokeinterface 85 2 0
    //   164: astore 12
    //   166: ldc 87
    //   168: astore 13
    //   170: aload 12
    //   172: aload 13
    //   174: invokestatic 89	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   177: aload 9
    //   179: getfield 91	com/truecaller/calling/a/d:b	I
    //   182: istore 15
    //   184: aload 8
    //   186: iload 15
    //   188: invokeinterface 95 2 0
    //   193: istore 16
    //   195: iload 16
    //   197: ifle +9 -> 206
    //   200: iconst_1
    //   201: istore 16
    //   203: goto +9 -> 212
    //   206: iconst_0
    //   207: istore 16
    //   209: aconst_null
    //   210: astore 8
    //   212: aload 11
    //   214: aload 12
    //   216: iload 16
    //   218: invokespecial 98	com/truecaller/calling/a/a/a:<init>	(Ljava/lang/String;Z)V
    //   221: goto +9 -> 230
    //   224: iconst_0
    //   225: istore 10
    //   227: aconst_null
    //   228: astore 11
    //   230: aload_1
    //   231: aconst_null
    //   232: invokestatic 103	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   235: aload 11
    //   237: astore 7
    //   239: goto +21 -> 260
    //   242: astore_2
    //   243: goto +9 -> 252
    //   246: astore_2
    //   247: aload_2
    //   248: astore 7
    //   250: aload_2
    //   251: athrow
    //   252: aload_1
    //   253: aload 7
    //   255: invokestatic 103	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   258: aload_2
    //   259: athrow
    //   260: invokestatic 38	java/lang/System:currentTimeMillis	()J
    //   263: lload_3
    //   264: lsub
    //   265: lstore 17
    //   267: iload 6
    //   269: anewarray 55	java/lang/String
    //   272: astore_1
    //   273: new 105	java/lang/StringBuilder
    //   276: astore_2
    //   277: aload_2
    //   278: ldc 107
    //   280: invokespecial 110	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   283: aload_2
    //   284: lload 17
    //   286: invokevirtual 114	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   289: pop
    //   290: aload_2
    //   291: ldc 116
    //   293: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   296: pop
    //   297: aload_2
    //   298: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   301: astore_2
    //   302: aload_1
    //   303: iconst_0
    //   304: aload_2
    //   305: aastore
    //   306: aload 7
    //   308: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	309	0	this	b
    //   0	309	1	paramString	String
    //   2	3	2	str1	String
    //   242	1	2	localObject1	Object
    //   246	13	2	localObject2	Object
    //   276	29	2	localObject3	Object
    //   11	253	3	l1	long
    //   13	9	5	localObject4	Object
    //   26	242	6	bool1	boolean
    //   29	278	7	localObject5	Object
    //   42	169	8	localObject6	Object
    //   47	131	9	localObject7	Object
    //   50	176	10	bool2	boolean
    //   53	183	11	localObject8	Object
    //   57	158	12	str2	String
    //   67	106	13	localObject9	Object
    //   153	5	14	i	int
    //   182	5	15	j	int
    //   193	24	16	k	int
    //   265	20	17	l2	long
    // Exception table:
    //   from	to	target	type
    //   250	252	242	finally
    //   100	104	246	finally
    //   106	109	246	finally
    //   113	118	246	finally
    //   118	125	246	finally
    //   138	143	246	finally
    //   143	146	246	finally
    //   148	153	246	finally
    //   157	164	246	finally
    //   172	177	246	finally
    //   177	182	246	finally
    //   186	193	246	finally
    //   216	221	246	finally
  }
  
  public final void a()
  {
    Object localObject = ContentProviderOperation.newDelete(TruecallerContract.e.a()).build();
    ContentResolver localContentResolver = b;
    String str = TruecallerContract.a();
    ContentProviderOperation[] arrayOfContentProviderOperation = new ContentProviderOperation[1];
    arrayOfContentProviderOperation[0] = localObject;
    localObject = c.a.m.d(arrayOfContentProviderOperation);
    localContentResolver.applyBatch(str, (ArrayList)localObject);
  }
  
  public final void a(com.truecaller.calling.a.a.a parama)
  {
    Object localObject1 = "contactSettings";
    k.b(parama, (String)localObject1);
    long l1 = System.currentTimeMillis();
    Object localObject2 = (CharSequence)a;
    boolean bool = c.n.m.a((CharSequence)localObject2);
    if (bool) {
      return;
    }
    localObject2 = ContentProviderOperation.newInsert(TruecallerContract.e.a());
    parama = parama.a();
    parama = ((ContentProviderOperation.Builder)localObject2).withValues(parama).build();
    localObject2 = b;
    String str = TruecallerContract.a();
    int i = 1;
    ContentProviderOperation[] arrayOfContentProviderOperation = new ContentProviderOperation[i];
    arrayOfContentProviderOperation[0] = parama;
    parama = c.a.m.d(arrayOfContentProviderOperation);
    ((ContentResolver)localObject2).applyBatch(str, parama);
    long l2 = System.currentTimeMillis() - l1;
    parama = new String[i];
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("insertContactSettings, query took: ");
    ((StringBuilder)localObject1).append(l2);
    ((StringBuilder)localObject1).append("ms");
    localObject1 = ((StringBuilder)localObject1).toString();
    parama[0] = localObject1;
  }
  
  public final void b(com.truecaller.calling.a.a.a parama)
  {
    Object localObject1 = "contactSettings";
    k.b(parama, (String)localObject1);
    long l1 = System.currentTimeMillis();
    Object localObject2 = (CharSequence)a;
    boolean bool = c.n.m.a((CharSequence)localObject2);
    if (bool) {
      return;
    }
    localObject2 = ContentProviderOperation.newUpdate(TruecallerContract.e.a());
    int i = 1;
    Object localObject3 = new String[i];
    String str1 = a;
    localObject3[0] = str1;
    localObject2 = ((ContentProviderOperation.Builder)localObject2).withSelection("tc_id = ?", (String[])localObject3);
    parama = parama.a();
    parama = ((ContentProviderOperation.Builder)localObject2).withValues(parama).build();
    localObject2 = b;
    String str2 = TruecallerContract.a();
    localObject3 = new ContentProviderOperation[i];
    localObject3[0] = parama;
    parama = c.a.m.d((Object[])localObject3);
    ((ContentResolver)localObject2).applyBatch(str2, parama);
    long l2 = System.currentTimeMillis() - l1;
    parama = new String[i];
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("updateContactSettings, query took: ");
    ((StringBuilder)localObject1).append(l2);
    ((StringBuilder)localObject1).append("ms");
    localObject1 = ((StringBuilder)localObject1).toString();
    parama[0] = localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */