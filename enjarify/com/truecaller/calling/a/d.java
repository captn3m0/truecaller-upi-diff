package com.truecaller.calling.a;

import android.database.Cursor;

public final class d
{
  final int a;
  final int b;
  
  public d(Cursor paramCursor)
  {
    int i = -1;
    int j;
    if (paramCursor != null)
    {
      String str1 = "tc_id";
      j = paramCursor.getColumnIndex(str1);
    }
    else
    {
      j = -1;
    }
    a = j;
    if (paramCursor != null)
    {
      String str2 = "hidden_from_identified";
      i = paramCursor.getColumnIndex(str2);
    }
    b = i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */