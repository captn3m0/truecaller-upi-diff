package com.truecaller.calling.a.a;

import android.content.ContentValues;

public final class a
{
  public String a;
  public boolean b;
  
  public a(String paramString, boolean paramBoolean)
  {
    a = paramString;
    b = paramBoolean;
  }
  
  public final ContentValues a()
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Object localObject = a;
    localContentValues.put("tc_id", (String)localObject);
    localObject = Boolean.valueOf(b);
    localContentValues.put("hidden_from_identified", (Boolean)localObject);
    return localContentValues;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */