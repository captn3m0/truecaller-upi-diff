package com.truecaller.calling;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l;
import c.l.g;
import c.n.m;
import com.truecaller.calling.dialer.CallIconType;
import com.truecaller.common.h.j;

public final class av
  implements az, h
{
  private final Drawable A;
  private Integer B;
  private boolean C;
  private boolean D;
  private boolean E;
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final f f;
  private final f g;
  private final f h;
  private final f i;
  private final f j;
  private final f k;
  private final ColorStateList l;
  private final ColorStateList m;
  private final Drawable n;
  private final Drawable o;
  private final Drawable p;
  private final Drawable q;
  private final Drawable r;
  private final Drawable s;
  private final Drawable t;
  private final Drawable u;
  private final Drawable v;
  private final Drawable w;
  private final Drawable x;
  private final Drawable y;
  private final Drawable z;
  
  static
  {
    g[] arrayOfg = new g[10];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(av.class);
    ((u)localObject).<init>(localb, "timestampText", "getTimestampText()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(av.class);
    ((u)localObject).<init>(localb, "callTypeIcon", "getCallTypeIcon()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(av.class);
    ((u)localObject).<init>(localb, "delimiter", "getDelimiter()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(av.class);
    ((u)localObject).<init>(localb, "simIndicator", "getSimIndicator()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[3] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(av.class);
    ((u)localObject).<init>(localb, "videoCallIndicator", "getVideoCallIndicator()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[4] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(av.class);
    ((u)localObject).<init>(localb, "callRecordingIndicator", "getCallRecordingIndicator()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[5] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(av.class);
    ((u)localObject).<init>(localb, "subtitle", "getSubtitle()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[6] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(av.class);
    ((u)localObject).<init>(localb, "groupCountText", "getGroupCountText()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[7] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(av.class);
    ((u)localObject).<init>(localb, "callDurationView", "getCallDurationView()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[8] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(av.class);
    ((u)localObject).<init>(localb, "delimiterTwo", "getDelimiterTwo()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[9] = localObject;
    a = arrayOfg;
  }
  
  public av(View paramView)
  {
    Object localObject = com.truecaller.utils.extensions.t.a(paramView, 2131364872);
    b = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131362385);
    c = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131362804);
    d = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131364520);
    e = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131365423);
    f = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131363765);
    g = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131363509);
    h = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131362608);
    i = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131363504);
    j = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131362805);
    k = ((f)localObject);
    localObject = paramView.getContext();
    int i1 = 2130969585;
    localObject = com.truecaller.utils.ui.b.b((Context)localObject, i1);
    l = ((ColorStateList)localObject);
    localObject = com.truecaller.utils.ui.b.b(paramView.getContext(), 2130969592);
    m = ((ColorStateList)localObject);
    localObject = paramView.getContext();
    int i2 = 2130968889;
    localObject = com.truecaller.utils.ui.b.a((Context)localObject, 2131233892, i2);
    n = ((Drawable)localObject);
    localObject = com.truecaller.utils.ui.b.a(paramView.getContext(), 2131234295, i2);
    o = ((Drawable)localObject);
    localObject = paramView.getContext();
    int i3 = 2130969533;
    localObject = com.truecaller.utils.ui.b.a((Context)localObject, 2131234205, i3);
    p = ((Drawable)localObject);
    localObject = com.truecaller.utils.ui.b.a(paramView.getContext(), 2131234352, i3);
    q = ((Drawable)localObject);
    localObject = com.truecaller.utils.ui.b.a(paramView.getContext(), 2131234275, i2);
    r = ((Drawable)localObject);
    localObject = com.truecaller.utils.ui.b.a(paramView.getContext(), 2131234140, 2130969528);
    s = ((Drawable)localObject);
    localObject = paramView.getContext();
    i2 = 2130969593;
    i3 = 2131234549;
    localObject = com.truecaller.utils.ui.b.a((Context)localObject, i3, i2);
    t = ((Drawable)localObject);
    localObject = paramView.getContext();
    int i4 = 2131234550;
    localObject = com.truecaller.utils.ui.b.a((Context)localObject, i4, i2);
    u = ((Drawable)localObject);
    localObject = com.truecaller.utils.ui.b.a(paramView.getContext(), i3, i1);
    v = ((Drawable)localObject);
    localObject = com.truecaller.utils.ui.b.a(paramView.getContext(), i4, i1);
    w = ((Drawable)localObject);
    localObject = paramView.getContext();
    i2 = 2130969183;
    i3 = 2131234658;
    localObject = com.truecaller.utils.ui.b.a((Context)localObject, i3, i2);
    x = ((Drawable)localObject);
    localObject = com.truecaller.utils.ui.b.a(paramView.getContext(), i3, i1);
    y = ((Drawable)localObject);
    localObject = paramView.getContext();
    i3 = 2131233801;
    localObject = com.truecaller.utils.ui.b.a((Context)localObject, i3, i2);
    z = ((Drawable)localObject);
    paramView = com.truecaller.utils.ui.b.a(paramView.getContext(), i3, i1);
    A = paramView;
    paramView = a();
    k.a(paramView, "timestampText");
    com.truecaller.utils.extensions.t.a((View)paramView);
    paramView = b();
    k.a(paramView, "callTypeIcon");
    com.truecaller.utils.extensions.t.a((View)paramView);
  }
  
  private final TextView a()
  {
    return (TextView)b.b();
  }
  
  private final void a(TextView paramTextView)
  {
    boolean bool = C;
    ColorStateList localColorStateList;
    if (bool) {
      localColorStateList = l;
    } else {
      localColorStateList = m;
    }
    paramTextView.setTextColor(localColorStateList);
  }
  
  private final ImageView b()
  {
    return (ImageView)c.b();
  }
  
  private final TextView c()
  {
    return (TextView)d.b();
  }
  
  private final ImageView d()
  {
    return (ImageView)e.b();
  }
  
  private final ImageView e()
  {
    return (ImageView)f.b();
  }
  
  private final ImageView f()
  {
    return (ImageView)g.b();
  }
  
  private final TextView g()
  {
    return (TextView)h.b();
  }
  
  private final TextView h()
  {
    return (TextView)i.b();
  }
  
  private final TextView i()
  {
    return (TextView)j.b();
  }
  
  private final TextView j()
  {
    return (TextView)k.b();
  }
  
  private final void k()
  {
    Object localObject = B;
    int i1;
    if (localObject != null)
    {
      i1 = ((Integer)localObject).intValue();
      if (i1 == 0)
      {
        boolean bool1 = C;
        if (bool1)
        {
          localObject = v;
          break label94;
        }
        localObject = t;
        break label94;
      }
    }
    if (localObject != null)
    {
      int i2 = ((Integer)localObject).intValue();
      i1 = 1;
      if (i2 == i1)
      {
        bool2 = C;
        if (bool2)
        {
          localObject = w;
          break label94;
        }
        localObject = u;
        break label94;
      }
    }
    boolean bool2 = false;
    localObject = null;
    label94:
    if (localObject != null)
    {
      ImageView localImageView = d();
      k.a(localImageView, "simIndicator");
      com.truecaller.utils.extensions.t.a((View)localImageView);
      d().setImageDrawable((Drawable)localObject);
      return;
    }
    localObject = d();
    k.a(localObject, "simIndicator");
    com.truecaller.utils.extensions.t.b((View)localObject);
  }
  
  private final void l()
  {
    boolean bool1 = D;
    if (bool1)
    {
      localImageView = e();
      Object localObject = "videoCallIndicator";
      k.a(localImageView, (String)localObject);
      com.truecaller.utils.extensions.t.a((View)localImageView);
      localImageView = e();
      boolean bool2 = C;
      if (bool2) {
        localObject = y;
      } else {
        localObject = x;
      }
      localImageView.setImageDrawable((Drawable)localObject);
      return;
    }
    ImageView localImageView = e();
    k.a(localImageView, "videoCallIndicator");
    com.truecaller.utils.extensions.t.b((View)localImageView);
  }
  
  public final void a(CallIconType paramCallIconType)
  {
    k.b(paramCallIconType, "callIconType");
    int[] arrayOfInt = aw.a;
    int i1 = paramCallIconType.ordinal();
    i1 = arrayOfInt[i1];
    switch (i1)
    {
    default: 
      paramCallIconType = new c/l;
      paramCallIconType.<init>();
      throw paramCallIconType;
    case 6: 
      paramCallIconType = s;
      break;
    case 5: 
      paramCallIconType = r;
      break;
    case 4: 
      paramCallIconType = q;
      break;
    case 3: 
      paramCallIconType = p;
      break;
    case 2: 
      paramCallIconType = o;
      break;
    case 1: 
      paramCallIconType = n;
    }
    b().setImageDrawable(paramCallIconType);
  }
  
  public final void a(Integer paramInteger)
  {
    B = paramInteger;
    k();
  }
  
  public final void a(Long paramLong)
  {
    boolean bool;
    CharSequence localCharSequence;
    if (paramLong != null)
    {
      bool = true;
    }
    else
    {
      bool = false;
      localCharSequence = null;
    }
    Object localObject1 = i();
    k.a(localObject1, "callDurationView");
    com.truecaller.utils.extensions.t.a((View)localObject1, bool);
    localObject1 = j();
    Object localObject2 = "delimiterTwo";
    k.a(localObject1, (String)localObject2);
    localObject1 = (View)localObject1;
    com.truecaller.utils.extensions.t.a((View)localObject1, bool);
    if (paramLong != null)
    {
      long l1 = ((Number)paramLong).longValue();
      paramLong = i();
      k.a(paramLong, "callDurationView");
      localObject2 = i();
      k.a(localObject2, "callDurationView");
      localCharSequence = (CharSequence)j.c(((TextView)localObject2).getContext(), l1);
      paramLong.setText(localCharSequence);
      return;
    }
  }
  
  public final void b(Integer paramInteger)
  {
    if (paramInteger != null)
    {
      Object localObject1 = paramInteger;
      ((Number)paramInteger).intValue();
      localObject1 = h();
      k.a(localObject1, "groupCountText");
      com.truecaller.utils.extensions.t.a((View)localObject1);
      localObject1 = h();
      k.a(localObject1, "groupCountText");
      Object localObject2 = h();
      k.a(localObject2, "groupCountText");
      localObject2 = ((TextView)localObject2).getContext();
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = paramInteger;
      paramInteger = (CharSequence)((Context)localObject2).getString(2131888175, arrayOfObject);
      ((TextView)localObject1).setText(paramInteger);
      return;
    }
    paramInteger = h();
    k.a(paramInteger, "groupCountText");
    com.truecaller.utils.extensions.t.b((View)paramInteger);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "timestamp");
    TextView localTextView = a();
    k.a(localTextView, "timestampText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b_(boolean paramBoolean)
  {
    C = paramBoolean;
    TextView localTextView = a();
    k.a(localTextView, "timestampText");
    a(localTextView);
    k();
    localTextView = c();
    k.a(localTextView, "delimiter");
    a(localTextView);
    localTextView = g();
    k.a(localTextView, "subtitle");
    a(localTextView);
    localTextView = h();
    k.a(localTextView, "groupCountText");
    a(localTextView);
    l();
    localTextView = i();
    k.a(localTextView, "callDurationView");
    a(localTextView);
    localTextView = j();
    k.a(localTextView, "delimiterTwo");
    a(localTextView);
  }
  
  public final void e(boolean paramBoolean)
  {
    D = paramBoolean;
    l();
  }
  
  public final void e_(String paramString)
  {
    paramString = (CharSequence)paramString;
    TextView localTextView;
    if (paramString != null)
    {
      bool = m.a(paramString);
      if (!bool)
      {
        bool = false;
        localTextView = null;
        break label30;
      }
    }
    boolean bool = true;
    label30:
    if (!bool)
    {
      localTextView = g();
      k.a(localTextView, "subtitle");
      com.truecaller.utils.extensions.t.a((View)localTextView);
      localTextView = c();
      k.a(localTextView, "delimiter");
      com.truecaller.utils.extensions.t.a((View)localTextView);
      localTextView = g();
      k.a(localTextView, "subtitle");
      localTextView.setText(paramString);
      return;
    }
    paramString = g();
    k.a(paramString, "subtitle");
    com.truecaller.utils.extensions.t.b((View)paramString);
    paramString = c();
    k.a(paramString, "delimiter");
    com.truecaller.utils.extensions.t.b((View)paramString);
  }
  
  public final void f(boolean paramBoolean)
  {
    E = paramBoolean;
    paramBoolean = E;
    if (paramBoolean)
    {
      localImageView = f();
      Object localObject = "callRecordingIndicator";
      k.a(localImageView, (String)localObject);
      com.truecaller.utils.extensions.t.a((View)localImageView);
      localImageView = f();
      boolean bool = C;
      if (bool) {
        localObject = A;
      } else {
        localObject = z;
      }
      localImageView.setImageDrawable((Drawable)localObject);
      return;
    }
    ImageView localImageView = f();
    k.a(localImageView, "callRecordingIndicator");
    com.truecaller.utils.extensions.t.b((View)localImageView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.av
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */