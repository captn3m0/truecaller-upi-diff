package com.truecaller.calling;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import com.truecaller.calling.dialer.CallIconType;
import com.truecaller.common.h.j;
import com.truecaller.utils.extensions.p;

public final class ap
  implements ag
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final f f;
  private final f g;
  private final f h;
  private final f i;
  private final Drawable j;
  private final Drawable k;
  private final Drawable l;
  private final Drawable m;
  private final Drawable n;
  private final Drawable o;
  
  static
  {
    g[] arrayOfg = new g[8];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(ap.class);
    ((u)localObject).<init>(localb, "timestampText", "getTimestampText()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(ap.class);
    ((u)localObject).<init>(localb, "callTypeIcon", "getCallTypeIcon()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(ap.class);
    ((u)localObject).<init>(localb, "delimiter", "getDelimiter()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(ap.class);
    ((u)localObject).<init>(localb, "simIndicator", "getSimIndicator()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[3] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(ap.class);
    ((u)localObject).<init>(localb, "videoCallIndicator", "getVideoCallIndicator()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[4] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(ap.class);
    ((u)localObject).<init>(localb, "subtitle", "getSubtitle()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[5] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(ap.class);
    ((u)localObject).<init>(localb, "callDurationView", "getCallDurationView()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[6] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(ap.class);
    ((u)localObject).<init>(localb, "delimiterTwo", "getDelimiterTwo()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[7] = localObject;
    a = arrayOfg;
  }
  
  public ap(View paramView)
  {
    Object localObject = com.truecaller.utils.extensions.t.a(paramView, 2131364872);
    b = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131362385);
    c = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131362804);
    d = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131364520);
    e = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131365423);
    f = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131363509);
    g = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131363504);
    h = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131362805);
    i = ((f)localObject);
    localObject = paramView.getContext();
    int i1 = 2130969533;
    localObject = com.truecaller.utils.ui.b.a((Context)localObject, 2131234205, i1);
    j = ((Drawable)localObject);
    localObject = com.truecaller.utils.ui.b.a(paramView.getContext(), 2131234352, i1);
    k = ((Drawable)localObject);
    localObject = com.truecaller.utils.ui.b.a(paramView.getContext(), 2131234275, 2130968889);
    l = ((Drawable)localObject);
    localObject = paramView.getContext();
    i1 = 2130969593;
    localObject = com.truecaller.utils.ui.b.a((Context)localObject, 2131234549, i1);
    m = ((Drawable)localObject);
    localObject = com.truecaller.utils.ui.b.a(paramView.getContext(), 2131234550, i1);
    n = ((Drawable)localObject);
    paramView = com.truecaller.utils.ui.b.a(paramView.getContext(), 2131234658, 2130969183);
    o = paramView;
  }
  
  private final ImageView a()
  {
    return (ImageView)c.b();
  }
  
  private final TextView b()
  {
    return (TextView)d.b();
  }
  
  private final ImageView c()
  {
    return (ImageView)e.b();
  }
  
  private final ImageView d()
  {
    return (ImageView)f.b();
  }
  
  private final TextView e()
  {
    return (TextView)h.b();
  }
  
  public final void a(CallIconType paramCallIconType)
  {
    if (paramCallIconType != null)
    {
      localObject = aq.a;
      i1 = paramCallIconType.ordinal();
      i1 = localObject[i1];
      switch (i1)
      {
      default: 
        break;
      case 3: 
        paramCallIconType = l;
        break;
      case 2: 
        paramCallIconType = k;
        break;
      case 1: 
        paramCallIconType = j;
        break;
      }
    }
    int i1 = 0;
    paramCallIconType = null;
    a().setImageDrawable(paramCallIconType);
    Object localObject = a();
    String str = "callTypeIcon";
    k.a(localObject, str);
    localObject = (View)localObject;
    if (paramCallIconType != null)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      paramCallIconType = null;
    }
    com.truecaller.utils.extensions.t.a((View)localObject, i1);
  }
  
  public final void a(Integer paramInteger)
  {
    int i1 = 1;
    if (paramInteger != null)
    {
      int i2 = paramInteger.intValue();
      if (i2 == 0)
      {
        paramInteger = m;
        break label58;
      }
    }
    if (paramInteger != null)
    {
      int i3 = paramInteger.intValue();
      if (i3 == i1)
      {
        paramInteger = n;
        break label58;
      }
    }
    int i4 = 0;
    paramInteger = null;
    label58:
    c().setImageDrawable(paramInteger);
    Object localObject = c();
    String str = "simIndicator";
    k.a(localObject, str);
    localObject = (View)localObject;
    if (paramInteger == null) {
      i1 = 0;
    }
    com.truecaller.utils.extensions.t.a((View)localObject, i1);
  }
  
  public final void a(Long paramLong)
  {
    Object localObject1 = e();
    k.a(localObject1, "callDurationView");
    Object localObject2 = e();
    String str1 = "callDurationView";
    k.a(localObject2, str1);
    localObject2 = ((TextView)localObject2).getContext();
    long l1;
    if (paramLong != null) {
      l1 = paramLong.longValue();
    } else {
      l1 = 0L;
    }
    localObject2 = (CharSequence)j.c((Context)localObject2, l1);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject1 = e();
    localObject2 = "callDurationView";
    k.a(localObject1, (String)localObject2);
    localObject1 = (View)localObject1;
    boolean bool1 = true;
    str1 = null;
    boolean bool2;
    if (paramLong != null)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      str2 = null;
    }
    com.truecaller.utils.extensions.t.a((View)localObject1, bool2);
    localObject1 = (TextView)i.b();
    String str2 = "delimiterTwo";
    k.a(localObject1, str2);
    localObject1 = (View)localObject1;
    if (paramLong == null)
    {
      bool1 = false;
      localObject2 = null;
    }
    com.truecaller.utils.extensions.t.a((View)localObject1, bool1);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = d();
    k.a(localObject, "videoCallIndicator");
    com.truecaller.utils.extensions.t.a((View)localObject, paramBoolean);
    ImageView localImageView = d();
    localObject = o;
    localImageView.setImageDrawable((Drawable)localObject);
  }
  
  public final void b(String paramString)
  {
    TextView localTextView = (TextView)b.b();
    k.a(localTextView, "timestampText");
    p.a(localTextView, paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    TextView localTextView = b();
    k.a(localTextView, "delimiter");
    com.truecaller.utils.extensions.t.a((View)localTextView, paramBoolean);
  }
  
  public final void e_(String paramString)
  {
    Object localObject = (TextView)g.b();
    k.a(localObject, "subtitle");
    p.a((TextView)localObject, paramString);
    localObject = b();
    String str = "delimiter";
    k.a(localObject, str);
    localObject = (View)localObject;
    boolean bool;
    if (paramString != null)
    {
      bool = true;
    }
    else
    {
      bool = false;
      paramString = null;
    }
    com.truecaller.utils.extensions.t.a((View)localObject, bool);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */