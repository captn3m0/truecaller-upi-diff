package com.truecaller.calling;

import android.view.View;
import c.a.m;
import c.f;
import c.g.b.u;
import c.l.b;
import c.l.g;
import com.truecaller.flashsdk.ui.FlashButton;
import java.util.List;

public final class y
  implements w
{
  private final f b;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = c.g.b.w.a(y.class);
    ((u)localObject).<init>(localb, "flashButton", "getFlashButton()Lcom/truecaller/flashsdk/ui/FlashButton;");
    localObject = (g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public y(View paramView)
  {
    paramView = com.truecaller.utils.extensions.t.a(paramView, 2131363109);
    b = paramView;
  }
  
  public final void a(x paramx)
  {
    if (paramx != null)
    {
      localObject = a;
      if (localObject != null)
      {
        localObject = (Long)m.e((List)localObject);
        break label26;
      }
    }
    Object localObject = null;
    label26:
    FlashButton localFlashButton = (FlashButton)b.b();
    long l;
    if (localObject != null) {
      l = ((Long)localObject).longValue();
    } else {
      l = 0L;
    }
    if (paramx != null)
    {
      localObject = ae.a(paramx);
      if (localObject != null) {}
    }
    else
    {
      localObject = "";
    }
    if (paramx != null)
    {
      paramx = c;
      if (paramx != null) {}
    }
    else
    {
      paramx = "";
    }
    localFlashButton.a(l, (String)localObject, paramx);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */