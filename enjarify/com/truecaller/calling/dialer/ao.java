package com.truecaller.calling.dialer;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Build.VERSION;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.d;
import android.support.transition.n;
import android.support.transition.p;
import android.support.v7.app.AlertDialog.Builder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.ui.keyboard.DialerKeypad;
import com.truecaller.ui.view.BottomBar;
import com.truecaller.ui.view.BottomBar.DialpadState;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.util.an;
import com.truecaller.util.ax;
import com.truecaller.util.ca;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class ao
  implements aj.d, a
{
  final aj.d.a a;
  private an b;
  private final ConstraintLayout c;
  private final ViewGroup d;
  private final BottomBar e;
  private HashMap f;
  
  public ao(ConstraintLayout paramConstraintLayout, aj.d.a parama, ViewGroup paramViewGroup, BottomBar paramBottomBar)
  {
    c = paramConstraintLayout;
    a = parama;
    d = paramViewGroup;
    e = paramBottomBar;
    int i = R.id.inputField;
    paramConstraintLayout = (SelectionAwareEditText)a(i);
    parama = new com/truecaller/calling/dialer/ao$a;
    parama.<init>(this);
    parama = (TextWatcher)parama;
    paramConstraintLayout.addTextChangedListener(parama);
    parama = (SelectionAwareEditText.a)a;
    paramConstraintLayout.setSelectionChangeListener(parama);
    parama = new com/truecaller/util/ax;
    paramViewGroup = paramConstraintLayout;
    paramViewGroup = (EditText)paramConstraintLayout;
    parama.<init>(paramViewGroup);
    parama = (View.OnTouchListener)parama;
    paramConstraintLayout.setOnTouchListener(parama);
    int j = Build.VERSION.SDK_INT;
    int k = 26;
    if (j >= k)
    {
      j = 0;
      parama = null;
      paramConstraintLayout.setShowSoftInputOnFocus(false);
    }
    i = R.id.dialpad;
    paramConstraintLayout = (DialerKeypad)a(i);
    parama = (com.truecaller.ui.keyboard.c)a;
    paramConstraintLayout.setDialpadListener(parama);
    parama = (com.truecaller.ui.keyboard.b)a;
    paramConstraintLayout.setActionsListener(parama);
    i = R.id.delete;
    paramConstraintLayout = (TintedImageView)a(i);
    parama = new com/truecaller/calling/dialer/ao$b;
    parama.<init>(this);
    parama = (View.OnClickListener)parama;
    paramConstraintLayout.setOnClickListener(parama);
    parama = new com/truecaller/calling/dialer/ao$c;
    parama.<init>(this);
    parama = (View.OnLongClickListener)parama;
    paramConstraintLayout.setOnLongClickListener(parama);
    i = R.id.addContact;
    paramConstraintLayout = (TintedImageView)a(i);
    parama = new com/truecaller/calling/dialer/ao$1;
    parama.<init>(this);
    parama = (View.OnClickListener)parama;
    paramConstraintLayout.setOnClickListener(parama);
    i = R.id.tapToPasteContainer;
    paramConstraintLayout = (LinearLayout)a(i);
    parama = new com/truecaller/calling/dialer/ao$2;
    parama.<init>(this);
    parama = (View.OnClickListener)parama;
    paramConstraintLayout.setOnClickListener(parama);
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    int i = R.id.inputField;
    SelectionAwareEditText localSelectionAwareEditText = (SelectionAwareEditText)a(i);
    k.a(localSelectionAwareEditText, "inputField");
    localSelectionAwareEditText.getEditableText().delete(paramInt1, paramInt2);
  }
  
  public final void a(int paramInt1, int paramInt2, String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.inputField;
    Object localObject = (SelectionAwareEditText)a(i);
    k.a(localObject, "inputField");
    localObject = ((SelectionAwareEditText)localObject).getEditableText();
    paramString = (CharSequence)paramString;
    ((Editable)localObject).replace(paramInt1, paramInt2, paramString);
  }
  
  public final void a(aj.a.b paramb)
  {
    k.b(paramb, "mode");
    int i = R.id.inputFieldContainer;
    Object localObject1 = (LinearLayout)a(i);
    k.a(localObject1, "inputFieldContainer");
    localObject1 = (View)localObject1;
    boolean bool = paramb instanceof aj.a.b.b;
    t.a((View)localObject1, bool);
    i = R.id.tapToPasteContainer;
    localObject1 = (LinearLayout)a(i);
    Object localObject2 = "tapToPasteContainer";
    k.a(localObject1, (String)localObject2);
    localObject1 = (View)localObject1;
    bool = paramb instanceof aj.a.b.c;
    t.a((View)localObject1, bool);
    i = R.id.tapToPasteNumber;
    localObject1 = (TextView)a(i);
    String str = "tapToPasteNumber";
    k.a(localObject1, str);
    if (bool)
    {
      int j = R.id.tapToPasteNumber;
      localObject2 = (TextView)a(j);
      str = "tapToPasteNumber";
      k.a(localObject2, str);
      localObject2 = ((TextView)localObject2).getContext();
      int k = 2131887278;
      int m = 1;
      Object[] arrayOfObject = new Object[m];
      paramb = a;
      arrayOfObject[0] = paramb;
      paramb = (CharSequence)((Context)localObject2).getString(k, arrayOfObject);
    }
    else
    {
      paramb = (CharSequence)"";
    }
    ((TextView)localObject1).setText(paramb);
  }
  
  public final void a(BottomBar.DialpadState paramDialpadState)
  {
    k.b(paramDialpadState, "state");
    e.setDialpadState(paramDialpadState);
  }
  
  public final void a(ca paramca)
  {
    k.b(paramca, "sequenceResponse");
    Context localContext = d.getContext();
    k.a(localContext, "containerView.context");
    paramca.a(localContext);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.inputField;
    Object localObject = (SelectionAwareEditText)a(i);
    k.a(localObject, "inputField");
    localObject = ((SelectionAwareEditText)localObject).getEditableText();
    paramString = (CharSequence)paramString;
    ((Editable)localObject).append(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject1 = (ViewGroup)c;
    Object localObject2 = new android/support/transition/c;
    ((android.support.transition.c)localObject2).<init>();
    ((android.support.transition.c)localObject2).a(2131362833, true);
    long l = 100;
    ((android.support.transition.c)localObject2).c(l);
    Object localObject3 = new android/view/animation/AccelerateDecelerateInterpolator;
    ((AccelerateDecelerateInterpolator)localObject3).<init>();
    localObject3 = (TimeInterpolator)localObject3;
    ((android.support.transition.c)localObject2).b((TimeInterpolator)localObject3);
    localObject2 = (n)localObject2;
    p.a((ViewGroup)localObject1, (n)localObject2);
    localObject1 = e;
    boolean bool = paramBoolean ^ true;
    ((BottomBar)localObject1).setShadowVisibility(bool);
    localObject1 = new android/support/constraint/d;
    ((d)localObject1).<init>();
    localObject2 = c;
    ((d)localObject1).a((ConstraintLayout)localObject2);
    int i = d.getId();
    int j = 3;
    ((d)localObject1).b(i, j);
    i = d.getId();
    int k = 4;
    ((d)localObject1).b(i, k);
    localObject2 = d;
    i = ((ViewGroup)localObject2).getId();
    if (paramBoolean) {
      j = 4;
    }
    ((d)localObject1).a(i, j);
    localObject2 = d;
    i = ((ViewGroup)localObject2).getId();
    if (paramBoolean)
    {
      paramBoolean = false;
      localConstraintLayout = null;
    }
    else
    {
      paramBoolean = true;
    }
    ((d)localObject1).c(i, paramBoolean);
    ConstraintLayout localConstraintLayout = c;
    ((d)localObject1).b(localConstraintLayout);
  }
  
  public final void b()
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/util/an;
      localObject2 = d.getContext();
      ((an)localObject1).<init>((Context)localObject2);
      b = ((an)localObject1);
    }
    int i = R.id.dialpad;
    localObject1 = (DialerKeypad)a(i);
    Object localObject2 = b;
    ((DialerKeypad)localObject1).setFeedback((an)localObject2);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "isoCode");
    ao.d locald = new com/truecaller/calling/dialer/ao$d;
    Object localObject = new com/truecaller/calling/dialer/ao$e;
    ((ao.e)localObject).<init>(this);
    localObject = (c.g.a.b)localObject;
    locald.<init>(paramString, (c.g.a.b)localObject);
    paramString = new Void[0];
    locald.execute(paramString);
  }
  
  public final void c()
  {
    int i = R.id.dialpad;
    ((DialerKeypad)a(i)).setFeedback(null);
    an localan = b;
    if (localan != null) {
      localan.a();
    }
    b = null;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "number");
    int i = R.id.inputField;
    SelectionAwareEditText localSelectionAwareEditText = (SelectionAwareEditText)a(i);
    paramString = (CharSequence)paramString;
    localSelectionAwareEditText.setText(paramString);
    paramString = localSelectionAwareEditText.getText();
    int j;
    if (paramString != null)
    {
      j = paramString.length();
    }
    else
    {
      j = 0;
      paramString = null;
    }
    localSelectionAwareEditText.setSelection(j);
    localSelectionAwareEditText.requestFocus();
    localSelectionAwareEditText.setInputType(524289);
    localSelectionAwareEditText.setTextIsSelectable(true);
  }
  
  public final void d()
  {
    int i = R.id.dialpad;
    ((DialerKeypad)a(i)).a();
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "number");
    Object localObject1 = c.getContext();
    Object localObject2 = new android/support/v7/app/AlertDialog$Builder;
    if (localObject1 == null) {
      return;
    }
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    localObject1 = (CharSequence)((Context)localObject1).getString(2131886590, arrayOfObject);
    localObject1 = ((AlertDialog.Builder)localObject2).setMessage((CharSequence)localObject1);
    Object localObject3 = new com/truecaller/calling/dialer/ao$f;
    ((ao.f)localObject3).<init>(this, paramString);
    localObject3 = (DialogInterface.OnClickListener)localObject3;
    localObject1 = ((AlertDialog.Builder)localObject1).setNegativeButton(2131886591, (DialogInterface.OnClickListener)localObject3);
    localObject3 = new com/truecaller/calling/dialer/ao$g;
    ((ao.g)localObject3).<init>(this, paramString);
    localObject3 = (DialogInterface.OnClickListener)localObject3;
    localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton(2131886592, (DialogInterface.OnClickListener)localObject3);
    localObject2 = new com/truecaller/calling/dialer/ao$h;
    ((ao.h)localObject2).<init>(this, paramString);
    localObject2 = (DialogInterface.OnCancelListener)localObject2;
    ((AlertDialog.Builder)localObject1).setOnCancelListener((DialogInterface.OnCancelListener)localObject2).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */