package com.truecaller.calling.dialer;

import c.g.b.k;
import c.t;
import com.truecaller.calling.ActionType;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.aj;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.g;
import com.truecaller.flashsdk.core.i;
import com.truecaller.multisim.ae;
import com.truecaller.network.search.e;
import com.truecaller.util.af;
import com.truecaller.util.cf;
import com.truecaller.voip.ai;

public final class o
  extends j
  implements n.a
{
  private final boolean g;
  private final boolean h;
  private final cb i;
  private c.n j;
  private final com.truecaller.search.local.model.c k;
  private final com.truecaller.i.c l;
  private final com.truecaller.utils.n m;
  private final ax n;
  private final g o;
  private final af p;
  private final ae q;
  private final aj r;
  private final e s;
  
  public o(i.a parama, com.truecaller.search.local.model.c paramc, a parama1, bd parambd, com.truecaller.analytics.b paramb, i parami, com.truecaller.flashsdk.core.b paramb1, com.truecaller.i.c paramc1, com.truecaller.utils.n paramn, ax paramax, g paramg, af paramaf, ae paramae, com.truecaller.multisim.h paramh, aj paramaj, com.truecaller.calling.recorder.h paramh1, e parame, cf paramcf, ai paramai)
  {
    super(parama, parambd, paramb, parama1, parami, paramb1, paramcf, paramai);
    k = paramc;
    l = paramc1;
    m = paramn;
    n = paramax;
    o = paramg;
    p = paramaf;
    localObject = paramae;
    q = paramae;
    localObject = paramaj;
    r = paramaj;
    localObject = parame;
    s = parame;
    boolean bool = paramh1.a();
    g = bool;
    bool = paramh.j();
    h = bool;
    localObject = this;
    localObject = (i.b)this;
    localObject = parama.a((i.b)localObject);
    i = ((cb)localObject);
  }
  
  private final void a(HistoryEvent paramHistoryEvent, ActionType paramActionType)
  {
    boolean bool = b();
    if (!bool) {
      paramActionType = ActionType.PROFILE;
    }
    a(paramHistoryEvent, paramActionType, "item");
  }
  
  private final boolean b()
  {
    String str = l.a("callLogTapBehavior");
    return k.a("call", str);
  }
  
  public final boolean a(int paramInt)
  {
    paramInt = c(paramInt);
    return paramInt == 0;
  }
  
  public final boolean a(com.truecaller.adapter_delegates.h paramh)
  {
    k.b(paramh, "event");
    Object localObject1 = ActionType.Companion;
    localObject1 = a;
    k.b(localObject1, "action");
    Object localObject2 = ActionType.values();
    int i1 = localObject2.length;
    int i2 = 0;
    String str1;
    for (;;)
    {
      str1 = null;
      if (i2 >= i1) {
        break;
      }
      localObject3 = localObject2[i2];
      String str2 = ((ActionType)localObject3).getEventAction();
      boolean bool = k.a(str2, localObject1);
      if (bool) {
        break label85;
      }
      i2 += 1;
    }
    Object localObject3 = null;
    label85:
    if (localObject3 != null)
    {
      localObject1 = e;
      if (localObject1 != null) {
        str1 = localObject1.toString();
      }
      localObject1 = t.a(localObject3, str1);
      localObject2 = (ActionType)a;
      localObject1 = (String)b;
      int i3 = b;
      paramh = b(i3);
      return a(paramh, (ActionType)localObject2, (String)localObject1);
    }
    return super.a(paramh);
  }
  
  protected final boolean a(ActionType paramActionType, int paramInt)
  {
    k.b(paramActionType, "primaryAction");
    HistoryEvent localHistoryEvent = b(paramInt);
    Object localObject = localHistoryEvent.b();
    boolean bool1 = ab.a((String)localObject);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    localObject = l;
    String str = "madeCallsFromCallLog";
    bool1 = ((com.truecaller.i.c)localObject).b(str);
    if (!bool1)
    {
      localObject = l;
      str = "madeCallsFromCallLog";
      ((com.truecaller.i.c)localObject).b(str, bool2);
      localObject = d;
      ((bd)localObject).w_();
      paramActionType = t.a(localHistoryEvent, paramActionType);
      j = paramActionType;
    }
    else
    {
      a(localHistoryEvent, paramActionType);
    }
    return bool2;
  }
  
  public final boolean d(int paramInt)
  {
    boolean bool = a;
    if (!bool)
    {
      String str = b(paramInt).b();
      paramInt = ab.a(str);
      if (paramInt == 0) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean u_()
  {
    Object localObject = j;
    if (localObject != null)
    {
      HistoryEvent localHistoryEvent = (HistoryEvent)a;
      localObject = (ActionType)b;
      a(localHistoryEvent, (ActionType)localObject);
      j = null;
      return true;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */