package com.truecaller.calling.dialer;

import android.view.View;
import com.truecaller.calling.dialer.suggested_contacts.e;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import java.util.List;
import java.util.Set;

public abstract interface ae$c
{
  public abstract void a(int paramInt);
  
  public abstract void a(View paramView);
  
  public abstract void a(View paramView, e parame, String paramString);
  
  public abstract void a(aq paramaq);
  
  public abstract void a(e parame);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, PremiumPresenterView.LaunchContext paramLaunchContext);
  
  public abstract void a(String paramString1, String paramString2, String paramString3);
  
  public abstract void a(String paramString, boolean paramBoolean);
  
  public abstract void a(List paramList1, List paramList2);
  
  public abstract void a(Set paramSet);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(int paramInt);
  
  public abstract void b(String paramString);
  
  public abstract void b(Set paramSet);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(int paramInt);
  
  public abstract void c(String paramString);
  
  public abstract void c(Set paramSet);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d();
  
  public abstract void d(int paramInt);
  
  public abstract void d(boolean paramBoolean);
  
  public abstract void e();
  
  public abstract void e(boolean paramBoolean);
  
  public abstract void f();
  
  public abstract void f(boolean paramBoolean);
  
  public abstract void g();
  
  public abstract void g(boolean paramBoolean);
  
  public abstract void h();
  
  public abstract void h(boolean paramBoolean);
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract void m();
  
  public abstract void n();
  
  public abstract void o();
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ae.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */