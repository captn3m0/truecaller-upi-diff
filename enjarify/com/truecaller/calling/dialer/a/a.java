package com.truecaller.calling.dialer.a;

import c.g.b.k;

public final class a
{
  public final int a;
  public final int b;
  public final String c;
  
  public a(int paramInt1, int paramInt2, String paramString)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof a;
      if (bool2)
      {
        paramObject = (a)paramObject;
        int i = a;
        int j = a;
        String str;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          str = null;
        }
        if (i != 0)
        {
          i = b;
          j = b;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            str = null;
          }
          if (i != 0)
          {
            str = c;
            paramObject = c;
            boolean bool3 = k.a(str, paramObject);
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    int j = b;
    i = (i + j) * 31;
    String str = c;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SuggestedPremium(iconRes=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", titleRes=");
    i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", premiumPage=");
    String str = c;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */