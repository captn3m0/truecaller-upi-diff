package com.truecaller.calling.dialer;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public final class SelectionAwareEditText
  extends AppCompatEditText
{
  private int a;
  private int b;
  private SelectionAwareEditText.a c;
  
  public SelectionAwareEditText(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private SelectionAwareEditText(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    int i = -1 << -1;
    a = i;
    b = i;
  }
  
  private final void a(int paramInt1, int paramInt2)
  {
    int i = a;
    if (paramInt1 == i)
    {
      i = b;
      if (paramInt2 == i) {}
    }
    else
    {
      SelectionAwareEditText.a locala = c;
      if (locala != null) {
        locala.b(paramInt1, paramInt2);
      }
      a = paramInt1;
      b = paramInt2;
    }
  }
  
  public final SelectionAwareEditText.a getSelectionChangeListener()
  {
    return c;
  }
  
  protected final void onSelectionChanged(int paramInt1, int paramInt2)
  {
    super.onSelectionChanged(paramInt1, paramInt2);
    a(paramInt2, paramInt2);
  }
  
  protected final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    super.onTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
    a(this);
  }
  
  public final void setSelectionChangeListener(SelectionAwareEditText.a parama)
  {
    c = parama;
    a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.SelectionAwareEditText
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */