package com.truecaller.calling.dialer;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.search.global.CompositeAdapterDelegate.SearchResultOrder;
import com.truecaller.util.al;

public final class bo
  extends c
  implements bn.a
{
  private final bm.a c;
  private final al d;
  private final com.truecaller.utils.n e;
  private final bd f;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(bo.class);
    ((u)localObject).<init>(localb, "searchResults", "getSearchResults()Lkotlin/Pair;");
    localObject = (g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public bo(bm.a parama, al paramal, com.truecaller.utils.n paramn, bd parambd)
  {
    d = paramal;
    e = paramn;
    f = parambd;
    c = parama;
  }
  
  private final c.n a()
  {
    bm.a locala = c;
    Object localObject = this;
    localObject = (ca)this;
    g localg = b[0];
    return locala.a((ca)localObject, localg);
  }
  
  public final boolean a(h paramh)
  {
    String str = "event";
    k.b(paramh, str);
    paramh = a;
    int i = paramh.hashCode();
    int j = -1743572928;
    if (i == j)
    {
      str = "ItemEvent.CLICKED";
      boolean bool1 = paramh.equals(str);
      if (bool1)
      {
        paramh = f;
        str = (String)aa;
        CompositeAdapterDelegate.SearchResultOrder localSearchResultOrder = CompositeAdapterDelegate.SearchResultOrder.ORDER_TCM;
        boolean bool2 = true;
        paramh.a(str, null, bool2, localSearchResultOrder);
        return bool2;
      }
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return 1;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */