package com.truecaller.calling.dialer;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.design.widget.AppBarLayout.c;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnItemTouchListener;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;
import c.a.ae;
import c.a.ag;
import c.g.b.u;
import c.k.i;
import com.truecaller.R.id;
import com.truecaller.adapter_delegates.l;
import com.truecaller.adapter_delegates.p;
import com.truecaller.adapter_delegates.s;
import com.truecaller.ads.b.j;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.dialer.suggested_contacts.e;
import com.truecaller.common.ui.b.a;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.ui.aa;
import com.truecaller.ui.aa.a;
import com.truecaller.ui.details.a.a;
import com.truecaller.ui.g.c;
import com.truecaller.ui.q;
import com.truecaller.ui.z;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class ai
  implements ae.c, kotlinx.a.a.a
{
  private final ViewTreeObserver.OnGlobalLayoutListener A;
  private final z B;
  private final aa C;
  private final ae.c.b D;
  private final View E;
  private final c.g.a.a F;
  private final n.a G;
  private final cf.a H;
  private final be.a I;
  private final aj.b J;
  private final ay.a K;
  private final b.a L;
  private final br M;
  private HashMap N;
  private final View b;
  private final Object c;
  private final c.f d;
  private final c.f e;
  private final c.f f;
  private final c.f g;
  private final c.f h;
  private final c.f i;
  private final c.f j;
  private final int k;
  private final int l;
  private Toast m;
  private com.truecaller.flashsdk.ui.b n;
  private boolean o;
  private boolean p;
  private final p q;
  private final p r;
  private final p s;
  private final com.truecaller.adapter_delegates.m t;
  private final com.truecaller.adapter_delegates.m u;
  private final p v;
  private final com.truecaller.adapter_delegates.g w;
  private final com.truecaller.adapter_delegates.f x;
  private final com.truecaller.adapter_delegates.f y;
  private final ai.h z;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[7];
    Object localObject = new c/g/b/u;
    c.l.b localb = c.g.b.w.a(ai.class);
    ((u)localObject).<init>(localb, "recycler", "getRecycler()Landroid/support/v7/widget/RecyclerView;");
    localObject = (c.l.g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = c.g.b.w.a(ai.class);
    ((u)localObject).<init>(localb, "toolbar", "getToolbar()Landroid/support/v7/widget/Toolbar;");
    localObject = (c.l.g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = c.g.b.w.a(ai.class);
    ((u)localObject).<init>(localb, "dialpadView", "getDialpadView()Landroid/view/ViewGroup;");
    localObject = (c.l.g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = c.g.b.w.a(ai.class);
    ((u)localObject).<init>(localb, "emptyView", "getEmptyView()Landroid/view/View;");
    localObject = (c.l.g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[3] = localObject;
    localObject = new c/g/b/u;
    localb = c.g.b.w.a(ai.class);
    ((u)localObject).<init>(localb, "emptyViewTitle", "getEmptyViewTitle()Landroid/widget/TextView;");
    localObject = (c.l.g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[4] = localObject;
    localObject = new c/g/b/u;
    localb = c.g.b.w.a(ai.class);
    ((u)localObject).<init>(localb, "emptyViewText", "getEmptyViewText()Landroid/widget/TextView;");
    localObject = (c.l.g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[5] = localObject;
    localObject = new c/g/b/u;
    localb = c.g.b.w.a(ai.class);
    ((u)localObject).<init>(localb, "emptyViewButton", "getEmptyViewButton()Landroid/widget/Button;");
    localObject = (c.l.g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[6] = localObject;
    a = arrayOfg;
  }
  
  public ai(ae.c.b paramb, com.truecaller.common.ui.a parama, View paramView, c.g.a.a parama1, n.a parama2, cf.a parama3, be.a parama4, ck.b paramb1, cn.b paramb2, com.truecaller.ads.b.w paramw, aj.b paramb3, bn.a parama5, bw.a parama6, ay.a parama7, b.a parama8, br parambr)
  {
    D = paramb;
    E = paramView;
    F = parama1;
    G = parama2;
    H = parama3;
    I = parama4;
    J = paramb3;
    K = parama7;
    localObject1 = parama8;
    L = parama8;
    M = parambr;
    localObject1 = E;
    b = ((View)localObject1);
    localObject1 = new java/lang/Object;
    localObject1.<init>();
    c = localObject1;
    localObject1 = com.truecaller.utils.extensions.t.a(E, 2131362833);
    d = ((c.f)localObject1);
    localObject1 = com.truecaller.utils.extensions.t.a(E, 2131363459);
    e = ((c.f)localObject1);
    localObject1 = com.truecaller.utils.extensions.t.a(E, 2131363475);
    f = ((c.f)localObject1);
    localObject1 = com.truecaller.utils.extensions.t.a(E, 2131362376);
    g = ((c.f)localObject1);
    localObject1 = com.truecaller.utils.extensions.t.a(E, 2131362378);
    h = ((c.f)localObject1);
    localObject1 = com.truecaller.utils.extensions.t.a(E, 2131362377);
    i = ((c.f)localObject1);
    localObject1 = com.truecaller.utils.extensions.t.a(E, 2131362375);
    j = ((c.f)localObject1);
    localObject1 = E.getContext();
    c.g.b.k.a(localObject1, "view.context");
    int i1 = ((Context)localObject1).getResources().getDimensionPixelSize(2131165482);
    k = i1;
    localObject1 = E.getContext();
    c.g.b.k.a(localObject1, "view.context");
    localObject1 = ((Context)localObject1).getResources();
    int i2 = 2131165497;
    i1 = ((Resources)localObject1).getDimensionPixelSize(i2);
    l = i1;
    localObject1 = new com/truecaller/adapter_delegates/p;
    localObject3 = parama5;
    localObject3 = (com.truecaller.adapter_delegates.b)parama5;
    localObject4 = new com/truecaller/calling/dialer/ai$i;
    ((ai.i)localObject4).<init>(this);
    localObject4 = (c.g.a.b)localObject4;
    localObject5 = (c.g.a.b)ai.j.a;
    ((p)localObject1).<init>((com.truecaller.adapter_delegates.b)localObject3, 2131559016, (c.g.a.b)localObject4, (c.g.a.b)localObject5);
    q = ((p)localObject1);
    localObject1 = new com/truecaller/adapter_delegates/p;
    localObject3 = parama6;
    localObject3 = (com.truecaller.adapter_delegates.b)parama6;
    localObject4 = new com/truecaller/calling/dialer/ai$k;
    ((ai.k)localObject4).<init>(this);
    localObject4 = (c.g.a.b)localObject4;
    localObject5 = (c.g.a.b)ai.l.a;
    ((p)localObject1).<init>((com.truecaller.adapter_delegates.b)localObject3, 2131559005, (c.g.a.b)localObject4, (c.g.a.b)localObject5);
    r = ((p)localObject1);
    localObject1 = new com/truecaller/adapter_delegates/p;
    localObject3 = (com.truecaller.adapter_delegates.b)H;
    localObject4 = new com/truecaller/calling/dialer/ai$z;
    ((ai.z)localObject4).<init>(paramb1, paramb2);
    localObject4 = (c.g.a.b)localObject4;
    localObject5 = (c.g.a.b)ai.aa.a;
    ((p)localObject1).<init>((com.truecaller.adapter_delegates.b)localObject3, 2131559019, (c.g.a.b)localObject4, (c.g.a.b)localObject5);
    s = ((p)localObject1);
    localObject1 = j.a(paramw);
    t = ((com.truecaller.adapter_delegates.m)localObject1);
    localObject1 = new com/truecaller/adapter_delegates/m;
    int i3 = 2;
    localObject3 = new l[i3];
    localObject4 = new com/truecaller/adapter_delegates/l;
    localObject5 = (com.truecaller.adapter_delegates.n)K;
    localObject6 = new com/truecaller/calling/dialer/ai$b;
    ((ai.b)localObject6).<init>(this);
    localObject6 = (c.g.a.b)localObject6;
    ((l)localObject4).<init>((com.truecaller.adapter_delegates.n)localObject5, 2131365501, (c.g.a.b)localObject6);
    localObject5 = null;
    localObject3[0] = localObject4;
    localObject4 = new com/truecaller/adapter_delegates/l;
    localObject6 = (com.truecaller.adapter_delegates.n)G;
    localObject7 = new com/truecaller/calling/dialer/ai$c;
    ((ai.c)localObject7).<init>(this);
    localObject7 = (c.g.a.b)localObject7;
    ((l)localObject4).<init>((com.truecaller.adapter_delegates.n)localObject6, 2131365483, (c.g.a.b)localObject7);
    boolean bool = true;
    localObject3[bool] = localObject4;
    ((com.truecaller.adapter_delegates.m)localObject1).<init>((l[])localObject3);
    u = ((com.truecaller.adapter_delegates.m)localObject1);
    localObject1 = new com/truecaller/adapter_delegates/p;
    localObject3 = (com.truecaller.adapter_delegates.b)I;
    localObject4 = new com/truecaller/calling/dialer/ai$f;
    ((ai.f)localObject4).<init>(this);
    localObject4 = (c.g.a.b)localObject4;
    localObject7 = (c.g.a.b)ai.g.a;
    int i4 = 2131559006;
    ((p)localObject1).<init>((com.truecaller.adapter_delegates.b)localObject3, i4, (c.g.a.b)localObject4, (c.g.a.b)localObject7);
    v = ((p)localObject1);
    localObject1 = new com/truecaller/adapter_delegates/g;
    ((com.truecaller.adapter_delegates.g)localObject1).<init>((byte)0);
    w = ((com.truecaller.adapter_delegates.g)localObject1);
    localObject1 = new com/truecaller/adapter_delegates/f;
    localObject3 = (com.truecaller.adapter_delegates.a)u;
    localObject3 = a((com.truecaller.adapter_delegates.a)localObject3);
    localObject4 = (com.truecaller.adapter_delegates.a)s;
    localObject7 = (s)w;
    localObject3 = ((com.truecaller.adapter_delegates.t)localObject3).a((com.truecaller.adapter_delegates.a)localObject4, (s)localObject7);
    localObject4 = (com.truecaller.adapter_delegates.a)v;
    localObject7 = (s)w;
    localObject3 = (com.truecaller.adapter_delegates.a)((com.truecaller.adapter_delegates.t)localObject3).a((com.truecaller.adapter_delegates.a)localObject4, (s)localObject7);
    ((com.truecaller.adapter_delegates.f)localObject1).<init>((com.truecaller.adapter_delegates.a)localObject3);
    ((com.truecaller.adapter_delegates.f)localObject1).setHasStableIds(bool);
    x = ((com.truecaller.adapter_delegates.f)localObject1);
    localObject1 = new com/truecaller/adapter_delegates/f;
    localObject3 = q;
    localObject4 = (com.truecaller.adapter_delegates.a)r;
    localObject4 = (com.truecaller.adapter_delegates.a)a((com.truecaller.adapter_delegates.a)localObject4);
    localObject7 = (s)w;
    localObject3 = (com.truecaller.adapter_delegates.a)((p)localObject3).a((com.truecaller.adapter_delegates.a)localObject4, (s)localObject7);
    ((com.truecaller.adapter_delegates.f)localObject1).<init>((com.truecaller.adapter_delegates.a)localObject3);
    ((com.truecaller.adapter_delegates.f)localObject1).setHasStableIds(bool);
    y = ((com.truecaller.adapter_delegates.f)localObject1);
    localObject1 = new com/truecaller/calling/dialer/ai$h;
    ((ai.h)localObject1).<init>(this);
    z = ((ai.h)localObject1);
    localObject1 = new com/truecaller/calling/dialer/ai$d;
    ((ai.d)localObject1).<init>(this);
    localObject1 = (ViewTreeObserver.OnGlobalLayoutListener)localObject1;
    A = ((ViewTreeObserver.OnGlobalLayoutListener)localObject1);
    localObject1 = new com/truecaller/ui/z;
    localObject3 = (com.truecaller.adapter_delegates.k)x;
    ((z)localObject1).<init>((com.truecaller.adapter_delegates.k)localObject3);
    B = ((z)localObject1);
    localObject1 = new com/truecaller/ui/aa;
    localObject3 = E.getContext();
    localObject2 = new c.n[i3];
    localObject4 = ActionType.WHATSAPP_CALL;
    int i5 = 2131234681;
    localObject8 = Integer.valueOf(i5);
    localObject4 = c.t.a(localObject4, localObject8);
    localObject2[0] = localObject4;
    localObject4 = ActionType.WHATSAPP_VIDEO_CALL;
    localObject7 = Integer.valueOf(i5);
    localObject4 = c.t.a(localObject4, localObject7);
    localObject2[bool] = localObject4;
    localObject2 = ag.a((c.n[])localObject2);
    localObject4 = (aa.a)B;
    ((aa)localObject1).<init>((Context)localObject3, (Map)localObject2, (aa.a)localObject4);
    C = ((aa)localObject1);
    localObject1 = p();
    localObject2 = x;
    ((com.truecaller.adapter_delegates.f)localObject2).a(bool);
    localObject2 = (RecyclerView.Adapter)localObject2;
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
    localObject2 = new android/support/v7/widget/LinearLayoutManager;
    localObject3 = E.getContext();
    ((LinearLayoutManager)localObject2).<init>((Context)localObject3);
    localObject2 = (RecyclerView.LayoutManager)localObject2;
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject2);
    localObject2 = (RecyclerView.OnScrollListener)z;
    ((RecyclerView)localObject1).addOnScrollListener((RecyclerView.OnScrollListener)localObject2);
    localObject2 = (RecyclerView.OnItemTouchListener)C;
    ((RecyclerView)localObject1).addOnItemTouchListener((RecyclerView.OnItemTouchListener)localObject2);
    localObject2 = (RecyclerView.ItemDecoration)C;
    ((RecyclerView)localObject1).addItemDecoration((RecyclerView.ItemDecoration)localObject2);
    i3 = 0;
    ((RecyclerView)localObject1).setItemAnimator(null);
    localObject2 = new com/truecaller/ui/q;
    localObject3 = E.getContext();
    int i6 = 2131559205;
    ((q)localObject2).<init>((Context)localObject3, i6, 0);
    localObject2 = (RecyclerView.ItemDecoration)localObject2;
    ((RecyclerView)localObject1).addItemDecoration((RecyclerView.ItemDecoration)localObject2);
    localObject1 = q();
    localObject2 = ((Toolbar)localObject1).getNavigationIcon();
    if (localObject2 != null)
    {
      localObject2 = ((Drawable)localObject2).mutate();
      if (localObject2 != null)
      {
        localObject3 = ((Toolbar)localObject1).getContext();
        i6 = 2130969592;
        i2 = com.truecaller.utils.ui.b.a((Context)localObject3, i6);
        localObject4 = PorterDuff.Mode.SRC_IN;
        ((Drawable)localObject2).setColorFilter(i2, (PorterDuff.Mode)localObject4);
        ((Toolbar)localObject1).setNavigationIcon((Drawable)localObject2);
      }
    }
    localObject2 = new com/truecaller/calling/dialer/ai$a;
    ((ai.a)localObject2).<init>(localai);
    localObject2 = (View.OnClickListener)localObject2;
    ((Toolbar)localObject1).setNavigationOnClickListener((View.OnClickListener)localObject2);
    com.truecaller.utils.extensions.t.b((View)localObject1);
    i1 = R.id.fab;
    localObject1 = (FloatingActionButton)localai.e(i1);
    localObject2 = new com/truecaller/calling/dialer/ai$1;
    ((ai.1)localObject2).<init>(localai);
    localObject2 = (View.OnClickListener)localObject2;
    ((FloatingActionButton)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = new com/truecaller/calling/dialer/ai$2;
    ((ai.2)localObject1).<init>(localai);
    localObject1 = (AppBarLayout.c)localObject1;
    localObject2 = parama;
    parama.a((AppBarLayout.c)localObject1);
  }
  
  private final com.truecaller.adapter_delegates.t a(com.truecaller.adapter_delegates.a parama)
  {
    com.truecaller.adapter_delegates.a locala = (com.truecaller.adapter_delegates.a)t;
    Object localObject = new com/truecaller/adapter_delegates/o;
    ((com.truecaller.adapter_delegates.o)localObject).<init>(2, 12, 4);
    localObject = (s)localObject;
    return parama.a(locala, (s)localObject);
  }
  
  private final void a(int paramInt1, int paramInt2, c.g.a.a parama)
  {
    AlertDialog.Builder localBuilder1 = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = E.getContext();
    localBuilder1.<init>(localContext);
    AlertDialog.Builder localBuilder2 = localBuilder1.setTitle(paramInt1).setMessage(paramInt2);
    Object localObject = new com/truecaller/calling/dialer/ai$u;
    ((ai.u)localObject).<init>(parama);
    localObject = (DialogInterface.OnClickListener)localObject;
    localBuilder2.setPositiveButton(2131887217, (DialogInterface.OnClickListener)localObject).setNegativeButton(2131887197, null).show();
  }
  
  private final RecyclerView p()
  {
    return (RecyclerView)d.b();
  }
  
  private final Toolbar q()
  {
    return (Toolbar)e.b();
  }
  
  private final View r()
  {
    return (View)g.b();
  }
  
  private final Button s()
  {
    return (Button)j.b();
  }
  
  private final void t()
  {
    Object localObject = p();
    c.g.b.k.a(localObject, "recycler");
    localObject = ((RecyclerView)localObject).getViewTreeObserver();
    ViewTreeObserver.OnGlobalLayoutListener localOnGlobalLayoutListener = A;
    ((ViewTreeObserver)localObject).removeOnGlobalLayoutListener(localOnGlobalLayoutListener);
  }
  
  public final View a()
  {
    return b;
  }
  
  public final void a(int paramInt)
  {
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = E.getContext();
    ((AlertDialog.Builder)localObject).<init>(localContext);
    AlertDialog.Builder localBuilder = ((AlertDialog.Builder)localObject).setMessage(paramInt);
    localObject = new com/truecaller/calling/dialer/ai$v;
    ((ai.v)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    localBuilder.setPositiveButton(2131886289, (DialogInterface.OnClickListener)localObject).setNegativeButton(2131887197, null).show();
  }
  
  public final void a(View paramView)
  {
    c.g.b.k.b(paramView, "anchorView");
    PopupMenu localPopupMenu = new android/widget/PopupMenu;
    Object localObject = E.getContext();
    localPopupMenu.<init>((Context)localObject, paramView);
    paramView = localPopupMenu.getMenuInflater();
    localObject = localPopupMenu.getMenu();
    paramView.inflate(2131623977, (Menu)localObject);
    paramView = new com/truecaller/calling/dialer/ai$y;
    paramView.<init>(this);
    paramView = (PopupMenu.OnMenuItemClickListener)paramView;
    localPopupMenu.setOnMenuItemClickListener(paramView);
    localPopupMenu.show();
  }
  
  public final void a(View paramView, e parame, String paramString)
  {
    c.g.b.k.b(paramView, "anchorView");
    c.g.b.k.b(parame, "suggestedContact");
    c.g.b.k.b(paramString, "displayName");
    Context localContext = E.getContext();
    PopupMenu localPopupMenu = new android/widget/PopupMenu;
    localPopupMenu.<init>(localContext, paramView);
    paramView = localPopupMenu.getMenuInflater();
    Object localObject1 = localPopupMenu.getMenu();
    paramView.inflate(2131623976, (Menu)localObject1);
    paramView = localPopupMenu.getMenu().findItem(2131361870);
    localObject1 = "menuItemHide";
    c.g.b.k.a(paramView, (String)localObject1);
    int i1 = 1;
    Object localObject2 = new Object[i1];
    localObject2[0] = paramString;
    int i2 = 2131887248;
    localObject2 = (CharSequence)localContext.getString(i2, (Object[])localObject2);
    paramView.setTitle((CharSequence)localObject2);
    boolean bool = c ^ i1;
    paramView.setVisible(bool);
    paramView = localPopupMenu.getMenu().findItem(2131361847);
    localObject2 = "popup.menu.findItem(R.id…ion_change_pinning_state)";
    c.g.b.k.a(paramView, (String)localObject2);
    bool = c;
    int i3;
    if (bool)
    {
      i3 = 2131887253;
      localObject1 = new Object[i1];
      localObject1[0] = paramString;
      paramString = (CharSequence)localContext.getString(i3, (Object[])localObject1);
    }
    else
    {
      i3 = 2131887249;
      localObject1 = new Object[i1];
      localObject1[0] = paramString;
      paramString = (CharSequence)localContext.getString(i3, (Object[])localObject1);
    }
    paramView.setTitle(paramString);
    paramView = new com/truecaller/calling/dialer/ai$x;
    paramView.<init>(this, parame);
    paramView = (PopupMenu.OnMenuItemClickListener)paramView;
    localPopupMenu.setOnMenuItemClickListener(paramView);
    localPopupMenu.show();
  }
  
  public final void a(aq paramaq)
  {
    if (paramaq == null)
    {
      x.a(false);
      paramaq = r();
      c.g.b.k.a(paramaq, "emptyView");
      com.truecaller.utils.extensions.t.a(paramaq, false);
      return;
    }
    Object localObject = x;
    boolean bool1 = true;
    ((com.truecaller.adapter_delegates.f)localObject).a(bool1);
    localObject = r();
    c.g.b.k.a(localObject, "emptyView");
    com.truecaller.utils.extensions.t.a((View)localObject, bool1);
    localObject = (TextView)h.b();
    int i1 = a;
    ((TextView)localObject).setText(i1);
    localObject = s();
    i1 = b;
    ((Button)localObject).setText(i1);
    localObject = (TextView)i.b();
    c.g.b.k.a(localObject, "emptyViewText");
    localObject = (View)localObject;
    boolean bool2 = c;
    com.truecaller.utils.extensions.t.a((View)localObject, bool2);
    paramaq = s();
    localObject = new com/truecaller/calling/dialer/ai$m;
    ((ai.m)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramaq.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void a(e parame)
  {
    c.g.b.k.b(parame, "suggestedContact");
    Snackbar localSnackbar = Snackbar.a((View)p(), 2131887247, 0);
    Object localObject = new com/truecaller/calling/dialer/ai$e;
    ((ai.e)localObject).<init>(this, parame);
    localObject = (View.OnClickListener)localObject;
    localSnackbar.a(2131886464, (View.OnClickListener)localObject).c();
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = E.getContext();
    ((AlertDialog.Builder)localObject).<init>(localContext);
    paramString = (CharSequence)paramString;
    paramString = ((AlertDialog.Builder)localObject).setMessage(paramString);
    localObject = new com/truecaller/calling/dialer/ai$n;
    ((ai.n)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    paramString.setPositiveButton(2131886772, (DialogInterface.OnClickListener)localObject).setNegativeButton(2131887197, null).show();
  }
  
  public final void a(String paramString, PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    c.g.b.k.b(paramString, "page");
    c.g.b.k.b(paramLaunchContext, "launchContext");
    Context localContext = E.getContext();
    c.g.b.k.a(localContext, "view.context");
    br.a(localContext, paramLaunchContext, paramString);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "name");
    com.truecaller.ui.dialogs.o localo = new com/truecaller/ui/dialogs/o;
    Context localContext = E.getContext();
    localo.<init>(localContext, paramString2, paramString1, paramString3);
    localo.show();
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    com.truecaller.ui.details.a locala = new com/truecaller/ui/details/a;
    Context localContext = E.getContext();
    locala.<init>(localContext, paramString, paramBoolean, true);
    paramString = new com/truecaller/calling/dialer/ai$o;
    paramString.<init>(this);
    paramString = (a.a)paramString;
    locala.a(paramString);
    locala.show();
  }
  
  public final void a(List paramList1, List paramList2)
  {
    c.g.b.k.b(paramList1, "oldItems");
    c.g.b.k.b(paramList2, "newItems");
    H.a(paramList1, paramList2);
  }
  
  public final void a(Set paramSet)
  {
    Object localObject = "itemPositions";
    c.g.b.k.b(paramSet, (String)localObject);
    paramSet = ((Iterable)paramSet).iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      localObject = (Number)paramSet.next();
      int i1 = ((Number)localObject).intValue();
      com.truecaller.adapter_delegates.f localf = x;
      com.truecaller.adapter_delegates.m localm = u;
      i1 = localm.a_(i1);
      localf.notifyItemChanged(i1);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    p = paramBoolean;
    if (paramBoolean)
    {
      Object localObject = p();
      c.g.b.k.a(localObject, "recycler");
      localObject = ((RecyclerView)localObject).getViewTreeObserver();
      ViewTreeObserver.OnGlobalLayoutListener localOnGlobalLayoutListener = A;
      ((ViewTreeObserver)localObject).addOnGlobalLayoutListener(localOnGlobalLayoutListener);
    }
  }
  
  public final void b()
  {
    x.notifyDataSetChanged();
  }
  
  public final void b(int paramInt)
  {
    int i1 = paramInt + 1;
    Object localObject1 = (Iterable)i.b(paramInt, i1);
    Object localObject2 = new java/util/ArrayList;
    int i3 = c.a.m.a((Iterable)localObject1, 10);
    ((ArrayList)localObject2).<init>(i3);
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    Object localObject3;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = localObject1;
      int i4 = ((ae)localObject1).a();
      p localp = r;
      i4 = localp.a_(i4);
      localObject3 = Integer.valueOf(i4);
      ((Collection)localObject2).add(localObject3);
    }
    localObject2 = (Iterable)localObject2;
    localObject1 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Number)((Iterator)localObject1).next();
      int i2 = ((Number)localObject2).intValue();
      localObject3 = y;
      ((com.truecaller.adapter_delegates.f)localObject3).notifyItemChanged(i2);
    }
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "dialogMessage");
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = E.getContext();
    ((AlertDialog.Builder)localObject).<init>(localContext);
    paramString = (CharSequence)paramString;
    paramString = ((AlertDialog.Builder)localObject).setMessage(paramString);
    localObject = new com/truecaller/calling/dialer/ai$p;
    ((ai.p)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    paramString = paramString.setNegativeButton(2131887216, (DialogInterface.OnClickListener)localObject);
    localObject = new com/truecaller/calling/dialer/ai$q;
    ((ai.q)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    paramString.setPositiveButton(2131886772, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public final void b(Set paramSet)
  {
    c.g.b.k.b(paramSet, "itemPositions");
    H.a(paramSet);
  }
  
  public final void b(boolean paramBoolean)
  {
    p localp = s;
    paramBoolean ^= true;
    a = paramBoolean;
    x.notifyDataSetChanged();
  }
  
  public final void c()
  {
    H.a();
  }
  
  public final void c(int paramInt)
  {
    Toolbar localToolbar = q();
    c.g.b.k.a(localToolbar, "toolbar");
    CharSequence localCharSequence = (CharSequence)E.getContext().getString(paramInt);
    localToolbar.setTitle(localCharSequence);
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject = m;
    if (localObject != null) {
      ((Toast)localObject).cancel();
    }
    localObject = E.getContext();
    paramString = (CharSequence)paramString;
    paramString = Toast.makeText((Context)localObject, paramString, 0);
    m = paramString;
    paramString = m;
    if (paramString != null)
    {
      paramString.show();
      return;
    }
  }
  
  public final void c(Set paramSet)
  {
    Object localObject = "adsPositions";
    c.g.b.k.b(paramSet, (String)localObject);
    paramSet = ((Iterable)paramSet).iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      localObject = (Number)paramSet.next();
      int i1 = ((Number)localObject).intValue();
      i1 = t.a_(i1);
      com.truecaller.adapter_delegates.f localf = x;
      int i2 = localf.getItemCount() - i1;
      localf.notifyItemRangeChanged(i1, i2);
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    G.b(paramBoolean);
    K.b(paramBoolean);
    x.notifyDataSetChanged();
  }
  
  public final void d()
  {
    com.truecaller.ui.g localg = new com/truecaller/ui/g;
    Object localObject = E.getContext();
    View localView = (View)F.invoke();
    localg.<init>((Context)localObject, localView);
    localObject = new com/truecaller/calling/dialer/ai$w;
    ((ai.w)localObject).<init>(this);
    localObject = (g.c)localObject;
    localg.a((g.c)localObject);
    localg.a();
  }
  
  public final void d(int paramInt)
  {
    int i1 = R.id.fab;
    ((FloatingActionButton)e(i1)).setImageResource(paramInt);
  }
  
  public final void d(boolean paramBoolean)
  {
    Object localObject1;
    if (paramBoolean) {
      localObject1 = y;
    } else {
      localObject1 = x;
    }
    Object localObject2 = p();
    Object localObject3 = "recycler";
    c.g.b.k.a(localObject2, (String)localObject3);
    localObject2 = ((RecyclerView)localObject2).getAdapter();
    boolean bool = c.g.b.k.a(localObject2, localObject1) ^ true;
    if (bool)
    {
      localObject2 = p();
      c.g.b.k.a(localObject2, "recycler");
      localObject3 = localObject1;
      localObject3 = (RecyclerView.Adapter)localObject1;
      ((RecyclerView)localObject2).setAdapter((RecyclerView.Adapter)localObject3);
      localObject2 = B;
      localObject1 = (com.truecaller.adapter_delegates.k)localObject1;
      localObject3 = "<set-?>";
      c.g.b.k.b(localObject1, (String)localObject3);
      a = ((com.truecaller.adapter_delegates.k)localObject1);
    }
  }
  
  public final View e(int paramInt)
  {
    Object localObject1 = N;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      N = ((HashMap)localObject1);
    }
    localObject1 = N;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = N;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void e()
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = E.getContext();
    localBuilder.<init>((Context)localObject);
    localBuilder = localBuilder.setMessage(2131887245).setNegativeButton(2131887197, null);
    localObject = new com/truecaller/calling/dialer/ai$r;
    ((ai.r)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    localBuilder.setPositiveButton(2131887244, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public final void e(boolean paramBoolean)
  {
    p localp = q;
    paramBoolean ^= true;
    a = paramBoolean;
    y.notifyDataSetChanged();
  }
  
  public final void f()
  {
    Object localObject = new com/truecaller/calling/dialer/ai$s;
    ((ai.s)localObject).<init>(this);
    localObject = (c.g.a.a)localObject;
    a(2131888341, 2131888354, (c.g.a.a)localObject);
  }
  
  public final void f(boolean paramBoolean)
  {
    Toolbar localToolbar = q();
    c.g.b.k.a(localToolbar, "toolbar");
    com.truecaller.utils.extensions.t.a((View)localToolbar, paramBoolean);
  }
  
  public final void g()
  {
    Object localObject = new com/truecaller/calling/dialer/ai$t;
    ((ai.t)localObject).<init>(this);
    localObject = (c.g.a.a)localObject;
    a(2131888342, 2131888355, (c.g.a.a)localObject);
  }
  
  public final void g(boolean paramBoolean)
  {
    int i1 = R.id.fab;
    FloatingActionButton localFloatingActionButton = (FloatingActionButton)e(i1);
    c.g.b.k.a(localFloatingActionButton, "fab");
    com.truecaller.utils.extensions.t.a((View)localFloatingActionButton, paramBoolean);
  }
  
  public final void h()
  {
    J.b();
  }
  
  public final void h(boolean paramBoolean)
  {
    p localp = v;
    paramBoolean ^= true;
    a = paramBoolean;
    x.notifyDataSetChanged();
    k();
  }
  
  public final void i()
  {
    o = true;
  }
  
  public final void j()
  {
    J.d();
  }
  
  public final void k()
  {
    p().scrollToPosition(0);
  }
  
  public final void l()
  {
    Toast.makeText(E.getContext(), 2131888358, 0).show();
  }
  
  public final void m()
  {
    b.a locala = L;
    if (locala != null)
    {
      locala.ag_();
      return;
    }
  }
  
  public final void n()
  {
    t();
  }
  
  public final void o()
  {
    x.notifyDataSetChanged();
    k();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */