package com.truecaller.calling.dialer;

import c.a.m;
import c.g.b.k;
import com.truecaller.common.h.ab;
import com.truecaller.data.entity.HistoryEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.a.a.b;

public final class ar
  implements as
{
  private final String a = "Default";
  
  public final String a()
  {
    return a;
  }
  
  public final List a(List paramList)
  {
    k.b(paramList, "events");
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramList.size();
    localArrayList.<init>(i);
    long l1 = System.currentTimeMillis();
    Iterator localIterator = paramList.iterator();
    boolean bool2;
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      int j = 0;
      x localx = null;
      bool2 = true;
      if (!bool1) {
        break;
      }
      HistoryEvent localHistoryEvent = (HistoryEvent)localIterator.next();
      Object localObject1 = localArrayList;
      localObject1 = (x)m.g((List)localArrayList);
      if (localObject1 != null)
      {
        Object localObject2 = c;
        int k = ((HistoryEvent)localObject2).f();
        int n = localHistoryEvent.f();
        if (k == n)
        {
          Object localObject3 = h.e;
          Object localObject4 = ab;
          localObject3 = ab;
          boolean bool3 = k.a(localObject4, localObject3);
          if (bool3)
          {
            int m = ((HistoryEvent)localObject2).m();
            n = localHistoryEvent.m();
            if (m == n)
            {
              m = ((HistoryEvent)localObject2).h();
              n = localHistoryEvent.h();
              if (m == n)
              {
                localObject3 = ((HistoryEvent)localObject2).b();
                boolean bool4 = ab.a((String)localObject3);
                if (!bool4)
                {
                  localObject3 = ((HistoryEvent)localObject2).b();
                  localObject4 = localHistoryEvent.b();
                  bool4 = ab.a((String)localObject3, (String)localObject4, bool2);
                  if (bool4)
                  {
                    localObject3 = new org/a/a/b;
                    long l2 = ((HistoryEvent)localObject2).j();
                    ((b)localObject3).<init>(l2);
                    localObject2 = ((b)localObject3).e();
                    localObject3 = new org/a/a/b;
                    l2 = localHistoryEvent.j();
                    ((b)localObject3).<init>(l2);
                    localObject3 = ((b)localObject3).e();
                    boolean bool5 = k.a(localObject2, localObject3);
                    if (bool5) {
                      j = 1;
                    }
                  }
                }
              }
            }
          }
        }
        if (j != 0)
        {
          ((x)localObject1).a(localHistoryEvent);
          continue;
        }
      }
      localx = new com/truecaller/calling/dialer/x;
      localx.<init>(localHistoryEvent);
      localArrayList.add(localx);
    }
    long l3 = System.currentTimeMillis() - l1;
    String[] arrayOfString = new String[bool2];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Merged ");
    int i1 = paramList.size();
    localStringBuilder.append(i1);
    localStringBuilder.append(" history events in ");
    localStringBuilder.append(l3);
    localStringBuilder.append("ms");
    paramList = localStringBuilder.toString();
    arrayOfString[0] = paramList;
    return (List)localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */