package com.truecaller.calling.dialer;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.adapter_delegates.i;

public final class bq
  extends RecyclerView.ViewHolder
  implements bn.b
{
  private final f b;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(bq.class);
    ((u)localObject).<init>(localb, "searchTextView", "getSearchTextView()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public bq(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    Object localObject = com.truecaller.utils.extensions.t.a(paramView, 2131363718);
    b = ((f)localObject);
    localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, 12);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "searchText");
    TextView localTextView = (TextView)b.b();
    c.g.b.k.a(localTextView, "searchTextView");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */