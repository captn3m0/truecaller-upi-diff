package com.truecaller.calling.dialer;

import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.LinearLayout;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.p;
import com.truecaller.adapter_delegates.s;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class ci
  extends RecyclerView.ViewHolder
  implements cf.b
{
  private final c.f b;
  private final c.f c;
  private final p d;
  private final p e;
  private final com.truecaller.adapter_delegates.f f;
  private Parcelable g;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[2];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(ci.class);
    ((u)localObject).<init>(localb, "recycleView", "getRecycleView()Landroid/support/v7/widget/RecyclerView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(ci.class);
    ((u)localObject).<init>(localb, "emptyStateLinearLayout", "getEmptyStateLinearLayout()Landroid/widget/LinearLayout;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public ci(View paramView, ck.b paramb, cn.b paramb1)
  {
    super(paramView);
    Object localObject1 = com.truecaller.utils.extensions.t.a(paramView, 2131364093);
    b = ((c.f)localObject1);
    localObject1 = com.truecaller.utils.extensions.t.a(paramView, 2131363644);
    c = ((c.f)localObject1);
    localObject1 = new com/truecaller/adapter_delegates/p;
    paramb = (com.truecaller.adapter_delegates.b)paramb;
    Object localObject2 = new com/truecaller/calling/dialer/ci$a;
    ((ci.a)localObject2).<init>(this);
    localObject2 = (c.g.a.b)localObject2;
    c.g.a.b localb = (c.g.a.b)ci.b.a;
    ((p)localObject1).<init>(paramb, 2131559020, (c.g.a.b)localObject2, localb);
    d = ((p)localObject1);
    paramb = new com/truecaller/adapter_delegates/p;
    paramb1 = (com.truecaller.adapter_delegates.b)paramb1;
    localObject1 = new com/truecaller/calling/dialer/ci$c;
    ((ci.c)localObject1).<init>(this);
    localObject1 = (c.g.a.b)localObject1;
    localObject2 = (c.g.a.b)ci.d.a;
    paramb.<init>(paramb1, 2131559021, (c.g.a.b)localObject1, (c.g.a.b)localObject2);
    e = paramb;
    paramb = new com/truecaller/adapter_delegates/f;
    paramb1 = d;
    localObject1 = (a)e;
    localObject2 = new com/truecaller/adapter_delegates/g;
    ((com.truecaller.adapter_delegates.g)localObject2).<init>((byte)0);
    localObject2 = (s)localObject2;
    paramb1 = (a)paramb1.a((a)localObject1, (s)localObject2);
    paramb.<init>(paramb1);
    paramb.setHasStableIds(true);
    f = paramb;
    paramb = d();
    k.a(paramb, "recycleView");
    paramb1 = new android/support/v7/widget/LinearLayoutManager;
    paramView = paramView.getContext();
    paramb1.<init>(paramView, 0, false);
    paramb1 = (RecyclerView.LayoutManager)paramb1;
    paramb.setLayoutManager(paramb1);
    paramView = d();
    k.a(paramView, "recycleView");
    paramb = (RecyclerView.Adapter)f;
    paramView.setAdapter(paramb);
  }
  
  private final RecyclerView d()
  {
    return (RecyclerView)b.b();
  }
  
  public final int a()
  {
    f.notifyDataSetChanged();
    return d.getItemCount();
  }
  
  public final void a(List paramList1, List paramList2)
  {
    k.b(paramList1, "oldItems");
    String str = "newItems";
    k.b(paramList2, str);
    int i = paramList1.size();
    int j = paramList2.size();
    if (i < j)
    {
      f.notifyItemInserted(0);
      return;
    }
    int k = paramList1.size();
    int m = paramList2.size();
    if (k > m)
    {
      f.notifyItemRemoved(0);
      return;
    }
    f.notifyItemChanged(0);
  }
  
  public final void a(Set paramSet)
  {
    Object localObject = "itemPositions";
    k.b(paramSet, (String)localObject);
    paramSet = ((Iterable)paramSet).iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      localObject = (Number)paramSet.next();
      int i = ((Number)localObject).intValue();
      com.truecaller.adapter_delegates.f localf = f;
      p localp = d;
      i = localp.a_(i);
      localf.notifyItemChanged(i);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = d();
    k.a(localObject, "recycleView");
    localObject = (View)localObject;
    boolean bool = paramBoolean ^ true;
    com.truecaller.utils.extensions.t.a((View)localObject, bool);
    localObject = (LinearLayout)c.b();
    k.a(localObject, "emptyStateLinearLayout");
    com.truecaller.utils.extensions.t.a((View)localObject, paramBoolean);
  }
  
  public final void b()
  {
    Parcelable localParcelable = g;
    if (localParcelable != null)
    {
      Object localObject = d();
      String str = "recycleView";
      k.a(localObject, str);
      localObject = ((RecyclerView)localObject).getLayoutManager();
      if (localObject != null) {
        ((RecyclerView.LayoutManager)localObject).onRestoreInstanceState(localParcelable);
      }
      g = null;
      return;
    }
  }
  
  public final void c()
  {
    Object localObject = d();
    String str = "recycleView";
    k.a(localObject, str);
    localObject = ((RecyclerView)localObject).getLayoutManager();
    if (localObject != null) {
      localObject = ((RecyclerView.LayoutManager)localObject).onSaveInstanceState();
    } else {
      localObject = null;
    }
    g = ((Parcelable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ci
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */