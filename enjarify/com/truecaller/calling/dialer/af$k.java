package com.truecaller.calling.dialer;

import android.os.CancellationSignal;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

final class af$k
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  int c;
  private ag f;
  
  af$k(af paramaf, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    k localk = new com/truecaller/calling/dialer/af$k;
    af localaf = d;
    String str = e;
    localk.<init>(localaf, str, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localk;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = c;
    int j = 2;
    String str = null;
    Object localObject2;
    Object localObject4;
    Object localObject5;
    Object localObject6;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label539;
      }
      throw a;
    case 1: 
      localObject2 = (ao)a;
      boolean bool3 = paramObject instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label624;
      }
      paramObject = f;
      localObject2 = d;
      localObject3 = e;
      localObject4 = (bz)g.b;
      ((af)localObject2).a((String)localObject3, (bz)localObject4);
      localObject2 = ((ag)paramObject).V_();
      localObject3 = new com/truecaller/calling/dialer/af$k$a;
      ((af.k.a)localObject3).<init>(this, null);
      localObject3 = (c.g.a.m)localObject3;
      localObject2 = e.a((ag)paramObject, (f)localObject2, (c.g.a.m)localObject3, j);
      localObject3 = d.s;
      paramObject = d.j;
      localObject4 = d.A;
      localObject4 = ((bn)paramObject).plus((f)localObject4);
      localObject5 = e;
      localObject6 = Integer.valueOf(100);
      CancellationSignal localCancellationSignal = d.k;
      a = localObject2;
      k = 1;
      c = k;
      paramObject = ((br)localObject3).a((f)localObject4, (String)localObject5, (Integer)localObject6, localCancellationSignal, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Iterable)paramObject;
    Object localObject3 = new java/util/ArrayList;
    int m = c.a.m.a((Iterable)paramObject, 10);
    ((ArrayList)localObject3).<init>(m);
    localObject3 = (Collection)localObject3;
    paramObject = ((Iterable)paramObject).iterator();
    for (;;)
    {
      boolean bool4 = ((Iterator)paramObject).hasNext();
      if (!bool4) {
        break;
      }
      localObject4 = (n)((Iterator)paramObject).next();
      localObject5 = new com/truecaller/calling/dialer/av;
      localObject6 = (Contact)a;
      localObject4 = (String)b;
      ((av)localObject5).<init>((Contact)localObject6, (String)localObject4, null);
      ((Collection)localObject3).add(localObject5);
    }
    localObject3 = (List)localObject3;
    ((ao)localObject2).n();
    int k = ((List)localObject3).isEmpty();
    if (k != 0)
    {
      paramObject = d;
      str = e;
      localObject4 = (bz)bz.d.a;
      ((af)paramObject).a(str, (bz)localObject4);
      paramObject = d.s;
      str = e;
      localObject4 = d.j;
      localObject5 = d.A;
      localObject4 = ((bn)localObject4).plus((f)localObject5);
      a = localObject2;
      b = localObject3;
      c = j;
      paramObject = ((br)paramObject).a(str, (f)localObject4, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label539:
      paramObject = (Contact)paramObject;
      localObject1 = d;
      localObject2 = e;
      Object localObject7 = new com/truecaller/calling/dialer/bz$c;
      ((bz.c)localObject7).<init>((Contact)paramObject);
      localObject7 = (bz)localObject7;
      ((af)localObject1).a((String)localObject2, (bz)localObject7);
    }
    else
    {
      paramObject = d;
      localObject1 = e;
      localObject2 = new com/truecaller/calling/dialer/bz$a;
      ((bz.a)localObject2).<init>((List)localObject3);
      localObject2 = (bz)localObject2;
      ((af)paramObject).a((String)localObject1, (bz)localObject2);
    }
    return x.a;
    label624:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (k)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((k)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */