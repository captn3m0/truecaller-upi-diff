package com.truecaller.calling.dialer;

import c.g.b.k;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class cc
  implements bu
{
  private final HashMap a;
  
  public cc()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    a = localHashMap;
  }
  
  public final Set a(String paramString)
  {
    k.b(paramString, "searchTerm");
    return (Set)a.get(paramString);
  }
  
  public final void a()
  {
    a.clear();
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "searchTerm");
    Map localMap = (Map)a;
    Object localObject = localMap.get(paramString);
    if (localObject == null)
    {
      localObject = new java/util/HashSet;
      ((HashSet)localObject).<init>();
      localMap.put(paramString, localObject);
    }
    localObject = (Set)localObject;
    paramString = Integer.valueOf(paramInt);
    ((Set)localObject).add(paramString);
  }
  
  public final boolean a(int paramInt)
  {
    Object localObject1 = a.values();
    Object localObject2 = "keyToPositionsMap.values";
    k.a(localObject1, (String)localObject2);
    localObject1 = (Iterable)localObject1;
    boolean bool = localObject1 instanceof Collection;
    if (bool)
    {
      localObject2 = localObject1;
      localObject2 = (Collection)localObject1;
      bool = ((Collection)localObject2).isEmpty();
      if (bool) {}
    }
    else
    {
      localObject1 = ((Iterable)localObject1).iterator();
      do
      {
        bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        localObject2 = (Set)((Iterator)localObject1).next();
        Integer localInteger = Integer.valueOf(paramInt);
        bool = ((Set)localObject2).contains(localInteger);
      } while (!bool);
      return true;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */