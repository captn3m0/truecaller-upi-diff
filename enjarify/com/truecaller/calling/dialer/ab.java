package com.truecaller.calling.dialer;

import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private ab(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static ab a(Provider paramProvider1, Provider paramProvider2)
  {
    ab localab = new com/truecaller/calling/dialer/ab;
    localab.<init>(paramProvider1, paramProvider2);
    return localab;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */