package com.truecaller.calling.dialer;

import c.l.g;
import com.truecaller.calling.af;
import com.truecaller.data.entity.HistoryEvent;
import java.util.List;
import java.util.Map;

public abstract interface i$a
{
  public abstract cb a(i.b paramb);
  
  public abstract List a(i.b paramb, g paramg);
  
  public abstract void a(HistoryEvent paramHistoryEvent);
  
  public abstract boolean b(HistoryEvent paramHistoryEvent);
  
  public abstract Map e();
  
  public abstract af f();
  
  public abstract Map g();
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */