package com.truecaller.calling.dialer;

import c.a.m;
import c.g.b.k;
import c.l;
import c.m.i;
import com.truecaller.data.entity.HistoryEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class at
  implements as
{
  private final String a = "Slim";
  
  public final String a()
  {
    return a;
  }
  
  public final List a(List paramList)
  {
    Object localObject1 = "events";
    k.b(paramList, (String)localObject1);
    long l1 = System.currentTimeMillis();
    paramList = m.n((Iterable)paramList);
    Object localObject2 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject2).<init>();
    localObject2 = (Map)localObject2;
    paramList = paramList.a();
    Object localObject3;
    Object localObject4;
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      boolean bool3 = false;
      String str1 = null;
      k = 1;
      if (!bool1) {
        break;
      }
      localObject3 = paramList.next();
      localObject4 = localObject3;
      localObject4 = (HistoryEvent)localObject3;
      Object localObject5 = ((HistoryEvent)localObject4).b();
      if (localObject5 != null)
      {
        Object localObject6 = localObject5;
        localObject6 = (CharSequence)localObject5;
        int m = ((CharSequence)localObject6).length();
        if (m == 0)
        {
          m = 1;
        }
        else
        {
          m = 0;
          localObject6 = null;
        }
        boolean bool4;
        if (m != 0)
        {
          bool4 = false;
          localObject5 = null;
        }
        if (localObject5 != null)
        {
          localObject6 = new java/lang/StringBuilder;
          ((StringBuilder)localObject6).<init>();
          String str2 = ((HistoryEvent)localObject4).a();
          if (str2 != null) {
            localObject5 = str2;
          }
          ((StringBuilder)localObject6).append((String)localObject5);
          ((StringBuilder)localObject6).append("--");
          localObject5 = h.e;
          localObject4 = h.b.a((HistoryEvent)localObject4, null);
          bool4 = localObject4 instanceof h.e;
          int j;
          if (!bool4)
          {
            bool3 = localObject4 instanceof h.c;
            if (!bool3)
            {
              bool3 = localObject4 instanceof h.a;
              if (!bool3)
              {
                bool3 = localObject4 instanceof h.d;
                if (bool3)
                {
                  j = 2;
                  break label287;
                }
                paramList = new c/l;
                paramList.<init>();
                throw paramList;
              }
            }
            j = 1;
          }
          label287:
          ((StringBuilder)localObject6).append(j);
          str1 = ((StringBuilder)localObject6).toString();
          break label319;
        }
      }
      long l2 = ((HistoryEvent)localObject4).j();
      str1 = String.valueOf(l2);
      label319:
      Object localObject7 = ((Map)localObject2).get(str1);
      if (localObject7 == null)
      {
        localObject7 = new java/util/ArrayList;
        ((ArrayList)localObject7).<init>();
        ((Map)localObject2).put(str1, localObject7);
      }
      localObject7 = (List)localObject7;
      ((List)localObject7).add(localObject3);
    }
    paramList = new java/util/ArrayList;
    int i = ((Map)localObject2).size();
    paramList.<init>(i);
    paramList = (Collection)paramList;
    localObject2 = ((Map)localObject2).entrySet().iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      localObject4 = new com/truecaller/calling/dialer/ce;
      localObject3 = (List)((Map.Entry)localObject3).getValue();
      ((ce)localObject4).<init>((List)localObject3);
      paramList.add(localObject4);
    }
    paramList = (List)paramList;
    long l3 = System.currentTimeMillis() - l1;
    localObject1 = new String[k];
    Object localObject8 = new java/lang/StringBuilder;
    ((StringBuilder)localObject8).<init>("Merged ");
    int k = paramList.size();
    ((StringBuilder)localObject8).append(k);
    ((StringBuilder)localObject8).append(" history events in ");
    ((StringBuilder)localObject8).append(l3);
    ((StringBuilder)localObject8).append("ms");
    localObject8 = ((StringBuilder)localObject8).toString();
    localObject1[0] = localObject8;
    return paramList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.at
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */