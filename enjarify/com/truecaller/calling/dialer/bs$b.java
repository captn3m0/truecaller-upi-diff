package com.truecaller.calling.dialer;

import android.os.CancellationSignal;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.data.access.s;
import kotlinx.coroutines.ag;

final class bs$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag f;
  
  bs$b(bs parambs, String paramString, CancellationSignal paramCancellationSignal, Integer paramInteger, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/calling/dialer/bs$b;
    bs localbs = b;
    String str = c;
    CancellationSignal localCancellationSignal = d;
    Integer localInteger = e;
    localb.<init>(localbs, str, localCancellationSignal, localInteger, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.a;
        localObject = c;
        CancellationSignal localCancellationSignal = d;
        Integer localInteger1 = e;
        if (localInteger1 != null)
        {
          ((Number)localInteger1).intValue();
          localInteger1 = Integer.valueOf(0);
        }
        else
        {
          localInteger1 = null;
        }
        Integer localInteger2 = e;
        return ((s)paramObject).a((String)localObject, localCancellationSignal, localInteger1, localInteger2);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bs.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */