package com.truecaller.calling.dialer;

import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.ui.details.DetailsFragment.SourceType;

public abstract interface t
{
  public abstract void a(Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3);
  
  public abstract void a(HistoryEvent paramHistoryEvent, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2);
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */