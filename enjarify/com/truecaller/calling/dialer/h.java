package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.calling.ActionType;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.util.cf;
import com.truecaller.utils.n;

public abstract class h
{
  public static final h.b e;
  public final boolean a;
  final Integer b;
  public final ActionType c;
  final ActionType d;
  
  static
  {
    h.b localb = new com/truecaller/calling/dialer/h$b;
    localb.<init>((byte)0);
    e = localb;
  }
  
  private h(boolean paramBoolean, Integer paramInteger, ActionType paramActionType1, ActionType paramActionType2)
  {
    a = paramBoolean;
    b = paramInteger;
    c = paramActionType1;
    d = paramActionType2;
  }
  
  public static final h a(HistoryEvent paramHistoryEvent, cf paramcf)
  {
    return h.b.a(paramHistoryEvent, paramcf);
  }
  
  public final String a(n paramn)
  {
    k.b(paramn, "resourceProvider");
    Integer localInteger = b;
    if (localInteger != null)
    {
      int i = ((Number)localInteger).intValue();
      Object[] arrayOfObject = new Object[0];
      return paramn.a(i, arrayOfObject);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */