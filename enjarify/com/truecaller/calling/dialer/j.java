package com.truecaller.calling.dialer;

import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.d;
import com.truecaller.analytics.e.a;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.ae;
import com.truecaller.calling.af;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.flashsdk.core.i;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.cf;
import com.truecaller.voip.ai;
import java.util.List;
import java.util.Map;

public abstract class j
  extends d
  implements i.b
{
  final i.a c;
  final bd d;
  final com.truecaller.flashsdk.core.b e;
  final cf f;
  private final i.a g;
  private final com.truecaller.analytics.b h;
  private final a i;
  private final i j;
  private final ai k;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(j.class);
    ((u)localObject).<init>(localb, "historyEvents", "getHistoryEvents()Ljava/util/List;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public j(i.a parama, bd parambd, com.truecaller.analytics.b paramb, a parama1, i parami, com.truecaller.flashsdk.core.b paramb1, cf paramcf, ai paramai)
  {
    c = parama;
    d = parambd;
    h = paramb;
    i = parama1;
    j = parami;
    e = paramb1;
    f = paramcf;
    k = paramai;
    parama = c;
    g = parama;
  }
  
  private final c.x a(HistoryEvent paramHistoryEvent)
  {
    paramHistoryEvent = paramHistoryEvent.s();
    if (paramHistoryEvent != null)
    {
      c.g.b.k.a(paramHistoryEvent, "contact");
      paramHistoryEvent = paramHistoryEvent.A();
      Object localObject = "contact.numbers";
      c.g.b.k.a(paramHistoryEvent, (String)localObject);
      paramHistoryEvent = (com.truecaller.data.entity.Number)c.a.m.e(paramHistoryEvent);
      if (paramHistoryEvent != null)
      {
        localObject = k;
        paramHistoryEvent = paramHistoryEvent.a();
        c.g.b.k.a(paramHistoryEvent, "number.normalizedNumber");
        String str = "callLog";
        ((ai)localObject).a(paramHistoryEvent, str);
      }
      return c.x.a;
    }
    return null;
  }
  
  private final void a(HistoryEvent paramHistoryEvent, String paramString)
  {
    bd localbd = d;
    paramHistoryEvent = paramHistoryEvent.b();
    c.g.b.k.a(paramHistoryEvent, "historyEvent.rawNumber");
    localbd.a(paramHistoryEvent, "callHistory");
    b("message", paramString);
  }
  
  private final void a(HistoryEvent paramHistoryEvent, boolean paramBoolean, String paramString)
  {
    String str = paramHistoryEvent.b();
    if (str == null) {
      return;
    }
    bd localbd = d;
    Object localObject = paramHistoryEvent.s();
    if (localObject != null)
    {
      localObject = ((Contact)localObject).s();
      if (localObject != null) {}
    }
    else
    {
      localObject = paramHistoryEvent.e();
    }
    localbd.a(str, (String)localObject, paramBoolean, "callHistory");
    b("call", paramString);
  }
  
  private final boolean a(int paramInt, ActionType paramActionType)
  {
    HistoryEvent localHistoryEvent = b(paramInt);
    a(localHistoryEvent, paramActionType, "swipe");
    return true;
  }
  
  private final void b(HistoryEvent paramHistoryEvent, String paramString)
  {
    bd localbd = d;
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.CallLog;
    localbd.a(paramHistoryEvent, localSourceType, false, false);
    b("details", paramString);
  }
  
  private final void b(String paramString1, String paramString2)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("ViewAction");
    String str1 = "callLog";
    localObject = ((e.a)localObject).a("Context", str1);
    String str2 = "Action";
    paramString1 = ((e.a)localObject).a(str2, paramString1);
    if (paramString2 != null)
    {
      localObject = "SubAction";
      paramString1.a((String)localObject, paramString2);
    }
    paramString2 = h;
    paramString1 = paramString1.a();
    c.g.b.k.a(paramString1, "eventBuilder.build()");
    paramString2.b(paramString1);
  }
  
  private final void c(HistoryEvent paramHistoryEvent, String paramString)
  {
    String str = paramHistoryEvent.a();
    if (str == null) {
      return;
    }
    c.g.b.k.a(str, "historyEvent.normalizedNumber ?: return");
    bd localbd = d;
    paramHistoryEvent = paramHistoryEvent.s();
    localbd.a(paramHistoryEvent, str, paramString, "callHistory");
  }
  
  private final void e(int paramInt)
  {
    HistoryEvent localHistoryEvent = b(paramInt);
    c.a(localHistoryEvent);
  }
  
  protected final com.truecaller.calling.x a(String paramString1, String paramString2)
  {
    Object localObject = j;
    boolean bool1 = ae.a((i)localObject);
    if ((bool1) && (paramString1 != null) && (paramString2 != null))
    {
      boolean bool2 = ((i)localObject).b(paramString1);
      if (!bool2) {
        paramString1 = null;
      }
      if (paramString1 != null)
      {
        localObject = "+";
        String str = "";
        paramString1 = c.n.m.d(c.n.m.a(paramString1, (String)localObject, str));
        if (paramString1 != null)
        {
          long l = ((Number)paramString1).longValue();
          paramString1 = new com/truecaller/calling/x;
          localObject = c.a.m.a(Long.valueOf(l));
          paramString1.<init>((List)localObject, paramString2, "callHistory");
          return paramString1;
        }
        return null;
      }
      return null;
    }
    return null;
  }
  
  protected final List a()
  {
    i.a locala = g;
    Object localObject = this;
    localObject = (i.b)this;
    c.l.g localg = b[0];
    return locala.a((i.b)localObject, localg);
  }
  
  public boolean a(com.truecaller.adapter_delegates.h paramh)
  {
    String str1 = "event";
    c.g.b.k.b(paramh, str1);
    int m = b;
    Object localObject = e;
    boolean bool1 = localObject instanceof ActionType;
    if (!bool1) {
      localObject = null;
    }
    localObject = (ActionType)localObject;
    if (localObject == null) {
      localObject = ActionType.CELLULAR_CALL;
    }
    paramh = a;
    int n = paramh.hashCode();
    boolean bool2 = true;
    boolean bool3;
    String str2;
    switch (n)
    {
    default: 
      break;
    case 1140909776: 
      localObject = "ItemEvent.INVALIDATE_ITEM";
      bool3 = paramh.equals(localObject);
      if (bool3)
      {
        paramh = new String[bool2];
        str2 = String.valueOf(m);
        localObject = "invalidate view for position : ".concat(str2);
        paramh[0] = localObject;
        c.f().a(m);
        return bool2;
      }
      break;
    case 39226006: 
      localObject = "ItemEvent.SWIPE_START";
      bool3 = paramh.equals(localObject);
      if (bool3) {
        return d(m);
      }
      break;
    case -245760723: 
      str2 = "ItemEvent.SWIPE_COMPLETED_FROM_START";
      bool3 = paramh.equals(str2);
      if (bool3) {
        return a(m, (ActionType)localObject);
      }
      break;
    case -1314591573: 
      localObject = "ItemEvent.LONG_CLICKED";
      bool3 = paramh.equals(localObject);
      if (bool3)
      {
        bool3 = a;
        if (!bool3)
        {
          i.b();
          e(m);
          return bool2;
        }
        return false;
      }
      break;
    case -1743572928: 
      str2 = "ItemEvent.CLICKED";
      bool3 = paramh.equals(str2);
      if (bool3)
      {
        bool3 = a;
        if (bool3)
        {
          e(m);
          return bool2;
        }
        return a((ActionType)localObject, m);
      }
      break;
    case -1837138842: 
      localObject = "ItemEvent.SWIPE_COMPLETED_FROM_END";
      bool3 = paramh.equals(localObject);
      if (bool3)
      {
        paramh = ActionType.SMS;
        return a(m, paramh);
      }
      break;
    }
    return false;
  }
  
  protected abstract boolean a(ActionType paramActionType, int paramInt);
  
  protected final boolean a(HistoryEvent paramHistoryEvent, ActionType paramActionType, String paramString)
  {
    c.g.b.k.b(paramHistoryEvent, "historyEvent");
    c.g.b.k.b(paramActionType, "action");
    int[] arrayOfInt = k.a;
    int m = paramActionType.ordinal();
    m = arrayOfInt[m];
    boolean bool = true;
    switch (m)
    {
    default: 
      break;
    case 7: 
      a(paramHistoryEvent);
      break;
    case 6: 
      b(paramHistoryEvent, paramString);
      break;
    case 5: 
      a(paramHistoryEvent, paramString);
      break;
    case 4: 
      m = 0;
      paramActionType = null;
      a(paramHistoryEvent, false, paramString);
      break;
    case 3: 
      a(paramHistoryEvent, bool, paramString);
      break;
    case 2: 
      paramActionType = "video";
      c(paramHistoryEvent, paramActionType);
      break;
    case 1: 
      paramActionType = "call";
      c(paramHistoryEvent, paramActionType);
    }
    return bool;
  }
  
  protected final HistoryEvent b(int paramInt)
  {
    return agetc;
  }
  
  protected final boolean c(int paramInt)
  {
    Object localObject1 = b(paramInt);
    String str1 = ((HistoryEvent)localObject1).a();
    boolean bool1 = false;
    if (str1 == null) {
      return false;
    }
    c.g.b.k.a(str1, "historyEvent.normalizedNumber ?: return false");
    Map localMap = c.g();
    Integer localInteger = Integer.valueOf(paramInt);
    Object localObject2 = localMap.get(localInteger);
    if (localObject2 == null)
    {
      localObject2 = h.e;
      localObject2 = f;
      localObject2 = h.b.a((HistoryEvent)localObject1, (cf)localObject2);
      h.c localc = h.c.f;
      boolean bool2 = c.g.b.k.a(localObject2, localc);
      if (bool2)
      {
        long l1 = System.currentTimeMillis();
        long l2 = ((HistoryEvent)localObject1).j();
        l1 -= l2;
        l2 = 60000L;
        boolean bool3 = l1 < l2;
        if (bool3)
        {
          l1 = System.currentTimeMillis();
          localObject1 = e;
          String str2 = "+";
          String str3 = "";
          str1 = c.n.m.a(str1, str2, str3);
          localObject1 = ((com.truecaller.flashsdk.core.b)localObject1).g(str1);
          long l3 = b;
          l1 -= l3;
          bool3 = l1 < l2;
          if (bool3) {
            bool1 = true;
          }
        }
      }
      localObject2 = Boolean.valueOf(bool1);
      localMap.put(localInteger, localObject2);
    }
    return ((Boolean)localObject2).booleanValue();
  }
  
  public abstract boolean d(int paramInt);
  
  public int getItemCount()
  {
    return a().size();
  }
  
  public long getItemId(int paramInt)
  {
    Long localLong = b(paramInt).getId();
    if (localLong != null) {
      return localLong.longValue();
    }
    return -1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */