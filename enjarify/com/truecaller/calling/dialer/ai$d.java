package com.truecaller.calling.dialer;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.ui.b;
import com.truecaller.flashsdk.ui.b.a;

final class ai$d
  implements ViewTreeObserver.OnGlobalLayoutListener
{
  ai$d(ai paramai) {}
  
  public final void onGlobalLayout()
  {
    Object localObject1 = a;
    boolean bool1 = ai.g((ai)localObject1);
    if (bool1)
    {
      localObject1 = a;
      bool1 = ai.i((ai)localObject1);
      if (!bool1)
      {
        localObject1 = ai.j(a);
        if (localObject1 == null)
        {
          localObject1 = ai.k(a);
          k.a(localObject1, "recycler");
          localObject1 = ((RecyclerView)localObject1).getAdapter();
          int i;
          if (localObject1 != null)
          {
            i = ((RecyclerView.Adapter)localObject1).getItemCount();
          }
          else
          {
            i = 0;
            localObject1 = null;
          }
          int j = 0;
          Object localObject2 = null;
          while (j < i)
          {
            Object localObject3 = ai.a(a, j);
            if (localObject3 != null)
            {
              int k = ((View)localObject3).getVisibility();
              if (k == 0)
              {
                float f1 = 2.8E-45F;
                Object localObject4 = new int[2];
                ((View)localObject3).getLocationOnScreen((int[])localObject4);
                boolean bool3 = true;
                int n = localObject4[bool3];
                int i1 = ai.l(a);
                n += i1;
                k = localObject4[bool3];
                Object localObject5 = a;
                i1 = ai.m((ai)localObject5);
                if (k > i1)
                {
                  f1 = n;
                  ViewGroup localViewGroup = ai.n(a);
                  localObject5 = "dialpadView";
                  k.a(localViewGroup, (String)localObject5);
                  float f2 = localViewGroup.getY();
                  boolean bool2 = f1 < f2;
                  if (bool2)
                  {
                    localObject1 = new com/truecaller/flashsdk/ui/b;
                    localObject2 = ((View)localObject3).getContext();
                    localObject4 = "flashButton.context";
                    k.a(localObject2, (String)localObject4);
                    ((b)localObject1).<init>((Context)localObject2, (View)localObject3);
                    localObject2 = b;
                    if (localObject2 == null)
                    {
                      localObject2 = new android/widget/PopupWindow;
                      localObject3 = a;
                      f1 = 0.0F / 0.0F;
                      n = -2;
                      f2 = 0.0F / 0.0F;
                      ((PopupWindow)localObject2).<init>((View)localObject3, -1, n, bool3);
                      b = ((PopupWindow)localObject2);
                      localObject2 = a;
                      int i2 = R.id.text;
                      localObject2 = ((View)localObject2).findViewById(i2);
                      k.a(localObject2, "contentView.findViewById(R.id.text)");
                      localObject2 = (TextView)localObject2;
                      localObject3 = a.getContext();
                      localObject4 = "contentView.context";
                      k.a(localObject3, (String)localObject4);
                      localObject3 = ((Context)localObject3).getResources();
                      int m = R.string.flash_tooltip_text;
                      localObject3 = ((Resources)localObject3).getText(m);
                      ((TextView)localObject2).setText((CharSequence)localObject3);
                      localObject2 = b;
                      if (localObject2 != null)
                      {
                        localObject3 = new android/graphics/drawable/ColorDrawable;
                        ((ColorDrawable)localObject3).<init>();
                        localObject3 = (Drawable)localObject3;
                        ((PopupWindow)localObject2).setBackgroundDrawable((Drawable)localObject3);
                      }
                    }
                    localObject2 = c.getViewTreeObserver();
                    localObject3 = localObject1;
                    localObject3 = (ViewTreeObserver.OnGlobalLayoutListener)localObject1;
                    ((ViewTreeObserver)localObject2).addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject3);
                    localObject2 = b;
                    if (localObject2 != null)
                    {
                      localObject3 = new com/truecaller/flashsdk/ui/b$a;
                      ((b.a)localObject3).<init>((b)localObject1);
                      localObject3 = (PopupWindow.OnDismissListener)localObject3;
                      ((PopupWindow)localObject2).setOnDismissListener((PopupWindow.OnDismissListener)localObject3);
                    }
                    localObject2 = b;
                    if (localObject2 != null)
                    {
                      localObject3 = c;
                      ((PopupWindow)localObject2).showAsDropDown((View)localObject3, 0, 0);
                    }
                    ai.a(a, (b)localObject1);
                    ai.a(a).C();
                    ai.h(a);
                    ai.o(a);
                    return;
                  }
                }
              }
            }
            j += 1;
          }
          return;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ai.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */