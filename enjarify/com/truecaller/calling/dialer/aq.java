package com.truecaller.calling.dialer;

public final class aq
{
  final int a;
  final int b;
  final boolean c;
  
  public aq(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramBoolean;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof aq;
      if (bool2)
      {
        paramObject = (aq)paramObject;
        int i = a;
        int j = a;
        if (i == j) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0)
        {
          i = b;
          j = b;
          if (i == j) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0)
          {
            boolean bool3 = c;
            boolean bool4 = c;
            if (bool3 == bool4)
            {
              bool4 = true;
            }
            else
            {
              bool4 = false;
              paramObject = null;
            }
            if (bool4) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    int j = b;
    i = (i + j) * 31;
    int k = c;
    if (k != 0) {
      k = 1;
    }
    return i + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("EmptyViewData(titleRes=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", buttonTextRes=");
    i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", shouldShowSubtitleText=");
    boolean bool = c;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */