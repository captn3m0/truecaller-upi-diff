package com.truecaller.calling.dialer;

import android.content.Context;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import c.g.b.k;

public final class b
  implements ax
{
  private final Context a;
  
  public b(Context paramContext)
  {
    a = paramContext;
  }
  
  public final String a(int paramInt)
  {
    Context localContext = a;
    paramInt = ContactsContract.CommonDataKinds.Phone.getTypeLabelResource(paramInt);
    String str = localContext.getString(paramInt);
    k.a(str, "applicationContext.getSt…peLabelResource(telType))");
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */