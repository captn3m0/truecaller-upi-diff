package com.truecaller.calling.dialer;

import com.truecaller.ui.view.BottomBar.DialpadState;
import com.truecaller.util.ca;

public abstract interface aj$d
  extends aj.a
{
  public abstract void a(BottomBar.DialpadState paramDialpadState);
  
  public abstract void a(ca paramca);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(String paramString);
  
  public abstract void c();
  
  public abstract void c(String paramString);
  
  public abstract void d();
  
  public abstract void d(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.aj.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */