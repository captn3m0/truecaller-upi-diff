package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import com.truecaller.analytics.TimingEvent;
import com.truecaller.analytics.au;
import com.truecaller.bd;
import com.truecaller.callhistory.FilterType;
import com.truecaller.data.entity.HistoryEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlinx.coroutines.bn;

final class af$h
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private kotlinx.coroutines.ag h;
  
  af$h(af paramaf, FilterType paramFilterType, int paramInt, boolean paramBoolean, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    h localh = new com/truecaller/calling/dialer/af$h;
    af localaf = d;
    FilterType localFilterType = e;
    int i = f;
    boolean bool = g;
    localh.<init>(localaf, localFilterType, i, bool, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    h = ((kotlinx.coroutines.ag)paramObject);
    return localh;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = c;
    int k = 1;
    boolean bool1;
    Object localObject2;
    Object localObject5;
    Object localObject6;
    Object localObject7;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject1 = (af)b;
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label1131;
      }
      paramObject = d;
      bool2 = ((af)paramObject).L();
      if (bool2)
      {
        paramObject = new com/truecaller/calling/dialer/at;
        ((at)paramObject).<init>();
      }
      else
      {
        paramObject = new com/truecaller/calling/dialer/ar;
        ((ar)paramObject).<init>();
      }
      localObject2 = (as)paramObject;
      paramObject = d;
      localObject3 = r;
      localObject4 = d.h;
      localObject5 = d.A;
      localObject5 = ((bn)localObject4).plus((f)localObject5);
      localObject6 = e;
      j = f;
      Integer localInteger = Integer.valueOf(j);
      a = localObject2;
      b = paramObject;
      c = k;
      localObject7 = this;
      localObject4 = ((g)localObject3).a((f)localObject5, (FilterType)localObject6, (as)localObject2, localInteger, this);
      if (localObject4 == localObject1) {
        return localObject1;
      }
      localObject1 = paramObject;
      paramObject = localObject4;
    }
    paramObject = (List)paramObject;
    f = ((List)paramObject);
    d.v.a();
    d.I();
    paramObject = d;
    localObject1 = (Collection)d;
    boolean bool3 = ((Collection)localObject1).isEmpty() ^ k;
    int j = 0;
    Object localObject4 = null;
    boolean bool4;
    if (bool3)
    {
      localObject1 = (Iterable)f;
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject3 = (Collection)localObject3;
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        bool4 = ((Iterator)localObject1).hasNext();
        if (!bool4) {
          break;
        }
        localObject5 = ((Iterator)localObject1).next();
        localObject6 = localObject5;
        localObject6 = (Iterable)a;
        boolean bool5 = localObject6 instanceof Collection;
        if (bool5)
        {
          localObject2 = localObject6;
          localObject2 = (Collection)localObject6;
          bool5 = ((Collection)localObject2).isEmpty();
          if (bool5) {}
        }
        else
        {
          localObject6 = ((Iterable)localObject6).iterator();
          do
          {
            bool5 = ((Iterator)localObject6).hasNext();
            if (!bool5) {
              break;
            }
            long l = ((Number)((Iterator)localObject6).next()).longValue();
            localObject7 = d;
            localObject2 = Long.valueOf(l);
            bool5 = ((Set)localObject7).contains(localObject2);
          } while (!bool5);
          n = 1;
          break label486;
        }
        int n = 0;
        localObject6 = null;
        label486:
        if (n != 0) {
          ((Collection)localObject3).add(localObject5);
        }
      }
      localObject3 = (Iterable)localObject3;
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject1 = (Collection)localObject1;
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        bool4 = ((Iterator)localObject3).hasNext();
        if (!bool4) {
          break;
        }
        localObject5 = nextc.getId();
        if (localObject5 != null) {
          ((Collection)localObject1).add(localObject5);
        }
      }
      localObject1 = (List)localObject1;
      d.clear();
      localObject3 = d;
      localObject1 = (Collection)localObject1;
      ((Set)localObject3).addAll((Collection)localObject1);
      localObject1 = d;
      bool3 = ((Set)localObject1).isEmpty();
      if (bool3)
      {
        localObject1 = (ae.b)c;
        if (localObject1 != null) {
          ((ae.b)localObject1).c();
        }
      }
      paramObject = (ae.b)c;
      if (paramObject != null) {
        ((ae.b)paramObject).d();
      }
    }
    paramObject = d;
    localObject1 = e;
    Object localObject3 = l;
    int m;
    if (localObject3 != localObject1)
    {
      l = ((FilterType)localObject1);
      localObject3 = FilterType.NONE;
      boolean bool6;
      if (localObject1 == localObject3)
      {
        bool6 = true;
      }
      else
      {
        bool6 = false;
        localObject3 = null;
      }
      if (!bool6)
      {
        localObject5 = (ae.c)b;
        if (localObject5 != null)
        {
          localObject6 = ag.b;
          m = ((FilterType)localObject1).ordinal();
          m = localObject6[m];
          switch (m)
          {
          default: 
            paramObject = new c/l;
            ((l)paramObject).<init>();
            throw ((Throwable)paramObject);
          case 6: 
            m = 2131888106;
            break;
          case 5: 
            m = 2131886593;
            break;
          case 4: 
            m = 2131886595;
            break;
          case 3: 
            m = 2131886596;
            break;
          case 2: 
            m = 2131886594;
            break;
          case 1: 
            m = 0;
            localObject1 = null;
          }
          ((ae.c)localObject5).c(m);
        }
      }
      localObject1 = (ae.c)b;
      if (localObject1 != null)
      {
        bool4 = ((af)paramObject).G();
        ((ae.c)localObject1).f(bool4);
      }
      localObject1 = (ae.c)b;
      if (localObject1 != null) {
        ((ae.c)localObject1).m();
      }
      localObject1 = (ae.c)b;
      if (localObject1 != null)
      {
        bool4 = ((af)paramObject).M();
        if ((bool4) && (bool6))
        {
          bool4 = true;
        }
        else
        {
          bool4 = false;
          localObject5 = null;
        }
        ((ae.c)localObject1).b(bool4);
      }
      localObject1 = (ae.c)b;
      if (localObject1 != null) {
        ((ae.c)localObject1).h(bool6);
      }
      paramObject = (ae.c)b;
      if (paramObject != null) {
        ((ae.c)paramObject).k();
      }
    }
    paramObject = (ae.c)d.b;
    if (paramObject != null)
    {
      localObject1 = d.J();
      ((ae.c)paramObject).a((aq)localObject1);
    }
    d.o.clear();
    paramObject = (ae.c)d.b;
    if (paramObject != null) {
      ((ae.c)paramObject).b();
    }
    boolean bool2 = g;
    if (bool2)
    {
      paramObject = (Collection)d.f;
      bool2 = ((Collection)paramObject).isEmpty() ^ k;
      if (bool2)
      {
        paramObject = d;
        m = 0;
        localObject1 = null;
        af.a((af)paramObject, null, false, k);
      }
    }
    paramObject = d.y;
    localObject1 = TimingEvent.CALL_LOG_STARTUP;
    ((au)paramObject).a((TimingEvent)localObject1);
    return x.a;
    label1131:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (h)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((h)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */