package com.truecaller.calling.dialer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View.OnClickListener;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.f;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.utils.extensions.t;

public final class AdsCloseView
  extends TintedImageView
{
  private final bp a;
  private final String b;
  
  public AdsCloseView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramAttributeSet = paramContext.getApplicationContext();
    if (paramAttributeSet != null)
    {
      paramAttributeSet = ((TrueApp)paramAttributeSet).a();
      k.a(paramAttributeSet, "(context.applicationCont… as TrueApp).objectsGraph");
      a = paramAttributeSet;
      paramAttributeSet = a.aF().ag().e();
      b = paramAttributeSet;
      paramAttributeSet = (CharSequence)b;
      localObject = (CharSequence)"megaAdsViews";
      boolean bool = m.a(paramAttributeSet, (CharSequence)localObject, false);
      if (bool)
      {
        t.a(this);
        paramAttributeSet = new com/truecaller/calling/dialer/AdsCloseView$1;
        paramAttributeSet.<init>(this, paramContext);
        paramAttributeSet = (View.OnClickListener)paramAttributeSet;
        setOnClickListener(paramAttributeSet);
      }
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.AdsCloseView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */