package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import com.truecaller.flashsdk.core.b;
import com.truecaller.flashsdk.db.l;
import java.util.List;
import java.util.Map;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.g;

final class af$g
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  af$g(af paramaf, Contact paramContact, int paramInt, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    g localg = new com/truecaller/calling/dialer/af$g;
    af localaf = b;
    Contact localContact = c;
    int i = d;
    localg.<init>(localaf, localContact, i, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localg;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    int k = 1;
    Object localObject2 = null;
    boolean bool2;
    boolean bool1;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label562;
      }
      paramObject = b.i;
      localObject3 = b.A;
      paramObject = ((bn)paramObject).plus((f)localObject3);
      localObject3 = new com/truecaller/calling/dialer/af$g$1;
      ((af.g.1)localObject3).<init>(this, null);
      localObject3 = (m)localObject3;
      a = k;
      paramObject = g.a((f)paramObject, (m)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Contact)paramObject;
    if (paramObject != null)
    {
      localObject1 = b.g.b;
      bool1 = localObject1 instanceof bz.a;
      if (!bool1)
      {
        bool2 = false;
        localObject1 = null;
      }
      localObject1 = (bz.a)localObject1;
      int j = d;
      c.g.b.k.a(paramObject, "refreshedContact");
      Object localObject4 = "contact";
      c.g.b.k.b(paramObject, (String)localObject4);
      if (localObject1 != null)
      {
        localObject4 = "newContact";
        c.g.b.k.b(paramObject, (String)localObject4);
        localObject1 = a;
        int n = ((List)localObject1).size();
        if (n > j)
        {
          n = 1;
        }
        else
        {
          n = 0;
          localObject4 = null;
        }
        if (n == 0)
        {
          bool2 = false;
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          localObject4 = (av)((List)localObject1).get(j);
          Long localLong1 = a.getId();
          Long localLong2 = ((Contact)paramObject).getId();
          boolean bool3 = c.g.b.k.a(localLong1, localLong2);
          if (bool3)
          {
            int i1 = 6;
            localObject2 = av.a((av)localObject4, (Contact)paramObject, null, null, i1);
            ((List)localObject1).set(j, localObject2);
            localObject1 = new String[k];
            localObject5 = new java/lang/StringBuilder;
            localObject2 = "T9 Replaced contact at pos: ";
            ((StringBuilder)localObject5).<init>((String)localObject2);
            ((StringBuilder)localObject5).append(j);
            localObject3 = ": id: ";
            ((StringBuilder)localObject5).append((String)localObject3);
            paramObject = ((Contact)paramObject).getId();
            ((StringBuilder)localObject5).append(paramObject);
            ((StringBuilder)localObject5).append(". Took ");
            long l1 = System.currentTimeMillis();
            paramObject = c;
            long l2;
            if (paramObject != null) {
              l2 = ((Long)paramObject).longValue();
            } else {
              l2 = 0L;
            }
            l1 -= l2;
            ((StringBuilder)localObject5).append(l1);
            ((StringBuilder)localObject5).append("ms");
            paramObject = ((StringBuilder)localObject5).toString();
            localObject1[0] = paramObject;
          }
          paramObject = x.a;
        }
      }
      paramObject = b;
      localObject1 = n;
      j = d;
      Object localObject5 = Integer.valueOf(j);
      localObject5 = (l)((Map)localObject1).get(localObject5);
      if (localObject5 != null)
      {
        paramObject = z;
        ((b)paramObject).a((l)localObject5);
      }
      paramObject = Integer.valueOf(j);
      ((Map)localObject1).remove(paramObject);
      paramObject = (ae.c)b.b;
      if (paramObject != null)
      {
        int m = d;
        ((ae.c)paramObject).b(m);
      }
    }
    return x.a;
    label562:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (g)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((g)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */