package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.util.cf;
import com.truecaller.utils.extensions.c;

public final class h$b
{
  public static h a(HistoryEvent paramHistoryEvent, cf paramcf)
  {
    String str = "historyEvent";
    k.b(paramHistoryEvent, str);
    if (paramcf != null)
    {
      int i = paramHistoryEvent.m();
      bool2 = paramcf.a(i);
      paramcf = Boolean.valueOf(bool2);
    }
    else
    {
      bool2 = false;
      paramcf = null;
    }
    boolean bool2 = c.a(paramcf);
    boolean bool1 = b(paramHistoryEvent);
    if (bool1)
    {
      paramHistoryEvent = new com/truecaller/calling/dialer/h$e;
      paramHistoryEvent.<init>(bool2);
      return (h)paramHistoryEvent;
    }
    bool1 = a(paramHistoryEvent);
    if (bool1) {
      return (h)h.c.f;
    }
    boolean bool3 = c(paramHistoryEvent);
    if (bool3)
    {
      paramHistoryEvent = new com/truecaller/calling/dialer/h$d;
      paramHistoryEvent.<init>();
      return (h)paramHistoryEvent;
    }
    paramHistoryEvent = new com/truecaller/calling/dialer/h$a;
    paramHistoryEvent.<init>(bool2);
    return (h)paramHistoryEvent;
  }
  
  private static boolean a(HistoryEvent paramHistoryEvent)
  {
    int i = paramHistoryEvent.r();
    int j = 3;
    return i == j;
  }
  
  private static boolean b(HistoryEvent paramHistoryEvent)
  {
    paramHistoryEvent = paramHistoryEvent.q();
    return k.a("com.whatsapp", paramHistoryEvent);
  }
  
  private static boolean c(HistoryEvent paramHistoryEvent)
  {
    paramHistoryEvent = paramHistoryEvent.q();
    return k.a("com.truecaller.voip.manager.VOIP", paramHistoryEvent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */