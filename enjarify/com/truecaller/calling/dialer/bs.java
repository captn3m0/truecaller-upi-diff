package com.truecaller.calling.dialer;

import android.os.CancellationSignal;
import c.d.c;
import c.d.f;
import c.g.a.m;
import com.truecaller.data.access.s;
import com.truecaller.network.search.l;
import kotlinx.coroutines.g;

public final class bs
  implements br
{
  final s a;
  final l b;
  
  public bs(s params, l paraml)
  {
    a = params;
    b = paraml;
  }
  
  public final Object a(f paramf, String paramString, Integer paramInteger, CancellationSignal paramCancellationSignal, c paramc)
  {
    Object localObject = new com/truecaller/calling/dialer/bs$b;
    ((bs.b)localObject).<init>(this, paramString, paramCancellationSignal, paramInteger, null);
    localObject = (m)localObject;
    return g.a(paramf, (m)localObject, paramc);
  }
  
  public final Object a(String paramString, f paramf, c paramc)
  {
    Object localObject = new com/truecaller/calling/dialer/bs$a;
    ((bs.a)localObject).<init>(this, 1000L, paramString, null);
    localObject = (m)localObject;
    return g.a(paramf, (m)localObject, paramc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bs
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */