package com.truecaller.calling.dialer;

import dagger.a.d;
import javax.inject.Provider;

public final class cp
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private cp(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static cp a(Provider paramProvider1, Provider paramProvider2)
  {
    cp localcp = new com/truecaller/calling/dialer/cp;
    localcp.<init>(paramProvider1, paramProvider2);
    return localcp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cp
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */