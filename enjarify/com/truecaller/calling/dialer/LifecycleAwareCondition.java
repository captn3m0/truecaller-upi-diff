package com.truecaller.calling.dialer;

import android.arch.lifecycle.e;
import android.arch.lifecycle.e.b;
import android.arch.lifecycle.g;
import c.g.a.a;
import c.x;

public final class LifecycleAwareCondition
  implements g, q
{
  private a a;
  private final e b;
  private final e.b c;
  
  public LifecycleAwareCondition(e parame, e.b paramb)
  {
    b = parame;
    c = paramb;
    parame = b;
    paramb = this;
    paramb = (g)this;
    parame.a(paramb);
  }
  
  private final x onLifeCycleStateChange()
  {
    a locala = a;
    if (locala != null) {
      return (x)locala.invoke();
    }
    return null;
  }
  
  public final void a(a parama)
  {
    a = parama;
  }
  
  public final boolean a()
  {
    e.b localb1 = b.a();
    e.b localb2 = c;
    return localb1.a(localb2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.LifecycleAwareCondition
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */