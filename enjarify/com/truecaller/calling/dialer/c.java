package com.truecaller.calling.dialer;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import com.truecaller.common.c.b;

public abstract class c
  implements v
{
  v.a a;
  private final b b;
  private final ContentResolver c;
  private final Uri d;
  
  public c(ContentResolver paramContentResolver, Uri paramUri, Long paramLong)
  {
    c = paramContentResolver;
    d = paramUri;
    if (paramLong != null)
    {
      long l1 = paramLong.longValue();
      long l2 = 0L;
      boolean bool = l1 < l2;
      if (bool)
      {
        paramContentResolver = new com/truecaller/calling/dialer/c$a;
        Handler localHandler = new android/os/Handler;
        localHandler.<init>();
        long l3 = paramLong.longValue();
        paramContentResolver.<init>(this, paramLong, localHandler, l3);
        paramContentResolver = (b)paramContentResolver;
        break label116;
      }
    }
    paramContentResolver = new com/truecaller/calling/dialer/c$b;
    paramUri = new android/os/Handler;
    paramUri.<init>();
    paramContentResolver.<init>(this, paramUri);
    paramContentResolver = (b)paramContentResolver;
    label116:
    b = paramContentResolver;
  }
  
  protected abstract void a();
  
  public final void a(v.a parama)
  {
    Object localObject = a;
    int i = 1;
    int j;
    if (localObject != null)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject = null;
    }
    a = parama;
    parama = a;
    ContentObserver localContentObserver;
    if (parama == null)
    {
      i = 0;
      localContentObserver = null;
    }
    if ((i != 0) && (j == 0))
    {
      parama = c;
      localObject = d;
      localContentObserver = (ContentObserver)b;
      parama.registerContentObserver((Uri)localObject, false, localContentObserver);
      return;
    }
    if ((i == 0) && (j != 0))
    {
      parama = c;
      localObject = (ContentObserver)b;
      parama.unregisterContentObserver((ContentObserver)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */