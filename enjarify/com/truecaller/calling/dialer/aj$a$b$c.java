package com.truecaller.calling.dialer;

import c.g.b.k;

public final class aj$a$b$c
  extends aj.a.b
{
  final String a;
  
  public aj$a$b$c(String paramString)
  {
    super((byte)0);
    a = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof c;
      if (bool1)
      {
        paramObject = (c)paramObject;
        String str = a;
        paramObject = a;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("TapToPaste(number=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.aj.a.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */