package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.filters.FilterManager;
import java.util.List;
import kotlinx.coroutines.ag;

final class af$f$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  af$f$1(af.f paramf, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/calling/dialer/af$f$1;
    af.f localf = b;
    local1.<init>(localf, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        localObject = b.b.u;
        List localList = b.c;
        TruecallerContract.Filters.WildCardType localWildCardType = TruecallerContract.Filters.WildCardType.NONE;
        TruecallerContract.Filters.EntityType localEntityType = b.d;
        return Integer.valueOf(((FilterManager)localObject).a(localList, "PHONE_NUMBER", "callHistory", false, localWildCardType, localEntityType));
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.f.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */