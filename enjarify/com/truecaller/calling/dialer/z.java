package com.truecaller.calling.dialer;

import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d
{
  private final Provider a;
  
  private z(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static z a(Provider paramProvider)
  {
    z localz = new com/truecaller/calling/dialer/z;
    localz.<init>(paramProvider);
    return localz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */