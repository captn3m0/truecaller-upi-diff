package com.truecaller.calling.dialer;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.a.q;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import com.truecaller.adapter_delegates.i;
import com.truecaller.calling.an;
import com.truecaller.calling.ao;
import com.truecaller.calling.ba;
import com.truecaller.calling.bc;
import com.truecaller.calling.c;
import com.truecaller.calling.d;
import com.truecaller.calling.o;
import com.truecaller.calling.p;
import com.truecaller.search.local.model.c.a;
import com.truecaller.ui.view.AvailabilityView;

public final class cj
  extends RecyclerView.ViewHolder
  implements an, ao, bc, c, ck.c, p
{
  int b;
  q c;
  private final f d;
  private final f e;
  private final f f;
  
  static
  {
    g[] arrayOfg = new g[3];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(cj.class);
    ((u)localObject).<init>(localb, "titleTextView", "getTitleTextView()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(cj.class);
    ((u)localObject).<init>(localb, "pinBadge", "getPinBadge()Landroid/view/View;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(cj.class);
    ((u)localObject).<init>(localb, "availability", "getAvailability()Lcom/truecaller/ui/view/AvailabilityView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    a = arrayOfg;
  }
  
  private cj(View paramView, com.truecaller.adapter_delegates.k paramk, o paramo)
  {
    super(paramView);
    g = paramo;
    h = paramo;
    i = paramo;
    paramo = new com/truecaller/calling/ba;
    paramo.<init>(paramView);
    j = paramo;
    paramo = new com/truecaller/calling/d;
    paramo.<init>(paramView);
    k = paramo;
    b = -1;
    paramo = com.truecaller.utils.extensions.t.a(paramView, 2131363510);
    d = paramo;
    paramo = com.truecaller.utils.extensions.t.a(paramView, 2131363946);
    e = paramo;
    paramo = com.truecaller.utils.extensions.t.a(paramView, 2131362067);
    f = paramo;
    paramo = this;
    paramo = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, paramo, null, 12);
    i.a(paramView, paramk, paramo);
    a().setSingleLine();
    paramk = a();
    c.g.b.k.a(paramk, "titleTextView");
    boolean bool = true;
    paramk.setHorizontalFadingEdgeEnabled(bool);
    b().setSingleLine();
    paramk = b();
    c.g.b.k.a(paramk, "availability");
    paramk.setHorizontalFadingEdgeEnabled(bool);
    paramk = b();
    paramo = new com/truecaller/calling/dialer/cj$1;
    paramo.<init>(this, paramView);
    paramo = (c.g.a.b)paramo;
    paramk.setCustomTextProvider(paramo);
  }
  
  private final TextView a()
  {
    return (TextView)d.b();
  }
  
  private final AvailabilityView b()
  {
    return (AvailabilityView)f.b();
  }
  
  public final void a(int paramInt)
  {
    i.a(paramInt);
  }
  
  public final void a(q paramq)
  {
    c = paramq;
  }
  
  public final void a(c.a parama)
  {
    k.a(parama);
  }
  
  public final void a(Object paramObject)
  {
    h.a(paramObject);
  }
  
  public final void a_(boolean paramBoolean)
  {
    g.a_(paramBoolean);
  }
  
  public final void b(int paramInt)
  {
    b = paramInt;
  }
  
  public final void b(boolean paramBoolean)
  {
    View localView = (View)e.b();
    c.g.b.k.a(localView, "pinBadge");
    com.truecaller.utils.extensions.t.a(localView, paramBoolean);
  }
  
  public final void b_(String paramString)
  {
    j.b_(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */