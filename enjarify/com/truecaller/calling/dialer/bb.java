package com.truecaller.calling.dialer;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import c.f;
import c.g.b.u;
import c.l.g;
import com.truecaller.adapter_delegates.i;
import com.truecaller.calling.ao;
import com.truecaller.calling.ba;
import com.truecaller.calling.bc;
import com.truecaller.calling.o;
import com.truecaller.calling.p;
import com.truecaller.calling.x;
import com.truecaller.calling.y;
import com.truecaller.flashsdk.ui.ProgressAwareFlashButton;
import com.truecaller.flashsdk.ui.ProgressAwareFlashButton.b;

public final class bb
  extends RecyclerView.ViewHolder
  implements ao, bc, ay.b, p, com.truecaller.calling.w
{
  private final f b;
  private final View c;
  private final Object d;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = c.g.b.w.a(bb.class);
    ((u)localObject).<init>(localb, "flashButton", "getFlashButton()Lcom/truecaller/flashsdk/ui/ProgressAwareFlashButton;");
    localObject = (g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  private bb(View paramView, com.truecaller.adapter_delegates.k paramk, Object paramObject, o paramo)
  {
    super(paramView);
    y localy = new com/truecaller/calling/y;
    localy.<init>(paramView);
    e = localy;
    f = paramo;
    g = paramo;
    paramo = new com/truecaller/calling/ba;
    paramo.<init>(paramView);
    h = paramo;
    c = paramView;
    d = paramObject;
    paramView = com.truecaller.utils.extensions.t.a(c, 2131363109);
    b = paramView;
    paramView = c;
    paramObject = new com/truecaller/calling/dialer/bb$a;
    ((bb.a)paramObject).<init>(this, paramk);
    paramObject = (View.OnClickListener)paramObject;
    paramView.setOnClickListener((View.OnClickListener)paramObject);
    paramObject = this;
    paramObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)paramObject);
    paramView = a();
    paramView.setClickable(false);
    int i = com.truecaller.utils.ui.b.a(paramView.getContext(), 2130969528);
    paramView.setThemeColor(i);
    c.g.b.k.b(paramView, "receiver$0");
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramObject, "viewHolder");
    paramo = new com/truecaller/calling/dialer/bc$a;
    paramo.<init>((RecyclerView.ViewHolder)paramObject, paramk);
    paramo = (ProgressAwareFlashButton.b)paramo;
    paramView.setFlashProgressFinishListener(paramo);
  }
  
  private final ProgressAwareFlashButton a()
  {
    return (ProgressAwareFlashButton)b.b();
  }
  
  public final void a(int paramInt)
  {
    g.a(paramInt);
  }
  
  public final void a(x paramx)
  {
    e.a(paramx);
  }
  
  public final void a(Object paramObject)
  {
    f.a(paramObject);
  }
  
  public final void b_(String paramString)
  {
    h.b_(paramString);
  }
  
  public final void g(boolean paramBoolean)
  {
    c.setActivated(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */