package com.truecaller.calling.dialer;

import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import c.a.ae;
import c.a.m;
import c.g.b.k;
import c.k.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public final class l$a
  implements ActionMode.Callback
{
  final int a = 1;
  
  l$a(l paraml) {}
  
  public final boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
  {
    k.b(paramActionMode, "actionMode");
    k.b(paramMenuItem, "menuItem");
    paramActionMode = b.a();
    int i = a;
    int j = paramMenuItem.getItemId();
    return paramActionMode.a(i, j);
  }
  
  public final boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    k.b(paramActionMode, "actionMode");
    k.b(paramMenu, "menu");
    Object localObject1 = b.a();
    int i = a;
    int j = ((ae.a)localObject1).c(i);
    localObject1 = Integer.valueOf(j);
    Object localObject2 = localObject1;
    localObject2 = (Number)localObject1;
    i = ((Number)localObject2).intValue();
    boolean bool = true;
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject2 = null;
    }
    if (i == 0)
    {
      j = 0;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = (Number)localObject1;
      j = ((Number)localObject1).intValue();
      localObject2 = paramActionMode.getMenuInflater();
      ((MenuInflater)localObject2).inflate(j, paramMenu);
    }
    paramMenu = Integer.valueOf(a);
    paramActionMode.setTag(paramMenu);
    l.a(b, paramActionMode);
    paramActionMode = b.a();
    int k = a;
    paramActionMode.a(k);
    return bool;
  }
  
  public final void onDestroyActionMode(ActionMode paramActionMode)
  {
    k.b(paramActionMode, "actionMode");
    paramActionMode = b.a();
    int i = a;
    paramActionMode.b(i);
  }
  
  public final boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    k.b(paramActionMode, "actionMode");
    k.b(paramMenu, "menu");
    Object localObject1 = b.a().c();
    if (localObject1 != null)
    {
      localObject1 = (CharSequence)localObject1;
      paramActionMode.setTitle((CharSequence)localObject1);
    }
    int i = paramMenu.size();
    paramActionMode = (Iterable)i.b(0, i);
    localObject1 = new java/util/ArrayList;
    int j = m.a(paramActionMode, 10);
    ((ArrayList)localObject1).<init>(j);
    localObject1 = (Collection)localObject1;
    paramActionMode = paramActionMode.iterator();
    int k;
    for (;;)
    {
      boolean bool2 = paramActionMode.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject2 = paramActionMode;
      k = ((ae)paramActionMode).a();
      localObject2 = paramMenu.getItem(k);
      ((Collection)localObject1).add(localObject2);
    }
    localObject1 = (Iterable)localObject1;
    paramActionMode = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool3 = paramActionMode.hasNext();
      if (!bool3) {
        break;
      }
      paramMenu = (MenuItem)paramActionMode.next();
      k.a(paramMenu, "it");
      localObject1 = b.a();
      k = a;
      int m = paramMenu.getItemId();
      boolean bool1 = ((ae.a)localObject1).b(k, m);
      paramMenu.setVisible(bool1);
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.l.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */