package com.truecaller.calling.dialer;

import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private f(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static f a(Provider paramProvider1, Provider paramProvider2)
  {
    f localf = new com/truecaller/calling/dialer/f;
    localf.<init>(paramProvider1, paramProvider2);
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */