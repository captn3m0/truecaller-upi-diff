package com.truecaller.calling.dialer;

import android.os.CancellationSignal;
import android.view.View;
import c.a.y;
import c.g.b.k;
import c.g.b.z;
import c.t;
import c.x;
import com.truecaller.analytics.au;
import com.truecaller.analytics.bc;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.w;
import com.truecaller.bd;
import com.truecaller.callhistory.FilterType;
import com.truecaller.calling.ar;
import com.truecaller.common.h.ab;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.filters.FilterManager;
import com.truecaller.i.a;
import com.truecaller.messaging.j;
import com.truecaller.multisim.h;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.search.global.CompositeAdapterDelegate.SearchResultOrder;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.bs;

public final class af
  extends bd
  implements ae.a
{
  final c.d.f A;
  final j B;
  private boolean C;
  private boolean D;
  private final af.a E;
  private final af.b F;
  private boolean G;
  private boolean H;
  private boolean I;
  private boolean J;
  private List K;
  private v L;
  private bn M;
  private boolean N;
  private final Map O;
  private final com.truecaller.calling.af P;
  private final com.truecaller.calling.af Q;
  private final com.truecaller.calling.e.e R;
  private final al S;
  private final com.truecaller.i.c T;
  private final com.truecaller.analytics.b U;
  private final h V;
  private final com.truecaller.androidactors.f W;
  private final c.d.f X;
  private final com.truecaller.ads.provider.e Y;
  private final com.truecaller.search.local.model.c Z;
  d a;
  private final ar aa;
  private final a ab;
  private final com.truecaller.voip.d ac;
  private final com.truecaller.calling.dialer.a.b ad;
  private final com.truecaller.featuretoggles.e ae;
  final Set d;
  List e;
  List f;
  c.n g;
  final bn h;
  final bn i;
  bn j;
  CancellationSignal k;
  FilterType l;
  boolean m;
  final Map n;
  final Map o;
  final com.truecaller.calling.dialer.suggested_contacts.f p;
  final com.truecaller.utils.n q;
  final g r;
  final br s;
  final com.truecaller.data.access.c t;
  final FilterManager u;
  final bu v;
  final bu w;
  final com.truecaller.androidactors.f x;
  final au y;
  final com.truecaller.flashsdk.core.b z;
  
  public af(com.truecaller.calling.dialer.suggested_contacts.f paramf, com.truecaller.calling.e.e parame, com.truecaller.utils.n paramn, al paramal, com.truecaller.i.c paramc, g paramg, br parambr, com.truecaller.data.access.c paramc1, FilterManager paramFilterManager, bu parambu1, bu parambu2, com.truecaller.androidactors.f paramf1, au paramau, com.truecaller.analytics.b paramb, h paramh, com.truecaller.flashsdk.core.b paramb1, com.truecaller.androidactors.f paramf2, c.d.f paramf3, c.d.f paramf4, com.truecaller.ads.provider.e parame1, com.truecaller.search.local.model.c paramc2, ar paramar, a parama, j paramj, com.truecaller.voip.d paramd, com.truecaller.calling.dialer.a.b paramb2, com.truecaller.featuretoggles.e parame2)
  {
    p = paramf;
    R = parame;
    q = paramn;
    S = paramal;
    T = paramc;
    r = paramg;
    s = parambr;
    t = paramc1;
    u = paramFilterManager;
    v = parambu1;
    w = parambu2;
    x = paramf1;
    y = paramau;
    U = paramb;
    localObject1 = paramh;
    V = paramh;
    z = paramb1;
    localObject1 = paramf2;
    localObject2 = paramf3;
    W = paramf2;
    X = paramf3;
    localObject1 = paramf4;
    localObject2 = parame1;
    A = paramf4;
    Y = parame1;
    localObject1 = paramc2;
    localObject2 = paramar;
    Z = paramc2;
    aa = paramar;
    localObject1 = parama;
    localObject2 = paramj;
    ab = parama;
    B = paramj;
    localObject1 = paramd;
    localObject2 = paramb2;
    ac = paramd;
    ad = paramb2;
    localObject1 = parame2;
    ae = parame2;
    localObject1 = new com/truecaller/calling/dialer/af$a;
    ((af.a)localObject1).<init>(this);
    E = ((af.a)localObject1);
    localObject1 = new com/truecaller/calling/dialer/af$b;
    ((af.b)localObject1).<init>(this);
    F = ((af.b)localObject1);
    boolean bool = R.b();
    J = bool;
    localObject1 = new java/util/LinkedHashSet;
    ((LinkedHashSet)localObject1).<init>();
    localObject1 = (Set)localObject1;
    d = ((Set)localObject1);
    localObject1 = (List)y.a;
    e = ((List)localObject1);
    localObject1 = (List)y.a;
    K = ((List)localObject1);
    localObject1 = (List)y.a;
    f = ((List)localObject1);
    localObject2 = new com/truecaller/calling/dialer/bz$a;
    localObject3 = (List)y.a;
    ((bz.a)localObject2).<init>((List)localObject3);
    localObject1 = t.a("", localObject2);
    g = ((c.n)localObject1);
    localObject2 = bs.a(null);
    h = ((bn)localObject2);
    localObject2 = bs.a(h);
    i = ((bn)localObject2);
    localObject2 = bs.a(null);
    j = ((bn)localObject2);
    localObject1 = bs.a(null);
    M = ((bn)localObject1);
    localObject1 = new android/os/CancellationSignal;
    ((CancellationSignal)localObject1).<init>();
    k = ((CancellationSignal)localObject1);
    localObject1 = FilterType.NONE;
    l = ((FilterType)localObject1);
    localObject1 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject1).<init>();
    localObject1 = (Map)localObject1;
    O = ((Map)localObject1);
    localObject1 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject1).<init>();
    localObject1 = (Map)localObject1;
    n = ((Map)localObject1);
    localObject1 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject1).<init>();
    localObject1 = (Map)localObject1;
    o = ((Map)localObject1);
    localObject1 = new com/truecaller/calling/dialer/af$c;
    ((af.c)localObject1).<init>(this);
    localObject1 = (com.truecaller.calling.af)localObject1;
    P = ((com.truecaller.calling.af)localObject1);
    localObject1 = new com/truecaller/calling/dialer/af$j;
    ((af.j)localObject1).<init>(this);
    localObject1 = (com.truecaller.calling.af)localObject1;
    Q = ((com.truecaller.calling.af)localObject1);
  }
  
  private final List N()
  {
    Object localObject1 = (Iterable)f;
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject3 = ((Iterator)localObject1).next();
      Object localObject4 = localObject3;
      localObject4 = (aw)localObject3;
      Iterable localIterable = (Iterable)d;
      localObject4 = c.getId();
      boolean bool2 = c.a.m.a(localIterable, localObject4);
      if (bool2) {
        ((Collection)localObject2).add(localObject3);
      }
    }
    return (List)localObject2;
  }
  
  private final void O()
  {
    kotlinx.coroutines.ag localag = (kotlinx.coroutines.ag)bg.a;
    Object localObject1 = h;
    Object localObject2 = A;
    localObject1 = ((bn)localObject1).plus((c.d.f)localObject2);
    localObject2 = new com/truecaller/calling/dialer/af$d;
    ((af.d)localObject2).<init>(this, null);
    localObject2 = (c.g.a.m)localObject2;
    kotlinx.coroutines.e.b(localag, (c.d.f)localObject1, (c.g.a.m)localObject2, 2);
    ac.d();
  }
  
  private final void P()
  {
    Map localMap = n;
    a(localMap);
  }
  
  private final void Q()
  {
    Object localObject1 = (ae.c)b;
    if (localObject1 != null)
    {
      boolean bool1 = M();
      ((ae.c)localObject1).b(bool1);
    }
    boolean bool2 = M();
    if (bool2)
    {
      localObject1 = (kotlinx.coroutines.ag)bg.a;
      Object localObject2 = h;
      Object localObject3 = X;
      localObject2 = ((bn)localObject2).plus((c.d.f)localObject3);
      localObject3 = new com/truecaller/calling/dialer/af$i;
      ((af.i)localObject3).<init>(this, null);
      localObject3 = (c.g.a.m)localObject3;
      int i1 = 2;
      kotlinx.coroutines.e.b((kotlinx.coroutines.ag)localObject1, (c.d.f)localObject2, (c.g.a.m)localObject3, i1);
    }
  }
  
  private final void R()
  {
    List localList1 = K;
    Object localObject = ad.a();
    K = ((List)localObject);
    localObject = K;
    boolean bool = k.a(localList1, localObject) ^ true;
    if (bool)
    {
      localObject = (ae.c)b;
      if (localObject != null)
      {
        List localList2 = K;
        ((ae.c)localObject).a(localList1, localList2);
        return;
      }
    }
  }
  
  private final void S()
  {
    d.clear();
    Object localObject1 = d;
    Object localObject2 = (Iterable)f;
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject3 = (Collection)localObject3;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      Long localLong = nextc.getId();
      if (localLong != null) {
        ((Collection)localObject3).add(localLong);
      }
    }
    localObject3 = (Collection)localObject3;
    ((Set)localObject1).addAll((Collection)localObject3);
    localObject1 = (ae.c)b;
    if (localObject1 != null) {
      ((ae.c)localObject1).b();
    }
    localObject1 = (ae.b)c;
    if (localObject1 != null)
    {
      ((ae.b)localObject1).d();
      return;
    }
  }
  
  private final void T()
  {
    FilterType localFilterType = FilterType.NONE;
    a(this, localFilterType, false, 2);
  }
  
  private final boolean U()
  {
    String str = T.a("callLogTapBehavior");
    return k.a("call", str);
  }
  
  private final void V()
  {
    Object localObject = V;
    boolean bool = ((h)localObject).e();
    if (bool)
    {
      localObject = V;
      bool = ((h)localObject).k();
      if (bool)
      {
        bool = true;
        break label41;
      }
    }
    bool = false;
    localObject = null;
    label41:
    ae.c localc = (ae.c)b;
    if (localc != null) {
      localc.g(bool);
    }
    if (bool)
    {
      localObject = (ae.c)b;
      if (localObject != null)
      {
        int i1 = aa.d();
        ((ae.c)localObject).d(i1);
        return;
      }
    }
  }
  
  private static aq a(FilterType paramFilterType)
  {
    int[] arrayOfInt = ag.c;
    int i1 = paramFilterType.ordinal();
    i1 = arrayOfInt[i1];
    arrayOfInt = null;
    int i2 = 2131886503;
    switch (i1)
    {
    default: 
      paramFilterType = new c/l;
      paramFilterType.<init>();
      throw paramFilterType;
    case 6: 
      paramFilterType = new com/truecaller/calling/dialer/aq;
      paramFilterType.<init>(2131886499, i2, false);
      return paramFilterType;
    case 5: 
      paramFilterType = new com/truecaller/calling/dialer/aq;
      paramFilterType.<init>(2131886495, i2, false);
      return paramFilterType;
    case 4: 
      paramFilterType = new com/truecaller/calling/dialer/aq;
      paramFilterType.<init>(2131886501, i2, false);
      return paramFilterType;
    case 3: 
      paramFilterType = new com/truecaller/calling/dialer/aq;
      paramFilterType.<init>(2131886502, i2, false);
      return paramFilterType;
    case 2: 
      paramFilterType = new com/truecaller/calling/dialer/aq;
      paramFilterType.<init>(2131886500, i2, false);
      return paramFilterType;
    }
    paramFilterType = new com/truecaller/calling/dialer/aq;
    paramFilterType.<init>(2131886498, 2131886496, true);
    return paramFilterType;
  }
  
  private final void a(FilterType paramFilterType, boolean paramBoolean)
  {
    int i1;
    if (paramBoolean) {
      i1 = 20;
    } else {
      i1 = 1000;
    }
    Object localObject1 = M;
    Object localObject2 = new java/util/concurrent/CancellationException;
    ((CancellationException)localObject2).<init>("New refresh requested");
    localObject2 = (Throwable)localObject2;
    ((bn)localObject1).c((Throwable)localObject2);
    localObject1 = (kotlinx.coroutines.ag)bg.a;
    localObject2 = h;
    Object localObject3 = X;
    c.d.f localf = ((bn)localObject2).plus((c.d.f)localObject3);
    Object localObject4 = new com/truecaller/calling/dialer/af$h;
    localObject2 = localObject4;
    localObject3 = this;
    ((af.h)localObject4).<init>(this, paramFilterType, i1, paramBoolean, null);
    localObject4 = (c.g.a.m)localObject4;
    paramFilterType = kotlinx.coroutines.e.b((kotlinx.coroutines.ag)localObject1, localf, (c.g.a.m)localObject4, 2);
    M = paramFilterType;
  }
  
  private final void a(String paramString1, String paramString2, String paramString3)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("ViewAction");
    paramString1 = locala.a("Context", paramString3).a("Action", paramString1).a("SubAction", paramString2);
    paramString2 = U;
    paramString1 = paramString1.a();
    k.a(paramString1, "eventBuilder.build()");
    paramString2.b(paramString1);
  }
  
  private final void a(Map paramMap)
  {
    Iterator localIterator = ((Iterable)paramMap.values()).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      com.truecaller.flashsdk.db.l locall = (com.truecaller.flashsdk.db.l)localIterator.next();
      com.truecaller.flashsdk.core.b localb = z;
      localb.a(locall);
    }
    paramMap.clear();
  }
  
  private final void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    com.truecaller.ads.provider.e locale = Y;
    boolean bool;
    if ((!paramBoolean1) && (!paramBoolean2)) {
      bool = false;
    } else {
      bool = true;
    }
    locale.a(bool);
    C = paramBoolean1;
    D = paramBoolean2;
  }
  
  private final void b(Set paramSet)
  {
    paramSet = ((Iterable)paramSet).iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (String)paramSet.next();
      Object localObject2 = v.a((String)localObject1);
      if (localObject2 != null)
      {
        ae.c localc = (ae.c)b;
        if (localc != null) {
          localc.a((Set)localObject2);
        }
      }
      localObject2 = w;
      localObject1 = ((bu)localObject2).a((String)localObject1);
      if (localObject1 != null)
      {
        localObject2 = (ae.c)b;
        if (localObject2 != null) {
          ((ae.c)localObject2).b((Set)localObject1);
        }
      }
    }
  }
  
  private final void d(boolean paramBoolean)
  {
    m = paramBoolean;
    ae.c localc1 = (ae.c)b;
    if (localc1 != null)
    {
      aq localaq = J();
      localc1.a(localaq);
    }
    int i1;
    if (paramBoolean)
    {
      I();
      T();
      localc2 = (ae.c)b;
      if (localc2 != null)
      {
        boolean bool1 = true;
        localc2.d(bool1);
      }
      localc2 = (ae.c)b;
      if (localc2 != null)
      {
        i1 = 2131886505;
        localc2.c(i1);
      }
    }
    else
    {
      P();
      localc2 = (ae.c)b;
      if (localc2 != null)
      {
        i1 = 0;
        localc1 = null;
        localc2.d(false);
      }
    }
    ae.c localc2 = (ae.c)b;
    if (localc2 != null)
    {
      boolean bool2 = G();
      localc2.f(bool2);
    }
    localc2 = (ae.c)b;
    if (localc2 != null)
    {
      localc2.m();
      return;
    }
  }
  
  public final void A()
  {
    Object localObject = l;
    FilterType localFilterType = FilterType.NONE;
    if (localObject == localFilterType)
    {
      localObject = (ae.b)c;
      if (localObject != null) {
        ((ae.b)localObject).h();
      }
      return;
    }
    T();
  }
  
  public final void B()
  {
    aa.a();
    Object localObject1 = aa.b();
    if (localObject1 != null)
    {
      Object localObject2 = localObject1;
      localObject2 = (CharSequence)localObject1;
      int i1 = ((CharSequence)localObject2).length();
      if (i1 == 0)
      {
        i1 = 1;
      }
      else
      {
        i1 = 0;
        localObject2 = null;
      }
      if (i1 != 0)
      {
        i2 = 0;
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        localObject2 = (ae.c)b;
        if (localObject2 != null) {
          ((ae.c)localObject2).c((String)localObject1);
        }
      }
    }
    localObject1 = aa;
    int i2 = ((ar)localObject1).c();
    switch (i2)
    {
    default: 
      localObject1 = "alwaysAsk";
      break;
    case 1: 
      localObject1 = "sim2";
      break;
    case 0: 
      localObject1 = "sim1";
    }
    a("dualSim", (String)localObject1, "callLog");
    V();
  }
  
  public final void C()
  {
    z.i();
  }
  
  public final void D()
  {
    ad.b();
    R();
  }
  
  public final Map E()
  {
    return n;
  }
  
  public final com.truecaller.calling.af F()
  {
    return Q;
  }
  
  final boolean G()
  {
    boolean bool = m;
    if (!bool)
    {
      bool = N;
      if (!bool)
      {
        FilterType localFilterType1 = l;
        FilterType localFilterType2 = FilterType.NONE;
        if (localFilterType1 == localFilterType2) {
          return false;
        }
      }
    }
    return true;
  }
  
  public final void H()
  {
    ae.c localc = (ae.c)b;
    if (localc != null)
    {
      localc.o();
      return;
    }
  }
  
  final void I()
  {
    Map localMap = O;
    a(localMap);
  }
  
  final aq J()
  {
    List localList = f;
    boolean bool = localList.isEmpty();
    if (bool)
    {
      bool = m;
      if (!bool) {
        return a(l);
      }
    }
    return null;
  }
  
  final x K()
  {
    ae.c localc = (ae.c)b;
    if (localc != null)
    {
      Set localSet = Y.b();
      localc.c(localSet);
      return x.a;
    }
    return null;
  }
  
  final boolean L()
  {
    com.truecaller.i.c localc = T;
    String str = "merge_by";
    int i1 = 3;
    int i2 = localc.a(str, i1);
    return i2 == i1;
  }
  
  final boolean M()
  {
    return T.a("showFrequentlyCalledContacts", true);
  }
  
  public final c.n a(ca paramca, c.l.g paramg)
  {
    k.b(paramca, "thisRef");
    k.b(paramg, "property");
    return g;
  }
  
  public final List a(ck.b paramb, c.l.g paramg)
  {
    k.b(paramb, "thisRef");
    k.b(paramg, "property");
    return e;
  }
  
  public final List a(cn.b paramb, c.l.g paramg)
  {
    k.b(paramb, "thisRef");
    k.b(paramg, "property");
    return K;
  }
  
  public final List a(i.b paramb, c.l.g paramg)
  {
    k.b(paramb, "thisRef");
    k.b(paramg, "property");
    return f;
  }
  
  public final void a(int paramInt)
  {
    int i1 = 1;
    if (paramInt == i1)
    {
      N = i1;
      ae.c localc = (ae.c)b;
      if (localc != null)
      {
        localc.m();
        return;
      }
    }
  }
  
  public final void a(long paramLong)
  {
    long l1 = 3;
    boolean bool = paramLong < l1;
    if (!bool)
    {
      localObject1 = FilterType.INCOMING;
      localObject2 = "incoming";
      localObject1 = t.a(localObject1, localObject2);
    }
    else
    {
      l1 = 4;
      bool = paramLong < l1;
      if (!bool)
      {
        localObject1 = FilterType.OUTGOING;
        localObject2 = "outgoing";
        localObject1 = t.a(localObject1, localObject2);
      }
      else
      {
        l1 = 5;
        bool = paramLong < l1;
        if (!bool)
        {
          localObject1 = FilterType.MISSED;
          localObject2 = "missed";
          localObject1 = t.a(localObject1, localObject2);
        }
        else
        {
          l1 = 6;
          bool = paramLong < l1;
          if (!bool)
          {
            localObject1 = FilterType.BLOCKED;
            localObject2 = "blocked";
            localObject1 = t.a(localObject1, localObject2);
          }
          else
          {
            l1 = 11;
            bool = paramLong < l1;
            if (bool) {
              return;
            }
            localObject1 = FilterType.FLASH;
            localObject2 = "flash";
            localObject1 = t.a(localObject1, localObject2);
          }
        }
      }
    }
    Object localObject2 = (FilterType)a;
    Object localObject1 = (String)b;
    a(this, (FilterType)localObject2, false, 2);
    a("menu", (String)localObject1, "callLog");
    return;
  }
  
  public final void a(View paramView)
  {
    k.b(paramView, "anchorView");
    ae.c localc = (ae.c)b;
    if (localc != null)
    {
      localc.a(paramView);
      return;
    }
  }
  
  public final void a(View paramView, com.truecaller.calling.dialer.suggested_contacts.e parame, String paramString)
  {
    k.b(paramView, "anchorView");
    k.b(parame, "suggestedContact");
    k.b(paramString, "displayName");
    ae.c localc = (ae.c)b;
    if (localc != null)
    {
      localc.a(paramView, parame, paramString);
      return;
    }
  }
  
  public final void a(com.truecaller.calling.dialer.suggested_contacts.e parame)
  {
    k.b(parame, "suggestedContact");
    p.a(parame);
    Q();
    ae.c localc = (ae.c)b;
    if (localc != null)
    {
      localc.a(parame);
      return;
    }
  }
  
  public final void a(v paramv)
  {
    k.b(paramv, "observer");
    L = paramv;
  }
  
  public final void a(Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(paramContact, "contact");
    k.b(paramSourceType, "sourceType");
    Object localObject1 = c;
    Object localObject2 = localObject1;
    localObject2 = (ae.b)localObject1;
    if (localObject2 != null)
    {
      ((ae.b)localObject2).a(paramContact, paramSourceType, paramBoolean1, paramBoolean2, paramBoolean3);
      return;
    }
  }
  
  public final void a(Contact paramContact, String paramString)
  {
    k.b(paramContact, "contact");
    k.b(paramString, "analyticsContext");
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.a(paramContact, paramString);
      return;
    }
  }
  
  public final void a(Contact paramContact, String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "fallbackNumber");
    k.b(paramString2, "callType");
    k.b(paramString3, "analyticsContext");
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.a(paramContact, paramString1, paramString2, paramString3);
      return;
    }
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    Object localObject = "callLogItem";
    k.b(paramHistoryEvent, (String)localObject);
    paramHistoryEvent = paramHistoryEvent.getId();
    if (paramHistoryEvent == null) {
      return;
    }
    localObject = "callLogItem.id ?: return";
    k.a(paramHistoryEvent, (String)localObject);
    long l1 = paramHistoryEvent.longValue();
    paramHistoryEvent = d;
    Long localLong = Long.valueOf(l1);
    boolean bool1 = paramHistoryEvent.remove(localLong);
    if (!bool1)
    {
      localObject = Long.valueOf(l1);
      paramHistoryEvent.add(localObject);
    }
    boolean bool2 = paramHistoryEvent.isEmpty();
    if (bool2)
    {
      paramHistoryEvent = (ae.b)c;
      if (paramHistoryEvent != null) {
        paramHistoryEvent.c();
      }
    }
    paramHistoryEvent = (ae.c)b;
    if (paramHistoryEvent != null) {
      paramHistoryEvent.b();
    }
    paramHistoryEvent = (ae.b)c;
    if (paramHistoryEvent != null)
    {
      paramHistoryEvent.d();
      return;
    }
  }
  
  public final void a(HistoryEvent paramHistoryEvent, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramHistoryEvent, "historyEvent");
    k.b(paramSourceType, "sourceType");
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.a(paramHistoryEvent, paramSourceType, paramBoolean1, paramBoolean2);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "promotionType");
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("DIALER_PROMO_clicked");
    paramString = ((e.a)localObject).a("DIALER_PROMO_name", paramString);
    localObject = U;
    paramString = paramString.a();
    k.a(paramString, "eventBuilder.build()");
    ((com.truecaller.analytics.b)localObject).a(paramString);
  }
  
  final void a(String paramString, bz parambz)
  {
    bn localbn = i;
    Object localObject = new java/util/concurrent/CancellationException;
    String str = "New searchState appeared";
    ((CancellationException)localObject).<init>(str);
    localObject = (Throwable)localObject;
    kotlinx.coroutines.br.a(localbn, (Throwable)localObject);
    paramString = t.a(paramString, parambz);
    g = paramString;
    P();
    paramString = (ae.c)b;
    if (paramString != null)
    {
      boolean bool = parambz instanceof bz.a;
      paramString.e(bool);
      return;
    }
  }
  
  public final void a(String paramString, TruecallerContract.Filters.EntityType paramEntityType)
  {
    k.b(paramEntityType, "entityType");
    Object localObject1 = (Iterable)N();
    Object localObject2 = new java/util/ArrayList;
    int i1 = 10;
    int i2 = c.a.m.a((Iterable)localObject1, i1);
    ((ArrayList)localObject2).<init>(i2);
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool2;
    Object localObject3;
    for (;;)
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = nextc;
      ((Collection)localObject2).add(localObject3);
    }
    localObject2 = (Iterable)localObject2;
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = ((Iterator)localObject2).next();
      localObject4 = localObject3;
      localObject4 = ((HistoryEvent)localObject3).a();
      bool3 = ab.a((String)localObject4);
      if (!bool3) {
        ((Collection)localObject1).add(localObject3);
      }
    }
    localObject1 = (List)localObject1;
    boolean bool4 = ((List)localObject1).isEmpty();
    int i3 = 2;
    boolean bool3 = false;
    Object localObject4 = null;
    int i5 = 1;
    label259:
    int i7;
    Object localObject6;
    if (!bool4)
    {
      localObject2 = paramString;
      localObject2 = (CharSequence)paramString;
      if (localObject2 != null)
      {
        i4 = ((CharSequence)localObject2).length();
        if (i4 != 0)
        {
          i4 = 0;
          localObject2 = null;
          break label259;
        }
      }
      i4 = 1;
      if (i4 == 0)
      {
        localObject2 = ((HistoryEvent)((List)localObject1).get(0)).s();
        if (localObject2 != null)
        {
          localObject5 = ag.a;
          int i6 = paramEntityType.ordinal();
          i7 = localObject5[i6];
          if (i7 != i5) {
            i7 = 1;
          } else {
            i7 = 2;
          }
          localObject6 = (com.truecaller.tag.c)W.a();
          localObject2 = ((com.truecaller.tag.c)localObject6).a((Contact)localObject2, paramString, i7);
          ((w)localObject2).c();
        }
      }
    }
    localObject1 = (Iterable)localObject1;
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>();
    Object localObject5 = new java/util/ArrayList;
    ((ArrayList)localObject5).<init>();
    localObject1 = ((Iterable)localObject1).iterator();
    Object localObject7;
    for (;;)
    {
      boolean bool5 = ((Iterator)localObject1).hasNext();
      if (!bool5) {
        break;
      }
      localObject6 = ((Iterator)localObject1).next();
      localObject7 = localObject6;
      localObject7 = (HistoryEvent)localObject6;
      String str = ((HistoryEvent)localObject7).a();
      if (str == null) {
        str = ((HistoryEvent)localObject7).b();
      }
      boolean bool6 = ((HashSet)localObject2).add(str);
      if (bool6) {
        ((ArrayList)localObject5).add(localObject6);
      }
    }
    localObject5 = (Iterable)localObject5;
    localObject1 = new java/util/ArrayList;
    int i4 = c.a.m.a((Iterable)localObject5, i1);
    ((ArrayList)localObject1).<init>(i4);
    localObject1 = (Collection)localObject1;
    localObject2 = ((Iterable)localObject5).iterator();
    Object localObject8;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      i7 = 0;
      localObject5 = null;
      if (!bool1) {
        break;
      }
      localObject8 = (HistoryEvent)((Iterator)localObject2).next();
      localObject6 = ((HistoryEvent)localObject8).a();
      if (localObject6 == null) {
        localObject6 = ((HistoryEvent)localObject8).b();
      }
      if (paramString != null)
      {
        localObject7 = paramString;
        localObject7 = (CharSequence)paramString;
        int i8 = ((CharSequence)localObject7).length();
        if (i8 == 0)
        {
          i8 = 1;
        }
        else
        {
          i8 = 0;
          localObject7 = null;
        }
        if (i8 == 0)
        {
          localObject7 = paramString;
        }
        else
        {
          i8 = 0;
          localObject7 = null;
        }
        if (localObject7 != null)
        {
          localObject5 = localObject7;
          break label646;
        }
      }
      localObject7 = ((HistoryEvent)localObject8).s();
      if (localObject7 != null) {
        localObject5 = ((Contact)localObject7).s();
      }
      label646:
      if (localObject5 == null) {
        localObject5 = ((HistoryEvent)localObject8).b();
      }
      localObject8 = t.a(localObject6, localObject5);
      ((Collection)localObject1).add(localObject8);
    }
    localObject1 = (List)localObject1;
    paramString = (String)localObject1;
    paramString = (Collection)localObject1;
    boolean bool7 = paramString.isEmpty() ^ i5;
    if (bool7)
    {
      paramString = (kotlinx.coroutines.ag)bg.a;
      localObject2 = h;
      localObject8 = X;
      localObject2 = ((bn)localObject2).plus((c.d.f)localObject8);
      localObject8 = new com/truecaller/calling/dialer/af$f;
      ((af.f)localObject8).<init>(this, (List)localObject1, paramEntityType, null);
      localObject8 = (c.g.a.m)localObject8;
      kotlinx.coroutines.e.b(paramString, (c.d.f)localObject2, (c.g.a.m)localObject8, i3);
    }
    paramString = (ae.b)c;
    if (paramString != null)
    {
      paramString.c();
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "number");
    k.b(paramString2, "analyticsContext");
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.a(paramString1, paramString2);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, int paramInt)
  {
    k.b(paramString1, "normalizedNumber");
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      Object localObject1 = z.a;
      localObject1 = Locale.ENGLISH;
      k.a(localObject1, "Locale.ENGLISH");
      String str = "Suggested%d_Frequency";
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      Object localObject2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject2;
      localObject2 = Arrays.copyOf(arrayOfObject, i1);
      localObject2 = String.format((Locale)localObject1, str, (Object[])localObject2);
      localObject1 = "java.lang.String.format(locale, format, *args)";
      k.a(localObject2, (String)localObject1);
      localb.a(paramString1, paramString2, false, (String)localObject2);
    }
    a("call", "item", "Suggested%d_Frequency");
  }
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean, CompositeAdapterDelegate.SearchResultOrder paramSearchResultOrder)
  {
    k.b(paramSearchResultOrder, "searchOrder");
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.a(paramString1, paramString2, paramBoolean, paramSearchResultOrder);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean, String paramString3)
  {
    k.b(paramString1, "number");
    k.b(paramString3, "analyticsContext");
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.a(paramString1, paramString2, paramBoolean, paramString3);
      return;
    }
  }
  
  public final void a(Collection paramCollection)
  {
    k.b(paramCollection, "normalizedNumbers");
    paramCollection = c.a.m.i((Iterable)paramCollection);
    b(paramCollection);
  }
  
  public final void a(Set paramSet)
  {
    k.b(paramSet, "normalizedNumbers");
    b(paramSet);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i1 = 3;
    String str;
    if (paramBoolean)
    {
      localObject = T;
      str = "merge_by";
      ((com.truecaller.i.c)localObject).b(str, i1);
      localObject = (ae.c)b;
      if (localObject != null) {
        ((ae.c)localObject).l();
      }
    }
    else
    {
      localObject = T;
      str = "merge_by";
      int i2 = 1;
      ((com.truecaller.i.c)localObject).b(str, i2);
    }
    Object localObject = (ae.c)b;
    if (localObject != null) {
      ((ae.c)localObject).k();
    }
    a(this, null, false, i1);
    a("menu", "groupByNumber", "callLog");
  }
  
  public final boolean a(int paramInt1, int paramInt2)
  {
    String str = null;
    int i1 = 1;
    if (paramInt1 != i1) {
      return false;
    }
    paramInt1 = 2131361835;
    Object localObject1;
    if (paramInt2 != paramInt1)
    {
      paramInt1 = 2131361848;
      if (paramInt2 != paramInt1)
      {
        paramInt1 = 2131361934;
        if (paramInt2 == paramInt1) {
          S();
        }
      }
      else
      {
        localObject1 = (ae.c)b;
        if (localObject1 != null)
        {
          paramInt2 = L();
          if (paramInt2 != 0) {
            paramInt2 = 2131886291;
          } else {
            paramInt2 = 2131886290;
          }
          ((ae.c)localObject1).a(paramInt2);
        }
      }
    }
    else
    {
      localObject1 = (Iterable)N();
      Object localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      localObject2 = (Collection)localObject2;
      localObject1 = ((Iterable)localObject1).iterator();
      Object localObject3;
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject3 = ((Iterator)localObject1).next();
        Object localObject4 = localObject3;
        localObject4 = c.a();
        boolean bool2 = ab.a((String)localObject4);
        if (!bool2) {
          ((Collection)localObject2).add(localObject3);
        }
      }
      localObject2 = (List)localObject2;
      localObject1 = localObject2;
      localObject1 = (Collection)localObject2;
      paramInt1 = ((Collection)localObject1).isEmpty() ^ i1;
      if (paramInt1 != 0)
      {
        localObject1 = S;
        paramInt1 = ((al)localObject1).a();
        if (paramInt1 != 0)
        {
          localObject1 = get0c.s();
          if (localObject1 != null)
          {
            paramInt1 = ((Contact)localObject1).ac();
          }
          else
          {
            paramInt1 = 0;
            localObject1 = null;
          }
          if (paramInt1 != 0)
          {
            paramInt1 = 1;
            break label298;
          }
        }
        paramInt1 = 0;
        localObject1 = null;
        label298:
        int i2 = ((List)localObject2).size();
        if (i2 != i1)
        {
          localObject1 = (ae.c)b;
          if (localObject1 != null)
          {
            localObject3 = q;
            int i3 = 2131755008;
            int i4 = ((List)localObject2).size();
            Object[] arrayOfObject = new Object[i1];
            paramInt2 = ((List)localObject2).size();
            localObject2 = Integer.valueOf(paramInt2);
            arrayOfObject[0] = localObject2;
            localObject2 = ((com.truecaller.utils.n)localObject3).a(i3, i4, arrayOfObject);
            str = "resourceProvider.getQuan…ize\n                    )";
            k.a(localObject2, str);
            ((ae.c)localObject1).a((String)localObject2);
          }
        }
        else
        {
          localObject3 = (ae.c)b;
          if (localObject3 != null)
          {
            localObject2 = get0c.s();
            if (localObject2 != null)
            {
              localObject2 = ((Contact)localObject2).t();
            }
            else
            {
              paramInt2 = 0;
              localObject2 = null;
            }
            ((ae.c)localObject3).a((String)localObject2, paramInt1);
          }
        }
      }
    }
    return i1;
  }
  
  public final void b(int paramInt)
  {
    int i1 = 1;
    if (paramInt == i1)
    {
      paramInt = 0;
      ae.c localc1 = null;
      N = false;
      d.clear();
      ae.c localc2 = (ae.c)b;
      if (localc2 != null) {
        localc2.c(false);
      }
      localc1 = (ae.c)b;
      if (localc1 != null)
      {
        localc1.m();
        return;
      }
    }
  }
  
  public final void b(com.truecaller.calling.dialer.suggested_contacts.e parame)
  {
    k.b(parame, "suggestedContact");
    com.truecaller.calling.dialer.suggested_contacts.f localf = p;
    boolean bool = c;
    if (bool) {
      localf.d(parame);
    } else {
      localf.c(parame);
    }
    Q();
  }
  
  public final void b(Contact paramContact, String paramString)
  {
    k.b(paramContact, "contact");
    k.b(paramString, "analyticsContext");
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.b(paramContact, paramString);
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "page");
    ae.c localc = (ae.c)b;
    if (localc != null)
    {
      PremiumPresenterView.LaunchContext localLaunchContext = PremiumPresenterView.LaunchContext.SUGGESTED_CONTACTS;
      localc.a(paramString, localLaunchContext);
      return;
    }
  }
  
  final void b(boolean paramBoolean)
  {
    Object localObject = a;
    if (localObject == null) {
      return;
    }
    ae.c localc;
    String str1;
    if (paramBoolean)
    {
      localc = (ae.c)b;
      if (localc != null)
      {
        str1 = a;
        String str2 = b;
        localObject = c;
        localc.a(str1, str2, (String)localObject);
      }
    }
    else
    {
      localc = (ae.c)b;
      if (localc != null)
      {
        str1 = a;
        localObject = b;
        ae.c.a.a(localc, str1, (String)localObject);
      }
    }
    a = null;
  }
  
  public final boolean b()
  {
    Object localObject = (ae.b)c;
    if (localObject != null) {
      ((ae.b)localObject).b();
    }
    localObject = (ae.c)b;
    boolean bool = true;
    if (localObject != null) {
      ((ae.c)localObject).c(bool);
    }
    return bool;
  }
  
  public final boolean b(int paramInt1, int paramInt2)
  {
    int i1 = 1;
    if (paramInt1 == i1)
    {
      paramInt1 = 2131361835;
      Object localObject2;
      if (paramInt2 != paramInt1)
      {
        paramInt1 = 2131361848;
        if (paramInt2 != paramInt1)
        {
          paramInt1 = 2131361934;
          if (paramInt2 != paramInt1) {
            return false;
          }
          localObject1 = d;
          paramInt1 = ((Set)localObject1).size();
          localObject2 = f;
          paramInt2 = ((List)localObject2).size();
          if (paramInt1 != paramInt2) {
            return i1;
          }
          return false;
        }
        return i1;
      }
      Object localObject1 = S;
      paramInt1 = ((al)localObject1).a();
      if (paramInt1 != 0)
      {
        localObject1 = (Iterable)N();
        localObject2 = new java/util/ArrayList;
        int i2 = c.a.m.a((Iterable)localObject1, 10);
        ((ArrayList)localObject2).<init>(i2);
        localObject2 = (Collection)localObject2;
        localObject1 = ((Iterable)localObject1).iterator();
        for (;;)
        {
          boolean bool = ((Iterator)localObject1).hasNext();
          if (!bool) {
            break;
          }
          String str = nextc.a();
          ((Collection)localObject2).add(str);
        }
        localObject2 = (Iterable)localObject2;
        localObject1 = (Iterable)c.a.m.i((Iterable)localObject2);
        paramInt2 = localObject1 instanceof Collection;
        if (paramInt2 != 0)
        {
          localObject2 = localObject1;
          localObject2 = (Collection)localObject1;
          paramInt2 = ((Collection)localObject2).isEmpty();
          if (paramInt2 != 0) {}
        }
        else
        {
          localObject1 = ((Iterable)localObject1).iterator();
          do
          {
            paramInt2 = ((Iterator)localObject1).hasNext();
            if (paramInt2 == 0) {
              break;
            }
            localObject2 = (String)((Iterator)localObject1).next();
            paramInt2 = ab.a((String)localObject2);
          } while (paramInt2 == 0);
          paramInt1 = 0;
          localObject1 = null;
          break label293;
        }
        paramInt1 = 1;
        label293:
        if (paramInt1 != 0) {
          return i1;
        }
      }
      return false;
    }
    return false;
  }
  
  public final boolean b(HistoryEvent paramHistoryEvent)
  {
    k.b(paramHistoryEvent, "callLogItem");
    Iterable localIterable = (Iterable)d;
    paramHistoryEvent = paramHistoryEvent.getId();
    return c.a.m.a(localIterable, paramHistoryEvent);
  }
  
  public final void b_(int paramInt)
  {
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.b_(paramInt);
      return;
    }
  }
  
  public final int c(int paramInt)
  {
    int i1 = 1;
    if (paramInt == i1) {
      return 2131623936;
    }
    return -1;
  }
  
  public final String c()
  {
    Object localObject = q;
    Object[] arrayOfObject = new Object[2];
    Integer localInteger = Integer.valueOf(d.size());
    arrayOfObject[0] = localInteger;
    localInteger = Integer.valueOf(f.size());
    arrayOfObject[1] = localInteger;
    localObject = ((com.truecaller.utils.n)localObject).a(2131886292, arrayOfObject);
    k.a(localObject, "resourceProvider.getStri…s.size, callHistory.size)");
    return (String)localObject;
  }
  
  public final void c(com.truecaller.calling.dialer.suggested_contacts.e parame)
  {
    k.b(parame, "suggestedContact");
    p.b(parame);
    Q();
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "input");
    paramString = (CharSequence)c.n.m.b((CharSequence)paramString).toString();
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject1 = (Appendable)localObject1;
    int i1 = paramString.length();
    int i2 = 0;
    Object localObject2 = null;
    int i3 = 0;
    Object localObject3 = null;
    boolean bool3;
    for (;;)
    {
      bool3 = true;
      if (i3 >= i1) {
        break;
      }
      char c1 = paramString.charAt(i3);
      char c2 = '9';
      char c3 = '0';
      if ((c3 > c1) || (c2 < c1))
      {
        c2 = '+';
        if (c1 != c2) {
          bool3 = false;
        }
      }
      if (bool3) {
        ((Appendable)localObject1).append(c1);
      }
      i3 += 1;
    }
    paramString = ((StringBuilder)localObject1).toString();
    k.a(paramString, "filterTo(StringBuilder(), predicate).toString()");
    localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    i1 = ((CharSequence)localObject1).length();
    if (i1 > 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject4 = null;
    }
    boolean bool2;
    if (i1 != 0)
    {
      bool2 = m;
      if (!bool2)
      {
        d(bool3);
        break label219;
      }
    }
    if (i1 == 0)
    {
      bool1 = m;
      if (bool1) {
        d(false);
      }
    }
    label219:
    Object localObject4 = (String)g.a;
    boolean bool1 = k.a(paramString, localObject4) ^ bool3;
    if (bool1)
    {
      k.cancel();
      localObject4 = new android/os/CancellationSignal;
      ((CancellationSignal)localObject4).<init>();
      k = ((CancellationSignal)localObject4);
      localObject4 = j;
      localObject3 = new java/util/concurrent/CancellationException;
      String str = "User changed search token";
      ((CancellationException)localObject3).<init>(str);
      localObject3 = (Throwable)localObject3;
      ((bn)localObject4).c((Throwable)localObject3);
      int i4 = ((CharSequence)localObject1).length();
      if (i4 == 0) {
        i2 = 1;
      }
      if (i2 != 0)
      {
        localObject1 = new com/truecaller/calling/dialer/bz$a;
        localObject4 = (List)y.a;
        ((bz.a)localObject1).<init>((List)localObject4);
        localObject1 = (bz)localObject1;
        a(paramString, (bz)localObject1);
        return;
      }
      localObject1 = (kotlinx.coroutines.ag)bg.a;
      localObject4 = h;
      localObject2 = X;
      localObject4 = ((bn)localObject4).plus((c.d.f)localObject2);
      localObject2 = new com/truecaller/calling/dialer/af$k;
      bool2 = false;
      localObject3 = null;
      ((af.k)localObject2).<init>(this, paramString, null);
      localObject2 = (c.g.a.m)localObject2;
      int i5 = 2;
      paramString = kotlinx.coroutines.e.b((kotlinx.coroutines.ag)localObject1, (c.d.f)localObject4, (c.g.a.m)localObject2, i5);
      j = paramString;
    }
  }
  
  final void c(boolean paramBoolean)
  {
    Q();
    a(this, null, paramBoolean, 1);
  }
  
  public final void d(int paramInt)
  {
    boolean bool = true;
    int i1 = 2;
    switch (paramInt)
    {
    default: 
      break;
    case 2: 
      a(this, bool, false, i1);
      return;
    case 1: 
      ae.c localc = (ae.c)b;
      if (localc != null) {
        localc.i();
      }
      a(this, bool, false, i1);
      localc = (ae.c)b;
      if (localc != null) {
        localc.h();
      }
      return;
    case 0: 
      a(this, false, false, i1);
    }
  }
  
  public final Map e()
  {
    return O;
  }
  
  public final void e(int paramInt)
  {
    Object localObject1 = g.b;
    boolean bool = localObject1 instanceof bz.a;
    if (!bool) {
      localObject1 = null;
    }
    localObject1 = (bz.a)localObject1;
    if (localObject1 != null)
    {
      Object localObject2 = (av)a.get(paramInt);
      int i1 = 1;
      Object localObject3 = null;
      Object localObject4;
      if (localObject2 != null)
      {
        localObject4 = c;
        int i2;
        if (localObject4 == null)
        {
          i2 = 1;
        }
        else
        {
          i2 = 0;
          localObject4 = null;
        }
        if (i2 == 0)
        {
          bool = false;
          localObject2 = null;
        }
        if (localObject2 != null)
        {
          localObject4 = a;
          localObject1 = (av)a.get(paramInt);
          long l1 = System.currentTimeMillis();
          Long localLong = Long.valueOf(l1);
          int i3 = 3;
          localObject1 = av.a((av)localObject1, null, null, localLong, i3);
          ((List)localObject4).set(paramInt, localObject1);
          if (localObject2 != null)
          {
            localObject1 = a;
            break label168;
          }
        }
      }
      localObject1 = null;
      label168:
      if (localObject1 != null)
      {
        localObject2 = new String[i1];
        Object localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>("T9 refetchContactIfNeeded started for id: ");
        localObject4 = ((Contact)localObject1).getId();
        ((StringBuilder)localObject5).append(localObject4);
        ((StringBuilder)localObject5).append(" for pos: ");
        ((StringBuilder)localObject5).append(paramInt);
        localObject5 = ((StringBuilder)localObject5).toString();
        localObject2[0] = localObject5;
        localObject2 = (kotlinx.coroutines.ag)bg.a;
        localObject5 = i;
        localObject3 = X;
        localObject5 = ((bn)localObject5).plus((c.d.f)localObject3);
        localObject3 = new com/truecaller/calling/dialer/af$g;
        ((af.g)localObject3).<init>(this, (Contact)localObject1, paramInt, null);
        localObject3 = (c.g.a.m)localObject3;
        kotlinx.coroutines.e.b((kotlinx.coroutines.ag)localObject2, (c.d.f)localObject5, (c.g.a.m)localObject3, 2);
        return;
      }
    }
  }
  
  public final com.truecaller.calling.af f()
  {
    return P;
  }
  
  public final Map g()
  {
    return o;
  }
  
  public final boolean h()
  {
    FilterType localFilterType1 = l;
    FilterType localFilterType2 = FilterType.NONE;
    if (localFilterType1 != localFilterType2)
    {
      T();
      return true;
    }
    return false;
  }
  
  public final void i()
  {
    V();
    boolean bool1 = M();
    boolean bool2 = H;
    if (bool1 != bool2)
    {
      bool1 = M();
      H = bool1;
      Q();
    }
    bool1 = L();
    bool2 = G;
    if (bool1 == bool2)
    {
      localObject = R;
      bool1 = ((com.truecaller.calling.e.e)localObject).b();
      bool2 = J;
      if (bool1 == bool2)
      {
        bool1 = I;
        bool2 = U();
        if (bool1 == bool2) {
          break label140;
        }
      }
    }
    bool1 = L();
    G = bool1;
    bool1 = U();
    I = bool1;
    bool1 = R.b();
    J = bool1;
    bool1 = false;
    Object localObject = null;
    bool2 = false;
    com.truecaller.flashsdk.core.b localb = null;
    int i1 = 1;
    a(this, null, false, i1);
    label140:
    localObject = ae.e();
    bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    if (!bool1) {
      O();
    }
    localObject = (ae.c)b;
    if (localObject != null)
    {
      localb = z;
      bool2 = localb.h();
      ((ae.c)localObject).a(bool2);
    }
    R();
  }
  
  public final void j()
  {
    Z.b();
    int i1 = 1;
    a(this, false, false, i1);
    Y.f();
    K();
    com.truecaller.featuretoggles.b localb = ae.e();
    boolean bool = localb.a();
    if (bool) {
      O();
    }
  }
  
  public final void k()
  {
    Z.c();
    Object localObject = (ae.c)b;
    if (localObject != null) {
      ((ae.c)localObject).n();
    }
    int i1 = 1;
    a(this, false, i1, i1);
    localObject = TimeUnit.SECONDS;
    a locala = ab;
    String str = "adFeatureRetentionTime";
    long l1 = 0L;
    long l2 = locala.a(str, l1);
    long l3 = ((TimeUnit)localObject).toMillis(l2);
    boolean bool = l3 < l1;
    if (!bool)
    {
      Y.d();
      return;
    }
    Y.a(l3);
  }
  
  public final void l()
  {
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void m()
  {
    com.truecaller.analytics.b localb = U;
    Object localObject = new com/truecaller/analytics/bc;
    ((bc)localObject).<init>("callLog", null);
    localObject = (com.truecaller.analytics.e)localObject;
    localb.a((com.truecaller.analytics.e)localObject);
  }
  
  public final int n()
  {
    boolean bool = N;
    if (bool) {
      return 4;
    }
    bool = G();
    if (bool) {
      return 8;
    }
    return 0;
  }
  
  public final void o()
  {
    ae.c localc = (ae.c)b;
    if (localc != null)
    {
      localc.d();
      return;
    }
  }
  
  public final void p()
  {
    ae.c localc = (ae.c)b;
    if (localc != null) {
      localc.e();
    }
    a("menu", "bringBackHiddenMostCalledContacts", "callLog");
  }
  
  public final void q()
  {
    p.a();
    Q();
  }
  
  public final void r()
  {
    int i1 = 1;
    Object localObject1 = new String[i1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("onDeleteSelectedCallsConfirmed for hid= ");
    Object localObject3 = (Iterable)N();
    Object localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject4 = (Collection)localObject4;
    localObject3 = ((Iterable)localObject3).iterator();
    boolean bool1;
    for (;;)
    {
      bool1 = ((Iterator)localObject3).hasNext();
      if (!bool1) {
        break;
      }
      localObject5 = (Iterable)nexta;
      c.a.m.a((Collection)localObject4, (Iterable)localObject5);
    }
    localObject4 = (List)localObject4;
    ((StringBuilder)localObject2).append(localObject4);
    ((StringBuilder)localObject2).append(" \n cid = ");
    localObject3 = (Iterable)N();
    localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject4 = (Collection)localObject4;
    localObject3 = ((Iterable)localObject3).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject3).hasNext();
      if (!bool1) {
        break;
      }
      localObject5 = (Iterable)nextb;
      c.a.m.a((Collection)localObject4, (Iterable)localObject5);
    }
    localObject4 = (List)localObject4;
    ((StringBuilder)localObject2).append(localObject4);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = r;
    localObject2 = h;
    localObject3 = A;
    localObject2 = ((bn)localObject2).plus((c.d.f)localObject3);
    localObject3 = (Iterable)N();
    localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject4 = (Collection)localObject4;
    localObject3 = ((Iterable)localObject3).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject3).hasNext();
      if (!bool1) {
        break;
      }
      localObject5 = (Iterable)nexta;
      c.a.m.a((Collection)localObject4, (Iterable)localObject5);
    }
    localObject4 = (List)localObject4;
    localObject3 = (Iterable)N();
    Object localObject5 = new java/util/ArrayList;
    ((ArrayList)localObject5).<init>();
    localObject5 = (Collection)localObject5;
    localObject3 = ((Iterable)localObject3).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject3).hasNext();
      if (!bool2) {
        break;
      }
      Iterable localIterable = (Iterable)nextb;
      c.a.m.a((Collection)localObject5, localIterable);
    }
    localObject5 = (List)localObject5;
    localObject3 = HistoryEventsScope.CALL_AND_FLASH_EVENTS;
    ((g)localObject1).a((c.d.f)localObject2, (List)localObject4, (List)localObject5, (HistoryEventsScope)localObject3);
    localObject1 = (ae.b)c;
    if (localObject1 != null)
    {
      ((ae.b)localObject1).c();
      return;
    }
  }
  
  public final void s()
  {
    ae.c localc = (ae.c)b;
    if (localc != null) {
      localc.f();
    }
    a("menu", "deleteAllCalls", "callLog");
  }
  
  public final void t()
  {
    new String[1][0] = "onClearCallLogConfirmed";
    g localg = r;
    Object localObject1 = h;
    Object localObject2 = A;
    localObject1 = ((bn)localObject1).plus((c.d.f)localObject2);
    localObject2 = HistoryEventsScope.ONLY_CALL_EVENTS;
    g.a.a(localg, (c.d.f)localObject1, (HistoryEventsScope)localObject2);
  }
  
  public final void u()
  {
    ae.c localc = (ae.c)b;
    if (localc != null) {
      localc.g();
    }
    a("menu", "deleteFlashEvents", "callLog");
  }
  
  public final void v()
  {
    new String[1][0] = "onClearFlashEventsConfirmed";
    g localg = r;
    Object localObject1 = h;
    Object localObject2 = A;
    localObject1 = ((bn)localObject1).plus((c.d.f)localObject2);
    localObject2 = HistoryEventsScope.ONLY_FLASH_EVENTS;
    g.a.a(localg, (c.d.f)localObject1, (HistoryEventsScope)localObject2);
  }
  
  public final void v_()
  {
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.v_();
      return;
    }
  }
  
  public final void w()
  {
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.i();
      return;
    }
  }
  
  public final void w_()
  {
    ae.b localb = (ae.b)c;
    if (localb != null)
    {
      localb.w_();
      return;
    }
  }
  
  public final void x()
  {
    b(true);
  }
  
  public final void x_()
  {
    super.x_();
    h.n();
  }
  
  public final void y()
  {
    b(false);
  }
  
  public final void y_()
  {
    super.y_();
    P();
    I();
    Y.a(null);
    Y.e();
    v localv = L;
    if (localv != null)
    {
      localv.a(null);
      return;
    }
  }
  
  public final void z()
  {
    T();
    d(false);
    ae.c localc = (ae.c)b;
    if (localc != null)
    {
      localc.j();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */