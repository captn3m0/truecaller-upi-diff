package com.truecaller.calling.dialer;

import android.text.Editable;

public final class w
  implements ap
{
  private final Editable a;
  
  public w(Editable paramEditable)
  {
    a = paramEditable;
  }
  
  public final String a()
  {
    return a.toString();
  }
  
  public final void b()
  {
    a.clear();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */