package com.truecaller.calling.dialer;

import dagger.a.d;
import javax.inject.Provider;

public final class bp
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private bp(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static bp a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    bp localbp = new com/truecaller/calling/dialer/bp;
    localbp.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localbp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bp
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */