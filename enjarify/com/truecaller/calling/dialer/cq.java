package com.truecaller.calling.dialer;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.f;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.adapter_delegates.i;
import com.truecaller.adapter_delegates.k;

public final class cq
  extends RecyclerView.ViewHolder
  implements cn.c
{
  private final f b;
  private final f c;
  
  static
  {
    g[] arrayOfg = new g[2];
    Object localObject = new c/g/b/u;
    b localb = w.a(cq.class);
    ((u)localObject).<init>(localb, "iconView", "getIconView()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(cq.class);
    ((u)localObject).<init>(localb, "titleView", "getTitleView()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public cq(View paramView, k paramk)
  {
    super(paramView);
    Object localObject = com.truecaller.utils.extensions.t.a(paramView, 2131363301);
    b = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131364884);
    c = ((f)localObject);
    localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, 12);
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject);
  }
  
  public final void a(int paramInt)
  {
    ((ImageView)b.b()).setImageResource(paramInt);
  }
  
  public final void b(int paramInt)
  {
    ((TextView)c.b()).setText(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */