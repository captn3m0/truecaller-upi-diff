package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.c;
import c.g.a.b;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.analytics.TimingEvent;
import com.truecaller.analytics.au;
import com.truecaller.analytics.av;
import com.truecaller.callhistory.FilterType;
import kotlinx.coroutines.ag;

final class e$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag f;
  
  e$b(e parame, Integer paramInteger, as paramas, FilterType paramFilterType, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/calling/dialer/e$b;
    e locale = b;
    Integer localInteger = c;
    as localas = d;
    FilterType localFilterType = e;
    localb.<init>(locale, localInteger, localas, localFilterType, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.b;
        localObject1 = TimingEvent.CALL_LOG_FETCH_AND_MERGE;
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("limit:");
        Object localObject3 = c;
        ((StringBuilder)localObject2).append(localObject3);
        localObject2 = ((StringBuilder)localObject2).toString();
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("merger:");
        Object localObject4 = d.a();
        ((StringBuilder)localObject3).append((String)localObject4);
        localObject3 = ((StringBuilder)localObject3).toString();
        localObject4 = new com/truecaller/calling/dialer/e$b$1;
        ((e.b.1)localObject4).<init>(this);
        localObject4 = (b)localObject4;
        return av.a((au)paramObject, (TimingEvent)localObject1, (String)localObject2, (String)localObject3, (b)localObject4);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */