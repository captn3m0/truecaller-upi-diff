package com.truecaller.calling.dialer;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l;
import c.n;
import com.truecaller.adapter_delegates.h;
import com.truecaller.analytics.e.a;
import com.truecaller.calling.ActionType;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.flashsdk.core.i;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class bx
  extends com.truecaller.adapter_delegates.c
  implements bw.a
{
  private final bm.a c;
  private final com.truecaller.search.local.model.c d;
  private final bm.a e;
  private final bd f;
  private final com.truecaller.search.local.b.e g;
  private final com.truecaller.i.c h;
  private final i i;
  private final com.truecaller.analytics.b j;
  private final com.truecaller.flashsdk.core.b k;
  private final com.truecaller.data.entity.g l;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(bx.class);
    ((u)localObject).<init>(localb, "searchResults", "getSearchResults()Lkotlin/Pair;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public bx(com.truecaller.search.local.model.c paramc, bm.a parama, bd parambd, com.truecaller.search.local.b.e parame, com.truecaller.i.c paramc1, i parami, com.truecaller.analytics.b paramb, com.truecaller.flashsdk.core.b paramb1, com.truecaller.data.entity.g paramg)
  {
    d = paramc;
    e = parama;
    f = parambd;
    g = parame;
    h = paramc1;
    i = parami;
    j = paramb;
    k = paramb1;
    l = paramg;
    paramc = e;
    c = paramc;
  }
  
  private final n a()
  {
    bm.a locala = c;
    Object localObject = this;
    localObject = (ca)this;
    c.l.g localg = b[0];
    return locala.a((ca)localObject, localg);
  }
  
  private static List a(Contact paramContact)
  {
    paramContact = paramContact.A();
    k.a(paramContact, "numbers");
    paramContact = (Iterable)paramContact;
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramContact = paramContact.iterator();
    for (;;)
    {
      boolean bool = paramContact.hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (Number)paramContact.next();
      String str = "it";
      k.a(localObject2, str);
      localObject2 = ((Number)localObject2).a();
      if (localObject2 != null) {
        ((Collection)localObject1).add(localObject2);
      }
    }
    return (List)localObject1;
  }
  
  private final boolean a(int paramInt)
  {
    bd localbd = f;
    Contact localContact = c(paramInt);
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.SearchResult;
    localbd.a(localContact, localSourceType, true, true, true);
    return true;
  }
  
  private final boolean b()
  {
    String str = h.a("callLogTapBehavior");
    return k.a("call", str);
  }
  
  private final boolean b(int paramInt)
  {
    bd localbd = f;
    Contact localContact = c(paramInt);
    localbd.a(localContact, "dialpadSearchResult");
    return true;
  }
  
  private final Contact c(int paramInt)
  {
    Object localObject1 = (bz)ab;
    boolean bool1 = localObject1 instanceof bz.a;
    int m = 1;
    Contact localContact;
    Object localObject2;
    if (bool1)
    {
      localContact = a.get(paramInt)).a;
      localObject1 = localContact.A();
      boolean bool2 = ((List)localObject1).isEmpty();
      if (bool2)
      {
        localObject1 = localContact.p();
        if (localObject1 != null)
        {
          localObject2 = l;
          String[] arrayOfString = new String[m];
          arrayOfString[0] = localObject1;
          localObject1 = ((com.truecaller.data.entity.g)localObject2).b(arrayOfString);
          localContact.a((Number)localObject1);
        }
      }
    }
    else
    {
      paramInt = localObject1 instanceof bz.c;
      if (paramInt != 0)
      {
        localObject1 = (bz.c)localObject1;
        localContact = a;
      }
      else
      {
        paramInt = 0;
        localContact = null;
      }
    }
    if (localContact == null)
    {
      localContact = new com/truecaller/data/entity/Contact;
      localContact.<init>();
      localObject1 = (String)aa;
      localContact.l((String)localObject1);
      localObject1 = l;
      localObject2 = new String[m];
      String str = (String)aa;
      localObject2[0] = str;
      localObject1 = ((com.truecaller.data.entity.g)localObject1).b((String[])localObject2);
      localContact.a((Number)localObject1);
    }
    return localContact;
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject1 = a;
    Object localObject2 = "ItemEvent.CLICKED";
    boolean bool1 = k.a(localObject1, localObject2);
    boolean bool2;
    if (bool1)
    {
      m = b;
      bool2 = b();
      if (bool2) {
        return b(m);
      }
      return a(m);
    }
    localObject2 = "ItemEvent.SWIPE_START";
    bool1 = k.a(localObject1, localObject2);
    boolean bool3 = true;
    if (bool1) {
      return bool3;
    }
    localObject2 = "ItemEvent.SWIPE_COMPLETED_FROM_START";
    bool1 = k.a(localObject1, localObject2);
    if (!bool1)
    {
      localObject2 = ActionType.CELLULAR_CALL.getEventAction();
      bool1 = k.a(localObject1, localObject2);
      if (!bool1)
      {
        localObject2 = "ItemEvent.SWIPE_COMPLETED_FROM_END";
        bool1 = k.a(localObject1, localObject2);
        if (!bool1)
        {
          localObject2 = ActionType.SMS.getEventAction();
          bool1 = k.a(localObject1, localObject2);
          if (!bool1)
          {
            localObject2 = ActionType.PROFILE.getEventAction();
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              m = b;
              return a(m);
            }
            return false;
          }
        }
        m = b;
        localObject1 = new com/truecaller/analytics/e$a;
        ((e.a)localObject1).<init>("ViewAction");
        localObject1 = ((e.a)localObject1).a("Action", "message").a("Context", "dialpadSearchResult");
        localObject2 = j;
        localObject1 = ((e.a)localObject1).a();
        k.a(localObject1, "event.build()");
        ((com.truecaller.analytics.b)localObject2).a((com.truecaller.analytics.e)localObject1);
        localObject1 = f;
        paramh = c(m);
        ((bd)localObject1).b(paramh, "dialpadSearchResult");
        return bool3;
      }
    }
    int m = b;
    return b(m);
  }
  
  public final int getItemCount()
  {
    Object localObject1 = (bz)ab;
    boolean bool1 = localObject1 instanceof bz.a;
    if (bool1) {
      return a.size();
    }
    Object localObject2 = bz.b.a;
    bool1 = k.a(localObject1, localObject2);
    if (bool1) {
      return 0;
    }
    bool1 = localObject1 instanceof bz.c;
    if (!bool1)
    {
      localObject2 = bz.d.a;
      boolean bool2 = k.a(localObject1, localObject2);
      if (!bool2)
      {
        localObject1 = new c/l;
        ((l)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    return 1;
  }
  
  public final long getItemId(int paramInt)
  {
    Long localLong = c(paramInt).getId();
    if (localLong != null) {
      return localLong.longValue();
    }
    return -1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bx
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */