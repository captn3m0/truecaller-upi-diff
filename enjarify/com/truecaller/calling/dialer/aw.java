package com.truecaller.calling.dialer;

import c.a.an;
import c.g.b.k;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.search.local.model.i.a;
import java.util.LinkedHashSet;
import java.util.Set;

public abstract class aw
  implements i.a, Comparable
{
  final Set a;
  final Set b;
  final HistoryEvent c;
  
  private aw(HistoryEvent paramHistoryEvent)
  {
    c = paramHistoryEvent;
    paramHistoryEvent = c.getId();
    int i = 1;
    if (paramHistoryEvent != null)
    {
      Long[] arrayOfLong = new Long[i];
      arrayOfLong[0] = paramHistoryEvent;
      paramHistoryEvent = an.b(arrayOfLong);
      if (paramHistoryEvent != null) {}
    }
    else
    {
      paramHistoryEvent = new java/util/LinkedHashSet;
      paramHistoryEvent.<init>();
      paramHistoryEvent = (Set)paramHistoryEvent;
    }
    a = paramHistoryEvent;
    paramHistoryEvent = c.i();
    if (paramHistoryEvent != null)
    {
      long l = ((Number)paramHistoryEvent).longValue();
      paramHistoryEvent = new Long[i];
      Long localLong = Long.valueOf(l);
      paramHistoryEvent[0] = localLong;
      paramHistoryEvent = an.b(paramHistoryEvent);
      if (paramHistoryEvent != null) {}
    }
    else
    {
      paramHistoryEvent = new java/util/LinkedHashSet;
      paramHistoryEvent.<init>();
      paramHistoryEvent = (Set)paramHistoryEvent;
    }
    b = paramHistoryEvent;
  }
  
  public final String a()
  {
    String str = c.a();
    if (str == null) {
      str = c.b();
    }
    if (str == null) {
      str = "";
    }
    return str;
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    k.b(paramHistoryEvent, "event");
    Long localLong = paramHistoryEvent.getId();
    if (localLong != null)
    {
      Set localSet = a;
      localSet.add(localLong);
    }
    paramHistoryEvent = paramHistoryEvent.i();
    if (paramHistoryEvent != null)
    {
      long l = ((Number)paramHistoryEvent).longValue();
      paramHistoryEvent = b;
      localLong = Long.valueOf(l);
      paramHistoryEvent.add(localLong);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.aw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */