package com.truecaller.calling.dialer;

import android.app.Activity;
import android.arch.lifecycle.e;
import android.arch.lifecycle.e.b;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;
import c.u;
import com.truecaller.ads.b.w;
import com.truecaller.analytics.az;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.c.c;
import com.truecaller.calling.c.c.a;
import com.truecaller.calling.d.m;
import com.truecaller.calling.e.i;
import com.truecaller.calling.initiate_call.b;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.common.ui.b.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.d;
import com.truecaller.messaging.defaultsms.DefaultSmsActivity;
import com.truecaller.referral.ReferralManager;
import com.truecaller.search.global.CompositeAdapterDelegate.SearchResultOrder;
import com.truecaller.search.global.n;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.ac;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.p;
import com.truecaller.ui.view.BottomBar;
import com.truecaller.util.bj;
import com.truecaller.util.br.a;
import java.util.HashMap;
import java.util.List;

public final class l
  extends Fragment
  implements DialogInterface.OnClickListener, az, ae.b, aj.c, com.truecaller.ui.ab, p
{
  public ae.a a;
  public n.a b;
  public cf.a c;
  public ck.b d;
  public cn.b e;
  public aj.b f;
  public s g;
  public bn.a h;
  public bw.a i;
  public be.a j;
  public ay.a k;
  public w l;
  public b m;
  public com.truecaller.premium.br n;
  public ReferralManager o;
  private ActionMode p;
  private final l.a q;
  private HashMap r;
  
  public l()
  {
    l.a locala = new com/truecaller/calling/dialer/l$a;
    locala.<init>(this);
    q = locala;
  }
  
  public final boolean W_()
  {
    Object localObject = f;
    String str;
    if (localObject == null)
    {
      str = "dialpadPresenter";
      k.a(str);
    }
    boolean bool = ((aj.b)localObject).f();
    if (!bool)
    {
      localObject = a;
      if (localObject == null)
      {
        str = "dialerPresenter";
        k.a(str);
      }
      bool = ((ae.a)localObject).h();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public final ae.a a()
  {
    ae.a locala = a;
    if (locala == null)
    {
      String str = "dialerPresenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(Contact paramContact)
  {
    Object localObject = "contact";
    k.b(paramContact, (String)localObject);
    try
    {
      localObject = new com/truecaller/calling/dialer/l$b;
      ((l.b)localObject).<init>(this, paramContact);
      localObject = (br.a)localObject;
      paramContact = com.truecaller.util.br.a(paramContact, (br.a)localObject);
      localObject = getFragmentManager();
      String str = com.truecaller.util.br.a;
      paramContact.show((j)localObject, str);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      d.a((Throwable)localActivityNotFoundException, "Cannot find an activity to insert contact");
    }
  }
  
  public final void a(Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(paramContact, "contact");
    k.b(paramSourceType, "sourceType");
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    DetailsFragment.a((Context)localf, paramContact, paramSourceType, paramBoolean1, paramBoolean2, paramBoolean3);
  }
  
  public final void a(Contact paramContact, String paramString)
  {
    k.b(paramContact, "contact");
    Object localObject = "analyticsContext";
    k.b(paramString, (String)localObject);
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    localObject = c.e;
    List localList = paramContact.A();
    c.a.a(localf, paramContact, localList, false, true, false, false, paramString, 232);
  }
  
  public final void a(Contact paramContact, String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "fallbackNumber");
    k.b(paramString2, "callType");
    k.b(paramString3, "analyticsContext");
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    i.a((Activity)localf, paramContact, paramString1, paramString2, paramString3);
  }
  
  public final void a(HistoryEvent paramHistoryEvent, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramHistoryEvent, "historyEvent");
    k.b(paramSourceType, "sourceType");
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    Object localObject2 = localObject1;
    localObject2 = (Context)localObject1;
    localObject1 = paramHistoryEvent.s();
    Object localObject3;
    if (localObject1 != null)
    {
      localObject1 = ((Contact)localObject1).getTcId();
      localObject3 = localObject1;
    }
    else
    {
      localObject3 = null;
    }
    localObject1 = paramHistoryEvent.s();
    Object localObject4;
    if (localObject1 != null)
    {
      localObject1 = ((Contact)localObject1).t();
      localObject4 = localObject1;
    }
    else
    {
      localObject4 = null;
    }
    String str1 = paramHistoryEvent.a();
    String str2 = paramHistoryEvent.b();
    String str3 = paramHistoryEvent.d();
    DetailsFragment.a((Context)localObject2, (String)localObject3, (String)localObject4, str1, str2, str3, paramSourceType, paramBoolean1, paramBoolean2);
  }
  
  public final void a(String paramString)
  {
    paramString = a;
    if (paramString == null)
    {
      String str = "dialerPresenter";
      k.a(str);
    }
    paramString.m();
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "number");
    String str = "analyticsContext";
    k.b(paramString2, str);
    paramString2 = getActivity();
    if (paramString2 == null) {
      return;
    }
    k.a(paramString2, "activity ?: return");
    bj.a((Context)paramString2, paramString1);
  }
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean, CompositeAdapterDelegate.SearchResultOrder paramSearchResultOrder)
  {
    k.b(paramSearchResultOrder, "searchOrder");
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    n.a((Activity)localf, paramString1, paramString2, paramBoolean, paramSearchResultOrder);
  }
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean, String paramString3)
  {
    k.b(paramString1, "number");
    k.b(paramString3, "analyticsContext");
    b.a.a locala = new com/truecaller/calling/initiate_call/b$a$a;
    locala.<init>(paramString1, paramString3);
    paramString1 = locala.a(paramString2).a(paramBoolean);
    paramString2 = m;
    if (paramString2 == null)
    {
      String str = "initiateCallHelper";
      k.a(str);
    }
    paramString1 = paramString1.a();
    paramString2.a(paramString1);
  }
  
  public final void a(boolean paramBoolean)
  {
    ae.a locala = a;
    if (locala == null)
    {
      String str = "dialerPresenter";
      k.a(str);
    }
    locala.k();
  }
  
  public final void b()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = (AppCompatActivity)localObject;
      ActionMode.Callback localCallback = (ActionMode.Callback)q;
      ((AppCompatActivity)localObject).startSupportActionMode(localCallback);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw ((Throwable)localObject);
  }
  
  public final void b(Contact paramContact, String paramString)
  {
    k.b(paramContact, "contact");
    Object localObject = "analyticsContext";
    k.b(paramString, (String)localObject);
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    localObject = c.e;
    List localList = paramContact.A();
    c.a.a(localf, paramContact, localList, false, false, true, false, paramString, 184);
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "tag");
    j localj = getFragmentManager();
    if (localj != null) {
      paramString = localj.a(paramString);
    } else {
      paramString = null;
    }
    return paramString != null;
  }
  
  public final void b_(int paramInt)
  {
    m.a((Fragment)this, paramInt, null, true);
  }
  
  public final void c()
  {
    ActionMode localActionMode = p;
    if (localActionMode != null)
    {
      l.a locala = q;
      int i1 = a;
      Object localObject = localActionMode.getTag();
      boolean bool = localObject instanceof Integer;
      if (bool)
      {
        localObject = (Integer)localObject;
        int i2 = ((Integer)localObject).intValue();
        if (i1 == i2)
        {
          i1 = 1;
          break label69;
        }
      }
      i1 = 0;
      locala = null;
      label69:
      if (i1 == 0) {
        localActionMode = null;
      }
      if (localActionMode != null)
      {
        localActionMode.finish();
        return;
      }
    }
  }
  
  public final void d()
  {
    ActionMode localActionMode = p;
    if (localActionMode != null)
    {
      localActionMode.invalidate();
      return;
    }
  }
  
  public final int f()
  {
    ae.a locala = a;
    if (locala == null)
    {
      String str = "dialerPresenter";
      k.a(str);
    }
    return locala.n();
  }
  
  public final void h()
  {
    Object localObject = getActivity();
    boolean bool = localObject instanceof TruecallerInit;
    if (!bool) {
      localObject = null;
    }
    localObject = (TruecallerInit)localObject;
    if (localObject == null) {
      return;
    }
    bool = isAdded();
    if (bool)
    {
      String str = "contacts";
      ((TruecallerInit)localObject).a(str);
    }
  }
  
  public final void i()
  {
    Object localObject = getContext();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "context ?: return");
    localObject = DefaultSmsActivity.a((Context)localObject, "callHistory");
    startActivityForResult((Intent)localObject, 4);
  }
  
  public final void m()
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = "dialerPresenter";
      k.a((String)localObject);
    }
  }
  
  public final void n()
  {
    ae.a locala = a;
    if (locala == null)
    {
      String str = "dialerPresenter";
      k.a(str);
    }
    locala.j();
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Object localObject = null;
    boolean bool = m.a(paramInt1, paramInt2, paramIntent, null);
    if (bool) {
      return;
    }
    localObject = new com/truecaller/calling/dialer/l$c;
    ((l.c)localObject).<init>(this);
    localObject = (c.g.a.a)localObject;
    int i1 = 4;
    if (paramInt1 == i1)
    {
      ((c.g.a.a)localObject).invoke();
      bool = true;
    }
    else
    {
      bool = false;
      localObject = null;
    }
    if (bool) {
      return;
    }
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    int i1 = -1;
    if (paramInt == i1)
    {
      paramDialogInterface = b;
      if (paramDialogInterface == null)
      {
        String str = "completedCallLogItemsPresenter";
        k.a(str);
      }
      paramDialogInterface.u_();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    boolean bool;
    if (paramBundle != null)
    {
      paramBundle = paramBundle.getApplicationContext();
    }
    else
    {
      bool = false;
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      paramBundle = ((bk)paramBundle).a();
      Object localObject1 = new com/truecaller/calling/dialer/bi;
      Object localObject2 = o;
      Object localObject3 = getChildFragmentManager();
      ((bi)localObject1).<init>((ReferralManager)localObject2, (j)localObject3);
      paramBundle.a((bi)localObject1).a(this);
      paramBundle = g;
      if (paramBundle == null)
      {
        localObject1 = "callHistoryObserver";
        k.a((String)localObject1);
      }
      localObject1 = new com/truecaller/calling/dialer/LifecycleAwareCondition;
      localObject2 = getLifecycle();
      k.a(localObject2, "lifecycle");
      localObject3 = e.b.d;
      ((LifecycleAwareCondition)localObject1).<init>((e)localObject2, (e.b)localObject3);
      localObject1 = (q)localObject1;
      paramBundle.a((q)localObject1);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "dialerPresenter";
        k.a((String)localObject1);
      }
      localObject1 = g;
      if (localObject1 == null)
      {
        localObject2 = "callHistoryObserver";
        k.a((String)localObject2);
      }
      localObject1 = (v)localObject1;
      paramBundle.a((v)localObject1);
      bool = true;
      setHasOptionsMenu(bool);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "dialerPresenter";
        k.a((String)localObject1);
      }
      paramBundle.b(this);
      paramBundle = f;
      if (paramBundle == null)
      {
        localObject1 = "dialpadPresenter";
        k.a((String)localObject1);
      }
      paramBundle.b(this);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    k.b(paramMenu, "menu");
    k.b(paramMenuInflater, "inflater");
    paramMenuInflater.inflate(2131623941, paramMenu);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558668, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    Object localObject = f;
    String str;
    if (localObject == null)
    {
      str = "dialpadPresenter";
      k.a(str);
    }
    ((aj.b)localObject).x_();
    localObject = a;
    if (localObject == null)
    {
      str = "dialerPresenter";
      k.a(str);
    }
    ((ae.a)localObject).x_();
    super.onDestroy();
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    String str;
    if (localObject == null)
    {
      str = "dialerPresenter";
      k.a(str);
    }
    ((ae.a)localObject).y_();
    localObject = f;
    if (localObject == null)
    {
      str = "dialpadPresenter";
      k.a(str);
    }
    ((aj.b)localObject).y_();
    super.onDestroyView();
    localObject = r;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onHiddenChanged(boolean paramBoolean)
  {
    super.onHiddenChanged(paramBoolean);
    if (paramBoolean)
    {
      ae.a locala = a;
      if (locala == null)
      {
        String str = "dialerPresenter";
        k.a(str);
      }
      locala.l();
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    String str = "item";
    k.b(paramMenuItem, str);
    int i1 = paramMenuItem.getItemId();
    int i2 = 2131361866;
    if (i1 != i2) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    paramMenuItem = a;
    if (paramMenuItem == null)
    {
      str = "dialerPresenter";
      k.a(str);
    }
    paramMenuItem.o();
    return true;
  }
  
  public final void onPause()
  {
    aj.b localb = f;
    if (localb == null)
    {
      String str = "dialpadPresenter";
      k.a(str);
    }
    localb.e();
    super.onPause();
  }
  
  public final void onResume()
  {
    super.onResume();
    Object localObject1 = getActivity();
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = "activity ?: return";
      k.a(localObject1, (String)localObject2);
      localObject1 = ((f)localObject1).getIntent();
      if (localObject1 != null)
      {
        localObject2 = ((Intent)localObject1).getAction();
        Object localObject3;
        Object localObject4;
        if (localObject2 != null)
        {
          int i1 = ((String)localObject2).hashCode();
          int i2 = -1173708363;
          if (i1 != i2)
          {
            i2 = -1173171990;
            if (i1 != i2) {
              break label234;
            }
            localObject3 = "android.intent.action.VIEW";
            bool = ((String)localObject2).equals(localObject3);
            if (!bool) {
              break label234;
            }
          }
          else
          {
            localObject3 = "android.intent.action.DIAL";
            bool = ((String)localObject2).equals(localObject3);
            if (!bool) {
              break label234;
            }
          }
          localObject2 = ((Intent)localObject1).getType();
          localObject3 = "vnd.android.cursor.dir/calls";
          bool = k.a(localObject2, localObject3);
          if (bool)
          {
            localObject2 = f;
            if (localObject2 == null)
            {
              localObject3 = "dialpadPresenter";
              k.a((String)localObject3);
            }
            ((aj.b)localObject2).d();
          }
          bool = false;
          localObject2 = null;
          try
          {
            localObject3 = getContext();
            localObject3 = com.truecaller.common.h.ab.a((Intent)localObject1, (Context)localObject3);
          }
          catch (SecurityException localSecurityException)
          {
            i1 = 0;
            localObject3 = null;
          }
          localObject4 = f;
          if (localObject4 == null)
          {
            String str = "dialpadPresenter";
            k.a(str);
          }
          localObject3 = bj.b((String)localObject3);
          ((aj.b)localObject4).c((String)localObject3);
          ((Intent)localObject1).setAction(null);
        }
        label234:
        localObject2 = "promotion_setting_key";
        boolean bool = ((Intent)localObject1).hasExtra((String)localObject2);
        if (bool)
        {
          localObject2 = ((Intent)localObject1).getStringExtra("promotion_setting_key");
          localObject3 = a;
          if (localObject3 == null)
          {
            localObject4 = "dialerPresenter";
            k.a((String)localObject4);
          }
          localObject4 = "promotionType";
          k.a(localObject2, (String)localObject4);
          ((ae.a)localObject3).a((String)localObject2);
          localObject2 = "promotion_setting_key";
          ((Intent)localObject1).removeExtra((String)localObject2);
        }
      }
    }
    localObject1 = f;
    if (localObject1 == null)
    {
      localObject2 = "dialpadPresenter";
      k.a((String)localObject2);
    }
    ((aj.b)localObject1).c();
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "dialerPresenter";
      k.a((String)localObject2);
    }
    ((ae.a)localObject1).i();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject1 = this;
    Object localObject2 = paramView;
    Object localObject3 = "view";
    k.b(paramView, (String)localObject3);
    Object localObject4 = a;
    if (localObject4 == null)
    {
      localObject3 = "dialerPresenter";
      k.a((String)localObject3);
    }
    Object localObject5 = new com/truecaller/calling/dialer/ai;
    localObject3 = a;
    if (localObject3 == null)
    {
      localObject6 = "dialerPresenter";
      k.a((String)localObject6);
    }
    Object localObject6 = localObject3;
    localObject6 = (ae.c.b)localObject3;
    localObject3 = getActivity();
    if (localObject3 != null)
    {
      Object localObject7 = localObject3;
      localObject7 = (com.truecaller.common.ui.a)localObject3;
      localObject3 = new com/truecaller/calling/dialer/l$d;
      ((l.d)localObject3).<init>((l)localObject1);
      Object localObject8 = localObject3;
      localObject8 = (c.g.a.a)localObject3;
      Object localObject9 = b;
      if (localObject9 == null)
      {
        localObject3 = "completedCallLogItemsPresenter";
        k.a((String)localObject3);
      }
      cf.a locala = c;
      if (locala == null)
      {
        localObject3 = "suggestedBarPresenter";
        k.a((String)localObject3);
      }
      be.a locala1 = j;
      if (locala1 == null)
      {
        localObject3 = "promotionsPresenter";
        k.a((String)localObject3);
      }
      ck.b localb = d;
      if (localb == null)
      {
        localObject3 = "suggestedContactsPresenter";
        k.a((String)localObject3);
      }
      cn.b localb1 = e;
      if (localb1 == null)
      {
        localObject3 = "suggestedPremiumPresenter";
        k.a((String)localObject3);
      }
      w localw = l;
      if (localw == null)
      {
        localObject3 = "multiAdsPresenter";
        k.a((String)localObject3);
      }
      aj.b localb2 = f;
      if (localb2 == null)
      {
        localObject3 = "dialpadPresenter";
        k.a((String)localObject3);
      }
      Object localObject10 = h;
      if (localObject10 == null)
      {
        localObject3 = "searchMorePresenter";
        k.a((String)localObject3);
      }
      localObject3 = i;
      if (localObject3 == null)
      {
        localObject11 = "searchResultItemsPresenter";
        k.a((String)localObject11);
      }
      localObject2 = k;
      if (localObject2 == null)
      {
        localObject11 = "onGoingFlashPresenter";
        k.a((String)localObject11);
      }
      Object localObject11 = (b.a)getActivity();
      Object localObject12 = localObject2;
      localObject2 = n;
      if (localObject2 == null)
      {
        localObject13 = "premiumScreenNavigator";
        k.a((String)localObject13);
      }
      Object localObject13 = localObject3;
      localObject3 = localObject5;
      Object localObject14 = localObject10;
      localObject10 = paramView;
      localObject1 = localObject5;
      localObject5 = localObject14;
      localObject14 = localObject3;
      localObject1 = localObject4;
      localObject4 = localObject13;
      localObject13 = localObject2;
      localObject2 = localObject12;
      localObject12 = localObject13;
      ((ai)localObject3).<init>((ae.c.b)localObject6, (com.truecaller.common.ui.a)localObject7, paramView, (c.g.a.a)localObject8, (n.a)localObject9, locala, locala1, localb, localb1, localw, localb2, (bn.a)localObject5, (bw.a)localObject4, (ay.a)localObject2, (b.a)localObject11, (com.truecaller.premium.br)localObject13);
      ((ae.a)localObject1).a(localObject3);
      localObject1 = this;
      localObject3 = f;
      if (localObject3 == null)
      {
        localObject6 = "dialpadPresenter";
        k.a((String)localObject6);
      }
      localObject6 = new com/truecaller/calling/dialer/ao;
      localObject7 = paramView;
      localObject10 = paramView;
      localObject10 = (ConstraintLayout)paramView;
      localObject8 = f;
      if (localObject8 == null)
      {
        localObject9 = "dialpadPresenter";
        k.a((String)localObject9);
      }
      localObject8 = (aj.d.a)localObject8;
      int i1 = 2131363475;
      localObject7 = ((View)localObject7).findViewById(i1);
      k.a(localObject7, "view.findViewById(R.id.input_window)");
      localObject7 = (ViewGroup)localObject7;
      localObject9 = getActivity();
      if (localObject9 != null)
      {
        int i2 = 2131362140;
        localObject9 = ((f)localObject9).findViewById(i2);
      }
      else
      {
        i1 = 0;
        localObject9 = null;
      }
      if (localObject9 != null)
      {
        localObject9 = (BottomBar)localObject9;
        ((ao)localObject6).<init>((ConstraintLayout)localObject10, (aj.d.a)localObject8, (ViewGroup)localObject7, (BottomBar)localObject9);
        ((aj.b)localObject3).a(localObject6);
        return;
      }
      localObject3 = new c/u;
      ((u)localObject3).<init>("null cannot be cast to non-null type com.truecaller.ui.view.BottomBar");
      throw ((Throwable)localObject3);
    }
    localObject3 = new c/u;
    ((u)localObject3).<init>("null cannot be cast to non-null type com.truecaller.common.ui.AppBarContainer");
    throw ((Throwable)localObject3);
  }
  
  public final void v_()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      b localb = m;
      if (localb == null)
      {
        String str = "initiateCallHelper";
        k.a(str);
      }
      k.a(localObject, "it");
      localObject = (Activity)localObject;
      localb.a((Activity)localObject);
      return;
    }
  }
  
  public final void w_()
  {
    ac localac = new com/truecaller/ui/ac;
    localac.<init>();
    j localj = getChildFragmentManager();
    localac.show(localj, null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */