package com.truecaller.calling.dialer;

public enum HistoryEventsScope
{
  static
  {
    HistoryEventsScope[] arrayOfHistoryEventsScope = new HistoryEventsScope[3];
    HistoryEventsScope localHistoryEventsScope = new com/truecaller/calling/dialer/HistoryEventsScope;
    localHistoryEventsScope.<init>("ONLY_CALL_EVENTS", 0);
    ONLY_CALL_EVENTS = localHistoryEventsScope;
    arrayOfHistoryEventsScope[0] = localHistoryEventsScope;
    localHistoryEventsScope = new com/truecaller/calling/dialer/HistoryEventsScope;
    int i = 1;
    localHistoryEventsScope.<init>("ONLY_FLASH_EVENTS", i);
    ONLY_FLASH_EVENTS = localHistoryEventsScope;
    arrayOfHistoryEventsScope[i] = localHistoryEventsScope;
    localHistoryEventsScope = new com/truecaller/calling/dialer/HistoryEventsScope;
    i = 2;
    localHistoryEventsScope.<init>("CALL_AND_FLASH_EVENTS", i);
    CALL_AND_FLASH_EVENTS = localHistoryEventsScope;
    arrayOfHistoryEventsScope[i] = localHistoryEventsScope;
    $VALUES = arrayOfHistoryEventsScope;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.HistoryEventsScope
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */