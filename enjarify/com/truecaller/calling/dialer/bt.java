package com.truecaller.calling.dialer;

import dagger.a.d;
import javax.inject.Provider;

public final class bt
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private bt(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static bt a(Provider paramProvider1, Provider paramProvider2)
  {
    bt localbt = new com/truecaller/calling/dialer/bt;
    localbt.<init>(paramProvider1, paramProvider2);
    return localbt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bt
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */