package com.truecaller.calling.dialer;

public enum CallIconType
{
  static
  {
    CallIconType[] arrayOfCallIconType = new CallIconType[6];
    CallIconType localCallIconType = new com/truecaller/calling/dialer/CallIconType;
    localCallIconType.<init>("HUNG_UP_CALL_ICON", 0);
    HUNG_UP_CALL_ICON = localCallIconType;
    arrayOfCallIconType[0] = localCallIconType;
    localCallIconType = new com/truecaller/calling/dialer/CallIconType;
    int i = 1;
    localCallIconType.<init>("MUTED_CALL_ICON", i);
    MUTED_CALL_ICON = localCallIconType;
    arrayOfCallIconType[i] = localCallIconType;
    localCallIconType = new com/truecaller/calling/dialer/CallIconType;
    i = 2;
    localCallIconType.<init>("INCOMING_CALL_ICON", i);
    INCOMING_CALL_ICON = localCallIconType;
    arrayOfCallIconType[i] = localCallIconType;
    localCallIconType = new com/truecaller/calling/dialer/CallIconType;
    i = 3;
    localCallIconType.<init>("OUTGOING_CALL_ICON", i);
    OUTGOING_CALL_ICON = localCallIconType;
    arrayOfCallIconType[i] = localCallIconType;
    localCallIconType = new com/truecaller/calling/dialer/CallIconType;
    i = 4;
    localCallIconType.<init>("MISSED_CALL_ICON", i);
    MISSED_CALL_ICON = localCallIconType;
    arrayOfCallIconType[i] = localCallIconType;
    localCallIconType = new com/truecaller/calling/dialer/CallIconType;
    i = 5;
    localCallIconType.<init>("FLASH", i);
    FLASH = localCallIconType;
    arrayOfCallIconType[i] = localCallIconType;
    $VALUES = arrayOfCallIconType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.CallIconType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */