package com.truecaller.calling.dialer;

import android.view.View;
import c.a.ag;
import c.g.b.k;
import c.t;
import com.truecaller.adapter_delegates.c;
import com.truecaller.f.a.i;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.featuretoggles.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class bf
  extends c
  implements be.a
{
  private final Map b;
  private i c;
  private final be.b.b d;
  
  public bf(be.b.b paramb, e parame, Set paramSet)
  {
    d = paramb;
    paramSet = (Iterable)paramSet;
    paramb = new java/util/ArrayList;
    int i = c.a.m.a(paramSet, 10);
    paramb.<init>(i);
    paramb = (Collection)paramb;
    paramSet = paramSet.iterator();
    for (;;)
    {
      boolean bool1 = paramSet.hasNext();
      if (!bool1) {
        break;
      }
      localObject = (i)paramSet.next();
      String str = ((i)localObject).e();
      localObject = t.a(str, localObject);
      paramb.add(localObject);
    }
    paramb = ag.a((Iterable)paramb);
    b = paramb;
    paramb = G;
    paramSet = e.a[92];
    paramb = (CharSequence)((f)paramb.a(parame, paramSet)).e();
    parame = new String[] { "," };
    boolean bool2 = false;
    paramSet = null;
    int j = 6;
    paramb = (Iterable)c.n.m.c(paramb, parame, false, j);
    parame = new java/util/ArrayList;
    parame.<init>();
    parame = (Collection)parame;
    paramb = paramb.iterator();
    for (;;)
    {
      bool2 = paramb.hasNext();
      if (!bool2) {
        break;
      }
      paramSet = (String)paramb.next();
      localObject = b;
      paramSet = (i)((Map)localObject).get(paramSet);
      if (paramSet != null) {
        parame.add(paramSet);
      }
    }
    parame = (Iterable)parame;
    paramb = parame.iterator();
    do
    {
      bool3 = paramb.hasNext();
      if (!bool3) {
        break;
      }
      parame = paramb.next();
      paramSet = parame;
      paramSet = (i)parame;
      bool2 = paramSet.b();
    } while (!bool2);
    break label336;
    boolean bool3 = false;
    parame = null;
    label336:
    parame = (i)parame;
    c = parame;
  }
  
  private final void b()
  {
    c = null;
    d.H();
  }
  
  public final void a()
  {
    i locali = c;
    if (locali != null) {
      locali.c();
    }
    b();
  }
  
  public final void a(View paramView)
  {
    k.b(paramView, "view");
    i locali = c;
    if (locali != null) {
      locali.a(paramView);
    }
    b();
  }
  
  public final int getItemCount()
  {
    i locali = c;
    if (locali != null) {
      return 1;
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bf
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */