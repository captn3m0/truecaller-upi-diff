package com.truecaller.calling.dialer;

import dagger.a.d;
import javax.inject.Provider;

public final class bg
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private bg(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static bg a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    bg localbg = new com/truecaller/calling/dialer/bg;
    localbg.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localbg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */