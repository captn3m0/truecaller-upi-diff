package com.truecaller.calling.dialer;

import android.text.Editable;
import android.text.TextWatcher;
import c.g.b.k;

public final class ao$a
  implements TextWatcher
{
  ao$a(ao paramao) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    k.b(paramEditable, "s");
    aj.d.a locala = a.a;
    Object localObject = new com/truecaller/calling/dialer/w;
    ((w)localObject).<init>(paramEditable);
    localObject = (ap)localObject;
    locala.a((ap)localObject);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ao.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */