package com.truecaller.calling.dialer;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.n;
import c.n.m;
import com.truecaller.analytics.e.a;
import com.truecaller.calling.d.q;
import com.truecaller.common.account.r;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.search.global.CompositeAdapterDelegate.SearchResultOrder;
import com.truecaller.ui.keyboard.DialerKeypad.KeyActionState;
import com.truecaller.ui.view.BottomBar.DialpadState;
import java.util.HashSet;
import java.util.List;

public final class ak
  extends com.truecaller.bd
  implements aj.b
{
  private boolean d;
  private String e;
  private int f;
  private int g;
  private String h;
  private final HashSet i;
  private final bm.a j;
  private final bd k;
  private final r l;
  private final com.truecaller.util.bz m;
  private final com.truecaller.util.al n;
  private final com.truecaller.i.c o;
  private final q p;
  private final aj.a.a q;
  private final com.truecaller.analytics.b r;
  private final com.truecaller.data.entity.g s;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(ak.class);
    ((u)localObject).<init>(localb, "searchState", "getSearchState()Lkotlin/Pair;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public ak(bd parambd, r paramr, com.truecaller.util.bz parambz, com.truecaller.util.al paramal, com.truecaller.i.c paramc, q paramq, aj.a.a parama, com.truecaller.analytics.b paramb, com.truecaller.data.entity.g paramg, bm.a parama1)
  {
    k = parambd;
    l = paramr;
    m = parambz;
    n = paramal;
    o = paramc;
    p = paramq;
    q = parama;
    r = paramb;
    s = paramg;
    d = true;
    e = "";
    int i1 = -1;
    f = i1;
    g = i1;
    parambd = new java/util/HashSet;
    parambd.<init>();
    i = parambd;
    j = parama1;
  }
  
  private final void a(String paramString1, String paramString2, String paramString3)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("ViewAction");
    String str = "Context";
    paramString1 = ((e.a)localObject).a(str, paramString1);
    localObject = "Action";
    paramString1 = paramString1.a((String)localObject, paramString2);
    if (paramString3 != null)
    {
      paramString2 = "SubAction";
      paramString1.a(paramString2, paramString3);
    }
    paramString2 = r;
    paramString1 = paramString1.a();
    k.a(paramString1, "eventBuilder.build()");
    paramString2.b(paramString1);
  }
  
  private final boolean a(char paramChar)
  {
    char c1 = ',';
    char c2 = ';';
    int i1 = 1;
    if ((paramChar != c1) && (paramChar != c2)) {
      return i1;
    }
    int i2 = f;
    char c3 = '￿';
    if (i2 == c3)
    {
      String str1 = e;
      i2 = str1.length();
    }
    if (i2 != 0)
    {
      CharSequence localCharSequence = (CharSequence)e;
      int i4 = localCharSequence.length();
      if (i4 == 0)
      {
        i4 = 1;
      }
      else
      {
        i4 = 0;
        localCharSequence = null;
      }
      if (i4 == 0)
      {
        if (paramChar == c1) {
          return i1;
        }
        String str2 = e;
        int i3;
        i2 -= i1;
        paramChar = str2.charAt(i3);
        if (paramChar == c2) {
          return false;
        }
        paramChar = g;
        if (paramChar != c3)
        {
          String str3 = e;
          c1 = str3.length();
          if (paramChar < c1)
          {
            str2 = e;
            c1 = g;
            paramChar = str2.charAt(c1);
            if (paramChar == c2) {
              return false;
            }
          }
        }
        return i1;
      }
    }
    return false;
  }
  
  private final boolean a(int paramInt)
  {
    int i1 = 1;
    if (paramInt == i1)
    {
      k.v_();
      return i1;
    }
    int i2 = 9;
    int i3 = 2;
    if ((i3 <= paramInt) && (i2 >= paramInt))
    {
      Object localObject = p.a(paramInt);
      if (localObject != null)
      {
        bd localbd = k;
        i3 = 0;
        String str = "dialpad";
        localbd.a((String)localObject, null, false, str);
      }
      else
      {
        localObject = k;
        ((bd)localObject).b_(paramInt);
      }
      return i1;
    }
    return false;
  }
  
  private final void b(char paramChar)
  {
    int i1 = f;
    int i2 = -1;
    String str;
    if (i1 == i2)
    {
      locald = (aj.d)b;
      if (locald != null)
      {
        str = String.valueOf(paramChar);
        locald.a(str);
      }
      return;
    }
    aj.d locald = (aj.d)b;
    if (locald != null)
    {
      i2 = f;
      int i3 = g;
      str = String.valueOf(paramChar);
      locald.a(i2, i3, str);
      return;
    }
  }
  
  private final boolean k()
  {
    CharSequence localCharSequence = (CharSequence)e;
    int i1 = localCharSequence.length();
    return i1 > 0;
  }
  
  private final void l()
  {
    boolean bool1 = d;
    if (!bool1)
    {
      bool1 = true;
      d = bool1;
      aj.d locald = (aj.d)b;
      if (locald != null)
      {
        boolean bool2 = d;
        locald.a(bool2);
      }
      n();
    }
  }
  
  private final void m()
  {
    i.clear();
    aj.d locald = (aj.d)b;
    if (locald != null)
    {
      locald.c("");
      return;
    }
  }
  
  private final void n()
  {
    boolean bool1 = d;
    if (bool1)
    {
      bool1 = k();
      if (bool1)
      {
        localObject1 = BottomBar.DialpadState.NUMBER_ENTERED;
        break label45;
      }
    }
    bool1 = d;
    if (bool1) {
      localObject1 = BottomBar.DialpadState.DIALPAD_UP;
    } else {
      localObject1 = BottomBar.DialpadState.DIALPAD_DOWN;
    }
    label45:
    Object localObject2 = (aj.d)b;
    if (localObject2 != null) {
      ((aj.d)localObject2).a((BottomBar.DialpadState)localObject1);
    }
    Object localObject1 = (aj.d)b;
    if (localObject1 != null)
    {
      boolean bool2 = k();
      if (bool2) {}
      for (localObject2 = aj.a.b.b.a;; localObject2 = aj.a.b.a.a)
      {
        localObject2 = (aj.a.b)localObject2;
        break;
        localObject2 = h;
        if (localObject2 != null)
        {
          aj.a.b.c localc = new com/truecaller/calling/dialer/aj$a$b$c;
          localc.<init>((String)localObject2);
          localObject2 = localc;
          localObject2 = (aj.a.b)localc;
          break;
        }
      }
      ((aj.d)localObject1).a((aj.a.b)localObject2);
      return;
    }
  }
  
  public final void a()
  {
    int i1 = f;
    int i2 = -1;
    Object localObject;
    int i3;
    if (i1 == i2)
    {
      localObject = e;
      i1 = ((String)localObject).length();
      if (i1 > 0)
      {
        aj.d locald = (aj.d)b;
        if (locald != null)
        {
          i3 = i1 + -1;
          locald.a(i3, i1);
        }
      }
    }
    else
    {
      i2 = g;
      if (i2 <= i1)
      {
        if (i1 != 0)
        {
          localObject = (aj.d)b;
          if (localObject != null)
          {
            i2 = f;
            i3 = i2 + -1;
            ((aj.d)localObject).a(i3, i2);
          }
        }
      }
      else
      {
        localObject = (aj.d)b;
        if (localObject != null)
        {
          i2 = f;
          i3 = g;
          ((aj.d)localObject).a(i2, i3);
          return;
        }
      }
    }
  }
  
  public final void a(char paramChar, DialerKeypad.KeyActionState paramKeyActionState)
  {
    k.b(paramKeyActionState, "keyState");
    Object localObject = al.a;
    int i1 = paramKeyActionState.ordinal();
    i1 = localObject[i1];
    Character localCharacter;
    switch (i1)
    {
    default: 
      break;
    case 3: 
      paramKeyActionState = i;
      localCharacter = Character.valueOf(paramChar);
      paramKeyActionState.remove(localCharacter);
      break;
    case 2: 
      paramKeyActionState = i;
      localObject = Character.valueOf(paramChar);
      boolean bool = paramKeyActionState.remove(localObject);
      if (bool)
      {
        b(paramChar);
        return;
      }
      break;
    case 1: 
      paramKeyActionState = i;
      localCharacter = Character.valueOf(paramChar);
      paramKeyActionState.add(localCharacter);
      return;
    }
  }
  
  public final void a(ap paramap)
  {
    k.b(paramap, "editable");
    String str = paramap.a();
    Object localObject = e;
    boolean bool = k.a(localObject, str);
    if (bool) {
      return;
    }
    e = str;
    bool = false;
    h = null;
    n();
    localObject = m.a(str);
    if (localObject != null)
    {
      paramap.b();
      paramap = (aj.d)b;
      if (paramap != null) {
        paramap.a((com.truecaller.util.ca)localObject);
      }
      return;
    }
    q.c(str);
  }
  
  public final void a(String paramString)
  {
    bd localbd = k;
    CompositeAdapterDelegate.SearchResultOrder localSearchResultOrder = CompositeAdapterDelegate.SearchResultOrder.ORDER_TCM;
    localbd.a(paramString, null, true, localSearchResultOrder);
  }
  
  public final boolean a(int paramInt1, int paramInt2)
  {
    boolean bool = true;
    switch (paramInt1)
    {
    case -4715: 
    case -4713: 
    default: 
      bool = false;
      break;
    case -4712: 
      bool = a(paramInt2);
      break;
    case -4714: 
      paramInt1 = 43;
      b(paramInt1);
      break;
    case -4716: 
      paramInt1 = 59;
      paramInt2 = a(paramInt1);
      if (paramInt2 != 0) {
        b(paramInt1);
      }
      break;
    case -4717: 
      paramInt1 = 44;
      paramInt2 = a(paramInt1);
      if (paramInt2 != 0) {
        b(paramInt1);
      }
      break;
    }
    if (bool)
    {
      HashSet localHashSet = i;
      localHashSet.clear();
    }
    return bool;
  }
  
  public final void b()
  {
    boolean bool1 = d;
    if (bool1)
    {
      bool1 = false;
      d = false;
      aj.d locald = (aj.d)b;
      if (locald != null)
      {
        boolean bool2 = d;
        locald.a(bool2);
      }
      n();
    }
  }
  
  public final void b(int paramInt1, int paramInt2)
  {
    f = paramInt1;
    g = paramInt2;
  }
  
  public final void b(String paramString)
  {
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    if (localObject != null)
    {
      bool = m.a((CharSequence)localObject);
      if (!bool)
      {
        bool = false;
        localObject = null;
        break label32;
      }
    }
    boolean bool = true;
    label32:
    if (bool) {
      paramString = null;
    }
    h = paramString;
    n();
  }
  
  public final void c()
  {
    Object localObject1 = (aj.d)b;
    if (localObject1 != null) {
      ((aj.d)localObject1).b();
    }
    localObject1 = (aj.d)b;
    if (localObject1 != null) {
      ((aj.d)localObject1).d();
    }
    localObject1 = o;
    Object localObject2 = "lastCopied";
    localObject1 = ((com.truecaller.i.c)localObject1).a((String)localObject2);
    if (localObject1 != null)
    {
      localObject2 = localObject1;
      localObject2 = (CharSequence)localObject1;
      int i1 = ((CharSequence)localObject2).length();
      int i2 = 1;
      if (i1 > 0)
      {
        i1 = 1;
      }
      else
      {
        i1 = 0;
        localObject2 = null;
      }
      if (i1 == 0) {
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        localObject2 = am.a();
        int i3 = localObject2.length;
        int i4 = 0;
        String str;
        while (i4 < i3)
        {
          Object localObject3 = localObject2[i4];
          aj.c localc = (aj.c)c;
          if (localc != null)
          {
            bool2 = localc.b((String)localObject3);
            localObject3 = Boolean.valueOf(bool2);
          }
          else
          {
            bool2 = false;
            localObject3 = null;
          }
          boolean bool2 = com.truecaller.utils.extensions.c.a((Boolean)localObject3);
          if (bool2)
          {
            i2 = 0;
            str = null;
            break;
          }
          i4 += 1;
        }
        if (i2 == 0) {
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          localObject2 = o;
          str = "lastCopiedFromTc";
          boolean bool1 = ((com.truecaller.i.c)localObject2).b(str);
          if (bool1)
          {
            b((String)localObject1);
          }
          else
          {
            localObject2 = (aj.d)b;
            if (localObject2 != null)
            {
              str = "number";
              k.a(localObject1, str);
              ((aj.d)localObject2).d((String)localObject1);
            }
          }
          o.d("lastCopied");
          o.d("lastCopiedFromTc");
          return;
        }
      }
    }
  }
  
  public final void c(String paramString)
  {
    aj.d locald = (aj.d)b;
    if (locald != null)
    {
      if (paramString == null) {
        paramString = "";
      }
      locald.c(paramString);
    }
    l();
  }
  
  public final void d()
  {
    m();
  }
  
  public final void e()
  {
    aj.d locald = (aj.d)b;
    if (locald != null)
    {
      locald.c();
      return;
    }
  }
  
  public final boolean f()
  {
    boolean bool1 = d;
    boolean bool2 = true;
    if (bool1)
    {
      b();
      return bool2;
    }
    bool1 = k();
    if (bool1)
    {
      m();
      n();
      return bool2;
    }
    return false;
  }
  
  public final void g()
  {
    boolean bool1 = d;
    int i2 = 1;
    if (bool1)
    {
      bool1 = k();
      if (bool1)
      {
        localObject = e;
        int i1 = ((String)localObject).length();
        if (i1 == i2)
        {
          i1 = 57;
          i2 = 49;
          str1 = e;
          i3 = str1.charAt(0);
          if ((i2 <= i3) && (i1 >= i3))
          {
            localObject = Integer.valueOf(String.valueOf(e.charAt(0)));
            str2 = "Integer.valueOf(\"${currentText[0]}\")";
            k.a(localObject, str2);
            i1 = ((Integer)localObject).intValue();
            a(i1);
            break label152;
          }
        }
        localObject = k;
        String str2 = e;
        int i3 = 0;
        String str1 = null;
        String str3 = "dialpad";
        ((bd)localObject).a(str2, null, false, str3);
        localObject = "call";
        a(this, (String)localObject);
        label152:
        m();
        return;
      }
    }
    boolean bool2 = d ^ i2;
    d = bool2;
    Object localObject = (aj.d)b;
    if (localObject != null)
    {
      boolean bool3 = d;
      ((aj.d)localObject).a(bool3);
    }
    n();
  }
  
  public final void h()
  {
    String str1 = n.d();
    if (str1 != null)
    {
      aj.d locald = (aj.d)b;
      if (locald != null)
      {
        String str2 = "it";
        k.a(str1, str2);
        locald.c(str1);
      }
    }
    l();
    a("callLog", "menu", "paste");
  }
  
  public final void i()
  {
    String str = h;
    if (str != null)
    {
      c(str);
      return;
    }
  }
  
  public final void j()
  {
    Object localObject1 = j;
    Object localObject2 = this;
    localObject2 = (ca)this;
    Object localObject3 = a[0];
    localObject1 = (bz)ab;
    boolean bool1 = localObject1 instanceof bz.c;
    int i2 = 1;
    Object localObject4 = null;
    if (bool1)
    {
      localObject1 = (bz.c)localObject1;
      localObject4 = a;
    }
    else
    {
      bool1 = localObject1 instanceof bz.a;
      if (bool1)
      {
        localObject1 = a;
        int i1 = ((List)localObject1).size();
        if (i1 == i2)
        {
          i1 = 1;
        }
        else
        {
          i1 = 0;
          localObject2 = null;
        }
        if (i1 == 0) {
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          localObject1 = (av)((List)localObject1).get(0);
          if (localObject1 != null)
          {
            localObject1 = a;
            if (localObject1 != null)
            {
              boolean bool2 = ((Contact)localObject1).Z();
              if (!bool2) {
                localObject4 = localObject1;
              }
            }
          }
        }
      }
    }
    if (localObject4 == null)
    {
      localObject4 = new com/truecaller/data/entity/Contact;
      ((Contact)localObject4).<init>();
      localObject1 = s;
      localObject2 = new String[i2];
      localObject3 = e;
      localObject2[0] = localObject3;
      localObject1 = ((com.truecaller.data.entity.g)localObject1).b((String[])localObject2);
      ((Contact)localObject4).a((Number)localObject1);
    }
    localObject1 = (aj.c)c;
    if (localObject1 != null) {
      ((aj.c)localObject1).a((Contact)localObject4);
    }
    a(this, "save");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */