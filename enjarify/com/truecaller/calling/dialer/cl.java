package com.truecaller.calling.dialer;

import android.view.View;
import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import com.truecaller.adapter_delegates.h;
import com.truecaller.common.h.aj;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.utils.n;
import java.util.List;

public final class cl
  extends com.truecaller.adapter_delegates.c
  implements ck.b
{
  private final ck.a c;
  private final cb d;
  private final com.truecaller.search.local.model.c e;
  private final n f;
  private final com.truecaller.network.search.e g;
  private final cf.b.a h;
  private final ax i;
  private final com.truecaller.data.entity.g j;
  private final aj k;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(cl.class);
    ((u)localObject).<init>(localb, "suggestedContacts", "getSuggestedContacts()Ljava/util/List;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public cl(ck.a parama, com.truecaller.search.local.model.c paramc, n paramn, com.truecaller.network.search.e parame, cf.b.a parama1, ax paramax, com.truecaller.data.entity.g paramg, aj paramaj)
  {
    e = paramc;
    f = paramn;
    g = parame;
    h = parama1;
    i = paramax;
    j = paramg;
    k = paramaj;
    c = parama;
    paramc = this;
    paramc = (ck.b)this;
    parama = parama.a(paramc);
    d = parama;
  }
  
  private static String a(Contact paramContact, Number paramNumber, String paramString, n paramn, aj paramaj)
  {
    if (paramContact != null)
    {
      paramContact = paramContact.t();
    }
    else
    {
      bool = false;
      paramContact = null;
    }
    int m = 1;
    if (paramContact != null)
    {
      Object localObject = paramContact;
      localObject = (CharSequence)paramContact;
      int n = ((CharSequence)localObject).length();
      if (n > 0)
      {
        n = 1;
      }
      else
      {
        n = 0;
        localObject = null;
      }
      if (n != 0) {
        return paramContact;
      }
    }
    paramContact = new String[m];
    paramContact[0] = paramString;
    boolean bool = paramaj.a(paramContact);
    if (bool)
    {
      paramNumber = new Object[0];
      paramContact = paramn.a(2131888906, paramNumber);
      k.a(paramContact, "resourceProvider.getStri…(R.string.text_voicemail)");
      return paramContact;
    }
    bool = paramaj.a(paramString);
    if (bool)
    {
      paramContact = paramaj.a();
      if (paramContact == null) {
        return paramString;
      }
    }
    else
    {
      paramContact = paramNumber.n();
      if (paramContact == null) {
        paramContact = paramString;
      }
    }
    return paramContact;
  }
  
  private final List a()
  {
    ck.a locala = c;
    Object localObject = this;
    localObject = (ck.b)this;
    c.l.g localg = b[0];
    return locala.a((ck.b)localObject, localg);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject1 = a;
    int m = ((String)localObject1).hashCode();
    int n = -1743572928;
    boolean bool1 = true;
    Object localObject2;
    boolean bool2;
    int i1;
    Object localObject3;
    if (m != n)
    {
      n = -1314591573;
      if (m == n)
      {
        localObject2 = "ItemEvent.LONG_CLICKED";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2)
        {
          localObject1 = d;
          i1 = b;
          paramh = (com.truecaller.calling.dialer.suggested_contacts.e)a().get(i1);
          localObject2 = b;
          localObject3 = j;
          localObject3 = paramh.a((com.truecaller.data.entity.g)localObject3);
          String str = a;
          n localn = f;
          aj localaj = k;
          localObject2 = a((Contact)localObject2, (Number)localObject3, str, localn, localaj);
          h.a((View)localObject1, paramh, (String)localObject2);
          return bool1;
        }
      }
    }
    else
    {
      localObject2 = "ItemEvent.CLICKED";
      bool2 = ((String)localObject1).equals(localObject2);
      if (bool2)
      {
        i1 = b;
        localObject1 = (com.truecaller.calling.dialer.suggested_contacts.e)a().get(i1);
        localObject2 = h;
        localObject3 = a;
        localObject1 = b;
        if (localObject1 != null)
        {
          localObject1 = ((Contact)localObject1).s();
        }
        else
        {
          bool2 = false;
          localObject1 = null;
        }
        ((cf.b.a)localObject2).a((String)localObject3, (String)localObject1, i1);
        return bool1;
      }
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    return ageta.hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */