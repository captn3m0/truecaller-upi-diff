package com.truecaller.calling.dialer;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.truecaller.R.id;
import com.truecaller.ui.view.TintedImageView;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class bh
  extends RecyclerView.ViewHolder
  implements be.b, a
{
  final View a;
  private HashMap b;
  
  public bh(View paramView, be.b.a parama)
  {
    super(paramView);
    a = paramView;
    int i = R.id.promoContainer;
    paramView = (CardView)c(i);
    Object localObject = new com/truecaller/calling/dialer/bh$1;
    ((bh.1)localObject).<init>(this, parama);
    localObject = (View.OnClickListener)localObject;
    paramView.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.close;
    paramView = (TintedImageView)c(i);
    localObject = new com/truecaller/calling/dialer/bh$2;
    ((bh.2)localObject).<init>(parama);
    localObject = (View.OnClickListener)localObject;
    paramView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  private View c(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final View a()
  {
    return a;
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.promoView;
    ((TextView)c(i)).setText(paramInt);
  }
  
  public final void b(int paramInt)
  {
    int i = R.id.promoView;
    ((TextView)c(i)).setCompoundDrawablesWithIntrinsicBounds(paramInt, 0, 0, 0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bh
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */