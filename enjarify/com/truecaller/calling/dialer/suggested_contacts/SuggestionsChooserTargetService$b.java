package com.truecaller.calling.dialer.suggested_contacts;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.cn;

final class SuggestionsChooserTargetService$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  SuggestionsChooserTargetService$b(SuggestionsChooserTargetService paramSuggestionsChooserTargetService, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/calling/dialer/suggested_contacts/SuggestionsChooserTargetService$b;
    SuggestionsChooserTargetService localSuggestionsChooserTargetService = b;
    localb.<init>(localSuggestionsChooserTargetService, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label121;
      }
      long l = 2000L;
      paramObject = new com/truecaller/calling/dialer/suggested_contacts/SuggestionsChooserTargetService$b$1;
      ((SuggestionsChooserTargetService.b.1)paramObject).<init>(this, null);
      paramObject = (m)paramObject;
      int j = 1;
      a = j;
      paramObject = cn.a(l, (m)paramObject, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return paramObject;
    label121:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.SuggestionsChooserTargetService.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */