package com.truecaller.calling.dialer.suggested_contacts;

import c.a.ag;
import c.g.b.k;
import c.n;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class c
  implements f
{
  private final String a;
  private final com.truecaller.i.c b;
  private final g c;
  private final com.truecaller.data.access.c d;
  
  public c(com.truecaller.i.c paramc, g paramg, com.truecaller.data.access.c paramc1)
  {
    b = paramc;
    c = paramg;
    d = paramc1;
    a = "_";
  }
  
  private static String a(String paramString)
  {
    paramString = (CharSequence)paramString;
    Object localObject1 = new java/util/ArrayList;
    int i = paramString.length();
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    i = 0;
    Object localObject2 = null;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      boolean bool = Character.isDigit(j);
      char c1;
      if (bool) {
        c1 = '0';
      }
      localObject3 = Character.valueOf(c1);
      ((Collection)localObject1).add(localObject3);
      i += 1;
    }
    localObject1 = (List)localObject1;
    localObject2 = localObject1;
    localObject2 = (Iterable)localObject1;
    Object localObject3 = (CharSequence)"";
    return c.a.m.a((Iterable)localObject2, (CharSequence)localObject3, null, null, 0, null, null, 62);
  }
  
  private final void a(List paramList)
  {
    com.truecaller.i.c localc = b;
    String str1 = "pinnedSuggestions";
    if (paramList != null)
    {
      paramList = (Iterable)paramList;
      Object localObject1 = new java/util/ArrayList;
      int i = c.a.m.a(paramList, 10);
      ((ArrayList)localObject1).<init>(i);
      localObject1 = (Collection)localObject1;
      i = 0;
      String str2 = null;
      paramList = paramList.iterator();
      for (;;)
      {
        boolean bool = paramList.hasNext();
        if (!bool) {
          break;
        }
        Object localObject2 = paramList.next();
        int j = i + 1;
        if (i < 0) {
          c.a.m.a();
        }
        localObject2 = (String)localObject2;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(i);
        str2 = a;
        localStringBuilder.append(str2);
        localStringBuilder.append((String)localObject2);
        str2 = localStringBuilder.toString();
        ((Collection)localObject1).add(str2);
        i = j;
      }
      localObject1 = (Iterable)localObject1;
      paramList = c.a.m.i((Iterable)localObject1);
    }
    else
    {
      paramList = null;
    }
    localc.a(str1, paramList);
  }
  
  private final void a(Set paramSet)
  {
    b.a("hiddenSuggestions", paramSet);
  }
  
  private final Set c()
  {
    Set localSet = b.f("hiddenSuggestions");
    k.a(localSet, "callingSettings.getStrin…tings.HIDDEN_SUGGESTIONS)");
    return localSet;
  }
  
  private final List d()
  {
    Object localObject1 = b.f("pinnedSuggestions");
    k.a(localObject1, "callingSettings.getStrin…tings.PINNED_SUGGESTIONS)");
    localObject1 = (Iterable)localObject1;
    Object localObject2 = new java/util/ArrayList;
    int i = 10;
    int j = c.a.m.a((Iterable)localObject1, i);
    ((ArrayList)localObject2).<init>(j);
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool2;
    int k;
    int m;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    for (;;)
    {
      bool2 = ((Iterator)localObject1).hasNext();
      k = 2;
      m = 1;
      if (!bool2) {
        break;
      }
      localObject3 = (String)((Iterator)localObject1).next();
      k.a(localObject3, "it");
      localObject3 = (CharSequence)localObject3;
      localObject4 = new String[m];
      localObject5 = a;
      localObject4[0] = localObject5;
      localObject3 = c.n.m.b((CharSequence)localObject3, (String[])localObject4, m, k);
      ((Collection)localObject2).add(localObject3);
    }
    localObject2 = (Iterable)localObject2;
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (List)((Iterator)localObject2).next();
      int n = ((List)localObject3).size();
      localObject5 = null;
      Object localObject6;
      if (n == k)
      {
        try
        {
          localObject4 = new c/n;
          localObject6 = ((List)localObject3).get(0);
          localObject6 = (String)localObject6;
          int i1 = Integer.parseInt((String)localObject6);
          localObject6 = Integer.valueOf(i1);
          Object localObject7 = ((List)localObject3).get(m);
          ((n)localObject4).<init>(localObject6, localObject7);
          localObject5 = localObject4;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          localObject4 = new java/lang/StringBuilder;
          localObject6 = "Cannot parse prefix ";
          ((StringBuilder)localObject4).<init>((String)localObject6);
          localObject3 = (String)((List)localObject3).get(0);
          ((StringBuilder)localObject4).append((String)localObject3);
          localObject3 = ((StringBuilder)localObject4).toString();
          AssertionUtil.reportWeirdnessButNeverCrash((String)localObject3);
        }
      }
      else
      {
        localObject4 = new java/lang/StringBuilder;
        localObject6 = "Cannot proceed prefixed string ";
        ((StringBuilder)localObject4).<init>((String)localObject6);
        localObject3 = a((String)((List)localObject3).get(0));
        ((StringBuilder)localObject4).append((String)localObject3);
        localObject3 = ((StringBuilder)localObject4).toString();
        AssertionUtil.reportWeirdnessButNeverCrash((String)localObject3);
      }
      if (localObject5 != null) {
        ((Collection)localObject1).add(localObject5);
      }
    }
    localObject1 = (Iterable)localObject1;
    localObject2 = new com/truecaller/calling/dialer/suggested_contacts/c$a;
    ((c.a)localObject2).<init>();
    localObject2 = (Comparator)localObject2;
    localObject1 = (Iterable)c.a.m.a((Iterable)localObject1, (Comparator)localObject2);
    localObject2 = new java/util/ArrayList;
    i = c.a.m.a((Iterable)localObject1, i);
    ((ArrayList)localObject2).<init>(i);
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      String str = (String)nextb;
      ((Collection)localObject2).add(str);
    }
    return (List)localObject2;
  }
  
  public final List a(int paramInt)
  {
    Object localObject1 = (Iterable)c();
    Object localObject2 = new java/util/ArrayList;
    int i = 10;
    int j = c.a.m.a((Iterable)localObject1, i);
    ((ArrayList)localObject2).<init>(j);
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (String)((Iterator)localObject1).next();
      localObject4 = new com/truecaller/calling/dialer/suggested_contacts/e;
      k.a(localObject3, "it");
      localObject5 = null;
      ((e)localObject4).<init>((String)localObject3, null, false);
      ((Collection)localObject2).add(localObject4);
    }
    localObject2 = (List)localObject2;
    localObject1 = c;
    int k;
    if (paramInt > 0)
    {
      k = ((List)localObject2).size() + paramInt;
    }
    else
    {
      k = 0;
      localObject3 = null;
    }
    localObject1 = (Iterable)((g)localObject1).a(k);
    Object localObject3 = ag.a((Iterable)localObject1);
    Object localObject4 = (Iterable)d();
    Object localObject5 = new java/util/ArrayList;
    int m = c.a.m.a((Iterable)localObject4, i);
    ((ArrayList)localObject5).<init>(m);
    localObject5 = (Collection)localObject5;
    localObject4 = ((Iterable)localObject4).iterator();
    Object localObject6;
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject4).hasNext();
      if (!bool3) {
        break;
      }
      localObject6 = (String)((Iterator)localObject4).next();
      e locale = new com/truecaller/calling/dialer/suggested_contacts/e;
      Contact localContact = (Contact)((Map)localObject3).get(localObject6);
      boolean bool4 = true;
      locale.<init>((String)localObject6, localContact, bool4);
      ((Collection)localObject5).add(locale);
    }
    localObject5 = (List)localObject5;
    localObject3 = new java/util/ArrayList;
    i = c.a.m.a((Iterable)localObject1, i);
    ((ArrayList)localObject3).<init>(i);
    localObject3 = (Collection)localObject3;
    localObject1 = ((Iterable)localObject1).iterator();
    Object localObject7;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject7 = (n)((Iterator)localObject1).next();
      localObject4 = (String)a;
      localObject7 = (Contact)b;
      localObject6 = new com/truecaller/calling/dialer/suggested_contacts/e;
      ((e)localObject6).<init>((String)localObject4, (Contact)localObject7, false);
      ((Collection)localObject3).add(localObject6);
    }
    localObject3 = (List)localObject3;
    localObject5 = (Collection)localObject5;
    localObject3 = (Iterable)localObject3;
    localObject1 = (Iterable)c.a.m.c((Collection)localObject5, (Iterable)localObject3);
    localObject2 = (Iterable)localObject2;
    localObject1 = c.a.m.k((Iterable)c.a.m.c((Iterable)localObject1, (Iterable)localObject2));
    if (paramInt > 0) {
      localObject1 = c.a.m.d((Iterable)localObject1, paramInt);
    }
    Object localObject8 = localObject1;
    localObject8 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool5 = ((Iterator)localObject8).hasNext();
      if (!bool5) {
        break;
      }
      localObject2 = (e)((Iterator)localObject8).next();
      localObject7 = b;
      if (localObject7 == null)
      {
        localObject7 = d;
        localObject3 = a;
        localObject7 = ((com.truecaller.data.access.c)localObject7).b((String)localObject3);
      }
      b = ((Contact)localObject7);
    }
    return (List)localObject1;
  }
  
  public final void a()
  {
    a(null);
  }
  
  public final void a(e parame)
  {
    k.b(parame, "suggestedContact");
    Object localObject1 = c();
    parame = a;
    k.b(localObject1, "receiver$0");
    Object localObject2 = new java/util/LinkedHashSet;
    int i = ag.a(((Set)localObject1).size() + 1);
    ((LinkedHashSet)localObject2).<init>(i);
    localObject1 = (Collection)localObject1;
    ((LinkedHashSet)localObject2).addAll((Collection)localObject1);
    ((LinkedHashSet)localObject2).add(parame);
    localObject2 = (Set)localObject2;
    a((Set)localObject2);
  }
  
  public final void b(e parame)
  {
    k.b(parame, "suggestedContact");
    Object localObject1 = c();
    parame = a;
    k.b(localObject1, "receiver$0");
    Object localObject2 = new java/util/LinkedHashSet;
    int i = ag.a(((Set)localObject1).size());
    ((LinkedHashSet)localObject2).<init>(i);
    localObject1 = ((Iterable)localObject1).iterator();
    i = 0;
    int j = 0;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject3 = ((Iterator)localObject1).next();
      int k = 1;
      Object localObject4;
      if (j == 0)
      {
        boolean bool2 = k.a(localObject3, parame);
        if (bool2)
        {
          j = 1;
          k = 0;
          localObject4 = null;
        }
      }
      if (k != 0)
      {
        localObject4 = localObject2;
        localObject4 = (Collection)localObject2;
        ((Collection)localObject4).add(localObject3);
      }
    }
    localObject2 = (Set)localObject2;
    a((Set)localObject2);
  }
  
  public final boolean b()
  {
    Collection localCollection = (Collection)c();
    boolean bool = localCollection.isEmpty();
    return !bool;
  }
  
  public final void c(e parame)
  {
    k.b(parame, "suggestedContact");
    Collection localCollection = (Collection)d();
    parame = a;
    parame = c.a.m.k((Iterable)c.a.m.a(localCollection, parame));
    a(parame);
  }
  
  public final void d(e parame)
  {
    k.b(parame, "suggestedContact");
    Object localObject1 = (Iterable)d();
    parame = a;
    k.b(localObject1, "receiver$0");
    Object localObject2 = new java/util/ArrayList;
    int i = c.a.m.a((Iterable)localObject1, 10);
    ((ArrayList)localObject2).<init>(i);
    localObject1 = ((Iterable)localObject1).iterator();
    i = 0;
    int j = 0;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject3 = ((Iterator)localObject1).next();
      int k = 1;
      Object localObject4;
      if (j == 0)
      {
        boolean bool2 = k.a(localObject3, parame);
        if (bool2)
        {
          j = 1;
          k = 0;
          localObject4 = null;
        }
      }
      if (k != 0)
      {
        localObject4 = localObject2;
        localObject4 = (Collection)localObject2;
        ((Collection)localObject4).add(localObject3);
      }
    }
    localObject2 = (List)localObject2;
    a((List)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */