package com.truecaller.calling.dialer.suggested_contacts;

import android.content.ComponentName;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.service.chooser.ChooserTarget;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.data.entity.Contact;
import com.truecaller.messaging.sharing.SharingActivity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class SuggestionsChooserTargetService$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  SuggestionsChooserTargetService$a(SuggestionsChooserTargetService paramSuggestionsChooserTargetService, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/calling/dialer/suggested_contacts/SuggestionsChooserTargetService$a;
    SuggestionsChooserTargetService localSuggestionsChooserTargetService = b;
    locala.<init>(localSuggestionsChooserTargetService, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = b.c;
        if (paramObject == null)
        {
          localObject1 = "suggestedContactsManager";
          c.g.b.k.a((String)localObject1);
        }
        int j = 4;
        paramObject = ((f)paramObject).a(j);
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        SuggestionsChooserTargetService localSuggestionsChooserTargetService = b;
        paramObject = ((List)paramObject).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)paramObject).hasNext();
          if (!bool2) {
            break;
          }
          Object localObject2 = (e)((Iterator)paramObject).next();
          Object localObject3 = b;
          Class localClass = null;
          String str;
          float f2;
          if (localObject3 != null)
          {
            str = ((Contact)localObject3).u();
            boolean bool3 = true;
            f1 = Float.MIN_VALUE;
            localObject3 = ((Contact)localObject3).a(bool3);
            if (localObject3 != null)
            {
              localObject4 = localSuggestionsChooserTargetService;
              try
              {
                localObject4 = (Context)localSuggestionsChooserTargetService;
                localObject4 = w.a((Context)localObject4);
                localObject3 = ((w)localObject4).a((Uri)localObject3);
                localObject4 = aq.d.b();
                localObject4 = (ai)localObject4;
                localObject3 = ((ab)localObject3).a((ai)localObject4);
                int k = 2131166217;
                f1 = 1.7946673E38F;
                localObject3 = ((ab)localObject3).a(k, k);
                localObject3 = ((ab)localObject3).d();
                if (localObject3 != null) {
                  localObject3 = Icon.createWithBitmap((Bitmap)localObject3);
                }
              }
              catch (IOException localIOException) {}
            }
            bool5 = false;
            localObject3 = null;
            f2 = 0.0F;
          }
          else
          {
            bool5 = false;
            localObject3 = null;
            f2 = 0.0F;
            str = null;
          }
          Object localObject4 = str;
          localObject4 = (CharSequence)str;
          boolean bool4 = org.c.a.a.a.k.b((CharSequence)localObject4);
          if (bool4) {
            str = a;
          }
          Object localObject5;
          if (localObject3 == null)
          {
            localObject3 = localSuggestionsChooserTargetService;
            localObject3 = (Context)localSuggestionsChooserTargetService;
            m = 2131233849;
            f1 = 1.8083847E38F;
            localObject3 = Icon.createWithResource((Context)localObject3, m);
            localObject5 = localObject3;
          }
          else
          {
            localObject5 = localObject3;
          }
          boolean bool5 = c;
          int m = 1065353216;
          float f1 = 1.0F;
          float f4;
          if (!bool5)
          {
            int n = ((ArrayList)localObject1).size();
            f2 = n;
            float f3 = 4.0F;
            f2 /= f3;
            f1 -= f2;
            f4 = f1;
          }
          else
          {
            f4 = 1.0F;
          }
          Bundle localBundle = new android/os/Bundle;
          localBundle.<init>();
          localObject2 = a;
          localObject2 = (Parcelable)Uri.fromParts("smsto", (String)localObject2, null);
          localBundle.putParcelable("com.truecaller.suggestions.extra.PHONE_NUMBER", (Parcelable)localObject2);
          localObject2 = new android/service/chooser/ChooserTarget;
          Object localObject6 = str;
          localObject6 = (CharSequence)str;
          ComponentName localComponentName = new android/content/ComponentName;
          localObject3 = localSuggestionsChooserTargetService;
          localObject3 = (Context)localSuggestionsChooserTargetService;
          localClass = SharingActivity.class;
          localComponentName.<init>((Context)localObject3, localClass);
          localObject4 = localObject2;
          ((ChooserTarget)localObject2).<init>((CharSequence)localObject6, (Icon)localObject5, f4, localComponentName, localBundle);
          ((ArrayList)localObject1).add(localObject2);
        }
        return localObject1;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.SuggestionsChooserTargetService.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */