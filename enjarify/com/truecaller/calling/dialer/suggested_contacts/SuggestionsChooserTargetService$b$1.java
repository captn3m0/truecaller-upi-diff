package com.truecaller.calling.dialer.suggested_contacts;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

final class SuggestionsChooserTargetService$b$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  SuggestionsChooserTargetService$b$1(SuggestionsChooserTargetService.b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/calling/dialer/suggested_contacts/SuggestionsChooserTargetService$b$1;
    SuggestionsChooserTargetService.b localb = b;
    local1.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label110;
      }
      paramObject = SuggestionsChooserTargetService.a(b.b);
      j = 1;
      a = j;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return paramObject;
    label110:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.SuggestionsChooserTargetService.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */