package com.truecaller.calling.dialer.suggested_contacts;

import android.content.ComponentName;
import android.content.IntentFilter;
import android.service.chooser.ChooserTargetService;
import c.a.y;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.bs;

public final class SuggestionsChooserTargetService
  extends ChooserTargetService
  implements ag
{
  public c.d.f a;
  public c.d.f b;
  public f c;
  private final bn d;
  
  public SuggestionsChooserTargetService()
  {
    bn localbn = bs.a(null);
    d = localbn;
  }
  
  public final c.d.f V_()
  {
    c.d.f localf = a;
    if (localf == null)
    {
      localObject = "uiContext";
      k.a((String)localObject);
    }
    Object localObject = (c.d.f)d;
    return localf.plus((c.d.f)localObject);
  }
  
  public final void onCreate()
  {
    super.onCreate();
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final void onDestroy()
  {
    d.n();
    super.onDestroy();
  }
  
  public final List onGetChooserTargets(ComponentName paramComponentName, IntentFilter paramIntentFilter)
  {
    String str = "targetActivityName";
    k.b(paramComponentName, str);
    paramComponentName = "matchedFilter";
    k.b(paramIntentFilter, paramComponentName);
    try
    {
      paramComponentName = new com/truecaller/calling/dialer/suggested_contacts/SuggestionsChooserTargetService$b;
      paramIntentFilter = null;
      paramComponentName.<init>(this, null);
      paramComponentName = (m)paramComponentName;
      paramComponentName = kotlinx.coroutines.f.a(paramComponentName);
      paramComponentName = (ArrayList)paramComponentName;
      if (paramComponentName != null)
      {
        paramComponentName = (List)paramComponentName;
      }
      else
      {
        paramComponentName = y.a;
        paramComponentName = (List)paramComponentName;
      }
    }
    catch (CancellationException localCancellationException)
    {
      paramComponentName = (List)y.a;
    }
    return paramComponentName;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.SuggestionsChooserTargetService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */