package com.truecaller.calling.dialer.suggested_contacts;

import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import java.util.Iterator;

public final class e
{
  public final String a;
  public Contact b;
  public final boolean c;
  
  public e(String paramString, Contact paramContact, boolean paramBoolean)
  {
    a = paramString;
    b = paramContact;
    c = paramBoolean;
  }
  
  public final Number a(g paramg)
  {
    k.b(paramg, "numberProvider");
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = ((Contact)localObject1).A();
      if (localObject1 != null)
      {
        localObject1 = ((Iterable)localObject1).iterator();
        boolean bool2;
        do
        {
          bool1 = ((Iterator)localObject1).hasNext();
          if (!bool1) {
            break;
          }
          localObject2 = ((Iterator)localObject1).next();
          localObject3 = localObject2;
          localObject3 = (Number)localObject2;
          k.a(localObject3, "it");
          localObject3 = ((Number)localObject3).a();
          String str = a;
          bool2 = k.a(localObject3, str);
        } while (!bool2);
        break label106;
        bool1 = false;
        localObject2 = null;
        label106:
        localObject2 = (Number)localObject2;
        if (localObject2 != null) {
          break label158;
        }
      }
    }
    int i = 1;
    localObject1 = new String[i];
    boolean bool1 = false;
    Object localObject3 = a;
    localObject1[0] = localObject3;
    Object localObject2 = paramg.b((String[])localObject1);
    paramg = "numberProvider.provideFr…berSafe(normalizedNumber)";
    k.a(localObject2, paramg);
    label158:
    return (Number)localObject2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof e;
    if (bool1)
    {
      String str = a;
      paramObject = a;
      boolean bool2 = k.a(str, paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SuggestedContact(normalizedNumber=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", contact=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", isPinned=");
    boolean bool = c;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */