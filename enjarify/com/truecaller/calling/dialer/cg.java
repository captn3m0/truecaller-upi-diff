package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.adapter_delegates.c;
import java.util.List;
import java.util.Set;

public final class cg
  extends c
  implements cf.a
{
  private cf.b b;
  
  private static void a(cf.b paramb)
  {
    int i = paramb.a();
    if (i <= 0) {
      i = 1;
    } else {
      i = 0;
    }
    paramb.a(i);
  }
  
  public final void a()
  {
    cf.b localb = b;
    if (localb != null)
    {
      a(localb);
      return;
    }
  }
  
  public final void a(List paramList1, List paramList2)
  {
    k.b(paramList1, "oldItems");
    k.b(paramList2, "newItems");
    cf.b localb = b;
    if (localb != null)
    {
      localb.a(paramList1, paramList2);
      return;
    }
  }
  
  public final void a(Set paramSet)
  {
    k.b(paramSet, "itemPositions");
    cf.b localb = b;
    if (localb != null)
    {
      localb.a(paramSet);
      return;
    }
  }
  
  public final int getItemCount()
  {
    return 1;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */