package com.truecaller.calling.dialer;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.calling.dialer.a.a;
import java.util.List;

public final class co
  extends c
  implements cn.b
{
  private final cn.a c;
  private final cf.b.b d;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(co.class);
    ((u)localObject).<init>(localb, "suggestedPremiumList", "getSuggestedPremiumList()Ljava/util/List;");
    localObject = (g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public co(cn.a parama, cf.b.b paramb)
  {
    d = paramb;
    c = parama;
  }
  
  private final List a()
  {
    cn.a locala = c;
    Object localObject = this;
    localObject = (cn.b)this;
    g localg = b[0];
    return locala.a((cn.b)localObject, localg);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject = a;
    int i = ((String)localObject).hashCode();
    int j = -1743572928;
    boolean bool1 = true;
    String str;
    boolean bool2;
    if (i != j)
    {
      j = -1314591573;
      if (i == j)
      {
        str = "ItemEvent.LONG_CLICKED";
        bool2 = ((String)localObject).equals(str);
        if (bool2)
        {
          paramh = d;
          d.a(paramh);
          return bool1;
        }
      }
    }
    else
    {
      str = "ItemEvent.CLICKED";
      bool2 = ((String)localObject).equals(str);
      if (bool2)
      {
        int k = b;
        localObject = d;
        paramh = agetc;
        ((cf.b.b)localObject).b(paramh);
        return bool1;
      }
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    return ((a)a().get(paramInt)).hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.co
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */