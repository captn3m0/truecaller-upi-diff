package com.truecaller.calling.dialer;

import android.content.ContentResolver;
import android.net.Uri;
import c.g.a.a;
import c.g.b.k;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public final class r
  extends c
  implements s
{
  private final HashSet b;
  private boolean c;
  
  public r(ContentResolver paramContentResolver, Uri paramUri, Long paramLong)
  {
    super(paramContentResolver, paramUri, paramLong);
    paramContentResolver = new java/util/HashSet;
    paramContentResolver.<init>();
    b = paramContentResolver;
  }
  
  protected final void a()
  {
    Object localObject1 = (Iterable)b;
    boolean bool1 = localObject1 instanceof Collection;
    boolean bool2 = false;
    Object localObject2;
    if (bool1)
    {
      localObject2 = localObject1;
      localObject2 = (Collection)localObject1;
      bool1 = ((Collection)localObject2).isEmpty();
      if (bool1) {}
    }
    else
    {
      localObject1 = ((Iterable)localObject1).iterator();
      do
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (q)((Iterator)localObject1).next();
        bool1 = ((q)localObject2).a();
      } while (bool1);
      i = 0;
      localObject1 = null;
      break label92;
    }
    int i = 1;
    label92:
    if (i != 0)
    {
      localObject1 = a;
      if (localObject1 != null) {
        ((v.a)localObject1).D_();
      }
    }
    else
    {
      bool2 = true;
    }
    c = bool2;
  }
  
  public final void a(q paramq)
  {
    k.b(paramq, "condition");
    Object localObject = new com/truecaller/calling/dialer/r$a;
    r localr = this;
    localr = (r)this;
    ((r.a)localObject).<init>(localr);
    localObject = (a)localObject;
    paramq.a((a)localObject);
    b.add(paramq);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */