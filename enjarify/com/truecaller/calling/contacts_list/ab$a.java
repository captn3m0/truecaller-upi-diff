package com.truecaller.calling.contacts_list;

import c.g.b.k;
import com.truecaller.ads.b.w;
import com.truecaller.ads.provider.e;

public final class ab$a
{
  final e a;
  final w b;
  
  public ab$a(e parame, w paramw)
  {
    a = parame;
    b = paramw;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        Object localObject = a;
        e locale = a;
        bool1 = k.a(localObject, locale);
        if (bool1)
        {
          localObject = b;
          paramObject = b;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    e locale = a;
    int i = 0;
    int j;
    if (locale != null)
    {
      j = locale.hashCode();
    }
    else
    {
      j = 0;
      locale = null;
    }
    j *= 31;
    w localw = b;
    if (localw != null) {
      i = localw.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AdsPresenterWithLoader(adsLoader=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", multiAdsPresenter=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ab.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */