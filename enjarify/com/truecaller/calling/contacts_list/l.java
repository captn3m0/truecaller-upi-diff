package com.truecaller.calling.contacts_list;

import android.content.ContentResolver;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.calling.dialer.r;
import com.truecaller.calling.dialer.s;
import com.truecaller.content.TruecallerContract.e;
import com.truecaller.content.TruecallerContract.f;

public abstract class l
{
  public static final l.b a;
  
  static
  {
    l.b localb = new com/truecaller/calling/contacts_list/l$b;
    localb.<init>((byte)0);
    a = localb;
  }
  
  public static final s a(ContentResolver paramContentResolver)
  {
    k.b(paramContentResolver, "contentResolver");
    r localr = new com/truecaller/calling/dialer/r;
    Uri localUri = TruecallerContract.f.b();
    k.a(localUri, "TruecallerContract.Conta…rtedContactsWithDataUri()");
    Long localLong = Long.valueOf(-1);
    localr.<init>(paramContentResolver, localUri, localLong);
    return (s)localr;
  }
  
  public static final s b(ContentResolver paramContentResolver)
  {
    k.b(paramContentResolver, "contentResolver");
    r localr = new com/truecaller/calling/dialer/r;
    Uri localUri = TruecallerContract.e.a();
    k.a(localUri, "TruecallerContract.Conta…ingsTable.getContentUri()");
    Long localLong = Long.valueOf(-1);
    localr.<init>(paramContentResolver, localUri, localLong);
    return (s)localr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */