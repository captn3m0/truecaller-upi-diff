package com.truecaller.calling.contacts_list;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;

public final class e
  extends RecyclerView.ViewHolder
  implements b.c
{
  public e(View paramView, b.c.a parama)
  {
    super(paramView);
    int i = R.id.btn_turn_on_backup;
    Object localObject1 = (Button)paramView.findViewById(i);
    Object localObject2 = new com/truecaller/calling/contacts_list/e$1;
    ((e.1)localObject2).<init>(parama);
    localObject2 = (View.OnClickListener)localObject2;
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    i = R.id.btn_dismiss_backup;
    paramView = (Button)paramView.findViewById(i);
    localObject1 = new com/truecaller/calling/contacts_list/e$2;
    ((e.2)localObject1).<init>(parama);
    localObject1 = (View.OnClickListener)localObject1;
    paramView.setOnClickListener((View.OnClickListener)localObject1);
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    Object localObject = itemView;
    k.a(localObject, "itemView");
    int i = R.id.title;
    localObject = (TextView)((View)localObject).findViewById(i);
    k.a(localObject, "itemView.title");
    ((TextView)localObject).setText(paramCharSequence);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */