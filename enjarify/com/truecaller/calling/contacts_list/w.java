package com.truecaller.calling.contacts_list;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.View;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.m;
import com.truecaller.adapter_delegates.o;
import com.truecaller.adapter_delegates.p;
import com.truecaller.adapter_delegates.s;
import com.truecaller.ads.b.j;
import com.truecaller.ui.q;
import java.util.Iterator;
import java.util.Set;

public final class w
  implements p.c
{
  final com.truecaller.adapter_delegates.f b;
  final View c;
  private final c.f d;
  private final p e;
  private final p f;
  private final p g;
  private final m h;
  private final p.c.a i;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = c.g.b.w.a(w.class);
    ((c.g.b.u)localObject).<init>(localb, "emptyView", "getEmptyView()Landroid/view/View;");
    localObject = (c.l.g)c.g.b.w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public w(p.c.a parama, View paramView, b.b paramb, ContactsHolder.PhonebookFilter paramPhonebookFilter, ContactsHolder paramContactsHolder, y paramy, com.truecaller.ads.b.w paramw)
  {
    i = parama;
    c = paramView;
    parama = com.truecaller.utils.extensions.t.a(c, 2131362962);
    d = parama;
    parama = new com/truecaller/adapter_delegates/p;
    paramView = ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES;
    paramView = (com.truecaller.adapter_delegates.b)paramy.a(paramPhonebookFilter, paramView);
    Object localObject = new com/truecaller/calling/contacts_list/w$e;
    ((w.e)localObject).<init>(this);
    localObject = (c.g.a.b)localObject;
    c.g.a.b localb = (c.g.a.b)w.f.a;
    parama.<init>(paramView, 2131559064, (c.g.a.b)localObject, localb);
    e = parama;
    parama = new com/truecaller/adapter_delegates/p;
    paramView = ContactsHolder.FavoritesFilter.FAVORITES_ONLY;
    paramView = (com.truecaller.adapter_delegates.b)paramy.a(paramPhonebookFilter, paramView);
    paramy = new com/truecaller/calling/contacts_list/w$g;
    paramy.<init>(this);
    paramy = (c.g.a.b)paramy;
    localObject = (c.g.a.b)w.h.a;
    parama.<init>(paramView, 2131558615, paramy, (c.g.a.b)localObject);
    f = parama;
    parama = new com/truecaller/adapter_delegates/p;
    paramView = paramb;
    paramView = (com.truecaller.adapter_delegates.b)paramb;
    paramy = new com/truecaller/calling/contacts_list/w$c;
    paramy.<init>(paramb);
    paramy = (c.g.a.b)paramy;
    paramb = (c.g.a.b)w.d.a;
    parama.<init>(paramView, 2131559004, paramy, paramb);
    g = parama;
    parama = j.a(paramw);
    h = parama;
    parama = new com/truecaller/adapter_delegates/f;
    paramView = e;
    paramb = (a)f;
    paramy = new com/truecaller/adapter_delegates/g;
    paramw = null;
    paramy.<init>((byte)0);
    paramy = (s)paramy;
    paramView = paramView.a(paramb, paramy);
    paramb = (a)g;
    paramy = new com/truecaller/adapter_delegates/g;
    paramy.<init>((byte)0);
    paramy = (s)paramy;
    paramView = (a)paramView.a(paramb, paramy);
    paramb = (a)h;
    paramy = new com/truecaller/adapter_delegates/o;
    int j = 2;
    int k = 12;
    int m = 4;
    paramy.<init>(j, k, m);
    paramy = (s)paramy;
    paramView = (a)paramView.a(paramb, paramy);
    parama.<init>(paramView);
    boolean bool = true;
    parama.setHasStableIds(bool);
    b = parama;
    parama = c;
    int n = R.id.contacts_list;
    parama = (RecyclerView)parama.findViewById(n);
    paramb = b;
    paramb.a(bool);
    paramb = (RecyclerView.Adapter)paramb;
    parama.setAdapter(paramb);
    bool = false;
    parama.setItemAnimator(null);
    paramView = new com/truecaller/ui/q;
    paramb = c.getContext();
    int i1 = 2131559208;
    paramView.<init>(paramb, i1, 0);
    paramView = (RecyclerView.ItemDecoration)paramView;
    parama.addItemDecoration(paramView);
    paramView = new com/truecaller/calling/contacts_list/w$a;
    paramView.<init>(this, paramPhonebookFilter, paramContactsHolder);
    paramView = (RecyclerView.OnScrollListener)paramView;
    parama.addOnScrollListener(paramView);
    paramView = c;
    n = R.id.fast_scroller;
    paramView = (FastScroller)paramView.findViewById(n);
    k.a(parama, "this");
    paramb = new com/truecaller/calling/contacts_list/w$b;
    paramb.<init>(this, paramPhonebookFilter, paramContactsHolder);
    paramb = (c.g.a.b)paramb;
    k.b(parama, "recyclerView");
    paramPhonebookFilter = "indexByPosition";
    k.b(paramb, paramPhonebookFilter);
    a = parama;
    c = paramb;
    paramb = parama.getLayoutManager();
    if (paramb != null)
    {
      paramb = (LinearLayoutManager)paramb;
      b = paramb;
      paramb = new com/truecaller/calling/contacts_list/FastScroller$a;
      paramb.<init>(paramView);
      paramb = (RecyclerView.OnScrollListener)paramb;
      parama.addOnScrollListener(paramb);
      paramView.a();
      return;
    }
    parama = new c/u;
    parama.<init>("null cannot be cast to non-null type android.support.v7.widget.LinearLayoutManager");
    throw parama;
  }
  
  private final View c()
  {
    return (View)d.b();
  }
  
  public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter, boolean paramBoolean)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    b.a(paramBoolean);
    com.truecaller.utils.extensions.t.a(c(), paramBoolean);
  }
  
  public final void a(Set paramSet)
  {
    Object localObject = "adsPositions";
    k.b(paramSet, (String)localObject);
    paramSet = ((Iterable)paramSet).iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      localObject = (Number)paramSet.next();
      int j = ((Number)localObject).intValue();
      j = h.a_(j);
      com.truecaller.adapter_delegates.f localf = b;
      int k = localf.getItemCount() - j;
      localf.notifyItemRangeChanged(j, k);
    }
  }
  
  public final void b()
  {
    b.notifyDataSetChanged();
    View localView = c;
    int j = R.id.fast_scroller;
    ((FastScroller)localView.findViewById(j)).a();
  }
  
  public final void s_()
  {
    b.notifyDataSetChanged();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */