package com.truecaller.calling.contacts_list;

public enum ContactsHolder$PhonebookFilter
{
  static
  {
    PhonebookFilter[] arrayOfPhonebookFilter = new PhonebookFilter[2];
    PhonebookFilter localPhonebookFilter = new com/truecaller/calling/contacts_list/ContactsHolder$PhonebookFilter;
    localPhonebookFilter.<init>("NON_PHONEBOOK_ONLY", 0);
    NON_PHONEBOOK_ONLY = localPhonebookFilter;
    arrayOfPhonebookFilter[0] = localPhonebookFilter;
    localPhonebookFilter = new com/truecaller/calling/contacts_list/ContactsHolder$PhonebookFilter;
    int i = 1;
    localPhonebookFilter.<init>("PHONEBOOK_ONLY", i);
    PHONEBOOK_ONLY = localPhonebookFilter;
    arrayOfPhonebookFilter[i] = localPhonebookFilter;
    $VALUES = arrayOfPhonebookFilter;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */