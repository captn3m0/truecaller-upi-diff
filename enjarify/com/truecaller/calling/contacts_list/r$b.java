package com.truecaller.calling.contacts_list;

import c.a.m;
import c.a.y;
import c.g.a.b;
import c.g.b.k;
import c.m.i;
import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import com.truecaller.data.entity.Contact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

final class r$b
{
  private final List[][] a;
  
  public r$b(List paramList)
  {
    Object localObject1 = ContactsHolder.FavoritesFilter.values();
    int i = localObject1.length;
    Object localObject2 = new List[i][];
    Object localObject3 = null;
    int j = 0;
    Integer localInteger = null;
    Object localObject4;
    Object localObject5;
    Object localObject6;
    while (j < i)
    {
      int k = ContactsHolder.PhonebookFilter.values().length;
      localObject4 = new List[k];
      int m = localObject4.length;
      int n = 0;
      localObject5 = null;
      while (n < m)
      {
        localObject6 = (List)y.a;
        localObject4[n] = localObject6;
        n += 1;
      }
      localObject2[j] = localObject4;
      j += 1;
    }
    localObject2 = (List[][])localObject2;
    a = ((List[][])localObject2);
    paramList = (Iterable)paramList;
    localObject1 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject1).<init>();
    localObject1 = (Map)localObject1;
    paramList = paramList.iterator();
    Object localObject7;
    for (;;)
    {
      boolean bool3 = paramList.hasNext();
      j = 1;
      if (!bool3) {
        break;
      }
      localObject2 = paramList.next();
      localObject4 = localObject2;
      localObject4 = a;
      localObject7 = ((Contact)localObject4).E();
      if (localObject7 == null)
      {
        boolean bool1 = ((Contact)localObject4).Y();
        if (!bool1)
        {
          j = 0;
          localInteger = null;
        }
      }
      localInteger = Integer.valueOf(j);
      localObject4 = ((Map)localObject1).get(localInteger);
      if (localObject4 == null)
      {
        localObject4 = new java/util/ArrayList;
        ((ArrayList)localObject4).<init>();
        ((Map)localObject1).put(localInteger, localObject4);
      }
      localObject4 = (List)localObject4;
      ((List)localObject4).add(localObject2);
    }
    paramList = Integer.valueOf(0);
    paramList = (List)((Map)localObject1).get(paramList);
    if (paramList != null)
    {
      localObject2 = ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES;
      localObject4 = ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY;
      paramList = (Iterable)paramList;
      localObject7 = new java/util/ArrayList;
      ((ArrayList)localObject7).<init>();
      localObject7 = (Collection)localObject7;
      paramList = paramList.iterator();
      for (;;)
      {
        boolean bool2 = paramList.hasNext();
        if (!bool2) {
          break;
        }
        localObject5 = paramList.next();
        localObject6 = localObject5;
        localObject6 = (SortedContactsDao.b)localObject5;
        Contact localContact = a;
        boolean bool4 = localContact.U();
        if (!bool4)
        {
          bool5 = c;
          if (!bool5)
          {
            bool5 = false;
            localObject6 = null;
            break label415;
          }
        }
        boolean bool5 = true;
        label415:
        if (!bool5) {
          ((Collection)localObject7).add(localObject5);
        }
      }
      localObject7 = (List)localObject7;
      a((ContactsHolder.FavoritesFilter)localObject2, (ContactsHolder.PhonebookFilter)localObject4, (List)localObject7);
    }
    paramList = Integer.valueOf(j);
    paramList = (List)((Map)localObject1).get(paramList);
    if (paramList != null)
    {
      localObject1 = ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES;
      localObject2 = ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY;
      a((ContactsHolder.FavoritesFilter)localObject1, (ContactsHolder.PhonebookFilter)localObject2, paramList);
    }
    paramList = ContactsHolder.FavoritesFilter.FAVORITES_ONLY;
    localObject1 = ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY;
    localObject2 = ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES;
    localObject3 = ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY;
    localObject2 = m.n((Iterable)a((ContactsHolder.FavoritesFilter)localObject2, (ContactsHolder.PhonebookFilter)localObject3));
    localObject3 = (b)r.b.1.a;
    localObject2 = c.m.l.a((i)localObject2, (b)localObject3);
    localObject3 = (b)r.b.2.a;
    localObject2 = c.m.l.d(c.m.l.c((i)localObject2, (b)localObject3));
    a(paramList, (ContactsHolder.PhonebookFilter)localObject1, (List)localObject2);
  }
  
  private static int a(ContactsHolder.FavoritesFilter paramFavoritesFilter)
  {
    int[] arrayOfInt = t.a;
    int i = paramFavoritesFilter.ordinal();
    i = arrayOfInt[i];
    switch (i)
    {
    default: 
      paramFavoritesFilter = new c/l;
      paramFavoritesFilter.<init>();
      throw paramFavoritesFilter;
    case 2: 
      return 1;
    }
    return 0;
  }
  
  private static int a(ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    int[] arrayOfInt = t.b;
    int i = paramPhonebookFilter.ordinal();
    i = arrayOfInt[i];
    switch (i)
    {
    default: 
      paramPhonebookFilter = new c/l;
      paramPhonebookFilter.<init>();
      throw paramPhonebookFilter;
    case 2: 
      return 1;
    }
    return 0;
  }
  
  public final List a(ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    k.b(paramFavoritesFilter, "favoritesFilter");
    k.b(paramPhonebookFilter, "phonebookFilter");
    List[][] arrayOfList = a;
    int i = a(paramFavoritesFilter);
    paramFavoritesFilter = arrayOfList[i];
    int j = a(paramPhonebookFilter);
    return paramFavoritesFilter[j];
  }
  
  final void a(ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter, List paramList)
  {
    List[][] arrayOfList = a;
    int i = a(paramFavoritesFilter);
    paramFavoritesFilter = arrayOfList[i];
    int j = a(paramPhonebookFilter);
    paramFavoritesFilter[j] = paramList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.r.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */