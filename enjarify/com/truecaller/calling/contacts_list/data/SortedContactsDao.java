package com.truecaller.calling.contacts_list.data;

import com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter;
import com.truecaller.calling.contacts_list.ContactsHolder.SortingMode;
import java.util.List;

public abstract interface SortedContactsDao
{
  public abstract SortedContactsRepository.b a(ContactsHolder.SortingMode paramSortingMode, ContactsHolder.PhonebookFilter paramPhonebookFilter, boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract List a(ContactsHolder.SortingMode paramSortingMode, SortedContactsDao.ContactFullness paramContactFullness, Integer paramInteger, ContactsHolder.PhonebookFilter paramPhonebookFilter);
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.SortedContactsDao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */