package com.truecaller.calling.contacts_list.data;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter;
import com.truecaller.calling.contacts_list.ContactsHolder.SortingMode;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

final class d$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  int f;
  private ag l;
  
  d$a(d paramd, SortedContactsRepository.ContactsLoadingMode paramContactsLoadingMode, ContactsHolder.SortingMode paramSortingMode, boolean paramBoolean1, boolean paramBoolean2, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/calling/contacts_list/data/d$a;
    d locald = g;
    SortedContactsRepository.ContactsLoadingMode localContactsLoadingMode = h;
    ContactsHolder.SortingMode localSortingMode = i;
    boolean bool1 = j;
    boolean bool2 = k;
    locala.<init>(locald, localContactsLoadingMode, localSortingMode, bool1, bool2, paramc);
    paramObject = (ag)paramObject;
    l = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int m = f;
    int i1 = 2;
    boolean bool2;
    boolean bool3;
    boolean bool4;
    boolean bool1;
    int i5;
    int i4;
    int i3;
    switch (m)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 8: 
      localObject1 = (SortedContactsRepository.b)e;
      localObject2 = (List)d;
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label832;
      }
      throw a;
    case 7: 
      localObject2 = (List)d;
      localObject3 = (ao)c;
      localObject4 = (ao)b;
      localObject5 = (ao)a;
      bool3 = paramObject instanceof o.b;
      if (!bool3) {
        break label765;
      }
      throw a;
    case 6: 
      localObject2 = (ao)c;
      localObject3 = (ao)b;
      localObject4 = (ao)a;
      bool4 = paramObject instanceof o.b;
      if (!bool4)
      {
        localObject5 = localObject4;
        localObject4 = localObject3;
        localObject3 = localObject2;
      }
      else
      {
        throw a;
      }
      break;
    case 5: 
      localObject1 = (SortedContactsRepository.b)e;
      localObject2 = (List)d;
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label1130;
      }
      throw a;
    case 4: 
      localObject2 = (List)d;
      localObject3 = (ao)c;
      localObject4 = (ao)b;
      localObject5 = (ao)a;
      bool3 = paramObject instanceof o.b;
      if (!bool3) {
        break label1064;
      }
      throw a;
    case 3: 
      localObject2 = (ao)c;
      localObject3 = (ao)b;
      localObject4 = (ao)a;
      bool4 = paramObject instanceof o.b;
      if (!bool4)
      {
        localObject5 = localObject4;
        localObject4 = localObject3;
        localObject3 = localObject2;
        break label1001;
      }
      throw a;
    case 2: 
      localObject1 = (List)c;
      bool1 = paramObject instanceof o.b;
      if (!bool1) {
        break label1300;
      }
      throw a;
    case 1: 
      localObject2 = (ao)b;
      localObject4 = (ao)a;
      bool4 = paramObject instanceof o.b;
      if (!bool4) {
        break label1249;
      }
      throw a;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label1442;
      }
      paramObject = l;
      localObject2 = h;
      localObject4 = e.a;
      n = ((SortedContactsRepository.ContactsLoadingMode)localObject2).ordinal();
      n = localObject4[n];
      i5 = 100;
      i4 = 6;
      i3 = 3;
      switch (n)
      {
      default: 
        paramObject = new c/l;
        ((l)paramObject).<init>();
        throw ((Throwable)paramObject);
      case 5: 
        localObject2 = new com/truecaller/calling/contacts_list/data/d$a$f;
        ((d.a.f)localObject2).<init>(this, null);
        localObject2 = (m)localObject2;
        localObject2 = kotlinx.coroutines.e.a((ag)paramObject, null, (m)localObject2, i3);
        localObject3 = new com/truecaller/calling/contacts_list/data/d$a$h;
        ((d.a.h)localObject3).<init>(this, null);
        localObject3 = (m)localObject3;
        localObject3 = kotlinx.coroutines.e.a((ag)paramObject, null, (m)localObject3, i3);
        localObject4 = new com/truecaller/calling/contacts_list/data/d$a$c;
        ((d.a.c)localObject4).<init>(this, null);
        localObject4 = (m)localObject4;
        paramObject = kotlinx.coroutines.e.a((ag)paramObject, null, (m)localObject4, i3);
        a = localObject2;
        b = localObject3;
        c = paramObject;
        f = i4;
        localObject4 = ((ao)paramObject).a(this);
        if (localObject4 == localObject1) {
          return localObject1;
        }
        localObject5 = localObject2;
        localObject6 = localObject3;
        localObject3 = paramObject;
        paramObject = localObject4;
        localObject4 = localObject6;
      }
      break;
    }
    paramObject = (List)paramObject;
    a = localObject5;
    b = localObject4;
    c = localObject3;
    d = paramObject;
    int n = 7;
    f = n;
    Object localObject2 = ((ao)localObject5).a(this);
    if (localObject2 == localObject1) {
      return localObject1;
    }
    Object localObject6 = localObject2;
    localObject2 = paramObject;
    paramObject = localObject6;
    label765:
    paramObject = (SortedContactsRepository.b)paramObject;
    a = localObject5;
    b = localObject4;
    c = localObject3;
    d = localObject2;
    e = paramObject;
    int i2 = 8;
    f = i2;
    Object localObject3 = ((ao)localObject4).a(this);
    if (localObject3 == localObject1) {
      return localObject1;
    }
    localObject1 = paramObject;
    paramObject = localObject3;
    label832:
    paramObject = (SortedContactsRepository.b)paramObject;
    localObject3 = new com/truecaller/calling/contacts_list/data/SortedContactsRepository$a;
    ((SortedContactsRepository.a)localObject3).<init>((List)localObject2, (SortedContactsRepository.b)localObject1, (SortedContactsRepository.b)paramObject);
    return localObject3;
    localObject2 = new com/truecaller/calling/contacts_list/data/d$a$e;
    ((d.a.e)localObject2).<init>(this, null);
    localObject2 = (m)localObject2;
    localObject2 = kotlinx.coroutines.e.a((ag)paramObject, null, (m)localObject2, i3);
    localObject3 = new com/truecaller/calling/contacts_list/data/d$a$g;
    ((d.a.g)localObject3).<init>(this, null);
    localObject3 = (m)localObject3;
    localObject3 = kotlinx.coroutines.e.a((ag)paramObject, null, (m)localObject3, i3);
    Object localObject4 = new com/truecaller/calling/contacts_list/data/d$a$b;
    ((d.a.b)localObject4).<init>(this, null);
    localObject4 = (m)localObject4;
    paramObject = kotlinx.coroutines.e.a((ag)paramObject, null, (m)localObject4, i3);
    a = localObject2;
    b = localObject3;
    c = paramObject;
    f = i3;
    localObject4 = ((ao)paramObject).a(this);
    if (localObject4 == localObject1) {
      return localObject1;
    }
    Object localObject5 = localObject2;
    localObject6 = localObject3;
    localObject3 = paramObject;
    paramObject = localObject4;
    localObject4 = localObject6;
    label1001:
    paramObject = (List)paramObject;
    a = localObject5;
    b = localObject4;
    c = localObject3;
    d = paramObject;
    n = 4;
    f = n;
    localObject2 = ((ao)localObject5).a(this);
    if (localObject2 == localObject1) {
      return localObject1;
    }
    localObject6 = localObject2;
    localObject2 = paramObject;
    paramObject = localObject6;
    label1064:
    paramObject = (SortedContactsRepository.b)paramObject;
    a = localObject5;
    b = localObject4;
    c = localObject3;
    d = localObject2;
    e = paramObject;
    i2 = 5;
    f = i2;
    localObject3 = ((ao)localObject4).a(this);
    if (localObject3 == localObject1) {
      return localObject1;
    }
    localObject1 = paramObject;
    paramObject = localObject3;
    label1130:
    paramObject = (SortedContactsRepository.b)paramObject;
    localObject3 = new com/truecaller/calling/contacts_list/data/SortedContactsRepository$a;
    ((SortedContactsRepository.a)localObject3).<init>((List)localObject2, (SortedContactsRepository.b)localObject1, (SortedContactsRepository.b)paramObject);
    return localObject3;
    localObject2 = new com/truecaller/calling/contacts_list/data/d$a$d;
    ((d.a.d)localObject2).<init>(this, null);
    localObject2 = (m)localObject2;
    localObject4 = kotlinx.coroutines.e.a((ag)paramObject, null, (m)localObject2, i3);
    localObject2 = new com/truecaller/calling/contacts_list/data/d$a$a;
    ((d.a.a)localObject2).<init>(this, null);
    localObject2 = (m)localObject2;
    localObject2 = kotlinx.coroutines.e.a((ag)paramObject, null, (m)localObject2, i3);
    a = localObject4;
    b = localObject2;
    int i6 = 1;
    f = i6;
    paramObject = ((ao)localObject2).a(this);
    if (paramObject == localObject1) {
      return localObject1;
    }
    label1249:
    paramObject = (List)paramObject;
    a = localObject4;
    b = localObject2;
    c = paramObject;
    f = i2;
    localObject2 = ((ao)localObject4).a(this);
    if (localObject2 == localObject1) {
      return localObject1;
    }
    localObject1 = paramObject;
    paramObject = localObject2;
    label1300:
    paramObject = (SortedContactsRepository.b)paramObject;
    localObject2 = new com/truecaller/calling/contacts_list/data/SortedContactsRepository$a;
    ((SortedContactsRepository.a)localObject2).<init>((List)localObject1, (SortedContactsRepository.b)paramObject, i2);
    return localObject2;
    paramObject = new com/truecaller/calling/contacts_list/data/SortedContactsRepository$a;
    localObject1 = g.a;
    localObject2 = i;
    localObject3 = SortedContactsDao.ContactFullness.BARE_MINIMUM;
    localObject4 = Integer.valueOf(i5);
    ContactsHolder.PhonebookFilter localPhonebookFilter = ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY;
    localObject1 = ((SortedContactsDao)localObject1).a((ContactsHolder.SortingMode)localObject2, (SortedContactsDao.ContactFullness)localObject3, (Integer)localObject4, localPhonebookFilter);
    ((SortedContactsRepository.a)paramObject).<init>((List)localObject1, null, i4);
    return paramObject;
    paramObject = new com/truecaller/calling/contacts_list/data/SortedContactsRepository$a;
    localObject1 = g.a;
    localObject2 = i;
    localObject3 = SortedContactsDao.ContactFullness.BARE_MINIMUM;
    localObject4 = Integer.valueOf(i5);
    localPhonebookFilter = ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY;
    localObject1 = ((SortedContactsDao)localObject1).a((ContactsHolder.SortingMode)localObject2, (SortedContactsDao.ContactFullness)localObject3, (Integer)localObject4, localPhonebookFilter);
    ((SortedContactsRepository.a)paramObject).<init>((List)localObject1, null, i4);
    return paramObject;
    label1442:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */