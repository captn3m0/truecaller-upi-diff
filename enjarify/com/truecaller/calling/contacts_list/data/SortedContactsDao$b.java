package com.truecaller.calling.contacts_list.data;

import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.b;

public final class SortedContactsDao$b
{
  public final Contact a;
  public final b b;
  public final boolean c;
  
  public SortedContactsDao$b(Contact paramContact, b paramb, boolean paramBoolean)
  {
    a = paramContact;
    b = paramb;
    c = paramBoolean;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof b;
      if (bool2)
      {
        paramObject = (b)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            bool2 = c;
            boolean bool3 = c;
            if (bool2 == bool3)
            {
              bool3 = true;
            }
            else
            {
              bool3 = false;
              paramObject = null;
            }
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    Contact localContact = a;
    int i = 0;
    if (localContact != null)
    {
      k = localContact.hashCode();
    }
    else
    {
      k = 0;
      localContact = null;
    }
    k *= 31;
    b localb = b;
    if (localb != null) {
      i = localb.hashCode();
    }
    int k = (k + i) * 31;
    int j = c;
    if (j != 0) {
      j = 1;
    }
    return k + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SortedContact(contact=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", sortingData=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", isHidden=");
    boolean bool = c;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.SortedContactsDao.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */