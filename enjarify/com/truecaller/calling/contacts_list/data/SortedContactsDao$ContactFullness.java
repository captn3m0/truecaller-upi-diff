package com.truecaller.calling.contacts_list.data;

public enum SortedContactsDao$ContactFullness
{
  static
  {
    ContactFullness[] arrayOfContactFullness = new ContactFullness[2];
    ContactFullness localContactFullness = new com/truecaller/calling/contacts_list/data/SortedContactsDao$ContactFullness;
    localContactFullness.<init>("BARE_MINIMUM", 0);
    BARE_MINIMUM = localContactFullness;
    arrayOfContactFullness[0] = localContactFullness;
    localContactFullness = new com/truecaller/calling/contacts_list/data/SortedContactsDao$ContactFullness;
    int i = 1;
    localContactFullness.<init>("COMPLETE_WITH_ENTITIES", i);
    COMPLETE_WITH_ENTITIES = localContactFullness;
    arrayOfContactFullness[i] = localContactFullness;
    $VALUES = arrayOfContactFullness;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.SortedContactsDao.ContactFullness
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */