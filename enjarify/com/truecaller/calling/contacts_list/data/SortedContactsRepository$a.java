package com.truecaller.calling.contacts_list.data;

import c.g.b.k;
import java.util.List;

public final class SortedContactsRepository$a
{
  public final List a;
  public final SortedContactsRepository.b b;
  private final SortedContactsRepository.b c;
  
  public SortedContactsRepository$a(List paramList, SortedContactsRepository.b paramb1, SortedContactsRepository.b paramb2)
  {
    a = paramList;
    c = paramb1;
    b = paramb2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = c;
          localObject2 = c;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = b;
            paramObject = b;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final int hashCode()
  {
    List localList = a;
    int i = 0;
    if (localList != null)
    {
      j = localList.hashCode();
    }
    else
    {
      j = 0;
      localList = null;
    }
    j *= 31;
    SortedContactsRepository.b localb = c;
    int k;
    if (localb != null)
    {
      k = localb.hashCode();
    }
    else
    {
      k = 0;
      localb = null;
    }
    int j = (j + k) * 31;
    localb = b;
    if (localb != null) {
      i = localb.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ContactsWithIndexes(contacts=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", nonPhonebookContactsIndexes=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", phonebookContactsIndexes=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.SortedContactsRepository.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */