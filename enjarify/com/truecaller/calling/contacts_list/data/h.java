package com.truecaller.calling.contacts_list.data;

import c.g.b.k;
import com.truecaller.data.entity.Contact;

public final class h
  implements g
{
  private final android.support.v4.f.g a;
  
  public h()
  {
    android.support.v4.f.g localg = new android/support/v4/f/g;
    localg.<init>(256);
    a = localg;
  }
  
  public final Boolean a(Contact paramContact)
  {
    String str = "contact";
    k.b(paramContact, str);
    paramContact = paramContact.getTcId();
    if (paramContact != null) {
      return (Boolean)a.get(paramContact);
    }
    return null;
  }
  
  public final void a(Contact paramContact, boolean paramBoolean)
  {
    Object localObject = "contact";
    k.b(paramContact, (String)localObject);
    paramContact = paramContact.getTcId();
    if (paramContact != null)
    {
      localObject = a;
      Boolean localBoolean = Boolean.valueOf(paramBoolean);
      ((android.support.v4.f.g)localObject).put(paramContact, localBoolean);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */