package com.truecaller.calling.contacts_list.data;

import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d
{
  private final Provider a;
  
  private c(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static c a(Provider paramProvider)
  {
    c localc = new com/truecaller/calling/contacts_list/data/c;
    localc.<init>(paramProvider);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */