package com.truecaller.calling.contacts_list.data;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter;
import com.truecaller.calling.contacts_list.ContactsHolder.SortingMode;
import kotlinx.coroutines.ag;

final class d$a$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  d$a$d(d.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/calling/contacts_list/data/d$a$d;
    d.a locala = b;
    locald.<init>(locala, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.g.a;
        localObject = b.i;
        ContactsHolder.PhonebookFilter localPhonebookFilter = ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY;
        return SortedContactsDao.a.a((SortedContactsDao)paramObject, (ContactsHolder.SortingMode)localObject, localPhonebookFilter);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.d.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */