package com.truecaller.calling.contacts_list.data;

public enum SortedContactsRepository$ContactsLoadingMode
{
  static
  {
    ContactsLoadingMode[] arrayOfContactsLoadingMode = new ContactsLoadingMode[5];
    ContactsLoadingMode localContactsLoadingMode = new com/truecaller/calling/contacts_list/data/SortedContactsRepository$ContactsLoadingMode;
    localContactsLoadingMode.<init>("PHONEBOOK_LIMITED", 0);
    PHONEBOOK_LIMITED = localContactsLoadingMode;
    arrayOfContactsLoadingMode[0] = localContactsLoadingMode;
    localContactsLoadingMode = new com/truecaller/calling/contacts_list/data/SortedContactsRepository$ContactsLoadingMode;
    int i = 1;
    localContactsLoadingMode.<init>("NON_PHONEBOOK_LIMITED", i);
    NON_PHONEBOOK_LIMITED = localContactsLoadingMode;
    arrayOfContactsLoadingMode[i] = localContactsLoadingMode;
    localContactsLoadingMode = new com/truecaller/calling/contacts_list/data/SortedContactsRepository$ContactsLoadingMode;
    i = 2;
    localContactsLoadingMode.<init>("PHONEBOOK_INITIAL", i);
    PHONEBOOK_INITIAL = localContactsLoadingMode;
    arrayOfContactsLoadingMode[i] = localContactsLoadingMode;
    localContactsLoadingMode = new com/truecaller/calling/contacts_list/data/SortedContactsRepository$ContactsLoadingMode;
    i = 3;
    localContactsLoadingMode.<init>("FULL_INITIAL", i);
    FULL_INITIAL = localContactsLoadingMode;
    arrayOfContactsLoadingMode[i] = localContactsLoadingMode;
    localContactsLoadingMode = new com/truecaller/calling/contacts_list/data/SortedContactsRepository$ContactsLoadingMode;
    i = 4;
    localContactsLoadingMode.<init>("FULL_WITH_ENTITIES", i);
    FULL_WITH_ENTITIES = localContactsLoadingMode;
    arrayOfContactsLoadingMode[i] = localContactsLoadingMode;
    $VALUES = arrayOfContactsLoadingMode;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.SortedContactsRepository.ContactsLoadingMode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */