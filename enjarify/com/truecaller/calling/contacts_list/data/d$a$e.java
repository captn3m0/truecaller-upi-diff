package com.truecaller.calling.contacts_list.data;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter;
import com.truecaller.calling.contacts_list.ContactsHolder.SortingMode;
import kotlinx.coroutines.ag;

final class d$a$e
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  d$a$e(d.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/calling/contacts_list/data/d$a$e;
    d.a locala = b;
    locale.<init>(locala, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = b.g.a;
        localObject = b.i;
        ContactsHolder.PhonebookFilter localPhonebookFilter = ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY;
        boolean bool2 = b.j;
        boolean bool3 = b.k;
        return ((SortedContactsDao)paramObject).a((ContactsHolder.SortingMode)localObject, localPhonebookFilter, bool2, bool3);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.d.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */