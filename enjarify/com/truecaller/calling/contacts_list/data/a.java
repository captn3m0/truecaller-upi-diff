package com.truecaller.calling.contacts_list.data;

import android.content.ContentResolver;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.net.Uri.Builder;
import c.a.m;
import c.a.y;
import c.g.a.q;
import c.g.b.k;
import c.l;
import c.n;
import c.t;
import com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter;
import com.truecaller.calling.contacts_list.ContactsHolder.SortingMode;
import com.truecaller.content.TruecallerContract.f;
import com.truecaller.data.access.e;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.d;
import com.truecaller.util.z;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class a
  implements SortedContactsDao
{
  private final String a;
  private final ContentResolver b;
  
  public a(ContentResolver paramContentResolver)
  {
    b = paramContentResolver;
    a = "data_type = 4 OR data_type = 1 OR data_type IS NULL OR data_type = 7";
  }
  
  public final SortedContactsRepository.b a(ContactsHolder.SortingMode paramSortingMode, ContactsHolder.PhonebookFilter paramPhonebookFilter, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramSortingMode, "sortingMode");
    String str1 = "phonebookFilter";
    k.b(paramPhonebookFilter, str1);
    long l1 = System.currentTimeMillis();
    Object localObject1 = b.d;
    int i = paramSortingMode.ordinal();
    int j = localObject1[i];
    switch (j)
    {
    default: 
      paramSortingMode = new c/l;
      paramSortingMode.<init>();
      throw paramSortingMode;
    case 2: 
      localObject1 = "last_name";
      break;
    case 1: 
      localObject1 = "first_name";
    }
    Object localObject2 = b.e;
    int k = paramPhonebookFilter.ordinal();
    i = localObject2[k];
    switch (i)
    {
    default: 
      paramSortingMode = new c/l;
      paramSortingMode.<init>();
      throw paramSortingMode;
    case 2: 
      localObject2 = "non_phonebook";
      break;
    case 1: 
      localObject2 = "phonebook";
    }
    Object localObject3 = TruecallerContract.f.a((String)localObject1, (String)localObject2, paramBoolean1, paramBoolean2);
    k.a(localObject3, "getFastScrollIndexingUri…SpamIndexes\n            )");
    paramBoolean1 = false;
    Object localObject4 = null;
    try
    {
      Object localObject5 = b;
      int m = 0;
      String str2 = null;
      Cursor localCursor = ((ContentResolver)localObject5).query((Uri)localObject3, null, null, null, null);
      Object localObject7;
      if (localCursor != null)
      {
        localObject1 = localCursor;
        localObject1 = (Closeable)localCursor;
        try
        {
          localObject2 = new java/util/ArrayList;
          ((ArrayList)localObject2).<init>();
          localObject2 = (Collection)localObject2;
          for (;;)
          {
            boolean bool = localCursor.moveToNext();
            if (!bool) {
              break;
            }
            localObject5 = new com/truecaller/calling/contacts_list/data/a$a;
            localObject3 = "group_label";
            int n = localCursor.getColumnIndexOrThrow((String)localObject3);
            localObject3 = localCursor.getString(n);
            str2 = "label_count";
            m = localCursor.getColumnIndexOrThrow(str2);
            m = localCursor.getInt(m);
            ((a.a)localObject5).<init>((String)localObject3, m);
            ((Collection)localObject2).add(localObject5);
          }
          localObject2 = (List)localObject2;
          c.f.b.a((Closeable)localObject1, null);
          localObject4 = localObject2;
        }
        finally
        {
          try
          {
            throw ((Throwable)localObject6);
          }
          finally
          {
            localObject2 = localObject6;
            localObject7 = localObject8;
            c.f.b.a((Closeable)localObject1, (Throwable)localObject2);
          }
        }
      }
      long l2;
      StringBuilder localStringBuilder;
      return (SortedContactsRepository.b)paramSortingMode;
    }
    catch (SQLException localSQLException)
    {
      localObject7 = (Throwable)localSQLException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject7);
      if (localObject4 == null) {
        localObject4 = (List)y.a;
      }
      l2 = System.currentTimeMillis() - l1;
      localObject7 = new String[1];
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("getFastScrollIndexes, sortingMode: ");
      localStringBuilder.append(paramSortingMode);
      localStringBuilder.append(", phonebookFilter: ");
      localStringBuilder.append(paramPhonebookFilter);
      localStringBuilder.append(", took: ");
      localStringBuilder.append(l2);
      localStringBuilder.append("ms");
      paramSortingMode = localStringBuilder.toString();
      localObject7[0] = paramSortingMode;
      localObject4 = (Iterable)localObject4;
      paramSortingMode = new com/truecaller/calling/contacts_list/data/a$b;
      paramSortingMode.<init>((Iterable)localObject4);
    }
  }
  
  public final List a(ContactsHolder.SortingMode paramSortingMode, SortedContactsDao.ContactFullness paramContactFullness, Integer paramInteger, ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    Object localObject1 = this;
    SortedContactsDao.ContactFullness localContactFullness = paramContactFullness;
    Object localObject2 = paramInteger;
    ContactsHolder.PhonebookFilter localPhonebookFilter = paramPhonebookFilter;
    Object localObject3 = paramSortingMode;
    k.b(paramSortingMode, "sortingMode");
    k.b(paramContactFullness, "fullness");
    long l1 = System.currentTimeMillis();
    Object localObject4 = b.a;
    int i = paramSortingMode.ordinal();
    int j = localObject4[i];
    switch (j)
    {
    default: 
      localObject4 = new c/l;
      ((l)localObject4).<init>();
      throw ((Throwable)localObject4);
    case 2: 
      localObject8 = "sorting_group_2";
      localObject4 = t.a("sorting_key_2 IS NULL, sorting_key_2, contact_name", localObject8);
      break;
    case 1: 
      localObject8 = "sorting_group_1";
      localObject4 = t.a("sorting_key_1 IS NULL, sorting_key_1, contact_name", localObject8);
    }
    Object localObject8 = a;
    Object localObject9 = localObject8;
    localObject9 = (String)localObject8;
    localObject4 = b;
    Object localObject10 = localObject4;
    localObject10 = (String)localObject4;
    localObject4 = b.b;
    i = paramContactFullness.ordinal();
    j = localObject4[i];
    switch (j)
    {
    default: 
      localObject4 = new c/l;
      ((l)localObject4).<init>();
      throw ((Throwable)localObject4);
    case 2: 
      localObject4 = new c/n;
      localObject8 = Boolean.TRUE;
      localObject11 = TruecallerContract.f.b();
      ((n)localObject4).<init>(localObject8, localObject11);
      break;
    case 1: 
      localObject4 = new c/n;
      localObject8 = Boolean.FALSE;
      localObject11 = TruecallerContract.f.c();
      ((n)localObject4).<init>(localObject8, localObject11);
    }
    localObject8 = (Boolean)a;
    boolean bool1 = ((Boolean)localObject8).booleanValue();
    localObject4 = (Uri)b;
    if (localObject2 != null)
    {
      localObject4 = ((Uri)localObject4).buildUpon();
      localObject8 = "limit";
      localObject12 = String.valueOf(paramInteger);
      localObject4 = ((Uri.Builder)localObject4).appendQueryParameter((String)localObject8, (String)localObject12).build();
    }
    i = 2;
    localObject8 = new String[i];
    if (bool1)
    {
      localObject12 = a;
    }
    else
    {
      k = 0;
      localObject12 = null;
    }
    localObject8[0] = localObject12;
    if (localPhonebookFilter != null)
    {
      localObject12 = b.c;
      int m = paramPhonebookFilter.ordinal();
      k = localObject12[m];
      switch (k)
      {
      default: 
        break;
      case 2: 
        localObject12 = "contact_phonebook_id IS NULL AND (contact_source & 32)!=32";
        break;
      case 1: 
        localObject12 = "contact_phonebook_id IS NOT NULL OR (contact_source & 32)=32";
        break;
      }
    }
    int k = 0;
    Object localObject12 = null;
    int n = 1;
    localObject8[n] = localObject12;
    localObject8 = m.e((Object[])localObject8);
    k = ((List)localObject8).size();
    if (k == n)
    {
      localObject8 = (String)m.d((List)localObject8);
    }
    else
    {
      Object localObject13 = localObject8;
      localObject13 = (Iterable)localObject8;
      CharSequence localCharSequence = (CharSequence)" AND ";
      localObject8 = a.f.a;
      Object localObject14 = localObject8;
      localObject14 = (c.g.a.b)localObject8;
      int i1 = 30;
      localObject8 = m.a((Iterable)localObject13, localCharSequence, null, null, 0, null, (c.g.a.b)localObject14, i1);
    }
    try
    {
      long l2 = System.currentTimeMillis();
      Object localObject15 = b;
      a.c localc = null;
      localObject15 = ((ContentResolver)localObject15).query((Uri)localObject4, null, (String)localObject8, null, (String)localObject9);
      long l3 = System.currentTimeMillis();
      long l4 = l3 - l2;
      localObject12 = new String[n];
      Object localObject16 = new java/lang/StringBuilder;
      Object localObject17 = "getAllContactsSorted, uri = ";
      ((StringBuilder)localObject16).<init>((String)localObject17);
      ((StringBuilder)localObject16).append(localObject4);
      localObject4 = ", selection = ";
      ((StringBuilder)localObject16).append((String)localObject4);
      ((StringBuilder)localObject16).append((String)localObject8);
      localObject4 = ", query took: ";
      ((StringBuilder)localObject16).append((String)localObject4);
      ((StringBuilder)localObject16).append(l4);
      localObject4 = "ms";
      ((StringBuilder)localObject16).append((String)localObject4);
      localObject4 = ((StringBuilder)localObject16).toString();
      localObject12[0] = localObject4;
      if (localObject15 != null)
      {
        localObject4 = new com/truecaller/data/c;
        ((com.truecaller.data.c)localObject4).<init>((Cursor)localObject15, (String)localObject10);
        localObject8 = "hidden_from_identified";
        i = ((Cursor)localObject15).getColumnIndex((String)localObject8);
        if (bool1)
        {
          localObject12 = new com/truecaller/calling/contacts_list/data/a$d;
          ((a.d)localObject12).<init>((com.truecaller.data.c)localObject4);
          localObject4 = localObject12;
          localObject4 = (c.g.a.b)localObject12;
          localObject12 = new com/truecaller/calling/contacts_list/data/a$e;
          ((a.e)localObject12).<init>(i);
          localObject1 = localObject12;
          localObject1 = (c.g.a.b)localObject12;
          localc = new com/truecaller/calling/contacts_list/data/a$c;
          localObject8 = localc;
          localObject12 = this;
          localObject3 = paramSortingMode;
          localObject16 = paramContactFullness;
          localObject17 = paramInteger;
          localObject2 = null;
          localObject18 = paramPhonebookFilter;
        }
        try
        {
          localc.<init>((String)localObject10, bool1, this, paramSortingMode, paramContactFullness, paramInteger, paramPhonebookFilter);
          localObject8 = (q)localc;
          localObject4 = z.a((Cursor)localObject15, (c.g.a.b)localObject4, (c.g.a.b)localObject1, (q)localObject8);
          break label1094;
          localObject2 = null;
          localObject10 = new com/truecaller/data/access/e;
          ((e)localObject10).<init>((Cursor)localObject15);
          localObject11 = localObject15;
          localObject11 = (Closeable)localObject15;
          try
          {
            localObject12 = new java/util/ArrayList;
            ((ArrayList)localObject12).<init>();
            localObject12 = (Collection)localObject12;
            for (;;)
            {
              boolean bool2 = ((Cursor)localObject15).moveToNext();
              if (!bool2) {
                break;
              }
              localObject18 = ((e)localObject10).a((Cursor)localObject15);
              bool2 = z.a((Contact)localObject18);
              if (bool2)
              {
                localObject3 = ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY;
                if (localPhonebookFilter == localObject3)
                {
                  if (localObject18 != null)
                  {
                    bool2 = ((Contact)localObject18).U();
                    localObject3 = Boolean.valueOf(bool2);
                  }
                  else
                  {
                    bool2 = false;
                    localObject3 = null;
                  }
                  bool2 = com.truecaller.utils.extensions.c.a((Boolean)localObject3);
                  if (bool2) {}
                }
                else
                {
                  bool2 = true;
                  break label974;
                }
              }
              bool2 = false;
              localObject3 = null;
              label974:
              if (!bool2) {
                localObject18 = null;
              }
              if (localObject18 != null)
              {
                localObject3 = new com/truecaller/calling/contacts_list/data/SortedContactsDao$b;
                localObject16 = "it";
                k.a(localObject18, (String)localObject16);
                localObject16 = ((com.truecaller.data.c)localObject4).a((Cursor)localObject15);
                n = ((Cursor)localObject15).getInt(i);
                if (n > 0)
                {
                  n = 1;
                }
                else
                {
                  n = 0;
                  localObject17 = null;
                }
                ((SortedContactsDao.b)localObject3).<init>((Contact)localObject18, (com.truecaller.data.entity.b)localObject16, n);
              }
              else
              {
                bool2 = false;
                localObject3 = null;
              }
              ((Collection)localObject12).add(localObject3);
            }
            localObject4 = localObject12;
            localObject4 = (List)localObject12;
            c.f.b.a((Closeable)localObject11, null);
            label1094:
            if (localObject4 == null) {
              break label1154;
            }
            localObject4 = (Iterable)localObject4;
            localObject4 = m.e((Iterable)localObject4);
            localObject18 = localObject4;
          }
          finally
          {
            localObject18 = localObject5;
            try
            {
              throw ((Throwable)localObject5);
            }
            finally
            {
              c.f.b.a((Closeable)localObject11, (Throwable)localObject18);
            }
          }
          localObject2 = null;
        }
        catch (IllegalStateException localIllegalStateException1)
        {
          break label1169;
        }
        catch (SQLException localSQLException1) {}
      }
      label1154:
      Object localObject18 = null;
      localObject2 = localObject18;
    }
    catch (IllegalStateException localIllegalStateException2)
    {
      localObject2 = null;
      localObject8 = new com/truecaller/log/UnmutedException$d;
      localObject10 = new java/lang/StringBuilder;
      localObject11 = "Error while reading contacts: ";
      ((StringBuilder)localObject10).<init>((String)localObject11);
      String str = localIllegalStateException2.getMessage();
      ((StringBuilder)localObject10).append(str);
      str = ((StringBuilder)localObject10).toString();
      ((UnmutedException.d)localObject8).<init>(str);
      localObject8 = (Throwable)localObject8;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject8);
    }
    catch (SQLException localSQLException2)
    {
      label1169:
      localObject2 = null;
      localObject7 = (Throwable)localSQLException2;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject7);
    }
    if (localObject2 == null)
    {
      localObject7 = y.a;
      localObject2 = localObject7;
      localObject2 = (List)localObject7;
    }
    long l5 = System.currentTimeMillis() - l1;
    localObject10 = new String[1];
    Object localObject11 = new java/lang/StringBuilder;
    ((StringBuilder)localObject11).<init>("getContacts: fullness: ");
    ((StringBuilder)localObject11).append(localContactFullness);
    ((StringBuilder)localObject11).append(", phonebookFilter: ");
    ((StringBuilder)localObject11).append(localPhonebookFilter);
    ((StringBuilder)localObject11).append(", limit: ");
    localObject12 = paramInteger;
    ((StringBuilder)localObject11).append(paramInteger);
    ((StringBuilder)localObject11).append(", returned ");
    k = ((List)localObject2).size();
    ((StringBuilder)localObject11).append(k);
    ((StringBuilder)localObject11).append(" items, took: ");
    ((StringBuilder)localObject11).append(l5);
    ((StringBuilder)localObject11).append("ms");
    Object localObject7 = ((StringBuilder)localObject11).toString();
    localObject10[0] = localObject7;
    return (List)localObject2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */