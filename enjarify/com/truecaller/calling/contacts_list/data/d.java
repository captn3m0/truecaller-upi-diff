package com.truecaller.calling.contacts_list.data;

import c.d.c;
import c.d.f;
import c.g.a.m;
import com.truecaller.calling.contacts_list.ContactsHolder.SortingMode;
import kotlinx.coroutines.g;

public final class d
  implements SortedContactsRepository
{
  final SortedContactsDao a;
  private final f b;
  
  public d(SortedContactsDao paramSortedContactsDao, f paramf)
  {
    a = paramSortedContactsDao;
    b = paramf;
  }
  
  public final Object a(ContactsHolder.SortingMode paramSortingMode, SortedContactsRepository.ContactsLoadingMode paramContactsLoadingMode, c paramc)
  {
    f localf = b;
    Object localObject = new com/truecaller/calling/contacts_list/data/d$a;
    ((d.a)localObject).<init>(this, paramContactsLoadingMode, paramSortingMode, false, false, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */