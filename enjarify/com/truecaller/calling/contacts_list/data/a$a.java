package com.truecaller.calling.contacts_list.data;

import c.g.b.k;

public final class a$a
{
  String a;
  int b;
  
  public a$a(String paramString, int paramInt)
  {
    a = paramString;
    b = paramInt;
    int i = b;
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramString = null;
    }
    if (i != 0) {
      return;
    }
    paramString = new java/lang/IllegalArgumentException;
    String str = "Failed requirement.".toString();
    paramString.<init>(str);
    throw ((Throwable)paramString);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof a;
      if (bool2)
      {
        paramObject = (a)paramObject;
        String str1 = a;
        String str2 = a;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          int i = b;
          int j = b;
          if (i == j)
          {
            j = 1;
          }
          else
          {
            j = 0;
            paramObject = null;
          }
          if (j != 0) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    i *= 31;
    int j = b;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FastScrollIndex(groupLabel=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", count=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */