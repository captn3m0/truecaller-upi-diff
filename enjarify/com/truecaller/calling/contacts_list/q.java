package com.truecaller.calling.contacts_list;

import android.support.v4.view.o;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.a.ag;
import c.a.f;
import c.a.m;
import c.g.b.k;
import c.t;
import com.truecaller.ads.g;
import com.truecaller.ads.provider.e;
import com.truecaller.common.account.r;
import com.truecaller.i.a;
import com.truecaller.utils.extensions.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class q
  extends o
  implements p.c.a
{
  final w[] a;
  final ab.a[] b;
  final ContactsHolder.PhonebookFilter[] c;
  private final Map[] d;
  private final long e;
  private boolean f;
  private int g;
  private final ContactsHolder h;
  private final p.a i;
  private final ab j;
  private final com.truecaller.utils.n k;
  private final y l;
  private final r m;
  private final b.b n;
  
  public q(ContactsHolder paramContactsHolder, p.a parama, ab paramab, com.truecaller.utils.n paramn, y paramy, r paramr, b.b paramb, a parama1)
  {
    h = paramContactsHolder;
    i = parama;
    j = paramab;
    k = paramn;
    l = paramy;
    m = paramr;
    n = paramb;
    int i1 = 2;
    parama = new w[i1];
    a = parama;
    parama = new ab.a[i1];
    b = parama;
    parama = new ContactsHolder.PhonebookFilter[i1];
    paramab = ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY;
    paramn = null;
    parama[0] = paramab;
    paramab = ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY;
    int i2 = 1;
    parama[i2] = paramab;
    c = parama;
    parama = new Map[i1];
    int i3 = 0;
    paramab = null;
    while (i3 < i1)
    {
      paramr = new c.n[i1];
      paramb = q.a.a;
      localObject = Boolean.FALSE;
      paramb = t.a(paramb, localObject);
      paramr[0] = paramb;
      paramb = q.a.b;
      localObject = Boolean.FALSE;
      paramb = t.a(paramb, localObject);
      paramr[i2] = paramb;
      paramr = ag.b(paramr);
      parama[i3] = paramr;
      i3 += 1;
    }
    d = parama;
    paramContactsHolder = TimeUnit.SECONDS;
    long l1 = parama1.a("adFeatureRetentionTime", 0L);
    long l2 = paramContactsHolder.toMillis(l1);
    e = l2;
    g = -1;
  }
  
  private final void a(int paramInt, q.a parama, boolean paramBoolean)
  {
    Map localMap = d[paramInt];
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    localMap.put(parama, localBoolean);
    parama = b;
    e locale = parama[paramInt];
    if (locale != null)
    {
      locale = a;
      if (locale != null)
      {
        parama = q.a.a;
        parama = (Boolean)localMap.get(parama);
        boolean bool = c.a(parama);
        if (!bool)
        {
          parama = q.a.b;
          parama = (Boolean)localMap.get(parama);
          bool = c.a(parama);
          if (!bool)
          {
            bool = false;
            parama = null;
            break label120;
          }
        }
        bool = true;
        label120:
        locale.a(bool);
        return;
      }
    }
  }
  
  public final void a(int paramInt)
  {
    int i1 = g;
    int i2 = -1;
    if (i1 != i2) {
      b(i1);
    }
    g = paramInt;
    Object localObject = q.a.a;
    i2 = 0;
    w[] arrayOfw = null;
    a(paramInt, (q.a)localObject, false);
    localObject = b[paramInt];
    if (localObject != null)
    {
      localObject = a;
      if (localObject != null)
      {
        ((e)localObject).f();
        arrayOfw = a;
        w localw = arrayOfw[paramInt];
        if (localw != null)
        {
          localObject = ((e)localObject).b();
          localw.a((Set)localObject);
          return;
        }
        return;
      }
    }
  }
  
  public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    k.b(paramPhonebookFilter, "phonebookFilter");
  }
  
  public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter, int paramInt)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    ContactsHolder.PhonebookFilter[] arrayOfPhonebookFilter = c;
    int i1 = f.c(arrayOfPhonebookFilter, paramPhonebookFilter);
    boolean bool = true;
    q.a locala;
    switch (paramInt)
    {
    default: 
      break;
    case 2: 
      locala = q.a.b;
      a(i1, locala, bool);
      return;
    case 1: 
      locala = q.a.b;
      a(i1, locala, bool);
      return;
    case 0: 
      locala = q.a.b;
      bool = false;
      arrayOfPhonebookFilter = null;
      a(i1, locala, false);
    }
  }
  
  public final void b(int paramInt)
  {
    Object localObject = q.a.a;
    boolean bool1 = true;
    a(paramInt, (q.a)localObject, bool1);
    localObject = b;
    e locale = localObject[paramInt];
    if (locale != null)
    {
      locale = a;
      if (locale != null)
      {
        long l1 = e;
        long l2 = 0L;
        boolean bool2 = l1 < l2;
        if (!bool2) {
          locale.d();
        } else {
          locale.a(l1);
        }
      }
    }
    g = -1;
  }
  
  public final void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    k.b(paramViewGroup, "container");
    k.b(paramObject, "object");
    paramObject = (View)paramObject;
    paramViewGroup.removeView((View)paramObject);
  }
  
  public final void g() {}
  
  public final int getCount()
  {
    r localr = m;
    boolean bool = localr.c();
    if (bool) {
      return 2;
    }
    return 1;
  }
  
  public final CharSequence getPageTitle(int paramInt)
  {
    return (CharSequence)k.a(2130903065)[paramInt];
  }
  
  public final Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "container");
    Object localObject1 = LayoutInflater.from(paramViewGroup.getContext());
    int i1 = 0;
    int i2 = 2131558534;
    View localView = ((LayoutInflater)localObject1).inflate(i2, paramViewGroup, false);
    paramViewGroup.addView(localView);
    ContactsHolder.PhonebookFilter localPhonebookFilter = c[paramInt];
    paramViewGroup = j.a(localPhonebookFilter);
    localObject1 = new com/truecaller/calling/contacts_list/w;
    Object localObject2 = this;
    localObject2 = (p.c.a)this;
    k.a(localView, "pageView");
    b.b localb = n;
    ContactsHolder localContactsHolder = h;
    y localy = l;
    com.truecaller.ads.b.w localw = b;
    Object localObject3 = localObject1;
    ((w)localObject1).<init>((p.c.a)localObject2, localView, localb, localPhonebookFilter, localContactsHolder, localy, localw);
    a[paramInt] = localObject1;
    b[paramInt] = paramViewGroup;
    Object localObject4 = a;
    int i3 = g;
    int i4 = -1;
    if (i3 == i4)
    {
      if (paramInt == 0)
      {
        g = 0;
        ((e)localObject4).a(false);
      }
    }
    else if (paramInt == i3) {
      ((e)localObject4).a(false);
    }
    paramViewGroup = a;
    Object localObject5 = new com/truecaller/calling/contacts_list/q$c;
    ((q.c)localObject5).<init>((w)localObject1, paramViewGroup);
    localObject5 = (g)localObject5;
    paramViewGroup.a((g)localObject5);
    boolean bool = f;
    if (!bool)
    {
      paramViewGroup = a;
      paramInt = getCount();
      localObject4 = "receiver$0";
      k.b(paramViewGroup, (String)localObject4);
      i2 = 1;
      if (paramInt >= 0)
      {
        i3 = 1;
      }
      else
      {
        i3 = 0;
        localObject3 = null;
      }
      if (i3 != 0)
      {
        if (paramInt == 0)
        {
          paramViewGroup = (List)c.a.y.a;
        }
        else
        {
          i3 = paramViewGroup.length;
          if (paramInt >= i3)
          {
            paramViewGroup = f.f(paramViewGroup);
          }
          else if (paramInt == i2)
          {
            paramViewGroup = m.a(paramViewGroup[0]);
          }
          else
          {
            localObject3 = new java/util/ArrayList;
            ((ArrayList)localObject3).<init>(paramInt);
            i4 = paramViewGroup.length;
            int i5 = 0;
            localView = null;
            int i6 = 0;
            localb = null;
            while (i5 < i4)
            {
              localPhonebookFilter = paramViewGroup[i5];
              int i7 = i6 + 1;
              if (i6 == paramInt) {
                break;
              }
              ((ArrayList)localObject3).add(localPhonebookFilter);
              i5 += 1;
              i6 = i7;
            }
            paramViewGroup = (ViewGroup)localObject3;
            paramViewGroup = (List)localObject3;
          }
        }
        paramViewGroup = (Iterable)paramViewGroup;
        paramInt = paramViewGroup instanceof Collection;
        if (paramInt != 0)
        {
          localObject5 = paramViewGroup;
          localObject5 = (Collection)paramViewGroup;
          paramInt = ((Collection)localObject5).isEmpty();
          if (paramInt != 0) {}
        }
        else
        {
          paramViewGroup = ((Iterable)paramViewGroup).iterator();
          do
          {
            paramInt = paramViewGroup.hasNext();
            if (paramInt == 0) {
              break;
            }
            localObject5 = (w)paramViewGroup.next();
            if (localObject5 != null)
            {
              paramInt = 1;
            }
            else
            {
              paramInt = 0;
              localObject5 = null;
            }
          } while (paramInt != 0);
          break label502;
        }
        i1 = 1;
        label502:
        if (i1 != 0)
        {
          f = i2;
          paramViewGroup = i;
          localObject5 = new com/truecaller/calling/contacts_list/q$b;
          ((q.b)localObject5).<init>(this);
          paramViewGroup.a(localObject5);
        }
      }
      else
      {
        paramViewGroup = new java/lang/StringBuilder;
        paramViewGroup.<init>("Requested element count ");
        paramViewGroup.append(paramInt);
        paramViewGroup.append(" is less than zero.");
        paramViewGroup = paramViewGroup.toString();
        localObject5 = new java/lang/IllegalArgumentException;
        paramViewGroup = paramViewGroup.toString();
        ((IllegalArgumentException)localObject5).<init>(paramViewGroup);
        throw ((Throwable)localObject5);
      }
    }
    return localObject1;
  }
  
  public final boolean isViewFromObject(View paramView, Object paramObject)
  {
    k.b(paramView, "view");
    k.b(paramObject, "object");
    w[] arrayOfw = a;
    int i1 = arrayOfw.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Object localObject = arrayOfw[i2];
      boolean bool1 = k.a(localObject, paramObject);
      boolean bool2 = true;
      if (bool1)
      {
        localObject = c;
        bool3 = k.a(localObject, paramView);
        if (bool3)
        {
          bool3 = true;
          break label86;
        }
      }
      boolean bool3 = false;
      localObject = null;
      label86:
      if (bool3) {
        return bool2;
      }
      i2 += 1;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */