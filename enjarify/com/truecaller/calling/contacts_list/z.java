package com.truecaller.calling.contacts_list;

import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.calling.contacts_list.data.g;
import com.truecaller.search.local.model.c;

public final class z
  implements y
{
  final ContactsHolder a;
  private final c b;
  private final ae c;
  private final b d;
  private final g e;
  
  public z(ContactsHolder paramContactsHolder, c paramc, ae paramae, b paramb, g paramg)
  {
    a = paramContactsHolder;
    b = paramc;
    c = paramae;
    d = paramb;
    e = paramg;
  }
  
  public final g.b a(ContactsHolder.PhonebookFilter paramPhonebookFilter, ContactsHolder.FavoritesFilter paramFavoritesFilter)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    k.b(paramFavoritesFilter, "favoritesFilter");
    h localh = new com/truecaller/calling/contacts_list/h;
    Object localObject1 = new com/truecaller/calling/contacts_list/z$a;
    ((z.a)localObject1).<init>(this, paramFavoritesFilter, paramPhonebookFilter);
    Object localObject2 = localObject1;
    localObject2 = (g.a)localObject1;
    c localc = b;
    ae localae = c;
    b localb = d;
    g localg = e;
    localObject1 = localh;
    localh.<init>((g.a)localObject2, localc, localae, localb, localg);
    return (g.b)localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */