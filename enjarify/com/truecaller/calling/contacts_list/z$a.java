package com.truecaller.calling.contacts_list;

import java.util.List;

public final class z$a
  implements g.a
{
  z$a(z paramz, ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter) {}
  
  public final List a()
  {
    ContactsHolder localContactsHolder = a.a;
    ContactsHolder.FavoritesFilter localFavoritesFilter = b;
    ContactsHolder.PhonebookFilter localPhonebookFilter = c;
    return localContactsHolder.a(localFavoritesFilter, localPhonebookFilter);
  }
  
  public final ContactsHolder.SortingMode b()
  {
    return a.a.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.z.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */