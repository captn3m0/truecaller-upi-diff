package com.truecaller.calling.contacts_list;

import java.util.List;

public abstract interface ContactsHolder
{
  public abstract String a(int paramInt, ContactsHolder.PhonebookFilter paramPhonebookFilter);
  
  public abstract List a(ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter);
  
  public abstract void a(ContactsHolder.SortingMode paramSortingMode);
  
  public abstract ContactsHolder.SortingMode b();
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ContactsHolder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */