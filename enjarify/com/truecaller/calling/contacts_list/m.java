package com.truecaller.calling.contacts_list;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final l.a a;
  private final Provider b;
  private final Provider c;
  
  private m(l.a parama, Provider paramProvider1, Provider paramProvider2)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static m a(l.a parama, Provider paramProvider1, Provider paramProvider2)
  {
    m localm = new com/truecaller/calling/contacts_list/m;
    localm.<init>(parama, paramProvider1, paramProvider2);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */