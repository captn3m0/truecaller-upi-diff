package com.truecaller.calling.contacts_list;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.ContactsLoadingMode;
import com.truecaller.calling.contacts_list.data.a.a;
import com.truecaller.data.entity.b;
import com.truecaller.utils.extensions.q;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import kotlinx.coroutines.ag;

final class r$e$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag f;
  
  r$e$a(c paramc, r.e parame, SortedContactsRepository.ContactsLoadingMode paramContactsLoadingMode, long paramLong, q paramq)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/calling/contacts_list/r$e$a;
    r.e locale = b;
    SortedContactsRepository.ContactsLoadingMode localContactsLoadingMode = c;
    long l = d;
    q localq = e;
    locala.<init>(paramc, locale, localContactsLoadingMode, l, localq);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = r.d(b.f);
        localObject1 = ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES;
        Object localObject2 = ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY;
        paramObject = (Iterable)((r.b)paramObject).a((ContactsHolder.FavoritesFilter)localObject1, (ContactsHolder.PhonebookFilter)localObject2);
        localObject1 = new java/util/LinkedHashMap;
        ((LinkedHashMap)localObject1).<init>();
        localObject1 = (Map)localObject1;
        paramObject = ((Iterable)paramObject).iterator();
        Object localObject3;
        Object localObject4;
        for (;;)
        {
          boolean bool2 = ((Iterator)paramObject).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = ((Iterator)paramObject).next();
          localObject3 = localObject2;
          localObject3 = b.c;
          localObject4 = ((Map)localObject1).get(localObject3);
          if (localObject4 == null)
          {
            localObject4 = new java/util/ArrayList;
            ((ArrayList)localObject4).<init>();
            ((Map)localObject1).put(localObject3, localObject4);
          }
          localObject4 = (List)localObject4;
          ((List)localObject4).add(localObject2);
        }
        paramObject = new java/util/ArrayList;
        int j = ((Map)localObject1).size();
        ((ArrayList)paramObject).<init>(j);
        paramObject = (Collection)paramObject;
        localObject1 = ((Map)localObject1).entrySet().iterator();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject1).hasNext();
          if (!bool3) {
            break;
          }
          localObject2 = (Map.Entry)((Iterator)localObject1).next();
          localObject3 = new com/truecaller/calling/contacts_list/data/a$a;
          localObject4 = (String)((Map.Entry)localObject2).getKey();
          localObject2 = (List)((Map.Entry)localObject2).getValue();
          int k = ((List)localObject2).size();
          ((a.a)localObject3).<init>((String)localObject4, k);
          ((Collection)paramObject).add(localObject3);
        }
        return (List)paramObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.r.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */