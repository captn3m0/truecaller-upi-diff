package com.truecaller.calling.contacts_list;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import c.g.a.b;
import c.g.b.k;
import c.k.e;
import c.k.h;
import c.k.i;
import com.truecaller.R.id;
import com.truecaller.R.styleable;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class FastScroller
  extends RelativeLayout
{
  RecyclerView a;
  LinearLayoutManager b;
  b c;
  private final int d;
  private int e;
  private boolean f;
  private HashMap g;
  
  public FastScroller(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    int[] arrayOfInt = R.styleable.FastScroller;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
    int i = paramContext.getDimensionPixelSize(0, 100);
    d = i;
    paramContext.recycle();
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      g = ((HashMap)localObject1);
    }
    localObject1 = g;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = g;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  private final void b()
  {
    boolean bool = f;
    if (!bool)
    {
      RecyclerView localRecyclerView = a;
      if (localRecyclerView == null)
      {
        localObject = "recyclerView";
        k.a((String)localObject);
      }
      int i = localRecyclerView.computeVerticalScrollOffset();
      Object localObject = a;
      if (localObject == null)
      {
        String str = "recyclerView";
        k.a(str);
      }
      int j = ((RecyclerView)localObject).computeVerticalScrollRange();
      float f1 = i;
      float f2 = j;
      f1 /= f2;
      j = getHeight();
      f2 = j * f1;
      setContainerAndScrollBarPosition(f2);
    }
  }
  
  private final void setContainerAndScrollBarPosition(float paramFloat)
  {
    int i = R.id.fast_scroller_bar;
    TintedImageView localTintedImageView = (TintedImageView)a(i);
    k.a(localTintedImageView, "fast_scroller_bar");
    i = localTintedImageView.getHeight();
    int j = R.id.fast_scroller_bubble;
    FrameLayout localFrameLayout = (FrameLayout)a(j);
    k.a(localFrameLayout, "fast_scroller_bubble");
    j = localFrameLayout.getHeight();
    int k = R.id.fast_scroller_bar;
    Object localObject1 = (TintedImageView)a(k);
    k.a(localObject1, "fast_scroller_bar");
    int m = i / 2;
    float f1 = m;
    int n = (int)(paramFloat - f1);
    Object localObject2 = new c/k/h;
    int i1 = getHeight() - i;
    ((h)localObject2).<init>(0, i1);
    localObject2 = (e)localObject2;
    f1 = i.a(n, (e)localObject2);
    ((TintedImageView)localObject1).setY(f1);
    k = R.id.fast_scroller_bubble;
    localObject1 = (FrameLayout)a(k);
    k.a(localObject1, "fast_scroller_bubble");
    f1 = j;
    int i2 = (int)(paramFloat - f1);
    Object localObject3 = new c/k/h;
    int i3 = getHeight() - j - m;
    ((h)localObject3).<init>(0, i3);
    localObject3 = (e)localObject3;
    paramFloat = i.a(i2, (e)localObject3);
    ((FrameLayout)localObject1).setY(paramFloat);
  }
  
  private final void setRecyclerViewPosition(float paramFloat)
  {
    int i = e;
    if (i > 0)
    {
      float f1 = getHeight();
      paramFloat /= f1;
      i = e;
      float f2 = i;
      paramFloat *= f2;
      int j = (int)paramFloat;
      f2 = 0.0F;
      Object localObject1 = (e)i.b(0, i);
      j = i.a(j, (e)localObject1);
      i = 1;
      f1 = Float.MIN_VALUE;
      Object localObject2 = new String[i];
      String str = String.valueOf(j);
      Object localObject3 = "cl: scrollToPositionWithOffset ".concat(str);
      localObject2[0] = localObject3;
      localObject2 = b;
      if (localObject2 == null)
      {
        localObject3 = "layoutManager";
        k.a((String)localObject3);
      }
      ((LinearLayoutManager)localObject2).scrollToPositionWithOffset(j, 0);
      int k = R.id.fast_scroller_bubble_text;
      localObject2 = (TextView)a(k);
      k.a(localObject2, "fast_scroller_bubble_text");
      localObject3 = c;
      if (localObject3 == null)
      {
        str = "indexByPosition";
        k.a(str);
      }
      Object localObject4 = Integer.valueOf(j);
      localObject4 = (CharSequence)((b)localObject3).invoke(localObject4);
      ((TextView)localObject2).setText((CharSequence)localObject4);
      localObject4 = new String[i];
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("cl: indexByPosition, group = ");
      k = R.id.fast_scroller_bubble_text;
      localObject2 = (TextView)a(k);
      ((StringBuilder)localObject1).append(localObject2);
      localObject2 = ".text";
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject1 = ((StringBuilder)localObject1).toString();
      localObject4[0] = localObject1;
    }
  }
  
  public final void a()
  {
    LinearLayoutManager localLinearLayoutManager = b;
    if (localLinearLayoutManager == null)
    {
      localObject1 = "layoutManager";
      k.a((String)localObject1);
    }
    int i = localLinearLayoutManager.findLastVisibleItemPosition();
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "layoutManager";
      k.a((String)localObject2);
    }
    int j = ((LinearLayoutManager)localObject1).findFirstVisibleItemPosition();
    i -= j;
    j = 1;
    i += j;
    Object localObject2 = a;
    if (localObject2 == null)
    {
      str = "recyclerView";
      k.a(str);
    }
    localObject2 = ((RecyclerView)localObject2).getAdapter();
    String str = null;
    if (localObject2 != null)
    {
      k = ((RecyclerView.Adapter)localObject2).getItemCount();
    }
    else
    {
      k = 0;
      localObject2 = null;
    }
    e = k;
    int k = e;
    if (k <= i)
    {
      j = 0;
      localObject1 = null;
    }
    t.a(this, j);
    b();
  }
  
  public final boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    Object localObject = "event";
    k.b(paramMotionEvent, (String)localObject);
    boolean bool1 = f;
    int k = 1;
    if (!bool1)
    {
      int i = getLayoutDirection();
      int m;
      float f2;
      boolean bool2;
      if (i == k)
      {
        f1 = paramMotionEvent.getX();
        m = d;
        f2 = m;
        bool2 = f1 < f2;
        if (bool2)
        {
          bool2 = true;
          f1 = Float.MIN_VALUE;
        }
        else
        {
          bool2 = false;
          f1 = 0.0F;
          localObject = null;
        }
      }
      else
      {
        f1 = paramMotionEvent.getX();
        m = getWidth();
        f2 = m;
        int n = d;
        float f3 = n;
        f2 -= f3;
        bool2 = f1 < f2;
        if (bool2)
        {
          bool2 = true;
          f1 = Float.MIN_VALUE;
        }
        else
        {
          bool2 = false;
          f1 = 0.0F;
          localObject = null;
        }
      }
      if (!bool2) {
        return super.onTouchEvent(paramMotionEvent);
      }
    }
    int j = paramMotionEvent.getAction();
    switch (j)
    {
    default: 
      return super.onTouchEvent(paramMotionEvent);
    case 2: 
      f1 = paramMotionEvent.getY();
      setContainerAndScrollBarPosition(f1);
      f4 = paramMotionEvent.getY();
      setRecyclerViewPosition(f4);
      return k;
    case 1: 
    case 3: 
      f = false;
      int i1 = R.id.fast_scroller_bubble;
      paramMotionEvent = (FrameLayout)a(i1);
      k.a(paramMotionEvent, "fast_scroller_bubble");
      t.c((View)paramMotionEvent);
      i1 = R.id.fast_scroller_bar;
      paramMotionEvent = (TintedImageView)a(i1);
      k.a(paramMotionEvent, "fast_scroller_bar");
      paramMotionEvent.setSelected(false);
      return k;
    }
    f = k;
    j = R.id.fast_scroller_bubble;
    localObject = (FrameLayout)a(j);
    k.a(localObject, "fast_scroller_bubble");
    t.a((View)localObject);
    j = R.id.fast_scroller_bar;
    localObject = (TintedImageView)a(j);
    k.a(localObject, "fast_scroller_bar");
    ((TintedImageView)localObject).setSelected(k);
    float f1 = paramMotionEvent.getY();
    setContainerAndScrollBarPosition(f1);
    float f4 = paramMotionEvent.getY();
    setRecyclerViewPosition(f4);
    return k;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.FastScroller
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */