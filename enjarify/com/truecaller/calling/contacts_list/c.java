package com.truecaller.calling.contacts_list;

import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.backup.e;
import com.truecaller.utils.n;

public final class c
  extends com.truecaller.adapter_delegates.c
  implements b.b
{
  private final b.c.b b;
  private final b.a c;
  private final com.truecaller.i.c d;
  private final e e;
  private final b f;
  private final n g;
  
  public c(b.c.b paramb, b.a parama, com.truecaller.i.c paramc, e parame, b paramb1, n paramn)
  {
    b = paramb;
    c = parama;
    d = paramc;
    e = parame;
    f = paramb1;
    g = paramn;
  }
  
  private final void a(String paramString)
  {
    b localb = f;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("ViewAction");
    paramString = locala.a("Action", paramString).a("Context", "contacts").a();
    k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.b(paramString);
  }
  
  private final void c()
  {
    d.a_("contactListPromoteBackupCount");
    b.s_();
  }
  
  public final void a()
  {
    a("backupPromoClicked");
    c.t_();
    c();
  }
  
  public final void b()
  {
    a("backupPromoDismissed");
    c();
  }
  
  public final int getItemCount()
  {
    Object localObject = d;
    String str = "contactListPromoteBackupCount";
    int i = ((com.truecaller.i.c)localObject).a(str, 0);
    int j = 1;
    if (i <= 0)
    {
      localObject = e;
      bool = ((e)localObject).a();
      if (!bool)
      {
        bool = true;
        break label49;
      }
    }
    boolean bool = false;
    localObject = null;
    label49:
    if (bool) {
      return j;
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */