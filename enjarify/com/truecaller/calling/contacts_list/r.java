package com.truecaller.calling.contacts_list;

import c.d.f;
import c.g.b.k;
import c.l;
import com.truecaller.analytics.b;
import com.truecaller.bc;
import com.truecaller.bd;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.ContactsLoadingMode;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.b;
import com.truecaller.calling.dialer.v;
import com.truecaller.data.entity.Contact;
import com.truecaller.utils.a;
import java.util.List;
import kotlinx.coroutines.bn;

public final class r
  extends bc
  implements p.a
{
  private boolean d;
  private boolean e;
  private boolean f;
  private final r.c g;
  private final r.d h;
  private r.a i;
  private final r.b j;
  private v k;
  private v l;
  private final com.truecaller.utils.extensions.q m;
  private final SortedContactsRepository n;
  private final com.truecaller.i.c o;
  private final f p;
  private final f q;
  private final com.truecaller.common.account.r r;
  private final com.truecaller.search.local.model.c s;
  private final b t;
  private final a u;
  
  public r(SortedContactsRepository paramSortedContactsRepository, com.truecaller.i.c paramc, f paramf1, f paramf2, com.truecaller.common.account.r paramr, com.truecaller.search.local.model.c paramc1, b paramb, a parama)
  {
    super(paramf1);
    n = paramSortedContactsRepository;
    o = paramc;
    p = paramf1;
    q = paramf2;
    r = paramr;
    s = paramc1;
    t = paramb;
    u = parama;
    d = true;
    paramSortedContactsRepository = new com/truecaller/calling/contacts_list/r$c;
    paramSortedContactsRepository.<init>(this);
    g = paramSortedContactsRepository;
    paramSortedContactsRepository = new com/truecaller/calling/contacts_list/r$d;
    paramSortedContactsRepository.<init>(this);
    h = paramSortedContactsRepository;
    paramSortedContactsRepository = new com/truecaller/calling/contacts_list/r$a;
    paramSortedContactsRepository.<init>();
    i = paramSortedContactsRepository;
    paramSortedContactsRepository = new com/truecaller/calling/contacts_list/r$b;
    paramSortedContactsRepository.<init>();
    j = paramSortedContactsRepository;
    paramSortedContactsRepository = new com/truecaller/calling/contacts_list/r$e;
    paramSortedContactsRepository.<init>(this, null);
    paramSortedContactsRepository = (c.g.a.q)paramSortedContactsRepository;
    k.b(this, "receiver$0");
    k.b(paramSortedContactsRepository, "block");
    paramc = new com/truecaller/utils/extensions/q;
    paramc.<init>(this, paramSortedContactsRepository);
    m = paramc;
  }
  
  public final String a(int paramInt, ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    Object localObject1 = i;
    k.b(paramPhonebookFilter, "phonebookFilter");
    localObject1 = a;
    int[] arrayOfInt = s.a;
    int i1 = paramPhonebookFilter.ordinal();
    i1 = arrayOfInt[i1];
    Object localObject2;
    switch (i1)
    {
    default: 
      localObject2 = new c/l;
      ((l)localObject2).<init>();
      throw ((Throwable)localObject2);
    case 2: 
      i1 = 1;
      break;
    case 1: 
      i1 = 0;
      paramPhonebookFilter = null;
    }
    paramPhonebookFilter = localObject1[i1];
    if (paramPhonebookFilter != null)
    {
      localObject2 = paramPhonebookFilter.a(paramInt);
      if (localObject2 == null) {
        localObject2 = "?";
      }
      return (String)localObject2;
    }
    return null;
  }
  
  public final List a(ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    k.b(paramFavoritesFilter, "favoritesFilter");
    k.b(paramPhonebookFilter, "phonebookFilter");
    return j.a(paramFavoritesFilter, paramPhonebookFilter);
  }
  
  public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    ContactsHolder.PhonebookFilter localPhonebookFilter = ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY;
    boolean bool;
    if (paramPhonebookFilter == localPhonebookFilter)
    {
      bool = true;
    }
    else
    {
      bool = false;
      paramPhonebookFilter = null;
    }
    d = bool;
    paramPhonebookFilter = (p.b)c;
    if (paramPhonebookFilter != null)
    {
      paramPhonebookFilter.b();
      return;
    }
  }
  
  public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter, int paramInt)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    k.b(paramPhonebookFilter, "phonebookFilter");
    k.b(paramPhonebookFilter, "phonebookFilter");
  }
  
  public final void a(ContactsHolder.SortingMode paramSortingMode)
  {
    k.b(paramSortingMode, "value");
    com.truecaller.i.c localc = o;
    String str = "sorting_mode";
    int[] arrayOfInt = u.a;
    int i1 = paramSortingMode.ordinal();
    i1 = arrayOfInt[i1];
    switch (i1)
    {
    default: 
      paramSortingMode = new c/l;
      paramSortingMode.<init>();
      throw paramSortingMode;
    case 2: 
      i1 = 1;
      break;
    case 1: 
      i1 = 0;
      paramSortingMode = null;
    }
    localc.b(str, i1);
  }
  
  public final void a(v paramv)
  {
    k.b(paramv, "observer");
    k = paramv;
  }
  
  public final void a(Contact paramContact)
  {
    k.b(paramContact, "contact");
    p.b localb = (p.b)c;
    if (localb != null)
    {
      localb.a(paramContact);
      return;
    }
  }
  
  public final ContactsHolder.SortingMode b()
  {
    com.truecaller.i.c localc = o;
    String str = "sorting_mode";
    int i1 = localc.a(str, 0);
    if (i1 != 0) {
      return ContactsHolder.SortingMode.BY_LAST_NAME;
    }
    return ContactsHolder.SortingMode.BY_FIRST_NAME;
  }
  
  public final void b(v paramv)
  {
    k.b(paramv, "observer");
    l = paramv;
  }
  
  public final void b(Contact paramContact)
  {
    k.b(paramContact, "contact");
    p.b localb = (p.b)c;
    if (localb != null)
    {
      localb.b(paramContact);
      return;
    }
  }
  
  public final void c(Contact paramContact)
  {
    k.b(paramContact, "contact");
    p.b localb = (p.b)c;
    if (localb != null)
    {
      localb.c(paramContact);
      return;
    }
  }
  
  public final boolean e()
  {
    return d;
  }
  
  public final void f()
  {
    com.truecaller.utils.extensions.q localq = m;
    c.n();
    Object localObject = localq.a();
    c = ((bn)localObject);
    localObject = SortedContactsRepository.ContactsLoadingMode.PHONEBOOK_LIMITED;
    localq.a(localObject);
  }
  
  public final void g()
  {
    h();
  }
  
  public final void h()
  {
    p.b localb = (p.b)c;
    if (localb != null)
    {
      localb.h();
      return;
    }
  }
  
  public final void i()
  {
    s.b();
  }
  
  public final void j()
  {
    s.c();
  }
  
  public final void s_()
  {
    p.c localc = (p.c)b;
    if (localc != null)
    {
      localc.s_();
      return;
    }
  }
  
  public final void t_()
  {
    p.b localb = (p.b)c;
    if (localb != null)
    {
      localb.t_();
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    v localv = k;
    if (localv != null) {
      localv.a(null);
    }
    localv = l;
    if (localv != null)
    {
      localv.a(null);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */