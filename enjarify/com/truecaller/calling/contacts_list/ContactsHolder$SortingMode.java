package com.truecaller.calling.contacts_list;

public enum ContactsHolder$SortingMode
{
  static
  {
    SortingMode[] arrayOfSortingMode = new SortingMode[2];
    SortingMode localSortingMode = new com/truecaller/calling/contacts_list/ContactsHolder$SortingMode;
    localSortingMode.<init>("BY_FIRST_NAME", 0);
    BY_FIRST_NAME = localSortingMode;
    arrayOfSortingMode[0] = localSortingMode;
    localSortingMode = new com/truecaller/calling/contacts_list/ContactsHolder$SortingMode;
    int i = 1;
    localSortingMode.<init>("BY_LAST_NAME", i);
    BY_LAST_NAME = localSortingMode;
    arrayOfSortingMode[i] = localSortingMode;
    $VALUES = arrayOfSortingMode;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ContactsHolder.SortingMode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */