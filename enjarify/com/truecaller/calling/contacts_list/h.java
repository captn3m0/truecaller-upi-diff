package com.truecaller.calling.contacts_list;

import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import com.truecaller.calling.contacts_list.data.g;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.e;
import java.util.List;

public final class h
  extends com.truecaller.adapter_delegates.c
  implements g.b
{
  private final g.a b;
  private final com.truecaller.search.local.model.c c;
  private final ae d;
  private final b e;
  private final g f;
  
  public h(g.a parama, com.truecaller.search.local.model.c paramc, ae paramae, b paramb, g paramg)
  {
    b = parama;
    c = paramc;
    d = paramae;
    e = paramb;
    f = paramg;
  }
  
  private final List a()
  {
    return b.a();
  }
  
  private final boolean b(com.truecaller.adapter_delegates.h paramh)
  {
    try
    {
      localObject1 = d;
      localObject2 = a();
      int i = b;
      localObject2 = ((List)localObject2).get(i);
      localObject2 = (SortedContactsDao.b)localObject2;
      localObject2 = a;
      ((ae)localObject1).c((Contact)localObject2);
    }
    catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
    {
      Object localObject1 = new com/truecaller/log/UnmutedException$e;
      int j = b;
      Object localObject2 = a();
      int k = ((List)localObject2).size();
      ((UnmutedException.e)localObject1).<init>(j, k);
      localObject1 = (Throwable)localObject1;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
    }
    return true;
  }
  
  public final boolean a(com.truecaller.adapter_delegates.h paramh)
  {
    k.b(paramh, "event");
    Object localObject1 = a;
    Object localObject2 = "ItemEvent.CLICKED";
    boolean bool1 = k.a(localObject1, localObject2);
    if (bool1) {
      return b(paramh);
    }
    localObject2 = ActionType.SMS.getEventAction();
    bool1 = k.a(localObject1, localObject2);
    boolean bool2 = true;
    int i;
    if (bool1)
    {
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("ViewAction");
      localObject1 = ((e.a)localObject1).a("Action", "message").a("Context", "contacts");
      localObject2 = e;
      localObject1 = ((e.a)localObject1).a();
      k.a(localObject1, "analyticsEvent.build()");
      ((b)localObject2).b((e)localObject1);
      localObject1 = d;
      localObject2 = a();
      i = b;
      paramh = geta;
      ((ae)localObject1).a(paramh);
      return bool2;
    }
    localObject2 = ActionType.VOIP_CALL.getEventAction();
    boolean bool3 = k.a(localObject1, localObject2);
    if (bool3)
    {
      localObject1 = d;
      localObject2 = a();
      i = b;
      paramh = geta;
      ((ae)localObject1).b(paramh);
      return bool2;
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    List localList = a();
    Long localLong = geta.getId();
    if (localLong != null) {
      return localLong.longValue();
    }
    return -1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */