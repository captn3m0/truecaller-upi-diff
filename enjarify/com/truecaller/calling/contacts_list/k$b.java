package com.truecaller.calling.contacts_list;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

final class k$b
  implements DialogInterface.OnClickListener
{
  k$b(k paramk) {}
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    p.a locala = a.d();
    ContactsHolder.SortingMode localSortingMode = ContactsHolder.SortingMode.values()[paramInt];
    locala.a(localSortingMode);
    a.d().f();
    paramDialogInterface.dismiss();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.k.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */