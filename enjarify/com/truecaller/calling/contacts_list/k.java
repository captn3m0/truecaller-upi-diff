package com.truecaller.calling.contacts_list;

import android.arch.lifecycle.e.b;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.f;
import android.support.v4.view.o;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import c.u;
import com.truecaller.R.id;
import com.truecaller.backup.n;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.c.c;
import com.truecaller.calling.c.c.a;
import com.truecaller.calling.dialer.LifecycleAwareCondition;
import com.truecaller.calling.dialer.s;
import com.truecaller.calling.dialer.v;
import com.truecaller.common.ui.b;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.ab;
import com.truecaller.ui.ae;
import com.truecaller.ui.ae.a;
import com.truecaller.ui.components.FloatingActionButton.a;
import com.truecaller.ui.components.FloatingActionButton.c;
import com.truecaller.ui.components.FloatingActionButton.c.a;
import com.truecaller.ui.components.i;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.y;
import com.truecaller.utils.extensions.t;
import com.truecaller.voip.ai;
import java.util.HashMap;
import java.util.List;

public final class k
  extends Fragment
  implements p.b, b, ab, ae, FloatingActionButton.c
{
  public p.a a;
  public s b;
  public s c;
  public q d;
  public ai e;
  private final int f;
  private boolean g = true;
  private HashMap h;
  
  private View a(int paramInt)
  {
    Object localObject1 = h;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      h = ((HashMap)localObject1);
    }
    localObject1 = h;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = h;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    int i = R.id.tabs_layout;
    Object localObject = (TabLayout)a(i);
    String str = "tabs_layout";
    c.g.b.k.a(localObject, str);
    t.b((View)localObject);
    i = 0;
    g = false;
    localObject = getActivity();
    boolean bool = localObject instanceof ae.a;
    if (!bool)
    {
      i = 0;
      localObject = null;
    }
    localObject = (ae.a)localObject;
    if (localObject != null)
    {
      ((ae.a)localObject).d();
      return;
    }
  }
  
  public final void a(Contact paramContact)
  {
    Object localObject = "contact";
    c.g.b.k.b(paramContact, (String)localObject);
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    c.g.b.k.a(localf, "activity ?: return");
    localObject = c.e;
    List localList = paramContact.A();
    c.a.a(localf, paramContact, localList, false, false, true, false, "contacts", 184);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    ((p.a)localObject1).j();
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject2 = "pagerAdapter";
      c.g.b.k.a((String)localObject2);
    }
    int i = R.id.view_pager;
    Object localObject2 = (ViewPager)a(i);
    c.g.b.k.a(localObject2, "view_pager");
    i = ((ViewPager)localObject2).getCurrentItem();
    ((q)localObject1).b(i);
  }
  
  public final void b()
  {
    Object localObject = getActivity();
    boolean bool = localObject instanceof FloatingActionButton.c.a;
    if (!bool) {
      localObject = null;
    }
    localObject = (FloatingActionButton.c.a)localObject;
    if (localObject != null)
    {
      ((FloatingActionButton.c.a)localObject).h();
      return;
    }
  }
  
  public final void b(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    ai localai = e;
    if (localai == null)
    {
      localObject = "voipUtil";
      c.g.b.k.a((String)localObject);
    }
    Object localObject = getActivity();
    localai.a((f)localObject, paramContact, "contacts");
  }
  
  public final void c(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    Context localContext = getContext();
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.Contacts;
    DetailsFragment.b(localContext, paramContact, localSourceType, false, true);
  }
  
  public final p.a d()
  {
    p.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locala;
  }
  
  public final q e()
  {
    q localq = d;
    if (localq == null)
    {
      String str = "pagerAdapter";
      c.g.b.k.a(str);
    }
    return localq;
  }
  
  public final int f()
  {
    return f;
  }
  
  public final boolean g()
  {
    return g;
  }
  
  public final void h()
  {
    y.a((Fragment)this);
  }
  
  public final int i()
  {
    return 2131233811;
  }
  
  public final i[] j()
  {
    return null;
  }
  
  public final FloatingActionButton.a k()
  {
    k.a locala = new com/truecaller/calling/contacts_list/k$a;
    locala.<init>(this);
    return (FloatingActionButton.a)locala;
  }
  
  public final boolean l()
  {
    p.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locala.e();
  }
  
  public final void m() {}
  
  public final void n()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    ((p.a)localObject1).i();
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject2 = "pagerAdapter";
      c.g.b.k.a((String)localObject2);
    }
    int i = R.id.view_pager;
    Object localObject2 = (ViewPager)a(i);
    c.g.b.k.a(localObject2, "view_pager");
    i = ((ViewPager)localObject2).getCurrentItem();
    ((q)localObject1).a(i);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    boolean bool;
    if (paramBundle != null)
    {
      paramBundle = paramBundle.getApplicationContext();
    }
    else
    {
      bool = false;
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().ct().a(this);
      bool = true;
      setHasOptionsMenu(bool);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        c.g.b.k.a((String)localObject1);
      }
      Object localObject1 = b;
      if (localObject1 == null)
      {
        localObject2 = "contactsListObserver";
        c.g.b.k.a((String)localObject2);
      }
      Object localObject2 = new com/truecaller/calling/dialer/LifecycleAwareCondition;
      android.arch.lifecycle.e locale = getLifecycle();
      c.g.b.k.a(locale, "lifecycle");
      e.b localb = e.b.d;
      ((LifecycleAwareCondition)localObject2).<init>(locale, localb);
      localObject2 = (com.truecaller.calling.dialer.q)localObject2;
      ((s)localObject1).a((com.truecaller.calling.dialer.q)localObject2);
      localObject1 = (v)localObject1;
      paramBundle.a((v)localObject1);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        c.g.b.k.a((String)localObject1);
      }
      localObject1 = c;
      if (localObject1 == null)
      {
        localObject2 = "contactsSettingsObserver";
        c.g.b.k.a((String)localObject2);
      }
      localObject2 = new com/truecaller/calling/dialer/LifecycleAwareCondition;
      locale = getLifecycle();
      c.g.b.k.a(locale, "lifecycle");
      localb = e.b.d;
      ((LifecycleAwareCondition)localObject2).<init>(locale, localb);
      localObject2 = (com.truecaller.calling.dialer.q)localObject2;
      ((s)localObject1).a((com.truecaller.calling.dialer.q)localObject2);
      localObject1 = (v)localObject1;
      paramBundle.b((v)localObject1);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        c.g.b.k.a((String)localObject1);
      }
      paramBundle.b(this);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    c.g.b.k.b(paramMenu, "menu");
    c.g.b.k.b(paramMenuInflater, "inflater");
    paramMenuInflater.inflate(2131623942, paramMenu);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131559063, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    p.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.x_();
    super.onDestroy();
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    String str;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((p.a)localObject).y_();
    localObject = d;
    if (localObject == null)
    {
      str = "pagerAdapter";
      c.g.b.k.a(str);
    }
    localObject = b;
    int i = localObject.length;
    int j = 0;
    while (j < i)
    {
      com.truecaller.ads.provider.e locale = localObject[j];
      if (locale != null)
      {
        locale = a;
        if (locale != null)
        {
          locale.e();
          locale.a(null);
        }
      }
      j += 1;
    }
    super.onDestroyView();
    localObject = h;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Object localObject1 = "item";
    c.g.b.k.b(paramMenuItem, (String)localObject1);
    int i = paramMenuItem.getItemId();
    int j = 2131364554;
    if (i != j) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    paramMenuItem = getContext();
    if (paramMenuItem != null)
    {
      localObject1 = new android/support/v7/app/AlertDialog$Builder;
      ((AlertDialog.Builder)localObject1).<init>(paramMenuItem);
      int k = 2130903063;
      p.a locala = a;
      if (locala == null)
      {
        localObject2 = "presenter";
        c.g.b.k.a((String)localObject2);
      }
      j = locala.b().ordinal();
      Object localObject2 = new com/truecaller/calling/contacts_list/k$b;
      ((k.b)localObject2).<init>(this);
      localObject2 = (DialogInterface.OnClickListener)localObject2;
      ((AlertDialog.Builder)localObject1).setSingleChoiceItems(k, j, (DialogInterface.OnClickListener)localObject2).show();
      return true;
    }
    return false;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    int i = R.id.view_pager;
    paramView = (ViewPager)a(i);
    c.g.b.k.a(paramView, "view_pager");
    paramBundle = d;
    if (paramBundle == null)
    {
      String str = "pagerAdapter";
      c.g.b.k.a(str);
    }
    paramBundle = (o)paramBundle;
    paramView.setAdapter(paramBundle);
    i = R.id.view_pager;
    paramView = (ViewPager)a(i);
    paramBundle = new com/truecaller/calling/contacts_list/k$c;
    paramBundle.<init>(this);
    paramBundle = (ViewPager.f)paramBundle;
    paramView.a(paramBundle);
  }
  
  public final void t_()
  {
    n localn = new com/truecaller/backup/n;
    localn.<init>();
    android.support.v4.app.j localj = getFragmentManager();
    String str = n.class.getSimpleName();
    localn.show(localj, str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */