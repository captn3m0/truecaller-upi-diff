package com.truecaller.calling.contacts_list;

public enum ContactsHolder$FavoritesFilter
{
  static
  {
    FavoritesFilter[] arrayOfFavoritesFilter = new FavoritesFilter[2];
    FavoritesFilter localFavoritesFilter = new com/truecaller/calling/contacts_list/ContactsHolder$FavoritesFilter;
    localFavoritesFilter.<init>("INCLUDE_NON_FAVORITES", 0);
    INCLUDE_NON_FAVORITES = localFavoritesFilter;
    arrayOfFavoritesFilter[0] = localFavoritesFilter;
    localFavoritesFilter = new com/truecaller/calling/contacts_list/ContactsHolder$FavoritesFilter;
    int i = 1;
    localFavoritesFilter.<init>("FAVORITES_ONLY", i);
    FAVORITES_ONLY = localFavoritesFilter;
    arrayOfFavoritesFilter[i] = localFavoritesFilter;
    $VALUES = arrayOfFavoritesFilter;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ContactsHolder.FavoritesFilter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */