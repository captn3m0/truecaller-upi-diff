package com.truecaller.calling.contacts_list;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.ContactsLoadingMode;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.a;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.b;
import com.truecaller.calling.contacts_list.data.a.b;
import java.util.List;
import kotlinx.coroutines.g;

final class r$e
  extends c.d.b.a.k
  implements c.g.a.q
{
  Object a;
  Object b;
  Object c;
  long d;
  int e;
  private SortedContactsRepository.ContactsLoadingMode g;
  private com.truecaller.utils.extensions.q h;
  
  r$e(r paramr, c paramc)
  {
    super(3, paramc);
  }
  
  public final Object a(Object paramObject)
  {
    e locale = this;
    Object localObject1 = paramObject;
    Object localObject2 = c.d.a.a.a;
    int i = e;
    int k;
    long l3;
    Object localObject7;
    switch (i)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 2: 
      localObject3 = (SortedContactsRepository.a)c;
      l1 = d;
      localObject4 = (com.truecaller.utils.extensions.q)b;
      localObject5 = (SortedContactsRepository.ContactsLoadingMode)a;
      k = paramObject instanceof o.b;
      if (k == 0) {
        break label582;
      }
      throw a;
    case 1: 
      long l2 = d;
      localObject6 = (com.truecaller.utils.extensions.q)b;
      localObject4 = (SortedContactsRepository.ContactsLoadingMode)a;
      m = paramObject instanceof o.b;
      if (m == 0)
      {
        l3 = l2;
        localObject7 = localObject6;
        localObject8 = localObject4;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      bool = paramObject instanceof o.b;
      if (bool) {
        break label934;
      }
      localObject1 = g;
      localObject3 = h;
      localObject9 = r.b(f);
      l1 = ((com.truecaller.utils.a)localObject9).b();
      localObject4 = r.c(f);
      localObject5 = f.b();
      a = localObject1;
      b = localObject3;
      d = l1;
      k = 1;
      e = k;
      localObject4 = ((SortedContactsRepository)localObject4).a((ContactsHolder.SortingMode)localObject5, (SortedContactsRepository.ContactsLoadingMode)localObject1, this);
      if (localObject4 == localObject2) {
        return localObject2;
      }
      localObject8 = localObject1;
      localObject7 = localObject3;
      l3 = l1;
      localObject1 = localObject4;
    }
    Object localObject10 = localObject1;
    localObject10 = (SortedContactsRepository.a)localObject1;
    localObject1 = r.d(f);
    Object localObject3 = new com/truecaller/calling/contacts_list/r$b;
    Object localObject9 = a;
    ((r.b)localObject3).<init>((List)localObject9);
    c.g.b.k.b(localObject3, "another");
    localObject9 = ContactsHolder.FavoritesFilter.values();
    int i1 = localObject9.length;
    int m = 0;
    Object localObject5 = null;
    while (m < i1)
    {
      localObject11 = localObject9[m];
      localObject12 = ContactsHolder.PhonebookFilter.values();
      int i2 = localObject12.length;
      int i3 = 0;
      localObject4 = null;
      while (i3 < i2)
      {
        localObject13 = localObject9;
        localObject9 = localObject12[i3];
        i4 = i1;
        localObject6 = ((r.b)localObject3).a((ContactsHolder.FavoritesFilter)localObject11, (ContactsHolder.PhonebookFilter)localObject9);
        if (localObject6 != null) {
          ((r.b)localObject1).a((ContactsHolder.FavoritesFilter)localObject11, (ContactsHolder.PhonebookFilter)localObject9, (List)localObject6);
        }
        i3 += 1;
        localObject9 = localObject13;
        i1 = i4;
      }
      Object localObject13 = localObject9;
      int i4 = i1;
      int n;
      m += 1;
    }
    Object localObject12 = r.e(f);
    Object localObject14 = new com/truecaller/calling/contacts_list/r$e$a;
    boolean bool = false;
    localObject3 = null;
    localObject1 = localObject14;
    localObject9 = this;
    Object localObject6 = localObject8;
    Object localObject11 = localObject7;
    ((r.e.a)localObject14).<init>(null, this, (SortedContactsRepository.ContactsLoadingMode)localObject8, l3, (com.truecaller.utils.extensions.q)localObject7);
    localObject14 = (m)localObject14;
    a = localObject8;
    b = localObject7;
    d = l3;
    c = localObject10;
    int i5 = 2;
    e = i5;
    localObject1 = g.a((f)localObject12, (m)localObject14, locale);
    if (localObject1 == localObject2) {
      return localObject2;
    }
    localObject5 = localObject8;
    Object localObject4 = localObject7;
    long l1 = l3;
    localObject3 = localObject10;
    label582:
    localObject1 = (List)localObject1;
    localObject11 = f;
    localObject2 = new com/truecaller/calling/contacts_list/r$a;
    Object localObject8 = new com/truecaller/calling/contacts_list/data/a$b;
    localObject1 = (Iterable)localObject1;
    ((a.b)localObject8).<init>((Iterable)localObject1);
    localObject8 = (SortedContactsRepository.b)localObject8;
    localObject1 = b;
    ((r.a)localObject2).<init>((SortedContactsRepository.b)localObject8, (SortedContactsRepository.b)localObject1);
    r.a((r)localObject11, (r.a)localObject2);
    localObject1 = SortedContactsRepository.ContactsLoadingMode.NON_PHONEBOOK_LIMITED;
    if (localObject5 != localObject1)
    {
      localObject1 = r.f(f);
      if (localObject1 != null)
      {
        localObject3 = ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY;
        localObject11 = r.d(f);
        localObject2 = ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES;
        localObject8 = ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY;
        localObject11 = ((r.b)localObject11).a((ContactsHolder.FavoritesFilter)localObject2, (ContactsHolder.PhonebookFilter)localObject8);
        k = ((List)localObject11).isEmpty();
        ((p.c)localObject1).a((ContactsHolder.PhonebookFilter)localObject3, k);
      }
    }
    localObject1 = SortedContactsRepository.ContactsLoadingMode.PHONEBOOK_LIMITED;
    if (localObject5 != localObject1)
    {
      localObject1 = r.f(f);
      if (localObject1 != null)
      {
        localObject3 = ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY;
        localObject11 = r.d(f);
        localObject2 = ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES;
        localObject8 = ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY;
        localObject11 = ((r.b)localObject11).a((ContactsHolder.FavoritesFilter)localObject2, (ContactsHolder.PhonebookFilter)localObject8);
        k = ((List)localObject11).isEmpty();
        ((p.c)localObject1).a((ContactsHolder.PhonebookFilter)localObject3, k);
      }
    }
    localObject1 = r.f(f);
    if (localObject1 != null) {
      ((p.c)localObject1).b();
    }
    localObject1 = u.b;
    int j = ((SortedContactsRepository.ContactsLoadingMode)localObject5).ordinal();
    i5 = localObject1[j];
    switch (i5)
    {
    default: 
      break;
    case 4: 
      localObject1 = SortedContactsRepository.ContactsLoadingMode.FULL_WITH_ENTITIES;
      ((com.truecaller.utils.extensions.q)localObject4).a(localObject1);
      break;
    case 3: 
      localObject1 = SortedContactsRepository.ContactsLoadingMode.FULL_INITIAL;
      ((com.truecaller.utils.extensions.q)localObject4).a(localObject1);
      break;
    case 2: 
      r.b(f, l1);
      localObject1 = SortedContactsRepository.ContactsLoadingMode.PHONEBOOK_INITIAL;
      ((com.truecaller.utils.extensions.q)localObject4).a(localObject1);
      break;
    case 1: 
      r.a(f, l1);
      localObject1 = SortedContactsRepository.ContactsLoadingMode.NON_PHONEBOOK_LIMITED;
      ((com.truecaller.utils.extensions.q)localObject4).a(localObject1);
    }
    return x.a;
    label934:
    throw a;
  }
  
  public final Object a(Object paramObject1, Object paramObject2, Object paramObject3)
  {
    paramObject1 = (SortedContactsRepository.ContactsLoadingMode)paramObject1;
    paramObject2 = (com.truecaller.utils.extensions.q)paramObject2;
    paramObject3 = (c)paramObject3;
    c.g.b.k.b(paramObject1, "loadingMode");
    c.g.b.k.b(paramObject2, "itself");
    c.g.b.k.b(paramObject3, "continuation");
    e locale = new com/truecaller/calling/contacts_list/r$e;
    r localr = f;
    locale.<init>(localr, (c)paramObject3);
    g = ((SortedContactsRepository.ContactsLoadingMode)paramObject1);
    h = ((com.truecaller.utils.extensions.q)paramObject2);
    locale = (e)locale;
    paramObject1 = x.a;
    return locale.a(paramObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.r.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */