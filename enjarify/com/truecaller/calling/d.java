package com.truecaller.calling;

import android.view.View;
import c.f;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.search.local.model.c.a;
import com.truecaller.ui.view.AvailabilityView;

public final class d
  implements c
{
  private final f b;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(d.class);
    ((u)localObject).<init>(localb, "availability", "getAvailability()Lcom/truecaller/ui/view/AvailabilityView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public d(View paramView)
  {
    paramView = com.truecaller.utils.extensions.t.a(paramView, 2131362067);
    b = paramView;
  }
  
  public final void a(c.a parama)
  {
    ((AvailabilityView)b.b()).a(parama);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */