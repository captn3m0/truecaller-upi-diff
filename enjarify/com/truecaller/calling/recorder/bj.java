package com.truecaller.calling.recorder;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.g;
import com.truecaller.adapter_delegates.p;
import com.truecaller.adapter_delegates.s;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.ui.SingleActivity;
import com.truecaller.ui.SingleActivity.FragmentSingle;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.details.d;
import com.truecaller.ui.q;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public final class bj
  extends d
  implements bq
{
  public static final bj.a f;
  public bl a;
  public ad b;
  public ar c;
  public br d;
  public aj e;
  private RecyclerView g;
  private a h;
  private com.truecaller.adapter_delegates.f i;
  private a l;
  private final Object m;
  private ActionMode n;
  private final g o;
  private final bj.b p;
  private HashMap q;
  
  static
  {
    bj.a locala = new com/truecaller/calling/recorder/bj$a;
    locala.<init>((byte)0);
    f = locala;
  }
  
  public bj()
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    m = localObject;
    localObject = new com/truecaller/adapter_delegates/g;
    ((g)localObject).<init>((byte)0);
    o = ((g)localObject);
    localObject = new com/truecaller/calling/recorder/bj$b;
    ((bj.b)localObject).<init>(this);
    p = ((bj.b)localObject);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = q;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      q = ((HashMap)localObject1);
    }
    localObject1 = q;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = q;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    Object localObject = SingleActivity.FragmentSingle.CALL_RECORDINGS;
    localObject = SingleActivity.a(paramContext, (SingleActivity.FragmentSingle)localObject);
    paramContext.startActivity((Intent)localObject);
  }
  
  public final void A_()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = (AppCompatActivity)localObject;
      ActionMode.Callback localCallback = (ActionMode.Callback)p;
      ((AppCompatActivity)localObject).startSupportActionMode(localCallback);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw ((Throwable)localObject);
  }
  
  public final void B_()
  {
    ActionMode localActionMode = n;
    if (localActionMode != null)
    {
      bj.b localb = p;
      int j = a;
      Object localObject = localActionMode.getTag();
      boolean bool = localObject instanceof Integer;
      if (bool)
      {
        localObject = (Integer)localObject;
        int k = ((Integer)localObject).intValue();
        if (j == k)
        {
          j = 1;
          break label69;
        }
      }
      j = 0;
      localb = null;
      label69:
      if (j == 0) {
        localActionMode = null;
      }
      if (localActionMode != null)
      {
        localActionMode.finish();
        return;
      }
    }
  }
  
  public final void a(Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(paramContact, "contact");
    k.b(paramSourceType, "sourceType");
    android.support.v4.app.f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    DetailsFragment.a((Context)localf, paramContact, paramSourceType, paramBoolean1, paramBoolean2, paramBoolean3);
  }
  
  public final void a(HistoryEvent paramHistoryEvent, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramHistoryEvent, "historyEvent");
    k.b(paramSourceType, "sourceType");
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    Object localObject2 = localObject1;
    localObject2 = (Context)localObject1;
    localObject1 = paramHistoryEvent.s();
    Object localObject3;
    if (localObject1 != null)
    {
      localObject1 = ((Contact)localObject1).getTcId();
      localObject3 = localObject1;
    }
    else
    {
      localObject3 = null;
    }
    localObject1 = paramHistoryEvent.s();
    Object localObject4;
    if (localObject1 != null)
    {
      localObject1 = ((Contact)localObject1).t();
      localObject4 = localObject1;
    }
    else
    {
      localObject4 = null;
    }
    String str1 = paramHistoryEvent.a();
    String str2 = paramHistoryEvent.b();
    String str3 = paramHistoryEvent.d();
    DetailsFragment.a((Context)localObject2, (String)localObject3, (String)localObject4, str1, str2, str3, paramSourceType, paramBoolean1, paramBoolean2);
  }
  
  public final void a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramLaunchContext, "launchContext");
    Object localObject = d;
    if (localObject == null)
    {
      localObject = "premiumScreenNavigator";
      k.a((String)localObject);
    }
    localObject = requireContext();
    k.a(localObject, "requireContext()");
    br.a((Context)localObject, paramLaunchContext, "premiumCallRecording");
  }
  
  public final void a(String paramString, Object paramObject, by paramby)
  {
    k.b(paramString, "message");
    k.b(paramObject, "objectsDeleted");
    k.b(paramby, "caller");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
      localBuilder.<init>((Context)localObject1);
      localObject1 = paramString;
      localObject1 = (CharSequence)paramString;
      localBuilder.setMessage((CharSequence)localObject1);
      Object localObject2 = new com/truecaller/calling/recorder/bj$h;
      ((bj.h)localObject2).<init>(paramString, paramby, paramObject);
      localObject2 = (DialogInterface.OnClickListener)localObject2;
      localBuilder.setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject2);
      localObject2 = new com/truecaller/calling/recorder/bj$i;
      ((bj.i)localObject2).<init>(paramString, paramby, paramObject);
      localObject2 = (DialogInterface.OnClickListener)localObject2;
      localBuilder.setNegativeButton(2131887197, (DialogInterface.OnClickListener)localObject2);
      localBuilder.show();
      return;
    }
  }
  
  public final void a(Set paramSet)
  {
    Object localObject1 = "items";
    k.b(paramSet, (String)localObject1);
    paramSet = ((Iterable)paramSet).iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (Number)paramSet.next();
      int j = ((Number)localObject1).intValue();
      com.truecaller.adapter_delegates.f localf = i;
      if (localf == null)
      {
        localObject2 = "callRecordingsAdapter";
        k.a((String)localObject2);
      }
      Object localObject2 = h;
      if (localObject2 == null)
      {
        String str = "callRecordingsDelegate";
        k.a(str);
      }
      j = ((a)localObject2).a_(j);
      localf.notifyItemChanged(j);
    }
  }
  
  public final void a(boolean paramBoolean, String paramString)
  {
    Object localObject = getActivity();
    if (localObject != null) {
      ((android.support.v4.app.f)localObject).invalidateOptionsMenu();
    }
    int j = R.id.emptyView;
    localObject = (LinearLayout)a(j);
    String str = "emptyView";
    k.a(localObject, str);
    localObject = (View)localObject;
    t.a((View)localObject, paramBoolean);
    if (paramString != null)
    {
      paramBoolean = R.id.emptyText;
      TextView localTextView = (TextView)a(paramBoolean);
      k.a(localTextView, "emptyText");
      paramString = (CharSequence)paramString;
      localTextView.setText(paramString);
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject == null)
    {
      String str = "callRecordingsItemsPresenter";
      k.a(str);
    }
    a = paramBoolean;
    com.truecaller.adapter_delegates.f localf = i;
    if (localf == null)
    {
      localObject = "callRecordingsAdapter";
      k.a((String)localObject);
    }
    localf.notifyDataSetChanged();
  }
  
  public final void c()
  {
    ActionMode localActionMode = n;
    if (localActionMode != null)
    {
      localActionMode.invalidate();
      return;
    }
  }
  
  public final void g()
  {
    Object localObject = i;
    String str;
    if (localObject == null)
    {
      str = "callRecordingsAdapter";
      k.a(str);
    }
    ((com.truecaller.adapter_delegates.f)localObject).notifyDataSetChanged();
    localObject = b;
    if (localObject == null)
    {
      str = "callRecordingsItemsPresenter";
      k.a(str);
    }
    boolean bool = a;
    if (bool) {
      c();
    }
  }
  
  public final void h()
  {
    Context localContext1 = getContext();
    if (localContext1 != null)
    {
      Intent localIntent = new android/content/Intent;
      Context localContext2 = getContext();
      localIntent.<init>(localContext2, CallRecordingSettingsActivity.class);
      localContext1.startActivity(localIntent);
      return;
    }
  }
  
  public final void i()
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      Object localObject2 = e;
      if (localObject2 == null)
      {
        localObject2 = "callRecordingOnBoardingNavigator";
        k.a((String)localObject2);
      }
      k.a(localObject1, "it");
      localObject1 = (Context)localObject1;
      localObject2 = CallRecordingOnBoardingState.WHATS_NEW;
      CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext = CallRecordingOnBoardingLaunchContext.LIST;
      aj.a((Context)localObject1, (CallRecordingOnBoardingState)localObject2, localCallRecordingOnBoardingLaunchContext);
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    String str = "context";
    k.b(paramContext, str);
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof SingleActivity;
    if (bool)
    {
      paramContext = (SingleActivity)paramContext;
      int j = 2131887579;
      paramContext.setTitle(j);
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null) {
      paramBundle = paramBundle.getApplicationContext();
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().cy().a(this);
      paramBundle = new com/truecaller/adapter_delegates/p;
      Object localObject1 = b;
      if (localObject1 == null)
      {
        localObject2 = "callRecordingsItemsPresenter";
        k.a((String)localObject2);
      }
      localObject1 = (com.truecaller.adapter_delegates.b)localObject1;
      int j = 2131559005;
      Object localObject3 = new com/truecaller/calling/recorder/bj$c;
      ((bj.c)localObject3).<init>(this);
      localObject3 = (c.g.a.b)localObject3;
      c.g.a.b localb = (c.g.a.b)bj.d.a;
      paramBundle.<init>((com.truecaller.adapter_delegates.b)localObject1, j, (c.g.a.b)localObject3, localb);
      paramBundle = (a)paramBundle;
      h = paramBundle;
      paramBundle = new com/truecaller/adapter_delegates/p;
      localObject1 = c;
      if (localObject1 == null)
      {
        localObject2 = "promoPresenter";
        k.a((String)localObject2);
      }
      localObject1 = (com.truecaller.adapter_delegates.b)localObject1;
      j = 2131559090;
      localObject3 = new com/truecaller/calling/recorder/bj$e;
      ((bj.e)localObject3).<init>(this);
      localObject3 = (c.g.a.b)localObject3;
      localb = (c.g.a.b)bj.f.a;
      paramBundle.<init>((com.truecaller.adapter_delegates.b)localObject1, j, (c.g.a.b)localObject3, localb);
      paramBundle = (a)paramBundle;
      l = paramBundle;
      paramBundle = new com/truecaller/adapter_delegates/f;
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject2 = "callRecordingsDelegate";
        k.a((String)localObject2);
      }
      Object localObject2 = l;
      if (localObject2 == null)
      {
        localObject3 = "promoDelegate";
        k.a((String)localObject3);
      }
      localObject3 = (s)o;
      localObject1 = (a)((a)localObject1).a((a)localObject2, (s)localObject3);
      paramBundle.<init>((a)localObject1);
      i = paramBundle;
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenuInflater != null)
    {
      paramMenuInflater.inflate(2131623940, paramMenu);
      return;
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558722, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((bl)localObject).y_();
    localObject = q;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem != null)
    {
      int j = paramMenuItem.getItemId();
      int k = 2131361845;
      if (j == k)
      {
        paramMenuItem = a;
        if (paramMenuItem == null)
        {
          String str = "presenter";
          k.a(str);
        }
        paramMenuItem.f();
      }
    }
    return true;
  }
  
  public final void onPrepareOptionsMenu(Menu paramMenu)
  {
    super.onPrepareOptionsMenu(paramMenu);
    if (paramMenu != null)
    {
      int j = 2131361845;
      paramMenu = paramMenu.findItem(j);
      if (paramMenu != null)
      {
        bl localbl = a;
        if (localbl == null)
        {
          String str = "presenter";
          k.a(str);
        }
        boolean bool = localbl.h();
        paramMenu.setVisible(bool);
        return;
      }
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    bl localbl = a;
    if (localbl == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localbl.g();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    paramBundle = paramView.findViewById(2131364092);
    k.a(paramBundle, "view.findViewById(R.id.recyclerView)");
    paramBundle = (RecyclerView)paramBundle;
    g = paramBundle;
    int j = R.id.settingsButton;
    paramBundle = (Button)a(j);
    Object localObject = new com/truecaller/calling/recorder/bj$g;
    ((bj.g)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    paramBundle = g;
    if (paramBundle == null)
    {
      localObject = "list";
      k.a((String)localObject);
    }
    localObject = new android/support/v7/widget/LinearLayoutManager;
    Context localContext = getContext();
    ((LinearLayoutManager)localObject).<init>(localContext);
    localObject = (RecyclerView.LayoutManager)localObject;
    paramBundle.setLayoutManager((RecyclerView.LayoutManager)localObject);
    paramBundle = g;
    if (paramBundle == null)
    {
      localObject = "list";
      k.a((String)localObject);
    }
    localObject = new com/truecaller/ui/q;
    paramView = paramView.getContext();
    int k = 2131559205;
    ((q)localObject).<init>(paramView, k, 0);
    localObject = (RecyclerView.ItemDecoration)localObject;
    paramBundle.addItemDecoration((RecyclerView.ItemDecoration)localObject);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = g;
    if (paramView == null)
    {
      paramBundle = "list";
      k.a(paramBundle);
    }
    paramBundle = i;
    if (paramBundle == null)
    {
      localObject = "callRecordingsAdapter";
      k.a((String)localObject);
    }
    paramBundle = (RecyclerView.Adapter)paramBundle;
    paramView.setAdapter(paramBundle);
    setHasOptionsMenu(true);
  }
  
  public final void toast(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = (Context)getActivity();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 0).show();
  }
  
  public final bl z_()
  {
    bl localbl = a;
    if (localbl == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localbl;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */