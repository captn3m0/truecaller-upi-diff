package com.truecaller.calling.recorder;

public enum CallRecordingOnBoardingState
{
  static
  {
    CallRecordingOnBoardingState[] arrayOfCallRecordingOnBoardingState = new CallRecordingOnBoardingState[7];
    CallRecordingOnBoardingState localCallRecordingOnBoardingState = new com/truecaller/calling/recorder/CallRecordingOnBoardingState;
    localCallRecordingOnBoardingState.<init>("WHATS_NEW", 0);
    WHATS_NEW = localCallRecordingOnBoardingState;
    arrayOfCallRecordingOnBoardingState[0] = localCallRecordingOnBoardingState;
    localCallRecordingOnBoardingState = new com/truecaller/calling/recorder/CallRecordingOnBoardingState;
    int i = 1;
    localCallRecordingOnBoardingState.<init>("WHATS_NEW_NEGATIVE", i);
    WHATS_NEW_NEGATIVE = localCallRecordingOnBoardingState;
    arrayOfCallRecordingOnBoardingState[i] = localCallRecordingOnBoardingState;
    localCallRecordingOnBoardingState = new com/truecaller/calling/recorder/CallRecordingOnBoardingState;
    i = 2;
    localCallRecordingOnBoardingState.<init>("TERMS", i);
    TERMS = localCallRecordingOnBoardingState;
    arrayOfCallRecordingOnBoardingState[i] = localCallRecordingOnBoardingState;
    localCallRecordingOnBoardingState = new com/truecaller/calling/recorder/CallRecordingOnBoardingState;
    i = 3;
    localCallRecordingOnBoardingState.<init>("PERMISSIONS", i);
    PERMISSIONS = localCallRecordingOnBoardingState;
    arrayOfCallRecordingOnBoardingState[i] = localCallRecordingOnBoardingState;
    localCallRecordingOnBoardingState = new com/truecaller/calling/recorder/CallRecordingOnBoardingState;
    i = 4;
    localCallRecordingOnBoardingState.<init>("POST_ENABLE", i);
    POST_ENABLE = localCallRecordingOnBoardingState;
    arrayOfCallRecordingOnBoardingState[i] = localCallRecordingOnBoardingState;
    localCallRecordingOnBoardingState = new com/truecaller/calling/recorder/CallRecordingOnBoardingState;
    i = 5;
    localCallRecordingOnBoardingState.<init>("PAY_WALL", i);
    PAY_WALL = localCallRecordingOnBoardingState;
    arrayOfCallRecordingOnBoardingState[i] = localCallRecordingOnBoardingState;
    localCallRecordingOnBoardingState = new com/truecaller/calling/recorder/CallRecordingOnBoardingState;
    i = 6;
    localCallRecordingOnBoardingState.<init>("PAY_WALL_ON_EXPIRY", i);
    PAY_WALL_ON_EXPIRY = localCallRecordingOnBoardingState;
    arrayOfCallRecordingOnBoardingState[i] = localCallRecordingOnBoardingState;
    $VALUES = arrayOfCallRecordingOnBoardingState;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingOnBoardingState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */