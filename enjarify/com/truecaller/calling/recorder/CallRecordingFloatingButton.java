package com.truecaller.calling.recorder;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.u;
import c.x;
import com.truecaller.R.id;
import com.truecaller.R.styleable;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class CallRecordingFloatingButton
  extends FrameLayout
  implements bw
{
  public CallRecordingFloatingButtonPresenter a;
  private int b;
  private HashMap c;
  
  public CallRecordingFloatingButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private CallRecordingFloatingButton(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    int i = 2131362382;
    b = i;
    Object localObject2 = paramContext.getApplicationContext();
    if (localObject2 != null)
    {
      localObject2 = ((bk)localObject2).a().cy();
      ((b)localObject2).a(this);
      if (paramAttributeSet != null)
      {
        localObject2 = R.styleable.CallRecordingFloatingButton;
        paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject2, 0, 0);
        paramChar = paramAttributeSet.getResourceId(0, i);
        b = paramChar;
        paramAttributeSet.recycle();
      }
      paramContext = LayoutInflater.from(paramContext);
      localObject1 = this;
      localObject1 = (ViewGroup)this;
      paramContext.inflate(2131559200, (ViewGroup)localObject1);
      int j = R.id.callRecordingTCLogo;
      paramContext = (ImageView)a(j);
      k.a(paramContext, "callRecordingTCLogo");
      t.b((View)paramContext);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  private final void a(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i = R.id.callRecordingLabel;
    TextView localTextView = (TextView)a(i);
    paramInt1 = com.truecaller.utils.ui.b.a(getContext(), paramInt1);
    localTextView.setTextColor(paramInt1);
    paramInt1 = R.id.callRecordingButton;
    ((LinearLayout)a(paramInt1)).setBackgroundResource(paramInt2);
    paramInt1 = R.id.callRecordingIconLeft;
    ((ImageView)a(paramInt1)).setImageResource(paramInt3);
    paramInt1 = R.id.callRecordingIconLeft;
    ImageView localImageView = (ImageView)a(paramInt1);
    k.a(localImageView, "callRecordingIconLeft");
    t.a((View)localImageView, paramBoolean1);
    paramInt1 = R.id.callRecordingIconRight;
    localImageView = (ImageView)a(paramInt1);
    k.a(localImageView, "callRecordingIconRight");
    t.a((View)localImageView, paramBoolean2);
  }
  
  public final void a()
  {
    Object localObject = this;
    a(2130968719, 2131230994, 2131234274, true, false);
    localObject = x.a;
    int i = R.id.callRecordingLabel;
    localObject = (TextView)a(i);
    k.a(localObject, "callRecordingLabel");
    ((TextView)localObject).setGravity(8388611);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.callRecordingTCLogo;
    ImageView localImageView = (ImageView)a(i);
    k.a(localImageView, "callRecordingTCLogo");
    t.a((View)localImageView, paramBoolean);
  }
  
  public final void b()
  {
    Object localObject = this;
    a(2130968718, 2131230995, 2131234274, false, true);
    localObject = x.a;
    int i = R.id.callRecordingLabel;
    localObject = (TextView)a(i);
    k.a(localObject, "callRecordingLabel");
    ((TextView)localObject).setGravity(8388613);
  }
  
  public final void c()
  {
    Object localObject = this;
    a(2130968719, 2131230994, 2131234389, true, false);
    localObject = x.a;
    int i = R.id.callRecordingLabel;
    localObject = (TextView)a(i);
    k.a(localObject, "callRecordingLabel");
    ((TextView)localObject).setGravity(8388611);
  }
  
  public final void d()
  {
    t.b(this);
  }
  
  public final CallRecordingFloatingButtonPresenter getPresenter()
  {
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localCallRecordingFloatingButtonPresenter;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null)
    {
      localObject1 = "presenter";
      k.a((String)localObject1);
    }
    Object localObject1 = this;
    localObject1 = (bw)this;
    localCallRecordingFloatingButtonPresenter.a((bw)localObject1);
    localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null)
    {
      localObject1 = "presenter";
      k.a((String)localObject1);
    }
    int i = b;
    d = i;
    Object localObject2 = i;
    boolean bool = ((CallRecordingManager)localObject2).f();
    if (bool)
    {
      localCallRecordingFloatingButtonPresenter.b();
      return;
    }
    int j = 2131362381;
    if (i == j)
    {
      localObject1 = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.ENDED;
      c = ((CallRecordingFloatingButtonPresenter.CallRecordingButtonMode)localObject1);
      localObject1 = (ag)bg.a;
      localObject2 = k;
      Object localObject3 = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$g;
      ((CallRecordingFloatingButtonPresenter.g)localObject3).<init>(localCallRecordingFloatingButtonPresenter, null);
      localObject3 = (m)localObject3;
      int k = 2;
      e.b((ag)localObject1, (f)localObject2, (m)localObject3, k);
    }
    localCallRecordingFloatingButtonPresenter.c();
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localCallRecordingFloatingButtonPresenter.y_();
  }
  
  public final void setCallRecording(CallRecording paramCallRecording)
  {
    k.b(paramCallRecording, "callRecording");
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null)
    {
      localObject1 = "presenter";
      k.a((String)localObject1);
    }
    Object localObject1 = "callRecording";
    k.b(paramCallRecording, (String)localObject1);
    int i = d;
    int j = 2131362381;
    if (i == j)
    {
      localObject1 = (ag)bg.a;
      f localf = k;
      Object localObject2 = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$f;
      ((CallRecordingFloatingButtonPresenter.f)localObject2).<init>(localCallRecordingFloatingButtonPresenter, paramCallRecording, null);
      localObject2 = (m)localObject2;
      int k = 2;
      e.b((ag)localObject1, localf, (m)localObject2, k);
    }
  }
  
  public final void setLabel(String paramString)
  {
    k.b(paramString, "label");
    int i = R.id.callRecordingLabel;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "callRecordingLabel");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void setPhoneNumber(String paramString)
  {
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null)
    {
      String str = "presenter";
      k.a(str);
    }
    h = paramString;
  }
  
  public final void setPresenter(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter)
  {
    k.b(paramCallRecordingFloatingButtonPresenter, "<set-?>");
    a = paramCallRecordingFloatingButtonPresenter;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */