package com.truecaller.calling.recorder;

import dagger.a.d;
import javax.inject.Provider;

public final class be
  implements d
{
  private final bd a;
  private final Provider b;
  
  private be(bd parambd, Provider paramProvider)
  {
    a = parambd;
    b = paramProvider;
  }
  
  public static be a(bd parambd, Provider paramProvider)
  {
    be localbe = new com/truecaller/calling/recorder/be;
    localbe.<init>(parambd, paramProvider);
    return localbe;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.be
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */