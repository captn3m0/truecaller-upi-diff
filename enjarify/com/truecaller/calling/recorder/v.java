package com.truecaller.calling.recorder;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;
import c.g.b.k;
import com.truecaller.common.h.ai;
import java.io.File;

public final class v
  implements u
{
  private final Context a;
  
  public v(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Intent a(String paramString)
  {
    k.b(paramString, "path");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.VIEW");
    localIntent.setFlags(268435456);
    Context localContext = a;
    String str = ai.a(localContext);
    File localFile = new java/io/File;
    localFile.<init>(paramString);
    paramString = FileProvider.a(localContext, str, localFile);
    localIntent.setDataAndType(paramString, "audio/*");
    localIntent.addFlags(1);
    return localIntent;
  }
  
  public final boolean a(Intent paramIntent)
  {
    Object localObject = "intent";
    k.b(paramIntent, (String)localObject);
    try
    {
      localObject = a;
      ((Context)localObject).startActivity(paramIntent);
      return true;
    }
    catch (ActivityNotFoundException localActivityNotFoundException) {}
    return false;
  }
  
  public final Intent b(String paramString)
  {
    k.b(paramString, "path");
    paramString = a(paramString);
    paramString.setAction("android.intent.action.SEND");
    Parcelable localParcelable = (Parcelable)paramString.getData();
    paramString.putExtra("android.intent.extra.STREAM", localParcelable);
    CharSequence localCharSequence = (CharSequence)a.getString(2131887198);
    paramString = Intent.createChooser(paramString, localCharSequence);
    k.a(paramString, "this");
    paramString.setFlags(268435456);
    k.a(paramString, "Intent.createChooser(sha….FLAG_ACTIVITY_NEW_TASK }");
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */