package com.truecaller.calling.recorder;

public enum CallRecordingManager$PlaybackLaunchContext
{
  static
  {
    PlaybackLaunchContext[] arrayOfPlaybackLaunchContext = new PlaybackLaunchContext[3];
    PlaybackLaunchContext localPlaybackLaunchContext = new com/truecaller/calling/recorder/CallRecordingManager$PlaybackLaunchContext;
    localPlaybackLaunchContext.<init>("CALL_LIST", 0);
    CALL_LIST = localPlaybackLaunchContext;
    arrayOfPlaybackLaunchContext[0] = localPlaybackLaunchContext;
    localPlaybackLaunchContext = new com/truecaller/calling/recorder/CallRecordingManager$PlaybackLaunchContext;
    int i = 1;
    localPlaybackLaunchContext.<init>("AFTER_CALL", i);
    AFTER_CALL = localPlaybackLaunchContext;
    arrayOfPlaybackLaunchContext[i] = localPlaybackLaunchContext;
    localPlaybackLaunchContext = new com/truecaller/calling/recorder/CallRecordingManager$PlaybackLaunchContext;
    i = 2;
    localPlaybackLaunchContext.<init>("RECORDINGS", i);
    RECORDINGS = localPlaybackLaunchContext;
    arrayOfPlaybackLaunchContext[i] = localPlaybackLaunchContext;
    $VALUES = arrayOfPlaybackLaunchContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingManager.PlaybackLaunchContext
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */