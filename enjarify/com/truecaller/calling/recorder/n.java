package com.truecaller.calling.recorder;

import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private n(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static n a(Provider paramProvider1, Provider paramProvider2)
  {
    n localn = new com/truecaller/calling/recorder/n;
    localn.<init>(paramProvider1, paramProvider2);
    return localn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */