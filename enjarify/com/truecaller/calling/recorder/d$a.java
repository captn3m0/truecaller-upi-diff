package com.truecaller.calling.recorder;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.CallRecording;

final class d$a
  extends u
{
  private final CallRecording b;
  
  private d$a(e parame, CallRecording paramCallRecording)
  {
    super(parame);
    b = paramCallRecording;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".getDuration(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */