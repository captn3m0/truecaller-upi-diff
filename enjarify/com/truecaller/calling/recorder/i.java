package com.truecaller.calling.recorder;

import c.l.g;
import c.n.m;
import com.truecaller.common.g.a;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.featuretoggles.f;
import com.truecaller.util.al;
import com.truecaller.utils.d;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class i
  implements h
{
  private final e a;
  private final al b;
  private final d c;
  private final a d;
  
  public i(e parame, al paramal, d paramd, a parama)
  {
    a = parame;
    b = paramal;
    c = paramd;
    d = parama;
  }
  
  private boolean e()
  {
    String str = c.l();
    Object localObject1 = (CharSequence)a.ak().e();
    Object localObject2 = { "," };
    int i = 4;
    boolean bool1 = true;
    localObject1 = m.c((CharSequence)localObject1, (String[])localObject2, bool1, i).iterator();
    boolean bool2;
    do
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      bool2 = m.a(str, (String)localObject2, bool1);
    } while (!bool2);
    return bool1;
    str = c.m();
    localObject1 = (CharSequence)a.al().e();
    localObject2 = new String[] { "," };
    localObject1 = m.c((CharSequence)localObject1, (String[])localObject2, bool1, i).iterator();
    do
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      bool2 = m.a(str, (String)localObject2, bool1);
    } while (!bool2);
    return bool1;
    return false;
  }
  
  private boolean f()
  {
    d locald = c;
    int i = locald.h();
    int j = 19;
    return i >= j;
  }
  
  private boolean g()
  {
    Pattern localPattern = Pattern.compile(a.am().e());
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    int i = c.h();
    ((StringBuilder)localObject).append(i);
    ((StringBuilder)localObject).append(' ');
    String str = c.l();
    ((StringBuilder)localObject).append(str);
    localObject = (CharSequence)((StringBuilder)localObject).toString();
    return localPattern.matcher((CharSequence)localObject).matches();
  }
  
  public final boolean a()
  {
    Object localObject = b;
    boolean bool = ((al)localObject).a();
    if (bool)
    {
      localObject = a.s();
      bool = ((b)localObject).a();
      if (bool)
      {
        bool = e();
        if (!bool)
        {
          bool = f();
          if (bool)
          {
            bool = g();
            if (!bool) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  public final boolean b()
  {
    e locale = a;
    e.a locala = Q;
    g localg = e.a[113];
    return ((f)locala.a(locale, localg)).a();
  }
  
  public final boolean c()
  {
    e locale = a;
    e.a locala = R;
    g localg = e.a[114];
    return ((f)locala.a(locale, localg)).a();
  }
  
  public final CallRecordingSettingsMvp.Configuration d()
  {
    Object localObject = c;
    int i = ((d)localObject).h();
    int j = 21;
    if (i >= j)
    {
      localObject = d;
      String str1 = "";
      localObject = ((a)localObject).b("callRecordingConfiguration", str1);
      String str2 = CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER.toString();
      boolean bool2 = true;
      boolean bool1 = m.a((String)localObject, str2, bool2);
      if (!bool1) {
        return CallRecordingSettingsMvp.Configuration.DEFAULT;
      }
    }
    return CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CallRecordingFeatureHelper: Feature enabled: ");
    boolean bool = a.s().a();
    localStringBuilder.append(bool);
    localStringBuilder.append(" \nBlack listed: ");
    bool = e();
    localStringBuilder.append(bool);
    localStringBuilder.append(" \nAndroid version supported: ");
    bool = f();
    localStringBuilder.append(bool);
    localStringBuilder.append(' ');
    localStringBuilder.append("\nDoes device match blacklist regex: ");
    bool = g();
    localStringBuilder.append(bool);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */