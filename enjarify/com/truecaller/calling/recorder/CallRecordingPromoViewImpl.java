package com.truecaller.calling.recorder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;

public final class CallRecordingPromoViewImpl
  extends FrameLayout
  implements av, bu, cb
{
  public ar a;
  public com.truecaller.premium.br b;
  public aj c;
  private TextView d;
  private TextView e;
  private TextView f;
  private ImageView g;
  
  public CallRecordingPromoViewImpl(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private CallRecordingPromoViewImpl(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = br.a();
    Object localObject1 = paramContext.getApplicationContext();
    if (localObject1 != null)
    {
      localObject1 = ((bk)localObject1).a();
      paramAttributeSet = paramAttributeSet.a((bp)localObject1);
      localObject1 = new com/truecaller/calling/recorder/ap;
      Object localObject2 = this;
      localObject2 = (cb)this;
      Object localObject3 = this;
      localObject3 = (bu)this;
      ((ap)localObject1).<init>((cb)localObject2, (bu)localObject3);
      paramAttributeSet.a((ap)localObject1).a().a(this);
      paramContext = LayoutInflater.from(paramContext);
      localObject1 = this;
      localObject1 = (ViewGroup)this;
      paramContext.inflate(2131559090, (ViewGroup)localObject1);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
  
  public final void a()
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = "callRecordingOnBoardingNavigator";
      k.a((String)localObject);
    }
    localObject = getContext();
    k.a(localObject, "context");
    CallRecordingOnBoardingState localCallRecordingOnBoardingState = CallRecordingOnBoardingState.WHATS_NEW;
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext = CallRecordingOnBoardingLaunchContext.PROMO;
    aj.a((Context)localObject, localCallRecordingOnBoardingState, localCallRecordingOnBoardingLaunchContext);
  }
  
  public final void a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    Object localObject = "launchContext";
    k.b(paramLaunchContext, (String)localObject);
    paramLaunchContext = b;
    if (paramLaunchContext == null)
    {
      paramLaunchContext = "premiumScreenNavigator";
      k.a(paramLaunchContext);
    }
    paramLaunchContext = getContext();
    k.a(paramLaunchContext, "context");
    localObject = PremiumPresenterView.LaunchContext.CALL_RECORDINGS;
    com.truecaller.premium.br.a(paramLaunchContext, (PremiumPresenterView.LaunchContext)localObject, "premiumCallRecording");
  }
  
  public final aj getCallRecordingOnBoardingNavigator()
  {
    aj localaj = c;
    if (localaj == null)
    {
      String str = "callRecordingOnBoardingNavigator";
      k.a(str);
    }
    return localaj;
  }
  
  public final com.truecaller.premium.br getPremiumScreenNavigator()
  {
    com.truecaller.premium.br localbr = b;
    if (localbr == null)
    {
      String str = "premiumScreenNavigator";
      k.a(str);
    }
    return localbr;
  }
  
  public final ar getPresenter()
  {
    ar localar = a;
    if (localar == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localar;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    ar localar = a;
    String str;
    if (localar == null)
    {
      str = "presenter";
      k.a(str);
    }
    localar.c(this);
    localar = a;
    if (localar == null)
    {
      str = "presenter";
      k.a(str);
    }
    localar.a(this, 0);
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    ar localar = a;
    if (localar == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localar.d(this);
  }
  
  protected final void onFinishInflate()
  {
    super.onFinishInflate();
    Object localObject1 = (TextView)findViewById(2131362347);
    d = ((TextView)localObject1);
    localObject1 = (TextView)findViewById(2131364029);
    e = ((TextView)localObject1);
    localObject1 = (TextView)findViewById(2131364028);
    f = ((TextView)localObject1);
    int i = 2131364026;
    localObject1 = (ImageView)findViewById(i);
    g = ((ImageView)localObject1);
    localObject1 = e;
    if (localObject1 != null)
    {
      int j = 2;
      ((TextView)localObject1).setMaxLines(j);
    }
    localObject1 = d;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = getContext();
      int k = 2131887234;
      localObject2 = (CharSequence)((Context)localObject2).getString(k);
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = new com/truecaller/calling/recorder/CallRecordingPromoViewImpl$a;
      ((CallRecordingPromoViewImpl.a)localObject2).<init>(this);
      localObject2 = (View.OnClickListener)localObject2;
      ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      ((ImageView)localObject1).setImageResource(2131233934);
      return;
    }
  }
  
  public final void setCTATitle(String paramString)
  {
    k.b(paramString, "ctaTitle");
    TextView localTextView = d;
    if (localTextView != null)
    {
      paramString = (CharSequence)paramString;
      localTextView.setText(paramString);
      return;
    }
  }
  
  public final void setCallRecordingOnBoardingNavigator(aj paramaj)
  {
    k.b(paramaj, "<set-?>");
    c = paramaj;
  }
  
  public final void setPremiumScreenNavigator(com.truecaller.premium.br parambr)
  {
    k.b(parambr, "<set-?>");
    b = parambr;
  }
  
  public final void setPresenter(ar paramar)
  {
    k.b(paramar, "<set-?>");
    a = paramar;
  }
  
  public final void setText(String paramString)
  {
    k.b(paramString, "text");
    TextView localTextView = f;
    if (localTextView != null)
    {
      paramString = (CharSequence)paramString;
      localTextView.setText(paramString);
      return;
    }
  }
  
  public final void setTitle(String paramString)
  {
    k.b(paramString, "title");
    TextView localTextView = e;
    if (localTextView != null)
    {
      paramString = (CharSequence)paramString;
      localTextView.setText(paramString);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingPromoViewImpl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */