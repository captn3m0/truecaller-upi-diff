package com.truecaller.calling.recorder;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;

public final class aw
  extends RecyclerView.ViewHolder
  implements av
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final ar f;
  
  static
  {
    g[] arrayOfg = new g[4];
    Object localObject = new c/g/b/u;
    b localb = w.a(aw.class);
    ((u)localObject).<init>(localb, "upgradeView", "getUpgradeView()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(aw.class);
    ((u)localObject).<init>(localb, "titleView", "getTitleView()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(aw.class);
    ((u)localObject).<init>(localb, "textView", "getTextView()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(aw.class);
    ((u)localObject).<init>(localb, "iconView", "getIconView()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[3] = localObject;
    a = arrayOfg;
  }
  
  public aw(View paramView, ar paramar)
  {
    super(paramView);
    f = paramar;
    paramar = com.truecaller.utils.extensions.t.a(paramView, 2131362347);
    b = paramar;
    paramar = com.truecaller.utils.extensions.t.a(paramView, 2131364029);
    c = paramar;
    paramar = com.truecaller.utils.extensions.t.a(paramView, 2131364028);
    d = paramar;
    paramar = com.truecaller.utils.extensions.t.a(paramView, 2131364026);
    e = paramar;
    paramar = b();
    k.a(paramar, "titleView");
    paramar.setMaxLines(2);
    paramar = a();
    k.a(paramar, "upgradeView");
    paramView = (CharSequence)paramView.getContext().getString(2131887234);
    paramar.setText(paramView);
    paramView = a();
    paramar = new com/truecaller/calling/recorder/aw$1;
    paramar.<init>(this);
    paramar = (View.OnClickListener)paramar;
    paramView.setOnClickListener(paramar);
    ((ImageView)e.b()).setImageResource(2131233934);
  }
  
  private final TextView a()
  {
    return (TextView)b.b();
  }
  
  private final TextView b()
  {
    return (TextView)c.b();
  }
  
  public final void setCTATitle(String paramString)
  {
    k.b(paramString, "ctaTitle");
    TextView localTextView = a();
    if (localTextView != null)
    {
      paramString = (CharSequence)paramString;
      localTextView.setText(paramString);
      return;
    }
  }
  
  public final void setText(String paramString)
  {
    k.b(paramString, "text");
    TextView localTextView = (TextView)d.b();
    k.a(localTextView, "textView");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void setTitle(String paramString)
  {
    k.b(paramString, "title");
    TextView localTextView = b();
    k.a(localTextView, "titleView");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.aw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */