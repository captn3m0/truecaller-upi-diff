package com.truecaller.calling.recorder;

import android.database.Cursor;
import c.a.m;
import c.a.y;
import c.g.a.b;
import c.l.g;
import com.truecaller.androidactors.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.bb;
import com.truecaller.callhistory.r;
import com.truecaller.callhistory.z;
import com.truecaller.calling.dialer.bu;
import com.truecaller.calling.dialer.v;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.utils.n;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public final class bm
  extends bb
  implements bl
{
  final Set a;
  private z c;
  private a d;
  private a e;
  private final f f;
  private final bu g;
  private final n h;
  private final v i;
  private final bv j;
  private final com.truecaller.androidactors.k k;
  
  public bm(f paramf, bu parambu, n paramn, v paramv, bv parambv, com.truecaller.androidactors.k paramk)
  {
    f = paramf;
    g = parambu;
    h = paramn;
    i = paramv;
    j = parambv;
    k = paramk;
    paramf = new java/util/LinkedHashSet;
    paramf.<init>();
    paramf = (Set)paramf;
    a = paramf;
  }
  
  private final b i()
  {
    bm.b localb = new com/truecaller/calling/recorder/bm$b;
    localb.<init>(this);
    return (b)localb;
  }
  
  private final int j()
  {
    z localz = c;
    if (localz != null) {
      return localz.getCount();
    }
    return 0;
  }
  
  public final void D_()
  {
    Object localObject1 = ((r)f.a()).a();
    i locali = k.a();
    b localb = i();
    Object localObject2 = new com/truecaller/calling/recorder/bn;
    ((bn)localObject2).<init>(localb);
    localObject2 = (ac)localObject2;
    localObject1 = ((w)localObject1).a(locali, (ac)localObject2);
    d = ((a)localObject1);
  }
  
  public final z a(ad paramad, g paramg)
  {
    c.g.b.k.b(paramad, "callRecordingItemsPresenter");
    c.g.b.k.b(paramg, "property");
    return c;
  }
  
  public final void a()
  {
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.i();
      return;
    }
  }
  
  public final void a(int paramInt) {}
  
  public final void a(Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    c.g.b.k.b(paramContact, "contact");
    c.g.b.k.b(paramSourceType, "sourceType");
    Object localObject1 = b;
    Object localObject2 = localObject1;
    localObject2 = (bq)localObject1;
    if (localObject2 != null)
    {
      ((bq)localObject2).a(paramContact, paramSourceType, paramBoolean1, paramBoolean2, paramBoolean3);
      return;
    }
  }
  
  public final void a(HistoryEvent paramHistoryEvent, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2)
  {
    c.g.b.k.b(paramHistoryEvent, "historyEvent");
    c.g.b.k.b(paramSourceType, "sourceType");
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.a(paramHistoryEvent, paramSourceType, paramBoolean1, paramBoolean2);
      return;
    }
  }
  
  public final void a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    c.g.b.k.b(paramLaunchContext, "launchContext");
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.a(paramLaunchContext);
      return;
    }
  }
  
  public final void a(Object paramObject, by paramby)
  {
    c.g.b.k.b(paramObject, "objectsDeleted");
    c.g.b.k.b(paramby, "eventListener");
    bq localbq = (bq)b;
    if (localbq != null)
    {
      Object localObject = h;
      Object[] arrayOfObject = new Object[0];
      localObject = ((n)localObject).a(2131887561, arrayOfObject);
      c.g.b.k.a(localObject, "resourceProvider.getStri…_menu_delete_prompt_text)");
      localbq.a((String)localObject, paramObject, paramby);
      return;
    }
  }
  
  public final void a(Collection paramCollection)
  {
    Object localObject1 = "normalizedNumbers";
    c.g.b.k.b(paramCollection, (String)localObject1);
    paramCollection = ((Iterable)m.i((Iterable)paramCollection)).iterator();
    for (;;)
    {
      boolean bool = paramCollection.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (String)paramCollection.next();
      Object localObject2 = g;
      localObject1 = ((bu)localObject2).a((String)localObject1);
      if (localObject1 != null)
      {
        localObject2 = d;
        if (localObject2 != null) {
          ((a)localObject2).a();
        }
        localObject2 = ((r)f.a()).a();
        i locali = k.a();
        b localb = i();
        Object localObject3 = new com/truecaller/calling/recorder/bo;
        ((bo)localObject3).<init>(localb);
        localObject3 = (ac)localObject3;
        localObject2 = ((w)localObject2).a(locali, (ac)localObject3);
        d = ((a)localObject2);
        localObject2 = (bq)b;
        if (localObject2 != null) {
          ((bq)localObject2).a((Set)localObject1);
        }
      }
    }
  }
  
  public final void a(Set paramSet)
  {
    Object localObject1 = "normalizedNumbers";
    c.g.b.k.b(paramSet, (String)localObject1);
    paramSet = ((Iterable)paramSet).iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (String)paramSet.next();
      Object localObject2 = g;
      localObject1 = ((bu)localObject2).a((String)localObject1);
      if (localObject1 != null)
      {
        localObject2 = (bq)b;
        if (localObject2 != null) {
          ((bq)localObject2).a((Set)localObject1);
        }
      }
    }
  }
  
  public final boolean a(int paramInt1, int paramInt2)
  {
    int m = 1;
    if (paramInt1 != m) {
      return false;
    }
    paramInt1 = 2131361848;
    Object localObject1;
    Object localObject2;
    if (paramInt2 != paramInt1)
    {
      paramInt1 = 2131361934;
      if (paramInt2 == paramInt1)
      {
        a.clear();
        localObject1 = a;
        localObject2 = c;
        Object localObject3;
        if (localObject2 != null)
        {
          localObject2 = (Cursor)localObject2;
          ((Cursor)localObject2).moveToFirst();
          localObject3 = new java/util/LinkedHashSet;
          ((LinkedHashSet)localObject3).<init>();
          localObject3 = (Set)localObject3;
          boolean bool;
          do
          {
            int n = ((Cursor)localObject2).getColumnIndex("history_call_recording_id");
            long l = ((Cursor)localObject2).getLong(n);
            Long localLong = Long.valueOf(l);
            ((Set)localObject3).add(localLong);
            bool = ((Cursor)localObject2).moveToNext();
          } while (bool);
          localObject3 = (Collection)localObject3;
        }
        else
        {
          localObject2 = (List)y.a;
          localObject3 = localObject2;
          localObject3 = (Collection)localObject2;
        }
        ((Set)localObject1).addAll((Collection)localObject3);
        localObject1 = (bq)b;
        if (localObject1 != null) {
          ((bq)localObject1).g();
        }
        localObject1 = (bq)b;
        if (localObject1 != null) {
          ((bq)localObject1).c();
        }
      }
    }
    else
    {
      localObject1 = a;
      localObject2 = new com/truecaller/calling/recorder/bm$a;
      ((bm.a)localObject2).<init>(this);
      localObject2 = (by)localObject2;
      a(localObject1, (by)localObject2);
    }
    return m;
  }
  
  public final boolean a(CallRecording paramCallRecording)
  {
    c.g.b.k.b(paramCallRecording, "callRecording");
    Set localSet = a;
    paramCallRecording = Long.valueOf(a);
    return localSet.contains(paramCallRecording);
  }
  
  public final void b(int paramInt)
  {
    int m = 1;
    if (paramInt == m)
    {
      a.clear();
      bq localbq = (bq)b;
      if (localbq != null)
      {
        localbq.b(false);
        return;
      }
    }
  }
  
  public final void b(CallRecording paramCallRecording)
  {
    Object localObject = "callRecording";
    c.g.b.k.b(paramCallRecording, (String)localObject);
    long l = a;
    paramCallRecording = a;
    Long localLong = Long.valueOf(l);
    boolean bool1 = paramCallRecording.remove(localLong);
    if (!bool1)
    {
      localObject = Long.valueOf(l);
      paramCallRecording.add(localObject);
    }
    boolean bool2 = paramCallRecording.isEmpty();
    if (bool2)
    {
      paramCallRecording = (bq)b;
      if (paramCallRecording != null) {
        paramCallRecording.B_();
      }
    }
    paramCallRecording = (bq)b;
    if (paramCallRecording != null) {
      paramCallRecording.g();
    }
    paramCallRecording = (bq)b;
    if (paramCallRecording != null)
    {
      paramCallRecording.c();
      return;
    }
  }
  
  public final boolean b()
  {
    bq localbq = (bq)b;
    if (localbq != null) {
      localbq.A_();
    }
    localbq = (bq)b;
    boolean bool = true;
    if (localbq != null) {
      localbq.b(bool);
    }
    return bool;
  }
  
  public final boolean b(int paramInt1, int paramInt2)
  {
    int m = 1;
    if (paramInt1 == m)
    {
      paramInt1 = 2131361848;
      if (paramInt2 != paramInt1)
      {
        paramInt1 = 2131361934;
        if (paramInt2 != paramInt1) {
          return false;
        }
        Set localSet = a;
        paramInt1 = localSet.size();
        paramInt2 = j();
        if (paramInt1 != paramInt2) {
          return m;
        }
        return false;
      }
      return m;
    }
    return false;
  }
  
  public final int c(int paramInt)
  {
    int m = 1;
    if (paramInt == m) {
      return 2131623937;
    }
    return -1;
  }
  
  public final w c(CallRecording paramCallRecording)
  {
    c.g.b.k.b(paramCallRecording, "callRecording");
    Set localSet = a;
    Long localLong = Long.valueOf(a);
    localSet.remove(localLong);
    return ((r)f.a()).a(paramCallRecording);
  }
  
  public final String c()
  {
    n localn = h;
    Object[] arrayOfObject = new Object[2];
    Integer localInteger = Integer.valueOf(a.size());
    arrayOfObject[0] = localInteger;
    localInteger = Integer.valueOf(j());
    arrayOfObject[1] = localInteger;
    return localn.a(2131886292, arrayOfObject);
  }
  
  public final void e()
  {
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.g();
      return;
    }
  }
  
  public final void f()
  {
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.h();
      return;
    }
  }
  
  public final void g()
  {
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.g();
      return;
    }
  }
  
  public final boolean h()
  {
    z localz = c;
    if (localz != null)
    {
      int m = localz.getCount();
      if (m == 0) {
        return false;
      }
    }
    return true;
  }
  
  public final void toast(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.toast(paramString);
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    a locala = d;
    if (locala != null) {
      locala.a();
    }
    i.a(null);
    locala = e;
    if (locala != null)
    {
      locala.a();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bm
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */