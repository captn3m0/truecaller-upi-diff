package com.truecaller.calling.recorder;

public enum CallRecordingSettingsMvp$Configuration
{
  static
  {
    Configuration[] arrayOfConfiguration = new Configuration[2];
    Configuration localConfiguration = new com/truecaller/calling/recorder/CallRecordingSettingsMvp$Configuration;
    localConfiguration.<init>("DEFAULT", 0);
    DEFAULT = localConfiguration;
    arrayOfConfiguration[0] = localConfiguration;
    localConfiguration = new com/truecaller/calling/recorder/CallRecordingSettingsMvp$Configuration;
    int i = 1;
    localConfiguration.<init>("SDK_MEDIA_RECORDER", i);
    SDK_MEDIA_RECORDER = localConfiguration;
    arrayOfConfiguration[i] = localConfiguration;
    $VALUES = arrayOfConfiguration;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsMvp.Configuration
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */