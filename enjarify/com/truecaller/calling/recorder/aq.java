package com.truecaller.calling.recorder;

import dagger.a.d;
import javax.inject.Provider;

public final class aq
  implements d
{
  private final ap a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private aq(ap paramap, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramap;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static aq a(ap paramap, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    aq localaq = new com/truecaller/calling/recorder/aq;
    localaq.<init>(paramap, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localaq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */