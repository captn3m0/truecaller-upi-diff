package com.truecaller.calling.recorder;

public enum CallRecordingOnBoardingLaunchContext
{
  static
  {
    CallRecordingOnBoardingLaunchContext[] arrayOfCallRecordingOnBoardingLaunchContext = new CallRecordingOnBoardingLaunchContext[8];
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext = new com/truecaller/calling/recorder/CallRecordingOnBoardingLaunchContext;
    localCallRecordingOnBoardingLaunchContext.<init>("DEEP_LINK", 0);
    DEEP_LINK = localCallRecordingOnBoardingLaunchContext;
    arrayOfCallRecordingOnBoardingLaunchContext[0] = localCallRecordingOnBoardingLaunchContext;
    localCallRecordingOnBoardingLaunchContext = new com/truecaller/calling/recorder/CallRecordingOnBoardingLaunchContext;
    int i = 1;
    localCallRecordingOnBoardingLaunchContext.<init>("LIST", i);
    LIST = localCallRecordingOnBoardingLaunchContext;
    arrayOfCallRecordingOnBoardingLaunchContext[i] = localCallRecordingOnBoardingLaunchContext;
    localCallRecordingOnBoardingLaunchContext = new com/truecaller/calling/recorder/CallRecordingOnBoardingLaunchContext;
    i = 2;
    localCallRecordingOnBoardingLaunchContext.<init>("SETTINGS", i);
    SETTINGS = localCallRecordingOnBoardingLaunchContext;
    arrayOfCallRecordingOnBoardingLaunchContext[i] = localCallRecordingOnBoardingLaunchContext;
    localCallRecordingOnBoardingLaunchContext = new com/truecaller/calling/recorder/CallRecordingOnBoardingLaunchContext;
    i = 3;
    localCallRecordingOnBoardingLaunchContext.<init>("PROMO", i);
    PROMO = localCallRecordingOnBoardingLaunchContext;
    arrayOfCallRecordingOnBoardingLaunchContext[i] = localCallRecordingOnBoardingLaunchContext;
    localCallRecordingOnBoardingLaunchContext = new com/truecaller/calling/recorder/CallRecordingOnBoardingLaunchContext;
    i = 4;
    localCallRecordingOnBoardingLaunchContext.<init>("WHATS_NEW", i);
    WHATS_NEW = localCallRecordingOnBoardingLaunchContext;
    arrayOfCallRecordingOnBoardingLaunchContext[i] = localCallRecordingOnBoardingLaunchContext;
    localCallRecordingOnBoardingLaunchContext = new com/truecaller/calling/recorder/CallRecordingOnBoardingLaunchContext;
    i = 5;
    localCallRecordingOnBoardingLaunchContext.<init>("DIALOG", i);
    DIALOG = localCallRecordingOnBoardingLaunchContext;
    arrayOfCallRecordingOnBoardingLaunchContext[i] = localCallRecordingOnBoardingLaunchContext;
    localCallRecordingOnBoardingLaunchContext = new com/truecaller/calling/recorder/CallRecordingOnBoardingLaunchContext;
    i = 6;
    localCallRecordingOnBoardingLaunchContext.<init>("UNKNOWN", i);
    UNKNOWN = localCallRecordingOnBoardingLaunchContext;
    arrayOfCallRecordingOnBoardingLaunchContext[i] = localCallRecordingOnBoardingLaunchContext;
    localCallRecordingOnBoardingLaunchContext = new com/truecaller/calling/recorder/CallRecordingOnBoardingLaunchContext;
    i = 7;
    localCallRecordingOnBoardingLaunchContext.<init>("PAY_WALL", i);
    PAY_WALL = localCallRecordingOnBoardingLaunchContext;
    arrayOfCallRecordingOnBoardingLaunchContext[i] = localCallRecordingOnBoardingLaunchContext;
    $VALUES = arrayOfCallRecordingOnBoardingLaunchContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */