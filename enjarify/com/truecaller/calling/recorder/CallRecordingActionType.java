package com.truecaller.calling.recorder;

public enum CallRecordingActionType
{
  private final String eventAction;
  
  static
  {
    CallRecordingActionType[] arrayOfCallRecordingActionType = new CallRecordingActionType[2];
    CallRecordingActionType localCallRecordingActionType = new com/truecaller/calling/recorder/CallRecordingActionType;
    localCallRecordingActionType.<init>("PLAY_CALL_RECORDING", 0, "ItemEvent.ACTION_PLAY_CALL_RECORDING");
    PLAY_CALL_RECORDING = localCallRecordingActionType;
    arrayOfCallRecordingActionType[0] = localCallRecordingActionType;
    localCallRecordingActionType = new com/truecaller/calling/recorder/CallRecordingActionType;
    int i = 1;
    localCallRecordingActionType.<init>("SHOW_CALL_RECORDING_MENU_OPTIONS", i, "ItemEvent.ACTION_SHOW_CALL_RECORDING_MENU_OPTIONS");
    SHOW_CALL_RECORDING_MENU_OPTIONS = localCallRecordingActionType;
    arrayOfCallRecordingActionType[i] = localCallRecordingActionType;
    $VALUES = arrayOfCallRecordingActionType;
  }
  
  private CallRecordingActionType(String paramString1)
  {
    eventAction = paramString1;
  }
  
  public final String getEventAction()
  {
    return eventAction;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingActionType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */