package com.truecaller.calling.recorder;

import c.a.m;
import c.g.b.k;
import com.truecaller.bb;
import com.truecaller.common.f.c;
import com.truecaller.common.g.a;
import com.truecaller.utils.d;
import com.truecaller.utils.l;
import com.truecaller.wizard.utils.PermissionPoller;
import com.truecaller.wizard.utils.PermissionPoller.Permission;
import java.util.Iterator;
import java.util.List;

public final class bg
  extends bb
  implements CallRecordingSettingsMvp.a
{
  private final List a;
  private final List c;
  private final List d;
  private final a e;
  private final c f;
  private final CallRecordingManager g;
  private final com.truecaller.utils.n h;
  private final l i;
  private final d j;
  private final PermissionPoller k;
  private final h l;
  
  public bg(a parama, c paramc, CallRecordingManager paramCallRecordingManager, com.truecaller.utils.n paramn, l paraml, d paramd, PermissionPoller paramPermissionPoller, h paramh)
  {
    e = parama;
    f = paramc;
    g = paramCallRecordingManager;
    h = paramn;
    i = paraml;
    j = paramd;
    k = paramPermissionPoller;
    l = paramh;
    int m = 2;
    paramc = new com.truecaller.ui.components.n[m];
    paramCallRecordingManager = new com/truecaller/ui/components/n;
    paramn = CallRecordingSettingsMvp.RecordingModes.AUTO;
    paramCallRecordingManager.<init>(0, 2131887575, 2131887576, paramn);
    paramc[0] = paramCallRecordingManager;
    paramCallRecordingManager = new com/truecaller/ui/components/n;
    paramn = CallRecordingSettingsMvp.RecordingModes.MANUAL;
    paramCallRecordingManager.<init>(0, 2131887577, 2131887578, paramn);
    int n = 1;
    paramc[n] = paramCallRecordingManager;
    paramc = m.b(paramc);
    a = paramc;
    paramc = new com.truecaller.ui.components.n[3];
    paramCallRecordingManager = new com/truecaller/ui/components/n;
    paramd = CallRecordingSettingsMvp.CallsFilter.ALL_CALLS;
    int i1 = -1;
    paramCallRecordingManager.<init>(0, 2131887556, i1, paramd);
    paramc[0] = paramCallRecordingManager;
    paramCallRecordingManager = new com/truecaller/ui/components/n;
    paramd = CallRecordingSettingsMvp.CallsFilter.UNKNOWN_NUMBERS;
    paramCallRecordingManager.<init>(0, 2131887558, i1, paramd);
    paramc[n] = paramCallRecordingManager;
    paramCallRecordingManager = new com/truecaller/ui/components/n;
    paramd = CallRecordingSettingsMvp.CallsFilter.SELECTED_CONTACTS;
    paramCallRecordingManager.<init>(0, 2131887557, i1, paramd);
    paramc[m] = paramCallRecordingManager;
    paramc = m.b(paramc);
    c = paramc;
    parama = new com.truecaller.ui.components.n[m];
    paramc = new com/truecaller/ui/components/n;
    paramCallRecordingManager = CallRecordingSettingsMvp.Configuration.DEFAULT;
    paramc.<init>(0, 2131887553, 2131887554, paramCallRecordingManager);
    parama[0] = paramc;
    paramc = new com/truecaller/ui/components/n;
    paramCallRecordingManager = CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER;
    paramc.<init>(0, 2131887555, i1, paramCallRecordingManager);
    parama[n] = paramc;
    parama = m.b(parama);
    d = parama;
  }
  
  private final void c(boolean paramBoolean)
  {
    Object localObject = e;
    String str = "callRecordingEnbaled";
    ((a)localObject).b(str, paramBoolean);
    localObject = (CallRecordingSettingsMvp.b)b;
    if (localObject != null)
    {
      ((CallRecordingSettingsMvp.b)localObject).c(paramBoolean);
      return;
    }
  }
  
  public final void a()
  {
    k.a();
    Object localObject1 = (CallRecordingSettingsMvp.b)b;
    boolean bool1 = true;
    if (localObject1 != null)
    {
      localc = f;
      bool2 = localc.d() ^ bool1;
      ((CallRecordingSettingsMvp.b)localObject1).b(bool2);
    }
    localObject1 = f;
    boolean bool3 = ((c)localObject1).d();
    boolean bool2 = false;
    c localc = null;
    if (!bool3)
    {
      localObject1 = g.n();
      localObject2 = FreeTrialStatus.EXPIRED;
      if (localObject1 == localObject2)
      {
        bool3 = true;
        break label99;
      }
    }
    bool3 = false;
    localObject1 = null;
    label99:
    Object localObject2 = e;
    Object localObject3 = "callRecordingEnbaled";
    boolean bool4 = ((a)localObject2).b((String)localObject3);
    if ((bool4) && (!bool3)) {
      bool2 = true;
    }
    localObject2 = (CallRecordingSettingsMvp.b)b;
    if (localObject2 != null) {
      ((CallRecordingSettingsMvp.b)localObject2).a(bool2);
    }
    localObject2 = (CallRecordingSettingsMvp.b)b;
    if (localObject2 != null) {
      ((CallRecordingSettingsMvp.b)localObject2).c(bool2);
    }
    localObject2 = (CallRecordingSettingsMvp.b)b;
    Object localObject4;
    Object localObject5;
    if (localObject2 != null)
    {
      localObject3 = e;
      localObject4 = bf.a();
      localObject3 = ((a)localObject3).b("callRecordingStoragePath", (String)localObject4);
      localObject5 = "coreSettings.getString(\n…oragePath()\n            )";
      k.a(localObject3, (String)localObject5);
      ((CallRecordingSettingsMvp.b)localObject2).a((String)localObject3);
    }
    localObject2 = ((Iterable)a).iterator();
    boolean bool6;
    Object localObject6;
    Object localObject7;
    String str;
    boolean bool7;
    do
    {
      bool5 = ((Iterator)localObject2).hasNext();
      bool6 = false;
      localObject5 = null;
      if (!bool5) {
        break;
      }
      localObject3 = ((Iterator)localObject2).next();
      localObject4 = localObject3;
      localObject4 = ((com.truecaller.ui.components.n)localObject3).q().toString();
      localObject6 = e;
      localObject7 = "callRecordingMode";
      str = CallRecordingSettingsMvp.RecordingModes.MANUAL.name();
      localObject6 = ((a)localObject6).b((String)localObject7, str);
      bool7 = k.a(localObject4, localObject6);
    } while (!bool7);
    break label351;
    boolean bool5 = false;
    localObject3 = null;
    label351:
    localObject3 = (com.truecaller.ui.components.n)localObject3;
    if (localObject3 != null)
    {
      localObject2 = (CallRecordingSettingsMvp.b)b;
      if (localObject2 != null) {
        ((CallRecordingSettingsMvp.b)localObject2).a((com.truecaller.ui.components.n)localObject3);
      }
    }
    localObject2 = ((Iterable)c).iterator();
    do
    {
      bool5 = ((Iterator)localObject2).hasNext();
      if (!bool5) {
        break;
      }
      localObject3 = ((Iterator)localObject2).next();
      localObject4 = localObject3;
      localObject4 = ((com.truecaller.ui.components.n)localObject3).q().toString();
      localObject6 = e;
      localObject7 = "callRecordingFilter";
      str = CallRecordingSettingsMvp.CallsFilter.ALL_CALLS.name();
      localObject6 = ((a)localObject6).b((String)localObject7, str);
      bool7 = k.a(localObject4, localObject6);
    } while (!bool7);
    break label494;
    bool5 = false;
    localObject3 = null;
    label494:
    localObject3 = (com.truecaller.ui.components.n)localObject3;
    if (localObject3 != null)
    {
      localObject2 = (CallRecordingSettingsMvp.b)b;
      if (localObject2 != null) {
        ((CallRecordingSettingsMvp.b)localObject2).b((com.truecaller.ui.components.n)localObject3);
      }
    }
    localObject2 = ((Iterable)d).iterator();
    do
    {
      bool5 = ((Iterator)localObject2).hasNext();
      if (!bool5) {
        break;
      }
      localObject3 = ((Iterator)localObject2).next();
      localObject4 = localObject3;
      localObject4 = (com.truecaller.ui.components.n)localObject3;
      localObject6 = l.d();
      localObject7 = CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER;
      if (localObject6 == localObject7)
      {
        localObject4 = ((com.truecaller.ui.components.n)localObject4).q().toString();
        localObject6 = CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER.name();
        bool7 = k.a(localObject4, localObject6);
      }
      else
      {
        localObject4 = ((com.truecaller.ui.components.n)localObject4).q().toString();
        localObject6 = e;
        localObject7 = "callRecordingConfiguration";
        str = CallRecordingSettingsMvp.Configuration.DEFAULT.name();
        localObject6 = ((a)localObject6).b((String)localObject7, str);
        bool7 = k.a(localObject4, localObject6);
      }
    } while (!bool7);
    break label694;
    bool5 = false;
    localObject3 = null;
    label694:
    localObject3 = (com.truecaller.ui.components.n)localObject3;
    if (localObject3 != null)
    {
      localObject2 = (CallRecordingSettingsMvp.b)b;
      if (localObject2 != null) {
        ((CallRecordingSettingsMvp.b)localObject2).c((com.truecaller.ui.components.n)localObject3);
      }
    }
    if (bool3)
    {
      localObject1 = (CallRecordingSettingsMvp.b)b;
      if (localObject1 != null) {
        ((CallRecordingSettingsMvp.b)localObject1).b();
      }
    }
    if (bool2)
    {
      localObject1 = (CallRecordingSettingsMvp.b)b;
      if (localObject1 != null)
      {
        bool2 = i.a() ^ bool1;
        bool4 = j.f() ^ bool1;
        bool5 = i.c() ^ bool1;
        localObject5 = i;
        localObject4 = new String[] { "android.permission.RECORD_AUDIO" };
        bool6 = ((l)localObject5).a((String[])localObject4);
        bool1 ^= bool6;
        ((CallRecordingSettingsMvp.b)localObject1).a(bool2, bool4, bool1, bool5);
        return;
      }
    }
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    g.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void a(com.truecaller.ui.components.n paramn)
  {
    k.b(paramn, "modeSelected");
    Object localObject = e;
    String str = "callRecordingMode";
    paramn = paramn.q().toString();
    ((a)localObject).a(str, paramn);
    paramn = e;
    localObject = "callRecordingEnbaled";
    boolean bool = paramn.b((String)localObject);
    if (!bool)
    {
      bool = true;
      a(bool);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    CallRecordingSettingsMvp.b localb;
    if (paramBoolean)
    {
      localObject1 = g;
      bool = ((CallRecordingManager)localObject1).g();
      if (bool)
      {
        localb = (CallRecordingSettingsMvp.b)b;
        if (localb != null)
        {
          localObject1 = CallRecordingOnBoardingLaunchContext.PAY_WALL;
          localb.a((CallRecordingOnBoardingLaunchContext)localObject1);
          return;
        }
        return;
      }
    }
    Object localObject2;
    if (paramBoolean)
    {
      localObject1 = e;
      localObject2 = "callRecordingPostEnableShown";
      bool = ((a)localObject1).b((String)localObject2);
      if (!bool)
      {
        localb = (CallRecordingSettingsMvp.b)b;
        if (localb != null)
        {
          localObject1 = CallRecordingOnBoardingLaunchContext.SETTINGS;
          localb.a((CallRecordingOnBoardingLaunchContext)localObject1);
        }
        return;
      }
    }
    boolean bool = false;
    Object localObject1 = null;
    if (paramBoolean)
    {
      localb = (CallRecordingSettingsMvp.b)b;
      if (localb != null)
      {
        localObject2 = h;
        int m = 2131887610;
        localObject1 = new Object[0];
        localObject1 = ((com.truecaller.utils.n)localObject2).b(m, (Object[])localObject1);
        localObject2 = "resourceProvider.getRich…recording_terms_subtitle)";
        k.a(localObject1, (String)localObject2);
        localb.a((CharSequence)localObject1);
      }
      return;
    }
    c(false);
  }
  
  public final void b()
  {
    CallRecordingSettingsMvp.b localb = (CallRecordingSettingsMvp.b)b;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void b(com.truecaller.ui.components.n paramn)
  {
    k.b(paramn, "filterSelected");
    a locala = e;
    paramn = paramn.q().toString();
    locala.a("callRecordingFilter", paramn);
  }
  
  public final void b(boolean paramBoolean)
  {
    c(paramBoolean);
  }
  
  public final void c()
  {
    Object localObject = k;
    PermissionPoller.Permission localPermission = PermissionPoller.Permission.DRAW_OVERLAY;
    ((PermissionPoller)localObject).a(localPermission);
    localObject = (CallRecordingSettingsMvp.b)b;
    if (localObject != null)
    {
      ((CallRecordingSettingsMvp.b)localObject).d();
      return;
    }
  }
  
  public final void c(com.truecaller.ui.components.n paramn)
  {
    k.b(paramn, "configSelected");
    a locala = e;
    paramn = paramn.q().toString();
    locala.a("callRecordingConfiguration", paramn);
  }
  
  public final void e()
  {
    Object localObject = k;
    PermissionPoller.Permission localPermission = PermissionPoller.Permission.BATTERY_OPTIMISATIONS;
    ((PermissionPoller)localObject).a(localPermission);
    localObject = (CallRecordingSettingsMvp.b)b;
    if (localObject != null)
    {
      ((CallRecordingSettingsMvp.b)localObject).e();
      return;
    }
  }
  
  public final void f()
  {
    CallRecordingSettingsMvp.b localb = (CallRecordingSettingsMvp.b)b;
    if (localb != null)
    {
      localb.b("android.permission.WRITE_EXTERNAL_STORAGE");
      return;
    }
  }
  
  public final void g()
  {
    CallRecordingSettingsMvp.b localb = (CallRecordingSettingsMvp.b)b;
    if (localb != null)
    {
      localb.b("android.permission.RECORD_AUDIO");
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    k.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */