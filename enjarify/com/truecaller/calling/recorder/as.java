package com.truecaller.calling.recorder;

import c.g.b.k;
import c.l;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.utils.n;

public final class as
  extends com.truecaller.adapter_delegates.c
  implements ar
{
  private final bv b;
  private final n c;
  private final cb d;
  private final bu e;
  private final com.truecaller.common.f.c f;
  private final bk g;
  private final CallRecordingManager h;
  
  public as(bv parambv, n paramn, cb paramcb, bu parambu, com.truecaller.common.f.c paramc, bk parambk, CallRecordingManager paramCallRecordingManager)
  {
    b = parambv;
    c = paramn;
    d = paramcb;
    e = parambu;
    f = paramc;
    g = parambk;
    h = paramCallRecordingManager;
  }
  
  private void b(av paramav)
  {
    k.b(paramav, "itemView");
    Object localObject1 = b.n();
    Object localObject2 = h;
    boolean bool1 = ((CallRecordingManager)localObject2).g();
    int j = 2131887568;
    int k = 2131887567;
    if (bool1)
    {
      localObject1 = c;
      localObject2 = new Object[0];
      localObject1 = ((n)localObject1).a(k, (Object[])localObject2);
      k.a(localObject1, "resourceProvider.getStri…all_recording_list_promo)");
      paramav.setText((String)localObject1);
      localObject1 = c;
      localObject2 = new Object[0];
      localObject1 = ((n)localObject1).a(j, (Object[])localObject2);
      k.a(localObject1, "resourceProvider.getStri…ing_list_promo_cta_start)");
      paramav.setCTATitle((String)localObject1);
      localObject1 = c;
      localObject3 = new Object[0];
      localObject1 = ((n)localObject1).a(2131887629, (Object[])localObject3);
      k.a(localObject1, "resourceProvider.getStri…ecording_whats_new_title)");
      paramav.setTitle((String)localObject1);
      return;
    }
    localObject2 = c;
    boolean bool2 = ((FreeTrialStatus)localObject1).isActive();
    if (bool2) {
      k = 2131887570;
    }
    Object localObject4 = new Object[0];
    localObject2 = ((n)localObject2).a(k, (Object[])localObject4);
    Object localObject5 = "resourceProvider.getStri…      }\n                )";
    k.a(localObject2, (String)localObject5);
    paramav.setText((String)localObject2);
    localObject2 = at.a;
    k = ((FreeTrialStatus)localObject1).ordinal();
    int i = localObject2[k];
    switch (i)
    {
    default: 
      paramav = new c/l;
      paramav.<init>();
      throw paramav;
    case 2: 
    case 3: 
      localObject2 = c;
      j = 2131887569;
      localObject5 = new Object[0];
      localObject2 = ((n)localObject2).a(j, (Object[])localObject5);
      break;
    case 1: 
      localObject2 = c;
      localObject5 = new Object[0];
      localObject2 = ((n)localObject2).a(j, (Object[])localObject5);
    }
    Object localObject3 = "when (freeTrialStatus) {…pgrade)\n                }";
    k.a(localObject2, (String)localObject3);
    paramav.setCTATitle((String)localObject2);
    localObject2 = at.b;
    int m = ((FreeTrialStatus)localObject1).ordinal();
    m = localObject2[m];
    switch (m)
    {
    default: 
      paramav = new c/l;
      paramav.<init>();
      throw paramav;
    case 3: 
      localObject1 = c;
      i = 2131887572;
      localObject3 = new Object[0];
      localObject1 = ((n)localObject1).a(i, (Object[])localObject3);
      break;
    case 2: 
      m = b.m();
      localObject2 = c;
      j = 2131755040;
      k = 1;
      localObject5 = new Object[k];
      localObject4 = Integer.valueOf(m);
      localObject5[0] = localObject4;
      localObject1 = ((n)localObject2).a(j, m, (Object[])localObject5);
      break;
    case 1: 
      localObject1 = c;
      i = 2131887571;
      localObject3 = new Object[0];
      localObject1 = ((n)localObject1).a(i, (Object[])localObject3);
    }
    k.a(localObject1, "when (freeTrialStatus) {…_on_expiry)\n            }");
    paramav.setTitle((String)localObject1);
  }
  
  public final void a()
  {
    Object localObject1 = h;
    boolean bool = ((CallRecordingManager)localObject1).g();
    if (bool)
    {
      localObject1 = d;
      localObject2 = PremiumPresenterView.LaunchContext.CALL_RECORDING_PAY_WALL;
      ((cb)localObject1).a((PremiumPresenterView.LaunchContext)localObject2);
      return;
    }
    localObject1 = b.n();
    Object localObject2 = at.c;
    int i = ((FreeTrialStatus)localObject1).ordinal();
    i = localObject2[i];
    switch (i)
    {
    default: 
      localObject1 = new c/l;
      ((l)localObject1).<init>();
      throw ((Throwable)localObject1);
    case 2: 
    case 3: 
      localObject1 = d;
      localObject2 = PremiumPresenterView.LaunchContext.CALL_RECORDINGS;
      ((cb)localObject1).a((PremiumPresenterView.LaunchContext)localObject2);
      return;
    }
    e.a();
  }
  
  public final void a(av paramav)
  {
    b(paramav);
  }
  
  public final int getItemCount()
  {
    com.truecaller.common.f.c localc = f;
    boolean bool = localc.d();
    if (!bool) {
      return 1;
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.as
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */