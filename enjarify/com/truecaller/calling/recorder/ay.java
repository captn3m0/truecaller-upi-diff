package com.truecaller.calling.recorder;

import android.content.Context;
import c.g.b.k;
import com.nll.nativelibs.callrecording.AACCallRecorder;
import com.truecaller.callrecording.CallRecorder;
import com.truecaller.callrecording.CallRecorder.a;
import com.truecaller.callrecording.a;
import com.truecaller.callrecording.b;

public final class ay
  implements ax
{
  private final Context a;
  private final a b;
  private final h c;
  
  public ay(Context paramContext, a parama, h paramh)
  {
    a = paramContext;
    b = parama;
    c = paramh;
  }
  
  public final CallRecorder a(CallRecorder.a parama)
  {
    k.b(parama, "errorListener");
    Object localObject1 = c.d();
    Object localObject2 = CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER;
    if (localObject1 == localObject2)
    {
      new String[1][0] = "newInstance:: Providing KitkatCallRecorder";
      parama = new com/truecaller/callrecording/b;
      parama.<init>();
      return (CallRecorder)parama;
    }
    new String[1][0] = "newInstance:: Providign AACCallRecorder";
    localObject1 = new com/nll/nativelibs/callrecording/AACCallRecorder;
    localObject2 = a;
    a locala = b;
    ((AACCallRecorder)localObject1).<init>((Context)localObject2, locala, parama);
    return (CallRecorder)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ay
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */