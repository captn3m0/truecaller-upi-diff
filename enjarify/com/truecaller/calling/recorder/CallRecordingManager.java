package com.truecaller.calling.recorder;

import com.truecaller.data.entity.CallRecording;
import org.a.a.b;

public abstract interface CallRecordingManager
  extends bv, g
{
  public abstract void a(bi parambi);
  
  public abstract void a(f paramf);
  
  public abstract void a(CallRecording paramCallRecording, CallRecordingManager.PlaybackLaunchContext paramPlaybackLaunchContext);
  
  public abstract void a(String paramString);
  
  public abstract boolean a();
  
  public abstract boolean a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
  
  public abstract boolean a(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext);
  
  public abstract String b();
  
  public abstract b c();
  
  public abstract boolean d();
  
  public abstract void e();
  
  public abstract boolean f();
  
  public abstract boolean g();
  
  public abstract void h();
  
  public abstract boolean i();
  
  public abstract boolean j();
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */