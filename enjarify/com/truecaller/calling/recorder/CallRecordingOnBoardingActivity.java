package com.truecaller.calling.recorder;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v4.app.e;
import android.support.v4.app.j;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog.Companion;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog.Companion.Mode;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.wizard.utils.i;

public final class CallRecordingOnBoardingActivity
  extends AppCompatActivity
  implements an
{
  public al a;
  
  public final al a()
  {
    al localal = a;
    if (localal == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localal;
  }
  
  public final void a(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    CallRecordingOnBoardingDialog.Companion.a((j)localObject, false, paramCallRecordingOnBoardingLaunchContext);
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "message");
    Object localObject1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    paramCharSequence = ((AlertDialog.Builder)localObject1).setTitle(2131887611).setMessage(paramCharSequence);
    localObject1 = new com/truecaller/calling/recorder/CallRecordingOnBoardingActivity$g;
    ((CallRecordingOnBoardingActivity.g)localObject1).<init>(this);
    localObject1 = (DialogInterface.OnClickListener)localObject1;
    paramCharSequence = paramCharSequence.setPositiveButton(2131887608, (DialogInterface.OnClickListener)localObject1);
    localObject1 = new com/truecaller/calling/recorder/CallRecordingOnBoardingActivity$h;
    ((CallRecordingOnBoardingActivity.h)localObject1).<init>(this);
    localObject1 = (DialogInterface.OnClickListener)localObject1;
    paramCharSequence = paramCharSequence.setNegativeButton(2131887609, (DialogInterface.OnClickListener)localObject1);
    localObject1 = new com/truecaller/calling/recorder/CallRecordingOnBoardingActivity$i;
    ((CallRecordingOnBoardingActivity.i)localObject1).<init>(this);
    localObject1 = (DialogInterface.OnCancelListener)localObject1;
    paramCharSequence.setOnCancelListener((DialogInterface.OnCancelListener)localObject1).show();
  }
  
  public final void a(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    k.b(paramCharSequence1, "message");
    k.b(paramCharSequence2, "ctaText");
    Object localObject1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    paramCharSequence1 = ((AlertDialog.Builder)localObject1).setMessage(paramCharSequence1);
    localObject1 = new com/truecaller/calling/recorder/CallRecordingOnBoardingActivity$a;
    ((CallRecordingOnBoardingActivity.a)localObject1).<init>(this);
    localObject1 = (DialogInterface.OnClickListener)localObject1;
    paramCharSequence1 = paramCharSequence1.setPositiveButton(2131887619, (DialogInterface.OnClickListener)localObject1);
    localObject1 = new com/truecaller/calling/recorder/CallRecordingOnBoardingActivity$b;
    ((CallRecordingOnBoardingActivity.b)localObject1).<init>(this);
    localObject1 = (DialogInterface.OnClickListener)localObject1;
    paramCharSequence1 = paramCharSequence1.setNegativeButton(paramCharSequence2, (DialogInterface.OnClickListener)localObject1);
    paramCharSequence2 = new com/truecaller/calling/recorder/CallRecordingOnBoardingActivity$c;
    paramCharSequence2.<init>(this);
    paramCharSequence2 = (DialogInterface.OnCancelListener)paramCharSequence2;
    paramCharSequence1.setOnCancelListener(paramCharSequence2).show();
  }
  
  public final void a(String[] paramArrayOfString)
  {
    k.b(paramArrayOfString, "requiredPermissions");
    android.support.v4.app.a.a((Activity)this, paramArrayOfString, 102);
  }
  
  public final void b(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    k.b(localObject, "fragmentManager");
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    paramCallRecordingOnBoardingLaunchContext = CallRecordingOnBoardingDialog.Companion.a(CallRecordingOnBoardingDialog.Companion.Mode.PAY_WALL, paramCallRecordingOnBoardingLaunchContext);
    String str = CallRecordingOnBoardingDialog.class.getName();
    paramCallRecordingOnBoardingLaunchContext.show((j)localObject, str);
  }
  
  public final void b(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "message");
    Object localObject1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    paramCharSequence = ((AlertDialog.Builder)localObject1).setTitle(2131887583).setMessage(paramCharSequence);
    localObject1 = new com/truecaller/calling/recorder/CallRecordingOnBoardingActivity$d;
    ((CallRecordingOnBoardingActivity.d)localObject1).<init>(this);
    localObject1 = (DialogInterface.OnClickListener)localObject1;
    paramCharSequence = paramCharSequence.setPositiveButton(2131887580, (DialogInterface.OnClickListener)localObject1);
    localObject1 = new com/truecaller/calling/recorder/CallRecordingOnBoardingActivity$e;
    ((CallRecordingOnBoardingActivity.e)localObject1).<init>(this);
    localObject1 = (DialogInterface.OnClickListener)localObject1;
    paramCharSequence = paramCharSequence.setNegativeButton(2131887581, (DialogInterface.OnClickListener)localObject1);
    localObject1 = new com/truecaller/calling/recorder/CallRecordingOnBoardingActivity$f;
    ((CallRecordingOnBoardingActivity.f)localObject1).<init>(this);
    localObject1 = (DialogInterface.OnCancelListener)localObject1;
    paramCharSequence.setOnCancelListener((DialogInterface.OnCancelListener)localObject1).show();
  }
  
  public final void c(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    k.b(localObject, "fragmentManager");
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    paramCallRecordingOnBoardingLaunchContext = CallRecordingOnBoardingDialog.Companion.a(CallRecordingOnBoardingDialog.Companion.Mode.PAY_WALL_ON_EXPIRY, paramCallRecordingOnBoardingLaunchContext);
    String str = CallRecordingOnBoardingDialog.class.getName();
    paramCallRecordingOnBoardingLaunchContext.show((j)localObject, str);
  }
  
  public final void d(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    CallRecordingOnBoardingDialog.Companion.a((j)localObject, true, paramCallRecordingOnBoardingLaunchContext);
  }
  
  public final void e(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    k.b(localObject, "fragmentManager");
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    paramCallRecordingOnBoardingLaunchContext = CallRecordingOnBoardingDialog.Companion.a(CallRecordingOnBoardingDialog.Companion.Mode.AFTER_ENABLE, paramCallRecordingOnBoardingLaunchContext);
    String str = CallRecordingOnBoardingDialog.class.getName();
    paramCallRecordingOnBoardingLaunchContext.show((j)localObject, str);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    paramBundle = getTheme();
    Object localObject1 = ThemeManager.a();
    int i = resId;
    Object localObject2 = null;
    paramBundle.applyStyle(i, false);
    paramBundle = getApplicationContext();
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().cy().a(this);
      paramBundle = (CallRecordingOnBoardingState)getIntent().getSerializableExtra("State");
      localObject1 = getIntent();
      localObject2 = "LaunchContext";
      localObject1 = ((Intent)localObject1).getSerializableExtra((String)localObject2);
      if (localObject1 != null)
      {
        localObject1 = (CallRecordingOnBoardingLaunchContext)localObject1;
        localObject2 = a;
        String str;
        if (localObject2 == null)
        {
          str = "presenter";
          k.a(str);
        }
        ((al)localObject2).a(this);
        localObject2 = a;
        if (localObject2 == null)
        {
          str = "presenter";
          k.a(str);
        }
        if (localObject1 != null) {
          c = ((CallRecordingOnBoardingLaunchContext)localObject1);
        }
        if (paramBundle == null)
        {
          ((al)localObject2).b();
          return;
        }
        localObject1 = f;
        boolean bool = ((CallRecordingManager)localObject1).g();
        if (bool)
        {
          localObject1 = CallRecordingOnBoardingState.WHATS_NEW;
          if (paramBundle == localObject1) {
            paramBundle = CallRecordingOnBoardingState.PAY_WALL;
          }
        }
        a = paramBundle;
        ((al)localObject2).a();
        return;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext");
      throw paramBundle;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    al localal = a;
    if (localal == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localal.y_();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    al localal = a;
    if (localal == null)
    {
      str = "presenter";
      k.a(str);
    }
    k.b(paramArrayOfString, "permissions");
    String str = "grantResults";
    k.b(paramArrayOfInt, str);
    int i = 102;
    if (paramInt == i)
    {
      i.a(paramArrayOfString, paramArrayOfInt);
      paramInt = localal.c();
      if (paramInt != 0)
      {
        localal.e();
        localan = (an)b;
        if (localan != null)
        {
          paramArrayOfString = c;
          localan.e(paramArrayOfString);
        }
        return;
      }
      an localan = (an)b;
      if (localan != null)
      {
        localan.finish();
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingOnBoardingActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */