package com.truecaller.calling.recorder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import java.io.Serializable;

public final class aj
{
  public static Intent a(Context paramContext, CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext, CallRecordingOnBoardingState paramCallRecordingOnBoardingState)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, CallRecordingOnBoardingActivity.class);
    String str = "LaunchContext";
    paramCallRecordingOnBoardingLaunchContext = (Serializable)paramCallRecordingOnBoardingLaunchContext;
    localIntent.putExtra(str, paramCallRecordingOnBoardingLaunchContext);
    if (paramCallRecordingOnBoardingState != null)
    {
      paramCallRecordingOnBoardingLaunchContext = "State";
      paramCallRecordingOnBoardingState = (Serializable)paramCallRecordingOnBoardingState;
      localIntent.putExtra(paramCallRecordingOnBoardingLaunchContext, paramCallRecordingOnBoardingState);
    }
    boolean bool = paramContext instanceof Activity;
    if (!bool)
    {
      int i = 268435456;
      localIntent.setFlags(i);
    }
    return localIntent;
  }
  
  public static void a(Context paramContext, CallRecordingOnBoardingState paramCallRecordingOnBoardingState, CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramContext, "context");
    k.b(paramCallRecordingOnBoardingState, "state");
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    paramCallRecordingOnBoardingState = a(paramContext, paramCallRecordingOnBoardingLaunchContext, paramCallRecordingOnBoardingState);
    paramContext.startActivity(paramCallRecordingOnBoardingState);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */