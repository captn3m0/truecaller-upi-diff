package com.truecaller.calling.recorder;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class CallRecordingFloatingButtonPresenter$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  CallRecordingFloatingButtonPresenter$d(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$d;
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = b;
    locald.<init>(localCallRecordingFloatingButtonPresenter, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = (bw)b.b;
        if (paramObject != null) {
          ((bw)paramObject).d();
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */