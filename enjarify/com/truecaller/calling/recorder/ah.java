package com.truecaller.calling.recorder;

import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private ah(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static ah a(Provider paramProvider1, Provider paramProvider2)
  {
    ah localah = new com/truecaller/calling/recorder/ah;
    localah.<init>(paramProvider1, paramProvider2);
    return localah;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */