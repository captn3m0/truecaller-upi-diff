package com.truecaller.calling.recorder;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final Provider a;
  
  private o(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static o a(Provider paramProvider)
  {
    o localo = new com/truecaller/calling/recorder/o;
    localo.<init>(paramProvider);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */