package com.truecaller.calling.recorder;

import android.telephony.TelephonyManager;
import com.truecaller.log.AssertionUtil;
import java.util.TimerTask;

public final class ae$b
  extends TimerTask
{
  public ae$b(ae paramae) {}
  
  public final void run()
  {
    Object localObject = a.j;
    int i = ((TelephonyManager)localObject).getCallState();
    if (i == 0)
    {
      a.l();
      a.h();
      localObject = new java/lang/IllegalStateException;
      String str = "Call recording hasn't ended after call. Ending it forcefully!";
      ((IllegalStateException)localObject).<init>(str);
      localObject = (Throwable)localObject;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ae.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */