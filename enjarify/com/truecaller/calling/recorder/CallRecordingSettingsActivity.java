package com.truecaller.calling.recorder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import c.g.b.k;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.Iterator;

public final class CallRecordingSettingsActivity
  extends AppCompatActivity
{
  public final void onCreate(Bundle paramBundle)
  {
    ThemeManager.Theme localTheme = ThemeManager.a();
    int i = resId;
    setTheme(i);
    super.onCreate(paramBundle);
    setContentView(2131558438);
    int j = 2131364907;
    paramBundle = (Toolbar)findViewById(j);
    setSupportActionBar(paramBundle);
    paramBundle = getSupportActionBar();
    i = 1;
    if (paramBundle != null) {
      paramBundle.setDisplayHomeAsUpEnabled(i);
    }
    paramBundle = getSupportActionBar();
    if (paramBundle != null)
    {
      paramBundle.setDisplayShowHomeEnabled(i);
      return;
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    String str = "item";
    k.b(paramMenuItem, str);
    int i = paramMenuItem.getItemId();
    int j = 16908332;
    if (i != j) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    onBackPressed();
    return true;
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    Object localObject1 = getSupportFragmentManager();
    k.a(localObject1, "supportFragmentManager");
    localObject1 = ((j)localObject1).f();
    Object localObject2 = "supportFragmentManager.fragments";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Fragment)((Iterator)localObject1).next();
      ((Fragment)localObject2).onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */