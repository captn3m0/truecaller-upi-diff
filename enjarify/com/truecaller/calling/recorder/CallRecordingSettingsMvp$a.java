package com.truecaller.calling.recorder;

import com.truecaller.bm;
import com.truecaller.ui.components.n;

public abstract interface CallRecordingSettingsMvp$a
  extends bm
{
  public abstract void a();
  
  public abstract void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
  
  public abstract void a(n paramn);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(n paramn);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(n paramn);
  
  public abstract void e();
  
  public abstract void f();
  
  public abstract void g();
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsMvp.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */