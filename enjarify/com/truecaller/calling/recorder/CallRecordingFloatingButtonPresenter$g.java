package com.truecaller.calling.recorder;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import org.a.a.aa;
import org.a.a.d.n;
import org.a.a.r;
import org.a.a.s;

final class CallRecordingFloatingButtonPresenter$g
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  CallRecordingFloatingButtonPresenter$g(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    g localg = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$g;
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = b;
    localg.<init>(localCallRecordingFloatingButtonPresenter, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localg;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = (bw)b.b;
        if (paramObject != null)
        {
          localObject1 = b.g;
          Object localObject2 = r.a(0);
          s locals = s.a();
          localObject2 = (aa)((r)localObject2).a(locals);
          localObject1 = ((n)localObject1).a((aa)localObject2);
          localObject2 = "periodFormatter.print(Pe…(0).normalizedStandard())";
          c.g.b.k.a(localObject1, (String)localObject2);
          ((bw)paramObject).setLabel((String)localObject1);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (g)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((g)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */