package com.truecaller.calling.recorder;

public enum CallRecordingFloatingButtonPresenter$CallRecordingButtonMode
{
  static
  {
    CallRecordingButtonMode[] arrayOfCallRecordingButtonMode = new CallRecordingButtonMode[3];
    CallRecordingButtonMode localCallRecordingButtonMode = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$CallRecordingButtonMode;
    localCallRecordingButtonMode.<init>("NOT_STARTED", 0);
    NOT_STARTED = localCallRecordingButtonMode;
    arrayOfCallRecordingButtonMode[0] = localCallRecordingButtonMode;
    localCallRecordingButtonMode = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$CallRecordingButtonMode;
    int i = 1;
    localCallRecordingButtonMode.<init>("RECORDING", i);
    RECORDING = localCallRecordingButtonMode;
    arrayOfCallRecordingButtonMode[i] = localCallRecordingButtonMode;
    localCallRecordingButtonMode = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$CallRecordingButtonMode;
    i = 2;
    localCallRecordingButtonMode.<init>("ENDED", i);
    ENDED = localCallRecordingButtonMode;
    arrayOfCallRecordingButtonMode[i] = localCallRecordingButtonMode;
    $VALUES = arrayOfCallRecordingButtonMode;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.CallRecordingButtonMode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */