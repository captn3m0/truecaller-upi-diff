package com.truecaller.calling.recorder;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.a;
import android.view.View;
import android.widget.ImageView;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import com.truecaller.calling.ActionType;
import com.truecaller.util.at;

public final class cc
  implements com.truecaller.calling.b
{
  private final f b;
  private final f c;
  private final ColorStateList d;
  private final Drawable e;
  
  static
  {
    g[] arrayOfg = new g[2];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(cc.class);
    ((u)localObject).<init>(localb, "actionTwoView", "getActionTwoView()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(cc.class);
    ((u)localObject).<init>(localb, "actionTwoClickArea", "getActionTwoClickArea()Landroid/view/View;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public cc(View paramView)
  {
    Object localObject = com.truecaller.utils.extensions.t.a(paramView, 2131361939);
    b = ((f)localObject);
    localObject = com.truecaller.utils.extensions.t.a(paramView, 2131361843);
    c = ((f)localObject);
    localObject = com.truecaller.utils.ui.b.b(paramView.getContext(), 2130969592);
    d = ((ColorStateList)localObject);
    paramView = a.e(at.a(paramView.getContext(), 2131234256)).mutate();
    localObject = d;
    a.a(paramView, (ColorStateList)localObject);
    e = paramView;
    paramView = a();
    localObject = e;
    paramView.setImageDrawable((Drawable)localObject);
  }
  
  final ImageView a()
  {
    return (ImageView)b.b();
  }
  
  final View b()
  {
    return (View)c.b();
  }
  
  public final void b(ActionType paramActionType) {}
  
  public final void c(boolean paramBoolean)
  {
    Object localObject = b();
    k.a(localObject, "actionTwoClickArea");
    com.truecaller.utils.extensions.t.a((View)localObject, paramBoolean);
    localObject = a();
    k.a(localObject, "actionTwoView");
    com.truecaller.utils.extensions.t.a((View)localObject, paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.cc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */