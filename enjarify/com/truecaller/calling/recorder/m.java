package com.truecaller.calling.recorder;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private m(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static m a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    m localm = new com/truecaller/calling/recorder/m;
    localm.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */