package com.truecaller.calling.recorder;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.data.entity.CallRecording;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import org.a.a.aa;
import org.a.a.d.n;
import org.a.a.r;
import org.a.a.s;

final class CallRecordingFloatingButtonPresenter$f
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  int d;
  private ag g;
  
  CallRecordingFloatingButtonPresenter$f(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, CallRecording paramCallRecording, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$f;
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = e;
    CallRecording localCallRecording = f;
    localf.<init>(localCallRecordingFloatingButtonPresenter, localCallRecording, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = d;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject1 = (n)c;
      localObject2 = (bw)b;
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label255;
      }
      paramObject = f;
      localObject2 = e.n;
      localObject3 = e.l;
      paramObject = com.truecaller.data.entity.a.a((CallRecording)paramObject, (bx)localObject2, (f)localObject3);
      localObject2 = (bw)e.b;
      if (localObject2 == null) {
        break label251;
      }
      localObject3 = e.g;
      a = paramObject;
      b = localObject2;
      c = localObject3;
      int j = 1;
      d = j;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      localObject1 = localObject3;
    }
    long l = ((Number)paramObject).longValue();
    int k = (int)l;
    paramObject = r.b(k);
    Object localObject3 = s.a();
    paramObject = (aa)((r)paramObject).a((s)localObject3);
    paramObject = ((n)localObject1).a((aa)paramObject);
    localObject1 = "periodFormatter.print(Pe…()).normalizedStandard())";
    c.g.b.k.a(paramObject, (String)localObject1);
    ((bw)localObject2).setLabel((String)paramObject);
    label251:
    return x.a;
    label255:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */