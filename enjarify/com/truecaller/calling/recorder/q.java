package com.truecaller.calling.recorder;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final Provider a;
  
  private q(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static q a(Provider paramProvider)
  {
    q localq = new com/truecaller/calling/recorder/q;
    localq.<init>(paramProvider);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */