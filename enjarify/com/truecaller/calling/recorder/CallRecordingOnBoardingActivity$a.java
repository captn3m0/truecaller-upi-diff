package com.truecaller.calling.recorder;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import com.truecaller.common.g.a;

final class CallRecordingOnBoardingActivity$a
  implements DialogInterface.OnClickListener
{
  CallRecordingOnBoardingActivity$a(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    paramDialogInterface = a.a();
    a locala = e;
    String str = "callRecordingOnBoardDismissed";
    boolean bool = true;
    locala.b(str, bool);
    paramDialogInterface = (an)b;
    if (paramDialogInterface != null)
    {
      paramDialogInterface.finish();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingOnBoardingActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */