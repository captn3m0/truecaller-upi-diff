package com.truecaller.calling.recorder.floatingbutton;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import java.util.List;

public final class d
{
  static d a;
  boolean b;
  BubblesService c;
  int d;
  j e;
  c.b f;
  private Context g;
  private ServiceConnection h;
  
  d(Context paramContext)
  {
    d.1 local1 = new com/truecaller/calling/recorder/floatingbutton/d$1;
    local1.<init>(this);
    h = local1;
    g = paramContext;
  }
  
  public final void a()
  {
    Context localContext = g;
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(localContext, BubblesService.class);
    ServiceConnection localServiceConnection = h;
    localContext.bindService(localIntent, localServiceConnection, 1);
  }
  
  public final void a(int paramInt)
  {
    boolean bool = b;
    if (bool)
    {
      BubblesService localBubblesService = c;
      localBubblesService.a(paramInt);
    }
  }
  
  public final void a(BubbleLayout paramBubbleLayout)
  {
    boolean bool = b;
    if (bool)
    {
      BubblesService localBubblesService = c;
      localBubblesService.a(paramBubbleLayout);
    }
  }
  
  public final void a(BubbleLayout paramBubbleLayout, int paramInt1, int paramInt2)
  {
    boolean bool = b;
    if (bool)
    {
      BubblesService localBubblesService = c;
      Object localObject = BubblesService.a(paramInt1, paramInt2);
      WindowManager localWindowManager = localBubblesService.a();
      paramBubbleLayout.setWindowManager(localWindowManager);
      paramBubbleLayout.setViewParams((WindowManager.LayoutParams)localObject);
      localObject = e;
      paramBubbleLayout.setLayoutCoordinator((c)localObject);
      localObject = a;
      ((List)localObject).add(paramBubbleLayout);
      localBubblesService.a(paramBubbleLayout);
    }
  }
  
  public final void b()
  {
    Context localContext = g;
    ServiceConnection localServiceConnection = h;
    localContext.unbindService(localServiceConnection);
  }
  
  public final void c()
  {
    boolean bool = b;
    if (bool)
    {
      c.b();
      return;
    }
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = BubblesService.class.getSimpleName();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(" is not bounded.");
    localObject = ((StringBuilder)localObject).toString();
    arrayOfString[0] = localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */