package com.truecaller.calling.recorder.floatingbutton;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BubblesService
  extends Service
{
  List a;
  final Handler b;
  b c;
  WindowManager d;
  c e;
  c.b f;
  private BubblesService.a g;
  
  public BubblesService()
  {
    Object localObject = new com/truecaller/calling/recorder/floatingbutton/BubblesService$a;
    ((BubblesService.a)localObject).<init>(this);
    g = ((BubblesService.a)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    a = ((List)localObject);
    localObject = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    ((Handler)localObject).<init>(localLooper);
    b = ((Handler)localObject);
  }
  
  static WindowManager.LayoutParams a(int paramInt1, int paramInt2)
  {
    WindowManager.LayoutParams localLayoutParams = new android/view/WindowManager$LayoutParams;
    int i = com.truecaller.callerid.b.c.a(TrueApp.y().a().bw());
    localLayoutParams.<init>(-2, -2, i, 524296, -3);
    gravity = 8388659;
    x = paramInt1;
    y = paramInt2;
    return localLayoutParams;
  }
  
  private void b(BubbleLayout paramBubbleLayout)
  {
    if (paramBubbleLayout == null) {
      return;
    }
    Handler localHandler = b;
    -..Lambda.BubblesService.zNp1oYJH4PhOKXSOTuhscvk7arg localzNp1oYJH4PhOKXSOTuhscvk7arg = new com/truecaller/calling/recorder/floatingbutton/-$$Lambda$BubblesService$zNp1oYJH4PhOKXSOTuhscvk7arg;
    localzNp1oYJH4PhOKXSOTuhscvk7arg.<init>(this, paramBubbleLayout);
    localHandler.post(localzNp1oYJH4PhOKXSOTuhscvk7arg);
  }
  
  static WindowManager.LayoutParams c()
  {
    WindowManager.LayoutParams localLayoutParams = new android/view/WindowManager$LayoutParams;
    int i = com.truecaller.callerid.b.c.a(TrueApp.y().a().bw());
    localLayoutParams.<init>(-1, -1, i, 524296, -3);
    x = 0;
    y = 0;
    return localLayoutParams;
  }
  
  final WindowManager a()
  {
    WindowManager localWindowManager = d;
    if (localWindowManager == null)
    {
      localWindowManager = (WindowManager)getSystemService("window");
      d = localWindowManager;
    }
    return d;
  }
  
  public final void a(int paramInt)
  {
    Object localObject = a;
    boolean bool = ((List)localObject).isEmpty();
    if (bool) {
      return;
    }
    localObject = (BubbleLayout)a.get(0);
    WindowManager.LayoutParams localLayoutParams = a(getViewParamsx, paramInt);
    ((BubbleLayout)localObject).setViewParams(localLayoutParams);
    Handler localHandler = b;
    -..Lambda.BubblesService.WU5TTlSiGKsoYVKjK5ocQn2p7qg localWU5TTlSiGKsoYVKjK5ocQn2p7qg = new com/truecaller/calling/recorder/floatingbutton/-$$Lambda$BubblesService$WU5TTlSiGKsoYVKjK5ocQn2p7qg;
    localWU5TTlSiGKsoYVKjK5ocQn2p7qg.<init>(this, (BubbleLayout)localObject, localLayoutParams);
    localHandler.post(localWU5TTlSiGKsoYVKjK5ocQn2p7qg);
  }
  
  public final void a(BubbleLayout paramBubbleLayout)
  {
    b(paramBubbleLayout);
  }
  
  final void a(a parama)
  {
    Handler localHandler = b;
    -..Lambda.BubblesService.6kL6qIJ9tiXKA5heZJ774PkQpw4 local6kL6qIJ9tiXKA5heZJ774PkQpw4 = new com/truecaller/calling/recorder/floatingbutton/-$$Lambda$BubblesService$6kL6qIJ9tiXKA5heZJ774PkQpw4;
    local6kL6qIJ9tiXKA5heZJ774PkQpw4.<init>(this, parama);
    localHandler.post(local6kL6qIJ9tiXKA5heZJ774PkQpw4);
  }
  
  final void b()
  {
    Handler localHandler = b;
    -..Lambda.BubblesService.Kqd19gcwp8oMi87hB1V1YWaNLIs localKqd19gcwp8oMi87hB1V1YWaNLIs = new com/truecaller/calling/recorder/floatingbutton/-$$Lambda$BubblesService$Kqd19gcwp8oMi87hB1V1YWaNLIs;
    localKqd19gcwp8oMi87hB1V1YWaNLIs.<init>(this);
    localHandler.post(localKqd19gcwp8oMi87hB1V1YWaNLIs);
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return g;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    b();
  }
  
  public boolean onUnbind(Intent paramIntent)
  {
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      BubbleLayout localBubbleLayout = (BubbleLayout)localIterator.next();
      b(localBubbleLayout);
    }
    a.clear();
    return super.onUnbind(paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.BubblesService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */