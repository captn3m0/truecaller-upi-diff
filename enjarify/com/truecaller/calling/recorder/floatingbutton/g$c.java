package com.truecaller.calling.recorder.floatingbutton;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import c.c.a;
import c.n;
import c.u;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.Timer;
import java.util.TimerTask;

final class g$c
  implements j
{
  g$c(g paramg, String paramString, i parami) {}
  
  public final void a()
  {
    g.a(a);
    Object localObject1 = a;
    Object localObject2 = new android/view/ContextThemeWrapper;
    Object localObject3 = a;
    Object localObject4 = ThemeManager.a();
    int i = resId;
    ((ContextThemeWrapper)localObject2).<init>((Context)localObject3, i);
    localObject2 = LayoutInflater.from((Context)localObject2);
    int j = 0;
    localObject3 = null;
    i = 2131558636;
    localObject2 = ((LayoutInflater)localObject2).inflate(i, null);
    if (localObject2 != null)
    {
      localObject2 = (BubbleLayout)localObject2;
      g.a((g)localObject1, (BubbleLayout)localObject2);
      localObject1 = a;
      localObject2 = g.b((g)localObject1);
      if (localObject2 != null)
      {
        j = 2131362380;
        localObject2 = ((BubbleLayout)localObject2).findViewById(j);
        localObject3 = localObject2;
        localObject3 = (CallRecordingFloatingButton)localObject2;
      }
      g.a((g)localObject1, (CallRecordingFloatingButton)localObject3);
      localObject1 = g.c(a);
      if (localObject1 != null)
      {
        localObject2 = b;
        ((CallRecordingFloatingButton)localObject1).setPhoneNumber((String)localObject2);
      }
      localObject1 = g.b(a);
      if (localObject1 != null)
      {
        localObject2 = new com/truecaller/calling/recorder/floatingbutton/g$c$a;
        ((g.c.a)localObject2).<init>(this);
        localObject2 = (BubbleLayout.b)localObject2;
        ((BubbleLayout)localObject1).setOnBubbleClickListener((BubbleLayout.b)localObject2);
        localObject2 = new com/truecaller/calling/recorder/floatingbutton/g$c$b;
        ((g.c.b)localObject2).<init>(this);
        localObject2 = (BubbleLayout.d)localObject2;
        ((BubbleLayout)localObject1).setOnBubbleRemoveListener((BubbleLayout.d)localObject2);
        localObject2 = new com/truecaller/calling/recorder/floatingbutton/g$c$c;
        ((g.c.c)localObject2).<init>((BubbleLayout)localObject1, this);
        localObject2 = (BubbleLayout.c)localObject2;
        ((BubbleLayout)localObject1).setOnBubbleMovedListener((BubbleLayout.c)localObject2);
        localObject2 = a;
        j = Settings.c("callerIdLastYPosition");
        localObject2 = g.a((g)localObject2, j);
        localObject3 = (Number)a;
        j = ((Number)localObject3).intValue();
        localObject2 = (Number)b;
        int k = ((Number)localObject2).intValue();
        localObject4 = g.e(a);
        if (localObject4 != null)
        {
          BubbleLayout localBubbleLayout = g.b(a);
          ((d)localObject4).a(localBubbleLayout, j, k);
        }
        localObject1 = android.support.v4.content.d.a(((BubbleLayout)localObject1).getContext());
        localObject2 = (BroadcastReceiver)g.h(a);
        localObject3 = new android/content/IntentFilter;
        localObject4 = "BroadcastCallerIdPosY";
        ((IntentFilter)localObject3).<init>((String)localObject4);
        ((android.support.v4.content.d)localObject1).a((BroadcastReceiver)localObject2, (IntentFilter)localObject3);
        localObject1 = c;
        if (localObject1 != null) {
          ((i)localObject1).onCallRecordingButtonInitialised();
        }
        localObject1 = a;
        long l1 = g.i((g)localObject1);
        long l2 = g.i(a);
        localObject2 = a.a("SafeRecordingCloser");
        localObject3 = new com/truecaller/calling/recorder/floatingbutton/g$c$d;
        ((g.c.d)localObject3).<init>(this);
        localObject4 = localObject3;
        localObject4 = (TimerTask)localObject3;
        localObject3 = localObject2;
        ((Timer)localObject2).schedule((TimerTask)localObject4, l1, l2);
        g.a((g)localObject1, (Timer)localObject2);
        return;
      }
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type com.truecaller.calling.recorder.floatingbutton.BubbleLayout");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.g.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */