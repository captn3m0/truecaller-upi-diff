package com.truecaller.calling.recorder.floatingbutton;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

final class d$1
  implements ServiceConnection
{
  d$1(d paramd) {}
  
  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    paramIBinder = (BubblesService.a)paramIBinder;
    paramComponentName = a;
    paramIBinder = a;
    c = paramIBinder;
    paramComponentName = a;
    paramIBinder = c;
    int i = d;
    boolean bool = true;
    if (i != 0)
    {
      Object localObject1 = c;
      if (localObject1 != null) {
        paramIBinder.b();
      }
      localObject1 = new com/truecaller/calling/recorder/floatingbutton/b;
      ((b)localObject1).<init>(paramIBinder);
      c = ((b)localObject1);
      localObject1 = c;
      Object localObject2 = d;
      ((b)localObject1).setWindowManager((WindowManager)localObject2);
      localObject1 = c;
      localObject2 = BubblesService.c();
      ((b)localObject1).setViewParams((WindowManager.LayoutParams)localObject2);
      localObject1 = c;
      int j = 8;
      ((b)localObject1).setVisibility(j);
      localObject1 = LayoutInflater.from(paramIBinder);
      localObject2 = c;
      ((LayoutInflater)localObject1).inflate(i, (ViewGroup)localObject2, bool);
      paramComponentName = c;
      paramIBinder.a(paramComponentName);
      paramComponentName = new com/truecaller/calling/recorder/floatingbutton/c$a;
      paramComponentName.<init>(paramIBinder);
      localObject1 = paramIBinder.a();
      a.c = ((WindowManager)localObject1);
      localObject1 = f;
      a.e = ((c.b)localObject1);
      localObject1 = c;
      localObject2 = a;
      b = ((b)localObject1);
      paramComponentName = a;
      e = paramComponentName;
    }
    paramComponentName = a;
    b = bool;
    paramComponentName = e;
    if (paramComponentName != null)
    {
      paramComponentName = a.e;
      paramComponentName.a();
    }
    paramComponentName = a.c;
    paramIBinder = a.f;
    f = paramIBinder;
    c localc = e;
    if (localc != null)
    {
      paramComponentName = e;
      e = paramIBinder;
    }
  }
  
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    a.b = false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.d.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */