package com.truecaller.calling.recorder.floatingbutton;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

public class BubbleLayout
  extends a
{
  BubbleLayout.d a;
  private float b;
  private float c;
  private int d;
  private int e;
  private BubbleLayout.b f;
  private long g;
  private BubbleLayout.a h;
  private int i;
  private WindowManager j;
  private boolean k;
  private BubbleLayout.c l;
  
  public BubbleLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    boolean bool = true;
    k = bool;
    BubbleLayout.a locala = new com/truecaller/calling/recorder/floatingbutton/BubbleLayout$a;
    locala.<init>(this, (byte)0);
    h = locala;
    paramContext = (WindowManager)paramContext.getSystemService("window");
    j = paramContext;
    setClickable(bool);
    setLongClickable(bool);
    paramContext = new com/truecaller/calling/recorder/floatingbutton/-$$Lambda$BubbleLayout$VRyMLcFZGM04YFKzy368ClEo_Rs;
    paramContext.<init>(this);
    setOnLongClickListener(paramContext);
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    boolean bool = isInEditMode();
    if (!bool)
    {
      Object localObject = getContext();
      int m = 2130837505;
      localObject = (AnimatorSet)AnimatorInflater.loadAnimator((Context)localObject, m);
      ((AnimatorSet)localObject).setTarget(this);
      ((AnimatorSet)localObject).start();
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent != null)
    {
      int m = paramMotionEvent.getAction();
      float f1;
      float f2;
      int i3;
      int i5;
      WindowManager.LayoutParams localLayoutParams;
      Object localObject1;
      Object localObject2;
      float f4;
      int i4;
      long l1;
      switch (m)
      {
      default: 
        break;
      case 2: 
        m = d;
        f1 = paramMotionEvent.getRawX();
        f2 = b;
        f1 -= f2;
        i3 = (int)f1;
        m += i3;
        i3 = e;
        f2 = paramMotionEvent.getRawY();
        float f3 = c;
        f2 -= f3;
        i5 = (int)f2;
        i3 += i5;
        localLayoutParams = getViewParams();
        x = m;
        getViewParamsy = i3;
        localObject1 = getWindowManager();
        localObject2 = getViewParams();
        ((WindowManager)localObject1).updateViewLayout(this, (ViewGroup.LayoutParams)localObject2);
        localObject1 = getLayoutCoordinator();
        if (localObject1 != null)
        {
          localObject1 = getLayoutCoordinator();
          ((c)localObject1).a(this);
        }
        localObject1 = l;
        if (localObject1 != null) {
          ((BubbleLayout.c)localObject1).a();
        }
        break;
      case 1: 
        boolean bool1 = k;
        if (bool1)
        {
          int n = i / 2;
          localObject2 = getViewParams();
          i3 = x;
          if (i3 >= n)
          {
            n = i;
            f4 = n;
          }
          else
          {
            n = 0;
            f4 = 0.0F;
            localObject1 = null;
          }
          localObject2 = h;
          localLayoutParams = getViewParams();
          i5 = y;
          f2 = i5;
          BubbleLayout.a.a((BubbleLayout.a)localObject2, f4, f2);
        }
        localObject1 = getLayoutCoordinator();
        if (localObject1 != null)
        {
          localObject1 = getLayoutCoordinator();
          localObject2 = b;
          if (localObject2 != null)
          {
            boolean bool4 = ((c)localObject1).b(this);
            if (bool4)
            {
              localObject2 = d;
              ((BubblesService)localObject2).a(this);
            }
            localObject1 = b;
            i4 = 8;
            f1 = 1.1E-44F;
            ((b)localObject1).setVisibility(i4);
          }
          boolean bool2 = isInEditMode();
          if (!bool2)
          {
            localObject1 = getContext();
            i4 = 2130837510;
            f1 = 1.7279976E38F;
            localObject1 = (AnimatorSet)AnimatorInflater.loadAnimator((Context)localObject1, i4);
            ((AnimatorSet)localObject1).setTarget(this);
            ((AnimatorSet)localObject1).start();
          }
        }
        l1 = System.currentTimeMillis();
        long l2 = g;
        l1 -= l2;
        l2 = 150L;
        boolean bool5 = l1 < l2;
        if (bool5)
        {
          localObject1 = f;
          if (localObject1 != null) {
            ((BubbleLayout.b)localObject1).a();
          }
        }
        break;
      case 0: 
        int i1 = getViewParamsx;
        d = i1;
        localObject1 = getViewParams();
        i1 = y;
        e = i1;
        f4 = paramMotionEvent.getRawX();
        b = f4;
        f4 = paramMotionEvent.getRawY();
        c = f4;
        boolean bool3 = isInEditMode();
        if (!bool3)
        {
          localObject1 = getContext();
          i4 = 2130837504;
          f1 = 1.7279964E38F;
          localObject1 = (AnimatorSet)AnimatorInflater.loadAnimator((Context)localObject1, i4);
          ((AnimatorSet)localObject1).setTarget(this);
          ((AnimatorSet)localObject1).start();
        }
        l1 = System.currentTimeMillis();
        g = l1;
        localObject1 = new android/util/DisplayMetrics;
        ((DisplayMetrics)localObject1).<init>();
        j.getDefaultDisplay().getMetrics((DisplayMetrics)localObject1);
        localObject1 = getWindowManager().getDefaultDisplay();
        localObject2 = new android/graphics/Point;
        ((Point)localObject2).<init>();
        ((Display)localObject1).getSize((Point)localObject2);
        int i2 = x;
        i4 = getWidth();
        i2 -= i4;
        i = i2;
        localObject1 = h;
        BubbleLayout.a.a((BubbleLayout.a)localObject1);
      }
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public void setOnBubbleClickListener(BubbleLayout.b paramb)
  {
    f = paramb;
  }
  
  public void setOnBubbleMovedListener(BubbleLayout.c paramc)
  {
    l = paramc;
  }
  
  public void setOnBubbleRemoveListener(BubbleLayout.d paramd)
  {
    a = paramd;
  }
  
  public void setShouldStickToWall(boolean paramBoolean)
  {
    k = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.BubbleLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */