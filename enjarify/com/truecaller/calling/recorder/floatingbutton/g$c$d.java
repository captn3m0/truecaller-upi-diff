package com.truecaller.calling.recorder.floatingbutton;

import android.telephony.TelephonyManager;
import c.g.b.k;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.w;
import com.truecaller.callhistory.a;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter;
import com.truecaller.calling.recorder.ba;
import com.truecaller.calling.recorder.ba.a;
import java.net.URLDecoder;
import java.util.TimerTask;

public final class g$c$d
  extends TimerTask
{
  public g$c$d(g.c paramc) {}
  
  public final void run()
  {
    Object localObject1 = a.a.c;
    int i = ((TelephonyManager)localObject1).getCallState();
    if (i == 0)
    {
      localObject1 = g.c(a.a);
      if (localObject1 != null)
      {
        localObject1 = a;
        if (localObject1 == null)
        {
          str = "presenter";
          k.a(str);
        }
        String str = a;
        if (str != null)
        {
          localObject1 = m;
          k.b(str, "recordingFileAbsolutePath");
          Object localObject2 = URLDecoder.decode(str, "UTF-8");
          k.a(localObject2, "URLDecoder.decode(record…ileAbsolutePath, \"UTF-8\")");
          localObject2 = (CharSequence)localObject2;
          Object localObject3 = { "-" };
          int j = 6;
          localObject2 = (CharSequence)c.a.m.f(c.n.m.c((CharSequence)localObject2, (String[])localObject3, false, j));
          localObject3 = new String[] { "." };
          localObject2 = (String)c.a.m.d(c.n.m.c((CharSequence)localObject2, (String[])localObject3, false, j));
          localObject2 = a.c((String)localObject2);
          localObject3 = new com/truecaller/calling/recorder/ba$a;
          ((ba.a)localObject3).<init>((ba)localObject1, str);
          localObject3 = (ac)localObject3;
          ((w)localObject2).a((ac)localObject3);
        }
      }
      localObject1 = a.a;
      ((g)localObject1).a();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.g.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */