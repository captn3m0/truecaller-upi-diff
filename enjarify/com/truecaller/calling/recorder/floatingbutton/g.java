package com.truecaller.calling.recorder.floatingbutton;

import android.content.Context;
import android.content.res.Resources;
import android.telephony.TelephonyManager;
import c.d.f;
import c.g.a.m;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import com.truecaller.calling.recorder.CallRecordingManager;
import java.util.Timer;
import kotlinx.coroutines.ag;

public final class g
  implements e, ag
{
  final Context a;
  final CallRecordingManager b;
  final TelephonyManager c;
  private d d;
  private boolean e;
  private BubbleLayout f;
  private Timer g;
  private CallRecordingFloatingButton h;
  private boolean i;
  private final g.a j;
  private final Resources k;
  private final f l;
  private final long m;
  
  public g(Context paramContext, Resources paramResources, CallRecordingManager paramCallRecordingManager, f paramf, TelephonyManager paramTelephonyManager, long paramLong)
  {
    a = paramContext;
    k = paramResources;
    b = paramCallRecordingManager;
    l = paramf;
    c = paramTelephonyManager;
    m = paramLong;
    paramContext = new com/truecaller/calling/recorder/floatingbutton/g$a;
    paramContext.<init>(this);
    j = paramContext;
  }
  
  public final f V_()
  {
    return l;
  }
  
  public final void a()
  {
    BubbleLayout localBubbleLayout = f;
    if (localBubbleLayout != null)
    {
      d locald = d;
      if (locald != null) {
        locald.a(localBubbleLayout);
      }
      return;
    }
  }
  
  public final void a(String paramString, i parami)
  {
    d.a locala = new com/truecaller/calling/recorder/floatingbutton/d$a;
    Object localObject = a;
    locala.<init>((Context)localObject);
    locala = locala.a();
    localObject = new com/truecaller/calling/recorder/floatingbutton/g$c;
    ((g.c)localObject).<init>(this, paramString, parami);
    localObject = (j)localObject;
    paramString = locala.a((j)localObject);
    parami = new com/truecaller/calling/recorder/floatingbutton/g$d;
    parami.<init>(this);
    parami = (c.b)parami;
    paramString = paramString.a(parami).b();
    d = paramString;
    paramString = d;
    if (paramString != null)
    {
      paramString.a();
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = new com/truecaller/calling/recorder/floatingbutton/g$b;
    ((g.b)localObject).<init>(this, paramBoolean, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */