package com.truecaller.calling.recorder.floatingbutton;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class f$c
  extends u
{
  private final String b;
  private final i c;
  
  private f$c(e parame, String paramString, i parami)
  {
    super(parame);
    b = paramString;
    c = parami;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".showCallRecordingButton(");
    String str = b;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(c, i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.f.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */