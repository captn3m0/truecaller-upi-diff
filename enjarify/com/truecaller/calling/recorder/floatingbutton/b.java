package com.truecaller.calling.recorder.floatingbutton;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.os.Vibrator;
import android.view.View;

public final class b
  extends a
{
  private boolean a = false;
  private boolean b = false;
  private boolean c = false;
  
  public b(Context paramContext)
  {
    super(paramContext);
  }
  
  private void a(int paramInt)
  {
    boolean bool = isInEditMode();
    if (!bool)
    {
      AnimatorSet localAnimatorSet = (AnimatorSet)AnimatorInflater.loadAnimator(getContext(), paramInt);
      bool = false;
      View localView = getChildAt(0);
      localAnimatorSet.setTarget(localView);
      localAnimatorSet.start();
    }
  }
  
  final void a()
  {
    boolean bool = a;
    if (!bool)
    {
      a = true;
      int i = 2130837509;
      a(i);
    }
  }
  
  final void b()
  {
    boolean bool = c;
    if (!bool)
    {
      Object localObject = getContext();
      String str = "vibrator";
      localObject = (Vibrator)((Context)localObject).getSystemService(str);
      long l = 70;
      ((Vibrator)localObject).vibrate(l);
      bool = true;
      c = bool;
    }
  }
  
  final void c()
  {
    boolean bool = a;
    if (bool)
    {
      a = false;
      int i = 2130837507;
      a(i);
    }
    c = false;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    b = true;
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    b = false;
  }
  
  public final void setVisibility(int paramInt)
  {
    boolean bool = b;
    if (bool)
    {
      int i = getVisibility();
      if (paramInt != i) {
        if (paramInt == 0)
        {
          i = 2130837508;
          a(i);
        }
        else
        {
          i = 2130837506;
          a(i);
        }
      }
    }
    super.setVisibility(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */