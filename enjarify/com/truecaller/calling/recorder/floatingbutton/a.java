package com.truecaller.calling.recorder.floatingbutton;

import android.content.Context;
import android.util.AttributeSet;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;

class a
  extends FrameLayout
{
  private WindowManager a;
  private WindowManager.LayoutParams b;
  private c c;
  
  public a(Context paramContext)
  {
    super(paramContext);
  }
  
  public a(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  c getLayoutCoordinator()
  {
    return c;
  }
  
  WindowManager.LayoutParams getViewParams()
  {
    return b;
  }
  
  WindowManager getWindowManager()
  {
    return a;
  }
  
  void setLayoutCoordinator(c paramc)
  {
    c = paramc;
  }
  
  void setViewParams(WindowManager.LayoutParams paramLayoutParams)
  {
    b = paramLayoutParams;
  }
  
  void setWindowManager(WindowManager paramWindowManager)
  {
    a = paramWindowManager;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */