package com.truecaller.calling.recorder.floatingbutton;

import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

public final class c
{
  static c a;
  b b;
  WindowManager c;
  BubblesService d;
  c.b e;
  
  private View a()
  {
    return b.getChildAt(0);
  }
  
  private void c(BubbleLayout paramBubbleLayout)
  {
    Object localObject = a();
    int i = ((View)localObject).getLeft();
    int j = ((View)localObject).getMeasuredWidth() / 2;
    i += j;
    j = ((View)localObject).getTop();
    int k = ((View)localObject).getMeasuredHeight() / 2;
    j += k;
    k = paramBubbleLayout.getMeasuredWidth() / 2;
    i -= k;
    k = paramBubbleLayout.getMeasuredHeight() / 2;
    j -= k;
    getViewParamsx = i;
    getViewParamsy = j;
    localObject = c;
    WindowManager.LayoutParams localLayoutParams = paramBubbleLayout.getViewParams();
    ((WindowManager)localObject).updateViewLayout(paramBubbleLayout, localLayoutParams);
  }
  
  public final void a(BubbleLayout paramBubbleLayout)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = e;
      boolean bool;
      if (localObject != null)
      {
        bool = ((c.b)localObject).a();
        if (!bool) {}
      }
      else
      {
        localObject = b;
        ((b)localObject).setVisibility(0);
        bool = b(paramBubbleLayout);
        if (bool)
        {
          b.a();
          b.b();
          c(paramBubbleLayout);
          return;
        }
        paramBubbleLayout = b;
        paramBubbleLayout.c();
      }
    }
  }
  
  final boolean b(BubbleLayout paramBubbleLayout)
  {
    Object localObject = b;
    int i = ((b)localObject).getVisibility();
    if (i == 0)
    {
      localObject = a();
      int j = ((View)localObject).getMeasuredHeight();
      i = ((View)localObject).getTop() - j;
      paramBubbleLayout = paramBubbleLayout.getViewParams();
      k = y;
      if (k >= i)
      {
        k = 1;
        break label61;
      }
    }
    int k = 0;
    paramBubbleLayout = null;
    label61:
    return k;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */