package com.truecaller.calling.recorder.floatingbutton;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import kotlinx.coroutines.ag;

final class g$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  g$b(g paramg, boolean paramBoolean, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/calling/recorder/floatingbutton/g$b;
    g localg = b;
    boolean bool = c;
    localb.<init>(localg, bool, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = g.b(b);
        if (paramObject != null)
        {
          int j = 2131362380;
          paramObject = (CallRecordingFloatingButton)((BubbleLayout)paramObject).findViewById(j);
          if (paramObject != null)
          {
            boolean bool2 = c;
            ((CallRecordingFloatingButton)paramObject).a(bool2);
          }
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.g.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */