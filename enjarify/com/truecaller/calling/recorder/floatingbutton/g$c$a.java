package com.truecaller.calling.recorder.floatingbutton;

import android.os.Handler;
import android.os.Looper;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.CallRecordingButtonMode;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.c;
import com.truecaller.calling.recorder.CallRecordingManager;
import java.util.Timer;
import java.util.TimerTask;
import org.a.a.b;

final class g$c$a
  implements BubbleLayout.b
{
  g$c$a(g.c paramc) {}
  
  public final void a()
  {
    boolean bool1 = true;
    Object localObject1 = new String[bool1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("showCallRecordingButton:: Debounce? ");
    boolean bool2 = g.d(a.a);
    ((StringBuilder)localObject2).append(bool2);
    localObject2 = ((StringBuilder)localObject2).toString();
    bool2 = false;
    Object localObject3 = null;
    localObject1[0] = localObject2;
    localObject1 = a.a;
    boolean bool3 = g.d((g)localObject1);
    if (!bool3)
    {
      localObject1 = g.c(a.a);
      if (localObject1 != null)
      {
        localObject2 = a;
        if (localObject2 == null)
        {
          localObject3 = "presenter";
          c.g.b.k.a((String)localObject3);
        }
        localObject3 = c;
        Object localObject4 = com.truecaller.calling.recorder.k.a;
        int i = ((CallRecordingFloatingButtonPresenter.CallRecordingButtonMode)localObject3).ordinal();
        i = localObject4[i];
        switch (i)
        {
        default: 
          break;
        case 2: 
          localObject3 = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.ENDED;
          c = ((CallRecordingFloatingButtonPresenter.CallRecordingButtonMode)localObject3);
          i.e();
          localObject3 = f;
          if (localObject3 != null) {
            ((Timer)localObject3).cancel();
          }
          i = 0;
          localObject3 = null;
          f = null;
          a = null;
          ((CallRecordingFloatingButtonPresenter)localObject2).c();
          break;
        case 1: 
          localObject3 = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.RECORDING;
          c = ((CallRecordingFloatingButtonPresenter.CallRecordingButtonMode)localObject3);
          localObject3 = i;
          localObject4 = h;
          ((CallRecordingManager)localObject3).a((String)localObject4);
          localObject3 = new org/a/a/b;
          long l1 = j.a();
          ((b)localObject3).<init>(l1);
          e = ((b)localObject3);
          localObject3 = c.c.a.a("CallRecorderCountUpTimer");
          localObject4 = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$c;
          ((CallRecordingFloatingButtonPresenter.c)localObject4).<init>((CallRecordingFloatingButtonPresenter)localObject2);
          Object localObject5 = localObject4;
          localObject5 = (TimerTask)localObject4;
          long l2 = 500L;
          long l3 = 1000L;
          localObject4 = localObject3;
          ((Timer)localObject3).schedule((TimerTask)localObject5, l2, l3);
          f = ((Timer)localObject3);
          ((CallRecordingFloatingButtonPresenter)localObject2).c();
        }
        ((CallRecordingFloatingButton)localObject1).performClick();
      }
      g.a(a.a, bool1);
      Handler localHandler = new android/os/Handler;
      localObject1 = Looper.getMainLooper();
      localHandler.<init>((Looper)localObject1);
      localObject1 = new com/truecaller/calling/recorder/floatingbutton/g$c$a$1;
      ((g.c.a.1)localObject1).<init>(this);
      localObject1 = (Runnable)localObject1;
      long l4 = 1000L;
      localHandler.postDelayed((Runnable)localObject1, l4);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.g.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */