package com.truecaller.calling.recorder.floatingbutton;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.WindowManager.LayoutParams;

final class BubbleLayout$a
  implements Runnable
{
  private Handler b;
  private float c;
  private float d;
  private long e;
  
  private BubbleLayout$a(BubbleLayout paramBubbleLayout)
  {
    paramBubbleLayout = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    paramBubbleLayout.<init>(localLooper);
    b = paramBubbleLayout;
  }
  
  public final void run()
  {
    Object localObject = a.getRootView();
    if (localObject != null)
    {
      localObject = a.getRootView().getParent();
      if (localObject != null)
      {
        long l1 = System.currentTimeMillis();
        long l2 = e;
        l1 -= l2;
        float f1 = (float)l1 / 400.0F;
        float f2 = 1.0F;
        f1 = Math.min(f2, f1);
        float f3 = c;
        WindowManager.LayoutParams localLayoutParams = a.getViewParams();
        int i = x;
        float f4 = i;
        f3 = (f3 - f4) * f1;
        f4 = d;
        int j = a.getViewParams().y;
        float f5 = j;
        f4 = (f4 - f5) * f1;
        BubbleLayout localBubbleLayout = a;
        BubbleLayout.a(localBubbleLayout, f3, f4);
        boolean bool = f1 < f2;
        if (bool)
        {
          localObject = b;
          ((Handler)localObject).post(this);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.BubbleLayout.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */