package com.truecaller.calling.recorder.floatingbutton;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class f$b
  extends u
{
  private final boolean b;
  
  private f$b(e parame, boolean paramBoolean)
  {
    super(parame);
    b = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".showBrandingView(");
    String str = a(Boolean.valueOf(b), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */