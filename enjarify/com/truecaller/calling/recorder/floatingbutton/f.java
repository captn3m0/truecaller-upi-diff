package com.truecaller.calling.recorder.floatingbutton;

import com.truecaller.androidactors.v;

public final class f
  implements e
{
  private final v a;
  
  public f(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return e.class.equals(paramClass);
  }
  
  public final void a()
  {
    v localv = a;
    f.a locala = new com/truecaller/calling/recorder/floatingbutton/f$a;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, (byte)0);
    localv.a(locala);
  }
  
  public final void a(String paramString, i parami)
  {
    v localv = a;
    f.c localc = new com/truecaller/calling/recorder/floatingbutton/f$c;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramString, parami, (byte)0);
    localv.a(localc);
  }
  
  public final void a(boolean paramBoolean)
  {
    v localv = a;
    f.b localb = new com/truecaller/calling/recorder/floatingbutton/f$b;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramBoolean, (byte)0);
    localv.a(localb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */