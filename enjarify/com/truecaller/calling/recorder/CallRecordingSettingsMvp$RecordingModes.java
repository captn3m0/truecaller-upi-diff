package com.truecaller.calling.recorder;

public enum CallRecordingSettingsMvp$RecordingModes
{
  static
  {
    RecordingModes[] arrayOfRecordingModes = new RecordingModes[2];
    RecordingModes localRecordingModes = new com/truecaller/calling/recorder/CallRecordingSettingsMvp$RecordingModes;
    localRecordingModes.<init>("AUTO", 0);
    AUTO = localRecordingModes;
    arrayOfRecordingModes[0] = localRecordingModes;
    localRecordingModes = new com/truecaller/calling/recorder/CallRecordingSettingsMvp$RecordingModes;
    int i = 1;
    localRecordingModes.<init>("MANUAL", i);
    MANUAL = localRecordingModes;
    arrayOfRecordingModes[i] = localRecordingModes;
    $VALUES = arrayOfRecordingModes;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsMvp.RecordingModes
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */