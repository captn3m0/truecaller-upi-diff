package com.truecaller.calling.recorder;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.view.View;
import c.g.b.k;

public final class y
  implements x
{
  public final void a(int paramInt, View paramView, ca paramca)
  {
    k.b(paramView, "anchorView");
    k.b(paramca, "clickListener");
    PopupMenu localPopupMenu = new android/support/v7/widget/PopupMenu;
    Context localContext = paramView.getContext();
    localPopupMenu.<init>(localContext, paramView);
    paramView = new com/truecaller/calling/recorder/y$a;
    paramView.<init>(paramca, paramInt);
    paramView = (PopupMenu.OnMenuItemClickListener)paramView;
    localPopupMenu.setOnMenuItemClickListener(paramView);
    localPopupMenu.inflate(2131623939);
    localPopupMenu.show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */