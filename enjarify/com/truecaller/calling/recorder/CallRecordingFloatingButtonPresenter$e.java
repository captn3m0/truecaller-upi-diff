package com.truecaller.calling.recorder;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class CallRecordingFloatingButtonPresenter$e
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  CallRecordingFloatingButtonPresenter$e(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$e;
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = b;
    locale.<init>(localCallRecordingFloatingButtonPresenter, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.c;
        localObject = k.b;
        int j = ((CallRecordingFloatingButtonPresenter.CallRecordingButtonMode)paramObject).ordinal();
        j = localObject[j];
        switch (j)
        {
        default: 
          break;
        case 3: 
          paramObject = (bw)b.b;
          if (paramObject != null) {
            ((bw)paramObject).c();
          }
          break;
        case 2: 
          paramObject = (bw)b.b;
          if (paramObject != null) {
            ((bw)paramObject).b();
          }
          break;
        case 1: 
          paramObject = (bw)b.b;
          if (paramObject != null) {
            ((bw)paramObject).a();
          }
          break;
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */