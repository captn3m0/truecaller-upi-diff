package com.truecaller.calling.recorder;

import c.l.g;
import com.truecaller.androidactors.w;
import com.truecaller.callhistory.z;
import com.truecaller.calling.dialer.cb;
import com.truecaller.data.entity.CallRecording;

public abstract interface bk
{
  public abstract z a(ad paramad, g paramg);
  
  public abstract cb a(ac paramac);
  
  public abstract boolean a(CallRecording paramCallRecording);
  
  public abstract void b(CallRecording paramCallRecording);
  
  public abstract w c(CallRecording paramCallRecording);
  
  public abstract void e();
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bk
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */