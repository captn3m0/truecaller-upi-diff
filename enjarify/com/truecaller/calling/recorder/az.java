package com.truecaller.calling.recorder;

import dagger.a.d;
import javax.inject.Provider;

public final class az
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private az(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static az a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    az localaz = new com/truecaller/calling/recorder/az;
    localaz.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localaz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.az
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */