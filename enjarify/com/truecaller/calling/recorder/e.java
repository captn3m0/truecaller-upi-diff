package com.truecaller.calling.recorder;

import c.g.a.m;
import c.g.b.k;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.CallRecording;

public final class e
  implements c
{
  private final bx a;
  private final c.d.f b;
  
  public e(bx parambx, c.d.f paramf)
  {
    a = parambx;
    b = paramf;
  }
  
  public final w a(CallRecording paramCallRecording)
  {
    k.b(paramCallRecording, "callRecording");
    e.a locala = new com/truecaller/calling/recorder/e$a;
    locala.<init>(this, paramCallRecording, null);
    paramCallRecording = w.b(Long.valueOf(((Number)kotlinx.coroutines.f.a((m)locala)).longValue()));
    k.a(paramCallRecording, "Promise.wrap(duration)");
    return paramCallRecording;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */