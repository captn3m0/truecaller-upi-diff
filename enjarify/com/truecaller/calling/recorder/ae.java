package com.truecaller.calling.recorder;

import android.content.Context;
import android.telephony.TelephonyManager;
import c.g.b.k;
import com.truecaller.analytics.e.a;
import com.truecaller.callrecording.CallRecorder;
import com.truecaller.callrecording.CallRecorder.RecordingState;
import com.truecaller.callrecording.CallRecorder.a;
import com.truecaller.common.f.c;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings;
import com.truecaller.util.aq;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import com.truecaller.wizard.utils.i;
import java.util.Timer;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import org.a.a.x;

public final class ae
  implements CallRecordingManager, g, CallRecorder.a
{
  CallRecorder a;
  String b;
  org.a.a.b c;
  bi d;
  final org.a.a.d.b e;
  Timer f;
  final com.truecaller.androidactors.f g;
  final ax h;
  final com.truecaller.utils.a i;
  final TelephonyManager j;
  final long k;
  private final String[] l;
  private f m;
  private long n;
  private final Context o;
  private final com.truecaller.common.g.a p;
  private final aq q;
  private final h r;
  private final u s;
  private final c t;
  private final c.d.f u;
  private final com.truecaller.analytics.b v;
  private final l w;
  private final n x;
  private final aj y;
  private final cd z;
  
  public ae(Context paramContext, com.truecaller.common.g.a parama, com.truecaller.androidactors.f paramf, ax paramax, com.truecaller.utils.a parama1, aq paramaq, h paramh, u paramu, c paramc, c.d.f paramf1, com.truecaller.analytics.b paramb, l paraml, n paramn, TelephonyManager paramTelephonyManager, aj paramaj, cd paramcd, long paramLong)
  {
    o = paramContext;
    p = parama;
    g = paramf;
    h = paramax;
    i = parama1;
    q = paramaq;
    r = paramh;
    s = paramu;
    t = paramc;
    u = paramf1;
    v = paramb;
    w = paraml;
    x = paramn;
    j = paramTelephonyManager;
    localObject = paramaj;
    y = paramaj;
    z = paramcd;
    k = paramLong;
    String[] tmp223_220 = new String[5];
    String[] tmp224_223 = tmp223_220;
    String[] tmp224_223 = tmp223_220;
    tmp224_223[0] = "android.permission.RECORD_AUDIO";
    tmp224_223[1] = "android.permission.CALL_PHONE";
    String[] tmp233_224 = tmp224_223;
    String[] tmp233_224 = tmp224_223;
    tmp233_224[2] = "android.permission.READ_EXTERNAL_STORAGE";
    tmp233_224[3] = "android.permission.WRITE_EXTERNAL_STORAGE";
    tmp233_224[4] = "android.permission.READ_PHONE_STATE";
    tmp223_220 = tmp233_224;
    l = ((String[])tmp223_220);
    tmp223_220 = org.a.a.d.a.a("yyMMdd-HHmmss");
    e = ((org.a.a.d.b)tmp223_220);
  }
  
  private final void b(e.a parama)
  {
    Object localObject1 = "PremiumStatus";
    Object localObject2 = t;
    boolean bool1 = ((c)localObject2).d();
    if (bool1) {
      localObject2 = "Premium";
    } else {
      localObject2 = "Free";
    }
    parama.a((String)localObject1, (String)localObject2);
    localObject2 = n().name();
    parama.a("TrialStatus", (String)localObject2);
    localObject1 = "RecordingMode";
    localObject2 = p.a("callRecordingMode");
    Object localObject3 = CallRecordingSettingsMvp.RecordingModes.AUTO.name();
    boolean bool2 = true;
    bool1 = c.n.m.a((String)localObject2, (String)localObject3, bool2);
    if (bool1) {
      localObject2 = "Auto";
    } else {
      localObject2 = "Manual";
    }
    parama.a((String)localObject1, (String)localObject2);
    localObject1 = "RecordingConfig";
    localObject2 = r.d();
    localObject3 = CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER;
    if (localObject2 == localObject3) {
      localObject2 = "SdkConfig";
    } else {
      localObject2 = "Default";
    }
    parama.a((String)localObject1, (String)localObject2);
    localObject1 = v;
    parama = parama.a();
    k.a(parama, "eventBuilder.build()");
    ((com.truecaller.analytics.b)localObject1).a(parama);
  }
  
  public final void a(e.a parama)
  {
    k.b(parama, "eventBuilder");
    b(parama);
  }
  
  public final void a(bi parambi)
  {
    d = parambi;
  }
  
  public final void a(f paramf)
  {
    k.b(paramf, "listener");
    m = paramf;
  }
  
  public final void a(CallRecording paramCallRecording, CallRecordingManager.PlaybackLaunchContext paramPlaybackLaunchContext)
  {
    k.b(paramCallRecording, "callRecording");
    k.b(paramPlaybackLaunchContext, "playbackLaunchContext");
    Object localObject1 = CallRecordingOnBoardingLaunchContext.UNKNOWN;
    boolean bool1 = a((CallRecordingOnBoardingLaunchContext)localObject1);
    if (bool1) {
      return;
    }
    localObject1 = s;
    paramCallRecording = c;
    paramCallRecording = ((u)localObject1).a(paramCallRecording);
    boolean bool2 = ((u)localObject1).a(paramCallRecording);
    bool1 = false;
    localObject1 = null;
    Object localObject2;
    int i1;
    if (!bool2)
    {
      paramCallRecording = z;
      localObject2 = x;
      i1 = 2131887612;
      localObject1 = new Object[0];
      localObject1 = ((n)localObject2).a(i1, (Object[])localObject1);
      localObject2 = "resourceProvider.getStri…r_no_activity_found_play)";
      k.a(localObject1, (String)localObject2);
      paramCallRecording.toast((String)localObject1);
    }
    else
    {
      paramCallRecording = z;
      localObject2 = x;
      i1 = 2131887616;
      localObject1 = new Object[0];
      localObject1 = ((n)localObject2).a(i1, (Object[])localObject1);
      localObject2 = "resourceProvider.getStri…ecording_toast_item_play)";
      k.a(localObject1, (String)localObject2);
      paramCallRecording.toast((String)localObject1);
    }
    paramCallRecording = new com/truecaller/analytics/e$a;
    paramCallRecording.<init>("CallRecordingPlayback");
    paramPlaybackLaunchContext = paramPlaybackLaunchContext.name();
    paramCallRecording = paramCallRecording.a("Source", paramPlaybackLaunchContext);
    k.a(paramCallRecording, "AnalyticsEvent.Builder(C…aybackLaunchContext.name)");
    b(paramCallRecording);
  }
  
  public final void a(String paramString)
  {
    boolean bool1 = a();
    if (!bool1) {
      return;
    }
    try
    {
      Object localObject1 = q;
      Object localObject2 = k();
      bool1 = ((aq)localObject1).a((String)localObject2);
      if (!bool1)
      {
        localObject1 = q;
        localObject2 = k();
        bool1 = ((aq)localObject1).b((String)localObject2);
        if (!bool1)
        {
          paramString = "Failed to create recording directory";
          AssertionUtil.reportWeirdnessButNeverCrash(paramString);
          return;
        }
      }
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject2 = k();
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject2 = "/.nomedia";
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject1 = ((StringBuilder)localObject1).toString();
      localObject2 = q;
      boolean bool2 = ((aq)localObject2).d((String)localObject1);
      if (!bool2)
      {
        localObject2 = q;
        bool1 = ((aq)localObject2).e((String)localObject1);
        if (!bool1)
        {
          localObject1 = "Failed to create nomedia file";
          AssertionUtil.reportWeirdnessButNeverCrash((String)localObject1);
        }
      }
      localObject1 = (ag)bg.a;
      localObject2 = u;
      Object localObject3 = new com/truecaller/calling/recorder/ae$a;
      ((ae.a)localObject3).<init>(this, paramString, null);
      localObject3 = (c.g.a.m)localObject3;
      kotlinx.coroutines.e.b((ag)localObject1, (c.d.f)localObject2, (c.g.a.m)localObject3, 2);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
    }
  }
  
  public final boolean a()
  {
    Object localObject1 = r;
    boolean bool1 = ((h)localObject1).a();
    if (bool1)
    {
      bool1 = g();
      if (!bool1)
      {
        localObject1 = n();
        bool1 = ((FreeTrialStatus)localObject1).isActive();
        if (!bool1)
        {
          localObject1 = t;
          bool1 = ((c)localObject1).d();
          if (!bool1) {}
        }
        else
        {
          localObject1 = p;
          String str = "callRecordingEnbaled";
          bool1 = ((com.truecaller.common.g.a)localObject1).b(str);
          if (bool1)
          {
            localObject1 = l;
            int i1 = localObject1.length;
            int i2 = 0;
            boolean bool2;
            for (;;)
            {
              bool2 = true;
              if (i2 >= i1) {
                break;
              }
              Object localObject2 = localObject1[i2];
              l locall = w;
              String[] arrayOfString = new String[bool2];
              arrayOfString[0] = localObject2;
              boolean bool3 = locall.a(arrayOfString);
              if (!bool3)
              {
                bool1 = false;
                localObject1 = null;
                break label157;
              }
              i2 += 1;
            }
            bool1 = true;
            label157:
            if (bool1) {
              return bool2;
            }
          }
        }
      }
    }
    return false;
  }
  
  public final boolean a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    String str = "grantResults";
    k.b(paramArrayOfInt, str);
    int i1 = 102;
    if (paramInt == i1)
    {
      i.a(paramArrayOfString, paramArrayOfInt);
      return true;
    }
    return false;
  }
  
  public final boolean a(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject1 = r;
    boolean bool = ((h)localObject1).c();
    if (bool)
    {
      localObject1 = t;
      bool = ((c)localObject1).d();
      if (!bool)
      {
        localObject1 = n();
        Object localObject2 = FreeTrialStatus.EXPIRED;
        if (localObject1 == localObject2)
        {
          localObject1 = o;
          localObject2 = CallRecordingOnBoardingState.PAY_WALL_ON_EXPIRY;
          aj.a((Context)localObject1, (CallRecordingOnBoardingState)localObject2, paramCallRecordingOnBoardingLaunchContext);
          return true;
        }
      }
    }
    return false;
  }
  
  public final String b()
  {
    CallRecorder localCallRecorder = a;
    if (localCallRecorder != null) {
      return localCallRecorder.getOutputFile();
    }
    return null;
  }
  
  public final org.a.a.b c()
  {
    return c;
  }
  
  public final boolean d()
  {
    String str1 = p.a("callRecordingMode");
    String str2 = CallRecordingSettingsMvp.RecordingModes.AUTO.name();
    return c.n.m.a(str1, str2, true);
  }
  
  public final void e()
  {
    Object localObject = a;
    if (localObject != null) {
      localObject = ((CallRecorder)localObject).getRecordingState();
    } else {
      localObject = null;
    }
    CallRecorder.RecordingState localRecordingState = CallRecorder.RecordingState.RECORDING;
    if (localObject != localRecordingState) {
      return;
    }
    l();
  }
  
  public final boolean f()
  {
    CallRecorder localCallRecorder = a;
    if (localCallRecorder != null) {
      return localCallRecorder.isRecording();
    }
    return false;
  }
  
  public final boolean g()
  {
    Object localObject = r;
    boolean bool = ((h)localObject).b();
    if (bool)
    {
      localObject = t;
      bool = ((c)localObject).d();
      if (!bool)
      {
        localObject = n();
        bool = ((FreeTrialStatus)localObject).isActive();
        if (!bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final void h()
  {
    CallRecorder localCallRecorder = a;
    if (localCallRecorder != null) {
      localCallRecorder.setOutputFile(null);
    }
    b = null;
    c = null;
    n = 0L;
  }
  
  public final boolean i()
  {
    String str = b;
    if (str != null)
    {
      boolean bool1 = d();
      if (bool1)
      {
        Object localObject = str;
        localObject = (CharSequence)str;
        int i1 = ((CharSequence)localObject).length();
        boolean bool3 = true;
        if (i1 > 0)
        {
          i1 = 1;
        }
        else
        {
          i1 = 0;
          localObject = null;
        }
        if (i1 != 0)
        {
          localObject = c;
          if (localObject != null)
          {
            long l1 = n;
            localObject = TimeUnit.SECONDS;
            long l2 = ((TimeUnit)localObject).toMillis(4);
            boolean bool2 = l1 < l2;
            if (bool2)
            {
              q.c(str);
              b = null;
              return bool3;
            }
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final boolean j()
  {
    return Settings.e("qaEnableRecorderLeak");
  }
  
  public final String k()
  {
    Object localObject = p;
    String str = bf.a();
    localObject = ((com.truecaller.common.g.a)localObject).b("callRecordingStoragePath", str);
    k.a(localObject, "coreSettings.getString(\n…ngStoragePath()\n        )");
    return (String)localObject;
  }
  
  final void l()
  {
    ((com.truecaller.callerid.e)g.a()).d();
    Object localObject = f;
    if (localObject != null) {
      ((Timer)localObject).cancel();
    }
    localObject = null;
    f = null;
    try
    {
      localObject = a;
      if (localObject != null) {
        ((CallRecorder)localObject).stop();
      }
      localObject = a;
      if (localObject != null) {
        ((CallRecorder)localObject).release();
      }
      localObject = a;
      if (localObject != null) {
        ((CallRecorder)localObject).reset();
      }
      localObject = c;
      if (localObject != null)
      {
        com.truecaller.utils.a locala = i;
        long l1 = locala.a();
        long l2 = a;
        l1 -= l2;
        n = l1;
      }
      return;
    }
    catch (Exception localException) {}
  }
  
  public final int m()
  {
    Object localObject1 = p;
    Object localObject2 = "key_call_recording_trial_start_timestamp";
    long l1 = -1;
    long l2 = ((com.truecaller.common.g.a)localObject1).a((String)localObject2, l1);
    int i1 = -1;
    boolean bool1 = l2 < l1;
    if (!bool1) {
      return i1;
    }
    Object localObject3 = new org/a/a/b;
    ((org.a.a.b)localObject3).<init>(l2);
    int i2 = 14;
    localObject1 = ((org.a.a.b)localObject3).a(i2);
    localObject2 = new org/a/a/b;
    l1 = i.a();
    ((org.a.a.b)localObject2).<init>(l1);
    localObject3 = localObject1;
    localObject3 = (x)localObject1;
    boolean bool2 = ((org.a.a.b)localObject2).b((x)localObject3);
    if (bool2) {
      return i1;
    }
    localObject2 = (x)((org.a.a.b)localObject2).az_();
    localObject1 = (x)((org.a.a.b)localObject1).az_();
    localObject1 = org.a.a.g.a((x)localObject2, (x)localObject1);
    k.a(localObject1, "Days.daysBetween(today.w…e.withTimeAtStartOfDay())");
    return ((org.a.a.g)localObject1).c();
  }
  
  public final FreeTrialStatus n()
  {
    com.truecaller.common.g.a locala = p;
    String str = "key_call_recording_trial_start_timestamp";
    long l1 = -1;
    long l2 = locala.a(str, l1);
    boolean bool = l2 < l1;
    if (!bool) {
      return FreeTrialStatus.NOT_STARTED;
    }
    int i1 = m();
    int i2 = -1;
    if (i1 == i2) {
      return FreeTrialStatus.EXPIRED;
    }
    return FreeTrialStatus.ACTIVE;
  }
  
  public final void onError(Exception paramException)
  {
    String str = "e";
    k.b(paramException, str);
    b = null;
    l();
    paramException = m;
    if (paramException != null)
    {
      paramException.a();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */