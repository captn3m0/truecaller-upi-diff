package com.truecaller.calling.recorder;

public enum FreeTrialStatus
{
  private final String freeTrialStatus;
  
  static
  {
    FreeTrialStatus[] arrayOfFreeTrialStatus = new FreeTrialStatus[3];
    FreeTrialStatus localFreeTrialStatus = new com/truecaller/calling/recorder/FreeTrialStatus;
    localFreeTrialStatus.<init>("NOT_STARTED", 0, "NotStarted");
    NOT_STARTED = localFreeTrialStatus;
    arrayOfFreeTrialStatus[0] = localFreeTrialStatus;
    localFreeTrialStatus = new com/truecaller/calling/recorder/FreeTrialStatus;
    int i = 1;
    localFreeTrialStatus.<init>("ACTIVE", i, "NotStarted");
    ACTIVE = localFreeTrialStatus;
    arrayOfFreeTrialStatus[i] = localFreeTrialStatus;
    localFreeTrialStatus = new com/truecaller/calling/recorder/FreeTrialStatus;
    i = 2;
    localFreeTrialStatus.<init>("EXPIRED", i, "NotStarted");
    EXPIRED = localFreeTrialStatus;
    arrayOfFreeTrialStatus[i] = localFreeTrialStatus;
    $VALUES = arrayOfFreeTrialStatus;
  }
  
  private FreeTrialStatus(String paramString1)
  {
    freeTrialStatus = paramString1;
  }
  
  public final String getFreeTrialStatus()
  {
    return freeTrialStatus;
  }
  
  public final boolean isActive()
  {
    FreeTrialStatus localFreeTrialStatus1 = this;
    localFreeTrialStatus1 = (FreeTrialStatus)this;
    FreeTrialStatus localFreeTrialStatus2 = ACTIVE;
    return localFreeTrialStatus1 == localFreeTrialStatus2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.FreeTrialStatus
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */