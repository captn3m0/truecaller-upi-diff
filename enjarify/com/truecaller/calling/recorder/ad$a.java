package com.truecaller.calling.recorder;

import c.d.b.a.b;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import java.util.Map;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

final class ad$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private ag n;
  
  ad$a(c paramc, aa paramaa1, ad paramad, Contact paramContact, boolean paramBoolean1, HistoryEvent paramHistoryEvent, String paramString, CallRecording paramCallRecording, boolean paramBoolean2, aa paramaa2, int paramInt)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/calling/recorder/ad$a;
    aa localaa1 = d;
    ad localad = e;
    Contact localContact = f;
    boolean bool1 = g;
    HistoryEvent localHistoryEvent = h;
    String str = i;
    CallRecording localCallRecording = j;
    boolean bool2 = k;
    aa localaa2 = l;
    int i1 = m;
    locala.<init>(paramc, localaa1, localad, localContact, bool1, localHistoryEvent, str, localCallRecording, bool2, localaa2, i1);
    paramObject = (ag)paramObject;
    n = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i1 = c;
    Object localObject2;
    switch (i1)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject1 = (CallRecording)b;
      localObject2 = (Map)a;
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label226;
      }
      paramObject = ad.a(e);
      localObject2 = paramObject;
      localObject2 = (Map)paramObject;
      paramObject = j;
      Object localObject3 = ad.b(e);
      f localf = ad.c(e);
      localObject3 = com.truecaller.data.entity.a.a((CallRecording)paramObject, (bx)localObject3, localf);
      a = localObject2;
      b = paramObject;
      int i2 = 1;
      c = i2;
      localObject3 = ((ao)localObject3).a(this);
      if (localObject3 == localObject1) {
        return localObject1;
      }
      localObject1 = paramObject;
      paramObject = localObject3;
    }
    paramObject = b.a(((Number)paramObject).longValue() / 1000L);
    ((Map)localObject2).put(localObject1, paramObject);
    ad.d(e).e();
    return x.a;
    label226:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ad.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */