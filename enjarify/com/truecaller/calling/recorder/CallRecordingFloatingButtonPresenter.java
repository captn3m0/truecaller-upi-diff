package com.truecaller.calling.recorder;

import c.g.a.m;
import c.g.b.k;
import com.truecaller.bb;
import java.util.Timer;
import java.util.TimerTask;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;
import org.a.a.b;
import org.a.a.d.n;
import org.a.a.d.o;

public final class CallRecordingFloatingButtonPresenter
  extends bb
  implements f
{
  public String a;
  public CallRecordingFloatingButtonPresenter.CallRecordingButtonMode c;
  int d;
  public b e;
  public Timer f;
  final n g;
  public String h;
  public final CallRecordingManager i;
  public final com.truecaller.utils.a j;
  final c.d.f k;
  final c.d.f l;
  public final ba m;
  final bx n;
  
  public CallRecordingFloatingButtonPresenter(CallRecordingManager paramCallRecordingManager, com.truecaller.utils.a parama, c.d.f paramf1, c.d.f paramf2, ba paramba, bx parambx)
  {
    i = paramCallRecordingManager;
    j = parama;
    k = paramf1;
    l = paramf2;
    m = paramba;
    n = parambx;
    paramCallRecordingManager = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.NOT_STARTED;
    c = paramCallRecordingManager;
    paramCallRecordingManager = new org/a/a/d/o;
    paramCallRecordingManager.<init>();
    int i1 = 4;
    b = i1;
    int i2 = 2;
    a = i2;
    paramCallRecordingManager.a(5);
    paramf2 = ":";
    paramCallRecordingManager = paramCallRecordingManager.a(paramf2, paramf2, true);
    b = i1;
    a = i2;
    paramCallRecordingManager.a(6);
    paramCallRecordingManager = paramCallRecordingManager.a();
    g = paramCallRecordingManager;
  }
  
  public final void a()
  {
    Object localObject1 = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.ENDED;
    c = ((CallRecordingFloatingButtonPresenter.CallRecordingButtonMode)localObject1);
    localObject1 = f;
    if (localObject1 != null) {
      ((Timer)localObject1).cancel();
    }
    f = null;
    a = null;
    ag localag = (ag)bg.a;
    c.d.f localf = k;
    Object localObject2 = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$d;
    ((CallRecordingFloatingButtonPresenter.d)localObject2).<init>(this, null);
    localObject2 = (m)localObject2;
    e.b(localag, localf, (m)localObject2, 2);
  }
  
  public final void a(bw parambw)
  {
    k.b(parambw, "presenterView");
    super.a(parambw);
    parambw = i;
    Object localObject = this;
    localObject = (f)this;
    parambw.a((f)localObject);
    parambw = i;
    localObject = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$b;
    ((CallRecordingFloatingButtonPresenter.b)localObject).<init>(this);
    localObject = (bi)localObject;
    parambw.a((bi)localObject);
  }
  
  final void b()
  {
    Object localObject1 = i.b();
    a = ((String)localObject1);
    localObject1 = c;
    Object localObject2 = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.RECORDING;
    if (localObject1 == localObject2) {
      return;
    }
    localObject1 = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.RECORDING;
    c = ((CallRecordingFloatingButtonPresenter.CallRecordingButtonMode)localObject1);
    c();
    localObject1 = i.c();
    e = ((b)localObject1);
    localObject1 = c.c.a.a("CallRecorderCountUpTimer");
    localObject2 = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$a;
    ((CallRecordingFloatingButtonPresenter.a)localObject2).<init>(this);
    Object localObject3 = localObject2;
    localObject3 = (TimerTask)localObject2;
    localObject2 = localObject1;
    ((Timer)localObject1).schedule((TimerTask)localObject3, 500L, 1000L);
    f = ((Timer)localObject1);
  }
  
  public final void c()
  {
    ag localag = (ag)bg.a;
    c.d.f localf = k;
    Object localObject = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$e;
    ((CallRecordingFloatingButtonPresenter.e)localObject).<init>(this, null);
    localObject = (m)localObject;
    e.b(localag, localf, (m)localObject, 2);
  }
  
  public final void y_()
  {
    super.y_();
    Timer localTimer = f;
    if (localTimer != null) {
      localTimer.cancel();
    }
    a = null;
    i.a(null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */