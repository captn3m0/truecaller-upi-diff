package com.truecaller.calling.recorder;

public enum CallRecordingSettingsMvp$CallsFilter
{
  static
  {
    CallsFilter[] arrayOfCallsFilter = new CallsFilter[3];
    CallsFilter localCallsFilter = new com/truecaller/calling/recorder/CallRecordingSettingsMvp$CallsFilter;
    localCallsFilter.<init>("ALL_CALLS", 0);
    ALL_CALLS = localCallsFilter;
    arrayOfCallsFilter[0] = localCallsFilter;
    localCallsFilter = new com/truecaller/calling/recorder/CallRecordingSettingsMvp$CallsFilter;
    int i = 1;
    localCallsFilter.<init>("UNKNOWN_NUMBERS", i);
    UNKNOWN_NUMBERS = localCallsFilter;
    arrayOfCallsFilter[i] = localCallsFilter;
    localCallsFilter = new com/truecaller/calling/recorder/CallRecordingSettingsMvp$CallsFilter;
    i = 2;
    localCallsFilter.<init>("SELECTED_CONTACTS", i);
    SELECTED_CONTACTS = localCallsFilter;
    arrayOfCallsFilter[i] = localCallsFilter;
    $VALUES = arrayOfCallsFilter;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsMvp.CallsFilter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */