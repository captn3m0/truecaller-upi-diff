package com.truecaller.calling.recorder;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.transition.p;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.premium.br;
import com.truecaller.ui.components.ComboBase;
import com.truecaller.ui.components.ComboBase.a;
import com.truecaller.ui.components.n;
import com.truecaller.util.dh;
import com.truecaller.utils.extensions.t;
import com.truecaller.wizard.utils.i;
import java.util.HashMap;
import java.util.List;

public final class CallRecordingSettingsFragment
  extends Fragment
  implements CallRecordingSettingsMvp.b
{
  public CallRecordingSettingsMvp.a a;
  public br b;
  public aj c;
  private HashMap d;
  
  private final m f()
  {
    CallRecordingSettingsFragment.j localj = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$j;
    localj.<init>(this);
    return (m)localj;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final CallRecordingSettingsMvp.a a()
  {
    CallRecordingSettingsMvp.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      Object localObject2 = c;
      if (localObject2 == null)
      {
        localObject2 = "callRecordingOnBoardingNavigator";
        k.a((String)localObject2);
      }
      k.a(localObject1, "it");
      localObject1 = (Context)localObject1;
      localObject2 = CallRecordingOnBoardingState.WHATS_NEW;
      aj.a((Context)localObject1, (CallRecordingOnBoardingState)localObject2, paramCallRecordingOnBoardingLaunchContext);
      return;
    }
  }
  
  public final void a(n paramn)
  {
    k.b(paramn, "mode");
    int i = R.id.settingsCallRecordingMode;
    ComboBase localComboBase = (ComboBase)a(i);
    k.a(localComboBase, "settingsCallRecordingMode");
    localComboBase.setSelection(paramn);
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "message");
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "context ?: return");
    Object localObject2 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
    paramCharSequence = ((AlertDialog.Builder)localObject2).setTitle(2131887611).setMessage(paramCharSequence);
    localObject2 = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$k;
    ((CallRecordingSettingsFragment.k)localObject2).<init>(this);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    paramCharSequence = paramCharSequence.setPositiveButton(2131887608, (DialogInterface.OnClickListener)localObject2);
    localObject2 = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$l;
    ((CallRecordingSettingsFragment.l)localObject2).<init>(this);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    paramCharSequence = paramCharSequence.setNegativeButton(2131887609, (DialogInterface.OnClickListener)localObject2);
    localObject1 = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$m;
    ((CallRecordingSettingsFragment.m)localObject1).<init>(this);
    localObject1 = (DialogInterface.OnCancelListener)localObject1;
    paramCharSequence.setOnCancelListener((DialogInterface.OnCancelListener)localObject1).show();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "path");
    int i = R.id.settingRecordingStoragePathDescription;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "settingRecordingStoragePathDescription");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(List paramList1, List paramList2, List paramList3)
  {
    k.b(paramList1, "modeItems");
    k.b(paramList2, "filterItems");
    k.b(paramList3, "configItems");
    int i = R.id.settingsCallRecordingMode;
    ((ComboBase)a(i)).setData(paramList1);
    int j = R.id.settingsCallRecordingCallsFilter;
    ((ComboBase)a(j)).setData(paramList2);
    j = R.id.settingsCallRecordingConfiguration;
    ((ComboBase)a(j)).setData(paramList3);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.settingRecordingEnabledSwitch;
    ((SwitchCompat)a(i)).setOnCheckedChangeListener(null);
    i = R.id.settingRecordingEnabledSwitch;
    Object localObject1 = (SwitchCompat)a(i);
    k.a(localObject1, "settingRecordingEnabledSwitch");
    ((SwitchCompat)localObject1).setChecked(paramBoolean);
    paramBoolean = R.id.settingRecordingEnabledSwitch;
    SwitchCompat localSwitchCompat = (SwitchCompat)a(paramBoolean);
    localObject1 = f();
    Object localObject2 = new com/truecaller/calling/recorder/bc;
    ((bc)localObject2).<init>((m)localObject1);
    localObject2 = (CompoundButton.OnCheckedChangeListener)localObject2;
    localSwitchCompat.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject2);
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    int i = R.id.settingsCallRecordingTroubleshootingContainer;
    Object localObject1 = a(i);
    if (localObject1 != null)
    {
      p.a((ViewGroup)localObject1);
      i = R.id.troubleshootingDrawOverOtherApps;
      localObject1 = (CardView)a(i);
      k.a(localObject1, "troubleshootingDrawOverOtherApps");
      t.a((View)localObject1, paramBoolean1);
      paramBoolean1 = R.id.troubleshootingBatteryOptimisation;
      localObject2 = (CardView)a(paramBoolean1);
      k.a(localObject2, "troubleshootingBatteryOptimisation");
      t.a((View)localObject2, paramBoolean2);
      paramBoolean1 = R.id.troubleshootingAllowMicrophone;
      localObject2 = (CardView)a(paramBoolean1);
      k.a(localObject2, "troubleshootingAllowMicrophone");
      t.a((View)localObject2, paramBoolean3);
      paramBoolean1 = R.id.troubleshootingAllowStorage;
      localObject2 = (CardView)a(paramBoolean1);
      k.a(localObject2, "troubleshootingAllowStorage");
      t.a((View)localObject2, paramBoolean4);
      return;
    }
    Object localObject2 = new c/u;
    ((u)localObject2).<init>("null cannot be cast to non-null type android.view.ViewGroup");
    throw ((Throwable)localObject2);
  }
  
  public final void b()
  {
    int i = R.id.settingsCallRecordingMode;
    Object localObject = (ComboBase)a(i);
    k.a(localObject, "settingsCallRecordingMode");
    ((ComboBase)localObject).setEnabled(false);
    i = R.id.settingRecordingEnabledSwitch;
    localObject = (SwitchCompat)a(i);
    k.a(localObject, "settingRecordingEnabledSwitch");
    ((SwitchCompat)localObject).setEnabled(false);
    i = R.id.settingsCallRecordingMode;
    localObject = (ComboBase)a(i);
    k.a(localObject, "settingsCallRecordingMode");
    ((ComboBase)localObject).setEnabled(false);
    i = R.id.settingsCallRecordingConfiguration;
    localObject = (ComboBase)a(i);
    k.a(localObject, "settingsCallRecordingConfiguration");
    ((ComboBase)localObject).setEnabled(false);
    i = R.id.settingsCallRecordingCallsFilter;
    localObject = (ComboBase)a(i);
    k.a(localObject, "settingsCallRecordingCallsFilter");
    ((ComboBase)localObject).setEnabled(false);
    i = R.id.settingCallRecordingEnaledSwitchHolder;
    localObject = (LinearLayout)a(i);
    k.a(localObject, "settingCallRecordingEnaledSwitchHolder");
    ((LinearLayout)localObject).setEnabled(false);
  }
  
  public final void b(n paramn)
  {
    k.b(paramn, "filter");
    int i = R.id.settingsCallRecordingCallsFilter;
    ComboBase localComboBase = (ComboBase)a(i);
    k.a(localComboBase, "settingsCallRecordingCallsFilter");
    localComboBase.setSelection(paramn);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "permission");
    i.a((Fragment)this, paramString, 102, false);
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.callRecordingPromoView;
    Object localObject1 = (CallRecordingPromoViewImpl)a(i);
    String str = "callRecordingPromoView";
    k.a(localObject1, str);
    localObject1 = (View)localObject1;
    t.a((View)localObject1, paramBoolean);
    if (paramBoolean)
    {
      paramBoolean = R.id.callRecordingPromoView;
      Object localObject2 = (CallRecordingPromoViewImpl)a(paramBoolean);
      localObject1 = a;
      if (localObject1 == null)
      {
        str = "presenter";
        k.a(str);
      }
      localObject2 = (av)localObject2;
      ((ar)localObject1).a((av)localObject2);
    }
  }
  
  public final void c()
  {
    dh.a(requireContext(), "https://support.truecaller.com/hc/en-us/articles/360001264545", false);
  }
  
  public final void c(n paramn)
  {
    k.b(paramn, "config");
    int i = R.id.settingsCallRecordingConfiguration;
    ComboBase localComboBase = (ComboBase)a(i);
    k.a(localComboBase, "settingsCallRecordingConfiguration");
    localComboBase.setSelection(paramn);
  }
  
  public final void c(boolean paramBoolean)
  {
    int i = R.id.settingsCallRecordingTroubleshootingContainer;
    View localView = a(i);
    k.a(localView, "settingsCallRecordingTroubleshootingContainer");
    t.a(localView, paramBoolean);
  }
  
  public final void d()
  {
    i.a((Activity)requireActivity());
  }
  
  public final void e()
  {
    i.b((Activity)requireActivity());
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = bs.a();
    Object localObject1 = requireContext();
    Object localObject2 = "requireContext()";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((Context)localObject1).getApplicationContext();
    if (localObject1 != null)
    {
      localObject1 = ((bk)localObject1).a();
      paramContext = paramContext.a((bp)localObject1);
      localObject1 = new com/truecaller/calling/recorder/bd;
      localObject2 = requireActivity();
      k.a(localObject2, "requireActivity()");
      localObject2 = ((f)localObject2).getIntent();
      ((bd)localObject1).<init>((Intent)localObject2);
      paramContext.a((bd)localObject1).a().a(this);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    return paramLayoutInflater.inflate(2131558786, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((CallRecordingSettingsMvp.a)localObject).y_();
    localObject = d;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    CallRecordingSettingsMvp.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void onResume()
  {
    super.onResume();
    CallRecordingSettingsMvp.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.settingsCallRecordingMode;
    paramView = (ComboBase)a(i);
    paramBundle = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$a;
    paramBundle.<init>(this);
    paramBundle = (ComboBase.a)paramBundle;
    paramView.a(paramBundle);
    i = R.id.settingsCallRecordingCallsFilter;
    paramView = (ComboBase)a(i);
    paramBundle = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$b;
    paramBundle.<init>(this);
    paramBundle = (ComboBase.a)paramBundle;
    paramView.a(paramBundle);
    i = R.id.settingsCallRecordingConfiguration;
    paramView = (ComboBase)a(i);
    paramBundle = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$c;
    paramBundle.<init>(this);
    paramBundle = (ComboBase.a)paramBundle;
    paramView.a(paramBundle);
    i = R.id.settingRecordingEnabledSwitch;
    paramView = (SwitchCompat)a(i);
    paramBundle = f();
    Object localObject = new com/truecaller/calling/recorder/bc;
    ((bc)localObject).<init>(paramBundle);
    localObject = (CompoundButton.OnCheckedChangeListener)localObject;
    paramView.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject);
    i = R.id.settingCallRecordingEnaledSwitchHolder;
    paramView = (LinearLayout)a(i);
    paramBundle = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.settingsCallRecordingMode;
    paramView = (ComboBase)a(i);
    int j = 2131559029;
    paramView.setListItemLayoutRes(j);
    i = R.id.settingsCallRecordingCallsFilter;
    ((ComboBase)a(i)).setListItemLayoutRes(j);
    i = R.id.settingsCallRecordingConfiguration;
    ((ComboBase)a(i)).setListItemLayoutRes(j);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    i = R.id.settingsCallRecordingCallsFilterContainer;
    paramView = (CardView)a(i);
    k.a(paramView, "settingsCallRecordingCallsFilterContainer");
    t.b((View)paramView);
    i = R.id.settingsCallRecordingStoragePathContainer;
    paramView = (CardView)a(i);
    k.a(paramView, "settingsCallRecordingStoragePathContainer");
    t.b((View)paramView);
    i = R.id.troubleshootingVisitHelpCenter;
    paramView = (CardView)a(i);
    paramBundle = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$e;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.troubleshootingDrawOverOtherApps;
    paramView = (CardView)a(i);
    paramBundle = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$f;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.troubleshootingBatteryOptimisation;
    paramView = (CardView)a(i);
    paramBundle = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$g;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.troubleshootingAllowStorage;
    paramView = (CardView)a(i);
    paramBundle = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$h;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.troubleshootingAllowMicrophone;
    paramView = (CardView)a(i);
    paramBundle = new com/truecaller/calling/recorder/CallRecordingSettingsFragment$i;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsFragment
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */