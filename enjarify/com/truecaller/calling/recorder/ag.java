package com.truecaller.calling.recorder;

import dagger.a.d;
import javax.inject.Provider;

public final class ag
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private ag(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static ag a(Provider paramProvider1, Provider paramProvider2)
  {
    ag localag = new com/truecaller/calling/recorder/ag;
    localag.<init>(paramProvider1, paramProvider2);
    return localag;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */