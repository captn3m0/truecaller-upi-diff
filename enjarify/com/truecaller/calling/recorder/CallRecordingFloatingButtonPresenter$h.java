package com.truecaller.calling.recorder;

import c.d.c;
import c.g.a.m;
import c.o.b;
import kotlinx.coroutines.ag;
import org.a.a.aa;
import org.a.a.ab;
import org.a.a.b;
import org.a.a.d.n;
import org.a.a.r;
import org.a.a.s;

final class CallRecordingFloatingButtonPresenter$h
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  CallRecordingFloatingButtonPresenter$h(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    h localh = new com/truecaller/calling/recorder/CallRecordingFloatingButtonPresenter$h;
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = b;
    localh.<init>(localCallRecordingFloatingButtonPresenter, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localh;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.e;
        if (paramObject != null)
        {
          localObject1 = (bw)b.b;
          if (localObject1 != null)
          {
            Object localObject2 = b.g;
            paramObject = (org.a.a.x)paramObject;
            Object localObject3 = new org/a/a/b;
            com.truecaller.utils.a locala = b.j;
            long l = locala.a();
            ((b)localObject3).<init>(l);
            localObject3 = (org.a.a.x)localObject3;
            paramObject = ab.a((org.a.a.x)paramObject, (org.a.a.x)localObject3);
            c.g.b.k.a(paramObject, "Seconds.secondsBetween(i…ock.currentTimeMillis()))");
            int j = ((ab)paramObject).c();
            paramObject = r.a(j);
            localObject3 = s.a();
            paramObject = (aa)((r)paramObject).a((s)localObject3);
            paramObject = ((n)localObject2).a((aa)paramObject);
            localObject2 = "periodFormatter.print(\n …ndard()\n                )";
            c.g.b.k.a(paramObject, (String)localObject2);
            ((bw)localObject1).setLabel((String)paramObject);
          }
        }
        return c.x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (h)a(paramObject1, (c)paramObject2);
    paramObject2 = c.x.a;
    return ((h)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */