package com.truecaller.calling.recorder;

import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bb;
import com.truecaller.common.f.c;
import com.truecaller.utils.l;
import com.truecaller.utils.n;

public final class al
  extends bb
{
  CallRecordingOnBoardingState a;
  CallRecordingOnBoardingLaunchContext c;
  final String[] d;
  final com.truecaller.common.g.a e;
  final CallRecordingManager f;
  private final n g;
  private final com.truecaller.utils.a h;
  private final c i;
  private final b j;
  private final l k;
  
  public al(com.truecaller.common.g.a parama, n paramn, com.truecaller.utils.a parama1, c paramc, b paramb, l paraml, CallRecordingManager paramCallRecordingManager)
  {
    e = parama;
    g = paramn;
    h = parama1;
    i = paramc;
    j = paramb;
    k = paraml;
    f = paramCallRecordingManager;
    parama = CallRecordingOnBoardingState.WHATS_NEW;
    a = parama;
    parama = CallRecordingOnBoardingLaunchContext.UNKNOWN;
    c = parama;
    String[] tmp111_108 = new String[5];
    String[] tmp112_111 = tmp111_108;
    String[] tmp112_111 = tmp111_108;
    tmp112_111[0] = "android.permission.RECORD_AUDIO";
    tmp112_111[1] = "android.permission.CALL_PHONE";
    String[] tmp121_112 = tmp112_111;
    String[] tmp121_112 = tmp112_111;
    tmp121_112[2] = "android.permission.READ_EXTERNAL_STORAGE";
    tmp121_112[3] = "android.permission.WRITE_EXTERNAL_STORAGE";
    tmp121_112[4] = "android.permission.READ_PHONE_STATE";
    parama = tmp121_112;
    d = parama;
  }
  
  final void a()
  {
    Object localObject1 = a;
    Object localObject2 = am.a;
    int m = ((CallRecordingOnBoardingState)localObject1).ordinal();
    m = localObject2[m];
    localObject2 = null;
    Object localObject3;
    int n;
    switch (m)
    {
    default: 
      break;
    case 7: 
      localObject1 = (an)b;
      if (localObject1 != null)
      {
        localObject2 = c;
        ((an)localObject1).e((CallRecordingOnBoardingLaunchContext)localObject2);
      }
      e();
      break;
    case 6: 
      localObject1 = (an)b;
      if (localObject1 != null)
      {
        localObject3 = g;
        n = 2131887582;
        localObject2 = new Object[0];
        localObject2 = ((n)localObject3).b(n, (Object[])localObject2);
        localObject3 = "resourceProvider.getRich…ing_permissions_subtitle)";
        k.a(localObject2, (String)localObject3);
        ((an)localObject1).b((CharSequence)localObject2);
      }
      return;
    case 5: 
      localObject1 = (an)b;
      if (localObject1 != null)
      {
        localObject3 = g;
        n = 2131887610;
        localObject2 = new Object[0];
        localObject2 = ((n)localObject3).b(n, (Object[])localObject2);
        localObject3 = "resourceProvider.getRich…recording_terms_subtitle)";
        k.a(localObject2, (String)localObject3);
        ((an)localObject1).a((CharSequence)localObject2);
      }
      return;
    case 4: 
      localObject1 = (an)b;
      if (localObject1 != null)
      {
        localObject3 = g;
        Object localObject4 = i;
        boolean bool2 = ((c)localObject4).d();
        int i1;
        if (bool2) {
          i1 = 2131887625;
        } else {
          i1 = 2131887621;
        }
        Object localObject5 = new Object[0];
        localObject3 = ((n)localObject3).b(i1, (Object[])localObject5);
        k.a(localObject3, "resourceProvider.getRich…      }\n                )");
        localObject4 = g;
        localObject5 = i;
        boolean bool3 = ((c)localObject5).d();
        int i2;
        if (bool3) {
          i2 = 2131887624;
        } else {
          i2 = 2131887620;
        }
        localObject2 = new Object[0];
        localObject2 = ((n)localObject4).a(i2, (Object[])localObject2);
        localObject4 = "resourceProvider.getStri…      }\n                )";
        k.a(localObject2, (String)localObject4);
        localObject2 = (CharSequence)localObject2;
        ((an)localObject1).a((CharSequence)localObject3, (CharSequence)localObject2);
      }
      return;
    case 3: 
      localObject1 = i;
      boolean bool1 = ((c)localObject1).d();
      if (bool1)
      {
        localObject1 = (an)b;
        if (localObject1 != null)
        {
          localObject2 = c;
          ((an)localObject1).d((CallRecordingOnBoardingLaunchContext)localObject2);
        }
        return;
      }
      localObject1 = (an)b;
      if (localObject1 != null)
      {
        localObject2 = c;
        ((an)localObject1).a((CallRecordingOnBoardingLaunchContext)localObject2);
      }
      return;
    case 2: 
      localObject1 = (an)b;
      if (localObject1 != null)
      {
        localObject2 = c;
        ((an)localObject1).c((CallRecordingOnBoardingLaunchContext)localObject2);
      }
      return;
    case 1: 
      localObject1 = (an)b;
      if (localObject1 != null)
      {
        localObject2 = c;
        ((an)localObject1).b((CallRecordingOnBoardingLaunchContext)localObject2);
      }
      return;
    }
  }
  
  final void b()
  {
    Object localObject = e;
    String str = "callRecordingTermsAccepted";
    boolean bool = ((com.truecaller.common.g.a)localObject).b(str);
    if (!bool)
    {
      localObject = CallRecordingOnBoardingState.TERMS;
    }
    else
    {
      bool = c();
      if (!bool)
      {
        localObject = CallRecordingOnBoardingState.PERMISSIONS;
      }
      else
      {
        localObject = e;
        str = "callRecordingPostEnableShown";
        ((com.truecaller.common.g.a)localObject).b(str);
        localObject = CallRecordingOnBoardingState.POST_ENABLE;
      }
    }
    a = ((CallRecordingOnBoardingState)localObject);
    a();
  }
  
  final boolean c()
  {
    String[] arrayOfString1 = d;
    int m = arrayOfString1.length;
    int n = 0;
    boolean bool;
    for (;;)
    {
      bool = true;
      if (n >= m) {
        break;
      }
      String str = arrayOfString1[n];
      l locall = k;
      String[] arrayOfString2 = new String[bool];
      arrayOfString2[0] = str;
      bool = locall.a(arrayOfString2);
      if (!bool) {
        return false;
      }
      n += 1;
    }
    return bool;
  }
  
  final void e()
  {
    Object localObject1 = e;
    boolean bool = true;
    ((com.truecaller.common.g.a)localObject1).b("callRecordingPostEnableShown", bool);
    localObject1 = e;
    com.truecaller.utils.a locala = h;
    long l = locala.a();
    ((com.truecaller.common.g.a)localObject1).b("key_call_recording_trial_start_timestamp", l);
    e.b("callRecordingEnbaled", bool);
    localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("CallRecordingEnabled");
    Object localObject2 = c.name();
    ((e.a)localObject1).a("Source", (String)localObject2);
    Object localObject3 = "PremiumStatus";
    localObject2 = i;
    bool = ((c)localObject2).d();
    if (bool) {
      localObject2 = "Premium";
    } else {
      localObject2 = "Free";
    }
    ((e.a)localObject1).a((String)localObject3, (String)localObject2);
    localObject3 = j;
    localObject1 = ((e.a)localObject1).a();
    k.a(localObject1, "event.build()");
    ((b)localObject3).b((e)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */