package com.truecaller.calling.recorder;

import android.net.Uri;
import android.net.Uri.Builder;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.callerid.e;
import com.truecaller.callrecording.CallRecorder;
import com.truecaller.callrecording.CallRecorder.a;
import com.truecaller.log.AssertionUtil;
import java.util.Timer;
import java.util.TimerTask;
import kotlinx.coroutines.ag;

final class ae$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  ae$a(ae paramae, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/calling/recorder/ae$a;
    ae localae = b;
    String str = c;
    locala.<init>(localae, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b;
        localObject1 = c;
        try
        {
          Object localObject2 = h;
          Object localObject3 = paramObject;
          localObject3 = (CallRecorder.a)paramObject;
          localObject2 = ((ax)localObject2).a((CallRecorder.a)localObject3);
          a = ((CallRecorder)localObject2);
          localObject2 = a;
          if (localObject2 != null)
          {
            localObject3 = paramObject;
            localObject3 = (CallRecorder.a)paramObject;
            ((CallRecorder)localObject2).setErrorListener((CallRecorder.a)localObject3);
          }
          localObject2 = ((ae)paramObject).k();
          localObject2 = Uri.parse((String)localObject2);
          localObject2 = ((Uri)localObject2).buildUpon();
          localObject3 = new java/lang/StringBuilder;
          Object localObject4 = "TC-";
          ((StringBuilder)localObject3).<init>((String)localObject4);
          localObject4 = e;
          Object localObject5 = org.a.a.f.a;
          localObject4 = ((org.a.a.d.b)localObject4).a((org.a.a.f)localObject5);
          localObject5 = i;
          long l1 = ((com.truecaller.utils.a)localObject5).a();
          localObject4 = ((org.a.a.d.b)localObject4).a(l1);
          ((StringBuilder)localObject3).append((String)localObject4);
          char c1 = '-';
          ((StringBuilder)localObject3).append(c1);
          if (localObject1 == null) {
            localObject1 = "unknown";
          }
          ((StringBuilder)localObject3).append((String)localObject1);
          localObject1 = ".m4a";
          ((StringBuilder)localObject3).append((String)localObject1);
          localObject1 = ((StringBuilder)localObject3).toString();
          localObject1 = ((Uri.Builder)localObject2).appendPath((String)localObject1);
          localObject1 = ((Uri.Builder)localObject1).toString();
          b = ((String)localObject1);
          localObject1 = a;
          if (localObject1 != null)
          {
            localObject2 = b;
            ((CallRecorder)localObject1).setOutputFile((String)localObject2);
          }
          localObject1 = a;
          if (localObject1 != null) {
            ((CallRecorder)localObject1).prepare();
          }
          localObject1 = a;
          if (localObject1 != null) {
            ((CallRecorder)localObject1).start();
          }
          localObject1 = new org/a/a/b;
          ((org.a.a.b)localObject1).<init>();
          c = ((org.a.a.b)localObject1);
          localObject1 = d;
          if (localObject1 != null) {
            ((bi)localObject1).a();
          }
          localObject1 = a;
          if (localObject1 != null)
          {
            localObject1 = g;
            localObject1 = ((com.truecaller.androidactors.f)localObject1).a();
            localObject1 = (e)localObject1;
            ((e)localObject1).c();
          }
          localObject1 = "SafeRecordingCloser";
          long l2 = k;
          long l3 = k;
          localObject1 = c.c.a.a((String)localObject1);
          localObject2 = new com/truecaller/calling/recorder/ae$b;
          ((ae.b)localObject2).<init>((ae)paramObject);
          localObject3 = localObject2;
          localObject3 = (TimerTask)localObject2;
          localObject2 = localObject1;
          ((Timer)localObject1).schedule((TimerTask)localObject3, l2, l3);
          f = ((Timer)localObject1);
        }
        catch (Exception localException)
        {
          ((ae)paramObject).onError(localException);
          Throwable localThrowable = (Throwable)localException;
          AssertionUtil.reportThrowableButNeverCrash(localThrowable);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ae.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */