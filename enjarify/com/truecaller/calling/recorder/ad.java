package com.truecaller.calling.recorder;

import android.content.Intent;
import c.d.f;
import c.g.b.w;
import c.n.m;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.analytics.e.a;
import com.truecaller.callhistory.z;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.dialer.a;
import com.truecaller.calling.dialer.ax;
import com.truecaller.calling.dialer.cb;
import com.truecaller.common.h.aj;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.network.search.e;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.af;
import com.truecaller.utils.n;
import java.util.HashMap;

public final class ad
  extends c
  implements ac, ca
{
  private final bk c;
  private final cb d;
  private final HashMap e;
  private final HashMap f;
  private final bk g;
  private final aj h;
  private final n i;
  private final af j;
  private final ax k;
  private final com.truecaller.calling.dialer.t l;
  private final u m;
  private final x n;
  private final cd o;
  private final bt p;
  private final a q;
  private final com.truecaller.analytics.b r;
  private final com.truecaller.androidactors.k s;
  private final e t;
  private final f u;
  private final g v;
  private final bx w;
  private final CallRecordingManager x;
  private final f y;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(ad.class);
    ((c.g.b.u)localObject).<init>(localb, "callRecordingsCursor", "getCallRecordingsCursor()Lcom/truecaller/callhistory/HistoryEventCursor;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public ad(bk parambk, aj paramaj, n paramn, af paramaf, ax paramax, com.truecaller.calling.dialer.t paramt, u paramu, x paramx, cd paramcd, bt parambt, a parama, com.truecaller.analytics.b paramb, com.truecaller.androidactors.k paramk, e parame, f paramf1, g paramg, bx parambx, CallRecordingManager paramCallRecordingManager, f paramf2)
  {
    g = parambk;
    h = paramaj;
    i = paramn;
    j = paramaf;
    k = paramax;
    l = paramt;
    m = paramu;
    n = paramx;
    o = paramcd;
    p = parambt;
    q = parama;
    r = paramb;
    s = paramk;
    t = parame;
    localObject1 = paramf1;
    u = paramf1;
    v = paramg;
    localObject1 = parambx;
    localObject2 = paramCallRecordingManager;
    w = parambx;
    x = paramCallRecordingManager;
    localObject1 = paramf2;
    y = paramf2;
    localObject1 = g;
    c = ((bk)localObject1);
    localObject2 = this;
    localObject2 = (ac)this;
    localObject1 = ((bk)localObject1).a((ac)localObject2);
    d = ((cb)localObject1);
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    e = ((HashMap)localObject1);
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    f = ((HashMap)localObject1);
  }
  
  private final z a()
  {
    bk localbk = c;
    c.l.g localg = b[0];
    return localbk.a(this, localg);
  }
  
  private final void a(String paramString1, String paramString2)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("ViewAction");
    paramString1 = locala.a("Context", "notificationCallRecording").a("Action", paramString1).a("SubAction", paramString2);
    paramString2 = r;
    paramString1 = paramString1.a();
    c.g.b.k.a(paramString1, "eventBuilder.build()");
    paramString2.a(paramString1);
  }
  
  private final boolean a(int paramInt, String paramString)
  {
    HistoryEvent localHistoryEvent = d(paramInt);
    if (localHistoryEvent != null)
    {
      com.truecaller.calling.dialer.t localt = l;
      DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.CallRecording;
      localt.a(localHistoryEvent, localSourceType, false, false);
      a("details", paramString);
      return true;
    }
    return false;
  }
  
  private final HistoryEvent d(int paramInt)
  {
    z localz1 = a();
    if (localz1 != null) {
      localz1.moveToPosition(paramInt);
    }
    z localz2 = a();
    if (localz2 != null) {
      return localz2.d();
    }
    return null;
  }
  
  private final c.x e(int paramInt)
  {
    Object localObject = d(paramInt);
    if (localObject != null)
    {
      localObject = ((HistoryEvent)localObject).t();
      if (localObject != null)
      {
        bk localbk = g;
        c.g.b.k.a(localObject, "it");
        localbk.b((CallRecording)localObject);
        return c.x.a;
      }
    }
    return null;
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = x;
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext = CallRecordingOnBoardingLaunchContext.LIST;
    boolean bool = ((CallRecordingManager)localObject1).a(localCallRecordingOnBoardingLaunchContext);
    if (bool) {
      return;
    }
    Object localObject2 = d(paramInt);
    if (localObject2 != null)
    {
      localObject2 = ((HistoryEvent)localObject2).t();
      if (localObject2 != null)
      {
        localObject2 = c;
        if (localObject2 != null)
        {
          localObject1 = localObject2;
          localObject1 = (CharSequence)localObject2;
          bool = m.a((CharSequence)localObject1) ^ true;
          if (!bool)
          {
            paramInt = 0;
            localObject2 = null;
          }
          if (localObject2 != null)
          {
            localObject1 = m;
            localObject2 = ((u)localObject1).b((String)localObject2);
            paramInt = ((u)localObject1).a((Intent)localObject2);
            if (paramInt == 0)
            {
              localObject2 = o;
              localObject1 = i;
              Object[] arrayOfObject = new Object[0];
              localObject1 = ((n)localObject1).a(2131887613, arrayOfObject);
              c.g.b.k.a(localObject1, "resourceProvider.getStri…_no_activity_found_share)");
              ((cd)localObject2).toast((String)localObject1);
              return;
            }
            localObject2 = v;
            localObject1 = new com/truecaller/analytics/e$a;
            ((e.a)localObject1).<init>("CallRecordingShared");
            ((g)localObject2).a((e.a)localObject1);
            return;
          }
        }
      }
    }
  }
  
  public final boolean a(h paramh)
  {
    Object localObject1 = "event";
    c.g.b.k.b(paramh, (String)localObject1);
    int i1 = b;
    Object localObject2 = a;
    Object localObject3 = "ItemEvent.LONG_CLICKED";
    boolean bool1 = c.g.b.k.a(localObject2, localObject3);
    boolean bool2 = true;
    boolean bool3;
    if (bool1)
    {
      bool3 = a;
      if (!bool3)
      {
        q.b();
        a = bool2;
        e(i1);
        return bool2;
      }
      return false;
    }
    localObject3 = "ItemEvent.CLICKED";
    bool1 = c.g.b.k.a(localObject2, localObject3);
    if (bool1)
    {
      bool3 = a;
      if (bool3) {
        e(i1);
      }
      return bool2;
    }
    localObject3 = ActionType.PROFILE.getEventAction();
    bool1 = c.g.b.k.a(localObject2, localObject3);
    if (bool1) {
      return a(i1, "avatar");
    }
    localObject3 = CallRecordingActionType.PLAY_CALL_RECORDING.getEventAction();
    bool1 = c.g.b.k.a(localObject2, localObject3);
    if (bool1)
    {
      paramh = x;
      localObject2 = CallRecordingOnBoardingLaunchContext.LIST;
      bool3 = paramh.a((CallRecordingOnBoardingLaunchContext)localObject2);
      if (!bool3)
      {
        paramh = d(i1);
        if (paramh != null)
        {
          paramh = paramh.t();
          if (paramh != null)
          {
            localObject1 = m;
            paramh = c;
            paramh = ((u)localObject1).a(paramh);
            bool3 = ((u)localObject1).a(paramh);
            int i2;
            if (bool3)
            {
              paramh = o;
              localObject1 = i;
              i2 = 2131887616;
              localObject3 = new Object[0];
              localObject1 = ((n)localObject1).a(i2, (Object[])localObject3);
              c.g.b.k.a(localObject1, "resourceProvider.getStri…ecording_toast_item_play)");
              paramh.toast((String)localObject1);
              paramh = v;
              localObject1 = new com/truecaller/analytics/e$a;
              ((e.a)localObject1).<init>("CallRecordingPlayback");
              localObject3 = CallRecordingManager.PlaybackLaunchContext.RECORDINGS.name();
              localObject1 = ((e.a)localObject1).a("Source", (String)localObject3);
              localObject2 = "AnalyticsEvent.Builder(A…                        )";
              c.g.b.k.a(localObject1, (String)localObject2);
              paramh.a((e.a)localObject1);
            }
            else
            {
              paramh = o;
              localObject1 = i;
              i2 = 2131887612;
              localObject3 = new Object[0];
              localObject1 = ((n)localObject1).a(i2, (Object[])localObject3);
              localObject2 = "resourceProvider.getStri…r_no_activity_found_play)";
              c.g.b.k.a(localObject1, (String)localObject2);
              paramh.toast((String)localObject1);
            }
          }
        }
      }
      return bool2;
    }
    localObject3 = CallRecordingActionType.SHOW_CALL_RECORDING_MENU_OPTIONS.getEventAction();
    boolean bool4 = c.g.b.k.a(localObject2, localObject3);
    if (bool4)
    {
      paramh = d;
      localObject2 = n;
      localObject3 = this;
      localObject3 = (ca)this;
      ((x)localObject2).a(i1, paramh, (ca)localObject3);
      return bool2;
    }
    return false;
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = d(paramInt);
    if (localObject1 != null)
    {
      localObject1 = ((HistoryEvent)localObject1).t();
      if (localObject1 != null)
      {
        bt localbt = p;
        c.g.b.k.a(localObject1, "callRecording");
        Object localObject2 = new com/truecaller/calling/recorder/ad$b;
        ((ad.b)localObject2).<init>(this);
        localObject2 = (by)localObject2;
        localbt.a(localObject1, (by)localObject2);
        return;
      }
    }
  }
  
  public final void c(int paramInt)
  {
    a(paramInt, "callRecording");
  }
  
  public final int getItemCount()
  {
    z localz = a();
    if (localz != null) {
      return localz.getCount();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    z localz = a();
    long l1 = -1;
    if (localz == null) {
      return l1;
    }
    localz = a();
    if (localz != null) {
      localz.moveToPosition(paramInt);
    }
    Object localObject = a();
    if (localObject != null)
    {
      localObject = ((z)localObject).d();
      if (localObject != null)
      {
        localObject = ((HistoryEvent)localObject).t();
        if (localObject != null) {
          return a;
        }
      }
    }
    return l1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */