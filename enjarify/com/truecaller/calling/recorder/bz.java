package com.truecaller.calling.recorder;

import android.view.View;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.a;
import com.truecaller.ui.view.TintedImageView;

public final class bz
  implements a
{
  private final f b;
  private final f c;
  
  static
  {
    g[] arrayOfg = new g[2];
    Object localObject = new c/g/b/u;
    b localb = w.a(bz.class);
    ((u)localObject).<init>(localb, "actionOneView", "getActionOneView()Lcom/truecaller/ui/view/TintedImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bz.class);
    ((u)localObject).<init>(localb, "actionOneClickArea", "getActionOneClickArea()Landroid/view/View;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public bz(View paramView)
  {
    f localf = com.truecaller.utils.extensions.t.a(paramView, 2131361918);
    b = localf;
    paramView = com.truecaller.utils.extensions.t.a(paramView, 2131361842);
    c = paramView;
    a().setTint(null);
    a().setImageResource(2131234388);
  }
  
  final TintedImageView a()
  {
    return (TintedImageView)b.b();
  }
  
  public final void a(ActionType paramActionType) {}
  
  final View b()
  {
    return (View)c.b();
  }
  
  public final void b(boolean paramBoolean)
  {
    Object localObject = b();
    k.a(localObject, "actionOneClickArea");
    com.truecaller.utils.extensions.t.a((View)localObject, paramBoolean);
    localObject = a();
    k.a(localObject, "actionOneView");
    com.truecaller.utils.extensions.t.a((View)localObject, paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bz
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */