package com.truecaller.calling;

import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;
import c.a.ab;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.flashsdk.core.i;
import com.truecaller.search.local.b.a;
import com.truecaller.search.local.b.e;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class ae
{
  private static final int a(String paramString1, String paramString2)
  {
    return c.n.m.a((CharSequence)paramString2, paramString1, 0, false, 6);
  }
  
  private static final n a(e parame, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i = a(paramString2, paramString3);
    paramString3 = Integer.valueOf(i);
    Object localObject = paramString3;
    localObject = (Number)paramString3;
    int j = ((Number)localObject).intValue();
    if (j >= 0)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject = null;
    }
    if (j == 0)
    {
      i = 0;
      paramString3 = null;
    }
    if (paramString3 != null)
    {
      localObject = paramString3;
      localObject = (Number)paramString3;
      ((Number)localObject).intValue();
      boolean bool = parame.a(paramString1, paramString2, paramBoolean1, paramBoolean2);
      if (!bool)
      {
        i = 0;
        paramString3 = null;
      }
      if (paramString3 != null)
      {
        int k = ((Number)paramString3).intValue();
        paramString2 = Integer.valueOf(a + k);
        parame = Integer.valueOf(b + k);
        return t.a(paramString2, parame);
      }
    }
    return null;
  }
  
  static final String a(x paramx)
  {
    CharSequence localCharSequence = (CharSequence)b;
    int i = localCharSequence.length();
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localCharSequence = null;
    }
    if (i != 0)
    {
      paramx = (Long)c.a.m.e(a);
      if (paramx != null) {
        return String.valueOf(paramx.longValue());
      }
      return null;
    }
    return b;
  }
  
  public static final void a(TextView paramTextView, int paramInt1, int paramInt2)
  {
    k.b(paramTextView, "field");
    Object localObject1 = new android/text/SpannableStringBuilder;
    Object localObject2 = paramTextView.getText();
    ((SpannableStringBuilder)localObject1).<init>((CharSequence)localObject2);
    localObject2 = new android/text/style/ForegroundColorSpan;
    int i = com.truecaller.utils.ui.b.a(paramTextView.getContext(), 2130968888);
    ((ForegroundColorSpan)localObject2).<init>(i);
    ((SpannableStringBuilder)localObject1).setSpan(localObject2, paramInt1, paramInt2, 33);
    localObject1 = (CharSequence)localObject1;
    paramTextView.setText((CharSequence)localObject1);
  }
  
  public static final boolean a(bb parambb, e parame, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(parambb, "receiver$0");
    k.b(parame, "searchMatcher");
    k.b(paramString1, "pattern");
    k.b(paramString2, "originalValue");
    k.b(paramString3, "formattedValue");
    ae.a locala = new com/truecaller/calling/ae$a;
    locala.<init>(parambb);
    Object localObject = locala;
    localObject = (c.g.a.b)locala;
    return a(parame, paramString1, paramString2, paramString3, paramBoolean1, paramBoolean2, paramBoolean3, (c.g.a.b)localObject);
  }
  
  public static final boolean a(u paramu, e parame, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramu, "receiver$0");
    k.b(parame, "searchMatcher");
    k.b(paramString1, "pattern");
    k.b(paramString2, "originalValue");
    k.b(paramString3, "formattedValue");
    ae.b localb = new com/truecaller/calling/ae$b;
    localb.<init>(paramu);
    Object localObject = localb;
    localObject = (c.g.a.b)localb;
    return a(parame, paramString1, paramString2, paramString3, paramBoolean1, paramBoolean2, false, (c.g.a.b)localObject);
  }
  
  public static final boolean a(i parami)
  {
    k.b(parami, "receiver$0");
    return parami.b(null);
  }
  
  private static final boolean a(e parame, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, c.g.a.b paramb)
  {
    c.g.a.b localb = paramb;
    int i = 6;
    int j = 32;
    int k = 1;
    Object localObject1;
    Object localObject2;
    boolean bool2;
    if ((!paramBoolean1) && (!paramBoolean2))
    {
      if (paramBoolean3)
      {
        localObject1 = paramString1;
        localObject1 = (CharSequence)paramString1;
        localObject2 = new char[k];
        localObject2[0] = j;
        localObject3 = c.n.m.a((CharSequence)localObject1, (char[])localObject2, 0, i);
      }
      else
      {
        localObject3 = c.a.m.a(paramString1);
      }
      localObject3 = ((Iterable)localObject3).iterator();
      j = 0;
      for (;;)
      {
        bool2 = ((Iterator)localObject3).hasNext();
        if (!bool2) {
          break;
        }
        localObject1 = (String)((Iterator)localObject3).next();
        localObject1 = b(paramString3, (String)localObject1);
        if (localObject1 != null)
        {
          localObject1 = (Boolean)localb.invoke(localObject1);
        }
        else
        {
          bool2 = false;
          localObject1 = null;
        }
        bool2 = com.truecaller.utils.extensions.c.a((Boolean)localObject1);
        if ((!bool2) && (j == 0)) {
          j = 0;
        } else {
          j = 1;
        }
      }
      return j;
    }
    if (paramBoolean3)
    {
      localObject1 = paramString1;
      localObject1 = (CharSequence)paramString1;
      localObject2 = new char[k];
      localObject2[0] = j;
      localObject3 = c.n.m.a((CharSequence)localObject1, (char[])localObject2, 0, i);
    }
    else
    {
      localObject3 = c.a.m.a(paramString1);
    }
    Object localObject3 = ((Iterable)localObject3).iterator();
    boolean bool1 = false;
    for (;;)
    {
      bool2 = ((Iterator)localObject3).hasNext();
      if (!bool2) {
        break;
      }
      localObject1 = ((Iterator)localObject3).next();
      localObject2 = localObject1;
      localObject2 = (String)localObject1;
      localObject1 = parame;
      localObject1 = a(parame, (String)localObject2, paramString2, paramString3, paramBoolean1, paramBoolean2);
      if (localObject1 != null)
      {
        localObject1 = (Boolean)localb.invoke(localObject1);
      }
      else
      {
        bool2 = false;
        localObject1 = null;
      }
      bool2 = com.truecaller.utils.extensions.c.a((Boolean)localObject1);
      if ((!bool2) && (!bool1)) {
        bool1 = false;
      } else {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  private static n b(String paramString1, String paramString2)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "what");
    paramString2 = (CharSequence)paramString2;
    Object localObject1 = new java/util/ArrayList;
    int i = paramString2.length();
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    i = 0;
    int j = 0;
    Object localObject2 = null;
    Character localCharacter1;
    for (;;)
    {
      int m = paramString2.length();
      if (j >= m) {
        break;
      }
      m = a.a(paramString2.charAt(j));
      localCharacter1 = Character.valueOf(m);
      ((Collection)localObject1).add(localCharacter1);
      j += 1;
    }
    localObject1 = (Iterable)localObject1;
    paramString2 = new java/util/ArrayList;
    paramString2.<init>();
    paramString2 = (Collection)paramString2;
    localObject1 = ((Iterable)localObject1).iterator();
    int n;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      n = 1;
      if (!bool1) {
        break;
      }
      localObject2 = ((Iterator)localObject1).next();
      localObject3 = localObject2;
      localObject3 = (Character)localObject2;
      char c2 = ((Character)localObject3).charValue();
      boolean bool6 = com.truecaller.search.local.b.c.c(c2);
      if (!bool6)
      {
        boolean bool4 = com.truecaller.search.local.b.c.b(c2);
        if (!bool4) {}
      }
      else
      {
        n = 0;
        localCharacter1 = null;
      }
      if (n != 0) {
        paramString2.add(localObject2);
      }
    }
    paramString2 = (Iterable)paramString2;
    localObject1 = new java/util/ArrayList;
    int k = 10;
    int i1 = c.a.m.a(paramString2, k);
    ((ArrayList)localObject1).<init>(i1);
    localObject1 = (Collection)localObject1;
    paramString2 = paramString2.iterator();
    for (;;)
    {
      boolean bool5 = paramString2.hasNext();
      if (!bool5) {
        break;
      }
      c3 = Character.toLowerCase(((Character)paramString2.next()).charValue());
      localObject3 = Character.valueOf(c3);
      ((Collection)localObject1).add(localObject3);
    }
    localObject1 = (List)localObject1;
    boolean bool8 = ((List)localObject1).isEmpty();
    char c3 = '\000';
    Object localObject3 = null;
    if (bool8) {
      return null;
    }
    paramString1 = (CharSequence)paramString1;
    paramString2 = new java/util/ArrayList;
    int i2 = paramString1.length();
    paramString2.<init>(i2);
    paramString2 = (Collection)paramString2;
    i2 = 0;
    Object localObject4 = null;
    Character localCharacter2;
    for (;;)
    {
      int i4 = paramString1.length();
      if (i2 >= i4) {
        break;
      }
      char c4 = a.a(paramString1.charAt(i2));
      localCharacter2 = Character.valueOf(c4);
      paramString2.add(localCharacter2);
      i2 += 1;
    }
    paramString2 = (Iterable)paramString2;
    paramString1 = new java/util/ArrayList;
    k = c.a.m.a(paramString2, k);
    paramString1.<init>(k);
    paramString1 = (Collection)paramString1;
    paramString2 = paramString2.iterator();
    for (;;)
    {
      boolean bool2 = paramString2.hasNext();
      if (!bool2) {
        break;
      }
      char c1 = Character.toLowerCase(((Character)paramString2.next()).charValue());
      localObject2 = Character.valueOf(c1);
      paramString1.add(localObject2);
    }
    paramString1 = c.a.m.j((Iterable)paramString1);
    paramString2 = new java/util/ArrayList;
    paramString2.<init>();
    paramString2 = (Collection)paramString2;
    paramString1 = paramString1.iterator();
    boolean bool3;
    boolean bool7;
    for (;;)
    {
      bool3 = paramString1.hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = paramString1.next();
      localObject4 = localObject2;
      localObject4 = (Character)b;
      i2 = ((Character)localObject4).charValue();
      boolean bool9 = com.truecaller.search.local.b.c.c(i2);
      if (!bool9)
      {
        bool7 = com.truecaller.search.local.b.c.b(i2);
        if (!bool7)
        {
          bool7 = true;
          break label622;
        }
      }
      bool7 = false;
      localObject4 = null;
      label622:
      if (bool7) {
        paramString2.add(localObject2);
      }
    }
    paramString2 = (Iterable)paramString2;
    int i6 = ((List)localObject1).size();
    paramString1 = c.a.m.f(paramString2, i6);
    bool8 = paramString1.isEmpty();
    if (bool8) {
      return null;
    }
    paramString1 = ((Iterable)paramString1).iterator();
    do
    {
      bool8 = paramString1.hasNext();
      if (!bool8) {
        break;
      }
      paramString2 = paramString1.next();
      localObject2 = paramString2;
      localObject2 = (List)paramString2;
      localObject4 = localObject1;
      localObject4 = (Iterable)localObject1;
      localObject2 = (Iterable)localObject2;
      localObject2 = (Iterable)c.a.m.d((Iterable)localObject4, (Iterable)localObject2);
      localObject4 = localObject2;
      localObject4 = (Collection)localObject2;
      bool7 = ((Collection)localObject4).isEmpty();
      if (!bool7)
      {
        localObject2 = ((Iterable)localObject2).iterator();
        int i3;
        do
        {
          bool7 = ((Iterator)localObject2).hasNext();
          if (!bool7) {
            break;
          }
          localObject4 = (n)((Iterator)localObject2).next();
          localCharacter2 = (Character)a;
          int i5 = localCharacter2.charValue();
          localObject4 = (Character)b).b;
          i3 = ((Character)localObject4).charValue();
          if (i5 == i3)
          {
            i3 = 1;
          }
          else
          {
            i3 = 0;
            localObject4 = null;
          }
        } while (i3 != 0);
        bool3 = false;
        localObject2 = null;
      }
      else
      {
        bool3 = true;
      }
    } while (!bool3);
    break label893;
    bool8 = false;
    paramString2 = null;
    label893:
    paramString2 = (List)paramString2;
    if (paramString2 != null)
    {
      paramString1 = Integer.valueOf(da);
      paramString2 = Integer.valueOf(fa + n);
      return t.a(paramString1, paramString2);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */