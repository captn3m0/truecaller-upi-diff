package com.truecaller.calling;

import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import c.g.b.w;
import c.l.b;
import c.l.g;

public final class ad
  implements u, v
{
  private final f b;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(ad.class);
    ((c.g.b.u)localObject).<init>(localb, "subtitle", "getSubtitle()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public ad(View paramView)
  {
    paramView = com.truecaller.utils.extensions.t.a(paramView, 2131363509);
    b = paramView;
  }
  
  private final TextView a()
  {
    return (TextView)b.b();
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    TextView localTextView = a();
    k.a(localTextView, "subtitle");
    ae.a(localTextView, paramInt1, paramInt2);
  }
  
  public final void e_(String paramString)
  {
    TextView localTextView = a();
    if (paramString != null)
    {
      k.a(localTextView, "this");
      com.truecaller.utils.extensions.t.a((View)localTextView);
      paramString = (CharSequence)paramString;
      localTextView.setText(paramString);
      return;
    }
    k.a(localTextView, "this");
    com.truecaller.utils.extensions.t.b((View)localTextView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */