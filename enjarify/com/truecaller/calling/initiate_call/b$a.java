package com.truecaller.calling.initiate_call;

import android.telecom.PhoneAccountHandle;

public final class b$a
{
  final String a;
  final String b;
  final String c;
  final Integer d;
  final boolean e;
  final boolean f;
  final PhoneAccountHandle g;
  
  private b$a(String paramString1, String paramString2, String paramString3, Integer paramInteger, boolean paramBoolean1, boolean paramBoolean2, PhoneAccountHandle paramPhoneAccountHandle)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramInteger;
    e = paramBoolean1;
    f = paramBoolean2;
    g = paramPhoneAccountHandle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */