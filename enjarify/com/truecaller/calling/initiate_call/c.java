package com.truecaller.calling.initiate_call;

import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.calling.ar;
import com.truecaller.common.h.aj;
import com.truecaller.data.entity.g;
import com.truecaller.multisim.h;
import com.truecaller.util.bz;
import com.truecaller.utils.d;
import com.truecaller.utils.l;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class c
  implements b
{
  final bz a;
  final com.truecaller.androidactors.f b;
  final com.truecaller.analytics.b c;
  final l d;
  final g e;
  final h f;
  final ar g;
  final d h;
  final com.truecaller.i.c i;
  final com.truecaller.calling.b.c j;
  final aj k;
  final e l;
  final Context m;
  private final TelephonyManager n;
  private final c.d.f o;
  
  public c(bz parambz, com.truecaller.androidactors.f paramf, com.truecaller.analytics.b paramb, l paraml, g paramg, h paramh, ar paramar, d paramd, com.truecaller.i.c paramc, com.truecaller.calling.b.c paramc1, TelephonyManager paramTelephonyManager, aj paramaj, e parame, Context paramContext, c.d.f paramf1)
  {
    a = parambz;
    b = paramf;
    c = paramb;
    d = paraml;
    e = paramg;
    f = paramh;
    g = paramar;
    h = paramd;
    i = paramc;
    j = paramc1;
    n = paramTelephonyManager;
    k = paramaj;
    l = parame;
    m = paramContext;
    o = paramf1;
  }
  
  public final void a(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    Object localObject1 = d;
    Object localObject2 = { "android.permission.READ_PHONE_STATE", "android.permission.CALL_PHONE" };
    boolean bool1 = ((l)localObject1).a((String[])localObject2);
    int i2 = 1;
    if (bool1)
    {
      localObject1 = (CharSequence)n.getVoiceMailNumber();
      if (localObject1 != null)
      {
        i1 = ((CharSequence)localObject1).length();
        if (i1 != 0)
        {
          i1 = 0;
          localObject1 = null;
          break label85;
        }
      }
      i1 = 1;
      label85:
      if (i1 == 0)
      {
        l.b(paramActivity);
        return;
      }
    }
    localObject1 = h;
    int i1 = ((d)localObject1).h();
    int i3 = 23;
    if (i1 < i3)
    {
      i2 = 0;
      localObject2 = null;
    }
    if (i2 != 0)
    {
      localObject1 = l;
      localObject2 = paramActivity;
      localObject2 = (Context)paramActivity;
      boolean bool2 = ((e)localObject1).a((Context)localObject2);
      if (bool2) {
        return;
      }
    }
    l.a(paramActivity);
  }
  
  public final void a(b.a parama)
  {
    k.b(parama, "callOptions");
    ag localag = (ag)bg.a;
    c.d.f localf = o;
    Object localObject = new com/truecaller/calling/initiate_call/c$a;
    ((c.a)localObject).<init>(this, parama, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (m)localObject, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */