package com.truecaller.calling.initiate_call;

import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.telecom.PhoneAccountHandle;
import android.widget.ListAdapter;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.b.c;
import com.truecaller.common.h.ab;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.utils.extensions.a;
import java.util.List;

public final class SelectPhoneAccountActivity
  extends AppCompatActivity
  implements k.a, k.b
{
  public static final SelectPhoneAccountActivity.a c;
  public l a;
  public b b;
  
  static
  {
    SelectPhoneAccountActivity.a locala = new com/truecaller/calling/initiate_call/SelectPhoneAccountActivity$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public final l a()
  {
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locall;
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, Integer paramInteger, PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean)
  {
    k.b(paramString1, "number");
    k.b(paramString2, "analyticsContext");
    b.a.a locala = new com/truecaller/calling/initiate_call/b$a$a;
    locala.<init>(paramString1, paramString2);
    paramString1 = (b.a.a)locala.a(paramString3).a(paramInteger);
    a = paramPhoneAccountHandle;
    paramString1 = paramString1.b(paramBoolean);
    paramString2 = b;
    if (paramString2 == null)
    {
      paramString3 = "initiateCallHelper";
      k.a(paramString3);
    }
    paramString1 = paramString1.a();
    paramString2.a(paramString1);
  }
  
  public final void a(List paramList, String paramString)
  {
    k.b(paramList, "accounts");
    k.b(paramString, "displayString");
    i locali = new com/truecaller/calling/initiate_call/i;
    Object localObject1 = this;
    localObject1 = (Context)this;
    locali.<init>((Context)localObject1, paramList);
    paramList = new android/support/v7/app/AlertDialog$Builder;
    paramList.<init>((Context)localObject1);
    boolean bool = true;
    Object localObject2 = new Object[bool];
    localObject2[0] = paramString;
    paramString = (CharSequence)getString(2131887929, (Object[])localObject2);
    paramList = paramList.setTitle(paramString);
    paramString = locali;
    paramString = (ListAdapter)locali;
    localObject2 = new com/truecaller/calling/initiate_call/SelectPhoneAccountActivity$b;
    ((SelectPhoneAccountActivity.b)localObject2).<init>(this, locali);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    paramList = paramList.setAdapter(paramString, (DialogInterface.OnClickListener)localObject2).setCancelable(bool);
    paramString = new com/truecaller/calling/initiate_call/SelectPhoneAccountActivity$c;
    paramString.<init>(this);
    paramString = (DialogInterface.OnCancelListener)paramString;
    paramList.setOnCancelListener(paramString).show();
  }
  
  public final void b()
  {
    finish();
  }
  
  public final void finish()
  {
    super.finish();
    overridePendingTransition(0, 0);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a.a(this);
    paramBundle = getTheme();
    Object localObject1 = ThemeManager.a();
    int i = resId;
    boolean bool1 = false;
    paramBundle.applyStyle(i, false);
    paramBundle = getApplicationContext();
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().cC().a(this);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        k.a((String)localObject1);
      }
      c = this;
      paramBundle = getIntent().getStringExtra("extraNumber");
      localObject1 = getIntent().getStringExtra("extraDisplayName");
      String str1 = getIntent().getStringExtra("extraAnalyticsContext");
      Object localObject2 = getIntent();
      boolean bool2 = ((Intent)localObject2).getBooleanExtra("noCallMeBack", false);
      Object localObject3 = a;
      String str2;
      if (localObject3 == null)
      {
        str2 = "presenter";
        k.a(str2);
      }
      ((l)localObject3).a(this);
      try
      {
        localObject3 = a;
        if (localObject3 == null)
        {
          str2 = "presenter";
          k.a(str2);
        }
        str2 = "number";
        k.a(paramBundle, str2);
        str2 = "displayName";
        k.a(localObject1, str2);
        str2 = "analyticsContext";
        k.a(str1, str2);
        str2 = "number";
        k.b(paramBundle, str2);
        str2 = "displayName";
        k.b(localObject1, str2);
        str2 = "analyticsContext";
        k.b(str1, str2);
        a = paramBundle;
        d = ((String)localObject1);
        e = str1;
        f = bool2;
        localObject2 = paramBundle;
        localObject2 = (CharSequence)paramBundle;
        bool2 = ab.a((CharSequence)localObject2);
        if (!bool2)
        {
          localObject2 = "Non-callable number was passed";
          localObject2 = new String[] { localObject2 };
          AssertionUtil.OnlyInDebug.fail((String[])localObject2);
          localObject2 = b;
          localObject2 = (k.b)localObject2;
          if (localObject2 != null)
          {
            ((k.b)localObject2).b();
            return;
          }
          return;
        }
        localObject2 = g;
        localObject2 = ((c)localObject2).a();
        localObject3 = b;
        localObject3 = (k.b)localObject3;
        if (localObject3 != null)
        {
          ((k.b)localObject3).a((List)localObject2, (String)localObject1);
          return;
        }
        return;
      }
      catch (IllegalStateException localIllegalStateException)
      {
        localObject2 = new java/lang/StringBuilder;
        localObject3 = "number is null: ";
        ((StringBuilder)localObject2).<init>((String)localObject3);
        boolean bool3;
        if (paramBundle == null)
        {
          bool3 = true;
        }
        else
        {
          bool3 = false;
          paramBundle = null;
        }
        ((StringBuilder)localObject2).append(bool3);
        paramBundle = ", displayName is null: ";
        ((StringBuilder)localObject2).append(paramBundle);
        if (localObject1 == null)
        {
          bool3 = true;
        }
        else
        {
          bool3 = false;
          paramBundle = null;
        }
        ((StringBuilder)localObject2).append(bool3);
        paramBundle = ", analyticsContext is null: ";
        ((StringBuilder)localObject2).append(paramBundle);
        if (str1 == null) {
          bool1 = true;
        }
        ((StringBuilder)localObject2).append(bool1);
        AssertionUtil.reportWeirdnessButNeverCrash(((StringBuilder)localObject2).toString());
        return;
      }
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.SelectPhoneAccountActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */