package com.truecaller.calling.initiate_call;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private g(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static g a(Provider paramProvider1, Provider paramProvider2)
  {
    g localg = new com/truecaller/calling/initiate_call/g;
    localg.<init>(paramProvider1, paramProvider2);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */