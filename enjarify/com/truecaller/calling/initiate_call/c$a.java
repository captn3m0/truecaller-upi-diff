package com.truecaller.calling.initiate_call;

import android.content.Context;
import android.net.Uri;
import android.telecom.PhoneAccountHandle;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.callerid.ai;
import com.truecaller.calling.ar;
import com.truecaller.common.h.aj;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.tracking.events.f.a;
import com.truecaller.util.bz;
import com.truecaller.util.ca;
import com.truecaller.utils.l;
import java.util.List;
import kotlinx.coroutines.ag;

final class c$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  Object c;
  int d;
  private ag g;
  
  c$a(c paramc, b.a parama, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/calling/initiate_call/c$a;
    c localc = e;
    b.a locala1 = f;
    locala.<init>(localc, locala1, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = this;
    Object localObject1 = paramObject;
    Object localObject2 = c.d.a.a.a;
    int i = d;
    int j = 1;
    Object localObject3 = null;
    Object localObject5;
    boolean bool2;
    switch (i)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 1: 
      localObject2 = (String)c;
      localObject4 = (Number)b;
      localObject5 = (e.a)a;
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label1791;
      }
      localObject1 = f.a;
      if (localObject1 == null) {
        break label1774;
      }
      localObject1 = (CharSequence)f.a;
      int n = ((CharSequence)localObject1).length();
      if (n == 0)
      {
        n = 1;
      }
      else
      {
        n = 0;
        localObject1 = null;
      }
      if (n != 0) {
        break label1774;
      }
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("TruecallerCallInitiated");
      localObject5 = f.b;
      localObject5 = ((e.a)localObject1).a("Context", (String)localObject5);
      localObject1 = e.k;
      localObject4 = f.a;
      bool5 = ((aj)localObject1).b((String)localObject4);
      if (!bool5) {
        return x.a;
      }
      localObject1 = e.a;
      localObject4 = f.a;
      localObject1 = ((bz)localObject1).a((String)localObject4);
      if (localObject1 != null)
      {
        localObject2 = e.m;
        ((ca)localObject1).a((Context)localObject2);
        return x.a;
      }
      localObject1 = e.e;
      localObject4 = new String[j];
      localObject6 = f.a;
      localObject4[0] = localObject6;
      localObject4 = ((g)localObject1).a((String[])localObject4);
      if (localObject4 == null) {
        return x.a;
      }
      c.g.b.k.a(localObject4, "numberProvider.provideFr….number) ?: return@launch");
      localObject1 = f.a;
      localObject6 = f;
      bool2 = f;
      if (bool2) {
        break label479;
      }
      localObject6 = e.l;
      localObject7 = e.m;
      localObject8 = f.a;
      str1 = ((Number)localObject4).a();
      a = localObject5;
      b = localObject4;
      c = localObject1;
      d = j;
      localObject6 = ((e)localObject6).a((Context)localObject7, (String)localObject8, str1, locala);
      if (localObject6 == localObject2) {
        return localObject2;
      }
      localObject2 = localObject1;
      localObject1 = localObject6;
    }
    localObject1 = (Boolean)localObject1;
    boolean bool5 = ((Boolean)localObject1).booleanValue();
    if (bool5) {
      return x.a;
    }
    localObject1 = localObject2;
    label479:
    localObject2 = e;
    Object localObject6 = f.d;
    Object localObject7 = f.g;
    c.g.b.k.b(localObject1, "phoneNumber");
    Object localObject8 = f;
    boolean bool6 = ((h)localObject8).e();
    int i1;
    if (bool6)
    {
      localObject8 = f;
      bool6 = ((h)localObject8).k();
      if (bool6)
      {
        if (localObject6 != null)
        {
          i1 = ((Integer)localObject6).intValue();
          if (i1 == 0) {}
        }
        else if (localObject6 != null)
        {
          int k = ((Integer)localObject6).intValue();
          if (k == j) {}
        }
        else
        {
          bool3 = false;
          localObject6 = null;
          break label611;
        }
        boolean bool3 = true;
        label611:
        if (localObject7 != null)
        {
          i2 = 1;
        }
        else
        {
          i2 = 0;
          localObject7 = null;
        }
        if ((!bool3) && (i2 == 0))
        {
          localObject6 = k;
          bool3 = ((aj)localObject6).a((String)localObject1);
          if (bool3)
          {
            bool8 = false;
            localObject2 = null;
            break label789;
          }
          localObject6 = j.a();
          int m = ((List)localObject6).size();
          if (m > j)
          {
            bool4 = true;
          }
          else
          {
            bool4 = false;
            localObject6 = null;
          }
          if (!bool4)
          {
            bool8 = false;
            localObject2 = null;
            break label789;
          }
          localObject2 = g.e();
          localObject6 = "-1";
          bool8 = c.g.b.k.a(localObject2, localObject6) ^ j;
          if (bool8)
          {
            bool8 = false;
            localObject2 = null;
            break label789;
          }
          bool8 = true;
          break label789;
        }
        bool8 = false;
        localObject2 = null;
        break label789;
      }
    }
    boolean bool8 = false;
    localObject2 = null;
    label789:
    Object localObject9;
    if (bool8)
    {
      localObject2 = e.l;
      localObject4 = e.m;
      localObject9 = f.c;
      if (localObject9 == null) {
        localObject3 = localObject1;
      } else {
        localObject3 = localObject9;
      }
      localObject5 = f.b;
      bool4 = f.f;
      localObject9 = localObject1;
      ((e)localObject2).a((Context)localObject4, (String)localObject1, (String)localObject3, (String)localObject5, bool4);
      return x.a;
    }
    localObject2 = e;
    localObject6 = f.b;
    i.b("key_temp_latest_call_made_with_tc", j);
    localObject7 = i;
    long l = System.currentTimeMillis();
    ((com.truecaller.i.c)localObject7).b("lastCallMadeWithTcTime", l);
    localObject7 = i;
    localObject8 = "key_last_call_origin";
    localObject7 = (CharSequence)((com.truecaller.i.c)localObject7).a((String)localObject8);
    if (localObject7 != null)
    {
      i2 = ((CharSequence)localObject7).length();
      if (i2 != 0)
      {
        i2 = 0;
        localObject7 = null;
        break label988;
      }
    }
    int i2 = 1;
    label988:
    if (i2 != 0)
    {
      localObject2 = i;
      localObject7 = "key_last_call_origin";
      ((com.truecaller.i.c)localObject2).a((String)localObject7, (String)localObject6);
    }
    localObject2 = e;
    String str1 = f.b;
    localObject6 = f.g;
    localObject7 = f;
    boolean bool7 = e;
    localObject8 = f.d;
    String str2 = ((Number)localObject4).a();
    c.g.b.k.a(str2, "parsedNumber.normalizedNumber");
    Object localObject10 = d;
    Object localObject11 = { "android.permission.CALL_PRIVILEGED" };
    boolean bool9 = ((l)localObject10).a((String[])localObject11);
    if (bool9) {
      localObject10 = "android.intent.action.CALL_PRIVILEGED";
    } else {
      localObject10 = "android.intent.action.CALL";
    }
    localObject11 = h;
    int i3 = ((com.truecaller.utils.d)localObject11).h();
    int i4 = 23;
    if (i3 >= i4)
    {
      i3 = 1;
    }
    else
    {
      i3 = 0;
      localObject11 = null;
    }
    i4 = 0;
    if ((i3 == 0) || (localObject6 == null))
    {
      bool4 = false;
      localObject6 = null;
    }
    boolean bool10;
    if ((i3 != 0) && (bool7)) {
      bool10 = true;
    } else {
      bool10 = false;
    }
    localObject7 = f;
    bool7 = ((h)localObject7).k();
    if (bool7)
    {
      localObject7 = f;
      bool7 = ((h)localObject7).e();
      if (bool7)
      {
        if (localObject8 == null)
        {
          localObject7 = g.e();
          localObject12 = localObject7;
          break label1324;
        }
        localObject7 = f;
        i1 = ((Integer)localObject8).intValue();
        localObject7 = ((h)localObject7).a(i1);
        if (localObject7 != null)
        {
          localObject7 = b;
          localObject12 = localObject7;
          break label1324;
        }
        localObject12 = null;
        break label1324;
      }
    }
    Object localObject12 = null;
    label1324:
    localObject2 = k;
    localObject7 = localObject1;
    localObject7 = (CharSequence)localObject1;
    bool8 = ((aj)localObject2).a((CharSequence)localObject7);
    if (bool8)
    {
      if (localObject1 != null)
      {
        localObject1 = c.n.m.b((CharSequence)localObject7).toString();
        localObject2 = "sip:";
        bool8 = c.n.m.b((String)localObject1, (String)localObject2, false);
        if (bool8)
        {
          localObject1 = Uri.parse((String)localObject1);
        }
        else
        {
          localObject2 = "sip";
          localObject1 = Uri.fromParts((String)localObject2, (String)localObject1, null);
        }
      }
      else
      {
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
        throw ((Throwable)localObject1);
      }
    }
    else
    {
      localObject2 = "tel";
      localObject1 = Uri.fromParts((String)localObject2, (String)localObject1, null);
    }
    localObject2 = new com/truecaller/calling/initiate_call/a;
    localObject7 = localObject2;
    localObject8 = localObject10;
    localObject10 = localObject1;
    localObject11 = localObject6;
    ((a)localObject2).<init>((String)localObject8, str1, (Uri)localObject1, (PhoneAccountHandle)localObject6, (String)localObject12, bool10, str2);
    localObject1 = e;
    localObject6 = f.b;
    localObject7 = com.truecaller.tracking.events.f.b();
    localObject6 = (CharSequence)localObject6;
    ((f.a)localObject7).b((CharSequence)localObject6);
    Object localObject4 = (CharSequence)((Number)localObject4).b();
    ((f.a)localObject7).a((CharSequence)localObject4);
    localObject4 = (CharSequence)ai.a();
    ((f.a)localObject7).c((CharSequence)localObject4);
    try
    {
      localObject1 = b;
      localObject1 = ((com.truecaller.androidactors.f)localObject1).a();
      localObject1 = (ae)localObject1;
      localObject4 = ((f.a)localObject7).a();
      localObject4 = (org.apache.a.d.d)localObject4;
      ((ae)localObject1).a((org.apache.a.d.d)localObject4);
    }
    catch (org.apache.a.a locala1)
    {
      localObject1 = (Throwable)locala1;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
    }
    localObject1 = e;
    c.g.b.k.a(localObject5, "event");
    boolean bool4 = e;
    ((e.a)localObject5).a("IsVideo", bool4);
    localObject4 = c;
    if (localObject4 == null) {
      localObject4 = "PSTN";
    } else {
      localObject4 = "SIP";
    }
    ((e.a)localObject5).a("Network", (String)localObject4);
    localObject6 = a;
    ((e.a)localObject5).a("IntentAction", (String)localObject6);
    localObject4 = "IsSpecificSim";
    localObject6 = d;
    if (localObject6 == null)
    {
      j = 0;
      localObject9 = null;
    }
    ((e.a)localObject5).a((String)localObject4, j);
    localObject1 = c;
    localObject4 = ((e.a)localObject5).a();
    c.g.b.k.a(localObject4, "event.build()");
    ((b)localObject1).b((com.truecaller.analytics.e)localObject4);
    localObject1 = e.l;
    localObject4 = e.m;
    ((e)localObject1).a((Context)localObject4, (a)localObject2);
    return x.a;
    label1774:
    AssertionUtil.OnlyInDebug.fail(new String[] { "Cannot call null or empty number" });
    return x.a;
    label1791:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */