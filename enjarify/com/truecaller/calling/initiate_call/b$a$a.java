package com.truecaller.calling.initiate_call;

import android.telecom.PhoneAccountHandle;

public final class b$a$a
{
  PhoneAccountHandle a;
  private String b;
  private Integer c;
  private boolean d;
  private boolean e;
  private final String f;
  private final String g;
  
  public b$a$a(String paramString1, String paramString2)
  {
    f = paramString1;
    g = paramString2;
  }
  
  public final a a(Integer paramInteger)
  {
    a locala = this;
    locala = (a)this;
    c = paramInteger;
    return locala;
  }
  
  public final a a(String paramString)
  {
    a locala = this;
    locala = (a)this;
    b = paramString;
    return locala;
  }
  
  public final a a(boolean paramBoolean)
  {
    a locala = this;
    locala = (a)this;
    d = paramBoolean;
    return locala;
  }
  
  public final b.a a()
  {
    b.a locala = new com/truecaller/calling/initiate_call/b$a;
    String str1 = f;
    String str2 = g;
    String str3 = b;
    Integer localInteger = c;
    boolean bool1 = d;
    boolean bool2 = e;
    PhoneAccountHandle localPhoneAccountHandle = a;
    locala.<init>(str1, str2, str3, localInteger, bool1, bool2, localPhoneAccountHandle, (byte)0);
    return locala;
  }
  
  public final a b(boolean paramBoolean)
  {
    a locala = this;
    locala = (a)this;
    e = paramBoolean;
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.b.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */