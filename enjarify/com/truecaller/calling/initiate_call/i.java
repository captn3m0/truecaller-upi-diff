package com.truecaller.calling.initiate_call;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.utils.extensions.t;
import java.util.List;

public final class i
  extends ArrayAdapter
{
  public i(Context paramContext, List paramList)
  {
    super(paramContext, 2131558923, paramList);
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    k.b(paramViewGroup, "parent");
    boolean bool1 = false;
    Context localContext = null;
    Object localObject1;
    int i;
    if (paramView == null)
    {
      paramView = getContext();
      localObject1 = "layout_inflater";
      paramView = paramView.getSystemService((String)localObject1);
      if (paramView != null)
      {
        paramView = (LayoutInflater)paramView;
        i = 2131558923;
        paramView = paramView.inflate(i, paramViewGroup, false);
        k.a(paramView, "inflater.inflate(R.layou…er_dialog, parent, false)");
        paramViewGroup = new com/truecaller/calling/initiate_call/i$a;
        paramViewGroup.<init>(paramView);
        paramView.setTag(paramViewGroup);
      }
      else
      {
        localObject2 = new c/u;
        ((u)localObject2).<init>("null cannot be cast to non-null type android.view.LayoutInflater");
        throw ((Throwable)localObject2);
      }
    }
    else
    {
      paramViewGroup = paramView.getTag();
      if (paramViewGroup == null) {
        break label352;
      }
      paramViewGroup = (i.a)paramViewGroup;
    }
    Object localObject2 = (h)getItem(paramInt);
    if (localObject2 == null)
    {
      localObject2 = a;
      bool1 = false;
      localContext = null;
      ((TextView)localObject2).setText(null);
      localObject2 = b;
      i = 8;
      ((TextView)localObject2).setVisibility(i);
      localObject2 = c;
      ((ImageView)localObject2).setImageDrawable(null);
    }
    else
    {
      localObject1 = a;
      CharSequence localCharSequence = a;
      ((TextView)localObject1).setText(localCharSequence);
      localObject1 = b;
      localCharSequence = b;
      ((TextView)localObject1).setText(localCharSequence);
      localObject1 = (View)b;
      localCharSequence = b;
      boolean bool2 = true;
      if (localCharSequence != null)
      {
        int j = localCharSequence.length();
        if (j != 0) {}
      }
      else
      {
        bool1 = true;
      }
      bool1 ^= bool2;
      t.a((View)localObject1, bool1);
      bool1 = localObject2 instanceof h.b;
      if (bool1)
      {
        paramViewGroup = c;
        localObject2 = c;
        localContext = getContext();
        localObject2 = ((Icon)localObject2).loadDrawable(localContext);
        paramViewGroup.setImageDrawable((Drawable)localObject2);
      }
      else
      {
        bool1 = localObject2 instanceof h.a;
        if (bool1)
        {
          paramViewGroup = c;
          localObject2 = c;
          paramViewGroup.setImageDrawable((Drawable)localObject2);
        }
      }
    }
    return paramView;
    label352:
    localObject2 = new c/u;
    ((u)localObject2).<init>("null cannot be cast to non-null type com.truecaller.calling.initiate_call.SelectPhoneAccountAdapter.ViewHolder");
    throw ((Throwable)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */