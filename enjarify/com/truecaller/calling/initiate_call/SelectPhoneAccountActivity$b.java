package com.truecaller.calling.initiate_call;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.telecom.PhoneAccountHandle;
import c.g.b.k;
import com.truecaller.bd;

final class SelectPhoneAccountActivity$b
  implements DialogInterface.OnClickListener
{
  SelectPhoneAccountActivity$b(SelectPhoneAccountActivity paramSelectPhoneAccountActivity, i parami) {}
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    paramDialogInterface = (h)b.getItem(paramInt);
    if (paramDialogInterface == null) {
      return;
    }
    l locall = a.a();
    Object localObject1 = "phoneAccountInfo";
    k.b(paramDialogInterface, (String)localObject1);
    boolean bool1 = paramDialogInterface instanceof h.b;
    Object localObject2;
    String str1;
    String str2;
    String str3;
    Integer localInteger;
    PhoneAccountHandle localPhoneAccountHandle;
    boolean bool2;
    if (bool1)
    {
      localObject1 = c;
      localObject2 = localObject1;
      localObject2 = (k.a)localObject1;
      if (localObject2 != null)
      {
        str1 = a;
        if (str1 == null)
        {
          localObject1 = "number";
          k.a((String)localObject1);
        }
        str2 = e;
        if (str2 == null)
        {
          localObject1 = "analyticsContext";
          k.a((String)localObject1);
        }
        str3 = d;
        if (str3 == null)
        {
          localObject1 = "displayName";
          k.a((String)localObject1);
        }
        localInteger = null;
        paramDialogInterface = (h.b)paramDialogInterface;
        localPhoneAccountHandle = d;
        bool2 = f;
        ((k.a)localObject2).a(str1, str2, str3, null, localPhoneAccountHandle, bool2);
      }
    }
    else
    {
      bool1 = paramDialogInterface instanceof h.a;
      if (bool1)
      {
        localObject1 = c;
        localObject2 = localObject1;
        localObject2 = (k.a)localObject1;
        if (localObject2 != null)
        {
          str1 = a;
          if (str1 == null)
          {
            localObject1 = "number";
            k.a((String)localObject1);
          }
          str2 = e;
          if (str2 == null)
          {
            localObject1 = "analyticsContext";
            k.a((String)localObject1);
          }
          str3 = d;
          if (str3 == null)
          {
            localObject1 = "displayName";
            k.a((String)localObject1);
          }
          paramDialogInterface = (h.a)paramDialogInterface;
          int i = d;
          localInteger = Integer.valueOf(i);
          localPhoneAccountHandle = null;
          bool2 = f;
          ((k.a)localObject2).a(str1, str2, str3, localInteger, null, bool2);
        }
      }
    }
    paramDialogInterface = (k.b)b;
    if (paramDialogInterface != null)
    {
      paramDialogInterface.b();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.SelectPhoneAccountActivity.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */