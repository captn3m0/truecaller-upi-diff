package com.truecaller.calling.initiate_call;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.content.d;
import android.support.v7.app.AlertDialog.Builder;
import c.d.c;
import c.g.b.k;
import com.truecaller.common.h.o;
import com.truecaller.multisim.h;

public final class f
  implements e
{
  private final h a;
  private final com.truecaller.service.a b;
  
  public f(h paramh, com.truecaller.service.a parama)
  {
    a = paramh;
    b = parama;
  }
  
  public final Object a(Context paramContext, String paramString1, String paramString2, c paramc)
  {
    return b.a(paramContext, paramString1, paramString2, paramc);
  }
  
  public final void a(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    paramActivity = (Context)paramActivity;
    localBuilder.<init>(paramActivity);
    localBuilder.setTitle(2131887937).setMessage(2131887936).setPositiveButton(2131887217, null).show();
  }
  
  public final void a(Context paramContext, a parama)
  {
    k.b(paramContext, "context");
    k.b(parama, "callIntent");
    Object localObject1 = a;
    Uri localUri = b;
    Object localObject2 = c;
    String str = d;
    boolean bool1 = e;
    Intent localIntent = new android/content/Intent;
    localIntent.<init>((String)localObject1, localUri);
    if (localObject2 != null)
    {
      localObject1 = "android.telecom.extra.PHONE_ACCOUNT_HANDLE";
      localObject2 = (Parcelable)localObject2;
      localIntent.putExtra((String)localObject1, (Parcelable)localObject2);
    }
    else if (str != null)
    {
      localObject1 = a;
      ((h)localObject1).a(localIntent, str);
    }
    if (bool1)
    {
      localIntent.putExtra("android.telecom.extra.START_CALL_WITH_VIDEO_STATE", 3);
      parama = "com.android.phone.extra.video";
      boolean bool2 = true;
      localIntent.putExtra(parama, bool2);
    }
    int i = 268435456;
    localIntent.addFlags(i);
    try
    {
      paramContext.startActivity(localIntent);
      paramContext = d.a(paramContext);
      paramContext.b(localIntent);
      return;
    }
    catch (SecurityException localSecurityException) {}catch (ActivityNotFoundException localActivityNotFoundException) {}
  }
  
  public final void a(Context paramContext, String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    k.b(paramContext, "context");
    k.b(paramString1, "number");
    k.b(paramString2, "displayName");
    k.b(paramString3, "analyticsContext");
    Object localObject = SelectPhoneAccountActivity.c;
    k.b(paramContext, "context");
    k.b(paramString1, "number");
    k.b(paramString2, "displayName");
    k.b(paramString3, "analyticsContext");
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>(paramContext, SelectPhoneAccountActivity.class);
    ((Intent)localObject).putExtra("extraNumber", paramString1);
    ((Intent)localObject).putExtra("extraDisplayName", paramString2);
    ((Intent)localObject).putExtra("extraAnalyticsContext", paramString3);
    ((Intent)localObject).putExtra("noCallMeBack", paramBoolean);
    ((Intent)localObject).addFlags(268435456);
    paramContext.startActivity((Intent)localObject);
  }
  
  public final boolean a(Context paramContext)
  {
    k.b(paramContext, "activity");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.telephony.action.CONFIGURE_VOICEMAIL");
    return o.a(paramContext, localIntent);
  }
  
  public final void b(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    Intent localIntent = new android/content/Intent;
    Uri localUri = Uri.fromParts("voicemail", "", null);
    localIntent.<init>("android.intent.action.CALL", localUri);
    o.a((Context)paramActivity, localIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */