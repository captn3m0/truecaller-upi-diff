package com.truecaller.calling.initiate_call;

import android.graphics.drawable.Drawable;

public final class h$a
  extends h
{
  final Drawable c;
  final int d;
  
  public h$a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, Drawable paramDrawable, int paramInt)
  {
    super(paramCharSequence1, paramCharSequence2, (byte)0);
    c = paramDrawable;
    d = paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */