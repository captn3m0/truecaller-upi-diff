package com.truecaller.calling;

public enum ActionType
{
  public static final ActionType.a Companion;
  private final String eventAction;
  
  static
  {
    Object localObject = new ActionType[8];
    ActionType localActionType = new com/truecaller/calling/ActionType;
    localActionType.<init>("SMS", 0, "ItemEvent.ACTION_SMS");
    SMS = localActionType;
    localObject[0] = localActionType;
    localActionType = new com/truecaller/calling/ActionType;
    int i = 1;
    localActionType.<init>("FLASH", i, "ItemEvent.ACTION_FLASH");
    FLASH = localActionType;
    localObject[i] = localActionType;
    localActionType = new com/truecaller/calling/ActionType;
    i = 2;
    localActionType.<init>("PROFILE", i, "ItemEvent.ACTION_OPEN_PROFILE");
    PROFILE = localActionType;
    localObject[i] = localActionType;
    localActionType = new com/truecaller/calling/ActionType;
    i = 3;
    localActionType.<init>("CELLULAR_CALL", i, "ItemEvent.ACTION_CELLULAR_CALL");
    CELLULAR_CALL = localActionType;
    localObject[i] = localActionType;
    localActionType = new com/truecaller/calling/ActionType;
    i = 4;
    localActionType.<init>("CELLULAR_VIDEO_CALL", i, "ItemEvent.ACTION_CELLULAR_VIDEO_CALL");
    CELLULAR_VIDEO_CALL = localActionType;
    localObject[i] = localActionType;
    localActionType = new com/truecaller/calling/ActionType;
    i = 5;
    localActionType.<init>("WHATSAPP_CALL", i, "ItemEvent.ACTION_WHATSAPP_CALL");
    WHATSAPP_CALL = localActionType;
    localObject[i] = localActionType;
    localActionType = new com/truecaller/calling/ActionType;
    i = 6;
    localActionType.<init>("WHATSAPP_VIDEO_CALL", i, "ItemEvent.ACTION_WHATSAPP_VIDEO_CALL");
    WHATSAPP_VIDEO_CALL = localActionType;
    localObject[i] = localActionType;
    localActionType = new com/truecaller/calling/ActionType;
    i = 7;
    localActionType.<init>("VOIP_CALL", i, "ItemEvent.ACTION_VOIP_CALL");
    VOIP_CALL = localActionType;
    localObject[i] = localActionType;
    $VALUES = (ActionType[])localObject;
    localObject = new com/truecaller/calling/ActionType$a;
    ((ActionType.a)localObject).<init>((byte)0);
    Companion = (ActionType.a)localObject;
  }
  
  private ActionType(String paramString1)
  {
    eventAction = paramString1;
  }
  
  public final String getEventAction()
  {
    return eventAction;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ActionType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */