package com.truecaller.calling;

import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;

public final class s
  implements t
{
  private final f b;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(s.class);
    ((u)localObject).<init>(localb, "dateTextView", "getDateTextView()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public s(View paramView)
  {
    paramView = com.truecaller.utils.extensions.t.a(paramView, 2131363505);
    b = paramView;
  }
  
  public final void a(String paramString)
  {
    TextView localTextView = (TextView)b.b();
    if (paramString != null)
    {
      com.truecaller.utils.extensions.t.a((View)localTextView);
      paramString = (CharSequence)paramString;
      localTextView.setText(paramString);
      return;
    }
    k.a(localTextView, "this");
    com.truecaller.utils.extensions.t.b((View)localTextView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */