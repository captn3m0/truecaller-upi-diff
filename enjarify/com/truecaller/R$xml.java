package com.truecaller;

public final class R$xml
{
  public static final int authenticator = 2132082688;
  public static final int backup_rules = 2132082689;
  public static final int ecommerce_tracker = 2132082690;
  public static final int fileprovider = 2132082691;
  public static final int incoming_incallui_call_answer_motion_scene = 2132082692;
  public static final int incoming_voip_call_answer_motion_scene = 2132082693;
  public static final int network_security_config = 2132082694;
  public static final int outgoing_incallui_call_ended_motion_scene = 2132082695;
  public static final int outgoing_voip_call_ended_motion_scene = 2132082696;
  public static final int provider_paths = 2132082697;
  public static final int quick_shortcuts_main = 2132082698;
  public static final int quick_shortcuts_messages = 2132082699;
  public static final int remote_config_defaults = 2132082700;
  public static final int widget_config = 2132082701;
}

/* Location:
 * Qualified Name:     com.truecaller.R.xml
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */