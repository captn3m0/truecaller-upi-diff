package com.truecaller.callerid;

import com.truecaller.aftercall.PromotionType;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.i.c;

public abstract interface k
{
  public abstract void a();
  
  public abstract void a(PromotionType paramPromotionType, HistoryEvent paramHistoryEvent, c paramc);
  
  public abstract void a(i parami, boolean paramBoolean);
  
  public abstract void a(HistoryEvent paramHistoryEvent, int paramInt);
  
  public abstract void b();
  
  public abstract void c();
  
  public abstract w d();
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */