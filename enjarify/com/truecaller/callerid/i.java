package com.truecaller.callerid;

import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.filters.g;
import com.truecaller.util.w;
import java.util.Random;

public final class i
{
  public final Number a;
  public final int b;
  public final long c;
  public final long d;
  public final boolean e;
  public final boolean f;
  public final String g;
  public final int h;
  public int i;
  public boolean j;
  public boolean k;
  public Contact l;
  final g m;
  boolean n;
  String o = null;
  
  public i(int paramInt1, int paramInt2, Number paramNumber, int paramInt3, boolean paramBoolean, long paramLong, Contact paramContact, String paramString, g paramg)
  {
    a = paramNumber;
    b = paramInt3;
    f = paramBoolean;
    l = paramContact;
    paramNumber = new java/util/Random;
    paramNumber.<init>();
    long l1 = paramNumber.nextLong();
    c = l1;
    d = paramLong;
    boolean bool;
    if (paramInt1 != 0)
    {
      bool = true;
    }
    else
    {
      bool = false;
      paramNumber = null;
    }
    e = bool;
    g = paramString;
    h = paramInt2;
    i = paramInt1;
    m = paramg;
  }
  
  public final boolean a()
  {
    Contact localContact = l;
    g localg = m;
    return w.a(localContact, localg);
  }
  
  public final int b()
  {
    boolean bool1 = c();
    if (bool1) {
      return 7;
    }
    int i1 = h;
    int i3 = 2;
    if (i1 == i3) {
      return 12;
    }
    boolean bool2 = e;
    if (bool2)
    {
      int i2 = i;
      int i4 = 3;
      if (i2 == i4)
      {
        boolean bool3 = j;
        if (!bool3) {
          return 6;
        }
      }
      return i3;
    }
    return 1;
  }
  
  public final boolean c()
  {
    int i1 = h;
    int i2 = 1;
    if (i1 != i2)
    {
      int i3 = 3;
      if (i1 != i3) {
        return false;
      }
    }
    return i2;
  }
  
  public final String toString()
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("CallState{simSlotIndex=");
    int i1 = b;
    ((StringBuilder)localObject1).append(i1);
    ((StringBuilder)localObject1).append(", sessionId=");
    long l1 = c;
    ((StringBuilder)localObject1).append(l1);
    ((StringBuilder)localObject1).append(", startTime=");
    l1 = d;
    ((StringBuilder)localObject1).append(l1);
    ((StringBuilder)localObject1).append(", isIncoming=");
    boolean bool1 = e;
    ((StringBuilder)localObject1).append(bool1);
    ((StringBuilder)localObject1).append(", isFromTrueCaller=");
    bool1 = f;
    ((StringBuilder)localObject1).append(bool1);
    ((StringBuilder)localObject1).append(", callId='");
    String str = g;
    ((StringBuilder)localObject1).append(str);
    char c1 = '\'';
    ((StringBuilder)localObject1).append(c1);
    ((StringBuilder)localObject1).append(", action=");
    int i2 = h;
    ((StringBuilder)localObject1).append(i2);
    ((StringBuilder)localObject1).append(", state=");
    i2 = i;
    ((StringBuilder)localObject1).append(i2);
    ((StringBuilder)localObject1).append(", wasConnected=");
    boolean bool2 = j;
    ((StringBuilder)localObject1).append(bool2);
    ((StringBuilder)localObject1).append(", isSearching=");
    bool2 = k;
    ((StringBuilder)localObject1).append(bool2);
    ((StringBuilder)localObject1).append(", contact=");
    Object localObject2 = l;
    ((StringBuilder)localObject1).append(localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    if (localObject1 == null) {
      return "null";
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("<non-null contact>, filter action=");
    localObject2 = m.h;
    ((StringBuilder)localObject1).append(localObject2);
    ((StringBuilder)localObject1).append(", wasSearchPerformed=");
    bool2 = n;
    ((StringBuilder)localObject1).append(bool2);
    ((StringBuilder)localObject1).append(", noSearchReason='");
    localObject2 = o;
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(c1);
    ((StringBuilder)localObject1).append('}');
    return ((StringBuilder)localObject1).toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */