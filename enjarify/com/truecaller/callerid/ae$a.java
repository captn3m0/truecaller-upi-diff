package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Number;
import com.truecaller.network.search.j;

final class ae$a
  extends u
{
  private final Number b;
  private final boolean c;
  private final int d;
  private final j e;
  
  private ae$a(e parame, Number paramNumber, boolean paramBoolean, int paramInt, j paramj)
  {
    super(parame);
    b = paramNumber;
    c = paramBoolean;
    d = paramInt;
    e = paramj;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".performSearch(");
    Object localObject = b;
    int i = 1;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = Boolean.valueOf(c);
    int j = 2;
    localObject = a(localObject, j);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(d), j);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(e, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.ae.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */