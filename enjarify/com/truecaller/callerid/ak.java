package com.truecaller.callerid;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

final class ak
  implements aj
{
  private final ScheduledThreadPoolExecutor a;
  
  ak(ScheduledThreadPoolExecutor paramScheduledThreadPoolExecutor)
  {
    a = paramScheduledThreadPoolExecutor;
  }
  
  public final void a(long paramLong, TimeUnit paramTimeUnit, Runnable paramRunnable)
  {
    a.schedule(paramRunnable, paramLong, paramTimeUnit);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */