package com.truecaller.callerid;

import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d
{
  private final q a;
  private final Provider b;
  private final Provider c;
  
  private s(q paramq, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramq;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static s a(q paramq, Provider paramProvider1, Provider paramProvider2)
  {
    s locals = new com/truecaller/callerid/s;
    locals.<init>(paramq, paramProvider1, paramProvider2);
    return locals;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */