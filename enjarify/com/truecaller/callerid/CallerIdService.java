package com.truecaller.callerid;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import com.truecaller.TrueApp;
import com.truecaller.aftercall.AfterCallPromotionActivity;
import com.truecaller.aftercall.PromotionType;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callerid.b.c.a;
import com.truecaller.calling.after_call.AfterCallActivity;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.notificationchannels.e;
import com.truecaller.ui.o;

public class CallerIdService
  extends Service
  implements c.a, k
{
  public f a;
  public e b;
  private com.truecaller.callerid.b.c c;
  
  public static void a(Context paramContext, Bundle paramBundle)
  {
    Intent localIntent = new android/content/Intent;
    Class localClass = CallerIdService.class;
    localIntent.<init>(paramContext, localClass);
    localIntent.addFlags(32);
    localIntent.putExtras(paramBundle);
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    if (i >= j)
    {
      paramContext.startForegroundService(localIntent);
      return;
    }
    try
    {
      paramContext.startService(localIntent);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localSecurityException;
    }
  }
  
  public final void a()
  {
    stopSelf();
  }
  
  public final void a(PromotionType paramPromotionType, HistoryEvent paramHistoryEvent, com.truecaller.i.c paramc)
  {
    AfterCallPromotionActivity.a(this, paramc, paramPromotionType, paramHistoryEvent);
  }
  
  public final void a(i parami, boolean paramBoolean)
  {
    com.truecaller.callerid.b.c localc = c;
    if ((localc == null) && (paramBoolean))
    {
      localObject = new com/truecaller/callerid/b/b;
      ((com.truecaller.callerid.b.b)localObject).<init>(this, this);
      boolean bool = ((com.truecaller.callerid.b.c)localObject).d();
      if (bool)
      {
        c = ((com.truecaller.callerid.b.c)localObject);
        localObject = (n)a.a();
        ((n)localObject).a(parami);
      }
    }
    Object localObject = c;
    if (localObject != null)
    {
      ((n)a.a()).b(parami);
      localObject = c;
      ((com.truecaller.callerid.b.c)localObject).a(parami);
    }
  }
  
  public final void a(HistoryEvent paramHistoryEvent, int paramInt)
  {
    AfterCallActivity.b(this, paramHistoryEvent, paramInt);
  }
  
  public final void b()
  {
    com.truecaller.callerid.b.c localc = c;
    if (localc != null) {
      localc.h();
    }
  }
  
  public final void c()
  {
    o.a(this, "com.truecaller.EVENT_AFTER_CALL_START");
  }
  
  public final w d()
  {
    com.truecaller.callerid.b.c localc = c;
    if (localc != null)
    {
      bool = d;
      if (bool)
      {
        bool = true;
        break label27;
      }
    }
    boolean bool = false;
    localc = null;
    label27:
    return w.b(Boolean.valueOf(bool));
  }
  
  public final void e()
  {
    c = null;
    ((n)a.a()).a();
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    paramConfiguration = c;
    if (paramConfiguration != null) {
      paramConfiguration.i();
    }
  }
  
  public void onCreate()
  {
    super.onCreate();
    Object localObject = ((bk)getApplicationContext()).a();
    boolean bool = ((bp)localObject).D().b("hasNativeDialerCallerId") ^ true;
    localObject = ((bp)localObject).m().a().a(k.class, this);
    bp localbp = ((TrueApp)getApplication()).a();
    q localq = new com/truecaller/callerid/q;
    localq.<init>(bool, (f)localObject);
    localbp.a(localq).a(this);
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    paramInt1 = 1;
    if (paramIntent != null)
    {
      Object localObject1 = "CALL_STATE";
      int i = -1;
      paramInt2 = paramIntent.getIntExtra((String)localObject1, i);
      n localn = null;
      boolean bool;
      if (paramInt2 != i)
      {
        bool = true;
      }
      else
      {
        bool = false;
        str1 = null;
      }
      Object localObject2 = new String[0];
      AssertionUtil.AlwaysFatal.isTrue(bool, (String[])localObject2);
      String str1 = paramIntent.getStringExtra("NUMBER");
      i = paramIntent.getIntExtra("SIM_SLOT_INDEX", i);
      localObject2 = "ACTION";
      int j = paramIntent.getIntExtra((String)localObject2, 0);
      localn = (n)a.a();
      localn.a(paramInt2, str1, i, j);
      j = Build.VERSION.SDK_INT;
      paramInt2 = 26;
      if (j >= paramInt2)
      {
        j = 2131362409;
        localObject1 = new android/app/Notification$Builder;
        String str2 = b.d();
        ((Notification.Builder)localObject1).<init>(this, str2);
        localObject1 = ((Notification.Builder)localObject1).setSmallIcon(2131234787);
        str2 = getString(2131886350);
        localObject1 = ((Notification.Builder)localObject1).setContentTitle(str2);
        i = android.support.v4.content.b.c(this, 2131100594);
        localObject1 = ((Notification.Builder)localObject1).setColor(i).build();
        startForeground(j, (Notification)localObject1);
      }
    }
    return paramInt1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.CallerIdService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */