package com.truecaller.callerid;

import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d
{
  private final q a;
  private final Provider b;
  
  private ab(q paramq, Provider paramProvider)
  {
    a = paramq;
    b = paramProvider;
  }
  
  public static ab a(q paramq, Provider paramProvider)
  {
    ab localab = new com/truecaller/callerid/ab;
    localab.<init>(paramq, paramProvider);
    return localab;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */