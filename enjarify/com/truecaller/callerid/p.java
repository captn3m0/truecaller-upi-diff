package com.truecaller.callerid;

import android.text.TextUtils;
import com.google.common.base.Strings;
import com.truecaller.ads.j.a;
import com.truecaller.aftercall.PromotionType;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.w;
import com.truecaller.calling.dialer.v;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.common.account.r;
import com.truecaller.common.h.ab;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.k;
import com.truecaller.tracking.events.bc;
import com.truecaller.tracking.events.bc.a;
import com.truecaller.tracking.events.g.a;
import com.truecaller.tracking.events.i.a;
import com.truecaller.util.ce;
import com.truecaller.util.u;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

final class p
  implements n
{
  private final com.truecaller.service.e A;
  private final com.truecaller.data.entity.g B;
  private al C;
  private final boolean D;
  private boolean E = true;
  private final com.truecaller.ads.provider.f F;
  private final com.truecaller.androidactors.f G;
  private final com.truecaller.androidactors.i H;
  private final CallRecordingManager I;
  private com.truecaller.androidactors.a J = null;
  private com.truecaller.androidactors.f K;
  private com.truecaller.utils.l L;
  private com.truecaller.featuretoggles.e M;
  private com.truecaller.b.c N;
  private com.truecaller.androidactors.f O;
  private com.truecaller.utils.a P;
  private com.truecaller.search.b Q;
  private i.a R;
  private v S;
  private long T;
  i a;
  private final com.truecaller.androidactors.f b;
  private final com.truecaller.androidactors.i c;
  private final com.truecaller.utils.d d;
  private final com.truecaller.util.al e;
  private final a f;
  private final c g;
  private final com.truecaller.network.search.l h;
  private final com.truecaller.tcpermissions.l i;
  private final com.truecaller.common.h.aj j;
  private final com.truecaller.androidactors.f k;
  private final u l;
  private final com.truecaller.data.access.c m;
  private final com.truecaller.androidactors.f n;
  private final com.truecaller.androidactors.f o;
  private final com.truecaller.androidactors.f p;
  private final com.truecaller.androidactors.f q;
  private final com.truecaller.androidactors.f r;
  private final aj s;
  private final FilterManager t;
  private final com.truecaller.analytics.b u;
  private final com.truecaller.common.g.a v;
  private final com.truecaller.i.c w;
  private final com.truecaller.aftercall.a x;
  private final ag y;
  private final r z;
  
  p(boolean paramBoolean, com.truecaller.androidactors.f paramf1, com.truecaller.androidactors.i parami1, com.truecaller.utils.d paramd, com.truecaller.util.al paramal, a parama, c paramc, com.truecaller.network.search.l paraml, com.truecaller.common.h.aj paramaj, com.truecaller.androidactors.f paramf2, u paramu, com.truecaller.data.access.c paramc1, com.truecaller.androidactors.f paramf3, com.truecaller.androidactors.f paramf4, com.truecaller.androidactors.f paramf5, com.truecaller.androidactors.f paramf6, com.truecaller.androidactors.f paramf7, aj paramaj1, FilterManager paramFilterManager, com.truecaller.analytics.b paramb, com.truecaller.common.g.a parama1, com.truecaller.i.c paramc2, com.truecaller.aftercall.a parama2, ag paramag, r paramr, com.truecaller.service.e parame, com.truecaller.data.entity.g paramg, al paramal1, com.truecaller.ads.provider.f paramf, com.truecaller.androidactors.f paramf8, com.truecaller.androidactors.i parami2, CallRecordingManager paramCallRecordingManager, com.truecaller.androidactors.f paramf9, com.truecaller.tcpermissions.l paraml1, com.truecaller.utils.l paraml2, com.truecaller.featuretoggles.e parame1, com.truecaller.b.c paramc3, com.truecaller.utils.a parama3, com.truecaller.search.b paramb1, v paramv, com.truecaller.androidactors.f paramf10)
  {
    Object localObject = com.truecaller.tracking.events.i.b();
    R = ((i.a)localObject);
    D = paramBoolean;
    localObject = paramf1;
    b = paramf1;
    localObject = parami1;
    c = parami1;
    localObject = paramd;
    d = paramd;
    localObject = paramal;
    e = paramal;
    localObject = parama;
    f = parama;
    localObject = paramc;
    g = paramc;
    localObject = paraml;
    h = paraml;
    localObject = paramaj;
    j = paramaj;
    localObject = paramf2;
    k = paramf2;
    localObject = paramu;
    l = paramu;
    localObject = paramc1;
    m = paramc1;
    localObject = paramf4;
    o = paramf4;
    localObject = paramf5;
    p = paramf5;
    localObject = paramf3;
    n = paramf3;
    localObject = paramf6;
    q = paramf6;
    localObject = paramaj1;
    s = paramaj1;
    localObject = paramFilterManager;
    t = paramFilterManager;
    localObject = paramb;
    u = paramb;
    localObject = parama1;
    v = parama1;
    localObject = paramc2;
    w = paramc2;
    localObject = parama2;
    x = parama2;
    localObject = paramag;
    y = paramag;
    localObject = paramr;
    z = paramr;
    localObject = parame;
    A = parame;
    localObject = paramg;
    B = paramg;
    localObject = paramal1;
    C = paramal1;
    localObject = paramf7;
    r = paramf7;
    localObject = paramf;
    F = paramf;
    localObject = paramf8;
    G = paramf8;
    localObject = parami2;
    H = parami2;
    localObject = paramCallRecordingManager;
    I = paramCallRecordingManager;
    localObject = paramf9;
    K = paramf9;
    localObject = paraml1;
    i = paraml1;
    localObject = paraml2;
    L = paraml2;
    localObject = parame1;
    M = parame1;
    localObject = paramc3;
    N = paramc3;
    localObject = paramf10;
    O = paramf10;
    localObject = parama3;
    P = parama3;
    localObject = paramb1;
    Q = paramb1;
    localObject = paramv;
    S = paramv;
  }
  
  private Contact a(Number paramNumber, com.truecaller.filters.g paramg, long paramLong)
  {
    Object localObject = m;
    String str = paramNumber.a();
    localObject = ((com.truecaller.data.access.c)localObject).b(str);
    if (localObject != null) {
      return (Contact)localObject;
    }
    int i1 = m;
    paramNumber.a(i1);
    localObject = new com/truecaller/data/entity/Contact;
    ((Contact)localObject).<init>();
    paramg = k;
    ((Contact)localObject).l(paramg);
    ((Contact)localObject).a(paramNumber);
    ((Contact)localObject).a(paramLong);
    d = true;
    return (Contact)localObject;
  }
  
  private static List a(Contact paramContact)
  {
    paramContact = paramContact.J();
    boolean bool1 = paramContact.isEmpty();
    if (!bool1)
    {
      localArrayList = new java/util/ArrayList;
      int i1 = paramContact.size();
      localArrayList.<init>(i1);
      paramContact = paramContact.iterator();
      for (;;)
      {
        boolean bool2 = paramContact.hasNext();
        if (!bool2) {
          break;
        }
        String str = ((Tag)paramContact.next()).a();
        boolean bool3 = TextUtils.isEmpty(str);
        if (!bool3) {
          localArrayList.add(str);
        }
      }
    }
    bool1 = false;
    ArrayList localArrayList = null;
    return localArrayList;
  }
  
  private void a(android.support.v4.f.j paramj)
  {
    if (paramj != null)
    {
      Object localObject1 = Boolean.TRUE;
      Object localObject2 = a;
      boolean bool1 = ((Boolean)localObject1).equals(localObject2);
      if (bool1)
      {
        paramj = (String)b;
        localObject1 = "initiated";
        boolean bool2 = ((String)localObject1).equals(paramj);
        if (bool2) {
          paramj = "CALLERID_Push_Sent";
        } else {
          paramj = "AFTERCALL_Push_Sent";
        }
        localObject1 = u;
        localObject2 = new com/truecaller/analytics/e$a;
        ((com.truecaller.analytics.e.a)localObject2).<init>(paramj);
        paramj = ((com.truecaller.analytics.e.a)localObject2).a();
        ((com.truecaller.analytics.b)localObject1).a(paramj);
      }
    }
  }
  
  private void a(i parami, boolean paramBoolean)
  {
    boolean bool = D;
    if (bool)
    {
      com.truecaller.utils.l locall = L;
      bool = locall.a();
      if (bool)
      {
        ((k)b.a()).a(parami, paramBoolean);
        return;
      }
      parami = (e)o.a();
      parami.b();
    }
  }
  
  private void a(Contact paramContact, HistoryEvent paramHistoryEvent)
  {
    Object localObject = y;
    boolean bool = ((ag)localObject).a(paramHistoryEvent);
    if (bool)
    {
      localObject = v;
      String str = "featureCacheAdAfterCall";
      bool = ((com.truecaller.common.g.a)localObject).a(str, false);
      if (bool)
      {
        bool = f();
        if (bool) {
          b(paramContact, paramHistoryEvent);
        }
      }
    }
  }
  
  private void a(Boolean paramBoolean)
  {
    com.truecaller.calling.recorder.floatingbutton.e locale = (com.truecaller.calling.recorder.floatingbutton.e)O.a();
    if (paramBoolean != null)
    {
      bool = paramBoolean.booleanValue();
      if (!bool)
      {
        bool = true;
        break label35;
      }
    }
    boolean bool = false;
    paramBoolean = null;
    label35:
    locale.a(bool);
  }
  
  private boolean a(HistoryEvent paramHistoryEvent)
  {
    com.truecaller.utils.d locald = d;
    boolean bool = locald.e();
    if (bool)
    {
      int i2 = o;
      int i1 = 2;
      if (i2 == i1) {
        return false;
      }
    }
    return true;
  }
  
  private boolean a(Number paramNumber, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramNumber != null)
    {
      Object localObject1 = paramNumber.d();
      boolean bool1 = ab.a((String)localObject1);
      boolean bool2 = true;
      if (bool1)
      {
        localObject1 = paramNumber.a();
        bool1 = ab.a((String)localObject1);
        if (bool1)
        {
          paramNumber = paramNumber.c();
          bool3 = ab.a(paramNumber);
          if (bool3)
          {
            bool3 = true;
            break label70;
          }
        }
      }
      boolean bool3 = false;
      paramNumber = null;
      label70:
      if (!bool3)
      {
        paramNumber = w;
        localObject1 = "hasNativeDialerCallerId";
        bool3 = paramNumber.b((String)localObject1);
        if (!bool3)
        {
          bool3 = D;
          if (bool3)
          {
            if (!paramBoolean1) {
              return bool2;
            }
            Object localObject2;
            if (paramBoolean2)
            {
              paramNumber = M;
              localObject2 = o;
              localObject1 = com.truecaller.featuretoggles.e.a;
              int i1 = 42;
              localObject1 = localObject1[i1];
              paramNumber = ((com.truecaller.featuretoggles.e.a)localObject2).a(paramNumber, (c.l.g)localObject1);
              bool3 = paramNumber.a();
              if (bool3) {}
            }
            else
            {
              paramNumber = w;
              localObject2 = "enabledCallerIDforPB";
              bool3 = paramNumber.b((String)localObject2);
              if ((!bool3) || (!paramBoolean1)) {
                break label209;
              }
            }
            return bool2;
            label209:
            return false;
          }
        }
        return false;
      }
    }
    return false;
  }
  
  private void b()
  {
    i locali = a;
    if (locali != null)
    {
      int i1 = i;
      if (i1 == 0)
      {
        locali = a;
        boolean bool = j;
        if (!bool)
        {
          locali = a;
          int i2 = 3;
          i = i2;
          c();
        }
      }
    }
  }
  
  private void b(Contact paramContact, HistoryEvent paramHistoryEvent)
  {
    e();
    Object localObject1 = new com/truecaller/ads/j$a;
    ((j.a)localObject1).<init>("AFTERCALL");
    Object localObject2 = b;
    a = ((String)localObject2);
    int i1 = paramContact.I();
    b = i1;
    localObject2 = Integer.valueOf(paramHistoryEvent.g());
    d = ((Integer)localObject2);
    localObject2 = paramContact.t();
    e = ((String)localObject2);
    paramHistoryEvent = d;
    f = paramHistoryEvent;
    paramContact = a(paramContact);
    g = paramContact;
    paramContact = ((j.a)localObject1).a();
    paramHistoryEvent = ((com.truecaller.ads.provider.a.b)G.a()).a(paramContact);
    localObject1 = H;
    localObject2 = new com/truecaller/callerid/-$$Lambda$p$eNW91e7pFyz5PLvitLsXov_fQdI;
    ((-..Lambda.p.eNW91e7pFyz5PLvitLsXov_fQdI)localObject2).<init>(this, paramContact);
    paramContact = paramHistoryEvent.a((com.truecaller.androidactors.i)localObject1, (ac)localObject2);
    J = paramContact;
  }
  
  private void c()
  {
    Object localObject = a;
    int i1;
    if (localObject != null)
    {
      i1 = i;
      int i2 = 3;
      if (i1 != i2) {}
    }
    else
    {
      i1 = 0;
      E = false;
      localObject = (k)b.a();
      ((k)localObject).a();
    }
  }
  
  private void c(i parami)
  {
    Object localObject1 = e;
    boolean bool = ((com.truecaller.util.al)localObject1).a();
    if (!bool) {
      return;
    }
    int i1 = i;
    if (i1 == 0) {
      localObject1 = "initiated";
    } else {
      localObject1 = "ended";
    }
    Object localObject2 = (com.truecaller.network.util.c)p.a();
    parami = a;
    parami = ((com.truecaller.network.util.c)localObject2).a((String)localObject1, parami);
    localObject1 = c;
    localObject2 = new com/truecaller/callerid/-$$Lambda$p$A5bhd-4eFfyF2qtl_1WWQgiz3TQ;
    ((-..Lambda.p.A5bhd-4eFfyF2qtl_1WWQgiz3TQ)localObject2).<init>(this);
    parami.a((com.truecaller.androidactors.i)localObject1, (ac)localObject2);
  }
  
  private void d()
  {
    ((k)b.a()).b();
    Object localObject = I;
    boolean bool1 = ((CallRecordingManager)localObject).a();
    if (bool1)
    {
      localObject = (com.truecaller.calling.recorder.floatingbutton.e)O.a();
      boolean bool2 = true;
      ((com.truecaller.calling.recorder.floatingbutton.e)localObject).a(bool2);
    }
  }
  
  private void d(i parami)
  {
    Object localObject1 = l;
    Object localObject2 = l;
    int i1 = 0;
    com.truecaller.analytics.b localb = null;
    Object localObject3 = new String[0];
    AssertionUtil.isNotNull(localObject2, (String[])localObject3);
    localObject2 = a;
    localObject3 = new String[0];
    AssertionUtil.isNotNull(localObject2, (String[])localObject3);
    localObject2 = a;
    M.A().a();
    localObject3 = l;
    boolean bool1 = ((u)localObject3).a((Number)localObject2);
    Contact localContact = l;
    int i3 = 32;
    boolean bool3 = localContact.a(i3);
    bool3 = a((Number)localObject2, bool1, bool3);
    if (!bool3)
    {
      if (bool1)
      {
        o = "inPhonebook";
        return;
      }
      o = "notNumber";
      return;
    }
    localObject2 = ((Number)localObject2).a();
    boolean bool4 = ((Contact)localObject1).p((String)localObject2);
    int i2 = ((Contact)localObject1).getSource() & 0x40;
    bool3 = true;
    if (i2 != 0) {
      i1 = 1;
    }
    localObject3 = Q;
    long l1 = ((Contact)localObject1).H();
    int i4 = ((Contact)localObject1).getSource();
    boolean bool2 = ((com.truecaller.search.b)localObject3).a(l1, i4);
    if (((!bool4) && (i1 == 0)) || (bool2))
    {
      e(parami);
    }
    else
    {
      localObject2 = new com/truecaller/analytics/e$a;
      ((com.truecaller.analytics.e.a)localObject2).<init>("CallerIdSearch");
      localObject3 = "Cache";
      ((com.truecaller.analytics.e.a)localObject2).a("Result", (String)localObject3);
      localb = u;
      localObject2 = ((com.truecaller.analytics.e.a)localObject2).a();
      localb.a((com.truecaller.analytics.e)localObject2);
      o = "validCacheResult";
      localObject2 = j.b(parami);
      a((Contact)localObject1, (HistoryEvent)localObject2);
    }
    a(parami, bool3);
    boolean bool5 = D;
    long l2 = 0L;
    if (bool5)
    {
      localObject1 = a;
      l3 = c;
      l1 = c;
      bool5 = l3 < l1;
      if (bool5)
      {
        l3 = 6;
        break label363;
      }
      bool6 = e;
      if (!bool6)
      {
        l3 = 10;
        break label363;
      }
    }
    long l3 = l2;
    label363:
    boolean bool6 = l3 < l2;
    if (bool6)
    {
      parami = s;
      localObject1 = TimeUnit.SECONDS;
      localObject2 = new com/truecaller/callerid/-$$Lambda$p$BNVa7om4scb3DVa_276vzMAE7NU;
      ((-..Lambda.p.BNVa7om4scb3DVa_276vzMAE7NU)localObject2).<init>(this);
      parami.a(l3, (TimeUnit)localObject1, (Runnable)localObject2);
    }
  }
  
  private void e()
  {
    com.truecaller.androidactors.a locala = J;
    if (locala != null)
    {
      locala.a();
      locala = null;
      J = null;
    }
  }
  
  private void e(i parami)
  {
    boolean bool1 = E;
    if (bool1)
    {
      bool1 = D;
      if (bool1)
      {
        Object localObject1 = e;
        bool1 = ((com.truecaller.util.al)localObject1).a();
        if (bool1)
        {
          localObject1 = a;
          int i1 = parami.b();
          o = null;
          boolean bool2 = true;
          k = bool2;
          n = bool2;
          Object localObject2 = (ad)r.a();
          Object localObject3 = h;
          UUID localUUID = UUID.randomUUID();
          localObject3 = ((com.truecaller.network.search.l)localObject3).a(localUUID, "callerId");
          localObject1 = ((ad)localObject2).a((Number)localObject1, false, i1, (com.truecaller.network.search.j)localObject3);
          com.truecaller.androidactors.i locali = c;
          localObject2 = new com/truecaller/callerid/-$$Lambda$p$oMsaQ6Cu5SUCspybc1y_uPydpH4;
          ((-..Lambda.p.oMsaQ6Cu5SUCspybc1y_uPydpH4)localObject2).<init>(this, parami);
          ((w)localObject1).a(locali, (ac)localObject2);
          return;
        }
      }
    }
  }
  
  private boolean f()
  {
    Object localObject = M.J();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      localObject = d;
      bool = ((com.truecaller.utils.d)localObject).r();
      if (bool) {
        return false;
      }
    }
    return true;
  }
  
  public final void a()
  {
    E = false;
    e();
    c();
    Object localObject = I;
    boolean bool1 = ((CallRecordingManager)localObject).a();
    if (bool1)
    {
      localObject = (com.truecaller.calling.recorder.floatingbutton.e)O.a();
      bool2 = true;
      ((com.truecaller.calling.recorder.floatingbutton.e)localObject).a(bool2);
    }
    localObject = S;
    boolean bool2 = false;
    com.truecaller.tracking.events.i locali = null;
    ((v)localObject).a(null);
    localObject = R;
    bool1 = ((i.a)localObject).a();
    if (bool1)
    {
      localObject = R;
      bool1 = ((i.a)localObject).b();
      if (!bool1)
      {
        localObject = R;
        long l1 = -1;
        ((i.a)localObject).b(l1);
      }
      localObject = (ae)q.a();
      locali = R.c();
      ((ae)localObject).a(locali);
    }
  }
  
  public final void a(int paramInt1, String paramString, int paramInt2, int paramInt3)
  {
    p localp = this;
    Object localObject1 = paramString;
    int i1 = paramInt3;
    Object localObject4 = e;
    boolean bool1 = ((com.truecaller.util.al)localObject4).a();
    boolean bool5 = false;
    Object localObject5 = null;
    int i5 = 1;
    if (!bool1)
    {
      localObject4 = e;
      bool1 = ((com.truecaller.util.al)localObject4).b();
      if (bool1) {}
    }
    else
    {
      localObject4 = j;
      bool1 = ((com.truecaller.common.h.aj)localObject4).a((String)localObject1);
      if (!bool1)
      {
        localObject4 = i;
        bool1 = ((com.truecaller.tcpermissions.l)localObject4).f();
        if (bool1)
        {
          bool1 = true;
          break label115;
        }
      }
    }
    bool1 = false;
    localObject4 = null;
    label115:
    if (!bool1)
    {
      c();
      return;
    }
    int i6 = 3;
    Object localObject6;
    Object localObject7;
    Object localObject8;
    label641:
    label667:
    Object localObject9;
    Object localObject10;
    int i16;
    int i15;
    label1792:
    Object localObject11;
    boolean bool14;
    label1945:
    Contact localContact1;
    long l3;
    int i8;
    int i10;
    long l5;
    Object localObject2;
    int i14;
    int i18;
    Object localObject13;
    Object localObject14;
    int i11;
    label3229:
    int i12;
    boolean bool4;
    switch (paramInt1)
    {
    default: 
      break;
    case 3: 
      localObject1 = a;
      if (localObject1 != null)
      {
        int i7 = i;
        if (i7 != i6)
        {
          d();
          a.i = i6;
          ((com.truecaller.h.c)k.a()).b().c();
          localObject1 = j.b(a);
          localObject4 = I;
          bool1 = ((CallRecordingManager)localObject4).j();
          boolean bool10;
          if (!bool1)
          {
            localObject4 = I;
            bool1 = ((CallRecordingManager)localObject4).a();
            if (bool1)
            {
              localObject4 = I;
              bool1 = ((CallRecordingManager)localObject4).f();
              if (bool1)
              {
                localObject4 = I;
                ((CallRecordingManager)localObject4).e();
              }
              localObject4 = (com.truecaller.calling.recorder.floatingbutton.e)O.a();
              ((com.truecaller.calling.recorder.floatingbutton.e)localObject4).a();
              bool1 = localp.a((HistoryEvent)localObject1);
              if (bool1)
              {
                localObject4 = I.b();
                bool7 = org.c.a.a.a.k.b((CharSequence)localObject4);
                if (!bool7)
                {
                  localObject6 = a;
                  if (localObject6 != null)
                  {
                    localObject6 = I;
                    bool7 = ((CallRecordingManager)localObject6).i();
                    if (bool7)
                    {
                      localObject4 = "maybeSaveCallRecording:: Short recording ignore";
                      new String[1][0] = localObject4;
                      break label667;
                    }
                  }
                  localObject6 = new com/truecaller/data/entity/CallRecording;
                  long l1 = -1;
                  localObject7 = a;
                  ((CallRecording)localObject6).<init>(l1, (String)localObject7, (String)localObject4);
                  m = ((CallRecording)localObject6);
                  ((com.truecaller.callhistory.a)K.a()).a((CallRecording)localObject6);
                  I.h();
                  localObject4 = new com/truecaller/analytics/e$a;
                  ((com.truecaller.analytics.e.a)localObject4).<init>("CallRecorded");
                  localObject6 = "CallType";
                  int i13 = o;
                  switch (i13)
                  {
                  default: 
                    localObject8 = "Unknown";
                    break;
                  case 3: 
                    localObject8 = "Missed";
                    break;
                  case 2: 
                    localObject8 = "Outgoing";
                    break;
                  case 1: 
                    localObject8 = "Incoming";
                  }
                  ((com.truecaller.analytics.e.a)localObject4).a((String)localObject6, (String)localObject8);
                  localObject6 = "NumberType";
                  localObject8 = f;
                  if (localObject8 != null)
                  {
                    bool10 = ((Contact)localObject8).U();
                    if (bool10)
                    {
                      localObject8 = "Spam";
                      break label641;
                    }
                    bool9 = ((Contact)localObject8).Z();
                    if (bool9)
                    {
                      localObject8 = "PhoneBook";
                      break label641;
                    }
                  }
                  localObject8 = "Unknown";
                  ((com.truecaller.analytics.e.a)localObject4).a((String)localObject6, (String)localObject8);
                  localObject6 = I;
                  ((CallRecordingManager)localObject6).a((com.truecaller.analytics.e.a)localObject4);
                }
              }
            }
          }
          bool1 = localp.a((HistoryEvent)localObject1);
          if (bool1)
          {
            localObject4 = (com.truecaller.util.aa)n.a();
            ((com.truecaller.util.aa)localObject4).a((HistoryEvent)localObject1);
          }
          localObject4 = a;
          boolean bool7 = ((i)localObject4).c();
          if (bool7)
          {
            localObject6 = w;
            localObject8 = "blockCallCounter";
            ((com.truecaller.i.c)localObject6).a_((String)localObject8);
            localObject6 = g;
            ((c)localObject6).a((i)localObject4);
          }
          localObject4 = f;
          localObject6 = a;
          ((a)localObject4).a((i)localObject6);
          localObject4 = a;
          bool1 = e;
          if (!bool1)
          {
            localObject4 = a;
            localp.c((i)localObject4);
          }
          localObject4 = a;
          bool1 = f;
          bool7 = f();
          if (bool7)
          {
            localObject6 = a;
            if (localObject6 != null)
            {
              bool7 = true;
            }
            else
            {
              bool7 = false;
              localObject6 = null;
            }
            localObject8 = new String[0];
            AssertionUtil.isTrue(bool7, (String[])localObject8);
            localObject6 = x;
            localObject8 = a;
            localObject6 = ((com.truecaller.aftercall.a)localObject6).a((i)localObject8, (HistoryEvent)localObject1);
            if (localObject6 != null)
            {
              localObject4 = (k)b.a();
              localObject8 = w;
              ((k)localObject4).a((PromotionType)localObject6, (HistoryEvent)localObject1, (com.truecaller.i.c)localObject8);
            }
            else
            {
              localObject6 = y;
              bool7 = ((ag)localObject6).b((HistoryEvent)localObject1);
              if (!bool7)
              {
                localObject6 = y;
                bool7 = ((ag)localObject6).a((HistoryEvent)localObject1);
                if ((!bool7) || (bool1)) {}
              }
              else
              {
                localObject4 = (k)b.a();
                ((k)localObject4).a((HistoryEvent)localObject1, 0);
              }
            }
          }
          localObject4 = a;
          localObject6 = l;
          localObject8 = a;
          bool7 = ((u)localObject6).a((Number)localObject8);
          boolean bool9 = D;
          if (bool9)
          {
            localObject8 = new com/truecaller/analytics/e$a;
            ((com.truecaller.analytics.e.a)localObject8).<init>("Call");
            localObject9 = "Direction";
            boolean bool11 = e;
            if (bool11) {
              localObject7 = "Incoming";
            } else {
              localObject7 = "Outgoing";
            }
            ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
            bool10 = e;
            if (bool10)
            {
              bool10 = ((i)localObject4).c();
              if (bool10)
              {
                localObject9 = "Blocked";
              }
              else
              {
                bool10 = j;
                if (bool10) {
                  localObject9 = "Answered";
                } else {
                  localObject9 = "NotAnswered";
                }
              }
              localObject7 = "Action";
              ((com.truecaller.analytics.e.a)localObject8).a((String)localObject7, (String)localObject9);
            }
            else
            {
              bool10 = f;
              if (bool10)
              {
                localObject9 = w;
                localObject7 = "key_last_call_origin";
                localObject10 = "";
                localObject9 = ((com.truecaller.i.c)localObject9).b((String)localObject7, (String)localObject10);
              }
              else
              {
                localObject9 = "outsideTC";
              }
              bool11 = Strings.isNullOrEmpty((String)localObject9);
              localObject10 = new String[] { "Call event analytics context shouldn't be empty" };
              AssertionUtil.isFalse(bool11, (String[])localObject10);
              localObject7 = "Context";
              ((com.truecaller.analytics.e.a)localObject8).a((String)localObject7, (String)localObject9);
            }
            localObject9 = t;
            localObject7 = a.d();
            localObject9 = ((FilterManager)localObject9).b((String)localObject7);
            localObject7 = p.1.b;
            localObject10 = j;
            int i17 = ((FilterManager.ActionSource)localObject10).ordinal();
            i16 = localObject7[i17];
            switch (i16)
            {
            default: 
              break;
            case 7: 
              localObject7 = p.1.a;
              localObject9 = n;
              i15 = ((TruecallerContract.Filters.WildCardType)localObject9).ordinal();
              i15 = localObject7[i15];
              switch (i15)
              {
              default: 
                break;
              case 4: 
                localObject9 = "BlockingSource";
                localObject7 = "FilterExplicit";
                ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
                break;
              case 3: 
                localObject9 = "BlockingSource";
                localObject7 = "FilterEnd";
                ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
                break;
              case 2: 
                localObject9 = "BlockingSource";
                localObject7 = "FilterStart";
                ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
                break;
              case 1: 
                localObject9 = "BlockingSource";
                localObject7 = "FilterContains";
                ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
              }
              break;
            case 6: 
              localObject9 = "BlockingSource";
              localObject7 = "IndianRegisteredTelemarketer";
              ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
              break;
            case 5: 
              localObject9 = "BlockingSource";
              localObject7 = "NeighbourSpoofing";
              ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
              break;
            case 4: 
              localObject9 = "BlockingSource";
              localObject7 = "HiddenNumber";
              ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
              break;
            case 3: 
              localObject9 = "BlockingSource";
              localObject7 = "ForeignNumber";
              ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
              break;
            case 2: 
              localObject9 = "BlockingSource";
              localObject7 = "NonPhonebookContact";
              ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
              break;
            case 1: 
              localObject9 = "BlockingSource";
              localObject7 = "TopSpammer";
              ((com.truecaller.analytics.e.a)localObject8).a((String)localObject9, (String)localObject7);
            }
            if (bool7) {
              localObject9 = "Phonebook";
            } else {
              localObject9 = "Unknown";
            }
            localObject7 = l;
            if (localObject7 != null)
            {
              localObject10 = "Unknown";
              bool12 = ((String)localObject9).equals(localObject10);
              if (bool12)
              {
                localObject7 = ((Contact)localObject7).A().iterator();
                do
                {
                  bool12 = ((Iterator)localObject7).hasNext();
                  if (!bool12) {
                    break;
                  }
                  localObject10 = ((Number)((Iterator)localObject7).next()).a();
                  bool12 = Strings.isNullOrEmpty((String)localObject10);
                } while (bool12);
                i16 = 1;
                break label1792;
                i16 = 0;
                localObject7 = null;
                if (i16 == 0) {
                  localObject9 = "Hidden";
                }
              }
            }
            localObject7 = "Participant";
            ((com.truecaller.analytics.e.a)localObject8).a((String)localObject7, (String)localObject9);
            localObject9 = u;
            localObject8 = ((com.truecaller.analytics.e.a)localObject8).a();
            ((com.truecaller.analytics.b)localObject9).a((com.truecaller.analytics.e)localObject8);
          }
          localObject8 = w;
          localObject9 = "key_last_call_origin";
          ((com.truecaller.i.c)localObject8).d((String)localObject9);
          boolean bool6 = y.a((HistoryEvent)localObject1);
          localObject8 = t;
          long l2 = T;
          localObject10 = a;
          localObject11 = l;
          if (localObject11 != null)
          {
            localObject11 = l;
            int i19 = 32;
            bool14 = ((Contact)localObject11).a(i19);
            if (bool14)
            {
              bool14 = true;
              break label1945;
            }
          }
          bool14 = false;
          localObject11 = null;
          boolean bool12 = localp.a((Number)localObject10, bool7, bool14);
          localObject11 = com.truecaller.tracking.events.e.b();
          Object localObject12 = org.c.a.a.a.k.n(a.a());
          boolean bool15 = false;
          localContact1 = null;
          localObject12 = com.truecaller.common.h.aa.c((String)localObject12, null);
          ((com.truecaller.tracking.events.e.a)localObject11).a((CharSequence)localObject12);
          boolean bool13 = e;
          if (bool13) {
            localObject12 = "incoming";
          } else {
            localObject12 = "outgoing";
          }
          ((com.truecaller.tracking.events.e.a)localObject11).c((CharSequence)localObject12);
          ((com.truecaller.tracking.events.e.a)localObject11).g(null);
          ((com.truecaller.tracking.events.e.a)localObject11).a(null);
          localObject12 = a.l();
          bool15 = TextUtils.isEmpty((CharSequence)localObject12);
          if (bool15) {
            localObject12 = "unknown";
          }
          ((com.truecaller.tracking.events.e.a)localObject11).b((CharSequence)localObject12);
          bool13 = e;
          if (bool13)
          {
            bool13 = j;
            if (bool13) {
              localObject12 = "answered";
            } else {
              localObject12 = "notAnswered";
            }
            ((com.truecaller.tracking.events.e.a)localObject11).d((CharSequence)localObject12);
          }
          else
          {
            localObject12 = "unknown";
            ((com.truecaller.tracking.events.e.a)localObject11).d((CharSequence)localObject12);
          }
          ((com.truecaller.tracking.events.e.a)localObject11).a(bool6);
          l3 = System.currentTimeMillis();
          long l4 = d;
          l3 -= l4;
          ((com.truecaller.tracking.events.e.a)localObject11).a(l3);
          if (bool7) {
            localObject1 = "fromPhonebook";
          } else {
            localObject1 = "fromServer";
          }
          localObject6 = a.d();
          localObject6 = ((FilterManager)localObject8).a((String)localObject6);
          localObject8 = j.1.a;
          localObject6 = j;
          int i9 = ((FilterManager.ActionSource)localObject6).ordinal();
          i9 = localObject8[i9];
          switch (i9)
          {
          default: 
            localObject6 = l;
            if (localObject6 != null)
            {
              localObject6 = l;
              bool8 = ((Contact)localObject6).P();
              if (!bool8)
              {
                localObject1 = "noHit";
                bool8 = false;
                localObject6 = null;
              }
            }
            break;
          case 3: 
            localObject1 = "fromUserSpammerList";
            bool8 = true;
            break;
          case 2: 
            localObject1 = "fromUserWhiteList";
            bool8 = false;
            localObject6 = null;
            break;
          case 1: 
            localObject1 = "fromTopSpammerList";
            bool8 = true;
            break;
          }
          boolean bool8 = false;
          localObject6 = null;
          ((com.truecaller.tracking.events.e.a)localObject11).e((CharSequence)localObject1);
          if (!bool8)
          {
            bool6 = ((i)localObject4).a();
            if (!bool6) {}
          }
          else
          {
            bool5 = true;
          }
          ((com.truecaller.tracking.events.e.a)localObject11).b(bool5);
          i8 = h;
          if (i8 == i5)
          {
            localObject1 = "autoBlock";
            ((com.truecaller.tracking.events.e.a)localObject11).f((CharSequence)localObject1);
          }
          else
          {
            i8 = h;
            i10 = 3;
            if (i8 == i10)
            {
              localObject1 = "silentRing";
              ((com.truecaller.tracking.events.e.a)localObject11).f((CharSequence)localObject1);
            }
            else
            {
              localObject1 = "none";
              ((com.truecaller.tracking.events.e.a)localObject11).f((CharSequence)localObject1);
            }
          }
          ((com.truecaller.tracking.events.e.a)localObject11).b(l2);
          localObject1 = l;
          if (localObject1 != null)
          {
            localObject1 = bc.b();
            localObject6 = new java/util/ArrayList;
            ((ArrayList)localObject6).<init>();
            localObject8 = new java/util/ArrayList;
            ((ArrayList)localObject8).<init>();
            localObject9 = l.J();
            localObject7 = ((List)localObject9).iterator();
            for (;;)
            {
              bool13 = ((Iterator)localObject7).hasNext();
              if (!bool13) {
                break;
              }
              localObject12 = (Tag)((Iterator)localObject7).next();
              int i20 = ((Tag)localObject12).getSource() & 0x10;
              if (i20 != 0)
              {
                localObject12 = ((Tag)localObject12).a();
                ((List)localObject6).add(localObject12);
              }
              else
              {
                localObject12 = ((Tag)localObject12).a();
                ((List)localObject8).add(localObject12);
              }
            }
            ((bc.a)localObject1).a((List)localObject8);
            ((bc.a)localObject1).b((List)localObject6);
            if (bool12)
            {
              localObject6 = ce.a((List)localObject9);
              if (localObject6 != null)
              {
                localObject8 = new java/util/ArrayList;
                ((ArrayList)localObject8).<init>();
                l2 = a;
                localObject6 = String.valueOf(l2);
                ((List)localObject8).add(localObject6);
                ((bc.a)localObject1).c((List)localObject8);
              }
            }
            localObject1 = ((bc.a)localObject1).a();
            ((com.truecaller.tracking.events.e.a)localObject11).a((bc)localObject1);
          }
          localObject1 = g;
          ((com.truecaller.tracking.events.e.a)localObject11).g((CharSequence)localObject1);
          localObject1 = ((com.truecaller.tracking.events.e.a)localObject11).a();
          try
          {
            localObject6 = q;
            localObject6 = ((com.truecaller.androidactors.f)localObject6).a();
            localObject6 = (ae)localObject6;
            ((ae)localObject6).a((org.apache.a.d.d)localObject1);
          }
          catch (org.apache.a.a locala)
          {
            AssertionUtil.reportThrowableButNeverCrash(locala);
          }
          l5 = 0L;
          T = l5;
          localObject2 = j.a((i)localObject4);
          if (localObject2 != null)
          {
            localObject4 = (ae)q.a();
            ((ae)localObject4).a((org.apache.a.d.d)localObject2);
          }
          c();
          localObject2 = (e)o.a();
          ((e)localObject2).e();
          break;
        }
      }
      c();
      return;
    case 2: 
      localObject4 = a;
      if (localObject4 != null)
      {
        int i2 = i;
        i6 = 3;
        if (i2 != i6)
        {
          localObject4 = a;
          j = i5;
        }
        localObject4 = a;
        i2 = i;
        if (i2 == i5)
        {
          localObject4 = a;
          i10 = 2;
          i = i10;
          localObject4 = d;
          boolean bool2 = ((com.truecaller.utils.d)localObject4).e();
          if (bool2)
          {
            localObject4 = new com/truecaller/callerid/i;
            i14 = a.i;
            i15 = a.h;
            localObject7 = a.a;
            i18 = a.b;
            bool14 = a.f;
            l3 = P.a();
            localObject13 = a.l;
            localObject14 = a.g;
            com.truecaller.filters.g localg = a.m;
            localObject6 = localObject4;
            ((i)localObject4).<init>(i14, i15, (Number)localObject7, i18, bool14, l3, (Contact)localObject13, (String)localObject14, localg);
            a = ((i)localObject4);
            localObject4 = a;
            j = i5;
          }
          d();
        }
        localObject4 = a.a.a();
        localObject6 = I;
        i11 = ((CallRecordingManager)localObject6).a();
        if (i11 != 0)
        {
          localObject6 = I;
          i11 = ((CallRecordingManager)localObject6).d();
          if (i11 != 0)
          {
            localObject6 = (com.truecaller.calling.recorder.floatingbutton.e)O.a();
            localObject8 = new com/truecaller/callerid/-$$Lambda$p$rzcMCX1b61meQ6Afzht3FWjGYOM;
            ((-..Lambda.p.rzcMCX1b61meQ6Afzht3FWjGYOM)localObject8).<init>(localp, (String)localObject4);
            ((com.truecaller.calling.recorder.floatingbutton.e)localObject6).a((String)localObject4, (com.truecaller.calling.recorder.floatingbutton.i)localObject8);
          }
        }
      }
      localObject4 = "#";
      if (localObject2 != null)
      {
        i3 = org.c.a.a.a.c.a((CharSequence)localObject2, (CharSequence)localObject4, 0);
        if (i3 >= 0)
        {
          i3 = 1;
          break label3229;
        }
      }
      int i3 = 0;
      localObject4 = null;
      if (i3 != 0)
      {
        localObject4 = I;
        boolean bool3 = ((CallRecordingManager)localObject4).a();
        if (bool3)
        {
          localObject4 = B;
          localObject6 = new String[i5];
          localObject6[0] = localObject2;
          localObject4 = ((com.truecaller.data.entity.g)localObject4).b((String[])localObject6);
          localObject6 = (com.truecaller.calling.recorder.floatingbutton.e)O.a();
          localObject8 = new com/truecaller/callerid/-$$Lambda$p$04RAVmm4LF92OwVGrr8mIrWQ-sg;
          ((-..Lambda.p.04RAVmm4LF92OwVGrr8mIrWQ-sg)localObject8).<init>(localp, (Number)localObject4, (String)localObject2);
          ((com.truecaller.calling.recorder.floatingbutton.e)localObject6).a((String)localObject2, (com.truecaller.calling.recorder.floatingbutton.i)localObject8);
        }
      }
      return;
    case 1: 
      localObject4 = B;
      localObject6 = new String[i5];
      localObject6[0] = localObject2;
      localObject13 = ((com.truecaller.data.entity.g)localObject4).b((String[])localObject6);
      localObject2 = t.a((String)localObject2);
      long l6 = P.a();
      localContact1 = localp.a((Number)localObject13, (com.truecaller.filters.g)localObject2, l6);
      localObject11 = new com/truecaller/callerid/i;
      i11 = 1;
      i18 = 0;
      localObject10 = null;
      long l7 = P.a();
      String str1 = ai.b();
      localObject4 = localObject11;
      i14 = paramInt3;
      localObject9 = localObject13;
      i16 = paramInt2;
      localObject5 = localObject11;
      Object localObject15 = localObject13;
      localObject13 = str1;
      i5 = 3;
      localObject14 = localObject2;
      ((i)localObject11).<init>(i11, paramInt3, (Number)localObject9, paramInt2, false, l7, localContact1, str1, (com.truecaller.filters.g)localObject2);
      localObject2 = a;
      int i4 = 12785645;
      if (localObject2 != null)
      {
        i8 = i;
        if (i8 != i5)
        {
          i8 = 1;
          if ((i1 != i8) && (i1 != i4))
          {
            localp.d((i)localObject11);
            return;
          }
          return;
        }
      }
      a = ((i)localObject5);
      localObject2 = (k)b.a();
      ((k)localObject2).c();
      i8 = 1;
      if ((i1 != i8) && (i1 != i4))
      {
        localObject2 = w;
        i12 = 0;
        localObject6 = null;
        ((com.truecaller.i.c)localObject2).b("key_temp_latest_call_made_with_tc", false);
        localObject2 = a;
        localp.d((i)localObject2);
        localObject2 = ((Number)localObject15).a();
        localObject4 = I;
        bool4 = ((CallRecordingManager)localObject4).a();
        if (bool4)
        {
          localObject4 = I;
          bool4 = ((CallRecordingManager)localObject4).d();
          if (!bool4)
          {
            localObject4 = (com.truecaller.calling.recorder.floatingbutton.e)O.a();
            localObject6 = new com/truecaller/callerid/-$$Lambda$p$Ej25DuGJuBIc1bBF5YDwp2vv3PA;
            ((-..Lambda.p.Ej25DuGJuBIc1bBF5YDwp2vv3PA)localObject6).<init>(localp);
            ((com.truecaller.calling.recorder.floatingbutton.e)localObject4).a((String)localObject2, (com.truecaller.calling.recorder.floatingbutton.i)localObject6);
          }
        }
        return;
      }
      a.o = "inSpammerList";
      return;
    case 0: 
      i5 = 3;
      localObject4 = B;
      i12 = 1;
      localObject6 = new String[i12];
      i14 = 0;
      localObject6[0] = localObject2;
      localObject4 = ((com.truecaller.data.entity.g)localObject4).b((String[])localObject6);
      localObject2 = t.a((String)localObject2);
      l5 = P.a();
      Contact localContact2 = localp.a((Number)localObject4, (com.truecaller.filters.g)localObject2, l5);
      boolean bool16 = w.b("key_temp_latest_call_made_with_tc");
      localObject6 = new com/truecaller/callerid/i;
      localObject8 = P;
      long l8 = ((com.truecaller.utils.a)localObject8).a();
      String str2 = ai.b();
      ((i)localObject6).<init>(0, 0, (Number)localObject4, paramInt2, bool16, l8, localContact2, str2, (com.truecaller.filters.g)localObject2);
      localObject2 = a;
      if (localObject2 != null)
      {
        i8 = i;
        if (i8 != i5) {}
      }
      else
      {
        a = ((i)localObject6);
        localObject2 = a;
        localp.c((i)localObject2);
      }
      localObject2 = com.truecaller.tracking.events.g.b();
      localObject4 = g;
      ((g.a)localObject2).d((CharSequence)localObject4);
      localObject4 = z.a();
      localObject8 = org.c.a.a.a.k.n((String)localObject4);
      ((g.a)localObject2).c((CharSequence)localObject8);
      localObject8 = a;
      localObject9 = org.c.a.a.a.k.n(((Number)localObject8).d());
      ((g.a)localObject2).a((CharSequence)localObject9);
      localObject8 = ((Number)localObject8).a();
      localObject4 = com.truecaller.common.h.aa.c((String)localObject8, (String)localObject4);
      ((g.a)localObject2).b((CharSequence)localObject4);
      try
      {
        localObject4 = q;
        localObject4 = ((com.truecaller.androidactors.f)localObject4).a();
        localObject4 = (ae)localObject4;
        localObject2 = ((g.a)localObject2).a();
        ((ae)localObject4).a((org.apache.a.d.d)localObject2);
      }
      catch (RuntimeException localRuntimeException)
      {
        bool4 = localRuntimeException instanceof IllegalArgumentException;
        if (bool4)
        {
          localObject4 = new com/truecaller/log/UnmutedException$k;
          localObject3 = ((RuntimeException)localRuntimeException).getMessage();
          ((UnmutedException.k)localObject4).<init>((String)localObject3);
          localObject3 = localObject4;
        }
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject3);
      }
      Object localObject3 = w;
      i14 = 0;
      localObject8 = null;
      ((com.truecaller.i.c)localObject3).b("key_temp_latest_call_made_with_tc", false);
      localObject3 = a.a();
      A.a((String)localObject3);
      localp.d((i)localObject6);
      localObject4 = s;
      l5 = 30;
      localObject9 = TimeUnit.SECONDS;
      localObject7 = new com/truecaller/callerid/-$$Lambda$p$CbdsIqRFl5Z4pHLtFvwssd8vDKE;
      ((-..Lambda.p.CbdsIqRFl5Z4pHLtFvwssd8vDKE)localObject7).<init>(localp);
      ((aj)localObject4).a(l5, (TimeUnit)localObject9, (Runnable)localObject7);
      localObject4 = I;
      bool4 = ((CallRecordingManager)localObject4).a();
      if (bool4)
      {
        localObject4 = I;
        bool4 = ((CallRecordingManager)localObject4).d();
        if (!bool4)
        {
          localObject4 = (com.truecaller.calling.recorder.floatingbutton.e)O.a();
          localObject6 = new com/truecaller/callerid/-$$Lambda$p$WYR4MxjNWnyBxf8iO64KsOovIlg;
          ((-..Lambda.p.WYR4MxjNWnyBxf8iO64KsOovIlg)localObject6).<init>(localp);
          ((com.truecaller.calling.recorder.floatingbutton.e)localObject4).a((String)localObject3, (com.truecaller.calling.recorder.floatingbutton.i)localObject6);
        }
      }
      return;
    }
  }
  
  public final void a(i parami)
  {
    v localv = S;
    p.a locala = new com/truecaller/callerid/p$a;
    locala.<init>(this, parami);
    localv.a(locala);
  }
  
  public final void b(i parami)
  {
    Contact localContact = l;
    Object localObject = l;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localObject, arrayOfString);
    localObject = P;
    long l1 = ((com.truecaller.utils.a)localObject).a();
    long l2 = d;
    l1 -= l2;
    parami = localContact.G();
    boolean bool1 = localContact.T();
    if (bool1)
    {
      i.a locala = R;
      bool1 = locala.a();
      if ((!bool1) && (parami != null))
      {
        locala = R;
        locala.a(parami);
        parami = R;
        parami.a(l1);
      }
    }
    int i1 = localContact.getSource() & 0x33;
    if (i1 != 0)
    {
      parami = R;
      boolean bool2 = parami.b();
      if (!bool2)
      {
        parami = R;
        parami.b(l1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */