package com.truecaller.callerid;

import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d
{
  private final q a;
  private final Provider b;
  
  private ac(q paramq, Provider paramProvider)
  {
    a = paramq;
    b = paramProvider;
  }
  
  public static ac a(q paramq, Provider paramProvider)
  {
    ac localac = new com/truecaller/callerid/ac;
    localac.<init>(paramq, paramProvider);
    return localac;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */