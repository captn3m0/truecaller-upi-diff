package com.truecaller.callerid;

import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.calling.recorder.h;
import com.truecaller.common.h.ab;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.FilterAction;
import com.truecaller.filters.g;
import com.truecaller.i.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.k;
import com.truecaller.tracking.events.k.a;
import com.truecaller.util.al;
import com.truecaller.util.cn;
import com.truecaller.util.u;
import org.apache.a.a;
import org.apache.a.d.d;

final class ah
  implements ag
{
  private final c a;
  private final u b;
  private final al c;
  private final cn d;
  private final FilterManager e;
  private final f f;
  private final h g;
  
  ah(c paramc, u paramu, al paramal, cn paramcn, FilterManager paramFilterManager, f paramf, h paramh)
  {
    a = paramc;
    b = paramu;
    c = paramal;
    d = paramcn;
    e = paramFilterManager;
    f = paramf;
    g = paramh;
  }
  
  private boolean c(HistoryEvent paramHistoryEvent)
  {
    int i = o;
    boolean bool2 = true;
    Object localObject = null;
    int j = 3;
    if (i == j)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramHistoryEvent = null;
    }
    if (i != 0)
    {
      paramHistoryEvent = a;
      String str = "showAfterCallForPBContacts";
      boolean bool1 = paramHistoryEvent.a(str, false);
      if (!bool1) {
        try
        {
          paramHistoryEvent = f;
          paramHistoryEvent = paramHistoryEvent.a();
          paramHistoryEvent = (ae)paramHistoryEvent;
          localObject = k.b();
          str = "SettingChanged";
          localObject = ((k.a)localObject).a(str);
          str = "ACSPhonebookContacts";
          localObject = ((k.a)localObject).b(str);
          localObject = ((k.a)localObject).a();
          paramHistoryEvent.a((d)localObject);
          paramHistoryEvent = a;
          localObject = "showAfterCallForPBContacts";
          paramHistoryEvent.b((String)localObject, bool2);
        }
        catch (a paramHistoryEvent)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramHistoryEvent);
        }
      }
      return bool2;
    }
    return false;
  }
  
  public final boolean a(HistoryEvent paramHistoryEvent)
  {
    Object localObject = f;
    if (localObject != null)
    {
      localObject = a;
      String str1 = "afterCall";
      boolean bool1 = ((c)localObject).b(str1);
      if (bool1)
      {
        localObject = b;
        bool1 = ab.e((String)localObject);
        if (bool1)
        {
          localObject = b;
          str1 = c;
          bool1 = ((u)localObject).a(str1);
          if (bool1)
          {
            localObject = a;
            str1 = "enabledCallerIDforPB";
            bool1 = ((c)localObject).b(str1);
            if (!bool1)
            {
              bool1 = c(paramHistoryEvent);
              if (!bool1) {
                break label193;
              }
            }
          }
          localObject = c;
          bool1 = ((al)localObject).a();
          if (bool1)
          {
            localObject = d;
            bool1 = ((cn)localObject).b();
            if (!bool1)
            {
              localObject = e;
              str1 = c;
              String str2 = b;
              paramHistoryEvent = d;
              boolean bool2 = true;
              paramHistoryEvent = ah;
              localObject = FilterManager.FilterAction.FILTER_BLACKLISTED;
              if (paramHistoryEvent != localObject) {
                return bool2;
              }
            }
          }
        }
      }
    }
    label193:
    return false;
  }
  
  public final boolean b(HistoryEvent paramHistoryEvent)
  {
    Object localObject = f;
    if (localObject != null)
    {
      localObject = f.p();
      boolean bool = ab.a((String)localObject);
      if (!bool)
      {
        localObject = c;
        bool = ((al)localObject).a();
        if (bool)
        {
          localObject = d;
          bool = ((cn)localObject).b();
          if (!bool)
          {
            localObject = g;
            bool = ((h)localObject).a();
            if (bool)
            {
              paramHistoryEvent = m;
              if (paramHistoryEvent != null) {
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */