package com.truecaller.callerid;

import com.truecaller.common.h.am;
import com.truecaller.common.tag.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.g;
import com.truecaller.tracking.events.al;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ay;
import com.truecaller.tracking.events.ay.a;
import com.truecaller.tracking.events.bc;
import com.truecaller.tracking.events.bc.a;
import com.truecaller.tracking.events.z;
import com.truecaller.tracking.events.z.a;
import com.truecaller.util.ce;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.c.a.a.a.k;

public final class j
{
  public static z a(i parami)
  {
    boolean bool1 = parami.n;
    ArrayList localArrayList = null;
    if (bool1) {
      return null;
    }
    z.a locala = z.b();
    Object localObject1 = UUID.randomUUID().toString();
    localObject1 = locala.a((CharSequence)localObject1).d("callerId");
    int i = parami.b();
    Object localObject2 = String.valueOf(i);
    ((z.a)localObject1).c((CharSequence)localObject2);
    locala.b(null);
    boolean bool2 = false;
    localObject1 = null;
    locala.a(false);
    locala.b(false);
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    Object localObject3 = parami.l;
    Object localObject4;
    if (localObject3 != null)
    {
      localObject3 = parami.l;
      localObject4 = parami.m;
      Object localObject5 = al.b();
      Object localObject6 = ((Contact)localObject3).z();
      boolean bool3 = am.b((CharSequence)localObject6);
      int m = 1;
      bool3 ^= m;
      localObject5 = ((al.a)localObject5).b(bool3);
      int j = ((Contact)localObject3).getSource() & 0x2;
      if (j != 0)
      {
        j = 1;
      }
      else
      {
        j = 0;
        localObject6 = null;
      }
      localObject5 = ((al.a)localObject5).a(j);
      int k = ((Contact)localObject3).I();
      localObject6 = Integer.valueOf(Math.max(0, k));
      localObject5 = ((al.a)localObject5).a((Integer)localObject6);
      boolean bool4 = ((Contact)localObject3).U();
      localObject6 = Boolean.valueOf(bool4);
      localObject5 = ((al.a)localObject5).d((Boolean)localObject6);
      localObject6 = j;
      Object localObject7 = FilterManager.ActionSource.CUSTOM_BLACKLIST;
      if (localObject6 == localObject7)
      {
        bool4 = true;
      }
      else
      {
        bool4 = false;
        localObject6 = null;
      }
      localObject6 = Boolean.valueOf(bool4);
      localObject5 = ((al.a)localObject5).a((Boolean)localObject6);
      localObject6 = j;
      localObject7 = FilterManager.ActionSource.CUSTOM_WHITELIST;
      if (localObject6 == localObject7)
      {
        bool4 = true;
      }
      else
      {
        bool4 = false;
        localObject6 = null;
      }
      localObject6 = Boolean.valueOf(bool4);
      localObject5 = ((al.a)localObject5).c((Boolean)localObject6);
      localObject4 = j;
      localObject6 = FilterManager.ActionSource.TOP_SPAMMER;
      boolean bool5;
      if (localObject4 == localObject6)
      {
        bool5 = true;
      }
      else
      {
        bool5 = false;
        localObject4 = null;
      }
      localObject4 = Boolean.valueOf(bool5);
      localObject4 = ((al.a)localObject5).b((Boolean)localObject4);
      int n = ((Contact)localObject3).getSource() & 0x40;
      if (n != 0) {
        bool2 = true;
      }
      localObject1 = Boolean.valueOf(bool2);
      localObject1 = ((al.a)localObject4).e((Boolean)localObject1).a();
      localObject4 = new java/util/ArrayList;
      ((ArrayList)localObject4).<init>();
      localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>();
      localObject6 = new java/util/ArrayList;
      ((ArrayList)localObject6).<init>();
      localObject7 = ((Contact)localObject3).J().iterator();
      for (;;)
      {
        bool7 = ((Iterator)localObject7).hasNext();
        if (!bool7) {
          break;
        }
        Object localObject8 = (Tag)((Iterator)localObject7).next();
        int i1 = ((Tag)localObject8).getSource();
        if (i1 == m)
        {
          localObject8 = ((Tag)localObject8).a();
          ((List)localObject4).add(localObject8);
        }
        else
        {
          localObject8 = ((Tag)localObject8).a();
          ((List)localObject5).add(localObject8);
        }
      }
      localObject7 = ce.a((Contact)localObject3);
      if (localObject7 != null)
      {
        long l = a;
        localObject7 = String.valueOf(l);
        ((List)localObject6).add(localObject7);
      }
      localObject7 = bc.b();
      boolean bool7 = ((List)localObject4).isEmpty();
      if (bool7)
      {
        bool5 = false;
        localObject4 = null;
      }
      localObject4 = ((bc.a)localObject7).a((List)localObject4);
      boolean bool8 = ((List)localObject5).isEmpty();
      if (bool8)
      {
        n = 0;
        localObject5 = null;
      }
      localObject4 = ((bc.a)localObject4).b((List)localObject5);
      boolean bool6 = ((List)localObject6).isEmpty();
      if (bool6)
      {
        bool4 = false;
        localObject6 = null;
      }
      localObject4 = ((bc.a)localObject4).c((List)localObject6).a();
      localObject3 = ((Contact)localObject3).A().iterator();
      bool6 = false;
      localObject5 = null;
      for (;;)
      {
        bool4 = ((Iterator)localObject3).hasNext();
        if (!bool4) {
          break;
        }
        localObject6 = (Number)((Iterator)localObject3).next();
        int i2 = ((Number)localObject6).getSource() & m;
        if (i2 != 0) {
          localObject5 = ((Number)localObject6).b();
        }
      }
      localObject3 = ay.b();
      localObject6 = k.n(a.d());
      localObject1 = ((ay.a)localObject3).a((CharSequence)localObject6).a((bc)localObject4).a((al)localObject1);
      localObject3 = o;
      localObject1 = ((ay.a)localObject1).b((CharSequence)localObject3).c((CharSequence)localObject5).a();
      ((List)localObject2).add(localObject1);
    }
    else
    {
      localObject3 = al.b().b(false).a(false);
      localObject1 = Integer.valueOf(0);
      localObject1 = ((al.a)localObject3).a((Integer)localObject1);
      localObject3 = Boolean.FALSE;
      localObject1 = ((al.a)localObject1).d((Boolean)localObject3);
      localObject3 = Boolean.FALSE;
      localObject1 = ((al.a)localObject1).a((Boolean)localObject3);
      localObject3 = Boolean.FALSE;
      localObject1 = ((al.a)localObject1).c((Boolean)localObject3);
      localObject3 = Boolean.FALSE;
      localObject1 = ((al.a)localObject1).b((Boolean)localObject3);
      localObject3 = Boolean.FALSE;
      localObject1 = ((al.a)localObject1).e((Boolean)localObject3).a();
      localObject3 = ay.b();
      localObject4 = k.n(a.d());
      localObject3 = ((ay.a)localObject3).a((CharSequence)localObject4).a(null);
      localObject1 = ((ay.a)localObject3).a((al)localObject1).b(null).c(null).a();
      ((List)localObject2).add(localObject1);
    }
    locala.a((List)localObject2);
    localObject1 = g;
    bool2 = k.b((CharSequence)localObject1);
    if (bool2)
    {
      locala.b(null);
    }
    else
    {
      localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      parami = g;
      localArrayList.add(parami);
      locala.b(localArrayList);
    }
    return locala.a();
  }
  
  public static HistoryEvent b(i parami)
  {
    HistoryEvent localHistoryEvent = new com/truecaller/data/entity/HistoryEvent;
    Object localObject = a;
    localHistoryEvent.<init>((Number)localObject);
    long l = d;
    h = l;
    localObject = parami.l;
    f = ((Contact)localObject);
    localObject = UUID.randomUUID().toString();
    a = ((String)localObject);
    int i = h;
    int m = 2;
    int n = 1;
    if (i == 0)
    {
      boolean bool1 = parami.a();
      if (bool1)
      {
        p = m;
        break label122;
      }
    }
    int j = h;
    int i1 = 12785645;
    if (j == i1)
    {
      p = n;
    }
    else
    {
      j = h;
      p = j;
    }
    label122:
    boolean bool2 = e;
    if (bool2)
    {
      int k = parami.i;
      m = 3;
      if (k == m)
      {
        boolean bool3 = parami.j;
        if (!bool3)
        {
          o = m;
          return localHistoryEvent;
        }
      }
      o = n;
    }
    else
    {
      o = m;
    }
    return localHistoryEvent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */