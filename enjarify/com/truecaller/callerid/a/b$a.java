package com.truecaller.callerid.a;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.z.d;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.featuretoggles.e;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;

final class b$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag h;
  
  b$a(b paramb, String paramString1, String paramString2, String paramString3, com.truecaller.old.data.entity.Notification paramNotification, long paramLong, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/callerid/a/b$a;
    b localb = b;
    String str1 = c;
    String str2 = d;
    String str3 = e;
    com.truecaller.old.data.entity.Notification localNotification = f;
    long l = g;
    locala.<init>(localb, str1, str2, str3, localNotification, l, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = this;
    Object localObject1 = paramObject;
    Object localObject2 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        localObject1 = b.b;
        localObject2 = c;
        localObject1 = ((com.truecaller.data.access.c)localObject1).b((String)localObject2);
        localObject2 = b;
        String str1 = d;
        Object localObject3 = e;
        Object localObject4 = c;
        Object localObject5 = f.d();
        Object localObject6 = "notification.notificationId";
        c.g.b.k.a(localObject5, (String)localObject6);
        long l1 = ((Long)localObject5).longValue();
        Contact localContact = new com/truecaller/data/entity/Contact;
        localContact.<init>();
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(str1);
        localStringBuilder.append(' ');
        localStringBuilder.append((String)localObject3);
        str1 = localStringBuilder.toString();
        localContact.l(str1);
        int j = 64;
        localContact.setSource(j);
        localObject3 = c;
        boolean bool2 = true;
        Object localObject7 = new String[bool2];
        localObject7[0] = localObject4;
        localObject3 = ((g)localObject3).a((String[])localObject7);
        if (localObject3 != null) {
          localContact.a((Number)localObject3);
        }
        localObject3 = String.valueOf(l1);
        localContact.n((String)localObject3);
        long l2 = d.a();
        localContact.a(l2);
        localObject2 = b;
        l2 = g;
        localObject5 = d;
        l2 = Math.abs(((com.truecaller.utils.a)localObject5).a() - l2);
        l1 = 20000L;
        boolean bool3 = l2 < l1;
        boolean bool4;
        if (!bool3)
        {
          bool4 = true;
        }
        else
        {
          bool4 = false;
          localObject3 = null;
        }
        if (bool4)
        {
          localObject2 = f;
          localObject3 = "showIncomingCallNotifications";
          bool1 = ((com.truecaller.i.c)localObject2).a((String)localObject3, bool2);
          if (bool1)
          {
            bool1 = true;
            break label360;
          }
        }
        bool1 = false;
        localObject2 = null;
        label360:
        if (bool1)
        {
          localObject2 = b.e;
          if (localObject1 == null) {
            localObject3 = localContact;
          } else {
            localObject3 = localObject1;
          }
          String str2 = c;
          c.g.b.k.b(localObject3, "contact");
          c.g.b.k.b(str2, "normalizedNumber");
          localObject4 = f.V();
          boolean bool5 = ((com.truecaller.featuretoggles.b)localObject4).a();
          if (bool5)
          {
            localObject4 = new android/support/v4/app/z$d;
            localObject5 = e;
            localObject6 = a.i();
            ((z.d)localObject4).<init>((Context)localObject5, (String)localObject6);
            localObject4 = ((z.d)localObject4).a(2131234787);
            int m = android.support.v4.content.b.c(e, 2131100594);
            localObject4 = ((z.d)localObject4).f(m);
            localObject5 = c;
            localObject7 = new Object[bool2];
            Object localObject8 = ((Contact)localObject3).t();
            localObject7[0] = localObject8;
            localObject5 = (CharSequence)((n)localObject5).a(2131887303, (Object[])localObject7);
            localObject4 = ((z.d)localObject4).a((CharSequence)localObject5);
            localObject5 = c;
            localObject7 = new Object[0];
            localObject5 = (CharSequence)((n)localObject5).a(2131887302, (Object[])localObject7);
            localObject4 = ((z.d)localObject4).b((CharSequence)localObject5);
            m = 2;
            localObject4 = ((z.d)localObject4).e(m);
            localObject5 = e;
            localObject6 = DetailsFragment.SourceType.Notification;
            localObject5 = DetailsFragment.a((Context)localObject5, (Contact)localObject3, (DetailsFragment.SourceType)localObject6, false, bool2);
            int n = 268468224;
            localObject5 = ((Intent)localObject5).addFlags(n);
            localObject6 = e;
            int k = 2131364155;
            int i1 = 268435456;
            localObject5 = PendingIntent.getActivity((Context)localObject6, k, (Intent)localObject5, i1);
            c.g.b.k.a(localObject5, "PendingIntent.getActivit…tent.FLAG_CANCEL_CURRENT)");
            localObject4 = ((z.d)localObject4).a((PendingIntent)localObject5);
            localObject5 = d;
            localObject6 = new com/truecaller/callerid/a/d$a;
            ((d.a)localObject6).<init>((d)localObject2, (Contact)localObject3);
            localObject6 = (l.a)localObject6;
            android.app.Notification localNotification = ((l)localObject5).a((z.d)localObject4, (l.a)localObject6);
            c.g.b.k.a(localNotification, "notificationIconHelper.c…) { contact.getAvatar() }");
            localObject8 = b;
            int i2 = 2131365509;
            String str3 = "notificationPushCallerId";
            localObject2 = new android/os/Bundle;
            ((Bundle)localObject2).<init>();
            localObject4 = "Subtype";
            bool4 = ((Contact)localObject3).Z();
            if (bool4) {
              localObject3 = "pushNotificationPbContact";
            } else {
              localObject3 = "pushNotificationNotPbContact";
            }
            ((Bundle)localObject2).putString((String)localObject4, (String)localObject3);
            ((com.truecaller.notifications.a)localObject8).a(str2, i2, localNotification, str3, (Bundle)localObject2);
          }
        }
        localObject2 = b;
        boolean bool6 = b.a((b)localObject2, (Contact)localObject1);
        if (bool6)
        {
          localObject1 = b;
          long l3 = f.d().longValue();
          localObject2 = String.valueOf(l3);
          b.a((b)localObject1, (String)localObject2, bool2);
        }
        else
        {
          localObject1 = b;
          l2 = f.d().longValue();
          localObject2 = String.valueOf(l2);
          b.a((b)localObject1, (String)localObject2, false);
          localObject1 = b.a;
          localObject2 = c;
          localObject3 = new int[bool2];
          localObject3[0] = j;
          ((com.truecaller.data.access.m)localObject1).a((String)localObject2, (int[])localObject3);
          localObject1 = b.a;
          ((com.truecaller.data.access.m)localObject1).a(localContact);
        }
        return x.a;
      }
      throw a;
    }
    localObject1 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)localObject1);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */