package com.truecaller.callerid;

import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d
{
  private final q a;
  private final Provider b;
  
  private v(q paramq, Provider paramProvider)
  {
    a = paramq;
    b = paramProvider;
  }
  
  public static v a(q paramq, Provider paramProvider)
  {
    v localv = new com/truecaller/callerid/v;
    localv.<init>(paramq, paramProvider);
    return localv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */