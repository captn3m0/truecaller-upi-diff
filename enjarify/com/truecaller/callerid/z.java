package com.truecaller.callerid;

import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d
{
  private final q a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  
  private z(q paramq, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7)
  {
    a = paramq;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
  }
  
  public static z a(q paramq, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7)
  {
    z localz = new com/truecaller/callerid/z;
    localz.<init>(paramq, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
    return localz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */