package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;

public final class o
  implements n
{
  private final v a;
  
  public o(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return n.class.equals(paramClass);
  }
  
  public final void a()
  {
    v localv = a;
    o.a locala = new com/truecaller/callerid/o$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, (byte)0);
    localv.a(locala);
  }
  
  public final void a(int paramInt1, String paramString, int paramInt2, int paramInt3)
  {
    v localv = a;
    o.d locald = new com/truecaller/callerid/o$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramInt1, paramString, paramInt2, paramInt3, (byte)0);
    localv.a(locald);
  }
  
  public final void a(i parami)
  {
    v localv = a;
    o.b localb = new com/truecaller/callerid/o$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, parami, (byte)0);
    localv.a(localb);
  }
  
  public final void b(i parami)
  {
    v localv = a;
    o.c localc = new com/truecaller/callerid/o$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, parami, (byte)0);
    localv.a(localc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */