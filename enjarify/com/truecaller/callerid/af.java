package com.truecaller.callerid;

import android.text.TextUtils;
import c.l.g;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.w;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.featuretoggles.f;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.search.j;
import com.truecaller.network.search.n;
import com.truecaller.util.al;
import com.truecaller.utils.a;
import com.truecaller.utils.i;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

final class af
  implements ad
{
  private final c a;
  private final al b;
  private final a c;
  private final am d;
  private final b e;
  private final i f;
  private final com.truecaller.featuretoggles.e g;
  
  af(c paramc, al paramal, a parama, am paramam, b paramb, i parami, com.truecaller.featuretoggles.e parame)
  {
    a = paramc;
    b = paramal;
    c = parama;
    d = paramam;
    e = paramb;
    f = parami;
    g = parame;
  }
  
  public final w a(Number paramNumber, boolean paramBoolean, int paramInt, j paramj)
  {
    af localaf = this;
    Object localObject1 = paramj;
    Object localObject2 = new com/truecaller/analytics/e$a;
    Object localObject3 = "CallerIdSearch";
    ((com.truecaller.analytics.e.a)localObject2).<init>((String)localObject3);
    if (paramBoolean)
    {
      localObject3 = paramNumber.a();
      bool1 = TextUtils.isEmpty((CharSequence)localObject3);
      if (!bool1)
      {
        new String[1][0] = "Trying to find contact in AggregatedContactDao.";
        localObject3 = a;
        str1 = paramNumber.a();
        localObject3 = ((c)localObject3).b(str1);
        if (localObject3 != null)
        {
          bool3 = ((Contact)localObject3).T();
          if (!bool3)
          {
            ((com.truecaller.analytics.e.a)localObject2).a("Result", "Cache");
            localObject1 = e;
            localObject2 = ((com.truecaller.analytics.e.a)localObject2).a();
            ((b)localObject1).a((com.truecaller.analytics.e)localObject2);
            new String[1][0] = "AggregatedContactDao cache hit, returning contact";
            return w.b(localObject3);
          }
        }
      }
    }
    new String[1][0] = "AggregatedContactDao cache miss, performing server side search.";
    localObject3 = b;
    boolean bool1 = ((al)localObject3).a();
    boolean bool3 = false;
    String str1 = null;
    if (!bool1)
    {
      new String[1][0] = "Cannot perform a search without a valid account.";
      return w.b(null);
    }
    localObject3 = g;
    Object localObject5 = L;
    Object localObject6 = com.truecaller.featuretoggles.e.a[103];
    int i = ((f)((com.truecaller.featuretoggles.e.a)localObject5).a((com.truecaller.featuretoggles.e)localObject3, (g)localObject6)).a(3000);
    localObject5 = paramNumber.d();
    i = ((String)localObject5);
    localObject5 = TimeUnit.MILLISECONDS;
    l = i;
    m = ((TimeUnit)localObject5);
    boolean bool4 = true;
    n = bool4;
    localObject6 = paramNumber.l();
    localObject6 = ((j)localObject1).b((String)localObject6);
    h = paramInt;
    b = false;
    d = bool4;
    e = bool4;
    c = bool4;
    ((com.truecaller.analytics.e.a)localObject2).a("Result", "Fail");
    String str2 = "no-connection";
    ((com.truecaller.analytics.e.a)localObject2).a("LastAttemptNetworkType", str2);
    ((com.truecaller.analytics.e.a)localObject2).a("ConnectTimeout", i);
    localObject3 = c;
    long l1 = ((a)localObject3).b();
    n localn = null;
    long l2 = l1;
    int j = 0;
    localObject6 = null;
    for (;;)
    {
      i = 6;
      if (j >= i) {
        break;
      }
      int k = j + 1;
      ((com.truecaller.analytics.e.a)localObject2).a("Attempts", k);
      localObject3 = f;
      boolean bool2 = ((i)localObject3).a();
      int m = 5;
      Object localObject7;
      if (bool2)
      {
        localObject3 = "LastAttemptNetworkType";
        try
        {
          localObject7 = f;
          localObject7 = ((i)localObject7).b();
          ((com.truecaller.analytics.e.a)localObject2).a((String)localObject3, (String)localObject7);
          localn = paramj.f();
          localObject3 = "Result";
          localObject7 = "Success";
          ((com.truecaller.analytics.e.a)localObject2).a((String)localObject3, (String)localObject7);
          localObject3 = "Received response from backend";
          new String[1][0] = localObject3;
        }
        catch (RuntimeException localRuntimeException) {}catch (IOException localIOException) {}
        String[] arrayOfString = new String[bool4];
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("Search for ");
        localObject7 = paramNumber;
        localStringBuilder.append(paramNumber);
        localStringBuilder.append(" failed");
        localObject5 = localStringBuilder.toString();
        arrayOfString[0] = localObject5;
        AssertionUtil.reportThrowableButNeverCrash(localIOException);
        if (j < m)
        {
          new String[1][0] = "Connection error, retrying in 500 ms";
          localObject4 = d;
          l3 = 500L;
          ((am)localObject4).a(l3);
        }
      }
      else
      {
        localObject7 = paramNumber;
        if (j < m)
        {
          new String[1][0] = "No internet connection, retrying in 1500 ms";
          localObject4 = d;
          l3 = 1500L;
          ((am)localObject4).a(l3);
        }
      }
      localObject4 = c;
      l2 = ((a)localObject4).b();
      j = k;
      bool4 = true;
    }
    long l3 = c.b();
    double d1 = l3;
    double d2 = l1;
    Double.isNaN(d1);
    Double.isNaN(d2);
    d1 -= d2;
    Object localObject4 = Double.valueOf(d1);
    a = ((Double)localObject4);
    localObject4 = "LastAttemptDurationBucket";
    localObject1 = TimeUnit.MILLISECONDS;
    l3 -= l2;
    l3 = ((TimeUnit)localObject1).toSeconds(l3);
    long l4 = 1L;
    d2 = Double.MIN_VALUE;
    boolean bool5 = l3 < l4;
    if (bool5)
    {
      localObject1 = "0-1";
    }
    else
    {
      l4 = 2;
      d2 = 1.0E-323D;
      bool5 = l3 < l4;
      if (bool5)
      {
        localObject1 = "1-2";
      }
      else
      {
        l4 = 3;
        d2 = 1.5E-323D;
        bool5 = l3 < l4;
        if (bool5)
        {
          localObject1 = "2-3";
        }
        else
        {
          l4 = 4;
          d2 = 2.0E-323D;
          bool5 = l3 < l4;
          if (bool5)
          {
            localObject1 = "3-4";
          }
          else
          {
            l4 = 5;
            d2 = 2.5E-323D;
            bool5 = l3 < l4;
            if (bool5)
            {
              localObject1 = "4-5";
            }
            else
            {
              l4 = 6;
              d2 = 3.0E-323D;
              bool5 = l3 < l4;
              if (bool5)
              {
                localObject1 = "5-6";
              }
              else
              {
                l4 = 7;
                d2 = 3.5E-323D;
                bool5 = l3 < l4;
                if (bool5)
                {
                  localObject1 = "6-7";
                }
                else
                {
                  l4 = 8;
                  d2 = 4.0E-323D;
                  bool5 = l3 < l4;
                  if (bool5)
                  {
                    localObject1 = "7-8";
                  }
                  else
                  {
                    l4 = 9;
                    d2 = 4.4E-323D;
                    bool5 = l3 < l4;
                    if (bool5) {
                      localObject1 = "8-9";
                    } else {
                      localObject1 = ">9";
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    ((com.truecaller.analytics.e.a)localObject2).a((String)localObject4, (String)localObject1);
    localObject4 = e;
    localObject1 = ((com.truecaller.analytics.e.a)localObject2).a();
    ((b)localObject4).a((com.truecaller.analytics.e)localObject1);
    if (localn == null) {
      return w.b(null);
    }
    return w.b(localn.a());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */