package com.truecaller.callerid;

import android.content.Context;
import c.g.b.k;
import com.truecaller.ui.FeedbackDialogActivity;
import com.truecaller.ui.components.FeedbackItemView;
import com.truecaller.ui.components.FeedbackItemView.DisplaySource;
import com.truecaller.ui.components.FeedbackItemView.FeedbackItem;

public final class d
  implements c
{
  private final Context a;
  
  public d(Context paramContext)
  {
    a = paramContext;
  }
  
  public final void a(i parami)
  {
    k.b(parami, "callState");
    parami = FeedbackItemView.DisplaySource.BLOCKED_CALL;
    Context localContext = a;
    parami = FeedbackItemView.a(parami, localContext);
    if (parami != null)
    {
      localContext = a;
      FeedbackItemView.DisplaySource localDisplaySource = parami.b();
      parami = parami.d();
      FeedbackDialogActivity.a(localContext, localDisplaySource, parami);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */