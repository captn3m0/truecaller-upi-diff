package com.truecaller.callerid.b;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.content.d;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;

public final class a
  implements View.OnTouchListener
{
  private final c a;
  private final float b;
  private final float c;
  private float d;
  private float e;
  private int f;
  private boolean g;
  private boolean h;
  private boolean i;
  private final com.truecaller.i.c j;
  private final int k;
  private final VelocityTracker l;
  
  public a(c paramc, int paramInt)
  {
    VelocityTracker localVelocityTracker = VelocityTracker.obtain();
    l = localVelocityTracker;
    a = paramc;
    k = paramInt;
    float f1 = a.a.getResources().getDisplayMetrics().density;
    float f2 = 25.0F * f1;
    c = f2;
    f1 *= 400.0F;
    b = f1;
    paramc = ((bk)paramc.g().getApplicationContext()).a().D();
    j = paramc;
  }
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    paramView = a;
    boolean bool1 = d;
    boolean bool3 = true;
    if (!bool1) {
      return bool3;
    }
    paramView = l;
    paramView.addMovement(paramMotionEvent);
    int m = paramMotionEvent.getAction();
    int i1 = 0;
    float f1 = 0.0F;
    Object localObject1 = null;
    int i2 = 0;
    float f2 = 0.0F;
    Object localObject2 = null;
    float f4;
    float f5;
    float f6;
    Object localObject3;
    boolean bool6;
    switch (m)
    {
    default: 
      return false;
    case 2: 
      f3 = paramMotionEvent.getRawX();
      f4 = paramMotionEvent.getRawY();
      f5 = d;
      f3 -= f5;
      f5 = e;
      f4 -= f5;
      boolean bool4 = g;
      Object localObject4;
      if (!bool4)
      {
        bool4 = h;
        if (!bool4)
        {
          f5 = Math.abs(f4);
          int i5 = k;
          f6 = i5;
          bool4 = f5 < f6;
          if (bool4)
          {
            h = bool3;
            bool4 = i;
            if (!bool4)
            {
              i = bool3;
              localObject3 = j;
              localObject4 = "callerIdHintCount";
              ((com.truecaller.i.c)localObject3).a_((String)localObject4);
            }
          }
          else
          {
            f5 = Math.abs(f3);
            i5 = k;
            f6 = i5;
            bool4 = f5 < f6;
            if (bool4) {
              g = bool3;
            }
          }
        }
      }
      bool4 = h;
      if (bool4)
      {
        int i3 = f;
        f5 = i3 + f4;
        int i6 = (int)f5;
        if (i6 >= 0)
        {
          localObject2 = a;
          i2 = g;
          localObject3 = a;
          i3 = ((c)localObject3).e();
          i2 -= i3;
          if (i6 > i2)
          {
            paramMotionEvent = a;
            i6 = g;
            localObject2 = a;
            i2 = ((c)localObject2).e();
            i2 = i6 - i2;
          }
          else
          {
            i2 = i6;
          }
        }
        paramMotionEvent = a;
        localObject3 = e;
        if (localObject3 != null)
        {
          paramMotionEvent = e;
          y = i2;
        }
        paramMotionEvent = a;
        localObject3 = c;
        localObject4 = b;
        paramMotionEvent = e;
        ((WindowManager)localObject3).updateViewLayout((View)localObject4, paramMotionEvent);
        paramMotionEvent = d.a(TrueApp.x());
        localObject3 = new android/content/Intent;
        ((Intent)localObject3).<init>("BroadcastCallerIdPosY");
        localObject4 = "ExtraPosY";
        localObject2 = ((Intent)localObject3).putExtra((String)localObject4, i2);
        paramMotionEvent.a((Intent)localObject2);
      }
      bool6 = g;
      if (bool6)
      {
        f4 = Math.abs(f3);
        localObject2 = a;
        f2 = g;
        f4 /= f2;
        i2 = 1065353216;
        f2 = 1.0F;
        f4 = f2 - f4;
        f4 = Math.min(f2, f4);
        f4 = Math.max(0.0F, f4);
        localObject1 = a;
        ((c)localObject1).b(f4);
        paramMotionEvent = a;
        paramMotionEvent.a(f3);
      }
      return bool3;
    case 1: 
      boolean bool2 = g;
      if (bool2)
      {
        l.computeCurrentVelocity(1000);
        paramView = l;
        f3 = paramView.getXVelocity();
        f5 = Math.abs(f3);
        f6 = b;
        boolean bool5 = f5 < f6;
        if (bool5)
        {
          f5 = d;
          f4 = paramMotionEvent.getRawX();
          f4 = Math.abs(f5 - f4);
          f5 = c;
          bool6 = f4 < f5;
          if (bool6) {}
        }
        else
        {
          paramMotionEvent = a;
          f4 = Math.abs(paramMotionEvent.f());
          localObject3 = a;
          int i4 = f / 2;
          f5 = i4;
          bool6 = f4 < f5;
          if (bool6) {
            break label855;
          }
        }
        paramMotionEvent = a;
        f4 = Math.abs(paramMotionEvent.f());
        localObject1 = a;
        i1 = f / 2;
        f1 = i1;
        bool6 = f4 < f1;
        if (!bool6)
        {
          paramView = a;
          f3 = paramView.f();
        }
        paramMotionEvent = a;
        i1 = f;
        f1 = i1;
        n = (int)Math.copySign(f1, f3);
        f3 = n;
        paramMotionEvent.a(f3, bool3);
        paramView = TrueApp.y().a().c();
        paramMotionEvent = new com/truecaller/analytics/e$a;
        paramMotionEvent.<init>("CALLERID_CallerIDWindow_Dismissed");
        localObject1 = "Dismiss_Type";
        localObject3 = "SwipeAway";
        paramMotionEvent = paramMotionEvent.a((String)localObject1, (String)localObject3).a();
        paramView.a(paramMotionEvent);
        break label866;
        label855:
        paramView = a;
        paramView.a(0.0F, false);
        label866:
        g = false;
      }
      else
      {
        paramView = TrueApp.y().a().c();
        paramMotionEvent = new com/truecaller/analytics/e$a;
        localObject1 = "CALLERID_CallerIDWindow_Moved";
        paramMotionEvent.<init>((String)localObject1);
        paramMotionEvent = paramMotionEvent.a();
        paramView.a(paramMotionEvent);
      }
      h = false;
      return bool3;
    }
    float f3 = paramMotionEvent.getRawX();
    d = f3;
    f3 = paramMotionEvent.getRawY();
    e = f3;
    paramView = a;
    paramMotionEvent = e;
    if (paramMotionEvent != null)
    {
      paramView = e;
      i2 = y;
    }
    f = i2;
    paramView = a;
    int n = g;
    paramMotionEvent = a;
    int i7 = paramMotionEvent.e();
    n -= i7;
    i7 = f;
    if (i7 > n) {
      f = n;
    }
    return bool3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */