package com.truecaller.callerid.b;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.r;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callerid.i;
import com.truecaller.common.h.k;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.util.at;
import com.truecaller.utils.l;

public abstract class c
{
  protected final Context a;
  FrameLayout b;
  WindowManager c;
  public boolean d;
  WindowManager.LayoutParams e;
  int f;
  int g;
  protected View h;
  private final c.a i;
  private final int j;
  private i k;
  private l l;
  
  c(Context paramContext, c.a parama)
  {
    ContextThemeWrapper localContextThemeWrapper = new android/view/ContextThemeWrapper;
    int m = aresId;
    localContextThemeWrapper.<init>(paramContext, m);
    a = localContextThemeWrapper;
    i = parama;
    paramContext = ((bk)paramContext.getApplicationContext()).a().bw();
    l = paramContext;
    int n = a.getResources().getInteger(17694720);
    j = n;
  }
  
  public static int a(l paraml)
  {
    boolean bool1 = k.h();
    if (bool1) {
      return 2038;
    }
    boolean bool2 = paraml.a();
    if (bool2) {
      return 2010;
    }
    return 2005;
  }
  
  private void j()
  {
    d = true;
    b.setVisibility(0);
    h.clearAnimation();
    h.setAlpha(0.0F);
    View localView = h;
    float f1 = f;
    localView.setTranslationX(f1);
    a(0.0F, false);
    b();
  }
  
  protected abstract a a();
  
  final void a(float paramFloat)
  {
    View localView = h;
    if (localView != null) {
      localView.setTranslationX(paramFloat);
    }
  }
  
  final void a(float paramFloat, boolean paramBoolean)
  {
    float f1;
    Object localObject;
    if (!paramBoolean)
    {
      f1 = 1.0F;
      localObject = new android/view/animation/AccelerateDecelerateInterpolator;
      ((AccelerateDecelerateInterpolator)localObject).<init>();
    }
    else
    {
      localObject = new android/view/animation/LinearInterpolator;
      ((LinearInterpolator)localObject).<init>();
      f1 = 0.0F;
      local1 = null;
    }
    boolean bool = paramBoolean ^ true;
    d = bool;
    ViewPropertyAnimator localViewPropertyAnimator = h.animate().translationX(paramFloat).alpha(f1);
    long l1 = j;
    localViewPropertyAnimator = localViewPropertyAnimator.setDuration(l1).setInterpolator((TimeInterpolator)localObject);
    c.1 local1 = new com/truecaller/callerid/b/c$1;
    local1.<init>(this, paramBoolean);
    localViewPropertyAnimator.setListener(local1);
  }
  
  protected abstract void a(View paramView);
  
  public final void a(i parami)
  {
    i locali = k;
    if (locali != null)
    {
      long l1 = c;
      long l2 = c;
      boolean bool1 = l1 < l2;
      if (!bool1)
      {
        bool2 = false;
        locali = null;
        break label45;
      }
    }
    boolean bool2 = true;
    label45:
    Object localObject = (com.truecaller.common.b.a)a.getApplicationContext();
    boolean bool3 = ((com.truecaller.common.b.a)localObject).p();
    if (bool3)
    {
      localObject = l;
      if (localObject != null)
      {
        bool3 = d;
        if (!bool3) {
          if (bool2) {
            j();
          } else {
            return;
          }
        }
        k = parami;
        a(parami, bool2);
        return;
      }
    }
  }
  
  protected void a(i parami, boolean paramBoolean) {}
  
  protected abstract void b();
  
  final void b(float paramFloat)
  {
    View localView = h;
    if (localView != null) {
      localView.setAlpha(paramFloat);
    }
  }
  
  public void c()
  {
    boolean bool = d;
    if (!bool)
    {
      Object localObject1 = e;
      int m = 0;
      Object localObject2 = null;
      if (localObject1 != null)
      {
        bool = true;
      }
      else
      {
        bool = false;
        localObject1 = null;
      }
      localObject2 = new String[0];
      AssertionUtil.OnlyInDebug.isTrue(bool, (String[])localObject2);
      localObject1 = e;
      if (localObject1 != null)
      {
        m = -2;
        height = m;
        try
        {
          localObject2 = c;
          FrameLayout localFrameLayout = b;
          ((WindowManager)localObject2).updateViewLayout(localFrameLayout, (ViewGroup.LayoutParams)localObject1);
        }
        catch (IllegalArgumentException localIllegalArgumentException) {}
      }
      localObject2 = e;
      m = y;
      long l1 = m;
      Settings.a("callerIdLastYPosition", l1);
      localObject1 = b;
      bool = r.H((View)localObject1);
      if (bool)
      {
        localObject1 = b;
        m = 8;
        ((FrameLayout)localObject1).setVisibility(m);
        localObject1 = c;
        localObject2 = b;
        ((WindowManager)localObject1).removeView((View)localObject2);
      }
      i.e();
      return;
    }
    j();
  }
  
  public final boolean d()
  {
    Object localObject1 = (LayoutInflater)a.getSystemService("layout_inflater");
    Object localObject2 = (WindowManager)a.getSystemService("window");
    c = ((WindowManager)localObject2);
    localObject2 = a.getResources().getDisplayMetrics();
    int m = widthPixels;
    f = m;
    int n = heightPixels;
    m = at.a(a.getResources());
    n -= m;
    g = n;
    int i1 = a(l);
    localObject2 = new android/view/WindowManager$LayoutParams;
    int i2 = -1;
    int i3 = -2;
    int i4 = 524296;
    int i5 = -3;
    Object localObject3 = localObject2;
    ((WindowManager.LayoutParams)localObject2).<init>(i2, i3, i1, i4, i5);
    e = ((WindowManager.LayoutParams)localObject2);
    localObject2 = e;
    gravity = 49;
    dimAmount = 0.6F;
    m = Settings.c("callerIdLastYPosition");
    y = m;
    localObject2 = new android/widget/FrameLayout;
    localObject3 = a;
    ((FrameLayout)localObject2).<init>((Context)localObject3);
    b = ((FrameLayout)localObject2);
    localObject2 = b;
    m = 8;
    ((FrameLayout)localObject2).setVisibility(m);
    try
    {
      localObject2 = c;
      localObject3 = b;
      WindowManager.LayoutParams localLayoutParams = e;
      ((WindowManager)localObject2).addView((View)localObject3, localLayoutParams);
      n = 2131559156;
      m = 0;
      localObject3 = null;
      localObject1 = ((LayoutInflater)localObject1).inflate(n, null);
      h = ((View)localObject1);
      localObject1 = b;
      localObject2 = h;
      ((FrameLayout)localObject1).addView((View)localObject2);
      localObject1 = b;
      localObject2 = a();
      ((FrameLayout)localObject1).setOnTouchListener((View.OnTouchListener)localObject2);
      localObject1 = h;
      a((View)localObject1);
      return true;
    }
    catch (RuntimeException localRuntimeException)
    {
      d.a(localRuntimeException, "Cannot add caller id window");
    }
    return false;
  }
  
  final int e()
  {
    View localView = h;
    if (localView != null) {
      return localView.getHeight();
    }
    return 0;
  }
  
  final float f()
  {
    View localView = h;
    if (localView != null) {
      return localView.getTranslationX();
    }
    return 0.0F;
  }
  
  public final Context g()
  {
    return a;
  }
  
  public final void h()
  {
    d = false;
    float f1 = h.getTranslationX();
    a(f1, true);
  }
  
  public final void i()
  {
    DisplayMetrics localDisplayMetrics = a.getResources().getDisplayMetrics();
    int m = widthPixels;
    f = m;
    int n = heightPixels;
    m = at.a(a.getResources());
    n -= m;
    g = n;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */