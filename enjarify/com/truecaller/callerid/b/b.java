package com.truecaller.callerid.b;

import android.arch.lifecycle.e.b;
import android.arch.lifecycle.h;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v4.e.d;
import android.support.v4.widget.m;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.t;
import com.truecaller.TrueApp;
import com.truecaller.ads.campaigns.AdCampaign;
import com.truecaller.ads.campaigns.AdCampaign.Style;
import com.truecaller.ads.campaigns.AdCampaigns;
import com.truecaller.ads.j;
import com.truecaller.ads.j.a;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.common.h.aq.b;
import com.truecaller.common.tag.TagView;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.ShineView;
import com.truecaller.ui.components.AvatarView;
import com.truecaller.ui.components.RippleView;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.util.b.ao;
import com.truecaller.util.b.j.b;
import com.truecaller.util.ce;
import com.truecaller.util.co;
import java.util.List;

public final class b
  extends c
  implements h
{
  private TintedImageView A;
  private TintedImageView B;
  private View C;
  private TintedImageView D;
  private View E;
  private ShineView F;
  private final com.truecaller.i.c G;
  private final g H;
  private final f I;
  private final k J;
  private final f K;
  private final boolean L;
  private com.truecaller.androidactors.a M;
  private com.truecaller.utils.i N;
  private android.arch.lifecycle.i O;
  private final bp i;
  private RippleView j;
  private TextView k;
  private TextView l;
  private TextView m;
  private TextView n;
  private TextView o;
  private TextView p;
  private View q;
  private TextView r;
  private View s;
  private ImageView t;
  private AvatarView u;
  private TextView v;
  private TextView w;
  private ViewGroup x;
  private View y;
  private View z;
  
  public b(Context paramContext, c.a parama)
  {
    super(paramContext, parama);
    paramContext = ((bk)paramContext.getApplicationContext()).a();
    i = paramContext;
    paramContext = i.D();
    G = paramContext;
    paramContext = i.aa();
    H = paramContext;
    paramContext = i.ad();
    I = paramContext;
    paramContext = i.m();
    J = paramContext;
    paramContext = i.au();
    K = paramContext;
    paramContext = i.v();
    N = paramContext;
    boolean bool = i.aF().A().a();
    L = bool;
    paramContext = new android/arch/lifecycle/i;
    paramContext.<init>(this);
    O = paramContext;
  }
  
  public static int a(Context paramContext)
  {
    Resources localResources = paramContext.getResources();
    int i1 = getDisplayMetricsheightPixels / 2;
    int i2 = com.truecaller.util.at.a(paramContext, 180.0F) / 2;
    i1 -= i2;
    i2 = com.truecaller.util.at.a(localResources);
    return i1 - i2;
  }
  
  private void a(AdCampaigns paramAdCampaigns)
  {
    if (paramAdCampaigns != null)
    {
      paramAdCampaigns = paramAdCampaigns.b();
      if (paramAdCampaigns != null)
      {
        paramAdCampaigns = b;
        break label23;
      }
    }
    paramAdCampaigns = null;
    label23:
    if (paramAdCampaigns == null) {
      return;
    }
    Object localObject1 = h.findViewById(2131362414);
    int i1 = a.getResources().getDimensionPixelSize(2131165186);
    GradientDrawable localGradientDrawable = new android/graphics/drawable/GradientDrawable;
    localGradientDrawable.<init>();
    int i2 = a;
    localGradientDrawable.setColor(i2);
    float[] arrayOfFloat = new float[8];
    arrayOfFloat[0] = 0.0F;
    arrayOfFloat[1] = 0.0F;
    arrayOfFloat[2] = 0.0F;
    arrayOfFloat[3] = 0.0F;
    float f = i1;
    arrayOfFloat[4] = f;
    arrayOfFloat[5] = f;
    arrayOfFloat[6] = f;
    arrayOfFloat[7] = f;
    localGradientDrawable.setCornerRadii(arrayOfFloat);
    com.truecaller.util.at.a((View)localObject1, localGradientDrawable);
    localObject1 = (TextView)h.findViewById(2131361975);
    i1 = b;
    ((TextView)localObject1).setTextColor(i1);
    localObject1 = (ImageView)h.findViewById(2131361973);
    Object localObject2 = com.d.b.w.a(a);
    paramAdCampaigns = e;
    paramAdCampaigns = ((com.d.b.w)localObject2).a(paramAdCampaigns);
    localObject2 = new com/truecaller/callerid/b/b$1;
    ((b.1)localObject2).<init>(this);
    paramAdCampaigns.a((ImageView)localObject1, (com.d.b.e)localObject2);
  }
  
  protected final a a()
  {
    a locala = new com/truecaller/callerid/b/a;
    int i1 = ViewConfiguration.get(a).getScaledTouchSlop();
    locala.<init>(this, i1);
    return locala;
  }
  
  protected final void a(View paramView)
  {
    Object localObject1 = paramView.findViewById(2131363270);
    s = ((View)localObject1);
    localObject1 = paramView.findViewById(2131362401);
    C = ((View)localObject1);
    localObject1 = (ImageView)paramView.findViewById(2131363990);
    t = ((ImageView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131362413);
    v = ((TextView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131362411);
    w = ((TextView)localObject1);
    localObject1 = (ViewGroup)paramView.findViewById(2131364653);
    x = ((ViewGroup)localObject1);
    localObject1 = paramView.findViewById(2131364018);
    y = ((View)localObject1);
    localObject1 = paramView.findViewById(2131362412);
    z = ((View)localObject1);
    localObject1 = (AvatarView)h.findViewById(2131362407);
    u = ((AvatarView)localObject1);
    localObject1 = t;
    Object localObject2 = a;
    int i1 = 2131100098;
    int i2 = android.support.v4.content.b.c((Context)localObject2, i1);
    ((ImageView)localObject1).setColorFilter(i2);
    localObject1 = (TintedImageView)paramView.findViewById(2131362399);
    A = ((TintedImageView)localObject1);
    localObject1 = A;
    localObject2 = new com/truecaller/callerid/b/-$$Lambda$b$GsnYxHuUuc6Gy_rihjDbuFZy3XA;
    ((-..Lambda.b.GsnYxHuUuc6Gy_rihjDbuFZy3XA)localObject2).<init>(this);
    ((TintedImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = (TintedImageView)paramView.findViewById(2131362402);
    D = ((TintedImageView)localObject1);
    localObject1 = a;
    boolean bool = co.c((Context)localObject1);
    if (bool)
    {
      localObject1 = D;
      i2 = 2131234510;
      ((TintedImageView)localObject1).setImageResource(i2);
    }
    localObject1 = (RippleView)paramView.findViewById(2131364180);
    j = ((RippleView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131362398);
    k = ((TextView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131362408);
    l = ((TextView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131362403);
    m = ((TextView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131362405);
    n = ((TextView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131362406);
    o = ((TextView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131362404);
    p = ((TextView)localObject1);
    localObject1 = paramView.findViewById(2131363941);
    q = ((View)localObject1);
    localObject1 = (TintedImageView)paramView.findViewById(2131362410);
    B = ((TintedImageView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131362271);
    r = ((TextView)localObject1);
    localObject1 = paramView.findViewById(2131363192);
    E = ((View)localObject1);
    paramView = (ShineView)paramView.findViewById(2131363196);
    F = paramView;
    F.setLifecycleOwner(this);
    paramView = u;
    localObject1 = new com/truecaller/callerid/b/-$$Lambda$b$nIGAkseUnfoZb7dH7T3TAACRPos;
    ((-..Lambda.b.nIGAkseUnfoZb7dH7T3TAACRPos)localObject1).<init>(this);
    paramView.addOnLayoutChangeListener((View.OnLayoutChangeListener)localObject1);
  }
  
  protected final void a(com.truecaller.callerid.i parami, boolean paramBoolean)
  {
    b localb = this;
    Object localObject1 = parami;
    Object localObject2 = l;
    int i1 = 0;
    Object localObject3 = null;
    Object localObject4 = new String[0];
    AssertionUtil.isNotNull(localObject2, (String[])localObject4);
    int i6 = 8;
    if (paramBoolean)
    {
      localObject4 = t;
      ((ImageView)localObject4).setVisibility(i6);
    }
    localObject4 = "";
    w.setMaxLines(3);
    Object localObject5 = w;
    float f1 = 12.0F;
    ((TextView)localObject5).setTextSize(f1);
    int i11 = 117;
    boolean bool10 = L;
    if (bool10) {
      i11 = 245;
    }
    Object localObject6 = l;
    boolean bool8 = ((Contact)localObject6).c(i11);
    int i12;
    if (!bool8)
    {
      localObject5 = N;
      bool8 = ((com.truecaller.utils.i)localObject5).a();
      if (!bool8)
      {
        localObject4 = a;
        i12 = 2131886344;
        localObject4 = ((Context)localObject4).getString(i12);
      }
    }
    localObject5 = l.t();
    bool10 = am.a((CharSequence)localObject5);
    if (!bool10) {
      localObject5 = com.truecaller.util.at.a(a.n());
    }
    localObject6 = l;
    bool10 = ((Contact)localObject6).a(16);
    Object localObject7 = l;
    int i15 = 32;
    float f2 = 4.5E-44F;
    boolean bool13 = ((Contact)localObject7).a(i15);
    Object localObject8 = l;
    int i19;
    if (bool10)
    {
      i19 = 0;
      localObject9 = null;
    }
    else
    {
      i19 = 8;
    }
    ((TextView)localObject8).setVisibility(i19);
    localObject8 = l;
    Object localObject9 = u;
    Object localObject10 = l;
    ((AvatarView)localObject9).a((Contact)localObject10);
    boolean bool16 = ((Contact)localObject8).a(i15);
    localObject10 = null;
    Object localObject12;
    int i16;
    boolean bool17;
    if (bool16)
    {
      bool16 = parami.a();
      if (!bool16)
      {
        s.setBackground(null);
        com.truecaller.util.at.a(C, 2131230999);
        E.setVisibility(0);
        F.setVisibility(0);
        localObject9 = a;
        i20 = android.support.v4.content.b.c((Context)localObject9, 2131100383);
        Context localContext1 = a;
        int i21 = android.support.v4.content.b.c(localContext1, 2131100384);
        Object localObject11 = a;
        float f3 = 1.0F;
        int i22 = com.truecaller.util.at.a((Context)localObject11, f3);
        localObject2 = a;
        f2 = 1.7813142E38F;
        i6 = android.support.v4.content.b.c((Context)localObject2, 2131100381);
        localObject12 = A;
        Context localContext2 = a;
        i1 = android.support.v4.content.b.c(localContext2, 2131100380);
        ((TintedImageView)localObject12).setTint(i1);
        D.setTint(null);
        localObject3 = D;
        localObject12 = a;
        boolean bool12 = co.c((Context)localObject12);
        if (bool12)
        {
          i16 = 2131234630;
          f2 = 1.8085431E38F;
        }
        else
        {
          i16 = 2131234629;
          f2 = 1.808543E38F;
        }
        ((TintedImageView)localObject3).setImageResource(i16);
        v.setTextColor(i20);
        localObject3 = v;
        f2 = i22;
        bool17 = false;
        localContext2 = null;
        ((TextView)localObject3).setShadowLayer(f3, 0.0F, f2, i6);
        localObject3 = v;
        localObject11 = Typeface.create("sans-serif-medium", 0);
        ((TextView)localObject3).setTypeface((Typeface)localObject11);
        w.setTextColor(i21);
        w.setShadowLayer(f3, 0.0F, f2, i6);
        k.setTextColor(i21);
        k.setShadowLayer(f3, 0.0F, f2, i6);
        r.setTextColor(i21);
        r.setShadowLayer(f3, 0.0F, f2, i6);
        m.setTextColor(i21);
        m.setShadowLayer(f3, 0.0F, f2, i6);
        n.setTextColor(i20);
        n.setShadowLayer(f3, 0.0F, f2, i6);
        o.setTextColor(i21);
        o.setShadowLayer(f3, 0.0F, f2, i6);
        p.setTextColor(i21);
        localObject3 = p;
        ((TextView)localObject3).setShadowLayer(f3, 0.0F, f2, i6);
        localObject2 = new com/truecaller/ui/components/b;
        i1 = 1;
        localObject12 = ((Contact)localObject8).a(i1);
        i20 = 0;
        localObject9 = null;
        localObject10 = ((Contact)localObject8).a(false);
        ((com.truecaller.ui.components.b)localObject2).<init>((Uri)localObject12, (Uri)localObject10, i1, false);
        localObject12 = u;
        ((AvatarView)localObject12).a((com.truecaller.ui.components.b)localObject2);
        break label1029;
      }
    }
    boolean bool1 = true;
    int i20 = 0;
    localObject9 = null;
    i6 = 4;
    int i7;
    if (bool10)
    {
      localObject12 = new com/truecaller/ui/components/b;
      localObject10 = ((Contact)localObject8).a(bool1);
      localObject3 = ((Contact)localObject8).a(false);
      i20 = 32;
      bool17 = ((Contact)localObject8).a(i20);
      i7 = ((Contact)localObject8).a(i6);
      ((com.truecaller.ui.components.b)localObject12).<init>((Uri)localObject10, (Uri)localObject3, bool17, i7);
      u.a((com.truecaller.ui.components.b)localObject12);
      localObject2 = s;
      int i2 = 2130969576;
      com.truecaller.utils.ui.b.a((View)localObject2, i2);
    }
    else
    {
      boolean bool2 = parami.a();
      int i3;
      if (bool2)
      {
        u.a();
        localObject2 = s;
        i3 = 2130969585;
        com.truecaller.utils.ui.b.a((View)localObject2, i3);
      }
      else
      {
        i3 = 1;
        localObject12 = ((Contact)localObject8).a(i3);
        localObject3 = new com/truecaller/ui/components/b;
        localObject9 = null;
        localObject10 = ((Contact)localObject8).a(false);
        i20 = 32;
        bool17 = ((Contact)localObject8).a(i20);
        bool4 = ((Contact)localObject8).a(i7);
        ((com.truecaller.ui.components.b)localObject3).<init>((Uri)localObject12, (Uri)localObject10, bool17, bool4);
        localObject2 = u;
        ((AvatarView)localObject2).a((com.truecaller.ui.components.b)localObject3);
      }
    }
    label1029:
    boolean bool4 = parami.a();
    int i4 = 2;
    int i13;
    if (bool4)
    {
      localObject2 = l;
      localObject6 = a;
      int i8 = com.truecaller.util.w.b((Contact)localObject2, (Number)localObject6);
      if (i8 > 0)
      {
        localObject4 = g();
        i13 = 2131886146;
        f1 = 1.9406863E38F;
        localObject12 = new Object[1];
        localObject2 = Integer.valueOf(i8);
        bool13 = false;
        localObject7 = null;
        localObject12[0] = localObject2;
        localObject4 = ((Context)localObject4).getString(i13, (Object[])localObject12);
      }
    }
    else
    {
      bool5 = am.a((CharSequence)localObject4);
      if (!bool5)
      {
        localObject2 = l.g();
        if (localObject2 != null)
        {
          localObject12 = new CharSequence[i4];
          localObject9 = ((Address)localObject2).getCityOrArea();
          localObject10 = null;
          localObject12[0] = localObject9;
          localObject2 = ((Address)localObject2).getCountryName();
          i20 = 1;
          localObject12[i20] = localObject2;
          localObject2 = am.a(", ", (CharSequence[])localObject12);
          localObject4 = localObject2;
        }
        else
        {
          i20 = 1;
          localObject2 = "";
          localObject4 = localObject2;
        }
        w.setMaxLines(i20);
        localObject2 = w;
        i16 = 1096810496;
        f2 = 14.0F;
        ((TextView)localObject2).setTextSize(f2);
      }
      else
      {
        i20 = 1;
      }
      localObject2 = l.a(i20);
      if ((i13 == 0) && (!bool13))
      {
        localObject6 = s;
        int i17 = 2130968622;
        com.truecaller.utils.ui.b.a((View)localObject6, i17);
        if (localObject2 != null)
        {
          localObject2 = com.d.b.w.a(a).a((Uri)localObject2);
          c = i20;
          localObject2 = ((ab)localObject2).b().a();
          localObject6 = new com/truecaller/common/h/aq$b;
          ((aq.b)localObject6).<init>();
          localObject2 = ((ab)localObject2).a((ai)localObject6);
          bool11 = Settings.d();
          if (!bool11)
          {
            localObject6 = t.c;
            i17 = 0;
            localObject7 = null;
            localObject12 = new t[0];
            ((ab)localObject2).a((t)localObject6);
          }
          else
          {
            i17 = 0;
            localObject7 = null;
          }
          t.setVisibility(0);
          localObject6 = t;
          localObject7 = new com/truecaller/callerid/b/b$2;
          ((b.2)localObject7).<init>(localb);
          ((ab)localObject2).a((ImageView)localObject6, (com.d.b.e)localObject7);
        }
        else
        {
          localObject2 = t;
          bool11 = false;
          f1 = 0.0F;
          localObject6 = null;
          ((ImageView)localObject2).setImageBitmap(null);
        }
      }
    }
    boolean bool5 = k;
    boolean bool11 = true;
    f1 = Float.MIN_VALUE;
    bool5 ^= bool11;
    localObject6 = y;
    boolean bool14 = k;
    com.truecaller.util.at.a((View)localObject6, bool14);
    localObject6 = v;
    com.truecaller.util.at.b((TextView)localObject6, (CharSequence)localObject5);
    localObject5 = w;
    com.truecaller.util.at.b((TextView)localObject5, (CharSequence)localObject4);
    localObject4 = l;
    boolean bool18 = ((Contact)localObject4).a(i4);
    if (bool18)
    {
      localObject4 = g();
      i12 = 2131233985;
      localObject10 = com.truecaller.util.at.a((Context)localObject4, i12);
    }
    else
    {
      localObject10 = null;
    }
    localObject4 = v;
    bool11 = false;
    f1 = 0.0F;
    localObject6 = null;
    m.a((TextView)localObject4, null, (Drawable)localObject10);
    int i18;
    boolean bool9;
    if (bool5)
    {
      localObject4 = a;
      localObject5 = l;
      if (localObject5 != null)
      {
        i18 = 32;
        bool9 = ((Contact)localObject5).a(i18);
        if (bool9) {}
      }
      else
      {
        localObject4 = ((Number)localObject4).a();
        bool9 = Settings.h();
        if (bool9)
        {
          localObject5 = G;
          localObject7 = "afterCall";
          bool9 = ((com.truecaller.i.c)localObject5).b((String)localObject7);
          if ((bool9) && (localObject4 != null))
          {
            localObject5 = (com.truecaller.ads.provider.a.b)K.a();
            localObject7 = new com/truecaller/ads/j$a;
            localObject12 = "CALLERID";
            ((j.a)localObject7).<init>((String)localObject12);
            a = ((String)localObject4);
            localObject4 = ((j.a)localObject7).a();
            localObject4 = ((com.truecaller.ads.provider.a.b)localObject5).a((j)localObject4);
            localObject5 = J.a();
            localObject7 = new com/truecaller/callerid/b/-$$Lambda$b$9kbigglasJRfuEOsFnuvk8Ke7FA;
            ((-..Lambda.b.9kbigglasJRfuEOsFnuvk8Ke7FA)localObject7).<init>(localb);
            localObject4 = ((com.truecaller.androidactors.w)localObject4).a((com.truecaller.androidactors.i)localObject5, (ac)localObject7);
            M = ((com.truecaller.androidactors.a)localObject4);
          }
        }
      }
    }
    localObject4 = x;
    int i23;
    if (localObject4 != null)
    {
      if (bool5)
      {
        localObject2 = l.J();
        localObject10 = ce.a((List)localObject2);
        localObject6 = localObject10;
      }
      if (localObject6 != null)
      {
        x.removeAllViews();
        localObject2 = new com/truecaller/common/tag/TagView;
        localObject4 = g();
        bool9 = true;
        ((TagView)localObject2).<init>((Context)localObject4, bool9, bool9);
        ((TagView)localObject2).setTag((com.truecaller.common.tag.c)localObject6);
        x.addView((View)localObject2);
        localObject2 = x;
        localObject4 = null;
        ((ViewGroup)localObject2).setVisibility(0);
        i23 = 8;
      }
      else
      {
        localObject2 = x;
        i23 = 8;
        ((ViewGroup)localObject2).setVisibility(i23);
      }
    }
    else
    {
      i23 = 8;
    }
    localObject2 = l;
    int i24;
    int i5;
    if (localObject2 != null)
    {
      if (paramBoolean)
      {
        localObject2 = a.d();
        bool5 = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool5)
        {
          p.setVisibility(i23);
          localObject2 = (com.truecaller.callhistory.a)I.a();
          localObject4 = a.a();
          localObject2 = ((com.truecaller.callhistory.a)localObject2).c((String)localObject4);
          localObject4 = J.a();
          localObject5 = new com/truecaller/callerid/b/-$$Lambda$b$-XX1C3u_MwwTadPF7h9-8zqgp74;
          ((-..Lambda.b.-XX1C3u_MwwTadPF7h9-8zqgp74)localObject5).<init>(localb);
          ((com.truecaller.androidactors.w)localObject2).a((com.truecaller.androidactors.i)localObject4, (ac)localObject5);
        }
      }
      localObject2 = k;
      localObject4 = l.K();
      localObject5 = l.l();
      localObject4 = am.e((CharSequence)localObject4, (CharSequence)localObject5);
      com.truecaller.util.at.b((TextView)localObject2, (CharSequence)localObject4);
      localObject2 = m;
      localObject5 = new CharSequence[i4];
      localObject6 = l.x();
      localObject7 = null;
      localObject5[0] = localObject6;
      localObject6 = l.o();
      i18 = 1;
      localObject5[i18] = localObject6;
      localObject4 = am.a(" @ ", (CharSequence[])localObject5);
      com.truecaller.util.at.b((TextView)localObject2, (CharSequence)localObject4);
      localObject2 = l;
      localObject4 = a;
      localObject2 = com.truecaller.util.w.a((Contact)localObject2, (Number)localObject4);
      if (localObject2 == null) {
        localObject2 = a;
      }
      localObject4 = H.a((Number)localObject2);
      localObject5 = "";
      localObject6 = ((Number)localObject2).g();
      localObject2 = ((Number)localObject2).n();
      boolean bool15 = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool15)
      {
        localObject5 = android.support.v4.e.a.a();
        localObject7 = android.support.v4.e.e.a;
        localObject5 = ((android.support.v4.e.a)localObject5).a((String)localObject2, (d)localObject7);
      }
      com.truecaller.util.at.b(n, (CharSequence)localObject5);
      localObject2 = o;
      localObject5 = " - ";
      localObject3 = new CharSequence[i4];
      localObject10 = null;
      localObject3[0] = localObject4;
      i24 = 1;
      localObject3[i24] = localObject6;
      localObject3 = am.a((String)localObject5, (CharSequence[])localObject3);
      com.truecaller.util.at.b((TextView)localObject2, (CharSequence)localObject3);
      localObject2 = j;
      boolean bool3 = e;
      if (bool3)
      {
        bool3 = false;
        localObject3 = null;
      }
      else
      {
        i5 = 8;
      }
      ((RippleView)localObject2).setVisibility(i5);
      int i9 = b;
      switch (i9)
      {
      default: 
        localObject2 = B;
        i5 = 8;
        ((TintedImageView)localObject2).setVisibility(i5);
        break;
      case 1: 
        localObject2 = B;
        ((TintedImageView)localObject2).setImageResource(2131234550);
        i5 = 8;
        break;
      case 0: 
        localObject2 = B;
        ((TintedImageView)localObject2).setImageResource(2131234549);
        i5 = 8;
        break;
      }
    }
    else
    {
      i5 = 8;
      localObject10 = null;
      i24 = 1;
    }
    if (paramBoolean)
    {
      localObject2 = new com/truecaller/analytics/e$a;
      ((e.a)localObject2).<init>("CALLERID_CallerIDWindow_Viewed");
      localObject4 = l;
      if (localObject4 != null)
      {
        localObject4 = l;
        bool19 = ((Contact)localObject4).Z();
        if (bool19)
        {
          bool19 = e;
          if (bool19)
          {
            localObject4 = "IncomingCallPhonebook";
            break label2549;
          }
          localObject4 = "OutgoingCallPhonebook";
          break label2549;
        }
      }
      boolean bool19 = e;
      if (bool19) {
        localObject4 = "IncomingCall";
      } else {
        localObject4 = "OutgoingCall";
      }
      label2549:
      ((e.a)localObject2).a("Call_Type", (String)localObject4);
      localObject4 = "IsGold";
      localObject5 = l;
      if (localObject5 != null)
      {
        localObject5 = l;
        int i14 = 32;
        f1 = 4.5E-44F;
        bool9 = ((Contact)localObject5).a(i14);
        if (bool9)
        {
          bool9 = true;
          break label2619;
        }
      }
      bool9 = false;
      localObject5 = null;
      label2619:
      ((e.a)localObject2).a((String)localObject4, bool9);
      localObject4 = "IsBusiness";
      localObject5 = l;
      if (localObject5 != null)
      {
        localObject1 = l;
        bool20 = ((Contact)localObject1).N();
        if (bool20)
        {
          bool20 = true;
          break label2677;
        }
      }
      boolean bool20 = false;
      localObject1 = null;
      label2677:
      ((e.a)localObject2).a((String)localObject4, bool20);
      localObject1 = TrueApp.y().a().c();
      localObject2 = ((e.a)localObject2).a();
      ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject2);
    }
    localObject1 = k;
    boolean bool6 = ((Contact)localObject8).N();
    int i10;
    if (bool6)
    {
      i10 = 8;
    }
    else
    {
      i10 = 0;
      localObject2 = null;
    }
    ((TextView)localObject1).setVisibility(i10);
    localObject1 = r;
    boolean bool7 = ((Contact)localObject8).N();
    if (bool7)
    {
      i5 = 0;
      localObject3 = null;
    }
    ((TextView)localObject1).setVisibility(i5);
  }
  
  protected final void b()
  {
    Object localObject1 = O;
    Object localObject2 = e.b.e;
    ((android.arch.lifecycle.i)localObject1).a((e.b)localObject2);
    localObject1 = h;
    int i1 = 2131363887;
    localObject1 = ((View)localObject1).findViewById(i1);
    localObject2 = com.truecaller.util.b.at.b(a);
    boolean bool1 = ((j.b)localObject2).a();
    int i3 = 1;
    int i4 = 8;
    if (!bool1)
    {
      ((View)localObject1).setVisibility(i4);
    }
    else
    {
      int i2 = a.getResources().getDimensionPixelSize(2131165186);
      GradientDrawable localGradientDrawable = new android/graphics/drawable/GradientDrawable;
      localGradientDrawable.<init>();
      int i5 = e;
      localGradientDrawable.setColor(i5);
      float[] arrayOfFloat = new float[i4];
      arrayOfFloat[0] = 0.0F;
      arrayOfFloat[i3] = 0.0F;
      arrayOfFloat[2] = 0.0F;
      int i6 = 3;
      arrayOfFloat[i6] = 0.0F;
      float f = i2;
      arrayOfFloat[4] = f;
      arrayOfFloat[5] = f;
      arrayOfFloat[6] = f;
      int i7 = 7;
      arrayOfFloat[i7] = f;
      localGradientDrawable.setCornerRadii(arrayOfFloat);
      com.truecaller.util.at.a((View)localObject1, localGradientDrawable);
      Object localObject3 = (ImageView)h.findViewById(2131363886);
      int i8 = d;
      ((ImageView)localObject3).setImageResource(i8);
      i2 = 2131363968;
      f = 1.834776E38F;
      localObject3 = (TextView)((View)localObject1).findViewById(i2);
      i1 = f;
      ((TextView)localObject3).setTextColor(i1);
      ((View)localObject1).setVisibility(0);
    }
    localObject1 = com.truecaller.util.b.at.a(a);
    boolean bool2 = localObject1 instanceof ao;
    if (!bool2)
    {
      localObject1 = G;
      localObject2 = "callerIdHintCount";
      int i9 = ((com.truecaller.i.c)localObject1).a((String)localObject2, 0);
      if (i9 <= 0) {}
    }
    else
    {
      i3 = 0;
    }
    if (i3 != 0)
    {
      localObject1 = z;
      ((View)localObject1).setVisibility(0);
    }
    else
    {
      localObject1 = z;
      ((View)localObject1).setVisibility(i4);
    }
    localObject1 = h;
    localObject2 = new com/truecaller/callerid/b/-$$Lambda$b$w1_21gPSzMPLjOne--ld7p6B4GQ;
    ((-..Lambda.b.w1_21gPSzMPLjOne--ld7p6B4GQ)localObject2).<init>(this);
    ((View)localObject1).postDelayed((Runnable)localObject2, 1000L);
  }
  
  public final void c()
  {
    Object localObject = O;
    e.b localb = e.b.c;
    ((android.arch.lifecycle.i)localObject).a(localb);
    localObject = M;
    if (localObject != null)
    {
      ((com.truecaller.androidactors.a)localObject).a();
      localObject = null;
      M = null;
    }
    super.c();
  }
  
  public final android.arch.lifecycle.e getLifecycle()
  {
    return O;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */