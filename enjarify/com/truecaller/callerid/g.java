package com.truecaller.callerid;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.z.d;
import android.support.v4.app.z.f;
import android.support.v4.app.z.g;
import android.text.format.DateUtils;
import c.g.b.k;
import com.truecaller.calling.notifications.CallingNotificationsBroadcastReceiver;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.common.h.j;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.i.c;
import com.truecaller.notifications.a;
import com.truecaller.service.MissedCallsNotificationService;
import com.truecaller.service.Receiver;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.util.bl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class g
  implements e
{
  private final Context a;
  private final c b;
  private final com.truecaller.notificationchannels.e c;
  private final com.truecaller.notificationchannels.b d;
  private final a e;
  
  public g(Context paramContext, c paramc, com.truecaller.notificationchannels.e parame, com.truecaller.notificationchannels.b paramb, a parama)
  {
    a = paramContext;
    b = paramc;
    c = parame;
    d = paramb;
    e = parama;
  }
  
  private void a(List paramList, int paramInt1, int paramInt2)
  {
    g localg = this;
    int i = paramInt1;
    Object localObject1 = a.getResources();
    int j = paramList.size();
    int k = 1;
    Object localObject2 = new Object[k];
    Object localObject3 = Integer.valueOf(paramList.size());
    localObject2[0] = localObject3;
    int m = paramInt2;
    Object localObject4 = ((Resources)localObject1).getQuantityString(paramInt2, j, (Object[])localObject2);
    localObject2 = new android/support/v4/app/z$d;
    localObject3 = a;
    Object localObject5 = c.a();
    ((z.d)localObject2).<init>((Context)localObject3, (String)localObject5);
    localObject2 = ((z.d)localObject2).a(2131234787);
    localObject3 = a;
    int n = 2131100594;
    m = android.support.v4.content.b.c((Context)localObject3, n);
    C = m;
    if (paramInt1 != 0) {
      localObject1 = ((Resources)localObject1).getString(paramInt1);
    } else {
      localObject1 = localObject4;
    }
    localObject1 = ((z.d)localObject2).a((CharSequence)localObject1);
    localObject2 = new android/support/v4/app/z$f;
    ((z.f)localObject2).<init>();
    ((z.f)localObject2).a((CharSequence)localObject4);
    m = paramList.size() - k;
    Object localObject9;
    Object localObject10;
    while (m >= 0)
    {
      localObject5 = paramList;
      Object localObject6 = (com.truecaller.old.data.entity.e)paramList.get(m);
      Context localContext = a;
      int i1 = 2;
      Object[] arrayOfObject = new Object[i1];
      long l1 = a;
      boolean bool2 = DateUtils.isToday(l1);
      Object localObject7;
      Object localObject8;
      if (bool2)
      {
        localObject7 = a;
        localObject8 = localObject2;
        long l2 = a;
        localObject9 = j.f((Context)localObject7, l2);
      }
      else
      {
        localObject8 = localObject2;
        localObject9 = a;
        long l3 = a;
        localObject9 = j.e((Context)localObject9, l3);
      }
      arrayOfObject[0] = localObject9;
      localObject9 = a;
      localObject10 = b;
      String str = b;
      boolean bool1 = ab.a(str);
      localObject9 = ab.a((Context)localObject9, (String)localObject10, bool1);
      localObject10 = c;
      boolean bool3 = am.a((CharSequence)localObject10);
      if (bool3)
      {
        localObject10 = a;
        int i2 = 2131886766;
        int i3 = 2;
        localObject7 = new Object[i3];
        localObject6 = c;
        localObject7[0] = localObject6;
        i4 = 1;
        localObject7[i4] = localObject9;
        localObject9 = ((Context)localObject10).getString(i2, (Object[])localObject7);
      }
      else
      {
        i4 = 1;
      }
      arrayOfObject[i4] = localObject9;
      localObject9 = localContext.getString(2131886768, arrayOfObject);
      localObject2 = localObject8;
      ((z.f)localObject8).c((CharSequence)localObject9);
      m += -1;
      k = 1;
    }
    localObject5 = paramList;
    int i4 = 1;
    k = paramList.size();
    m = 5;
    if (k > m)
    {
      localObject9 = a;
      int i5 = 2131888375;
      localObject10 = new Object[i4];
      n = paramList.size() - m;
      localObject3 = Integer.valueOf(n);
      localObject10[0] = localObject3;
      localObject9 = ((Context)localObject9).getString(i5, (Object[])localObject10);
      ((z.f)localObject2).b((CharSequence)localObject9);
    }
    if (i == 0)
    {
      localObject11 = a;
      j = 2131886774;
      localObject4 = ((Context)localObject11).getString(j);
    }
    ((z.d)localObject1).b((CharSequence)localObject4);
    ((z.d)localObject1).a((z.g)localObject2);
    Object localObject11 = new android/content/Intent;
    localObject4 = a;
    ((Intent)localObject11).<init>((Context)localObject4, Receiver.class);
    ((Intent)localObject11).setAction("com.truecaller.intent.action.PHONE_NOTIFICATION_CANCELLED");
    ((Intent)localObject11).putExtra("notificationType", 1);
    localObject4 = TruecallerInit.a(a, "calls", "notificationBlockedCall", "openApp");
    localObject11 = PendingIntent.getBroadcast(a, 2131364140, (Intent)localObject11, 268435456);
    ((z.d)localObject1).b((PendingIntent)localObject11);
    localObject11 = bl.a(a, (Intent)localObject4, 2131364141);
    f = ((PendingIntent)localObject11);
    ((z.d)localObject1).d(16);
    l = 0;
    ((z.d)localObject1).c(0);
    localObject11 = e;
    localObject1 = ((z.d)localObject1).h();
    ((a)localObject11).a("OsNotificationUtils", 222, (Notification)localObject1, "notificationBlockedCall");
  }
  
  public final void a()
  {
    Object localObject1 = new android/support/v4/app/z$d;
    Object localObject2 = a;
    Object localObject3 = c.a();
    ((z.d)localObject1).<init>((Context)localObject2, (String)localObject3);
    localObject1 = ((z.d)localObject1).a(2131234787);
    int i = android.support.v4.content.b.c(a, 2131100594);
    C = i;
    localObject2 = a.getString(2131886306);
    localObject1 = ((z.d)localObject1).a((CharSequence)localObject2);
    ((z.d)localObject1).d(16);
    ((z.d)localObject1).d(2);
    localObject2 = a;
    k.b(localObject2, "context");
    localObject3 = new android/content/Intent;
    ((Intent)localObject3).<init>((Context)localObject2, CallingNotificationsBroadcastReceiver.class);
    ((Intent)localObject3).setAction("com.truecaller.request_set_as_default_phone_app");
    localObject2 = PendingIntent.getBroadcast((Context)localObject2, 0, (Intent)localObject3, 268435456);
    k.a(localObject2, "PendingIntent.getBroadca…tent.FLAG_CANCEL_CURRENT)");
    f = ((PendingIntent)localObject2);
    localObject2 = a.getString(2131886305);
    localObject1 = ((z.d)localObject1).b((CharSequence)localObject2).h();
    e.a(2131365393, (Notification)localObject1, "notificationUnableToBlockCall");
  }
  
  public final void a(i parami)
  {
    ??? = l;
    if (??? != null)
    {
      boolean bool1 = parami.c();
      if (bool1)
      {
        Object localObject2 = b;
        Object localObject3 = "blockCallNotification";
        int k = 1;
        bool1 = ((c)localObject2).a((String)localObject3, k);
        if (!bool1) {
          return;
        }
        int i = h;
        int n = 0;
        localObject3 = null;
        if (i == k)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localObject2 = null;
        }
        com.truecaller.old.data.access.g localg = new com/truecaller/old/data/access/g;
        Object localObject4 = a;
        localg.<init>((Context)localObject4);
        com.truecaller.old.data.entity.e locale = new com/truecaller/old/data/entity/e;
        long l1 = d;
        String str1 = a.d();
        Object localObject5 = ((Contact)???).t();
        int i1 = parami.b();
        String str2 = String.valueOf(i1);
        localObject4 = locale;
        locale.<init>(l1, str1, (String)localObject5, str2, i);
        synchronized (com.truecaller.old.data.access.b.a)
        {
          localObject4 = localg.a();
          ((List)localObject4).remove(locale);
          ((List)localObject4).add(locale);
          localg.c();
          ??? = new java/util/ArrayList;
          localObject4 = localg.f();
          ((ArrayList)???).<init>((Collection)localObject4);
          int i3 = ((List)???).size();
          int j;
          if (i3 == k)
          {
            ??? = l;
            if (??? != null)
            {
              localObject2 = b;
              localObject4 = "blockCallNotification";
              boolean bool2 = ((c)localObject2).a((String)localObject4, k);
              if (bool2)
              {
                localObject2 = d.g();
                localObject4 = new android/support/v4/app/z$d;
                Object localObject6 = a;
                ((z.d)localObject4).<init>((Context)localObject6, (String)localObject2);
                j = 2131234787;
                localObject2 = ((z.d)localObject4).a(j);
                int i4 = android.support.v4.content.b.c(a, 2131100594);
                C = i4;
                localObject4 = a;
                int i5 = 2131886117;
                localObject4 = ((Context)localObject4).getString(i5);
                localObject2 = ((z.d)localObject2).d((CharSequence)localObject4);
                localObject4 = a.n();
                localObject6 = TruecallerInit.a(a, "calls", "notificationBlockedCall");
                Object localObject7 = a;
                str1 = a.d();
                boolean bool4 = ab.a(str1);
                localObject4 = ab.a((Context)localObject7, (String)localObject4, bool4);
                localObject7 = ((Contact)???).t();
                boolean bool5 = am.a((CharSequence)localObject7);
                if (bool5)
                {
                  localObject7 = a;
                  int i6 = 2131886766;
                  int i8 = 2;
                  localObject5 = new Object[i8];
                  ??? = ((Contact)???).t();
                  localObject5[0] = ???;
                  localObject5[k] = localObject4;
                  localObject4 = ((Context)localObject7).getString(i6, (Object[])localObject5);
                }
                i1 = 2131886779;
                n = 2131234317;
                int i9 = h;
                if (i9 == k) {
                  i1 = 2131886777;
                } else {
                  n = 2131234331;
                }
                ((z.d)localObject2).a(n);
                parami = BitmapFactory.decodeResource(a.getResources(), 2131234320);
                ((z.d)localObject2).a(parami);
                parami = a.getString(i1);
                ??? = new android/content/Intent;
                localObject3 = a;
                localObject7 = Receiver.class;
                ((Intent)???).<init>((Context)localObject3, (Class)localObject7);
                ((Intent)???).setAction("com.truecaller.intent.action.PHONE_NOTIFICATION_CANCELLED");
                ((Intent)???).putExtra("notificationType", k);
                localObject3 = a;
                int m = 2131364140;
                int i7 = 268435456;
                ??? = PendingIntent.getBroadcast((Context)localObject3, m, (Intent)???, i7);
                ((z.d)localObject2).b((PendingIntent)???);
                ??? = new java/lang/StringBuilder;
                ((StringBuilder)???).<init>("truecaller://");
                long l2 = System.currentTimeMillis();
                ((StringBuilder)???).append(l2);
                ??? = Uri.parse(((StringBuilder)???).toString());
                ((Intent)localObject6).setData((Uri)???);
                boolean bool3 = am.a((CharSequence)localObject4);
                n = 0;
                localObject3 = null;
                if (!bool3)
                {
                  i4 = 0;
                  localObject4 = null;
                }
                parami = ((z.d)localObject2).a(parami).b((CharSequence)localObject4).c(null);
                ??? = bl.a(a, (Intent)localObject6, 2131364141);
                f = ((PendingIntent)???);
                int i2 = 16;
                parami.d(i2);
                parami = e;
                n = 222;
                parami.a("OsNotificationUtils", n);
                parami = e;
                ??? = "OsNotificationUtils";
                localObject2 = ((z.d)localObject2).h();
                String str3 = "notificationBlockedCall";
                parami.a((String)???, n, (Notification)localObject2, str3);
              }
            }
            return;
          }
          int i10;
          if (j != 0) {
            i10 = 2131886773;
          } else {
            i10 = 2131886775;
          }
          if (j != 0) {
            j = 2131755030;
          } else {
            j = 2131755031;
          }
          a((List)???, i10, j);
          return;
        }
      }
    }
  }
  
  public final void b()
  {
    Object localObject1 = new android/support/v4/app/z$d;
    Object localObject2 = a;
    Object localObject3 = c.a();
    ((z.d)localObject1).<init>((Context)localObject2, (String)localObject3);
    localObject1 = ((z.d)localObject1).a(2131234787);
    int i = android.support.v4.content.b.c(a, 2131100594);
    C = i;
    localObject2 = a.getString(2131886304);
    localObject1 = ((z.d)localObject1).a((CharSequence)localObject2);
    ((z.d)localObject1).d(16);
    ((z.d)localObject1).d(2);
    localObject2 = a;
    k.b(localObject2, "context");
    localObject3 = new android/content/Intent;
    ((Intent)localObject3).<init>((Context)localObject2, CallingNotificationsBroadcastReceiver.class);
    ((Intent)localObject3).setAction("com.truecaller.request_allow_draw_over_other_apps");
    localObject2 = PendingIntent.getBroadcast((Context)localObject2, 0, (Intent)localObject3, 268435456);
    k.a(localObject2, "PendingIntent.getBroadca…tent.FLAG_CANCEL_CURRENT)");
    f = ((PendingIntent)localObject2);
    localObject2 = a.getString(2131886303);
    localObject1 = ((z.d)localObject1).b((CharSequence)localObject2).h();
    e.a(2131362902, (Notification)localObject1, "notificationDrawOverOtherApps");
  }
  
  public final void c()
  {
    Object localObject = new android/support/v4/app/z$d;
    Context localContext = a;
    String str = c.a();
    ((z.d)localObject).<init>(localContext, str);
    localObject = ((z.d)localObject).a(2131234787);
    int i = android.support.v4.content.b.c(a, 2131100594);
    C = i;
    localObject = ((z.d)localObject).a("Call Recording");
    ((z.d)localObject).d(2);
    localObject = ((z.d)localObject).b("In Progress").h();
    e.a(2131362383, (Notification)localObject, "notificationCallRecording");
  }
  
  public final void d()
  {
    e.a(2131362383);
  }
  
  public final void e()
  {
    MissedCallsNotificationService.a(a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */