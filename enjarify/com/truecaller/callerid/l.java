package com.truecaller.callerid;

import com.truecaller.aftercall.PromotionType;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.i.c;

public final class l
  implements k
{
  private final v a;
  
  public l(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return k.class.equals(paramClass);
  }
  
  public final void a()
  {
    v localv = a;
    l.f localf = new com/truecaller/callerid/l$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, (byte)0);
    localv.a(localf);
  }
  
  public final void a(PromotionType paramPromotionType, HistoryEvent paramHistoryEvent, c paramc)
  {
    v localv = a;
    l.d locald = new com/truecaller/callerid/l$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramPromotionType, paramHistoryEvent, paramc, (byte)0);
    localv.a(locald);
  }
  
  public final void a(i parami, boolean paramBoolean)
  {
    v localv = a;
    l.g localg = new com/truecaller/callerid/l$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, parami, paramBoolean, (byte)0);
    localv.a(localg);
  }
  
  public final void a(HistoryEvent paramHistoryEvent, int paramInt)
  {
    v localv = a;
    l.e locale = new com/truecaller/callerid/l$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, paramHistoryEvent, paramInt, (byte)0);
    localv.a(locale);
  }
  
  public final void b()
  {
    v localv = a;
    l.b localb = new com/truecaller/callerid/l$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, (byte)0);
    localv.a(localb);
  }
  
  public final void c()
  {
    v localv = a;
    l.a locala = new com/truecaller/callerid/l$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, (byte)0);
    localv.a(locala);
  }
  
  public final w d()
  {
    v localv = a;
    l.c localc = new com/truecaller/callerid/l$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, (byte)0);
    return w.a(localv, localc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */