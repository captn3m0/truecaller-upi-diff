package com.truecaller.callerid;

import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d
{
  private final q a;
  private final Provider b;
  private final Provider c;
  
  private y(q paramq, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramq;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static y a(q paramq, Provider paramProvider1, Provider paramProvider2)
  {
    y localy = new com/truecaller/callerid/y;
    localy.<init>(paramq, paramProvider1, paramProvider2);
    return localy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */