package com.truecaller.callerid;

import com.truecaller.androidactors.v;

public final class f
  implements e
{
  private final v a;
  
  public f(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return e.class.equals(paramClass);
  }
  
  public final void a()
  {
    v localv = a;
    f.f localf = new com/truecaller/callerid/f$f;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, (byte)0);
    localv.a(localf);
  }
  
  public final void a(i parami)
  {
    v localv = a;
    f.b localb = new com/truecaller/callerid/f$b;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, parami, (byte)0);
    localv.a(localb);
  }
  
  public final void b()
  {
    v localv = a;
    f.e locale = new com/truecaller/callerid/f$e;
    com.truecaller.androidactors.e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, (byte)0);
    localv.a(locale);
  }
  
  public final void c()
  {
    v localv = a;
    f.d locald = new com/truecaller/callerid/f$d;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, (byte)0);
    localv.a(locald);
  }
  
  public final void d()
  {
    v localv = a;
    f.a locala = new com/truecaller/callerid/f$a;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, (byte)0);
    localv.a(locala);
  }
  
  public final void e()
  {
    v localv = a;
    f.c localc = new com/truecaller/callerid/f$c;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, (byte)0);
    localv.a(localc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */