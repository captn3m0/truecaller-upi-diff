package com.truecaller.callerid;

import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d
{
  private final q a;
  private final Provider b;
  
  private u(q paramq, Provider paramProvider)
  {
    a = paramq;
    b = paramProvider;
  }
  
  public static u a(q paramq, Provider paramProvider)
  {
    u localu = new com/truecaller/callerid/u;
    localu.<init>(paramq, paramProvider);
    return localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */