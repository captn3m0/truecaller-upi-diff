package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.HistoryEvent;

final class l$e
  extends u
{
  private final HistoryEvent b;
  private final int c;
  
  private l$e(e parame, HistoryEvent paramHistoryEvent, int paramInt)
  {
    super(parame);
    b = paramHistoryEvent;
    c = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".showRegularAfterCallScreen(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(Integer.valueOf(c), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.l.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */