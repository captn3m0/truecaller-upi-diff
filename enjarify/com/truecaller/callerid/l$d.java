package com.truecaller.callerid;

import com.truecaller.aftercall.PromotionType;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.i.c;

final class l$d
  extends u
{
  private final PromotionType b;
  private final HistoryEvent c;
  private final c d;
  
  private l$d(e parame, PromotionType paramPromotionType, HistoryEvent paramHistoryEvent, c paramc)
  {
    super(parame);
    b = paramPromotionType;
    c = paramHistoryEvent;
    d = paramc;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".showAfterCallPromo(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, 1);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.l.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */