package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class o$c
  extends u
{
  private final i b;
  
  private o$c(e parame, i parami)
  {
    super(parame);
    b = parami;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".onCallerIdWindowUpdated(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.o.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */