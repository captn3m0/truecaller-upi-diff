package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class o$d
  extends u
{
  private final int b;
  private final String c;
  private final int d;
  private final int e;
  
  private o$d(e parame, int paramInt1, String paramString, int paramInt2, int paramInt3)
  {
    super(parame);
    b = paramInt1;
    c = paramString;
    d = paramInt2;
    e = paramInt3;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".onStateChanged(");
    Object localObject = Integer.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, 1);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(d), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(e), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.o.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */