package com.truecaller;

import com.truecaller.androidactors.f;
import com.truecaller.callerid.CallerIdService;
import com.truecaller.callerid.aa;
import com.truecaller.callerid.ab;
import com.truecaller.callerid.ac;
import com.truecaller.callerid.m;
import com.truecaller.callerid.q;
import com.truecaller.callerid.r;
import com.truecaller.callerid.s;
import com.truecaller.callerid.t;
import com.truecaller.callerid.u;
import com.truecaller.callerid.v;
import com.truecaller.callerid.w;
import com.truecaller.callerid.x;
import com.truecaller.callerid.y;
import com.truecaller.callerid.z;
import com.truecaller.notificationchannels.e;
import dagger.a.c;
import javax.inject.Provider;

final class be$e
  implements m
{
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  
  private be$e(be parambe, q paramq)
  {
    localObject2 = be.K(a);
    localObject2 = ac.a(paramq, (Provider)localObject2);
    b = ((Provider)localObject2);
    localObject2 = be.K(a);
    localObject2 = ab.a(paramq, (Provider)localObject2);
    c = ((Provider)localObject2);
    localObject2 = aa.a(paramq);
    d = ((Provider)localObject2);
    Object localObject3 = be.R(a);
    Provider localProvider1 = be.h(a);
    Provider localProvider2 = be.t(a);
    Provider localProvider3 = d;
    Provider localProvider4 = be.S(a);
    Provider localProvider5 = be.T(a);
    Provider localProvider6 = be.a(a);
    localObject2 = paramq;
    localObject2 = z.a(paramq, (Provider)localObject3, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6);
    e = ((Provider)localObject2);
    localObject2 = c;
    localObject3 = e;
    localObject2 = c.a(y.a(paramq, (Provider)localObject2, (Provider)localObject3));
    f = ((Provider)localObject2);
    localObject2 = w.a(paramq);
    g = ((Provider)localObject2);
    localObject2 = g;
    localObject2 = v.a(paramq, (Provider)localObject2);
    h = ((Provider)localObject2);
    localObject3 = be.w(a);
    localProvider1 = be.U(a);
    localProvider2 = be.h(a);
    localProvider3 = be.s(a);
    localProvider4 = be.V(a);
    localProvider5 = be.W(a);
    localProvider6 = be.q(a);
    localObject2 = paramq;
    localObject2 = r.a(paramq, (Provider)localObject3, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6);
    i = ((Provider)localObject2);
    localObject2 = c.a(x.a(paramq));
    j = ((Provider)localObject2);
    localObject2 = be.X(a);
    localObject2 = u.a(paramq, (Provider)localObject2);
    k = ((Provider)localObject2);
    Provider localProvider7 = b;
    Provider localProvider8 = be.v(a);
    Provider localProvider9 = be.h(a);
    Provider localProvider10 = be.Y(a);
    Provider localProvider11 = be.Z(a);
    localObject2 = be.aa(a);
    localObject3 = paramq;
    localObject1 = localObject2;
    Provider localProvider12 = be.ab(a);
    Provider localProvider13 = be.ac(a);
    Provider localProvider14 = be.U(a);
    Provider localProvider15 = be.R(a);
    Provider localProvider16 = be.ad(a);
    Provider localProvider17 = be.ae(a);
    Provider localProvider18 = be.af(a);
    Provider localProvider19 = f;
    Provider localProvider20 = be.W(a);
    Provider localProvider21 = h;
    Provider localProvider22 = be.V(a);
    Provider localProvider23 = be.S(a);
    Provider localProvider24 = be.r(a);
    Provider localProvider25 = be.w(a);
    Provider localProvider26 = be.ag(a);
    Provider localProvider27 = i;
    Provider localProvider28 = be.C(a);
    Provider localProvider29 = be.ah(a);
    Provider localProvider30 = be.ai(a);
    Provider localProvider31 = j;
    Provider localProvider32 = be.aj(a);
    Provider localProvider33 = be.ak(a);
    Provider localProvider34 = be.K(a);
    Provider localProvider35 = be.u(a);
    Provider localProvider36 = be.al(a);
    Provider localProvider37 = be.am(a);
    Provider localProvider38 = be.y(a);
    Provider localProvider39 = be.a(a);
    Provider localProvider40 = be.O(a);
    Provider localProvider41 = be.t(a);
    Provider localProvider42 = be.an(a);
    Provider localProvider43 = k;
    Provider localProvider44 = be.ao(a);
    localObject2 = t.a(paramq, localProvider7, localProvider8, localProvider9, localProvider10, localProvider11, (Provider)localObject1, localProvider12, localProvider13, localProvider14, localProvider15, localProvider16, localProvider17, localProvider18, localProvider19, localProvider20, localProvider21, localProvider22, localProvider23, localProvider24, localProvider25, localProvider26, localProvider27, localProvider28, localProvider29, localProvider30, localProvider31, localProvider32, localProvider33, localProvider34, localProvider35, localProvider36, localProvider37, localProvider38, localProvider39, localProvider40, localProvider41, localProvider42, localProvider43, localProvider44);
    l = ((Provider)localObject2);
    localObject2 = b;
    localProvider1 = l;
    localObject2 = c.a(s.a(paramq, (Provider)localObject2, localProvider1));
    m = ((Provider)localObject2);
  }
  
  public final void a(CallerIdService paramCallerIdService)
  {
    Object localObject = (f)m.get();
    a = ((f)localObject);
    localObject = a.aC();
    b = ((e)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */