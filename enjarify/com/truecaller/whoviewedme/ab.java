package com.truecaller.whoviewedme;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.support.v4.content.b;
import c.g.b.k;
import c.n.m;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.callhistory.z;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.utils.extensions.d;
import java.io.Closeable;

public final class ab
{
  private final Context a;
  private final com.truecaller.i.e b;
  private final com.truecaller.utils.a c;
  private final c d;
  private final f e;
  private final com.truecaller.notifications.a f;
  private final com.truecaller.notificationchannels.e g;
  
  public ab(Context paramContext, com.truecaller.i.e parame, com.truecaller.utils.a parama, c paramc, f paramf, com.truecaller.notifications.a parama1, com.truecaller.notificationchannels.e parame1)
  {
    a = paramContext;
    b = parame;
    c = parama;
    d = paramc;
    e = paramf;
    f = parama1;
    g = parame1;
  }
  
  private static String a(Address paramAddress)
  {
    if (paramAddress != null)
    {
      Object localObject = paramAddress.getCityOrArea();
      if (localObject != null) {
        return paramAddress.getCityOrArea();
      }
      localObject = paramAddress.getCountryName();
      String str = "address.countryName";
      k.a(localObject, str);
      localObject = (CharSequence)localObject;
      boolean bool = m.a((CharSequence)localObject) ^ true;
      if (bool) {
        return paramAddress.getCountryName();
      }
    }
    return null;
  }
  
  private final void a(String paramString, int paramInt)
  {
    Object localObject1 = WhoViewedMeActivity.a;
    localObject1 = a;
    Object localObject2 = WhoViewedMeLaunchContext.NOTIFICATION;
    localObject1 = WhoViewedMeActivity.a.a((Context)localObject1, (WhoViewedMeLaunchContext)localObject2);
    localObject2 = a;
    z.d locald = null;
    int i = 134217728;
    localObject1 = PendingIntent.getActivity((Context)localObject2, 0, (Intent)localObject1, i);
    int j = 1;
    if (paramString == null)
    {
      paramString = a.getResources();
      i = 2131755038;
      localObject2 = new Object[j];
      Integer localInteger1 = Integer.valueOf(paramInt);
      localObject2[0] = localInteger1;
      paramString = paramString.getQuantityString(i, paramInt, (Object[])localObject2);
    }
    else
    {
      localObject3 = a.getResources();
      int k = 2131755039;
      int m = 2;
      Object[] arrayOfObject = new Object[m];
      Integer localInteger2 = Integer.valueOf(paramInt);
      arrayOfObject[0] = localInteger2;
      arrayOfObject[j] = paramString;
      paramString = ((Resources)localObject3).getQuantityString(k, paramInt, arrayOfObject);
    }
    Object localObject4 = a.getResources();
    j = 2131887334;
    localObject4 = ((Resources)localObject4).getString(j);
    localObject2 = g.o();
    if (localObject2 != null)
    {
      locald = new android/support/v4/app/z$d;
      localObject3 = a;
      locald.<init>((Context)localObject3, (String)localObject2);
    }
    else
    {
      locald = new android/support/v4/app/z$d;
      localObject2 = a;
      locald.<init>((Context)localObject2);
    }
    paramString = (CharSequence)paramString;
    paramString = locald.a(paramString).a(paramString);
    localObject4 = (CharSequence)localObject4;
    paramString = paramString.b((CharSequence)localObject4);
    j = b.c(a, 2131100594);
    paramString = paramString.f(j).c(-1);
    localObject2 = BitmapFactory.decodeResource(a.getResources(), 2131234314);
    paramString = paramString.a((Bitmap)localObject2).a(2131234787);
    localObject2 = new android/support/v4/app/z$c;
    ((z.c)localObject2).<init>();
    localObject4 = (z.g)((z.c)localObject2).b((CharSequence)localObject4);
    Object localObject3 = paramString.a((z.g)localObject4).a((PendingIntent)localObject1).e().h();
    localObject1 = f;
    k.a(localObject3, "notification");
    ((com.truecaller.notifications.a)localObject1).a(null, 2131365540, (Notification)localObject3, "notificationWhoViewedMe", null);
    paramString = b;
    long l = c.a();
    paramString.b("whoViewedMeNotificationTimestamp", l);
  }
  
  public final void a(int paramInt, Address paramAddress)
  {
    String str = a(paramAddress);
    if (str != null)
    {
      paramAddress = a(paramAddress);
      a(paramAddress, paramInt);
      return;
    }
    paramAddress = (com.truecaller.callhistory.a)e.a();
    int i = 6;
    paramAddress = (z)paramAddress.b(i).d();
    if (paramAddress != null)
    {
      i = paramAddress.getCount();
      if (i > 0)
      {
        i = 0;
        str = null;
        Object localObject1 = null;
        while (localObject1 == null)
        {
          boolean bool = paramAddress.moveToNext();
          if (!bool) {
            break;
          }
          c localc = d;
          Object localObject2 = paramAddress.d();
          if (localObject2 != null)
          {
            localObject2 = ((HistoryEvent)localObject2).s();
            if (localObject2 != null)
            {
              localObject2 = ((Contact)localObject2).getTcId();
              if (localObject2 != null)
              {
                localObject1 = localc.a((String)localObject2);
                if (localObject1 != null) {
                  localObject1 = ((Contact)localObject1).g();
                } else {
                  localObject1 = null;
                }
                localObject1 = a((Address)localObject1);
              }
            }
          }
        }
        a((String)localObject1, paramInt);
      }
      d.a((Closeable)paramAddress);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */