package com.truecaller.whoviewedme;

import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import com.truecaller.adapter_delegates.i;
import com.truecaller.common.h.j;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.utils.extensions.t;

public final class n
  extends RecyclerView.ViewHolder
  implements m
{
  private final ContactPhoto a;
  private final TextView b;
  private final TextView c;
  private final TextView d;
  private final View e;
  
  public n(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    e = paramView;
    paramView = itemView.findViewById(2131362554);
    c.g.b.k.a(paramView, "itemView.findViewById(R.id.contact_photo)");
    paramView = (ContactPhoto)paramView;
    a = paramView;
    paramView = itemView.findViewById(2131363718);
    c.g.b.k.a(paramView, "itemView.findViewById(R.id.main_text)");
    paramView = (TextView)paramView;
    b = paramView;
    paramView = itemView.findViewById(2131364281);
    c.g.b.k.a(paramView, "itemView.findViewById(R.id.secondary_text)");
    paramView = (TextView)paramView;
    c = paramView;
    paramView = itemView.findViewById(2131364107);
    c.g.b.k.a(paramView, "itemView.findViewById(R.id.relative_date)");
    paramView = (TextView)paramView;
    d = paramView;
    paramView = e;
    Object localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, 12);
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject);
  }
  
  public final void a(long paramLong)
  {
    TextView localTextView = d;
    View localView = itemView;
    c.g.b.k.a(localView, "itemView");
    CharSequence localCharSequence = j.a(localView.getContext(), paramLong, true);
    localTextView.setText(localCharSequence);
  }
  
  public final void a(Uri paramUri)
  {
    a.a(paramUri, null);
  }
  
  public final void a(String paramString)
  {
    TextView localTextView = b;
    if (paramString == null)
    {
      paramString = itemView;
      String str = "itemView";
      c.g.b.k.a(paramString, str);
      paramString = paramString.getResources();
      int i = 2131887338;
      paramString = paramString.getString(i);
    }
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    t.a((View)c, paramBoolean);
  }
  
  public final void b(String paramString)
  {
    TextView localTextView = c;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    e.setActivated(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */