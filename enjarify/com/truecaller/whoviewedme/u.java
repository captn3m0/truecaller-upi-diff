package com.truecaller.whoviewedme;

import c.l.g;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.ba;
import com.truecaller.callhistory.a;
import com.truecaller.callhistory.z;
import com.truecaller.calling.dialer.v;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.utils.extensions.d;
import com.truecaller.utils.n;
import java.io.Closeable;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlinx.coroutines.e;

public final class u
  extends ba
  implements t
{
  final c.d.f c;
  private z d;
  private final Set e;
  private final Map f;
  private int g;
  private final com.truecaller.androidactors.f h;
  private final com.truecaller.common.f.c i;
  private final w j;
  private final v k;
  private final n l;
  private final com.truecaller.androidactors.k m;
  private final com.truecaller.data.access.c n;
  private final c.d.f o;
  
  public u(com.truecaller.androidactors.f paramf, com.truecaller.common.f.c paramc, w paramw, v paramv, n paramn, com.truecaller.androidactors.k paramk, com.truecaller.data.access.c paramc1, c.d.f paramf1, c.d.f paramf2)
  {
    super(paramf1);
    h = paramf;
    i = paramc;
    j = paramw;
    k = paramv;
    l = paramn;
    m = paramk;
    n = paramc1;
    o = paramf1;
    c = paramf2;
    paramf = new java/util/LinkedHashSet;
    paramf.<init>();
    paramf = (Set)paramf;
    e = paramf;
    paramf = new java/util/LinkedHashMap;
    paramf.<init>();
    paramf = (Map)paramf;
    f = paramf;
  }
  
  private final int i()
  {
    z localz = d;
    if (localz != null) {
      return localz.getCount();
    }
    return 0;
  }
  
  public final void D_()
  {
    g();
  }
  
  public final z a(r paramr, g paramg)
  {
    c.g.b.k.b(paramr, "whoViewedMeListItemPresenter");
    c.g.b.k.b(paramg, "property");
    return d;
  }
  
  public final Contact a(String paramString)
  {
    return (Contact)f.get(paramString);
  }
  
  public final void a(Contact paramContact, DetailsFragment.SourceType paramSourceType)
  {
    c.g.b.k.b(paramContact, "contact");
    c.g.b.k.b(paramSourceType, "sourceType");
    ad localad = (ad)b;
    if (localad != null)
    {
      localad.a(paramContact, paramSourceType);
      return;
    }
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    Object localObject = "historyEvent";
    c.g.b.k.b(paramHistoryEvent, (String)localObject);
    paramHistoryEvent = paramHistoryEvent.getId();
    if (paramHistoryEvent == null) {
      return;
    }
    localObject = "historyEvent.id ?: return";
    c.g.b.k.a(paramHistoryEvent, (String)localObject);
    long l1 = paramHistoryEvent.longValue();
    paramHistoryEvent = e;
    Long localLong = Long.valueOf(l1);
    boolean bool1 = paramHistoryEvent.remove(localLong);
    if (!bool1)
    {
      localObject = Long.valueOf(l1);
      paramHistoryEvent.add(localObject);
    }
    boolean bool2 = paramHistoryEvent.isEmpty();
    if (bool2)
    {
      paramHistoryEvent = (ad)b;
      if (paramHistoryEvent != null) {
        paramHistoryEvent.h();
      }
    }
    paramHistoryEvent = (ad)b;
    if (paramHistoryEvent != null) {
      paramHistoryEvent.c();
    }
    paramHistoryEvent = (ad)b;
    if (paramHistoryEvent != null)
    {
      paramHistoryEvent.i();
      return;
    }
  }
  
  public final void a(WhoViewedMeLaunchContext paramWhoViewedMeLaunchContext)
  {
    c.g.b.k.b(paramWhoViewedMeLaunchContext, "launchContext");
    j.a(paramWhoViewedMeLaunchContext);
  }
  
  public final boolean a(int paramInt)
  {
    int i1 = 2131361848;
    Object localObject1;
    Object localObject2;
    Object localObject3;
    if (paramInt != i1)
    {
      i1 = 2131361934;
      if (paramInt == i1)
      {
        localObject1 = d;
        if (localObject1 != null)
        {
          boolean bool = ((z)localObject1).moveToFirst();
          if (bool) {
            do
            {
              localObject2 = e;
              long l1 = ((z)localObject1).a();
              localObject3 = Long.valueOf(l1);
              ((Set)localObject2).add(localObject3);
              bool = ((z)localObject1).moveToNext();
            } while (bool);
          }
          localObject1 = (ad)b;
          if (localObject1 != null) {
            ((ad)localObject1).c();
          }
          localObject1 = (ad)b;
          if (localObject1 != null) {
            ((ad)localObject1).i();
          }
        }
      }
    }
    else
    {
      localObject1 = (a)h.a();
      int i2 = 6;
      localObject3 = (Collection)e;
      localObject1 = ((a)localObject1).a(i2, (Collection)localObject3);
      localObject2 = m.a();
      localObject3 = new com/truecaller/whoviewedme/u$a;
      ((u.a)localObject3).<init>(this);
      localObject3 = (ac)localObject3;
      ((com.truecaller.androidactors.w)localObject1).a((i)localObject2, (ac)localObject3);
    }
    return true;
  }
  
  public final boolean b()
  {
    ad localad = (ad)b;
    boolean bool = true;
    if (localad != null)
    {
      localad.g();
      localad.c(bool);
    }
    return bool;
  }
  
  public final boolean b(int paramInt)
  {
    int i1 = 2131361934;
    if (paramInt == i1)
    {
      Set localSet = e;
      paramInt = localSet.size();
      i1 = i();
      if (paramInt == i1) {
        return false;
      }
    }
    return true;
  }
  
  public final boolean b(HistoryEvent paramHistoryEvent)
  {
    c.g.b.k.b(paramHistoryEvent, "historyEvent");
    Iterable localIterable = (Iterable)e;
    paramHistoryEvent = paramHistoryEvent.getId();
    return c.a.m.a(localIterable, paramHistoryEvent);
  }
  
  public final void c()
  {
    e.clear();
    ad localad = (ad)b;
    if (localad != null)
    {
      localad.c(false);
      return;
    }
  }
  
  public final String e()
  {
    n localn = l;
    Object[] arrayOfObject = new Object[2];
    Integer localInteger = Integer.valueOf(e.size());
    arrayOfObject[0] = localInteger;
    localInteger = Integer.valueOf(i());
    arrayOfObject[1] = localInteger;
    return localn.a(2131886292, arrayOfObject);
  }
  
  public final int f()
  {
    return g;
  }
  
  public final void g()
  {
    Object localObject = new com/truecaller/whoviewedme/u$b;
    ((u.b)localObject).<init>(this, null);
    localObject = (c.g.a.m)localObject;
    e.b(this, null, (c.g.a.m)localObject, 3);
  }
  
  public final void h()
  {
    j.i();
  }
  
  public final void y_()
  {
    super.y_();
    k.a(null);
    z localz = d;
    if (localz != null)
    {
      d.a((Closeable)localz);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */