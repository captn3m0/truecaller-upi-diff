package com.truecaller.whoviewedme;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import c.d.a.a;
import c.d.b.a.b;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.content.TruecallerContract.ag;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

final class d$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  d$c(d paramd, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/whoviewedme/d$c;
    d locald = b;
    String str = c;
    localc.<init>(locald, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = new android/content/ContentValues;
        ((ContentValues)paramObject).<init>();
        long l = System.currentTimeMillis();
        Object localObject2 = b.a(l);
        ((ContentValues)paramObject).put("timestamp", (Long)localObject2);
        localObject1 = b.a;
        localObject2 = TruecallerContract.ag.a();
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("tc_id='");
        String str = c;
        ((StringBuilder)localObject3).append(str);
        ((StringBuilder)localObject3).append("'");
        localObject3 = ((StringBuilder)localObject3).toString();
        str = null;
        int j = ((ContentResolver)localObject1).update((Uri)localObject2, (ContentValues)paramObject, (String)localObject3, null);
        if (j <= 0)
        {
          paramObject = b;
          localObject1 = c;
          localObject2 = (ag)bg.a;
          localObject3 = new com/truecaller/whoviewedme/d$b;
          ((d.b)localObject3).<init>((d)paramObject, (String)localObject1, null);
          localObject3 = (m)localObject3;
          j = 3;
          e.b((ag)localObject2, null, (m)localObject3, j);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */