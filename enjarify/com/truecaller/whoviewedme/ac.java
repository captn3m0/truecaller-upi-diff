package com.truecaller.whoviewedme;

import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  
  private ac(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
    f = paramProvider6;
    g = paramProvider7;
  }
  
  public static ac a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7)
  {
    ac localac = new com/truecaller/whoviewedme/ac;
    localac.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
    return localac;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */