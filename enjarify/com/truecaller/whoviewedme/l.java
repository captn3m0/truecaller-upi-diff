package com.truecaller.whoviewedme;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;

public final class l
  extends RecyclerView.ViewHolder
  implements k
{
  private final TextView a;
  private final Context b;
  
  public l(View paramView, Context paramContext)
  {
    super(paramView);
    b = paramContext;
    paramView = itemView.findViewById(2131363718);
    c.g.b.k.a(paramView, "itemView.findViewById(R.id.main_text)");
    paramView = (TextView)paramView;
    a = paramView;
  }
  
  public final void a(int paramInt)
  {
    TextView localTextView = a;
    Resources localResources = b.getResources();
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt);
    arrayOfObject[0] = localInteger;
    CharSequence localCharSequence = (CharSequence)localResources.getQuantityString(2131755032, paramInt, arrayOfObject);
    localTextView.setText(localCharSequence);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */