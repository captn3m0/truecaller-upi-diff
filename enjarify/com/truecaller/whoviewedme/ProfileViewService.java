package com.truecaller.whoviewedme;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.af;
import android.support.v4.app.v;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.data.access.m;
import com.truecaller.data.entity.Contact;
import java.io.IOException;
import java.util.Iterator;

public final class ProfileViewService
  extends af
{
  public static final ProfileViewService.a n;
  public w j;
  public c k;
  public com.truecaller.analytics.b l;
  public m m;
  
  static
  {
    ProfileViewService.a locala = new com/truecaller/whoviewedme/ProfileViewService$a;
    locala.<init>((byte)0);
    n = locala;
  }
  
  public static final void a(Context paramContext, long paramLong, boolean paramBoolean, int paramInt)
  {
    k.b(paramContext, "context");
    Intent localIntent1 = new android/content/Intent;
    localIntent1.<init>(paramContext, ProfileViewService.class);
    Intent localIntent2 = localIntent1.putExtra("EXTRA_AGGR_CONTACT_ID", paramLong).putExtra("EXTRA_SEARCH_TYPE", paramInt).putExtra("EXTRA_IS_PB_CONTACT", paramBoolean);
    v.a(paramContext.getApplicationContext(), ProfileViewService.class, 2131364005, localIntent2);
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    Object localObject1 = "EXTRA_AGGR_CONTACT_ID";
    long l1 = -1;
    long l2 = paramIntent.getLongExtra((String)localObject1, l1);
    long l3 = 0L;
    int i = l2 < l3;
    if (i < 0) {
      return;
    }
    Object localObject2 = "EXTRA_SEARCH_TYPE";
    int i1 = 999;
    int i2 = paramIntent.getIntExtra((String)localObject2, i1);
    i = 1;
    boolean bool3 = paramIntent.getBooleanExtra("EXTRA_IS_PB_CONTACT", i);
    Object localObject3 = m;
    if (localObject3 == null)
    {
      localObject4 = "rawContactDao";
      k.a((String)localObject4);
    }
    localObject1 = ((m)localObject3).a(l2);
    k.a(localObject1, "rawContactDao.getByAggre…edId(aggregatedContactId)");
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool4 = false;
    Object localObject5 = null;
    i1 = 0;
    localObject3 = null;
    Object localObject6 = null;
    int i3 = 0;
    Object localObject4 = null;
    for (;;)
    {
      boolean bool5 = ((Iterator)localObject1).hasNext();
      if (!bool5) {
        break;
      }
      Object localObject7 = ((Iterator)localObject1).next();
      Object localObject8 = localObject7;
      localObject8 = (Contact)localObject7;
      String str1 = "c";
      k.a(localObject8, str1);
      int i4 = ((Contact)localObject8).getSource();
      int i5;
      if (i4 == i)
      {
        i5 = 1;
      }
      else
      {
        i5 = 0;
        localObject8 = null;
      }
      if (i5 != 0)
      {
        if (i3 != 0) {
          break label237;
        }
        localObject6 = localObject7;
        i3 = 1;
      }
    }
    if (i3 != 0) {
      localObject3 = localObject6;
    }
    label237:
    localObject3 = (Contact)localObject3;
    if (localObject3 == null) {
      return;
    }
    localObject1 = ((Contact)localObject3).getTcId();
    if (localObject1 == null) {
      return;
    }
    localObject4 = "contact.tcId ?: return";
    k.a(localObject1, (String)localObject4);
    boolean bool1 = ((Contact)localObject3).a(i);
    try
    {
      localObject4 = j;
      if (localObject4 == null)
      {
        localObject6 = "whoViewedMeManager";
        k.a((String)localObject6);
      }
      boolean bool2 = ((w)localObject4).a((String)localObject1, i2, bool1, bool3);
      if (bool2)
      {
        localObject5 = k;
        if (localObject5 == null)
        {
          localObject2 = "profileViewDao";
          k.a((String)localObject2);
        }
        ((c)localObject5).a((String)localObject1);
        localObject5 = f.a;
        localObject5 = f.a((String)localObject1);
        ((e.b)localObject5).c();
        localObject5 = new com/truecaller/analytics/e$a;
        localObject2 = "WhoViewedMeEvent";
        ((e.a)localObject5).<init>((String)localObject2);
        localObject2 = l;
        if (localObject2 == null)
        {
          localObject4 = "analytics";
          k.a((String)localObject4);
        }
        localObject5 = ((e.a)localObject5).a();
        localObject4 = "builder.build()";
        k.a(localObject5, (String)localObject4);
        ((com.truecaller.analytics.b)localObject2).a((e)localObject5);
        bool4 = true;
      }
      localObject2 = j;
      if (localObject2 == null)
      {
        String str2 = "whoViewedMeManager";
        k.a(str2);
      }
      ((w)localObject2).a((String)localObject1, bool1, bool3, bool4);
      return;
    }
    catch (IOException localIOException) {}
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject = getApplication();
    if (localObject != null)
    {
      ((TrueApp)localObject).a().a(this);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.ProfileViewService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */