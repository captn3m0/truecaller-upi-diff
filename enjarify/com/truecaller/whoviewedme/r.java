package com.truecaller.whoviewedme;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import c.x;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.callhistory.z;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.ui.details.DetailsFragment.SourceType;

public final class r
  extends c
  implements q
{
  private final s c;
  private final s d;
  private final a e;
  private final b f;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(r.class);
    ((u)localObject).<init>(localb, "cursor", "getCursor()Lcom/truecaller/callhistory/HistoryEventCursor;");
    localObject = (g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public r(s params, a parama, b paramb)
  {
    d = params;
    e = parama;
    f = paramb;
    params = d;
    c = params;
  }
  
  private final z a()
  {
    s locals = c;
    g localg = b[0];
    return locals.a(this, localg);
  }
  
  private final HistoryEvent a(int paramInt)
  {
    z localz1 = a();
    if (localz1 != null) {
      localz1.moveToPosition(paramInt);
    }
    z localz2 = a();
    if (localz2 != null) {
      return localz2.d();
    }
    return null;
  }
  
  private final x b(int paramInt)
  {
    HistoryEvent localHistoryEvent = a(paramInt);
    if (localHistoryEvent != null)
    {
      d.a(localHistoryEvent);
      return x.a;
    }
    return null;
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject1 = a;
    int i = ((String)localObject1).hashCode();
    int j = -1743572928;
    boolean bool1 = true;
    Object localObject2;
    boolean bool2;
    int k;
    if (i != j)
    {
      j = -1314591573;
      if (i == j)
      {
        localObject2 = "ItemEvent.LONG_CLICKED";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2)
        {
          k = b;
          bool2 = a;
          if (!bool2)
          {
            e.b();
            a = bool1;
            b(k);
            return bool1;
          }
        }
      }
    }
    else
    {
      localObject2 = "ItemEvent.CLICKED";
      bool2 = ((String)localObject1).equals(localObject2);
      if (bool2)
      {
        k = b;
        bool2 = a;
        if (bool2)
        {
          b(k);
        }
        else
        {
          paramh = a(k);
          if (paramh != null)
          {
            paramh = paramh.s();
            if (paramh != null)
            {
              k.a(paramh, "getHistoryEvent(position)?.contact ?: return false");
              localObject1 = f;
              localObject2 = DetailsFragment.SourceType.WhoViewedMe;
              ((b)localObject1).a(paramh, (DetailsFragment.SourceType)localObject2);
              return bool1;
            }
          }
        }
      }
    }
    return false;
  }
  
  public final int getItemCount()
  {
    z localz = a();
    if (localz != null) {
      return localz.getCount();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return -1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */