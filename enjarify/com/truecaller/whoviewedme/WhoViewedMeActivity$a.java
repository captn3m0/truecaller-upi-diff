package com.truecaller.whoviewedme;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import java.io.Serializable;

public final class WhoViewedMeActivity$a
{
  public static Intent a(Context paramContext, WhoViewedMeLaunchContext paramWhoViewedMeLaunchContext)
  {
    k.b(paramContext, "context");
    k.b(paramWhoViewedMeLaunchContext, "launchContext");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, WhoViewedMeActivity.class);
    paramWhoViewedMeLaunchContext = (Serializable)paramWhoViewedMeLaunchContext;
    paramContext = localIntent.putExtra("launch_context", paramWhoViewedMeLaunchContext);
    k.a(paramContext, "Intent(context, WhoViewe…H_CONTEXT, launchContext)");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.WhoViewedMeActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */