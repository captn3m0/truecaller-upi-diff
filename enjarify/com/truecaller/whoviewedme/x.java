package com.truecaller.whoviewedme;

import c.g.b.k;
import c.l;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.bc;
import com.truecaller.androidactors.f;
import com.truecaller.common.g.a;
import com.truecaller.common.h.an;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.util.al;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.a.a.a.g;
import org.apache.a.d.d;

public final class x
  implements w
{
  private final com.truecaller.featuretoggles.e a;
  private final al b;
  private final com.truecaller.i.e c;
  private final a d;
  private final c e;
  private final com.truecaller.common.f.c f;
  private final com.truecaller.analytics.b g;
  private final f h;
  private final an i;
  private final ab j;
  
  public x(com.truecaller.featuretoggles.e parame, al paramal, com.truecaller.i.e parame1, a parama, c paramc, com.truecaller.common.f.c paramc1, com.truecaller.analytics.b paramb, f paramf, an paraman, ab paramab)
  {
    a = parame;
    b = paramal;
    c = parame1;
    d = parama;
    e = paramc;
    f = paramc1;
    g = paramb;
    h = paramf;
    i = paraman;
    j = paramab;
  }
  
  public final void a(WhoViewedMeLaunchContext paramWhoViewedMeLaunchContext)
  {
    k.b(paramWhoViewedMeLaunchContext, "launchContext");
    Object localObject = y.a;
    int k = paramWhoViewedMeLaunchContext.ordinal();
    k = localObject[k];
    switch (k)
    {
    default: 
      paramWhoViewedMeLaunchContext = new c/l;
      paramWhoViewedMeLaunchContext.<init>();
      throw paramWhoViewedMeLaunchContext;
    case 5: 
      paramWhoViewedMeLaunchContext = "unknown";
      break;
    case 4: 
      paramWhoViewedMeLaunchContext = "callLogPromo";
      break;
    case 3: 
      paramWhoViewedMeLaunchContext = "notification";
      break;
    case 2: 
      paramWhoViewedMeLaunchContext = "deepLink";
      break;
    case 1: 
      paramWhoViewedMeLaunchContext = "navigationDrawer";
    }
    localObject = new com/truecaller/analytics/bc;
    Boolean localBoolean = Boolean.valueOf(f.d());
    ((bc)localObject).<init>("whoViewedMe", paramWhoViewedMeLaunchContext, localBoolean, 4);
    paramWhoViewedMeLaunchContext = g;
    localObject = (com.truecaller.analytics.e)localObject;
    paramWhoViewedMeLaunchContext.b((com.truecaller.analytics.e)localObject);
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "tcId");
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    localObject1 = (Map)localObject1;
    ((Map)localObject1).put("Type", "Receiver");
    ((Map)localObject1).put("TcId", paramString);
    Object localObject2 = String.valueOf(paramBoolean);
    ((Map)localObject1).put("Status", localObject2);
    paramString = ao.b();
    localObject2 = (CharSequence)"WhoViewedMeEvent";
    paramString = paramString.a((CharSequence)localObject2).a((Map)localObject1);
    localObject2 = UUID.randomUUID().toString();
    k.a(localObject2, "UUID.randomUUID().toString()");
    localObject2 = (CharSequence)localObject2;
    paramString = paramString.b((CharSequence)localObject2).a();
    localObject2 = (ae)h.a();
    paramString = (d)paramString;
    ((ae)localObject2).a(paramString);
  }
  
  public final void a(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(paramString, "tcId");
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    localObject = (Map)localObject;
    ((Map)localObject).put("Type", "Sender");
    ((Map)localObject).put("TcId", paramString);
    String str1 = String.valueOf(paramBoolean3);
    ((Map)localObject).put("Status", str1);
    String str2 = Boolean.toString(paramBoolean1);
    k.a(str2, "toString(hasBadge)");
    ((Map)localObject).put("UserBadge", str2);
    str2 = Boolean.toString(paramBoolean2);
    k.a(str2, "toString(isPBContact)");
    ((Map)localObject).put("PBContact", str2);
  }
  
  public final void a(boolean paramBoolean)
  {
    d.b("whoViewedMeIncognitoEnabled", paramBoolean);
  }
  
  public final boolean a()
  {
    Object localObject = b;
    boolean bool = ((al)localObject).a();
    if (bool)
    {
      localObject = a.t();
      bool = ((com.truecaller.featuretoggles.b)localObject).a();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean a(String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    String str1 = "tcId";
    k.b(paramString, str1);
    boolean bool1 = a();
    if (bool1)
    {
      int k = 21;
      if (paramInt != k)
      {
        Object localObject = paramString;
        localObject = (CharSequence)paramString;
        paramInt = ((CharSequence)localObject).length();
        k = 1;
        if (paramInt > 0)
        {
          paramInt = 1;
        }
        else
        {
          paramInt = 0;
          localObject = null;
        }
        if ((paramInt != 0) && (paramBoolean1))
        {
          localObject = d;
          String str2 = "whoViewedMePBContactEnabled";
          paramInt = ((a)localObject).a(str2, false);
          if ((paramInt != 0) || (!paramBoolean2))
          {
            paramInt = e();
            if (paramInt == 0)
            {
              long l1 = System.currentTimeMillis();
              long l2 = e.b(paramString);
              l1 -= l2;
              paramString = TimeUnit.DAYS;
              a locala = d;
              String str3 = "featureWhoViewdMeNewViewIntervalInDays";
              long l3 = 5;
              l2 = locala.a(str3, l3);
              l2 = paramString.toMillis(l2);
              boolean bool2 = l1 < l2;
              if (bool2) {
                return k;
              }
            }
          }
        }
      }
    }
    return false;
  }
  
  public final long b()
  {
    com.truecaller.i.e locale = c;
    org.a.a.b localb = org.a.a.b.ay_().c(1);
    k.a(localb, "DateTime.now().minusDays(1)");
    long l = a;
    return locale.a("whoViewedMeLastVisitTimestamp", l);
  }
  
  public final boolean c()
  {
    String str = d.a("whoViewedMeIncognitoBucket");
    return "Pro".equals(str);
  }
  
  public final int d()
  {
    c localc = e;
    long l = b();
    return localc.a(l);
  }
  
  public final boolean e()
  {
    Object localObject = f;
    boolean bool1 = ((com.truecaller.common.f.c)localObject).d();
    if (bool1)
    {
      localObject = d;
      String str = "whoViewedMeIncognitoEnabled";
      boolean bool2 = true;
      bool1 = ((a)localObject).a(str, bool2);
      if (bool1) {
        return bool2;
      }
    }
    return false;
  }
  
  public final boolean f()
  {
    Object localObject1 = c;
    String str1 = "whoViewedMeNotificationTimestamp";
    long l1 = ((com.truecaller.i.e)localObject1).a(str1, 0L);
    a locala = d;
    String str2 = "featureWhoViewdMeShowNotificationAfterXLookups";
    long l2 = 5;
    long l3 = locala.a(str2, l2);
    Object localObject2 = d;
    String str3 = "featureWhoViewdMeShowNotificationAfterXDays";
    l2 = ((a)localObject2).a(str3, l2);
    localObject2 = e;
    int k = ((c)localObject2).a(l1);
    long l4 = k;
    boolean bool1 = l4 < l3;
    if (bool1)
    {
      l3 = System.currentTimeMillis() - l1;
      localObject1 = TimeUnit.DAYS;
      l1 = ((TimeUnit)localObject1).toMillis(l2);
      boolean bool2 = l3 < l1;
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  public final void g()
  {
    e.a();
  }
  
  public final c h()
  {
    return e;
  }
  
  public final void i()
  {
    com.truecaller.i.e locale = c;
    long l = System.currentTimeMillis();
    locale.b("whoViewedMeLastVisitTimestamp", l);
  }
  
  public final int j()
  {
    return e.a(0L);
  }
  
  public final boolean k()
  {
    return d.a("whoViewedMeACSEnabled", false);
  }
  
  public final void l()
  {
    Object localObject1 = f;
    boolean bool1 = ((com.truecaller.common.f.c)localObject1).d();
    if (!bool1)
    {
      bool1 = a();
      if (bool1)
      {
        localObject1 = c;
        Object localObject2 = "whoViewedMeNotificationTimestamp";
        long l1 = 0L;
        long l2 = ((com.truecaller.i.e)localObject1).a((String)localObject2, l1);
        localObject1 = e;
        int k = ((c)localObject1).a(l1);
        if (k > 0)
        {
          an localan = i;
          long l3 = 30;
          TimeUnit localTimeUnit = TimeUnit.DAYS;
          boolean bool2 = localan.a(l2, l3, localTimeUnit);
          if (bool2)
          {
            localObject2 = j;
            ((ab)localObject2).a(k, null);
          }
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */