package com.truecaller.whoviewedme;

import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import c.a.ae;
import c.a.m;
import c.g.b.k;
import c.k.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public final class j$b
  implements ActionMode.Callback
{
  j$b(j paramj) {}
  
  public final boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
  {
    k.b(paramActionMode, "actionMode");
    k.b(paramMenuItem, "menuItem");
    paramActionMode = a.b();
    int i = paramMenuItem.getItemId();
    return paramActionMode.a(i);
  }
  
  public final boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    k.b(paramActionMode, "actionMode");
    k.b(paramMenu, "menu");
    paramActionMode.getMenuInflater().inflate(2131623979, paramMenu);
    j.a(a, paramActionMode);
    return true;
  }
  
  public final void onDestroyActionMode(ActionMode paramActionMode)
  {
    k.b(paramActionMode, "actionMode");
    a.b().c();
  }
  
  public final boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    k.b(paramActionMode, "actionMode");
    k.b(paramMenu, "menu");
    Object localObject1 = a.b().e();
    if (localObject1 != null)
    {
      localObject1 = (CharSequence)localObject1;
      paramActionMode.setTitle((CharSequence)localObject1);
    }
    int i = paramMenu.size();
    paramActionMode = (Iterable)i.b(0, i);
    localObject1 = new java/util/ArrayList;
    int j = m.a(paramActionMode, 10);
    ((ArrayList)localObject1).<init>(j);
    localObject1 = (Collection)localObject1;
    paramActionMode = paramActionMode.iterator();
    int k;
    for (;;)
    {
      boolean bool2 = paramActionMode.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject2 = paramActionMode;
      k = ((ae)paramActionMode).a();
      localObject2 = paramMenu.getItem(k);
      ((Collection)localObject1).add(localObject2);
    }
    localObject1 = (Iterable)localObject1;
    paramActionMode = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool3 = paramActionMode.hasNext();
      if (!bool3) {
        break;
      }
      paramMenu = (MenuItem)paramActionMode.next();
      k.a(paramMenu, "it");
      localObject1 = a.b();
      k = paramMenu.getItemId();
      boolean bool1 = ((t)localObject1).b(k);
      paramMenu.setVisible(bool1);
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.j.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */