package com.truecaller.whoviewedme;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import c.d.a.a;
import c.d.b.a.b;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.content.TruecallerContract.ag;
import kotlinx.coroutines.ag;

final class d$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  d$b(d paramd, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/whoviewedme/d$b;
    d locald = b;
    String str = c;
    localb.<init>(locald, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    Object localObject2;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = new android/content/ContentValues;
        ((ContentValues)paramObject).<init>();
        localObject2 = c;
        ((ContentValues)paramObject).put("tc_id", (String)localObject2);
        localObject1 = "timestamp";
        long l = System.currentTimeMillis();
        localObject2 = b.a(l);
        ((ContentValues)paramObject).put((String)localObject1, (Long)localObject2);
      }
    }
    try
    {
      localObject1 = b;
      localObject1 = a;
      localObject2 = TruecallerContract.ag.a();
      ((ContentResolver)localObject1).insert((Uri)localObject2, (ContentValues)paramObject);
    }
    catch (SQLiteConstraintException localSQLiteConstraintException)
    {
      for (;;) {}
    }
    return x.a;
    throw a;
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */