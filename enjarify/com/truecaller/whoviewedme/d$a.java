package com.truecaller.whoviewedme;

import android.content.ContentResolver;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.content.TruecallerContract.ag;
import kotlinx.coroutines.ag;

final class d$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  d$a(d paramd, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/whoviewedme/d$a;
    d locald = b;
    locala.<init>(locald, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.a;
        localObject = TruecallerContract.ag.a();
        ((ContentResolver)paramObject).delete((Uri)localObject, null, null);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */