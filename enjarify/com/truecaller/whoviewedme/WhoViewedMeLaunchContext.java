package com.truecaller.whoviewedme;

public enum WhoViewedMeLaunchContext
{
  static
  {
    WhoViewedMeLaunchContext[] arrayOfWhoViewedMeLaunchContext = new WhoViewedMeLaunchContext[5];
    WhoViewedMeLaunchContext localWhoViewedMeLaunchContext = new com/truecaller/whoviewedme/WhoViewedMeLaunchContext;
    localWhoViewedMeLaunchContext.<init>("NAVIGATION_DRAWER", 0);
    NAVIGATION_DRAWER = localWhoViewedMeLaunchContext;
    arrayOfWhoViewedMeLaunchContext[0] = localWhoViewedMeLaunchContext;
    localWhoViewedMeLaunchContext = new com/truecaller/whoviewedme/WhoViewedMeLaunchContext;
    int i = 1;
    localWhoViewedMeLaunchContext.<init>("DEEPLINK", i);
    DEEPLINK = localWhoViewedMeLaunchContext;
    arrayOfWhoViewedMeLaunchContext[i] = localWhoViewedMeLaunchContext;
    localWhoViewedMeLaunchContext = new com/truecaller/whoviewedme/WhoViewedMeLaunchContext;
    i = 2;
    localWhoViewedMeLaunchContext.<init>("NOTIFICATION", i);
    NOTIFICATION = localWhoViewedMeLaunchContext;
    arrayOfWhoViewedMeLaunchContext[i] = localWhoViewedMeLaunchContext;
    localWhoViewedMeLaunchContext = new com/truecaller/whoviewedme/WhoViewedMeLaunchContext;
    i = 3;
    localWhoViewedMeLaunchContext.<init>("CALL_LOG_PROMO", i);
    CALL_LOG_PROMO = localWhoViewedMeLaunchContext;
    arrayOfWhoViewedMeLaunchContext[i] = localWhoViewedMeLaunchContext;
    localWhoViewedMeLaunchContext = new com/truecaller/whoviewedme/WhoViewedMeLaunchContext;
    i = 4;
    localWhoViewedMeLaunchContext.<init>("UNKNOWN", i);
    UNKNOWN = localWhoViewedMeLaunchContext;
    arrayOfWhoViewedMeLaunchContext[i] = localWhoViewedMeLaunchContext;
    $VALUES = arrayOfWhoViewedMeLaunchContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.WhoViewedMeLaunchContext
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */