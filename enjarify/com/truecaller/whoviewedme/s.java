package com.truecaller.whoviewedme;

import c.l.g;
import com.truecaller.callhistory.z;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;

public abstract interface s
{
  public abstract z a(r paramr, g paramg);
  
  public abstract Contact a(String paramString);
  
  public abstract void a(HistoryEvent paramHistoryEvent);
  
  public abstract boolean b(HistoryEvent paramHistoryEvent);
  
  public abstract int f();
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */