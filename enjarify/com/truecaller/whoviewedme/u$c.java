package com.truecaller.whoviewedme;

import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.androidactors.f;
import com.truecaller.callhistory.z;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.util.bs;
import java.util.Map;
import kotlinx.coroutines.ag;

final class u$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  u$c(u paramu, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/whoviewedme/u$c;
    u localu = b;
    localc.<init>(localu, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    int i = a;
    int k;
    int j;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      k = paramObject instanceof o.b;
      if (k != 0) {
        throw a;
      }
      break;
    case 0: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        break label313;
      }
      u.a(b).clear();
      paramObject = b;
      j = u.b((u)paramObject).j();
      u.a((u)paramObject, j);
      paramObject = ((com.truecaller.callhistory.a)u.c(b).a()).b(6);
      String str = "historyManager.tell().ge…YPE_WHO_VIEWED_ME_SEARCH)";
      c.g.b.k.a(paramObject, str);
      j = 1;
      a = j;
      paramObject = bs.a((com.truecaller.androidactors.w)paramObject, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = (z)paramObject;
    if (paramObject != null)
    {
      k = 0;
      locala = null;
      j = ((z)paramObject).getCount();
      while (k < j)
      {
        ((z)paramObject).moveToPosition(k);
        Object localObject1 = ((z)paramObject).d();
        if (localObject1 != null)
        {
          localObject1 = ((HistoryEvent)localObject1).s();
          if (localObject1 != null)
          {
            localObject1 = ((Contact)localObject1).getTcId();
            if (localObject1 != null)
            {
              Object localObject2 = u.a(b).get(localObject1);
              if (localObject2 == null)
              {
                localObject2 = u.a(b);
                Contact localContact = u.d(b).a((String)localObject1);
                if (localContact != null) {
                  ((Map)localObject2).put(localObject1, localContact);
                }
              }
            }
          }
        }
        int m;
        k += 1;
      }
      return paramObject;
    }
    return null;
    label313:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.u.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */