package com.truecaller.whoviewedme;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.o;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import c.g.b.k;
import c.u;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.io.Serializable;

public final class WhoViewedMeActivity
  extends AppCompatActivity
{
  public static final WhoViewedMeActivity.a a;
  
  static
  {
    WhoViewedMeActivity.a locala = new com/truecaller/whoviewedme/WhoViewedMeActivity$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final Intent a(Context paramContext, WhoViewedMeLaunchContext paramWhoViewedMeLaunchContext)
  {
    return WhoViewedMeActivity.a.a(paramContext, paramWhoViewedMeLaunchContext);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    int i = aresId;
    setTheme(i);
    super.onCreate(paramBundle);
    setContentView(2131558480);
    i = 2131364907;
    Object localObject1 = (Toolbar)findViewById(i);
    setSupportActionBar((Toolbar)localObject1);
    localObject1 = getSupportActionBar();
    boolean bool = true;
    if (localObject1 != null) {
      ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(bool);
    }
    localObject1 = getSupportActionBar();
    if (localObject1 != null) {
      ((ActionBar)localObject1).setDisplayShowHomeEnabled(bool);
    }
    localObject1 = getSupportActionBar();
    int j;
    if (localObject1 != null)
    {
      j = 2131887337;
      ((ActionBar)localObject1).setTitle(j);
    }
    if (paramBundle == null)
    {
      paramBundle = getIntent();
      if (paramBundle != null)
      {
        localObject1 = "launch_context";
        paramBundle = paramBundle.getSerializableExtra((String)localObject1);
        if (paramBundle != null) {}
      }
      else
      {
        paramBundle = (Serializable)WhoViewedMeLaunchContext.UNKNOWN;
      }
      if (paramBundle != null)
      {
        paramBundle = (WhoViewedMeLaunchContext)paramBundle;
        localObject1 = getSupportFragmentManager().a();
        j = 2131365539;
        Object localObject2 = j.e;
        k.b(paramBundle, "launchContext");
        localObject2 = new com/truecaller/whoviewedme/j;
        ((j)localObject2).<init>();
        Bundle localBundle = new android/os/Bundle;
        localBundle.<init>();
        ((j)localObject2).setArguments(localBundle);
        String str = "launch_context";
        paramBundle = (Serializable)paramBundle;
        localBundle.putSerializable(str, paramBundle);
        localObject2 = (Fragment)localObject2;
        paramBundle = ((o)localObject1).b(j, (Fragment)localObject2);
        paramBundle.c();
      }
      else
      {
        paramBundle = new c/u;
        paramBundle.<init>("null cannot be cast to non-null type com.truecaller.whoviewedme.WhoViewedMeLaunchContext");
        throw paramBundle;
      }
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    String str = "item";
    k.b(paramMenuItem, str);
    int i = paramMenuItem.getItemId();
    int j = 16908332;
    if (i != j) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    onBackPressed();
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.WhoViewedMeActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */