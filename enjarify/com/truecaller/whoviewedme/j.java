package com.truecaller.whoviewedme;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.s;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.premium.EmbeddedSubscriptionButtonsView;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.premium.d;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import java.io.Serializable;
import java.util.HashMap;

public final class j
  extends Fragment
  implements d, ad
{
  public static final j.a e;
  public t a;
  public r b;
  public p c;
  public br d;
  private a f;
  private com.truecaller.adapter_delegates.f g;
  private a h;
  private ActionMode i;
  private final j.b j;
  private HashMap k;
  
  static
  {
    j.a locala = new com/truecaller/whoviewedme/j$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public j()
  {
    j.b localb = new com/truecaller/whoviewedme/j$b;
    localb.<init>(this);
    j = localb;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = k;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      k = ((HashMap)localObject1);
    }
    localObject1 = k;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = k;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  private final void a(boolean paramBoolean, String paramString1, String paramString2)
  {
    int m = R.id.empty_list_view;
    Object localObject = (TextView)a(m);
    k.a(localObject, "premiumNoProfileView");
    com.truecaller.utils.extensions.t.a((View)localObject, false);
    m = R.id.recyclerView;
    localObject = (RecyclerView)a(m);
    String str1 = "recyclerView";
    k.a(localObject, str1);
    com.truecaller.utils.extensions.t.a((View)localObject, false);
    m = R.id.ic_blurred_list_view;
    localObject = (FrameLayout)a(m);
    k.a(localObject, "nonPremiumView");
    com.truecaller.utils.extensions.t.a((View)localObject);
    m = R.id.profile_view_count_desc;
    localObject = (TextView)a(m);
    String str2 = "profileViewCountDescText";
    k.a(localObject, str2);
    paramString1 = (CharSequence)paramString1;
    ((TextView)localObject).setText(paramString1);
    int n = R.id.profile_view_count_desc;
    paramString1 = (TextView)a(n);
    localObject = "profileViewCountDescText";
    k.a(paramString1, (String)localObject);
    com.truecaller.utils.extensions.t.a((View)paramString1);
    n = R.id.premiumFloatingButtons;
    paramString1 = (EmbeddedSubscriptionButtonsView)a(n);
    if (paramString1 != null)
    {
      paramString1 = (View)paramString1;
      com.truecaller.utils.extensions.t.a(paramString1);
    }
    n = R.id.ic_who_viewed_img;
    paramString1 = (ImageView)a(n);
    k.a(paramString1, "blurredImage");
    com.truecaller.utils.extensions.t.a((View)paramString1);
    n = R.id.ic_blurred_list;
    paramString1 = (ImageView)a(n);
    k.a(paramString1, "blurredListImage");
    com.truecaller.utils.extensions.t.a((View)paramString1, paramBoolean);
    paramBoolean = R.id.upgrade_premium;
    TextView localTextView = (TextView)a(paramBoolean);
    k.a(localTextView, "upgradePremiumText");
    paramString2 = (CharSequence)paramString2;
    localTextView.setText(paramString2);
    paramBoolean = R.id.upgrade_premium;
    localTextView = (TextView)a(paramBoolean);
    k.a(localTextView, "upgradePremiumText");
    com.truecaller.utils.extensions.t.a((View)localTextView);
  }
  
  private final void d(boolean paramBoolean)
  {
    int m = R.id.ic_blurred_list_view;
    Object localObject = (FrameLayout)a(m);
    k.a(localObject, "nonPremiumView");
    com.truecaller.utils.extensions.t.a((View)localObject, false);
    m = R.id.empty_list_view;
    localObject = (TextView)a(m);
    k.a(localObject, "premiumNoProfileView");
    localObject = (View)localObject;
    boolean bool = paramBoolean ^ true;
    com.truecaller.utils.extensions.t.a((View)localObject, bool);
    m = R.id.recyclerView;
    localObject = (RecyclerView)a(m);
    String str = "recyclerView";
    k.a(localObject, str);
    localObject = (View)localObject;
    com.truecaller.utils.extensions.t.a((View)localObject, paramBoolean);
    paramBoolean = R.id.premiumFloatingButtons;
    EmbeddedSubscriptionButtonsView localEmbeddedSubscriptionButtonsView = (EmbeddedSubscriptionButtonsView)a(paramBoolean);
    if (localEmbeddedSubscriptionButtonsView != null)
    {
      com.truecaller.utils.extensions.t.a((View)localEmbeddedSubscriptionButtonsView, false);
      return;
    }
  }
  
  public final void a()
  {
    int m = R.id.progress;
    Object localObject = (ProgressBar)a(m);
    String str = null;
    if (localObject != null)
    {
      localObject = (View)localObject;
      com.truecaller.utils.extensions.t.a((View)localObject, false);
    }
    m = R.id.premiumFloatingButtons;
    localObject = (EmbeddedSubscriptionButtonsView)a(m);
    if (localObject != null)
    {
      localObject = (View)localObject;
      com.truecaller.utils.extensions.t.a((View)localObject, false);
    }
    localObject = a;
    if (localObject == null)
    {
      str = "listPresenter";
      k.a(str);
    }
    ((t)localObject).g();
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[2];
    Integer localInteger = Integer.valueOf(paramInt1);
    arrayOfObject[0] = localInteger;
    Object localObject = Integer.valueOf(paramInt2);
    boolean bool = true;
    arrayOfObject[bool] = localObject;
    String str = localResources.getQuantityString(2131755032, paramInt1, arrayOfObject);
    k.a(str, "getPremiumUpgradeText(nu…mberOfDaysSinceLastVisit)");
    localObject = getResources().getString(2131887336);
    k.a(localObject, "resources.getString(R.st…edMeTextUpgradeToPremium)");
    a(bool, str, (String)localObject);
  }
  
  public final void a(Contact paramContact, DetailsFragment.SourceType paramSourceType)
  {
    k.b(paramContact, "contact");
    String str1 = "sourceType";
    k.b(paramSourceType, str1);
    paramSourceType = getActivity();
    if (paramSourceType == null) {
      return;
    }
    k.a(paramSourceType, "activity ?: return");
    Object localObject = paramSourceType;
    localObject = (Context)paramSourceType;
    String str2 = paramContact.getTcId();
    String str3 = paramContact.t();
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.WhoViewedMe;
    DetailsFragment.a((Context)localObject, str2, str3, "", "", "", localSourceType, true, 21);
  }
  
  public final void a(boolean paramBoolean)
  {
    int m = R.id.premiumFloatingButtons;
    Object localObject = (EmbeddedSubscriptionButtonsView)a(m);
    if (localObject != null)
    {
      localObject = (View)localObject;
      boolean bool = paramBoolean ^ true;
      com.truecaller.utils.extensions.t.a((View)localObject, bool);
    }
    m = R.id.progress;
    localObject = (ProgressBar)a(m);
    if (localObject != null)
    {
      com.truecaller.utils.extensions.t.a((View)localObject, paramBoolean);
      return;
    }
  }
  
  public final t b()
  {
    t localt = a;
    if (localt == null)
    {
      String str = "listPresenter";
      k.a(str);
    }
    return localt;
  }
  
  public final void b(boolean paramBoolean)
  {
    int m = R.id.progress;
    ProgressBar localProgressBar = (ProgressBar)a(m);
    k.a(localProgressBar, "progress");
    com.truecaller.utils.extensions.t.a((View)localProgressBar, paramBoolean);
  }
  
  public final void c()
  {
    com.truecaller.adapter_delegates.f localf = g;
    if (localf == null)
    {
      String str = "listAdapter";
      k.a(str);
    }
    localf.notifyDataSetChanged();
  }
  
  public final void c(boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject == null)
    {
      String str = "listItemPresenter";
      k.a(str);
    }
    a = paramBoolean;
    com.truecaller.adapter_delegates.f localf = g;
    if (localf == null)
    {
      localObject = "listAdapter";
      k.a((String)localObject);
    }
    localf.notifyDataSetChanged();
  }
  
  public final void d()
  {
    d(false);
  }
  
  public final void e()
  {
    d(true);
  }
  
  public final void f()
  {
    String str1 = getResources().getString(2131886755);
    k.a(str1, "resources.getString(R.string.NoProfileViews)");
    String str2 = getResources().getString(2131886756);
    k.a(str2, "resources.getString(R.st…ileViewsUpgradeToPremium)");
    a(false, str1, str2);
  }
  
  public final void g()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = (AppCompatActivity)localObject;
      ActionMode.Callback localCallback = (ActionMode.Callback)j;
      ((AppCompatActivity)localObject).startSupportActionMode(localCallback);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw ((Throwable)localObject);
  }
  
  public final void h()
  {
    ActionMode localActionMode = i;
    if (localActionMode != null)
    {
      localActionMode.finish();
      return;
    }
  }
  
  public final void i()
  {
    ActionMode localActionMode = i;
    if (localActionMode != null)
    {
      localActionMode.invalidate();
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject1;
    if (paramContext != null) {
      localObject1 = paramContext.getApplicationContext();
    } else {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      ((bk)localObject1).a().cA().a(this);
      localObject1 = new com/truecaller/adapter_delegates/p;
      Object localObject2 = b;
      if (localObject2 == null)
      {
        localObject3 = "listItemPresenter";
        k.a((String)localObject3);
      }
      localObject2 = (com.truecaller.adapter_delegates.b)localObject2;
      int m = 2131558929;
      Object localObject4 = new com/truecaller/whoviewedme/j$c;
      ((j.c)localObject4).<init>(this);
      localObject4 = (c.g.a.b)localObject4;
      c.g.a.b localb = (c.g.a.b)j.d.a;
      ((com.truecaller.adapter_delegates.p)localObject1).<init>((com.truecaller.adapter_delegates.b)localObject2, m, (c.g.a.b)localObject4, localb);
      localObject1 = (a)localObject1;
      f = ((a)localObject1);
      localObject1 = new com/truecaller/adapter_delegates/p;
      localObject2 = c;
      if (localObject2 == null)
      {
        localObject3 = "listHeaderItemPresenter";
        k.a((String)localObject3);
      }
      localObject2 = (com.truecaller.adapter_delegates.b)localObject2;
      m = 2131558930;
      localObject4 = new com/truecaller/whoviewedme/j$e;
      ((j.e)localObject4).<init>(paramContext);
      localObject4 = (c.g.a.b)localObject4;
      paramContext = (c.g.a.b)j.f.a;
      ((com.truecaller.adapter_delegates.p)localObject1).<init>((com.truecaller.adapter_delegates.b)localObject2, m, (c.g.a.b)localObject4, paramContext);
      localObject1 = (a)localObject1;
      h = ((a)localObject1);
      paramContext = new com/truecaller/adapter_delegates/f;
      localObject1 = f;
      if (localObject1 == null)
      {
        localObject2 = "listDelegate";
        k.a((String)localObject2);
      }
      localObject2 = h;
      if (localObject2 == null)
      {
        localObject3 = "listHeaderDelegate";
        k.a((String)localObject3);
      }
      Object localObject3 = new com/truecaller/adapter_delegates/g;
      ((com.truecaller.adapter_delegates.g)localObject3).<init>((byte)0);
      localObject3 = (s)localObject3;
      localObject1 = (a)((a)localObject1).a((a)localObject2, (s)localObject3);
      paramContext.<init>((a)localObject1);
      g = paramContext;
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      int m = 2131887337;
      paramBundle.setTitle(m);
    }
    paramLayoutInflater = paramLayoutInflater.inflate(2131559235, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…e_list, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDetach()
  {
    super.onDetach();
    t localt = a;
    if (localt == null)
    {
      String str = "listPresenter";
      k.a(str);
    }
    localt.y_();
  }
  
  public final void onPause()
  {
    super.onPause();
    t localt = a;
    if (localt == null)
    {
      String str = "listPresenter";
      k.a(str);
    }
    localt.h();
  }
  
  public final void onResume()
  {
    super.onResume();
    t localt = a;
    if (localt == null)
    {
      String str = "listPresenter";
      k.a(str);
    }
    localt.g();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.recyclerView;
    paramView = (RecyclerView)a(m);
    k.a(paramView, "recyclerView");
    paramBundle = new android/support/v7/widget/LinearLayoutManager;
    Object localObject = getContext();
    paramBundle.<init>((Context)localObject);
    paramBundle = (RecyclerView.LayoutManager)paramBundle;
    paramView.setLayoutManager(paramBundle);
    m = R.id.recyclerView;
    paramView = (RecyclerView)a(m);
    k.a(paramView, "recyclerView");
    paramBundle = g;
    if (paramBundle == null)
    {
      localObject = "listAdapter";
      k.a((String)localObject);
    }
    paramBundle = (RecyclerView.Adapter)paramBundle;
    paramView.setAdapter(paramBundle);
    m = R.id.recyclerView;
    paramView = (RecyclerView)a(m);
    boolean bool = true;
    paramView.setHasFixedSize(bool);
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = "launch_context";
      paramView = paramView.getSerializable(paramBundle);
      if (paramView != null) {}
    }
    else
    {
      paramView = (Serializable)WhoViewedMeLaunchContext.UNKNOWN;
    }
    if (paramView != null)
    {
      paramView = (WhoViewedMeLaunchContext)paramView;
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject = "listPresenter";
        k.a((String)localObject);
      }
      paramBundle.a(this);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject = "listPresenter";
        k.a((String)localObject);
      }
      paramBundle.a(paramView);
      m = R.id.premiumFloatingButtons;
      paramView = (EmbeddedSubscriptionButtonsView)a(m);
      paramBundle = this;
      paramBundle = (d)this;
      paramView.setCallBack(paramBundle);
      m = R.id.premiumFloatingButtons;
      paramView = (EmbeddedSubscriptionButtonsView)a(m);
      paramBundle = PremiumPresenterView.LaunchContext.WHO_VIEWED_ME;
      paramView.setLaunchContext(paramBundle);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.truecaller.whoviewedme.WhoViewedMeLaunchContext");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */