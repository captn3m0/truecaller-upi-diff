package com.truecaller.whoviewedme;

import android.content.ContentResolver;
import c.g.a.m;
import c.g.b.k;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class d
  implements c
{
  final ContentResolver a;
  
  public d(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  /* Error */
  public final int a(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 22	com/truecaller/whoviewedme/d:a	Landroid/content/ContentResolver;
    //   4: astore_3
    //   5: invokestatic 28	com/truecaller/content/TruecallerContract$n:d	()Landroid/net/Uri;
    //   8: astore 4
    //   10: iconst_1
    //   11: anewarray 32	java/lang/String
    //   14: dup
    //   15: iconst_0
    //   16: ldc 30
    //   18: aastore
    //   19: astore 5
    //   21: ldc 34
    //   23: astore 6
    //   25: iconst_1
    //   26: istore 7
    //   28: iload 7
    //   30: anewarray 32	java/lang/String
    //   33: astore 8
    //   35: lload_1
    //   36: invokestatic 39	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   39: astore 9
    //   41: aload 8
    //   43: iconst_0
    //   44: aload 9
    //   46: aastore
    //   47: aload_3
    //   48: aload 4
    //   50: aload 5
    //   52: aload 6
    //   54: aload 8
    //   56: aconst_null
    //   57: invokevirtual 45	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   60: astore 9
    //   62: aload 9
    //   64: astore_3
    //   65: aload 9
    //   67: checkcast 47	java/io/Closeable
    //   70: astore_3
    //   71: aconst_null
    //   72: astore 4
    //   74: new 49	java/util/ArrayList
    //   77: astore 5
    //   79: aload 5
    //   81: invokespecial 50	java/util/ArrayList:<init>	()V
    //   84: aload 5
    //   86: checkcast 52	java/util/Collection
    //   89: astore 5
    //   91: aload 9
    //   93: invokeinterface 58 1 0
    //   98: istore 10
    //   100: iload 10
    //   102: ifeq +36 -> 138
    //   105: ldc 60
    //   107: astore 6
    //   109: aload 9
    //   111: aload 6
    //   113: invokestatic 65	com/truecaller/utils/extensions/k:b	(Landroid/database/Cursor;Ljava/lang/String;)I
    //   116: istore 10
    //   118: iload 10
    //   120: invokestatic 70	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   123: astore 6
    //   125: aload 5
    //   127: aload 6
    //   129: invokeinterface 74 2 0
    //   134: pop
    //   135: goto -44 -> 91
    //   138: aload 5
    //   140: checkcast 76	java/util/List
    //   143: astore 5
    //   145: aload_3
    //   146: aconst_null
    //   147: invokestatic 81	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   150: aload 5
    //   152: invokestatic 87	c/a/m:e	(Ljava/util/List;)Ljava/lang/Object;
    //   155: checkcast 67	java/lang/Integer
    //   158: astore 9
    //   160: aload 9
    //   162: ifnull +9 -> 171
    //   165: aload 9
    //   167: invokevirtual 91	java/lang/Integer:intValue	()I
    //   170: ireturn
    //   171: iconst_0
    //   172: ireturn
    //   173: astore 9
    //   175: goto +12 -> 187
    //   178: astore 9
    //   180: aload 9
    //   182: astore 4
    //   184: aload 9
    //   186: athrow
    //   187: aload_3
    //   188: aload 4
    //   190: invokestatic 81	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   193: aload 9
    //   195: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	196	0	this	d
    //   0	196	1	paramLong	long
    //   4	184	3	localObject1	Object
    //   8	181	4	localObject2	Object
    //   19	132	5	localObject3	Object
    //   23	105	6	localObject4	Object
    //   26	3	7	i	int
    //   33	22	8	arrayOfString	String[]
    //   39	127	9	localObject5	Object
    //   173	1	9	localObject6	Object
    //   178	16	9	localObject7	Object
    //   98	3	10	bool	boolean
    //   116	3	10	j	int
    // Exception table:
    //   from	to	target	type
    //   184	187	173	finally
    //   74	77	178	finally
    //   79	84	178	finally
    //   84	89	178	finally
    //   91	98	178	finally
    //   111	116	178	finally
    //   118	123	178	finally
    //   127	135	178	finally
    //   138	143	178	finally
  }
  
  public final bn a()
  {
    ag localag = (ag)bg.a;
    Object localObject = new com/truecaller/whoviewedme/d$a;
    ((d.a)localObject).<init>(this, null);
    localObject = (m)localObject;
    return e.b(localag, null, (m)localObject, 3);
  }
  
  public final bn a(String paramString)
  {
    k.b(paramString, "tcId");
    ag localag = (ag)bg.a;
    Object localObject = new com/truecaller/whoviewedme/d$c;
    ((d.c)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    return e.b(localag, null, (m)localObject, 3);
  }
  
  /* Error */
  public final long b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 22	com/truecaller/whoviewedme/d:a	Landroid/content/ContentResolver;
    //   4: astore_1
    //   5: invokestatic 28	com/truecaller/content/TruecallerContract$n:d	()Landroid/net/Uri;
    //   8: astore_2
    //   9: iconst_1
    //   10: anewarray 32	java/lang/String
    //   13: dup
    //   14: iconst_0
    //   15: ldc 120
    //   17: aastore
    //   18: astore_3
    //   19: ldc 122
    //   21: astore 4
    //   23: iconst_0
    //   24: istore 5
    //   26: aconst_null
    //   27: astore 6
    //   29: aload_1
    //   30: aload_2
    //   31: aload_3
    //   32: aload 4
    //   34: aconst_null
    //   35: aconst_null
    //   36: invokevirtual 45	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   39: astore_1
    //   40: aload_1
    //   41: astore_2
    //   42: aload_1
    //   43: checkcast 47	java/io/Closeable
    //   46: astore_2
    //   47: aconst_null
    //   48: astore_3
    //   49: new 49	java/util/ArrayList
    //   52: astore 4
    //   54: aload 4
    //   56: invokespecial 50	java/util/ArrayList:<init>	()V
    //   59: aload 4
    //   61: checkcast 52	java/util/Collection
    //   64: astore 4
    //   66: aload_1
    //   67: invokeinterface 58 1 0
    //   72: istore 5
    //   74: iload 5
    //   76: ifeq +35 -> 111
    //   79: ldc 124
    //   81: astore 6
    //   83: aload_1
    //   84: aload 6
    //   86: invokestatic 128	com/truecaller/utils/extensions/k:c	(Landroid/database/Cursor;Ljava/lang/String;)J
    //   89: lstore 7
    //   91: lload 7
    //   93: invokestatic 133	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   96: astore 6
    //   98: aload 4
    //   100: aload 6
    //   102: invokeinterface 74 2 0
    //   107: pop
    //   108: goto -42 -> 66
    //   111: aload 4
    //   113: checkcast 76	java/util/List
    //   116: astore 4
    //   118: aload_2
    //   119: aconst_null
    //   120: invokestatic 81	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   123: aload 4
    //   125: invokestatic 87	c/a/m:e	(Ljava/util/List;)Ljava/lang/Object;
    //   128: checkcast 130	java/lang/Long
    //   131: astore_1
    //   132: aload_1
    //   133: ifnull +8 -> 141
    //   136: aload_1
    //   137: invokevirtual 137	java/lang/Long:longValue	()J
    //   140: lreturn
    //   141: invokestatic 142	java/lang/System:currentTimeMillis	()J
    //   144: lreturn
    //   145: astore_1
    //   146: goto +8 -> 154
    //   149: astore_1
    //   150: aload_1
    //   151: astore_3
    //   152: aload_1
    //   153: athrow
    //   154: aload_2
    //   155: aload_3
    //   156: invokestatic 81	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   159: aload_1
    //   160: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	161	0	this	d
    //   4	133	1	localObject1	Object
    //   145	1	1	localObject2	Object
    //   149	11	1	localObject3	Object
    //   8	147	2	localObject4	Object
    //   18	138	3	localObject5	Object
    //   21	103	4	localObject6	Object
    //   24	51	5	bool	boolean
    //   27	74	6	localObject7	Object
    //   89	3	7	l	long
    // Exception table:
    //   from	to	target	type
    //   152	154	145	finally
    //   49	52	149	finally
    //   54	59	149	finally
    //   59	64	149	finally
    //   66	72	149	finally
    //   84	89	149	finally
    //   91	96	149	finally
    //   100	108	149	finally
    //   111	116	149	finally
  }
  
  /* Error */
  public final long b(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 113
    //   3: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_0
    //   7: getfield 22	com/truecaller/whoviewedme/d:a	Landroid/content/ContentResolver;
    //   10: astore_2
    //   11: invokestatic 146	com/truecaller/content/TruecallerContract$ag:a	()Landroid/net/Uri;
    //   14: astore_3
    //   15: ldc -108
    //   17: astore 4
    //   19: iconst_1
    //   20: anewarray 32	java/lang/String
    //   23: astore 5
    //   25: aload 5
    //   27: iconst_0
    //   28: aload_1
    //   29: aastore
    //   30: iconst_0
    //   31: istore 6
    //   33: aconst_null
    //   34: astore 7
    //   36: aload_2
    //   37: aload_3
    //   38: aconst_null
    //   39: aload 4
    //   41: aload 5
    //   43: aconst_null
    //   44: invokevirtual 45	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   47: astore_1
    //   48: aload_1
    //   49: astore 8
    //   51: aload_1
    //   52: checkcast 47	java/io/Closeable
    //   55: astore 8
    //   57: aconst_null
    //   58: astore_2
    //   59: new 49	java/util/ArrayList
    //   62: astore_3
    //   63: aload_3
    //   64: invokespecial 50	java/util/ArrayList:<init>	()V
    //   67: aload_3
    //   68: checkcast 52	java/util/Collection
    //   71: astore_3
    //   72: aload_1
    //   73: invokeinterface 58 1 0
    //   78: istore 6
    //   80: iload 6
    //   82: ifeq +34 -> 116
    //   85: ldc 124
    //   87: astore 7
    //   89: aload_1
    //   90: aload 7
    //   92: invokestatic 128	com/truecaller/utils/extensions/k:c	(Landroid/database/Cursor;Ljava/lang/String;)J
    //   95: lstore 9
    //   97: lload 9
    //   99: invokestatic 133	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   102: astore 7
    //   104: aload_3
    //   105: aload 7
    //   107: invokeinterface 74 2 0
    //   112: pop
    //   113: goto -41 -> 72
    //   116: aload_3
    //   117: checkcast 76	java/util/List
    //   120: astore_3
    //   121: aload 8
    //   123: aconst_null
    //   124: invokestatic 81	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   127: aload_3
    //   128: invokestatic 87	c/a/m:e	(Ljava/util/List;)Ljava/lang/Object;
    //   131: checkcast 130	java/lang/Long
    //   134: astore_1
    //   135: aload_1
    //   136: ifnull +8 -> 144
    //   139: aload_1
    //   140: invokevirtual 137	java/lang/Long:longValue	()J
    //   143: lreturn
    //   144: lconst_0
    //   145: lreturn
    //   146: astore_1
    //   147: goto +8 -> 155
    //   150: astore_1
    //   151: aload_1
    //   152: astore_2
    //   153: aload_1
    //   154: athrow
    //   155: aload 8
    //   157: aload_2
    //   158: invokestatic 81	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   161: aload_1
    //   162: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	163	0	this	d
    //   0	163	1	paramString	String
    //   10	148	2	localObject1	Object
    //   14	114	3	localObject2	Object
    //   17	23	4	str	String
    //   23	19	5	arrayOfString	String[]
    //   31	50	6	bool	boolean
    //   34	72	7	localObject3	Object
    //   49	107	8	localObject4	Object
    //   95	3	9	l	long
    // Exception table:
    //   from	to	target	type
    //   153	155	146	finally
    //   59	62	150	finally
    //   63	67	150	finally
    //   67	71	150	finally
    //   72	78	150	finally
    //   90	95	150	finally
    //   97	102	150	finally
    //   105	113	150	finally
    //   116	120	150	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */