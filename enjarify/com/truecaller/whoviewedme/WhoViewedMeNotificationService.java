package com.truecaller.whoviewedme;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.af;
import android.support.v4.app.v;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.callhistory.a;
import com.truecaller.callhistory.z;
import com.truecaller.common.h.q;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.search.ContactDto;
import com.truecaller.search.ContactDto.Contact;
import com.truecaller.search.i;
import com.truecaller.search.j;
import e.r;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;

public final class WhoViewedMeNotificationService
  extends af
{
  public static final WhoViewedMeNotificationService.a p;
  public f j;
  public w k;
  public com.truecaller.i.e l;
  public com.truecaller.data.access.c m;
  public com.truecaller.analytics.b n;
  public ab o;
  
  static
  {
    WhoViewedMeNotificationService.a locala = new com/truecaller/whoviewedme/WhoViewedMeNotificationService$a;
    locala.<init>((byte)0);
    p = locala;
  }
  
  public static final void a(Context paramContext, Notification paramNotification)
  {
    k.b(paramContext, "context");
    k.b(paramNotification, "notification");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, WhoViewedMeNotificationService.class);
    paramNotification = paramNotification.a();
    localIntent.putExtra("EXTRA_TC_ID", paramNotification);
    v.a(paramContext, WhoViewedMeNotificationService.class, 2131365540, localIntent);
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    Object localObject1 = k;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "whoViewedMeManager";
      k.a((String)localObject2);
    }
    boolean bool1 = ((w)localObject1).a();
    if (bool1)
    {
      localObject1 = l;
      if (localObject1 == null)
      {
        localObject2 = "generalSettings";
        k.a((String)localObject2);
      }
      localObject2 = "showProfileViewNotifications";
      boolean bool2 = true;
      bool1 = ((com.truecaller.i.e)localObject1).a((String)localObject2, bool2);
      if (bool1)
      {
        paramIntent = paramIntent.getStringExtra("EXTRA_TC_ID");
        localObject1 = m;
        if (localObject1 == null)
        {
          localObject2 = "aggregateContactDao";
          k.a((String)localObject2);
        }
        localObject1 = ((com.truecaller.data.access.c)localObject1).a(paramIntent);
        boolean bool3 = false;
        localObject2 = null;
        Object localObject5;
        int i1;
        Object localObject6;
        if (localObject1 == null)
        {
          Object localObject3 = i.a();
          localObject5 = "tcId";
          k.a(paramIntent, (String)localObject5);
          localObject3 = ((j)localObject3).a(paramIntent);
          try
          {
            localObject3 = q.a((e.b)localObject3);
            i1 = 0;
            localObject5 = null;
            if (localObject3 != null)
            {
              bool4 = ((r)localObject3).d();
              localObject6 = Boolean.valueOf(bool4);
            }
            else
            {
              bool4 = false;
              localObject6 = null;
            }
            boolean bool4 = com.truecaller.utils.extensions.c.a((Boolean)localObject6);
            if (bool4)
            {
              if (localObject3 != null)
              {
                localObject3 = ((r)localObject3).e();
                localObject3 = (ContactDto)localObject3;
                if (localObject3 != null)
                {
                  localObject6 = "contactDto";
                  k.a(localObject3, (String)localObject6);
                  localObject3 = data;
                  if (localObject3 != null)
                  {
                    localObject3 = c.a.m.a((List)localObject3, 0);
                    localObject3 = (ContactDto.Contact)localObject3;
                  }
                  else
                  {
                    localObject3 = null;
                  }
                  if (localObject3 != null)
                  {
                    localObject6 = access;
                  }
                  else
                  {
                    bool4 = false;
                    localObject6 = null;
                  }
                  String str1 = "PRIVATE";
                  bool4 = c.n.m.a((String)localObject6, str1, bool2);
                  if ((bool4) && (localObject3 != null)) {
                    phones = null;
                  }
                  if (localObject3 != null)
                  {
                    localObject5 = new com/truecaller/data/entity/Contact;
                    ((Contact)localObject5).<init>((ContactDto.Contact)localObject3);
                  }
                }
              }
              localObject1 = localObject5;
            }
          }
          catch (IOException localIOException)
          {
            localIOException.printStackTrace();
          }
        }
        int i;
        if (localObject1 != null)
        {
          Object localObject4 = j;
          if (localObject4 == null)
          {
            localObject5 = "historyManager";
            k.a((String)localObject5);
          }
          localObject4 = (a)((f)localObject4).a();
          localObject5 = new com/truecaller/data/entity/HistoryEvent;
          int i2 = 6;
          ((HistoryEvent)localObject5).<init>((Contact)localObject1, i2);
          ((a)localObject4).a((HistoryEvent)localObject5, (Contact)localObject1).d();
          localObject4 = j;
          if (localObject4 == null)
          {
            localObject5 = "historyManager";
            k.a((String)localObject5);
          }
          localObject4 = (z)((a)((f)localObject4).a()).b(i2).d();
          if (localObject4 != null)
          {
            i1 = ((z)localObject4).getCount();
            if (i1 > 0)
            {
              localObject1 = ((Contact)localObject1).g();
              localObject2 = new android/content/Intent;
              ((Intent)localObject2).<init>("com.truecaller.notification.action.NOTIFICATIONS_UPDATED");
              localObject5 = this;
              localObject5 = android.support.v4.content.d.a((Context)this);
              ((android.support.v4.content.d)localObject5).a((Intent)localObject2);
              localObject2 = k;
              if (localObject2 == null)
              {
                localObject5 = "whoViewedMeManager";
                k.a((String)localObject5);
              }
              bool3 = ((w)localObject2).f();
              if (bool3)
              {
                localObject2 = k;
                if (localObject2 == null)
                {
                  localObject5 = "whoViewedMeManager";
                  k.a((String)localObject5);
                }
                localObject2 = ((w)localObject2).h();
                localObject5 = k;
                if (localObject5 == null)
                {
                  localObject6 = "whoViewedMeManager";
                  k.a((String)localObject6);
                }
                long l1 = ((w)localObject5).b();
                i = ((c)localObject2).a(l1);
                localObject5 = o;
                if (localObject5 == null)
                {
                  localObject6 = "whoViewedMeNotifier";
                  k.a((String)localObject6);
                }
                ((ab)localObject5).a(i, (Address)localObject1);
              }
              localObject1 = new com/truecaller/analytics/e$a;
              ((e.a)localObject1).<init>("WhoViewedMeReceivedEvent");
              localObject2 = n;
              if (localObject2 == null)
              {
                localObject5 = "analytics";
                k.a((String)localObject5);
              }
              localObject1 = ((e.a)localObject1).a();
              localObject5 = "builder.build()";
              k.a(localObject1, (String)localObject5);
              ((com.truecaller.analytics.b)localObject2).b((com.truecaller.analytics.e)localObject1);
              i = 1;
            }
            else
            {
              localObject1 = "No entries for Who Viewed Me after creating event";
              AssertionUtil.reportWeirdnessButNeverCrash((String)localObject1);
            }
            localObject4 = (Closeable)localObject4;
            com.truecaller.utils.extensions.d.a((Closeable)localObject4);
          }
        }
        localObject1 = k;
        if (localObject1 == null)
        {
          String str2 = "whoViewedMeManager";
          k.a(str2);
        }
        k.a(paramIntent, "tcId");
        ((w)localObject1).a(paramIntent, i);
        return;
      }
    }
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject = getApplication();
    if (localObject != null)
    {
      ((TrueApp)localObject).a().a(this);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.WhoViewedMeNotificationService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */