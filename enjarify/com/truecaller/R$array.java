package com.truecaller;

public final class R$array
{
  public static final int BlockAdvancedTypes = 2130903040;
  public static final int DialogReportNotSpamOptions = 2130903041;
  public static final int MmsEmptySubject = 2130903042;
  public static final int MmsExpirationMonth = 2130903043;
  public static final int Profile_PhotoMenu = 2130903044;
  public static final int Profile_PhotoMenuWithRemove = 2130903045;
  public static final int SdkPartnerLoginIntentOptionsArray = 2130903046;
  public static final int SettingsTapInCallBehaviorVariants = 2130903047;
  public static final int carrier_telenor_actions = 2130903048;
  public static final int carrier_telenor_links = 2130903049;
  public static final int carrier_tim_actions = 2130903050;
  public static final int carrier_tim_links = 2130903051;
  public static final int com_google_android_gms_fonts_certs = 2130903052;
  public static final int com_google_android_gms_fonts_certs_dev = 2130903053;
  public static final int com_google_android_gms_fonts_certs_prod = 2130903054;
  public static final int dial_pad_feedback_entries = 2130903055;
  public static final int emojiicons = 2130903056;
  public static final int flash_tips = 2130903057;
  public static final int history_tabs = 2130903058;
  public static final int id_types = 2130903059;
  public static final int incallui_button_message_options = 2130903060;
  public static final int pref_items_multi_sim_slot = 2130903061;
  public static final int preloaded_fonts = 2130903062;
  public static final int sorting_modes = 2130903063;
  public static final int swish_flash_buttons = 2130903064;
  public static final int tab_titles = 2130903065;
  public static final int top_cities = 2130903066;
  public static final int voip_button_message_options = 2130903067;
}

/* Location:
 * Qualified Name:     com.truecaller.R.array
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */