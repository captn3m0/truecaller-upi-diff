package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.c.a.b.d;
import com.google.c.a.g;
import com.google.c.a.j;
import com.google.c.a.k.c;
import com.google.c.a.k.d;
import com.google.c.a.l.b;
import com.google.c.a.m.a;
import com.google.c.a.n;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.search.ContactDto.Contact.PhoneNumber;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Number
  extends RowEntity
  implements f
{
  public static final Parcelable.Creator CREATOR;
  static final Comparator a;
  private static final int[] b;
  private static String d;
  private int c;
  
  static
  {
    Object localObject = new int[5];
    localObject[0] = 2;
    localObject[1] = 17;
    localObject[2] = 1;
    localObject[3] = 3;
    localObject[4] = 7;
    b = (int[])localObject;
    a = -..Lambda.Number.vmwOABLbFcqzJfnAtqXybcO4iLE.INSTANCE;
    localObject = new com/truecaller/data/entity/Number$1;
    ((Number.1)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public Number()
  {
    this(localPhoneNumber);
  }
  
  protected Number(Parcel paramParcel)
  {
    super(paramParcel);
    int i = paramParcel.readInt();
    c = i;
  }
  
  public Number(Number paramNumber)
  {
    this(localPhoneNumber1);
    int i = paramNumber.getSource();
    setSource(i);
  }
  
  Number(ContactDto.Contact.PhoneNumber paramPhoneNumber)
  {
    super(paramPhoneNumber);
  }
  
  public Number(String paramString)
  {
    this(paramString, null);
  }
  
  Number(String paramString1, String paramString2)
  {
    this();
    c(paramString1);
    Object localObject1 = g(paramString2);
    Object localObject2 = n.a();
    int i = 2;
    Object localObject3 = new String[i];
    localObject3[0] = paramString2;
    String str = d;
    int j = 1;
    localObject3[j] = str;
    localObject3 = am.b((String[])localObject3);
    if (localObject1 != null)
    {
      boolean bool1 = TextUtils.isEmpty((CharSequence)localObject3);
      if (!bool1)
      {
        paramString2 = Collections.unmodifiableSet(g);
        boolean bool2 = paramString2.contains(localObject3);
        if (!bool2)
        {
          str = String.valueOf(localObject3);
          paramString2 = "Invalid country iso: ".concat(str);
          AssertionUtil.reportWeirdnessButNeverCrash(paramString2);
          a(paramString1);
        }
        try
        {
          bool2 = ((n)localObject2).a(paramString1, (String)localObject3);
          if (bool2)
          {
            a(paramString1);
            b(paramString1);
            paramString2 = k.d.c;
            a(paramString2);
          }
          else
          {
            paramString2 = ((com.google.c.a.k)localObject1).a(paramString1, (String)localObject3);
            boolean bool3 = ((n)localObject2).a(paramString2);
            if (!bool3)
            {
              bool3 = ((com.google.c.a.k)localObject1).e(paramString2);
              if (bool3)
              {
                localObject2 = k.c.a;
                localObject2 = ((com.google.c.a.k)localObject1).a(paramString2, (k.c)localObject2);
                a((String)localObject2);
                localObject2 = k.c.c;
                localObject2 = ((com.google.c.a.k)localObject1).a(paramString2, (k.c)localObject2);
                b((String)localObject2);
                break label248;
              }
            }
            a(paramString1);
            b(paramString1);
            label248:
            int k = b;
            c(k);
            paramString2 = ((com.google.c.a.k)localObject1).b(paramString2);
            a(paramString2);
          }
          paramString2 = a();
          paramString2 = h.c(paramString2);
          if (paramString2 == null)
          {
            f((String)localObject3);
            return;
          }
          paramString2 = c;
          localObject1 = Locale.ENGLISH;
          paramString2 = am.c(paramString2, (Locale)localObject1);
          f(paramString2);
          return;
        }
        catch (g paramString2)
        {
          a(paramString1);
          localObject1 = new String[j];
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("Invalid number, cannot parse \"");
          ((StringBuilder)localObject2).append(paramString1);
          ((StringBuilder)localObject2).append("\" using ");
          ((StringBuilder)localObject2).append((String)localObject3);
          ((StringBuilder)localObject2).append(", ");
          paramString1 = paramString2.getMessage();
          ((StringBuilder)localObject2).append(paramString1);
          paramString1 = ((StringBuilder)localObject2).toString();
          localObject1[0] = paramString1;
          return;
        }
      }
    }
    a(paramString1);
    f(paramString2);
  }
  
  public static Number a(String paramString1, String paramString2, String paramString3)
  {
    boolean bool = TextUtils.isEmpty(paramString1);
    if (bool)
    {
      bool = TextUtils.isEmpty(paramString2);
      if (bool) {
        return null;
      }
    }
    bool = TextUtils.isEmpty(paramString1);
    Number localNumber;
    if (!bool)
    {
      localNumber = new com/truecaller/data/entity/Number;
      localNumber.<init>(paramString1);
    }
    else
    {
      localNumber = new com/truecaller/data/entity/Number;
      localNumber.<init>(paramString2, paramString3);
    }
    String str = localNumber.a();
    paramString1 = (String)am.e(paramString1, str);
    localNumber.a(paramString1);
    paramString1 = localNumber.d();
    paramString1 = (String)am.e(paramString2, paramString1);
    localNumber.c(paramString1);
    paramString1 = localNumber.l();
    paramString1 = (String)am.e(paramString3, paramString1);
    localNumber.f(paramString1);
    return localNumber;
  }
  
  private static com.google.c.a.k g(String paramString)
  {
    String str = d;
    if (str == null)
    {
      boolean bool = TextUtils.isEmpty(paramString);
      if (bool) {
        paramString = com.truecaller.common.b.a.F().H();
      }
      bool = am.b(paramString);
      if (bool) {
        return null;
      }
      paramString = paramString.toUpperCase();
      d = paramString;
    }
    return com.google.c.a.k.a();
  }
  
  public final String a()
  {
    return mRow).e164Format;
  }
  
  public final void a(int paramInt)
  {
    ContactDto.Contact.PhoneNumber localPhoneNumber = (ContactDto.Contact.PhoneNumber)mRow;
    String str = String.valueOf(paramInt);
    spamScore = str;
  }
  
  public final void a(k.d paramd)
  {
    ContactDto.Contact.PhoneNumber localPhoneNumber = (ContactDto.Contact.PhoneNumber)mRow;
    if (paramd == null) {
      paramd = null;
    } else {
      paramd = paramd.toString();
    }
    numberType = paramd;
  }
  
  public final void a(String paramString)
  {
    mRow).e164Format = paramString;
  }
  
  public final String b()
  {
    String str1 = mRow).e164Format;
    String str2 = "+";
    boolean bool = org.c.a.a.a.k.a(str1, str2, false);
    if (bool) {
      return mRow).e164Format.substring(1);
    }
    return mRow).e164Format;
  }
  
  public final void b(int paramInt)
  {
    ContactDto.Contact.PhoneNumber localPhoneNumber = (ContactDto.Contact.PhoneNumber)mRow;
    String str = String.valueOf(paramInt);
    telType = str;
  }
  
  public final void b(String paramString)
  {
    mRow).nationalFormat = paramString;
  }
  
  public final String c()
  {
    return mRow).nationalFormat;
  }
  
  public final void c(int paramInt)
  {
    ContactDto.Contact.PhoneNumber localPhoneNumber = (ContactDto.Contact.PhoneNumber)mRow;
    String str = String.valueOf(paramInt);
    dialingCode = str;
  }
  
  public final void c(String paramString)
  {
    mRow).rawNumberFormat = paramString;
  }
  
  public final String d()
  {
    return mRow).rawNumberFormat;
  }
  
  public final void d(String paramString)
  {
    mRow).carrier = paramString;
  }
  
  public final String e()
  {
    Object localObject = m();
    k.d locald = k.d.d;
    if (localObject == locald)
    {
      localObject = c();
      if (localObject != null) {
        return c();
      }
    }
    localObject = new String[3];
    String str = d();
    localObject[0] = str;
    str = a();
    localObject[1] = str;
    str = c();
    localObject[2] = str;
    return am.b((String[])localObject);
  }
  
  public final void e(String paramString)
  {
    mRow).telTypeLabel = paramString;
  }
  
  public final String f()
  {
    return mRow).carrier;
  }
  
  public final void f(String paramString)
  {
    mRow).countryCode = paramString;
  }
  
  public final String g()
  {
    String str1 = f();
    boolean bool1 = TextUtils.isEmpty(str1);
    if (!bool1) {
      return f();
    }
    bool1 = false;
    str1 = null;
    Object localObject1 = g(null);
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = a();
      boolean bool2 = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool2)
      {
        bool2 = false;
        localObject2 = null;
      }
    }
    try
    {
      Object localObject3 = a();
      Object localObject4 = l();
      localObject1 = ((com.google.c.a.k)localObject1).a((CharSequence)localObject3, (String)localObject4);
      localObject3 = j.a();
      localObject4 = Locale.getDefault();
      Object localObject5 = b;
      Object localObject6 = b;
      localObject6 = ((com.google.c.a.k)localObject6).d((m.a)localObject1);
      localObject5 = ((com.google.c.a.k)localObject5).b((String)localObject6);
      if (localObject5 == null)
      {
        localObject5 = com.google.c.a.k.a;
        Level localLevel = Level.WARNING;
        String str2 = "Invalid or unknown region code provided: ";
        localObject6 = String.valueOf(localObject6);
        localObject6 = str2.concat((String)localObject6);
        ((Logger)localObject5).log(localLevel, (String)localObject6);
        bool3 = false;
        localObject5 = null;
      }
      else
      {
        bool3 = B;
      }
      if (bool3) {
        return "";
      }
      localObject5 = b;
      localObject5 = ((com.google.c.a.k)localObject5).b((m.a)localObject1);
      localObject6 = k.d.b;
      int i = 1;
      if (localObject5 != localObject6)
      {
        localObject6 = k.d.c;
        if (localObject5 != localObject6)
        {
          localObject6 = k.d.i;
          if (localObject5 != localObject6)
          {
            bool3 = false;
            localObject5 = null;
            break label257;
          }
        }
      }
      boolean bool3 = true;
      label257:
      if (bool3)
      {
        localObject5 = ((Locale)localObject4).getLanguage();
        localObject6 = "";
        localObject4 = ((Locale)localObject4).getCountry();
        localObject3 = a;
        int j = b;
        if (j == i)
        {
          long l1 = d;
          long l2 = 10000000L;
          l1 /= l2;
          j = (int)l1 + 1000;
        }
        localObject4 = ((com.google.c.a.b.f)localObject3).a(j, (String)localObject5, (String)localObject6, (String)localObject4);
        if (localObject4 != null) {
          localObject4 = ((d)localObject4).a((m.a)localObject1);
        } else {
          localObject4 = null;
        }
        if (localObject4 != null)
        {
          int k = ((String)localObject4).length();
          if (k != 0) {}
        }
        else
        {
          bool3 = com.google.c.a.b.f.a((String)localObject5);
          if (bool3)
          {
            localObject4 = "en";
            localObject5 = "";
            localObject6 = "";
            localObject3 = ((com.google.c.a.b.f)localObject3).a(j, (String)localObject4, (String)localObject5, (String)localObject6);
            if (localObject3 == null) {
              break label445;
            }
            localObject4 = ((d)localObject3).a((m.a)localObject1);
          }
        }
        if (localObject4 != null) {
          return (String)localObject4;
        }
        label445:
        return "";
      }
      return "";
    }
    catch (Exception localException)
    {
      localObject2 = new String[0];
      AssertionUtil.shouldNeverHappen(localException, (String[])localObject2);
      return null;
    }
    catch (g localg)
    {
      for (;;) {}
    }
  }
  
  public int getSource()
  {
    return c;
  }
  
  public String getTcId()
  {
    return mRow).id;
  }
  
  public final int h()
  {
    return org.c.a.a.a.b.a.a(mRow).spamScore);
  }
  
  public final int i()
  {
    return org.c.a.a.a.b.a.a(mRow).telType);
  }
  
  public final int j()
  {
    String str = mRow).telType;
    int i = ContactDto.Contact.PhoneNumber.EMPTY_TEL_TYPE;
    return org.c.a.a.a.b.a.a(str, i);
  }
  
  public final String k()
  {
    return mRow).telTypeLabel;
  }
  
  public final String l()
  {
    return mRow).countryCode;
  }
  
  public final k.d m()
  {
    String str = mRow).numberType;
    k.d locald = k.d.l;
    return ab.a(str, locald);
  }
  
  public boolean mergeEquals(f paramf)
  {
    if (this == paramf) {
      return true;
    }
    boolean bool = paramf instanceof Number;
    if (!bool) {
      return false;
    }
    paramf = (Number)paramf;
    String str = a();
    paramf = paramf.a();
    return TextUtils.equals(str, paramf);
  }
  
  public final String n()
  {
    String str1 = d();
    boolean bool;
    if (str1 != null)
    {
      bool = ab.b(str1);
      if (bool) {
        return str1;
      }
    }
    String str2 = d;
    if (str2 != null)
    {
      str2 = l();
      if (str2 != null)
      {
        str2 = c();
        bool = TextUtils.isEmpty(str2);
        String str3;
        if (!bool)
        {
          str2 = d;
          str3 = l();
          bool = str2.contains(str3);
          if (bool) {
            return c();
          }
        }
        str2 = a();
        bool = TextUtils.isEmpty(str2);
        if (!bool)
        {
          str2 = d;
          str3 = l();
          bool = str2.contains(str3);
          if (!bool) {
            return aa.d(a());
          }
        }
      }
    }
    str2 = a();
    if (str1 == null) {
      return str2;
    }
    return str1;
  }
  
  public final String o()
  {
    String str = d();
    boolean bool = ab.e(str);
    if (bool) {
      return d();
    }
    str = a();
    bool = ab.e(str);
    if (bool) {
      return a();
    }
    str = c();
    bool = ab.e(str);
    if (bool) {
      return c();
    }
    return null;
  }
  
  public void setSource(int paramInt)
  {
    c = paramInt;
  }
  
  public void setTcId(String paramString)
  {
    super.setTcId(paramString);
    mRow).id = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramInt = c;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Number
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */