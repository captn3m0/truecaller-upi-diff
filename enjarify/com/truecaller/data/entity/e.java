package com.truecaller.data.entity;

import android.content.Intent;
import android.graphics.drawable.Drawable;

public final class e
{
  public final String a;
  public final Drawable b;
  public final Intent c;
  public final String d;
  
  public e(String paramString1, Drawable paramDrawable, Intent paramIntent, String paramString2)
  {
    a = paramString1;
    b = paramDrawable;
    c = paramIntent;
    d = paramString2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */