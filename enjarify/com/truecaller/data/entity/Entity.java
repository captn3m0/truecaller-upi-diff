package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.truecaller.common.h.am;

public abstract class Entity
  implements Parcelable
{
  private Long mId;
  private Object mTag;
  private String mTcId;
  
  protected Entity() {}
  
  protected Entity(Parcel paramParcel)
  {
    Object localObject = Long.class.getClassLoader();
    localObject = (Long)paramParcel.readValue((ClassLoader)localObject);
    mId = ((Long)localObject);
    paramParcel = paramParcel.readString();
    mTcId = paramParcel;
  }
  
  static int presentationCompare(String... paramVarArgs)
  {
    int i = 0;
    String str1 = null;
    int k = 0;
    int j;
    while (i == 0)
    {
      int m = paramVarArgs.length;
      if (k >= m) {
        break;
      }
      str1 = am.a(paramVarArgs[k]);
      m = k + 1;
      String str2 = am.a(paramVarArgs[m]);
      boolean bool3 = TextUtils.isEmpty(str1);
      boolean bool4 = true;
      if (!bool3)
      {
        bool3 = TextUtils.isEmpty(str2);
        if (!bool3)
        {
          i = am.a(str1, str2, bool4);
          break label118;
        }
      }
      boolean bool2 = TextUtils.equals(str1, str2);
      if (bool2)
      {
        i = 0;
        str1 = null;
      }
      else
      {
        boolean bool1 = TextUtils.isEmpty(str1);
        if (bool1) {
          bool1 = true;
        } else {
          j = -1;
        }
      }
      label118:
      k += 2;
    }
    return j;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Long getId()
  {
    return mId;
  }
  
  public Object getTag()
  {
    return mTag;
  }
  
  public String getTcId()
  {
    return mTcId;
  }
  
  public void setId(Long paramLong)
  {
    mId = paramLong;
  }
  
  public void setTag(Object paramObject)
  {
    mTag = paramObject;
  }
  
  public void setTcId(String paramString)
  {
    mTcId = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject = mId;
    paramParcel.writeValue(localObject);
    localObject = mTcId;
    paramParcel.writeString((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Entity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */