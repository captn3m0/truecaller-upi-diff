package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.truecaller.search.ContactDto.Contact.Source;

public class Source
  extends RowEntity
  implements f
{
  public static final Parcelable.Creator CREATOR;
  
  static
  {
    Source.1 local1 = new com/truecaller/data/entity/Source$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public Source()
  {
    this(localSource);
  }
  
  protected Source(Parcel paramParcel)
  {
    super(paramParcel);
  }
  
  Source(ContactDto.Contact.Source paramSource)
  {
    super(paramSource);
  }
  
  public final String a()
  {
    return mRow).id;
  }
  
  public final String b()
  {
    return mRow).url;
  }
  
  public final String c()
  {
    return mRow).logo;
  }
  
  public final String d()
  {
    return mRow).caption;
  }
  
  public boolean mergeEquals(f paramf)
  {
    if (this == paramf) {
      return true;
    }
    boolean bool = paramf instanceof Source;
    if (!bool) {
      return false;
    }
    paramf = (Source)paramf;
    String str = a();
    paramf = paramf.a();
    return TextUtils.equals(str, paramf);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Source
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */