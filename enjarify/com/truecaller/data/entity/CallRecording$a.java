package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class CallRecording$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    CallRecording localCallRecording = new com/truecaller/data/entity/CallRecording;
    long l = paramParcel.readLong();
    String str = paramParcel.readString();
    paramParcel = paramParcel.readString();
    localCallRecording.<init>(l, str, paramParcel);
    return localCallRecording;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new CallRecording[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.CallRecording.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */