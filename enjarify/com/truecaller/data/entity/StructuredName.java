package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.truecaller.search.ContactDto.Contact.StructuredName;

public final class StructuredName
  extends RowEntity
{
  public static final Parcelable.Creator CREATOR;
  public static final StructuredName.a Companion;
  
  static
  {
    Object localObject = new com/truecaller/data/entity/StructuredName$a;
    ((StructuredName.a)localObject).<init>((byte)0);
    Companion = (StructuredName.a)localObject;
    localObject = new com/truecaller/data/entity/StructuredName$b;
    ((StructuredName.b)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public StructuredName()
  {
    this(localStructuredName);
  }
  
  private StructuredName(Parcel paramParcel)
  {
    super(paramParcel);
  }
  
  public StructuredName(StructuredName paramStructuredName)
  {
    this(localStructuredName);
  }
  
  public StructuredName(ContactDto.Contact.StructuredName paramStructuredName)
  {
    super(paramStructuredName);
  }
  
  public final String getFamilyName()
  {
    return mRow).familyName;
  }
  
  public final String getGivenName()
  {
    return mRow).givenName;
  }
  
  public final String getMiddleName()
  {
    return mRow).middleName;
  }
  
  public final void setFamilyName(String paramString)
  {
    mRow).familyName = paramString;
  }
  
  public final void setGivenName(String paramString)
  {
    mRow).givenName = paramString;
  }
  
  public final void setMiddleName(String paramString)
  {
    mRow).middleName = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.StructuredName
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */