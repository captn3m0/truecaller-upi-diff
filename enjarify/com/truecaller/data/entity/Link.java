package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.truecaller.search.ContactDto.Contact.InternetAddress;

public class Link
  extends RowEntity
  implements f
{
  public static final Parcelable.Creator CREATOR;
  
  static
  {
    Link.1 local1 = new com/truecaller/data/entity/Link$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public Link()
  {
    this(localInternetAddress);
  }
  
  protected Link(Parcel paramParcel)
  {
    super(paramParcel);
  }
  
  public Link(ContactDto.Contact.InternetAddress paramInternetAddress)
  {
    super(paramInternetAddress);
  }
  
  public String getCaption()
  {
    return mRow).caption;
  }
  
  public String getInfo()
  {
    return mRow).id;
  }
  
  public String getService()
  {
    return mRow).service;
  }
  
  public boolean mergeEquals(f paramf)
  {
    boolean bool1 = true;
    if (this == paramf) {
      return bool1;
    }
    boolean bool2 = paramf instanceof Link;
    if (!bool2) {
      return false;
    }
    paramf = (Link)paramf;
    String str1 = getInfo();
    String str2 = paramf.getInfo();
    bool2 = TextUtils.equals(str1, str2);
    if (bool2)
    {
      str1 = getService();
      paramf = paramf.getService();
      boolean bool3 = TextUtils.equals(str1, paramf);
      if (bool3) {
        return bool1;
      }
    }
    return false;
  }
  
  public void setCaption(String paramString)
  {
    mRow).caption = paramString;
  }
  
  public void setInfo(String paramString)
  {
    mRow).id = paramString;
  }
  
  public void setService(String paramString)
  {
    mRow).service = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Link
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */