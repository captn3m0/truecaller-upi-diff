package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.truecaller.search.ContactDto.Contact.Note;

public final class Note
  extends RowEntity
{
  public static final Parcelable.Creator CREATOR;
  public static final Note.a Companion;
  
  static
  {
    Object localObject = new com/truecaller/data/entity/Note$a;
    ((Note.a)localObject).<init>((byte)0);
    Companion = (Note.a)localObject;
    localObject = new com/truecaller/data/entity/Note$b;
    ((Note.b)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public Note()
  {
    this(localNote);
  }
  
  private Note(Parcel paramParcel)
  {
    super(paramParcel);
  }
  
  public Note(Note paramNote)
  {
    this(localNote);
  }
  
  public Note(ContactDto.Contact.Note paramNote)
  {
    super(paramNote);
  }
  
  public final String getValue()
  {
    return mRow).note;
  }
  
  public final void setValue(String paramString)
  {
    mRow).note = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Note
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */