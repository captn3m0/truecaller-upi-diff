package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.truecaller.search.ContactDto.Contact.Tag;

public class Tag
  extends RowEntity
  implements f
{
  public static final Parcelable.Creator CREATOR;
  
  static
  {
    Tag.1 local1 = new com/truecaller/data/entity/Tag$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public Tag()
  {
    this(localTag);
  }
  
  protected Tag(Parcel paramParcel)
  {
    super(paramParcel);
  }
  
  Tag(ContactDto.Contact.Tag paramTag)
  {
    super(paramTag);
  }
  
  public final String a()
  {
    return mRow).tag;
  }
  
  public final void a(String paramString)
  {
    mRow).tag = paramString;
  }
  
  public boolean mergeEquals(f paramf)
  {
    boolean bool1 = true;
    if (this == paramf) {
      return bool1;
    }
    boolean bool2 = paramf instanceof Tag;
    if (!bool2) {
      return false;
    }
    paramf = (Tag)paramf;
    String str1 = a();
    String str2 = paramf.a();
    bool2 = TextUtils.equals(str1, str2);
    if (bool2)
    {
      int i = getSource();
      int j = paramf.getSource();
      if (i == j) {
        return bool1;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Tag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */