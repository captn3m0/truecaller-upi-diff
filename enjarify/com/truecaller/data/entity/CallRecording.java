package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class CallRecording
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final long a;
  public final String b;
  public final String c;
  
  static
  {
    CallRecording.a locala = new com/truecaller/data/entity/CallRecording$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public CallRecording(long paramLong, String paramString1, String paramString2)
  {
    a = paramLong;
    b = paramString1;
    c = paramString2;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof CallRecording;
      if (bool2)
      {
        paramObject = (CallRecording)paramObject;
        long l1 = a;
        long l2 = a;
        bool2 = l1 < l2;
        String str1;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          str1 = null;
        }
        if (bool2)
        {
          str1 = b;
          String str2 = b;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            str1 = c;
            paramObject = c;
            boolean bool3 = k.a(str1, paramObject);
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    long l2 = l1 >>> 32;
    l1 ^= l2;
    int i = (int)l1 * 31;
    String str = b;
    int j = 0;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = c;
    if (str != null) {
      j = str.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CallRecording(rowId=");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(", historyEventId=");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", absolutePath=");
    str = c;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    long l = a;
    paramParcel.writeLong(l);
    String str = b;
    paramParcel.writeString(str);
    str = c;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.CallRecording
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */