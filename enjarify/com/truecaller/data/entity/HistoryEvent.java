package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.c.a.g;
import com.google.c.a.k;
import com.google.c.a.k.c;
import com.google.c.a.k.d;
import com.truecaller.backup.CallLogBackupItem;
import com.truecaller.common.b.a;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.flash.CallLogFlashItem;
import java.util.UUID;

public class HistoryEvent
  extends Entity
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private static volatile k s;
  private static volatile String t;
  public String a = "";
  public String b;
  public String c;
  public String d;
  public String e;
  public Contact f;
  Long g;
  public long h;
  public long i;
  public String j;
  int k;
  int l;
  public CallRecording m;
  int n;
  public int o;
  public int p;
  public String q;
  public int r;
  private k.d u;
  
  static
  {
    HistoryEvent.1 local1 = new com/truecaller/data/entity/HistoryEvent$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private HistoryEvent()
  {
    j = "-1";
    n = 1;
    p = 4;
  }
  
  private HistoryEvent(Parcel paramParcel)
  {
    Object localObject1 = "-1";
    j = ((String)localObject1);
    int i1 = 1;
    n = i1;
    p = 4;
    Object localObject2 = paramParcel.readString();
    setTcId((String)localObject2);
    localObject2 = paramParcel.readString();
    b = ((String)localObject2);
    localObject2 = paramParcel.readString();
    c = ((String)localObject2);
    localObject2 = paramParcel.readString();
    d = ((String)localObject2);
    int i2 = paramParcel.readInt();
    Long localLong = null;
    int i3 = -1;
    if (i2 == i3)
    {
      u = null;
    }
    else
    {
      k.d[] arrayOfd = k.d.values();
      localObject2 = arrayOfd[i2];
      u = ((k.d)localObject2);
    }
    i2 = paramParcel.readInt();
    o = i2;
    i2 = paramParcel.readInt();
    p = i2;
    long l1 = paramParcel.readLong();
    h = l1;
    l1 = paramParcel.readLong();
    i = l1;
    i2 = paramParcel.readInt();
    k = i2;
    i2 = paramParcel.readInt();
    n = i2;
    i2 = paramParcel.readInt();
    l = i2;
    localObject2 = paramParcel.readString();
    q = ((String)localObject2);
    i2 = paramParcel.readInt();
    r = i2;
    i2 = paramParcel.readByte();
    if (i2 == i1)
    {
      l1 = paramParcel.readLong();
      localObject2 = Long.valueOf(l1);
    }
    else
    {
      i2 = 0;
      localObject2 = null;
    }
    setId((Long)localObject2);
    i2 = paramParcel.readByte();
    if (i2 == i1)
    {
      long l2 = paramParcel.readLong();
      localLong = Long.valueOf(l2);
    }
    g = localLong;
    i2 = paramParcel.readByte();
    if (i2 == i1)
    {
      localObject2 = Contact.class.getClassLoader();
      localObject2 = (Contact)paramParcel.readParcelable((ClassLoader)localObject2);
      f = ((Contact)localObject2);
    }
    localObject2 = paramParcel.readString();
    j = ((String)localObject2);
    localObject2 = paramParcel.readString();
    a = ((String)localObject2);
    i2 = paramParcel.readByte();
    if (i2 == i1)
    {
      localObject1 = CallRecording.class.getClassLoader();
      paramParcel = (CallRecording)paramParcel.readParcelable((ClassLoader)localObject1);
      m = paramParcel;
    }
  }
  
  public HistoryEvent(CallLogBackupItem paramCallLogBackupItem)
  {
    this(str);
    long l1 = paramCallLogBackupItem.getTimestamp();
    h = l1;
    l1 = paramCallLogBackupItem.getDuration();
    i = l1;
    int i1 = paramCallLogBackupItem.getType();
    o = i1;
    i1 = paramCallLogBackupItem.getAction();
    p = i1;
    i1 = paramCallLogBackupItem.getFeatures();
    k = i1;
    paramCallLogBackupItem = paramCallLogBackupItem.getComponentName();
    q = paramCallLogBackupItem;
    n = 0;
    l = 1;
    r = 2;
  }
  
  public HistoryEvent(Contact paramContact, int paramInt)
  {
    j = "-1";
    n = 1;
    int i1 = 4;
    p = i1;
    v();
    Object localObject1 = paramContact.getTcId();
    setTcId((String)localObject1);
    localObject1 = paramContact.p();
    b = ((String)localObject1);
    localObject1 = paramContact.r();
    if (localObject1 != null)
    {
      Object localObject2 = ((Number)localObject1).d();
      c = ((String)localObject2);
      localObject2 = ((Number)localObject1).a();
      b = ((String)localObject2);
      localObject2 = ((Number)localObject1).m();
      u = ((k.d)localObject2);
      localObject1 = ((Number)localObject1).l();
      d = ((String)localObject1);
    }
    o = paramInt;
    paramInt = 0;
    g = null;
    long l1 = System.currentTimeMillis();
    h = l1;
    l1 = 0L;
    i = l1;
    boolean bool = paramContact.U();
    if (bool)
    {
      int i2 = 2;
      p = i2;
    }
  }
  
  public HistoryEvent(Number paramNumber)
  {
    j = "-1";
    n = 1;
    p = 4;
    Object localObject = paramNumber.d();
    c = ((String)localObject);
    localObject = paramNumber.a();
    b = ((String)localObject);
    localObject = paramNumber.m();
    u = ((k.d)localObject);
    paramNumber = paramNumber.l();
    d = paramNumber;
  }
  
  public HistoryEvent(CallLogFlashItem paramCallLogFlashItem)
  {
    this(str);
    long l1 = paramCallLogFlashItem.getTimestamp();
    h = l1;
    l1 = paramCallLogFlashItem.getDuration();
    i = l1;
    int i1 = paramCallLogFlashItem.getType();
    o = i1;
    i1 = paramCallLogFlashItem.getAction();
    p = i1;
    i1 = paramCallLogFlashItem.getFeatures();
    k = i1;
    paramCallLogFlashItem = paramCallLogFlashItem.getComponentName();
    q = paramCallLogFlashItem;
    n = 0;
    l = 1;
    r = 3;
    paramCallLogFlashItem = UUID.randomUUID().toString();
  }
  
  public HistoryEvent(String paramString)
  {
    Object localObject1 = "-1";
    j = ((String)localObject1);
    int i1 = 1;
    n = i1;
    p = 4;
    boolean bool = ab.a(paramString);
    if (bool) {
      return;
    }
    v();
    c = paramString;
    try
    {
      Object localObject2 = s;
      localObject3 = t;
      paramString = ((k)localObject2).a(paramString, (String)localObject3);
      localObject2 = s;
      localObject3 = k.c.a;
      localObject2 = ((k)localObject2).a(paramString, (k.c)localObject3);
      b = ((String)localObject2);
      localObject2 = s;
      paramString = ((k)localObject2).b(paramString);
      u = paramString;
      paramString = b;
      paramString = h.c(paramString);
      if (paramString != null)
      {
        localObject2 = c;
        bool = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool)
        {
          paramString = c;
          paramString = paramString.toUpperCase();
          d = paramString;
          return;
        }
      }
      paramString = t;
      d = paramString;
      return;
    }
    catch (g paramString)
    {
      localObject1 = new String[i1];
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Cannot parse number, ");
      paramString = paramString.getMessage();
      ((StringBuilder)localObject3).append(paramString);
      paramString = ((StringBuilder)localObject3).toString();
      localObject1[0] = paramString;
    }
  }
  
  private void v()
  {
    Object localObject1 = s;
    if (localObject1 == null) {
      try
      {
        localObject1 = s;
        if (localObject1 == null)
        {
          localObject1 = a.F();
          localObject1 = ((a)localObject1).H();
          t = (String)localObject1;
          localObject1 = k.a();
          s = (k)localObject1;
        }
        return;
      }
      finally {}
    }
  }
  
  public final String a()
  {
    return b;
  }
  
  public final void a(int paramInt)
  {
    o = paramInt;
  }
  
  public final void a(long paramLong)
  {
    h = paramLong;
  }
  
  public final void a(CallRecording paramCallRecording)
  {
    m = paramCallRecording;
  }
  
  public final void a(Long paramLong)
  {
    g = paramLong;
  }
  
  public final void a(String paramString)
  {
    c = paramString;
  }
  
  public final String b()
  {
    return c;
  }
  
  public final void b(int paramInt)
  {
    p = paramInt;
  }
  
  public final void b(long paramLong)
  {
    i = paramLong;
  }
  
  public final void b(String paramString)
  {
    q = paramString;
  }
  
  public final k.d c()
  {
    return u;
  }
  
  public final String d()
  {
    return d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final String e()
  {
    return e;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    String str = null;
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (HistoryEvent)paramObject;
        localObject1 = a;
        localObject2 = a;
        boolean bool1 = ((String)localObject1).equals(localObject2);
        if (!bool1) {
          return false;
        }
        int i1 = o;
        int i2 = o;
        if (i1 != i2) {
          return false;
        }
        i1 = p;
        i2 = p;
        if (i1 != i2) {
          return false;
        }
        long l1 = h;
        long l2 = h;
        boolean bool3 = l1 < l2;
        if (bool3) {
          return false;
        }
        l1 = i;
        l2 = i;
        bool3 = l1 < l2;
        if (bool3) {
          return false;
        }
        i1 = k;
        i2 = k;
        if (i1 != i2) {
          return false;
        }
        localObject1 = b;
        boolean bool2;
        if (localObject1 != null)
        {
          localObject2 = b;
          bool2 = ((String)localObject1).equals(localObject2);
          if (bool2) {
            break label221;
          }
        }
        else
        {
          localObject1 = b;
          if (localObject1 == null) {
            break label221;
          }
        }
        return false;
        label221:
        localObject1 = c;
        if (localObject1 != null)
        {
          localObject2 = c;
          bool2 = ((String)localObject1).equals(localObject2);
          if (bool2) {
            break label263;
          }
        }
        else
        {
          localObject1 = c;
          if (localObject1 == null) {
            break label263;
          }
        }
        return false;
        label263:
        localObject1 = d;
        if (localObject1 != null)
        {
          localObject2 = d;
          bool2 = ((String)localObject1).equals(localObject2);
          if (bool2) {
            break label305;
          }
        }
        else
        {
          localObject1 = d;
          if (localObject1 == null) {
            break label305;
          }
        }
        return false;
        label305:
        localObject1 = e;
        if (localObject1 != null)
        {
          localObject2 = e;
          bool2 = ((String)localObject1).equals(localObject2);
          if (bool2) {
            break label347;
          }
        }
        else
        {
          localObject1 = e;
          if (localObject1 == null) {
            break label347;
          }
        }
        return false;
        label347:
        localObject1 = u;
        localObject2 = u;
        if (localObject1 != localObject2) {
          return false;
        }
        localObject1 = g;
        if (localObject1 != null)
        {
          localObject2 = g;
          bool2 = ((Long)localObject1).equals(localObject2);
          if (bool2) {
            break label408;
          }
        }
        else
        {
          localObject1 = g;
          if (localObject1 == null) {
            break label408;
          }
        }
        return false;
        label408:
        localObject1 = m;
        if (localObject1 != null)
        {
          localObject2 = m;
          bool2 = ((CallRecording)localObject1).equals(localObject2);
          if (!bool2) {
            break label450;
          }
        }
        else
        {
          localObject1 = m;
          if (localObject1 == null) {
            break label450;
          }
        }
        return false;
        label450:
        str = j;
        paramObject = j;
        return str.equals(paramObject);
      }
    }
    return false;
  }
  
  public final int f()
  {
    return o;
  }
  
  public final int g()
  {
    int i1 = o;
    switch (i1)
    {
    case 4: 
    default: 
      return 0;
    case 6: 
      return 21;
    case 5: 
      return 4;
    case 3: 
      return 6;
    case 2: 
      return 1;
    }
    return 2;
  }
  
  public final int h()
  {
    return p;
  }
  
  public int hashCode()
  {
    String str = b;
    int i1 = 0;
    if (str != null)
    {
      i2 = str.hashCode();
    }
    else
    {
      i2 = 0;
      str = null;
    }
    i2 *= 31;
    Object localObject = c;
    if (localObject != null)
    {
      i3 = ((String)localObject).hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    int i2 = (i2 + i3) * 31;
    localObject = d;
    if (localObject != null)
    {
      i3 = ((String)localObject).hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    localObject = e;
    if (localObject != null)
    {
      i3 = ((String)localObject).hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    localObject = u;
    if (localObject != null)
    {
      i3 = ((k.d)localObject).hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    int i3 = o;
    i2 = (i2 + i3) * 31;
    i3 = p;
    i2 = (i2 + i3) * 31;
    localObject = g;
    if (localObject != null)
    {
      i3 = ((Long)localObject).hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    long l1 = h;
    int i4 = 32;
    long l2 = l1 >>> i4;
    int i5 = (int)(l1 ^ l2);
    i2 = (i2 + i5) * 31;
    l1 = i;
    long l3 = l1 >>> i4;
    l1 ^= l3;
    i5 = (int)l1;
    i2 = (i2 + i5) * 31;
    i3 = j.hashCode();
    i2 = (i2 + i3) * 31;
    i3 = k;
    i2 = (i2 + i3) * 31;
    i3 = a.hashCode();
    i2 = (i2 + i3) * 31;
    localObject = m;
    if (localObject != null) {
      i1 = ((CallRecording)localObject).hashCode();
    }
    return i2 + i1;
  }
  
  public final Long i()
  {
    return g;
  }
  
  public final long j()
  {
    return h;
  }
  
  public final long k()
  {
    return i;
  }
  
  public final String l()
  {
    return j;
  }
  
  public final int m()
  {
    return k;
  }
  
  public final void n()
  {
    k = 1;
  }
  
  public final int o()
  {
    return n;
  }
  
  public final int p()
  {
    return l;
  }
  
  public final String q()
  {
    return q;
  }
  
  public final int r()
  {
    return r;
  }
  
  public final Contact s()
  {
    return f;
  }
  
  public final CallRecording t()
  {
    return m;
  }
  
  public String toString()
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("HistoryEvent:{id=");
    Object localObject2 = getId();
    ((StringBuilder)localObject1).append(localObject2);
    ((StringBuilder)localObject1).append(", tcId=");
    localObject2 = getTcId();
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(", normalizedNumber=");
    localObject2 = b;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    if (localObject1 == null) {
      return "null";
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("<non-null normalized number>, rawNumber=");
    localObject2 = c;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    if (localObject1 == null) {
      return "null";
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("<non-null raw number>, cachedName=");
    localObject2 = e;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    if (localObject1 == null) {
      return "null";
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("<non-null cached name>, numberType=");
    localObject2 = u;
    ((StringBuilder)localObject1).append(localObject2);
    ((StringBuilder)localObject1).append(", type=");
    int i1 = o;
    ((StringBuilder)localObject1).append(i1);
    ((StringBuilder)localObject1).append(", action=");
    i1 = p;
    ((StringBuilder)localObject1).append(i1);
    ((StringBuilder)localObject1).append(", callLogId=");
    localObject2 = g;
    ((StringBuilder)localObject1).append(localObject2);
    ((StringBuilder)localObject1).append(", timestamp=");
    long l1 = h;
    ((StringBuilder)localObject1).append(l1);
    ((StringBuilder)localObject1).append(", duration=");
    l1 = i;
    ((StringBuilder)localObject1).append(l1);
    ((StringBuilder)localObject1).append(", features=");
    i1 = k;
    ((StringBuilder)localObject1).append(i1);
    ((StringBuilder)localObject1).append(", isNew=");
    i1 = k;
    ((StringBuilder)localObject1).append(i1);
    ((StringBuilder)localObject1).append(", isRead=");
    i1 = k;
    ((StringBuilder)localObject1).append(i1);
    ((StringBuilder)localObject1).append(", phoneAccountComponentName=");
    localObject2 = q;
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(", contact=");
    localObject2 = f;
    ((StringBuilder)localObject1).append(localObject2);
    ((StringBuilder)localObject1).append(", eventId=");
    localObject2 = a;
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(", callRecording=");
    localObject2 = m;
    ((StringBuilder)localObject1).append(localObject2);
    ((StringBuilder)localObject1).append("}");
    return ((StringBuilder)localObject1).toString();
  }
  
  public final String u()
  {
    return a;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject = getTcId();
    paramParcel.writeString((String)localObject);
    localObject = b;
    paramParcel.writeString((String)localObject);
    localObject = c;
    paramParcel.writeString((String)localObject);
    localObject = d;
    paramParcel.writeString((String)localObject);
    localObject = u;
    if (localObject == null) {
      i1 = -1;
    } else {
      i1 = ((k.d)localObject).ordinal();
    }
    paramParcel.writeInt(i1);
    int i1 = o;
    paramParcel.writeInt(i1);
    i1 = p;
    paramParcel.writeInt(i1);
    long l1 = h;
    paramParcel.writeLong(l1);
    l1 = i;
    paramParcel.writeLong(l1);
    i1 = k;
    paramParcel.writeInt(i1);
    i1 = n;
    paramParcel.writeInt(i1);
    i1 = l;
    paramParcel.writeInt(i1);
    localObject = q;
    paramParcel.writeString((String)localObject);
    i1 = r;
    paramParcel.writeInt(i1);
    localObject = getId();
    byte b1 = 1;
    long l2;
    if (localObject == null)
    {
      paramParcel.writeByte((byte)0);
    }
    else
    {
      paramParcel.writeByte(b1);
      localObject = getId();
      l2 = ((Long)localObject).longValue();
      paramParcel.writeLong(l2);
    }
    localObject = g;
    if (localObject == null)
    {
      paramParcel.writeByte((byte)0);
    }
    else
    {
      paramParcel.writeByte(b1);
      localObject = g;
      l2 = ((Long)localObject).longValue();
      paramParcel.writeLong(l2);
    }
    localObject = f;
    if (localObject == null)
    {
      paramParcel.writeByte((byte)0);
    }
    else
    {
      paramParcel.writeByte(b1);
      localObject = f;
      paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    }
    localObject = j;
    paramParcel.writeString((String)localObject);
    localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = m;
    if (localObject == null)
    {
      paramParcel.writeByte((byte)0);
      return;
    }
    paramParcel.writeByte(b1);
    localObject = m;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.HistoryEvent
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */