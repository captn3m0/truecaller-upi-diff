package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.truecaller.search.ContactDto.Contact.Business;

public final class Business
  extends RowEntity
{
  public static final Parcelable.Creator CREATOR;
  public static final Business.a Companion;
  
  static
  {
    Object localObject = new com/truecaller/data/entity/Business$a;
    ((Business.a)localObject).<init>((byte)0);
    Companion = (Business.a)localObject;
    localObject = new com/truecaller/data/entity/Business$b;
    ((Business.b)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public Business()
  {
    this(localBusiness);
  }
  
  private Business(Parcel paramParcel)
  {
    super(paramParcel);
  }
  
  public Business(Business paramBusiness)
  {
    this(localBusiness);
  }
  
  public Business(ContactDto.Contact.Business paramBusiness)
  {
    super(paramBusiness);
  }
  
  public final String getBranch()
  {
    return mRow).branch;
  }
  
  public final String getCompanySize()
  {
    return mRow).companySize;
  }
  
  public final String getDepartment()
  {
    return mRow).department;
  }
  
  public final String getLandline()
  {
    return mRow).landline;
  }
  
  public final String getOpeningHours()
  {
    return mRow).openingHours;
  }
  
  public final String getScore()
  {
    return mRow).score;
  }
  
  public final String getSwishNumber()
  {
    return mRow).swishNumber;
  }
  
  public final void setBranch(String paramString)
  {
    mRow).branch = paramString;
  }
  
  public final void setCompanySize(String paramString)
  {
    mRow).companySize = paramString;
  }
  
  public final void setDepartment(String paramString)
  {
    mRow).department = paramString;
  }
  
  public final void setLandline(String paramString)
  {
    mRow).landline = paramString;
  }
  
  public final void setOpeningHours(String paramString)
  {
    mRow).openingHours = paramString;
  }
  
  public final void setScore(String paramString)
  {
    mRow).score = paramString;
  }
  
  public final void setSwishNumber(String paramString)
  {
    mRow).swishNumber = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Business
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */