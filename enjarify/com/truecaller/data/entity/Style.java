package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.truecaller.search.ContactDto.Contact.Style;

public final class Style
  extends RowEntity
{
  public static final Parcelable.Creator CREATOR;
  public static final Style.a Companion;
  
  static
  {
    Object localObject = new com/truecaller/data/entity/Style$a;
    ((Style.a)localObject).<init>((byte)0);
    Companion = (Style.a)localObject;
    localObject = new com/truecaller/data/entity/Style$b;
    ((Style.b)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public Style()
  {
    this(localStyle);
  }
  
  private Style(Parcel paramParcel)
  {
    super(paramParcel);
  }
  
  public Style(Style paramStyle)
  {
    this(localStyle);
  }
  
  public Style(ContactDto.Contact.Style paramStyle)
  {
    super(paramStyle);
  }
  
  public final String getBackgroundColor()
  {
    return mRow).backgroundColor;
  }
  
  public final String getImageUrls()
  {
    return mRow).imageUrls;
  }
  
  public final void setBackgroundColor(String paramString)
  {
    mRow).backgroundColor = paramString;
  }
  
  public final void setImageUrls(String paramString)
  {
    mRow).imageUrls = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Style
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */