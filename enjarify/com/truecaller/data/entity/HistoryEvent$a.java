package com.truecaller.data.entity;

import com.google.c.a.k.d;

public final class HistoryEvent$a
{
  public final HistoryEvent a;
  
  public HistoryEvent$a()
  {
    HistoryEvent localHistoryEvent = new com/truecaller/data/entity/HistoryEvent;
    localHistoryEvent.<init>((byte)0);
    a = localHistoryEvent;
  }
  
  public final a a(int paramInt)
  {
    a.o = paramInt;
    return this;
  }
  
  public final a a(long paramLong)
  {
    a.h = paramLong;
    return this;
  }
  
  public final a a(k.d paramd)
  {
    HistoryEvent.a(a, paramd);
    return this;
  }
  
  public final a a(Long paramLong)
  {
    a.g = paramLong;
    return this;
  }
  
  public final a a(String paramString)
  {
    a.b = paramString;
    return this;
  }
  
  public final a b(int paramInt)
  {
    a.p = paramInt;
    return this;
  }
  
  public final a b(long paramLong)
  {
    a.i = paramLong;
    return this;
  }
  
  public final a b(String paramString)
  {
    a.c = paramString;
    return this;
  }
  
  public final a c(int paramInt)
  {
    a.k = paramInt;
    return this;
  }
  
  public final a c(String paramString)
  {
    HistoryEvent.a(a, paramString);
    return this;
  }
  
  public final a d(int paramInt)
  {
    a.n = paramInt;
    return this;
  }
  
  public final a d(String paramString)
  {
    HistoryEvent.b(a, paramString);
    return this;
  }
  
  public final a e(int paramInt)
  {
    a.l = paramInt;
    return this;
  }
  
  public final a e(String paramString)
  {
    a.j = paramString;
    return this;
  }
  
  public final a f(String paramString)
  {
    a.q = paramString;
    return this;
  }
  
  public final a g(String paramString)
  {
    a.a = paramString;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.HistoryEvent.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */