package com.truecaller.data.entity;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.search.ContactDto.Contact;
import com.truecaller.search.ContactDto.Contact.Address;
import com.truecaller.search.ContactDto.Contact.Business;
import com.truecaller.search.ContactDto.Contact.InternetAddress;
import com.truecaller.search.ContactDto.Contact.PhoneNumber;
import com.truecaller.search.ContactDto.Contact.Source;
import com.truecaller.search.ContactDto.Contact.Style;
import com.truecaller.search.ContactDto.Contact.Tag;
import com.truecaller.search.ContactDto.Row;
import com.truecaller.search.local.model.a.g;
import com.truecaller.util.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Contact
  extends RowEntity
{
  public static final Parcelable.Creator CREATOR;
  public final List a;
  public final List b;
  public transient Uri c;
  public transient boolean d;
  public List e;
  public int f;
  public StructuredName g;
  public Note h;
  public Business i;
  public Style j;
  public int k;
  public Integer l;
  private final List m;
  private final List n;
  private final List o;
  private transient List p;
  private List q;
  private List r;
  private List s;
  private List t;
  
  static
  {
    Contact.1 local1 = new com/truecaller/data/entity/Contact$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public Contact()
  {
    this(localContact);
  }
  
  protected Contact(Parcel paramParcel)
  {
    super(paramParcel);
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    m = ((List)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    n = ((List)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    a = ((List)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    b = ((List)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    o = ((List)localObject1);
    boolean bool = false;
    localObject1 = null;
    f = 0;
    Object localObject2 = m;
    Object localObject3 = Address.CREATOR;
    localObject3 = paramParcel.createTypedArrayList((Parcelable.Creator)localObject3);
    ((List)localObject2).addAll((Collection)localObject3);
    localObject2 = n;
    localObject3 = Number.CREATOR;
    localObject3 = paramParcel.createTypedArrayList((Parcelable.Creator)localObject3);
    ((List)localObject2).addAll((Collection)localObject3);
    localObject2 = a;
    localObject3 = Tag.CREATOR;
    localObject3 = paramParcel.createTypedArrayList((Parcelable.Creator)localObject3);
    ((List)localObject2).addAll((Collection)localObject3);
    localObject2 = b;
    localObject3 = Source.CREATOR;
    localObject3 = paramParcel.createTypedArrayList((Parcelable.Creator)localObject3);
    ((List)localObject2).addAll((Collection)localObject3);
    localObject2 = o;
    localObject3 = Link.CREATOR;
    localObject3 = paramParcel.createTypedArrayList((Parcelable.Creator)localObject3);
    ((List)localObject2).addAll((Collection)localObject3);
    int i1 = paramParcel.readInt();
    f = i1;
    localObject2 = Integer.class.getClassLoader();
    localObject2 = (Integer)paramParcel.readValue((ClassLoader)localObject2);
    l = ((Integer)localObject2);
    localObject2 = Uri.class.getClassLoader();
    localObject2 = (Uri)paramParcel.readParcelable((ClassLoader)localObject2);
    c = ((Uri)localObject2);
    i1 = paramParcel.readByte();
    if (i1 != 0) {
      bool = true;
    }
    d = bool;
    localObject1 = StructuredName.class.getClassLoader();
    localObject1 = (StructuredName)paramParcel.readParcelable((ClassLoader)localObject1);
    g = ((StructuredName)localObject1);
    localObject1 = Note.class.getClassLoader();
    localObject1 = (Note)paramParcel.readParcelable((ClassLoader)localObject1);
    h = ((Note)localObject1);
    localObject1 = Business.class.getClassLoader();
    localObject1 = (Business)paramParcel.readParcelable((ClassLoader)localObject1);
    i = ((Business)localObject1);
    localObject1 = Style.class.getClassLoader();
    paramParcel = (Style)paramParcel.readParcelable((ClassLoader)localObject1);
    j = paramParcel;
  }
  
  public Contact(Contact paramContact)
  {
    this((ContactDto.Contact)localObject);
    localObject = c;
    c = ((Uri)localObject);
    boolean bool = d;
    d = bool;
    paramContact = l;
    l = paramContact;
  }
  
  public Contact(ContactDto.Contact paramContact)
  {
    super(paramContact);
    paramContact = new java/util/ArrayList;
    paramContact.<init>();
    m = paramContact;
    paramContact = new java/util/ArrayList;
    paramContact.<init>();
    n = paramContact;
    paramContact = new java/util/ArrayList;
    paramContact.<init>();
    a = paramContact;
    paramContact = new java/util/ArrayList;
    paramContact.<init>();
    b = paramContact;
    paramContact = new java/util/ArrayList;
    paramContact.<init>();
    o = paramContact;
    int i1 = 0;
    f = 0;
    paramContact = mRow).addresses;
    Object localObject1;
    List localList;
    Object localObject2;
    if (paramContact != null)
    {
      paramContact = mRow).addresses.iterator();
      for (;;)
      {
        boolean bool1 = paramContact.hasNext();
        if (!bool1) {
          break;
        }
        localObject1 = (ContactDto.Contact.Address)paramContact.next();
        localList = m;
        localObject2 = new com/truecaller/data/entity/Address;
        ((Address)localObject2).<init>((ContactDto.Contact.Address)localObject1);
        int i2 = getSource();
        localObject1 = a((RowEntity)localObject2, i2);
        localList.add(localObject1);
      }
    }
    paramContact = mRow).phones;
    if (paramContact != null)
    {
      paramContact = mRow).phones.iterator();
      for (;;)
      {
        boolean bool2 = paramContact.hasNext();
        if (!bool2) {
          break;
        }
        localObject1 = (ContactDto.Contact.PhoneNumber)paramContact.next();
        localList = n;
        localObject2 = new com/truecaller/data/entity/Number;
        ((Number)localObject2).<init>((ContactDto.Contact.PhoneNumber)localObject1);
        int i3 = getSource();
        localObject1 = a((RowEntity)localObject2, i3);
        localList.add(localObject1);
      }
    }
    paramContact = mRow).internetAddresses;
    if (paramContact != null)
    {
      paramContact = mRow).internetAddresses.iterator();
      for (;;)
      {
        boolean bool3 = paramContact.hasNext();
        if (!bool3) {
          break;
        }
        localObject1 = (ContactDto.Contact.InternetAddress)paramContact.next();
        localList = o;
        localObject2 = new com/truecaller/data/entity/Link;
        ((Link)localObject2).<init>((ContactDto.Contact.InternetAddress)localObject1);
        int i4 = getSource();
        localObject1 = a((RowEntity)localObject2, i4);
        localList.add(localObject1);
      }
    }
    paramContact = mRow).tags;
    if (paramContact != null)
    {
      paramContact = mRow).tags.iterator();
      for (;;)
      {
        boolean bool4 = paramContact.hasNext();
        if (!bool4) {
          break;
        }
        localObject1 = (ContactDto.Contact.Tag)paramContact.next();
        localList = a;
        localObject2 = new com/truecaller/data/entity/Tag;
        ((Tag)localObject2).<init>((ContactDto.Contact.Tag)localObject1);
        int i5 = getSource();
        localObject1 = a((RowEntity)localObject2, i5);
        localList.add(localObject1);
      }
    }
    paramContact = mRow).sources;
    int i6;
    if (paramContact != null)
    {
      paramContact = mRow).sources.iterator();
      for (;;)
      {
        boolean bool5 = paramContact.hasNext();
        if (!bool5) {
          break;
        }
        localObject1 = (ContactDto.Contact.Source)paramContact.next();
        localList = b;
        localObject2 = new com/truecaller/data/entity/Source;
        ((Source)localObject2).<init>((ContactDto.Contact.Source)localObject1);
        i6 = getSource();
        localObject1 = a((RowEntity)localObject2, i6);
        localList.add(localObject1);
      }
    }
    paramContact = mRow).business;
    if (paramContact != null)
    {
      paramContact = new com/truecaller/data/entity/Business;
      localObject1 = mRow).business;
      paramContact.<init>((ContactDto.Contact.Business)localObject1);
      i6 = getSource();
      paramContact = (Business)a(paramContact, i6);
      i = paramContact;
    }
    paramContact = mRow).style;
    if (paramContact != null)
    {
      paramContact = new com/truecaller/data/entity/Style;
      localObject1 = mRow).style;
      paramContact.<init>((ContactDto.Contact.Style)localObject1);
      i6 = getSource();
      paramContact = (Style)a(paramContact, i6);
      j = paramContact;
    }
    i1 = com.truecaller.content.a.a.a(mRow).badges);
    f = i1;
  }
  
  private Entity a(Entity paramEntity)
  {
    String str = getTcId();
    paramEntity.setTcId(str);
    return paramEntity;
  }
  
  private RowEntity a(RowEntity paramRowEntity, int paramInt)
  {
    String str = getTcId();
    paramRowEntity.setTcId(str);
    paramRowEntity.setSource(paramInt);
    return paramRowEntity;
  }
  
  private List a(List paramList1, List paramList2, Entity paramEntity, ContactDto.Row paramRow)
  {
    if (paramList2 == null)
    {
      paramList2 = new java/util/ArrayList;
      paramList2.<init>();
    }
    paramEntity = a(paramEntity);
    paramList1.add(paramEntity);
    paramList2.add(paramRow);
    return paramList2;
  }
  
  private static void a(f paramf1, f paramf2)
  {
    boolean bool1 = paramf1 instanceof Number;
    if (bool1)
    {
      paramf1 = (Number)paramf1;
      paramf2 = (Number)paramf2;
      int i1 = paramf2.h();
      int i3 = paramf1.h();
      if (i1 > i3)
      {
        i1 = paramf2.h();
        paramf1.a(i1);
      }
      Object localObject = paramf1.f();
      boolean bool2 = am.b((CharSequence)localObject);
      if (bool2)
      {
        localObject = paramf2.f();
        paramf1.d((String)localObject);
      }
      localObject = paramf1.getDataPhonebookId();
      if (localObject == null)
      {
        localObject = paramf2.getDataPhonebookId();
        paramf1.setDataPhonebookId((Long)localObject);
      }
      int i2 = paramf1.getSource();
      i3 = paramf2.getSource();
      i2 |= i3;
      paramf1.setSource(i2);
      i2 = paramf2.i();
      i3 = ContactDto.Contact.PhoneNumber.EMPTY_TEL_TYPE;
      if (i2 != i3)
      {
        i2 = paramf2.i();
        paramf1.b(i2);
        localObject = paramf2.k();
        paramf1.e((String)localObject);
        paramf2 = paramf2.m();
        paramf1.a(paramf2);
      }
    }
  }
  
  private void a(List paramList)
  {
    if (paramList != null)
    {
      boolean bool = paramList.isEmpty();
      if (!bool)
      {
        Object localObject = new java/util/ArrayList;
        int i1 = paramList.size();
        ((ArrayList)localObject).<init>(i1);
        p = ((List)localObject);
        paramList = paramList.iterator();
        for (;;)
        {
          bool = paramList.hasNext();
          if (!bool) {
            break;
          }
          localObject = (Number)paramList.next();
          i1 = ((Number)localObject).getSource() & 0xD;
          if (i1 != 0)
          {
            List localList = p;
            localList.add(localObject);
          }
        }
        return;
      }
    }
  }
  
  private String af()
  {
    Object localObject = o();
    boolean bool = N();
    if (bool)
    {
      bool = am.b((CharSequence)localObject);
      if (!bool)
      {
        String str1 = z();
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append((String)localObject);
        if (str1 != null)
        {
          localObject = new java/lang/StringBuilder;
          String str2 = " (";
          ((StringBuilder)localObject).<init>(str2);
          ((StringBuilder)localObject).append(str1);
          str1 = ")";
          ((StringBuilder)localObject).append(str1);
          localObject = ((StringBuilder)localObject).toString();
        }
        else
        {
          localObject = "";
        }
        localStringBuilder.append((String)localObject);
        return localStringBuilder.toString();
      }
    }
    return null;
  }
  
  private static void b(List paramList)
  {
    if (paramList == null) {
      return;
    }
    ListIterator localListIterator1 = paramList.listIterator();
    label144:
    for (;;)
    {
      boolean bool1 = localListIterator1.hasNext();
      if (!bool1) {
        break;
      }
      f localf1 = (f)localListIterator1.next();
      int i1 = localListIterator1.previousIndex();
      int i2 = -1;
      if (i1 != i2)
      {
        i2 = 0;
        f localf2 = null;
        ListIterator localListIterator2 = paramList.listIterator();
        for (;;)
        {
          if (i2 != 0) {
            break label144;
          }
          boolean bool2 = localListIterator2.hasNext();
          if (!bool2) {
            break;
          }
          int i3 = localListIterator2.nextIndex();
          if (i3 == i1) {
            break;
          }
          localf2 = (f)localListIterator2.next();
          int i4 = localf2.mergeEquals(localf1);
          if (i4 != 0)
          {
            a(localf2, localf1);
            localListIterator1.remove();
          }
          i3 = i4;
        }
      }
    }
  }
  
  public final List A()
  {
    List localList = r;
    if (localList == null)
    {
      localList = Collections.unmodifiableList(n);
      r = localList;
    }
    return r;
  }
  
  public final String B()
  {
    List localList = b;
    boolean bool = localList.isEmpty();
    if (bool) {
      return "";
    }
    return ((Source)b.get(0)).c();
  }
  
  public final String C()
  {
    List localList = b;
    boolean bool = localList.isEmpty();
    if (bool) {
      return "";
    }
    return ((Source)b.get(0)).d();
  }
  
  public final String D()
  {
    List localList = b;
    boolean bool = localList.isEmpty();
    if (bool) {
      return "";
    }
    return ((Source)b.get(0)).b();
  }
  
  public final Long E()
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    long l1 = phonebookId;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool) {
      return null;
    }
    return Long.valueOf(mRow).phonebookId);
  }
  
  public final String F()
  {
    return mRow).phonebookLookupKey;
  }
  
  public final String G()
  {
    return mRow).searchQuery;
  }
  
  public final long H()
  {
    return mRow).searchTime;
  }
  
  public final int I()
  {
    Object localObject = l;
    int i1 = -1;
    int i2 = 10;
    if (localObject != null)
    {
      int i3 = ((Integer)localObject).intValue();
      if (i3 >= i2) {
        return l.intValue();
      }
      return i1;
    }
    localObject = n.iterator();
    Number localNumber;
    int i4;
    do
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      localNumber = (Number)((Iterator)localObject).next();
      i4 = localNumber.h();
    } while (i4 < i2);
    return localNumber.h();
    return i1;
  }
  
  public final List J()
  {
    List localList = s;
    if (localList == null)
    {
      localList = Collections.unmodifiableList(a);
      s = localList;
    }
    return s;
  }
  
  public final String K()
  {
    return mRow).transliteratedName;
  }
  
  public final boolean L()
  {
    return a(1);
  }
  
  public final boolean M()
  {
    return a(32);
  }
  
  public final boolean N()
  {
    boolean bool = a(64);
    if (bool)
    {
      b localb = TrueApp.y().a().aF().y();
      bool = localb.a();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean O()
  {
    List localList = n;
    int i1 = localList.size();
    return i1 > 0;
  }
  
  public final boolean P()
  {
    int i1 = getSource() & 0x4;
    if (i1 == 0)
    {
      String str = z();
      boolean bool = am.b(str);
      if (!bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean Q()
  {
    return ab.a(p());
  }
  
  public final boolean R()
  {
    String str1 = "private";
    String str2 = i();
    boolean bool = str1.equalsIgnoreCase(str2);
    if (bool)
    {
      bool = O();
      if (!bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean S()
  {
    int i1 = getSource() & 0xD;
    return i1 != 0;
  }
  
  public final boolean T()
  {
    int i1 = getSource() & 0x33;
    if (i1 == 0)
    {
      boolean bool = c(64);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean U()
  {
    Object localObject = l;
    boolean bool1 = true;
    int i1 = 10;
    if (localObject != null)
    {
      int i2 = ((Integer)localObject).intValue();
      if (i2 >= i1) {
        return bool1;
      }
      return false;
    }
    localObject = n.iterator();
    int i3;
    do
    {
      boolean bool2 = ((Iterator)localObject).hasNext();
      if (!bool2) {
        break;
      }
      Number localNumber = (Number)((Iterator)localObject).next();
      i3 = localNumber.h();
    } while (i3 < i1);
    return bool1;
    return false;
  }
  
  public final boolean V()
  {
    com.truecaller.common.g.a locala = TrueApp.y().a().I();
    int i1 = getSource() & 0x8;
    if (i1 != 0)
    {
      String str = "backup";
      boolean bool = locala.b(str);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean W()
  {
    return am.b(z());
  }
  
  public final boolean X()
  {
    return mRow).isFavorite;
  }
  
  public final boolean Y()
  {
    int i1 = getSource();
    int i2 = 32;
    i1 &= i2;
    return i1 == i2;
  }
  
  public final boolean Z()
  {
    Long localLong = E();
    return localLong != null;
  }
  
  public final Uri a(boolean paramBoolean)
  {
    long l1 = mRow).phonebookId;
    String str = mRow).image;
    return y.a(l1, str, paramBoolean);
  }
  
  public final String a()
  {
    Object localObject1 = g();
    String str1 = " ";
    if (localObject1 == null) {
      return null;
    }
    boolean bool = R();
    if (!bool)
    {
      Object localObject2 = ((Address)localObject1).getStreet();
      bool = am.a((CharSequence)localObject2);
      if (!bool)
      {
        localObject2 = ((Address)localObject1).getZipCode();
        bool = am.a((CharSequence)localObject2);
        if (!bool)
        {
          localObject2 = ((Address)localObject1).getCity();
          bool = am.a((CharSequence)localObject2);
          if (!bool) {
            break label143;
          }
        }
      }
      int i1 = 2;
      String[] arrayOfString = new String[i1];
      String str2 = ((Address)localObject1).getStreet();
      arrayOfString[0] = str2;
      localObject2 = new CharSequence[i1];
      str2 = ((Address)localObject1).getZipCode();
      localObject2[0] = str2;
      localObject1 = ((Address)localObject1).getCity();
      int i2 = 1;
      localObject2[i2] = localObject1;
      localObject1 = am.a(str1, (CharSequence[])localObject2);
      arrayOfString[i2] = localObject1;
      return am.a(arrayOfString);
    }
    label143:
    return ((Address)localObject1).getCity();
  }
  
  public final void a(long paramLong)
  {
    mRow).searchTime = paramLong;
  }
  
  public final void a(Address paramAddress)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    List localList1 = m;
    List localList2 = mRow).addresses;
    ContactDto.Row localRow = paramAddress.row();
    paramAddress = a(localList1, localList2, paramAddress, localRow);
    addresses = paramAddress;
  }
  
  public final void a(Link paramLink)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    List localList1 = o;
    List localList2 = mRow).internetAddresses;
    ContactDto.Row localRow = paramLink.row();
    paramLink = a(localList1, localList2, paramLink, localRow);
    internetAddresses = paramLink;
  }
  
  public final void a(Number paramNumber)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    List localList1 = n;
    List localList2 = mRow).phones;
    ContactDto.Row localRow = paramNumber.row();
    paramNumber = a(localList1, localList2, paramNumber, localRow);
    phones = paramNumber;
  }
  
  public final void a(Source paramSource)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    List localList1 = b;
    List localList2 = mRow).sources;
    ContactDto.Row localRow = paramSource.row();
    paramSource = a(localList1, localList2, paramSource, localRow);
    sources = paramSource;
  }
  
  public final void a(Tag paramTag)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    List localList1 = a;
    List localList2 = mRow).tags;
    ContactDto.Row localRow = paramTag.row();
    paramTag = a(localList1, localList2, paramTag, localRow);
    tags = paramTag;
  }
  
  public final void a(Integer paramInteger)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    int i1;
    if (paramInteger != null) {
      i1 = paramInteger.intValue();
    } else {
      i1 = -1;
    }
    favoritePosition = i1;
  }
  
  public final void a(Long paramLong)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    long l1;
    if (paramLong == null) {
      l1 = 0L;
    } else {
      l1 = paramLong.longValue();
    }
    aggregatedRowId = l1;
  }
  
  public final void a(String paramString)
  {
    mRow).about = paramString;
  }
  
  public final boolean a(int paramInt)
  {
    int i1 = f;
    paramInt &= i1;
    return paramInt != 0;
  }
  
  public final boolean aa()
  {
    int i1 = c(2);
    if (i1 == 0)
    {
      String str = z();
      i1 = am.b(str);
      if (i1 == 0)
      {
        i1 = 1;
        boolean bool = a(i1);
        if (!bool) {
          return i1;
        }
      }
    }
    return false;
  }
  
  public final void ab()
  {
    a.clear();
    mRow).tags = null;
  }
  
  public final boolean ac()
  {
    boolean bool = Z();
    if (!bool)
    {
      bool = a(2);
      if (!bool)
      {
        bool = R();
        if (!bool)
        {
          bool = Y();
          if (!bool) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  public final void ad()
  {
    List localList = m;
    Comparator localComparator = Address.PRESENTATION_COMPARATOR;
    Collections.sort(localList, localComparator);
    b(m);
    localList = n;
    localComparator = Number.a;
    Collections.sort(localList, localComparator);
    localList = n;
    a(localList);
    b(n);
    b(b);
    b(o);
    b(a);
  }
  
  public final void ae()
  {
    k = 2;
  }
  
  public final String b()
  {
    Address localAddress = g();
    if (localAddress == null) {
      return "";
    }
    return localAddress.getDisplayableAddress();
  }
  
  public final void b(int paramInt)
  {
    mRow).commonConnections = paramInt;
  }
  
  public final void b(Long paramLong)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    long l1;
    if (paramLong == null) {
      l1 = 0L;
    } else {
      l1 = paramLong.longValue();
    }
    phonebookHash = l1;
  }
  
  public final void b(String paramString)
  {
    mRow).access = paramString;
  }
  
  public final void b(boolean paramBoolean)
  {
    mRow).isFavorite = paramBoolean;
  }
  
  public final String c()
  {
    Address localAddress = g();
    if (localAddress == null) {
      return "";
    }
    return localAddress.getShortDisplayableAddress();
  }
  
  public final void c(Long paramLong)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    long l1;
    if (paramLong == null) {
      l1 = 0L;
    } else {
      l1 = paramLong.longValue();
    }
    phonebookId = l1;
  }
  
  public final void c(String paramString)
  {
    mRow).imId = paramString;
  }
  
  public final boolean c(int paramInt)
  {
    int i1 = getSource();
    paramInt &= i1;
    return paramInt != 0;
  }
  
  public final List d()
  {
    List localList = q;
    if (localList == null)
    {
      localList = Collections.unmodifiableList(m);
      q = localList;
    }
    return q;
  }
  
  public final void d(String paramString)
  {
    mRow).altName = paramString;
  }
  
  public final StructuredName e()
  {
    return g;
  }
  
  public final void e(String paramString)
  {
    mRow).cacheControl = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof Contact;
    if (!bool2) {
      return false;
    }
    paramObject = (Contact)paramObject;
    Object localObject1 = mRow).defaultNumber;
    Object localObject2 = mRow).defaultNumber;
    bool2 = am.a((CharSequence)localObject1, (CharSequence)localObject2);
    if (!bool2) {
      return false;
    }
    bool2 = O();
    boolean bool3 = ((Contact)paramObject).O();
    if (bool2 == bool3)
    {
      localObject1 = n;
      int i1 = ((List)localObject1).size();
      localObject2 = n;
      int i2 = ((List)localObject2).size();
      if (i1 == i2)
      {
        localObject1 = n.iterator();
        boolean bool4;
        do
        {
          bool4 = ((Iterator)localObject1).hasNext();
          if (!bool4) {
            break;
          }
          localObject2 = (Number)((Iterator)localObject1).next();
          Iterator localIterator = n.iterator();
          boolean bool5;
          do
          {
            bool5 = localIterator.hasNext();
            if (!bool5) {
              break;
            }
            Object localObject3 = (Number)localIterator.next();
            String str = ((Number)localObject2).a();
            localObject3 = ((Number)localObject3).a();
            bool5 = str.equals(localObject3);
          } while (!bool5);
          bool4 = true;
          continue;
          bool4 = false;
          localObject2 = null;
        } while (bool4);
        return false;
        localObject1 = z();
        paramObject = ((Contact)paramObject).z();
        int i3 = am.a((String)localObject1, (String)paramObject, bool1);
        if (i3 == 0) {
          return bool1;
        }
        return false;
      }
    }
    return false;
  }
  
  public final Style f()
  {
    return j;
  }
  
  public final void f(String paramString)
  {
    mRow).companyName = paramString;
  }
  
  public final Address g()
  {
    Iterator localIterator = m.iterator();
    Address localAddress = null;
    Long localLong;
    do
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localAddress = (Address)localIterator.next();
      localLong = localAddress.getDataPhonebookId();
    } while (localLong == null);
    return localAddress;
    return localAddress;
  }
  
  public final void g(String paramString)
  {
    mRow).defaultNumber = paramString;
  }
  
  public int getSource()
  {
    return mRow).source;
  }
  
  public String getTcId()
  {
    return mRow).id;
  }
  
  public final String h()
  {
    return mRow).about;
  }
  
  public final void h(String paramString)
  {
    mRow).gender = paramString;
  }
  
  public final String i()
  {
    return mRow).access;
  }
  
  public final void i(String paramString)
  {
    mRow).handle = paramString;
  }
  
  public final String j()
  {
    return mRow).imId;
  }
  
  public final void j(String paramString)
  {
    mRow).image = paramString;
  }
  
  public final Long k()
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    long l1 = aggregatedRowId;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool) {
      return null;
    }
    return Long.valueOf(mRow).aggregatedRowId);
  }
  
  public final void k(String paramString)
  {
    mRow).jobTitle = paramString;
  }
  
  public final String l()
  {
    return mRow).altName;
  }
  
  public final void l(String paramString)
  {
    mRow).name = paramString;
  }
  
  public final int m()
  {
    return f;
  }
  
  public final void m(String paramString)
  {
    mRow).phonebookLookupKey = paramString;
  }
  
  public final String n()
  {
    return mRow).cacheControl;
  }
  
  public final void n(String paramString)
  {
    mRow).searchQuery = paramString;
  }
  
  public final String o()
  {
    return mRow).companyName;
  }
  
  public final void o(String paramString)
  {
    mRow).transliteratedName = paramString;
  }
  
  public final String p()
  {
    Object localObject1 = mRow).defaultNumber;
    boolean bool1 = am.a((CharSequence)localObject1);
    if (bool1) {
      return mRow).defaultNumber;
    }
    localObject1 = n.iterator();
    boolean bool2;
    do
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      Object localObject2 = (Number)((Iterator)localObject1).next();
      ContactDto.Contact localContact = (ContactDto.Contact)mRow;
      int i1 = 3;
      String[] arrayOfString = new String[i1];
      String str = ((Number)localObject2).a();
      arrayOfString[0] = str;
      str = ((Number)localObject2).d();
      arrayOfString[1] = str;
      int i2 = 2;
      localObject2 = ((Number)localObject2).c();
      arrayOfString[i2] = localObject2;
      localObject2 = am.b(arrayOfString);
      defaultNumber = ((String)localObject2);
      localObject2 = mRow).defaultNumber;
      bool2 = am.b((CharSequence)localObject2);
    } while (bool2);
    return mRow).defaultNumber;
  }
  
  public final boolean p(String paramString)
  {
    boolean bool1 = S();
    if (bool1)
    {
      Object localObject = p;
      if (localObject != null)
      {
        bool1 = ab.e(paramString);
        if (bool1)
        {
          localObject = p.iterator();
          int i1;
          do
          {
            Number localNumber;
            boolean bool3;
            do
            {
              boolean bool2 = ((Iterator)localObject).hasNext();
              if (!bool2) {
                break;
              }
              localNumber = (Number)((Iterator)localObject).next();
              String str = localNumber.a();
              bool3 = paramString.equals(str);
            } while (!bool3);
            i1 = localNumber.getSource() & 0xD;
          } while (i1 == 0);
          return true;
        }
      }
    }
    return false;
  }
  
  public final String q()
  {
    Object localObject = r();
    if (localObject != null) {
      return ((Number)localObject).n();
    }
    localObject = n;
    boolean bool = ((List)localObject).isEmpty();
    if (!bool) {
      return ((Number)n.get(0)).n();
    }
    localObject = mRow).defaultNumber;
    bool = am.b((CharSequence)localObject);
    if (!bool) {
      return aa.e(mRow).defaultNumber);
    }
    return mRow).defaultNumber;
  }
  
  public final Number r()
  {
    String str1 = p();
    boolean bool1 = am.b(str1);
    if (!bool1)
    {
      Iterator localIterator = n.iterator();
      Number localNumber;
      boolean bool3;
      do
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        localNumber = (Number)localIterator.next();
        String str2 = localNumber.a();
        bool3 = str1.equals(str2);
      } while (!bool3);
      return localNumber;
    }
    return null;
  }
  
  public final String s()
  {
    String str = u();
    boolean bool = am.b(str);
    if (bool)
    {
      str = q();
      bool = am.b(str);
      if (bool) {
        str = g.a;
      }
    }
    return str;
  }
  
  public void setSource(int paramInt)
  {
    mRow).source = paramInt;
  }
  
  public void setTcId(String paramString)
  {
    super.setTcId(paramString);
    mRow).id = paramString;
  }
  
  public final String t()
  {
    String str = af();
    if (str != null) {
      return str;
    }
    return z();
  }
  
  public final String u()
  {
    String str = af();
    if (str != null) {
      return str;
    }
    str = z();
    boolean bool = Z();
    if (bool) {
      return str;
    }
    Object localObject = K();
    bool = am.b((CharSequence)localObject);
    if (!bool)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append(" (");
      str = K();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append(")");
      str = ((StringBuilder)localObject).toString();
    }
    else
    {
      localObject = l();
      bool = am.b((CharSequence)localObject);
      if (!bool)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(str);
        ((StringBuilder)localObject).append(" (");
        str = l();
        ((StringBuilder)localObject).append(str);
        ((StringBuilder)localObject).append(")");
        str = ((StringBuilder)localObject).toString();
      }
    }
    return str;
  }
  
  public final String v()
  {
    return mRow).image;
  }
  
  public final String w()
  {
    CharSequence[] arrayOfCharSequence = new CharSequence[2];
    String str = x();
    arrayOfCharSequence[0] = str;
    str = o();
    arrayOfCharSequence[1] = str;
    return am.a(" @ ", arrayOfCharSequence);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    Object localObject = m;
    paramParcel.writeTypedList((List)localObject);
    localObject = n;
    paramParcel.writeTypedList((List)localObject);
    localObject = a;
    paramParcel.writeTypedList((List)localObject);
    localObject = b;
    paramParcel.writeTypedList((List)localObject);
    localObject = o;
    paramParcel.writeTypedList((List)localObject);
    int i1 = f;
    paramParcel.writeInt(i1);
    localObject = l;
    paramParcel.writeValue(localObject);
    localObject = c;
    paramParcel.writeParcelable((Parcelable)localObject, 0);
    int i2 = d;
    paramParcel.writeByte(i2);
    localObject = g;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = h;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = i;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = j;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
  
  public final String x()
  {
    return mRow).jobTitle;
  }
  
  public final List y()
  {
    List localList = t;
    if (localList == null)
    {
      localList = Collections.unmodifiableList(o);
      t = localList;
    }
    return t;
  }
  
  public final String z()
  {
    return mRow).name;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Contact
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */