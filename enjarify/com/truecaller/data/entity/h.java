package com.truecaller.data.entity;

import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.google.c.a.k;
import com.google.common.base.Strings;
import com.truecaller.calling.ah;
import com.truecaller.calling.dialer.ax;
import com.truecaller.common.h.am;
import com.truecaller.utils.n;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

final class h
  implements g
{
  private static final long a = TimeUnit.MINUTES.toMillis(2);
  private final TelephonyManager b;
  private final n c;
  private final ax d;
  private String e;
  private volatile long f;
  private String g;
  private volatile long h;
  
  h(TelephonyManager paramTelephonyManager, n paramn, ax paramax)
  {
    b = paramTelephonyManager;
    c = paramn;
    d = paramax;
  }
  
  private String a()
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return null;
    }
    long l1 = SystemClock.elapsedRealtime();
    long l2 = f;
    String str = e;
    l2 = l1 - l2;
    long l3 = a;
    boolean bool1 = l2 < l3;
    if (bool1) {
      return str;
    }
    try
    {
      l2 = f;
      str = e;
      l1 -= l2;
      l2 = a;
      boolean bool2 = l1 < l2;
      if (bool2) {
        return str;
      }
      localObject1 = b;
      localObject1 = ((TelephonyManager)localObject1).getNetworkCountryIso();
      Locale localLocale = Locale.ENGLISH;
      localObject1 = am.a((String)localObject1, localLocale);
      e = ((String)localObject1);
      long l4 = SystemClock.elapsedRealtime();
      f = l4;
      return (String)localObject1;
    }
    finally {}
  }
  
  private String b()
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return null;
    }
    long l1 = SystemClock.elapsedRealtime();
    long l2 = h;
    String str = g;
    l2 = l1 - l2;
    long l3 = a;
    boolean bool1 = l2 < l3;
    if (bool1) {
      return str;
    }
    try
    {
      l2 = h;
      str = g;
      l1 -= l2;
      l2 = a;
      boolean bool2 = l1 < l2;
      if (bool2) {
        return str;
      }
      localObject1 = b;
      localObject1 = ((TelephonyManager)localObject1).getSimCountryIso();
      Locale localLocale = Locale.ENGLISH;
      localObject1 = am.a((String)localObject1, localLocale);
      g = ((String)localObject1);
      long l4 = SystemClock.elapsedRealtime();
      h = l4;
      return (String)localObject1;
    }
    finally {}
  }
  
  public final Number a(String... paramVarArgs)
  {
    String str1 = null;
    if (paramVarArgs == null) {
      return null;
    }
    Object localObject1 = k.a();
    int i = paramVarArgs.length;
    int j = 0;
    Object localObject2 = null;
    while (j < i)
    {
      String str2 = paramVarArgs[j];
      boolean bool2 = Strings.isNullOrEmpty(str2);
      if (!bool2)
      {
        if (localObject2 == null) {
          localObject2 = str2;
        }
        try
        {
          ((k)localObject1).a(str2, null);
          Number localNumber = new com/truecaller/data/entity/Number;
          localNumber.<init>(str2);
          localNumber.c(str2);
          return localNumber;
        }
        catch (com.google.c.a.g localg) {}
      }
      j += 1;
    }
    if (localObject2 != null)
    {
      if (localObject2 == null) {
        return null;
      }
      paramVarArgs = a();
      str1 = b();
      localObject1 = new com/truecaller/data/entity/Number;
      boolean bool1 = TextUtils.isEmpty(paramVarArgs);
      if (bool1) {
        paramVarArgs = str1;
      }
      ((Number)localObject1).<init>((String)localObject2, paramVarArgs);
      ((Number)localObject1).c((String)localObject2);
      return (Number)localObject1;
    }
    return null;
  }
  
  public final String a(Number paramNumber)
  {
    n localn = c;
    ax localax = d;
    return ah.a(paramNumber, localn, localax);
  }
  
  public final Number b(String... paramVarArgs)
  {
    Number[] arrayOfNumber = new Number[2];
    paramVarArgs = a(paramVarArgs);
    arrayOfNumber[0] = paramVarArgs;
    paramVarArgs = new com/truecaller/data/entity/Number;
    paramVarArgs.<init>();
    arrayOfNumber[1] = paramVarArgs;
    return (Number)org.c.a.a.a.h.a(arrayOfNumber);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */