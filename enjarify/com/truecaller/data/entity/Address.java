package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.search.ContactDto.Contact.Address;
import java.util.Comparator;
import org.c.a.a.a.b.a;

public class Address
  extends RowEntity
  implements f
{
  public static final Parcelable.Creator CREATOR;
  static final Comparator PRESENTATION_COMPARATOR;
  
  static
  {
    Object localObject = new com/truecaller/data/entity/Address$1;
    ((Address.1)localObject).<init>();
    PRESENTATION_COMPARATOR = (Comparator)localObject;
    localObject = new com/truecaller/data/entity/Address$2;
    ((Address.2)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public Address()
  {
    this(localAddress);
  }
  
  protected Address(Parcel paramParcel)
  {
    super(paramParcel);
  }
  
  Address(Address paramAddress)
  {
    this(localAddress);
  }
  
  Address(ContactDto.Contact.Address paramAddress)
  {
    super(paramAddress);
  }
  
  private String mergeStr(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return "";
    }
    return paramString.trim().toLowerCase();
  }
  
  public String getArea()
  {
    return mRow).area;
  }
  
  public String getCity()
  {
    return mRow).city;
  }
  
  public String getCityOrArea()
  {
    String str = getCity();
    boolean bool = TextUtils.isEmpty(str);
    if (bool) {
      return getArea();
    }
    return getCity();
  }
  
  public String getCountryCode()
  {
    return mRow).countryCode;
  }
  
  public String getCountryName()
  {
    CountryListDto.a locala = h.a(am.m(getCountryCode()));
    if (locala == null) {
      return "";
    }
    return am.a(b);
  }
  
  public String getDisplayableAddress()
  {
    boolean bool = isSplit();
    int i = 1;
    String str1 = null;
    int j = 2;
    if (bool)
    {
      arrayOfString = new String[3];
      String str2 = getStreet();
      arrayOfString[0] = str2;
      CharSequence[] arrayOfCharSequence = new CharSequence[j];
      String str3 = getZipCode();
      arrayOfCharSequence[0] = str3;
      str1 = getCityOrArea();
      arrayOfCharSequence[i] = str1;
      str1 = am.a(" ", arrayOfCharSequence);
      arrayOfString[i] = str1;
      String str4 = getCountryName();
      arrayOfString[j] = str4;
      return am.a(arrayOfString);
    }
    String[] arrayOfString = new String[j];
    String str5 = getStreet();
    arrayOfString[0] = str5;
    str1 = getCountryName();
    arrayOfString[i] = str1;
    return am.a(arrayOfString);
  }
  
  public String getShortDisplayableAddress()
  {
    String[] arrayOfString = new String[2];
    String str = getCityOrArea();
    arrayOfString[0] = str;
    str = getCountryName();
    arrayOfString[1] = str;
    return am.a(arrayOfString);
  }
  
  public String getStreet()
  {
    return mRow).street;
  }
  
  public String getTimeZone()
  {
    return mRow).timeZone;
  }
  
  public int getType()
  {
    return a.a(mRow).type);
  }
  
  public String getTypeLabel()
  {
    return null;
  }
  
  public String getZipCode()
  {
    return mRow).zipCode;
  }
  
  public boolean isSplit()
  {
    String str = getZipCode();
    boolean bool = TextUtils.isEmpty(str);
    if (bool)
    {
      str = getCity();
      bool = TextUtils.isEmpty(str);
      if (bool)
      {
        str = getArea();
        bool = TextUtils.isEmpty(str);
        if (bool) {
          return false;
        }
      }
    }
    return true;
  }
  
  public boolean mergeEquals(f paramf)
  {
    boolean bool1 = true;
    if (this == paramf) {
      return bool1;
    }
    boolean bool2 = paramf instanceof Address;
    if (!bool2) {
      return false;
    }
    paramf = (Address)paramf;
    String str1 = getCountryCode();
    String str2 = paramf.getCountryCode();
    bool2 = am.b(str1, str2);
    if (!bool2) {
      return false;
    }
    str1 = getCity();
    str1 = mergeStr(str1);
    str2 = getStreet();
    str2 = mergeStr(str2);
    String str3 = getZipCode();
    str3 = mergeStr(str3);
    String str4 = getArea();
    str4 = mergeStr(str4);
    String str5 = paramf.getCity();
    str5 = mergeStr(str5);
    String str6 = paramf.getStreet();
    str6 = mergeStr(str6);
    String str7 = paramf.getZipCode();
    str7 = mergeStr(str7);
    String str8 = paramf.getArea();
    str8 = mergeStr(str8);
    boolean bool3 = isSplit();
    if (bool3)
    {
      bool4 = paramf.isSplit();
      if (bool4)
      {
        bool4 = str1.equals(str5);
        if (bool4)
        {
          bool4 = str2.equals(str6);
          if (bool4)
          {
            bool4 = str3.equals(str7);
            if (bool4)
            {
              bool4 = str4.equals(str8);
              if (bool4) {
                return bool1;
              }
            }
          }
        }
        return false;
      }
      return str6.contains(str2);
    }
    boolean bool4 = paramf.isSplit();
    if (bool4) {
      return str2.contains(str6);
    }
    return str2.equals(str6);
  }
  
  public void setArea(String paramString)
  {
    mRow).area = paramString;
  }
  
  public void setCity(String paramString)
  {
    mRow).city = paramString;
  }
  
  public void setCountryCode(String paramString)
  {
    mRow).countryCode = paramString;
  }
  
  public void setStreet(String paramString)
  {
    mRow).street = paramString;
  }
  
  public void setTimeZone(String paramString)
  {
    mRow).timeZone = paramString;
  }
  
  public void setType(int paramInt)
  {
    ContactDto.Contact.Address localAddress = (ContactDto.Contact.Address)mRow;
    String str = String.valueOf(paramInt);
    type = str;
  }
  
  public void setTypeLabel(String paramString) {}
  
  public void setZipCode(String paramString)
  {
    mRow).zipCode = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Address
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */