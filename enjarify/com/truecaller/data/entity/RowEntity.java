package com.truecaller.data.entity;

import android.os.Parcel;
import com.truecaller.search.ContactDto.Row;

public abstract class RowEntity
  extends Entity
{
  public final ContactDto.Row mRow;
  
  protected RowEntity(Parcel paramParcel)
  {
    super(paramParcel);
    ClassLoader localClassLoader = ContactDto.Row.class.getClassLoader();
    paramParcel = (ContactDto.Row)paramParcel.readParcelable(localClassLoader);
    mRow = paramParcel;
  }
  
  RowEntity(ContactDto.Row paramRow)
  {
    mRow = paramRow;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Long getDataPhonebookId()
  {
    ContactDto.Row localRow = mRow;
    long l1 = phonebookId;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool) {
      return null;
    }
    return Long.valueOf(mRow.phonebookId);
  }
  
  public Long getId()
  {
    ContactDto.Row localRow = mRow;
    long l1 = rowId;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool) {
      return null;
    }
    return Long.valueOf(mRow.rowId);
  }
  
  public int getSource()
  {
    return mRow.source;
  }
  
  public String getTcId()
  {
    return mRow.tcId;
  }
  
  public boolean isPrimary()
  {
    return mRow.isPrimary;
  }
  
  final ContactDto.Row row()
  {
    return mRow;
  }
  
  public void setDataPhonebookId(Long paramLong)
  {
    ContactDto.Row localRow = mRow;
    long l;
    if (paramLong == null) {
      l = 0L;
    } else {
      l = paramLong.longValue();
    }
    phonebookId = l;
  }
  
  public void setId(Long paramLong)
  {
    ContactDto.Row localRow = mRow;
    long l;
    if (paramLong == null) {
      l = 0L;
    } else {
      l = paramLong.longValue();
    }
    rowId = l;
  }
  
  public void setIsPrimary(boolean paramBoolean)
  {
    mRow.isPrimary = paramBoolean;
  }
  
  public void setSource(int paramInt)
  {
    mRow.source = paramInt;
  }
  
  public void setTcId(String paramString)
  {
    mRow.tcId = paramString;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = getClass().getSimpleName();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append("{row=");
    localObject = mRow;
    localStringBuilder.append(localObject);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    ContactDto.Row localRow = mRow;
    paramParcel.writeParcelable(localRow, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.RowEntity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */