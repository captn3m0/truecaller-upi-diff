package com.truecaller.data;

import android.database.Cursor;
import android.database.CursorWrapper;
import c.g.b.k;
import com.truecaller.data.access.e;
import com.truecaller.data.entity.Contact;
import com.truecaller.util.z;

public final class a
  extends CursorWrapper
{
  private final e a;
  private final int b;
  
  public a(Cursor paramCursor)
  {
    super(paramCursor);
    e locale = new com/truecaller/data/access/e;
    locale.<init>(paramCursor);
    a = locale;
    int i = getColumnIndex("matched_value");
    b = i;
  }
  
  public final Contact a()
  {
    Object localObject1 = a;
    Object localObject2 = this;
    localObject2 = (Cursor)this;
    localObject1 = ((e)localObject1).a((Cursor)localObject2);
    boolean bool = z.a((Contact)localObject1);
    if (bool) {
      return (Contact)localObject1;
    }
    return null;
  }
  
  public final String b()
  {
    int i = b;
    String str = getString(i);
    k.a(str, "getString(matchedValueIndex)");
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */