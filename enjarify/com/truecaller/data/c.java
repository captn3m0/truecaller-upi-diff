package com.truecaller.data;

import android.database.Cursor;
import c.g.b.k;
import com.truecaller.data.entity.b;

public final class c
{
  private final int a;
  private final int b;
  private final int c;
  
  public c(Cursor paramCursor, String paramString)
  {
    int i = -1;
    String str2;
    int j;
    if (paramCursor != null)
    {
      str2 = "first_name";
      j = paramCursor.getColumnIndex(str2);
    }
    else
    {
      j = -1;
    }
    a = j;
    if (paramCursor != null)
    {
      str2 = "last_name";
      j = paramCursor.getColumnIndex(str2);
    }
    else
    {
      j = -1;
    }
    b = j;
    if (paramCursor != null) {
      i = paramCursor.getColumnIndex(paramString);
    }
    c = i;
  }
  
  public final b a(Cursor paramCursor)
  {
    k.b(paramCursor, "cursor");
    b localb = new com/truecaller/data/entity/b;
    int i = a;
    String str1 = paramCursor.getString(i);
    int j = b;
    String str2 = paramCursor.getString(j);
    int k = c;
    paramCursor = paramCursor.getString(k);
    localb.<init>(str1, str2, paramCursor);
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */