package com.truecaller.data.access;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Business;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Entity;
import com.truecaller.data.entity.Link;
import com.truecaller.data.entity.Note;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Source;
import com.truecaller.data.entity.StructuredName;
import com.truecaller.data.entity.Style;
import com.truecaller.data.entity.Tag;

public final class e
  extends a
{
  private final int A;
  private final int B;
  private final int C;
  private final int D;
  private final int E;
  private Uri F;
  private e.a G;
  private e.j H;
  private e.f I;
  private e.d J;
  private e.g K;
  private e.h L;
  private e.e M;
  private e.b N;
  private e.i O;
  private boolean P;
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  private final int p;
  private final int q;
  private final int r;
  private final int s;
  private final int t;
  private final int u;
  private final int v;
  private final int w;
  private final int x;
  private final int y;
  private final int z;
  
  public e(Cursor paramCursor)
  {
    String[] tmp8_5 = new String[3];
    String[] tmp9_8 = tmp8_5;
    String[] tmp9_8 = tmp8_5;
    tmp9_8[0] = "history_aggregated_contact_id";
    tmp9_8[1] = "history_raw_contact_id";
    tmp9_8[2] = "_id";
    Object localObject = tmp9_8;
    int i1 = a(paramCursor, (String[])localObject);
    a = i1;
    String str1 = "history_raw_contact_tc_id";
    String str2 = "tc_id";
    String[] tmp46_43 = new String[3];
    String[] tmp47_46 = tmp46_43;
    String[] tmp47_46 = tmp46_43;
    tmp47_46[0] = "history_aggregated_contact_tc_id";
    tmp47_46[1] = str1;
    tmp47_46[2] = str2;
    localObject = tmp47_46;
    i1 = a(paramCursor, (String[])localObject);
    c = i1;
    i1 = paramCursor.getColumnIndex("aggregated_contact_id");
    b = i1;
    i1 = paramCursor.getColumnIndex("contact_name");
    d = i1;
    i1 = paramCursor.getColumnIndex("contact_transliterated_name");
    g = i1;
    i1 = paramCursor.getColumnIndex("contact_is_favorite");
    e = i1;
    i1 = paramCursor.getColumnIndex("contact_favorite_position");
    f = i1;
    i1 = paramCursor.getColumnIndex("contact_handle");
    h = i1;
    i1 = paramCursor.getColumnIndex("contact_alt_name");
    i = i1;
    i1 = paramCursor.getColumnIndex("contact_gender");
    j = i1;
    i1 = paramCursor.getColumnIndex("contact_about");
    k = i1;
    i1 = paramCursor.getColumnIndex("contact_image_url");
    l = i1;
    i1 = paramCursor.getColumnIndex("contact_job_title");
    m = i1;
    i1 = paramCursor.getColumnIndex("contact_company");
    n = i1;
    i1 = paramCursor.getColumnIndex("contact_access");
    o = i1;
    i1 = paramCursor.getColumnIndex("contact_common_connections");
    p = i1;
    i1 = paramCursor.getColumnIndex("contact_search_time");
    q = i1;
    i1 = paramCursor.getColumnIndex("contact_source");
    r = i1;
    i1 = paramCursor.getColumnIndex("contact_default_number");
    s = i1;
    i1 = paramCursor.getColumnIndex("contact_phonebook_id");
    t = i1;
    i1 = paramCursor.getColumnIndex("contact_phonebook_hash");
    u = i1;
    i1 = paramCursor.getColumnIndex("contact_phonebook_lookup");
    v = i1;
    i1 = paramCursor.getColumnIndex("contact_badges");
    w = i1;
    i1 = paramCursor.getColumnIndex("search_query");
    y = i1;
    i1 = paramCursor.getColumnIndex("cache_control");
    z = i1;
    i1 = paramCursor.getColumnIndex("contact_spam_score");
    A = i1;
    i1 = paramCursor.getColumnIndex("tc_flag");
    B = i1;
    i1 = paramCursor.getColumnIndex("data_raw_contact_id");
    C = i1;
    i1 = paramCursor.getColumnIndex("insert_timestamp");
    D = i1;
    i1 = paramCursor.getColumnIndex("contact_im_id");
    E = i1;
    i1 = paramCursor.getColumnIndex("data_type");
    x = i1;
    localObject = "history_aggregated_contact_id";
    i1 = paramCursor.getColumnIndex((String)localObject);
    int i2 = a;
    if (i1 != i2)
    {
      i1 = b;
      i2 = -1;
      if (i1 != i2)
      {
        localObject = "aggregated_raw_contact_id";
        i3 = paramCursor.getColumnIndex((String)localObject);
        if (i3 == i2)
        {
          i3 = 0;
          paramCursor = null;
          break label548;
        }
      }
    }
    int i3 = 1;
    label548:
    a(i3);
  }
  
  public final Contact a(Cursor paramCursor)
  {
    int i1 = a;
    int i2 = -1;
    if (i1 != i2)
    {
      boolean bool1 = paramCursor.isNull(i1);
      if (!bool1)
      {
        Contact localContact = new com/truecaller/data/entity/Contact;
        localContact.<init>();
        int i5 = a;
        long l1 = paramCursor.getLong(i5);
        Object localObject1 = Long.valueOf(l1);
        localContact.setId((Long)localObject1);
        int i7 = b;
        if (i7 != i2)
        {
          boolean bool5 = P;
          if (!bool5)
          {
            localObject1 = b(paramCursor, i7);
            localContact.a((Long)localObject1);
          }
        }
        localObject1 = F;
        Object localObject2 = ContentUris.withAppendedId((Uri)localObject1, l1);
        c = ((Uri)localObject2);
        i5 = c;
        localObject2 = paramCursor.getString(i5);
        localContact.setTcId((String)localObject2);
        i5 = d;
        localObject2 = d(paramCursor, i5);
        localContact.l((String)localObject2);
        i5 = g;
        localObject2 = d(paramCursor, i5);
        localContact.o((String)localObject2);
        i5 = e;
        i5 = c(paramCursor, i5);
        int i8 = 0;
        i7 = 1;
        if (i5 != i7)
        {
          i7 = 0;
          localObject1 = null;
        }
        localContact.b(i7);
        i5 = f;
        localObject2 = a(paramCursor, i5);
        localContact.a((Integer)localObject2);
        i5 = h;
        localObject2 = d(paramCursor, i5);
        localContact.i((String)localObject2);
        i5 = i;
        localObject2 = d(paramCursor, i5);
        localContact.d((String)localObject2);
        i5 = j;
        localObject2 = d(paramCursor, i5);
        localContact.h((String)localObject2);
        i5 = k;
        localObject2 = d(paramCursor, i5);
        localContact.a((String)localObject2);
        i5 = l;
        localObject2 = d(paramCursor, i5);
        localContact.j((String)localObject2);
        i5 = m;
        localObject2 = d(paramCursor, i5);
        localContact.k((String)localObject2);
        i5 = n;
        localObject2 = d(paramCursor, i5);
        localContact.f((String)localObject2);
        i5 = o;
        localObject2 = d(paramCursor, i5);
        localContact.b((String)localObject2);
        i5 = p;
        i5 = c(paramCursor, i5);
        localContact.b(i5);
        i5 = q;
        if (i5 != i2)
        {
          boolean bool4 = paramCursor.isNull(i5);
          if (!bool4)
          {
            i6 = q;
            l2 = paramCursor.getLong(i6);
            break label497;
          }
        }
        long l2 = 0L;
        label497:
        localContact.a(l2);
        int i6 = r;
        i6 = c(paramCursor, i6);
        localContact.setSource(i6);
        i6 = s;
        localObject2 = d(paramCursor, i6);
        localContact.g((String)localObject2);
        i6 = t;
        localObject2 = b(paramCursor, i6);
        localContact.c((Long)localObject2);
        i6 = u;
        localObject2 = b(paramCursor, i6);
        localContact.b((Long)localObject2);
        i6 = v;
        localObject2 = d(paramCursor, i6);
        localContact.m((String)localObject2);
        i6 = w;
        if (i6 != i2)
        {
          boolean bool2 = paramCursor.isNull(i6);
          if (!bool2)
          {
            i3 = w;
            i8 = paramCursor.getInt(i3);
          }
        }
        f = i8;
        int i3 = y;
        Object localObject3 = d(paramCursor, i3);
        localContact.n((String)localObject3);
        i3 = z;
        localObject3 = d(paramCursor, i3);
        localContact.e((String)localObject3);
        boolean bool3 = P;
        d = bool3;
        int i4 = A;
        localObject3 = a(paramCursor, i4);
        l = ((Integer)localObject3);
        i4 = B;
        i4 = c(paramCursor, i4);
        k = i4;
        i4 = E;
        paramCursor = d(paramCursor, i4);
        localContact.c(paramCursor);
        return localContact;
      }
    }
    return null;
  }
  
  public final Entity a(Cursor paramCursor, Contact paramContact)
  {
    int i1 = x;
    int i3 = -1;
    if (i1 != i3)
    {
      boolean bool = paramCursor.isNull(i1);
      if (!bool)
      {
        int i2 = x;
        i2 = c(paramCursor, i2);
        i3 = 1;
        if (i2 != i3)
        {
          switch (i2)
          {
          default: 
            paramCursor = new String[i3];
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>("Encountered an unknown data type, ");
            localStringBuilder.append(i2);
            localStringBuilder.append(", contact=");
            localStringBuilder.append(paramContact);
            paramContact = localStringBuilder.toString();
            paramCursor[0] = paramContact;
            return null;
          case 10: 
            localObject = O;
            if (localObject == null)
            {
              localObject = new com/truecaller/data/access/e$i;
              ((e.i)localObject).<init>(paramCursor);
              O = ((e.i)localObject);
            }
            paramCursor = (Style)O.b(paramCursor);
            j = paramCursor;
            return paramCursor;
          case 9: 
            localObject = N;
            if (localObject == null)
            {
              localObject = new com/truecaller/data/access/e$b;
              ((e.b)localObject).<init>(paramCursor);
              N = ((e.b)localObject);
            }
            paramCursor = (Business)N.b(paramCursor);
            i = paramCursor;
            return paramCursor;
          case 8: 
            localObject = M;
            if (localObject == null)
            {
              localObject = new com/truecaller/data/access/e$e;
              ((e.e)localObject).<init>(paramCursor);
              M = ((e.e)localObject);
            }
            paramCursor = (Note)M.b(paramCursor);
            h = paramCursor;
            return paramCursor;
          case 7: 
            localObject = L;
            if (localObject == null)
            {
              localObject = new com/truecaller/data/access/e$h;
              ((e.h)localObject).<init>(paramCursor);
              L = ((e.h)localObject);
            }
            paramCursor = (StructuredName)L.b(paramCursor);
            g = paramCursor;
            return paramCursor;
          case 6: 
            localObject = H;
            if (localObject == null)
            {
              localObject = new com/truecaller/data/access/e$j;
              ((e.j)localObject).<init>(paramCursor);
              H = ((e.j)localObject);
            }
            localObject = H;
            paramCursor = (Tag)((e.j)localObject).b(paramCursor);
            if (paramCursor != null) {
              paramContact.a(paramCursor);
            }
            return paramCursor;
          case 5: 
            localObject = K;
            if (localObject == null)
            {
              localObject = new com/truecaller/data/access/e$g;
              ((e.g)localObject).<init>(paramCursor);
              K = ((e.g)localObject);
            }
            localObject = K;
            paramCursor = (Source)((e.g)localObject).b(paramCursor);
            if (paramCursor != null) {
              paramContact.a(paramCursor);
            }
            return paramCursor;
          case 4: 
            localObject = I;
            if (localObject == null)
            {
              localObject = new com/truecaller/data/access/e$f;
              ((e.f)localObject).<init>(paramCursor);
              I = ((e.f)localObject);
            }
            localObject = I;
            paramCursor = (Number)((e.f)localObject).b(paramCursor);
            if (paramCursor != null)
            {
              paramContact.a(paramCursor);
              localObject = paramContact.p();
              if (localObject == null)
              {
                localObject = paramCursor.a();
                paramContact.g((String)localObject);
              }
            }
            return paramCursor;
          }
          localObject = J;
          if (localObject == null)
          {
            localObject = new com/truecaller/data/access/e$d;
            ((e.d)localObject).<init>(paramCursor);
            J = ((e.d)localObject);
          }
          localObject = J;
          paramCursor = (Link)((e.d)localObject).b(paramCursor);
          if (paramCursor != null) {
            paramContact.a(paramCursor);
          }
          return paramCursor;
        }
        Object localObject = G;
        if (localObject == null)
        {
          localObject = new com/truecaller/data/access/e$a;
          ((e.a)localObject).<init>(paramCursor);
          G = ((e.a)localObject);
        }
        localObject = G;
        paramCursor = (Address)((e.a)localObject).b(paramCursor);
        if (paramCursor != null) {
          paramContact.a(paramCursor);
        }
        return paramCursor;
      }
    }
    return null;
  }
  
  public final void a(boolean paramBoolean)
  {
    P = paramBoolean;
    paramBoolean = x;
    boolean bool = true;
    Uri localUri;
    if (paramBoolean == bool)
    {
      paramBoolean = P;
      if (paramBoolean) {
        localUri = TruecallerContract.a.a();
      } else {
        localUri = TruecallerContract.ah.a();
      }
      F = localUri;
      return;
    }
    paramBoolean = P;
    if (paramBoolean) {
      localUri = TruecallerContract.a.b();
    } else {
      localUri = TruecallerContract.ah.b();
    }
    F = localUri;
  }
  
  public final Entity b(Cursor paramCursor)
  {
    int i1 = x;
    int i2 = -1;
    if (i1 == i2) {
      return null;
    }
    i1 = c(paramCursor, i1);
    i2 = 1;
    if (i1 != i2)
    {
      switch (i1)
      {
      default: 
        paramCursor = new String[i2];
        localObject = String.valueOf(i1);
        localObject = "Encountered an unknown data type, ".concat((String)localObject);
        paramCursor[0] = localObject;
        return null;
      case 10: 
        localObject = O;
        if (localObject == null)
        {
          localObject = new com/truecaller/data/access/e$i;
          ((e.i)localObject).<init>(paramCursor);
          O = ((e.i)localObject);
        }
        return O.b(paramCursor);
      case 9: 
        localObject = N;
        if (localObject == null)
        {
          localObject = new com/truecaller/data/access/e$b;
          ((e.b)localObject).<init>(paramCursor);
          N = ((e.b)localObject);
        }
        return N.b(paramCursor);
      case 8: 
        localObject = M;
        if (localObject == null)
        {
          localObject = new com/truecaller/data/access/e$e;
          ((e.e)localObject).<init>(paramCursor);
          M = ((e.e)localObject);
        }
        return M.b(paramCursor);
      case 7: 
        localObject = L;
        if (localObject == null)
        {
          localObject = new com/truecaller/data/access/e$h;
          ((e.h)localObject).<init>(paramCursor);
          L = ((e.h)localObject);
        }
        return L.b(paramCursor);
      case 6: 
        localObject = H;
        if (localObject == null)
        {
          localObject = new com/truecaller/data/access/e$j;
          ((e.j)localObject).<init>(paramCursor);
          H = ((e.j)localObject);
        }
        return H.b(paramCursor);
      case 5: 
        localObject = K;
        if (localObject == null)
        {
          localObject = new com/truecaller/data/access/e$g;
          ((e.g)localObject).<init>(paramCursor);
          K = ((e.g)localObject);
        }
        return K.b(paramCursor);
      case 4: 
        localObject = I;
        if (localObject == null)
        {
          localObject = new com/truecaller/data/access/e$f;
          ((e.f)localObject).<init>(paramCursor);
          I = ((e.f)localObject);
        }
        return I.b(paramCursor);
      }
      localObject = J;
      if (localObject == null)
      {
        localObject = new com/truecaller/data/access/e$d;
        ((e.d)localObject).<init>(paramCursor);
        J = ((e.d)localObject);
      }
      return J.b(paramCursor);
    }
    Object localObject = G;
    if (localObject == null)
    {
      localObject = new com/truecaller/data/access/e$a;
      ((e.a)localObject).<init>(paramCursor);
      G = ((e.a)localObject);
    }
    return G.b(paramCursor);
  }
  
  final long c(Cursor paramCursor)
  {
    int i1 = a;
    return paramCursor.getLong(i1);
  }
  
  public final Long d(Cursor paramCursor)
  {
    int i1 = C;
    return b(paramCursor, i1);
  }
  
  public final Long e(Cursor paramCursor)
  {
    int i1 = D;
    return b(paramCursor, i1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */