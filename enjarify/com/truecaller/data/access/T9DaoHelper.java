package com.truecaller.data.access;

import android.content.ContentValues;
import c.a.y;
import c.g.b.k;
import c.l;
import c.s;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Entity;
import com.truecaller.data.entity.Link;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public final class T9DaoHelper
{
  q a;
  
  private static ContentValues a(ContentValues paramContentValues, Long paramLong)
  {
    paramContentValues.put("data_id", paramLong);
    return paramContentValues;
  }
  
  private final List a(String paramString, int paramInt, T9DaoHelper.MappingRule paramMappingRule, boolean paramBoolean)
  {
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    int i = 0;
    Integer localInteger = null;
    int j = 1;
    if (localObject1 != null)
    {
      bool2 = c.n.m.a((CharSequence)localObject1);
      if (!bool2)
      {
        bool2 = false;
        localObject1 = null;
        break label50;
      }
    }
    boolean bool2 = true;
    label50:
    int k = 0;
    if (bool2) {
      paramString = null;
    }
    if (paramString != null)
    {
      localObject1 = a;
      if (localObject1 == null)
      {
        localObject2 = "t9Mapper";
        k.a((String)localObject2);
      }
      Object localObject2 = T9DaoHelper.MappingRule.MATCH_ANY_WORD;
      boolean bool3;
      if (paramMappingRule == localObject2)
      {
        bool3 = true;
      }
      else
      {
        bool3 = false;
        localObject2 = null;
      }
      Object localObject3 = ((q)localObject1).a(paramString, bool3, paramBoolean);
      localObject1 = localObject3;
      localObject1 = (Collection)localObject3;
      bool2 = ((Collection)localObject1).isEmpty() ^ j;
      if (!bool2)
      {
        paramBoolean = false;
        localObject3 = null;
      }
      if (localObject3 == null) {
        localObject3 = c.a.m.a(null);
      }
      localObject3 = (Iterable)localObject3;
      localObject1 = new java/util/ArrayList;
      j = c.a.m.a((Iterable)localObject3, 10);
      ((ArrayList)localObject1).<init>(j);
      localObject1 = (Collection)localObject1;
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject3).hasNext();
        if (!bool1) {
          break;
        }
        Object localObject4 = ((Iterator)localObject3).next();
        k = i + 1;
        if (i < 0) {
          c.a.m.a();
        }
        localObject4 = (String)localObject4;
        localObject2 = new android/content/ContentValues;
        ((ContentValues)localObject2).<init>();
        Object localObject5 = o.a;
        int m = paramMappingRule.ordinal();
        int n = localObject5[m];
        switch (n)
        {
        default: 
          paramString = new c/l;
          paramString.<init>();
          throw paramString;
        case 3: 
          localObject5 = "t9_anywhere";
          break;
        case 1: 
        case 2: 
          localObject5 = "t9_starts_with";
        }
        ((ContentValues)localObject2).put((String)localObject5, (String)localObject4);
        ((ContentValues)localObject2).put("matched_value", paramString);
        localObject4 = "hit_priority";
        localInteger = Integer.valueOf(paramInt - i);
        ((ContentValues)localObject2).put((String)localObject4, localInteger);
        ((Collection)localObject1).add(localObject2);
        i = k;
      }
      return (List)localObject1;
    }
    return (List)y.a;
  }
  
  public final List a(s params)
  {
    k.b(params, "entityWithIdAndTimeStamp");
    Object localObject1 = (Number)a;
    long l = ((Number)localObject1).longValue();
    Object localObject2 = (Entity)b;
    params = (Long)c;
    boolean bool1 = localObject2 instanceof Address;
    int j = 10;
    int k = 6;
    int m = 0;
    Object localObject3 = null;
    int i2;
    Object localObject5;
    int i3;
    Object localObject6;
    if (bool1)
    {
      localObject2 = (Address)localObject2;
      localObject4 = ((Address)localObject2).getId();
      if (localObject4 != null)
      {
        localObject4 = ((Address)localObject2).getCountryName();
        i2 = 690;
        localObject4 = (Collection)a(this, (String)localObject4, i2, null, k);
        localObject5 = ((Address)localObject2).getCity();
        i3 = 700;
        localObject6 = (Iterable)a(this, (String)localObject5, i3, null, k);
        localObject4 = (Iterable)c.a.m.c((Collection)localObject4, (Iterable)localObject6);
        localObject6 = new java/util/ArrayList;
        m = c.a.m.a((Iterable)localObject4, j);
        ((ArrayList)localObject6).<init>(m);
        localObject6 = (Collection)localObject6;
        localObject4 = ((Iterable)localObject4).iterator();
        for (;;)
        {
          boolean bool4 = ((Iterator)localObject4).hasNext();
          if (!bool4) {
            break;
          }
          localObject3 = (ContentValues)((Iterator)localObject4).next();
          localObject5 = ((Address)localObject2).getId();
          localObject3 = a((ContentValues)localObject3, (Long)localObject5);
          ((Collection)localObject6).add(localObject3);
        }
        localObject3 = localObject6;
        localObject3 = (List)localObject6;
      }
    }
    else
    {
      bool1 = localObject2 instanceof Link;
      if (bool1)
      {
        localObject2 = (Link)localObject2;
        localObject4 = ((Link)localObject2).getId();
        if (localObject4 != null)
        {
          localObject4 = ((Link)localObject2).getInfo();
          i2 = 200;
          localObject4 = a(this, (String)localObject4, i2, null, k);
          if (localObject4 != null)
          {
            localObject4 = (Iterable)localObject4;
            localObject6 = new java/util/ArrayList;
            int n = c.a.m.a((Iterable)localObject4, j);
            ((ArrayList)localObject6).<init>(n);
            localObject6 = (Collection)localObject6;
            localObject4 = ((Iterable)localObject4).iterator();
            for (;;)
            {
              boolean bool5 = ((Iterator)localObject4).hasNext();
              if (!bool5) {
                break;
              }
              localObject3 = (ContentValues)((Iterator)localObject4).next();
              localObject5 = ((Link)localObject2).getId();
              localObject3 = a((ContentValues)localObject3, (Long)localObject5);
              ((Collection)localObject6).add(localObject3);
            }
            localObject3 = localObject6;
            localObject3 = (List)localObject6;
          }
        }
      }
      else
      {
        bool1 = localObject2 instanceof com.truecaller.data.entity.Number;
        i2 = 1;
        if (bool1)
        {
          localObject2 = (com.truecaller.data.entity.Number)localObject2;
          localObject4 = ((com.truecaller.data.entity.Number)localObject2).getId();
          if (localObject4 != null)
          {
            localObject4 = ((com.truecaller.data.entity.Number)localObject2).a();
            localObject6 = T9DaoHelper.MappingRule.MATCH_IF_STARTS_WITH;
            int i1 = 900;
            localObject4 = (Collection)a((String)localObject4, i1, (T9DaoHelper.MappingRule)localObject6, i2);
            localObject6 = ((com.truecaller.data.entity.Number)localObject2).c();
            localObject5 = T9DaoHelper.MappingRule.MATCH_IF_STARTS_WITH;
            i3 = 4;
            localObject6 = (Iterable)a(this, (String)localObject6, i1, (T9DaoHelper.MappingRule)localObject5, i3);
            localObject4 = (Collection)c.a.m.c((Collection)localObject4, (Iterable)localObject6);
            localObject6 = ((com.truecaller.data.entity.Number)localObject2).c();
            localObject5 = T9DaoHelper.MappingRule.MATCH_ANYWHERE;
            localObject6 = (Iterable)a(this, (String)localObject6, 890, (T9DaoHelper.MappingRule)localObject5, i3);
            localObject4 = (Collection)c.a.m.c((Collection)localObject4, (Iterable)localObject6);
            localObject6 = ((com.truecaller.data.entity.Number)localObject2).a();
            localObject5 = T9DaoHelper.MappingRule.MATCH_ANYWHERE;
            localObject6 = (Iterable)a(this, (String)localObject6, 880, (T9DaoHelper.MappingRule)localObject5, i3);
            localObject4 = (Iterable)c.a.m.c((Collection)localObject4, (Iterable)localObject6);
            localObject6 = new java/util/ArrayList;
            i1 = c.a.m.a((Iterable)localObject4, j);
            ((ArrayList)localObject6).<init>(i1);
            localObject6 = (Collection)localObject6;
            localObject4 = ((Iterable)localObject4).iterator();
            for (;;)
            {
              boolean bool6 = ((Iterator)localObject4).hasNext();
              if (!bool6) {
                break;
              }
              localObject3 = (ContentValues)((Iterator)localObject4).next();
              localObject5 = ((com.truecaller.data.entity.Number)localObject2).getId();
              localObject3 = a((ContentValues)localObject3, (Long)localObject5);
              ((Collection)localObject6).add(localObject3);
            }
            localObject3 = localObject6;
            localObject3 = (List)localObject6;
          }
        }
        else
        {
          bool1 = localObject2 instanceof Contact;
          if (bool1)
          {
            localObject2 = (Contact)localObject2;
            localObject4 = ((Contact)localObject2).z();
            localObject4 = (Collection)a(this, (String)localObject4, 1000, null, k);
            Object localObject7 = ((Contact)localObject2).l();
            localObject7 = (Iterable)a(this, (String)localObject7, 950, null, k);
            localObject4 = (Collection)c.a.m.c((Collection)localObject4, (Iterable)localObject7);
            localObject7 = ((Contact)localObject2).o();
            int i4 = 800;
            localObject7 = (Iterable)a(this, (String)localObject7, i4, null, k);
            localObject4 = (Collection)c.a.m.c((Collection)localObject4, (Iterable)localObject7);
            localObject2 = ((Contact)localObject2).x();
            i3 = 790;
            localObject2 = (Iterable)a(this, (String)localObject2, i3, null, k);
            localObject2 = c.a.m.c((Collection)localObject4, (Iterable)localObject2);
            localObject4 = localObject2;
            localObject4 = (Collection)localObject2;
            bool1 = ((Collection)localObject4).isEmpty() ^ i2;
            if (bool1) {
              localObject3 = localObject2;
            }
            if (localObject3 == null)
            {
              localObject2 = new android/content/ContentValues;
              ((ContentValues)localObject2).<init>();
              localObject3 = c.a.m.a(localObject2);
            }
          }
        }
      }
    }
    if (localObject3 == null)
    {
      localObject2 = y.a;
      localObject3 = localObject2;
      localObject3 = (List)localObject2;
    }
    localObject3 = (Iterable)localObject3;
    localObject2 = new java/util/ArrayList;
    int i = c.a.m.a((Iterable)localObject3, j);
    ((ArrayList)localObject2).<init>(i);
    localObject2 = (Collection)localObject2;
    Object localObject4 = ((Iterable)localObject3).iterator();
    ContentValues localContentValues;
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject4).hasNext();
      if (!bool3) {
        break;
      }
      localContentValues = (ContentValues)((Iterator)localObject4).next();
      localObject3 = Long.valueOf(l);
      localContentValues.put("raw_contact_id", (Long)localObject3);
      localObject6 = "raw_contact_insert_timestamp";
      localContentValues.put((String)localObject6, params);
      ((Collection)localObject2).add(localContentValues);
    }
    localObject2 = (Iterable)localObject2;
    params = new java/util/HashSet;
    params.<init>();
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Iterator localIterator = ((Iterable)localObject2).iterator();
    for (;;)
    {
      boolean bool7 = localIterator.hasNext();
      if (!bool7) {
        break;
      }
      localObject2 = localIterator.next();
      localObject4 = localObject2;
      localObject4 = (ContentValues)localObject2;
      localContentValues = new android/content/ContentValues;
      localContentValues.<init>((ContentValues)localObject4);
      localObject4 = "hit_priority";
      localContentValues.remove((String)localObject4);
      boolean bool2 = params.add(localContentValues);
      if (bool2) {
        ((ArrayList)localObject1).add(localObject2);
      }
    }
    return (List)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.T9DaoHelper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */