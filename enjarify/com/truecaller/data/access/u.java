package com.truecaller.data.access;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.content.TruecallerContract.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Tag;
import com.truecaller.log.d;
import java.util.ArrayList;

public final class u
  extends h
{
  private final m c;
  
  public u(Context paramContext)
  {
    super(paramContext);
    m localm = new com/truecaller/data/access/m;
    localm.<init>(paramContext);
    c = localm;
  }
  
  /* Error */
  public final boolean a(Contact paramContact)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 21	com/truecaller/data/access/c:b	(Lcom/truecaller/data/entity/Contact;)Z
    //   4: istore_2
    //   5: iload_2
    //   6: ifeq +298 -> 304
    //   9: aload_0
    //   10: getfield 25	com/truecaller/data/access/u:a	Landroid/content/Context;
    //   13: astore_3
    //   14: aload_3
    //   15: invokevirtual 31	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   18: astore_3
    //   19: invokestatic 36	com/truecaller/content/TruecallerContract$ah:a	()Landroid/net/Uri;
    //   22: astore 4
    //   24: ldc 38
    //   26: astore 5
    //   28: iconst_1
    //   29: anewarray 40	java/lang/String
    //   32: dup
    //   33: iconst_0
    //   34: aload 5
    //   36: aastore
    //   37: astore 6
    //   39: ldc 42
    //   41: astore 7
    //   43: iconst_1
    //   44: istore 8
    //   46: iload 8
    //   48: anewarray 40	java/lang/String
    //   51: astore 9
    //   53: aload_1
    //   54: invokevirtual 49	com/truecaller/data/entity/Contact:getId	()Ljava/lang/Long;
    //   57: astore 5
    //   59: aload 5
    //   61: invokestatic 53	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   64: astore 5
    //   66: aload 9
    //   68: iconst_0
    //   69: aload 5
    //   71: aastore
    //   72: aconst_null
    //   73: astore 10
    //   75: aload_3
    //   76: astore 5
    //   78: aload_3
    //   79: aload 4
    //   81: aload 6
    //   83: aload 7
    //   85: aload 9
    //   87: aconst_null
    //   88: invokevirtual 59	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   91: astore 5
    //   93: aload 5
    //   95: ifnull +209 -> 304
    //   98: new 61	java/util/ArrayList
    //   101: astore 4
    //   103: aload 4
    //   105: invokespecial 64	java/util/ArrayList:<init>	()V
    //   108: aload 5
    //   110: invokeinterface 70 1 0
    //   115: istore 11
    //   117: iload 11
    //   119: ifeq +71 -> 190
    //   122: invokestatic 73	com/truecaller/content/TruecallerContract$k:a	()Landroid/net/Uri;
    //   125: astore 6
    //   127: aload 6
    //   129: invokestatic 79	android/content/ContentProviderOperation:newDelete	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   132: astore 6
    //   134: ldc 81
    //   136: astore 7
    //   138: iload 8
    //   140: anewarray 40	java/lang/String
    //   143: astore 9
    //   145: aload 5
    //   147: iconst_0
    //   148: invokeinterface 85 2 0
    //   153: astore 10
    //   155: aload 9
    //   157: iconst_0
    //   158: aload 10
    //   160: aastore
    //   161: aload 6
    //   163: aload 7
    //   165: aload 9
    //   167: invokevirtual 91	android/content/ContentProviderOperation$Builder:withSelection	(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;
    //   170: astore 6
    //   172: aload 6
    //   174: invokevirtual 95	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   177: astore 6
    //   179: aload 4
    //   181: aload 6
    //   183: invokevirtual 99	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   186: pop
    //   187: goto -79 -> 108
    //   190: aload 5
    //   192: invokeinterface 102 1 0
    //   197: aload 4
    //   199: invokevirtual 105	java/util/ArrayList:isEmpty	()Z
    //   202: istore 12
    //   204: iload 12
    //   206: ifne +98 -> 304
    //   209: getstatic 110	com/truecaller/content/TruecallerContract:a	Ljava/lang/String;
    //   212: astore 5
    //   214: aload_3
    //   215: aload 5
    //   217: aload 4
    //   219: invokevirtual 114	android/content/ContentResolver:applyBatch	(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   222: astore_3
    //   223: aload_3
    //   224: arraylength
    //   225: istore 12
    //   227: iconst_0
    //   228: istore 13
    //   230: aconst_null
    //   231: astore 4
    //   233: iload 13
    //   235: iload 12
    //   237: if_icmpge +67 -> 304
    //   240: aload_3
    //   241: iload 13
    //   243: aaload
    //   244: astore 6
    //   246: aload 6
    //   248: getfield 120	android/content/ContentProviderResult:count	Ljava/lang/Integer;
    //   251: astore 6
    //   253: aload 6
    //   255: invokevirtual 126	java/lang/Integer:intValue	()I
    //   258: istore 11
    //   260: iload 11
    //   262: ifle +10 -> 272
    //   265: aload_1
    //   266: invokevirtual 129	com/truecaller/data/entity/Contact:ab	()V
    //   269: iload 8
    //   271: ireturn
    //   272: iload 13
    //   274: iconst_1
    //   275: iadd
    //   276: istore 13
    //   278: goto -45 -> 233
    //   281: astore_1
    //   282: aload 5
    //   284: invokeinterface 102 1 0
    //   289: aload_1
    //   290: athrow
    //   291: astore_1
    //   292: goto +8 -> 300
    //   295: astore_1
    //   296: goto +4 -> 300
    //   299: astore_1
    //   300: aload_1
    //   301: invokestatic 134	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   304: iconst_0
    //   305: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	306	0	this	u
    //   0	306	1	paramContact	Contact
    //   4	2	2	bool1	boolean
    //   13	228	3	localObject1	Object
    //   22	210	4	localObject2	Object
    //   26	257	5	localObject3	Object
    //   37	217	6	localObject4	Object
    //   41	123	7	str1	String
    //   44	226	8	bool2	boolean
    //   51	115	9	arrayOfString	String[]
    //   73	86	10	str2	String
    //   115	3	11	bool3	boolean
    //   258	3	11	i	int
    //   202	3	12	bool4	boolean
    //   225	13	12	j	int
    //   228	49	13	k	int
    // Exception table:
    //   from	to	target	type
    //   108	115	281	finally
    //   122	125	281	finally
    //   127	132	281	finally
    //   138	143	281	finally
    //   147	153	281	finally
    //   158	161	281	finally
    //   165	170	281	finally
    //   172	177	281	finally
    //   181	187	281	finally
    //   9	13	291	android/content/OperationApplicationException
    //   14	18	291	android/content/OperationApplicationException
    //   19	22	291	android/content/OperationApplicationException
    //   28	37	291	android/content/OperationApplicationException
    //   46	51	291	android/content/OperationApplicationException
    //   53	57	291	android/content/OperationApplicationException
    //   59	64	291	android/content/OperationApplicationException
    //   69	72	291	android/content/OperationApplicationException
    //   87	91	291	android/content/OperationApplicationException
    //   98	101	291	android/content/OperationApplicationException
    //   103	108	291	android/content/OperationApplicationException
    //   190	197	291	android/content/OperationApplicationException
    //   197	202	291	android/content/OperationApplicationException
    //   209	212	291	android/content/OperationApplicationException
    //   217	222	291	android/content/OperationApplicationException
    //   223	225	291	android/content/OperationApplicationException
    //   241	244	291	android/content/OperationApplicationException
    //   246	251	291	android/content/OperationApplicationException
    //   253	258	291	android/content/OperationApplicationException
    //   265	269	291	android/content/OperationApplicationException
    //   282	289	291	android/content/OperationApplicationException
    //   289	291	291	android/content/OperationApplicationException
    //   9	13	295	android/os/RemoteException
    //   14	18	295	android/os/RemoteException
    //   19	22	295	android/os/RemoteException
    //   28	37	295	android/os/RemoteException
    //   46	51	295	android/os/RemoteException
    //   53	57	295	android/os/RemoteException
    //   59	64	295	android/os/RemoteException
    //   69	72	295	android/os/RemoteException
    //   87	91	295	android/os/RemoteException
    //   98	101	295	android/os/RemoteException
    //   103	108	295	android/os/RemoteException
    //   190	197	295	android/os/RemoteException
    //   197	202	295	android/os/RemoteException
    //   209	212	295	android/os/RemoteException
    //   217	222	295	android/os/RemoteException
    //   223	225	295	android/os/RemoteException
    //   241	244	295	android/os/RemoteException
    //   246	251	295	android/os/RemoteException
    //   253	258	295	android/os/RemoteException
    //   265	269	295	android/os/RemoteException
    //   282	289	295	android/os/RemoteException
    //   289	291	295	android/os/RemoteException
    //   9	13	299	android/database/sqlite/SQLiteException
    //   14	18	299	android/database/sqlite/SQLiteException
    //   19	22	299	android/database/sqlite/SQLiteException
    //   28	37	299	android/database/sqlite/SQLiteException
    //   46	51	299	android/database/sqlite/SQLiteException
    //   53	57	299	android/database/sqlite/SQLiteException
    //   59	64	299	android/database/sqlite/SQLiteException
    //   69	72	299	android/database/sqlite/SQLiteException
    //   87	91	299	android/database/sqlite/SQLiteException
    //   98	101	299	android/database/sqlite/SQLiteException
    //   103	108	299	android/database/sqlite/SQLiteException
    //   190	197	299	android/database/sqlite/SQLiteException
    //   197	202	299	android/database/sqlite/SQLiteException
    //   209	212	299	android/database/sqlite/SQLiteException
    //   217	222	299	android/database/sqlite/SQLiteException
    //   223	225	299	android/database/sqlite/SQLiteException
    //   241	244	299	android/database/sqlite/SQLiteException
    //   246	251	299	android/database/sqlite/SQLiteException
    //   253	258	299	android/database/sqlite/SQLiteException
    //   265	269	299	android/database/sqlite/SQLiteException
    //   282	289	299	android/database/sqlite/SQLiteException
    //   289	291	299	android/database/sqlite/SQLiteException
  }
  
  public final boolean a(Contact paramContact, Tag paramTag)
  {
    u localu = this;
    Tag localTag1 = paramTag;
    boolean bool1 = c.b(paramContact);
    if (bool1)
    {
      Object localObject1 = paramContact.getId();
      if (localObject1 != null)
      {
        localObject1 = paramTag.a();
        bool1 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool1)
        {
          try
          {
            int i = paramContact.getSource();
            int j = 16;
            i &= j;
            int k = 1;
            if (i == j)
            {
              i = 1;
            }
            else
            {
              i = 0;
              localObject1 = null;
            }
            if (i != 0)
            {
              localObject1 = a;
              localObject1 = ((Context)localObject1).getContentResolver();
              Object localObject2 = TruecallerContract.ah.a();
              localObject3 = "_id";
              Object localObject4 = "tc_id";
              localObject4 = new String[] { localObject3, localObject4 };
              String str1 = "aggregated_contact_id=? AND contact_source=16";
              Object localObject5 = new String[k];
              localObject3 = paramContact.getId();
              localObject3 = String.valueOf(localObject3);
              localObject5[0] = localObject3;
              Object localObject6 = null;
              localObject3 = localObject1;
              localObject3 = ((ContentResolver)localObject1).query((Uri)localObject2, (String[])localObject4, str1, (String[])localObject5, null);
              if (localObject3 != null) {
                try
                {
                  boolean bool2 = ((Cursor)localObject3).moveToFirst();
                  if (bool2)
                  {
                    long l = ((Cursor)localObject3).getLong(0);
                    str1 = ((Cursor)localObject3).getString(k);
                    localObject5 = new java/util/ArrayList;
                    ((ArrayList)localObject5).<init>();
                    localObject6 = TruecallerContract.k.a();
                    localObject6 = ContentProviderOperation.newAssertQuery((Uri)localObject6);
                    String str2 = "data_raw_contact_id=? AND data_type=6 AND data1=?";
                    int n = 2;
                    Object localObject7 = new String[n];
                    String str3 = String.valueOf(l);
                    localObject7[0] = str3;
                    str3 = paramTag.a();
                    localObject7[k] = str3;
                    localObject6 = ((ContentProviderOperation.Builder)localObject6).withSelection(str2, (String[])localObject7);
                    localObject6 = ((ContentProviderOperation.Builder)localObject6).withExpectedCount(0);
                    localObject6 = ((ContentProviderOperation.Builder)localObject6).build();
                    ((ArrayList)localObject5).add(localObject6);
                    localObject6 = new android/content/ContentValues;
                    ((ContentValues)localObject6).<init>();
                    str2 = "data_type";
                    int i1 = 6;
                    localObject7 = Integer.valueOf(i1);
                    ((ContentValues)localObject6).put(str2, (Integer)localObject7);
                    str2 = "tc_id";
                    ((ContentValues)localObject6).put(str2, str1);
                    str2 = "data_raw_contact_id";
                    localObject2 = Long.valueOf(l);
                    ((ContentValues)localObject6).put(str2, (Long)localObject2);
                    localObject2 = "data1";
                    localObject4 = paramTag.a();
                    ((ContentValues)localObject6).put((String)localObject2, (String)localObject4);
                    localObject2 = TruecallerContract.k.a();
                    localObject2 = ContentProviderOperation.newInsert((Uri)localObject2);
                    localObject2 = ((ContentProviderOperation.Builder)localObject2).withValues((ContentValues)localObject6);
                    localObject2 = ((ContentProviderOperation.Builder)localObject2).build();
                    ((ArrayList)localObject5).add(localObject2);
                    try
                    {
                      localObject2 = TruecallerContract.a;
                      localObject1 = ((ContentResolver)localObject1).applyBatch((String)localObject2, (ArrayList)localObject5);
                      int m = localObject1.length;
                      if (m == n)
                      {
                        localObject2 = localObject1[k];
                        localObject2 = uri;
                        if (localObject2 != null)
                        {
                          localTag1.setTcId(str1);
                          localObject1 = localObject1[k];
                          localObject1 = uri;
                          l = ContentUris.parseId((Uri)localObject1);
                          localObject1 = Long.valueOf(l);
                          localTag1.setId((Long)localObject1);
                          return k;
                        }
                      }
                    }
                    catch (OperationApplicationException localOperationApplicationException)
                    {
                      return false;
                    }
                  }
                }
                finally
                {
                  ((Cursor)localObject3).close();
                }
              }
            }
            localObject1 = k.b(paramContact);
            Object localObject3 = null;
            localTag2.setId(null);
            localTag2.setTcId(null);
            ((Contact)localObject1).a(localTag2);
            localObject3 = c;
            ((m)localObject3).a((Contact)localObject1);
            localObject1 = ((Contact)localObject1).getId();
            if (localObject1 == null) {
              break label670;
            }
            i = paramContact.getSource() | j;
            paramContact.setSource(i);
            Long localLong = paramTag.getId();
            if (localLong != null) {
              return k;
            }
            return false;
          }
          catch (RemoteException localRemoteException) {}catch (SQLiteException localSQLiteException) {}
          d.a(localSQLiteException);
        }
      }
    }
    label670:
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */