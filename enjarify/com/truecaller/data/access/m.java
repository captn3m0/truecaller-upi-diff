package com.truecaller.data.access;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.ContactsContract.Data;
import android.text.TextUtils;
import com.google.c.a.k.d;
import com.google.gson.f;
import com.truecaller.content.TruecallerContract.ae;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.content.TruecallerContract.k;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Business;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Link;
import com.truecaller.data.entity.Note;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.RowEntity;
import com.truecaller.data.entity.Source;
import com.truecaller.data.entity.StructuredName;
import com.truecaller.data.entity.Style;
import com.truecaller.data.entity.Tag;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.search.ContactDto.Contact;
import com.truecaller.search.ContactDto.Contact.PhoneNumber;
import com.truecaller.search.ContactDto.Contact.Source;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.c.a.a.a.b.a;
import org.c.a.a.a.k;

public final class m
  extends h
{
  private boolean c = true;
  private boolean d = false;
  
  public m(Context paramContext)
  {
    super(paramContext);
  }
  
  private static void a(List paramList, String paramString)
  {
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    paramString = ContentProviderOperation.newDelete(TruecallerContract.k.a()).withSelection("tc_id=?", arrayOfString).build();
    paramList.add(paramString);
    paramString = ContentProviderOperation.newDelete(TruecallerContract.ah.a()).withSelection("tc_id=?", arrayOfString).build();
    paramList.add(paramString);
  }
  
  private void a(List paramList1, List paramList2, Contact paramContact, boolean paramBoolean)
  {
    m localm = this;
    List localList = paramList1;
    Object localObject1 = paramList2;
    Object localObject2 = paramContact;
    if (paramContact != null)
    {
      boolean bool1 = d;
      if (!bool1)
      {
        paramList2.add(paramContact);
        Object localObject3 = TruecallerContract.ah.a();
        boolean bool2 = c;
        Object localObject4;
        Object localObject5;
        if (!bool2)
        {
          localObject3 = ((Uri)localObject3).buildUpon();
          localObject4 = "aggregation";
          localObject5 = "false";
          localObject3 = ((Uri.Builder)localObject3).appendQueryParameter((String)localObject4, (String)localObject5).build();
        }
        bool2 = d;
        int j = 1;
        boolean bool4 = false;
        Object localObject6 = null;
        if (!bool2)
        {
          bool2 = paramContact.R();
          if (bool2)
          {
            localObject4 = paramContact.getTcId();
            bool2 = TextUtils.isEmpty((CharSequence)localObject4);
            if (!bool2)
            {
              localObject4 = ContentProviderOperation.newAssertQuery(TruecallerContract.ah.a());
              localObject7 = "tc_id=? AND contact_access LIKE ?";
              int m = 2;
              localObject8 = new String[m];
              localObject9 = paramContact.getTcId();
              localObject8[0] = localObject9;
              localObject9 = "public";
              localObject8[j] = localObject9;
              localObject4 = ((ContentProviderOperation.Builder)localObject4).withSelection((String)localObject7, (String[])localObject8).withExpectedCount(0).build();
              localList.add(localObject4);
            }
          }
        }
        int i = paramList1.size();
        localObject3 = ContentProviderOperation.newInsert((Uri)localObject3);
        boolean bool7 = c;
        Object localObject8 = new android/content/ContentValues;
        ((ContentValues)localObject8).<init>();
        Object localObject9 = paramContact.getTcId();
        if (localObject9 == null)
        {
          localObject9 = UUID.randomUUID().toString();
          ((Contact)localObject2).setTcId((String)localObject9);
        }
        if (!bool7)
        {
          localObject7 = "aggregated_contact_id";
          localObject9 = paramContact.k();
          ((ContentValues)localObject8).put((String)localObject7, (Long)localObject9);
        }
        localObject9 = paramContact.getTcId();
        ((ContentValues)localObject8).put("tc_id", (String)localObject9);
        localObject9 = paramContact.z();
        ((ContentValues)localObject8).put("contact_name", (String)localObject9);
        localObject9 = paramContact.K();
        ((ContentValues)localObject8).put("contact_transliterated_name", (String)localObject9);
        localObject9 = Boolean.valueOf(paramContact.X());
        ((ContentValues)localObject8).put("contact_is_favorite", (Boolean)localObject9);
        Object localObject7 = "contact_favorite_position";
        localObject9 = (ContactDto.Contact)mRow;
        int i2 = favoritePosition;
        int i4 = 0;
        Object localObject10 = null;
        if (i2 >= 0)
        {
          i2 = mRow).favoritePosition;
          localObject9 = Integer.valueOf(i2);
        }
        else
        {
          i2 = 0;
          localObject9 = null;
        }
        ((ContentValues)localObject8).put((String)localObject7, (Integer)localObject9);
        localObject9 = mRow).handle;
        ((ContentValues)localObject8).put("contact_handle", (String)localObject9);
        localObject9 = paramContact.l();
        ((ContentValues)localObject8).put("contact_alt_name", (String)localObject9);
        localObject9 = mRow).gender;
        ((ContentValues)localObject8).put("contact_gender", (String)localObject9);
        localObject9 = paramContact.h();
        ((ContentValues)localObject8).put("contact_about", (String)localObject9);
        localObject9 = paramContact.v();
        ((ContentValues)localObject8).put("contact_image_url", (String)localObject9);
        localObject9 = paramContact.x();
        ((ContentValues)localObject8).put("contact_job_title", (String)localObject9);
        localObject9 = paramContact.o();
        ((ContentValues)localObject8).put("contact_company", (String)localObject9);
        localObject9 = paramContact.i();
        ((ContentValues)localObject8).put("contact_access", (String)localObject9);
        localObject9 = Integer.valueOf(mRow).commonConnections);
        ((ContentValues)localObject8).put("contact_common_connections", (Integer)localObject9);
        localObject9 = Long.valueOf(paramContact.H());
        ((ContentValues)localObject8).put("contact_search_time", (Long)localObject9);
        localObject9 = Integer.valueOf(paramContact.getSource());
        ((ContentValues)localObject8).put("contact_source", (Integer)localObject9);
        localObject9 = paramContact.p();
        ((ContentValues)localObject8).put("contact_default_number", (String)localObject9);
        localObject9 = paramContact.E();
        ((ContentValues)localObject8).put("contact_phonebook_id", (Long)localObject9);
        localObject7 = "contact_phonebook_hash";
        localObject9 = (ContactDto.Contact)mRow;
        long l1 = phonebookHash;
        long l2 = 0L;
        boolean bool8 = l1 < l2;
        if (bool8)
        {
          localObject9 = (ContactDto.Contact)mRow;
          long l3 = phonebookHash;
          localObject10 = Long.valueOf(l3);
        }
        ((ContentValues)localObject8).put((String)localObject7, (Long)localObject10);
        localObject9 = paramContact.F();
        ((ContentValues)localObject8).put("contact_phonebook_lookup", (String)localObject9);
        localObject9 = paramContact.G();
        ((ContentValues)localObject8).put("search_query", (String)localObject9);
        localObject9 = paramContact.n();
        ((ContentValues)localObject8).put("cache_control", (String)localObject9);
        localObject9 = Integer.valueOf(f);
        ((ContentValues)localObject8).put("contact_badges", (Integer)localObject9);
        int i3 = k;
        localObject9 = Integer.valueOf(i3);
        ((ContentValues)localObject8).put("tc_flag", (Integer)localObject9);
        localObject7 = "contact_im_id";
        localObject9 = paramContact.j();
        ((ContentValues)localObject8).put((String)localObject7, (String)localObject9);
        localObject3 = ((ContentProviderOperation.Builder)localObject3).withValues((ContentValues)localObject8);
        bool7 = paramBoolean;
        localObject3 = ((ContentProviderOperation.Builder)localObject3).withYieldAllowed(paramBoolean).build();
        localList.add(localObject3);
        localObject3 = paramContact.A().iterator();
        for (;;)
        {
          bool7 = ((Iterator)localObject3).hasNext();
          if (!bool7) {
            break;
          }
          localObject7 = (Number)((Iterator)localObject3).next();
          ((List)localObject1).add(localObject7);
          localObject8 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
          localObject9 = a((RowEntity)localObject7, (Contact)localObject2);
          Object localObject11 = ((Number)localObject7).a();
          ((ContentValues)localObject9).put("data1", (String)localObject11);
          localObject11 = ((Number)localObject7).c();
          ((ContentValues)localObject9).put("data2", (String)localObject11);
          localObject11 = ((Number)localObject7).d();
          ((ContentValues)localObject9).put("data9", (String)localObject11);
          localObject11 = Integer.valueOf(((Number)localObject7).h());
          ((ContentValues)localObject9).put("data3", (Integer)localObject11);
          localObject11 = Integer.valueOf(((Number)localObject7).i());
          ((ContentValues)localObject9).put("data4", (Integer)localObject11);
          localObject11 = ((Number)localObject7).k();
          ((ContentValues)localObject9).put("data5", (String)localObject11);
          int i5 = a.a(mRow).dialingCode);
          localObject11 = Integer.valueOf(i5);
          ((ContentValues)localObject9).put("data6", (Integer)localObject11);
          localObject11 = ((Number)localObject7).l();
          ((ContentValues)localObject9).put("data7", (String)localObject11);
          localObject10 = "data8";
          localObject11 = ((Number)localObject7).m();
          if (localObject11 != null) {
            localObject11 = ((Number)localObject7).m();
          } else {
            localObject11 = k.d.l;
          }
          localObject11 = ((k.d)localObject11).name();
          ((ContentValues)localObject9).put((String)localObject10, (String)localObject11);
          localObject11 = ((Number)localObject7).f();
          ((ContentValues)localObject9).put("data10", (String)localObject11);
          localObject10 = "data_type";
          i5 = 4;
          localObject11 = Integer.valueOf(i5);
          ((ContentValues)localObject9).put((String)localObject10, (Integer)localObject11);
          localObject8 = ((ContentProviderOperation.Builder)localObject8).withValues((ContentValues)localObject9);
          localObject9 = "data_raw_contact_id";
          localObject8 = ((ContentProviderOperation.Builder)localObject8).withValueBackReference((String)localObject9, i).build();
          localList.add(localObject8);
          boolean bool5 = c;
          if (!bool5)
          {
            localObject8 = ((Number)localObject7).a();
            bool5 = k.b((CharSequence)localObject8);
            if (!bool5)
            {
              localObject8 = ContentProviderOperation.newUpdate(TruecallerContract.ae.a());
              localObject10 = new String[j];
              localObject7 = ((Number)localObject7).a();
              localObject10[0] = localObject7;
              localObject7 = ((ContentProviderOperation.Builder)localObject8).withSelection("normalized_destination=?", (String[])localObject10);
              localObject8 = "aggregated_contact_id";
              localObject9 = paramContact.k();
              localObject7 = ((ContentProviderOperation.Builder)localObject7).withValue((String)localObject8, localObject9).build();
              localList.add(localObject7);
            }
          }
        }
        localObject3 = paramContact.d().iterator();
        for (;;)
        {
          bool4 = ((Iterator)localObject3).hasNext();
          if (!bool4) {
            break;
          }
          localObject6 = (Address)((Iterator)localObject3).next();
          ((List)localObject1).add(localObject6);
          localObject7 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
          localObject8 = a((RowEntity)localObject6, (Contact)localObject2);
          localObject10 = ((Address)localObject6).getStreet();
          ((ContentValues)localObject8).put("data1", (String)localObject10);
          localObject10 = ((Address)localObject6).getZipCode();
          ((ContentValues)localObject8).put("data2", (String)localObject10);
          localObject10 = ((Address)localObject6).getCity();
          ((ContentValues)localObject8).put("data3", (String)localObject10);
          localObject10 = ((Address)localObject6).getCountryCode();
          ((ContentValues)localObject8).put("data4", (String)localObject10);
          i4 = ((Address)localObject6).getType();
          localObject10 = Integer.valueOf(i4);
          ((ContentValues)localObject8).put("data5", (Integer)localObject10);
          localObject10 = ((Address)localObject6).getTypeLabel();
          ((ContentValues)localObject8).put("data6", (String)localObject10);
          localObject10 = ((Address)localObject6).getTimeZone();
          ((ContentValues)localObject8).put("data7", (String)localObject10);
          localObject6 = ((Address)localObject6).getArea();
          ((ContentValues)localObject8).put("data8", (String)localObject6);
          localObject9 = Integer.valueOf(j);
          ((ContentValues)localObject8).put("data_type", (Integer)localObject9);
          localObject6 = ((ContentProviderOperation.Builder)localObject7).withValues((ContentValues)localObject8);
          localObject7 = "data_raw_contact_id";
          localObject6 = ((ContentProviderOperation.Builder)localObject6).withValueBackReference((String)localObject7, i).build();
          localList.add(localObject6);
        }
        localObject3 = paramContact.J().iterator();
        boolean bool3;
        int n;
        for (;;)
        {
          bool3 = ((Iterator)localObject3).hasNext();
          if (!bool3) {
            break;
          }
          localObject5 = (Tag)((Iterator)localObject3).next();
          ((List)localObject1).add(localObject5);
          localObject6 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
          localObject7 = a((RowEntity)localObject5, (Contact)localObject2);
          localObject5 = ((Tag)localObject5).a();
          ((ContentValues)localObject7).put("data1", (String)localObject5);
          n = 6;
          localObject8 = Integer.valueOf(n);
          ((ContentValues)localObject7).put("data_type", (Integer)localObject8);
          localObject5 = ((ContentProviderOperation.Builder)localObject6).withValues((ContentValues)localObject7);
          localObject6 = "data_raw_contact_id";
          localObject5 = ((ContentProviderOperation.Builder)localObject5).withValueBackReference((String)localObject6, i).build();
          localList.add(localObject5);
        }
        localObject3 = paramContact.y().iterator();
        for (;;)
        {
          bool3 = ((Iterator)localObject3).hasNext();
          if (!bool3) {
            break;
          }
          localObject5 = (Link)((Iterator)localObject3).next();
          ((List)localObject1).add(localObject5);
          localObject6 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
          localObject7 = a((RowEntity)localObject5, (Contact)localObject2);
          localObject9 = ((Link)localObject5).getInfo();
          ((ContentValues)localObject7).put("data1", (String)localObject9);
          localObject9 = ((Link)localObject5).getService();
          ((ContentValues)localObject7).put("data2", (String)localObject9);
          localObject5 = ((Link)localObject5).getCaption();
          ((ContentValues)localObject7).put("data3", (String)localObject5);
          n = 3;
          localObject8 = Integer.valueOf(n);
          ((ContentValues)localObject7).put("data_type", (Integer)localObject8);
          localObject5 = ((ContentProviderOperation.Builder)localObject6).withValues((ContentValues)localObject7);
          localObject6 = "data_raw_contact_id";
          localObject5 = ((ContentProviderOperation.Builder)localObject5).withValueBackReference((String)localObject6, i).build();
          localList.add(localObject5);
        }
        localObject3 = e;
        if (localObject3 == null)
        {
          localObject3 = Collections.unmodifiableList(b);
          e = ((List)localObject3);
        }
        localObject3 = e.iterator();
        for (;;)
        {
          bool3 = ((Iterator)localObject3).hasNext();
          if (!bool3) {
            break;
          }
          localObject5 = (Source)((Iterator)localObject3).next();
          ((List)localObject1).add(localObject5);
          localObject6 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
          localObject7 = a((RowEntity)localObject5, (Contact)localObject2);
          localObject9 = ((Source)localObject5).a();
          ((ContentValues)localObject7).put("data1", (String)localObject9);
          localObject9 = ((Source)localObject5).b();
          ((ContentValues)localObject7).put("data2", (String)localObject9);
          localObject9 = ((Source)localObject5).c();
          ((ContentValues)localObject7).put("data3", (String)localObject9);
          localObject9 = ((Source)localObject5).d();
          ((ContentValues)localObject7).put("data4", (String)localObject9);
          localObject8 = "data_type";
          i3 = 5;
          localObject9 = Integer.valueOf(i3);
          ((ContentValues)localObject7).put((String)localObject8, (Integer)localObject9);
          localObject5 = mRow).extra;
          if (localObject5 != null)
          {
            boolean bool6 = ((Map)localObject5).isEmpty();
            if (!bool6)
            {
              localObject8 = "data5";
              localObject9 = new com/google/gson/f;
              ((f)localObject9).<init>();
              localObject5 = ((f)localObject9).b(localObject5);
              ((ContentValues)localObject7).put((String)localObject8, (String)localObject5);
            }
          }
          localObject5 = ((ContentProviderOperation.Builder)localObject6).withValues((ContentValues)localObject7);
          localObject6 = "data_raw_contact_id";
          localObject5 = ((ContentProviderOperation.Builder)localObject5).withValueBackReference((String)localObject6, i).build();
          localList.add(localObject5);
        }
        localObject3 = g;
        int i1;
        if (localObject3 != null)
        {
          ((List)localObject1).add(localObject3);
          localObject5 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
          localObject6 = a((RowEntity)localObject3, (Contact)localObject2);
          localObject8 = ((StructuredName)localObject3).getGivenName();
          ((ContentValues)localObject6).put("data1", (String)localObject8);
          localObject8 = ((StructuredName)localObject3).getFamilyName();
          ((ContentValues)localObject6).put("data2", (String)localObject8);
          localObject3 = ((StructuredName)localObject3).getMiddleName();
          ((ContentValues)localObject6).put("data3", (String)localObject3);
          i1 = 7;
          localObject7 = Integer.valueOf(i1);
          ((ContentValues)localObject6).put("data_type", (Integer)localObject7);
          localObject3 = ((ContentProviderOperation.Builder)localObject5).withValues((ContentValues)localObject6);
          localObject5 = "data_raw_contact_id";
          localObject3 = ((ContentProviderOperation.Builder)localObject3).withValueBackReference((String)localObject5, i).build();
          localList.add(localObject3);
        }
        localObject3 = h;
        if (localObject3 != null)
        {
          ((List)localObject1).add(localObject3);
          localObject5 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
          localObject6 = a((RowEntity)localObject3, (Contact)localObject2);
          localObject3 = ((Note)localObject3).getValue();
          ((ContentValues)localObject6).put("data1", (String)localObject3);
          i1 = 8;
          localObject7 = Integer.valueOf(i1);
          ((ContentValues)localObject6).put("data_type", (Integer)localObject7);
          localObject3 = ((ContentProviderOperation.Builder)localObject5).withValues((ContentValues)localObject6);
          localObject5 = "data_raw_contact_id";
          localObject3 = ((ContentProviderOperation.Builder)localObject3).withValueBackReference((String)localObject5, i).build();
          localList.add(localObject3);
        }
        localObject3 = i;
        if (localObject3 != null)
        {
          ((List)localObject1).add(localObject3);
          localObject5 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
          localObject6 = a((RowEntity)localObject3, (Contact)localObject2);
          localObject8 = ((Business)localObject3).getBranch();
          ((ContentValues)localObject6).put("data1", (String)localObject8);
          localObject8 = ((Business)localObject3).getDepartment();
          ((ContentValues)localObject6).put("data2", (String)localObject8);
          localObject8 = ((Business)localObject3).getCompanySize();
          ((ContentValues)localObject6).put("data3", (String)localObject8);
          localObject8 = ((Business)localObject3).getOpeningHours();
          ((ContentValues)localObject6).put("data4", (String)localObject8);
          localObject8 = ((Business)localObject3).getLandline();
          ((ContentValues)localObject6).put("data5", (String)localObject8);
          localObject8 = ((Business)localObject3).getScore();
          ((ContentValues)localObject6).put("data6", (String)localObject8);
          localObject3 = ((Business)localObject3).getSwishNumber();
          ((ContentValues)localObject6).put("data7", (String)localObject3);
          i1 = 9;
          localObject7 = Integer.valueOf(i1);
          ((ContentValues)localObject6).put("data_type", (Integer)localObject7);
          localObject3 = ((ContentProviderOperation.Builder)localObject5).withValues((ContentValues)localObject6);
          localObject5 = "data_raw_contact_id";
          localObject3 = ((ContentProviderOperation.Builder)localObject3).withValueBackReference((String)localObject5, i).build();
          localList.add(localObject3);
        }
        localObject3 = j;
        if (localObject3 != null)
        {
          ((List)localObject1).add(localObject3);
          localObject1 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
          localObject2 = a((RowEntity)localObject3, (Contact)localObject2);
          localObject6 = ((Style)localObject3).getBackgroundColor();
          ((ContentValues)localObject2).put("data1", (String)localObject6);
          localObject3 = ((Style)localObject3).getImageUrls();
          ((ContentValues)localObject2).put("data2", (String)localObject3);
          localObject3 = "data_type";
          int k = 10;
          localObject5 = Integer.valueOf(k);
          ((ContentValues)localObject2).put((String)localObject3, (Integer)localObject5);
          localObject1 = ((ContentProviderOperation.Builder)localObject1).withValues((ContentValues)localObject2);
          localObject2 = "data_raw_contact_id";
          localObject1 = ((ContentProviderOperation.Builder)localObject1).withValueBackReference((String)localObject2, i).build();
          localList.add(localObject1);
        }
        return;
      }
    }
  }
  
  public final Cursor a()
  {
    ContentResolver localContentResolver = b;
    Uri localUri = TruecallerContract.ah.a();
    String[] tmp13_10 = new String[3];
    String[] tmp14_13 = tmp13_10;
    String[] tmp14_13 = tmp13_10;
    tmp14_13[0] = "tc_id";
    tmp14_13[1] = "contact_phonebook_id";
    tmp14_13[2] = "contact_phonebook_hash";
    String[] arrayOfString = tmp14_13;
    return localContentResolver.query(localUri, arrayOfString, "contact_phonebook_hash IS NOT NULL", null, "contact_phonebook_id ASC");
  }
  
  public final Contact a(Uri paramUri, String paramString, String... paramVarArgs)
  {
    ContentResolver localContentResolver = b;
    int i = paramVarArgs.length;
    String[] arrayOfString1 = null;
    String[] arrayOfString2;
    if (i == 0) {
      arrayOfString2 = null;
    } else {
      arrayOfString2 = paramVarArgs;
    }
    paramUri = localContentResolver.query(paramUri, null, paramString, arrayOfString2, null);
    if (paramUri != null) {
      try
      {
        boolean bool1 = paramUri.moveToFirst();
        if (bool1)
        {
          paramString = new com/truecaller/data/access/e;
          paramString.<init>(paramUri);
          paramVarArgs = null;
          paramString.a(false);
          paramVarArgs = paramString.a(paramUri);
          boolean bool2;
          do
          {
            paramString.a(paramUri, paramVarArgs);
            bool2 = paramUri.moveToNext();
          } while (bool2);
          arrayOfString1 = paramVarArgs;
        }
      }
      finally
      {
        paramUri.close();
      }
    }
    return arrayOfString1;
  }
  
  public final Contact a(String paramString)
  {
    Uri localUri = TruecallerContract.ah.b();
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    return a(localUri, "tc_id=?", arrayOfString);
  }
  
  public final List a(long paramLong)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (bool1)
    {
      Object localObject1 = b;
      Uri localUri = TruecallerContract.ah.b();
      Contact localContact = null;
      Object localObject2 = "aggregated_contact_id=?";
      bool1 = true;
      String[] arrayOfString = new String[bool1];
      Object localObject3 = String.valueOf(paramLong);
      Object localObject4 = null;
      arrayOfString[0] = localObject3;
      localObject3 = ((ContentResolver)localObject1).query(localUri, null, (String)localObject2, arrayOfString, null);
      if (localObject3 != null) {
        try
        {
          boolean bool2 = ((Cursor)localObject3).moveToFirst();
          if (bool2)
          {
            localObject1 = new com/truecaller/data/access/e;
            ((e)localObject1).<init>((Cursor)localObject3);
            ((e)localObject1).a(false);
            localContact = null;
            int i = 0;
            localUri = null;
            while (i == 0)
            {
              long l2;
              if (localContact == null)
              {
                l2 = l1;
              }
              else
              {
                localObject2 = localContact.getId();
                l2 = ((Long)localObject2).longValue();
              }
              long l3 = ((e)localObject1).c((Cursor)localObject3);
              boolean bool3;
              if (localContact != null)
              {
                bool3 = l2 < l3;
                if (!bool3) {}
              }
              else
              {
                localContact = ((e)localObject1).a((Cursor)localObject3);
                localArrayList.add(localContact);
              }
              boolean bool4;
              do
              {
                ((e)localObject1).a((Cursor)localObject3, localContact);
                bool4 = ((Cursor)localObject3).moveToNext();
                if (bool4)
                {
                  l2 = ((e)localObject1).c((Cursor)localObject3);
                  bool3 = l2 < l3;
                  if (!bool3)
                  {
                    bool4 = true;
                  }
                  else
                  {
                    bool4 = false;
                    localObject2 = null;
                  }
                }
                else
                {
                  i = 1;
                }
              } while (bool4);
            }
          }
        }
        finally
        {
          ((Cursor)localObject3).close();
        }
      }
    }
    return localArrayList;
  }
  
  public final void a(String paramString, Long paramLong)
  {
    int i = 1;
    if (paramLong != null)
    {
      long l1 = paramLong.longValue();
      long l2 = 0L;
      boolean bool = l1 < l2;
      if (bool)
      {
        localObject1 = new android/content/ContentValues;
        ((ContentValues)localObject1).<init>();
        Integer localInteger = Integer.valueOf(i);
        ((ContentValues)localObject1).put("is_super_primary", localInteger);
        localObject2 = ContactsContract.Data.CONTENT_URI;
        l2 = paramLong.longValue();
        paramLong = ContentUris.withAppendedId((Uri)localObject2, l2);
        localObject2 = b;
        localInteger = null;
        ((ContentResolver)localObject2).update(paramLong, (ContentValues)localObject1, null, null);
      }
    }
    paramLong = new android/content/ContentValues;
    paramLong.<init>();
    Object localObject2 = Integer.valueOf(i);
    paramLong.put("data_is_primary", (Integer)localObject2);
    Object localObject1 = TruecallerContract.k.a();
    localObject2 = b;
    String[] arrayOfString = new String[i];
    arrayOfString[0] = paramString;
    ((ContentResolver)localObject2).update((Uri)localObject1, paramLong, "_id=?", arrayOfString);
  }
  
  public final void a(List paramList)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramList.next();
      a(localArrayList, str);
    }
    paramList = Collections.emptyList();
    a(localArrayList, paramList);
  }
  
  public final void a(List paramList1, List paramList2)
  {
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    ArrayList localArrayList2 = new java/util/ArrayList;
    localArrayList2.<init>();
    paramList1 = paramList1.iterator();
    for (;;)
    {
      bool1 = paramList1.hasNext();
      if (!bool1) {
        break;
      }
      str = (String)paramList1.next();
      a(localArrayList1, str);
    }
    paramList1 = paramList2.iterator();
    paramList2 = null;
    boolean bool1 = false;
    String str = null;
    for (;;)
    {
      boolean bool2 = paramList1.hasNext();
      if (!bool2) {
        break;
      }
      Contact localContact = (Contact)paramList1.next();
      boolean bool3 = true;
      int i;
      bool1 += bool3;
      int j = i % 5;
      if (j != 0) {
        bool3 = false;
      }
      a(localArrayList1, localArrayList2, localContact, bool3);
    }
    a(localArrayList1, localArrayList2);
  }
  
  public final boolean a(Contact paramContact)
  {
    paramContact = Collections.singletonList(paramContact);
    return b(paramContact);
  }
  
  public final boolean a(String paramString, int... paramVarArgs)
  {
    int i = paramVarArgs.length;
    int k = 1;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      str1 = null;
    }
    Object localObject = { "At least one source is required" };
    AssertionUtil.AlwaysFatal.isFalse(i, (String[])localObject);
    String[] arrayOfString1 = new String[paramVarArgs.length + k];
    arrayOfString1[0] = paramString;
    paramString = new java/lang/StringBuilder;
    paramString.<init>();
    paramString.append("data1=? AND data_type=4 AND contact_source IN (?");
    String str1 = String.valueOf(paramVarArgs[0]);
    arrayOfString1[k] = str1;
    int n;
    for (int j = 1;; j = n)
    {
      int m = paramVarArgs.length;
      if (j >= m) {
        break;
      }
      localObject = ",?";
      paramString.append((String)localObject);
      n = j + 1;
      str1 = String.valueOf(paramVarArgs[j]);
      arrayOfString1[n] = str1;
    }
    paramString.append(")");
    localObject = b;
    Uri localUri = TruecallerContract.ah.b();
    String[] arrayOfString2 = { "tc_id" };
    String str2 = paramString.toString();
    paramString = ((ContentResolver)localObject).query(localUri, arrayOfString2, str2, arrayOfString1, null);
    paramVarArgs = new java/util/ArrayList;
    paramVarArgs.<init>();
    if (paramString != null) {
      try
      {
        for (;;)
        {
          j = paramString.moveToNext();
          if (j == 0) {
            break;
          }
          str1 = paramString.getString(0);
          paramVarArgs.add(str1);
        }
      }
      finally
      {
        paramString.close();
      }
    }
    a(paramVarArgs);
    boolean bool = paramVarArgs.isEmpty();
    if (!bool) {
      return k;
    }
    return false;
  }
  
  public final boolean b(List paramList)
  {
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    ArrayList localArrayList2 = new java/util/ArrayList;
    localArrayList2.<init>();
    paramList = paramList.iterator();
    int i = 0;
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      Contact localContact = (Contact)paramList.next();
      int j = 1;
      i += j;
      int k = i % 5;
      if (k != 0) {
        j = 0;
      }
      a(localArrayList1, localArrayList2, localContact, j);
    }
    return a(localArrayList1, localArrayList2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */