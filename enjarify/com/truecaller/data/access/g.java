package com.truecaller.data.access;

import c.n.m;
import c.t;

public final class g
{
  private final n[] a;
  
  public g(n... paramVarArgs)
  {
    a = paramVarArgs;
  }
  
  static String a(String paramString)
  {
    if (paramString != null)
    {
      paramString = m.f((CharSequence)paramString);
      if (paramString != null)
      {
        char c = paramString.charValue();
        boolean bool2 = Character.isLetter(c);
        if (bool2) {
          return String.valueOf(Character.toUpperCase(c));
        }
        boolean bool1 = Character.isDigit(c);
        if (bool1) {
          return "#";
        }
        return "?";
      }
    }
    return null;
  }
  
  final c.n b(String paramString)
  {
    n[] arrayOfn = a;
    paramString = t.a(paramString, null);
    int i = arrayOfn.length;
    int j = 0;
    while (j < i)
    {
      Object localObject = arrayOfn[j];
      String str = (String)a;
      paramString = (String)b;
      localObject = ((n)localObject).a(str);
      str = (String)a;
      localObject = (String)b;
      if (localObject != null) {
        paramString = (String)localObject;
      }
      paramString = t.a(str, paramString);
      j += 1;
    }
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */