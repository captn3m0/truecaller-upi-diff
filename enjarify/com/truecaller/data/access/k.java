package com.truecaller.data.access;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import java.util.Iterator;
import java.util.List;

public final class k
  extends h
{
  private final m c;
  
  public k(Context paramContext)
  {
    super(paramContext);
    m localm = new com/truecaller/data/access/m;
    localm.<init>(paramContext);
    c = localm;
  }
  
  static Contact b(Contact paramContact)
  {
    Contact localContact = new com/truecaller/data/entity/Contact;
    localContact.<init>();
    int i = 16;
    localContact.setSource(i);
    Object localObject = paramContact.getId();
    localContact.a((Long)localObject);
    paramContact = paramContact.A().iterator();
    for (;;)
    {
      boolean bool = paramContact.hasNext();
      if (!bool) {
        break;
      }
      localObject = (Number)paramContact.next();
      Number localNumber = new com/truecaller/data/entity/Number;
      localNumber.<init>((Number)localObject);
      bool = false;
      localObject = null;
      localNumber.setId(null);
      localNumber.setTcId(null);
      localContact.a(localNumber);
    }
    long l = System.currentTimeMillis();
    localContact.a(l);
    return localContact;
  }
  
  private Contact c(Contact paramContact)
  {
    ContentResolver localContentResolver = a.getContentResolver();
    Object localObject1 = TruecallerContract.ah.b();
    String str = "aggregated_contact_id=? AND contact_source=16";
    int i = 1;
    String[] arrayOfString = new String[i];
    Object localObject2 = String.valueOf(paramContact.getId());
    arrayOfString[0] = localObject2;
    Contact localContact = null;
    localObject2 = localContentResolver.query((Uri)localObject1, null, str, arrayOfString, null);
    localContentResolver = null;
    if (localObject2 != null) {
      try
      {
        boolean bool1 = ((Cursor)localObject2).moveToFirst();
        if (bool1)
        {
          localObject1 = new com/truecaller/data/access/e;
          ((e)localObject1).<init>((Cursor)localObject2);
          ((e)localObject1).a(false);
          localContact = ((e)localObject1).a((Cursor)localObject2);
          boolean bool2;
          do
          {
            ((e)localObject1).a((Cursor)localObject2, localContact);
            bool2 = ((Cursor)localObject2).moveToNext();
          } while (bool2);
        }
        else
        {
          localContact = null;
        }
      }
      finally
      {
        ((Cursor)localObject2).close();
      }
    }
    localContact = null;
    if (localContact == null) {
      localContact = b(paramContact);
    } else {
      localContact.setId(null);
    }
    return localContact;
  }
  
  public final Contact a(Contact paramContact, String paramString)
  {
    boolean bool = c.b(paramContact);
    if (bool)
    {
      Object localObject = paramContact.getId();
      if (localObject != null)
      {
        paramContact = c(paramContact);
        paramContact.l(paramString);
        c.a(paramContact);
        paramString = new com/truecaller/data/access/c;
        localObject = a;
        paramString.<init>((Context)localObject);
        return paramString.a(paramContact);
      }
    }
    return null;
  }
  
  /* Error */
  public final String a(Contact paramContact)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 140	com/truecaller/data/access/c:b	(Lcom/truecaller/data/entity/Contact;)Z
    //   4: istore_2
    //   5: iload_2
    //   6: ifeq +150 -> 156
    //   9: aload_1
    //   10: invokevirtual 29	com/truecaller/data/entity/Contact:getId	()Ljava/lang/Long;
    //   13: astore_3
    //   14: aload_3
    //   15: ifnull +141 -> 156
    //   18: aload_0
    //   19: getfield 79	com/truecaller/data/access/k:a	Landroid/content/Context;
    //   22: invokevirtual 85	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   25: astore 4
    //   27: invokestatic 152	com/truecaller/content/TruecallerContract$ah:a	()Landroid/net/Uri;
    //   30: astore 5
    //   32: iconst_1
    //   33: anewarray 96	java/lang/String
    //   36: dup
    //   37: iconst_0
    //   38: ldc -102
    //   40: aastore
    //   41: astore 6
    //   43: ldc 93
    //   45: astore 7
    //   47: iconst_1
    //   48: anewarray 96	java/lang/String
    //   51: astore 8
    //   53: iconst_0
    //   54: istore_2
    //   55: aconst_null
    //   56: astore_3
    //   57: aload_1
    //   58: invokevirtual 29	com/truecaller/data/entity/Contact:getId	()Ljava/lang/Long;
    //   61: invokestatic 100	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   64: astore_1
    //   65: aload 8
    //   67: iconst_0
    //   68: aload_1
    //   69: aastore
    //   70: aload 4
    //   72: aload 5
    //   74: aload 6
    //   76: aload 7
    //   78: aload 8
    //   80: aconst_null
    //   81: invokevirtual 106	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   84: astore_1
    //   85: aload_1
    //   86: ifnull +70 -> 156
    //   89: aload_1
    //   90: invokeinterface 111 1 0
    //   95: istore_2
    //   96: iload_2
    //   97: ifeq +30 -> 127
    //   100: ldc -102
    //   102: astore_3
    //   103: aload_1
    //   104: aload_3
    //   105: invokeinterface 158 2 0
    //   110: istore_2
    //   111: aload_1
    //   112: iload_2
    //   113: invokeinterface 162 2 0
    //   118: astore_3
    //   119: aload_1
    //   120: invokeinterface 131 1 0
    //   125: aload_3
    //   126: areturn
    //   127: aload_1
    //   128: invokeinterface 131 1 0
    //   133: goto +23 -> 156
    //   136: astore_3
    //   137: goto +11 -> 148
    //   140: astore_3
    //   141: aload_3
    //   142: invokestatic 167	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   145: goto -18 -> 127
    //   148: aload_1
    //   149: invokeinterface 131 1 0
    //   154: aload_3
    //   155: athrow
    //   156: aconst_null
    //   157: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	158	0	this	k
    //   0	158	1	paramContact	Contact
    //   4	93	2	bool	boolean
    //   110	3	2	i	int
    //   13	113	3	localObject1	Object
    //   136	1	3	localObject2	Object
    //   140	15	3	localSQLiteException	android.database.sqlite.SQLiteException
    //   25	46	4	localContentResolver	ContentResolver
    //   30	43	5	localUri	Uri
    //   41	34	6	arrayOfString1	String[]
    //   45	32	7	str	String
    //   51	28	8	arrayOfString2	String[]
    // Exception table:
    //   from	to	target	type
    //   89	95	136	finally
    //   104	110	136	finally
    //   112	118	136	finally
    //   141	145	136	finally
    //   89	95	140	android/database/sqlite/SQLiteException
    //   104	110	140	android/database/sqlite/SQLiteException
    //   112	118	140	android/database/sqlite/SQLiteException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */