package com.truecaller.data.access;

import android.content.ContentResolver;
import android.os.CancellationSignal;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.common.h.u;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import java.util.Iterator;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class j
  implements i
{
  final ContentResolver a;
  final c b;
  final f c;
  private final u d;
  private final f e;
  
  public j(ContentResolver paramContentResolver, u paramu, c paramc, f paramf1, f paramf2)
  {
    a = paramContentResolver;
    d = paramu;
    b = paramc;
    c = paramf1;
    e = paramf2;
  }
  
  public final CancellationSignal a(String paramString, Integer paramInteger, i.a parama)
  {
    k.b(paramString, "filter");
    k.b(parama, "callback");
    CancellationSignal localCancellationSignal = new android/os/CancellationSignal;
    localCancellationSignal.<init>();
    Object localObject1 = bg.a;
    Object localObject2 = localObject1;
    localObject2 = (ag)localObject1;
    f localf = e;
    Object localObject3 = new com/truecaller/data/access/j$c;
    localObject1 = localObject3;
    ((j.c)localObject3).<init>(this, paramString, paramInteger, localCancellationSignal, parama, null);
    localObject3 = (m)localObject3;
    e.b((ag)localObject2, localf, (m)localObject3, 2);
    return localCancellationSignal;
  }
  
  public final n a(String paramString)
  {
    k.b(paramString, "numberString");
    Object localObject1 = d.b(paramString);
    if (localObject1 != null) {
      paramString = (String)localObject1;
    }
    localObject1 = b.b(paramString);
    Object localObject2 = null;
    if (localObject1 != null)
    {
      Object localObject3 = ((Contact)localObject1).A();
      if (localObject3 != null)
      {
        localObject3 = ((Iterable)localObject3).iterator();
        Object localObject4;
        boolean bool2;
        do
        {
          boolean bool1 = ((Iterator)localObject3).hasNext();
          if (!bool1) {
            break;
          }
          localObject4 = ((Iterator)localObject3).next();
          Object localObject5 = localObject4;
          localObject5 = (Number)localObject4;
          String str = "it";
          k.a(localObject5, str);
          localObject5 = ((Number)localObject5).a();
          bool2 = k.a(localObject5, paramString);
        } while (!bool2);
        localObject2 = localObject4;
        localObject2 = (Number)localObject2;
      }
    }
    return t.a(localObject1, localObject2);
  }
  
  /* Error */
  public final java.util.List a(String paramString, Integer paramInteger)
  {
    // Byte code:
    //   0: ldc 46
    //   2: astore_3
    //   3: aload_1
    //   4: aload_3
    //   5: invokestatic 22	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   8: aload_1
    //   9: invokestatic 137	com/truecaller/content/TruecallerContract$a:b	(Ljava/lang/String;)Landroid/net/Uri;
    //   12: astore_1
    //   13: aload_2
    //   14: ifnull +66 -> 80
    //   17: aload_2
    //   18: checkcast 139	java/lang/Number
    //   21: astore_2
    //   22: aload_2
    //   23: invokevirtual 143	java/lang/Number:intValue	()I
    //   26: istore 4
    //   28: aload_1
    //   29: invokevirtual 149	android/net/Uri:buildUpon	()Landroid/net/Uri$Builder;
    //   32: astore_3
    //   33: ldc -105
    //   35: astore 5
    //   37: ldc -103
    //   39: astore 6
    //   41: iload 4
    //   43: invokestatic 159	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   46: astore_2
    //   47: aload 6
    //   49: aload_2
    //   50: invokevirtual 162	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   53: astore_2
    //   54: aload_3
    //   55: aload 5
    //   57: aload_2
    //   58: invokevirtual 168	android/net/Uri$Builder:appendQueryParameter	(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   61: astore_2
    //   62: aload_2
    //   63: invokevirtual 172	android/net/Uri$Builder:build	()Landroid/net/Uri;
    //   66: astore_2
    //   67: aload_2
    //   68: ifnonnull +6 -> 74
    //   71: goto +9 -> 80
    //   74: aload_2
    //   75: astore 5
    //   77: goto +6 -> 83
    //   80: aload_1
    //   81: astore 5
    //   83: aload_0
    //   84: getfield 36	com/truecaller/data/access/j:a	Landroid/content/ContentResolver;
    //   87: astore_3
    //   88: iconst_0
    //   89: istore 7
    //   91: aconst_null
    //   92: astore 6
    //   94: aconst_null
    //   95: astore 8
    //   97: aload_3
    //   98: aload 5
    //   100: aconst_null
    //   101: aconst_null
    //   102: aconst_null
    //   103: aconst_null
    //   104: invokevirtual 178	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   107: astore_1
    //   108: aload_1
    //   109: ifnull +163 -> 272
    //   112: new 180	com/truecaller/data/a
    //   115: astore_2
    //   116: aload_2
    //   117: aload_1
    //   118: invokespecial 183	com/truecaller/data/a:<init>	(Landroid/database/Cursor;)V
    //   121: aload_2
    //   122: checkcast 185	android/database/Cursor
    //   125: astore_2
    //   126: aload_2
    //   127: astore_1
    //   128: aload_2
    //   129: checkcast 187	java/io/Closeable
    //   132: astore_1
    //   133: aconst_null
    //   134: astore_3
    //   135: new 189	java/util/ArrayList
    //   138: astore 5
    //   140: aload 5
    //   142: invokespecial 190	java/util/ArrayList:<init>	()V
    //   145: aload 5
    //   147: checkcast 192	java/util/Collection
    //   150: astore 5
    //   152: aload_2
    //   153: invokeinterface 195 1 0
    //   158: istore 7
    //   160: iload 7
    //   162: ifeq +62 -> 224
    //   165: aload_2
    //   166: astore 6
    //   168: aload_2
    //   169: checkcast 180	com/truecaller/data/a
    //   172: astore 6
    //   174: aload 6
    //   176: invokevirtual 198	com/truecaller/data/a:a	()Lcom/truecaller/data/entity/Contact;
    //   179: astore 8
    //   181: aload 8
    //   183: ifnull +22 -> 205
    //   186: aload 6
    //   188: invokevirtual 200	com/truecaller/data/a:b	()Ljava/lang/String;
    //   191: astore 6
    //   193: aload 8
    //   195: aload 6
    //   197: invokestatic 122	c/t:a	(Ljava/lang/Object;Ljava/lang/Object;)Lc/n;
    //   200: astore 6
    //   202: goto +9 -> 211
    //   205: iconst_0
    //   206: istore 7
    //   208: aconst_null
    //   209: astore 6
    //   211: aload 5
    //   213: aload 6
    //   215: invokeinterface 204 2 0
    //   220: pop
    //   221: goto -69 -> 152
    //   224: aload 5
    //   226: checkcast 206	java/util/List
    //   229: astore 5
    //   231: aload_1
    //   232: aconst_null
    //   233: invokestatic 211	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   236: aload 5
    //   238: checkcast 91	java/lang/Iterable
    //   241: astore 5
    //   243: aload 5
    //   245: invokestatic 216	c/a/m:e	(Ljava/lang/Iterable;)Ljava/util/List;
    //   248: astore_1
    //   249: aload_1
    //   250: ifnonnull +31 -> 281
    //   253: goto +19 -> 272
    //   256: astore_2
    //   257: goto +8 -> 265
    //   260: astore_2
    //   261: aload_2
    //   262: astore_3
    //   263: aload_2
    //   264: athrow
    //   265: aload_1
    //   266: aload_3
    //   267: invokestatic 211	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   270: aload_2
    //   271: athrow
    //   272: getstatic 221	c/a/y:a	Lc/a/y;
    //   275: astore_1
    //   276: aload_1
    //   277: checkcast 206	java/util/List
    //   280: astore_1
    //   281: aload_1
    //   282: areturn
    //   283: invokestatic 227	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   286: getstatic 221	c/a/y:a	Lc/a/y;
    //   289: checkcast 206	java/util/List
    //   292: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	293	0	this	j
    //   0	293	1	paramString	String
    //   0	293	2	paramInteger	Integer
    //   2	265	3	localObject1	Object
    //   26	16	4	i	int
    //   35	209	5	localObject2	Object
    //   39	175	6	localObject3	Object
    //   89	118	7	bool	boolean
    //   95	99	8	localContact	Contact
    // Exception table:
    //   from	to	target	type
    //   263	265	256	finally
    //   135	138	260	finally
    //   140	145	260	finally
    //   145	150	260	finally
    //   152	158	260	finally
    //   168	172	260	finally
    //   174	179	260	finally
    //   186	191	260	finally
    //   195	200	260	finally
    //   213	221	260	finally
    //   224	229	260	finally
    //   8	12	283	finally
    //   17	21	283	finally
    //   22	26	283	finally
    //   28	32	283	finally
    //   41	46	283	finally
    //   49	53	283	finally
    //   57	61	283	finally
    //   62	66	283	finally
    //   83	87	283	finally
    //   103	107	283	finally
    //   112	115	283	finally
    //   117	121	283	finally
    //   121	125	283	finally
    //   128	132	283	finally
    //   232	236	283	finally
    //   236	241	283	finally
    //   243	248	283	finally
    //   266	270	283	finally
    //   270	272	283	finally
    //   272	275	283	finally
    //   276	280	283	finally
  }
  
  public final void a(i.a parama)
  {
    k.b(parama, "callback");
    ag localag = (ag)bg.a;
    f localf = e;
    Object localObject = new com/truecaller/data/access/j$a;
    ((j.a)localObject).<init>(this, parama, null);
    localObject = (m)localObject;
    e.b(localag, localf, (m)localObject, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */