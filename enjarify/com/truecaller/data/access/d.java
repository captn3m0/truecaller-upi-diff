package com.truecaller.data.access;

import android.database.Cursor;

public final class d
{
  public final int a;
  public final int b;
  public final int c;
  public final int d;
  
  public d(Cursor paramCursor)
  {
    int i = paramCursor.getColumnIndex("history_event_id");
    a = i;
    i = paramCursor.getColumnIndex("recording_path");
    b = i;
    i = paramCursor.getColumnIndex("_id");
    c = i;
    int j = paramCursor.getColumnIndex("history_call_recording_id");
    d = j;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */