package com.truecaller.data.access;

import android.os.CancellationSignal;
import c.d.c;
import c.g.a.m;
import c.x;
import kotlinx.coroutines.ag;

final class j$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private ag i;
  
  j$c(j paramj, String paramString, Integer paramInteger, CancellationSignal paramCancellationSignal, i.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/data/access/j$c;
    j localj = d;
    String str = e;
    Integer localInteger = f;
    CancellationSignal localCancellationSignal = g;
    i.a locala = h;
    localc.<init>(localj, str, localInteger, localCancellationSignal, locala, paramc);
    paramObject = (ag)paramObject;
    i = ((ag)paramObject);
    return localc;
  }
  
  /* Error */
  public final Object a(Object paramObject)
  {
    // Byte code:
    //   0: getstatic 57	c/d/a/a:a	Lc/d/a/a;
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 59	com/truecaller/data/access/j$c:c	I
    //   8: istore_3
    //   9: iconst_1
    //   10: istore 4
    //   12: aconst_null
    //   13: astore 5
    //   15: iload_3
    //   16: tableswitch	default:+32->48, 0:+120->136, 1:+88->104, 2:+66->82, 3:+44->60
    //   48: new 62	java/lang/IllegalStateException
    //   51: astore_1
    //   52: aload_1
    //   53: ldc 64
    //   55: invokespecial 67	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   58: aload_1
    //   59: athrow
    //   60: aload_1
    //   61: instanceof 69
    //   64: istore 6
    //   66: iload 6
    //   68: ifne +6 -> 74
    //   71: goto +629 -> 700
    //   74: aload_1
    //   75: checkcast 69	c/o$b
    //   78: getfield 72	c/o$b:a	Ljava/lang/Throwable;
    //   81: athrow
    //   82: aload_1
    //   83: instanceof 69
    //   86: istore 6
    //   88: iload 6
    //   90: ifne +6 -> 96
    //   93: goto +607 -> 700
    //   96: aload_1
    //   97: checkcast 69	c/o$b
    //   100: getfield 72	c/o$b:a	Ljava/lang/Throwable;
    //   103: athrow
    //   104: aload_1
    //   105: instanceof 69
    //   108: istore_3
    //   109: iload_3
    //   110: ifne +6 -> 116
    //   113: goto +587 -> 700
    //   116: aload_1
    //   117: checkcast 69	c/o$b
    //   120: astore_1
    //   121: aload_1
    //   122: getfield 72	c/o$b:a	Ljava/lang/Throwable;
    //   125: astore_1
    //   126: aload_1
    //   127: athrow
    //   128: astore_1
    //   129: goto +395 -> 524
    //   132: astore_1
    //   133: goto +454 -> 587
    //   136: aload_1
    //   137: instanceof 69
    //   140: istore_3
    //   141: iload_3
    //   142: ifne +562 -> 704
    //   145: aload_0
    //   146: getfield 27	com/truecaller/data/access/j$c:e	Ljava/lang/String;
    //   149: astore_1
    //   150: aload_1
    //   151: invokestatic 77	com/truecaller/content/TruecallerContract$a:b	(Ljava/lang/String;)Landroid/net/Uri;
    //   154: astore_1
    //   155: aload_0
    //   156: getfield 29	com/truecaller/data/access/j$c:f	Ljava/lang/Integer;
    //   159: astore 7
    //   161: aload 7
    //   163: ifnull +74 -> 237
    //   166: aload 7
    //   168: checkcast 79	java/lang/Number
    //   171: astore 7
    //   173: aload 7
    //   175: invokevirtual 83	java/lang/Number:intValue	()I
    //   178: istore_3
    //   179: aload_1
    //   180: invokevirtual 89	android/net/Uri:buildUpon	()Landroid/net/Uri$Builder;
    //   183: astore 8
    //   185: ldc 91
    //   187: astore 9
    //   189: ldc 93
    //   191: astore 10
    //   193: iload_3
    //   194: invokestatic 99	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   197: astore 7
    //   199: aload 10
    //   201: aload 7
    //   203: invokevirtual 103	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   206: astore 7
    //   208: aload 8
    //   210: aload 9
    //   212: aload 7
    //   214: invokevirtual 109	android/net/Uri$Builder:appendQueryParameter	(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   217: astore 7
    //   219: aload 7
    //   221: invokevirtual 113	android/net/Uri$Builder:build	()Landroid/net/Uri;
    //   224: astore 7
    //   226: aload 7
    //   228: ifnonnull +6 -> 234
    //   231: goto +6 -> 237
    //   234: aload 7
    //   236: astore_1
    //   237: aload_0
    //   238: getfield 25	com/truecaller/data/access/j$c:d	Lcom/truecaller/data/access/j;
    //   241: astore 7
    //   243: aload 7
    //   245: getfield 118	com/truecaller/data/access/j:a	Landroid/content/ContentResolver;
    //   248: astore 8
    //   250: iconst_0
    //   251: istore 11
    //   253: aconst_null
    //   254: astore 10
    //   256: aconst_null
    //   257: astore 12
    //   259: aload_0
    //   260: getfield 31	com/truecaller/data/access/j$c:g	Landroid/os/CancellationSignal;
    //   263: astore 13
    //   265: aload_1
    //   266: astore 9
    //   268: aload 8
    //   270: aload_1
    //   271: aconst_null
    //   272: aconst_null
    //   273: aconst_null
    //   274: aconst_null
    //   275: aload 13
    //   277: invokevirtual 124	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    //   280: astore 7
    //   282: aload 7
    //   284: ifnull +181 -> 465
    //   287: new 126	com/truecaller/data/a
    //   290: astore 8
    //   292: aload 8
    //   294: aload 7
    //   296: invokespecial 129	com/truecaller/data/a:<init>	(Landroid/database/Cursor;)V
    //   299: aload 8
    //   301: checkcast 131	android/database/Cursor
    //   304: astore 8
    //   306: aload 8
    //   308: astore 7
    //   310: aload 8
    //   312: checkcast 133	java/io/Closeable
    //   315: astore 7
    //   317: new 135	java/util/ArrayList
    //   320: astore 9
    //   322: aload 9
    //   324: invokespecial 138	java/util/ArrayList:<init>	()V
    //   327: aload 9
    //   329: checkcast 140	java/util/Collection
    //   332: astore 9
    //   334: aload 8
    //   336: invokeinterface 144 1 0
    //   341: istore 11
    //   343: iload 11
    //   345: ifeq +64 -> 409
    //   348: aload 8
    //   350: astore 10
    //   352: aload 8
    //   354: checkcast 126	com/truecaller/data/a
    //   357: astore 10
    //   359: aload 10
    //   361: invokevirtual 147	com/truecaller/data/a:a	()Lcom/truecaller/data/entity/Contact;
    //   364: astore 12
    //   366: aload 12
    //   368: ifnull +22 -> 390
    //   371: aload 10
    //   373: invokevirtual 150	com/truecaller/data/a:b	()Ljava/lang/String;
    //   376: astore 10
    //   378: aload 12
    //   380: aload 10
    //   382: invokestatic 155	c/t:a	(Ljava/lang/Object;Ljava/lang/Object;)Lc/n;
    //   385: astore 10
    //   387: goto +9 -> 396
    //   390: iconst_0
    //   391: istore 11
    //   393: aconst_null
    //   394: astore 10
    //   396: aload 9
    //   398: aload 10
    //   400: invokeinterface 159 2 0
    //   405: pop
    //   406: goto -72 -> 334
    //   409: aload 9
    //   411: checkcast 161	java/util/List
    //   414: astore 9
    //   416: aload 7
    //   418: aconst_null
    //   419: invokestatic 166	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   422: aload 9
    //   424: checkcast 168	java/lang/Iterable
    //   427: astore 9
    //   429: aload 9
    //   431: invokestatic 173	c/a/m:e	(Ljava/lang/Iterable;)Ljava/util/List;
    //   434: astore 7
    //   436: aload 7
    //   438: ifnonnull +39 -> 477
    //   441: goto +24 -> 465
    //   444: astore_1
    //   445: aload_1
    //   446: athrow
    //   447: astore 14
    //   449: aload_1
    //   450: astore 8
    //   452: aload 14
    //   454: astore_1
    //   455: aload 7
    //   457: aload 8
    //   459: invokestatic 166	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   462: aload 14
    //   464: athrow
    //   465: getstatic 178	c/a/y:a	Lc/a/y;
    //   468: astore 7
    //   470: aload 7
    //   472: checkcast 161	java/util/List
    //   475: astore 7
    //   477: aload_0
    //   478: getfield 25	com/truecaller/data/access/j$c:d	Lcom/truecaller/data/access/j;
    //   481: astore 8
    //   483: aload_0
    //   484: getfield 33	com/truecaller/data/access/j$c:h	Lcom/truecaller/data/access/i$a;
    //   487: astore 9
    //   489: aload_0
    //   490: aload_1
    //   491: putfield 180	com/truecaller/data/access/j$c:a	Ljava/lang/Object;
    //   494: aload_0
    //   495: aload 7
    //   497: putfield 182	com/truecaller/data/access/j$c:b	Ljava/lang/Object;
    //   500: aload_0
    //   501: iload 4
    //   503: putfield 59	com/truecaller/data/access/j$c:c	I
    //   506: aload 8
    //   508: aload 7
    //   510: aload 9
    //   512: aload_0
    //   513: invokevirtual 185	com/truecaller/data/access/j:a	(Ljava/util/List;Lcom/truecaller/data/access/i$a;Lc/d/c;)Ljava/lang/Object;
    //   516: astore_1
    //   517: aload_1
    //   518: aload_2
    //   519: if_acmpne +181 -> 700
    //   522: aload_2
    //   523: areturn
    //   524: aload_1
    //   525: invokestatic 191	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   528: aload_0
    //   529: getfield 25	com/truecaller/data/access/j$c:d	Lcom/truecaller/data/access/j;
    //   532: getfield 194	com/truecaller/data/access/j:c	Lc/d/f;
    //   535: astore 7
    //   537: new 196	com/truecaller/data/access/j$c$1
    //   540: astore 15
    //   542: aload 15
    //   544: aload_0
    //   545: aload_1
    //   546: aconst_null
    //   547: invokespecial 199	com/truecaller/data/access/j$c$1:<init>	(Lcom/truecaller/data/access/j$c;Ljava/lang/Throwable;Lc/d/c;)V
    //   550: aload 15
    //   552: checkcast 6	c/g/a/m
    //   555: astore 15
    //   557: aload_0
    //   558: aload_1
    //   559: putfield 180	com/truecaller/data/access/j$c:a	Ljava/lang/Object;
    //   562: iconst_3
    //   563: istore 16
    //   565: aload_0
    //   566: iload 16
    //   568: putfield 59	com/truecaller/data/access/j$c:c	I
    //   571: aload 7
    //   573: aload 15
    //   575: aload_0
    //   576: invokestatic 205	kotlinx/coroutines/g:a	(Lc/d/f;Lc/g/a/m;Lc/d/c;)Ljava/lang/Object;
    //   579: astore_1
    //   580: aload_1
    //   581: aload_2
    //   582: if_acmpne +118 -> 700
    //   585: aload_2
    //   586: areturn
    //   587: iload 4
    //   589: anewarray 95	java/lang/String
    //   592: astore 7
    //   594: iconst_0
    //   595: istore 4
    //   597: new 207	java/lang/StringBuilder
    //   600: astore 5
    //   602: aload 5
    //   604: ldc -47
    //   606: invokespecial 210	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   609: aload_0
    //   610: getfield 27	com/truecaller/data/access/j$c:e	Ljava/lang/String;
    //   613: astore 8
    //   615: aload 5
    //   617: aload 8
    //   619: invokevirtual 214	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   622: pop
    //   623: bipush 96
    //   625: istore 17
    //   627: aload 5
    //   629: iload 17
    //   631: invokevirtual 218	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   634: pop
    //   635: aload 5
    //   637: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   640: astore 5
    //   642: aload 7
    //   644: iconst_0
    //   645: aload 5
    //   647: aastore
    //   648: aload_0
    //   649: getfield 25	com/truecaller/data/access/j$c:d	Lcom/truecaller/data/access/j;
    //   652: astore 7
    //   654: getstatic 178	c/a/y:a	Lc/a/y;
    //   657: checkcast 161	java/util/List
    //   660: astore 15
    //   662: aload_0
    //   663: getfield 33	com/truecaller/data/access/j$c:h	Lcom/truecaller/data/access/i$a;
    //   666: astore 5
    //   668: aload_0
    //   669: aload_1
    //   670: putfield 180	com/truecaller/data/access/j$c:a	Ljava/lang/Object;
    //   673: iconst_2
    //   674: istore 16
    //   676: aload_0
    //   677: iload 16
    //   679: putfield 59	com/truecaller/data/access/j$c:c	I
    //   682: aload 7
    //   684: aload 15
    //   686: aload 5
    //   688: aload_0
    //   689: invokevirtual 185	com/truecaller/data/access/j:a	(Ljava/util/List;Lcom/truecaller/data/access/i$a;Lc/d/c;)Ljava/lang/Object;
    //   692: astore_1
    //   693: aload_1
    //   694: aload_2
    //   695: if_acmpne +5 -> 700
    //   698: aload_2
    //   699: areturn
    //   700: getstatic 226	c/x:a	Lc/x;
    //   703: areturn
    //   704: aload_1
    //   705: checkcast 69	c/o$b
    //   708: getfield 72	c/o$b:a	Ljava/lang/Throwable;
    //   711: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	712	0	this	c
    //   0	712	1	paramObject	Object
    //   3	696	2	locala	c.d.a.a
    //   8	8	3	j	int
    //   108	34	3	bool1	boolean
    //   178	16	3	k	int
    //   10	586	4	m	int
    //   13	674	5	localObject1	Object
    //   64	25	6	bool2	boolean
    //   159	524	7	localObject2	Object
    //   183	435	8	localObject3	Object
    //   187	324	9	localObject4	Object
    //   191	208	10	localObject5	Object
    //   251	141	11	bool3	boolean
    //   257	122	12	localContact	com.truecaller.data.entity.Contact
    //   263	13	13	localCancellationSignal	CancellationSignal
    //   447	16	14	localObject6	Object
    //   540	145	15	localObject7	Object
    //   563	115	16	n	int
    //   625	5	17	c1	char
    // Exception table:
    //   from	to	target	type
    //   116	120	128	finally
    //   121	125	128	finally
    //   126	128	128	finally
    //   145	149	128	finally
    //   150	154	128	finally
    //   155	159	128	finally
    //   166	171	128	finally
    //   173	178	128	finally
    //   179	183	128	finally
    //   193	197	128	finally
    //   201	206	128	finally
    //   212	217	128	finally
    //   219	224	128	finally
    //   237	241	128	finally
    //   243	248	128	finally
    //   259	263	128	finally
    //   275	280	128	finally
    //   287	290	128	finally
    //   294	299	128	finally
    //   299	304	128	finally
    //   310	315	128	finally
    //   418	422	128	finally
    //   422	427	128	finally
    //   429	434	128	finally
    //   457	462	128	finally
    //   462	465	128	finally
    //   465	468	128	finally
    //   470	475	128	finally
    //   477	481	128	finally
    //   483	487	128	finally
    //   490	494	128	finally
    //   495	500	128	finally
    //   501	506	128	finally
    //   512	516	128	finally
    //   116	120	132	android/os/OperationCanceledException
    //   121	125	132	android/os/OperationCanceledException
    //   126	128	132	android/os/OperationCanceledException
    //   145	149	132	android/os/OperationCanceledException
    //   150	154	132	android/os/OperationCanceledException
    //   155	159	132	android/os/OperationCanceledException
    //   166	171	132	android/os/OperationCanceledException
    //   173	178	132	android/os/OperationCanceledException
    //   179	183	132	android/os/OperationCanceledException
    //   193	197	132	android/os/OperationCanceledException
    //   201	206	132	android/os/OperationCanceledException
    //   212	217	132	android/os/OperationCanceledException
    //   219	224	132	android/os/OperationCanceledException
    //   237	241	132	android/os/OperationCanceledException
    //   243	248	132	android/os/OperationCanceledException
    //   259	263	132	android/os/OperationCanceledException
    //   275	280	132	android/os/OperationCanceledException
    //   287	290	132	android/os/OperationCanceledException
    //   294	299	132	android/os/OperationCanceledException
    //   299	304	132	android/os/OperationCanceledException
    //   310	315	132	android/os/OperationCanceledException
    //   418	422	132	android/os/OperationCanceledException
    //   422	427	132	android/os/OperationCanceledException
    //   429	434	132	android/os/OperationCanceledException
    //   457	462	132	android/os/OperationCanceledException
    //   462	465	132	android/os/OperationCanceledException
    //   465	468	132	android/os/OperationCanceledException
    //   470	475	132	android/os/OperationCanceledException
    //   477	481	132	android/os/OperationCanceledException
    //   483	487	132	android/os/OperationCanceledException
    //   490	494	132	android/os/OperationCanceledException
    //   495	500	132	android/os/OperationCanceledException
    //   501	506	132	android/os/OperationCanceledException
    //   512	516	132	android/os/OperationCanceledException
    //   317	320	444	finally
    //   322	327	444	finally
    //   327	332	444	finally
    //   334	341	444	finally
    //   352	357	444	finally
    //   359	364	444	finally
    //   371	376	444	finally
    //   380	385	444	finally
    //   398	406	444	finally
    //   409	414	444	finally
    //   445	447	447	finally
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.j.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */