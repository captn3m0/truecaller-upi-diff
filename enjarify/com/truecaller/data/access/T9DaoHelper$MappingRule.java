package com.truecaller.data.access;

public enum T9DaoHelper$MappingRule
{
  static
  {
    MappingRule[] arrayOfMappingRule = new MappingRule[3];
    MappingRule localMappingRule = new com/truecaller/data/access/T9DaoHelper$MappingRule;
    localMappingRule.<init>("MATCH_IF_STARTS_WITH", 0);
    MATCH_IF_STARTS_WITH = localMappingRule;
    arrayOfMappingRule[0] = localMappingRule;
    localMappingRule = new com/truecaller/data/access/T9DaoHelper$MappingRule;
    int i = 1;
    localMappingRule.<init>("MATCH_ANYWHERE", i);
    MATCH_ANYWHERE = localMappingRule;
    arrayOfMappingRule[i] = localMappingRule;
    localMappingRule = new com/truecaller/data/access/T9DaoHelper$MappingRule;
    i = 2;
    localMappingRule.<init>("MATCH_ANY_WORD", i);
    MATCH_ANY_WORD = localMappingRule;
    arrayOfMappingRule[i] = localMappingRule;
    $VALUES = arrayOfMappingRule;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.T9DaoHelper.MappingRule
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */