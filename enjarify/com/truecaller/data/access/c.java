package com.truecaller.data.access;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.truecaller.common.c.b.b;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.Contact;

public class c
  extends h
{
  private static UriMatcher c;
  
  public c(Context paramContext)
  {
    super(paramContext);
  }
  
  private Contact a(Uri paramUri, String paramString, String... paramVarArgs)
  {
    ContentResolver localContentResolver = b;
    paramUri = localContentResolver.query(paramUri, null, paramString, paramVarArgs, null);
    paramString = null;
    if (paramUri != null) {
      try
      {
        boolean bool1 = paramUri.moveToFirst();
        if (bool1)
        {
          paramString = new com/truecaller/data/access/e;
          paramString.<init>(paramUri);
          bool1 = true;
          paramString.a(bool1);
          paramVarArgs = paramString.a(paramUri);
          boolean bool2;
          do
          {
            paramString.a(paramUri, paramVarArgs);
            bool2 = paramUri.moveToNext();
          } while (bool2);
          paramVarArgs.ad();
          paramString = paramVarArgs;
        }
      }
      finally
      {
        paramUri.close();
      }
    }
    return paramString;
  }
  
  private static void a(UriMatcher paramUriMatcher, Uri paramUri, int paramInt)
  {
    String str = paramUri.getAuthority();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramUri = paramUri.getPathSegments();
    paramUri = TextUtils.join("/", paramUri);
    localStringBuilder.append(paramUri);
    localStringBuilder.append("/#");
    paramUri = localStringBuilder.toString();
    paramUriMatcher.addURI(str, paramUri, paramInt);
  }
  
  private static UriMatcher b()
  {
    ??? = c;
    if (??? == null) {
      synchronized (c.class)
      {
        UriMatcher localUriMatcher = c;
        if (localUriMatcher == null)
        {
          localUriMatcher = new android/content/UriMatcher;
          int i = -1;
          localUriMatcher.<init>(i);
          c = localUriMatcher;
          Uri localUri = TruecallerContract.ah.a();
          int j = 1;
          a(localUriMatcher, localUri, j);
          localUriMatcher = c;
          localUri = TruecallerContract.a.a();
          int k = 2;
          a(localUriMatcher, localUri, k);
          localUriMatcher = c;
          localUri = TruecallerContract.ah.b();
          a(localUriMatcher, localUri, j);
          localUriMatcher = c;
          localUri = TruecallerContract.a.b();
          a(localUriMatcher, localUri, k);
          localUriMatcher = c;
          localUri = TruecallerContract.n.a();
          j = 3;
          a(localUriMatcher, localUri, j);
          localUriMatcher = c;
          localUri = TruecallerContract.n.c();
          a(localUriMatcher, localUri, j);
          localUriMatcher = c;
          localUri = TruecallerContract.n.d();
          a(localUriMatcher, localUri, j);
        }
      }
    }
    return c;
  }
  
  public static boolean b(Contact paramContact)
  {
    boolean bool1 = false;
    if (paramContact != null)
    {
      Object localObject = c;
      if (localObject != null)
      {
        boolean bool2 = d;
        if (bool2)
        {
          localObject = b();
          paramContact = c;
          int j = ((UriMatcher)localObject).match(paramContact);
          int i = 2;
          if (j != i)
          {
            i = 3;
            if (j != i) {}
          }
          else
          {
            j = 1;
            bool1 = true;
          }
        }
      }
    }
    return bool1;
  }
  
  private Contact c(long paramLong)
  {
    Uri localUri = TruecallerContract.a.b();
    String[] arrayOfString = new String[1];
    String str = String.valueOf(paramLong);
    arrayOfString[0] = str;
    paramLong = a(localUri, "_id", "aggregated_raw_contact_id=?", arrayOfString);
    return a(paramLong);
  }
  
  public final int a()
  {
    ContentResolver localContentResolver = b;
    Uri localUri = TruecallerContract.a.a();
    return b.a(localContentResolver, localUri, null, null);
  }
  
  public final long a(Uri paramUri, String paramString1, String paramString2, String... paramVarArgs)
  {
    ContentResolver localContentResolver = b;
    int i = 1;
    String[] arrayOfString = new String[i];
    arrayOfString[0] = paramString1;
    int j = paramVarArgs.length;
    boolean bool;
    if (j == 0)
    {
      bool = false;
      paramVarArgs = null;
    }
    paramUri = localContentResolver.query(paramUri, arrayOfString, paramString2, paramVarArgs, null);
    long l = -1;
    if (paramUri != null) {
      try
      {
        bool = paramUri.moveToNext();
        if (bool) {
          l = paramUri.getLong(0);
        }
      }
      finally
      {
        paramUri.close();
      }
    }
    return l;
  }
  
  public final Contact a(long paramLong)
  {
    long l = 1L;
    boolean bool = paramLong < l;
    if (bool) {
      return null;
    }
    Uri localUri = TruecallerContract.a.b();
    String[] arrayOfString = new String[1];
    String str = String.valueOf(paramLong);
    arrayOfString[0] = str;
    return a(localUri, "_id=?", arrayOfString);
  }
  
  public final Contact a(Uri paramUri)
  {
    if (paramUri == null) {
      return null;
    }
    UriMatcher localUriMatcher = b();
    int i = localUriMatcher.match(paramUri);
    int j = -1;
    if (i == j) {
      return null;
    }
    long l = ContentUris.parseId(paramUri);
    switch (i)
    {
    default: 
      return null;
    case 3: 
      return b(l);
    case 2: 
      return a(l);
    }
    return c(l);
  }
  
  public final Contact a(Contact paramContact)
  {
    if (paramContact == null) {
      return null;
    }
    Object localObject = paramContact.getTcId();
    if (localObject != null)
    {
      localObject = a((String)localObject);
      if (localObject != null) {
        return (Contact)localObject;
      }
    }
    localObject = paramContact.k();
    long l;
    if (localObject != null)
    {
      l = ((Long)localObject).longValue();
      localObject = a(l);
      if (localObject != null) {
        return (Contact)localObject;
      }
    }
    localObject = c;
    if (localObject != null)
    {
      localObject = a((Uri)localObject);
      if (localObject != null) {
        return (Contact)localObject;
      }
    }
    paramContact = paramContact.E();
    if (paramContact != null)
    {
      l = paramContact.longValue();
      paramContact = TruecallerContract.ah.a().buildUpon().appendQueryParameter("limit", "1").build();
      String str1 = "aggregated_contact_id";
      String str2 = "contact_phonebook_id=";
      localObject = String.valueOf(l);
      localObject = str2.concat((String)localObject);
      String[] arrayOfString = new String[0];
      l = a(paramContact, str1, (String)localObject, arrayOfString);
      paramContact = a(l);
      if (paramContact != null) {
        return paramContact;
      }
    }
    return null;
  }
  
  public final Contact a(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    Uri localUri = TruecallerContract.ah.a();
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    long l = a(localUri, "aggregated_contact_id", "tc_id=?", arrayOfString);
    return a(l);
  }
  
  public final Contact b(long paramLong)
  {
    Uri localUri = TruecallerContract.n.d();
    String[] arrayOfString = new String[1];
    String str = String.valueOf(paramLong);
    arrayOfString[0] = str;
    paramLong = a(localUri, "history_aggregated_contact_id", "_id=?", arrayOfString);
    return a(paramLong);
  }
  
  public final Contact b(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    Uri localUri = TruecallerContract.ah.b().buildUpon().appendQueryParameter("limit", "1").build();
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    long l = a(localUri, "aggregated_contact_id", "data1=? AND data_type=4", arrayOfString);
    return a(l);
  }
  
  public final Contact c(Contact paramContact)
  {
    Object localObject = a(paramContact);
    if (localObject != null) {
      return (Contact)localObject;
    }
    boolean bool1 = d;
    if (!bool1)
    {
      int i = Integer.bitCount(paramContact.getSource());
      int j = 1;
      if (i == j)
      {
        localObject = paramContact.getTcId();
        boolean bool2 = TextUtils.isEmpty((CharSequence)localObject);
        if (!bool2)
        {
          localObject = new com/truecaller/data/access/m;
          Context localContext = a;
          ((m)localObject).<init>(localContext);
          ((m)localObject).a(paramContact);
          return a(paramContact);
        }
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */