package com.truecaller.data.access;

import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.RemoteException;
import com.truecaller.content.TruecallerContract;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Entity;
import com.truecaller.data.entity.RowEntity;
import com.truecaller.log.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class h
{
  final Context a;
  public final ContentResolver b;
  
  protected h(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    paramContext = a.getContentResolver();
    b = paramContext;
  }
  
  static ContentValues a(RowEntity paramRowEntity, Contact paramContact)
  {
    Object localObject1 = paramRowEntity.getTcId();
    if (localObject1 == null)
    {
      if (paramContact != null)
      {
        localObject1 = paramContact.getTcId();
        if (localObject1 != null)
        {
          paramContact = paramContact.getTcId();
          paramRowEntity.setTcId(paramContact);
          break label80;
        }
      }
      localObject1 = new java/lang/IllegalArgumentException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Cannot generate child entity content values without a tc_id value, child=");
      ((StringBuilder)localObject2).append(paramRowEntity);
      ((StringBuilder)localObject2).append(", parent=");
      ((StringBuilder)localObject2).append(paramContact);
      paramRowEntity = ((StringBuilder)localObject2).toString();
      ((IllegalArgumentException)localObject1).<init>(paramRowEntity);
      throw ((Throwable)localObject1);
    }
    label80:
    paramContact = new android/content/ContentValues;
    paramContact.<init>();
    Object localObject2 = paramRowEntity.getTcId();
    paramContact.put("tc_id", (String)localObject2);
    localObject2 = Integer.valueOf(paramRowEntity.isPrimary());
    paramContact.put("data_is_primary", (Integer)localObject2);
    paramRowEntity = paramRowEntity.getDataPhonebookId();
    paramContact.put("data_phonebook_id", paramRowEntity);
    return paramContact;
  }
  
  final boolean a(ArrayList paramArrayList, List paramList)
  {
    boolean bool1 = paramArrayList.isEmpty();
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    bool1 = false;
    try
    {
      try
      {
        ContentResolver localContentResolver = b;
        String str = TruecallerContract.a;
        paramArrayList = localContentResolver.applyBatch(str, paramArrayList);
        paramList = paramList.iterator();
        int i = paramArrayList.length;
        int j = 0;
        str = null;
        while (j < i)
        {
          Object localObject1 = paramArrayList[j];
          Object localObject2 = uri;
          if (localObject2 != null)
          {
            localObject2 = paramList.next();
            localObject2 = (Entity)localObject2;
            localObject1 = uri;
            long l = ContentUris.parseId((Uri)localObject1);
            localObject1 = Long.valueOf(l);
            ((Entity)localObject2).setId((Long)localObject1);
          }
          j += 1;
        }
        boolean bool3 = paramList.hasNext();
        if (!bool3) {
          return bool2;
        }
        return false;
      }
      catch (SQLiteException paramArrayList) {}catch (IllegalArgumentException paramArrayList) {}catch (RemoteException paramArrayList) {}
      d.a(paramArrayList);
    }
    catch (OperationApplicationException localOperationApplicationException)
    {
      for (;;) {}
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */