package com.truecaller.data.access;

import android.database.Cursor;
import com.truecaller.data.entity.RowEntity;

abstract class e$c
  extends a
{
  final int a;
  final int b;
  final int c;
  final int d;
  final int e;
  
  e$c(Cursor paramCursor)
  {
    String[] arrayOfString = { "data_id", "_id" };
    int i = a(paramCursor, arrayOfString);
    a = i;
    arrayOfString = new String[] { "data_tc_id", "tc_id" };
    i = a(paramCursor, arrayOfString);
    b = i;
    arrayOfString = new String[] { "data_is_primary" };
    i = a(paramCursor, arrayOfString);
    c = i;
    arrayOfString = new String[] { "data_phonebook_id" };
    i = a(paramCursor, arrayOfString);
    d = i;
    arrayOfString = new String[] { "aggregated_raw_contact_source", "contact_source" };
    int j = a(paramCursor, arrayOfString);
    e = j;
  }
  
  abstract RowEntity a(Cursor paramCursor);
  
  public final RowEntity b(Cursor paramCursor)
  {
    int i = a;
    int j = -1;
    if (i != j)
    {
      boolean bool = paramCursor.isNull(i);
      if (!bool)
      {
        RowEntity localRowEntity = a(paramCursor);
        j = a;
        Object localObject = b(paramCursor, j);
        localRowEntity.setId((Long)localObject);
        j = b;
        localObject = d(paramCursor, j);
        localRowEntity.setTcId((String)localObject);
        j = c;
        j = c(paramCursor, j);
        int k = 1;
        if (j != k) {
          k = 0;
        }
        localRowEntity.setIsPrimary(k);
        j = d;
        localObject = b(paramCursor, j);
        localRowEntity.setDataPhonebookId((Long)localObject);
        j = e;
        int m = c(paramCursor, j);
        localRowEntity.setSource(m);
        return localRowEntity;
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */