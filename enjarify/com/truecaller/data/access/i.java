package com.truecaller.data.access;

import android.os.CancellationSignal;
import c.n;
import java.util.List;

public abstract interface i
{
  public abstract CancellationSignal a(String paramString, Integer paramInteger, i.a parama);
  
  public abstract n a(String paramString);
  
  public abstract List a(String paramString, Integer paramInteger);
  
  public abstract void a(i.a parama);
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */