package com.truecaller.data.access;

import android.content.ContentResolver;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.CancellationSignal;
import android.os.OperationCanceledException;
import c.a.m;
import c.f.b;
import c.g.b.k;
import c.t;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.data.a;
import com.truecaller.log.AssertionUtil;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class s
{
  private final ContentResolver a;
  private final T9DaoHelper b;
  
  public s(ContentResolver paramContentResolver, T9DaoHelper paramT9DaoHelper)
  {
    a = paramContentResolver;
    b = paramT9DaoHelper;
  }
  
  public final List a(String paramString, CancellationSignal paramCancellationSignal, Integer paramInteger1, Integer paramInteger2)
  {
    String str1 = paramString;
    Object localObject1 = paramInteger1;
    Object localObject5 = "searchToken";
    k.b(paramString, (String)localObject5);
    long l1 = System.currentTimeMillis();
    Object localObject6 = TruecallerContract.a.a(paramString);
    Object localObject7;
    Object localObject8;
    if ((paramInteger1 != null) && (paramInteger2 != null))
    {
      localObject6 = ((Uri)localObject6).buildUpon();
      localObject7 = "limit";
      localObject8 = new java/lang/StringBuilder;
      ((StringBuilder)localObject8).<init>();
      ((StringBuilder)localObject8).append(paramInteger1);
      ((StringBuilder)localObject8).append(", ");
      ((StringBuilder)localObject8).append(paramInteger2);
      localObject1 = ((StringBuilder)localObject8).toString();
      localObject6 = ((Uri.Builder)localObject6).appendQueryParameter((String)localObject7, (String)localObject1).build();
      localObject1 = "buildUpon().appendQueryP…osition, $count\").build()";
      k.a(localObject6, (String)localObject1);
    }
    else
    {
      localObject1 = "this";
      k.a(localObject6, (String)localObject1);
    }
    int i = 1;
    Object localObject9 = null;
    try
    {
      long l2 = System.currentTimeMillis();
      localObject7 = a;
      Object localObject10 = null;
      StringBuilder localStringBuilder = null;
      String str2 = null;
      localObject8 = localObject6;
      localObject1 = ((ContentResolver)localObject7).query((Uri)localObject6, null, null, null, null, paramCancellationSignal);
      long l3 = System.currentTimeMillis() - l2;
      localObject10 = new String[i];
      localStringBuilder = new java/lang/StringBuilder;
      str2 = "getMappedContacts, query for ";
      localStringBuilder.<init>(str2);
      localStringBuilder.append(localObject6);
      localObject6 = " returned ";
      localStringBuilder.append((String)localObject6);
      int j;
      if (localObject1 != null)
      {
        j = ((Cursor)localObject1).getCount();
        localObject6 = Integer.valueOf(j);
      }
      else
      {
        j = 0;
        localObject6 = null;
      }
      localStringBuilder.append(localObject6);
      localObject6 = " rows, took: ";
      localStringBuilder.append((String)localObject6);
      localStringBuilder.append(l3);
      localObject6 = "ms";
      localStringBuilder.append((String)localObject6);
      localObject6 = localStringBuilder.toString();
      localObject10[0] = localObject6;
      if (localObject1 != null)
      {
        localObject6 = new com/truecaller/data/a;
        ((a)localObject6).<init>((Cursor)localObject1);
        localObject6 = (Cursor)localObject6;
        localObject7 = localObject6;
        localObject7 = (Closeable)localObject6;
        try
        {
          localObject1 = new java/util/ArrayList;
          ((ArrayList)localObject1).<init>();
          localObject1 = (Collection)localObject1;
          for (;;)
          {
            boolean bool = ((Cursor)localObject6).moveToNext();
            if (!bool) {
              break;
            }
            localObject8 = localObject6;
            localObject8 = (a)localObject6;
            localObject10 = ((a)localObject8).a();
            if (localObject10 != null)
            {
              localObject8 = ((a)localObject8).b();
              localObject8 = t.a(localObject10, localObject8);
            }
            else
            {
              bool = false;
              localObject8 = null;
            }
            ((Collection)localObject1).add(localObject8);
          }
          localObject1 = (List)localObject1;
          b.a((Closeable)localObject7, null);
          localObject1 = (Iterable)localObject1;
          localObject1 = m.e((Iterable)localObject1);
          localObject9 = localObject1;
        }
        finally
        {
          localObject6 = localObject2;
          try
          {
            throw ((Throwable)localObject2);
          }
          finally
          {
            b.a((Closeable)localObject7, (Throwable)localObject6);
          }
        }
      }
      char c;
      if (localObject9 != null) {
        break label582;
      }
    }
    catch (SQLException localSQLException)
    {
      localObject4 = (Throwable)localSQLException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject4);
    }
    catch (OperationCanceledException localOperationCanceledException)
    {
      localObject4 = new String[i];
      localObject6 = new java/lang/StringBuilder;
      localObject7 = "cancellation Exception while search for `";
      ((StringBuilder)localObject6).<init>((String)localObject7);
      ((StringBuilder)localObject6).append(str1);
      c = '`';
      ((StringBuilder)localObject6).append(c);
      localObject6 = ((StringBuilder)localObject6).toString();
      localObject4[0] = localObject6;
    }
    Object localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject9 = localObject4;
    localObject9 = (List)localObject4;
    label582:
    long l4 = System.currentTimeMillis() - l1;
    localObject4 = new String[i];
    localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>("getMappedContacts filtered with `");
    ((StringBuilder)localObject5).append(str1);
    ((StringBuilder)localObject5).append("` returns ");
    int k = ((List)localObject9).size();
    ((StringBuilder)localObject5).append(k);
    ((StringBuilder)localObject5).append(" contacts, took: ");
    ((StringBuilder)localObject5).append(l4);
    ((StringBuilder)localObject5).append("ms");
    str1 = ((StringBuilder)localObject5).toString();
    localObject4[0] = str1;
    return (List)localObject9;
  }
  
  /* Error */
  public final void a(android.content.Context paramContext, boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_3
    //   2: aload_1
    //   3: astore 4
    //   5: ldc -60
    //   7: astore 5
    //   9: aload_1
    //   10: aload 5
    //   12: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   15: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   18: lstore 6
    //   20: aload_0
    //   21: getfield 29	com/truecaller/data/access/s:a	Landroid/content/ContentResolver;
    //   24: astore 8
    //   26: invokestatic 200	com/truecaller/content/TruecallerContract$am:a	()Landroid/net/Uri;
    //   29: astore 9
    //   31: ldc -54
    //   33: astore 10
    //   35: aconst_null
    //   36: astore 11
    //   38: aload 8
    //   40: aload 9
    //   42: aload 10
    //   44: aconst_null
    //   45: invokevirtual 206	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   48: istore 12
    //   50: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   53: lload 6
    //   55: lsub
    //   56: lstore 13
    //   58: iconst_1
    //   59: istore 15
    //   61: iload 15
    //   63: anewarray 93	java/lang/String
    //   66: astore 16
    //   68: new 54	java/lang/StringBuilder
    //   71: astore 17
    //   73: aload 17
    //   75: ldc -48
    //   77: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   80: invokestatic 200	com/truecaller/content/TruecallerContract$am:a	()Landroid/net/Uri;
    //   83: astore 18
    //   85: aload 17
    //   87: aload 18
    //   89: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   92: pop
    //   93: bipush 32
    //   95: istore 19
    //   97: aload 17
    //   99: iload 19
    //   101: invokevirtual 178	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   104: pop
    //   105: aload 17
    //   107: iload 12
    //   109: invokevirtual 188	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   112: pop
    //   113: aload 17
    //   115: ldc -45
    //   117: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload 17
    //   123: lload 13
    //   125: invokevirtual 117	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   128: pop
    //   129: aload 17
    //   131: ldc 119
    //   133: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   136: pop
    //   137: aload 17
    //   139: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   142: astore 8
    //   144: aconst_null
    //   145: astore 9
    //   147: aload 16
    //   149: iconst_0
    //   150: aload 8
    //   152: aastore
    //   153: iload_2
    //   154: ifeq +9 -> 163
    //   157: aconst_null
    //   158: astore 20
    //   160: goto +11 -> 171
    //   163: ldc -43
    //   165: astore 16
    //   167: aload 16
    //   169: astore 20
    //   171: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   174: lstore 21
    //   176: aload_3
    //   177: getfield 29	com/truecaller/data/access/s:a	Landroid/content/ContentResolver;
    //   180: astore 23
    //   182: invokestatic 216	com/truecaller/content/TruecallerContract$ah:a	()Landroid/net/Uri;
    //   185: astore 24
    //   187: iconst_1
    //   188: anewarray 93	java/lang/String
    //   191: dup
    //   192: iconst_0
    //   193: ldc -38
    //   195: aastore
    //   196: astore 25
    //   198: aload 23
    //   200: aload 24
    //   202: aload 25
    //   204: aload 20
    //   206: aconst_null
    //   207: aconst_null
    //   208: invokevirtual 221	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   211: astore 10
    //   213: aload 10
    //   215: astore 17
    //   217: aload 10
    //   219: checkcast 126	java/io/Closeable
    //   222: astore 17
    //   224: new 128	java/util/ArrayList
    //   227: astore 23
    //   229: aload 23
    //   231: invokespecial 129	java/util/ArrayList:<init>	()V
    //   234: aload 23
    //   236: checkcast 131	java/util/Collection
    //   239: astore 23
    //   241: aload 10
    //   243: invokeinterface 135 1 0
    //   248: istore 26
    //   250: iload 26
    //   252: ifeq +33 -> 285
    //   255: aload 10
    //   257: iconst_0
    //   258: invokeinterface 225 2 0
    //   263: lstore 27
    //   265: lload 27
    //   267: invokestatic 230	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   270: astore 24
    //   272: aload 23
    //   274: aload 24
    //   276: invokeinterface 149 2 0
    //   281: pop
    //   282: goto -41 -> 241
    //   285: aload 23
    //   287: astore 10
    //   289: aload 23
    //   291: checkcast 151	java/util/List
    //   294: astore 10
    //   296: aload 17
    //   298: aconst_null
    //   299: invokestatic 156	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   302: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   305: lload 21
    //   307: lsub
    //   308: lstore 29
    //   310: iload 15
    //   312: anewarray 93	java/lang/String
    //   315: astore 16
    //   317: new 54	java/lang/StringBuilder
    //   320: astore 8
    //   322: aload 8
    //   324: ldc -24
    //   326: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   329: invokestatic 216	com/truecaller/content/TruecallerContract$ah:a	()Landroid/net/Uri;
    //   332: astore 17
    //   334: aload 8
    //   336: aload 17
    //   338: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   341: pop
    //   342: aload 8
    //   344: iload 19
    //   346: invokevirtual 178	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   349: pop
    //   350: aload 10
    //   352: astore 17
    //   354: aload 10
    //   356: checkcast 131	java/util/Collection
    //   359: astore 17
    //   361: aload 17
    //   363: invokeinterface 233 1 0
    //   368: istore 19
    //   370: aload 8
    //   372: iload 19
    //   374: invokevirtual 188	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   377: pop
    //   378: aload 8
    //   380: ldc -45
    //   382: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   385: pop
    //   386: aload 8
    //   388: lload 29
    //   390: invokevirtual 117	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   393: pop
    //   394: ldc 119
    //   396: astore 18
    //   398: aload 8
    //   400: aload 18
    //   402: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   405: pop
    //   406: aload 8
    //   408: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   411: astore 8
    //   413: aload 16
    //   415: iconst_0
    //   416: aload 8
    //   418: aastore
    //   419: aload 17
    //   421: invokeinterface 236 1 0
    //   426: iload 15
    //   428: ixor
    //   429: istore 31
    //   431: iload 31
    //   433: ifeq +6 -> 439
    //   436: goto +9 -> 445
    //   439: iconst_0
    //   440: istore 32
    //   442: aconst_null
    //   443: astore 10
    //   445: aload 10
    //   447: ifnull +251 -> 698
    //   450: aload 10
    //   452: checkcast 158	java/lang/Iterable
    //   455: astore 10
    //   457: bipush 100
    //   459: istore 31
    //   461: aload 10
    //   463: iload 31
    //   465: invokestatic 240	c/a/m:e	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   468: astore 8
    //   470: aload 8
    //   472: ifnull +226 -> 698
    //   475: aload 8
    //   477: checkcast 158	java/lang/Iterable
    //   480: invokeinterface 244 1 0
    //   485: astore 8
    //   487: aload 8
    //   489: invokeinterface 249 1 0
    //   494: istore 32
    //   496: iload 32
    //   498: ifeq +199 -> 697
    //   501: aload 8
    //   503: invokeinterface 253 1 0
    //   508: checkcast 151	java/util/List
    //   511: astore 10
    //   513: getstatic 259	com/truecaller/service/RefreshT9MappingService:j	Lcom/truecaller/service/RefreshT9MappingService$a;
    //   516: astore 17
    //   518: aload 4
    //   520: ldc_w 261
    //   523: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   526: aload 10
    //   528: ldc_w 263
    //   531: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   534: new 265	android/content/Intent
    //   537: astore 17
    //   539: aload 17
    //   541: aload 4
    //   543: ldc -1
    //   545: invokespecial 268	android/content/Intent:<init>	(Landroid/content/Context;Ljava/lang/Class;)V
    //   548: aload 10
    //   550: astore 18
    //   552: aload 10
    //   554: checkcast 131	java/util/Collection
    //   557: astore 18
    //   559: aload 18
    //   561: invokeinterface 236 1 0
    //   566: iload 15
    //   568: ixor
    //   569: istore 33
    //   571: iload 33
    //   573: ifeq +25 -> 598
    //   576: aload 10
    //   578: invokeinterface 185 1 0
    //   583: istore 32
    //   585: iload 32
    //   587: iload 31
    //   589: if_icmpgt +9 -> 598
    //   592: iconst_1
    //   593: istore 32
    //   595: goto +9 -> 604
    //   598: iconst_0
    //   599: istore 32
    //   601: aconst_null
    //   602: astore 10
    //   604: iload 32
    //   606: ifeq +6 -> 612
    //   609: goto +6 -> 615
    //   612: aconst_null
    //   613: astore 17
    //   615: aload 17
    //   617: ifnull -130 -> 487
    //   620: aload 17
    //   622: ldc_w 270
    //   625: invokevirtual 274	android/content/Intent:setAction	(Ljava/lang/String;)Landroid/content/Intent;
    //   628: astore 10
    //   630: aload 10
    //   632: ifnull -145 -> 487
    //   635: ldc_w 276
    //   638: astore 17
    //   640: aload 18
    //   642: invokestatic 280	c/a/m:c	(Ljava/util/Collection;)[J
    //   645: astore 18
    //   647: aload 10
    //   649: aload 17
    //   651: aload 18
    //   653: invokevirtual 284	android/content/Intent:putExtra	(Ljava/lang/String;[J)Landroid/content/Intent;
    //   656: astore 10
    //   658: aload 10
    //   660: ifnull -173 -> 487
    //   663: aload_1
    //   664: invokevirtual 290	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   667: astore 17
    //   669: ldc -1
    //   671: astore 18
    //   673: ldc_w 291
    //   676: istore 33
    //   678: aload 17
    //   680: aload 18
    //   682: iload 33
    //   684: aload 10
    //   686: invokestatic 297	android/support/v4/app/v:a	(Landroid/content/Context;Ljava/lang/Class;ILandroid/content/Intent;)V
    //   689: getstatic 302	c/x:a	Lc/x;
    //   692: astore 10
    //   694: goto -207 -> 487
    //   697: return
    //   698: return
    //   699: astore 4
    //   701: goto +12 -> 713
    //   704: astore 4
    //   706: aload 4
    //   708: astore 11
    //   710: aload 4
    //   712: athrow
    //   713: aload 17
    //   715: aload 11
    //   717: invokestatic 156	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   720: aload 4
    //   722: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	723	0	this	s
    //   0	723	1	paramContext	android.content.Context
    //   0	723	2	paramBoolean	boolean
    //   1	176	3	locals	s
    //   3	539	4	localContext	android.content.Context
    //   699	1	4	localObject1	Object
    //   704	17	4	localObject2	Object
    //   7	4	5	str	String
    //   18	36	6	l1	long
    //   24	478	8	localObject3	Object
    //   29	117	9	localUri	Uri
    //   33	660	10	localObject4	Object
    //   36	680	11	localObject5	Object
    //   48	60	12	i	int
    //   56	68	13	l2	long
    //   59	510	15	j	int
    //   66	348	16	localObject6	Object
    //   71	643	17	localObject7	Object
    //   83	598	18	localObject8	Object
    //   95	250	19	c	char
    //   368	5	19	k	int
    //   158	47	20	localObject9	Object
    //   174	132	21	l3	long
    //   180	110	23	localObject10	Object
    //   185	90	24	localObject11	Object
    //   196	7	25	arrayOfString	String[]
    //   248	3	26	bool1	boolean
    //   263	3	27	l4	long
    //   308	81	29	l5	long
    //   429	3	31	bool2	boolean
    //   459	131	31	m	int
    //   440	57	32	bool3	boolean
    //   583	22	32	n	int
    //   569	3	33	bool4	boolean
    //   676	7	33	i1	int
    // Exception table:
    //   from	to	target	type
    //   710	713	699	finally
    //   224	227	704	finally
    //   229	234	704	finally
    //   234	239	704	finally
    //   241	248	704	finally
    //   257	263	704	finally
    //   265	270	704	finally
    //   274	282	704	finally
    //   289	294	704	finally
  }
  
  /* Error */
  public final void a(long[] paramArrayOfLong, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_3
    //   2: aload_2
    //   3: astore 4
    //   5: aload_1
    //   6: astore 5
    //   8: aload_1
    //   9: ldc_w 304
    //   12: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   15: aload_2
    //   16: ldc_w 306
    //   19: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   22: aload_0
    //   23: getfield 31	com/truecaller/data/access/s:b	Lcom/truecaller/data/access/T9DaoHelper;
    //   26: astore 6
    //   28: new 308	com/truecaller/data/access/r
    //   31: astore 7
    //   33: aload 7
    //   35: aload_2
    //   36: invokespecial 309	com/truecaller/data/access/r:<init>	(Ljava/lang/String;)V
    //   39: aload 7
    //   41: checkcast 311	com/truecaller/data/access/q
    //   44: astore 7
    //   46: aload 7
    //   48: ldc_w 313
    //   51: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   54: aload 6
    //   56: aload 7
    //   58: putfield 316	com/truecaller/data/access/T9DaoHelper:a	Lcom/truecaller/data/access/q;
    //   61: invokestatic 200	com/truecaller/content/TruecallerContract$am:a	()Landroid/net/Uri;
    //   64: astore 4
    //   66: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   69: lstore 8
    //   71: aload_1
    //   72: invokestatic 321	c/a/f:b	([J)Ljava/util/List;
    //   75: astore 6
    //   77: aload 6
    //   79: astore 10
    //   81: aload 6
    //   83: checkcast 158	java/lang/Iterable
    //   86: astore 10
    //   88: ldc_w 323
    //   91: checkcast 325	java/lang/CharSequence
    //   94: astore 11
    //   96: ldc_w 327
    //   99: checkcast 325	java/lang/CharSequence
    //   102: astore 12
    //   104: bipush 57
    //   106: istore 13
    //   108: aload 10
    //   110: aconst_null
    //   111: aload 11
    //   113: aload 12
    //   115: iconst_0
    //   116: aconst_null
    //   117: aconst_null
    //   118: iload 13
    //   120: invokestatic 331	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   123: astore 6
    //   125: aload 6
    //   127: invokestatic 336	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   130: astore 14
    //   132: ldc_w 333
    //   135: aload 14
    //   137: invokevirtual 340	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   140: astore 10
    //   142: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   145: lstore 15
    //   147: aload_0
    //   148: getfield 29	com/truecaller/data/access/s:a	Landroid/content/ContentResolver;
    //   151: astore 12
    //   153: iconst_0
    //   154: istore 17
    //   156: aconst_null
    //   157: astore 18
    //   159: aload 12
    //   161: aload 4
    //   163: aload 10
    //   165: aconst_null
    //   166: invokevirtual 206	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   169: istore 19
    //   171: iload 19
    //   173: invokestatic 112	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   176: astore 12
    //   178: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   181: lload 15
    //   183: lsub
    //   184: lstore 20
    //   186: aload 12
    //   188: checkcast 342	java/lang/Number
    //   191: invokevirtual 345	java/lang/Number:intValue	()I
    //   194: istore 22
    //   196: iconst_1
    //   197: istore 23
    //   199: iload 23
    //   201: anewarray 93	java/lang/String
    //   204: astore 12
    //   206: new 54	java/lang/StringBuilder
    //   209: astore 24
    //   211: ldc_w 347
    //   214: astore 25
    //   216: aload 24
    //   218: aload 25
    //   220: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   223: aload 24
    //   225: aload 4
    //   227: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   230: pop
    //   231: bipush 32
    //   233: istore 26
    //   235: aload 24
    //   237: iload 26
    //   239: invokevirtual 178	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   242: pop
    //   243: aload 24
    //   245: iload 22
    //   247: invokevirtual 188	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   250: pop
    //   251: aload 24
    //   253: ldc -45
    //   255: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   258: pop
    //   259: aload 24
    //   261: lload 20
    //   263: invokevirtual 117	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   266: pop
    //   267: aload 24
    //   269: ldc_w 349
    //   272: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   275: pop
    //   276: aload 24
    //   278: aload 10
    //   280: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   283: pop
    //   284: aload 24
    //   286: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   289: astore 10
    //   291: iconst_0
    //   292: istore 22
    //   294: aconst_null
    //   295: astore 14
    //   297: aload 12
    //   299: iconst_0
    //   300: aload 10
    //   302: aastore
    //   303: iconst_2
    //   304: istore 27
    //   306: iload 27
    //   308: anewarray 352	c/s
    //   311: astore 10
    //   313: new 352	c/s
    //   316: astore 12
    //   318: invokestatic 216	com/truecaller/content/TruecallerContract$ah:a	()Landroid/net/Uri;
    //   321: astore 28
    //   323: aload 6
    //   325: invokestatic 336	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   328: astore 24
    //   330: ldc_w 354
    //   333: aload 24
    //   335: invokevirtual 340	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   338: astore 29
    //   340: getstatic 359	com/truecaller/data/access/s$a:a	Lcom/truecaller/data/access/s$a;
    //   343: astore 24
    //   345: aload 12
    //   347: aload 28
    //   349: aload 29
    //   351: aload 24
    //   353: invokespecial 362	c/s:<init>	(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   356: aload 10
    //   358: iconst_0
    //   359: aload 12
    //   361: aastore
    //   362: new 352	c/s
    //   365: astore 12
    //   367: invokestatic 365	com/truecaller/content/TruecallerContract$k:a	()Landroid/net/Uri;
    //   370: astore 28
    //   372: aload 6
    //   374: invokestatic 336	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   377: astore 6
    //   379: ldc_w 367
    //   382: aload 6
    //   384: invokevirtual 340	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   387: astore 6
    //   389: getstatic 372	com/truecaller/data/access/s$b:a	Lcom/truecaller/data/access/s$b;
    //   392: astore 29
    //   394: aload 12
    //   396: aload 28
    //   398: aload 6
    //   400: aload 29
    //   402: invokespecial 362	c/s:<init>	(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   405: aload 10
    //   407: iload 23
    //   409: aload 12
    //   411: aastore
    //   412: aload 10
    //   414: invokestatic 375	c/a/m:b	([Ljava/lang/Object;)Ljava/util/List;
    //   417: checkcast 158	java/lang/Iterable
    //   420: astore 6
    //   422: new 128	java/util/ArrayList
    //   425: astore 10
    //   427: aload 10
    //   429: invokespecial 129	java/util/ArrayList:<init>	()V
    //   432: aload 10
    //   434: checkcast 131	java/util/Collection
    //   437: astore 10
    //   439: aload 6
    //   441: invokeinterface 244 1 0
    //   446: astore 6
    //   448: aload 6
    //   450: invokeinterface 249 1 0
    //   455: istore 19
    //   457: iload 19
    //   459: ifeq +358 -> 817
    //   462: aload 6
    //   464: invokeinterface 253 1 0
    //   469: checkcast 352	c/s
    //   472: astore 12
    //   474: aload 12
    //   476: getfield 378	c/s:a	Ljava/lang/Object;
    //   479: checkcast 46	android/net/Uri
    //   482: astore 28
    //   484: aload 12
    //   486: getfield 380	c/s:b	Ljava/lang/Object;
    //   489: astore 29
    //   491: aload 29
    //   493: astore 30
    //   495: aload 29
    //   497: checkcast 93	java/lang/String
    //   500: astore 30
    //   502: aload 12
    //   504: getfield 382	c/s:c	Ljava/lang/Object;
    //   507: checkcast 384	c/g/a/b
    //   510: astore 12
    //   512: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   515: lstore 31
    //   517: aload_3
    //   518: getfield 29	com/truecaller/data/access/s:a	Landroid/content/ContentResolver;
    //   521: astore 29
    //   523: iconst_0
    //   524: istore 26
    //   526: aconst_null
    //   527: astore 25
    //   529: aload 28
    //   531: astore 24
    //   533: aload 30
    //   535: astore_2
    //   536: aload 29
    //   538: aload 28
    //   540: aconst_null
    //   541: aload 30
    //   543: aconst_null
    //   544: aconst_null
    //   545: invokevirtual 221	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   548: astore 29
    //   550: aload 29
    //   552: ifnull +109 -> 661
    //   555: aload 29
    //   557: astore 24
    //   559: aload 29
    //   561: checkcast 126	java/io/Closeable
    //   564: astore 24
    //   566: new 128	java/util/ArrayList
    //   569: astore 25
    //   571: aload 25
    //   573: invokespecial 129	java/util/ArrayList:<init>	()V
    //   576: aload 25
    //   578: checkcast 131	java/util/Collection
    //   581: astore 25
    //   583: aload 29
    //   585: invokeinterface 135 1 0
    //   590: istore 33
    //   592: iload 33
    //   594: ifeq +27 -> 621
    //   597: aload 12
    //   599: aload 29
    //   601: invokeinterface 388 2 0
    //   606: astore 30
    //   608: aload 25
    //   610: aload 30
    //   612: invokeinterface 149 2 0
    //   617: pop
    //   618: goto -35 -> 583
    //   621: aload 25
    //   623: checkcast 151	java/util/List
    //   626: astore 25
    //   628: aload 24
    //   630: aconst_null
    //   631: invokestatic 156	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   634: goto +43 -> 677
    //   637: astore 4
    //   639: goto +12 -> 651
    //   642: astore 4
    //   644: aload 4
    //   646: astore 18
    //   648: aload 4
    //   650: athrow
    //   651: aload 24
    //   653: aload 18
    //   655: invokestatic 156	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   658: aload 4
    //   660: athrow
    //   661: getstatic 393	c/a/y:a	Lc/a/y;
    //   664: astore 12
    //   666: aload 12
    //   668: astore 25
    //   670: aload 12
    //   672: checkcast 151	java/util/List
    //   675: astore 25
    //   677: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   680: lload 31
    //   682: lsub
    //   683: lstore 34
    //   685: iload 23
    //   687: anewarray 93	java/lang/String
    //   690: astore 12
    //   692: new 54	java/lang/StringBuilder
    //   695: astore 30
    //   697: aload 30
    //   699: ldc_w 395
    //   702: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   705: aload 30
    //   707: aload 28
    //   709: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   712: pop
    //   713: aload 30
    //   715: ldc 100
    //   717: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   720: pop
    //   721: aload 25
    //   723: astore 18
    //   725: aload 25
    //   727: checkcast 131	java/util/Collection
    //   730: invokeinterface 233 1 0
    //   735: istore 17
    //   737: aload 30
    //   739: iload 17
    //   741: invokevirtual 188	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   744: pop
    //   745: aload 30
    //   747: ldc -45
    //   749: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   752: pop
    //   753: aload 30
    //   755: lload 34
    //   757: invokevirtual 117	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   760: pop
    //   761: aload 30
    //   763: ldc_w 397
    //   766: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   769: pop
    //   770: aload_2
    //   771: astore 29
    //   773: aload 30
    //   775: aload_2
    //   776: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   779: pop
    //   780: aload 30
    //   782: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   785: astore 18
    //   787: aload 12
    //   789: iconst_0
    //   790: aload 18
    //   792: aastore
    //   793: aload 25
    //   795: checkcast 158	java/lang/Iterable
    //   798: astore 25
    //   800: aload 10
    //   802: aload 25
    //   804: invokestatic 400	c/a/m:a	(Ljava/util/Collection;Ljava/lang/Iterable;)Z
    //   807: pop
    //   808: iconst_0
    //   809: istore 17
    //   811: aconst_null
    //   812: astore 18
    //   814: goto -366 -> 448
    //   817: aload 10
    //   819: checkcast 151	java/util/List
    //   822: checkcast 158	java/lang/Iterable
    //   825: invokestatic 164	c/a/m:e	(Ljava/lang/Iterable;)Ljava/util/List;
    //   828: checkcast 158	java/lang/Iterable
    //   831: astore 6
    //   833: new 128	java/util/ArrayList
    //   836: astore 10
    //   838: aload 10
    //   840: invokespecial 129	java/util/ArrayList:<init>	()V
    //   843: aload 10
    //   845: checkcast 131	java/util/Collection
    //   848: astore 10
    //   850: aload 6
    //   852: invokeinterface 244 1 0
    //   857: astore 6
    //   859: aload 6
    //   861: invokeinterface 249 1 0
    //   866: istore 19
    //   868: iload 19
    //   870: ifeq +44 -> 914
    //   873: aload 6
    //   875: invokeinterface 253 1 0
    //   880: checkcast 352	c/s
    //   883: astore 12
    //   885: aload_3
    //   886: getfield 31	com/truecaller/data/access/s:b	Lcom/truecaller/data/access/T9DaoHelper;
    //   889: astore 18
    //   891: aload 18
    //   893: aload 12
    //   895: invokevirtual 403	com/truecaller/data/access/T9DaoHelper:a	(Lc/s;)Ljava/util/List;
    //   898: checkcast 158	java/lang/Iterable
    //   901: astore 12
    //   903: aload 10
    //   905: aload 12
    //   907: invokestatic 400	c/a/m:a	(Ljava/util/Collection;Ljava/lang/Iterable;)Z
    //   910: pop
    //   911: goto -52 -> 859
    //   914: aload 10
    //   916: checkcast 151	java/util/List
    //   919: checkcast 158	java/lang/Iterable
    //   922: astore 10
    //   924: new 128	java/util/ArrayList
    //   927: astore 6
    //   929: aload 10
    //   931: bipush 10
    //   933: invokestatic 407	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   936: istore 19
    //   938: aload 6
    //   940: iload 19
    //   942: invokespecial 410	java/util/ArrayList:<init>	(I)V
    //   945: aload 6
    //   947: checkcast 131	java/util/Collection
    //   950: astore 6
    //   952: aload 10
    //   954: invokeinterface 244 1 0
    //   959: astore 10
    //   961: aload 10
    //   963: invokeinterface 249 1 0
    //   968: istore 19
    //   970: iload 19
    //   972: ifeq +52 -> 1024
    //   975: aload 10
    //   977: invokeinterface 253 1 0
    //   982: checkcast 412	android/content/ContentValues
    //   985: astore 12
    //   987: aload 4
    //   989: invokestatic 418	android/content/ContentProviderOperation:newInsert	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   992: astore 18
    //   994: aload 18
    //   996: aload 12
    //   998: invokevirtual 424	android/content/ContentProviderOperation$Builder:withValues	(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;
    //   1001: iload 23
    //   1003: invokevirtual 428	android/content/ContentProviderOperation$Builder:withYieldAllowed	(Z)Landroid/content/ContentProviderOperation$Builder;
    //   1006: invokevirtual 431	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   1009: astore 12
    //   1011: aload 6
    //   1013: aload 12
    //   1015: invokeinterface 149 2 0
    //   1020: pop
    //   1021: goto -60 -> 961
    //   1024: aload 6
    //   1026: checkcast 151	java/util/List
    //   1029: astore 6
    //   1031: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   1034: lstore 36
    //   1036: aload_3
    //   1037: getfield 29	com/truecaller/data/access/s:a	Landroid/content/ContentResolver;
    //   1040: astore 4
    //   1042: invokestatic 435	com/truecaller/content/TruecallerContract:a	()Ljava/lang/String;
    //   1045: astore 10
    //   1047: new 128	java/util/ArrayList
    //   1050: astore 28
    //   1052: aload 6
    //   1054: astore 29
    //   1056: aload 6
    //   1058: checkcast 131	java/util/Collection
    //   1061: astore 29
    //   1063: aload 28
    //   1065: aload 29
    //   1067: invokespecial 438	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
    //   1070: aload 4
    //   1072: aload 10
    //   1074: aload 28
    //   1076: invokevirtual 442	android/content/ContentResolver:applyBatch	(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   1079: pop
    //   1080: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   1083: lload 36
    //   1085: lsub
    //   1086: lstore 20
    //   1088: iload 23
    //   1090: anewarray 93	java/lang/String
    //   1093: astore 4
    //   1095: new 54	java/lang/StringBuilder
    //   1098: astore 10
    //   1100: aload 10
    //   1102: ldc_w 444
    //   1105: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1108: aload 6
    //   1110: invokeinterface 185 1 0
    //   1115: istore 38
    //   1117: aload 10
    //   1119: iload 38
    //   1121: invokevirtual 188	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1124: pop
    //   1125: aload 10
    //   1127: ldc_w 446
    //   1130: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1133: pop
    //   1134: aload 10
    //   1136: lload 20
    //   1138: invokevirtual 117	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1141: pop
    //   1142: aload 10
    //   1144: ldc 119
    //   1146: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1149: pop
    //   1150: aload 10
    //   1152: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1155: astore 6
    //   1157: aload 4
    //   1159: iconst_0
    //   1160: aload 6
    //   1162: aastore
    //   1163: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   1166: lload 8
    //   1168: lsub
    //   1169: lstore 36
    //   1171: iload 23
    //   1173: anewarray 93	java/lang/String
    //   1176: astore 4
    //   1178: new 54	java/lang/StringBuilder
    //   1181: astore 6
    //   1183: aload 6
    //   1185: ldc_w 448
    //   1188: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1191: aload_1
    //   1192: invokestatic 453	java/util/Arrays:toString	([J)Ljava/lang/String;
    //   1195: astore 5
    //   1197: aload 6
    //   1199: aload 5
    //   1201: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1204: pop
    //   1205: aload 6
    //   1207: ldc_w 455
    //   1210: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1213: pop
    //   1214: aload 6
    //   1216: lload 36
    //   1218: invokevirtual 117	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1221: pop
    //   1222: aload 6
    //   1224: ldc 119
    //   1226: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1229: pop
    //   1230: aload 6
    //   1232: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1235: astore 6
    //   1237: aload 4
    //   1239: iconst_0
    //   1240: aload 6
    //   1242: aastore
    //   1243: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1244	0	this	s
    //   0	1244	1	paramArrayOfLong	long[]
    //   0	1244	2	paramString	String
    //   1	1036	3	locals	s
    //   3	223	4	localObject1	Object
    //   637	1	4	localObject2	Object
    //   642	346	4	localUri	Uri
    //   1040	198	4	localObject3	Object
    //   6	1194	5	localObject4	Object
    //   26	1215	6	localObject5	Object
    //   31	26	7	localObject6	Object
    //   69	1098	8	l1	long
    //   79	1072	10	localObject7	Object
    //   94	18	11	localCharSequence	CharSequence
    //   102	912	12	localObject8	Object
    //   106	13	13	i	int
    //   130	166	14	str	String
    //   145	37	15	l2	long
    //   154	656	17	j	int
    //   157	838	18	localObject9	Object
    //   169	3	19	k	int
    //   455	414	19	bool1	boolean
    //   936	5	19	m	int
    //   968	3	19	bool2	boolean
    //   184	953	20	l3	long
    //   194	99	22	n	int
    //   197	975	23	bool3	boolean
    //   209	443	24	localObject10	Object
    //   214	589	25	localObject11	Object
    //   233	292	26	c	char
    //   304	3	27	i1	int
    //   321	754	28	localObject12	Object
    //   338	728	29	localObject13	Object
    //   493	288	30	localObject14	Object
    //   515	166	31	l4	long
    //   590	3	33	bool4	boolean
    //   683	73	34	l5	long
    //   1034	183	36	l6	long
    //   1115	5	38	i2	int
    // Exception table:
    //   from	to	target	type
    //   648	651	637	finally
    //   566	569	642	finally
    //   571	576	642	finally
    //   576	581	642	finally
    //   583	590	642	finally
    //   599	606	642	finally
    //   610	618	642	finally
    //   621	626	642	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */