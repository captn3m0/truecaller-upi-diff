package com.truecaller.data.access;

import android.database.Cursor;

abstract class a
{
  static int a(Cursor paramCursor, String... paramVarArgs)
  {
    int i = 0;
    int k;
    for (;;)
    {
      int j = paramVarArgs.length;
      k = -1;
      if (i >= j) {
        break;
      }
      String str = paramVarArgs[i];
      j = paramCursor.getColumnIndex(str);
      if (j != k) {
        return j;
      }
      i += 1;
    }
    return k;
  }
  
  static Integer a(Cursor paramCursor, int paramInt)
  {
    int i = -1;
    if (paramInt != i)
    {
      boolean bool = paramCursor.isNull(paramInt);
      if (!bool) {
        return Integer.valueOf(paramCursor.getInt(paramInt));
      }
    }
    return null;
  }
  
  static Long b(Cursor paramCursor, int paramInt)
  {
    int i = -1;
    if (paramInt != i)
    {
      boolean bool = paramCursor.isNull(paramInt);
      if (!bool) {
        return Long.valueOf(paramCursor.getLong(paramInt));
      }
    }
    return null;
  }
  
  static int c(Cursor paramCursor, int paramInt)
  {
    int i = -1;
    if (paramInt != i)
    {
      boolean bool = paramCursor.isNull(paramInt);
      if (!bool) {
        return paramCursor.getInt(paramInt);
      }
    }
    return 0;
  }
  
  static String d(Cursor paramCursor, int paramInt)
  {
    int i = -1;
    if (paramInt != i)
    {
      boolean bool = paramCursor.isNull(paramInt);
      if (!bool) {
        return paramCursor.getString(paramInt);
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */