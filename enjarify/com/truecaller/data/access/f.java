package com.truecaller.data.access;

import android.content.ContentResolver;
import com.truecaller.service.RefreshContactIndexingService.a;

public final class f
{
  public final ContentResolver a;
  private final RefreshContactIndexingService.a b;
  
  public f(ContentResolver paramContentResolver, RefreshContactIndexingService.a parama)
  {
    a = paramContentResolver;
    b = parama;
  }
  
  /* Error */
  public final void a(boolean paramBoolean)
  {
    // Byte code:
    //   0: invokestatic 30	com/truecaller/content/TruecallerContract$a:a	()Landroid/net/Uri;
    //   3: astore_2
    //   4: invokestatic 33	com/truecaller/content/TruecallerContract$f:a	()Landroid/net/Uri;
    //   7: astore_3
    //   8: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   11: lstore 4
    //   13: aload_0
    //   14: getfield 23	com/truecaller/data/access/f:a	Landroid/content/ContentResolver;
    //   17: astore 6
    //   19: ldc 41
    //   21: astore 7
    //   23: aconst_null
    //   24: astore 8
    //   26: aload 6
    //   28: aload_3
    //   29: aload 7
    //   31: aconst_null
    //   32: invokevirtual 47	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   35: istore 9
    //   37: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   40: lload 4
    //   42: lsub
    //   43: lstore 10
    //   45: iconst_1
    //   46: istore 12
    //   48: iload 12
    //   50: anewarray 50	java/lang/String
    //   53: astore 13
    //   55: new 52	java/lang/StringBuilder
    //   58: astore 14
    //   60: ldc 54
    //   62: astore 15
    //   64: aload 14
    //   66: aload 15
    //   68: invokespecial 57	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   71: aload 14
    //   73: aload_3
    //   74: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   77: pop
    //   78: bipush 32
    //   80: istore 16
    //   82: aload 14
    //   84: iload 16
    //   86: invokevirtual 65	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: aload 14
    //   92: iload 9
    //   94: invokevirtual 68	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   97: pop
    //   98: aload 14
    //   100: ldc 70
    //   102: invokevirtual 73	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload 14
    //   108: lload 10
    //   110: invokevirtual 76	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   113: pop
    //   114: aload 14
    //   116: ldc 78
    //   118: invokevirtual 73	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   121: pop
    //   122: aload 14
    //   124: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   127: astore_3
    //   128: aload 13
    //   130: iconst_0
    //   131: aload_3
    //   132: aastore
    //   133: iload_1
    //   134: ifeq +12 -> 146
    //   137: iconst_0
    //   138: istore 9
    //   140: aconst_null
    //   141: astore 6
    //   143: goto +11 -> 154
    //   146: ldc 84
    //   148: astore 17
    //   150: aload 17
    //   152: astore 6
    //   154: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   157: lstore 18
    //   159: aload_0
    //   160: getfield 23	com/truecaller/data/access/f:a	Landroid/content/ContentResolver;
    //   163: astore_3
    //   164: iconst_1
    //   165: anewarray 50	java/lang/String
    //   168: dup
    //   169: iconst_0
    //   170: ldc 86
    //   172: aastore
    //   173: astore 14
    //   175: iconst_0
    //   176: istore 20
    //   178: aconst_null
    //   179: astore 7
    //   181: iconst_0
    //   182: istore 21
    //   184: aconst_null
    //   185: astore 22
    //   187: aload_2
    //   188: astore 13
    //   190: aload_3
    //   191: aload_2
    //   192: aload 14
    //   194: aload 6
    //   196: aconst_null
    //   197: aconst_null
    //   198: invokevirtual 90	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   201: astore 17
    //   203: aload 17
    //   205: ifnull +115 -> 320
    //   208: aload 17
    //   210: astore_3
    //   211: aload 17
    //   213: checkcast 92	java/io/Closeable
    //   216: astore_3
    //   217: new 94	java/util/ArrayList
    //   220: astore 13
    //   222: aload 13
    //   224: invokespecial 95	java/util/ArrayList:<init>	()V
    //   227: aload 13
    //   229: checkcast 97	java/util/Collection
    //   232: astore 13
    //   234: aload 17
    //   236: invokeinterface 103 1 0
    //   241: istore 23
    //   243: iload 23
    //   245: ifeq +33 -> 278
    //   248: aload 17
    //   250: iconst_0
    //   251: invokeinterface 107 2 0
    //   256: lstore 24
    //   258: lload 24
    //   260: invokestatic 113	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   263: astore 14
    //   265: aload 13
    //   267: aload 14
    //   269: invokeinterface 117 2 0
    //   274: pop
    //   275: goto -41 -> 234
    //   278: aload 13
    //   280: checkcast 119	java/util/List
    //   283: astore 13
    //   285: aload_3
    //   286: aconst_null
    //   287: invokestatic 124	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   290: aload 13
    //   292: astore 17
    //   294: goto +34 -> 328
    //   297: astore 17
    //   299: goto +12 -> 311
    //   302: astore 17
    //   304: aload 17
    //   306: astore 8
    //   308: aload 17
    //   310: athrow
    //   311: aload_3
    //   312: aload 8
    //   314: invokestatic 124	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   317: aload 17
    //   319: athrow
    //   320: getstatic 129	c/a/y:a	Lc/a/y;
    //   323: checkcast 119	java/util/List
    //   326: astore 17
    //   328: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   331: lload 18
    //   333: lsub
    //   334: lstore 26
    //   336: iload 12
    //   338: anewarray 50	java/lang/String
    //   341: astore 14
    //   343: new 52	java/lang/StringBuilder
    //   346: astore 6
    //   348: aload 6
    //   350: ldc -125
    //   352: invokespecial 57	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   355: aload 6
    //   357: aload_2
    //   358: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   361: pop
    //   362: aload 6
    //   364: iload 16
    //   366: invokevirtual 65	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   369: pop
    //   370: aload 17
    //   372: astore 7
    //   374: aload 17
    //   376: checkcast 97	java/util/Collection
    //   379: astore 7
    //   381: aload 7
    //   383: invokeinterface 135 1 0
    //   388: istore 21
    //   390: aload 6
    //   392: iload 21
    //   394: invokevirtual 68	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   397: pop
    //   398: ldc 70
    //   400: astore 22
    //   402: aload 6
    //   404: aload 22
    //   406: invokevirtual 73	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   409: pop
    //   410: aload 6
    //   412: lload 26
    //   414: invokevirtual 76	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   417: pop
    //   418: aload 6
    //   420: ldc 78
    //   422: invokevirtual 73	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   425: pop
    //   426: aload 6
    //   428: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   431: astore_3
    //   432: aload 14
    //   434: iconst_0
    //   435: aload_3
    //   436: aastore
    //   437: aload 7
    //   439: invokeinterface 138 1 0
    //   444: iload 12
    //   446: ixor
    //   447: istore 28
    //   449: iload 28
    //   451: ifeq +6 -> 457
    //   454: goto +8 -> 462
    //   457: iconst_0
    //   458: istore_1
    //   459: aconst_null
    //   460: astore 17
    //   462: aload 17
    //   464: ifnull +255 -> 719
    //   467: aload 17
    //   469: checkcast 140	java/lang/Iterable
    //   472: astore 17
    //   474: bipush 100
    //   476: istore 28
    //   478: aload 17
    //   480: iload 28
    //   482: invokestatic 147	c/a/m:e	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   485: astore 17
    //   487: aload 17
    //   489: ifnull +230 -> 719
    //   492: aload 17
    //   494: checkcast 140	java/lang/Iterable
    //   497: invokeinterface 151 1 0
    //   502: astore 17
    //   504: aload 17
    //   506: invokeinterface 156 1 0
    //   511: istore 29
    //   513: iload 29
    //   515: ifeq +203 -> 718
    //   518: aload 17
    //   520: invokeinterface 160 1 0
    //   525: checkcast 119	java/util/List
    //   528: astore 13
    //   530: aload_0
    //   531: getfield 25	com/truecaller/data/access/f:b	Lcom/truecaller/service/RefreshContactIndexingService$a;
    //   534: astore 14
    //   536: aload 13
    //   538: ldc -94
    //   540: invokestatic 15	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   543: new 164	android/content/Intent
    //   546: astore 6
    //   548: aload 14
    //   550: getfield 169	com/truecaller/service/RefreshContactIndexingService$a:a	Landroid/content/Context;
    //   553: astore 7
    //   555: ldc -85
    //   557: astore 22
    //   559: aload 6
    //   561: aload 7
    //   563: aload 22
    //   565: invokespecial 174	android/content/Intent:<init>	(Landroid/content/Context;Ljava/lang/Class;)V
    //   568: aload 13
    //   570: astore 7
    //   572: aload 13
    //   574: checkcast 97	java/util/Collection
    //   577: astore 7
    //   579: aload 7
    //   581: invokeinterface 138 1 0
    //   586: iload 12
    //   588: ixor
    //   589: istore 21
    //   591: iload 21
    //   593: ifeq +25 -> 618
    //   596: aload 13
    //   598: invokeinterface 175 1 0
    //   603: istore 29
    //   605: iload 29
    //   607: iload 28
    //   609: if_icmpgt +9 -> 618
    //   612: iconst_1
    //   613: istore 29
    //   615: goto +9 -> 624
    //   618: iconst_0
    //   619: istore 29
    //   621: aconst_null
    //   622: astore 13
    //   624: iload 29
    //   626: ifeq +6 -> 632
    //   629: goto +9 -> 638
    //   632: iconst_0
    //   633: istore 9
    //   635: aconst_null
    //   636: astore 6
    //   638: aload 6
    //   640: ifnull -136 -> 504
    //   643: aload 6
    //   645: ldc -79
    //   647: invokevirtual 181	android/content/Intent:setAction	(Ljava/lang/String;)Landroid/content/Intent;
    //   650: astore 13
    //   652: aload 13
    //   654: ifnull -150 -> 504
    //   657: ldc -73
    //   659: astore 6
    //   661: aload 7
    //   663: invokestatic 187	c/a/m:c	(Ljava/util/Collection;)[J
    //   666: astore 7
    //   668: aload 13
    //   670: aload 6
    //   672: aload 7
    //   674: invokevirtual 191	android/content/Intent:putExtra	(Ljava/lang/String;[J)Landroid/content/Intent;
    //   677: astore 13
    //   679: aload 13
    //   681: ifnull -177 -> 504
    //   684: aload 14
    //   686: getfield 169	com/truecaller/service/RefreshContactIndexingService$a:a	Landroid/content/Context;
    //   689: astore 14
    //   691: ldc -85
    //   693: astore 6
    //   695: ldc -64
    //   697: istore 20
    //   699: aload 14
    //   701: aload 6
    //   703: iload 20
    //   705: aload 13
    //   707: invokestatic 198	android/support/v4/app/v:a	(Landroid/content/Context;Ljava/lang/Class;ILandroid/content/Intent;)V
    //   710: getstatic 203	c/x:a	Lc/x;
    //   713: astore 13
    //   715: goto -211 -> 504
    //   718: return
    //   719: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	720	0	this	f
    //   0	720	1	paramBoolean	boolean
    //   3	355	2	localUri	android.net.Uri
    //   7	429	3	localObject1	Object
    //   11	30	4	l1	long
    //   17	685	6	localObject2	Object
    //   21	652	7	localObject3	Object
    //   24	289	8	localObject4	Object
    //   35	599	9	i	int
    //   43	66	10	l2	long
    //   46	543	12	j	int
    //   53	661	13	localObject5	Object
    //   58	642	14	localObject6	Object
    //   62	5	15	str	String
    //   80	285	16	c	char
    //   148	145	17	localObject7	Object
    //   297	1	17	localObject8	Object
    //   302	16	17	localObject9	Object
    //   326	193	17	localObject10	Object
    //   157	175	18	l3	long
    //   176	528	20	k	int
    //   182	211	21	m	int
    //   589	3	21	bool1	boolean
    //   185	379	22	localObject11	Object
    //   241	3	23	bool2	boolean
    //   256	3	24	l4	long
    //   334	79	26	l5	long
    //   447	3	28	bool3	boolean
    //   476	134	28	n	int
    //   511	3	29	bool4	boolean
    //   603	22	29	i1	int
    // Exception table:
    //   from	to	target	type
    //   308	311	297	finally
    //   217	220	302	finally
    //   222	227	302	finally
    //   227	232	302	finally
    //   234	241	302	finally
    //   250	256	302	finally
    //   258	263	302	finally
    //   267	275	302	finally
    //   278	283	302	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */