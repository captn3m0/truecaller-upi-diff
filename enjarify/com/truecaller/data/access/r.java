package com.truecaller.data.access;

import c.a.ae;
import c.g.a.b;
import c.g.b.k;
import c.k.c;
import c.k.i;
import com.truecaller.common.e.d;
import com.truecaller.utils.extensions.e;
import com.truecaller.utils.extensions.e.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class r
  implements q
{
  private final com.truecaller.search.local.b.f a;
  private final String b;
  
  public r(String paramString)
  {
    b = paramString;
    paramString = com.truecaller.search.local.b.f.a(b);
    k.a(paramString, "T9KeyMap.get(t9Lang)");
    a = paramString;
  }
  
  private static List a(List paramList, b paramb1, b paramb2)
  {
    paramList = (Iterable)paramList;
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (CharSequence)paramList.next();
      Object localObject3 = new java/util/ArrayList;
      int i = ((CharSequence)localObject2).length();
      ((ArrayList)localObject3).<init>(i);
      localObject3 = (Collection)localObject3;
      i = 0;
      Object localObject4 = null;
      Object localObject5;
      String str;
      for (;;)
      {
        int j = ((CharSequence)localObject2).length();
        if (i >= j) {
          break;
        }
        j = ((CharSequence)localObject2).charAt(i);
        localObject5 = Character.valueOf(j);
        Object localObject6 = Character.valueOf(((Character)localObject5).charValue());
        localObject6 = (Boolean)paramb1.invoke(localObject6);
        boolean bool3 = ((Boolean)localObject6).booleanValue();
        char c;
        if (!bool3)
        {
          c = '\000';
          localObject5 = null;
        }
        if (localObject5 != null)
        {
          c = ((Character)localObject5).charValue();
          try
          {
            localObject5 = Character.valueOf(c);
            localObject5 = paramb2.invoke(localObject5);
            localObject5 = (List)localObject5;
          }
          finally
          {
            c = '\000';
            localObject5 = null;
          }
          if (localObject5 != null) {}
        }
        else
        {
          str = String.valueOf(j);
          localObject5 = c.a.m.a(str);
        }
        ((Collection)localObject3).add(localObject5);
        i += 1;
      }
      localObject3 = (Iterable)localObject3;
      localObject2 = c.a.m.a("");
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject3).hasNext();
        if (!bool2) {
          break;
        }
        localObject4 = (List)((Iterator)localObject3).next();
        localObject2 = (Collection)localObject2;
        localObject4 = (Iterable)localObject4;
        str = "";
        k.b(localObject2, "receiver$0");
        k.b(localObject4, "other");
        k.b(str, "separator");
        localObject5 = new com/truecaller/utils/extensions/e$a;
        ((e.a)localObject5).<init>(str);
        localObject5 = (c.g.a.m)localObject5;
        localObject2 = e.a((Collection)localObject2, (Iterable)localObject4, (c.g.a.m)localObject5);
      }
      localObject2 = (Iterable)localObject2;
      c.a.m.a((Collection)localObject1, (Iterable)localObject2);
    }
    return (List)localObject1;
  }
  
  public final List a(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    r localr = this;
    Object localObject1 = paramString;
    k.b(paramString, "originalValue");
    String str = null;
    int i = 1;
    int j;
    boolean bool1;
    Object localObject5;
    int n;
    Object localObject6;
    CharSequence localCharSequence;
    int i3;
    Character localCharacter1;
    char c2;
    Object localObject7;
    char c3;
    Object localObject8;
    char c5;
    Object localObject9;
    char c6;
    if (paramBoolean1)
    {
      localObject2 = paramString;
      localObject2 = (CharSequence)paramString;
      j = 4;
      localObject3 = new char[j];
      Object tmp42_40 = localObject3;
      Object tmp43_42 = tmp42_40;
      Object tmp43_42 = tmp42_40;
      tmp43_42[0] = 32;
      tmp43_42[1] = 45;
      tmp43_42[2] = 64;
      tmp43_42[3] = 46;
      int k = 6;
      localObject2 = (Iterable)c.n.m.a((CharSequence)localObject2, (char[])localObject3, 0, k);
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject3 = (Collection)localObject3;
      localObject2 = ((Iterable)localObject2).iterator();
      for (;;)
      {
        bool1 = ((Iterator)localObject2).hasNext();
        if (!bool1) {
          break;
        }
        localObject4 = ((Iterator)localObject2).next();
        localObject5 = localObject4;
        localObject5 = (CharSequence)localObject4;
        boolean bool2 = c.n.m.a((CharSequence)localObject5) ^ i;
        if (bool2) {
          ((Collection)localObject3).add(localObject4);
        }
      }
      localObject3 = (List)localObject3;
      int i2 = ((List)localObject3).size();
      localObject2 = (Iterable)i.a(i2, i);
      localObject4 = new java/util/ArrayList;
      int m = c.a.m.a((Iterable)localObject2, 10);
      ((ArrayList)localObject4).<init>(m);
      localObject4 = (Collection)localObject4;
      localObject2 = ((Iterable)localObject2).iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject2).hasNext();
        if (!bool3) {
          break;
        }
        localObject5 = localObject2;
        n = ((ae)localObject2).a();
        localObject5 = c.a.m.b((List)localObject3, n);
        localObject6 = localObject5;
        localObject6 = (Iterable)localObject5;
        localCharSequence = (CharSequence)"";
        i3 = 0;
        localCharacter1 = null;
        c2 = '\000';
        localObject7 = null;
        c3 = '\000';
        localObject8 = null;
        c5 = '\000';
        localObject9 = null;
        c6 = '>';
        localObject5 = c.a.m.a((Iterable)localObject6, localCharSequence, null, null, 0, null, null, c6);
        ((Collection)localObject4).add(localObject5);
      }
      localObject4 = (List)localObject4;
    }
    else
    {
      localObject4 = c.a.m.a(paramString);
    }
    Object localObject2 = d.a;
    localObject2 = d.d();
    Object localObject3 = b;
    boolean bool4 = c.a.f.b((Object[])localObject2, localObject3);
    if (bool4)
    {
      localObject2 = (b)r.c.a;
      localObject3 = (b)r.d.a;
      localObject4 = a((List)localObject4, (b)localObject2, (b)localObject3);
    }
    localObject2 = d.a;
    localObject2 = d.e();
    localObject3 = b;
    bool4 = c.a.f.b((Object[])localObject2, localObject3);
    if (bool4)
    {
      localObject2 = (b)r.a.a;
      localObject3 = (b)r.b.a;
      localObject4 = a((List)localObject4, (b)localObject2, (b)localObject3);
    }
    Object localObject4 = (Iterable)localObject4;
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    localObject3 = ((Iterable)localObject4).iterator();
    label821:
    label838:
    int i1;
    for (;;)
    {
      bool1 = ((Iterator)localObject3).hasNext();
      n = 43;
      if (!bool1) {
        break;
      }
      localObject4 = com.truecaller.search.local.b.a.a((String)((Iterator)localObject3).next());
      k.a(localObject4, "AccentChars.foldToLowerCaseASCII(it)");
      localObject4 = (CharSequence)localObject4;
      localObject6 = new java/util/ArrayList;
      ((ArrayList)localObject6).<init>();
      localObject6 = (Collection)localObject6;
      int i4 = 0;
      localCharSequence = null;
      for (;;)
      {
        i3 = ((CharSequence)localObject4).length();
        if (i4 >= i3) {
          break;
        }
        i3 = ((CharSequence)localObject4).charAt(i4);
        localObject7 = a;
        localCharacter1 = Character.valueOf(i3);
        c3 = localCharacter1.charValue();
        c5 = '\002';
        localObject9 = new Character[c5];
        localCharacter2 = Character.valueOf(n);
        localObject9[0] = localCharacter2;
        c6 = '\'';
        localCharacter2 = Character.valueOf(c6);
        localObject9[i] = localCharacter2;
        localObject9 = c.a.m.b((Object[])localObject9);
        localObject8 = Character.valueOf(c3);
        boolean bool5 = ((List)localObject9).contains(localObject8);
        char c1;
        if (bool5)
        {
          c1 = '\000';
          localCharacter1 = null;
        }
        if (localCharacter1 != null)
        {
          c1 = localCharacter1.charValue();
          c1 = ((com.truecaller.search.local.b.f)localObject7).a(c1);
          localCharacter1 = Character.valueOf(c1);
        }
        else
        {
          c1 = '\000';
          localCharacter1 = null;
        }
        localObject7 = new c/k/c;
        char c4 = '0';
        c5 = '9';
        ((c)localObject7).<init>(c4, c5);
        if (localCharacter1 != null)
        {
          c4 = localCharacter1.charValue();
          c5 = a;
          if (c5 <= c4)
          {
            c2 = b;
            if (c4 <= c2)
            {
              c2 = '\001';
              break label821;
            }
          }
          c2 = '\000';
          localObject7 = null;
          if (c2 != 0)
          {
            c2 = '\001';
            break label838;
          }
        }
        c2 = '\000';
        localObject7 = null;
        if (c2 == 0)
        {
          c1 = '\000';
          localCharacter1 = null;
        }
        if (localCharacter1 != null) {
          ((Collection)localObject6).add(localCharacter1);
        }
        int i5;
        i4 += 1;
      }
      localObject6 = (List)localObject6;
      localObject7 = localObject6;
      localObject7 = (Iterable)localObject6;
      localObject8 = (CharSequence)"";
      c5 = '\000';
      localObject9 = null;
      c6 = '\000';
      Character localCharacter2 = null;
      int i6 = 62;
      localObject4 = c.a.m.a((Iterable)localObject7, (CharSequence)localObject8, null, null, 0, null, null, i6);
      localObject5 = localObject4;
      localObject5 = (CharSequence)localObject4;
      i1 = c.n.m.a((CharSequence)localObject5) ^ i;
      if (i1 == 0)
      {
        bool1 = false;
        localObject4 = null;
      }
      if (localObject4 != null) {
        ((Collection)localObject2).add(localObject4);
      }
    }
    localObject2 = c.a.m.k((Iterable)localObject2);
    if (paramBoolean2)
    {
      localObject3 = localObject1;
      localObject3 = (CharSequence)localObject1;
      j = ((CharSequence)localObject3).length();
      if (j > 0)
      {
        j = 1;
      }
      else
      {
        j = 0;
        localObject3 = null;
      }
      if (j != 0)
      {
        int i7 = ((String)localObject1).charAt(0);
        if (i7 == i1)
        {
          localObject1 = localObject2;
          localObject1 = (Collection)localObject2;
          boolean bool6 = ((Collection)localObject1).isEmpty() ^ i;
          if (bool6)
          {
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>("+");
            str = (String)((List)localObject2).get(0);
            ((StringBuilder)localObject1).append(str);
            localObject1 = (Collection)c.a.m.a(((StringBuilder)localObject1).toString());
            localObject2 = (Iterable)localObject2;
            return c.a.m.c((Collection)localObject1, (Iterable)localObject2);
          }
        }
      }
    }
    return (List)localObject2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */