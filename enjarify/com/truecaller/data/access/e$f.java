package com.truecaller.data.access;

import android.database.Cursor;

final class e$f
  extends e.c
{
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  private final int p;
  
  e$f(Cursor paramCursor)
  {
    super(paramCursor);
    int i1 = paramCursor.getColumnIndex("data1");
    f = i1;
    i1 = paramCursor.getColumnIndex("data2");
    g = i1;
    i1 = paramCursor.getColumnIndex("data3");
    h = i1;
    i1 = paramCursor.getColumnIndex("data4");
    i = i1;
    i1 = paramCursor.getColumnIndex("data5");
    j = i1;
    i1 = paramCursor.getColumnIndex("data6");
    k = i1;
    i1 = paramCursor.getColumnIndex("data7");
    l = i1;
    i1 = paramCursor.getColumnIndex("data8");
    m = i1;
    i1 = paramCursor.getColumnIndex("data9");
    n = i1;
    i1 = paramCursor.getColumnIndex("data10");
    o = i1;
    String[] arrayOfString = { "aggregated_raw_contact_source", "contact_source" };
    int i2 = a(paramCursor, arrayOfString);
    p = i2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.e.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */