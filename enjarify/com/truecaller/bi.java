package com.truecaller;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.e;
import com.bumptech.glide.e.a;
import com.bumptech.glide.k;
import com.bumptech.glide.l;
import com.bumptech.glide.load.b.j;
import com.bumptech.glide.load.m;

public final class bi
  extends k
  implements Cloneable
{
  bi(e parame, l paraml, Class paramClass, Context paramContext)
  {
    super(parame, paraml, paramClass, paramContext);
  }
  
  private bi c(a parama)
  {
    return (bi)super.a(parama);
  }
  
  public final bi b(int paramInt1, int paramInt2)
  {
    return (bi)super.a(paramInt1, paramInt2);
  }
  
  public final bi b(Drawable paramDrawable)
  {
    return (bi)super.a(paramDrawable);
  }
  
  public final bi b(j paramj)
  {
    return (bi)super.a(paramj);
  }
  
  public final bi b(m paramm)
  {
    return (bi)super.a(paramm);
  }
  
  public final bi c(boolean paramBoolean)
  {
    return (bi)super.b(paramBoolean);
  }
  
  public final bi e(int paramInt)
  {
    return (bi)super.a(paramInt);
  }
  
  public final bi f(int paramInt)
  {
    return (bi)super.b(paramInt);
  }
  
  public final bi g(int paramInt)
  {
    return (bi)super.c(paramInt);
  }
  
  public final bi k()
  {
    return (bi)super.e();
  }
  
  public final bi l()
  {
    return (bi)super.g();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.bi
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */