package com.truecaller.essentialnumber;

import c.g.a.m;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;

public final class f
  extends e
{
  final c.d.f a;
  final c.d.f c;
  final t d;
  private bn e;
  private final b f;
  
  public f(c.d.f paramf1, c.d.f paramf2, t paramt, b paramb)
  {
    a = paramf1;
    c = paramf2;
    d = paramt;
    f = paramb;
  }
  
  public final void a()
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("ViewAction");
    localObject = ((e.a)localObject).a("Action", "details").a("Context", "searchResults").a("category", "category").a();
    b localb = f;
    k.a(localObject, "analyticsEvent");
    localb.a((com.truecaller.analytics.e)localObject);
  }
  
  public final void a(EssentialCategory paramEssentialCategory)
  {
    k.b(paramEssentialCategory, "category");
    g localg = (g)b;
    if (localg == null) {
      return;
    }
    Object localObject1 = paramEssentialCategory.a();
    k.a(localObject1, "category.name");
    localg.a((String)localObject1);
    localg.a(true);
    localObject1 = (ag)bg.a;
    c.d.f localf = c;
    Object localObject2 = new com/truecaller/essentialnumber/f$a;
    ((f.a)localObject2).<init>(this, paramEssentialCategory, localg, null);
    localObject2 = (m)localObject2;
    paramEssentialCategory = kotlinx.coroutines.e.b((ag)localObject1, localf, (m)localObject2, 2);
    e = paramEssentialCategory;
  }
  
  public final void a(d paramd)
  {
    k.b(paramd, "essentialNumber");
    g localg = (g)b;
    if (localg != null)
    {
      Contact localContact = new com/truecaller/data/entity/Contact;
      localContact.<init>();
      Object localObject = paramd.a();
      localContact.l((String)localObject);
      localObject = new com/truecaller/data/entity/Number;
      paramd = paramd.c();
      ((Number)localObject).<init>(paramd);
      localContact.a((Number)localObject);
      localg.a(localContact);
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    bn localbn = e;
    if (localbn != null)
    {
      localbn.n();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */