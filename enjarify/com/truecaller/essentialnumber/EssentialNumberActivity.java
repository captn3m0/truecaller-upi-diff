package com.truecaller.essentialnumber;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ProgressBar;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import java.util.List;

public final class EssentialNumberActivity
  extends AppCompatActivity
  implements g, s.a
{
  public static final EssentialNumberActivity.a b;
  public e a;
  private RecyclerView c;
  private ProgressBar d;
  
  static
  {
    EssentialNumberActivity.a locala = new com/truecaller/essentialnumber/EssentialNumberActivity$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public final void a()
  {
    Object localObject = findViewById(2131364092);
    k.a(localObject, "findViewById(R.id.recyclerView)");
    localObject = (RecyclerView)localObject;
    c = ((RecyclerView)localObject);
    localObject = findViewById(2131364008);
    k.a(localObject, "findViewById(R.id.progressBar)");
    localObject = (ProgressBar)localObject;
    d = ((ProgressBar)localObject);
  }
  
  public final void a(Contact paramContact)
  {
    k.b(paramContact, "contact");
    Object localObject = this;
    localObject = (Context)this;
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.EssentialNumber;
    boolean bool = true;
    DetailsFragment.b((Context)localObject, paramContact, localSourceType, bool, bool);
    paramContact = a;
    if (paramContact == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    paramContact.a();
  }
  
  public final void a(d paramd)
  {
    k.b(paramd, "essentialNumber");
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.a(paramd);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "name");
    int i = 2131364907;
    Object localObject = findViewById(i);
    if (localObject != null)
    {
      localObject = (Toolbar)localObject;
      setSupportActionBar((Toolbar)localObject);
      localObject = getSupportActionBar();
      if (localObject != null)
      {
        paramString = (CharSequence)paramString;
        ((ActionBar)localObject).setTitle(paramString);
      }
      paramString = getSupportActionBar();
      if (paramString != null)
      {
        paramString.setDisplayHomeAsUpEnabled(true);
        return;
      }
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type android.support.v7.widget.Toolbar");
    throw paramString;
  }
  
  public final void a(List paramList, String paramString)
  {
    Object localObject1 = "categoryTag";
    k.b(paramString, (String)localObject1);
    if (paramList == null) {
      return;
    }
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "numberList";
      k.a((String)localObject2);
    }
    Object localObject2 = null;
    ((RecyclerView)localObject1).setVisibility(0);
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "numberList";
      k.a((String)localObject2);
    }
    localObject2 = new android/support/v7/widget/LinearLayoutManager;
    Object localObject3 = this;
    localObject3 = (Context)this;
    ((LinearLayoutManager)localObject2).<init>((Context)localObject3);
    localObject2 = (RecyclerView.LayoutManager)localObject2;
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject2);
    localObject1 = new com/truecaller/essentialnumber/s;
    ((s)localObject1).<init>((Context)localObject3, paramList, paramString);
    paramList = this;
    paramList = (s.a)this;
    a = paramList;
    paramList = c;
    if (paramList == null)
    {
      paramString = "numberList";
      k.a(paramString);
    }
    localObject1 = (RecyclerView.Adapter)localObject1;
    paramList.setAdapter((RecyclerView.Adapter)localObject1);
  }
  
  public final void a(boolean paramBoolean)
  {
    String str;
    if (paramBoolean)
    {
      localProgressBar = d;
      if (localProgressBar == null)
      {
        str = "progress";
        k.a(str);
      }
      localProgressBar.setVisibility(0);
      return;
    }
    ProgressBar localProgressBar = d;
    if (localProgressBar == null)
    {
      str = "progress";
      k.a(str);
    }
    localProgressBar.setVisibility(8);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    Object localObject1 = ThemeManager.a();
    int i = resId;
    setTheme(i);
    super.onCreate(paramBundle);
    int j = 2131558447;
    setContentView(j);
    paramBundle = getApplicationContext();
    if (paramBundle != null)
    {
      paramBundle = ((bk)paramBundle).a();
      k.a(paramBundle, "(applicationContext as GraphHolder).objectsGraph");
      paramBundle = b.a().a(paramBundle);
      localObject1 = new com/truecaller/essentialnumber/l;
      Object localObject2 = this;
      localObject2 = (AppCompatActivity)this;
      ((l)localObject1).<init>((AppCompatActivity)localObject2);
      paramBundle.a((l)localObject1).a().a(this);
      paramBundle = getIntent();
      if (paramBundle != null)
      {
        localObject1 = "category";
        paramBundle = (EssentialCategory)paramBundle.getParcelableExtra((String)localObject1);
        if (paramBundle != null)
        {
          localObject1 = a;
          if (localObject1 == null)
          {
            localObject2 = "presenter";
            k.a((String)localObject2);
          }
          ((e)localObject1).a(this);
          localObject1 = a;
          if (localObject1 == null)
          {
            localObject2 = "presenter";
            k.a((String)localObject2);
          }
          ((e)localObject1).a(paramBundle);
          return;
        }
      }
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.y_();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    String str = "item";
    k.b(paramMenuItem, str);
    int i = paramMenuItem.getItemId();
    int j = 16908332;
    if (i != j) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    super.onBackPressed();
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.EssentialNumberActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */