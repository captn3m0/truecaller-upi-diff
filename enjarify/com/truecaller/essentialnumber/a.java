package com.truecaller.essentialnumber;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import c.g.b.k;

public final class a
  extends RecyclerView.ItemDecoration
{
  private final int a;
  
  private a(int paramInt)
  {
    a = paramInt;
  }
  
  public a(Context paramContext)
  {
    this(i);
  }
  
  public final void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    k.b(paramRect, "outRect");
    k.b(paramView, "view");
    k.b(paramRecyclerView, "parent");
    k.b(paramState, "state");
    super.getItemOffsets(paramRect, paramView, paramRecyclerView, paramState);
    int i = a;
    paramRect.set(i, i, i, i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */