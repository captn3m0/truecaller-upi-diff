package com.truecaller.essentialnumber;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import c.g.b.k;
import java.util.List;

public final class h
  extends RecyclerView.Adapter
{
  h.a a;
  private final LayoutInflater b;
  private final View.OnClickListener c;
  private final Context d;
  private final List e;
  
  public h(Context paramContext, List paramList)
  {
    d = paramContext;
    e = paramList;
    paramContext = LayoutInflater.from(d);
    k.a(paramContext, "LayoutInflater.from(context)");
    b = paramContext;
    paramContext = new com/truecaller/essentialnumber/h$c;
    paramContext.<init>(this);
    paramContext = (View.OnClickListener)paramContext;
    c = paramContext;
  }
  
  public final int getItemCount()
  {
    return e.size();
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */