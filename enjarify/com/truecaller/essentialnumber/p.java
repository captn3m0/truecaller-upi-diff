package com.truecaller.essentialnumber;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final l a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  
  private p(l paraml, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paraml;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static p a(l paraml, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    p localp = new com/truecaller/essentialnumber/p;
    localp.<init>(paraml, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */