package com.truecaller.essentialnumber;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class EssentialCategory
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private String a;
  private String b;
  private int c;
  
  static
  {
    EssentialCategory.1 local1 = new com/truecaller/essentialnumber/EssentialCategory$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public EssentialCategory() {}
  
  protected EssentialCategory(Parcel paramParcel)
  {
    String str = paramParcel.readString();
    a = str;
    str = paramParcel.readString();
    b = str;
    int i = paramParcel.readInt();
    c = i;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final String b()
  {
    return b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    String str = a;
    paramParcel.writeString(str);
    str = b;
    paramParcel.writeString(str);
    paramInt = c;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.EssentialCategory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */