package com.truecaller.essentialnumber;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;

public final class s$b
  extends RecyclerView.ViewHolder
{
  final TextView a;
  final TextView b;
  final ImageView c;
  
  public s$b(View paramView)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131363718);
    k.a(localObject, "view.findViewById(R.id.main_text)");
    localObject = (TextView)localObject;
    a = ((TextView)localObject);
    localObject = paramView.findViewById(2131364281);
    k.a(localObject, "view.findViewById(R.id.secondary_text)");
    localObject = (TextView)localObject;
    b = ((TextView)localObject);
    paramView = paramView.findViewById(2131363336);
    k.a(paramView, "view.findViewById(R.id.imageEssentialCategory)");
    paramView = (ImageView)paramView;
    c = paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.s.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */