package com.truecaller.essentialnumber;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final l a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private m(l paraml, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paraml;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static m a(l paraml, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    m localm = new com/truecaller/essentialnumber/m;
    localm.<init>(paraml, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */