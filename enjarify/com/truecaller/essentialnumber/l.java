package com.truecaller.essentialnumber;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import com.truecaller.abtest.c;
import com.truecaller.analytics.b;
import com.truecaller.common.g.a;
import com.truecaller.utils.i;

public final class l
{
  final AppCompatActivity a;
  
  public l(AppCompatActivity paramAppCompatActivity)
  {
    a = paramAppCompatActivity;
  }
  
  public static e a(c.d.f paramf1, c.d.f paramf2, t paramt, b paramb)
  {
    k.b(paramf1, "asyncContext");
    k.b(paramf2, "parentContext");
    k.b(paramt, "essentialNumbersHelper");
    k.b(paramb, "analytics");
    f localf = new com/truecaller/essentialnumber/f;
    localf.<init>(paramf1, paramf2, paramt, paramb);
    return (e)localf;
  }
  
  public static t a(AppCompatActivity paramAppCompatActivity, c paramc, u paramu, com.google.gson.f paramf, i parami)
  {
    k.b(paramAppCompatActivity, "activity");
    k.b(paramc, "remoteConfig");
    k.b(paramu, "essentialNumbersSettings");
    k.b(paramf, "gson");
    k.b(parami, "networkUtil");
    j localj = new com/truecaller/essentialnumber/j;
    Context localContext = paramAppCompatActivity.getApplicationContext();
    k.a(localContext, "activity.applicationContext");
    localj.<init>(localContext, paramc, paramu, paramf, parami);
    return (t)localj;
  }
  
  public static u a()
  {
    v localv = new com/truecaller/essentialnumber/v;
    localv.<init>();
    return (u)localv;
  }
  
  public static w a(c.d.f paramf1, c.d.f paramf2, t paramt, b paramb, a parama)
  {
    k.b(paramf1, "asyncContext");
    k.b(paramf2, "parentContext");
    k.b(paramt, "essentialNumbersHelper");
    k.b(paramb, "analytics");
    k.b(parama, "coreSettings");
    x localx = new com/truecaller/essentialnumber/x;
    localx.<init>(paramf1, paramf2, paramt, paramb, parama);
    return (w)localx;
  }
  
  public static com.google.gson.f b()
  {
    com.google.gson.f localf = new com/google/gson/f;
    localf.<init>();
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */