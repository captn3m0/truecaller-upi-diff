package com.truecaller.essentialnumber;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ProgressBar;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.List;

public final class LocalServicesCategoryActivity
  extends AppCompatActivity
  implements h.a, y
{
  public static final LocalServicesCategoryActivity.a b;
  public w a;
  private RecyclerView c;
  private ProgressBar d;
  
  static
  {
    LocalServicesCategoryActivity.a locala = new com/truecaller/essentialnumber/LocalServicesCategoryActivity$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public static final Intent a(Context paramContext, String paramString)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, LocalServicesCategoryActivity.class);
    localIntent.putExtra("screen_context", paramString);
    return localIntent;
  }
  
  public final void a()
  {
    Object localObject = findViewById(2131364092);
    k.a(localObject, "findViewById(R.id.recyclerView)");
    localObject = (RecyclerView)localObject;
    c = ((RecyclerView)localObject);
    localObject = findViewById(2131364008);
    k.a(localObject, "findViewById(R.id.progressBar)");
    localObject = (ProgressBar)localObject;
    d = ((ProgressBar)localObject);
  }
  
  public final void a(EssentialCategory paramEssentialCategory)
  {
    k.b(paramEssentialCategory, "category");
    Object localObject1 = getIntent();
    if (localObject1 != null)
    {
      localObject2 = "screen_context";
      localObject1 = ((Intent)localObject1).getStringExtra((String)localObject2);
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = "";
    }
    Object localObject2 = EssentialNumberActivity.b;
    localObject2 = this;
    localObject2 = (Context)this;
    k.b(localObject2, "context");
    k.b(paramEssentialCategory, "category");
    Object localObject3 = new android/content/Intent;
    ((Intent)localObject3).<init>((Context)localObject2, EssentialNumberActivity.class);
    ((Intent)localObject3).putExtra("screen_context", (String)localObject1);
    Object localObject4 = paramEssentialCategory;
    localObject4 = (Parcelable)paramEssentialCategory;
    ((Intent)localObject3).putExtra("category", (Parcelable)localObject4);
    startActivity((Intent)localObject3);
    localObject2 = a;
    if (localObject2 == null)
    {
      localObject3 = "presenter";
      k.a((String)localObject3);
    }
    paramEssentialCategory = paramEssentialCategory.a();
    k.a(paramEssentialCategory, "category.name");
    ((w)localObject2).a(paramEssentialCategory, (String)localObject1);
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "categoryListFromFile");
    Object localObject1 = new com/truecaller/essentialnumber/h;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((h)localObject1).<init>((Context)localObject2, paramList);
    paramList = this;
    paramList = (h.a)this;
    a = paramList;
    paramList = c;
    if (paramList == null)
    {
      localObject3 = "categoryList";
      k.a((String)localObject3);
    }
    Object localObject3 = new android/support/v7/widget/GridLayoutManager;
    int i = 5;
    ((GridLayoutManager)localObject3).<init>((Context)localObject2, i);
    localObject3 = (RecyclerView.LayoutManager)localObject3;
    paramList.setLayoutManager((RecyclerView.LayoutManager)localObject3);
    paramList = new com/truecaller/essentialnumber/a;
    paramList.<init>((Context)localObject2);
    localObject2 = c;
    if (localObject2 == null)
    {
      localObject3 = "categoryList";
      k.a((String)localObject3);
    }
    paramList = (RecyclerView.ItemDecoration)paramList;
    ((RecyclerView)localObject2).addItemDecoration(paramList);
    paramList = c;
    if (paramList == null)
    {
      localObject2 = "categoryList";
      k.a((String)localObject2);
    }
    localObject1 = (RecyclerView.Adapter)localObject1;
    paramList.setAdapter((RecyclerView.Adapter)localObject1);
  }
  
  public final void a(boolean paramBoolean)
  {
    String str;
    if (paramBoolean)
    {
      localProgressBar = d;
      if (localProgressBar == null)
      {
        str = "progress";
        k.a(str);
      }
      localProgressBar.setVisibility(0);
      return;
    }
    ProgressBar localProgressBar = d;
    if (localProgressBar == null)
    {
      str = "progress";
      k.a(str);
    }
    localProgressBar.setVisibility(8);
  }
  
  public final void b()
  {
    int i = 2131364907;
    Object localObject = findViewById(i);
    if (localObject != null)
    {
      localObject = (Toolbar)localObject;
      setSupportActionBar((Toolbar)localObject);
      localObject = getSupportActionBar();
      if (localObject != null)
      {
        int j = 2131888924;
        CharSequence localCharSequence = (CharSequence)getString(j);
        ((ActionBar)localObject).setTitle(localCharSequence);
      }
      localObject = getSupportActionBar();
      if (localObject != null)
      {
        ((ActionBar)localObject).setDisplayHomeAsUpEnabled(true);
        return;
      }
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type android.support.v7.widget.Toolbar");
    throw ((Throwable)localObject);
  }
  
  public final void c()
  {
    finish();
  }
  
  public final void onBackPressed()
  {
    w localw = a;
    if (localw == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localw.a();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    Object localObject1 = ThemeManager.a();
    int i = resId;
    setTheme(i);
    super.onCreate(paramBundle);
    int j = 2131558447;
    setContentView(j);
    paramBundle = getApplicationContext();
    if (paramBundle != null)
    {
      paramBundle = ((bk)paramBundle).a();
      k.a(paramBundle, "(applicationContext as GraphHolder).objectsGraph");
      paramBundle = b.a().a(paramBundle);
      localObject1 = new com/truecaller/essentialnumber/l;
      Object localObject2 = this;
      localObject2 = (AppCompatActivity)this;
      ((l)localObject1).<init>((AppCompatActivity)localObject2);
      paramBundle.a((l)localObject1).a().a(this);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        k.a((String)localObject1);
      }
      paramBundle.a(this);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        k.a((String)localObject1);
      }
      paramBundle.b();
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    w localw = a;
    if (localw == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localw.y_();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    String str = "item";
    k.b(paramMenuItem, str);
    int i = paramMenuItem.getItemId();
    int j = 16908332;
    if (i != j) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    paramMenuItem = a;
    if (paramMenuItem == null)
    {
      str = "presenter";
      k.a(str);
    }
    paramMenuItem.a();
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.LocalServicesCategoryActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */