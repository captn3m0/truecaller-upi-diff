package com.truecaller.essentialnumber;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;

public final class h$b
  extends RecyclerView.ViewHolder
{
  final TextView a;
  final ImageView b;
  
  public h$b(View paramView)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131364704);
    k.a(localObject, "view.findViewById(R.id.textCategoryName)");
    localObject = (TextView)localObject;
    a = ((TextView)localObject);
    paramView = paramView.findViewById(2131363336);
    k.a(paramView, "view.findViewById(R.id.imageEssentialCategory)");
    paramView = (ImageView)paramView;
    b = paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */