package com.truecaller.essentialnumber;

import android.content.Context;
import com.google.gson.f;
import com.truecaller.abtest.c;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.ap;
import com.truecaller.utils.i;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class j
  implements t
{
  private final Context a;
  private final c b;
  private final u c;
  private final f d;
  private final i e;
  
  public j(Context paramContext, c paramc, u paramu, f paramf, i parami)
  {
    a = paramContext;
    b = paramc;
    c = paramu;
    d = paramf;
    e = parami;
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 39	com/truecaller/essentialnumber/j:b	Lcom/truecaller/abtest/c;
    //   4: ldc 47
    //   6: invokeinterface 52 2 0
    //   11: astore_1
    //   12: aload_0
    //   13: getfield 39	com/truecaller/essentialnumber/j:b	Lcom/truecaller/abtest/c;
    //   16: ldc 54
    //   18: invokeinterface 52 2 0
    //   23: astore_2
    //   24: aload_2
    //   25: invokestatic 59	org/c/a/a/a/b/a:a	(Ljava/lang/String;)I
    //   28: istore_3
    //   29: aload_0
    //   30: getfield 41	com/truecaller/essentialnumber/j:c	Lcom/truecaller/essentialnumber/u;
    //   33: astore 4
    //   35: ldc 54
    //   37: astore 5
    //   39: aload 4
    //   41: aload 5
    //   43: invokeinterface 63 2 0
    //   48: istore 6
    //   50: iload_3
    //   51: iload 6
    //   53: if_icmple +30 -> 83
    //   56: aload_1
    //   57: astore 4
    //   59: aload_1
    //   60: checkcast 65	java/lang/CharSequence
    //   63: astore 4
    //   65: aload 4
    //   67: invokestatic 70	com/truecaller/common/h/am:b	(Ljava/lang/CharSequence;)Z
    //   70: istore 6
    //   72: iload 6
    //   74: ifne +9 -> 83
    //   77: iconst_1
    //   78: istore 6
    //   80: goto +9 -> 89
    //   83: iconst_0
    //   84: istore 6
    //   86: aconst_null
    //   87: astore 4
    //   89: iload 6
    //   91: ifne +4 -> 95
    //   94: return
    //   95: aload_0
    //   96: getfield 37	com/truecaller/essentialnumber/j:a	Landroid/content/Context;
    //   99: invokevirtual 77	android/content/Context:getFilesDir	()Ljava/io/File;
    //   102: astore 4
    //   104: new 79	java/io/File
    //   107: astore 5
    //   109: ldc 81
    //   111: astore 7
    //   113: aload 5
    //   115: aload 4
    //   117: aload 7
    //   119: invokespecial 84	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   122: aload_0
    //   123: getfield 45	com/truecaller/essentialnumber/j:e	Lcom/truecaller/utils/i;
    //   126: astore 4
    //   128: aload 4
    //   130: invokeinterface 89 1 0
    //   135: istore 6
    //   137: iload 6
    //   139: ifne +4 -> 143
    //   142: return
    //   143: iconst_0
    //   144: istore 6
    //   146: aconst_null
    //   147: astore 4
    //   149: new 91	java/net/URL
    //   152: astore 7
    //   154: aload 7
    //   156: aload_1
    //   157: invokespecial 94	java/net/URL:<init>	(Ljava/lang/String;)V
    //   160: new 96	okhttp3/ab$a
    //   163: astore_1
    //   164: aload_1
    //   165: invokespecial 97	okhttp3/ab$a:<init>	()V
    //   168: aload_1
    //   169: aload 7
    //   171: invokevirtual 100	okhttp3/ab$a:a	(Ljava/net/URL;)Lokhttp3/ab$a;
    //   174: astore_1
    //   175: aload_1
    //   176: invokevirtual 103	okhttp3/ab$a:a	()Lokhttp3/ab;
    //   179: astore_1
    //   180: invokestatic 108	com/truecaller/common/network/util/d:a	()Lokhttp3/y;
    //   183: astore 7
    //   185: aload 7
    //   187: aload_1
    //   188: invokevirtual 113	okhttp3/y:a	(Lokhttp3/ab;)Lokhttp3/e;
    //   191: astore_1
    //   192: aload_1
    //   193: invokestatic 119	com/google/firebase/perf/network/FirebasePerfOkHttpClient:execute	(Lokhttp3/e;)Lokhttp3/ad;
    //   196: astore_1
    //   197: aload_1
    //   198: invokevirtual 124	okhttp3/ad:d	()Lokhttp3/ae;
    //   201: astore 7
    //   203: aload 7
    //   205: ifnonnull +12 -> 217
    //   208: aconst_null
    //   209: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   212: aconst_null
    //   213: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   216: return
    //   217: aload_1
    //   218: invokevirtual 124	okhttp3/ad:d	()Lokhttp3/ae;
    //   221: astore_1
    //   222: aload_1
    //   223: ifnull -15 -> 208
    //   226: aload_1
    //   227: invokevirtual 134	okhttp3/ae:d	()Ljava/io/InputStream;
    //   230: astore_1
    //   231: aload_1
    //   232: ifnonnull +6 -> 238
    //   235: goto -27 -> 208
    //   238: aload_1
    //   239: invokestatic 139	com/truecaller/util/ap:a	(Ljava/io/InputStream;)Ljava/lang/String;
    //   242: astore 7
    //   244: ldc -115
    //   246: astore 8
    //   248: aload 7
    //   250: aload 8
    //   252: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   255: new 145	java/io/FileOutputStream
    //   258: astore 8
    //   260: aload 8
    //   262: aload 5
    //   264: invokespecial 148	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   267: getstatic 153	c/n/d:a	Ljava/nio/charset/Charset;
    //   270: astore 4
    //   272: aload 7
    //   274: ifnull +66 -> 340
    //   277: aload 7
    //   279: aload 4
    //   281: invokevirtual 159	java/lang/String:getBytes	(Ljava/nio/charset/Charset;)[B
    //   284: astore 4
    //   286: ldc -95
    //   288: astore 5
    //   290: aload 4
    //   292: aload 5
    //   294: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   297: aload 8
    //   299: aload 4
    //   301: invokevirtual 165	java/io/FileOutputStream:write	([B)V
    //   304: aload_0
    //   305: getfield 41	com/truecaller/essentialnumber/j:c	Lcom/truecaller/essentialnumber/u;
    //   308: astore 4
    //   310: ldc 54
    //   312: astore 5
    //   314: aload 4
    //   316: aload 5
    //   318: iload_3
    //   319: invokeinterface 168 3 0
    //   324: aload_1
    //   325: checkcast 170	java/io/Closeable
    //   328: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   331: aload 8
    //   333: checkcast 170	java/io/Closeable
    //   336: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   339: return
    //   340: new 172	c/u
    //   343: astore_2
    //   344: ldc -82
    //   346: astore 4
    //   348: aload_2
    //   349: aload 4
    //   351: invokespecial 175	c/u:<init>	(Ljava/lang/String;)V
    //   354: aload_2
    //   355: athrow
    //   356: astore_2
    //   357: aload 8
    //   359: astore 4
    //   361: goto +18 -> 379
    //   364: pop
    //   365: aload 8
    //   367: astore 4
    //   369: goto +30 -> 399
    //   372: astore_2
    //   373: goto +6 -> 379
    //   376: astore_2
    //   377: aconst_null
    //   378: astore_1
    //   379: aload_1
    //   380: checkcast 170	java/io/Closeable
    //   383: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   386: aload 4
    //   388: checkcast 170	java/io/Closeable
    //   391: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   394: aload_2
    //   395: athrow
    //   396: pop
    //   397: aconst_null
    //   398: astore_1
    //   399: aload_1
    //   400: checkcast 170	java/io/Closeable
    //   403: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   406: aload 4
    //   408: checkcast 170	java/io/Closeable
    //   411: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   414: return
    //   415: pop
    //   416: goto -17 -> 399
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	419	0	this	j
    //   11	389	1	localObject1	Object
    //   23	332	2	localObject2	Object
    //   356	1	2	localObject3	Object
    //   372	1	2	localObject4	Object
    //   376	19	2	localObject5	Object
    //   28	291	3	i	int
    //   33	374	4	localObject6	Object
    //   37	280	5	localObject7	Object
    //   48	6	6	j	int
    //   70	75	6	bool	boolean
    //   111	167	7	localObject8	Object
    //   246	120	8	localObject9	Object
    //   364	1	13	localIOException1	java.io.IOException
    //   396	1	14	localIOException2	java.io.IOException
    //   415	1	15	localIOException3	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   267	270	356	finally
    //   279	284	356	finally
    //   292	297	356	finally
    //   299	304	356	finally
    //   304	308	356	finally
    //   318	324	356	finally
    //   340	343	356	finally
    //   349	354	356	finally
    //   354	356	356	finally
    //   267	270	364	java/io/IOException
    //   279	284	364	java/io/IOException
    //   292	297	364	java/io/IOException
    //   299	304	364	java/io/IOException
    //   304	308	364	java/io/IOException
    //   318	324	364	java/io/IOException
    //   340	343	364	java/io/IOException
    //   349	354	364	java/io/IOException
    //   354	356	364	java/io/IOException
    //   238	242	372	finally
    //   250	255	372	finally
    //   255	258	372	finally
    //   262	267	372	finally
    //   149	152	376	finally
    //   156	160	376	finally
    //   160	163	376	finally
    //   164	168	376	finally
    //   169	174	376	finally
    //   175	179	376	finally
    //   180	183	376	finally
    //   187	191	376	finally
    //   192	196	376	finally
    //   197	201	376	finally
    //   217	221	376	finally
    //   226	230	376	finally
    //   149	152	396	java/io/IOException
    //   156	160	396	java/io/IOException
    //   160	163	396	java/io/IOException
    //   164	168	396	java/io/IOException
    //   169	174	396	java/io/IOException
    //   175	179	396	java/io/IOException
    //   180	183	396	java/io/IOException
    //   187	191	396	java/io/IOException
    //   192	196	396	java/io/IOException
    //   197	201	396	java/io/IOException
    //   217	221	396	java/io/IOException
    //   226	230	396	java/io/IOException
    //   238	242	415	java/io/IOException
    //   250	255	415	java/io/IOException
    //   255	258	415	java/io/IOException
    //   262	267	415	java/io/IOException
  }
  
  public final boolean a(String paramString)
  {
    c.g.b.k.b(paramString, "fileName");
    File localFile1 = a.getFilesDir();
    File localFile2 = new java/io/File;
    localFile2.<init>(localFile1, paramString);
    long l = localFile2.length();
    int i = (int)l;
    return i > 0;
  }
  
  public final List b(String paramString)
  {
    c.g.b.k.b(paramString, "categoryName");
    Object localObject1 = a.getFilesDir();
    File localFile = new java/io/File;
    Object localObject2 = "essential_numbers.txt";
    localFile.<init>((File)localObject1, (String)localObject2);
    localObject1 = ap.b(localFile);
    localFile = null;
    try
    {
      localObject2 = d;
      Object localObject3 = k.class;
      localObject1 = ((f)localObject2).a((String)localObject1, (Class)localObject3);
      localObject1 = (k)localObject1;
      if (localObject1 == null)
      {
        paramString = new java/lang/IllegalStateException;
        localObject1 = "Numbers for category is null";
        paramString.<init>((String)localObject1);
        paramString = (Throwable)paramString;
        AssertionUtil.reportThrowableButNeverCrash(paramString);
        return null;
      }
      localObject1 = ((k)localObject1).a();
      if (localObject1 == null) {
        return null;
      }
      localObject2 = "essentialNumberList.numbers ?: return null";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      localObject1 = ((List)localObject1).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject3 = ((Iterator)localObject1).next();
        localObject3 = (d)localObject3;
        Object localObject4 = "essentialNumber";
        c.g.b.k.a(localObject3, (String)localObject4);
        localObject4 = ((d)localObject3).b();
        localObject4 = (CharSequence)localObject4;
        Object localObject5 = paramString;
        localObject5 = (CharSequence)paramString;
        boolean bool2 = am.a((CharSequence)localObject4, (CharSequence)localObject5);
        if (bool2) {
          ((ArrayList)localObject2).add(localObject3);
        }
      }
      return (List)localObject2;
    }
    catch (Exception localException) {}
    return null;
  }
  
  /* Error */
  public final List c(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -77
    //   3: invokestatic 23	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: new 215	java/util/ArrayList
    //   9: astore_2
    //   10: aload_2
    //   11: invokespecial 216	java/util/ArrayList:<init>	()V
    //   14: aconst_null
    //   15: astore_3
    //   16: aload_0
    //   17: getfield 37	com/truecaller/essentialnumber/j:a	Landroid/content/Context;
    //   20: astore 4
    //   22: aload 4
    //   24: invokevirtual 251	android/content/Context:getAssets	()Landroid/content/res/AssetManager;
    //   27: astore 4
    //   29: aload 4
    //   31: aload_1
    //   32: invokevirtual 257	android/content/res/AssetManager:open	(Ljava/lang/String;)Ljava/io/InputStream;
    //   35: astore_1
    //   36: aload_1
    //   37: ifnonnull +18 -> 55
    //   40: getstatic 262	c/a/y:a	Lc/a/y;
    //   43: astore_1
    //   44: aload_1
    //   45: checkcast 218	java/util/List
    //   48: astore_1
    //   49: aconst_null
    //   50: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   53: aload_1
    //   54: areturn
    //   55: aload_1
    //   56: invokestatic 139	com/truecaller/util/ap:a	(Ljava/io/InputStream;)Ljava/lang/String;
    //   59: astore_3
    //   60: ldc -115
    //   62: astore 4
    //   64: aload_3
    //   65: aload 4
    //   67: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   70: aload_0
    //   71: getfield 43	com/truecaller/essentialnumber/j:d	Lcom/google/gson/f;
    //   74: astore 4
    //   76: ldc_w 264
    //   79: astore 5
    //   81: aload 4
    //   83: aload_3
    //   84: aload 5
    //   86: invokevirtual 195	com/google/gson/f:a	(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    //   89: astore_3
    //   90: aload_3
    //   91: checkcast 264	com/truecaller/essentialnumber/c
    //   94: astore_3
    //   95: ldc_w 266
    //   98: astore 4
    //   100: aload_3
    //   101: aload 4
    //   103: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   106: aload_3
    //   107: invokevirtual 267	com/truecaller/essentialnumber/c:a	()Ljava/util/List;
    //   110: astore_3
    //   111: aload_3
    //   112: checkcast 269	java/util/Collection
    //   115: astore_3
    //   116: aload_2
    //   117: aload_3
    //   118: invokevirtual 273	java/util/ArrayList:addAll	(Ljava/util/Collection;)Z
    //   121: pop
    //   122: goto +22 -> 144
    //   125: astore_2
    //   126: goto +6 -> 132
    //   129: astore_2
    //   130: aconst_null
    //   131: astore_1
    //   132: aload_1
    //   133: checkcast 170	java/io/Closeable
    //   136: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   139: aload_2
    //   140: athrow
    //   141: pop
    //   142: aconst_null
    //   143: astore_1
    //   144: aload_1
    //   145: checkcast 170	java/io/Closeable
    //   148: invokestatic 129	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   151: aload_2
    //   152: checkcast 218	java/util/List
    //   155: areturn
    //   156: pop
    //   157: goto -13 -> 144
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	160	0	this	j
    //   0	160	1	paramString	String
    //   9	108	2	localArrayList	ArrayList
    //   125	1	2	localObject1	Object
    //   129	23	2	localObject2	Object
    //   15	103	3	localObject3	Object
    //   20	82	4	localObject4	Object
    //   79	6	5	localClass	Class
    //   141	1	8	localIOException1	java.io.IOException
    //   156	1	9	localIOException2	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   55	59	125	finally
    //   65	70	125	finally
    //   70	74	125	finally
    //   84	89	125	finally
    //   90	94	125	finally
    //   101	106	125	finally
    //   106	110	125	finally
    //   111	115	125	finally
    //   117	122	125	finally
    //   16	20	129	finally
    //   22	27	129	finally
    //   31	35	129	finally
    //   40	43	129	finally
    //   44	48	129	finally
    //   16	20	141	java/io/IOException
    //   22	27	141	java/io/IOException
    //   31	35	141	java/io/IOException
    //   40	43	141	java/io/IOException
    //   44	48	141	java/io/IOException
    //   55	59	156	java/io/IOException
    //   65	70	156	java/io/IOException
    //   70	74	156	java/io/IOException
    //   84	89	156	java/io/IOException
    //   90	94	156	java/io/IOException
    //   101	106	156	java/io/IOException
    //   106	110	156	java/io/IOException
    //   111	115	156	java/io/IOException
    //   117	122	156	java/io/IOException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.essentialnumber.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */