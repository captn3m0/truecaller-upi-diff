package com.truecaller;

import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.ClipboardManager.OnPrimaryClipChangedListener;
import android.os.SystemClock;
import c.d.f;
import c.g.b.k;
import c.n;
import com.truecaller.common.account.r;
import com.truecaller.common.h.u;
import com.truecaller.filters.FilterManager;
import com.truecaller.i.c;
import com.truecaller.util.al;
import com.truecaller.util.b;
import com.truecaller.util.bj;
import com.truecaller.utils.i;
import java.util.List;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class bo
  implements ClipboardManager.OnPrimaryClipChangedListener
{
  public bo.a a;
  public bn b;
  public final ClipboardManager c;
  private long d;
  private String e;
  private final c f;
  private final com.truecaller.utils.l g;
  private final b h;
  private final al i;
  private final com.truecaller.network.search.l j;
  private final FilterManager k;
  private final u l;
  private final r m;
  private final i n;
  private final f o;
  private final f p;
  
  public bo(ClipboardManager paramClipboardManager, c paramc, com.truecaller.utils.l paraml, b paramb, al paramal, com.truecaller.network.search.l paraml1, FilterManager paramFilterManager, u paramu, r paramr, i parami, f paramf1, f paramf2)
  {
    c = paramClipboardManager;
    f = paramc;
    g = paraml;
    h = paramb;
    i = paramal;
    j = paraml1;
    k = paramFilterManager;
    l = paramu;
    m = paramr;
    n = parami;
    o = paramf1;
    p = paramf2;
    paramClipboardManager = c;
    paramc = this;
    paramc = (ClipboardManager.OnPrimaryClipChangedListener)this;
    paramClipboardManager.addPrimaryClipChangedListener(paramc);
  }
  
  public final void onPrimaryClipChanged()
  {
    Object localObject1 = h;
    boolean bool1 = ((b)localObject1).a();
    Object localObject2 = c.getPrimaryClip();
    if (localObject2 != null)
    {
      Object localObject3 = ((ClipData)localObject2).getDescription();
      k.a(localObject3, "it.description");
      localObject3 = ((ClipDescription)localObject3).getLabel();
      Object localObject4 = "com.truecaller.OTP";
      boolean bool2 = k.a(localObject3, localObject4);
      boolean bool4 = true;
      bool2 ^= bool4;
      if (bool2)
      {
        i1 = ((ClipData)localObject2).getItemCount();
        if (i1 > 0)
        {
          i1 = 1;
          break label93;
        }
      }
      int i1 = 0;
      localObject3 = null;
      label93:
      boolean bool5;
      if (i1 == 0)
      {
        bool5 = false;
        localObject2 = null;
      }
      if (localObject2 != null)
      {
        localObject2 = ((ClipData)localObject2).getItemAt(0);
        if (localObject2 != null)
        {
          localObject2 = ((ClipData.Item)localObject2).getText();
          if (localObject2 != null)
          {
            localObject2 = localObject2.toString();
            if (localObject2 != null)
            {
              localObject3 = new com/truecaller/common/h/t;
              Object localObject5 = m.a();
              ((com.truecaller.common.h.t)localObject3).<init>((String)localObject5);
              localObject3 = ((com.truecaller.common.h.t)localObject3).a((String)localObject2);
              localObject5 = "PhoneNumberExtractor(acc…lidPhoneNumbers(clipText)";
              k.a(localObject3, (String)localObject5);
              localObject3 = (String)c.a.m.e((List)localObject3);
              if (localObject3 != null)
              {
                localObject2 = Boolean.TRUE;
                localObject2 = c.t.a(localObject3, localObject2);
              }
              else
              {
                boolean bool3 = bj.c((String)localObject2);
                if (!bool3)
                {
                  bool5 = false;
                  localObject2 = null;
                }
                if (localObject2 != null)
                {
                  localObject3 = Boolean.FALSE;
                  localObject2 = c.t.a(localObject2, localObject3);
                }
                else
                {
                  bool5 = false;
                  localObject2 = null;
                }
              }
              if (localObject2 == null) {
                return;
              }
              localObject3 = (String)a;
              localObject2 = (Boolean)b;
              bool5 = ((Boolean)localObject2).booleanValue();
              f.a("lastCopied", (String)localObject3);
              localObject5 = f;
              String str = "lastCopiedFromTc";
              ((c)localObject5).b(str, bool1);
              if ((bool5) && (!bool1))
              {
                localObject1 = e;
                bool1 = k.a(localObject3, localObject1) ^ bool4;
                long l1;
                if (!bool1)
                {
                  l1 = SystemClock.elapsedRealtime();
                  long l2 = d;
                  long l3 = 500L;
                  l2 += l3;
                  boolean bool6 = l1 < l2;
                  if (!bool6) {}
                }
                else
                {
                  localObject1 = f;
                  localObject2 = "clipboardSearchEnabled";
                  bool1 = ((c)localObject1).b((String)localObject2);
                  if (bool1)
                  {
                    localObject1 = g;
                    localObject2 = new String[] { "android.permission.READ_PHONE_STATE" };
                    bool1 = ((com.truecaller.utils.l)localObject1).a((String[])localObject2);
                    if (bool1)
                    {
                      localObject1 = g;
                      bool1 = ((com.truecaller.utils.l)localObject1).a();
                      if (bool1)
                      {
                        localObject1 = n;
                        bool1 = ((i)localObject1).a();
                        if (bool1)
                        {
                          localObject1 = i;
                          bool1 = ((al)localObject1).a();
                          if (bool1) {
                            break label483;
                          }
                        }
                      }
                    }
                  }
                  bool4 = false;
                  localObject4 = null;
                  label483:
                  if (bool4)
                  {
                    l1 = SystemClock.elapsedRealtime();
                    d = l1;
                    e = ((String)localObject3);
                    localObject1 = b;
                    if (localObject1 != null)
                    {
                      localObject2 = new java/util/concurrent/CancellationException;
                      localObject4 = "Next search requested";
                      ((CancellationException)localObject2).<init>((String)localObject4);
                      localObject2 = (Throwable)localObject2;
                      ((bn)localObject1).c((Throwable)localObject2);
                    }
                    localObject1 = (ag)bg.a;
                    localObject2 = o;
                    localObject4 = new com/truecaller/bo$b;
                    ((bo.b)localObject4).<init>(this, (String)localObject3, null);
                    localObject4 = (c.g.a.m)localObject4;
                    int i2 = 2;
                    localObject1 = e.b((ag)localObject1, (f)localObject2, (c.g.a.m)localObject4, i2);
                    b = ((bn)localObject1);
                  }
                }
              }
              return;
            }
          }
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.bo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */