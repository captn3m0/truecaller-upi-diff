package com.truecaller.log;

import c.g.b.k;
import c.n.m;
import c.w;

public final class c
  extends a
{
  public static final c a;
  
  static
  {
    c localc = new com/truecaller/log/c;
    localc.<init>();
    a = localc;
  }
  
  public final boolean a(Throwable paramThrowable)
  {
    String str = "e";
    k.b(paramThrowable, str);
    boolean bool1 = paramThrowable instanceof NullPointerException;
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    bool1 = paramThrowable instanceof w;
    if (bool1) {
      return bool2;
    }
    bool1 = paramThrowable instanceof IllegalStateException;
    boolean bool3;
    if (bool1)
    {
      paramThrowable = ((IllegalStateException)paramThrowable).getMessage();
      if (paramThrowable == null) {
        paramThrowable = "";
      }
      str = " must not be null";
      bool1 = m.c(paramThrowable, str, false);
      if (bool1) {
        return bool2;
      }
      str = "Field specified as non-null is null: ";
      bool1 = m.b(paramThrowable, str, false);
      if (bool1) {
        return bool2;
      }
      str = "Method specified as non-null returned null: ";
      bool3 = m.b(paramThrowable, str, false);
      if (bool3) {
        return bool2;
      }
      return false;
    }
    bool1 = paramThrowable instanceof IllegalArgumentException;
    if (bool1)
    {
      paramThrowable = ((IllegalArgumentException)paramThrowable).getMessage();
      if (paramThrowable == null) {
        paramThrowable = "";
      }
      str = "Parameter specified as non-null is null: method ";
      bool3 = m.b(paramThrowable, str, false);
      if (bool3) {
        return bool2;
      }
      return false;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */