package com.truecaller.log;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Field;

public final class b
{
  private static final a[] a;
  private static Field b;
  
  static
  {
    a[] arrayOfa = new a[3];
    Object localObject = g.a;
    arrayOfa[0] = localObject;
    localObject = c.a;
    arrayOfa[1] = localObject;
    localObject = e.a;
    arrayOfa[2] = localObject;
    a = arrayOfa;
  }
  
  /* Error */
  private static java.io.Serializable a(java.io.Serializable paramSerializable)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: new 32	java/io/ByteArrayOutputStream
    //   5: astore_2
    //   6: aload_2
    //   7: invokespecial 36	java/io/ByteArrayOutputStream:<init>	()V
    //   10: new 38	java/io/ObjectOutputStream
    //   13: astore_3
    //   14: aload_3
    //   15: aload_2
    //   16: invokespecial 41	java/io/ObjectOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   19: aload_3
    //   20: aload_0
    //   21: invokevirtual 45	java/io/ObjectOutputStream:writeObject	(Ljava/lang/Object;)V
    //   24: aload_3
    //   25: invokevirtual 48	java/io/ObjectOutputStream:flush	()V
    //   28: new 50	java/io/ByteArrayInputStream
    //   31: astore 4
    //   33: aload_2
    //   34: invokevirtual 54	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   37: astore_2
    //   38: aload 4
    //   40: aload_2
    //   41: invokespecial 57	java/io/ByteArrayInputStream:<init>	([B)V
    //   44: new 59	java/io/ObjectInputStream
    //   47: astore_2
    //   48: aload_2
    //   49: aload 4
    //   51: invokespecial 62	java/io/ObjectInputStream:<init>	(Ljava/io/InputStream;)V
    //   54: aload_2
    //   55: invokevirtual 66	java/io/ObjectInputStream:readObject	()Ljava/lang/Object;
    //   58: astore 4
    //   60: aload 4
    //   62: checkcast 68	java/io/Serializable
    //   65: astore 4
    //   67: aload_3
    //   68: invokestatic 71	com/truecaller/log/b:a	(Ljava/io/Closeable;)V
    //   71: aload_2
    //   72: invokestatic 71	com/truecaller/log/b:a	(Ljava/io/Closeable;)V
    //   75: aload 4
    //   77: areturn
    //   78: astore_0
    //   79: goto +49 -> 128
    //   82: astore_0
    //   83: goto +45 -> 128
    //   86: astore_0
    //   87: aconst_null
    //   88: astore_2
    //   89: goto +55 -> 144
    //   92: astore_0
    //   93: goto +4 -> 97
    //   96: astore_0
    //   97: aconst_null
    //   98: astore_2
    //   99: goto +29 -> 128
    //   102: pop
    //   103: aconst_null
    //   104: astore_2
    //   105: aload_3
    //   106: astore_1
    //   107: goto +45 -> 152
    //   110: astore_0
    //   111: aconst_null
    //   112: astore_2
    //   113: goto +111 -> 224
    //   116: astore_0
    //   117: goto +4 -> 121
    //   120: astore_0
    //   121: aconst_null
    //   122: astore_2
    //   123: iconst_0
    //   124: istore 5
    //   126: aconst_null
    //   127: astore_3
    //   128: aload_0
    //   129: invokevirtual 77	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   132: pop
    //   133: aload_3
    //   134: invokestatic 71	com/truecaller/log/b:a	(Ljava/io/Closeable;)V
    //   137: aload_2
    //   138: invokestatic 71	com/truecaller/log/b:a	(Ljava/io/Closeable;)V
    //   141: aconst_null
    //   142: areturn
    //   143: astore_0
    //   144: aload_3
    //   145: astore_1
    //   146: goto +78 -> 224
    //   149: pop
    //   150: aconst_null
    //   151: astore_2
    //   152: iconst_1
    //   153: istore 5
    //   155: iload 5
    //   157: anewarray 79	java/lang/String
    //   160: astore_3
    //   161: aconst_null
    //   162: astore 4
    //   164: new 81	java/lang/StringBuilder
    //   167: astore 6
    //   169: ldc 83
    //   171: astore 7
    //   173: aload 6
    //   175: aload 7
    //   177: invokespecial 86	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   180: aload_0
    //   181: invokevirtual 90	java/lang/Object:getClass	()Ljava/lang/Class;
    //   184: astore 7
    //   186: aload 7
    //   188: invokevirtual 95	java/lang/Class:getCanonicalName	()Ljava/lang/String;
    //   191: astore 7
    //   193: aload 6
    //   195: aload 7
    //   197: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   200: pop
    //   201: aload 6
    //   203: invokevirtual 102	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   206: astore 6
    //   208: aload_3
    //   209: iconst_0
    //   210: aload 6
    //   212: aastore
    //   213: aload_1
    //   214: invokestatic 71	com/truecaller/log/b:a	(Ljava/io/Closeable;)V
    //   217: aload_2
    //   218: invokestatic 71	com/truecaller/log/b:a	(Ljava/io/Closeable;)V
    //   221: aload_0
    //   222: areturn
    //   223: astore_0
    //   224: aload_1
    //   225: invokestatic 71	com/truecaller/log/b:a	(Ljava/io/Closeable;)V
    //   228: aload_2
    //   229: invokestatic 71	com/truecaller/log/b:a	(Ljava/io/Closeable;)V
    //   232: aload_0
    //   233: athrow
    //   234: pop
    //   235: goto -130 -> 105
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	238	0	paramSerializable	java.io.Serializable
    //   1	224	1	localObject1	Object
    //   5	224	2	localObject2	Object
    //   13	196	3	localObject3	Object
    //   31	132	4	localObject4	Object
    //   124	32	5	i	int
    //   167	44	6	localObject5	Object
    //   171	25	7	localObject6	Object
    //   102	1	8	localNotSerializableException1	java.io.NotSerializableException
    //   149	1	9	localNotSerializableException2	java.io.NotSerializableException
    //   234	1	10	localNotSerializableException3	java.io.NotSerializableException
    // Exception table:
    //   from	to	target	type
    //   54	58	78	java/lang/ClassNotFoundException
    //   60	65	78	java/lang/ClassNotFoundException
    //   54	58	82	java/io/IOException
    //   60	65	82	java/io/IOException
    //   20	24	86	finally
    //   24	28	86	finally
    //   28	31	86	finally
    //   33	37	86	finally
    //   40	44	86	finally
    //   44	47	86	finally
    //   49	54	86	finally
    //   20	24	92	java/lang/ClassNotFoundException
    //   24	28	92	java/lang/ClassNotFoundException
    //   28	31	92	java/lang/ClassNotFoundException
    //   33	37	92	java/lang/ClassNotFoundException
    //   40	44	92	java/lang/ClassNotFoundException
    //   44	47	92	java/lang/ClassNotFoundException
    //   49	54	92	java/lang/ClassNotFoundException
    //   20	24	96	java/io/IOException
    //   24	28	96	java/io/IOException
    //   28	31	96	java/io/IOException
    //   33	37	96	java/io/IOException
    //   40	44	96	java/io/IOException
    //   44	47	96	java/io/IOException
    //   49	54	96	java/io/IOException
    //   20	24	102	java/io/NotSerializableException
    //   24	28	102	java/io/NotSerializableException
    //   28	31	102	java/io/NotSerializableException
    //   33	37	102	java/io/NotSerializableException
    //   40	44	102	java/io/NotSerializableException
    //   44	47	102	java/io/NotSerializableException
    //   49	54	102	java/io/NotSerializableException
    //   2	5	110	finally
    //   6	10	110	finally
    //   10	13	110	finally
    //   15	19	110	finally
    //   2	5	116	java/lang/ClassNotFoundException
    //   6	10	116	java/lang/ClassNotFoundException
    //   10	13	116	java/lang/ClassNotFoundException
    //   15	19	116	java/lang/ClassNotFoundException
    //   2	5	120	java/io/IOException
    //   6	10	120	java/io/IOException
    //   10	13	120	java/io/IOException
    //   15	19	120	java/io/IOException
    //   54	58	143	finally
    //   60	65	143	finally
    //   128	133	143	finally
    //   2	5	149	java/io/NotSerializableException
    //   6	10	149	java/io/NotSerializableException
    //   10	13	149	java/io/NotSerializableException
    //   15	19	149	java/io/NotSerializableException
    //   155	160	223	finally
    //   164	167	223	finally
    //   175	180	223	finally
    //   180	184	223	finally
    //   186	191	223	finally
    //   195	201	223	finally
    //   201	206	223	finally
    //   210	213	223	finally
    //   54	58	234	java/io/NotSerializableException
    //   60	65	234	java/io/NotSerializableException
  }
  
  public static Throwable a(Throwable paramThrowable)
  {
    Field localField = a();
    if ((paramThrowable != null) && (localField != null))
    {
      paramThrowable = (Throwable)a(paramThrowable);
      Throwable localThrowable = paramThrowable;
      while (localThrowable != null) {
        try
        {
          a[] arrayOfa = a;
          int i = arrayOfa.length;
          int j = 0;
          String str = null;
          while (j < i)
          {
            a locala = arrayOfa[j];
            boolean bool = locala.a(localThrowable);
            if (bool) {
              str = locala.b(localThrowable);
            }
            j += 1;
          }
          if (str == null) {
            str = "<muted>";
          }
          localField.set(localThrowable, str);
          localThrowable = localThrowable.getCause();
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
          return null;
        }
      }
      return paramThrowable;
    }
    return null;
  }
  
  private static Field a()
  {
    synchronized (b.class)
    {
      Object localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = b;
        return (Field)localObject1;
      }
      localObject1 = Throwable.class;
      String str = "detailMessage";
      try
      {
        localObject1 = ((Class)localObject1).getDeclaredField(str);
        b = (Field)localObject1;
        boolean bool = true;
        ((Field)localObject1).setAccessible(bool);
      }
      catch (NoSuchFieldException localNoSuchFieldException)
      {
        for (;;) {}
      }
      localObject1 = b;
      return (Field)localObject1;
    }
  }
  
  private static void a(Closeable paramCloseable)
  {
    if (paramCloseable == null) {
      return;
    }
    try
    {
      paramCloseable.close();
      return;
    }
    catch (IOException localIOException) {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */