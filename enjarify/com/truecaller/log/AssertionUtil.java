package com.truecaller.log;

import android.os.Debug;
import android.os.Handler;
import android.os.Looper;

public final class AssertionUtil
{
  private static final String NOT_ON_MAIN_EXPLANATION = "Should be executing on main thread, but isn't!";
  private static final String ON_MAIN_EXPLANATION = "Should NOT be executing on main thread, but is! Naughty naughty!!";
  private static boolean sDisableAsserts = false;
  private static boolean sIsDebugBuild;
  
  public static void isFalse(boolean paramBoolean, String... paramVarArgs)
  {
    boolean bool = sDisableAsserts;
    if ((!bool) && (paramBoolean))
    {
      reportMessages(paramVarArgs);
      paramBoolean = sIsDebugBuild;
      bool = false;
      if (paramBoolean)
      {
        localObject = new com/truecaller/log/AssertionUtil$TcAssertionError;
        paramVarArgs = summarize(paramVarArgs);
        ((AssertionUtil.TcAssertionError)localObject).<init>(paramVarArgs, null);
        throwHard((AssertionUtil.TcAssertionError)localObject);
        return;
      }
      Object localObject = new com/truecaller/log/AssertionUtil$TcDryAssertionError;
      paramVarArgs = summarize(paramVarArgs);
      ((AssertionUtil.TcDryAssertionError)localObject).<init>(paramVarArgs, null);
      reportThrowableButNeverCrash((Throwable)localObject);
    }
  }
  
  public static void isNotNull(Object paramObject, String... paramVarArgs)
  {
    boolean bool1 = sDisableAsserts;
    if ((!bool1) && (paramObject == null))
    {
      reportMessages(paramVarArgs);
      boolean bool2 = sIsDebugBuild;
      bool1 = false;
      if (bool2)
      {
        paramObject = new com/truecaller/log/AssertionUtil$TcAssertionError;
        paramVarArgs = summarize(paramVarArgs);
        ((AssertionUtil.TcAssertionError)paramObject).<init>(paramVarArgs, null);
        throwHard((AssertionUtil.TcAssertionError)paramObject);
        return;
      }
      paramObject = new com/truecaller/log/AssertionUtil$TcDryAssertionError;
      paramVarArgs = summarize(paramVarArgs);
      ((AssertionUtil.TcDryAssertionError)paramObject).<init>(paramVarArgs, null);
      reportThrowableButNeverCrash((Throwable)paramObject);
    }
  }
  
  public static void isNull(Object paramObject, String... paramVarArgs)
  {
    boolean bool1 = sDisableAsserts;
    if ((!bool1) && (paramObject != null))
    {
      reportMessages(paramVarArgs);
      boolean bool2 = sIsDebugBuild;
      bool1 = false;
      if (bool2)
      {
        paramObject = new com/truecaller/log/AssertionUtil$TcAssertionError;
        paramVarArgs = summarize(paramVarArgs);
        ((AssertionUtil.TcAssertionError)paramObject).<init>(paramVarArgs, null);
        throwHard((AssertionUtil.TcAssertionError)paramObject);
        return;
      }
      paramObject = new com/truecaller/log/AssertionUtil$TcDryAssertionError;
      paramVarArgs = summarize(paramVarArgs);
      ((AssertionUtil.TcDryAssertionError)paramObject).<init>(paramVarArgs, null);
      reportThrowableButNeverCrash((Throwable)paramObject);
    }
  }
  
  private static boolean isOnMainThread()
  {
    Thread localThread1 = Looper.getMainLooper().getThread();
    Thread localThread2 = Thread.currentThread();
    return localThread1 == localThread2;
  }
  
  public static void isTrue(boolean paramBoolean, String... paramVarArgs)
  {
    boolean bool = sDisableAsserts;
    if ((!bool) && (!paramBoolean))
    {
      reportMessages(paramVarArgs);
      paramBoolean = sIsDebugBuild;
      bool = false;
      if (paramBoolean)
      {
        localObject = new com/truecaller/log/AssertionUtil$TcAssertionError;
        paramVarArgs = summarize(paramVarArgs);
        ((AssertionUtil.TcAssertionError)localObject).<init>(paramVarArgs, null);
        throwHard((AssertionUtil.TcAssertionError)localObject);
        return;
      }
      Object localObject = new com/truecaller/log/AssertionUtil$TcDryAssertionError;
      paramVarArgs = summarize(paramVarArgs);
      ((AssertionUtil.TcDryAssertionError)localObject).<init>(paramVarArgs, null);
      reportThrowableButNeverCrash((Throwable)localObject);
    }
  }
  
  public static void notOnMainThread(String... paramVarArgs)
  {
    boolean bool = sDisableAsserts;
    if (!bool)
    {
      bool = isOnMainThread();
      if (bool)
      {
        reportMessages(paramVarArgs);
        bool = sIsDebugBuild;
        if (bool)
        {
          localObject = new com/truecaller/log/AssertionUtil$TcAssertionError;
          localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>("Should NOT be executing on main thread, but is! Naughty naughty!! ");
          paramVarArgs = summarize(paramVarArgs);
          localStringBuilder.append(paramVarArgs);
          paramVarArgs = localStringBuilder.toString();
          ((AssertionUtil.TcAssertionError)localObject).<init>(paramVarArgs, null);
          throwHard((AssertionUtil.TcAssertionError)localObject);
          return;
        }
        Object localObject = new com/truecaller/log/AssertionUtil$TcDryAssertionError;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        String str = "Should NOT be executing on main thread, but is! Naughty naughty!! ";
        localStringBuilder.<init>(str);
        paramVarArgs = summarize(paramVarArgs);
        localStringBuilder.append(paramVarArgs);
        paramVarArgs = localStringBuilder.toString();
        ((AssertionUtil.TcDryAssertionError)localObject).<init>(paramVarArgs, null);
        reportThrowableButNeverCrash((Throwable)localObject);
      }
    }
  }
  
  public static void onMainThread(String... paramVarArgs)
  {
    boolean bool = sDisableAsserts;
    if (!bool)
    {
      bool = isOnMainThread();
      if (!bool)
      {
        reportMessages(paramVarArgs);
        bool = sIsDebugBuild;
        if (bool)
        {
          localObject = new com/truecaller/log/AssertionUtil$TcAssertionError;
          localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>("Should be executing on main thread, but isn't! ");
          paramVarArgs = summarize(paramVarArgs);
          localStringBuilder.append(paramVarArgs);
          paramVarArgs = localStringBuilder.toString();
          ((AssertionUtil.TcAssertionError)localObject).<init>(paramVarArgs, null);
          throwHard((AssertionUtil.TcAssertionError)localObject);
          return;
        }
        Object localObject = new com/truecaller/log/AssertionUtil$TcDryAssertionError;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        String str = "Should be executing on main thread, but isn't! ";
        localStringBuilder.<init>(str);
        paramVarArgs = summarize(paramVarArgs);
        localStringBuilder.append(paramVarArgs);
        paramVarArgs = localStringBuilder.toString();
        ((AssertionUtil.TcDryAssertionError)localObject).<init>(paramVarArgs, null);
        reportThrowableButNeverCrash((Throwable)localObject);
      }
    }
  }
  
  public static void onSameThread(Thread paramThread, String... paramVarArgs)
  {
    boolean bool = sDisableAsserts;
    if (!bool)
    {
      Object localObject1 = Thread.currentThread();
      if (localObject1 != paramThread)
      {
        reportMessages(paramVarArgs);
        bool = sIsDebugBuild;
        if (bool)
        {
          localObject1 = new com/truecaller/log/AssertionUtil$TcAssertionError;
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("Must be executed on thread [");
          paramThread = paramThread.getName();
          ((StringBuilder)localObject2).append(paramThread);
          ((StringBuilder)localObject2).append("] but was on thread [");
          paramThread = Thread.currentThread().getName();
          ((StringBuilder)localObject2).append(paramThread);
          ((StringBuilder)localObject2).append("] ");
          paramThread = summarize(paramVarArgs);
          ((StringBuilder)localObject2).append(paramThread);
          paramThread = ((StringBuilder)localObject2).toString();
          ((AssertionUtil.TcAssertionError)localObject1).<init>(paramThread, null);
          throwHard((AssertionUtil.TcAssertionError)localObject1);
          return;
        }
        paramThread = new com/truecaller/log/AssertionUtil$TcDryAssertionError;
        localObject1 = new java/lang/StringBuilder;
        Object localObject2 = "Should NOT be executing on main thread, but is! Naughty naughty!! ";
        ((StringBuilder)localObject1).<init>((String)localObject2);
        paramVarArgs = summarize(paramVarArgs);
        ((StringBuilder)localObject1).append(paramVarArgs);
        paramVarArgs = ((StringBuilder)localObject1).toString();
        paramThread.<init>(paramVarArgs, null);
        reportThrowableButNeverCrash(paramThread);
      }
    }
  }
  
  private static void removeMyselfFromTopOfStacktrace(Throwable paramThrowable)
  {
    StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
    Object localObject = AssertionUtil.class.getCanonicalName();
    int i = 0;
    for (;;)
    {
      int j = arrayOfStackTraceElement.length + -1;
      if (i >= j) {
        break;
      }
      String str = arrayOfStackTraceElement[i].getClassName();
      boolean bool = str.startsWith((String)localObject);
      if (!bool) {
        break;
      }
      i += 1;
    }
    if (i > 0)
    {
      int m = arrayOfStackTraceElement.length - i;
      localObject = new StackTraceElement[m];
      int k = localObject.length;
      System.arraycopy(arrayOfStackTraceElement, i, localObject, 0, k);
      paramThrowable.setStackTrace((StackTraceElement[])localObject);
    }
  }
  
  public static void report(String... paramVarArgs)
  {
    boolean bool = sDisableAsserts;
    if (bool) {
      return;
    }
    reportWithSummary(summarize(paramVarArgs), paramVarArgs);
  }
  
  private static void reportMessages(String... paramVarArgs)
  {
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      String str = paramVarArgs[j];
      String[] arrayOfString = new String[1];
      arrayOfString[0] = str;
      boolean bool = sIsDebugBuild;
      if (!bool)
      {
        bool = Debug.isDebuggerConnected();
        if (!bool) {
          d.a(str);
        }
      }
      j += 1;
    }
  }
  
  public static void reportThrowableButNeverCrash(Throwable paramThrowable)
  {
    boolean bool = sDisableAsserts;
    if (bool) {
      return;
    }
    bool = sIsDebugBuild;
    if (!bool)
    {
      bool = Debug.isDebuggerConnected();
      if (!bool)
      {
        d.a(paramThrowable);
        return;
      }
    }
    paramThrowable.printStackTrace();
  }
  
  public static void reportWeirdnessButNeverCrash(String paramString)
  {
    AssertionUtil.TcDryAssertionError localTcDryAssertionError = new com/truecaller/log/AssertionUtil$TcDryAssertionError;
    localTcDryAssertionError.<init>(paramString, null);
    paramString = new String[0];
    shouldNeverHappen(localTcDryAssertionError, paramString);
  }
  
  public static void reportWithSummary(String paramString, String... paramVarArgs)
  {
    boolean bool = sDisableAsserts;
    if (bool) {
      return;
    }
    reportMessages(paramVarArgs);
    paramVarArgs = new com/truecaller/log/AssertionUtil$TcDryAssertionError;
    paramVarArgs.<init>(paramString, null);
    reportThrowableButNeverCrash(paramVarArgs);
  }
  
  public static void setDisableAsserts(boolean paramBoolean)
  {
    sDisableAsserts = paramBoolean;
  }
  
  public static void setIsDebugBuild(boolean paramBoolean)
  {
    sIsDebugBuild = paramBoolean;
  }
  
  public static void shouldNeverHappen(Throwable paramThrowable, String... paramVarArgs)
  {
    boolean bool = sDisableAsserts;
    if (bool) {
      return;
    }
    reportMessages(paramVarArgs);
    bool = sIsDebugBuild;
    if (bool)
    {
      localObject = new com/truecaller/log/AssertionUtil$TcAssertionError;
      paramVarArgs = summarize(paramVarArgs);
      ((AssertionUtil.TcAssertionError)localObject).<init>(paramVarArgs, null);
      ((AssertionUtil.TcAssertionError)localObject).initCause(paramThrowable);
      throwHard((AssertionUtil.TcAssertionError)localObject);
      return;
    }
    Object localObject = new com/truecaller/log/AssertionUtil$TcDryAssertionError;
    paramVarArgs = summarize(paramVarArgs);
    ((AssertionUtil.TcDryAssertionError)localObject).<init>(paramVarArgs, null);
    ((AssertionUtil.TcDryAssertionError)localObject).initCause(paramThrowable);
    reportThrowableButNeverCrash((Throwable)localObject);
  }
  
  private static String summarize(String[] paramArrayOfString)
  {
    if (paramArrayOfString != null)
    {
      int i = paramArrayOfString.length;
      if (i > 0)
      {
        i = 0;
        String str = paramArrayOfString[0];
        if (str != null) {
          return paramArrayOfString[0];
        }
      }
    }
    return "";
  }
  
  private static void throwHard(AssertionUtil.TcAssertionError paramTcAssertionError)
  {
    boolean bool = sDisableAsserts;
    if (bool) {
      return;
    }
    reportThrowableButNeverCrash(paramTcAssertionError);
    try
    {
      Handler localHandler = new android/os/Handler;
      Object localObject = Looper.getMainLooper();
      localHandler.<init>((Looper)localObject);
      localObject = new com/truecaller/log/AssertionUtil$1;
      ((AssertionUtil.1)localObject).<init>(paramTcAssertionError);
      localHandler.post((Runnable)localObject);
    }
    finally
    {
      for (;;) {}
    }
    throw paramTcAssertionError;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.AssertionUtil
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */