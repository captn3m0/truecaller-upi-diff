package com.truecaller.log;

import c.g.b.k;
import com.crashlytics.android.a;
import io.grpc.bc;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import javax.net.ssl.SSLException;

public final class d
{
  public static boolean a;
  
  public static final void a(int paramInt, String paramString1, String paramString2)
  {
    k.b(paramString1, "tag");
    k.b(paramString2, "msg");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString1);
    localStringBuilder.append(": ");
    localStringBuilder.append(paramString2);
    paramString1 = localStringBuilder.toString();
    paramString2 = null;
    int i = 1;
    int j = 6;
    if (paramInt != j)
    {
      new String[i][0] = paramString1;
      return;
    }
    new String[i][0] = paramString1;
  }
  
  public static final void a(String paramString)
  {
    k.b(paramString, "msg");
    new String[1][0] = paramString;
  }
  
  public static final void a(Throwable paramThrowable)
  {
    k.b(paramThrowable, "throwable");
    a(paramThrowable, null);
  }
  
  public static final void a(Throwable paramThrowable, String paramString)
  {
    String str = "exception";
    k.b(paramThrowable, str);
    if (paramString == null) {
      paramThrowable.getMessage();
    }
    boolean bool = a;
    if (bool)
    {
      bool = b(paramThrowable);
      if (bool)
      {
        paramThrowable = b.a(paramThrowable);
        a.a(paramThrowable);
      }
    }
  }
  
  private static final boolean b(Throwable paramThrowable)
  {
    boolean bool1 = paramThrowable instanceof ConnectException;
    if (!bool1)
    {
      bool1 = paramThrowable instanceof UnknownHostException;
      if (!bool1)
      {
        bool1 = paramThrowable instanceof SocketTimeoutException;
        if (!bool1)
        {
          bool1 = paramThrowable instanceof SocketException;
          if (!bool1)
          {
            bool1 = paramThrowable instanceof SSLException;
            if (!bool1)
            {
              boolean bool2 = paramThrowable instanceof bc;
              if (!bool2) {
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */