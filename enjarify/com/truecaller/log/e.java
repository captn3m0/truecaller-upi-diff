package com.truecaller.log;

import android.database.sqlite.SQLiteException;
import c.g.b.k;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class e
  extends a
{
  public static final e a;
  
  static
  {
    e locale = new com/truecaller/log/e;
    locale.<init>();
    a = locale;
  }
  
  public final boolean a(Throwable paramThrowable)
  {
    k.b(paramThrowable, "e");
    return paramThrowable instanceof SQLiteException;
  }
  
  public final String b(Throwable paramThrowable)
  {
    k.b(paramThrowable, "e");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    Object localObject = "<muted> ";
    localStringBuilder.<init>((String)localObject);
    boolean bool = paramThrowable instanceof SQLiteException;
    String str = null;
    if (!bool) {
      paramThrowable = null;
    }
    paramThrowable = (SQLiteException)paramThrowable;
    if (paramThrowable != null)
    {
      paramThrowable = paramThrowable.getMessage();
      if (paramThrowable == null)
      {
        str = "";
      }
      else
      {
        localObject = Pattern.compile("\\(code \\d+\\)");
        paramThrowable = (CharSequence)paramThrowable;
        paramThrowable = ((Pattern)localObject).matcher(paramThrowable);
        localObject = "Pattern.compile(\"\\\\(code…\\d+\\\\)\").matcher(message)";
        k.a(paramThrowable, (String)localObject);
        bool = paramThrowable.find();
        if (!bool)
        {
          str = "";
        }
        else
        {
          str = paramThrowable.group();
          paramThrowable = "matcher.group()";
          k.a(str, paramThrowable);
        }
      }
    }
    localStringBuilder.append(str);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */