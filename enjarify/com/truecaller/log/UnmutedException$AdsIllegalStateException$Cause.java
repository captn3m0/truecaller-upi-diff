package com.truecaller.log;

public enum UnmutedException$AdsIllegalStateException$Cause
{
  private final String description;
  
  static
  {
    Cause[] arrayOfCause = new Cause[3];
    Cause localCause = new com/truecaller/log/UnmutedException$AdsIllegalStateException$Cause;
    localCause.<init>("CAMPAIGN_CONFIG_NULL_KEY", 0, "Key is null for campaign");
    CAMPAIGN_CONFIG_NULL_KEY = localCause;
    arrayOfCause[0] = localCause;
    localCause = new com/truecaller/log/UnmutedException$AdsIllegalStateException$Cause;
    int i = 1;
    localCause.<init>("CAMPAIGN_CONFIG_NULL_PLACEMENT", i, "Placement is null for campaign");
    CAMPAIGN_CONFIG_NULL_PLACEMENT = localCause;
    arrayOfCause[i] = localCause;
    localCause = new com/truecaller/log/UnmutedException$AdsIllegalStateException$Cause;
    i = 2;
    localCause.<init>("CAMPAIGN_REQUEST_ILLEGAL_STATE", i, "Campaign request failed with Illegal state");
    CAMPAIGN_REQUEST_ILLEGAL_STATE = localCause;
    arrayOfCause[i] = localCause;
    $VALUES = arrayOfCause;
  }
  
  private UnmutedException$AdsIllegalStateException$Cause(String paramString1)
  {
    description = paramString1;
  }
  
  public final String getDescription()
  {
    return description;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.UnmutedException.AdsIllegalStateException.Cause
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */