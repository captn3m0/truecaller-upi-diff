package com.truecaller.log;

import android.os.Bundle;
import android.util.Log;
import java.util.Iterator;
import java.util.Set;

public final class f
{
  public static boolean a;
  
  public static String a(Throwable paramThrowable)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Exception: ");
    String str = paramThrowable.getMessage();
    localStringBuilder.append(str);
    localStringBuilder.append(", Stack: ");
    paramThrowable = Log.getStackTraceString(paramThrowable);
    localStringBuilder.append(paramThrowable);
    return localStringBuilder.toString();
  }
  
  public static void a(Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      Iterator localIterator = paramBundle.keySet().iterator();
      Object localObject = "DumpIntentExtras - Dumping Intent start";
      new String[1][0] = localObject;
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject = (String)localIterator.next();
        int i = 1;
        String[] arrayOfString = new String[i];
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("[");
        localStringBuilder.append((String)localObject);
        String str = "=";
        localStringBuilder.append(str);
        localObject = paramBundle.get((String)localObject);
        localStringBuilder.append(localObject);
        localStringBuilder.append("]");
        localObject = localStringBuilder.toString();
        arrayOfString[0] = localObject;
      }
      paramBundle = "DumpIntentExtras - Dumping Intent end";
      new String[1][0] = paramBundle;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */