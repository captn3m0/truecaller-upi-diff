package com.truecaller.log;

public enum UnmutedException$InsightsExceptions$Cause
{
  private final String description;
  
  static
  {
    Cause[] arrayOfCause = new Cause[5];
    Cause localCause = new com/truecaller/log/UnmutedException$InsightsExceptions$Cause;
    localCause.<init>("PARSE_FAILURE", 0, "Insights parser failed to parse the message");
    PARSE_FAILURE = localCause;
    arrayOfCause[0] = localCause;
    localCause = new com/truecaller/log/UnmutedException$InsightsExceptions$Cause;
    int i = 1;
    localCause.<init>("PARSER_EXCEPTION", i, "Exception from facade of Controller.parse ");
    PARSER_EXCEPTION = localCause;
    arrayOfCause[i] = localCause;
    localCause = new com/truecaller/log/UnmutedException$InsightsExceptions$Cause;
    i = 2;
    localCause.<init>("PARSER_UNKNOWN_GRM_EXCEPTION", i, "Unknown grammar exception ");
    PARSER_UNKNOWN_GRM_EXCEPTION = localCause;
    arrayOfCause[i] = localCause;
    localCause = new com/truecaller/log/UnmutedException$InsightsExceptions$Cause;
    i = 3;
    localCause.<init>("BINDER_EXCEPTION", i, "Error in insights binder ");
    BINDER_EXCEPTION = localCause;
    arrayOfCause[i] = localCause;
    localCause = new com/truecaller/log/UnmutedException$InsightsExceptions$Cause;
    i = 4;
    localCause.<init>("UNBINDER_EXCEPTION", i, "Error in insights UnBinder ");
    UNBINDER_EXCEPTION = localCause;
    arrayOfCause[i] = localCause;
    $VALUES = arrayOfCause;
  }
  
  private UnmutedException$InsightsExceptions$Cause(String paramString1)
  {
    description = paramString1;
  }
  
  public final String getDescription()
  {
    return description;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.UnmutedException.InsightsExceptions.Cause
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */