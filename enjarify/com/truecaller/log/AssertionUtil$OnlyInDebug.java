package com.truecaller.log;

public class AssertionUtil$OnlyInDebug
{
  public static void fail(String... paramVarArgs)
  {
    boolean bool = AssertionUtil.access$600();
    if (bool)
    {
      AssertionUtil.TcAssertionError localTcAssertionError = new com/truecaller/log/AssertionUtil$TcAssertionError;
      paramVarArgs = AssertionUtil.access$300(paramVarArgs);
      localTcAssertionError.<init>(paramVarArgs, null);
      AssertionUtil.access$400(localTcAssertionError);
    }
  }
  
  public static void isTrue(boolean paramBoolean, String... paramVarArgs)
  {
    boolean bool = AssertionUtil.access$600();
    if ((bool) && (!paramBoolean))
    {
      AssertionUtil.access$200(paramVarArgs);
      AssertionUtil.TcAssertionError localTcAssertionError = new com/truecaller/log/AssertionUtil$TcAssertionError;
      paramVarArgs = AssertionUtil.access$300(paramVarArgs);
      bool = false;
      localTcAssertionError.<init>(paramVarArgs, null);
      AssertionUtil.access$400(localTcAssertionError);
    }
  }
  
  public static void notOnMainThread(String... paramVarArgs)
  {
    boolean bool = AssertionUtil.access$600();
    if (bool)
    {
      bool = AssertionUtil.access$500();
      if (bool)
      {
        AssertionUtil.access$200(paramVarArgs);
        AssertionUtil.TcAssertionError localTcAssertionError = new com/truecaller/log/AssertionUtil$TcAssertionError;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        String str = "Should NOT be executing on main thread, but is! Naughty naughty!! ";
        localStringBuilder.<init>(str);
        paramVarArgs = AssertionUtil.access$300(paramVarArgs);
        localStringBuilder.append(paramVarArgs);
        paramVarArgs = localStringBuilder.toString();
        localStringBuilder = null;
        localTcAssertionError.<init>(paramVarArgs, null);
        AssertionUtil.access$400(localTcAssertionError);
      }
    }
  }
  
  public static void onMainThread(String... paramVarArgs)
  {
    boolean bool = AssertionUtil.access$600();
    if (bool)
    {
      bool = AssertionUtil.access$500();
      if (!bool)
      {
        AssertionUtil.access$200(paramVarArgs);
        AssertionUtil.TcAssertionError localTcAssertionError = new com/truecaller/log/AssertionUtil$TcAssertionError;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        String str = "Should be executing on main thread, but isn't! ";
        localStringBuilder.<init>(str);
        paramVarArgs = AssertionUtil.access$300(paramVarArgs);
        localStringBuilder.append(paramVarArgs);
        paramVarArgs = localStringBuilder.toString();
        localStringBuilder = null;
        localTcAssertionError.<init>(paramVarArgs, null);
        AssertionUtil.access$400(localTcAssertionError);
      }
    }
  }
  
  public static void shouldNeverHappen(Throwable paramThrowable, String... paramVarArgs)
  {
    boolean bool = AssertionUtil.access$600();
    if (bool)
    {
      AssertionUtil.access$200(paramVarArgs);
      AssertionUtil.TcAssertionError localTcAssertionError = new com/truecaller/log/AssertionUtil$TcAssertionError;
      paramVarArgs = AssertionUtil.access$300(paramVarArgs);
      localTcAssertionError.<init>(paramVarArgs, null);
      localTcAssertionError.initCause(paramThrowable);
      AssertionUtil.access$400(localTcAssertionError);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.AssertionUtil.OnlyInDebug
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */