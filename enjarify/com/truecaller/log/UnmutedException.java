package com.truecaller.log;

public abstract class UnmutedException
  extends RuntimeException
{
  private UnmutedException(String paramString)
  {
    super(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.UnmutedException
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */