package com.truecaller;

public final class R$plurals
{
  public static final int BlockNumbersQuestion = 2131755008;
  public static final int ConversationMessagesDeleteConfirmation = 2131755009;
  public static final int ConversationMessagesDeleteQuestion = 2131755010;
  public static final int ConversationReportNotSpamConfirmation = 2131755011;
  public static final int ConversationReportSpamConfirmation = 2131755012;
  public static final int DeleteConversationsConfirmation = 2131755013;
  public static final int DeleteConversationsQuestion = 2131755014;
  public static final int EnhancedNotificationTitle = 2131755015;
  public static final int HistoryActionConfirmDeleteMessage = 2131755016;
  public static final int HistoryActionDeleted = 2131755017;
  public static final int HistoryActionSelected = 2131755018;
  public static final int ImGroupInvitesTitle = 2131755019;
  public static final int InboxMissedCallsPromoMore = 2131755020;
  public static final int MessageNotificationBlockedTitle = 2131755021;
  public static final int MessageNotificationNewMessages = 2131755022;
  public static final int MissedCallReminderText = 2131755023;
  public static final int MmsMultipleContactsVcardName = 2131755024;
  public static final int MmsTextAttachmentsSuffix = 2131755025;
  public static final int NewImGroupMemberCount = 2131755026;
  public static final int NewImGroupParticipantCount = 2131755027;
  public static final int NumbersBlockedMessage = 2131755028;
  public static final int OSNotificationTextNew = 2131755029;
  public static final int OSNotificationTitleBlockedCalls = 2131755030;
  public static final int OSNotificationTitleMutedCalls = 2131755031;
  public static final int ProfileViewCountDesc = 2131755032;
  public static final int SpamSmsFound = 2131755033;
  public static final int StatusMessageInvitedByMultiple = 2131755034;
  public static final int StatusMessageInvitedByYouMultiple = 2131755035;
  public static final int StrPhoneNumberAndMore = 2131755036;
  public static final int VerificationError_limitExceededHours = 2131755037;
  public static final int WhoViewedMeNotificationTitle = 2131755038;
  public static final int WhoViewedMeNotificationWithLocationTitle = 2131755039;
  public static final int call_recording_list_promo_on_trial_active_title = 2131755040;
  public static final int call_recording_toast_items_deleted = 2131755041;
  public static final int invitations = 2131755042;
  public static final int joda_time_android_abbrev_in_num_days = 2131755043;
  public static final int joda_time_android_abbrev_in_num_hours = 2131755044;
  public static final int joda_time_android_abbrev_in_num_minutes = 2131755045;
  public static final int joda_time_android_abbrev_in_num_seconds = 2131755046;
  public static final int joda_time_android_abbrev_num_days_ago = 2131755047;
  public static final int joda_time_android_abbrev_num_hours_ago = 2131755048;
  public static final int joda_time_android_abbrev_num_minutes_ago = 2131755049;
  public static final int joda_time_android_abbrev_num_seconds_ago = 2131755050;
  public static final int joda_time_android_duration_hours = 2131755051;
  public static final int joda_time_android_duration_minutes = 2131755052;
  public static final int joda_time_android_duration_seconds = 2131755053;
  public static final int joda_time_android_in_num_days = 2131755054;
  public static final int joda_time_android_in_num_hours = 2131755055;
  public static final int joda_time_android_in_num_minutes = 2131755056;
  public static final int joda_time_android_in_num_seconds = 2131755057;
  public static final int joda_time_android_num_days_ago = 2131755058;
  public static final int joda_time_android_num_hours_ago = 2131755059;
  public static final int joda_time_android_num_minutes_ago = 2131755060;
  public static final int joda_time_android_num_seconds_ago = 2131755061;
  public static final int plural_friend = 2131755062;
  public static final int referral_days_of_premium = 2131755063;
  public static final int rewards_time_days = 2131755064;
  public static final int spam_counts = 2131755065;
  public static final int voip_notification_missed_grouped_title = 2131755066;
}

/* Location:
 * Qualified Name:     com.truecaller.R.plurals
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */