package com.truecaller.premium;

import android.arch.lifecycle.h;
import android.net.Uri;
import com.truecaller.premium.data.PremiumType;
import java.util.List;

public abstract interface ay
  extends h
{
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(PremiumType paramPremiumType);
  
  public abstract void a(String paramString);
  
  public abstract void a(List paramList);
  
  public abstract void b(int paramInt);
  
  public abstract void c(int paramInt);
  
  public abstract void d(int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.premium.ay
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */