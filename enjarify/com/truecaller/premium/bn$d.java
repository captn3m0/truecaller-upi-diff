package com.truecaller.premium;

import c.g.b.k;
import com.google.common.util.concurrent.FutureCallback;
import com.truecaller.log.AssertionUtil;

public final class bn$d
  implements FutureCallback
{
  bn$d(bn parambn) {}
  
  public final void onFailure(Throwable paramThrowable)
  {
    k.b(paramThrowable, "t");
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Engagement rewards: onFailure:: ");
    String str = paramThrowable.getMessage();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    arrayOfString[0] = localObject;
    AssertionUtil.reportThrowableButNeverCrash(paramThrowable);
    a.a(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.bn.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */