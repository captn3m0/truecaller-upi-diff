package com.truecaller.premium;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.n;
import c.g.b.k;
import com.truecaller.premium.data.PremiumType;
import java.io.Serializable;

final class q$b
  extends n
{
  private final PremiumType a;
  private final int b;
  
  public q$b(PremiumType paramPremiumType, int paramInt, j paramj)
  {
    super(paramj);
    a = paramPremiumType;
    b = paramInt;
  }
  
  public final Fragment a(int paramInt)
  {
    Object localObject = al.b;
    localObject = a;
    k.b(localObject, "premiumType");
    al localal = new com/truecaller/premium/al;
    localal.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localObject = (Serializable)localObject;
    localBundle.putSerializable("type", (Serializable)localObject);
    localBundle.putInt("page_number", paramInt);
    localal.setArguments(localBundle);
    return (Fragment)localal;
  }
  
  public final int getCount()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.q.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */