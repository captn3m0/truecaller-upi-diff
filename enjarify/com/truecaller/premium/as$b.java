package com.truecaller.premium;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import c.a.ag;
import c.g.b.k;
import c.t;
import com.truecaller.premium.data.PremiumType;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

final class as$b
  extends android.support.v4.app.n
{
  private final Map a;
  private final Context b;
  private final List c;
  
  public as$b(Context paramContext, List paramList, j paramj)
  {
    super(paramj);
    b = paramContext;
    c = paramList;
    paramContext = new c.n[2];
    paramList = PremiumType.PREMIUM;
    paramj = Integer.valueOf(2131886901);
    paramList = t.a(paramList, paramj);
    paramContext[0] = paramList;
    paramList = PremiumType.GOLD;
    paramj = Integer.valueOf(2131886900);
    paramList = t.a(paramList, paramj);
    paramContext[1] = paramList;
    paramContext = ag.a(paramContext);
    a = paramContext;
  }
  
  public final Fragment a(int paramInt)
  {
    Object localObject1 = z.c;
    Object localObject2 = (PremiumType)c.get(paramInt);
    k.b(localObject2, "type");
    localObject1 = new com/truecaller/premium/z;
    ((z)localObject1).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localObject2 = (Serializable)localObject2;
    localBundle.putSerializable("type", (Serializable)localObject2);
    ((z)localObject1).setArguments(localBundle);
    return (Fragment)localObject1;
  }
  
  public final int getCount()
  {
    return c.size();
  }
  
  public final CharSequence getPageTitle(int paramInt)
  {
    Context localContext = b;
    Map localMap = a;
    Object localObject = c.get(paramInt);
    paramInt = ((Number)ag.b(localMap, localObject)).intValue();
    return (CharSequence)localContext.getString(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.as.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */