package com.truecaller.premium;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import c.u;
import com.truecaller.common.b.a;
import com.truecaller.common.b.e;
import com.truecaller.engagementrewards.ui.ClaimEngagementRewardsActivity.a;
import com.truecaller.premium.data.SubscriptionPromoEventMetaData;
import com.truecaller.ui.WizardActivity;
import com.truecaller.wizard.b.c;
import java.io.Serializable;

public final class br
{
  public static Intent a(Context paramContext, PremiumPresenterView.LaunchContext paramLaunchContext, String paramString, SubscriptionPromoEventMetaData paramSubscriptionPromoEventMetaData)
  {
    k.b(paramContext, "context");
    Class localClass = a(paramLaunchContext);
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, localClass);
    paramLaunchContext = (Serializable)paramLaunchContext;
    paramContext = localIntent.putExtra("launchContext", paramLaunchContext).putExtra("selectedPage", paramString);
    paramSubscriptionPromoEventMetaData = (Parcelable)paramSubscriptionPromoEventMetaData;
    paramContext = paramContext.putExtra("analyticsMetadata", paramSubscriptionPromoEventMetaData).addFlags(268435456);
    k.a(paramContext, "Intent(context, activity…t.FLAG_ACTIVITY_NEW_TASK)");
    return paramContext;
  }
  
  private static Class a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    if (paramLaunchContext != null)
    {
      int[] arrayOfInt = bs.a;
      int i = paramLaunchContext.ordinal();
      i = arrayOfInt[i];
      switch (i)
      {
      default: 
        break;
      case 1: 
      case 2: 
        return PremiumDialogActivity.class;
      }
    }
    return PremiumActivity.class;
  }
  
  public static void a(Context paramContext, PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    paramLaunchContext = a(paramContext, paramLaunchContext, null, 8);
    paramContext.startActivity(paramLaunchContext);
  }
  
  public static void a(Context paramContext, PremiumPresenterView.LaunchContext paramLaunchContext, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    k.b(paramString, "page");
    paramLaunchContext = a(paramContext, paramLaunchContext, paramString, 8);
    paramContext.startActivity(paramLaunchContext);
  }
  
  public static void a(Context paramContext, String paramString, PremiumPresenterView.LaunchContext paramLaunchContext, SubscriptionPromoEventMetaData paramSubscriptionPromoEventMetaData)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    k.b(paramSubscriptionPromoEventMetaData, "subscriptionPromoEventMetaData");
    paramString = a(paramContext, paramLaunchContext, paramString, paramSubscriptionPromoEventMetaData);
    paramContext.startActivity(paramString);
  }
  
  public static void b(Context paramContext, PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    paramLaunchContext = c(paramContext, paramLaunchContext);
    paramContext.startActivity(paramLaunchContext);
  }
  
  public static void b(Context paramContext, String paramString, PremiumPresenterView.LaunchContext paramLaunchContext, SubscriptionPromoEventMetaData paramSubscriptionPromoEventMetaData)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    k.b(paramSubscriptionPromoEventMetaData, "subscriptionPromoEventMetaData");
    Object localObject = paramContext.getApplicationContext();
    if (localObject != null)
    {
      localObject = (a)localObject;
      boolean bool1 = ((a)localObject).p();
      if (bool1)
      {
        bool1 = c.f();
        if (bool1)
        {
          a(paramContext, paramString, paramLaunchContext, paramSubscriptionPromoEventMetaData);
          return;
        }
      }
      paramString = "silentLoginFailed";
      paramLaunchContext = null;
      boolean bool2 = e.a(paramString, false);
      if (bool2) {
        ((a)localObject).a(false);
      }
      c.a(paramContext, WizardActivity.class);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw paramContext;
  }
  
  public static Intent c(Context paramContext, PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    paramLaunchContext = paramLaunchContext.name();
    return ClaimEngagementRewardsActivity.a.a(paramContext, paramLaunchContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.br
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */