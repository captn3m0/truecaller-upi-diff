package com.truecaller.premium.data;

public enum PremiumType
{
  static
  {
    Object localObject = new com/truecaller/premium/data/PremiumType;
    ((PremiumType)localObject).<init>("PREMIUM", 0);
    PREMIUM = (PremiumType)localObject;
    localObject = new com/truecaller/premium/data/PremiumType;
    int i = 1;
    ((PremiumType)localObject).<init>("GOLD", i);
    GOLD = (PremiumType)localObject;
    localObject = new PremiumType[2];
    PremiumType localPremiumType = PREMIUM;
    localObject[0] = localPremiumType;
    localPremiumType = GOLD;
    localObject[i] = localPremiumType;
    $VALUES = (PremiumType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.PremiumType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */