package com.truecaller.premium.data;

import c.g.b.k;
import com.truecaller.premium.bz;

public final class af$d
  extends af
{
  public final bz a;
  
  public af$d(bz parambz)
  {
    super((byte)0);
    a = parambz;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof d;
      if (bool1)
      {
        paramObject = (d)paramObject;
        bz localbz = a;
        paramObject = a;
        boolean bool2 = k.a(localbz, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    bz localbz = a;
    if (localbz != null) {
      return localbz.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("MovePremiumToAnotherNumber(receipt=");
    bz localbz = a;
    localStringBuilder.append(localbz);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.af.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */