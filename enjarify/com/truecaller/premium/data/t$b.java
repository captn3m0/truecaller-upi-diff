package com.truecaller.premium.data;

import c.g.b.k;
import com.truecaller.common.f.a;

final class t$b
{
  final int a;
  final String b;
  final a c;
  
  public t$b(int paramInt, String paramString, a parama)
  {
    a = paramInt;
    b = paramString;
    c = parama;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof b;
      if (bool2)
      {
        paramObject = (b)paramObject;
        int i = a;
        int j = a;
        Object localObject;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localObject = null;
        }
        if (i != 0)
        {
          localObject = b;
          String str = b;
          boolean bool3 = k.a(localObject, str);
          if (bool3)
          {
            localObject = c;
            paramObject = c;
            boolean bool4 = k.a(localObject, paramObject);
            if (bool4) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    Object localObject = b;
    int j = 0;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = c;
    if (localObject != null) {
      j = localObject.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VerificationResult(status=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", receipt=");
    Object localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", premium=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.t.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */