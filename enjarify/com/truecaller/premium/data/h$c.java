package com.truecaller.premium.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class h$c
  extends u
{
  private final String b;
  private final String c;
  
  private h$c(e parame, String paramString1, String paramString2)
  {
    super(parame);
    b = paramString1;
    c = paramString2;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".restorePurchase(");
    String str = b;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(c, i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.h.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */