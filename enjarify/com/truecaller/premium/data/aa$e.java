package com.truecaller.premium.data;

import c.g.b.k;

public final class aa$e
  extends aa
{
  public final int a;
  public final String b;
  
  public aa$e(int paramInt, String paramString)
  {
    super((byte)0);
    a = paramInt;
    b = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof e;
      if (bool2)
      {
        paramObject = (e)paramObject;
        int i = a;
        int j = a;
        String str;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          str = null;
        }
        if (i != 0)
        {
          str = b;
          paramObject = b;
          boolean bool3 = k.a(str, paramObject);
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    String str = b;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ReceiptVerificationError(status=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", receipt=");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.aa.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */