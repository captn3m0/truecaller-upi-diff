package com.truecaller.premium.data;

import android.net.Uri;
import c.g.b.k;
import java.util.List;

public final class w
{
  public final f a;
  public final int b;
  public final int c;
  public final String d;
  public final Uri e;
  public final List f;
  public final int g;
  
  public w(f paramf, int paramInt1, int paramInt2, String paramString, Uri paramUri, List paramList, int paramInt3)
  {
    a = paramf;
    b = paramInt1;
    c = paramInt2;
    d = paramString;
    e = paramUri;
    f = paramList;
    g = paramInt3;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof w;
      if (bool2)
      {
        paramObject = (w)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          int i = b;
          int k = b;
          if (i == k)
          {
            i = 1;
          }
          else
          {
            i = 0;
            localObject1 = null;
          }
          if (i != 0)
          {
            i = c;
            k = c;
            if (i == k)
            {
              i = 1;
            }
            else
            {
              i = 0;
              localObject1 = null;
            }
            if (i != 0)
            {
              localObject1 = d;
              localObject2 = d;
              boolean bool3 = k.a(localObject1, localObject2);
              if (bool3)
              {
                localObject1 = e;
                localObject2 = e;
                bool3 = k.a(localObject1, localObject2);
                if (bool3)
                {
                  localObject1 = f;
                  localObject2 = f;
                  bool3 = k.a(localObject1, localObject2);
                  if (bool3)
                  {
                    int j = g;
                    int m = g;
                    if (j == m)
                    {
                      m = 1;
                    }
                    else
                    {
                      m = 0;
                      paramObject = null;
                    }
                    if (m != 0) {
                      return bool1;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    f localf = a;
    int i = 0;
    if (localf != null)
    {
      j = localf.hashCode();
    }
    else
    {
      j = 0;
      localf = null;
    }
    j *= 31;
    int k = b;
    int j = (j + k) * 31;
    k = c;
    j = (j + k) * 31;
    Object localObject = d;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = e;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = f;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    j = (j + i) * 31;
    i = g;
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PremiumThemePart(listTitle=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", detailsTitleRes=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", detailsBackgroundRes=");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", toolbarTitle=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", topImage=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", features=");
    localObject = f;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", mainColor=");
    i = g;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */