package com.truecaller.premium.data;

import c.a.m;
import c.g.b.k;
import com.truecaller.premium.cb;
import java.util.Iterator;
import java.util.List;

public final class x
{
  private final String a;
  private final String b;
  private final String c;
  
  public x(String paramString1, String paramString2, String paramString3)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
  }
  
  public final cb a(List paramList)
  {
    Object localObject1 = "subscriptions";
    k.b(paramList, (String)localObject1);
    paramList = ((Iterable)paramList).iterator();
    boolean bool2;
    do
    {
      bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = paramList.next();
      Object localObject2 = localObject1;
      localObject2 = (cb)localObject1;
      String str = a;
      if (str != null)
      {
        localObject2 = a;
        str = a;
        bool2 = k.a(localObject2, str);
        if (bool2)
        {
          bool2 = true;
          continue;
        }
      }
      bool2 = false;
      localObject2 = null;
    } while (!bool2);
    break label108;
    boolean bool1 = false;
    localObject1 = null;
    label108:
    return (cb)localObject1;
  }
  
  public final List a()
  {
    String[] arrayOfString = new String[3];
    String str = a;
    arrayOfString[0] = str;
    str = b;
    arrayOfString[1] = str;
    str = c;
    arrayOfString[2] = str;
    return m.e(arrayOfString);
  }
  
  public final cb b(List paramList)
  {
    Object localObject1 = "subscriptions";
    k.b(paramList, (String)localObject1);
    paramList = ((Iterable)paramList).iterator();
    boolean bool2;
    do
    {
      bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = paramList.next();
      Object localObject2 = localObject1;
      localObject2 = (cb)localObject1;
      String str = a;
      if (str != null)
      {
        localObject2 = a;
        str = b;
        bool2 = k.a(localObject2, str);
        if (bool2)
        {
          bool2 = true;
          continue;
        }
      }
      bool2 = false;
      localObject2 = null;
    } while (!bool2);
    break label108;
    boolean bool1 = false;
    localObject1 = null;
    label108:
    return (cb)localObject1;
  }
  
  public final cb c(List paramList)
  {
    Object localObject1 = "subscriptions";
    k.b(paramList, (String)localObject1);
    paramList = ((Iterable)paramList).iterator();
    boolean bool2;
    do
    {
      bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = paramList.next();
      Object localObject2 = localObject1;
      localObject2 = (cb)localObject1;
      String str = a;
      if (str != null)
      {
        localObject2 = a;
        str = c;
        bool2 = k.a(localObject2, str);
        if (bool2)
        {
          bool2 = true;
          continue;
        }
      }
      bool2 = false;
      localObject2 = null;
    } while (!bool2);
    break label108;
    boolean bool1 = false;
    localObject1 = null;
    label108:
    return (cb)localObject1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof x;
      if (bool1)
      {
        paramObject = (x)paramObject;
        String str1 = a;
        String str2 = a;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = b;
          str2 = b;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = c;
            paramObject = c;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final int hashCode()
  {
    String str1 = a;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = b;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = c;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ProductIds(monthly=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", yearly=");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", gold=");
    str = c;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */