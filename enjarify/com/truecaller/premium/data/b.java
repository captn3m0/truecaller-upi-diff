package com.truecaller.premium.data;

public final class b
{
  final boolean a;
  final int b;
  
  public b(boolean paramBoolean, int paramInt)
  {
    a = paramBoolean;
    b = paramInt;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof b;
      if (bool2)
      {
        paramObject = (b)paramObject;
        bool2 = a;
        boolean bool3 = a;
        if (bool2 == bool3) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        if (bool2)
        {
          int i = b;
          int j = b;
          if (i == j)
          {
            j = 1;
          }
          else
          {
            j = 0;
            paramObject = null;
          }
          if (j != 0) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    boolean bool = a;
    if (bool) {
      bool = true;
    }
    int i;
    bool *= true;
    int j = b;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PremiumBlockingFeature(isEnabled=");
    boolean bool = a;
    localStringBuilder.append(bool);
    localStringBuilder.append(", descriptionRes=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */