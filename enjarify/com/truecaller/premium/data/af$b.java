package com.truecaller.premium.data;

import c.g.b.k;
import com.truecaller.common.f.a;
import com.truecaller.premium.cb;

public final class af$b
  extends af
{
  public final a a;
  public final boolean b;
  public final cb c;
  public final cb d;
  public final cb e;
  public final v f;
  
  public af$b(a parama, boolean paramBoolean, cb paramcb1, cb paramcb2, cb paramcb3, v paramv)
  {
    super((byte)0);
    a = parama;
    b = paramBoolean;
    c = paramcb1;
    d = paramcb2;
    e = paramcb3;
    f = paramv;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof b;
      if (bool2)
      {
        paramObject = (b)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          bool2 = b;
          boolean bool3 = b;
          if (bool2 == bool3)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            localObject1 = null;
          }
          if (bool2)
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              localObject1 = d;
              localObject2 = d;
              bool2 = k.a(localObject1, localObject2);
              if (bool2)
              {
                localObject1 = e;
                localObject2 = e;
                bool2 = k.a(localObject1, localObject2);
                if (bool2)
                {
                  localObject1 = f;
                  paramObject = f;
                  boolean bool4 = k.a(localObject1, paramObject);
                  if (bool4) {
                    return bool1;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    a locala = a;
    int i = 0;
    if (locala != null)
    {
      j = locala.hashCode();
    }
    else
    {
      j = 0;
      locala = null;
    }
    j *= 31;
    int k = b;
    if (k != 0) {
      k = 1;
    }
    int j = (j + k) * 31;
    Object localObject = c;
    int m;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = d;
    int n;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    j = (j + n) * 31;
    localObject = e;
    int i1;
    if (localObject != null)
    {
      i1 = localObject.hashCode();
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    j = (j + i1) * 31;
    localObject = f;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("DataFetched(premium=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", upgradeToGoldAvailable=");
    boolean bool = b;
    localStringBuilder.append(bool);
    localStringBuilder.append(", monthlySubscription=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", yearlySubscription=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", goldSubscription=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", theme=");
    localObject = f;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.af.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */