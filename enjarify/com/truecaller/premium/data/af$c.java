package com.truecaller.premium.data;

import c.g.b.k;
import java.util.List;

public final class af$c
  extends af
{
  public final List a;
  
  public af$c(List paramList)
  {
    super((byte)0);
    a = paramList;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof c;
      if (bool1)
      {
        paramObject = (c)paramObject;
        List localList = a;
        paramObject = a;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    List localList = a;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("MoreThanOneReceiptError(receipts=");
    List localList = a;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.af.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */