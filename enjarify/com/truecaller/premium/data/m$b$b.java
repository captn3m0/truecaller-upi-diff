package com.truecaller.premium.data;

import c.g.b.k;

public final class m$b$b
  extends m.b
{
  public final x a;
  final v b;
  
  public m$b$b(x paramx, v paramv)
  {
    super((byte)0);
    a = paramx;
    b = paramv;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof b;
      if (bool1)
      {
        paramObject = (b)paramObject;
        Object localObject = a;
        x localx = a;
        bool1 = k.a(localObject, localx);
        if (bool1)
        {
          localObject = b;
          paramObject = b;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    x localx = a;
    int i = 0;
    int j;
    if (localx != null)
    {
      j = localx.hashCode();
    }
    else
    {
      j = 0;
      localx = null;
    }
    j *= 31;
    v localv = b;
    if (localv != null) {
      i = localv.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Success(productIds=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", theme=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.m.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */