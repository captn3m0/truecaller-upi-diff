package com.truecaller.premium.data;

import android.net.Uri;
import c.g.b.k;

public final class a
{
  public final Uri a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  
  public a(Uri paramUri, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    a = paramUri;
    b = paramString1;
    c = paramString2;
    d = paramString3;
    e = paramString4;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = c;
            localObject2 = c;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = d;
              localObject2 = d;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = e;
                paramObject = e;
                boolean bool2 = k.a(localObject1, paramObject);
                if (bool2) {
                  break label134;
                }
              }
            }
          }
        }
      }
      return false;
    }
    label134:
    return true;
  }
  
  public final int hashCode()
  {
    Uri localUri = a;
    int i = 0;
    if (localUri != null)
    {
      j = localUri.hashCode();
    }
    else
    {
      j = 0;
      localUri = null;
    }
    j *= 31;
    String str = b;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    int j = (j + k) * 31;
    str = c;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    j = (j + k) * 31;
    str = d;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    j = (j + k) * 31;
    str = e;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("GoldCallerIdPreviewData(photoUri=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", title=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", subTitle=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", number=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", numberType=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */