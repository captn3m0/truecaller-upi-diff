package com.truecaller.premium.data;

import android.app.Activity;
import c.d.a.a;
import c.d.a.b;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.premium.a.d;
import com.truecaller.premium.a.d.b;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.j;

final class t$f
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag g;
  
  t$f(t paramt, d paramd, Activity paramActivity, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/premium/data/t$f;
    t localt = c;
    d locald = d;
    Activity localActivity = e;
    String str = f;
    localf.<init>(localt, locald, localActivity, str, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label214;
      }
      a = this;
      int j = 1;
      b = j;
      Object localObject1 = new kotlinx/coroutines/k;
      Object localObject2 = b.a(this);
      ((kotlinx.coroutines.k)localObject1).<init>((c)localObject2, j);
      paramObject = localObject1;
      paramObject = (j)localObject1;
      localObject2 = d;
      Activity localActivity = e;
      String str1 = f;
      String str2 = t.b(c);
      Object localObject3 = new com/truecaller/premium/data/t$f$a;
      ((t.f.a)localObject3).<init>((j)paramObject);
      localObject3 = (d.b)localObject3;
      ((d)localObject2).a(localActivity, str1, str2, (d.b)localObject3);
      paramObject = ((kotlinx.coroutines.k)localObject1).h();
      localObject1 = a.a;
      if (paramObject == localObject1)
      {
        localObject1 = "frame";
        c.g.b.k.b(this, (String)localObject1);
      }
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return paramObject;
    label214:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.t.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */