package com.truecaller.premium.data;

import android.support.v4.f.j;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.common.f.a.a;
import com.truecaller.common.f.c.b;
import com.truecaller.common.f.c.c;
import com.truecaller.common.h.am;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.engagementrewards.ui.d;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings;
import org.a.a.d.i.a;

public final class p
  implements com.truecaller.common.f.c
{
  private final com.truecaller.androidactors.k a;
  private final com.truecaller.androidactors.f b;
  private final com.truecaller.androidactors.f c;
  private final com.truecaller.common.g.a d;
  private final com.truecaller.filters.p e;
  private final com.truecaller.engagementrewards.c f;
  private final d g;
  private final com.truecaller.engagementrewards.g h;
  private final e i;
  
  public p(com.truecaller.androidactors.f paramf1, com.truecaller.androidactors.f paramf2, com.truecaller.androidactors.k paramk, com.truecaller.common.g.a parama, com.truecaller.filters.p paramp, com.truecaller.engagementrewards.c paramc, d paramd, com.truecaller.engagementrewards.g paramg, e parame)
  {
    b = paramf1;
    c = paramf2;
    a = paramk;
    d = parama;
    e = paramp;
    f = paramc;
    g = paramd;
    h = paramg;
    i = parame;
  }
  
  private static int a(boolean paramBoolean, z paramz)
  {
    if (!paramBoolean) {
      return -1;
    }
    if (paramz == null) {
      return -2;
    }
    return 0;
  }
  
  private static long a(String paramString)
  {
    boolean bool = org.c.a.a.a.k.b(paramString);
    if (bool) {
      return 0L;
    }
    return aba;
  }
  
  private static com.truecaller.common.f.a a(z paramz)
  {
    if (paramz != null)
    {
      Object localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = b.a;
        int j = Integer.parseInt((String)localObject1);
        String str1 = b.b;
        long l1 = a(str1);
        String str2 = b.c;
        long l2 = a(str2);
        String str3 = b.d;
        int k = b(str3);
        Object localObject2 = b.e;
        if (localObject2 != null) {
          localObject2 = b.e.a;
        } else {
          localObject2 = null;
        }
        String str4 = c(b.f);
        a.a locala = new com/truecaller/common/f/a$a;
        locala.<init>();
        a = j;
        b = l1;
        c = l2;
        d = k;
        f = ((String)localObject2);
        g = str4;
        paramz = b.g;
        e = paramz;
        return locala.a();
      }
    }
    paramz = new com/truecaller/common/f/a$a;
    paramz.<init>();
    return paramz.a();
  }
  
  private void a(j paramj, String paramString, c.c paramc)
  {
    Object localObject = (Boolean)a;
    boolean bool1 = ((Boolean)localObject).booleanValue();
    paramj = (z)b;
    com.truecaller.common.f.a locala = a(paramj);
    if (bool1) {
      a(locala);
    }
    if (paramc == null) {
      return;
    }
    int j;
    int k;
    if (!bool1)
    {
      j = -1;
    }
    else if (paramj == null)
    {
      j = -2;
    }
    else
    {
      paramj = a;
      localObject = "Successful";
      bool1 = am.b(paramj, (CharSequence)localObject);
      if (bool1)
      {
        j = 0;
        paramj = null;
      }
      else
      {
        localObject = "ExistsAnotherUser";
        bool1 = am.b(paramj, (CharSequence)localObject);
        if (bool1)
        {
          j = 2;
        }
        else
        {
          localObject = "ExistsSameUser";
          bool1 = am.b(paramj, (CharSequence)localObject);
          if (bool1)
          {
            j = 3;
          }
          else
          {
            localObject = "NotPremiumOwnerDevice";
            boolean bool2 = am.b(paramj, (CharSequence)localObject);
            if (bool2) {
              k = 4;
            } else {
              k = 1;
            }
          }
        }
      }
    }
    paramc.a(k, paramString, locala);
  }
  
  private void a(com.truecaller.common.f.a parama)
  {
    long l1 = b;
    boolean bool1 = a(l1);
    long l2 = c;
    boolean bool2 = a(l2);
    String str1 = null;
    if ((bool1) && (bool2))
    {
      d.d("premiumDuration");
      Settings.f("premiumGraceExpiration");
      a(0);
      d.d("subscriptionStatus");
      d.d("subscriptionErrorResolveUrl");
      d.d("subscriptionPaymentFailedViewShownOnce");
      localObject1 = d;
      str2 = "subscriptionStatusChangedReason";
      ((com.truecaller.common.g.a)localObject1).d(str2);
    }
    else
    {
      long l3 = b;
      l4 = System.currentTimeMillis();
      l3 -= l4;
      l4 = 1000L;
      l3 /= l4;
      localObject1 = d;
      String str3 = "premiumDuration";
      ((com.truecaller.common.g.a)localObject1).b(str3, l3);
      localObject1 = "premiumGraceExpiration";
      l3 = c;
      Settings.a((String)localObject1, l3);
      int j = a;
      a(j);
    }
    Object localObject1 = "premiumRenewable";
    int k = d;
    int m = 1;
    if (k == m)
    {
      k = 1;
    }
    else
    {
      k = 0;
      str2 = null;
    }
    Settings.a((String)localObject1, k);
    Settings.g("premiumTimestamp");
    localObject1 = d;
    long l4 = System.currentTimeMillis();
    ((com.truecaller.common.g.a)localObject1).b("premiumLastFetchDate", l4);
    String str2 = g;
    Settings.b("premiumLevel", str2);
    localObject1 = i.aq();
    boolean bool3 = ((com.truecaller.featuretoggles.b)localObject1).a();
    label335:
    Object localObject2;
    if (bool3)
    {
      localObject1 = e;
      if (localObject1 != null)
      {
        parama = e;
        bool4 = parama.booleanValue();
        if (bool4)
        {
          bool4 = true;
          break label335;
        }
      }
      bool4 = false;
      parama = null;
      localObject1 = new String[m];
      String str4 = String.valueOf(bool4);
      str2 = "Engagement Rewards: updatePremium:: isFreeTrial: ".concat(str4);
      localObject1[0] = str2;
      localObject1 = d;
      str1 = "subscriptionStatusChangedIsFreeTrial";
      ((com.truecaller.common.g.a)localObject1).b(str1, bool4);
      bool3 = d();
      if ((bool3) && (!bool1) && (!bool4))
      {
        parama = f;
        localObject2 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
        parama = parama.a((EngagementRewardActionType)localObject2);
        localObject2 = EngagementRewardState.PENDING;
        if (parama == localObject2)
        {
          parama = f;
          localObject2 = d;
          localObject1 = "subscriptionPurchaseSku";
          localObject2 = ((com.truecaller.common.g.a)localObject2).a((String)localObject1);
          bool4 = parama.a((String)localObject2);
          if (bool4)
          {
            parama = h;
            localObject2 = d.a("subscriptionPurchaseSource");
            parama.a((String)localObject2);
            parama = f;
            localObject2 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
            localObject1 = EngagementRewardState.COMPLETED;
            parama.a((EngagementRewardActionType)localObject2, (EngagementRewardState)localObject1);
            parama = g;
            parama.a();
          }
        }
      }
    }
    boolean bool4 = d();
    if (!bool4)
    {
      e.a(null);
      return;
    }
    parama = e.k();
    if (parama == null)
    {
      parama = e;
      localObject2 = Boolean.TRUE;
      parama.a((Boolean)localObject2);
    }
  }
  
  private static int b(String paramString)
  {
    boolean bool1 = org.c.a.a.a.k.b(paramString);
    if (bool1) {
      return 0;
    }
    boolean bool2 = org.c.a.a.a.b.a(paramString);
    if (bool2) {
      return 1;
    }
    return 2;
  }
  
  private static String c(String paramString)
  {
    String str = "regular";
    boolean bool1 = am.b(paramString, str);
    if (bool1) {
      return "regular";
    }
    str = "gold";
    boolean bool2 = am.b(paramString, str);
    if (bool2) {
      return "gold";
    }
    return "none";
  }
  
  private boolean p()
  {
    long l1 = q();
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      String str = "premiumTimestamp";
      boolean bool2 = Settings.b(str, l1);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  private long q()
  {
    return d.a("premiumDuration", 0L) * 1000L;
  }
  
  public final int a()
  {
    String str = "premiumRenewable";
    boolean bool = Settings.a(str);
    if (!bool) {
      return 0;
    }
    str = "premiumRenewable";
    bool = Settings.b(str, false);
    if (bool) {
      return 1;
    }
    return 2;
  }
  
  public final void a(int paramInt)
  {
    long l = paramInt;
    Settings.a("premiumRequests", l);
  }
  
  public final void a(String paramString1, String paramString2, c.b paramb)
  {
    String str1 = com.truecaller.profile.c.b(d);
    Object localObject1 = c.a();
    Object localObject2 = localObject1;
    localObject2 = (com.truecaller.network.util.f)localObject1;
    String str2 = k();
    paramString1 = ((com.truecaller.network.util.f)localObject2).a(str1, paramString2, "Unable to purchase Truecaller Professional", paramString1, str2);
    paramString2 = a.a();
    localObject1 = new com/truecaller/premium/data/-$$Lambda$p$-XlnCRH8s5Yax-cu3Xt0BtUrikM;
    ((-..Lambda.p.-XlnCRH8s5Yax-cu3Xt0BtUrikM)localObject1).<init>(paramb);
    paramString1.a(paramString2, (ac)localObject1);
  }
  
  public final void a(String paramString1, String paramString2, c.c paramc)
  {
    paramString2 = ((g)b.a()).a(paramString1, paramString2);
    i locali = a.a();
    -..Lambda.p.mvg2nsCNeThCI5F-OpnlO_krlLI localmvg2nsCNeThCI5F-OpnlO_krlLI = new com/truecaller/premium/data/-$$Lambda$p$mvg2nsCNeThCI5F-OpnlO_krlLI;
    localmvg2nsCNeThCI5F-OpnlO_krlLI.<init>(this, paramString1, paramc);
    paramString2.a(locali, localmvg2nsCNeThCI5F-OpnlO_krlLI);
  }
  
  public final boolean a(long paramLong)
  {
    long l = System.currentTimeMillis();
    boolean bool = paramLong < l;
    return bool;
  }
  
  public final j b()
  {
    try
    {
      Object localObject1 = b;
      localObject1 = ((com.truecaller.androidactors.f)localObject1).a();
      localObject1 = (g)localObject1;
      localObject1 = ((g)localObject1).a();
      localObject1 = ((w)localObject1).d();
      localObject1 = (j)localObject1;
      boolean bool = false;
      Object localObject2 = null;
      localObject2 = new String[0];
      AssertionUtil.isNotNull(localObject1, (String[])localObject2);
      localObject2 = a;
      localObject2 = (Boolean)localObject2;
      bool = ((Boolean)localObject2).booleanValue();
      localObject1 = b;
      localObject1 = (z)localObject1;
      com.truecaller.common.f.a locala = a((z)localObject1);
      int j = a(bool, (z)localObject1);
      if (j == 0) {
        a(locala);
      }
      localObject1 = Integer.valueOf(j);
      return j.a(localObject1, locala);
    }
    catch (InterruptedException localInterruptedException) {}
    return null;
  }
  
  public final void b(String paramString1, String paramString2, c.c paramc)
  {
    paramString2 = ((g)b.a()).b(paramString1, paramString2);
    i locali = a.a();
    -..Lambda.p.7VGc8q1eBIz3VblmELpeAQEjWaE local7VGc8q1eBIz3VblmELpeAQEjWaE = new com/truecaller/premium/data/-$$Lambda$p$7VGc8q1eBIz3VblmELpeAQEjWaE;
    local7VGc8q1eBIz3VblmELpeAQEjWaE.<init>(this, paramString1, paramc);
    paramString2.a(locali, local7VGc8q1eBIz3VblmELpeAQEjWaE);
  }
  
  public final void c()
  {
    w localw = ((g)b.a()).a();
    i locali = a.a();
    -..Lambda.p.fhcQ1lmar-c5o4pncGesNRAI8_g localfhcQ1lmar-c5o4pncGesNRAI8_g = new com/truecaller/premium/data/-$$Lambda$p$fhcQ1lmar-c5o4pncGesNRAI8_g;
    localfhcQ1lmar-c5o4pncGesNRAI8_g.<init>(this, null);
    localw.a(locali, localfhcQ1lmar-c5o4pncGesNRAI8_g);
  }
  
  public final boolean d()
  {
    boolean bool = p();
    if (bool)
    {
      bool = g();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public final long e()
  {
    long l1 = Settings.d("premiumTimestamp").longValue();
    long l2 = q();
    return l1 + l2;
  }
  
  public final String f()
  {
    return d.a("profileEmail");
  }
  
  public final boolean g()
  {
    boolean bool = p();
    if (bool)
    {
      Long localLong = Settings.d("premiumGraceExpiration");
      long l = localLong.longValue();
      bool = a(l);
      if (!bool) {
        return true;
      }
    }
    return false;
  }
  
  public final long h()
  {
    return Settings.d("premiumGraceExpiration").longValue();
  }
  
  public final int i()
  {
    return Settings.d("premiumRequests").intValue();
  }
  
  public final boolean j()
  {
    return d.e("premiumLastFetchDate");
  }
  
  public final String k()
  {
    String str1 = "none";
    String str2 = Settings.a("premiumLevel", str1);
    boolean bool = d();
    if (bool)
    {
      str1 = "none";
      bool = str2.equals(str1);
      if (bool) {
        str2 = "regular";
      }
    }
    return str2;
  }
  
  public final boolean l()
  {
    boolean bool = Settings.i();
    return !bool;
  }
  
  public final boolean m()
  {
    return am.b(d.a("subscriptionStatus"), "hold");
  }
  
  public final boolean n()
  {
    String str1 = d.a("subscriptionStatus");
    String str2 = "InActive";
    boolean bool = am.b(str1, str2);
    if (bool)
    {
      str1 = d.a("subscriptionStatusChangedReason");
      str2 = "SUBSCRIPTION_CANCELED";
      bool = am.b(str1, str2);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean o()
  {
    boolean bool = m();
    if (!bool)
    {
      bool = n();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */