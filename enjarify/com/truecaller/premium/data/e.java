package com.truecaller.premium.data;

import c.g.b.k;
import java.util.List;

public final class e
{
  public final String a;
  public final int b;
  public final int c;
  public final int d;
  public final List e;
  public final int f;
  public final a g;
  
  public e(String paramString, int paramInt1, int paramInt2, int paramInt3, List paramList, int paramInt4, a parama)
  {
    a = paramString;
    b = paramInt1;
    c = paramInt2;
    d = paramInt3;
    e = paramList;
    f = paramInt4;
    g = parama;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof e;
      if (bool2)
      {
        paramObject = (e)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          int i = b;
          int k = b;
          if (i == k)
          {
            i = 1;
          }
          else
          {
            i = 0;
            localObject1 = null;
          }
          if (i != 0)
          {
            i = c;
            k = c;
            if (i == k)
            {
              i = 1;
            }
            else
            {
              i = 0;
              localObject1 = null;
            }
            if (i != 0)
            {
              i = d;
              k = d;
              if (i == k)
              {
                i = 1;
              }
              else
              {
                i = 0;
                localObject1 = null;
              }
              if (i != 0)
              {
                localObject1 = e;
                localObject2 = e;
                boolean bool3 = k.a(localObject1, localObject2);
                if (bool3)
                {
                  int j = f;
                  k = f;
                  if (j == k)
                  {
                    j = 1;
                  }
                  else
                  {
                    j = 0;
                    localObject1 = null;
                  }
                  if (j != 0)
                  {
                    localObject1 = g;
                    paramObject = g;
                    boolean bool4 = k.a(localObject1, paramObject);
                    if (bool4) {
                      return bool1;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    int k = b;
    int j = (j + k) * 31;
    k = c;
    j = (j + k) * 31;
    k = d;
    j = (j + k) * 31;
    Object localObject = e;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    k = f;
    j = (j + k) * 31;
    localObject = g;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PremiumFeatureViewModel(pageName=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", titleRes=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", listIconRes=");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", shortDescriptionRes=");
    i = d;
    localStringBuilder.append(i);
    localStringBuilder.append(", descriptionsRes=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", detailsIconRes=");
    i = f;
    localStringBuilder.append(i);
    localStringBuilder.append(", goldCallerIdPreviewData=");
    localObject = g;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */