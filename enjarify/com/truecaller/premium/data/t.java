package com.truecaller.premium.data;

import c.d.f;
import c.g.a.b;
import c.g.b.k;
import com.truecaller.premium.a.d;
import com.truecaller.premium.bz;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.g;

public final class t
  implements s
{
  public static final t.a b;
  final f a;
  private List c;
  private String d;
  private af e;
  private final com.truecaller.common.f.c f;
  private final m g;
  private final d h;
  private final f i;
  
  static
  {
    t.a locala = new com/truecaller/premium/data/t$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public t(com.truecaller.common.f.c paramc, m paramm, d paramd, f paramf1, f paramf2)
  {
    f = paramc;
    g = paramm;
    h = paramd;
    i = paramf1;
    a = paramf2;
  }
  
  private static af b(Integer paramInteger, af paramaf)
  {
    if (paramInteger == null) {
      return (af)af.h.a;
    }
    int j = paramInteger.intValue();
    int k = -1;
    if (j == k) {
      return (af)af.e.a;
    }
    int m = paramInteger.intValue();
    j = -2;
    if (m == j) {
      return (af)af.h.a;
    }
    if (paramaf == null)
    {
      paramInteger = af.h.a;
      paramaf = paramInteger;
      paramaf = (af)paramInteger;
    }
    return paramaf;
  }
  
  public final Object a(c.d.c paramc)
  {
    f localf = i;
    Object localObject = new com/truecaller/premium/data/t$c;
    ((t.c)localObject).<init>(this, null);
    localObject = (c.g.a.m)localObject;
    return g.a(localf, (c.g.a.m)localObject, paramc);
  }
  
  public final Object a(b paramb, c.d.c paramc)
  {
    f localf = a;
    Object localObject = new com/truecaller/premium/data/t$d;
    ((t.d)localObject).<init>(this, paramb, null);
    localObject = (c.g.a.m)localObject;
    return g.a(localf, (c.g.a.m)localObject, paramc);
  }
  
  public final Object a(s.a parama, String paramString, c.g.a.a parama1, c.d.c paramc)
  {
    f localf = a;
    Object localObject = new com/truecaller/premium/data/t$e;
    ((t.e)localObject).<init>(this, parama, paramString, parama1, null);
    localObject = (c.g.a.m)localObject;
    return g.a(localf, (c.g.a.m)localObject, paramc);
  }
  
  public final boolean a()
  {
    af localaf = e;
    return localaf != null;
  }
  
  public final void b()
  {
    e = null;
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        bz localbz = (bz)((Iterator)localObject1).next();
        Object localObject2 = f;
        String str1 = b;
        Object localObject3 = c;
        ((com.truecaller.common.f.c)localObject2).a(str1, (String)localObject3, null);
        localObject2 = new java/util/Date;
        long l1 = e;
        ((Date)localObject2).<init>(l1);
        localObject2 = org.c.a.a.a.d.a.a((Date)localObject2);
        str1 = "DateUtils.addDays(Date(i…DURATION_PREMIUM_01_DAYS)";
        k.a(localObject2, str1);
        long l2 = ((Date)localObject2).getTime();
        localObject3 = a;
        String str2 = "premium_01";
        boolean bool2 = k.a(localObject3, str2);
        if (bool2)
        {
          localObject3 = f;
          boolean bool3 = ((com.truecaller.common.f.c)localObject3).a(l2);
          if (bool3)
          {
            localObject2 = h;
            ((d)localObject2).a(localbz);
          }
        }
      }
    }
    h.d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */