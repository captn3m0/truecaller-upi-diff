package com.truecaller.premium.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.google.gson.f;
import com.truecaller.common.h.an;

public final class k
  implements j
{
  private final SharedPreferences a;
  private final f b;
  private final an c;
  
  public k(Context paramContext, an paraman)
  {
    c = paraman;
    paramContext = paramContext.getSharedPreferences("premium_products_cache", 0);
    a = paramContext;
    paramContext = new com/google/gson/f;
    paramContext.<init>();
    b = paramContext;
  }
  
  public final y a()
  {
    Object localObject1 = a;
    boolean bool = ((SharedPreferences)localObject1).contains("cache_ttl");
    int i = 0;
    Object localObject2 = null;
    if (bool)
    {
      localObject1 = a;
      localObject3 = "last_timestamp";
      bool = ((SharedPreferences)localObject1).contains((String)localObject3);
      if (bool)
      {
        localObject1 = a;
        localObject3 = "dto";
        bool = ((SharedPreferences)localObject1).contains((String)localObject3);
        if (bool)
        {
          localObject1 = c;
          localObject3 = a;
          String str1 = "last_timestamp";
          long l1 = 0L;
          long l2 = ((SharedPreferences)localObject3).getLong(str1, l1);
          SharedPreferences localSharedPreferences = a;
          String str2 = "cache_ttl";
          l1 = localSharedPreferences.getLong(str2, l1);
          bool = ((an)localObject1).a(l2, l1);
          if (!bool) {
            i = 1;
          }
        }
      }
    }
    bool = false;
    localObject1 = null;
    if (i == 0) {
      return null;
    }
    localObject2 = a;
    Object localObject3 = "dto";
    localObject2 = ((SharedPreferences)localObject2).getString((String)localObject3, null);
    if (localObject2 == null) {
      return null;
    }
    return (y)b.a((String)localObject2, y.class);
  }
  
  public final void a(y paramy)
  {
    c.g.b.k.b(paramy, "dto");
    SharedPreferences.Editor localEditor = a.edit();
    long l = System.currentTimeMillis();
    localEditor = localEditor.putLong("last_timestamp", l);
    l = c;
    localEditor = localEditor.putLong("cache_ttl", l);
    paramy = b.b(paramy);
    localEditor.putString("dto", paramy).apply();
  }
  
  public final void b()
  {
    a.edit().remove("last_timestamp").remove("cache_ttl").remove("dto").apply();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */