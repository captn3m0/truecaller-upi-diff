package com.truecaller.premium.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.truecaller.common.h.am;

public class SubscriptionPromoEventMetaData
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private String a;
  private String b;
  
  static
  {
    SubscriptionPromoEventMetaData.1 local1 = new com/truecaller/premium/data/SubscriptionPromoEventMetaData$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  protected SubscriptionPromoEventMetaData(Parcel paramParcel)
  {
    String str = am.n(paramParcel.readString());
    a = str;
    paramParcel = paramParcel.readString();
    b = paramParcel;
  }
  
  public SubscriptionPromoEventMetaData(String paramString1, String paramString2)
  {
    a = paramString1;
    b = paramString2;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final String b()
  {
    return b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    String str = a;
    paramParcel.writeString(str);
    str = b;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.SubscriptionPromoEventMetaData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */