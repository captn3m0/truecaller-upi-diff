package com.truecaller.premium.data;

import android.app.Activity;
import c.d.f;
import c.g.a.m;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.premium.a.d;
import com.truecaller.premium.bz;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class t$e
  extends c.d.b.a.k
  implements m
{
  int a;
  Object b;
  int c;
  private ag h;
  
  t$e(t paramt, s.a parama, String paramString, c.g.a.a parama1, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/premium/data/t$e;
    t localt = d;
    s.a locala = e;
    String str = f;
    c.g.a.a locala1 = g;
    locale.<init>(localt, locala, str, locala1, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = c;
    int k = 3;
    int m = 2;
    boolean bool1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      localObject1 = (bz)b;
      bool1 = paramObject instanceof o.b;
      if (!bool1) {
        break label462;
      }
      throw a;
    case 2: 
      bool1 = paramObject instanceof o.b;
      if (!bool1) {
        break label299;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label687;
      }
      paramObject = t.e(d);
      boolean bool2 = ((d)paramObject).a();
      if (!bool2)
      {
        paramObject = t.f(d);
        localObject2 = new com/truecaller/premium/data/t$e$1;
        localObject3 = null;
        ((t.e.1)localObject2).<init>(this, null);
        localObject2 = (m)localObject2;
        int i1 = 1;
        c = i1;
        paramObject = g.a((f)paramObject, (m)localObject2, this);
        if (paramObject == localObject1) {
          return localObject1;
        }
      }
      break;
    }
    Object localObject4 = d;
    Object localObject5 = t.e((t)localObject4);
    Object localObject6 = e.provideActivity();
    String str = f;
    c = m;
    paramObject = a;
    Object localObject2 = new com/truecaller/premium/data/t$f;
    Object localObject3 = localObject2;
    ((t.f)localObject2).<init>((t)localObject4, (d)localObject5, (Activity)localObject6, str, null);
    localObject2 = (m)localObject2;
    paramObject = g.a((f)paramObject, (m)localObject2, this);
    if (paramObject == localObject1) {
      return localObject1;
    }
    label299:
    paramObject = (n)paramObject;
    localObject2 = (Number)a;
    int j = ((Number)localObject2).intValue();
    paramObject = (bz)b;
    switch (j)
    {
    default: 
      return (aa)aa.a.a;
    case 1: 
      return (aa)aa.d.a;
    }
    g.invoke();
    localObject3 = d;
    localObject4 = t.c((t)localObject3);
    localObject5 = b;
    c.g.b.k.a(localObject5, "purchaseReceipt.data");
    localObject6 = c;
    str = "purchaseReceipt.signature";
    c.g.b.k.a(localObject6, str);
    a = j;
    b = paramObject;
    c = k;
    localObject2 = ((t)localObject3).a((com.truecaller.common.f.c)localObject4, (String)localObject5, (String)localObject6, this);
    if (localObject2 == localObject1) {
      return localObject1;
    }
    localObject1 = paramObject;
    paramObject = localObject2;
    label462:
    paramObject = (t.b)paramObject;
    j = a;
    localObject3 = b;
    paramObject = c;
    if ((j != 0) && (j != k))
    {
      if (j != m)
      {
        int n = 4;
        if (j != n)
        {
          switch (j)
          {
          default: 
            paramObject = new com/truecaller/premium/data/aa$e;
            ((aa.e)paramObject).<init>(j, (String)localObject3);
            return (aa)paramObject;
          case -1: 
            return (aa)aa.c.a;
          }
          return (aa)aa.g.a;
        }
      }
      paramObject = new com/truecaller/premium/data/aa$b;
      ((aa.b)paramObject).<init>((bz)localObject1);
      return (aa)paramObject;
    }
    localObject1 = t.c(d);
    long l = b;
    boolean bool4 = ((com.truecaller.common.f.c)localObject1).a(l);
    if (bool4)
    {
      localObject1 = t.c(d);
      l = c;
      boolean bool3 = ((com.truecaller.common.f.c)localObject1).a(l);
      if (bool3)
      {
        paramObject = new com/truecaller/premium/data/aa$e;
        ((aa.e)paramObject).<init>(j, (String)localObject3);
        return (aa)paramObject;
      }
    }
    paramObject = new com/truecaller/premium/data/aa$f;
    localObject1 = f;
    localObject2 = t.b(d);
    ((aa.f)paramObject).<init>((String)localObject1, (String)localObject2);
    return (aa)paramObject;
    label687:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.t.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */