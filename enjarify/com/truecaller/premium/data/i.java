package com.truecaller.premium.data;

import android.support.v4.f.j;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import e.b;
import e.r;
import java.io.IOException;
import java.net.UnknownHostException;
import okhttp3.ac;
import okhttp3.ad;

public final class i
  implements g
{
  public final com.truecaller.androidactors.w a()
  {
    Object localObject1 = ((q)h.a(KnownEndpoints.PREMIUM, q.class)).a();
    try
    {
      localObject1 = ((b)localObject1).c();
      Object localObject3 = a;
      boolean bool1 = ((ad)localObject3).c();
      if (bool1)
      {
        localObject3 = b;
        if (localObject3 != null)
        {
          localObject1 = b;
          localObject1 = (z)localObject1;
          localObject3 = Boolean.TRUE;
          localObject1 = j.a(localObject3, localObject1);
          return com.truecaller.androidactors.w.b(localObject1);
        }
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, null));
    }
    catch (IOException localIOException)
    {
      boolean bool2 = localIOException instanceof UnknownHostException;
      if (!bool2)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        Object localObject2 = null;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.valueOf(bool2), null));
    }
  }
  
  public final com.truecaller.androidactors.w a(String paramString)
  {
    KnownEndpoints localKnownEndpoints = KnownEndpoints.PREMIUM;
    Object localObject = q.class;
    paramString = ((q)h.a(localKnownEndpoints, (Class)localObject)).a(paramString);
    localKnownEndpoints = null;
    try
    {
      paramString = paramString.c();
      localObject = a;
      boolean bool1 = ((ad)localObject).c();
      if (bool1)
      {
        localObject = b;
        if (localObject != null)
        {
          paramString = b;
          paramString = (y)paramString;
          localObject = Boolean.TRUE;
          paramString = j.a(localObject, paramString);
          return com.truecaller.androidactors.w.b(paramString);
        }
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, null));
    }
    catch (IOException paramString)
    {
      boolean bool2 = paramString instanceof UnknownHostException;
      if (!bool2)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        paramString = null;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.valueOf(bool2), null));
    }
  }
  
  public final com.truecaller.androidactors.w a(String paramString1, String paramString2)
  {
    paramString1 = ac.a(okhttp3.w.b("text/plain"), paramString1);
    Object localObject = KnownEndpoints.PREMIUM;
    Class localClass = q.class;
    localObject = (q)h.a((KnownEndpoints)localObject, localClass);
    paramString1 = ((q)localObject).a(paramString1, paramString2);
    paramString2 = null;
    try
    {
      paramString1 = paramString1.c();
      localObject = a;
      boolean bool1 = ((ad)localObject).c();
      if (bool1)
      {
        localObject = b;
        if (localObject != null)
        {
          paramString1 = b;
          paramString1 = (z)paramString1;
          localObject = Boolean.TRUE;
          paramString1 = j.a(localObject, paramString1);
          return com.truecaller.androidactors.w.b(paramString1);
        }
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, null));
    }
    catch (IOException paramString1)
    {
      boolean bool2 = paramString1 instanceof UnknownHostException;
      if (!bool2)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        paramString1 = null;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.valueOf(bool2), null));
    }
  }
  
  public final com.truecaller.androidactors.w b(String paramString1, String paramString2)
  {
    paramString1 = ac.a(okhttp3.w.b("text/plain"), paramString1);
    Object localObject = KnownEndpoints.PREMIUM;
    Class localClass = q.class;
    localObject = (q)h.a((KnownEndpoints)localObject, localClass);
    paramString1 = ((q)localObject).b(paramString1, paramString2);
    paramString2 = null;
    try
    {
      paramString1 = paramString1.c();
      localObject = a;
      boolean bool1 = ((ad)localObject).c();
      if (bool1)
      {
        localObject = b;
        if (localObject != null)
        {
          paramString1 = b;
          paramString1 = (z)paramString1;
          localObject = Boolean.TRUE;
          paramString1 = j.a(localObject, paramString1);
          return com.truecaller.androidactors.w.b(paramString1);
        }
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, null));
    }
    catch (IOException paramString1)
    {
      boolean bool2 = paramString1 instanceof UnknownHostException;
      if (!bool2)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        paramString1 = null;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.valueOf(bool2), null));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */