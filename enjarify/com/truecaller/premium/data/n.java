package com.truecaller.premium.data;

import android.content.Context;
import android.net.Uri;
import c.u;
import com.google.common.collect.ImmutableList;
import com.truecaller.calling.recorder.h;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.Number;
import com.truecaller.util.at;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public final class n
  implements m
{
  final com.truecaller.androidactors.f a;
  final j b;
  private final Context c;
  private final com.truecaller.util.b.j d;
  private final com.truecaller.whoviewedme.w e;
  private final h f;
  private final com.truecaller.common.f.c g;
  private final com.truecaller.common.g.a h;
  private final com.truecaller.data.entity.g i;
  private final c j;
  private final c.d.f k;
  
  public n(Context paramContext, com.truecaller.androidactors.f paramf, com.truecaller.util.b.j paramj, com.truecaller.whoviewedme.w paramw, h paramh, com.truecaller.common.f.c paramc, com.truecaller.common.g.a parama, com.truecaller.data.entity.g paramg, j paramj1, c paramc1, c.d.f paramf1)
  {
    c = paramContext;
    a = paramf;
    d = paramj;
    e = paramw;
    f = paramh;
    g = paramc;
    h = parama;
    i = paramg;
    b = paramj1;
    j = paramc1;
    k = paramf1;
  }
  
  private static String a(long paramLong)
  {
    Date localDate = new java/util/Date;
    localDate.<init>(paramLong);
    Object localObject = new java/text/SimpleDateFormat;
    Locale localLocale = Locale.getDefault();
    ((SimpleDateFormat)localObject).<init>("yyyy-MM-dd", localLocale);
    localObject = ((SimpleDateFormat)localObject).format(localDate);
    c.g.b.k.a(localObject, "SimpleDateFormat(\"yyyy-M…etDefault()).format(date)");
    return (String)localObject;
  }
  
  private final String b()
  {
    Object localObject1 = g;
    boolean bool = ((com.truecaller.common.f.c)localObject1).g();
    int n = 1;
    if (bool)
    {
      localObject1 = a(g.h());
      localObject2 = c;
      arrayOfObject = new Object[n];
      arrayOfObject[0] = localObject1;
      return ((Context)localObject2).getString(2131886819, arrayOfObject);
    }
    localObject1 = g;
    int m = ((com.truecaller.common.f.c)localObject1).a();
    if (m == n)
    {
      m = 1;
    }
    else
    {
      m = 0;
      localObject1 = null;
    }
    long l = g.e();
    Object localObject2 = a(l);
    Context localContext = c;
    if (m != 0) {
      m = 2131886820;
    } else {
      m = 2131886818;
    }
    Object[] arrayOfObject = new Object[n];
    arrayOfObject[0] = localObject2;
    return localContext.getString(m, arrayOfObject);
  }
  
  private final String c()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("android.resource://");
    String str = c.getPackageName();
    localStringBuilder.append(str);
    localStringBuilder.append("/2131234702");
    return localStringBuilder.toString();
  }
  
  private final String d()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("android.resource://");
    String str = c.getPackageName();
    localStringBuilder.append(str);
    localStringBuilder.append("/2131234701");
    return localStringBuilder.toString();
  }
  
  private final List e()
  {
    n localn = this;
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = f;
    boolean bool1 = ((h)localObject2).a();
    if (bool1)
    {
      localObject2 = new com/truecaller/premium/data/e;
      str1 = "premiumCallRecording";
      m = 2131886827;
      n = 2131233933;
      i2 = 2131886848;
      i3 = 2131886837;
      localObject3 = c.a.m.a(Integer.valueOf(i3));
      i4 = 2131234396;
      localObject4 = localObject2;
      ((e)localObject2).<init>(str1, m, n, i2, (List)localObject3, i4);
      ((ArrayList)localObject1).add(localObject2);
    }
    localObject2 = e;
    bool1 = ((com.truecaller.whoviewedme.w)localObject2).a();
    if (bool1)
    {
      localObject2 = new com/truecaller/premium/data/e;
      str1 = "premiumWhoViewedMe";
      m = 2131886853;
      n = 2131234420;
      i2 = 2131886852;
      localObject3 = c.a.m.a(Integer.valueOf(2131886843));
      i4 = 2131234401;
      localObject4 = localObject2;
      ((e)localObject2).<init>(str1, m, n, i2, (List)localObject3, i4);
      ((ArrayList)localObject1).add(localObject2);
      localObject2 = new com/truecaller/premium/data/e;
      str2 = "premiumIncognitoMode";
      i5 = 2131886844;
      i6 = 2131234204;
      i7 = 2131886850;
      i3 = 2131886840;
      localObject4 = Integer.valueOf(i3);
      localList = c.a.m.a(localObject4);
      i8 = 2131234398;
      localObject5 = localObject2;
      ((e)localObject2).<init>(str2, i5, i6, i7, localList, i8);
      ((ArrayList)localObject1).add(localObject2);
    }
    localObject2 = new com/truecaller/premium/data/e;
    String str1 = "premiumNoAds";
    int m = 2131886845;
    int n = 2131234303;
    int i2 = 2131886851;
    Object localObject3 = c.a.m.a(Integer.valueOf(2131886841));
    int i4 = 2131234399;
    Object localObject4 = localObject2;
    ((e)localObject2).<init>(str1, m, n, i2, (List)localObject3, i4);
    ((ArrayList)localObject1).add(localObject2);
    localObject2 = new com/truecaller/premium/data/e;
    String str2 = "premiumBadge";
    int i5 = 2131886826;
    int i6 = 2131234301;
    int i7 = 2131886847;
    int i3 = 2131886830;
    localObject4 = Integer.valueOf(i3);
    List localList = c.a.m.a(localObject4);
    int i8 = 2131234400;
    Object localObject5 = localObject2;
    ((e)localObject2).<init>(str2, i5, i6, i7, localList, i8);
    ((ArrayList)localObject1).add(localObject2);
    localObject2 = d;
    bool1 = ((com.truecaller.util.b.j)localObject2).b();
    if (bool1)
    {
      localObject2 = new com/truecaller/premium/data/e;
      str1 = "premiumContactsRequests";
      m = 2131886828;
      n = 2131234009;
      i2 = 2131886849;
      i3 = 2131886838;
      localObject3 = c.a.m.a(Integer.valueOf(i3));
      i4 = 2131234397;
      localObject4 = localObject2;
      ((e)localObject2).<init>(str1, m, n, i2, (List)localObject3, i4);
      ((ArrayList)localObject1).add(localObject2);
    }
    localObject2 = (Iterable)j.a();
    localObject4 = "receiver$0";
    c.g.b.k.b(localObject2, (String)localObject4);
    localObject2 = (Collection)localObject2;
    bool1 = ((Collection)localObject2).isEmpty() ^ true;
    if (bool1)
    {
      localObject2 = localObject1;
      localObject2 = (Collection)localObject1;
      str1 = "premiumAdvancedBlocking";
      localObject4 = (Iterable)j.a();
      Object localObject6 = new java/util/ArrayList;
      n = c.a.m.a((Iterable)localObject4, 10);
      ((ArrayList)localObject6).<init>(n);
      localObject6 = (Collection)localObject6;
      localObject4 = ((Iterable)localObject4).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject4).hasNext();
        if (!bool2) {
          break;
        }
        i1 = nextb;
        Integer localInteger = Integer.valueOf(i1);
        ((Collection)localObject6).add(localInteger);
      }
      localObject3 = localObject6;
      localObject3 = (List)localObject6;
      localObject5 = new com/truecaller/premium/data/e;
      m = 2131886825;
      int i1 = 2131234405;
      i2 = 2131886846;
      i4 = 2131234395;
      localObject4 = localObject5;
      ((e)localObject5).<init>(str1, m, i1, i2, (List)localObject3, i4);
      ((Collection)localObject2).add(localObject5);
    }
    localObject1 = ImmutableList.copyOf((Collection)localObject1);
    c.g.b.k.a(localObject1, "ImmutableList.copyOf(features)");
    return (List)localObject1;
  }
  
  final v a(Boolean paramBoolean1, Boolean paramBoolean2, y paramy)
  {
    n localn = this;
    Object localObject1 = paramy;
    Object localObject2 = new com/truecaller/premium/data/ag;
    ((ag)localObject2).<init>();
    c.g.b.k.b(paramy, "dto");
    Object localObject3 = (CharSequence)b;
    boolean bool1 = org.c.a.a.a.k.b((CharSequence)localObject3);
    if (!bool1) {
      try
      {
        localObject3 = new org/json/JSONObject;
        localObject1 = b;
        ((JSONObject)localObject3).<init>((String)localObject1);
        localObject1 = "topImage";
        localObject1 = ((JSONObject)localObject3).optString((String)localObject1);
        a = ((String)localObject1);
        localObject1 = "goldTopImage";
        localObject1 = ((JSONObject)localObject3).optString((String)localObject1);
        b = ((String)localObject1);
      }
      catch (JSONException localJSONException) {}
    }
    int n = 2131886752;
    boolean bool2;
    f localf;
    int i2;
    Object localObject4;
    int i3;
    Object localObject5;
    Object localObject6;
    w localw;
    int i4;
    String str1;
    Uri localUri;
    Object localObject7;
    int i6;
    if (paramBoolean1 != null)
    {
      bool2 = paramBoolean1.booleanValue();
      localf = new com/truecaller/premium/data/f;
      if (bool2) {
        i2 = 2131886908;
      } else {
        i2 = 2131886907;
      }
      if (bool2) {
        localObject4 = b();
      } else {
        localObject4 = c.getString(n);
      }
      localf.<init>(i2, (String)localObject4);
      if (bool2)
      {
        i2 = 2131886822;
        i3 = 2131886822;
      }
      else
      {
        i2 = 2131886821;
        i3 = 2131886821;
      }
      if (bool2)
      {
        localObject5 = d();
      }
      else
      {
        localObject5 = (CharSequence)a;
        localObject6 = (CharSequence)d();
        localObject5 = (String)org.c.a.a.a.k.e((CharSequence)localObject5, (CharSequence)localObject6);
      }
      c.g.b.k.a(localObject5, "topImageUri");
      localw = new com/truecaller/premium/data/w;
      i4 = 2131234403;
      localObject6 = c;
      int i5 = 2131886909;
      str1 = ((Context)localObject6).getString(i5);
      c.g.b.k.a(str1, "context.getString(R.string.PremiumToolbarTitle)");
      localUri = Uri.parse((String)localObject5);
      c.g.b.k.a(localUri, "Uri.parse(topImage)");
      localObject7 = e();
      localObject5 = c;
      i2 = 2131100401;
      i6 = android.support.v4.content.b.c((Context)localObject5, i2);
      localObject6 = localw;
      localw.<init>(localf, i3, i4, str1, localUri, (List)localObject7, i6);
    }
    else
    {
      localw = null;
    }
    if (paramBoolean2 != null)
    {
      bool2 = paramBoolean2.booleanValue();
      localf = new com/truecaller/premium/data/f;
      if (bool2) {
        i2 = 2131886868;
      } else {
        i2 = 2131886867;
      }
      if (bool2)
      {
        localObject1 = b();
      }
      else
      {
        localObject4 = c;
        localObject1 = ((Context)localObject4).getString(n);
      }
      localf.<init>(i2, (String)localObject1);
      if (bool2)
      {
        n = 2131886856;
        i3 = 2131886856;
      }
      else
      {
        n = 2131886855;
        i3 = 2131886855;
      }
      if (bool2)
      {
        localObject1 = c();
      }
      else
      {
        localObject1 = (CharSequence)b;
        localObject2 = (CharSequence)c();
        localObject1 = (String)org.c.a.a.a.k.e((CharSequence)localObject1, (CharSequence)localObject2);
      }
      c.g.b.k.a(localObject1, "topImageUri");
      localObject2 = new com/truecaller/premium/data/w;
      str1 = c.getString(2131886869);
      c.g.b.k.a(str1, "context.getString(R.stri….PremiumGoldToolbarTitle)");
      localUri = Uri.parse((String)localObject1);
      c.g.b.k.a(localUri, "Uri.parse(topImage)");
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject1 = (Collection)localObject1;
      localObject5 = new com/truecaller/premium/data/e;
      String str2 = "goldCallerId";
      int i7 = 2131886861;
      int i8 = 2131234165;
      int i9 = 2131886864;
      i2 = 2131886859;
      List localList = c.a.m.a(Integer.valueOf(i2));
      int i10 = 2131234048;
      localObject6 = c.getApplicationContext();
      if (localObject6 != null)
      {
        localObject6 = (com.truecaller.common.b.a)localObject6;
        boolean bool3 = ((com.truecaller.common.b.a)localObject6).p();
        Object localObject8;
        if (!bool3)
        {
          localObject8 = null;
        }
        else
        {
          localObject6 = h.a("profileAvatar");
          localObject7 = com.truecaller.profile.c.a(h);
          i6 = 2;
          localObject8 = null;
          if (localObject7 != null)
          {
            localObject3 = new com/truecaller/data/entity/Number;
            ((Number)localObject3).<init>((String)localObject7);
            localObject9 = ((Number)localObject3).g();
            localObject3 = i.a((Number)localObject3);
            c.g.b.k.a(localObject3, "numberProvider.getTypeForDisplay(number)");
            localObject10 = " - ";
            localObject11 = new CharSequence[i6];
            localObject3 = (CharSequence)localObject3;
            localObject11[0] = localObject3;
            localObject9 = (CharSequence)localObject9;
            bool1 = true;
            localObject11[bool1] = localObject9;
            localObject3 = am.a((String)localObject10, (CharSequence[])localObject11);
            localObject12 = localObject3;
          }
          else
          {
            localObject12 = null;
          }
          localObject3 = localObject6;
          localObject3 = (CharSequence)localObject6;
          if (localObject3 != null)
          {
            m = ((CharSequence)localObject3).length();
            if (m != 0)
            {
              m = 0;
              localObject3 = null;
              break label851;
            }
          }
          m = 1;
          label851:
          if (m == 0)
          {
            localObject3 = Uri.parse((String)localObject6);
            localObject13 = localObject3;
          }
          else
          {
            localObject13 = null;
          }
          String str3 = com.truecaller.profile.c.b(h);
          localObject3 = h.a("profileStreet");
          localObject6 = h.a("profileZip");
          Object localObject10 = h.a("profileCity");
          localObject11 = localObject3;
          localObject11 = (CharSequence)localObject3;
          if (localObject11 != null)
          {
            i11 = ((CharSequence)localObject11).length();
            if (i11 != 0)
            {
              i11 = 0;
              localObject11 = null;
              break label973;
            }
          }
          int i11 = 1;
          label973:
          if (i11 != 0)
          {
            localObject11 = localObject6;
            localObject11 = (CharSequence)localObject6;
            if (localObject11 != null)
            {
              i11 = ((CharSequence)localObject11).length();
              if (i11 != 0)
              {
                i11 = 0;
                localObject11 = null;
                break label1023;
              }
            }
            i11 = 1;
            label1023:
            if (i11 != 0)
            {
              localObject11 = localObject10;
              localObject11 = (CharSequence)localObject10;
              if (localObject11 != null)
              {
                i11 = ((CharSequence)localObject11).length();
                if (i11 != 0)
                {
                  i11 = 0;
                  localObject11 = null;
                  break label1073;
                }
              }
              i11 = 1;
              label1073:
              if (i11 != 0)
              {
                i12 = 0;
                localObject14 = null;
                break label1168;
              }
            }
          }
          localObject11 = new String[i6];
          localObject11[0] = localObject3;
          CharSequence[] arrayOfCharSequence = new CharSequence[i6];
          localObject6 = (CharSequence)localObject6;
          arrayOfCharSequence[0] = localObject6;
          localObject10 = (CharSequence)localObject10;
          bool3 = true;
          arrayOfCharSequence[bool3] = localObject10;
          localObject3 = am.a(" ", arrayOfCharSequence);
          localObject11[bool3] = localObject3;
          localObject3 = am.a((String[])localObject11);
          Object localObject14 = localObject3;
          label1168:
          localObject7 = (CharSequence)localObject7;
          String str4 = at.a((CharSequence)localObject7);
          localObject3 = new com/truecaller/premium/data/a;
          localObject9 = localObject3;
          ((a)localObject3).<init>((Uri)localObject13, str3, (String)localObject14, str4, (String)localObject12);
          localObject8 = localObject3;
        }
        Object localObject11 = localObject5;
        ((e)localObject5).<init>(str2, i7, i8, i9, localList, i10, (a)localObject8);
        ((Collection)localObject1).add(localObject5);
        localObject3 = new com/truecaller/premium/data/e;
        Object localObject13 = "goldAllPremium";
        int i13 = 2131886857;
        int i12 = 2131233824;
        int i14 = 2131886863;
        Object localObject12 = c.a.m.a(Integer.valueOf(2131886858));
        int i15 = 2131234047;
        Object localObject9 = localObject3;
        ((e)localObject3).<init>((String)localObject13, i13, i12, i14, (List)localObject12, i15);
        ((Collection)localObject1).add(localObject3);
        localObject3 = new com/truecaller/premium/data/e;
        str2 = "goldSupport";
        i7 = 2131886862;
        i8 = 2131234409;
        i9 = 2131886865;
        int i1 = 2131886860;
        localObject5 = Integer.valueOf(i1);
        localList = c.a.m.a(localObject5);
        i10 = 2131234049;
        localObject11 = localObject3;
        ((e)localObject3).<init>(str2, i7, i8, i9, localList, i10);
        ((Collection)localObject1).add(localObject3);
        localObject1 = ImmutableList.copyOf((Collection)localObject1);
        localObject3 = "ImmutableList.copyOf(features)";
        c.g.b.k.a(localObject1, (String)localObject3);
        localObject7 = localObject1;
        localObject7 = (List)localObject1;
        localObject1 = c;
        int m = 2131100399;
        i6 = android.support.v4.content.b.c((Context)localObject1, m);
        localObject6 = localObject2;
        n = 2131234166;
        i4 = n;
        ((w)localObject2).<init>(localf, i3, n, str1, localUri, (List)localObject7, i6);
      }
      else
      {
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
        throw ((Throwable)localObject1);
      }
    }
    else
    {
      localObject2 = null;
    }
    localObject1 = new com/truecaller/premium/data/v;
    ((v)localObject1).<init>(localw, (w)localObject2);
    return (v)localObject1;
  }
  
  public final Object a(Boolean paramBoolean1, Boolean paramBoolean2, c.d.c paramc)
  {
    c.d.f localf = k;
    Object localObject = new com/truecaller/premium/data/n$a;
    ((n.a)localObject).<init>(this, paramBoolean1, paramBoolean2, null);
    localObject = (c.g.a.m)localObject;
    return kotlinx.coroutines.g.a(localf, (c.g.a.m)localObject, paramc);
  }
  
  public final void a()
  {
    b.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */