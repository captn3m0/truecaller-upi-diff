package com.truecaller.premium.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;

public final class h
  implements g
{
  private final v a;
  
  public h(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return g.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    h.b localb = new com/truecaller/premium/data/h$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, (byte)0);
    return w.a(localv, localb);
  }
  
  public final w a(String paramString)
  {
    v localv = a;
    h.a locala = new com/truecaller/premium/data/h$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramString, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w a(String paramString1, String paramString2)
  {
    v localv = a;
    h.d locald = new com/truecaller/premium/data/h$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramString1, paramString2, (byte)0);
    return w.a(localv, locald);
  }
  
  public final w b(String paramString1, String paramString2)
  {
    v localv = a;
    h.c localc = new com/truecaller/premium/data/h$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramString1, paramString2, (byte)0);
    return w.a(localv, localc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */