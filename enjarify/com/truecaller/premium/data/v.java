package com.truecaller.premium.data;

import c.g.b.k;

public final class v
{
  public final w a;
  public final w b;
  
  public v(w paramw1, w paramw2)
  {
    a = paramw1;
    b = paramw2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof v;
      if (bool1)
      {
        paramObject = (v)paramObject;
        w localw1 = a;
        w localw2 = a;
        bool1 = k.a(localw1, localw2);
        if (bool1)
        {
          localw1 = b;
          paramObject = b;
          boolean bool2 = k.a(localw1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    w localw1 = a;
    int i = 0;
    int j;
    if (localw1 != null)
    {
      j = localw1.hashCode();
    }
    else
    {
      j = 0;
      localw1 = null;
    }
    j *= 31;
    w localw2 = b;
    if (localw2 != null) {
      i = localw2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PremiumTheme(premium=");
    w localw = a;
    localStringBuilder.append(localw);
    localStringBuilder.append(", gold=");
    localw = b;
    localStringBuilder.append(localw);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */