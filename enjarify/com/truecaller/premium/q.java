package com.truecaller.premium;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.view.ViewPager;
import android.support.v4.view.n;
import android.support.v4.view.o;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.R.id;
import com.truecaller.premium.data.PremiumType;
import com.truecaller.ui.view.DotPagerIndicator;
import java.io.Serializable;
import java.util.HashMap;

public final class q
  extends Fragment
  implements h, l, w
{
  public static final q.a b;
  public u a;
  private i c;
  private h d;
  private HashMap e;
  
  static
  {
    q.a locala = new com/truecaller/premium/q$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View d(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final ba.b a()
  {
    Object localObject = getParentFragment();
    if (localObject != null) {
      return ((h)localObject).a();
    }
    localObject = new c/u;
    ((c.u)localObject).<init>("null cannot be cast to non-null type com.truecaller.premium.FeaturesStyleProvider");
    throw ((Throwable)localObject);
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.title;
    ((TextView)d(i)).setText(paramInt);
  }
  
  public final void a(PremiumType paramPremiumType, int paramInt1, int paramInt2)
  {
    c.g.b.k.b(paramPremiumType, "premiumType");
    int i = R.id.viewPager;
    ViewPager localViewPager = (ViewPager)d(i);
    c.g.b.k.a(localViewPager, "viewPager");
    localViewPager.setOffscreenPageLimit(2);
    i = R.id.viewPager;
    localViewPager = (ViewPager)d(i);
    c.g.b.k.a(localViewPager, "viewPager");
    Object localObject = new com/truecaller/premium/q$b;
    j localj = getChildFragmentManager();
    c.g.b.k.a(localj, "childFragmentManager");
    ((q.b)localObject).<init>(paramPremiumType, paramInt1, localj);
    localObject = (o)localObject;
    localViewPager.setAdapter((o)localObject);
    int j = R.id.pagerIndicator;
    ((DotPagerIndicator)d(j)).setNumberOfPages(paramInt1);
    j = R.id.pagerIndicator;
    ((DotPagerIndicator)d(j)).setFirstPage(0);
    j = R.id.viewPager;
    paramPremiumType = (ViewPager)d(j);
    paramInt1 = R.id.pagerIndicator;
    DotPagerIndicator localDotPagerIndicator = (DotPagerIndicator)d(paramInt1);
    paramPremiumType.a(localDotPagerIndicator);
    j = R.id.viewPager;
    paramPremiumType = (ViewPager)d(j);
    c.g.b.k.a(paramPremiumType, "viewPager");
    paramPremiumType.setCurrentItem(paramInt2);
  }
  
  public final void b(int paramInt)
  {
    int i = R.id.background;
    ((ImageView)d(i)).setImageResource(paramInt);
  }
  
  public final k c()
  {
    Object localObject = getParentFragment();
    if (localObject != null) {
      return ((l)localObject).c();
    }
    localObject = new c/u;
    ((c.u)localObject).<init>("null cannot be cast to non-null type com.truecaller.premium.PremiumComponentProvider");
    throw ((Throwable)localObject);
  }
  
  public final void c(int paramInt)
  {
    int i = R.id.pagerIndicator;
    ((DotPagerIndicator)d(i)).setActiveColor(paramInt);
  }
  
  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    paramBundle = getArguments();
    Object localObject1;
    if (paramBundle != null)
    {
      localObject1 = "type";
      paramBundle = paramBundle.getSerializable((String)localObject1);
      if (paramBundle != null) {}
    }
    else
    {
      paramBundle = (Serializable)PremiumType.PREMIUM;
    }
    if (paramBundle != null)
    {
      paramBundle = (PremiumType)paramBundle;
      localObject1 = getArguments();
      int i;
      if (localObject1 != null)
      {
        localObject2 = "initial_position";
        i = ((Bundle)localObject1).getInt((String)localObject2);
      }
      else
      {
        i = 0;
        localObject1 = null;
      }
      Object localObject2 = getParentFragment();
      if (localObject2 != null)
      {
        localObject2 = ((l)localObject2).c();
        r localr = new com/truecaller/premium/r;
        localr.<init>(paramBundle, i);
        ((k)localObject2).a(localr).a(this);
        paramBundle = a;
        if (paramBundle == null)
        {
          localObject1 = "presenter";
          c.g.b.k.a((String)localObject1);
        }
        paramBundle.a(this);
        return;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.PremiumComponentProvider");
      throw paramBundle;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.data.PremiumType");
    throw paramBundle;
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getParentFragment();
    if (paramContext != null)
    {
      paramContext = (i)paramContext;
      c = paramContext;
      paramContext = getParentFragment();
      if (paramContext != null)
      {
        paramContext = (h)paramContext;
        d = paramContext;
        return;
      }
      paramContext = new c/u;
      paramContext.<init>("null cannot be cast to non-null type com.truecaller.premium.FeaturesStyleProvider");
      throw paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.premium.OnCloseDetailsListener");
    throw paramContext;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558756, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    ((u)localObject).y_();
    localObject = c;
    if (localObject != null) {
      ((i)localObject).b();
    }
    localObject = e;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onDetach()
  {
    super.onDetach();
    c = null;
    d = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    int i = R.id.btnClose;
    paramBundle = (ImageView)d(i);
    Object localObject1 = new com/truecaller/premium/q$c;
    ((q.c)localObject1).<init>(this);
    localObject1 = (View.OnClickListener)localObject1;
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    paramBundle = d;
    if (paramBundle != null)
    {
      paramBundle = paramBundle.a();
      if (paramBundle != null)
      {
        int j = R.id.title;
        localObject1 = (TextView)d(j);
        Object localObject2 = "title";
        c.g.b.k.a(localObject1, (String)localObject2);
        localObject1 = ((TextView)localObject1).getLayoutParams();
        if (localObject1 != null)
        {
          localObject1 = (ViewGroup.MarginLayoutParams)localObject1;
          int k = c;
          topMargin = k;
          j = R.id.btnClose;
          localObject1 = (ImageView)d(j);
          k = b;
          ((ImageView)localObject1).setImageResource(k);
          j = R.id.background;
          localObject1 = (ImageView)d(j);
          c.g.b.k.a(localObject1, "background");
          localObject1 = ((ImageView)localObject1).getLayoutParams();
          k = R.id.background;
          localObject2 = (ImageView)d(k);
          String str = "background";
          c.g.b.k.a(localObject2, str);
          localObject2 = ((ImageView)localObject2).getLayoutParams();
          k = height;
          i = d;
          k -= i;
          height = k;
        }
        else
        {
          paramView = new c/u;
          paramView.<init>("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
          throw paramView;
        }
      }
    }
    paramBundle = new com/truecaller/premium/a;
    paramBundle.<init>();
    paramBundle = (n)paramBundle;
    android.support.v4.view.r.a(paramView, paramBundle);
    i = R.id.viewPager;
    paramBundle = (ViewPager)d(i);
    localObject1 = new com/truecaller/premium/a;
    ((a)localObject1).<init>();
    localObject1 = (n)localObject1;
    android.support.v4.view.r.a(paramBundle, (n)localObject1);
    android.support.v4.view.r.t(paramView);
    int m = R.id.viewPager;
    android.support.v4.view.r.t((ViewPager)d(m));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */