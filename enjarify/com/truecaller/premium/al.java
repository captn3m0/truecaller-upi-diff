package com.truecaller.premium;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout.a;
import android.support.v4.app.Fragment;
import android.support.v4.content.b;
import android.support.v4.view.n;
import android.support.v4.view.r;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import c.a.m;
import c.u;
import c.x;
import com.truecaller.R.id;
import com.truecaller.premium.data.PremiumType;
import com.truecaller.premium.data.e;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public final class al
  extends Fragment
  implements aq
{
  public static final al.a b;
  public ao a;
  private HashMap c;
  
  static
  {
    al.a locala = new com/truecaller/premium/al$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(e parame)
  {
    c.g.b.k.b(parame, "feature");
    int i = R.id.title;
    Object localObject1 = (TextView)a(i);
    int j = b;
    ((TextView)localObject1).setText(j);
    i = R.id.icon;
    localObject1 = (ImageView)a(i);
    j = f;
    ((ImageView)localObject1).setImageResource(j);
    localObject1 = (Collection)e;
    i = ((Collection)localObject1).size();
    j = 0;
    int k = 1;
    float f = Float.MIN_VALUE;
    if (i == k)
    {
      i = R.id.description;
      localObject1 = (TextView)a(i);
      c.g.b.k.a(localObject1, "description");
      ((TextView)localObject1).setVisibility(0);
      i = R.id.description;
      localObject1 = (TextView)a(i);
      int n = ((Number)m.h(e)).intValue();
      ((TextView)localObject1).setText(n);
      return;
    }
    i = R.id.description;
    localObject1 = (TextView)a(i);
    Object localObject2 = "description";
    c.g.b.k.a(localObject1, (String)localObject2);
    ((TextView)localObject1).setVisibility(8);
    parame = (Iterable)e;
    localObject1 = new java/util/ArrayList;
    f = 1.4E-44F;
    k = m.a(parame, 10);
    ((ArrayList)localObject1).<init>(k);
    localObject1 = (Collection)localObject1;
    parame = parame.iterator();
    for (;;)
    {
      boolean bool = parame.hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Number)parame.next();
      int m = ((Number)localObject2).intValue();
      Context localContext = getContext();
      if (localContext != null)
      {
        c.g.b.k.a(localContext, "context ?: return");
        Object localObject3 = localContext.getResources();
        int i1 = 2131165470;
        int i2 = ((Resources)localObject3).getDimensionPixelSize(i1);
        Object localObject4 = new android/widget/LinearLayout$LayoutParams;
        int i3 = -2;
        ((LinearLayout.LayoutParams)localObject4).<init>(i3, i3);
        ((LinearLayout.LayoutParams)localObject4).setMargins(0, i2, 0, 0);
        localObject3 = new android/widget/TextView;
        ((TextView)localObject3).<init>(localContext);
        ((TextView)localObject3).setText(m);
        m = b.c(localContext, 2131100402);
        ((TextView)localObject3).setTextColor(m);
        ((TextView)localObject3).setCompoundDrawablesWithIntrinsicBounds(2131233985, 0, 0, 0);
        localObject2 = localContext.getResources();
        i3 = 2131165496;
        m = ((Resources)localObject2).getDimensionPixelSize(i3);
        ((TextView)localObject3).setCompoundDrawablePadding(m);
        localObject2 = localContext.getResources();
        int i4 = 2131166099;
        f = ((Resources)localObject2).getDimension(i4);
        ((TextView)localObject3).setTextSize(0, f);
        localObject4 = (ViewGroup.LayoutParams)localObject4;
        ((TextView)localObject3).setLayoutParams((ViewGroup.LayoutParams)localObject4);
        m = R.id.descriptionsContainer;
        localObject2 = (LinearLayout)a(m);
        localObject3 = (View)localObject3;
        ((LinearLayout)localObject2).addView((View)localObject3);
      }
      localObject2 = x.a;
      ((Collection)localObject1).add(localObject2);
    }
  }
  
  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    paramBundle = getArguments();
    Object localObject1;
    if (paramBundle != null)
    {
      localObject1 = "type";
      paramBundle = paramBundle.getSerializable((String)localObject1);
      if (paramBundle != null) {}
    }
    else
    {
      paramBundle = (Serializable)PremiumType.PREMIUM;
    }
    if (paramBundle != null)
    {
      paramBundle = (PremiumType)paramBundle;
      localObject1 = getArguments();
      int i;
      if (localObject1 != null)
      {
        localObject2 = "page_number";
        i = ((Bundle)localObject1).getInt((String)localObject2);
      }
      else
      {
        i = 0;
        localObject1 = null;
      }
      Object localObject2 = getParentFragment();
      if (localObject2 != null)
      {
        localObject2 = ((l)localObject2).c();
        am localam = new com/truecaller/premium/am;
        localam.<init>(paramBundle, i);
        ((k)localObject2).a(localam).a(this);
        paramBundle = a;
        if (paramBundle == null)
        {
          localObject1 = "presenter";
          c.g.b.k.a((String)localObject1);
        }
        paramBundle.a(this);
        return;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.PremiumComponentProvider");
      throw paramBundle;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.data.PremiumType");
    throw paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558757, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    ((ao)localObject).y_();
    localObject = c;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    paramBundle = new com/truecaller/premium/a;
    paramBundle.<init>();
    paramBundle = (n)paramBundle;
    r.a(paramView, paramBundle);
    r.t(paramView);
    int i = R.id.icon;
    paramView = (ImageView)a(i);
    paramBundle = "icon";
    c.g.b.k.a(paramView, paramBundle);
    paramView = paramView.getLayoutParams();
    if (paramView != null)
    {
      paramView = (ConstraintLayout.a)paramView;
      int j = topMargin;
      Fragment localFragment = getParentFragment();
      if (localFragment != null)
      {
        int k = ad;
        j -= k;
        topMargin = j;
        return;
      }
      paramView = new c/u;
      paramView.<init>("null cannot be cast to non-null type com.truecaller.premium.FeaturesStyleProvider");
      throw paramView;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.support.constraint.ConstraintLayout.LayoutParams");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */