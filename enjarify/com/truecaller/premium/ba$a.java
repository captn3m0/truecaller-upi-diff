package com.truecaller.premium;

import android.os.Bundle;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.premium.data.SubscriptionPromoEventMetaData;
import java.io.Serializable;

public final class ba$a
{
  public static ba a(PremiumPresenterView.LaunchContext paramLaunchContext, SubscriptionPromoEventMetaData paramSubscriptionPromoEventMetaData, String paramString, ba.b paramb)
  {
    k.b(paramLaunchContext, "launchContext");
    k.b(paramb, "premiumFeaturesStyle");
    ba localba = new com/truecaller/premium/ba;
    localba.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramLaunchContext = (Serializable)paramLaunchContext;
    localBundle.putSerializable("launchContext", paramLaunchContext);
    paramSubscriptionPromoEventMetaData = (Parcelable)paramSubscriptionPromoEventMetaData;
    localBundle.putParcelable("analyticsMetadata", paramSubscriptionPromoEventMetaData);
    localBundle.putString("selectedPage", paramString);
    paramb = (Serializable)paramb;
    localBundle.putSerializable("premiumFeaturesStyle", paramb);
    localba.setArguments(localBundle);
    return localba;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.ba.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */