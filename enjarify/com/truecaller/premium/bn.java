package com.truecaller.premium;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.o;
import c.a.ag;
import c.g.b.t;
import c.g.b.u;
import c.l;
import com.google.android.libraries.nbu.engagementrewards.models.Promotion;
import com.google.common.util.concurrent.FutureCallback;
import com.truecaller.analytics.e.a;
import com.truecaller.ba;
import com.truecaller.calling.recorder.CallRecordingSettingsMvp.RecordingModes;
import com.truecaller.calling.recorder.FreeTrialStatus;
import com.truecaller.calling.recorder.bv;
import com.truecaller.common.f.c.b;
import com.truecaller.common.f.c.c;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.premium.data.PremiumType;
import com.truecaller.premium.data.SubscriptionPromoEventMetaData;
import com.truecaller.premium.data.ac;
import com.truecaller.premium.data.s;
import com.truecaller.premium.data.s.a;
import com.truecaller.premium.data.v;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class bn
  extends ba
  implements bm
{
  private final com.truecaller.common.g.a A;
  private final com.truecaller.analytics.b B;
  private final com.truecaller.analytics.a.f C;
  private final bv D;
  private final s E;
  private final ac F;
  private final bb G;
  private final s.a H;
  private final com.truecaller.utils.d I;
  private final m J;
  private final com.truecaller.engagementrewards.k K;
  private final com.truecaller.engagementrewards.c L;
  private final com.truecaller.engagementrewards.g M;
  private final com.truecaller.engagementrewards.ui.d N;
  private final c.d.f O;
  boolean d;
  v e;
  List f;
  List g;
  PremiumType h;
  final HashMap i;
  final String j;
  final n k;
  final bw l;
  final com.truecaller.abtest.c m;
  private String n;
  private bz o;
  private boolean p;
  private final c.f q;
  private boolean r;
  private boolean s;
  private final boolean t;
  private bn.a u;
  private Promotion v;
  private final PremiumPresenterView.LaunchContext w;
  private final SubscriptionPromoEventMetaData x;
  private final com.truecaller.common.f.c y;
  private final com.truecaller.i.e z;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = c.g.b.w.a(bn.class);
    ((u)localObject).<init>(localb, "engagementRewardLiveData", "getEngagementRewardLiveData$truecaller_googlePlayRelease()Landroid/arch/lifecycle/MutableLiveData;");
    localObject = (c.l.g)c.g.b.w.a((t)localObject);
    arrayOfg[0] = localObject;
    c = arrayOfg;
  }
  
  public bn(PremiumPresenterView.LaunchContext paramLaunchContext, SubscriptionPromoEventMetaData paramSubscriptionPromoEventMetaData, String paramString, com.truecaller.common.f.c paramc, com.truecaller.i.e parame, com.truecaller.common.g.a parama, com.truecaller.analytics.b paramb, com.truecaller.analytics.a.f paramf, bv parambv, s params, n paramn, bw parambw, com.truecaller.abtest.c paramc1, ac paramac, bb parambb, s.a parama1, com.truecaller.utils.d paramd, m paramm, com.truecaller.engagementrewards.k paramk, com.truecaller.engagementrewards.c paramc2, com.truecaller.engagementrewards.g paramg, com.truecaller.engagementrewards.ui.d paramd1, c.d.f paramf1)
  {
    super(paramf1);
    w = paramLaunchContext;
    localObject2 = paramSubscriptionPromoEventMetaData;
    x = paramSubscriptionPromoEventMetaData;
    localObject2 = paramString;
    j = paramString;
    y = paramc;
    z = parame;
    A = parama;
    B = paramb;
    C = paramf;
    D = parambv;
    E = params;
    k = paramn;
    l = parambw;
    m = paramc1;
    F = paramac;
    G = parambb;
    H = parama1;
    localObject2 = paramd;
    I = paramd;
    localObject2 = paramm;
    J = paramm;
    localObject2 = paramk;
    localObject3 = paramc2;
    K = paramk;
    L = paramc2;
    localObject2 = paramg;
    localObject3 = paramd1;
    M = paramg;
    N = paramd1;
    O = paramf1;
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (List)localObject2;
    f = ((List)localObject2);
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (List)localObject2;
    g = ((List)localObject2);
    localObject2 = new java/util/HashMap;
    ((HashMap)localObject2).<init>();
    i = ((HashMap)localObject2);
    localObject2 = c.g.a((c.g.a.a)bn.b.a);
    q = ((c.f)localObject2);
    localObject2 = w;
    localObject3 = PremiumPresenterView.LaunchContext.BOTTOM_BAR;
    boolean bool;
    if (localObject2 == localObject3)
    {
      bool = true;
    }
    else
    {
      bool = false;
      localObject2 = null;
    }
    t = bool;
  }
  
  private final String a(cb paramcb)
  {
    String str1 = a;
    if (str1 != null)
    {
      c.g.b.k.a(str1, "sku");
      Object localObject1 = x;
      if (localObject1 != null)
      {
        localObject2 = F;
        localObject1 = ((SubscriptionPromoEventMetaData)localObject1).a();
        String str2 = "correlation";
        c.g.b.k.a(localObject1, str2);
        ((ac)localObject2).a((String)localObject1, str1);
      }
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("ANDROID_subscription_item_clk");
      localObject1 = ((e.a)localObject1).a("sku", str1);
      c.g.b.k.a(localObject1, "AnalyticsEvent.Builder(S…AM_SUBSCRIPTION_SKU, sku)");
      a((e.a)localObject1);
      localObject1 = J;
      Object localObject2 = w;
      ((m)localObject1).a((PremiumPresenterView.LaunchContext)localObject2, paramcb);
      localObject1 = new com/truecaller/premium/bn$l;
      ((bn.l)localObject1).<init>(str1, null, this, paramcb);
      localObject1 = (c.g.a.m)localObject1;
      kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject1, 3);
      return str1;
    }
    return null;
  }
  
  private final void a(e.a parama)
  {
    Object localObject1 = w;
    Object localObject2 = PremiumPresenterView.LaunchContext.DEEP_LINK;
    if (localObject1 == localObject2)
    {
      localObject1 = x;
      if (localObject1 != null)
      {
        localObject2 = "Campaign";
        localObject1 = am.a(((SubscriptionPromoEventMetaData)localObject1).b());
        parama.a((String)localObject2, (String)localObject1);
      }
    }
    localObject2 = w.name();
    parama.a("source", (String)localObject2);
    localObject1 = A;
    String str = "";
    localObject1 = ((com.truecaller.common.g.a)localObject1).b("callRecordingMode", str);
    localObject2 = CallRecordingSettingsMvp.RecordingModes.AUTO.name();
    boolean bool1 = true;
    boolean bool2 = c.n.m.a((String)localObject1, (String)localObject2, bool1);
    if (bool2) {
      localObject1 = "Auto";
    } else {
      localObject1 = "Manual";
    }
    parama.a("CallRecMode", (String)localObject1);
    int i1 = D.m();
    parama.a("CallRecDaysRemaining", i1);
    localObject2 = D.n().name();
    parama.a("CallRecTrialStatus", (String)localObject2);
    localObject1 = j;
    if (localObject1 != null)
    {
      localObject2 = "SelectedPage";
      parama.a((String)localObject2, (String)localObject1);
    }
    localObject1 = B;
    parama = parama.a();
    c.g.b.k.a(parama, "builder.build()");
    ((com.truecaller.analytics.b)localObject1).b(parama);
  }
  
  private final bn.a o()
  {
    bn.a locala = new com/truecaller/premium/bn$a;
    long l1 = y.e();
    String str = y.k();
    c.g.b.k.a(str, "repository.level");
    boolean bool1 = y.o();
    boolean bool2 = y.g();
    locala.<init>(l1, str, bool1, bool2);
    return locala;
  }
  
  public final LiveData a()
  {
    return (LiveData)g();
  }
  
  public final void a(cc paramcc)
  {
    c.g.b.k.b(paramcc, "button");
    ((c.g.a.a)ag.b((Map)i, paramcc)).invoke();
  }
  
  public final void a(PremiumType paramPremiumType)
  {
    c.g.b.k.b(paramPremiumType, "premiumType");
    PremiumPresenterView localPremiumPresenterView = (PremiumPresenterView)b;
    if (localPremiumPresenterView != null)
    {
      h = paramPremiumType;
      paramPremiumType = h;
      if (paramPremiumType == null)
      {
        localObject = "selectedType";
        c.g.b.k.a((String)localObject);
      }
      Object localObject = bo.a;
      int i1 = paramPremiumType.ordinal();
      i1 = localObject[i1];
      switch (i1)
      {
      default: 
        paramPremiumType = new c/l;
        paramPremiumType.<init>();
        throw paramPremiumType;
      case 2: 
        paramPremiumType = g;
        break;
      case 1: 
        paramPremiumType = f;
      }
      boolean bool = j();
      int i2;
      if (bool) {
        i2 = 2131559125;
      } else {
        i2 = 2131559124;
      }
      localPremiumPresenterView.a(paramPremiumType, i2);
      return;
    }
  }
  
  public final void a(PremiumType paramPremiumType, int paramInt)
  {
    c.g.b.k.b(paramPremiumType, "type");
    a(paramPremiumType, paramInt, true);
  }
  
  final void a(PremiumType paramPremiumType, int paramInt, boolean paramBoolean)
  {
    PremiumPresenterView localPremiumPresenterView = (PremiumPresenterView)b;
    if (localPremiumPresenterView != null)
    {
      localPremiumPresenterView.a(paramPremiumType, paramInt, paramBoolean);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    if (localObject1 != null)
    {
      i1 = ((CharSequence)localObject1).length();
      if (i1 != 0)
      {
        i1 = 0;
        localObject1 = null;
        break label34;
      }
    }
    int i1 = 1;
    label34:
    if (i1 == 0)
    {
      localObject1 = n;
      if (localObject1 != null)
      {
        com.truecaller.common.f.c localc = y;
        Object localObject2 = new com/truecaller/premium/bn$j;
        Object localObject3 = this;
        localObject3 = (bn)this;
        ((bn.j)localObject2).<init>((bn)localObject3);
        localObject2 = (c.g.a.b)localObject2;
        localObject3 = new com/truecaller/premium/bp;
        ((bp)localObject3).<init>((c.g.a.b)localObject2);
        localObject3 = (c.b)localObject3;
        localc.a((String)localObject1, paramString, (c.b)localObject3);
        return;
      }
    }
  }
  
  final void a(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString1);
    localStringBuilder.append("\n");
    localStringBuilder.append(paramString2);
    paramString1 = localStringBuilder.toString();
    n = paramString1;
    paramString1 = (PremiumPresenterView)b;
    if (paramString1 != null)
    {
      paramString2 = y.f();
      paramString1.b(paramString2);
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    e.a locala1 = new com/truecaller/analytics/e$a;
    locala1.<init>("ANDROID_subscription_launched");
    String str = "premium";
    Object localObject = y;
    boolean bool = ((com.truecaller.common.f.c)localObject).d();
    if (bool) {
      localObject = "yes";
    } else {
      localObject = "no";
    }
    e.a locala2 = locala1.a(str, (String)localObject).a("EngRewardShown", paramBoolean);
    c.g.b.k.a(locala2, "AnalyticsEvent.Builder(S…gagementRewardPromoShown)");
    a(locala2);
  }
  
  public final com.truecaller.premium.data.w b(PremiumType paramPremiumType)
  {
    c.g.b.k.b(paramPremiumType, "premiumType");
    Object localObject = bo.b;
    int i1 = paramPremiumType.ordinal();
    i1 = localObject[i1];
    switch (i1)
    {
    default: 
      paramPremiumType = new c/l;
      paramPremiumType.<init>();
      throw paramPremiumType;
    case 2: 
      paramPremiumType = e;
      if (paramPremiumType == null)
      {
        localObject = "theme";
        c.g.b.k.a((String)localObject);
      }
      paramPremiumType = b;
      if (paramPremiumType == null)
      {
        paramPremiumType = new java/lang/IllegalStateException;
        paramPremiumType.<init>("Gold part cannot be null");
        throw ((Throwable)paramPremiumType);
      }
      break;
    case 1: 
      paramPremiumType = e;
      if (paramPremiumType == null)
      {
        localObject = "theme";
        c.g.b.k.a((String)localObject);
      }
      paramPremiumType = a;
      if (paramPremiumType == null) {
        break label127;
      }
    }
    return paramPremiumType;
    label127:
    paramPremiumType = new java/lang/IllegalStateException;
    paramPremiumType.<init>("Premium part cannot be null");
    throw ((Throwable)paramPremiumType);
  }
  
  public final void b()
  {
    Object localObject1 = o;
    if (localObject1 != null)
    {
      localObject2 = y;
      String str = b;
      localObject1 = c;
      Object localObject3 = new com/truecaller/premium/bn$i;
      ((bn.i)localObject3).<init>(this);
      localObject3 = (c.c)localObject3;
      ((com.truecaller.common.f.c)localObject2).b(str, (String)localObject1, (c.c)localObject3);
      return;
    }
    Object localObject2 = { "Move premium dialog should not even be shown if there is no receipt." };
    AssertionUtil.shouldNeverHappen(null, (String[])localObject2);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "userInteractionContext");
    Object localObject1 = (PremiumPresenterView)b;
    if (localObject1 != null)
    {
      localObject2 = A;
      String str = "subscriptionErrorResolveUrl";
      localObject2 = ((com.truecaller.common.g.a)localObject2).a(str);
      if (localObject2 == null) {
        localObject2 = "https://play.google.com/store/account/subscriptions";
      }
      ((PremiumPresenterView)localObject1).a((String)localObject2);
    }
    localObject1 = (PremiumPresenterView)b;
    if (localObject1 != null) {
      ((PremiumPresenterView)localObject1).e();
    }
    localObject1 = B;
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("ViewAction");
    paramString = ((e.a)localObject2).a("Context", paramString).a();
    c.g.b.k.a(paramString, "AnalyticsEvent.Builder(V…\n                .build()");
    ((com.truecaller.analytics.b)localObject1).b(paramString);
  }
  
  final void b(boolean paramBoolean)
  {
    PremiumPresenterView localPremiumPresenterView = (PremiumPresenterView)b;
    if (localPremiumPresenterView != null) {
      localPremiumPresenterView.b(paramBoolean);
    }
    r = paramBoolean;
  }
  
  public final void c()
  {
    m();
  }
  
  public final void e()
  {
    PremiumPresenterView localPremiumPresenterView = (PremiumPresenterView)b;
    if (localPremiumPresenterView != null)
    {
      boolean bool = d;
      if (bool)
      {
        h();
        return;
      }
      m();
    }
  }
  
  public final void f()
  {
    boolean bool1 = r;
    if (bool1) {
      return;
    }
    bool1 = s;
    boolean bool2 = true;
    if (!bool1)
    {
      bn.a locala1 = o();
      bn.a locala2 = u;
      bool1 = c.g.b.k.a(locala1, locala2) ^ bool2;
      if (!bool1) {}
    }
    else
    {
      b(bool2);
      h();
    }
  }
  
  public final o g()
  {
    return (o)q.b();
  }
  
  final void h()
  {
    Object localObject1 = new com/truecaller/premium/bn$c;
    Object localObject2 = null;
    ((bn.c)localObject1).<init>(this, null);
    localObject1 = (c.g.a.m)localObject1;
    int i1 = 3;
    kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject1, i1);
    localObject1 = L;
    boolean bool = ((com.truecaller.engagementrewards.c)localObject1).b();
    if (bool)
    {
      localObject1 = y;
      bool = ((com.truecaller.common.f.c)localObject1).d();
      if (!bool)
      {
        localObject1 = K;
        localObject2 = new com/truecaller/premium/bn$d;
        ((bn.d)localObject2).<init>(this);
        localObject2 = (FutureCallback)localObject2;
        ((com.truecaller.engagementrewards.k)localObject1).a((FutureCallback)localObject2);
        return;
      }
    }
    g().b(null);
    a(false);
  }
  
  public final v i()
  {
    v localv = e;
    if (localv == null)
    {
      String str = "theme";
      c.g.b.k.a(str);
    }
    return localv;
  }
  
  final boolean j()
  {
    return c.n.m.a(m.a("premiumButtonsType_18492"), "vertical", true);
  }
  
  final void k()
  {
    PremiumPresenterView localPremiumPresenterView = (PremiumPresenterView)b;
    if (localPremiumPresenterView != null) {
      localPremiumPresenterView.p();
    }
    m();
  }
  
  final void l()
  {
    PremiumPresenterView localPremiumPresenterView = (PremiumPresenterView)b;
    if (localPremiumPresenterView != null) {
      localPremiumPresenterView.r();
    }
    m();
  }
  
  final void m()
  {
    boolean bool = t;
    if (bool)
    {
      s = true;
      b(false);
      return;
    }
    PremiumPresenterView localPremiumPresenterView = (PremiumPresenterView)b;
    if (localPremiumPresenterView != null)
    {
      localPremiumPresenterView.g();
      return;
    }
  }
  
  final void n()
  {
    boolean bool = t;
    if (bool)
    {
      b(true);
      h();
      return;
    }
    PremiumPresenterView localPremiumPresenterView = (PremiumPresenterView)b;
    if (localPremiumPresenterView != null)
    {
      localPremiumPresenterView.g();
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    E.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.bn
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */