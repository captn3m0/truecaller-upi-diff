package com.truecaller.premium;

import android.arch.lifecycle.LiveData;
import android.net.Uri;
import c.g.b.k;
import c.l;
import c.n;
import com.truecaller.bb;
import com.truecaller.engagementrewards.c;
import com.truecaller.engagementrewards.g;
import com.truecaller.engagementrewards.r;
import com.truecaller.premium.data.PremiumType;
import com.truecaller.premium.data.v;
import com.truecaller.premium.data.w;
import java.util.ArrayList;

public final class aw
  extends bb
  implements av
{
  final g a;
  private final ArrayList c;
  private PremiumType d;
  private final PremiumType e;
  private final bu f;
  private final r g;
  private final c h;
  
  public aw(PremiumType paramPremiumType, bu parambu, r paramr, g paramg, c paramc)
  {
    e = paramPremiumType;
    f = parambu;
    g = paramr;
    a = paramg;
    h = paramc;
    paramPremiumType = new java/util/ArrayList;
    paramPremiumType.<init>();
    c = paramPremiumType;
  }
  
  final Uri a()
  {
    Object localObject = d;
    PremiumType localPremiumType = PremiumType.PREMIUM;
    if (localObject == localPremiumType)
    {
      localObject = (n)g.a().b();
      if (localObject != null)
      {
        localObject = h.c();
        if (localObject != null) {
          return Uri.parse((String)localObject);
        }
      }
      return null;
    }
    return null;
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = c;
    Object localObject2 = (PremiumType)((ArrayList)localObject1).get(paramInt);
    d = ((PremiumType)localObject2);
    localObject2 = d;
    if (localObject2 != null)
    {
      localObject1 = ax.a;
      int i = ((PremiumType)localObject2).ordinal();
      int j = localObject1[i];
      switch (j)
      {
      default: 
        localObject2 = new c/l;
        ((l)localObject2).<init>();
        throw ((Throwable)localObject2);
      case 2: 
        localObject1 = f.i().b;
        break;
      case 1: 
        localObject1 = f.i().a;
      }
      if (localObject1 != null)
      {
        ay localay = (ay)b;
        if (localay != null)
        {
          Object localObject3 = a();
          if (localObject3 == null) {
            localObject3 = e;
          }
          localay.a((Uri)localObject3);
          localObject3 = d;
          localay.a((String)localObject3);
          int k = g;
          localay.c(k);
          j = g;
          localay.d(j);
          localay.a((PremiumType)localObject2);
          return;
        }
        return;
      }
      return;
    }
  }
  
  public final void a(PremiumType paramPremiumType)
  {
    k.b(paramPremiumType, "premiumType");
    int i = c.indexOf(paramPremiumType);
    ay localay = (ay)b;
    if (localay != null)
    {
      localay.b(i);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.aw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */