package com.truecaller.premium;

import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;
import c.g.b.k;

public final class GoldCallerIdPreviewView$1
  extends ViewOutlineProvider
{
  public final void getOutline(View paramView, Outline paramOutline)
  {
    k.b(paramView, "view");
    k.b(paramOutline, "outline");
    ViewOutlineProvider.BACKGROUND.getOutline(paramView, paramOutline);
    paramOutline.setAlpha(0.5F);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.GoldCallerIdPreviewView.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */