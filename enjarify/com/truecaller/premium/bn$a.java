package com.truecaller.premium;

import c.g.b.k;

final class bn$a
{
  private final long a;
  private final String b;
  private final boolean c;
  private final boolean d;
  
  public bn$a(long paramLong, String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    a = paramLong;
    b = paramString;
    c = paramBoolean1;
    d = paramBoolean2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof a;
      if (bool2)
      {
        paramObject = (a)paramObject;
        long l1 = a;
        long l2 = a;
        bool2 = l1 < l2;
        String str1;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          str1 = null;
        }
        if (bool2)
        {
          str1 = b;
          String str2 = b;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            bool2 = c;
            boolean bool3 = c;
            if (bool2 == bool3)
            {
              bool2 = true;
            }
            else
            {
              bool2 = false;
              str1 = null;
            }
            if (bool2)
            {
              bool2 = d;
              boolean bool4 = d;
              if (bool2 == bool4)
              {
                bool4 = true;
              }
              else
              {
                bool4 = false;
                paramObject = null;
              }
              if (bool4) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    int i = 32;
    long l2 = l1 >>> i;
    l1 ^= l2;
    int j = (int)l1 * 31;
    String str = b;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    j = (j + k) * 31;
    int m = c;
    i = 1;
    if (m != 0) {
      m = 1;
    }
    j = (j + m) * 31;
    int n = d;
    if (n != 0) {
      n = 1;
    }
    return j + n;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PremiumState(duration=");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(", level=");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", hasSubscriptionProblem=");
    boolean bool = c;
    localStringBuilder.append(bool);
    localStringBuilder.append(", isInGracePeriod=");
    bool = d;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.bn.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */