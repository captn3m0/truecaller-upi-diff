package com.truecaller.premium;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.b;
import android.support.design.widget.AppBarLayout.c;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.b;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.f;
import android.support.v4.view.o;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import c.u;
import c.x;
import com.d.b.ab;
import com.d.b.w;
import com.truecaller.R.id;
import com.truecaller.premium.data.PremiumType;
import com.truecaller.utils.extensions.t;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public final class as
  extends Fragment
  implements ay, l
{
  public static final as.a c;
  public av a;
  public PremiumPresenterView.LaunchContext b;
  private h d;
  private j e;
  private HashMap f;
  
  static
  {
    as.a locala = new com/truecaller/premium/as$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public final void a()
  {
    int i = R.id.appBar;
    Object localObject1 = (AppBarLayout)e(i);
    Object localObject2 = new com/truecaller/premium/as$f;
    ((as.f)localObject2).<init>(this);
    localObject2 = (AppBarLayout.c)localObject2;
    ((AppBarLayout)localObject1).a((AppBarLayout.c)localObject2);
    i = R.id.appBar;
    localObject1 = (AppBarLayout)e(i);
    c.g.b.k.a(localObject1, "appBar");
    localObject1 = ((AppBarLayout)localObject1).getLayoutParams();
    int j = getResources().getDimensionPixelSize(2131166097);
    int k = getResources().getDimensionPixelSize(2131166110);
    j += k;
    height = j;
    i = R.id.tabLayout;
    localObject1 = (TabLayout)e(i);
    c.g.b.k.a(localObject1, "tabLayout");
    t.a((View)localObject1);
    i = R.id.tabLayout;
    localObject1 = (TabLayout)e(i);
    j = R.id.viewPager;
    localObject2 = (ViewPager)e(j);
    ((TabLayout)localObject1).setupWithViewPager((ViewPager)localObject2);
    localObject1 = getResources();
    i = ((Resources)localObject1).getDimensionPixelSize(2131165470);
    j = R.id.tabLayout;
    localObject2 = (TabLayout)e(j);
    k = 0;
    localObject2 = ((TabLayout)localObject2).getChildAt(0);
    boolean bool1 = localObject2 instanceof ViewGroup;
    if (bool1)
    {
      localObject2 = (ViewGroup)localObject2;
      int m = ((ViewGroup)localObject2).getChildCount();
      while (k < m)
      {
        Object localObject3 = ((ViewGroup)localObject2).getChildAt(k);
        String str = "tabView";
        c.g.b.k.a(localObject3, str);
        localObject3 = ((View)localObject3).getLayoutParams();
        boolean bool2 = localObject3 instanceof ViewGroup.MarginLayoutParams;
        if (bool2)
        {
          localObject3 = (ViewGroup.MarginLayoutParams)localObject3;
          leftMargin = i;
          rightMargin = i;
        }
        k += 1;
      }
      i = R.id.tabLayout;
      localObject1 = (TabLayout)e(i);
      ((TabLayout)localObject1).requestLayout();
    }
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.viewPager;
    ((ViewPager)e(i)).a(paramInt, false);
  }
  
  public final void a(Uri paramUri)
  {
    c.g.b.k.b(paramUri, "uri");
    paramUri = w.a(requireContext()).a(paramUri);
    int i = R.id.premiumHeaderImage;
    ImageView localImageView = (ImageView)e(i);
    paramUri.a(localImageView, null);
  }
  
  public final void a(PremiumType paramPremiumType)
  {
    c.g.b.k.b(paramPremiumType, "premiumType");
    j localj = e;
    if (localj != null)
    {
      localj.a(paramPremiumType);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "title");
    int i = R.id.collapsingToolbar;
    CollapsingToolbarLayout localCollapsingToolbarLayout = (CollapsingToolbarLayout)e(i);
    c.g.b.k.a(localCollapsingToolbarLayout, "collapsingToolbar");
    paramString = (CharSequence)paramString;
    localCollapsingToolbarLayout.setTitle(paramString);
  }
  
  public final void a(List paramList)
  {
    c.g.b.k.b(paramList, "pages");
    Object localObject = new com/truecaller/premium/as$b;
    Context localContext = requireContext();
    c.g.b.k.a(localContext, "requireContext()");
    android.support.v4.app.j localj = getChildFragmentManager();
    c.g.b.k.a(localj, "childFragmentManager");
    ((as.b)localObject).<init>(localContext, paramList, localj);
    int i = R.id.viewPager;
    paramList = (ViewPager)e(i);
    c.g.b.k.a(paramList, "viewPager");
    localObject = (o)localObject;
    paramList.setAdapter((o)localObject);
    i = R.id.viewPager;
    paramList = (ViewPager)e(i);
    localObject = new com/truecaller/premium/as$e;
    ((as.e)localObject).<init>(this);
    localObject = (ViewPager.f)localObject;
    paramList.a((ViewPager.f)localObject);
  }
  
  public final void b(int paramInt)
  {
    int i = R.id.viewPager;
    ((ViewPager)e(i)).a(paramInt, true);
  }
  
  public final k c()
  {
    Object localObject = getParentFragment();
    if (localObject != null) {
      return ((l)localObject).c();
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.premium.PremiumComponentProvider");
    throw ((Throwable)localObject);
  }
  
  public final void c(int paramInt)
  {
    int i = R.id.tabLayout;
    ((TabLayout)e(i)).setSelectedTabIndicatorColor(paramInt);
    i = R.id.tabLayout;
    TabLayout localTabLayout = (TabLayout)e(i);
    int j = b.c(requireContext(), 2131100400);
    localTabLayout.a(j, paramInt);
  }
  
  public final void d(int paramInt)
  {
    int i = R.id.collapsingToolbar;
    ((CollapsingToolbarLayout)e(i)).setContentScrimColor(paramInt);
  }
  
  public final View e(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    paramBundle = getArguments();
    Object localObject;
    if (paramBundle != null)
    {
      localObject = "type";
      paramBundle = paramBundle.getSerializable((String)localObject);
      if (paramBundle != null) {}
    }
    else
    {
      paramBundle = (Serializable)PremiumType.PREMIUM;
    }
    if (paramBundle != null)
    {
      paramBundle = (PremiumType)paramBundle;
      localObject = getParentFragment();
      if (localObject != null)
      {
        localObject = ((l)localObject).c();
        at localat = new com/truecaller/premium/at;
        localat.<init>(paramBundle);
        ((k)localObject).a(localat).a(this);
        paramBundle = b;
        if (paramBundle == null)
        {
          localObject = "launchContext";
          c.g.b.k.a((String)localObject);
        }
        localObject = PremiumPresenterView.LaunchContext.BOTTOM_BAR;
        if (paramBundle == localObject)
        {
          int i = R.id.collapsingToolbar;
          paramBundle = (CollapsingToolbarLayout)e(i);
          localObject = "collapsingToolbar";
          c.g.b.k.a(paramBundle, (String)localObject);
          paramBundle = paramBundle.getLayoutParams();
          if (paramBundle != null)
          {
            paramBundle = (AppBarLayout.b)paramBundle;
            paramBundle.a();
          }
          else
          {
            paramBundle = new c/u;
            paramBundle.<init>("null cannot be cast to non-null type android.support.design.widget.AppBarLayout.LayoutParams");
            throw paramBundle;
          }
        }
        paramBundle = a;
        if (paramBundle == null)
        {
          localObject = "presenter";
          c.g.b.k.a((String)localObject);
        }
        paramBundle.a(this);
        return;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.PremiumComponentProvider");
      throw paramBundle;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.data.PremiumType");
    throw paramBundle;
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getParentFragment();
    if (paramContext != null)
    {
      paramContext = (h)paramContext;
      d = paramContext;
      paramContext = getParentFragment();
      if (paramContext != null)
      {
        paramContext = (j)paramContext;
        e = paramContext;
        return;
      }
      paramContext = new c/u;
      paramContext.<init>("null cannot be cast to non-null type com.truecaller.premium.OnTypeSelectedListener");
      throw paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.premium.FeaturesStyleProvider");
    throw paramContext;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558758, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    ((av)localObject).y_();
    localObject = f;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onDetach()
  {
    super.onDetach();
    d = null;
    e = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    int i = R.id.appBar;
    paramView = (AppBarLayout)e(i);
    paramBundle = new com/truecaller/premium/as$c;
    paramBundle.<init>(this);
    paramBundle = (AppBarLayout.c)paramBundle;
    paramView.a(paramBundle);
    paramView = d;
    if (paramView != null)
    {
      paramView = paramView.a();
      if (paramView != null)
      {
        paramView = a;
        if (paramView != null)
        {
          paramBundle = paramView;
          paramBundle = (Number)paramView;
          j = paramBundle.intValue();
          int k = R.id.toolbar;
          Toolbar localToolbar = (Toolbar)e(k);
          localToolbar.setNavigationIcon(j);
          if (paramView != null) {
            break label135;
          }
        }
      }
    }
    i = R.id.toolbar;
    paramView = (Toolbar)e(i);
    int j = 0;
    paramBundle = null;
    paramView.setNavigationIcon(null);
    paramView = x.a;
    label135:
    i = R.id.toolbar;
    paramView = (Toolbar)e(i);
    paramBundle = new com/truecaller/premium/as$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setNavigationOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.as
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */