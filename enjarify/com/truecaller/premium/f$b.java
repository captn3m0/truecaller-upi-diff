package com.truecaller.premium;

import c.g.b.k;
import com.google.common.util.concurrent.FutureCallback;
import com.truecaller.log.AssertionUtil;

public final class f$b
  implements FutureCallback
{
  f$b(f paramf) {}
  
  public final void onFailure(Throwable paramThrowable)
  {
    k.b(paramThrowable, "t");
    AssertionUtil.reportThrowableButNeverCrash(paramThrowable);
    a.a(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */