package com.truecaller.premium;

import c.g.b.k;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public final class bx
  implements bw
{
  private final Locale a;
  
  public bx(Locale paramLocale)
  {
    a = paramLocale;
  }
  
  public final String a(String paramString, long paramLong, boolean paramBoolean)
  {
    k.b(paramString, "currencyCode");
    NumberFormat localNumberFormat = NumberFormat.getCurrencyInstance(a);
    paramString = Currency.getInstance(paramString);
    String str = "format";
    k.a(localNumberFormat, str);
    localNumberFormat.setCurrency(paramString);
    int i;
    if (paramBoolean)
    {
      i = 0;
      paramString = null;
    }
    else
    {
      i = 2;
    }
    localNumberFormat.setMinimumFractionDigits(i);
    double d1 = paramLong;
    long l = 4696837146684686336L;
    double d2 = 1000000.0D;
    Double.isNaN(d1);
    d1 /= d2;
    boolean bool1 = Double.isNaN(d1);
    if (!bool1)
    {
      l = 4746794007244308480L;
      d2 = 2.147483647E9D;
      bool1 = d1 < d2;
      int k;
      if (bool1)
      {
        int j = -1 >>> 1;
      }
      else
      {
        l = -4476578029606273024L;
        d2 = -2.147483648E9D;
        boolean bool2 = d1 < d2;
        if (bool2)
        {
          k = -1 << -1;
        }
        else
        {
          l = Math.round(d1);
          k = (int)l;
        }
      }
      if (paramBoolean) {
        paramString = Integer.valueOf(k);
      } else {
        paramString = Double.valueOf(d1);
      }
      paramString = localNumberFormat.format(paramString);
      k.a(paramString, "format.format(if (should…e else priceWithDecimals)");
      return paramString;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("Cannot round NaN value.");
    throw ((Throwable)paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.bx
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */