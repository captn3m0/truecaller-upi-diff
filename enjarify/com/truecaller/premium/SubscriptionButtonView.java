package com.truecaller.premium;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.b;
import android.support.v4.widget.m;
import android.text.Html;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.R.styleable;
import com.truecaller.utils.extensions.t;

public final class SubscriptionButtonView
  extends RelativeLayout
{
  private final int a = 2131559120;
  private TextView b;
  private TextView c;
  private TextView d;
  private ImageView e;
  private cc f;
  private boolean g;
  
  public SubscriptionButtonView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private SubscriptionButtonView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    int i = 1;
    g = i;
    int j = a;
    if (paramAttributeSet != null)
    {
      int[] arrayOfInt = R.styleable.SubscriptionButtonView;
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
      int k = a;
      j = paramContext.getResourceId(i, k);
      boolean bool = paramContext.getBoolean(0, i);
      g = bool;
      paramContext.recycle();
    }
    paramContext = getContext();
    paramAttributeSet = this;
    paramAttributeSet = (ViewGroup)this;
    RelativeLayout.inflate(paramContext, j, paramAttributeSet);
    paramContext = findViewById(2131364681);
    k.a(paramContext, "findViewById(R.id.text)");
    paramContext = (TextView)paramContext;
    b = paramContext;
    paramContext = (TextView)findViewById(2131364006);
    c = paramContext;
    paramContext = (TextView)findViewById(2131364603);
    d = paramContext;
    paramContext = (ImageView)findViewById(2131363301);
    e = paramContext;
  }
  
  public final cc getButton()
  {
    return f;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    Object localObject1 = new android/util/DisplayMetrics;
    ((DisplayMetrics)localObject1).<init>();
    Object localObject2 = getContext();
    if (localObject2 != null)
    {
      localObject2 = ((Activity)localObject2).getWindowManager();
      k.a(localObject2, "(context as Activity).windowManager");
      localObject2 = ((WindowManager)localObject2).getDefaultDisplay();
      ((Display)localObject2).getRealMetrics((DisplayMetrics)localObject1);
      int i = widthPixels;
      double d1 = i;
      double d2 = 0.6D;
      Double.isNaN(d1);
      d1 *= d2;
      TextView localTextView = b;
      if (localTextView == null)
      {
        String str = "textView";
        k.a(str);
      }
      i = (int)d1;
      localTextView.setMaxWidth(i);
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.app.Activity");
    throw ((Throwable)localObject1);
  }
  
  public final void setButton(cc paramcc)
  {
    Object localObject1 = "button";
    k.b(paramcc, (String)localObject1);
    f = paramcc;
    int i = e;
    setBackgroundResource(i);
    boolean bool = g;
    int k = 1;
    if (bool)
    {
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject2 = "textView";
        k.a((String)localObject2);
      }
      ((TextView)localObject1).setMaxLines(k);
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject2 = "textView";
        k.a((String)localObject2);
      }
      m.c((TextView)localObject1);
    }
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "textView";
      k.a((String)localObject2);
    }
    Object localObject2 = (CharSequence)Html.fromHtml(a);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "textView";
      k.a((String)localObject2);
    }
    localObject2 = getContext();
    int m = d;
    int n = b.c((Context)localObject2, m);
    ((TextView)localObject1).setTextColor(n);
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = (CharSequence)b;
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject1 = (Number)localObject1;
      j = ((Number)localObject1).intValue();
      localObject2 = c;
      if (localObject2 != null) {
        ((TextView)localObject2).setBackgroundResource(j);
      }
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = (CharSequence)c;
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = getContext();
      m = d;
      n = b.c((Context)localObject2, m);
      ((TextView)localObject1).setTextColor(n);
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      localObject1 = g;
      if (localObject1 != null)
      {
        j = ((Integer)localObject1).intValue();
        if (j == 0) {}
      }
      else
      {
        localObject1 = e;
        if (localObject1 != null)
        {
          paramcc = g;
          int i1 = paramcc.intValue();
          ((ImageView)localObject1).setImageResource(i1);
        }
      }
    }
    paramcc = c;
    int j = 0;
    localObject1 = null;
    n = 0;
    localObject2 = null;
    Object localObject3;
    if (paramcc != null)
    {
      paramcc = (View)paramcc;
      localObject3 = f;
      if (localObject3 != null)
      {
        localObject3 = b;
      }
      else
      {
        m = 0;
        localObject3 = null;
      }
      if (localObject3 != null)
      {
        m = 1;
      }
      else
      {
        m = 0;
        localObject3 = null;
      }
      t.a(paramcc, m);
    }
    paramcc = d;
    if (paramcc != null)
    {
      paramcc = (View)paramcc;
      localObject3 = f;
      if (localObject3 != null) {
        localObject2 = c;
      }
      if (localObject2 != null) {
        j = 1;
      }
      t.a(paramcc, j);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.SubscriptionButtonView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */