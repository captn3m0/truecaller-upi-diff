package com.truecaller.premium;

import android.content.Context;
import com.google.android.libraries.nbu.engagementrewards.models.Promotion;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.ba;
import com.truecaller.engagementrewards.g;
import com.truecaller.premium.data.s;
import com.truecaller.premium.data.s.a;
import com.truecaller.utils.n;
import java.util.HashMap;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;

public final class f
  extends ba
  implements e
{
  public PremiumPresenterView.LaunchContext c;
  Promotion d;
  private HashMap e;
  private final com.truecaller.common.f.c f;
  private final s g;
  private final n h;
  private final b i;
  private final s.a j;
  private final com.truecaller.utils.d k;
  private final bw l;
  private final m m;
  private final com.truecaller.engagementrewards.k n;
  private final com.truecaller.engagementrewards.c o;
  private final g p;
  private final com.truecaller.common.g.a q;
  private final g r;
  private final com.truecaller.engagementrewards.ui.d s;
  private final c.d.f t;
  
  public f(com.truecaller.common.f.c paramc, s params, n paramn, b paramb, s.a parama, com.truecaller.utils.d paramd, bw parambw, m paramm, com.truecaller.engagementrewards.k paramk, com.truecaller.engagementrewards.c paramc1, g paramg1, com.truecaller.common.g.a parama1, g paramg2, com.truecaller.engagementrewards.ui.d paramd1, c.d.f paramf)
  {
    super(paramf);
    f = paramc;
    g = params;
    h = paramn;
    i = paramb;
    j = parama;
    k = paramd;
    l = parambw;
    m = paramm;
    n = paramk;
    o = paramc1;
    p = paramg1;
    q = parama1;
    r = paramg2;
    s = paramd1;
    t = paramf;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    e = ((HashMap)localObject);
  }
  
  private final bn a(cb paramcb)
  {
    String str = a;
    if (str != null)
    {
      ag localag = (ag)bg.a;
      c.d.f localf = t;
      Object localObject = new com/truecaller/premium/f$d;
      ((f.d)localObject).<init>(str, null, this, paramcb);
      localObject = (c.g.a.m)localObject;
      return kotlinx.coroutines.e.b(localag, localf, (c.g.a.m)localObject, 2);
    }
    return null;
  }
  
  private final void a(e.a parama)
  {
    b localb = i;
    String str1 = "source";
    Object localObject = c;
    if (localObject == null)
    {
      String str2 = "viewLaunchContext";
      c.g.b.k.a(str2);
    }
    localObject = ((PremiumPresenterView.LaunchContext)localObject).name();
    parama = parama.a(str1, (String)localObject).a();
    c.g.b.k.a(parama, "builder.param(Subscripti…unchContext.name).build()");
    localb.b(parama);
  }
  
  public final void a()
  {
    Object localObject1 = (EmbeddedSubscriptionButtonsView)b;
    if (localObject1 != null)
    {
      PremiumPresenterView.LaunchContext localLaunchContext = c;
      if (localLaunchContext == null)
      {
        localObject2 = "viewLaunchContext";
        c.g.b.k.a((String)localObject2);
      }
      c.g.b.k.b(localLaunchContext, "launchContext");
      Object localObject2 = b;
      if (localObject2 == null)
      {
        localObject2 = "premiumScreenNavigator";
        c.g.b.k.a((String)localObject2);
      }
      localObject1 = ((EmbeddedSubscriptionButtonsView)localObject1).getContext();
      c.g.b.k.a(localObject1, "context");
      br.a((Context)localObject1, localLaunchContext);
      return;
    }
  }
  
  public final void a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    c.g.b.k.b(paramLaunchContext, "launchContext");
    c = paramLaunchContext;
  }
  
  public final void a(cc paramcc)
  {
    c.g.b.k.b(paramcc, "subscriptionButton");
    Object localObject1 = e;
    paramcc = (cb)((HashMap)localObject1).get(paramcc);
    if (paramcc != null)
    {
      localObject1 = d;
      if (localObject1 != null)
      {
        localObject1 = o;
        boolean bool = ((com.truecaller.engagementrewards.c)localObject1).b();
        if (bool)
        {
          localObject1 = o;
          Object localObject2 = a;
          bool = ((com.truecaller.engagementrewards.c)localObject1).a((String)localObject2);
          if (bool)
          {
            localObject1 = (Context)j.provideActivity();
            localObject2 = new com/truecaller/premium/f$c;
            ((f.c)localObject2).<init>(paramcc, this);
            localObject2 = (c.g.a.a)localObject2;
            com.truecaller.engagementrewards.ui.d.c((Context)localObject1, (c.g.a.a)localObject2);
            return;
          }
        }
      }
      c.g.b.k.a(paramcc, "it");
      a(paramcc);
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    e.a locala1 = new com/truecaller/analytics/e$a;
    locala1.<init>("ANDROID_subscription_launched");
    e.a locala2 = locala1.a("EngRewardShown", paramBoolean);
    c.g.b.k.a(locala2, "Builder(SubscriptionsLau…gagementRewardPromoShown)");
    a(locala2);
  }
  
  public final PremiumPresenterView.LaunchContext e()
  {
    PremiumPresenterView.LaunchContext localLaunchContext = c;
    if (localLaunchContext == null)
    {
      String str = "viewLaunchContext";
      c.g.b.k.a(str);
    }
    return localLaunchContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */