package com.truecaller.premium;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.a;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.a.m;
import c.u;
import com.truecaller.R.id;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.engagementrewards.ui.d;
import com.truecaller.log.AssertionUtil;
import com.truecaller.premium.data.PremiumType;
import com.truecaller.premium.data.SubscriptionPromoEventMetaData;
import com.truecaller.ui.ab;
import com.truecaller.ui.components.WindowInsetsFrameLayout;
import com.truecaller.utils.extensions.t;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class ba
  extends Fragment
  implements PremiumPresenterView, h, i, j, l, ab
{
  public static final ba.a c;
  public bm a;
  public d b;
  private k d;
  private ba.b e;
  private as f;
  private Dialog g;
  private ViewGroup h;
  private PremiumPresenterView.LaunchContext i;
  private final int j;
  private HashMap k;
  
  static
  {
    ba.a locala = new com/truecaller/premium/ba$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = k;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      k = ((HashMap)localObject1);
    }
    localObject1 = k;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = k;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final ba a(PremiumPresenterView.LaunchContext paramLaunchContext, ba.b paramb)
  {
    return ba.a.a(paramLaunchContext, null, null, paramb);
  }
  
  private final void s()
  {
    f = null;
    Object localObject1 = getChildFragmentManager();
    c.g.b.k.a(localObject1, "childFragmentManager");
    int m = ((android.support.v4.app.j)localObject1).e();
    int n = 0;
    Object localObject2 = null;
    Object localObject3;
    while (n < m)
    {
      localObject3 = getChildFragmentManager();
      ((android.support.v4.app.j)localObject3).d();
      n += 1;
    }
    localObject1 = getChildFragmentManager();
    c.g.b.k.a(localObject1, "childFragmentManager");
    localObject1 = ((android.support.v4.app.j)localObject1).f();
    localObject2 = "childFragmentManager.fragments";
    c.g.b.k.a(localObject1, (String)localObject2);
    localObject1 = c.m.l.c(m.n((Iterable)localObject1)).a();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Fragment)((Iterator)localObject1).next();
      localObject3 = getChildFragmentManager().a();
      localObject2 = ((android.support.v4.app.o)localObject3).a((Fragment)localObject2);
      ((android.support.v4.app.o)localObject2).f();
    }
  }
  
  public final ba.b a()
  {
    ba.b localb = e;
    if (localb == null)
    {
      String str = "premiumFeaturesStyle";
      c.g.b.k.a(str);
    }
    return localb;
  }
  
  public final void a(PremiumType paramPremiumType)
  {
    c.g.b.k.b(paramPremiumType, "premiumType");
    bm localbm = a;
    if (localbm == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localbm.a(paramPremiumType);
  }
  
  public final void a(PremiumType paramPremiumType, int paramInt, boolean paramBoolean)
  {
    c.g.b.k.b(paramPremiumType, "type");
    android.support.v4.app.o localo = getChildFragmentManager().a();
    String str1 = "childFragmentManager.beginTransaction()";
    c.g.b.k.a(localo, str1);
    if (paramBoolean)
    {
      m = 4097;
      localo.a(m);
    }
    int m = 2131362590;
    Object localObject = q.b;
    c.g.b.k.b(paramPremiumType, "type");
    localObject = new com/truecaller/premium/q;
    ((q)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str2 = "type";
    paramPremiumType = (Serializable)paramPremiumType;
    localBundle.putSerializable(str2, paramPremiumType);
    localBundle.putInt("initial_position", paramInt);
    ((q)localObject).setArguments(localBundle);
    localObject = (Fragment)localObject;
    localo.a(m, (Fragment)localObject).a("details").d();
    int n = R.id.buttonsShadow;
    paramPremiumType = a(n);
    String str3 = "buttonsShadow";
    c.g.b.k.a(paramPremiumType, str3);
    t.b(paramPremiumType);
    if (!paramBoolean)
    {
      paramPremiumType = getChildFragmentManager();
      paramPremiumType.b();
    }
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "navigationUrl");
    Object localObject = this;
    localObject = (Fragment)this;
    paramString = com.truecaller.common.h.o.a(paramString);
    com.truecaller.common.h.o.a((Fragment)localObject, paramString);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    c.g.b.k.b(paramString1, "title");
    c.g.b.k.b(paramString2, "message");
    c.g.b.k.b(paramString3, "button");
    c.g.b.k.b(paramString4, "userInteractionContext");
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = new android/support/design/widget/a;
      Object localObject2 = requireContext();
      ((a)localObject1).<init>((Context)localObject2);
      localObject1 = (Dialog)localObject1;
      g = ((Dialog)localObject1);
      localObject1 = getLayoutInflater();
      int m = R.id.content;
      Object localObject3 = (WindowInsetsFrameLayout)a(m);
      localObject1 = ((LayoutInflater)localObject1).inflate(2131558828, (ViewGroup)localObject3, false);
      int n = 2131364884;
      localObject2 = ((View)localObject1).findViewById(n);
      localObject3 = "view.findViewById<TextView>(R.id.title)";
      c.g.b.k.a(localObject2, (String)localObject3);
      localObject2 = (TextView)localObject2;
      paramString1 = (CharSequence)paramString1;
      ((TextView)localObject2).setText(paramString1);
      paramString1 = ((View)localObject1).findViewById(2131363752);
      localObject2 = "view.findViewById<TextView>(R.id.message)";
      c.g.b.k.a(paramString1, (String)localObject2);
      paramString1 = (TextView)paramString1;
      paramString2 = (CharSequence)paramString2;
      paramString1.setText(paramString2);
      int i1 = 2131362240;
      paramString1 = (Button)((View)localObject1).findViewById(i1);
      paramString2 = paramString3;
      paramString2 = (CharSequence)paramString3;
      paramString1.setText(paramString2);
      paramString2 = new com/truecaller/premium/ba$h;
      paramString2.<init>(this, paramString3, paramString4);
      paramString2 = (View.OnClickListener)paramString2;
      paramString1.setOnClickListener(paramString2);
      paramString1 = g;
      if (paramString1 != null) {
        paramString1.setContentView((View)localObject1);
      }
    }
    paramString1 = g;
    if (paramString1 != null)
    {
      paramString1.show();
      return;
    }
  }
  
  public final void a(List paramList, int paramInt)
  {
    c.g.b.k.b(paramList, "subscriptionButtons");
    Object localObject1 = h;
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 != null)
      {
        int m = 2131364608;
        localObject1 = (ViewStub)((View)localObject1).findViewById(m);
        ((ViewStub)localObject1).setLayoutResource(paramInt);
        localObject2 = ((ViewStub)localObject1).inflate();
        if (localObject2 != null)
        {
          localObject2 = (ViewGroup)localObject2;
          h = ((ViewGroup)localObject2);
        }
        else
        {
          paramList = new c/u;
          paramList.<init>("null cannot be cast to non-null type android.view.ViewGroup");
          throw paramList;
        }
      }
    }
    Object localObject2 = h;
    if (localObject2 != null)
    {
      localObject1 = localObject2;
      localObject1 = (View)localObject2;
      boolean bool = ((Collection)paramList).isEmpty() ^ true;
      t.a((View)localObject1, bool);
      int i1 = R.id.buttonsShadow;
      localObject1 = a(i1);
      c.g.b.k.a(localObject1, "buttonsShadow");
      int n = ((ViewGroup)localObject2).getVisibility();
      ((View)localObject1).setVisibility(n);
      i1 = ((ViewGroup)localObject2).getChildCount();
      n = 0;
      int i2 = 0;
      while (i2 < i1)
      {
        int i3 = paramList.size();
        Object localObject3;
        Object localObject4;
        if (i2 < i3)
        {
          localObject3 = ((ViewGroup)localObject2).getChildAt(i2);
          if (localObject3 != null)
          {
            localObject3 = (SubscriptionButtonView)localObject3;
            localObject4 = (cc)paramList.get(i2);
            ((SubscriptionButtonView)localObject3).setButton((cc)localObject4);
            localObject4 = paramList.get(i2);
            ((SubscriptionButtonView)localObject3).setTag(localObject4);
            localObject4 = localObject3;
            t.a((View)localObject3);
            localObject4 = new com/truecaller/premium/ba$c;
            ((ba.c)localObject4).<init>((SubscriptionButtonView)localObject3, i2, this, paramList);
            localObject4 = (View.OnClickListener)localObject4;
            ((SubscriptionButtonView)localObject3).setOnClickListener((View.OnClickListener)localObject4);
          }
          else
          {
            paramList = new c/u;
            paramList.<init>("null cannot be cast to non-null type com.truecaller.premium.SubscriptionButtonView");
            throw paramList;
          }
        }
        else
        {
          localObject3 = ((ViewGroup)localObject2).getChildAt(i2);
          localObject4 = "it.getChildAt(index)";
          c.g.b.k.a(localObject3, (String)localObject4);
          t.a((View)localObject3, false);
        }
        i2 += 1;
      }
      return;
    }
  }
  
  public final void a(boolean paramBoolean) {}
  
  public final void b()
  {
    int m = R.id.buttonsShadow;
    View localView = a(m);
    c.g.b.k.a(localView, "buttonsShadow");
    ViewGroup localViewGroup = h;
    int n;
    if (localViewGroup != null) {
      n = localViewGroup.getVisibility();
    } else {
      n = 8;
    }
    localView.setVisibility(n);
  }
  
  public final void b(PremiumType paramPremiumType)
  {
    c.g.b.k.b(paramPremiumType, "selectedType");
    Object localObject1 = getChildFragmentManager();
    int m = 2131362590;
    localObject1 = ((android.support.v4.app.j)localObject1).a(m);
    if (localObject1 != null) {
      s();
    }
    localObject1 = as.c;
    c.g.b.k.b(paramPremiumType, "selectedType");
    localObject1 = new com/truecaller/premium/as;
    ((as)localObject1).<init>();
    Object localObject2 = new android/os/Bundle;
    ((Bundle)localObject2).<init>();
    paramPremiumType = (Serializable)paramPremiumType;
    ((Bundle)localObject2).putSerializable("type", paramPremiumType);
    ((as)localObject1).setArguments((Bundle)localObject2);
    paramPremiumType = getChildFragmentManager().a();
    localObject2 = localObject1;
    localObject2 = (Fragment)localObject1;
    paramPremiumType.a(m, (Fragment)localObject2).f();
    f = ((as)localObject1);
  }
  
  public final void b(String paramString)
  {
    az localaz = new com/truecaller/premium/az;
    Object localObject = requireContext();
    localaz.<init>((Context)localObject);
    localObject = new com/truecaller/premium/ba$f;
    ((ba.f)localObject).<init>(this);
    localObject = (az.a)localObject;
    localaz.a((az.a)localObject);
    localObject = new com/truecaller/premium/ba$g;
    ((ba.g)localObject).<init>(this);
    localObject = (az.a)localObject;
    localaz.b((az.a)localObject);
    localaz.a(paramString);
    localaz.show();
  }
  
  public final void b(boolean paramBoolean)
  {
    int m = R.id.progressBar;
    Object localObject1 = (ProgressBar)a(m);
    c.g.b.k.a(localObject1, "progressBar");
    t.a((View)localObject1, paramBoolean);
    m = R.id.content;
    localObject1 = (WindowInsetsFrameLayout)a(m);
    String str = "content";
    c.g.b.k.a(localObject1, str);
    int n;
    if (paramBoolean)
    {
      n = 4;
    }
    else
    {
      n = 0;
      str = null;
    }
    ((WindowInsetsFrameLayout)localObject1).setVisibility(n);
    if (paramBoolean)
    {
      Object localObject2 = h;
      if (localObject2 != null)
      {
        localObject2 = (View)localObject2;
        t.b((View)localObject2);
      }
      paramBoolean = R.id.buttonsShadow;
      localObject2 = a(paramBoolean);
      localObject1 = "buttonsShadow";
      c.g.b.k.a(localObject2, (String)localObject1);
      t.b((View)localObject2);
    }
  }
  
  public final k c()
  {
    k localk = d;
    if (localk == null)
    {
      String str = "component";
      c.g.b.k.a(str);
    }
    return localk;
  }
  
  public final void c(PremiumType paramPremiumType)
  {
    c.g.b.k.b(paramPremiumType, "type");
    Object localObject = getChildFragmentManager();
    String str = "details";
    ((android.support.v4.app.j)localObject).c(str);
    localObject = f;
    if (localObject != null)
    {
      str = "type";
      c.g.b.k.b(paramPremiumType, str);
      localObject = a;
      if (localObject == null)
      {
        str = "presenter";
        c.g.b.k.a(str);
      }
      ((av)localObject).a(paramPremiumType);
      return;
    }
  }
  
  public final bm d()
  {
    bm localbm = a;
    if (localbm == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return localbm;
  }
  
  public final void e()
  {
    Dialog localDialog = g;
    if (localDialog != null)
    {
      localDialog.dismiss();
      return;
    }
  }
  
  public final int f()
  {
    return j;
  }
  
  public final void g()
  {
    requireActivity().finish();
  }
  
  public final void h()
  {
    Toast.makeText(requireContext(), 2131886125, 0).show();
  }
  
  public final void i()
  {
    Toast.makeText(requireContext(), 2131886126, 0).show();
  }
  
  public final void j()
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = requireContext();
    localBuilder.<init>((Context)localObject);
    localBuilder = localBuilder.setMessage(2131886124).setCancelable(false);
    localObject = new com/truecaller/premium/ba$d;
    ((ba.d)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    localBuilder = localBuilder.setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject);
    localObject = new com/truecaller/premium/ba$e;
    ((ba.e)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    localBuilder.setNegativeButton(2131887214, (DialogInterface.OnClickListener)localObject).create().show();
  }
  
  public final void k()
  {
    Toast.makeText(requireContext(), 2131886899, 0).show();
  }
  
  public final void l()
  {
    Toast.makeText(requireContext(), 2131886874, 0).show();
  }
  
  public final void m() {}
  
  public final void n()
  {
    bm localbm = a;
    if (localbm == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localbm.f();
  }
  
  public final void o()
  {
    Toast.makeText(requireContext(), 2131886873, 0).show();
  }
  
  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    s();
    paramBundle = a;
    if (paramBundle == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    paramBundle.a(this);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getArguments();
    Object localObject1 = null;
    Object localObject2;
    if (paramBundle != null)
    {
      localObject2 = "launchContext";
      paramBundle = paramBundle.getSerializable((String)localObject2);
    }
    else
    {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      paramBundle = (PremiumPresenterView.LaunchContext)paramBundle;
      i = paramBundle;
      paramBundle = getArguments();
      if (paramBundle != null)
      {
        localObject2 = "analyticsMetadata";
        paramBundle = (SubscriptionPromoEventMetaData)paramBundle.getParcelable((String)localObject2);
      }
      else
      {
        paramBundle = null;
      }
      localObject2 = getArguments();
      if (localObject2 != null)
      {
        localObject3 = "selectedPage";
        localObject2 = ((Bundle)localObject2).getString((String)localObject3);
      }
      else
      {
        localObject2 = null;
      }
      Object localObject3 = getArguments();
      if (localObject3 != null) {
        localObject1 = ((Bundle)localObject3).getSerializable("premiumFeaturesStyle");
      }
      if (localObject1 != null)
      {
        localObject1 = (ba.b)localObject1;
        e = ((ba.b)localObject1);
        localObject1 = i;
        if (localObject1 == null)
        {
          localObject3 = "launchContext";
          c.g.b.k.a((String)localObject3);
        }
        localObject3 = new String[] { "Launch context is not set" };
        AssertionUtil.isNotNull(localObject1, (String[])localObject3);
        localObject1 = b.a();
        localObject3 = requireContext();
        Object localObject4 = "requireContext()";
        c.g.b.k.a(localObject3, (String)localObject4);
        localObject3 = ((Context)localObject3).getApplicationContext();
        if (localObject3 != null)
        {
          localObject3 = ((bk)localObject3).a();
          localObject1 = ((b.a)localObject1).a((bp)localObject3);
          localObject3 = new com/truecaller/premium/bf;
          localObject4 = (Activity)requireActivity();
          PremiumPresenterView.LaunchContext localLaunchContext = i;
          if (localLaunchContext == null)
          {
            String str = "launchContext";
            c.g.b.k.a(str);
          }
          ((bf)localObject3).<init>((Activity)localObject4, localLaunchContext, (String)localObject2, paramBundle);
          paramBundle = ((b.a)localObject1).a((bf)localObject3).a();
          localObject1 = "DaggerPremiumComponent.b…ta))\n            .build()";
          c.g.b.k.a(paramBundle, (String)localObject1);
          d = paramBundle;
          paramBundle = d;
          if (paramBundle == null)
          {
            localObject1 = "component";
            c.g.b.k.a((String)localObject1);
          }
          paramBundle.a(this);
          return;
        }
        paramBundle = new c/u;
        paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
        throw paramBundle;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.PremiumFragment.PremiumFeaturesStyle");
      throw paramBundle;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.PremiumPresenterView.LaunchContext");
    throw paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558755, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    ((bm)localObject).y_();
    localObject = k;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    bm localbm = a;
    if (localbm == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localbm.f();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    int m = R.id.outerContainer;
    paramView = (ConstraintLayout)a(m);
    paramBundle = "outerContainer";
    c.g.b.k.a(paramView, paramBundle);
    paramView = paramView.getLayoutParams();
    if (paramView != null)
    {
      paramView = (FrameLayout.LayoutParams)paramView;
      paramBundle = e;
      if (paramBundle == null)
      {
        String str = "premiumFeaturesStyle";
        c.g.b.k.a(str);
      }
      int n = d;
      bottomMargin = n;
      paramView = i;
      if (paramView == null)
      {
        paramBundle = "launchContext";
        c.g.b.k.a(paramBundle);
      }
      paramBundle = PremiumPresenterView.LaunchContext.BOTTOM_BAR;
      if (paramView == paramBundle)
      {
        m = R.id.outerContainer;
        paramView = (ConstraintLayout)a(m);
        paramBundle = requireContext();
        int i1 = 2131100638;
        n = android.support.v4.content.b.c(paramBundle, i1);
        paramView.setBackgroundColor(n);
      }
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
    throw paramView;
  }
  
  public final void p()
  {
    Toast.makeText(requireContext(), 2131886877, 0).show();
  }
  
  public final void q()
  {
    Toast.makeText(requireContext(), 2131886912, 0).show();
  }
  
  public final void r()
  {
    Toast.makeText(requireContext(), 2131886893, 0).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.ba
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */