package com.truecaller.premium;

import c.g.b.k;

public final class cc
{
  final String a;
  final String b;
  final String c;
  final int d;
  final int e;
  final Integer f;
  final Integer g;
  
  public cc(String paramString1, String paramString2, String paramString3, int paramInt1, int paramInt2, Integer paramInteger1, Integer paramInteger2)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramInt1;
    e = paramInt2;
    f = paramInteger1;
    g = paramInteger2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof cc;
      if (bool2)
      {
        paramObject = (cc)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              int i = d;
              int j = d;
              if (i == j)
              {
                i = 1;
              }
              else
              {
                i = 0;
                localObject1 = null;
              }
              if (i != 0)
              {
                i = e;
                j = e;
                if (i == j)
                {
                  i = 1;
                }
                else
                {
                  i = 0;
                  localObject1 = null;
                }
                if (i != 0)
                {
                  localObject1 = f;
                  localObject2 = f;
                  boolean bool3 = k.a(localObject1, localObject2);
                  if (bool3)
                  {
                    localObject1 = g;
                    paramObject = g;
                    boolean bool4 = k.a(localObject1, paramObject);
                    if (bool4) {
                      return bool1;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = b;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    int k = d;
    j = (j + k) * 31;
    k = e;
    j = (j + k) * 31;
    localObject = f;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = g;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SubscriptionButton(title=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", profit=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", subTitle=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", textColorRes=");
    int i = d;
    localStringBuilder.append(i);
    localStringBuilder.append(", backgroundDrawableRes=");
    i = e;
    localStringBuilder.append(i);
    localStringBuilder.append(", profitViewBgDrawableRes=");
    localObject = f;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", startDrawableRes=");
    localObject = g;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.cc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */