package com.truecaller.premium;

public final class ca$a
{
  final float a;
  final float b;
  private final float c;
  
  public ca$a(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    c = paramFloat1;
    a = paramFloat2;
    b = paramFloat3;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool = paramObject instanceof a;
      if (bool)
      {
        paramObject = (a)paramObject;
        float f1 = c;
        float f2 = c;
        int i = Float.compare(f1, f2);
        if (i == 0)
        {
          f1 = a;
          f2 = a;
          i = Float.compare(f1, f2);
          if (i == 0)
          {
            f1 = b;
            float f3 = b;
            int j = Float.compare(f1, f3);
            if (j == 0) {
              break label92;
            }
          }
        }
      }
      return false;
    }
    label92:
    return true;
  }
  
  public final int hashCode()
  {
    int i = Float.floatToIntBits(c) * 31;
    int j = Float.floatToIntBits(a);
    i = (i + j) * 31;
    j = Float.floatToIntBits(b);
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Data(azimuth=");
    float f = c;
    localStringBuilder.append(f);
    localStringBuilder.append(", pitch=");
    f = a;
    localStringBuilder.append(f);
    localStringBuilder.append(", roll=");
    f = b;
    localStringBuilder.append(f);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.ca.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */