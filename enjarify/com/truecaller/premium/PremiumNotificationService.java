package com.truecaller.premium;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.af;
import android.support.v4.app.v;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.webkit.URLUtil;
import c.n.m;
import c.u;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.engagementrewards.c;
import com.truecaller.engagementrewards.g;
import com.truecaller.engagementrewards.ui.d;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.notification.NotificationType;
import com.truecaller.old.data.entity.Notification;
import java.io.InvalidClassException;
import java.io.Serializable;

public final class PremiumNotificationService
  extends af
{
  public static final PremiumNotificationService.a j;
  
  static
  {
    PremiumNotificationService.a locala = new com/truecaller/premium/PremiumNotificationService$a;
    locala.<init>((byte)0);
    j = locala;
  }
  
  public static final void a(Context paramContext, Notification paramNotification)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramNotification, "notification");
    new String[1][0] = "onNotificationReceived:: Notification type";
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, PremiumNotificationService.class);
    String str1 = paramNotification.p();
    localIntent.putExtra("PAGE_URL", str1);
    str1 = paramNotification.a("st");
    localIntent.putExtra("STATUS", str1);
    str1 = paramNotification.a("rs");
    localIntent.putExtra("REASON", str1);
    int i = bvalue;
    localIntent.putExtra("TYPE", i);
    String str2 = "IS_FREE_TRIAL";
    str1 = "ft";
    paramNotification = paramNotification.a(str1);
    boolean bool;
    if (paramNotification != null)
    {
      bool = Boolean.parseBoolean(paramNotification);
      paramNotification = Boolean.valueOf(bool);
    }
    else
    {
      bool = false;
      paramNotification = null;
    }
    paramNotification = (Serializable)paramNotification;
    localIntent.putExtra(str2, paramNotification);
    v.a(paramContext, PremiumNotificationService.class, 2131363976, localIntent);
  }
  
  private final void a(String paramString1, String paramString2)
  {
    Object localObject = "hold";
    boolean bool1 = true;
    boolean bool2 = m.a((String)localObject, paramString1, bool1);
    if (bool2)
    {
      paramString1 = "OnHold";
    }
    else
    {
      localObject = "InActive";
      bool2 = m.a((String)localObject, paramString1, bool1);
      if (bool2)
      {
        localObject = "SUBSCRIPTION_CANCELED";
        boolean bool3 = m.a((String)localObject, paramString2, bool1);
        if (bool3)
        {
          paramString1 = "Cancelled";
          break label92;
        }
      }
      paramString2 = "SUBSCRIPTION_GRACE";
      boolean bool4 = c.g.b.k.a(paramString2, paramString1);
      if (bool4)
      {
        paramString1 = "Grace";
      }
      else
      {
        bool4 = false;
        paramString1 = null;
      }
    }
    label92:
    if (paramString1 != null)
    {
      paramString2 = getApplicationContext();
      if (paramString2 != null)
      {
        paramString2 = ((bk)paramString2).a().c();
        localObject = new com/truecaller/analytics/e$a;
        ((e.a)localObject).<init>("SubscriptionStatusChanged");
        paramString1 = ((e.a)localObject).a("Type", paramString1).a();
        c.g.b.k.a(paramString1, "AnalyticsEvent.Builder(A…                 .build()");
        paramString2.b(paramString1);
        return;
      }
      paramString1 = new c/u;
      paramString1.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
      throw paramString1;
    }
  }
  
  private static NotificationType b(Intent paramIntent)
  {
    String str = "TYPE";
    try
    {
      paramIntent = paramIntent.getSerializableExtra(str);
      boolean bool = paramIntent instanceof NotificationType;
      if (bool) {
        paramIntent = (NotificationType)paramIntent;
      } else {
        paramIntent = NotificationType.UNSUPPORTED;
      }
    }
    catch (InvalidClassException localInvalidClassException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localInvalidClassException);
      paramIntent = NotificationType.SUBSCRIPTION_STATUS_CHANGED;
    }
    return paramIntent;
  }
  
  public final void a(Intent paramIntent)
  {
    c.g.b.k.b(paramIntent, "intent");
    int i = -1;
    int k = paramIntent.getIntExtra("TYPE", i);
    Object localObject1 = Integer.valueOf(k);
    Object localObject2 = localObject1;
    localObject2 = (Number)localObject1;
    int m = ((Number)localObject2).intValue();
    boolean bool3 = true;
    if (m != i)
    {
      m = 1;
    }
    else
    {
      m = 0;
      localObject2 = null;
    }
    boolean bool4 = false;
    Object localObject3 = null;
    if (m == 0)
    {
      k = 0;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      k = ((Number)localObject1).intValue();
      localObject1 = NotificationType.valueOf(k);
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = b(paramIntent);
    }
    localObject2 = new String[bool3];
    Object localObject4 = String.valueOf(localObject1);
    Object localObject5 = "onHandleWork:: Notification type: ".concat((String)localObject4);
    localObject2[0] = localObject5;
    localObject2 = bl.a;
    int n = ((NotificationType)localObject1).ordinal();
    m = localObject2[n];
    Object localObject7;
    switch (m)
    {
    default: 
      paramIntent = new java/lang/IllegalArgumentException;
      localObject6 = new java/lang/StringBuilder;
      ((StringBuilder)localObject6).<init>("Notification type : ");
      ((StringBuilder)localObject6).append(localObject1);
      ((StringBuilder)localObject6).append(" not handled");
      localObject1 = ((StringBuilder)localObject6).toString();
      paramIntent.<init>((String)localObject1);
      paramIntent = (Throwable)paramIntent;
      localObject1 = new String[0];
      AssertionUtil.shouldNeverHappen(paramIntent, (String[])localObject1);
      return;
    case 2: 
      a("SUBSCRIPTION_GRACE", null);
      paramIntent = Uri.parse(paramIntent.getStringExtra("PAGE_URL"));
      k = 2131886898;
      localObject1 = getString(k);
      m = 2131886897;
      localObject2 = getString(m);
      if (paramIntent != null)
      {
        localObject3 = paramIntent.toString();
        bool4 = URLUtil.isValidUrl((String)localObject3);
        if (bool4) {}
      }
      else
      {
        bool3 = false;
        localObject7 = null;
      }
      if (!bool3)
      {
        new String[1][0] = "Invalid notification message.";
        return;
      }
      localObject7 = getApplicationContext();
      if (localObject7 != null)
      {
        localObject7 = ((bk)localObject7).a();
        c.g.b.k.a(localObject7, "(applicationContext as GraphHolder).objectsGraph");
        localObject3 = ((bp)localObject7).aC().a();
        localObject5 = this;
        localObject5 = (Context)this;
        if (localObject3 == null)
        {
          localObject3 = new android/support/v4/app/z$d;
          ((z.d)localObject3).<init>((Context)localObject5);
        }
        else
        {
          localObject4 = new android/support/v4/app/z$d;
          ((z.d)localObject4).<init>((Context)localObject5, (String)localObject3);
          localObject3 = localObject4;
        }
        localObject1 = (CharSequence)localObject1;
        localObject1 = ((z.d)localObject3).a((CharSequence)localObject1);
        localObject2 = (CharSequence)localObject2;
        localObject1 = ((z.d)localObject1).b((CharSequence)localObject2);
        localObject3 = new android/support/v4/app/z$c;
        ((z.c)localObject3).<init>();
        localObject2 = (z.g)((z.c)localObject3).b((CharSequence)localObject2);
        localObject1 = ((z.d)localObject1).a((z.g)localObject2);
        localObject2 = BitmapFactory.decodeResource(getResources(), 2131233771);
        localObject1 = ((z.d)localObject1).a((Bitmap)localObject2);
        m = android.support.v4.content.b.c((Context)localObject5, 2131100594);
        localObject1 = ((z.d)localObject1).f(m).c(i).a(2131234787);
        localObject6 = new android/content/Intent;
        ((Intent)localObject6).<init>();
        paramIntent = ((Intent)localObject6).setAction("android.intent.action.VIEW").setData(paramIntent);
        paramIntent = PendingIntent.getActivity((Context)localObject5, 0, paramIntent, 0);
        paramIntent = ((z.d)localObject1).a(paramIntent).e();
        localObject1 = ((bp)localObject7).W();
        c.g.b.k.a(localObject1, "graph.analyticsNotificationManager()");
        paramIntent = paramIntent.h();
        c.g.b.k.a(paramIntent, "builder.build()");
        ((com.truecaller.notifications.a)localObject1).a(2131363976, paramIntent, "notificationSubscriptionGrace");
        return;
      }
      paramIntent = new c/u;
      paramIntent.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
      throw paramIntent;
    }
    localObject1 = paramIntent.getStringExtra("PAGE_URL");
    if (localObject1 == null) {
      localObject1 = "https://play.google.com/store/account/subscriptions";
    }
    Object localObject6 = paramIntent.getStringExtra("STATUS");
    localObject2 = paramIntent.getStringExtra("REASON");
    localObject3 = new String[bool3];
    localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>("Engagement Rewards: handleStatusChangedNotification:: url: ");
    ((StringBuilder)localObject5).append((String)localObject1);
    ((StringBuilder)localObject5).append(" subscription status: ");
    ((StringBuilder)localObject5).append((String)localObject6);
    localObject4 = " reason: ";
    ((StringBuilder)localObject5).append((String)localObject4);
    ((StringBuilder)localObject5).append((String)localObject2);
    localObject5 = ((StringBuilder)localObject5).toString();
    localObject3[0] = localObject5;
    a((String)localObject6, (String)localObject2);
    localObject3 = getApplicationContext();
    if (localObject3 != null)
    {
      localObject3 = ((bk)localObject3).a();
      c.g.b.k.a(localObject3, "(applicationContext as GraphHolder).objectsGraph");
      localObject5 = ((bp)localObject3).I();
      c.g.b.k.a(localObject5, "objectsGraph.coreSettings()");
      ((com.truecaller.common.g.a)localObject5).a("subscriptionStatus", (String)localObject6);
      localObject4 = "subscriptionErrorResolveUrl";
      ((com.truecaller.common.g.a)localObject5).a((String)localObject4, (String)localObject1);
      ((com.truecaller.common.g.a)localObject5).a("subscriptionStatusChangedReason", (String)localObject2);
      ((com.truecaller.common.g.a)localObject5).d("subscriptionPaymentFailedViewShownOnce");
      localObject1 = ((bp)localObject3).aF().aq();
      boolean bool2 = ((com.truecaller.featuretoggles.b)localObject1).a();
      if (bool2)
      {
        localObject1 = ((bp)localObject3).j();
        localObject4 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
        localObject4 = ((c)localObject1).a((EngagementRewardActionType)localObject4);
        String str = "SUBSCRIPTION_PURCHASED";
        boolean bool5 = c.g.b.k.a(str, localObject2);
        boolean bool6;
        if (bool5)
        {
          bool6 = paramIntent.getBooleanExtra("IS_FREE_TRIAL", false);
          ((com.truecaller.common.g.a)localObject5).b("subscriptionStatusChangedIsFreeTrial", bool6);
          localObject6 = ((com.truecaller.common.g.a)localObject5).a("subscriptionPurchaseSku");
          boolean bool1 = ((c)localObject1).a((String)localObject6);
          localObject2 = new String[bool3];
          localObject7 = new java/lang/StringBuilder;
          ((StringBuilder)localObject7).<init>("Engagement Rewards: handleStatusChangedNotification:: is free trial: ");
          ((StringBuilder)localObject7).append(bool6);
          ((StringBuilder)localObject7).append(" State: ");
          ((StringBuilder)localObject7).append(localObject4);
          str = " skuEligile: ";
          ((StringBuilder)localObject7).append(str);
          ((StringBuilder)localObject7).append(bool1);
          localObject7 = ((StringBuilder)localObject7).toString();
          localObject2[0] = localObject7;
          if ((!bool6) && (bool1))
          {
            paramIntent = ((bp)localObject3).i();
            localObject6 = ((com.truecaller.common.g.a)localObject5).a("subscriptionPurchaseSource");
            paramIntent.a((String)localObject6);
            paramIntent = EngagementRewardState.PENDING;
            if (localObject4 == paramIntent)
            {
              paramIntent = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
              localObject6 = EngagementRewardState.COMPLETED;
              ((c)localObject1).a(paramIntent, (EngagementRewardState)localObject6);
              ((bp)localObject3).k().a();
            }
          }
        }
        else
        {
          paramIntent = EngagementRewardState.PENDING;
          if (localObject4 == paramIntent)
          {
            paramIntent = "InActive";
            bool6 = m.a(paramIntent, (String)localObject6, bool3);
            if (bool6)
            {
              paramIntent = "SUBSCRIPTION_CANCELED";
              bool6 = m.a(paramIntent, (String)localObject2, bool3);
              if (bool6)
              {
                paramIntent = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
                localObject6 = EngagementRewardState.NONE;
                ((c)localObject1).a(paramIntent, (EngagementRewardState)localObject6);
                paramIntent = ((bp)localObject3).h();
                paramIntent.b();
              }
            }
          }
        }
      }
      return;
    }
    paramIntent = new c/u;
    paramIntent.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.PremiumNotificationService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */