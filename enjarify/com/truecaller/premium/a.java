package com.truecaller.premium;

import android.support.v4.view.n;
import android.support.v4.view.r;
import android.support.v4.view.y;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;

public final class a
  implements n
{
  public final y onApplyWindowInsets(View paramView, y paramy)
  {
    k.b(paramView, "view");
    String str1 = "insets";
    k.b(paramy, str1);
    paramView = (ViewGroup)paramView;
    int i = paramView.getChildCount();
    int j = 0;
    int k = 0;
    while (j < i)
    {
      y localy = r.b(paramView.getChildAt(j), paramy);
      String str2 = "childResult";
      k.a(localy, str2);
      boolean bool = localy.e();
      if (bool) {
        k = 1;
      }
      j += 1;
    }
    if (k != 0)
    {
      paramView = paramy.f();
      k.a(paramView, "insets.consumeSystemWindowInsets()");
      return paramView;
    }
    return paramy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */