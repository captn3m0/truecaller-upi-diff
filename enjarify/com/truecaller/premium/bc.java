package com.truecaller.premium;

import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.filters.p;
import com.truecaller.filters.sync.FilterSettingsUploadWorker.a;

public final class bc
  implements bb
{
  private final b a;
  private final p b;
  
  public bc(b paramb, p paramp)
  {
    a = paramb;
    b = paramp;
  }
  
  private final void a()
  {
    b.g(true);
    FilterSettingsUploadWorker.a.a(null);
  }
  
  private final void a(String paramString)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>(paramString);
    paramString = ((e.a)localObject).a("BlocktabSettings_Action", "Enabled");
    localObject = a;
    paramString = paramString.a();
    k.a(paramString, "event.build()");
    ((b)localObject).b(paramString);
  }
  
  public final void a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramLaunchContext, "launchContext");
    int[] arrayOfInt = bd.a;
    int i = paramLaunchContext.ordinal();
    i = arrayOfInt[i];
    boolean bool = true;
    switch (i)
    {
    default: 
      break;
    case 6: 
      b.e(bool);
      a();
      paramLaunchContext = "BLOCKSETTINGS_BlockIndianTelemarketers";
      a(paramLaunchContext);
      break;
    case 5: 
      b.c(bool);
      a();
      a("BLOCKSETTINGS_BlockNeighbourSpoofing");
      return;
    case 4: 
      b.c(bool);
      a();
      a("BLOCKSETTINGS_BlockForeignNumbers");
      return;
    case 3: 
      b.b(bool);
      a();
      a("BLOCKSETTINGS_BlockNonPhonebook");
      return;
    case 2: 
      b.a(bool);
      a();
      a("BLOCKSETTINGS_BlockHiddenNumbers");
      return;
    case 1: 
      b.f(bool);
      a();
      a("BLOCKSETTINGS_BlockSpammers");
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.bc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */