package com.truecaller.premium;

import android.arch.lifecycle.e;
import android.arch.lifecycle.e.b;
import android.arch.lifecycle.g;
import android.arch.lifecycle.h;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.view.View;
import c.g.b.k;
import c.l.d;
import c.u;
import com.truecaller.utils.extensions.t;

public final class ShineView
  extends View
  implements g
{
  private float a;
  private float b;
  private final Paint c;
  private final RectF d;
  private final int e;
  private final int f;
  private final Matrix g;
  private Shader h;
  private boolean i;
  private final ca j;
  private final d k;
  private h l;
  
  public ShineView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private ShineView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = new android/graphics/Paint;
    paramAttributeSet.<init>();
    c = paramAttributeSet;
    paramAttributeSet = new android/graphics/RectF;
    paramAttributeSet.<init>();
    d = paramAttributeSet;
    int m = android.support.v4.content.b.c(paramContext, 2131100390);
    e = m;
    m = android.support.v4.content.b.c(paramContext, 2131100391);
    f = m;
    paramAttributeSet = new android/graphics/Matrix;
    paramAttributeSet.<init>();
    g = paramAttributeSet;
    paramAttributeSet = new com/truecaller/premium/ca;
    String str = "sensor";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null)
    {
      paramContext = (SensorManager)paramContext;
      paramAttributeSet.<init>(paramContext);
      j = paramAttributeSet;
      paramContext = new com/truecaller/premium/ShineView$a;
      paramAttributeSet = this;
      paramAttributeSet = (ShineView)this;
      paramContext.<init>(paramAttributeSet);
      k = paramContext;
      setLayerType(2, null);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.hardware.SensorManager");
    throw paramContext;
  }
  
  private final void setRotationData(ca.a parama)
  {
    i = true;
    float f1 = a + 0.5F;
    a = f1;
    float f2 = b;
    b = f2;
    invalidate();
  }
  
  private final void subscribeSensorData()
  {
    boolean bool = t.d(this);
    if (bool)
    {
      Object localObject1 = l;
      if (localObject1 != null)
      {
        localObject1 = ((h)localObject1).getLifecycle();
        if (localObject1 != null)
        {
          localObject1 = ((e)localObject1).a();
          if (localObject1 != null)
          {
            e.b localb = e.b.e;
            bool = ((e.b)localObject1).a(localb);
            int m = 1;
            if (bool != m) {
              return;
            }
            localObject1 = j;
            Object localObject2 = (c.g.a.b)k;
            k.b(localObject2, "subscriber");
            Object localObject3 = a;
            if (localObject3 != null) {
              return;
            }
            localObject3 = b;
            int n = 9;
            localObject3 = ((SensorManager)localObject3).getDefaultSensor(n);
            Sensor localSensor = b.getDefaultSensor(m);
            Object localObject4 = b;
            int i1 = 2;
            localObject4 = ((SensorManager)localObject4).getDefaultSensor(i1);
            if ((localObject4 != null) && ((localObject3 != null) || (localSensor != null)))
            {
              i1 = 1;
            }
            else
            {
              i1 = 0;
              localObject5 = null;
            }
            if (i1 == 0) {
              return;
            }
            Object localObject5 = new com/truecaller/premium/ca$b;
            ((ca.b)localObject5).<init>((c.g.a.b)localObject2);
            a = ((ca.b)localObject5);
            if (localObject3 != null)
            {
              localObject2 = b;
              localObject5 = (SensorEventListener)a;
              ((SensorManager)localObject2).registerListener((SensorEventListener)localObject5, (Sensor)localObject3, m);
            }
            if ((localObject3 == null) && (localSensor != null))
            {
              localObject2 = b;
              localObject3 = (SensorEventListener)a;
              ((SensorManager)localObject2).registerListener((SensorEventListener)localObject3, localSensor, m);
            }
            localObject2 = b;
            localObject1 = (SensorEventListener)a;
            ((SensorManager)localObject2).registerListener((SensorEventListener)localObject1, (Sensor)localObject4, m);
            return;
          }
        }
      }
      return;
    }
  }
  
  private final void unsubscribeSensorData()
  {
    i = false;
    ca localca = j;
    SensorManager localSensorManager = b;
    SensorEventListener localSensorEventListener = (SensorEventListener)a;
    localSensorManager.unregisterListener(localSensorEventListener);
    a = null;
  }
  
  public final h getLifecycleOwner()
  {
    return l;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    subscribeSensorData();
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    unsubscribeSensorData();
  }
  
  protected final void onDraw(Canvas paramCanvas)
  {
    Object localObject1 = "canvas";
    k.b(paramCanvas, (String)localObject1);
    boolean bool = t.d(this);
    if (bool)
    {
      bool = i;
      if (bool)
      {
        localObject1 = h;
        int n = 2;
        float f1 = 2.8E-45F;
        int i1 = 3;
        float f2 = 4.2E-45F;
        if (localObject1 == null)
        {
          m = getMeasuredWidth();
          f3 = m;
          f4 = 2.0F;
          f3 *= f4;
          float f5 = getMeasuredHeight() * f4;
          float f6 = -f3;
          double d1 = getMeasuredHeight();
          double d2 = 1.5D;
          Double.isNaN(d1);
          d1 *= d2;
          float f7 = (float)d1;
          float f8 = f6 + f3;
          float f9 = f7 - f5;
          localObject1 = new android/graphics/LinearGradient;
          int[] arrayOfInt = new int[i1];
          int i2 = f;
          arrayOfInt[0] = i2;
          int i3 = 1;
          f4 = Float.MIN_VALUE;
          int i4 = e;
          arrayOfInt[i3] = i4;
          arrayOfInt[n] = i2;
          float[] arrayOfFloat = new float[i1];
          float[] tmp188_186 = arrayOfFloat;
          tmp188_186[0] = 0.3F;
          float[] tmp193_188 = tmp188_186;
          tmp193_188[1] = 0.5F;
          tmp193_188[2] = 0.7F;
          Shader.TileMode localTileMode = Shader.TileMode.CLAMP;
          ((LinearGradient)localObject1).<init>(f6, f7, f8, f9, arrayOfInt, arrayOfFloat, localTileMode);
          localObject1 = (Shader)localObject1;
          h = ((Shader)localObject1);
        }
        int m = getMeasuredWidth() * 3;
        i1 = getMeasuredHeight() * 2;
        Object localObject2 = g;
        float f3 = m;
        float f4 = a;
        f3 *= f4;
        f2 = i1;
        f4 = b;
        f2 *= f4;
        ((Matrix)localObject2).setTranslate(f3, f2);
        localObject1 = h;
        if (localObject1 != null)
        {
          localObject2 = g;
          ((Shader)localObject1).setLocalMatrix((Matrix)localObject2);
        }
        localObject1 = c;
        localObject2 = h;
        ((Paint)localObject1).setShader((Shader)localObject2);
        localObject1 = d;
        left = 0.0F;
        top = 0.0F;
        f1 = getMeasuredHeight();
        bottom = f1;
        localObject1 = d;
        f1 = getMeasuredWidth();
        right = f1;
        localObject1 = d;
        localObject2 = c;
        paramCanvas.drawRect((RectF)localObject1, (Paint)localObject2);
        return;
      }
    }
  }
  
  public final void setLifecycleOwner(h paramh)
  {
    Object localObject = l;
    if (localObject == null)
    {
      l = paramh;
      paramh = l;
      if (paramh != null)
      {
        paramh = paramh.getLifecycle();
        if (paramh != null)
        {
          localObject = this;
          localObject = (g)this;
          paramh.a((g)localObject);
          return;
        }
      }
    }
  }
  
  public final void setVisibility(int paramInt)
  {
    super.setVisibility(paramInt);
    paramInt = t.d(this);
    if (paramInt != 0)
    {
      subscribeSensorData();
      return;
    }
    unsubscribeSensorData();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.ShineView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */