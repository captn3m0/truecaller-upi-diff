package com.truecaller.premium.a;

import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private c(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static c a(Provider paramProvider1, Provider paramProvider2)
  {
    c localc = new com/truecaller/premium/a/c;
    localc.<init>(paramProvider1, paramProvider2);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */