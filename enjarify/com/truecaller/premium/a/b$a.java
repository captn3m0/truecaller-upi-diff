package com.truecaller.premium.a;

import c.d.a.a;
import c.d.c;
import c.d.h;
import c.o.b;
import c.x;
import com.android.billingclient.api.j;
import com.android.billingclient.api.k.a;
import com.android.billingclient.api.l;
import com.truecaller.premium.cb.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class b$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  b$a(b paramb, List paramList, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/premium/a/b$a;
    b localb = b;
    List localList = c;
    locala.<init>(localb, localList, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    boolean bool1;
    Object localObject2;
    int k;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool3 = paramObject instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label544;
      }
      paramObject = b;
      localObject2 = c;
      k = 1;
      a = k;
      localObject2 = com.android.billingclient.api.k.a().a((List)localObject2).a("subs").a();
      paramObject = ((b)paramObject).c();
      c.g.b.k.a(localObject2, "params");
      localObject3 = new c/d/h;
      localObject4 = c.d.a.b.a(this);
      ((h)localObject3).<init>((c)localObject4);
      localObject4 = localObject3;
      localObject4 = (c)localObject3;
      localObject5 = new com/truecaller/premium/a/a$c;
      ((a.c)localObject5).<init>((c)localObject4);
      localObject5 = (l)localObject5;
      ((com.android.billingclient.api.b)paramObject).a((com.android.billingclient.api.k)localObject2, (l)localObject5);
      paramObject = ((h)localObject3).b();
      localObject2 = a.a;
      if (paramObject == localObject2)
      {
        localObject2 = "frame";
        c.g.b.k.b(this, (String)localObject2);
      }
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Iterable)paramObject;
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramObject = ((Iterable)paramObject).iterator();
    for (;;)
    {
      bool1 = ((Iterator)paramObject).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = ((Iterator)paramObject).next();
      localObject3 = localObject2;
      localObject3 = (j)localObject2;
      localObject4 = c;
      localObject3 = ((j)localObject3).a();
      k = ((List)localObject4).contains(localObject3);
      if (k != 0) {
        ((Collection)localObject1).add(localObject2);
      }
    }
    localObject1 = (Iterable)localObject1;
    paramObject = new java/util/ArrayList;
    int j = c.a.m.a((Iterable)localObject1, 10);
    ((ArrayList)paramObject).<init>(j);
    paramObject = (Collection)paramObject;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (j)((Iterator)localObject1).next();
      localObject3 = new com/truecaller/premium/cb$a;
      ((cb.a)localObject3).<init>();
      localObject4 = ((j)localObject2).b();
      localObject3 = ((cb.a)localObject3).c((String)localObject4);
      long l = ((j)localObject2).c();
      localObject3 = ((cb.a)localObject3).a(l);
      localObject4 = ((j)localObject2).a();
      localObject3 = ((cb.a)localObject3).a((String)localObject4);
      localObject4 = ((j)localObject2).e();
      if (localObject4 == null) {
        localObject4 = "";
      }
      localObject5 = "(";
      localObject4 = c.n.m.a((String)localObject4, (String)localObject5);
      localObject3 = ((cb.a)localObject3).b((String)localObject4);
      localObject4 = ((j)localObject2).d();
      localObject3 = ((cb.a)localObject3).f((String)localObject4);
      localObject4 = ((j)localObject2).f();
      localObject3 = ((cb.a)localObject3).d((String)localObject4);
      localObject2 = ((j)localObject2).g();
      localObject2 = ((cb.a)localObject3).e((String)localObject2).a();
      localObject3 = "Subscription.Builder()\n …ros)\n            .build()";
      c.g.b.k.a(localObject2, (String)localObject3);
      ((Collection)paramObject).add(localObject2);
    }
    return (List)paramObject;
    label544:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */