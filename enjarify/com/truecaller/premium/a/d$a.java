package com.truecaller.premium.a;

import c.g.b.k;
import java.util.List;

public final class d$a
{
  public final boolean a;
  public final List b;
  public final List c;
  
  public d$a(boolean paramBoolean, List paramList1, List paramList2)
  {
    a = paramBoolean;
    b = paramList1;
    c = paramList2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof a;
      if (bool2)
      {
        paramObject = (a)paramObject;
        bool2 = a;
        boolean bool3 = a;
        List localList1;
        if (bool2 == bool3)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localList1 = null;
        }
        if (bool2)
        {
          localList1 = b;
          List localList2 = b;
          bool2 = k.a(localList1, localList2);
          if (bool2)
          {
            localList1 = c;
            paramObject = c;
            boolean bool4 = k.a(localList1, paramObject);
            if (bool4) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    boolean bool = a;
    if (bool) {
      bool = true;
    }
    bool *= true;
    List localList = b;
    int j = 0;
    int k;
    if (localList != null)
    {
      k = localList.hashCode();
    }
    else
    {
      k = 0;
      localList = null;
    }
    int i = (i + k) * 31;
    localList = c;
    if (localList != null) {
      j = localList.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("InitializationResult(isBillingAvailable=");
    boolean bool = a;
    localStringBuilder.append(bool);
    localStringBuilder.append(", subscriptionReceipts=");
    List localList = b;
    localStringBuilder.append(localList);
    localStringBuilder.append(", consumableReceipts=");
    localList = c;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.a.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */