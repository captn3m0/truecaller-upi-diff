package com.truecaller.premium.a;

import c.a.y;
import c.d.a.a;
import c.d.c;
import c.o.b;
import c.x;
import com.android.billingclient.api.d;
import com.android.billingclient.api.h.a;
import com.truecaller.premium.bz.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class b$b
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  int b;
  private ag d;
  
  b$b(b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/premium/a/b$b;
    b localb1 = c;
    localb.<init>(localb1, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    Object localObject4;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label674;
      }
      paramObject = c;
      localObject2 = ((b)paramObject).c();
      a = paramObject;
      j = 1;
      b = j;
      paramObject = new c/d/h;
      localObject3 = c.d.a.b.a(this);
      ((c.d.h)paramObject).<init>((c)localObject3);
      localObject3 = paramObject;
      localObject3 = (c)paramObject;
      localObject4 = new com/truecaller/premium/a/a$a;
      ((a.a)localObject4).<init>((c)localObject3);
      localObject4 = (d)localObject4;
      ((com.android.billingclient.api.b)localObject2).a((d)localObject4);
      paramObject = ((c.d.h)paramObject).b();
      localObject2 = a.a;
      if (paramObject == localObject2)
      {
        localObject2 = "frame";
        c.g.b.k.b(this, (String)localObject2);
      }
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Integer)paramObject;
    int j = b.a((Integer)paramObject);
    if (j != 0)
    {
      paramObject = c.c();
      localObject1 = "subscriptions";
      paramObject = Integer.valueOf(((com.android.billingclient.api.b)paramObject).a((String)localObject1));
      j = b.a((Integer)paramObject);
      if (j != 0)
      {
        paramObject = c;
        localObject1 = ((b)paramObject).c().b("subs");
        localObject2 = "getBillingClient()\n     …    .queryPurchases(SUBS)";
        c.g.b.k.a(localObject1, (String)localObject2);
        localObject1 = ((h.a)localObject1).a();
        if (localObject1 == null) {
          localObject1 = (List)y.a;
        }
        localObject1 = (Iterable)localObject1;
        localObject2 = new java/util/ArrayList;
        int k = 10;
        int m = c.a.m.a((Iterable)localObject1, k);
        ((ArrayList)localObject2).<init>(m);
        localObject2 = (Collection)localObject2;
        localObject1 = ((Iterable)localObject1).iterator();
        for (;;)
        {
          boolean bool4 = ((Iterator)localObject1).hasNext();
          if (!bool4) {
            break;
          }
          localObject4 = b.a((com.android.billingclient.api.h)((Iterator)localObject1).next());
          ((Collection)localObject2).add(localObject4);
        }
        localObject2 = (List)localObject2;
        localObject1 = ((b)paramObject).c().b("inapp");
        localObject4 = "getBillingClient()\n     …   .queryPurchases(INAPP)";
        c.g.b.k.a(localObject1, (String)localObject4);
        localObject1 = ((h.a)localObject1).a();
        if (localObject1 == null) {
          localObject1 = (List)y.a;
        }
        localObject1 = (Iterable)localObject1;
        localObject4 = new java/util/ArrayList;
        k = c.a.m.a((Iterable)localObject1, k);
        ((ArrayList)localObject4).<init>(k);
        localObject4 = (Collection)localObject4;
        localObject1 = ((Iterable)localObject1).iterator();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject1).hasNext();
          if (!bool3) {
            break;
          }
          localObject3 = (com.android.billingclient.api.h)((Iterator)localObject1).next();
          Object localObject5;
          if (localObject3 == null)
          {
            localObject3 = new com/truecaller/premium/bz$a;
            ((bz.a)localObject3).<init>();
            localObject3 = ((bz.a)localObject3).a();
            localObject5 = "Receipt.Builder().build()";
            c.g.b.k.a(localObject3, (String)localObject5);
          }
          else
          {
            localObject5 = new com/truecaller/premium/bz$a;
            ((bz.a)localObject5).<init>();
            String str = ((com.android.billingclient.api.h)localObject3).a();
            localObject5 = ((bz.a)localObject5).a(str);
            str = ((com.android.billingclient.api.h)localObject3).d();
            localObject5 = ((bz.a)localObject5).b(str);
            str = ((com.android.billingclient.api.h)localObject3).e();
            localObject5 = ((bz.a)localObject5).c(str);
            str = "inapp";
            localObject5 = ((bz.a)localObject5).d(str);
            long l = ((com.android.billingclient.api.h)localObject3).b();
            localObject3 = ((bz.a)localObject5).a(l).a();
            localObject5 = "Receipt.Builder()\n      …ime)\n            .build()";
            c.g.b.k.a(localObject3, (String)localObject5);
          }
          ((Collection)localObject4).add(localObject3);
        }
        localObject4 = (List)localObject4;
        localObject1 = new com/truecaller/premium/a/d$a;
        j = ((b)paramObject).c().a();
        ((d.a)localObject1).<init>(j, (List)localObject2, (List)localObject4);
        return localObject1;
      }
    }
    paramObject = new com/truecaller/premium/a/d$a;
    Object localObject2 = (List)y.a;
    Object localObject3 = (List)y.a;
    ((d.a)paramObject).<init>(false, (List)localObject2, (List)localObject3);
    return paramObject;
    label674:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.a.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */