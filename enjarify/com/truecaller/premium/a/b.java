package com.truecaller.premium.a;

import android.app.Activity;
import android.content.Context;
import c.a.y;
import c.g.b.k;
import com.android.billingclient.api.e;
import com.android.billingclient.api.e.a;
import com.android.billingclient.api.h;
import com.android.billingclient.api.i;
import com.truecaller.premium.bz;
import com.truecaller.premium.bz.a;
import java.util.List;

public final class b
  implements i, d
{
  private com.android.billingclient.api.b a;
  private d.b b;
  private final Context c;
  private final c.d.f d;
  
  public b(Context paramContext, c.d.f paramf)
  {
    c = paramContext;
    d = paramf;
  }
  
  static bz a(h paramh)
  {
    if (paramh == null)
    {
      paramh = new com/truecaller/premium/bz$a;
      paramh.<init>();
      paramh = paramh.a();
      k.a(paramh, "Receipt.Builder().build()");
      return paramh;
    }
    bz.a locala = new com/truecaller/premium/bz$a;
    locala.<init>();
    String str = paramh.a();
    locala = locala.a(str);
    str = paramh.d();
    locala = locala.b(str);
    str = paramh.e();
    locala = locala.c(str).d("subs");
    long l = paramh.b();
    paramh = locala.a(l).a();
    k.a(paramh, "Receipt.Builder()\n      …ime)\n            .build()");
    return paramh;
  }
  
  static boolean a(Integer paramInteger)
  {
    if (paramInteger != null)
    {
      int i = paramInteger.intValue();
      if (i == 0) {
        return true;
      }
    }
    return false;
  }
  
  public final List a(List paramList)
  {
    Object localObject1 = "productIds";
    k.b(paramList, (String)localObject1);
    boolean bool = a();
    if (!bool) {
      return (List)y.a;
    }
    localObject1 = d;
    Object localObject2 = new com/truecaller/premium/a/b$a;
    ((b.a)localObject2).<init>(this, paramList, null);
    localObject2 = (c.g.a.m)localObject2;
    return (List)kotlinx.coroutines.f.a((c.d.f)localObject1, (c.g.a.m)localObject2);
  }
  
  public final void a(int paramInt, List paramList)
  {
    if (paramList == null) {
      paramList = (List)y.a;
    }
    paramList = (h)c.a.m.e(paramList);
    d.b localb = b;
    if (localb != null)
    {
      switch (paramInt)
      {
      default: 
        paramInt = 1;
        break;
      case 1: 
        paramInt = 2;
        break;
      case 0: 
        paramInt = 0;
      }
      paramList = a(paramList);
      localb.a(paramInt, paramList);
    }
    b = null;
  }
  
  public final void a(Activity paramActivity, String paramString1, String paramString2, d.b paramb)
  {
    k.b(paramActivity, "activity");
    k.b(paramString1, "sku");
    Object localObject = "listener";
    k.b(paramb, (String)localObject);
    boolean bool1 = a();
    if (!bool1) {
      return;
    }
    b = paramb;
    paramb = c();
    localObject = e.a();
    paramString1 = ((e.a)localObject).a(paramString1).c(paramString2).a();
    paramString2 = "subs";
    paramString1 = paramString1.b(paramString2).b();
    paramActivity = Integer.valueOf(paramb.a(paramActivity, paramString1));
    boolean bool2 = a(paramActivity);
    if (!bool2)
    {
      bool2 = false;
      paramActivity = null;
      b = null;
    }
  }
  
  public final void a(bz parambz)
  {
    Object localObject1 = "receipt";
    k.b(parambz, (String)localObject1);
    boolean bool = a();
    if (!bool) {
      return;
    }
    localObject1 = c();
    Object localObject2 = new com/android/billingclient/api/h;
    String str = b;
    parambz = c;
    ((h)localObject2).<init>(str, parambz);
    parambz = ((h)localObject2).c();
    k.a(parambz, "receipt.purchaseToken");
    k.b(localObject1, "receiver$0");
    k.b(parambz, "token");
    localObject2 = (com.android.billingclient.api.f)a.b.a;
    ((com.android.billingclient.api.b)localObject1).a(parambz, (com.android.billingclient.api.f)localObject2);
  }
  
  public final boolean a()
  {
    com.android.billingclient.api.b localb = a;
    if (localb != null) {
      return localb.a();
    }
    return false;
  }
  
  public final d.a b()
  {
    c.d.f localf = d;
    Object localObject = new com/truecaller/premium/a/b$b;
    ((b.b)localObject).<init>(this, null);
    localObject = (c.g.a.m)localObject;
    return (d.a)kotlinx.coroutines.f.a(localf, (c.g.a.m)localObject);
  }
  
  final com.android.billingclient.api.b c()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = com.android.billingclient.api.b.a(c);
      Object localObject2 = this;
      localObject2 = (i)this;
      localObject1 = ((com.android.billingclient.api.b.a)localObject1).a((i)localObject2).a();
      a = ((com.android.billingclient.api.b)localObject1);
      localObject2 = "BillingClient\n        .n… { billingClient = this }";
      k.a(localObject1, (String)localObject2);
    }
    return (com.android.billingclient.api.b)localObject1;
  }
  
  public final void d()
  {
    com.android.billingclient.api.b localb = a;
    if (localb != null) {
      localb.b();
    }
    a = null;
    b = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */