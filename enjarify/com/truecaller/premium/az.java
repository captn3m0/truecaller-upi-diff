package com.truecaller.premium;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.widget.Button;
import com.truecaller.util.at;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class az
  extends AlertDialog
{
  private AppCompatEditText a;
  
  az(Context paramContext)
  {
    super(paramContext);
    setCancelable(false);
    paramContext = getContext().getString(2131886890);
    setMessage(paramContext);
    paramContext = new android/support/v7/widget/AppCompatEditText;
    Object localObject = getContext();
    paramContext.<init>((Context)localObject);
    a = paramContext;
    a.setMaxLines(1);
    a.setHint(2131886854);
    paramContext = a;
    localObject = new com/truecaller/premium/az$1;
    ((az.1)localObject).<init>(this);
    paramContext.addTextChangedListener((TextWatcher)localObject);
    int i = at.a(getContext(), 18.0F);
    AppCompatEditText localAppCompatEditText = a;
    setView(localAppCompatEditText, i, 0, i, 0);
  }
  
  private void a()
  {
    int i = -1;
    Button localButton = getButton(i);
    if (localButton != null)
    {
      Object localObject = a;
      if (localObject != null)
      {
        localObject = b();
        boolean bool = a((CharSequence)localObject);
        localButton.setEnabled(bool);
      }
    }
  }
  
  private static boolean a(CharSequence paramCharSequence)
  {
    boolean bool1 = TextUtils.isEmpty(paramCharSequence);
    if (!bool1)
    {
      Pattern localPattern = Patterns.EMAIL_ADDRESS;
      paramCharSequence = localPattern.matcher(paramCharSequence);
      boolean bool2 = paramCharSequence.matches();
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  private String b()
  {
    AppCompatEditText localAppCompatEditText = a;
    if (localAppCompatEditText != null) {
      return localAppCompatEditText.getText().toString().trim();
    }
    return null;
  }
  
  public final void a(az.a parama)
  {
    String str = getContext().getString(2131886892);
    -..Lambda.az.K0xuUnJLhpJ3xsKfxkfGiooQlw0 localK0xuUnJLhpJ3xsKfxkfGiooQlw0 = new com/truecaller/premium/-$$Lambda$az$K0xuUnJLhpJ3xsKfxkfGiooQlw0;
    localK0xuUnJLhpJ3xsKfxkfGiooQlw0.<init>(this, parama);
    setButton(-1, str, localK0xuUnJLhpJ3xsKfxkfGiooQlw0);
  }
  
  public final void a(String paramString)
  {
    AppCompatEditText localAppCompatEditText = a;
    if (localAppCompatEditText != null) {
      localAppCompatEditText.setText(paramString);
    }
  }
  
  public final void b(az.a parama)
  {
    String str = getContext().getString(2131886891);
    -..Lambda.az.WfmZS9jP0lKa2HJD2Cd-qGECG4g localWfmZS9jP0lKa2HJD2Cd-qGECG4g = new com/truecaller/premium/-$$Lambda$az$WfmZS9jP0lKa2HJD2Cd-qGECG4g;
    localWfmZS9jP0lKa2HJD2Cd-qGECG4g.<init>(this, parama);
    setButton(-2, str, localWfmZS9jP0lKa2HJD2Cd-qGECG4g);
  }
  
  public final void show()
  {
    super.show();
    a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.az
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */