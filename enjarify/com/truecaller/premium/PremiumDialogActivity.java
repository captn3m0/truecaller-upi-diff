package com.truecaller.premium;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import c.g.b.k;
import com.truecaller.common.h.l;

public final class PremiumDialogActivity
  extends PremiumActivity
{
  protected final ba.b a()
  {
    ba.b localb = new com/truecaller/premium/ba$b;
    Integer localInteger = Integer.valueOf(2131233992);
    int i = l.a((Context)this, 16.0F);
    localb.<init>(localInteger, 2131233834, i);
    return localb;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = new android/util/DisplayMetrics;
    paramBundle.<init>();
    Object localObject = getWindowManager();
    k.a(localObject, "windowManager");
    ((WindowManager)localObject).getDefaultDisplay().getMetrics(paramBundle);
    int i = heightPixels;
    localObject = getWindow();
    k.a(localObject, "window");
    localObject = ((Window)localObject).getAttributes();
    i = i * 90 / 100;
    height = i;
    paramBundle = getWindow();
    k.a(paramBundle, "window");
    paramBundle.setAttributes((WindowManager.LayoutParams)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.PremiumDialogActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */