package com.truecaller.premium.b;

import c.l.g;
import com.truecaller.common.f.c;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;

public final class a
{
  public final e a;
  private final c b;
  
  public a(c paramc, e parame)
  {
    b = paramc;
    a = parame;
  }
  
  public final boolean a(Contact paramContact)
  {
    Object localObject1 = a;
    e.a locala = u;
    Object localObject2 = e.a;
    int i = 72;
    localObject2 = localObject2[i];
    localObject1 = locala.a((e)localObject1, (g)localObject2);
    boolean bool1 = ((b)localObject1).a();
    if (bool1)
    {
      bool1 = true;
      boolean bool2 = b(paramContact, bool1);
      if (bool2) {
        return bool1;
      }
    }
    return false;
  }
  
  public final boolean a(Contact paramContact, boolean paramBoolean)
  {
    Object localObject1 = a;
    e.a locala = v;
    Object localObject2 = e.a;
    int i = 73;
    localObject2 = localObject2[i];
    localObject1 = locala.a((e)localObject1, (g)localObject2);
    boolean bool1 = ((b)localObject1).a();
    if (bool1)
    {
      if (paramContact != null)
      {
        localObject1 = paramContact.g();
        if (localObject1 != null)
        {
          localObject1 = ((Address)localObject1).getStreet();
          break label75;
        }
      }
      bool1 = false;
      localObject1 = null;
      label75:
      localObject1 = (CharSequence)localObject1;
      bool1 = am.b((CharSequence)localObject1);
      if (!bool1)
      {
        boolean bool2 = b(paramContact, paramBoolean);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final boolean b(Contact paramContact)
  {
    Object localObject1 = a;
    e.a locala = w;
    Object localObject2 = e.a;
    int i = 74;
    localObject2 = localObject2[i];
    localObject1 = locala.a((e)localObject1, (g)localObject2);
    boolean bool1 = ((b)localObject1).a();
    if (bool1)
    {
      bool1 = true;
      boolean bool2 = b(paramContact, bool1);
      if (bool2) {
        return bool1;
      }
    }
    return false;
  }
  
  public final boolean b(Contact paramContact, boolean paramBoolean)
  {
    if (paramContact == null) {
      return false;
    }
    if (paramBoolean)
    {
      paramBoolean = paramContact.Z();
      if (paramBoolean) {
        return false;
      }
    }
    paramBoolean = paramContact.N();
    if (!paramBoolean)
    {
      c localc = b;
      paramBoolean = localc.d();
      if (!paramBoolean)
      {
        boolean bool = paramContact.L();
        if (bool) {
          return a.X().a();
        }
        return a.W().a();
      }
    }
    return false;
  }
  
  public final boolean c(Contact paramContact)
  {
    Object localObject1 = a;
    e.a locala = z;
    Object localObject2 = e.a;
    int i = 77;
    localObject2 = localObject2[i];
    localObject1 = locala.a((e)localObject1, (g)localObject2);
    boolean bool1 = ((b)localObject1).a();
    if (bool1)
    {
      bool1 = true;
      boolean bool2 = b(paramContact, bool1);
      if (bool2) {
        return bool1;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */