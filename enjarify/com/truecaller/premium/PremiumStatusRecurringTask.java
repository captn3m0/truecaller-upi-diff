package com.truecaller.premium;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.f.j;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.b.a;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.background.e.a;
import com.truecaller.common.f.c;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.a.d;
import com.truecaller.premium.a.d.a;
import com.truecaller.premium.data.m.b;
import com.truecaller.premium.data.m.b.b;
import com.truecaller.premium.data.x;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.f;

public final class PremiumStatusRecurringTask
  extends PersistentBackgroundTask
{
  public c a;
  public com.truecaller.premium.data.m b;
  public d c;
  
  public PremiumStatusRecurringTask()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final int a()
  {
    return 10009;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = "serviceContext";
    k.b(paramContext, paramBundle);
    paramContext = a;
    if (paramContext == null)
    {
      paramBundle = "premiumRepository";
      k.a(paramBundle);
    }
    boolean bool1 = paramContext.d();
    boolean bool3 = true;
    Object localObject1 = null;
    if (!bool1)
    {
      paramContext = a;
      if (paramContext == null)
      {
        localObject2 = "premiumRepository";
        k.a((String)localObject2);
      }
      bool1 = paramContext.j();
      if (bool1)
      {
        paramContext = "premiumLevel";
        bool1 = Settings.a(paramContext);
        if (bool1)
        {
          bool1 = false;
          paramContext = null;
          break label157;
        }
      }
    }
    paramContext = a;
    if (paramContext == null)
    {
      localObject2 = "premiumRepository";
      k.a((String)localObject2);
    }
    paramContext = paramContext.b();
    if (paramContext != null)
    {
      paramContext = (Integer)a;
      if (paramContext != null)
      {
        i = paramContext.intValue();
        if (i == 0)
        {
          i = 0;
          paramContext = null;
          break label157;
        }
      }
    }
    int i = 1;
    label157:
    Object localObject2 = a;
    Object localObject3;
    if (localObject2 == null)
    {
      localObject3 = "premiumRepository";
      k.a((String)localObject3);
    }
    boolean bool4 = ((c)localObject2).d();
    boolean bool2;
    if (!bool4)
    {
      localObject2 = c;
      if (localObject2 == null)
      {
        localObject3 = "inAppBilling";
        k.a((String)localObject3);
      }
      localObject2 = ((d)localObject2).b();
      if (i == 0)
      {
        bool2 = a;
        if (bool2)
        {
          bool2 = false;
          paramContext = null;
          break label246;
        }
      }
      bool2 = true;
      label246:
      localObject3 = new com/truecaller/premium/PremiumStatusRecurringTask$a;
      ((PremiumStatusRecurringTask.a)localObject3).<init>(this, null);
      localObject3 = (m.b)f.a((c.g.a.m)localObject3);
      if (!bool2)
      {
        bool2 = localObject3 instanceof m.b.b;
        if (bool2)
        {
          bool2 = false;
          paramContext = null;
          break label297;
        }
      }
      bool2 = true;
      label297:
      bool3 = a;
      if (bool3)
      {
        bool3 = localObject3 instanceof m.b.b;
        if (bool3)
        {
          paramBundle = c;
          if (paramBundle == null)
          {
            localObject1 = "inAppBilling";
            k.a((String)localObject1);
          }
          localObject3 = (m.b.b)localObject3;
          localObject1 = a.a();
          paramBundle.a((List)localObject1);
        }
      }
      paramBundle = c;
      if (paramBundle == null)
      {
        localObject1 = "inAppBilling";
        k.a((String)localObject1);
      }
      paramBundle.d();
    }
    if (bool2) {
      return PersistentBackgroundTask.RunResult.FailedRetry;
    }
    return PersistentBackgroundTask.RunResult.Success;
  }
  
  public final boolean a(Context paramContext)
  {
    String str = "serviceContext";
    k.b(paramContext, str);
    paramContext = paramContext.getApplicationContext();
    if (paramContext != null) {
      return ((a)paramContext).p();
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw paramContext;
  }
  
  public final e b()
  {
    Object localObject = new com/truecaller/common/background/e$a;
    int i = 1;
    ((e.a)localObject).<init>(i);
    TimeUnit localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).a(6, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).b(2, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).c(1L, localTimeUnit).a(i).b();
    k.a(localObject, "TaskConfiguration.Builde…ANY)\n            .build()");
    return (e)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.PremiumStatusRecurringTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */