package com.truecaller.premium;

import com.truecaller.common.h.am;

public final class cb
{
  public final String a;
  public final String b;
  public final String c;
  private final String d;
  private final long e;
  private final String f;
  private final Long g;
  
  private cb(String paramString1, String paramString2, String paramString3, String paramString4, long paramLong, String paramString5, Long paramLong1)
  {
    a = paramString1;
    b = paramString2;
    d = paramString3;
    c = paramString4;
    e = paramLong;
    f = paramString5;
    g = paramLong1;
  }
  
  final String a()
  {
    String str = f;
    boolean bool = am.b(str);
    if (bool) {
      return d;
    }
    return f;
  }
  
  final long b()
  {
    Long localLong = g;
    if (localLong == null) {
      return e;
    }
    return localLong.longValue();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.cb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */