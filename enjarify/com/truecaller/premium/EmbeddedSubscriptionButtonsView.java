package com.truecaller.premium;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.R.styleable;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class EmbeddedSubscriptionButtonsView
  extends LinearLayout
{
  public e a;
  public br b;
  List c;
  d d;
  private final int e = 2131559121;
  private final int f;
  private String g;
  private HashMap h;
  
  public EmbeddedSubscriptionButtonsView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private EmbeddedSubscriptionButtonsView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    int i = e;
    int j = 1;
    if (paramAttributeSet != null)
    {
      localObject1 = R.styleable.EmbeddedSubscriptionButtonsView;
      paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject1, 0, 0);
      str = paramAttributeSet.getString(0);
      int k = e;
      i = paramAttributeSet.getResourceId(2, k);
      Object localObject2 = paramAttributeSet.getString(j);
      if (localObject2 == null)
      {
        localObject2 = getResources();
        int m = 2131886907;
        localObject2 = ((Resources)localObject2).getString(m);
      }
      g = ((String)localObject2);
      paramAttributeSet.recycle();
    }
    else
    {
      str = null;
    }
    f = i;
    setOrientation(j);
    paramAttributeSet = getResources();
    if (paramAttributeSet != null)
    {
      i = 2131165496;
      int n = paramAttributeSet.getDimensionPixelSize(i);
      setPadding(n, n, n, n);
    }
    paramAttributeSet = new android/widget/ProgressBar;
    paramAttributeSet.<init>(paramContext);
    paramAttributeSet.setIndeterminate(j);
    paramAttributeSet = (View)paramAttributeSet;
    addView(paramAttributeSet);
    paramAttributeSet = c.a();
    Object localObject1 = new com/truecaller/premium/bf;
    paramContext = (Activity)paramContext;
    PremiumPresenterView.LaunchContext localLaunchContext = PremiumPresenterView.LaunchContext.WHO_VIEWED_ME;
    ((bf)localObject1).<init>(paramContext, localLaunchContext, null, null);
    paramContext = paramAttributeSet.a((bf)localObject1);
    paramAttributeSet = TrueApp.y();
    if (paramAttributeSet != null)
    {
      paramAttributeSet = ((bk)paramAttributeSet).a();
      paramContext = paramContext.a(paramAttributeSet).a();
      paramContext.a(this);
      if (str != null)
      {
        paramContext = PremiumPresenterView.LaunchContext.valueOf(str);
        setLaunchContext(paramContext);
      }
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
  
  final void a(Integer paramInteger)
  {
    removeAllViews();
    if (paramInteger != null) {
      i = paramInteger.intValue();
    } else {
      i = f;
    }
    boolean bool = true;
    t.a(this, i, bool);
    paramInteger = new SubscriptionButtonView[2];
    int j = 2131363093;
    Object localObject1 = findViewById(j);
    String str = "findViewById(R.id.first)";
    k.a(localObject1, str);
    localObject1 = (SubscriptionButtonView)localObject1;
    paramInteger[0] = localObject1;
    int k = 2131364279;
    Object localObject2 = findViewById(k);
    localObject1 = "findViewById(R.id.second)";
    k.a(localObject2, (String)localObject1);
    localObject2 = (SubscriptionButtonView)localObject2;
    paramInteger[bool] = localObject2;
    paramInteger = m.b(paramInteger);
    c = paramInteger;
    int i = R.id.premiumFeaturesButton;
    Object localObject3 = h;
    if (localObject3 == null)
    {
      localObject3 = new java/util/HashMap;
      ((HashMap)localObject3).<init>();
      h = ((HashMap)localObject3);
    }
    localObject3 = h;
    localObject2 = Integer.valueOf(i);
    localObject3 = (View)((HashMap)localObject3).get(localObject2);
    if (localObject3 == null)
    {
      localObject3 = findViewById(i);
      localObject2 = h;
      paramInteger = Integer.valueOf(i);
      ((HashMap)localObject2).put(paramInteger, localObject3);
    }
    localObject3 = (TextView)localObject3;
    paramInteger = new com/truecaller/premium/EmbeddedSubscriptionButtonsView$a;
    paramInteger.<init>(this);
    paramInteger = (View.OnClickListener)paramInteger;
    ((TextView)localObject3).setOnClickListener(paramInteger);
  }
  
  public final void a(boolean paramBoolean)
  {
    d locald = d;
    if (locald != null)
    {
      locald.a(paramBoolean);
      return;
    }
  }
  
  public final d getCallBack()
  {
    return d;
  }
  
  public final br getPremiumScreenNavigator()
  {
    br localbr = b;
    if (localbr == null)
    {
      String str = "premiumScreenNavigator";
      k.a(str);
    }
    return localbr;
  }
  
  public final e getPresenter()
  {
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locale;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.a(this);
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.y_();
  }
  
  public final void setCallBack(d paramd)
  {
    d = paramd;
  }
  
  public final void setErrorMessage(String paramString)
  {
    k.b(paramString, "message");
    removeAllViews();
    paramString = (TextView)t.a(this, 2131559204, true).findViewById(2131363032);
    Object localObject = (CharSequence)g;
    paramString.setText((CharSequence)localObject);
    localObject = new com/truecaller/premium/EmbeddedSubscriptionButtonsView$b;
    ((EmbeddedSubscriptionButtonsView.b)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramString.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void setLaunchContext(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramLaunchContext, "launchContext");
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.a(paramLaunchContext);
  }
  
  public final void setPremiumScreenNavigator(br parambr)
  {
    k.b(parambr, "<set-?>");
    b = parambr;
  }
  
  public final void setPresenter(e parame)
  {
    k.b(parame, "<set-?>");
    a = parame;
  }
  
  public final void setSubscriptionButtons(List paramList)
  {
    k.b(paramList, "buttons");
    a(null);
    setSubscriptionButtonsInternal(paramList);
  }
  
  final void setSubscriptionButtonsInternal(List paramList)
  {
    int i = paramList.size();
    Object localObject1 = c;
    if (localObject1 == null)
    {
      str = "subscriptionButtonViews";
      k.a(str);
    }
    int j = ((List)localObject1).size();
    if (i > j)
    {
      localObject2 = "We have more subscription items than the views.";
      AssertionUtil.reportWeirdnessButNeverCrash((String)localObject2);
    }
    Object localObject2 = c;
    if (localObject2 == null)
    {
      localObject1 = "subscriptionButtonViews";
      k.a((String)localObject1);
    }
    localObject2 = ((Iterable)localObject2).iterator();
    j = 0;
    localObject1 = null;
    int k = 0;
    String str = null;
    for (;;)
    {
      boolean bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      Object localObject3 = ((Iterator)localObject2).next();
      int m = k + 1;
      if (k < 0) {
        m.a();
      }
      localObject3 = (SubscriptionButtonView)localObject3;
      int n = paramList.size();
      if (k < n)
      {
        Object localObject4 = (cc)paramList.get(k);
        ((SubscriptionButtonView)localObject3).setButton((cc)localObject4);
        localObject4 = paramList.get(k);
        ((SubscriptionButtonView)localObject3).setTag(localObject4);
        localObject4 = localObject3;
        t.a((View)localObject3);
        localObject4 = new com/truecaller/premium/EmbeddedSubscriptionButtonsView$c;
        ((EmbeddedSubscriptionButtonsView.c)localObject4).<init>((SubscriptionButtonView)localObject3, k, this, paramList);
        localObject4 = (View.OnClickListener)localObject4;
        ((SubscriptionButtonView)localObject3).setOnClickListener((View.OnClickListener)localObject4);
      }
      else
      {
        localObject3 = (View)localObject3;
        t.a((View)localObject3, false);
      }
      k = m;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.EmbeddedSubscriptionButtonsView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */