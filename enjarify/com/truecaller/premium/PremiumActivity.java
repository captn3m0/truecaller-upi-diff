package com.truecaller.premium;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import c.u;
import c.x;
import com.truecaller.common.h.l;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.data.SubscriptionPromoEventMetaData;
import com.truecaller.utils.extensions.a;

public class PremiumActivity
  extends AppCompatActivity
{
  protected ba.b a()
  {
    ba.b localb = new com/truecaller/premium/ba$b;
    Integer localInteger = Integer.valueOf(2131233834);
    int i = l.a((Context)this, 41.0F);
    localb.<init>(localInteger, 2131233992, i);
    return localb;
  }
  
  public void onBackPressed()
  {
    Object localObject = getSupportFragmentManager();
    int i = 16908290;
    localObject = ((j)localObject).a(i);
    if (localObject != null)
    {
      localObject = ((Fragment)localObject).getChildFragmentManager();
      if (localObject != null)
      {
        i = ((j)localObject).e();
        if (i > 0) {
          ((j)localObject).c();
        } else {
          super.onBackPressed();
        }
        if (localObject != null) {
          return;
        }
      }
    }
    super.onBackPressed();
    localObject = x.a;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject1 = this;
    Settings.c((Context)this);
    a.a(this);
    int i = 2131558461;
    setContentView(i);
    localObject1 = getIntent();
    k.a(localObject1, "intent");
    localObject1 = ((Intent)localObject1).getExtras();
    String str = null;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = "launchContext";
      localObject1 = ((Bundle)localObject1).getSerializable((String)localObject2);
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = (PremiumPresenterView.LaunchContext)localObject1;
      localObject2 = getIntent();
      Object localObject3 = "intent";
      k.a(localObject2, (String)localObject3);
      localObject2 = ((Intent)localObject2).getExtras();
      if (localObject2 != null)
      {
        localObject3 = "analyticsMetadata";
        localObject2 = (SubscriptionPromoEventMetaData)((Bundle)localObject2).getParcelable((String)localObject3);
      }
      else
      {
        localObject2 = null;
      }
      localObject3 = getIntent();
      Object localObject4 = "intent";
      k.a(localObject3, (String)localObject4);
      localObject3 = ((Intent)localObject3).getExtras();
      if (localObject3 != null) {
        str = ((Bundle)localObject3).getString("selectedPage");
      }
      if (paramBundle == null)
      {
        paramBundle = getSupportFragmentManager().a();
        int j = 2131363154;
        localObject4 = ba.c;
        localObject4 = a();
        localObject1 = (Fragment)ba.a.a((PremiumPresenterView.LaunchContext)localObject1, (SubscriptionPromoEventMetaData)localObject2, str, (ba.b)localObject4);
        paramBundle = paramBundle.b(j, (Fragment)localObject1);
        paramBundle.f();
      }
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.PremiumPresenterView.LaunchContext");
    throw paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.PremiumActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */