package com.truecaller.premium;

import c.g.b.k;
import java.io.Serializable;

public final class ba$b
  implements Serializable
{
  final Integer a;
  final int b;
  final int c;
  final int d;
  
  public ba$b(Integer paramInteger, int paramInt1, int paramInt2, int paramInt3)
  {
    a = paramInteger;
    b = paramInt1;
    c = paramInt2;
    d = paramInt3;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof b;
      if (bool2)
      {
        paramObject = (b)paramObject;
        Integer localInteger1 = a;
        Integer localInteger2 = a;
        bool2 = k.a(localInteger1, localInteger2);
        if (bool2)
        {
          int i = b;
          int j = b;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            localInteger1 = null;
          }
          if (i != 0)
          {
            i = c;
            j = c;
            if (i == j)
            {
              i = 1;
            }
            else
            {
              i = 0;
              localInteger1 = null;
            }
            if (i != 0)
            {
              i = d;
              int k = d;
              if (i == k)
              {
                k = 1;
              }
              else
              {
                k = 0;
                paramObject = null;
              }
              if (k != 0) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    Integer localInteger = a;
    if (localInteger != null)
    {
      i = localInteger.hashCode();
    }
    else
    {
      i = 0;
      localInteger = null;
    }
    i *= 31;
    int j = b;
    int i = (i + j) * 31;
    j = c;
    i = (i + j) * 31;
    j = d;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PremiumFeaturesStyle(featuresNavigationIcon=");
    Integer localInteger = a;
    localStringBuilder.append(localInteger);
    localStringBuilder.append(", detailsNavigationIcon=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", detailsTitleTopMargin=");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", screenOffset=");
    i = d;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.ba.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */