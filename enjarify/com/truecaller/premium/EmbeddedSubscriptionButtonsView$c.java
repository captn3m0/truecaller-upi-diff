package com.truecaller.premium;

import android.view.View;
import android.view.View.OnClickListener;
import c.u;
import java.util.List;

final class EmbeddedSubscriptionButtonsView$c
  implements View.OnClickListener
{
  EmbeddedSubscriptionButtonsView$c(SubscriptionButtonView paramSubscriptionButtonView, int paramInt, EmbeddedSubscriptionButtonsView paramEmbeddedSubscriptionButtonsView, List paramList) {}
  
  public final void onClick(View paramView)
  {
    paramView = c.getPresenter();
    Object localObject = a.getTag();
    if (localObject != null)
    {
      localObject = (cc)localObject;
      paramView.a((cc)localObject);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.truecaller.premium.SubscriptionButton");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.EmbeddedSubscriptionButtonsView.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */