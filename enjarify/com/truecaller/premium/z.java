package com.truecaller.premium;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.u;
import com.truecaller.R.id;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.f;
import com.truecaller.adapter_delegates.g;
import com.truecaller.adapter_delegates.p;
import com.truecaller.adapter_delegates.s;
import com.truecaller.premium.data.PremiumType;
import java.io.Serializable;
import java.util.HashMap;

public final class z
  extends Fragment
{
  public static final z.a c;
  public ae a;
  public aa b;
  private f d;
  private HashMap e;
  
  static
  {
    z.a locala = new com/truecaller/premium/z$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    paramBundle = getArguments();
    Object localObject1;
    if (paramBundle != null)
    {
      localObject1 = "type";
      paramBundle = paramBundle.getSerializable((String)localObject1);
      if (paramBundle != null) {}
    }
    else
    {
      paramBundle = (Serializable)PremiumType.PREMIUM;
    }
    if (paramBundle != null)
    {
      paramBundle = (PremiumType)paramBundle;
      localObject1 = getParentFragment();
      if (localObject1 != null)
      {
        localObject1 = ((l)localObject1).c();
        Object localObject2 = new com/truecaller/premium/ai;
        ((ai)localObject2).<init>(paramBundle);
        ((k)localObject1).a((ai)localObject2).a(this);
        paramBundle = new com/truecaller/adapter_delegates/p;
        localObject1 = a;
        if (localObject1 == null)
        {
          localObject2 = "listItemPresenter";
          c.g.b.k.a((String)localObject2);
        }
        localObject1 = (com.truecaller.adapter_delegates.b)localObject1;
        int i = 2131559032;
        Object localObject3 = new com/truecaller/premium/z$b;
        ((z.b)localObject3).<init>(this);
        localObject3 = (c.g.a.b)localObject3;
        c.g.a.b localb1 = (c.g.a.b)z.c.a;
        paramBundle.<init>((com.truecaller.adapter_delegates.b)localObject1, i, (c.g.a.b)localObject3, localb1);
        localObject1 = new com/truecaller/adapter_delegates/p;
        localObject2 = b;
        if (localObject2 == null)
        {
          localObject3 = "listHeaderPresenter";
          c.g.b.k.a((String)localObject3);
        }
        localObject2 = (com.truecaller.adapter_delegates.b)localObject2;
        int j = 2131559033;
        localb1 = (c.g.a.b)z.d.a;
        c.g.a.b localb2 = (c.g.a.b)z.e.a;
        ((p)localObject1).<init>((com.truecaller.adapter_delegates.b)localObject2, j, localb1, localb2);
        localObject2 = new com/truecaller/adapter_delegates/f;
        localObject1 = (a)localObject1;
        localObject3 = new com/truecaller/adapter_delegates/g;
        localb1 = null;
        ((g)localObject3).<init>((byte)0);
        localObject3 = (s)localObject3;
        paramBundle = (a)paramBundle.a((a)localObject1, (s)localObject3);
        ((f)localObject2).<init>(paramBundle);
        d = ((f)localObject2);
        int k = R.id.list;
        paramBundle = (RecyclerView)a(k);
        c.g.b.k.a(paramBundle, "list");
        localObject1 = new android/support/v7/widget/LinearLayoutManager;
        localObject2 = requireContext();
        ((LinearLayoutManager)localObject1).<init>((Context)localObject2);
        localObject1 = (RecyclerView.LayoutManager)localObject1;
        paramBundle.setLayoutManager((RecyclerView.LayoutManager)localObject1);
        k = R.id.list;
        paramBundle = (RecyclerView)a(k);
        c.g.b.k.a(paramBundle, "list");
        localObject1 = d;
        if (localObject1 == null)
        {
          localObject2 = "listAdapter";
          c.g.b.k.a((String)localObject2);
        }
        localObject1 = (RecyclerView.Adapter)localObject1;
        paramBundle.setAdapter((RecyclerView.Adapter)localObject1);
        k = R.id.list;
        paramBundle = (RecyclerView)a(k);
        localObject1 = new com/truecaller/premium/y;
        localObject2 = requireContext();
        localObject3 = "requireContext()";
        c.g.b.k.a(localObject2, (String)localObject3);
        ((y)localObject1).<init>((Context)localObject2);
        localObject1 = (RecyclerView.ItemDecoration)localObject1;
        paramBundle.addItemDecoration((RecyclerView.ItemDecoration)localObject1);
        paramBundle = d;
        if (paramBundle == null)
        {
          localObject1 = "listAdapter";
          c.g.b.k.a((String)localObject1);
        }
        paramBundle.notifyDataSetChanged();
        return;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.PremiumComponentProvider");
      throw paramBundle;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.premium.data.PremiumType");
    throw paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558759, paramViewGroup, false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */