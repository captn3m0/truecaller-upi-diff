package com.truecaller.premium;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.premium.data.f;

public final class ad
  extends RecyclerView.ViewHolder
  implements ac
{
  private final TextView a;
  private final TextView b;
  
  public ad(View paramView)
  {
    super(paramView);
    paramView = itemView.findViewById(2131364884);
    k.a(paramView, "itemView.findViewById(R.id.title)");
    paramView = (TextView)paramView;
    a = paramView;
    paramView = itemView.findViewById(2131364603);
    k.a(paramView, "itemView.findViewById(R.id.subTitle)");
    paramView = (TextView)paramView;
    b = paramView;
  }
  
  public final void a(f paramf)
  {
    k.b(paramf, "title");
    TextView localTextView = a;
    int i = a;
    localTextView.setText(i);
    localTextView = b;
    paramf = (CharSequence)b;
    localTextView.setText(paramf);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */