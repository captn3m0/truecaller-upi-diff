package com.truecaller.premium;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import c.g.a.b;
import c.g.b.k;

final class ca$b
  implements SensorEventListener
{
  private final float[] a;
  private final float[] b;
  private final float[] c;
  private final float[] d;
  private final float[] e;
  private final float[] f;
  private boolean g;
  private boolean h;
  private boolean i;
  private final b j;
  
  public ca$b(b paramb)
  {
    j = paramb;
    int k = 3;
    float[] arrayOfFloat = new float[k];
    a = arrayOfFloat;
    arrayOfFloat = new float[k];
    b = arrayOfFloat;
    paramb = new float[k];
    c = paramb;
    k = 9;
    arrayOfFloat = new float[k];
    d = arrayOfFloat;
    arrayOfFloat = new float[k];
    e = arrayOfFloat;
    paramb = new float[k];
    f = paramb;
  }
  
  public final void onAccuracyChanged(Sensor paramSensor, int paramInt)
  {
    k.b(paramSensor, "sensor");
  }
  
  public final void onSensorChanged(SensorEvent paramSensorEvent)
  {
    k.b(paramSensorEvent, "event");
    Object localObject1 = sensor;
    Object localObject2 = "event.sensor";
    k.a(localObject1, (String)localObject2);
    int k = ((Sensor)localObject1).getType();
    int n = 9;
    float f1 = 1.3E-44F;
    float f2 = 0.0F;
    int i1 = 2;
    boolean bool2 = true;
    float f3 = Float.MIN_VALUE;
    float f4;
    if (k != n)
    {
      switch (k)
      {
      default: 
        return;
      case 2: 
        localObject1 = b;
        f1 = values[0];
        localObject1[0] = f1;
        localObject1 = b;
        localObject2 = values;
        f1 = localObject2[bool2];
        localObject1[bool2] = f1;
        localObject1 = b;
        paramSensorEvent = values;
        f4 = paramSensorEvent[i1];
        localObject1[i1] = f4;
        i = bool2;
        break;
      case 1: 
        boolean bool1 = g;
        if (bool1) {
          break;
        }
        localObject1 = a;
        f1 = values[0];
        localObject1[0] = f1;
        localObject1 = a;
        localObject2 = values;
        f1 = localObject2[bool2];
        localObject1[bool2] = f1;
        localObject1 = a;
        paramSensorEvent = values;
        f4 = paramSensorEvent[i1];
        localObject1[i1] = f4;
        h = bool2;
        break;
      }
    }
    else
    {
      localObject1 = a;
      f1 = values[0];
      localObject1[0] = f1;
      localObject1 = a;
      localObject2 = values;
      f1 = localObject2[bool2];
      localObject1[bool2] = f1;
      localObject1 = a;
      paramSensorEvent = values;
      f4 = paramSensorEvent[i1];
      localObject1[i1] = f4;
      g = bool2;
    }
    boolean bool3 = g;
    if (!bool3)
    {
      bool3 = h;
      if (!bool3) {}
    }
    else
    {
      bool3 = i;
      if (bool3)
      {
        paramSensorEvent = d;
        localObject1 = e;
        localObject2 = a;
        float[] arrayOfFloat = b;
        SensorManager.getRotationMatrix(paramSensorEvent, (float[])localObject1, (float[])localObject2, arrayOfFloat);
        paramSensorEvent = d;
        int m = 129;
        localObject2 = f;
        SensorManager.remapCoordinateSystem(paramSensorEvent, i1, m, (float[])localObject2);
        paramSensorEvent = f;
        localObject1 = c;
        SensorManager.getOrientation(paramSensorEvent, (float[])localObject1);
        paramSensorEvent = j;
        localObject1 = new com/truecaller/premium/ca$a;
        localObject2 = c;
        f2 = localObject2[0];
        f3 = localObject2[bool2];
        f1 = localObject2[i1];
        ((ca.a)localObject1).<init>(f2, f3, f1);
        paramSensorEvent.invoke(localObject1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.ca.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */