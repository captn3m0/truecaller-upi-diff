package com.truecaller.premium;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import c.g.b.k;
import com.truecaller.common.h.l;
import com.truecaller.util.at;

public final class y
  extends RecyclerView.ItemDecoration
{
  private final Drawable a;
  private final Rect b;
  private final int c;
  
  public y(Context paramContext)
  {
    Object localObject = at.a(paramContext, 2131231149);
    k.a(localObject, "getDrawable(context, R.d…divider_premium_features)");
    a = ((Drawable)localObject);
    localObject = new android/graphics/Rect;
    ((Rect)localObject).<init>();
    b = ((Rect)localObject);
    int i = l.a(paramContext, 16.0F);
    c = i;
  }
  
  public final void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    k.b(paramRect, "outRect");
    k.b(paramView, "view");
    k.b(paramRecyclerView, "parent");
    k.b(paramState, "state");
    int i = a.getIntrinsicHeight();
    paramRect.set(0, 0, 0, i);
  }
  
  public final void onDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    k.b(paramCanvas, "canvas");
    k.b(paramRecyclerView, "parent");
    String str = "state";
    k.b(paramState, str);
    int i = paramRecyclerView.getChildCount();
    int k = 1;
    if (i <= k) {
      return;
    }
    paramCanvas.save();
    boolean bool = paramRecyclerView.getClipToPadding();
    int j;
    int m;
    int i1;
    int i2;
    if (bool)
    {
      j = paramRecyclerView.getPaddingLeft();
      m = c;
      j += m;
      m = paramRecyclerView.getWidth();
      n = paramRecyclerView.getPaddingRight();
      m -= n;
      n = c;
      m -= n;
      n = paramRecyclerView.getPaddingTop();
      i1 = paramRecyclerView.getHeight();
      i2 = paramRecyclerView.getPaddingBottom();
      i1 -= i2;
      paramCanvas.clipRect(j, n, m, i1);
    }
    else
    {
      j = c;
      m = paramRecyclerView.getWidth();
      n = c;
      m -= n;
    }
    int n = paramRecyclerView.getChildCount();
    while (k < n)
    {
      Object localObject = paramRecyclerView.getChildAt(k);
      Rect localRect = b;
      paramRecyclerView.getDecoratedBoundsWithMargins((View)localObject, localRect);
      localRect = b;
      i2 = bottom;
      k.a(localObject, "child");
      float f = ((View)localObject).getTranslationY();
      i1 = Math.round(f);
      i2 += i1;
      i1 = a.getIntrinsicHeight();
      i1 = i2 - i1;
      Drawable localDrawable = a;
      localDrawable.setBounds(j, i1, m, i2);
      localObject = a;
      ((Drawable)localObject).draw(paramCanvas);
      k += 1;
    }
    paramCanvas.restore();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */