package com.truecaller.premium;

import com.truecaller.premium.data.PremiumType;
import java.util.List;

public abstract interface PremiumPresenterView
{
  public abstract void a(PremiumType paramPremiumType, int paramInt, boolean paramBoolean);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void a(List paramList, int paramInt);
  
  public abstract void b(PremiumType paramPremiumType);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c(PremiumType paramPremiumType);
  
  public abstract void e();
  
  public abstract void g();
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract void o();
  
  public abstract void p();
  
  public abstract void q();
  
  public abstract void r();
}

/* Location:
 * Qualified Name:     com.truecaller.premium.PremiumPresenterView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */