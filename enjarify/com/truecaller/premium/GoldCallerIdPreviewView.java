package com.truecaller.premium;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.b;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.util.at;
import com.truecaller.util.co;

public final class GoldCallerIdPreviewView
  extends ConstraintLayout
{
  final ContactPhoto k;
  final TextView l;
  final TextView m;
  final TextView n;
  final TextView o;
  private final ShineView p;
  
  public GoldCallerIdPreviewView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private GoldCallerIdPreviewView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    paramAttributeSet = this;
    paramAttributeSet = (ViewGroup)this;
    View.inflate(paramContext, 2131559203, paramAttributeSet);
    setClipChildren(false);
    setBackgroundResource(2131230999);
    paramAttributeSet = findViewById(2131362407);
    String str1 = "findViewById(R.id.caller_id_photo)";
    k.a(paramAttributeSet, str1);
    paramAttributeSet = (ContactPhoto)paramAttributeSet;
    k = paramAttributeSet;
    paramAttributeSet = k;
    boolean bool1 = true;
    paramAttributeSet.setIsGold(bool1);
    paramAttributeSet = findViewById(2131362413);
    k.a(paramAttributeSet, "findViewById(R.id.caller_id_title)");
    paramAttributeSet = (TextView)paramAttributeSet;
    l = paramAttributeSet;
    paramAttributeSet = findViewById(2131362411);
    k.a(paramAttributeSet, "findViewById(R.id.caller_id_subtitle)");
    paramAttributeSet = (TextView)paramAttributeSet;
    m = paramAttributeSet;
    paramAttributeSet = findViewById(2131362405);
    k.a(paramAttributeSet, "findViewById(R.id.caller_id_number)");
    paramAttributeSet = (TextView)paramAttributeSet;
    n = paramAttributeSet;
    paramAttributeSet = findViewById(2131362406);
    k.a(paramAttributeSet, "findViewById(R.id.caller_id_number_type)");
    paramAttributeSet = (TextView)paramAttributeSet;
    o = paramAttributeSet;
    paramAttributeSet = findViewById(2131363196);
    String str2 = "findViewById(R.id.gold_shine)";
    k.a(paramAttributeSet, str2);
    paramAttributeSet = (ShineView)paramAttributeSet;
    p = paramAttributeSet;
    int j = 2131363193;
    float f1 = 1.8346188E38F;
    paramAttributeSet = (ImageView)findViewById(j);
    boolean bool2 = co.c(paramContext);
    if (bool2) {
      i1 = 2131234630;
    } else {
      i1 = 2131234629;
    }
    paramAttributeSet.setImageResource(i1);
    j = 1065353216;
    f1 = 1.0F;
    int i1 = at.a(paramContext, f1);
    int i2 = b.c(paramContext, 2131100381);
    char c = '\004';
    TextView[] arrayOfTextView = new TextView[c];
    TextView localTextView = l;
    arrayOfTextView[0] = localTextView;
    localTextView = m;
    arrayOfTextView[bool1] = localTextView;
    localTextView = n;
    arrayOfTextView[2] = localTextView;
    int i = 3;
    localTextView = o;
    arrayOfTextView[i] = localTextView;
    while (paramChar < c)
    {
      str1 = arrayOfTextView[paramChar];
      localTextView = null;
      float f2 = i1;
      str1.setShadowLayer(f1, 0.0F, f2, i2);
      paramChar += '\001';
    }
    i2 = Build.VERSION.SDK_INT;
    j = 21;
    f1 = 2.9E-44F;
    if (i2 >= j)
    {
      paramContext = new com/truecaller/premium/GoldCallerIdPreviewView$1;
      paramContext.<init>();
      paramContext = (ViewOutlineProvider)paramContext;
      setOutlineProvider(paramContext);
    }
  }
  
  public final ShineView getShineView()
  {
    return p;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.GoldCallerIdPreviewView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */