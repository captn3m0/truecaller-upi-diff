package com.truecaller.premium;

import android.arch.lifecycle.h;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.adapter_delegates.i;
import com.truecaller.premium.data.a;
import com.truecaller.premium.data.e;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.utils.extensions.t;

public final class ah
  extends RecyclerView.ViewHolder
  implements ag
{
  private final TextView a;
  private final TextView b;
  private final ImageView c;
  private final GoldCallerIdPreviewView d;
  
  public ah(View paramView, com.truecaller.adapter_delegates.k paramk, h paramh)
  {
    super(paramView);
    paramView = itemView.findViewById(2131364884);
    c.g.b.k.a(paramView, "itemView.findViewById(R.id.title)");
    paramView = (TextView)paramView;
    a = paramView;
    paramView = itemView.findViewById(2131362810);
    c.g.b.k.a(paramView, "itemView.findViewById(R.id.description)");
    paramView = (TextView)paramView;
    b = paramView;
    paramView = itemView.findViewById(2131363301);
    c.g.b.k.a(paramView, "itemView.findViewById(R.id.icon)");
    paramView = (ImageView)paramView;
    c = paramView;
    paramView = itemView.findViewById(2131362388);
    c.g.b.k.a(paramView, "itemView.findViewById(R.id.callerIdPreview)");
    paramView = (GoldCallerIdPreviewView)paramView;
    d = paramView;
    paramView = itemView;
    c.g.b.k.a(paramView, "itemView");
    Object localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, 12);
    d.getShineView().setLifecycleOwner(paramh);
  }
  
  public final void a(e parame)
  {
    c.g.b.k.b(parame, "premiumFeature");
    Object localObject1 = c;
    int i = c;
    ((ImageView)localObject1).setImageResource(i);
    localObject1 = a;
    i = b;
    ((TextView)localObject1).setText(i);
    localObject1 = b;
    i = d;
    ((TextView)localObject1).setText(i);
    localObject1 = (View)d;
    Object localObject2 = g;
    boolean bool1 = false;
    CharSequence localCharSequence = null;
    boolean bool2 = true;
    if (localObject2 != null)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject2 = null;
    }
    t.a((View)localObject1, i);
    localObject1 = d;
    parame = g;
    if (parame != null)
    {
      localObject2 = k;
      Object localObject3 = a;
      ((ContactPhoto)localObject2).a(localObject3, null);
      localObject2 = l;
      localObject3 = (CharSequence)b;
      ((TextView)localObject2).setText((CharSequence)localObject3);
      localObject2 = m;
      localObject3 = (CharSequence)c;
      ((TextView)localObject2).setText((CharSequence)localObject3);
      localObject2 = (View)m;
      localObject3 = (CharSequence)c;
      if (localObject3 != null)
      {
        int j = ((CharSequence)localObject3).length();
        if (j != 0) {}
      }
      else
      {
        bool1 = true;
      }
      bool1 ^= bool2;
      t.a((View)localObject2, bool1);
      localObject2 = n;
      localCharSequence = (CharSequence)d;
      ((TextView)localObject2).setText(localCharSequence);
      localObject1 = o;
      parame = (CharSequence)e;
      ((TextView)localObject1).setText(parame);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */