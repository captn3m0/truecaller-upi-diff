package com.truecaller.premium;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.c;
import android.support.design.widget.CollapsingToolbarLayout;
import c.g.b.k;
import com.truecaller.R.id;

public final class as$c
  implements AppBarLayout.c
{
  private int b = -1;
  
  as$c(as paramas) {}
  
  public final void a(AppBarLayout paramAppBarLayout, int paramInt)
  {
    String str1 = "appBarLayout";
    k.b(paramAppBarLayout, str1);
    int i = b;
    int j = -1;
    if (i == j)
    {
      k = paramAppBarLayout.getTotalScrollRange();
      b = k;
    }
    int k = b + paramInt;
    if (k == 0)
    {
      paramAppBarLayout = a;
      paramInt = R.id.collapsingToolbar;
      paramAppBarLayout = (CollapsingToolbarLayout)paramAppBarLayout.e(paramInt);
      k.a(paramAppBarLayout, "collapsingToolbar");
      paramAppBarLayout.setTitleEnabled(true);
      return;
    }
    paramAppBarLayout = a;
    paramInt = R.id.collapsingToolbar;
    paramAppBarLayout = (CollapsingToolbarLayout)paramAppBarLayout.e(paramInt);
    String str2 = "collapsingToolbar";
    k.a(paramAppBarLayout, str2);
    boolean bool = paramAppBarLayout.a();
    if (bool)
    {
      paramAppBarLayout = a;
      paramInt = R.id.collapsingToolbar;
      paramAppBarLayout = (CollapsingToolbarLayout)paramAppBarLayout.e(paramInt);
      k.a(paramAppBarLayout, "collapsingToolbar");
      paramInt = 0;
      str2 = null;
      paramAppBarLayout.setTitleEnabled(false);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.as.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */