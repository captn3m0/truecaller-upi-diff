package com.truecaller.insights.database;

import android.arch.persistence.db.c.a;
import android.arch.persistence.db.c.b;
import android.arch.persistence.db.c.b.a;
import android.arch.persistence.db.c.c;
import android.arch.persistence.room.h;
import android.arch.persistence.room.h.a;
import com.truecaller.insights.database.a.b;
import com.truecaller.insights.database.a.e;
import com.truecaller.insights.database.a.f;

public class InsightsDb_Impl
  extends InsightsDb
{
  private volatile com.truecaller.insights.database.a.c g;
  private volatile e h;
  private volatile com.truecaller.insights.database.a.a i;
  
  public final android.arch.persistence.room.d a()
  {
    android.arch.persistence.room.d locald = new android/arch/persistence/room/d;
    String[] tmp8_5 = new String[4];
    String[] tmp9_8 = tmp8_5;
    String[] tmp9_8 = tmp8_5;
    tmp9_8[0] = "parsed_data_object_table";
    tmp9_8[1] = "sms_backup_table";
    tmp9_8[2] = "link_prune_table";
    String[] tmp22_9 = tmp9_8;
    tmp22_9[3] = "states_table";
    String[] arrayOfString = tmp22_9;
    locald.<init>(this, arrayOfString);
    return locald;
  }
  
  public final android.arch.persistence.db.c b(android.arch.persistence.room.a parama)
  {
    Object localObject1 = new android/arch/persistence/room/h;
    Object localObject2 = new com/truecaller/insights/database/InsightsDb_Impl$1;
    ((InsightsDb_Impl.1)localObject2).<init>(this);
    ((h)localObject1).<init>(parama, (h.a)localObject2, "69490ecd0e2ecba29d1d32dd23505ee7", "49ad842650d390dada07912b6ca72287");
    localObject2 = c.b.a(b);
    String str = c;
    b = str;
    c = ((c.a)localObject1);
    localObject1 = ((c.b.a)localObject2).a();
    return a.a((c.b)localObject1);
  }
  
  public final com.truecaller.insights.database.a.c h()
  {
    Object localObject1 = g;
    if (localObject1 != null) {
      return g;
    }
    try
    {
      localObject1 = g;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/insights/database/a/d;
        ((com.truecaller.insights.database.a.d)localObject1).<init>(this);
        g = ((com.truecaller.insights.database.a.c)localObject1);
      }
      localObject1 = g;
      return (com.truecaller.insights.database.a.c)localObject1;
    }
    finally {}
  }
  
  public final e i()
  {
    Object localObject1 = h;
    if (localObject1 != null) {
      return h;
    }
    try
    {
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/insights/database/a/f;
        ((f)localObject1).<init>(this);
        h = ((e)localObject1);
      }
      localObject1 = h;
      return (e)localObject1;
    }
    finally {}
  }
  
  public final com.truecaller.insights.database.a.a j()
  {
    Object localObject1 = i;
    if (localObject1 != null) {
      return i;
    }
    try
    {
      localObject1 = i;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/insights/database/a/b;
        ((b)localObject1).<init>(this);
        i = ((com.truecaller.insights.database.a.a)localObject1);
      }
      localObject1 = i;
      return (com.truecaller.insights.database.a.a)localObject1;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.InsightsDb_Impl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */