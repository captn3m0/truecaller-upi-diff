package com.truecaller.insights.database;

import android.arch.persistence.room.b.b.a;
import android.arch.persistence.room.b.b.b;
import android.arch.persistence.room.b.b.d;
import android.arch.persistence.room.f.b;
import android.arch.persistence.room.h.a;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class InsightsDb_Impl$1
  extends h.a
{
  InsightsDb_Impl$1(InsightsDb_Impl paramInsightsDb_Impl)
  {
    super(2);
  }
  
  public final void a(android.arch.persistence.db.b paramb)
  {
    paramb.c("DROP TABLE IF EXISTS `parsed_data_object_table`");
    paramb.c("DROP TABLE IF EXISTS `sms_backup_table`");
    paramb.c("DROP TABLE IF EXISTS `link_prune_table`");
    paramb.c("DROP TABLE IF EXISTS `states_table`");
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    paramb.c("CREATE TABLE IF NOT EXISTS `parsed_data_object_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `messageID` INTEGER NOT NULL, `d` TEXT NOT NULL, `k` TEXT NOT NULL, `p` TEXT NOT NULL, `c` TEXT NOT NULL, `o` TEXT NOT NULL, `f` TEXT NOT NULL, `g` TEXT NOT NULL, `s` TEXT NOT NULL, `val1` TEXT NOT NULL, `val2` TEXT NOT NULL, `val3` TEXT NOT NULL, `val4` TEXT NOT NULL, `val5` TEXT NOT NULL, `datetime` TEXT NOT NULL, `address` TEXT NOT NULL, `msgdatetime` TEXT NOT NULL, `date` TEXT NOT NULL, `msgdate` TEXT NOT NULL, `dff_val1` TEXT NOT NULL, `dff_val2` TEXT NOT NULL, `dff_val3` TEXT NOT NULL, `dff_val4` TEXT NOT NULL, `dff_val5` TEXT NOT NULL, `active` INTEGER NOT NULL, `state` TEXT NOT NULL, `created_at` INTEGER NOT NULL)");
    paramb.c("CREATE  INDEX `index_parsed_data_object_table_d` ON `parsed_data_object_table` (`d`)");
    paramb.c("CREATE TABLE IF NOT EXISTS `sms_backup_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `messageID` INTEGER NOT NULL, `address` TEXT NOT NULL, `message` TEXT NOT NULL, `date` INTEGER NOT NULL, `parseFailed` INTEGER NOT NULL, `errorMessage` TEXT NOT NULL, `retryCount` INTEGER NOT NULL, `created_at` INTEGER NOT NULL)");
    paramb.c("CREATE  INDEX `index_sms_backup_table_address` ON `sms_backup_table` (`address`)");
    paramb.c("CREATE TABLE IF NOT EXISTS `link_prune_table` (`parent_id` INTEGER NOT NULL, `child_id` INTEGER NOT NULL, `link_type` TEXT NOT NULL, `created_at` INTEGER NOT NULL, PRIMARY KEY(`parent_id`, `child_id`), FOREIGN KEY(`parent_id`) REFERENCES `parsed_data_object_table`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION , FOREIGN KEY(`child_id`) REFERENCES `parsed_data_object_table`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION )");
    paramb.c("CREATE  INDEX `index_link_prune_table_parent_id` ON `link_prune_table` (`parent_id`)");
    paramb.c("CREATE  INDEX `index_link_prune_table_child_id` ON `link_prune_table` (`child_id`)");
    paramb.c("CREATE TABLE IF NOT EXISTS `states_table` (`owner` TEXT NOT NULL, `last_updated_at` INTEGER NOT NULL, `last_updated_data` TEXT, `created_at` INTEGER NOT NULL, PRIMARY KEY(`owner`))");
    paramb.c("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    paramb.c("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"69490ecd0e2ecba29d1d32dd23505ee7\")");
  }
  
  public final void c(android.arch.persistence.db.b paramb)
  {
    InsightsDb_Impl.a(b, paramb);
    paramb.c("PRAGMA foreign_keys = ON");
    InsightsDb_Impl.b(b, paramb);
    List localList1 = InsightsDb_Impl.d(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = InsightsDb_Impl.e(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)InsightsDb_Impl.f(b).get(i);
        localb.b(paramb);
        i += 1;
      }
    }
  }
  
  public final void d(android.arch.persistence.db.b paramb)
  {
    List localList1 = InsightsDb_Impl.a(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = InsightsDb_Impl.b(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)InsightsDb_Impl.c(b).get(i);
        localb.a(paramb);
        i += 1;
      }
    }
  }
  
  public final void e(android.arch.persistence.db.b paramb)
  {
    Object localObject1 = paramb;
    Object localObject2 = new java/util/HashMap;
    ((HashMap)localObject2).<init>(28);
    Object localObject3 = new android/arch/persistence/room/b/b$a;
    int i = 1;
    ((b.a)localObject3).<init>("id", "INTEGER", i, i);
    ((HashMap)localObject2).put("id", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("messageID", "INTEGER", i, 0);
    ((HashMap)localObject2).put("messageID", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("d", "TEXT", i, 0);
    ((HashMap)localObject2).put("d", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("k", "TEXT", i, 0);
    ((HashMap)localObject2).put("k", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("p", "TEXT", i, 0);
    ((HashMap)localObject2).put("p", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("c", "TEXT", i, 0);
    ((HashMap)localObject2).put("c", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("o", "TEXT", i, 0);
    ((HashMap)localObject2).put("o", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("f", "TEXT", i, 0);
    ((HashMap)localObject2).put("f", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("g", "TEXT", i, 0);
    ((HashMap)localObject2).put("g", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("s", "TEXT", i, 0);
    ((HashMap)localObject2).put("s", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("val1", "TEXT", i, 0);
    ((HashMap)localObject2).put("val1", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("val2", "TEXT", i, 0);
    ((HashMap)localObject2).put("val2", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("val3", "TEXT", i, 0);
    ((HashMap)localObject2).put("val3", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("val4", "TEXT", i, 0);
    ((HashMap)localObject2).put("val4", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("val5", "TEXT", i, 0);
    ((HashMap)localObject2).put("val5", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("datetime", "TEXT", i, 0);
    ((HashMap)localObject2).put("datetime", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("address", "TEXT", i, 0);
    ((HashMap)localObject2).put("address", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("msgdatetime", "TEXT", i, 0);
    ((HashMap)localObject2).put("msgdatetime", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("date", "TEXT", i, 0);
    ((HashMap)localObject2).put("date", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("msgdate", "TEXT", i, 0);
    ((HashMap)localObject2).put("msgdate", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("dff_val1", "TEXT", i, 0);
    ((HashMap)localObject2).put("dff_val1", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("dff_val2", "TEXT", i, 0);
    ((HashMap)localObject2).put("dff_val2", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("dff_val3", "TEXT", i, 0);
    ((HashMap)localObject2).put("dff_val3", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("dff_val4", "TEXT", i, 0);
    ((HashMap)localObject2).put("dff_val4", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("dff_val5", "TEXT", i, 0);
    ((HashMap)localObject2).put("dff_val5", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("active", "INTEGER", i, 0);
    ((HashMap)localObject2).put("active", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("state", "TEXT", i, 0);
    ((HashMap)localObject2).put("state", localObject3);
    localObject3 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject3).<init>("created_at", "INTEGER", i, 0);
    ((HashMap)localObject2).put("created_at", localObject3);
    Object localObject4 = new java/util/HashSet;
    ((HashSet)localObject4).<init>(0);
    localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>(i);
    Object localObject5 = new android/arch/persistence/room/b/b$d;
    Object localObject6 = Arrays.asList(new String[] { "d" });
    ((b.d)localObject5).<init>("index_parsed_data_object_table_d", false, (List)localObject6);
    ((HashSet)localObject3).add(localObject5);
    localObject5 = new android/arch/persistence/room/b/b;
    Object localObject7 = "parsed_data_object_table";
    ((android.arch.persistence.room.b.b)localObject5).<init>((String)localObject7, (Map)localObject2, (Set)localObject4, (Set)localObject3);
    localObject2 = android.arch.persistence.room.b.b.a(paramb, "parsed_data_object_table");
    boolean bool1 = ((android.arch.persistence.room.b.b)localObject5).equals(localObject2);
    if (bool1)
    {
      localObject2 = new java/util/HashMap;
      ((HashMap)localObject2).<init>(9);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("id", "INTEGER", i, i);
      ((HashMap)localObject2).put("id", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("messageID", "INTEGER", i, 0);
      ((HashMap)localObject2).put("messageID", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("address", "TEXT", i, 0);
      ((HashMap)localObject2).put("address", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("message", "TEXT", i, 0);
      ((HashMap)localObject2).put("message", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("date", "INTEGER", i, 0);
      ((HashMap)localObject2).put("date", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("parseFailed", "INTEGER", i, 0);
      ((HashMap)localObject2).put("parseFailed", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("errorMessage", "TEXT", i, 0);
      ((HashMap)localObject2).put("errorMessage", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("retryCount", "INTEGER", i, 0);
      ((HashMap)localObject2).put("retryCount", localObject3);
      localObject3 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject3).<init>("created_at", "INTEGER", i, 0);
      ((HashMap)localObject2).put("created_at", localObject3);
      localObject4 = new java/util/HashSet;
      ((HashSet)localObject4).<init>(0);
      localObject3 = new java/util/HashSet;
      ((HashSet)localObject3).<init>(i);
      localObject5 = new android/arch/persistence/room/b/b$d;
      localObject6 = Arrays.asList(new String[] { "address" });
      ((b.d)localObject5).<init>("index_sms_backup_table_address", false, (List)localObject6);
      ((HashSet)localObject3).add(localObject5);
      localObject5 = new android/arch/persistence/room/b/b;
      localObject7 = "sms_backup_table";
      ((android.arch.persistence.room.b.b)localObject5).<init>((String)localObject7, (Map)localObject2, (Set)localObject4, (Set)localObject3);
      localObject2 = android.arch.persistence.room.b.b.a(paramb, "sms_backup_table");
      bool1 = ((android.arch.persistence.room.b.b)localObject5).equals(localObject2);
      if (bool1)
      {
        localObject2 = new java/util/HashMap;
        int j = 4;
        ((HashMap)localObject2).<init>(j);
        localObject5 = new android/arch/persistence/room/b/b$a;
        ((b.a)localObject5).<init>("parent_id", "INTEGER", i, i);
        ((HashMap)localObject2).put("parent_id", localObject5);
        localObject5 = new android/arch/persistence/room/b/b$a;
        int k = 2;
        ((b.a)localObject5).<init>("child_id", "INTEGER", i, k);
        ((HashMap)localObject2).put("child_id", localObject5);
        localObject5 = new android/arch/persistence/room/b/b$a;
        ((b.a)localObject5).<init>("link_type", "TEXT", i, 0);
        ((HashMap)localObject2).put("link_type", localObject5);
        localObject5 = new android/arch/persistence/room/b/b$a;
        ((b.a)localObject5).<init>("created_at", "INTEGER", i, 0);
        ((HashMap)localObject2).put("created_at", localObject5);
        localObject3 = new java/util/HashSet;
        ((HashSet)localObject3).<init>(k);
        localObject5 = new android/arch/persistence/room/b/b$b;
        String str1 = "parsed_data_object_table";
        String str2 = "NO ACTION";
        String str3 = "NO ACTION";
        List localList1 = Arrays.asList(new String[] { "parent_id" });
        List localList2 = Arrays.asList(new String[] { "id" });
        ((b.b)localObject5).<init>(str1, str2, str3, localList1, localList2);
        ((HashSet)localObject3).add(localObject5);
        localObject5 = new android/arch/persistence/room/b/b$b;
        String str4 = "parsed_data_object_table";
        String str5 = "NO ACTION";
        String str6 = "NO ACTION";
        List localList3 = Arrays.asList(new String[] { "child_id" });
        List localList4 = Arrays.asList(new String[] { "id" });
        ((b.b)localObject5).<init>(str4, str5, str6, localList3, localList4);
        ((HashSet)localObject3).add(localObject5);
        localObject5 = new java/util/HashSet;
        ((HashSet)localObject5).<init>(k);
        localObject7 = new android/arch/persistence/room/b/b$d;
        List localList5 = Arrays.asList(new String[] { "parent_id" });
        ((b.d)localObject7).<init>("index_link_prune_table_parent_id", false, localList5);
        ((HashSet)localObject5).add(localObject7);
        localObject7 = new android/arch/persistence/room/b/b$d;
        localList5 = Arrays.asList(new String[] { "child_id" });
        ((b.d)localObject7).<init>("index_link_prune_table_child_id", false, localList5);
        ((HashSet)localObject5).add(localObject7);
        localObject7 = new android/arch/persistence/room/b/b;
        localObject6 = "link_prune_table";
        ((android.arch.persistence.room.b.b)localObject7).<init>((String)localObject6, (Map)localObject2, (Set)localObject3, (Set)localObject5);
        localObject2 = android.arch.persistence.room.b.b.a(paramb, "link_prune_table");
        boolean bool2 = ((android.arch.persistence.room.b.b)localObject7).equals(localObject2);
        if (bool2)
        {
          localObject2 = new java/util/HashMap;
          ((HashMap)localObject2).<init>(j);
          localObject3 = new android/arch/persistence/room/b/b$a;
          ((b.a)localObject3).<init>("owner", "TEXT", i, i);
          ((HashMap)localObject2).put("owner", localObject3);
          localObject3 = new android/arch/persistence/room/b/b$a;
          ((b.a)localObject3).<init>("last_updated_at", "INTEGER", i, 0);
          ((HashMap)localObject2).put("last_updated_at", localObject3);
          localObject3 = new android/arch/persistence/room/b/b$a;
          ((b.a)localObject3).<init>("last_updated_data", "TEXT", false, 0);
          ((HashMap)localObject2).put("last_updated_data", localObject3);
          localObject3 = new android/arch/persistence/room/b/b$a;
          ((b.a)localObject3).<init>("created_at", "INTEGER", i, 0);
          ((HashMap)localObject2).put("created_at", localObject3);
          localObject4 = new java/util/HashSet;
          ((HashSet)localObject4).<init>(0);
          localObject3 = new java/util/HashSet;
          ((HashSet)localObject3).<init>(0);
          localObject5 = new android/arch/persistence/room/b/b;
          localObject7 = "states_table";
          ((android.arch.persistence.room.b.b)localObject5).<init>((String)localObject7, (Map)localObject2, (Set)localObject4, (Set)localObject3);
          localObject2 = "states_table";
          localObject1 = android.arch.persistence.room.b.b.a(paramb, (String)localObject2);
          boolean bool3 = ((android.arch.persistence.room.b.b)localObject5).equals(localObject1);
          if (bool3) {
            return;
          }
          localObject2 = new java/lang/IllegalStateException;
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>("Migration didn't properly handle states_table(com.truecaller.insights.models.InsightState).\n Expected:\n");
          ((StringBuilder)localObject4).append(localObject5);
          ((StringBuilder)localObject4).append("\n Found:\n");
          ((StringBuilder)localObject4).append(localObject1);
          localObject1 = ((StringBuilder)localObject4).toString();
          ((IllegalStateException)localObject2).<init>((String)localObject1);
          throw ((Throwable)localObject2);
        }
        localObject1 = new java/lang/IllegalStateException;
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>("Migration didn't properly handle link_prune_table(com.truecaller.insights.models.LinkPruneMap).\n Expected:\n");
        ((StringBuilder)localObject4).append(localObject7);
        ((StringBuilder)localObject4).append("\n Found:\n");
        ((StringBuilder)localObject4).append(localObject2);
        localObject2 = ((StringBuilder)localObject4).toString();
        ((IllegalStateException)localObject1).<init>((String)localObject2);
        throw ((Throwable)localObject1);
      }
      localObject1 = new java/lang/IllegalStateException;
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("Migration didn't properly handle sms_backup_table(com.truecaller.insights.models.SmsBackup).\n Expected:\n");
      ((StringBuilder)localObject4).append(localObject5);
      ((StringBuilder)localObject4).append("\n Found:\n");
      ((StringBuilder)localObject4).append(localObject2);
      localObject2 = ((StringBuilder)localObject4).toString();
      ((IllegalStateException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    localObject1 = new java/lang/IllegalStateException;
    localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>("Migration didn't properly handle parsed_data_object_table(com.truecaller.insights.models.ParsedDataObject).\n Expected:\n");
    ((StringBuilder)localObject4).append(localObject5);
    ((StringBuilder)localObject4).append("\n Found:\n");
    ((StringBuilder)localObject4).append(localObject2);
    localObject2 = ((StringBuilder)localObject4).toString();
    ((IllegalStateException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.InsightsDb_Impl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */