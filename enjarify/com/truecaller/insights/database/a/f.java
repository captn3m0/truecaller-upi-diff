package com.truecaller.insights.database.a;

import android.arch.persistence.room.c;
import android.arch.persistence.room.i;
import android.database.Cursor;
import com.truecaller.insights.d.a;
import com.truecaller.insights.models.InsightState;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class f
  implements e
{
  private final android.arch.persistence.room.f a;
  private final c b;
  private final a c;
  
  public f(android.arch.persistence.room.f paramf)
  {
    Object localObject = new com/truecaller/insights/d/a;
    ((a)localObject).<init>();
    c = ((a)localObject);
    a = paramf;
    localObject = new com/truecaller/insights/database/a/f$1;
    ((f.1)localObject).<init>(this, paramf);
    b = ((c)localObject);
  }
  
  public final List a(String paramString)
  {
    int i = 1;
    i locali = i.a("SELECT * FROM states_table where owner is ?", i);
    if (paramString == null)
    {
      paramString = e;
      paramString[i] = i;
    }
    else
    {
      locali.a(i, paramString);
    }
    paramString = a.a(locali);
    String str1 = "owner";
    try
    {
      i = paramString.getColumnIndexOrThrow(str1);
      String str2 = "last_updated_at";
      int j = paramString.getColumnIndexOrThrow(str2);
      String str3 = "last_updated_data";
      int k = paramString.getColumnIndexOrThrow(str3);
      String str4 = "created_at";
      int m = paramString.getColumnIndexOrThrow(str4);
      ArrayList localArrayList = new java/util/ArrayList;
      int n = paramString.getCount();
      localArrayList.<init>(n);
      for (;;)
      {
        boolean bool1 = paramString.moveToNext();
        if (!bool1) {
          break;
        }
        InsightState localInsightState = new com/truecaller/insights/models/InsightState;
        localInsightState.<init>();
        Object localObject2 = paramString.getString(i);
        localInsightState.setOwner((String)localObject2);
        boolean bool2 = paramString.isNull(j);
        Long localLong = null;
        if (bool2)
        {
          bool2 = false;
          localObject2 = null;
        }
        else
        {
          long l1 = paramString.getLong(j);
          localObject2 = Long.valueOf(l1);
        }
        localObject2 = a.a((Long)localObject2);
        localInsightState.setLastUpdatedAt((Date)localObject2);
        localObject2 = paramString.getString(k);
        localInsightState.setLastUpdatedData((String)localObject2);
        bool2 = paramString.isNull(m);
        if (!bool2)
        {
          long l2 = paramString.getLong(m);
          localLong = Long.valueOf(l2);
        }
        localObject2 = a.a(localLong);
        localInsightState.setCreatedAt((Date)localObject2);
        localArrayList.add(localInsightState);
      }
      return localArrayList;
    }
    finally
    {
      paramString.close();
      locali.b();
    }
  }
  
  public final void a(InsightState paramInsightState)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = b;
      ((c)localObject).a(paramInsightState);
      paramInsightState = a;
      paramInsightState.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */