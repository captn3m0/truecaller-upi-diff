package com.truecaller.insights.database.a;

import android.arch.persistence.room.c;
import android.arch.persistence.room.f;

final class d$3
  extends c
{
  d$3(d paramd, f paramf)
  {
    super(paramf);
  }
  
  public final String a()
  {
    return "INSERT OR REPLACE INTO `sms_backup_table`(`id`,`messageID`,`address`,`message`,`date`,`parseFailed`,`errorMessage`,`retryCount`,`created_at`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?)";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.a.d.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */