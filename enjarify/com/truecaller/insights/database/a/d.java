package com.truecaller.insights.database.a;

import android.arch.persistence.db.e;
import android.arch.persistence.room.i;
import android.database.Cursor;
import com.truecaller.insights.models.ParsedDataObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class d
  implements c
{
  private final android.arch.persistence.room.f a;
  private final android.arch.persistence.room.c b;
  private final com.truecaller.insights.d.a c;
  private final android.arch.persistence.room.c d;
  private final android.arch.persistence.room.c e;
  
  public d(android.arch.persistence.room.f paramf)
  {
    Object localObject = new com/truecaller/insights/d/a;
    ((com.truecaller.insights.d.a)localObject).<init>();
    c = ((com.truecaller.insights.d.a)localObject);
    a = paramf;
    localObject = new com/truecaller/insights/database/a/d$1;
    ((d.1)localObject).<init>(this, paramf);
    b = ((android.arch.persistence.room.c)localObject);
    localObject = new com/truecaller/insights/database/a/d$2;
    ((d.2)localObject).<init>(this, paramf);
    d = ((android.arch.persistence.room.c)localObject);
    localObject = new com/truecaller/insights/database/a/d$3;
    ((d.3)localObject).<init>(this, paramf);
    e = ((android.arch.persistence.room.c)localObject);
  }
  
  public final ParsedDataObject a(long paramLong)
  {
    int i = 1;
    Object localObject1 = i.a("SELECT * FROM parsed_data_object_table where messageID is ?", i);
    ((i)localObject1).a(i, paramLong);
    Object localObject2 = this;
    Cursor localCursor = a.a((e)localObject1);
    Object localObject3 = "id";
    Object localObject6;
    try
    {
      int j = localCursor.getColumnIndexOrThrow((String)localObject3);
      String str1 = "messageID";
      int k = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "d";
      int m = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "k";
      int n = localCursor.getColumnIndexOrThrow(str3);
      String str4 = "p";
      int i1 = localCursor.getColumnIndexOrThrow(str4);
      String str5 = "c";
      int i2 = localCursor.getColumnIndexOrThrow(str5);
      String str6 = "o";
      int i3 = localCursor.getColumnIndexOrThrow(str6);
      String str7 = "f";
      int i4 = localCursor.getColumnIndexOrThrow(str7);
      String str8 = "g";
      int i5 = localCursor.getColumnIndexOrThrow(str8);
      String str9 = "s";
      int i6 = localCursor.getColumnIndexOrThrow(str9);
      String str10 = "val1";
      int i7 = localCursor.getColumnIndexOrThrow(str10);
      String str11 = "val2";
      int i8 = localCursor.getColumnIndexOrThrow(str11);
      String str12 = "val3";
      i = localCursor.getColumnIndexOrThrow(str12);
      localObject2 = "val4";
      int i9 = localCursor.getColumnIndexOrThrow((String)localObject2);
      localObject6 = localObject1;
      localObject1 = "val5";
      try
      {
        int i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i11 = i10;
        localObject1 = "datetime";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i12 = i10;
        localObject1 = "address";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i13 = i10;
        localObject1 = "msgdatetime";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i14 = i10;
        localObject1 = "date";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i15 = i10;
        localObject1 = "msgdate";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i16 = i10;
        localObject1 = "dff_val1";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i17 = i10;
        localObject1 = "dff_val2";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i18 = i10;
        localObject1 = "dff_val3";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i19 = i10;
        localObject1 = "dff_val4";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i20 = i10;
        localObject1 = "dff_val5";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i21 = i10;
        localObject1 = "active";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i22 = i10;
        localObject1 = "state";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i23 = i10;
        localObject1 = "created_at";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i24 = localCursor.moveToFirst();
        Object localObject7 = null;
        if (i24 != 0)
        {
          i24 = i10;
          localObject1 = new com/truecaller/insights/models/ParsedDataObject;
          ((ParsedDataObject)localObject1).<init>();
          j = localCursor.getInt(j);
          ((ParsedDataObject)localObject1).setId(j);
          long l = localCursor.getLong(k);
          ((ParsedDataObject)localObject1).setMessageID(l);
          localObject3 = localCursor.getString(m);
          ((ParsedDataObject)localObject1).setD((String)localObject3);
          localObject3 = localCursor.getString(n);
          ((ParsedDataObject)localObject1).setK((String)localObject3);
          localObject3 = localCursor.getString(i1);
          ((ParsedDataObject)localObject1).setP((String)localObject3);
          localObject3 = localCursor.getString(i2);
          ((ParsedDataObject)localObject1).setC((String)localObject3);
          localObject3 = localCursor.getString(i3);
          ((ParsedDataObject)localObject1).setO((String)localObject3);
          localObject3 = localCursor.getString(i4);
          ((ParsedDataObject)localObject1).setF((String)localObject3);
          localObject3 = localCursor.getString(i5);
          ((ParsedDataObject)localObject1).setG((String)localObject3);
          localObject3 = localCursor.getString(i6);
          ((ParsedDataObject)localObject1).setS((String)localObject3);
          localObject3 = localCursor.getString(i7);
          ((ParsedDataObject)localObject1).setVal1((String)localObject3);
          localObject3 = localCursor.getString(i8);
          ((ParsedDataObject)localObject1).setVal2((String)localObject3);
          j = i;
          localObject3 = localCursor.getString(i);
          ((ParsedDataObject)localObject1).setVal3((String)localObject3);
          localObject3 = localCursor.getString(i9);
          ((ParsedDataObject)localObject1).setVal4((String)localObject3);
          j = i11;
          localObject3 = localCursor.getString(i11);
          ((ParsedDataObject)localObject1).setVal5((String)localObject3);
          j = i12;
          localObject3 = localCursor.getString(i12);
          ((ParsedDataObject)localObject1).setDatetime((String)localObject3);
          j = i13;
          localObject3 = localCursor.getString(i13);
          ((ParsedDataObject)localObject1).setAddress((String)localObject3);
          j = i14;
          localObject3 = localCursor.getString(i14);
          ((ParsedDataObject)localObject1).setMsgdatetime((String)localObject3);
          j = i15;
          localObject3 = localCursor.getString(i15);
          ((ParsedDataObject)localObject1).setDate((String)localObject3);
          j = i16;
          localObject3 = localCursor.getString(i16);
          ((ParsedDataObject)localObject1).setMsgdate((String)localObject3);
          j = i17;
          localObject3 = localCursor.getString(i17);
          ((ParsedDataObject)localObject1).setDiffVal1((String)localObject3);
          j = i18;
          localObject3 = localCursor.getString(i18);
          ((ParsedDataObject)localObject1).setDiffVal2((String)localObject3);
          j = i19;
          localObject3 = localCursor.getString(i19);
          ((ParsedDataObject)localObject1).setDiffVal3((String)localObject3);
          j = i20;
          localObject3 = localCursor.getString(i20);
          ((ParsedDataObject)localObject1).setDiffVal4((String)localObject3);
          j = i21;
          localObject3 = localCursor.getString(i21);
          ((ParsedDataObject)localObject1).setDiffVal5((String)localObject3);
          j = i22;
          j = localCursor.getInt(i22);
          if (j != 0)
          {
            i = 1;
          }
          else
          {
            i = 0;
            str12 = null;
          }
          ((ParsedDataObject)localObject1).setActive(i);
          j = i23;
          localObject3 = localCursor.getString(i23);
          ((ParsedDataObject)localObject1).setState((String)localObject3);
          j = i24;
          boolean bool = localCursor.isNull(i24);
          if (!bool)
          {
            l = localCursor.getLong(i24);
            localObject7 = Long.valueOf(l);
          }
          localObject3 = com.truecaller.insights.d.a.a((Long)localObject7);
          ((ParsedDataObject)localObject1).setCreatedAt((Date)localObject3);
          localObject7 = localObject1;
        }
        localCursor.close();
        ((i)localObject6).b();
        return (ParsedDataObject)localObject7;
      }
      finally {}
      localCursor.close();
    }
    finally
    {
      localObject6 = localObject1;
    }
    ((i)localObject6).b();
    throw ((Throwable)localObject5);
  }
  
  public final List a(Date paramDate)
  {
    int i = 1;
    Object localObject1 = i.a("SELECT * FROM parsed_data_object_table WHERE created_at > ? ORDER BY created_at", i);
    Object localObject2 = com.truecaller.insights.d.a.a(paramDate);
    Object localObject5;
    if (localObject2 == null)
    {
      localObject2 = e;
      localObject2[i] = i;
      localObject5 = this;
    }
    else
    {
      long l1 = ((Long)localObject2).longValue();
      ((i)localObject1).a(i, l1);
      localObject5 = this;
    }
    Cursor localCursor = a.a((e)localObject1);
    localObject2 = "id";
    Object localObject6;
    try
    {
      int j = localCursor.getColumnIndexOrThrow((String)localObject2);
      String str1 = "messageID";
      int k = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "d";
      int m = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "k";
      int n = localCursor.getColumnIndexOrThrow(str3);
      String str4 = "p";
      int i1 = localCursor.getColumnIndexOrThrow(str4);
      String str5 = "c";
      int i2 = localCursor.getColumnIndexOrThrow(str5);
      String str6 = "o";
      int i3 = localCursor.getColumnIndexOrThrow(str6);
      String str7 = "f";
      int i4 = localCursor.getColumnIndexOrThrow(str7);
      String str8 = "g";
      int i5 = localCursor.getColumnIndexOrThrow(str8);
      String str9 = "s";
      int i6 = localCursor.getColumnIndexOrThrow(str9);
      String str10 = "val1";
      int i7 = localCursor.getColumnIndexOrThrow(str10);
      String str11 = "val2";
      int i8 = localCursor.getColumnIndexOrThrow(str11);
      String str12 = "val3";
      i = localCursor.getColumnIndexOrThrow(str12);
      localObject5 = "val4";
      int i9 = localCursor.getColumnIndexOrThrow((String)localObject5);
      localObject6 = localObject1;
      localObject1 = "val5";
      try
      {
        int i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i11 = i10;
        localObject1 = "datetime";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i12 = i10;
        localObject1 = "address";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i13 = i10;
        localObject1 = "msgdatetime";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i14 = i10;
        localObject1 = "date";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i15 = i10;
        localObject1 = "msgdate";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i16 = i10;
        localObject1 = "dff_val1";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i17 = i10;
        localObject1 = "dff_val2";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i18 = i10;
        localObject1 = "dff_val3";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i19 = i10;
        localObject1 = "dff_val4";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i20 = i10;
        localObject1 = "dff_val5";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i21 = i10;
        localObject1 = "active";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i22 = i10;
        localObject1 = "state";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i23 = i10;
        localObject1 = "created_at";
        i10 = localCursor.getColumnIndexOrThrow((String)localObject1);
        int i24 = i10;
        localObject1 = new java/util/ArrayList;
        int i25 = i9;
        i9 = localCursor.getCount();
        ((ArrayList)localObject1).<init>(i9);
        for (;;)
        {
          boolean bool1 = localCursor.moveToNext();
          if (!bool1) {
            break;
          }
          localObject5 = new com/truecaller/insights/models/ParsedDataObject;
          ((ParsedDataObject)localObject5).<init>();
          Object localObject7 = localObject1;
          i10 = localCursor.getInt(j);
          ((ParsedDataObject)localObject5).setId(i10);
          i10 = j;
          int i26 = i;
          long l2 = localCursor.getLong(k);
          ((ParsedDataObject)localObject5).setMessageID(l2);
          localObject2 = localCursor.getString(m);
          ((ParsedDataObject)localObject5).setD((String)localObject2);
          localObject2 = localCursor.getString(n);
          ((ParsedDataObject)localObject5).setK((String)localObject2);
          localObject2 = localCursor.getString(i1);
          ((ParsedDataObject)localObject5).setP((String)localObject2);
          localObject2 = localCursor.getString(i2);
          ((ParsedDataObject)localObject5).setC((String)localObject2);
          localObject2 = localCursor.getString(i3);
          ((ParsedDataObject)localObject5).setO((String)localObject2);
          localObject2 = localCursor.getString(i4);
          ((ParsedDataObject)localObject5).setF((String)localObject2);
          localObject2 = localCursor.getString(i5);
          ((ParsedDataObject)localObject5).setG((String)localObject2);
          localObject2 = localCursor.getString(i6);
          ((ParsedDataObject)localObject5).setS((String)localObject2);
          localObject2 = localCursor.getString(i7);
          ((ParsedDataObject)localObject5).setVal1((String)localObject2);
          localObject2 = localCursor.getString(i8);
          ((ParsedDataObject)localObject5).setVal2((String)localObject2);
          j = i;
          str12 = localCursor.getString(i);
          ((ParsedDataObject)localObject5).setVal3(str12);
          i = i25;
          localObject2 = localCursor.getString(i25);
          ((ParsedDataObject)localObject5).setVal4((String)localObject2);
          j = i11;
          str12 = localCursor.getString(i11);
          ((ParsedDataObject)localObject5).setVal5(str12);
          i = i12;
          localObject2 = localCursor.getString(i12);
          ((ParsedDataObject)localObject5).setDatetime((String)localObject2);
          j = i13;
          str12 = localCursor.getString(i13);
          ((ParsedDataObject)localObject5).setAddress(str12);
          i = i14;
          localObject2 = localCursor.getString(i14);
          ((ParsedDataObject)localObject5).setMsgdatetime((String)localObject2);
          j = i15;
          str12 = localCursor.getString(i15);
          ((ParsedDataObject)localObject5).setDate(str12);
          i = i16;
          localObject2 = localCursor.getString(i16);
          ((ParsedDataObject)localObject5).setMsgdate((String)localObject2);
          j = i17;
          str12 = localCursor.getString(i17);
          ((ParsedDataObject)localObject5).setDiffVal1(str12);
          i = i18;
          localObject2 = localCursor.getString(i18);
          ((ParsedDataObject)localObject5).setDiffVal2((String)localObject2);
          j = i19;
          str12 = localCursor.getString(i19);
          ((ParsedDataObject)localObject5).setDiffVal3(str12);
          i = i20;
          localObject2 = localCursor.getString(i20);
          ((ParsedDataObject)localObject5).setDiffVal4((String)localObject2);
          j = i21;
          str12 = localCursor.getString(i21);
          ((ParsedDataObject)localObject5).setDiffVal5(str12);
          i = i22;
          i21 = localCursor.getInt(i22);
          if (i21 != 0)
          {
            i22 = j;
            j = 1;
          }
          else
          {
            i21 = 0;
            i22 = j;
            j = 0;
            localObject2 = null;
          }
          ((ParsedDataObject)localObject5).setActive(j);
          i21 = i;
          j = i23;
          str12 = localCursor.getString(i23);
          ((ParsedDataObject)localObject5).setState(str12);
          i = i24;
          boolean bool2 = localCursor.isNull(i24);
          Long localLong;
          if (bool2)
          {
            bool2 = false;
            localLong = null;
            i24 = j;
          }
          else
          {
            long l3 = localCursor.getLong(i24);
            localLong = Long.valueOf(l3);
            i24 = j;
          }
          localObject2 = com.truecaller.insights.d.a.a(localLong);
          ((ParsedDataObject)localObject5).setCreatedAt((Date)localObject2);
          localObject2 = localObject7;
          ((List)localObject7).add(localObject5);
          bool2 = i24;
          i24 = i;
          i = i26;
          localObject1 = localObject7;
          j = i10;
          int i27 = i22;
          i22 = i21;
          i21 = i27;
        }
        localObject2 = localObject1;
        localCursor.close();
        ((i)localObject6).b();
        return (List)localObject1;
      }
      finally {}
      localCursor.close();
    }
    finally
    {
      localObject6 = localObject1;
    }
    ((i)localObject6).b();
    throw ((Throwable)localObject4);
  }
  
  public final List a(Date paramDate, List paramList)
  {
    Object localObject1 = android.arch.persistence.room.b.a.a();
    ((StringBuilder)localObject1).append("SELECT * FROM parsed_data_object_table WHERE created_at > ");
    ((StringBuilder)localObject1).append("?");
    ((StringBuilder)localObject1).append(" and state NOT IN (");
    int i = paramList.size();
    android.arch.persistence.room.b.a.a((StringBuilder)localObject1, i);
    String str1 = ") ORDER BY created_at";
    ((StringBuilder)localObject1).append(str1);
    localObject1 = ((StringBuilder)localObject1).toString();
    int j = 1;
    i += j;
    Object localObject4 = i.a((String)localObject1, i);
    localObject1 = com.truecaller.insights.d.a.a(paramDate);
    if (localObject1 == null)
    {
      localObject1 = e;
      localObject1[j] = j;
    }
    else
    {
      long l1 = ((Long)localObject1).longValue();
      ((i)localObject4).a(j, l1);
    }
    int k = 2;
    Object localObject5 = paramList.iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject5).hasNext();
      if (!bool1) {
        break;
      }
      localObject6 = (String)((Iterator)localObject5).next();
      if (localObject6 == null)
      {
        localObject6 = e;
        localObject6[k] = j;
      }
      else
      {
        ((i)localObject4).a(k, (String)localObject6);
      }
      k += 1;
    }
    Object localObject6 = this;
    localObject5 = a.a((e)localObject4);
    localObject1 = "id";
    Object localObject7;
    try
    {
      k = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject1);
      String str2 = "messageID";
      int n = ((Cursor)localObject5).getColumnIndexOrThrow(str2);
      String str3 = "d";
      int i1 = ((Cursor)localObject5).getColumnIndexOrThrow(str3);
      String str4 = "k";
      int i2 = ((Cursor)localObject5).getColumnIndexOrThrow(str4);
      String str5 = "p";
      int i3 = ((Cursor)localObject5).getColumnIndexOrThrow(str5);
      String str6 = "c";
      int i4 = ((Cursor)localObject5).getColumnIndexOrThrow(str6);
      String str7 = "o";
      int i5 = ((Cursor)localObject5).getColumnIndexOrThrow(str7);
      String str8 = "f";
      int i6 = ((Cursor)localObject5).getColumnIndexOrThrow(str8);
      String str9 = "g";
      int i7 = ((Cursor)localObject5).getColumnIndexOrThrow(str9);
      String str10 = "s";
      int i8 = ((Cursor)localObject5).getColumnIndexOrThrow(str10);
      String str11 = "val1";
      int i9 = ((Cursor)localObject5).getColumnIndexOrThrow(str11);
      String str12 = "val2";
      int i10 = ((Cursor)localObject5).getColumnIndexOrThrow(str12);
      str1 = "val3";
      j = ((Cursor)localObject5).getColumnIndexOrThrow(str1);
      localObject6 = "val4";
      int m = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject6);
      localObject7 = localObject4;
      localObject4 = "val5";
      try
      {
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i11 = i;
        localObject4 = "datetime";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i12 = i;
        localObject4 = "address";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i13 = i;
        localObject4 = "msgdatetime";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i14 = i;
        localObject4 = "date";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i15 = i;
        localObject4 = "msgdate";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i16 = i;
        localObject4 = "dff_val1";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i17 = i;
        localObject4 = "dff_val2";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i18 = i;
        localObject4 = "dff_val3";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i19 = i;
        localObject4 = "dff_val4";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i20 = i;
        localObject4 = "dff_val5";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i21 = i;
        localObject4 = "active";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i22 = i;
        localObject4 = "state";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i23 = i;
        localObject4 = "created_at";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i24 = i;
        localObject4 = new java/util/ArrayList;
        int i25 = m;
        m = ((Cursor)localObject5).getCount();
        ((ArrayList)localObject4).<init>(m);
        for (;;)
        {
          boolean bool2 = ((Cursor)localObject5).moveToNext();
          if (!bool2) {
            break;
          }
          localObject6 = new com/truecaller/insights/models/ParsedDataObject;
          ((ParsedDataObject)localObject6).<init>();
          Object localObject8 = localObject4;
          i = ((Cursor)localObject5).getInt(k);
          ((ParsedDataObject)localObject6).setId(i);
          int i26 = k;
          long l2 = ((Cursor)localObject5).getLong(n);
          ((ParsedDataObject)localObject6).setMessageID(l2);
          localObject1 = ((Cursor)localObject5).getString(i1);
          ((ParsedDataObject)localObject6).setD((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i2);
          ((ParsedDataObject)localObject6).setK((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i3);
          ((ParsedDataObject)localObject6).setP((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i4);
          ((ParsedDataObject)localObject6).setC((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i5);
          ((ParsedDataObject)localObject6).setO((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i6);
          ((ParsedDataObject)localObject6).setF((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i7);
          ((ParsedDataObject)localObject6).setG((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i8);
          ((ParsedDataObject)localObject6).setS((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i9);
          ((ParsedDataObject)localObject6).setVal1((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i10);
          ((ParsedDataObject)localObject6).setVal2((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(j);
          ((ParsedDataObject)localObject6).setVal3((String)localObject1);
          k = i25;
          localObject4 = ((Cursor)localObject5).getString(i25);
          ((ParsedDataObject)localObject6).setVal4((String)localObject4);
          i = i11;
          localObject1 = ((Cursor)localObject5).getString(i11);
          ((ParsedDataObject)localObject6).setVal5((String)localObject1);
          k = i12;
          localObject4 = ((Cursor)localObject5).getString(i12);
          ((ParsedDataObject)localObject6).setDatetime((String)localObject4);
          i = i13;
          localObject1 = ((Cursor)localObject5).getString(i13);
          ((ParsedDataObject)localObject6).setAddress((String)localObject1);
          k = i14;
          localObject4 = ((Cursor)localObject5).getString(i14);
          ((ParsedDataObject)localObject6).setMsgdatetime((String)localObject4);
          i = i15;
          localObject1 = ((Cursor)localObject5).getString(i15);
          ((ParsedDataObject)localObject6).setDate((String)localObject1);
          k = i16;
          localObject4 = ((Cursor)localObject5).getString(i16);
          ((ParsedDataObject)localObject6).setMsgdate((String)localObject4);
          i = i17;
          localObject1 = ((Cursor)localObject5).getString(i17);
          ((ParsedDataObject)localObject6).setDiffVal1((String)localObject1);
          k = i18;
          localObject4 = ((Cursor)localObject5).getString(i18);
          ((ParsedDataObject)localObject6).setDiffVal2((String)localObject4);
          i = i19;
          localObject1 = ((Cursor)localObject5).getString(i19);
          ((ParsedDataObject)localObject6).setDiffVal3((String)localObject1);
          k = i20;
          localObject4 = ((Cursor)localObject5).getString(i20);
          ((ParsedDataObject)localObject6).setDiffVal4((String)localObject4);
          i = i21;
          localObject1 = ((Cursor)localObject5).getString(i21);
          ((ParsedDataObject)localObject6).setDiffVal5((String)localObject1);
          k = i22;
          i21 = ((Cursor)localObject5).getInt(i22);
          if (i21 != 0)
          {
            k = 1;
          }
          else
          {
            i21 = 0;
            k = 0;
            localObject1 = null;
          }
          ((ParsedDataObject)localObject6).setActive(k);
          i21 = i;
          k = i23;
          localObject4 = ((Cursor)localObject5).getString(i23);
          ((ParsedDataObject)localObject6).setState((String)localObject4);
          i = i24;
          boolean bool3 = ((Cursor)localObject5).isNull(i24);
          Long localLong;
          if (bool3)
          {
            bool3 = false;
            localLong = null;
            i24 = k;
          }
          else
          {
            long l3 = ((Cursor)localObject5).getLong(i24);
            localLong = Long.valueOf(l3);
            i24 = k;
          }
          localObject1 = com.truecaller.insights.d.a.a(localLong);
          ((ParsedDataObject)localObject6).setCreatedAt((Date)localObject1);
          localObject1 = localObject8;
          ((List)localObject8).add(localObject6);
          bool3 = i24;
          i24 = i;
          localObject4 = localObject8;
          k = i26;
        }
        localObject1 = localObject4;
        ((Cursor)localObject5).close();
        ((i)localObject7).b();
        return (List)localObject4;
      }
      finally {}
      ((Cursor)localObject5).close();
    }
    finally
    {
      localObject7 = localObject4;
    }
    ((i)localObject7).b();
    throw ((Throwable)localObject3);
  }
  
  public final void a(List paramList)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = b;
      ((android.arch.persistence.room.c)localObject).a(paramList);
      paramList = a;
      paramList.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
  
  public final void a(Set paramSet)
  {
    Object localObject = android.arch.persistence.room.b.a.a();
    ((StringBuilder)localObject).append("UPDATE parsed_data_object_table SET active = 0 WHERE messageID in (");
    int i = paramSet.size();
    android.arch.persistence.room.b.a.a((StringBuilder)localObject, i);
    ((StringBuilder)localObject).append(")");
    localObject = ((StringBuilder)localObject).toString();
    android.arch.persistence.room.f localf = a;
    localObject = localf.a((String)localObject);
    paramSet = paramSet.iterator();
    i = 1;
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      Long localLong = (Long)paramSet.next();
      if (localLong == null)
      {
        ((android.arch.persistence.db.f)localObject).a(i);
      }
      else
      {
        long l = localLong.longValue();
        ((android.arch.persistence.db.f)localObject).a(i, l);
      }
      i += 1;
    }
    paramSet = a;
    paramSet.d();
    try
    {
      ((android.arch.persistence.db.f)localObject).a();
      paramSet = a;
      paramSet.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
  
  public final List b(Set paramSet)
  {
    Object localObject1 = android.arch.persistence.room.b.a.a();
    ((StringBuilder)localObject1).append("SELECT * FROM parsed_data_object_table where messageID in (");
    int i = paramSet.size();
    android.arch.persistence.room.b.a.a((StringBuilder)localObject1, i);
    ((StringBuilder)localObject1).append(")");
    localObject1 = ((StringBuilder)localObject1).toString();
    int j = 0;
    String str1 = null;
    i += 0;
    Object localObject4 = i.a((String)localObject1, i);
    localObject1 = paramSet.iterator();
    int k = 1;
    int m = 1;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject5 = (Long)((Iterator)localObject1).next();
      if (localObject5 == null)
      {
        localObject5 = e;
        localObject5[m] = k;
      }
      else
      {
        long l1 = ((Long)localObject5).longValue();
        ((i)localObject4).a(m, l1);
      }
      m += 1;
    }
    Object localObject6 = this;
    Object localObject5 = a.a((e)localObject4);
    localObject1 = "id";
    Object localObject7;
    try
    {
      int n = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject1);
      String str2 = "messageID";
      int i1 = ((Cursor)localObject5).getColumnIndexOrThrow(str2);
      String str3 = "d";
      int i2 = ((Cursor)localObject5).getColumnIndexOrThrow(str3);
      String str4 = "k";
      int i3 = ((Cursor)localObject5).getColumnIndexOrThrow(str4);
      String str5 = "p";
      int i4 = ((Cursor)localObject5).getColumnIndexOrThrow(str5);
      String str6 = "c";
      int i5 = ((Cursor)localObject5).getColumnIndexOrThrow(str6);
      String str7 = "o";
      int i6 = ((Cursor)localObject5).getColumnIndexOrThrow(str7);
      String str8 = "f";
      int i7 = ((Cursor)localObject5).getColumnIndexOrThrow(str8);
      String str9 = "g";
      int i8 = ((Cursor)localObject5).getColumnIndexOrThrow(str9);
      String str10 = "s";
      int i9 = ((Cursor)localObject5).getColumnIndexOrThrow(str10);
      String str11 = "val1";
      int i10 = ((Cursor)localObject5).getColumnIndexOrThrow(str11);
      str1 = "val2";
      j = ((Cursor)localObject5).getColumnIndexOrThrow(str1);
      String str12 = "val3";
      k = ((Cursor)localObject5).getColumnIndexOrThrow(str12);
      localObject6 = "val4";
      m = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject6);
      localObject7 = localObject4;
      localObject4 = "val5";
      try
      {
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i11 = i;
        localObject4 = "datetime";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i12 = i;
        localObject4 = "address";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i13 = i;
        localObject4 = "msgdatetime";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i14 = i;
        localObject4 = "date";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i15 = i;
        localObject4 = "msgdate";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i16 = i;
        localObject4 = "dff_val1";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i17 = i;
        localObject4 = "dff_val2";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i18 = i;
        localObject4 = "dff_val3";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i19 = i;
        localObject4 = "dff_val4";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i20 = i;
        localObject4 = "dff_val5";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i21 = i;
        localObject4 = "active";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i22 = i;
        localObject4 = "state";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i23 = i;
        localObject4 = "created_at";
        i = ((Cursor)localObject5).getColumnIndexOrThrow((String)localObject4);
        int i24 = i;
        localObject4 = new java/util/ArrayList;
        int i25 = m;
        m = ((Cursor)localObject5).getCount();
        ((ArrayList)localObject4).<init>(m);
        for (;;)
        {
          boolean bool1 = ((Cursor)localObject5).moveToNext();
          if (!bool1) {
            break;
          }
          localObject6 = new com/truecaller/insights/models/ParsedDataObject;
          ((ParsedDataObject)localObject6).<init>();
          Object localObject8 = localObject4;
          i = ((Cursor)localObject5).getInt(n);
          ((ParsedDataObject)localObject6).setId(i);
          int i26 = n;
          long l2 = ((Cursor)localObject5).getLong(i1);
          ((ParsedDataObject)localObject6).setMessageID(l2);
          localObject1 = ((Cursor)localObject5).getString(i2);
          ((ParsedDataObject)localObject6).setD((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i3);
          ((ParsedDataObject)localObject6).setK((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i4);
          ((ParsedDataObject)localObject6).setP((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i5);
          ((ParsedDataObject)localObject6).setC((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i6);
          ((ParsedDataObject)localObject6).setO((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i7);
          ((ParsedDataObject)localObject6).setF((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i8);
          ((ParsedDataObject)localObject6).setG((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i9);
          ((ParsedDataObject)localObject6).setS((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(i10);
          ((ParsedDataObject)localObject6).setVal1((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(j);
          ((ParsedDataObject)localObject6).setVal2((String)localObject1);
          localObject1 = ((Cursor)localObject5).getString(k);
          ((ParsedDataObject)localObject6).setVal3((String)localObject1);
          n = i25;
          localObject4 = ((Cursor)localObject5).getString(i25);
          ((ParsedDataObject)localObject6).setVal4((String)localObject4);
          i = i11;
          localObject1 = ((Cursor)localObject5).getString(i11);
          ((ParsedDataObject)localObject6).setVal5((String)localObject1);
          n = i12;
          localObject4 = ((Cursor)localObject5).getString(i12);
          ((ParsedDataObject)localObject6).setDatetime((String)localObject4);
          i = i13;
          localObject1 = ((Cursor)localObject5).getString(i13);
          ((ParsedDataObject)localObject6).setAddress((String)localObject1);
          n = i14;
          localObject4 = ((Cursor)localObject5).getString(i14);
          ((ParsedDataObject)localObject6).setMsgdatetime((String)localObject4);
          i = i15;
          localObject1 = ((Cursor)localObject5).getString(i15);
          ((ParsedDataObject)localObject6).setDate((String)localObject1);
          n = i16;
          localObject4 = ((Cursor)localObject5).getString(i16);
          ((ParsedDataObject)localObject6).setMsgdate((String)localObject4);
          i = i17;
          localObject1 = ((Cursor)localObject5).getString(i17);
          ((ParsedDataObject)localObject6).setDiffVal1((String)localObject1);
          n = i18;
          localObject4 = ((Cursor)localObject5).getString(i18);
          ((ParsedDataObject)localObject6).setDiffVal2((String)localObject4);
          i = i19;
          localObject1 = ((Cursor)localObject5).getString(i19);
          ((ParsedDataObject)localObject6).setDiffVal3((String)localObject1);
          n = i20;
          localObject4 = ((Cursor)localObject5).getString(i20);
          ((ParsedDataObject)localObject6).setDiffVal4((String)localObject4);
          i = i21;
          localObject1 = ((Cursor)localObject5).getString(i21);
          ((ParsedDataObject)localObject6).setDiffVal5((String)localObject1);
          n = i22;
          i21 = ((Cursor)localObject5).getInt(i22);
          if (i21 != 0)
          {
            n = 1;
          }
          else
          {
            n = 0;
            localObject1 = null;
          }
          ((ParsedDataObject)localObject6).setActive(n);
          i21 = i;
          n = i23;
          localObject4 = ((Cursor)localObject5).getString(i23);
          ((ParsedDataObject)localObject6).setState((String)localObject4);
          i = i24;
          boolean bool3 = ((Cursor)localObject5).isNull(i24);
          Long localLong;
          if (bool3)
          {
            bool3 = false;
            localLong = null;
            i24 = n;
          }
          else
          {
            long l3 = ((Cursor)localObject5).getLong(i24);
            localLong = Long.valueOf(l3);
            i24 = n;
          }
          localObject1 = com.truecaller.insights.d.a.a(localLong);
          ((ParsedDataObject)localObject6).setCreatedAt((Date)localObject1);
          localObject1 = localObject8;
          ((List)localObject8).add(localObject6);
          bool3 = i24;
          i24 = i;
          localObject4 = localObject8;
          n = i26;
        }
        localObject1 = localObject4;
        ((Cursor)localObject5).close();
        ((i)localObject7).b();
        return (List)localObject4;
      }
      finally {}
      ((Cursor)localObject5).close();
    }
    finally
    {
      localObject7 = localObject4;
    }
    ((i)localObject7).b();
    throw ((Throwable)localObject3);
  }
  
  public final void b(List paramList)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = d;
      ((android.arch.persistence.room.c)localObject).a(paramList);
      paramList = a;
      paramList.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
  
  public final void c(List paramList)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = e;
      ((android.arch.persistence.room.c)localObject).a(paramList);
      paramList = a;
      paramList.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */