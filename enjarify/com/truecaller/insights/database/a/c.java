package com.truecaller.insights.database.a;

import com.truecaller.insights.models.ParsedDataObject;
import java.util.Date;
import java.util.List;
import java.util.Set;

public abstract interface c
{
  public abstract ParsedDataObject a(long paramLong);
  
  public abstract List a(Date paramDate);
  
  public abstract List a(Date paramDate, List paramList);
  
  public abstract void a(List paramList);
  
  public abstract void a(Set paramSet);
  
  public abstract List b(Set paramSet);
  
  public abstract void b(List paramList);
  
  public abstract void c(List paramList);
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */