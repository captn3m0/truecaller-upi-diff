package com.truecaller.insights.database.c;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.insights.models.LinkPruneMap;
import com.truecaller.insights.models.ParsedDataObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class b$h
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  b$h(b paramb, List paramList, ParsedDataObject paramParsedDataObject, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    h localh = new com/truecaller/insights/database/c/b$h;
    b localb = b;
    List localList = c;
    ParsedDataObject localParsedDataObject = d;
    localh.<init>(localb, localList, localParsedDataObject, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localh;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = new java/util/ArrayList;
        ((ArrayList)paramObject).<init>();
        paramObject = (List)paramObject;
        localObject1 = ((Iterable)c).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          Object localObject2 = (ParsedDataObject)((Iterator)localObject1).next();
          LinkPruneMap localLinkPruneMap = new com/truecaller/insights/models/LinkPruneMap;
          localLinkPruneMap.<init>();
          ParsedDataObject localParsedDataObject = d;
          int k = localParsedDataObject.getId();
          localLinkPruneMap.setParentId(k);
          int j = ((ParsedDataObject)localObject2).getId();
          localLinkPruneMap.setChildId(j);
          localObject2 = com.truecaller.insights.core.c.c.a;
          j = 2;
          localObject2 = com.truecaller.insights.core.c.c.a(j);
          localLinkPruneMap.setLinkType((String)localObject2);
          ((List)paramObject).add(localLinkPruneMap);
        }
        b.a.b((List)paramObject);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (h)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((h)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.c.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */