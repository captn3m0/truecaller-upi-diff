package com.truecaller.insights.database.c;

import c.d.f;
import c.g.a.m;
import com.truecaller.insights.database.a.e;
import com.truecaller.insights.models.InsightState;
import com.truecaller.insights.models.ParsedDataObject;
import java.util.Date;
import java.util.List;
import kotlinx.coroutines.g;

public final class b
  implements a
{
  final com.truecaller.insights.database.a.c a;
  final e b;
  private final com.truecaller.insights.database.a.a c;
  private final f d;
  
  public b(com.truecaller.insights.database.a.c paramc, e parame, com.truecaller.insights.database.a.a parama, f paramf)
  {
    a = paramc;
    b = parame;
    c = parama;
    d = paramf;
  }
  
  public final Object a(long paramLong, c.d.c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/insights/database/c/b$c;
    ((b.c)localObject).<init>(this, paramLong, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object a(InsightState paramInsightState, Date paramDate, String paramString, c.d.c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/insights/database/c/b$i;
    ((b.i)localObject).<init>(this, paramInsightState, paramString, paramDate, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object a(ParsedDataObject paramParsedDataObject, List paramList, c.d.c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/insights/database/c/b$h;
    ((b.h)localObject).<init>(this, paramList, paramParsedDataObject, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object a(String paramString, c.d.c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/insights/database/c/b$b;
    ((b.b)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object a(Date paramDate, c.d.c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/insights/database/c/b$e;
    ((b.e)localObject).<init>(this, paramDate, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object a(List paramList, c.d.c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/insights/database/c/b$d;
    ((b.d)localObject).<init>(this, paramList, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object b(Date paramDate, c.d.c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/insights/database/c/b$f;
    ((b.f)localObject).<init>(this, paramDate, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object b(List paramList, c.d.c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/insights/database/c/b$g;
    ((b.g)localObject).<init>(this, paramList, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final Object c(List paramList, c.d.c paramc)
  {
    f localf = d;
    Object localObject = new com/truecaller/insights/database/c/b$a;
    ((b.a)localObject).<init>(this, paramList, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */