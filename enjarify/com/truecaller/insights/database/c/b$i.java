package com.truecaller.insights.database.c;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.insights.database.a.e;
import com.truecaller.insights.models.InsightState;
import java.util.Date;
import kotlinx.coroutines.ag;

final class b$i
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag f;
  
  b$i(b paramb, InsightState paramInsightState, String paramString, Date paramDate, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    i locali = new com/truecaller/insights/database/c/b$i;
    b localb = b;
    InsightState localInsightState = c;
    String str = d;
    Date localDate = e;
    locali.<init>(localb, localInsightState, str, localDate, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locali;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = c;
        localObject = d;
        ((InsightState)paramObject).setLastUpdatedData((String)localObject);
        paramObject = c;
        localObject = e;
        ((InsightState)paramObject).setLastUpdatedAt((Date)localObject);
        paramObject = b.b;
        localObject = c;
        ((e)paramObject).a((InsightState)localObject);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (i)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((i)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.c.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */