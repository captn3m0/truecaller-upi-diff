package com.truecaller.insights.database.c;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.insights.database.a.e;
import com.truecaller.insights.models.InsightState;
import java.util.Collection;
import java.util.List;
import kotlinx.coroutines.ag;

final class b$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  b$b(b paramb, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/insights/database/c/b$b;
    b localb1 = b;
    String str = c;
    localb.<init>(localb1, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.b;
        localObject = c;
        paramObject = ((e)paramObject).a((String)localObject);
        localObject = paramObject;
        localObject = (Collection)paramObject;
        bool = ((Collection)localObject).isEmpty() ^ true;
        if (bool) {
          return ((List)paramObject).get(0);
        }
        paramObject = new com/truecaller/insights/models/InsightState;
        ((InsightState)paramObject).<init>();
        localObject = c;
        ((InsightState)paramObject).setOwner((String)localObject);
        ((InsightState)paramObject).setLastUpdatedData(null);
        return paramObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.c.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */