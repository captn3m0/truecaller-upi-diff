package com.truecaller.insights.d;

import java.util.Date;

public final class a
{
  public static Long a(Date paramDate)
  {
    if (paramDate != null) {
      return Long.valueOf(paramDate.getTime());
    }
    return null;
  }
  
  public static Date a(Long paramLong)
  {
    if (paramLong == null) {
      return null;
    }
    Date localDate = new java/util/Date;
    long l = paramLong.longValue();
    localDate.<init>(l);
    return localDate;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */