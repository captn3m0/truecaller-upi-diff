package com.truecaller.insights.d;

import android.content.Context;
import android.content.res.AssetManager;
import c.g.b.k;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public final class c
  implements b
{
  private final Context a;
  
  public c(Context paramContext)
  {
    a = paramContext;
  }
  
  public final BufferedReader a(String paramString)
  {
    k.b(paramString, "fileName");
    Object localObject1 = null;
    try
    {
      Object localObject2 = a;
      localObject2 = ((Context)localObject2).getAssets();
      paramString = ((AssetManager)localObject2).open(paramString);
      if (paramString != null)
      {
        localObject2 = new java/io/BufferedReader;
        Object localObject3 = new java/io/InputStreamReader;
        ((InputStreamReader)localObject3).<init>(paramString);
        localObject3 = (Reader)localObject3;
        ((BufferedReader)localObject2).<init>((Reader)localObject3);
        localObject1 = localObject2;
      }
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    return (BufferedReader)localObject1;
  }
  
  /* Error */
  public final String a(InputStream paramInputStream)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 52
    //   3: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: new 54	java/lang/StringBuilder
    //   9: astore_2
    //   10: aload_2
    //   11: sipush 16384
    //   14: invokespecial 58	java/lang/StringBuilder:<init>	(I)V
    //   17: iconst_0
    //   18: istore_3
    //   19: aconst_null
    //   20: astore 4
    //   22: new 40	java/io/InputStreamReader
    //   25: astore 5
    //   27: ldc 60
    //   29: astore 6
    //   31: aload 5
    //   33: aload_1
    //   34: aload 6
    //   36: invokespecial 63	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   39: new 38	java/io/BufferedReader
    //   42: astore_1
    //   43: aload 5
    //   45: astore 4
    //   47: aload 5
    //   49: checkcast 45	java/io/Reader
    //   52: astore 4
    //   54: aload_1
    //   55: aload 4
    //   57: invokespecial 48	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   60: aload_1
    //   61: invokevirtual 67	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   64: astore 4
    //   66: aload 4
    //   68: ifnull +28 -> 96
    //   71: aload_2
    //   72: aload 4
    //   74: invokevirtual 71	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: pop
    //   78: bipush 10
    //   80: istore_3
    //   81: aload_2
    //   82: iload_3
    //   83: invokevirtual 75	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   86: pop
    //   87: aload_1
    //   88: invokevirtual 67	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   91: astore 4
    //   93: goto -27 -> 66
    //   96: aload 5
    //   98: checkcast 77	java/io/Closeable
    //   101: astore 5
    //   103: aload_0
    //   104: aload 5
    //   106: invokevirtual 80	com/truecaller/insights/d/c:a	(Ljava/io/Closeable;)V
    //   109: goto +48 -> 157
    //   112: astore_1
    //   113: goto +15 -> 128
    //   116: pop
    //   117: aload 5
    //   119: astore 4
    //   121: goto +23 -> 144
    //   124: astore_1
    //   125: aconst_null
    //   126: astore 5
    //   128: aload 5
    //   130: checkcast 77	java/io/Closeable
    //   133: astore 5
    //   135: aload_0
    //   136: aload 5
    //   138: invokevirtual 80	com/truecaller/insights/d/c:a	(Ljava/io/Closeable;)V
    //   141: aload_1
    //   142: athrow
    //   143: pop
    //   144: aload 4
    //   146: checkcast 77	java/io/Closeable
    //   149: astore 4
    //   151: aload_0
    //   152: aload 4
    //   154: invokevirtual 80	com/truecaller/insights/d/c:a	(Ljava/io/Closeable;)V
    //   157: aload_2
    //   158: invokevirtual 83	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   161: astore_1
    //   162: aload_1
    //   163: ldc 85
    //   165: invokestatic 87	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   168: aload_1
    //   169: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	170	0	this	c
    //   0	170	1	paramInputStream	InputStream
    //   9	149	2	localStringBuilder	StringBuilder
    //   18	65	3	c	char
    //   20	133	4	localObject1	Object
    //   25	112	5	localObject2	Object
    //   29	6	6	str	String
    //   116	1	7	localIOException1	IOException
    //   143	1	8	localIOException2	IOException
    // Exception table:
    //   from	to	target	type
    //   39	42	112	finally
    //   47	52	112	finally
    //   55	60	112	finally
    //   60	64	112	finally
    //   72	78	112	finally
    //   82	87	112	finally
    //   87	91	112	finally
    //   39	42	116	java/io/IOException
    //   47	52	116	java/io/IOException
    //   55	60	116	java/io/IOException
    //   60	64	116	java/io/IOException
    //   72	78	116	java/io/IOException
    //   82	87	116	java/io/IOException
    //   87	91	116	java/io/IOException
    //   22	25	124	finally
    //   34	39	124	finally
    //   22	25	143	java/io/IOException
    //   34	39	143	java/io/IOException
  }
  
  public final void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
        return;
      }
      catch (IOException localIOException) {}
    }
  }
  
  public final InputStream b(String paramString)
  {
    Object localObject = "fileName";
    k.b(paramString, (String)localObject);
    try
    {
      localObject = a;
      localObject = ((Context)localObject).getAssets();
      return ((AssetManager)localObject).open(paramString);
    }
    catch (IOException localIOException) {}
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */