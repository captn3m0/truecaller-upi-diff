package com.truecaller.insights.models;

import c.g.b.k;
import com.truecaller.insights.core.c.c;
import java.util.Date;

public final class LinkPruneMap
{
  private int childId;
  private Date createdAt;
  private String linkType;
  private int parentId;
  
  public LinkPruneMap()
  {
    int i = -1;
    parentId = i;
    childId = i;
    Object localObject = c.a;
    localObject = c.a(0);
    linkType = ((String)localObject);
    localObject = new java/util/Date;
    ((Date)localObject).<init>();
    createdAt = ((Date)localObject);
  }
  
  public LinkPruneMap(ParsedDataObject paramParsedDataObject1, ParsedDataObject paramParsedDataObject2, String paramString)
  {
    int i = -1;
    parentId = i;
    childId = i;
    Object localObject = c.a;
    localObject = c.a(0);
    linkType = ((String)localObject);
    localObject = new java/util/Date;
    ((Date)localObject).<init>();
    createdAt = ((Date)localObject);
    int j = paramParsedDataObject1.getId();
    parentId = j;
    j = paramParsedDataObject2.getId();
    childId = j;
    linkType = paramString;
  }
  
  public final int getChildId()
  {
    return childId;
  }
  
  public final Date getCreatedAt()
  {
    return createdAt;
  }
  
  public final String getLinkType()
  {
    return linkType;
  }
  
  public final int getParentId()
  {
    return parentId;
  }
  
  public final void setChildId(int paramInt)
  {
    childId = paramInt;
  }
  
  public final void setCreatedAt(Date paramDate)
  {
    k.b(paramDate, "<set-?>");
    createdAt = paramDate;
  }
  
  public final void setLinkType(String paramString)
  {
    k.b(paramString, "<set-?>");
    linkType = paramString;
  }
  
  public final void setParentId(int paramInt)
  {
    parentId = paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.models.LinkPruneMap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */