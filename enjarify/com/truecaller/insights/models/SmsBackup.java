package com.truecaller.insights.models;

import c.g.b.k;
import java.util.Date;

public final class SmsBackup
{
  private String address = "";
  private Date createdAt;
  private Date date;
  private String errorMessage;
  private int id;
  private String message = "";
  private long messageID = -1;
  private boolean parseFailed;
  private int retryCount;
  
  public SmsBackup()
  {
    Date localDate = new java/util/Date;
    localDate.<init>();
    date = localDate;
    errorMessage = "";
    localDate = new java/util/Date;
    localDate.<init>();
    createdAt = localDate;
  }
  
  public SmsBackup(c paramc)
  {
    Object localObject = new java/util/Date;
    ((Date)localObject).<init>();
    date = ((Date)localObject);
    errorMessage = "";
    localObject = new java/util/Date;
    ((Date)localObject).<init>();
    createdAt = ((Date)localObject);
    long l = b;
    messageID = l;
    localObject = a;
    address = ((String)localObject);
    localObject = c;
    message = ((String)localObject);
    paramc = d;
    date = paramc;
  }
  
  public SmsBackup(c paramc, Throwable paramThrowable)
  {
    Object localObject = new java/util/Date;
    ((Date)localObject).<init>();
    date = ((Date)localObject);
    errorMessage = "";
    localObject = new java/util/Date;
    ((Date)localObject).<init>();
    createdAt = ((Date)localObject);
    long l = b;
    messageID = l;
    localObject = a;
    address = ((String)localObject);
    localObject = c;
    message = ((String)localObject);
    parseFailed = true;
    paramc = d;
    date = paramc;
    paramc = paramThrowable.getLocalizedMessage();
    k.a(paramc, "error.localizedMessage");
    errorMessage = paramc;
  }
  
  public final String getAddress()
  {
    return address;
  }
  
  public final Date getCreatedAt()
  {
    return createdAt;
  }
  
  public final Date getDate()
  {
    return date;
  }
  
  public final String getErrorMessage()
  {
    return errorMessage;
  }
  
  public final int getId()
  {
    return id;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final long getMessageID()
  {
    return messageID;
  }
  
  public final boolean getParseFailed()
  {
    return parseFailed;
  }
  
  public final int getRetryCount()
  {
    return retryCount;
  }
  
  public final void setAddress(String paramString)
  {
    k.b(paramString, "<set-?>");
    address = paramString;
  }
  
  public final void setCreatedAt(Date paramDate)
  {
    k.b(paramDate, "<set-?>");
    createdAt = paramDate;
  }
  
  public final void setDate(Date paramDate)
  {
    k.b(paramDate, "<set-?>");
    date = paramDate;
  }
  
  public final void setErrorMessage(String paramString)
  {
    k.b(paramString, "<set-?>");
    errorMessage = paramString;
  }
  
  public final void setId(int paramInt)
  {
    id = paramInt;
  }
  
  public final void setMessage(String paramString)
  {
    k.b(paramString, "<set-?>");
    message = paramString;
  }
  
  public final void setMessageID(long paramLong)
  {
    messageID = paramLong;
  }
  
  public final void setParseFailed(boolean paramBoolean)
  {
    parseFailed = paramBoolean;
  }
  
  public final void setRetryCount(int paramInt)
  {
    retryCount = paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.models.SmsBackup
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */