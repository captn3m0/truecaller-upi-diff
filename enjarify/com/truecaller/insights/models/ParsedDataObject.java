package com.truecaller.insights.models;

import c.g.b.k;
import java.util.Date;

public final class ParsedDataObject
{
  private boolean active = true;
  private String address = "";
  private String c = "";
  private Date createdAt;
  private String d = "";
  private String date = "";
  private String datetime = "";
  private String diffVal1 = "";
  private String diffVal2 = "";
  private String diffVal3 = "";
  private String diffVal4 = "";
  private String diffVal5 = "";
  private String f = "";
  private String g = "";
  private int id;
  private String k = "";
  private long messageID = -1;
  private String msgdate = "";
  private String msgdatetime = "";
  private String o = "";
  private String p = "";
  private String s = "";
  private String state = "";
  private String val1 = "";
  private String val2 = "";
  private String val3 = "";
  private String val4 = "";
  private String val5 = "";
  
  public ParsedDataObject()
  {
    Date localDate = new java/util/Date;
    localDate.<init>();
    createdAt = localDate;
  }
  
  public final boolean getActive()
  {
    return active;
  }
  
  public final String getAddress()
  {
    return address;
  }
  
  public final String getC()
  {
    return c;
  }
  
  public final Date getCreatedAt()
  {
    return createdAt;
  }
  
  public final String getD()
  {
    return d;
  }
  
  public final String getDate()
  {
    return date;
  }
  
  public final String getDatetime()
  {
    return datetime;
  }
  
  public final String getDiffVal1()
  {
    return diffVal1;
  }
  
  public final String getDiffVal2()
  {
    return diffVal2;
  }
  
  public final String getDiffVal3()
  {
    return diffVal3;
  }
  
  public final String getDiffVal4()
  {
    return diffVal4;
  }
  
  public final String getDiffVal5()
  {
    return diffVal5;
  }
  
  public final String getF()
  {
    return f;
  }
  
  public final String getG()
  {
    return g;
  }
  
  public final int getId()
  {
    return id;
  }
  
  public final String getK()
  {
    return k;
  }
  
  public final long getMessageID()
  {
    return messageID;
  }
  
  public final String getMsgdate()
  {
    return msgdate;
  }
  
  public final String getMsgdatetime()
  {
    return msgdatetime;
  }
  
  public final String getO()
  {
    return o;
  }
  
  public final String getP()
  {
    return p;
  }
  
  public final String getS()
  {
    return s;
  }
  
  public final String getState()
  {
    return state;
  }
  
  public final String getVal1()
  {
    return val1;
  }
  
  public final String getVal2()
  {
    return val2;
  }
  
  public final String getVal3()
  {
    return val3;
  }
  
  public final String getVal4()
  {
    return val4;
  }
  
  public final String getVal5()
  {
    return val5;
  }
  
  public final void setActive(boolean paramBoolean)
  {
    active = paramBoolean;
  }
  
  public final void setAddress(String paramString)
  {
    k.b(paramString, "<set-?>");
    address = paramString;
  }
  
  public final void setC(String paramString)
  {
    k.b(paramString, "<set-?>");
    c = paramString;
  }
  
  public final void setCreatedAt(Date paramDate)
  {
    k.b(paramDate, "<set-?>");
    createdAt = paramDate;
  }
  
  public final void setD(String paramString)
  {
    k.b(paramString, "<set-?>");
    d = paramString;
  }
  
  public final void setDate(String paramString)
  {
    k.b(paramString, "<set-?>");
    date = paramString;
  }
  
  public final void setDatetime(String paramString)
  {
    k.b(paramString, "<set-?>");
    datetime = paramString;
  }
  
  public final void setDiffVal1(String paramString)
  {
    k.b(paramString, "<set-?>");
    diffVal1 = paramString;
  }
  
  public final void setDiffVal2(String paramString)
  {
    k.b(paramString, "<set-?>");
    diffVal2 = paramString;
  }
  
  public final void setDiffVal3(String paramString)
  {
    k.b(paramString, "<set-?>");
    diffVal3 = paramString;
  }
  
  public final void setDiffVal4(String paramString)
  {
    k.b(paramString, "<set-?>");
    diffVal4 = paramString;
  }
  
  public final void setDiffVal5(String paramString)
  {
    k.b(paramString, "<set-?>");
    diffVal5 = paramString;
  }
  
  public final void setF(String paramString)
  {
    k.b(paramString, "<set-?>");
    f = paramString;
  }
  
  public final void setG(String paramString)
  {
    k.b(paramString, "<set-?>");
    g = paramString;
  }
  
  public final void setId(int paramInt)
  {
    id = paramInt;
  }
  
  public final void setK(String paramString)
  {
    k.b(paramString, "<set-?>");
    k = paramString;
  }
  
  public final void setMessageID(long paramLong)
  {
    messageID = paramLong;
  }
  
  public final void setMsgdate(String paramString)
  {
    k.b(paramString, "<set-?>");
    msgdate = paramString;
  }
  
  public final void setMsgdatetime(String paramString)
  {
    k.b(paramString, "<set-?>");
    msgdatetime = paramString;
  }
  
  public final void setO(String paramString)
  {
    k.b(paramString, "<set-?>");
    o = paramString;
  }
  
  public final void setP(String paramString)
  {
    k.b(paramString, "<set-?>");
    p = paramString;
  }
  
  public final void setS(String paramString)
  {
    k.b(paramString, "<set-?>");
    s = paramString;
  }
  
  public final void setState(String paramString)
  {
    k.b(paramString, "<set-?>");
    state = paramString;
  }
  
  public final void setVal1(String paramString)
  {
    k.b(paramString, "<set-?>");
    val1 = paramString;
  }
  
  public final void setVal2(String paramString)
  {
    k.b(paramString, "<set-?>");
    val2 = paramString;
  }
  
  public final void setVal3(String paramString)
  {
    k.b(paramString, "<set-?>");
    val3 = paramString;
  }
  
  public final void setVal4(String paramString)
  {
    k.b(paramString, "<set-?>");
    val4 = paramString;
  }
  
  public final void setVal5(String paramString)
  {
    k.b(paramString, "<set-?>");
    val5 = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.models.ParsedDataObject
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */