package com.truecaller.insights.models;

import c.g.b.k;
import c.l;
import java.util.Map;

public final class PdoBinderType$PdoBinder
  extends PdoBinderType
{
  private final Map GRM_BANK;
  private final Map GRM_BILL;
  private final Map GRM_DELIVERY;
  private final Map GRM_EVENT;
  private final Map GRM_NOTIF;
  private final Map GRM_OFFERS;
  private final Map GRM_OTP;
  private final Map GRM_TRAVEL;
  private final Map GRM_VOID;
  
  public PdoBinderType$PdoBinder(Map paramMap1, Map paramMap2, Map paramMap3, Map paramMap4, Map paramMap5, Map paramMap6, Map paramMap7, Map paramMap8, Map paramMap9)
  {
    super(null);
    GRM_BANK = paramMap1;
    GRM_EVENT = paramMap2;
    GRM_TRAVEL = paramMap3;
    GRM_BILL = paramMap4;
    GRM_OTP = paramMap5;
    GRM_OFFERS = paramMap6;
    GRM_NOTIF = paramMap7;
    GRM_DELIVERY = paramMap8;
    GRM_VOID = paramMap9;
  }
  
  public final Map component1()
  {
    return GRM_BANK;
  }
  
  public final Map component2()
  {
    return GRM_EVENT;
  }
  
  public final Map component3()
  {
    return GRM_TRAVEL;
  }
  
  public final Map component4()
  {
    return GRM_BILL;
  }
  
  public final Map component5()
  {
    return GRM_OTP;
  }
  
  public final Map component6()
  {
    return GRM_OFFERS;
  }
  
  public final Map component7()
  {
    return GRM_NOTIF;
  }
  
  public final Map component8()
  {
    return GRM_DELIVERY;
  }
  
  public final Map component9()
  {
    return GRM_VOID;
  }
  
  public final PdoBinder copy(Map paramMap1, Map paramMap2, Map paramMap3, Map paramMap4, Map paramMap5, Map paramMap6, Map paramMap7, Map paramMap8, Map paramMap9)
  {
    k.b(paramMap1, "GRM_BANK");
    k.b(paramMap2, "GRM_EVENT");
    k.b(paramMap3, "GRM_TRAVEL");
    k.b(paramMap4, "GRM_BILL");
    k.b(paramMap5, "GRM_OTP");
    k.b(paramMap6, "GRM_OFFERS");
    k.b(paramMap7, "GRM_NOTIF");
    k.b(paramMap8, "GRM_DELIVERY");
    k.b(paramMap9, "GRM_VOID");
    PdoBinder localPdoBinder = new com/truecaller/insights/models/PdoBinderType$PdoBinder;
    localPdoBinder.<init>(paramMap1, paramMap2, paramMap3, paramMap4, paramMap5, paramMap6, paramMap7, paramMap8, paramMap9);
    return localPdoBinder;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PdoBinder;
      if (bool1)
      {
        paramObject = (PdoBinder)paramObject;
        Map localMap1 = GRM_BANK;
        Map localMap2 = GRM_BANK;
        bool1 = k.a(localMap1, localMap2);
        if (bool1)
        {
          localMap1 = GRM_EVENT;
          localMap2 = GRM_EVENT;
          bool1 = k.a(localMap1, localMap2);
          if (bool1)
          {
            localMap1 = GRM_TRAVEL;
            localMap2 = GRM_TRAVEL;
            bool1 = k.a(localMap1, localMap2);
            if (bool1)
            {
              localMap1 = GRM_BILL;
              localMap2 = GRM_BILL;
              bool1 = k.a(localMap1, localMap2);
              if (bool1)
              {
                localMap1 = GRM_OTP;
                localMap2 = GRM_OTP;
                bool1 = k.a(localMap1, localMap2);
                if (bool1)
                {
                  localMap1 = GRM_OFFERS;
                  localMap2 = GRM_OFFERS;
                  bool1 = k.a(localMap1, localMap2);
                  if (bool1)
                  {
                    localMap1 = GRM_NOTIF;
                    localMap2 = GRM_NOTIF;
                    bool1 = k.a(localMap1, localMap2);
                    if (bool1)
                    {
                      localMap1 = GRM_DELIVERY;
                      localMap2 = GRM_DELIVERY;
                      bool1 = k.a(localMap1, localMap2);
                      if (bool1)
                      {
                        localMap1 = GRM_VOID;
                        paramObject = GRM_VOID;
                        boolean bool2 = k.a(localMap1, paramObject);
                        if (bool2) {
                          break label222;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label222:
    return true;
  }
  
  public final Map getBinderByCategory(e parame)
  {
    k.b(parame, "category");
    Object localObject = e.a.a;
    boolean bool1 = k.a(parame, localObject);
    if (bool1) {
      return GRM_BANK;
    }
    localObject = e.b.a;
    bool1 = k.a(parame, localObject);
    if (bool1) {
      return GRM_BILL;
    }
    localObject = e.d.a;
    bool1 = k.a(parame, localObject);
    if (bool1) {
      return GRM_EVENT;
    }
    localObject = e.g.a;
    bool1 = k.a(parame, localObject);
    if (bool1) {
      return GRM_OTP;
    }
    localObject = e.e.a;
    bool1 = k.a(parame, localObject);
    if (bool1) {
      return GRM_NOTIF;
    }
    localObject = e.i.a;
    bool1 = k.a(parame, localObject);
    if (bool1) {
      return GRM_TRAVEL;
    }
    localObject = e.f.a;
    bool1 = k.a(parame, localObject);
    if (bool1) {
      return GRM_OFFERS;
    }
    localObject = e.c.a;
    bool1 = k.a(parame, localObject);
    if (bool1) {
      return GRM_DELIVERY;
    }
    localObject = e.h.a;
    boolean bool2 = k.a(parame, localObject);
    if (bool2) {
      return GRM_VOID;
    }
    parame = new c/l;
    parame.<init>();
    throw parame;
  }
  
  public final Map getBinderByCategoryString(String paramString)
  {
    String str = "category";
    k.b(paramString, str);
    int i = paramString.hashCode();
    boolean bool;
    switch (i)
    {
    default: 
      break;
    case 888111124: 
      str = "Delivery";
      bool = paramString.equals(str);
      if (bool) {
        return GRM_DELIVERY;
      }
      break;
    case 75456272: 
      str = "Notif";
      bool = paramString.equals(str);
      if (bool) {
        return GRM_NOTIF;
      }
      break;
    case 67338874: 
      str = "Event";
      bool = paramString.equals(str);
      if (bool) {
        return GRM_EVENT;
      }
      break;
    case 2578847: 
      str = "Skip";
      bool = paramString.equals(str);
      if (bool) {
        return GRM_VOID;
      }
      break;
    case 2070567: 
      str = "Bill";
      bool = paramString.equals(str);
      if (bool) {
        return GRM_BILL;
      }
      break;
    case 2062940: 
      str = "Bank";
      bool = paramString.equals(str);
      if (bool) {
        return GRM_BANK;
      }
      break;
    case 78603: 
      str = "OTP";
      bool = paramString.equals(str);
      if (bool) {
        return GRM_OTP;
      }
      break;
    case -1781830854: 
      str = "Travel";
      bool = paramString.equals(str);
      if (bool) {
        return GRM_TRAVEL;
      }
      break;
    case -1935925833: 
      str = "Offers";
      bool = paramString.equals(str);
      if (bool) {
        return GRM_OFFERS;
      }
      break;
    }
    return GRM_VOID;
  }
  
  public final Map getGRM_BANK()
  {
    return GRM_BANK;
  }
  
  public final Map getGRM_BILL()
  {
    return GRM_BILL;
  }
  
  public final Map getGRM_DELIVERY()
  {
    return GRM_DELIVERY;
  }
  
  public final Map getGRM_EVENT()
  {
    return GRM_EVENT;
  }
  
  public final Map getGRM_NOTIF()
  {
    return GRM_NOTIF;
  }
  
  public final Map getGRM_OFFERS()
  {
    return GRM_OFFERS;
  }
  
  public final Map getGRM_OTP()
  {
    return GRM_OTP;
  }
  
  public final Map getGRM_TRAVEL()
  {
    return GRM_TRAVEL;
  }
  
  public final Map getGRM_VOID()
  {
    return GRM_VOID;
  }
  
  public final int hashCode()
  {
    Map localMap1 = GRM_BANK;
    int i = 0;
    if (localMap1 != null)
    {
      j = localMap1.hashCode();
    }
    else
    {
      j = 0;
      localMap1 = null;
    }
    j *= 31;
    Map localMap2 = GRM_EVENT;
    int k;
    if (localMap2 != null)
    {
      k = localMap2.hashCode();
    }
    else
    {
      k = 0;
      localMap2 = null;
    }
    int j = (j + k) * 31;
    localMap2 = GRM_TRAVEL;
    if (localMap2 != null)
    {
      k = localMap2.hashCode();
    }
    else
    {
      k = 0;
      localMap2 = null;
    }
    j = (j + k) * 31;
    localMap2 = GRM_BILL;
    if (localMap2 != null)
    {
      k = localMap2.hashCode();
    }
    else
    {
      k = 0;
      localMap2 = null;
    }
    j = (j + k) * 31;
    localMap2 = GRM_OTP;
    if (localMap2 != null)
    {
      k = localMap2.hashCode();
    }
    else
    {
      k = 0;
      localMap2 = null;
    }
    j = (j + k) * 31;
    localMap2 = GRM_OFFERS;
    if (localMap2 != null)
    {
      k = localMap2.hashCode();
    }
    else
    {
      k = 0;
      localMap2 = null;
    }
    j = (j + k) * 31;
    localMap2 = GRM_NOTIF;
    if (localMap2 != null)
    {
      k = localMap2.hashCode();
    }
    else
    {
      k = 0;
      localMap2 = null;
    }
    j = (j + k) * 31;
    localMap2 = GRM_DELIVERY;
    if (localMap2 != null)
    {
      k = localMap2.hashCode();
    }
    else
    {
      k = 0;
      localMap2 = null;
    }
    j = (j + k) * 31;
    localMap2 = GRM_VOID;
    if (localMap2 != null) {
      i = localMap2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PdoBinder(GRM_BANK=");
    Map localMap = GRM_BANK;
    localStringBuilder.append(localMap);
    localStringBuilder.append(", GRM_EVENT=");
    localMap = GRM_EVENT;
    localStringBuilder.append(localMap);
    localStringBuilder.append(", GRM_TRAVEL=");
    localMap = GRM_TRAVEL;
    localStringBuilder.append(localMap);
    localStringBuilder.append(", GRM_BILL=");
    localMap = GRM_BILL;
    localStringBuilder.append(localMap);
    localStringBuilder.append(", GRM_OTP=");
    localMap = GRM_OTP;
    localStringBuilder.append(localMap);
    localStringBuilder.append(", GRM_OFFERS=");
    localMap = GRM_OFFERS;
    localStringBuilder.append(localMap);
    localStringBuilder.append(", GRM_NOTIF=");
    localMap = GRM_NOTIF;
    localStringBuilder.append(localMap);
    localStringBuilder.append(", GRM_DELIVERY=");
    localMap = GRM_DELIVERY;
    localStringBuilder.append(localMap);
    localStringBuilder.append(", GRM_VOID=");
    localMap = GRM_VOID;
    localStringBuilder.append(localMap);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.models.PdoBinderType.PdoBinder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */