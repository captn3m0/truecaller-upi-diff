package com.truecaller.insights.models;

import c.g.b.k;
import java.util.Date;

public final class InsightState
{
  private Date createdAt;
  private Date lastUpdatedAt;
  private String lastUpdatedData;
  private String owner = "";
  
  public InsightState()
  {
    Date localDate = new java/util/Date;
    localDate.<init>(0L);
    lastUpdatedAt = localDate;
    localDate = new java/util/Date;
    localDate.<init>();
    createdAt = localDate;
  }
  
  public final Date getCreatedAt()
  {
    return createdAt;
  }
  
  public final Date getLastUpdatedAt()
  {
    return lastUpdatedAt;
  }
  
  public final String getLastUpdatedData()
  {
    return lastUpdatedData;
  }
  
  public final String getOwner()
  {
    return owner;
  }
  
  public final void setCreatedAt(Date paramDate)
  {
    k.b(paramDate, "<set-?>");
    createdAt = paramDate;
  }
  
  public final void setLastUpdatedAt(Date paramDate)
  {
    k.b(paramDate, "<set-?>");
    lastUpdatedAt = paramDate;
  }
  
  public final void setLastUpdatedData(String paramString)
  {
    lastUpdatedData = paramString;
  }
  
  public final void setOwner(String paramString)
  {
    k.b(paramString, "<set-?>");
    owner = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.models.InsightState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */