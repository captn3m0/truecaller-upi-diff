package com.truecaller.insights.models;

import c.g.b.k;
import com.twelfthmile.malana.compiler.types.Response;

public final class f$b
  extends f
{
  final Response a;
  
  public f$b(Response paramResponse)
  {
    super((byte)0);
    a = paramResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof b;
      if (bool1)
      {
        paramObject = (b)paramObject;
        Response localResponse = a;
        paramObject = a;
        boolean bool2 = k.a(localResponse, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Response localResponse = a;
    if (localResponse != null) {
      return localResponse.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SmsDetailedResponse(response=");
    Response localResponse = a;
    localStringBuilder.append(localResponse);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.models.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */