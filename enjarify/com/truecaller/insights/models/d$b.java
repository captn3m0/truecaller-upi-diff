package com.truecaller.insights.models;

import c.g.b.k;

public final class d$b
  extends d
{
  final c a;
  public final e b;
  final String c;
  public final f d;
  
  public d$b(c paramc, e parame, String paramString, f paramf)
  {
    super((byte)0);
    a = paramc;
    b = parame;
    c = paramString;
    d = paramf;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof b;
      if (bool1)
      {
        paramObject = (b)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = c;
            localObject2 = c;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = d;
              paramObject = d;
              boolean bool2 = k.a(localObject1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final int hashCode()
  {
    c localc = a;
    int i = 0;
    if (localc != null)
    {
      j = localc.hashCode();
    }
    else
    {
      j = 0;
      localc = null;
    }
    j *= 31;
    Object localObject = b;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = d;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ParseResponse(smsMessage=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", classification=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", address=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", detailedResponse=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.models.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */