package com.truecaller.insights.models;

import c.a.ag;
import c.g.b.t;
import c.g.b.w;
import com.truecaller.log.UnmutedException.InsightsExceptions;
import com.truecaller.log.UnmutedException.InsightsExceptions.Cause;
import com.truecaller.log.d;
import com.twelfthmile.malana.compiler.types.Response;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class b
  implements a
{
  private final c.f b;
  private final com.truecaller.insights.core.smscategorizer.db.a c;
  private final com.google.gson.f d;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(b.class);
    ((c.g.b.u)localObject).<init>(localb, "binder", "getBinder()Lcom/truecaller/insights/models/PdoBinderType;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public b(com.truecaller.insights.core.smscategorizer.db.a parama, com.google.gson.f paramf)
  {
    c = parama;
    d = paramf;
    parama = new com/truecaller/insights/models/b$a;
    parama.<init>(this);
    parama = c.g.a((c.g.a.a)parama);
    b = parama;
  }
  
  public final ParsedDataObject a(d.b paramb)
  {
    c.g.b.k.b(paramb, "smsResponse");
    Object localObject1 = d;
    if (localObject1 != null)
    {
      localObject1 = a.getValMap();
      c.g.b.k.a(localObject1, "(smsResponse.detailedRes…Response).response.valMap");
      localObject1 = ((com.twelfthmile.b.a.a)localObject1).getAll();
      c.g.b.k.a(localObject1, "(smsResponse.detailedRes…onse).response.valMap.all");
      Object localObject2 = new java/util/LinkedHashMap;
      int i = ag.a(((Map)localObject1).size());
      ((LinkedHashMap)localObject2).<init>(i);
      localObject2 = (Map)localObject2;
      localObject1 = ((Iterable)((Map)localObject1).entrySet()).iterator();
      boolean bool1;
      Object localObject4;
      Object localObject5;
      boolean bool2;
      for (;;)
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break label216;
        }
        localObject3 = (Map.Entry)((Iterator)localObject1).next();
        localObject4 = a();
        if (localObject4 == null) {
          break;
        }
        localObject4 = (PdoBinderType.PdoBinder)localObject4;
        localObject5 = b;
        localObject4 = ((PdoBinderType.PdoBinder)localObject4).getBinderByCategory((e)localObject5);
        localObject5 = ((Map.Entry)localObject3).getKey();
        localObject4 = ((Map)localObject4).get(localObject5);
        if (localObject4 == null)
        {
          bool2 = false;
          localObject4 = null;
        }
        localObject4 = (String)localObject4;
        localObject3 = ((Map.Entry)localObject3).getValue();
        ((Map)localObject2).put(localObject4, localObject3);
      }
      paramb = new c/u;
      paramb.<init>("null cannot be cast to non-null type com.truecaller.insights.models.PdoBinderType.PdoBinder");
      throw paramb;
      label216:
      localObject1 = new java/util/LinkedHashMap;
      ((LinkedHashMap)localObject1).<init>();
      localObject2 = ((Map)localObject2).entrySet().iterator();
      for (;;)
      {
        bool1 = ((Iterator)localObject2).hasNext();
        if (!bool1) {
          break;
        }
        localObject3 = (Map.Entry)((Iterator)localObject2).next();
        localObject4 = (String)((Map.Entry)localObject3).getKey();
        if (localObject4 != null)
        {
          localObject5 = "";
          bool2 = c.g.b.k.a(localObject4, localObject5);
          if (!bool2)
          {
            bool2 = true;
            break label307;
          }
        }
        bool2 = false;
        localObject4 = null;
        label307:
        if (bool2)
        {
          localObject4 = ((Map.Entry)localObject3).getKey();
          localObject3 = ((Map.Entry)localObject3).getValue();
          ((LinkedHashMap)localObject1).put(localObject4, localObject3);
        }
      }
      localObject1 = (Map)localObject1;
      localObject2 = d;
      localObject1 = ((com.google.gson.f)localObject2).a(localObject1);
      Object localObject3 = ParsedDataObject.class;
      localObject1 = ((com.google.gson.f)localObject2).a((com.google.gson.l)localObject1, (Type)localObject3);
      localObject1 = com.google.gson.b.k.a((Class)localObject3).cast(localObject1);
      c.g.b.k.a(localObject1, "gson.fromJson(gson.toJso…edDataObject::class.java)");
      localObject1 = (ParsedDataObject)localObject1;
      localObject2 = a;
      long l = b;
      ((ParsedDataObject)localObject1).setMessageID(l);
      localObject3 = c;
      ((ParsedDataObject)localObject1).setAddress((String)localObject3);
      paramb = b.a();
      ((ParsedDataObject)localObject1).setD(paramb);
      paramb = com.twelfthmile.e.b.a.a();
      localObject3 = d;
      paramb = paramb.format((Date)localObject3);
      c.g.b.k.a(paramb, "Constants.dateTimeFormat…).format(smsMessage.date)");
      ((ParsedDataObject)localObject1).setMsgdatetime(paramb);
      paramb = com.twelfthmile.e.b.a.b();
      localObject2 = d;
      paramb = paramb.format((Date)localObject2);
      c.g.b.k.a(paramb, "Constants.dateFormatter().format(smsMessage.date)");
      ((ParsedDataObject)localObject1).setDate(paramb);
      return (ParsedDataObject)localObject1;
    }
    paramb = new c/u;
    paramb.<init>("null cannot be cast to non-null type com.truecaller.insights.models.SmsDetailedResponseType.SmsDetailedResponse");
    throw paramb;
  }
  
  public final PdoBinderType a()
  {
    return (PdoBinderType)b.b();
  }
  
  public final e a(ParsedDataObject paramParsedDataObject)
  {
    String str = "pdo";
    c.g.b.k.b(paramParsedDataObject, str);
    paramParsedDataObject = paramParsedDataObject.getD();
    int i = paramParsedDataObject.hashCode();
    boolean bool;
    switch (i)
    {
    default: 
      break;
    case 888111124: 
      str = "Delivery";
      bool = paramParsedDataObject.equals(str);
      if (bool) {
        return (e)e.c.a;
      }
      break;
    case 75456272: 
      str = "Notif";
      bool = paramParsedDataObject.equals(str);
      if (bool) {
        return (e)e.e.a;
      }
      break;
    case 67338874: 
      str = "Event";
      bool = paramParsedDataObject.equals(str);
      if (bool) {
        return (e)e.d.a;
      }
      break;
    case 2070567: 
      str = "Bill";
      bool = paramParsedDataObject.equals(str);
      if (bool) {
        return (e)e.b.a;
      }
      break;
    case 2062940: 
      str = "Bank";
      bool = paramParsedDataObject.equals(str);
      if (bool) {
        return (e)e.a.a;
      }
      break;
    case 78603: 
      str = "OTP";
      bool = paramParsedDataObject.equals(str);
      if (bool) {
        return (e)e.g.a;
      }
      break;
    case -1781830854: 
      str = "Travel";
      bool = paramParsedDataObject.equals(str);
      if (bool) {
        return (e)e.i.a;
      }
      break;
    case -1935925833: 
      str = "Offers";
      bool = paramParsedDataObject.equals(str);
      if (bool) {
        return (e)e.f.a;
      }
      break;
    }
    return (e)e.h.a;
  }
  
  public final String a(ParsedDataObject paramParsedDataObject, String paramString)
  {
    c.g.b.k.b(paramParsedDataObject, "pdo");
    c.g.b.k.b(paramString, "key");
    Object localObject = a();
    if (localObject != null)
    {
      localObject = (PdoBinderType.PdoBinder)localObject;
      String str = paramParsedDataObject.getD();
      localObject = (String)((PdoBinderType.PdoBinder)localObject).getBinderByCategoryString(str).get(paramString);
      if (localObject != null)
      {
        int i = ((String)localObject).hashCode();
        boolean bool;
        switch (i)
        {
        default: 
          break;
        case 1793702779: 
          str = "datetime";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getDatetime();
          }
          break;
        case 1529956463: 
          str = "dff_val5";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getDiffVal5();
          }
          break;
        case 1529956462: 
          str = "dff_val4";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getDiffVal4();
          }
          break;
        case 1529956461: 
          str = "dff_val3";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getDiffVal3();
          }
          break;
        case 1529956460: 
          str = "dff_val2";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getDiffVal2();
          }
          break;
        case 1529956459: 
          str = "dff_val1";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getDiffVal1();
          }
          break;
        case 1344204463: 
          str = "msgdate";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getMsgdate();
          }
          break;
        case 886067708: 
          str = "msgdatetime";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getMsgdatetime();
          }
          break;
        case 3611956: 
          str = "val5";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getVal5();
          }
          break;
        case 3611955: 
          str = "val4";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getVal4();
          }
          break;
        case 3611954: 
          str = "val3";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getVal3();
          }
          break;
        case 3611953: 
          str = "val2";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getVal2();
          }
          break;
        case 3611952: 
          str = "val1";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getVal1();
          }
          break;
        case 3076014: 
          str = "date";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getDate();
          }
          break;
        case 115: 
          str = "s";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getS();
          }
          break;
        case 112: 
          str = "p";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getP();
          }
          break;
        case 111: 
          str = "o";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getO();
          }
          break;
        case 107: 
          str = "k";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getK();
          }
          break;
        case 103: 
          str = "g";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getG();
          }
          break;
        case 102: 
          str = "f";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getF();
          }
          break;
        case 99: 
          str = "c";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return paramParsedDataObject.getC();
          }
          break;
        }
      }
      paramString = String.valueOf(paramString);
      paramParsedDataObject = "Attempt to unBind an unknown key: ".concat(paramString);
      paramString = new com/truecaller/log/UnmutedException$InsightsExceptions;
      localObject = UnmutedException.InsightsExceptions.Cause.BINDER_EXCEPTION;
      paramString.<init>((UnmutedException.InsightsExceptions.Cause)localObject);
      d.a((Throwable)paramString, paramParsedDataObject);
      return "";
    }
    paramParsedDataObject = new c/u;
    paramParsedDataObject.<init>("null cannot be cast to non-null type com.truecaller.insights.models.PdoBinderType.PdoBinder");
    throw paramParsedDataObject;
  }
  
  public final SmsBackup b(d.b paramb)
  {
    c.g.b.k.b(paramb, "smsResponse");
    Object localObject = d;
    boolean bool1 = localObject instanceof f.a;
    if (bool1)
    {
      localObject = new com/truecaller/insights/models/SmsBackup;
      c localc = a;
      paramb = d).a;
      ((SmsBackup)localObject).<init>(localc, paramb);
      return (SmsBackup)localObject;
    }
    boolean bool2 = localObject instanceof f.b;
    if (bool2)
    {
      localObject = new com/truecaller/insights/models/SmsBackup;
      paramb = a;
      ((SmsBackup)localObject).<init>(paramb);
      return (SmsBackup)localObject;
    }
    paramb = new c/l;
    paramb.<init>();
    throw paramb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.models.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */