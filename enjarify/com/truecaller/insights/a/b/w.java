package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final r a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private w(r paramr, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramr;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static w a(r paramr, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    w localw = new com/truecaller/insights/a/b/w;
    localw.<init>(paramr, paramProvider1, paramProvider2, paramProvider3);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */