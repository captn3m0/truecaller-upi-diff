package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d
{
  private final r a;
  private final Provider b;
  
  private u(r paramr, Provider paramProvider)
  {
    a = paramr;
    b = paramProvider;
  }
  
  public static u a(r paramr, Provider paramProvider)
  {
    u localu = new com/truecaller/insights/a/b/u;
    localu.<init>(paramr, paramProvider);
    return localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */