package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d
{
  private final h a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private k(h paramh, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramh;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static k a(h paramh, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    k localk = new com/truecaller/insights/a/b/k;
    localk.<init>(paramh, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */