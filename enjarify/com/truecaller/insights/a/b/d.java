package com.truecaller.insights.a.b;

import android.arch.persistence.room.e;
import android.arch.persistence.room.f.a;
import android.arch.persistence.room.f.b;
import android.content.Context;
import c.g.b.k;
import com.truecaller.insights.database.InsightsDb;
import dagger.a.g;
import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final a a;
  private final Provider b;
  private final Provider c;
  
  private d(a parama, Provider paramProvider1, Provider paramProvider2)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static d a(a parama, Provider paramProvider1, Provider paramProvider2)
  {
    d locald = new com/truecaller/insights/a/b/d;
    locald.<init>(parama, paramProvider1, paramProvider2);
    return locald;
  }
  
  public static InsightsDb a(Context paramContext, com.truecaller.insights.database.a parama)
  {
    k.b(paramContext, "applicationContext");
    k.b(parama, "categorizerDBCallback");
    paramContext = e.a(paramContext, InsightsDb.class, "insights.db");
    android.arch.persistence.room.a.a[] arrayOfa = new android.arch.persistence.room.a.a[1];
    Object localObject = new com/truecaller/insights/database/b/a;
    ((com.truecaller.insights.database.b.a)localObject).<init>();
    localObject = (android.arch.persistence.room.a.a)localObject;
    arrayOfa[0] = localObject;
    paramContext = paramContext.a(arrayOfa);
    parama = (f.b)parama;
    paramContext = paramContext.a(parama).b();
    k.a(paramContext, "Room\n            .databa…ack)\n            .build()");
    return (InsightsDb)g.a((InsightsDb)paramContext, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */