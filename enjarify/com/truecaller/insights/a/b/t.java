package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d
{
  private final r a;
  private final Provider b;
  
  private t(r paramr, Provider paramProvider)
  {
    a = paramr;
    b = paramProvider;
  }
  
  public static t a(r paramr, Provider paramProvider)
  {
    t localt = new com/truecaller/insights/a/b/t;
    localt.<init>(paramr, paramProvider);
    return localt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */