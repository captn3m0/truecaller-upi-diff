package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final m a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private q(m paramm, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramm;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static q a(m paramm, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    q localq = new com/truecaller/insights/a/b/q;
    localq.<init>(paramm, paramProvider1, paramProvider2, paramProvider3);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */