package com.truecaller.insights.a.b;

import c.d.f;
import c.g.b.k;
import com.truecaller.insights.database.InsightsDb;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class b
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  public static com.truecaller.insights.core.b.a a(InsightsDb paramInsightsDb, com.truecaller.insights.models.a parama, f paramf)
  {
    k.b(paramInsightsDb, "db");
    k.b(parama, "insightsBinder");
    k.b(paramf, "coroutineContext");
    com.truecaller.insights.core.b.b localb = new com/truecaller/insights/core/b/b;
    localb.<init>(paramInsightsDb, parama, paramf);
    return (com.truecaller.insights.core.b.a)g.a((com.truecaller.insights.core.b.a)localb, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */