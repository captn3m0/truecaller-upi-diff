package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d
{
  private final r a;
  private final Provider b;
  
  private v(r paramr, Provider paramProvider)
  {
    a = paramr;
    b = paramProvider;
  }
  
  public static v a(r paramr, Provider paramProvider)
  {
    v localv = new com/truecaller/insights/a/b/v;
    localv.<init>(paramr, paramProvider);
    return localv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */