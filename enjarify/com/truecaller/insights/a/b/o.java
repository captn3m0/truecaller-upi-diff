package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final m a;
  private final Provider b;
  private final Provider c;
  
  private o(m paramm, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramm;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static o a(m paramm, Provider paramProvider1, Provider paramProvider2)
  {
    o localo = new com/truecaller/insights/a/b/o;
    localo.<init>(paramm, paramProvider1, paramProvider2);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */