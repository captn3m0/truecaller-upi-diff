package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final m a;
  private final Provider b;
  private final Provider c;
  
  private p(m paramm, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramm;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static p a(m paramm, Provider paramProvider1, Provider paramProvider2)
  {
    p localp = new com/truecaller/insights/a/b/p;
    localp.<init>(paramm, paramProvider1, paramProvider2);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */