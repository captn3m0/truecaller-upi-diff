package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d
{
  private final h a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private l(h paramh, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramh;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static l a(h paramh, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    l locall = new com/truecaller/insights/a/b/l;
    locall.<init>(paramh, paramProvider1, paramProvider2, paramProvider3);
    return locall;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */