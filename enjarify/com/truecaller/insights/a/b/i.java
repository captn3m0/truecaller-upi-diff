package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final h a;
  private final Provider b;
  
  private i(h paramh, Provider paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static i a(h paramh, Provider paramProvider)
  {
    i locali = new com/truecaller/insights/a/b/i;
    locali.<init>(paramh, paramProvider);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */