package com.truecaller.insights.a.a;

import android.content.Context;
import com.truecaller.insights.a.b.h;
import com.truecaller.insights.a.b.i;
import com.truecaller.insights.a.b.j;
import com.truecaller.insights.a.b.k;
import com.truecaller.insights.a.b.l;
import com.truecaller.insights.a.b.m;
import com.truecaller.insights.a.b.n;
import com.truecaller.insights.a.b.o;
import com.truecaller.insights.a.b.p;
import com.truecaller.insights.a.b.q;
import com.truecaller.insights.a.b.r;
import com.truecaller.insights.a.b.s;
import com.truecaller.insights.a.b.t;
import com.truecaller.insights.a.b.u;
import com.truecaller.insights.a.b.v;
import com.truecaller.insights.a.b.w;
import com.truecaller.insights.database.InsightsDb;
import javax.inject.Provider;

public final class a
  implements b
{
  private final com.truecaller.analytics.d a;
  private final com.truecaller.common.a b;
  private final com.truecaller.insights.a.b.a c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  private Provider w;
  private Provider x;
  private Provider y;
  
  private a(m paramm, r paramr, com.truecaller.insights.a.b.a parama, h paramh, com.truecaller.analytics.d paramd, com.truecaller.common.a parama1)
  {
    a = paramd;
    b = parama1;
    c = parama;
    Object localObject = new com/truecaller/insights/a/a/a$d;
    ((a.d)localObject).<init>(parama1);
    d = ((Provider)localObject);
    localObject = new com/truecaller/insights/a/a/a$b;
    ((a.b)localObject).<init>(paramd);
    e = ((Provider)localObject);
    paramd = new com/truecaller/insights/a/a/a$c;
    paramd.<init>(parama1);
    f = paramd;
    paramd = f;
    paramd = t.a(paramr, paramd);
    g = paramd;
    paramd = new com/truecaller/insights/a/a/a$e;
    paramd.<init>(parama1);
    h = paramd;
    paramd = g;
    parama1 = h;
    paramd = dagger.a.c.a(p.a(paramm, paramd, parama1));
    i = paramd;
    paramd = d;
    parama1 = e;
    localObject = i;
    paramd = dagger.a.c.a(q.a(paramm, paramd, parama1, (Provider)localObject));
    j = paramd;
    paramd = com.truecaller.insights.core.smscategorizer.db.c.a(g);
    k = paramd;
    paramd = k;
    paramd = s.a(paramr, paramd);
    l = paramd;
    paramd = f;
    parama1 = l;
    localObject = k;
    paramd = w.a(paramr, paramd, parama1, (Provider)localObject);
    m = paramd;
    paramd = m;
    paramd = u.a(paramr, paramd);
    n = paramd;
    paramd = n;
    paramr = v.a(paramr, paramd);
    o = paramr;
    paramr = dagger.a.c.a(n.a(paramm));
    p = paramr;
    paramr = k;
    paramd = p;
    paramm = dagger.a.c.a(o.a(paramm, paramr, paramd));
    q = paramm;
    paramm = f;
    paramr = com.truecaller.insights.database.b.a();
    paramm = com.truecaller.insights.a.b.d.a(parama, paramm, paramr);
    r = paramm;
    paramm = r;
    paramm = com.truecaller.insights.a.b.e.a(parama, paramm);
    s = paramm;
    paramm = r;
    paramm = com.truecaller.insights.a.b.f.a(parama, paramm);
    t = paramm;
    paramm = r;
    paramm = com.truecaller.insights.a.b.c.a(parama, paramm);
    u = paramm;
    paramm = s;
    paramr = t;
    parama = u;
    paramd = h;
    paramm = k.a(paramh, paramm, paramr, parama, paramd);
    v = paramm;
    paramm = v;
    paramr = q;
    parama = h;
    paramm = l.a(paramh, paramm, paramr, parama);
    w = paramm;
    paramm = v;
    paramm = i.a(paramh, paramm);
    x = paramm;
    paramm = w;
    paramr = x;
    parama = v;
    paramd = q;
    paramm = dagger.a.c.a(j.a(paramh, paramm, paramr, parama, paramd));
    y = paramm;
  }
  
  public static b.a a()
  {
    a.a locala = new com/truecaller/insights/a/a/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final com.truecaller.insights.core.d.a b()
  {
    return (com.truecaller.insights.core.d.a)j.get();
  }
  
  public final com.truecaller.insights.core.smscategorizer.d c()
  {
    Object localObject1 = (com.truecaller.analytics.b)dagger.a.g.a(a.c(), "Cannot return null from a non-@Nullable component method");
    Object localObject2 = (com.truecaller.androidactors.f)dagger.a.g.a(a.f(), "Cannot return null from a non-@Nullable component method");
    localObject1 = com.truecaller.insights.core.smscategorizer.f.a((com.truecaller.analytics.b)localObject1, (com.truecaller.androidactors.f)localObject2);
    localObject2 = dagger.a.c.b(o);
    a = ((dagger.a)localObject2);
    return (com.truecaller.insights.core.smscategorizer.d)localObject1;
  }
  
  public final com.truecaller.insights.core.b.c d()
  {
    Object localObject1 = (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method");
    Object localObject2 = new com/truecaller/insights/database/a;
    ((com.truecaller.insights.database.a)localObject2).<init>();
    localObject1 = com.truecaller.insights.a.b.d.a((Context)localObject1, (com.truecaller.insights.database.a)localObject2);
    localObject2 = (com.truecaller.insights.models.a)q.get();
    c.d.f localf = (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method");
    return com.truecaller.insights.a.b.g.a(com.truecaller.insights.a.b.b.a((InsightsDb)localObject1, (com.truecaller.insights.models.a)localObject2, localf));
  }
  
  public final com.truecaller.insights.core.c.a e()
  {
    return (com.truecaller.insights.core.c.a)y.get();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */