package com.truecaller.insights.c;

import c.g.b.k;
import com.truecaller.insights.models.ParsedDataObject;
import com.truecaller.insights.models.PdoBinderType.PdoBinder;
import com.truecaller.insights.models.e;
import com.twelfthmile.c.b.g;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class c
  implements g
{
  private final ParsedDataObject a;
  private final com.truecaller.insights.models.a b;
  
  public c(ParsedDataObject paramParsedDataObject, com.truecaller.insights.models.a parama)
  {
    a = paramParsedDataObject;
    b = parama;
  }
  
  public final String a()
  {
    com.truecaller.insights.models.a locala = b;
    ParsedDataObject localParsedDataObject = a;
    return locala.a(localParsedDataObject).b();
  }
  
  public final String a(String paramString)
  {
    if (paramString != null)
    {
      Object localObject = b.a();
      boolean bool = localObject instanceof PdoBinderType.PdoBinder;
      if (bool)
      {
        localObject = b;
        ParsedDataObject localParsedDataObject = a;
        return ((com.truecaller.insights.models.a)localObject).a(localParsedDataObject, paramString);
      }
    }
    return "";
  }
  
  public final long b()
  {
    return a.getMessageID();
  }
  
  public final Float b(String paramString)
  {
    if (paramString != null)
    {
      Object localObject = b.a();
      boolean bool = localObject instanceof PdoBinderType.PdoBinder;
      if (bool)
      {
        localObject = b;
        ParsedDataObject localParsedDataObject = a;
        return Float.valueOf(Float.parseFloat(((com.truecaller.insights.models.a)localObject).a(localParsedDataObject, paramString)));
      }
    }
    return null;
  }
  
  public final long c()
  {
    long l = -1;
    try
    {
      Object localObject1 = a;
      localObject1 = ((ParsedDataObject)localObject1).getMsgdatetime();
      Object localObject2 = "";
      boolean bool = k.a(localObject1, localObject2) ^ true;
      if (bool)
      {
        localObject1 = com.twelfthmile.e.b.a.a();
        localObject2 = a;
        localObject2 = ((ParsedDataObject)localObject2).getMsgdatetime();
        localObject1 = ((SimpleDateFormat)localObject1).parse((String)localObject2);
        localObject2 = "Constants.dateTimeFormat….parse(model.msgdatetime)";
        k.a(localObject1, (String)localObject2);
        l = ((Date)localObject1).getTime();
      }
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return l;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */