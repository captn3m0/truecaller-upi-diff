package com.truecaller.insights.c;

import c.g.a.m;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.insights.models.ParsedDataObject;
import com.truecaller.insights.models.e.a;
import com.twelfthmile.c.a.e;
import com.twelfthmile.c.b.b.b;
import com.twelfthmile.c.b.g;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class b$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag g;
  
  b$a(b paramb, long paramLong, b.b paramb1, List paramList, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/insights/c/b$a;
    b localb = c;
    long l = d;
    b.b localb1 = e;
    List localList = f;
    locala.<init>(localb, l, localb1, localList, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = b;
    int j = 1;
    boolean bool1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      localObject1 = (ParsedDataObject)a;
      bool1 = paramObject instanceof o.b;
      if (!bool1) {
        break label219;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label586;
      }
      paramObject = c.a;
      long l = d;
      b = j;
      paramObject = ((com.truecaller.insights.database.c.a)paramObject).a(l, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (ParsedDataObject)paramObject;
    Object localObject2 = c.a;
    Object localObject3 = e.a();
    Object localObject4 = "childrenIds.linkingIds";
    c.g.b.k.a(localObject3, (String)localObject4);
    a = paramObject;
    int k = 2;
    b = k;
    localObject2 = ((com.truecaller.insights.database.c.a)localObject2).a((List)localObject3, this);
    if (localObject2 == localObject1) {
      return localObject1;
    }
    localObject1 = paramObject;
    paramObject = localObject2;
    label219:
    paramObject = (List)paramObject;
    localObject2 = c;
    localObject3 = ((ParsedDataObject)localObject1).getD();
    localObject4 = e.a.a;
    localObject4 = "Bank";
    boolean bool2 = c.g.b.k.a(localObject3, localObject4);
    ParsedDataObject localParsedDataObject;
    if (bool2)
    {
      localObject3 = paramObject;
      localObject3 = (Iterable)paramObject;
      localObject4 = new java/util/ArrayList;
      ((ArrayList)localObject4).<init>();
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject3).hasNext();
        if (!bool3) {
          break;
        }
        Object localObject5 = ((Iterator)localObject3).next();
        Object localObject6 = localObject5;
        localObject6 = (ParsedDataObject)localObject5;
        Object localObject7 = new com/truecaller/insights/c/c;
        Object localObject8 = b;
        ((c)localObject7).<init>((ParsedDataObject)localObject1, (com.truecaller.insights.models.a)localObject8);
        localObject7 = (g)localObject7;
        localObject8 = new com/truecaller/insights/c/c;
        com.truecaller.insights.models.a locala = b;
        ((c)localObject8).<init>((ParsedDataObject)localObject6, locala);
        localObject8 = (g)localObject8;
        boolean bool4 = e.a((g)localObject7, (g)localObject8);
        if (bool4) {
          ((ArrayList)localObject4).add(localObject5);
        } else {
          localArrayList.add(localObject5);
        }
      }
      localObject3 = new c/n;
      ((n)localObject3).<init>(localObject4, localArrayList);
      localObject4 = (List)a;
      localObject3 = (Collection)b;
      bool2 = ((Collection)localObject3).isEmpty();
      j ^= bool2;
      if (j != 0) {
        b.b((ParsedDataObject)localObject1, (List)paramObject);
      }
      localParsedDataObject = b.a((ParsedDataObject)localObject1, (List)localObject4);
    }
    else
    {
      localParsedDataObject = b.a((ParsedDataObject)localObject1, (List)paramObject);
    }
    localObject3 = c;
    localObject4 = new com/truecaller/insights/c/b$c;
    ((b.c)localObject4).<init>((b)localObject2, (ParsedDataObject)localObject1, (List)paramObject, null);
    localObject4 = (m)localObject4;
    kotlinx.coroutines.f.a((c.d.f)localObject3, (m)localObject4);
    localObject2 = f;
    paramObject = (Collection)paramObject;
    ((List)localObject2).addAll((Collection)paramObject);
    f.add(localObject1);
    return Boolean.valueOf(f.add(localParsedDataObject));
    label586:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.c.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */