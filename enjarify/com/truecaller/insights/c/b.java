package com.truecaller.insights.c;

import c.g.a.m;
import c.g.b.k;
import com.truecaller.insights.models.ParsedDataObject;
import com.twelfthmile.c.a.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class b
  extends c
{
  final com.truecaller.insights.database.c.a a;
  final com.truecaller.insights.models.a b;
  final c.d.f c;
  
  public b(com.truecaller.insights.database.c.a parama, com.truecaller.insights.models.a parama1, c.d.f paramf)
  {
    a = parama;
    b = parama1;
    c = paramf;
  }
  
  static ParsedDataObject a(ParsedDataObject paramParsedDataObject, List paramList)
  {
    ParsedDataObject localParsedDataObject = new com/truecaller/insights/models/ParsedDataObject;
    localParsedDataObject.<init>();
    a(localParsedDataObject, paramParsedDataObject);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (ParsedDataObject)paramList.next();
      a(localParsedDataObject, (ParsedDataObject)localObject);
      String str1 = paramParsedDataObject.getState();
      String str2 = "SELF_TRANSFER";
      boolean bool2 = k.a(str1, str2);
      if (bool2) {
        str1 = "MERGED_SELF_TRANSFER";
      } else {
        str1 = "MERGED";
      }
      paramParsedDataObject.setState(str1);
      str1 = ((ParsedDataObject)localObject).getState();
      str2 = "SELF_TRANSFER";
      bool2 = k.a(str1, str2);
      if (bool2) {
        str1 = "MERGED_SELF_TRANSFER";
      } else {
        str1 = "MERGED";
      }
      ((ParsedDataObject)localObject).setState(str1);
      localObject = paramParsedDataObject.getState();
      str1 = "SELF_TRANSFER";
      bool1 = k.a(localObject, str1);
      if (!bool1)
      {
        localObject = paramParsedDataObject.getState();
        str1 = "MERGED_SELF_TRANSFER";
        bool1 = k.a(localObject, str1);
        if (!bool1)
        {
          localObject = "SYNTHETIC";
          break label190;
        }
      }
      localObject = "SYNTHETIC_SELF_TRANSFER";
      label190:
      localParsedDataObject.setState((String)localObject);
    }
    return localParsedDataObject;
  }
  
  private static void a(ParsedDataObject paramParsedDataObject1, ParsedDataObject paramParsedDataObject2)
  {
    Object localObject = (CharSequence)paramParsedDataObject2.getD();
    int i = ((CharSequence)localObject).length();
    int j = 1;
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getD();
    } else {
      localObject = paramParsedDataObject1.getD();
    }
    paramParsedDataObject1.setD((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getK();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getK();
    } else {
      localObject = paramParsedDataObject1.getK();
    }
    paramParsedDataObject1.setK((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getP();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getP();
    } else {
      localObject = paramParsedDataObject1.getP();
    }
    paramParsedDataObject1.setP((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getC();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getC();
    } else {
      localObject = paramParsedDataObject1.getD();
    }
    paramParsedDataObject1.setC((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getO();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getO();
    } else {
      localObject = paramParsedDataObject1.getO();
    }
    paramParsedDataObject1.setO((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getF();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getF();
    } else {
      localObject = paramParsedDataObject1.getF();
    }
    paramParsedDataObject1.setF((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getG();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getG();
    } else {
      localObject = paramParsedDataObject1.getG();
    }
    paramParsedDataObject1.setG((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getS();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getS();
    } else {
      localObject = paramParsedDataObject1.getS();
    }
    paramParsedDataObject1.setS((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getVal1();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getVal1();
    } else {
      localObject = paramParsedDataObject1.getVal1();
    }
    paramParsedDataObject1.setVal1((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getVal2();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getVal2();
    } else {
      localObject = paramParsedDataObject1.getVal2();
    }
    paramParsedDataObject1.setVal2((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getVal3();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getVal3();
    } else {
      localObject = paramParsedDataObject1.getVal3();
    }
    paramParsedDataObject1.setVal3((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getVal4();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getVal4();
    } else {
      localObject = paramParsedDataObject1.getVal4();
    }
    paramParsedDataObject1.setVal4((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getVal5();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getVal5();
    } else {
      localObject = paramParsedDataObject1.getVal5();
    }
    paramParsedDataObject1.setVal5((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getDatetime();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getDatetime();
    } else {
      localObject = paramParsedDataObject1.getDatetime();
    }
    paramParsedDataObject1.setDatetime((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getAddress();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getAddress();
    } else {
      localObject = paramParsedDataObject1.getAddress();
    }
    paramParsedDataObject1.setAddress((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getMsgdatetime();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getMsgdatetime();
    } else {
      localObject = paramParsedDataObject1.getMsgdatetime();
    }
    paramParsedDataObject1.setMsgdatetime((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getDate();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getDate();
    } else {
      localObject = paramParsedDataObject1.getDate();
    }
    paramParsedDataObject1.setDate((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getMsgdate();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getMsgdate();
    } else {
      localObject = paramParsedDataObject1.getMsgdate();
    }
    paramParsedDataObject1.setMsgdate((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getDiffVal1();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getDiffVal1();
    } else {
      localObject = paramParsedDataObject1.getDiffVal1();
    }
    paramParsedDataObject1.setDiffVal1((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getDiffVal2();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getDiffVal2();
    } else {
      localObject = paramParsedDataObject1.getDiffVal2();
    }
    paramParsedDataObject1.setDiffVal2((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getDiffVal3();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getDiffVal3();
    } else {
      localObject = paramParsedDataObject1.getDiffVal3();
    }
    paramParsedDataObject1.setDiffVal3((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getDiffVal4();
    i = ((CharSequence)localObject).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      localObject = paramParsedDataObject2.getDiffVal4();
    } else {
      localObject = paramParsedDataObject1.getDiffVal4();
    }
    paramParsedDataObject1.setDiffVal4((String)localObject);
    localObject = (CharSequence)paramParsedDataObject2.getDiffVal5();
    i = ((CharSequence)localObject).length();
    if (i <= 0) {
      j = 0;
    }
    if (j != 0) {
      paramParsedDataObject2 = paramParsedDataObject2.getDiffVal5();
    } else {
      paramParsedDataObject2 = paramParsedDataObject1.getDiffVal5();
    }
    paramParsedDataObject1.setDiffVal5(paramParsedDataObject2);
  }
  
  static void b(ParsedDataObject paramParsedDataObject, List paramList)
  {
    String str = "SELF_TRANSFER";
    paramParsedDataObject.setState(str);
    paramList = (Iterable)paramList;
    paramParsedDataObject = paramList.iterator();
    for (;;)
    {
      boolean bool = paramParsedDataObject.hasNext();
      if (!bool) {
        break;
      }
      paramList = (ParsedDataObject)paramParsedDataObject.next();
      str = "SELF_TRANSFER";
      paramList.setState(str);
    }
  }
  
  public final void a(HashMap paramHashMap)
  {
    if (paramHashMap != null)
    {
      Object localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject1 = (List)localObject1;
      paramHashMap = ((Map)paramHashMap).entrySet().iterator();
      for (;;)
      {
        boolean bool = paramHashMap.hasNext();
        if (!bool) {
          break;
        }
        localObject2 = (Map.Entry)paramHashMap.next();
        long l1 = ((Number)((Map.Entry)localObject2).getKey()).longValue();
        localObject2 = ((Map.Entry)localObject2).getValue();
        Object localObject3 = localObject2;
        localObject3 = (com.twelfthmile.c.b.b.b)localObject2;
        localObject2 = ((com.twelfthmile.c.b.b.b)localObject3).a();
        localObject4 = "childrenIds.linkingIds";
        k.a(localObject2, (String)localObject4);
        localObject2 = (Collection)localObject2;
        bool = ((Collection)localObject2).isEmpty() ^ true;
        if (bool)
        {
          localObject2 = ((com.twelfthmile.c.b.b.b)localObject3).a();
          localObject4 = null;
          localObject2 = (Long)((List)localObject2).get(0);
          if (localObject2 != null)
          {
            long l2 = ((Long)localObject2).longValue();
            bool = l1 < l2;
            if (!bool)
            {
              localObject2 = ((com.twelfthmile.c.b.b.b)localObject3).a();
              ((List)localObject2).remove(0);
            }
          }
        }
        c.d.f localf = c;
        Object localObject5 = new com/truecaller/insights/c/b$a;
        localObject2 = localObject5;
        localObject4 = this;
        ((b.a)localObject5).<init>(this, l1, (com.twelfthmile.c.b.b.b)localObject3, (List)localObject1, null);
        localObject5 = (m)localObject5;
        kotlinx.coroutines.f.a(localf, (m)localObject5);
      }
      paramHashMap = c;
      Object localObject2 = new com/truecaller/insights/c/b$b;
      Object localObject4 = null;
      ((b.b)localObject2).<init>(this, (List)localObject1, null);
      localObject2 = (m)localObject2;
      kotlinx.coroutines.f.a(paramHashMap, (m)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */