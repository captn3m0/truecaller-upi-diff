package com.truecaller.insights.b;

import com.truecaller.log.UnmutedException.InsightsExceptions;
import com.truecaller.log.UnmutedException.InsightsExceptions.Cause;
import com.truecaller.log.d;

public final class a
{
  public static final void a(String paramString)
  {
    UnmutedException.InsightsExceptions localInsightsExceptions = new com/truecaller/log/UnmutedException$InsightsExceptions;
    UnmutedException.InsightsExceptions.Cause localCause = UnmutedException.InsightsExceptions.Cause.PARSER_EXCEPTION;
    localInsightsExceptions.<init>(localCause);
    d.a((Throwable)localInsightsExceptions, paramString);
  }
  
  public static final void b(String paramString)
  {
    UnmutedException.InsightsExceptions localInsightsExceptions = new com/truecaller/log/UnmutedException$InsightsExceptions;
    UnmutedException.InsightsExceptions.Cause localCause = UnmutedException.InsightsExceptions.Cause.BINDER_EXCEPTION;
    localInsightsExceptions.<init>(localCause);
    d.a((Throwable)localInsightsExceptions, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */