package com.truecaller.insights.core.smscategorizer;

import android.util.Patterns;
import java.util.regex.Pattern;

public final class c
{
  private static final String a;
  private static final Pattern b = Pattern.compile("(\\d{2}[-\\s]\\w{3}[-\\s]\\d{2,4})|(\\d{2}-\\d{2}-\\d{4})");
  private static final Pattern c = Pattern.compile("(inr|INR|Rs|rupees|dollars|usd|paisa)");
  private static final Pattern d = Pattern.compile("\\s");
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("(\\b(");
    String str = Patterns.PHONE.toString();
    localStringBuilder.append(str);
    localStringBuilder.append(")\\b)");
    a = localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */