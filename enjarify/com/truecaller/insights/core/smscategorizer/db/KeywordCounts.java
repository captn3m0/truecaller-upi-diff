package com.truecaller.insights.core.smscategorizer.db;

import android.content.ContentValues;
import c.g.b.k;

public final class KeywordCounts
{
  private int hamValue;
  private int id;
  public String keyName;
  private int otpValue;
  private int pamValue;
  private int ranValue;
  
  public final int getHamValue()
  {
    return hamValue;
  }
  
  public final int getId()
  {
    return id;
  }
  
  public final String getKeyName()
  {
    String str1 = keyName;
    if (str1 == null)
    {
      String str2 = "keyName";
      k.a(str2);
    }
    return str1;
  }
  
  public final int getOtpValue()
  {
    return otpValue;
  }
  
  public final int getPamValue()
  {
    return pamValue;
  }
  
  public final int getRanValue()
  {
    return ranValue;
  }
  
  public final void setHamValue(int paramInt)
  {
    hamValue = paramInt;
  }
  
  public final void setId(int paramInt)
  {
    id = paramInt;
  }
  
  public final void setKeyName(String paramString)
  {
    k.b(paramString, "<set-?>");
    keyName = paramString;
  }
  
  public final void setOtpValue(int paramInt)
  {
    otpValue = paramInt;
  }
  
  public final void setPamValue(int paramInt)
  {
    pamValue = paramInt;
  }
  
  public final void setRanValue(int paramInt)
  {
    ranValue = paramInt;
  }
  
  public final ContentValues toContentValue()
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    String str1 = "key_name";
    Object localObject = keyName;
    if (localObject == null)
    {
      String str2 = "keyName";
      k.a(str2);
    }
    localContentValues.put(str1, (String)localObject);
    localObject = Integer.valueOf(ranValue);
    localContentValues.put("ran_value", (Integer)localObject);
    localObject = Integer.valueOf(pamValue);
    localContentValues.put("pam_value", (Integer)localObject);
    localObject = Integer.valueOf(hamValue);
    localContentValues.put("ham_value", (Integer)localObject);
    localObject = Integer.valueOf(otpValue);
    localContentValues.put("otp_value", (Integer)localObject);
    return localContentValues;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.KeywordCounts
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */