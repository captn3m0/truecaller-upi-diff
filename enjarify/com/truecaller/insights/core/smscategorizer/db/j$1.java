package com.truecaller.insights.core.smscategorizer.db;

import android.arch.persistence.room.c;
import android.arch.persistence.room.f;

final class j$1
  extends c
{
  j$1(j paramj, f paramf)
  {
    super(paramf);
  }
  
  public final String a()
  {
    return "INSERT OR REPLACE INTO `metaData`(`id`,`keyword`,`instances`,`words_count`) VALUES (nullif(?, 0),?,?,?)";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.j.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */