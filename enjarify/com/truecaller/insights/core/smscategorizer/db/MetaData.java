package com.truecaller.insights.core.smscategorizer.db;

import c.g.b.k;

public final class MetaData
{
  private int id;
  private int instances;
  private String keyword = "";
  private int wordsCount;
  
  public final int getId()
  {
    return id;
  }
  
  public final int getInstances()
  {
    return instances;
  }
  
  public final String getKeyword()
  {
    return keyword;
  }
  
  public final int getWordsCount()
  {
    return wordsCount;
  }
  
  public final void setId(int paramInt)
  {
    id = paramInt;
  }
  
  public final void setInstances(int paramInt)
  {
    instances = paramInt;
  }
  
  public final void setKeyword(String paramString)
  {
    k.b(paramString, "<set-?>");
    keyword = paramString;
  }
  
  public final void setWordsCount(int paramInt)
  {
    wordsCount = paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.MetaData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */