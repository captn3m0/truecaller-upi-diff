package com.truecaller.insights.core.smscategorizer.db;

import android.arch.persistence.db.b;
import c.g.b.k;

public final class e
  extends android.arch.persistence.room.a.a
{
  private final a c;
  
  public e(a parama)
  {
    super(1, 2);
    c = parama;
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "database");
    paramb.c("DELETE FROM keywordCounts");
    c.b(paramb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */