package com.truecaller.insights.core.smscategorizer.db;

import android.arch.persistence.room.b.b.a;
import android.arch.persistence.room.b.b.d;
import android.arch.persistence.room.f.b;
import android.arch.persistence.room.h.a;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class SmsCategorizerDb_Impl$1
  extends h.a
{
  SmsCategorizerDb_Impl$1(SmsCategorizerDb_Impl paramSmsCategorizerDb_Impl)
  {
    super(2);
  }
  
  public final void a(android.arch.persistence.db.b paramb)
  {
    paramb.c("DROP TABLE IF EXISTS `keywordCounts`");
    paramb.c("DROP TABLE IF EXISTS `metaData`");
    paramb.c("DROP TABLE IF EXISTS `StopWord`");
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    paramb.c("CREATE TABLE IF NOT EXISTS `keywordCounts` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `key_name` TEXT NOT NULL, `ran_value` INTEGER NOT NULL, `pam_value` INTEGER NOT NULL, `ham_value` INTEGER NOT NULL, `otp_value` INTEGER NOT NULL)");
    paramb.c("CREATE UNIQUE INDEX `index_keywordCounts_key_name` ON `keywordCounts` (`key_name`)");
    paramb.c("CREATE TABLE IF NOT EXISTS `metaData` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `keyword` TEXT NOT NULL, `instances` INTEGER NOT NULL, `words_count` INTEGER NOT NULL)");
    paramb.c("CREATE UNIQUE INDEX `index_metaData_keyword` ON `metaData` (`keyword`)");
    paramb.c("CREATE TABLE IF NOT EXISTS `StopWord` (`word` TEXT NOT NULL, PRIMARY KEY(`word`))");
    paramb.c("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    paramb.c("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"b652c181b86d452026f63cc52bb5f746\")");
  }
  
  public final void c(android.arch.persistence.db.b paramb)
  {
    SmsCategorizerDb_Impl.a(b, paramb);
    SmsCategorizerDb_Impl.b(b, paramb);
    List localList1 = SmsCategorizerDb_Impl.d(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = SmsCategorizerDb_Impl.e(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)SmsCategorizerDb_Impl.f(b).get(i);
        localb.b(paramb);
        i += 1;
      }
    }
  }
  
  public final void d(android.arch.persistence.db.b paramb)
  {
    List localList1 = SmsCategorizerDb_Impl.a(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = SmsCategorizerDb_Impl.b(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)SmsCategorizerDb_Impl.c(b).get(i);
        localb.a(paramb);
        i += 1;
      }
    }
  }
  
  public final void e(android.arch.persistence.db.b paramb)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>(6);
    Object localObject2 = new android/arch/persistence/room/b/b$a;
    int i = 1;
    ((b.a)localObject2).<init>("id", "INTEGER", i, i);
    ((HashMap)localObject1).put("id", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("key_name", "TEXT", i, 0);
    ((HashMap)localObject1).put("key_name", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("ran_value", "INTEGER", i, 0);
    ((HashMap)localObject1).put("ran_value", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("pam_value", "INTEGER", i, 0);
    ((HashMap)localObject1).put("pam_value", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("ham_value", "INTEGER", i, 0);
    ((HashMap)localObject1).put("ham_value", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("otp_value", "INTEGER", i, 0);
    ((HashMap)localObject1).put("otp_value", localObject2);
    Object localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>(0);
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>(i);
    Object localObject4 = new android/arch/persistence/room/b/b$d;
    List localList = Arrays.asList(new String[] { "key_name" });
    ((b.d)localObject4).<init>("index_keywordCounts_key_name", i, localList);
    ((HashSet)localObject2).add(localObject4);
    localObject4 = new android/arch/persistence/room/b/b;
    String str = "keywordCounts";
    ((android.arch.persistence.room.b.b)localObject4).<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
    localObject1 = android.arch.persistence.room.b.b.a(paramb, "keywordCounts");
    boolean bool1 = ((android.arch.persistence.room.b.b)localObject4).equals(localObject1);
    if (bool1)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>(4);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("id", "INTEGER", i, i);
      ((HashMap)localObject1).put("id", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("keyword", "TEXT", i, 0);
      ((HashMap)localObject1).put("keyword", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("instances", "INTEGER", i, 0);
      ((HashMap)localObject1).put("instances", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("words_count", "INTEGER", i, 0);
      ((HashMap)localObject1).put("words_count", localObject2);
      localObject3 = new java/util/HashSet;
      ((HashSet)localObject3).<init>(0);
      localObject2 = new java/util/HashSet;
      ((HashSet)localObject2).<init>(i);
      localObject4 = new android/arch/persistence/room/b/b$d;
      localList = Arrays.asList(new String[] { "keyword" });
      ((b.d)localObject4).<init>("index_metaData_keyword", i, localList);
      ((HashSet)localObject2).add(localObject4);
      localObject4 = new android/arch/persistence/room/b/b;
      str = "metaData";
      ((android.arch.persistence.room.b.b)localObject4).<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
      localObject1 = android.arch.persistence.room.b.b.a(paramb, "metaData");
      bool1 = ((android.arch.persistence.room.b.b)localObject4).equals(localObject1);
      if (bool1)
      {
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>(i);
        localObject2 = new android/arch/persistence/room/b/b$a;
        ((b.a)localObject2).<init>("word", "TEXT", i, i);
        ((HashMap)localObject1).put("word", localObject2);
        localObject3 = new java/util/HashSet;
        ((HashSet)localObject3).<init>(0);
        localObject2 = new java/util/HashSet;
        ((HashSet)localObject2).<init>(0);
        localObject4 = new android/arch/persistence/room/b/b;
        str = "StopWord";
        ((android.arch.persistence.room.b.b)localObject4).<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
        localObject1 = "StopWord";
        paramb = android.arch.persistence.room.b.b.a(paramb, (String)localObject1);
        boolean bool2 = ((android.arch.persistence.room.b.b)localObject4).equals(paramb);
        if (bool2) {
          return;
        }
        localObject1 = new java/lang/IllegalStateException;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("Migration didn't properly handle StopWord(com.truecaller.insights.core.smscategorizer.db.StopWord).\n Expected:\n");
        ((StringBuilder)localObject3).append(localObject4);
        ((StringBuilder)localObject3).append("\n Found:\n");
        ((StringBuilder)localObject3).append(paramb);
        paramb = ((StringBuilder)localObject3).toString();
        ((IllegalStateException)localObject1).<init>(paramb);
        throw ((Throwable)localObject1);
      }
      paramb = new java/lang/IllegalStateException;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Migration didn't properly handle metaData(com.truecaller.insights.core.smscategorizer.db.MetaData).\n Expected:\n");
      ((StringBuilder)localObject3).append(localObject4);
      ((StringBuilder)localObject3).append("\n Found:\n");
      ((StringBuilder)localObject3).append(localObject1);
      localObject1 = ((StringBuilder)localObject3).toString();
      paramb.<init>((String)localObject1);
      throw paramb;
    }
    paramb = new java/lang/IllegalStateException;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Migration didn't properly handle keywordCounts(com.truecaller.insights.core.smscategorizer.db.KeywordCounts).\n Expected:\n");
    ((StringBuilder)localObject3).append(localObject4);
    ((StringBuilder)localObject3).append("\n Found:\n");
    ((StringBuilder)localObject3).append(localObject1);
    localObject1 = ((StringBuilder)localObject3).toString();
    paramb.<init>((String)localObject1);
    throw paramb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.SmsCategorizerDb_Impl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */