package com.truecaller.insights.core.smscategorizer.db;

import android.arch.persistence.db.c;
import android.arch.persistence.db.c.a;
import android.arch.persistence.db.c.b;
import android.arch.persistence.db.c.b.a;
import android.arch.persistence.db.c.c;
import android.arch.persistence.room.a;
import android.arch.persistence.room.d;
import android.arch.persistence.room.h;
import android.arch.persistence.room.h.a;

public class SmsCategorizerDb_Impl
  extends SmsCategorizerDb
{
  private volatile i g;
  
  public final d a()
  {
    d locald = new android/arch/persistence/room/d;
    String[] tmp8_5 = new String[3];
    String[] tmp9_8 = tmp8_5;
    String[] tmp9_8 = tmp8_5;
    tmp9_8[0] = "keywordCounts";
    tmp9_8[1] = "metaData";
    tmp9_8[2] = "StopWord";
    String[] arrayOfString = tmp9_8;
    locald.<init>(this, arrayOfString);
    return locald;
  }
  
  public final c b(a parama)
  {
    Object localObject1 = new android/arch/persistence/room/h;
    Object localObject2 = new com/truecaller/insights/core/smscategorizer/db/SmsCategorizerDb_Impl$1;
    ((SmsCategorizerDb_Impl.1)localObject2).<init>(this);
    ((h)localObject1).<init>(parama, (h.a)localObject2, "b652c181b86d452026f63cc52bb5f746", "f9dd4c2516b8830e339d494730df964a");
    localObject2 = c.b.a(b);
    String str = c;
    b = str;
    c = ((c.a)localObject1);
    localObject1 = ((c.b.a)localObject2).a();
    return a.a((c.b)localObject1);
  }
  
  public final i h()
  {
    Object localObject1 = g;
    if (localObject1 != null) {
      return g;
    }
    try
    {
      localObject1 = g;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/insights/core/smscategorizer/db/j;
        ((j)localObject1).<init>(this);
        g = ((i)localObject1);
      }
      localObject1 = g;
      return (i)localObject1;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.SmsCategorizerDb_Impl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */