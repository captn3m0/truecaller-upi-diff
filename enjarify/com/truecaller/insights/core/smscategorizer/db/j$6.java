package com.truecaller.insights.core.smscategorizer.db;

import android.arch.persistence.room.f;

final class j$6
  extends android.arch.persistence.room.j
{
  j$6(j paramj, f paramf)
  {
    super(paramf);
  }
  
  public final String a()
  {
    return "UPDATE keywordCounts SET pam_value = pam_value + 5 WHERE key_name = ?";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.j.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */