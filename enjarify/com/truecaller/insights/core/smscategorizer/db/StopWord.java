package com.truecaller.insights.core.smscategorizer.db;

import c.g.b.k;

public final class StopWord
{
  private String word = "";
  
  public final String getWord()
  {
    return word;
  }
  
  public final void setWord(String paramString)
  {
    k.b(paramString, "<set-?>");
    word = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.StopWord
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */