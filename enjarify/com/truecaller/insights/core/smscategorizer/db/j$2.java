package com.truecaller.insights.core.smscategorizer.db;

import android.arch.persistence.room.c;
import android.arch.persistence.room.f;

final class j$2
  extends c
{
  j$2(j paramj, f paramf)
  {
    super(paramf);
  }
  
  public final String a()
  {
    return "INSERT OR REPLACE INTO `keywordCounts`(`id`,`key_name`,`ran_value`,`pam_value`,`ham_value`,`otp_value`) VALUES (nullif(?, 0),?,?,?,?,?)";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.j.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */