package com.truecaller.insights.core.smscategorizer.db;

import android.content.ContentValues;
import c.g.b.k;
import c.n.d;
import com.google.gson.f;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Iterator;

public final class b
  implements a
{
  private final f a;
  private final com.truecaller.insights.d.b b;
  
  public b(com.truecaller.insights.d.b paramb)
  {
    b = paramb;
    paramb = new com/google/gson/f;
    paramb.<init>();
    a = paramb;
  }
  
  /* Error */
  public final com.truecaller.insights.models.PdoBinderType a()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   6: astore_2
    //   7: ldc 30
    //   9: astore_3
    //   10: aload_2
    //   11: aload_3
    //   12: invokeinterface 35 2 0
    //   17: astore_2
    //   18: aload_0
    //   19: getfield 28	com/truecaller/insights/core/smscategorizer/db/b:a	Lcom/google/gson/f;
    //   22: astore_3
    //   23: aload_2
    //   24: astore 4
    //   26: aload_2
    //   27: checkcast 37	java/io/Reader
    //   30: astore 4
    //   32: ldc 39
    //   34: astore 5
    //   36: aload_3
    //   37: aload 4
    //   39: aload 5
    //   41: invokevirtual 42	com/google/gson/f:a	(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    //   44: astore_3
    //   45: aload_3
    //   46: checkcast 39	com/truecaller/insights/models/PdoBinderType$PdoBinder
    //   49: astore_3
    //   50: aload_0
    //   51: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   54: astore_1
    //   55: aload_2
    //   56: checkcast 44	java/io/Closeable
    //   59: astore_2
    //   60: aload_1
    //   61: aload_2
    //   62: invokeinterface 47 2 0
    //   67: aload_3
    //   68: astore_1
    //   69: goto +53 -> 122
    //   72: astore_1
    //   73: goto +10 -> 83
    //   76: astore 6
    //   78: aconst_null
    //   79: astore_2
    //   80: aload 6
    //   82: astore_1
    //   83: aload_0
    //   84: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   87: astore_3
    //   88: aload_2
    //   89: checkcast 44	java/io/Closeable
    //   92: astore_2
    //   93: aload_3
    //   94: aload_2
    //   95: invokeinterface 47 2 0
    //   100: aload_1
    //   101: athrow
    //   102: pop
    //   103: aconst_null
    //   104: astore_2
    //   105: aload_0
    //   106: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   109: astore_3
    //   110: aload_2
    //   111: checkcast 44	java/io/Closeable
    //   114: astore_2
    //   115: aload_3
    //   116: aload_2
    //   117: invokeinterface 47 2 0
    //   122: aload_1
    //   123: ifnull +8 -> 131
    //   126: aload_1
    //   127: checkcast 49	com/truecaller/insights/models/PdoBinderType
    //   130: areturn
    //   131: getstatic 54	com/truecaller/insights/models/PdoBinderType$a:a	Lcom/truecaller/insights/models/PdoBinderType$a;
    //   134: checkcast 49	com/truecaller/insights/models/PdoBinderType
    //   137: areturn
    //   138: pop
    //   139: goto -34 -> 105
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	142	0	this	b
    //   1	68	1	localObject1	Object
    //   72	1	1	localObject2	Object
    //   82	45	1	localObject3	Object
    //   6	111	2	localObject4	Object
    //   9	107	3	localObject5	Object
    //   24	14	4	localObject6	Object
    //   34	6	5	localClass	Class
    //   76	5	6	localObject7	Object
    //   102	1	9	localException1	Exception
    //   138	1	10	localException2	Exception
    // Exception table:
    //   from	to	target	type
    //   18	22	72	finally
    //   26	30	72	finally
    //   39	44	72	finally
    //   45	49	72	finally
    //   2	6	76	finally
    //   11	17	76	finally
    //   2	6	102	java/lang/Exception
    //   11	17	102	java/lang/Exception
    //   18	22	138	java/lang/Exception
    //   26	30	138	java/lang/Exception
    //   39	44	138	java/lang/Exception
    //   45	49	138	java/lang/Exception
  }
  
  /* Error */
  public final void a(android.arch.persistence.db.b paramb)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 58
    //   3: invokestatic 17	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aconst_null
    //   7: astore_2
    //   8: aload_0
    //   9: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   12: astore_3
    //   13: ldc 60
    //   15: astore 4
    //   17: aload_3
    //   18: aload 4
    //   20: invokeinterface 35 2 0
    //   25: astore_2
    //   26: aload_2
    //   27: ifnonnull +21 -> 48
    //   30: aload_0
    //   31: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   34: astore_1
    //   35: aload_2
    //   36: checkcast 44	java/io/Closeable
    //   39: astore_2
    //   40: aload_1
    //   41: aload_2
    //   42: invokeinterface 47 2 0
    //   47: return
    //   48: aload_2
    //   49: invokevirtual 66	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   52: astore_3
    //   53: aload_3
    //   54: ifnull +493 -> 547
    //   57: aload_3
    //   58: checkcast 68	java/lang/CharSequence
    //   61: astore_3
    //   62: aload_3
    //   63: invokeinterface 72 1 0
    //   68: istore 5
    //   70: iconst_1
    //   71: istore 6
    //   73: iload 5
    //   75: iload 6
    //   77: isub
    //   78: istore 5
    //   80: aconst_null
    //   81: astore 7
    //   83: iload 5
    //   85: istore 8
    //   87: iconst_0
    //   88: istore 5
    //   90: aconst_null
    //   91: astore 4
    //   93: iconst_0
    //   94: istore 9
    //   96: aconst_null
    //   97: astore 10
    //   99: iload 5
    //   101: iload 8
    //   103: if_icmpgt +88 -> 191
    //   106: iload 9
    //   108: ifne +10 -> 118
    //   111: iload 5
    //   113: istore 11
    //   115: goto +7 -> 122
    //   118: iload 8
    //   120: istore 11
    //   122: aload_3
    //   123: iload 11
    //   125: invokeinterface 77 2 0
    //   130: istore 11
    //   132: bipush 32
    //   134: istore 12
    //   136: iload 11
    //   138: iload 12
    //   140: if_icmpgt +9 -> 149
    //   143: iconst_1
    //   144: istore 11
    //   146: goto +6 -> 152
    //   149: iconst_0
    //   150: istore 11
    //   152: iload 9
    //   154: ifne +23 -> 177
    //   157: iload 11
    //   159: ifne +9 -> 168
    //   162: iconst_1
    //   163: istore 9
    //   165: goto -66 -> 99
    //   168: iload 5
    //   170: iconst_1
    //   171: iadd
    //   172: istore 5
    //   174: goto -75 -> 99
    //   177: iload 11
    //   179: ifeq +12 -> 191
    //   182: iload 8
    //   184: iconst_m1
    //   185: iadd
    //   186: istore 8
    //   188: goto -89 -> 99
    //   191: iload 8
    //   193: iconst_1
    //   194: iadd
    //   195: istore 8
    //   197: aload_3
    //   198: iload 5
    //   200: iload 8
    //   202: invokeinterface 82 3 0
    //   207: astore_3
    //   208: aload_3
    //   209: invokevirtual 85	java/lang/Object:toString	()Ljava/lang/String;
    //   212: astore_3
    //   213: aload_3
    //   214: checkcast 68	java/lang/CharSequence
    //   217: astore_3
    //   218: ldc 87
    //   220: astore 4
    //   222: new 89	c/n/k
    //   225: astore 13
    //   227: aload 13
    //   229: aload 4
    //   231: invokespecial 92	c/n/k:<init>	(Ljava/lang/String;)V
    //   234: aload 13
    //   236: aload_3
    //   237: iconst_0
    //   238: invokevirtual 95	c/n/k:a	(Ljava/lang/CharSequence;I)Ljava/util/List;
    //   241: astore_3
    //   242: aload_3
    //   243: invokeinterface 101 1 0
    //   248: istore 5
    //   250: iload 5
    //   252: ifne +116 -> 368
    //   255: aload_3
    //   256: invokeinterface 104 1 0
    //   261: istore 5
    //   263: aload_3
    //   264: iload 5
    //   266: invokeinterface 108 2 0
    //   271: astore 4
    //   273: aload 4
    //   275: invokeinterface 113 1 0
    //   280: istore 8
    //   282: iload 8
    //   284: ifeq +84 -> 368
    //   287: aload 4
    //   289: invokeinterface 117 1 0
    //   294: astore 13
    //   296: aload 13
    //   298: checkcast 119	java/lang/String
    //   301: astore 13
    //   303: aload 13
    //   305: checkcast 68	java/lang/CharSequence
    //   308: astore 13
    //   310: aload 13
    //   312: invokeinterface 72 1 0
    //   317: istore 8
    //   319: iload 8
    //   321: ifne +9 -> 330
    //   324: iconst_1
    //   325: istore 8
    //   327: goto +9 -> 336
    //   330: iconst_0
    //   331: istore 8
    //   333: aconst_null
    //   334: astore 13
    //   336: iload 8
    //   338: ifne -65 -> 273
    //   341: aload_3
    //   342: checkcast 121	java/lang/Iterable
    //   345: astore_3
    //   346: aload 4
    //   348: invokeinterface 124 1 0
    //   353: iload 6
    //   355: iadd
    //   356: istore 5
    //   358: aload_3
    //   359: iload 5
    //   361: invokestatic 130	c/a/m:d	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   364: astore_3
    //   365: goto +12 -> 377
    //   368: getstatic 135	c/a/y:a	Lc/a/y;
    //   371: astore_3
    //   372: aload_3
    //   373: checkcast 97	java/util/List
    //   376: astore_3
    //   377: aload_3
    //   378: checkcast 137	java/util/Collection
    //   381: astore_3
    //   382: aload_3
    //   383: ifnull +150 -> 533
    //   386: iconst_0
    //   387: anewarray 119	java/lang/String
    //   390: astore 4
    //   392: aload_3
    //   393: aload 4
    //   395: invokeinterface 141 2 0
    //   400: astore_3
    //   401: aload_3
    //   402: ifnull +117 -> 519
    //   405: aload_3
    //   406: checkcast 143	[Ljava/lang/String;
    //   409: astore_3
    //   410: aload_3
    //   411: arraylength
    //   412: istore 5
    //   414: iconst_3
    //   415: istore 8
    //   417: iload 5
    //   419: iload 8
    //   421: if_icmpeq +6 -> 427
    //   424: goto -394 -> 30
    //   427: ldc -110
    //   429: astore 4
    //   431: new 148	android/content/ContentValues
    //   434: astore 13
    //   436: aload 13
    //   438: invokespecial 149	android/content/ContentValues:<init>	()V
    //   441: ldc -105
    //   443: astore 10
    //   445: aload_3
    //   446: iconst_0
    //   447: aaload
    //   448: astore 7
    //   450: aload 13
    //   452: aload 10
    //   454: aload 7
    //   456: invokevirtual 155	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   459: ldc -99
    //   461: astore 7
    //   463: aload_3
    //   464: iload 6
    //   466: aaload
    //   467: astore 10
    //   469: aload 13
    //   471: aload 7
    //   473: aload 10
    //   475: invokevirtual 155	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   478: ldc -97
    //   480: astore 7
    //   482: iconst_2
    //   483: istore 9
    //   485: aload_3
    //   486: iload 9
    //   488: aaload
    //   489: astore_3
    //   490: aload 13
    //   492: aload 7
    //   494: aload_3
    //   495: invokevirtual 155	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   498: aload_1
    //   499: aload 4
    //   501: iload 6
    //   503: aload 13
    //   505: invokeinterface 165 4 0
    //   510: pop2
    //   511: aload_2
    //   512: invokevirtual 66	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   515: astore_3
    //   516: goto -463 -> 53
    //   519: new 167	c/u
    //   522: astore_1
    //   523: ldc -87
    //   525: astore_3
    //   526: aload_1
    //   527: aload_3
    //   528: invokespecial 170	c/u:<init>	(Ljava/lang/String;)V
    //   531: aload_1
    //   532: athrow
    //   533: new 167	c/u
    //   536: astore_1
    //   537: ldc -84
    //   539: astore_3
    //   540: aload_1
    //   541: aload_3
    //   542: invokespecial 170	c/u:<init>	(Ljava/lang/String;)V
    //   545: aload_1
    //   546: athrow
    //   547: aload_0
    //   548: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   551: astore_1
    //   552: aload_2
    //   553: checkcast 44	java/io/Closeable
    //   556: astore_2
    //   557: aload_1
    //   558: aload_2
    //   559: invokeinterface 47 2 0
    //   564: return
    //   565: astore_1
    //   566: aload_0
    //   567: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   570: astore_3
    //   571: aload_2
    //   572: checkcast 44	java/io/Closeable
    //   575: astore_2
    //   576: aload_3
    //   577: aload_2
    //   578: invokeinterface 47 2 0
    //   583: aload_1
    //   584: athrow
    //   585: pop
    //   586: goto -39 -> 547
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	589	0	this	b
    //   0	589	1	paramb	android.arch.persistence.db.b
    //   7	571	2	localObject1	Object
    //   12	565	3	localObject2	Object
    //   15	485	4	localObject3	Object
    //   68	131	5	i	int
    //   248	3	5	bool1	boolean
    //   261	161	5	j	int
    //   71	431	6	k	int
    //   81	412	7	str1	String
    //   85	116	8	m	int
    //   280	3	8	bool2	boolean
    //   317	105	8	n	int
    //   94	393	9	i1	int
    //   97	377	10	str2	String
    //   113	65	11	i2	int
    //   134	7	12	i3	int
    //   225	279	13	localObject4	Object
    //   585	1	18	localIOException	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   8	12	565	finally
    //   18	25	565	finally
    //   48	52	565	finally
    //   57	61	565	finally
    //   62	68	565	finally
    //   123	130	565	finally
    //   200	207	565	finally
    //   208	212	565	finally
    //   213	217	565	finally
    //   222	225	565	finally
    //   229	234	565	finally
    //   237	241	565	finally
    //   242	248	565	finally
    //   255	261	565	finally
    //   264	271	565	finally
    //   273	280	565	finally
    //   287	294	565	finally
    //   296	301	565	finally
    //   303	308	565	finally
    //   310	317	565	finally
    //   341	345	565	finally
    //   346	353	565	finally
    //   359	364	565	finally
    //   368	371	565	finally
    //   372	376	565	finally
    //   377	381	565	finally
    //   386	390	565	finally
    //   393	400	565	finally
    //   405	409	565	finally
    //   410	412	565	finally
    //   431	434	565	finally
    //   436	441	565	finally
    //   446	448	565	finally
    //   454	459	565	finally
    //   464	467	565	finally
    //   473	478	565	finally
    //   486	489	565	finally
    //   494	498	565	finally
    //   503	511	565	finally
    //   511	515	565	finally
    //   519	522	565	finally
    //   527	531	565	finally
    //   531	533	565	finally
    //   533	536	565	finally
    //   541	545	565	finally
    //   545	547	565	finally
    //   8	12	585	java/io/IOException
    //   18	25	585	java/io/IOException
    //   48	52	585	java/io/IOException
    //   57	61	585	java/io/IOException
    //   62	68	585	java/io/IOException
    //   123	130	585	java/io/IOException
    //   200	207	585	java/io/IOException
    //   208	212	585	java/io/IOException
    //   213	217	585	java/io/IOException
    //   222	225	585	java/io/IOException
    //   229	234	585	java/io/IOException
    //   237	241	585	java/io/IOException
    //   242	248	585	java/io/IOException
    //   255	261	585	java/io/IOException
    //   264	271	585	java/io/IOException
    //   273	280	585	java/io/IOException
    //   287	294	585	java/io/IOException
    //   296	301	585	java/io/IOException
    //   303	308	585	java/io/IOException
    //   310	317	585	java/io/IOException
    //   341	345	585	java/io/IOException
    //   346	353	585	java/io/IOException
    //   359	364	585	java/io/IOException
    //   368	371	585	java/io/IOException
    //   372	376	585	java/io/IOException
    //   377	381	585	java/io/IOException
    //   386	390	585	java/io/IOException
    //   393	400	585	java/io/IOException
    //   405	409	585	java/io/IOException
    //   410	412	585	java/io/IOException
    //   431	434	585	java/io/IOException
    //   436	441	585	java/io/IOException
    //   446	448	585	java/io/IOException
    //   454	459	585	java/io/IOException
    //   464	467	585	java/io/IOException
    //   473	478	585	java/io/IOException
    //   486	489	585	java/io/IOException
    //   494	498	585	java/io/IOException
    //   503	511	585	java/io/IOException
    //   511	515	585	java/io/IOException
    //   519	522	585	java/io/IOException
    //   527	531	585	java/io/IOException
    //   531	533	585	java/io/IOException
    //   533	536	585	java/io/IOException
    //   541	545	585	java/io/IOException
    //   545	547	585	java/io/IOException
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    k.b(paramb, "db");
    Object localObject1 = null;
    try
    {
      localObject2 = b;
      Object localObject3 = "countData.json";
      localObject1 = ((com.truecaller.insights.d.b)localObject2).b((String)localObject3);
      if (localObject1 == null)
      {
        paramb = b;
        localObject1 = (Closeable)localObject1;
        paramb.a((Closeable)localObject1);
        return;
      }
      localObject2 = a;
      localObject3 = d.a;
      Object localObject4 = new java/io/InputStreamReader;
      ((InputStreamReader)localObject4).<init>((InputStream)localObject1, (Charset)localObject3);
      localObject4 = (Reader)localObject4;
      localObject3 = new java/io/BufferedReader;
      int i = 8192;
      ((BufferedReader)localObject3).<init>((Reader)localObject4, i);
      localObject3 = (Reader)localObject3;
      localObject4 = KeyWordsList.class;
      localObject2 = ((f)localObject2).a((Reader)localObject3, (Class)localObject4);
      localObject2 = (KeyWordsList)localObject2;
      if (localObject2 != null)
      {
        localObject2 = ((KeyWordsList)localObject2).getKeyWordsList$insights_release();
        if (localObject2 != null)
        {
          localObject2 = (Iterable)localObject2;
          localObject2 = ((Iterable)localObject2).iterator();
          for (;;)
          {
            boolean bool = ((Iterator)localObject2).hasNext();
            if (!bool) {
              break;
            }
            localObject3 = ((Iterator)localObject2).next();
            localObject3 = (KeywordCounts)localObject3;
            localObject4 = "keywordCounts";
            i = 1;
            localObject3 = ((KeywordCounts)localObject3).toContentValue();
            paramb.a((String)localObject4, i, (ContentValues)localObject3);
          }
        }
      }
      paramb = b;
      localObject1 = (Closeable)localObject1;
      paramb.a((Closeable)localObject1);
      return;
    }
    finally
    {
      Object localObject2 = b;
      localObject1 = (Closeable)localObject1;
      ((com.truecaller.insights.d.b)localObject2).a((Closeable)localObject1);
    }
  }
  
  /* Error */
  public final void c(android.arch.persistence.db.b paramb)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 58
    //   3: invokestatic 17	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aconst_null
    //   7: astore_2
    //   8: aload_0
    //   9: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   12: astore_3
    //   13: ldc -35
    //   15: astore 4
    //   17: aload_3
    //   18: aload 4
    //   20: invokeinterface 179 2 0
    //   25: astore_2
    //   26: aload_2
    //   27: ifnonnull +21 -> 48
    //   30: aload_0
    //   31: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   34: astore_1
    //   35: aload_2
    //   36: checkcast 44	java/io/Closeable
    //   39: astore_2
    //   40: aload_1
    //   41: aload_2
    //   42: invokeinterface 47 2 0
    //   47: return
    //   48: aload_0
    //   49: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   52: astore_3
    //   53: aload_3
    //   54: aload_2
    //   55: invokeinterface 224 2 0
    //   60: astore_3
    //   61: ldc -30
    //   63: astore 4
    //   65: ldc -28
    //   67: astore 5
    //   69: aload_3
    //   70: aload 4
    //   72: aload 5
    //   74: invokestatic 233	c/n/m:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   77: astore_3
    //   78: ldc -21
    //   80: astore 4
    //   82: ldc -28
    //   84: astore 5
    //   86: aload_3
    //   87: aload 4
    //   89: aload 5
    //   91: invokestatic 233	c/n/m:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   94: astore_3
    //   95: aload_3
    //   96: checkcast 68	java/lang/CharSequence
    //   99: astore_3
    //   100: ldc 87
    //   102: astore 4
    //   104: new 89	c/n/k
    //   107: astore 5
    //   109: aload 5
    //   111: aload 4
    //   113: invokespecial 92	c/n/k:<init>	(Ljava/lang/String;)V
    //   116: aconst_null
    //   117: astore 4
    //   119: aload 5
    //   121: aload_3
    //   122: iconst_0
    //   123: invokevirtual 95	c/n/k:a	(Ljava/lang/CharSequence;I)Ljava/util/List;
    //   126: astore_3
    //   127: aload_3
    //   128: invokeinterface 101 1 0
    //   133: istore 6
    //   135: iconst_1
    //   136: istore 7
    //   138: iload 6
    //   140: ifne +116 -> 256
    //   143: aload_3
    //   144: invokeinterface 104 1 0
    //   149: istore 6
    //   151: aload_3
    //   152: iload 6
    //   154: invokeinterface 108 2 0
    //   159: astore 5
    //   161: aload 5
    //   163: invokeinterface 113 1 0
    //   168: istore 8
    //   170: iload 8
    //   172: ifeq +84 -> 256
    //   175: aload 5
    //   177: invokeinterface 117 1 0
    //   182: astore 9
    //   184: aload 9
    //   186: checkcast 119	java/lang/String
    //   189: astore 9
    //   191: aload 9
    //   193: checkcast 68	java/lang/CharSequence
    //   196: astore 9
    //   198: aload 9
    //   200: invokeinterface 72 1 0
    //   205: istore 8
    //   207: iload 8
    //   209: ifne +9 -> 218
    //   212: iconst_1
    //   213: istore 8
    //   215: goto +9 -> 224
    //   218: iconst_0
    //   219: istore 8
    //   221: aconst_null
    //   222: astore 9
    //   224: iload 8
    //   226: ifne -65 -> 161
    //   229: aload_3
    //   230: checkcast 121	java/lang/Iterable
    //   233: astore_3
    //   234: aload 5
    //   236: invokeinterface 124 1 0
    //   241: iload 7
    //   243: iadd
    //   244: istore 6
    //   246: aload_3
    //   247: iload 6
    //   249: invokestatic 130	c/a/m:d	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   252: astore_3
    //   253: goto +12 -> 265
    //   256: getstatic 135	c/a/y:a	Lc/a/y;
    //   259: astore_3
    //   260: aload_3
    //   261: checkcast 97	java/util/List
    //   264: astore_3
    //   265: aload_3
    //   266: checkcast 137	java/util/Collection
    //   269: astore_3
    //   270: aload_3
    //   271: ifnull +176 -> 447
    //   274: iconst_0
    //   275: anewarray 119	java/lang/String
    //   278: astore 5
    //   280: aload_3
    //   281: aload 5
    //   283: invokeinterface 141 2 0
    //   288: astore_3
    //   289: aload_3
    //   290: ifnull +143 -> 433
    //   293: aload_3
    //   294: arraylength
    //   295: istore 6
    //   297: iconst_0
    //   298: istore 8
    //   300: aconst_null
    //   301: astore 9
    //   303: iload 8
    //   305: iload 6
    //   307: if_icmpge +108 -> 415
    //   310: aload_3
    //   311: iload 8
    //   313: aaload
    //   314: astore 10
    //   316: aload 10
    //   318: checkcast 119	java/lang/String
    //   321: astore 10
    //   323: aload 10
    //   325: astore 11
    //   327: aload 10
    //   329: checkcast 68	java/lang/CharSequence
    //   332: astore 11
    //   334: aload 11
    //   336: invokeinterface 72 1 0
    //   341: istore 12
    //   343: iload 12
    //   345: ifle +9 -> 354
    //   348: iconst_1
    //   349: istore 12
    //   351: goto +9 -> 360
    //   354: iconst_0
    //   355: istore 12
    //   357: aconst_null
    //   358: astore 11
    //   360: iload 12
    //   362: ifeq +43 -> 405
    //   365: ldc -19
    //   367: astore 11
    //   369: new 148	android/content/ContentValues
    //   372: astore 13
    //   374: aload 13
    //   376: invokespecial 149	android/content/ContentValues:<init>	()V
    //   379: ldc -17
    //   381: astore 14
    //   383: aload 13
    //   385: aload 14
    //   387: aload 10
    //   389: invokevirtual 155	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   392: aload_1
    //   393: aload 11
    //   395: iload 7
    //   397: aload 13
    //   399: invokeinterface 165 4 0
    //   404: pop2
    //   405: iload 8
    //   407: iconst_1
    //   408: iadd
    //   409: istore 8
    //   411: goto -108 -> 303
    //   414: pop
    //   415: aload_0
    //   416: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   419: astore_1
    //   420: aload_2
    //   421: checkcast 44	java/io/Closeable
    //   424: astore_2
    //   425: aload_1
    //   426: aload_2
    //   427: invokeinterface 47 2 0
    //   432: return
    //   433: new 167	c/u
    //   436: astore_1
    //   437: ldc -87
    //   439: astore_3
    //   440: aload_1
    //   441: aload_3
    //   442: invokespecial 170	c/u:<init>	(Ljava/lang/String;)V
    //   445: aload_1
    //   446: athrow
    //   447: new 167	c/u
    //   450: astore_1
    //   451: ldc -84
    //   453: astore_3
    //   454: aload_1
    //   455: aload_3
    //   456: invokespecial 170	c/u:<init>	(Ljava/lang/String;)V
    //   459: aload_1
    //   460: athrow
    //   461: astore_1
    //   462: aload_0
    //   463: getfield 23	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   466: astore_3
    //   467: aload_2
    //   468: checkcast 44	java/io/Closeable
    //   471: astore_2
    //   472: aload_3
    //   473: aload_2
    //   474: invokeinterface 47 2 0
    //   479: aload_1
    //   480: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	481	0	this	b
    //   0	481	1	paramb	android.arch.persistence.db.b
    //   7	467	2	localObject1	Object
    //   12	461	3	localObject2	Object
    //   15	103	4	str1	String
    //   67	215	5	localObject3	Object
    //   133	6	6	bool1	boolean
    //   149	159	6	i	int
    //   136	260	7	j	int
    //   168	3	8	bool2	boolean
    //   205	205	8	k	int
    //   182	120	9	localObject4	Object
    //   314	74	10	str2	String
    //   325	69	11	localObject5	Object
    //   341	20	12	m	int
    //   372	26	13	localContentValues	ContentValues
    //   381	5	14	str3	String
    //   414	1	17	localException	Exception
    // Exception table:
    //   from	to	target	type
    //   8	12	414	java/lang/Exception
    //   18	25	414	java/lang/Exception
    //   48	52	414	java/lang/Exception
    //   54	60	414	java/lang/Exception
    //   72	77	414	java/lang/Exception
    //   89	94	414	java/lang/Exception
    //   95	99	414	java/lang/Exception
    //   104	107	414	java/lang/Exception
    //   111	116	414	java/lang/Exception
    //   122	126	414	java/lang/Exception
    //   127	133	414	java/lang/Exception
    //   143	149	414	java/lang/Exception
    //   152	159	414	java/lang/Exception
    //   161	168	414	java/lang/Exception
    //   175	182	414	java/lang/Exception
    //   184	189	414	java/lang/Exception
    //   191	196	414	java/lang/Exception
    //   198	205	414	java/lang/Exception
    //   229	233	414	java/lang/Exception
    //   234	241	414	java/lang/Exception
    //   247	252	414	java/lang/Exception
    //   256	259	414	java/lang/Exception
    //   260	264	414	java/lang/Exception
    //   265	269	414	java/lang/Exception
    //   274	278	414	java/lang/Exception
    //   281	288	414	java/lang/Exception
    //   293	295	414	java/lang/Exception
    //   311	314	414	java/lang/Exception
    //   316	321	414	java/lang/Exception
    //   327	332	414	java/lang/Exception
    //   334	341	414	java/lang/Exception
    //   369	372	414	java/lang/Exception
    //   374	379	414	java/lang/Exception
    //   387	392	414	java/lang/Exception
    //   397	405	414	java/lang/Exception
    //   433	436	414	java/lang/Exception
    //   441	445	414	java/lang/Exception
    //   445	447	414	java/lang/Exception
    //   447	450	414	java/lang/Exception
    //   455	459	414	java/lang/Exception
    //   459	461	414	java/lang/Exception
    //   8	12	461	finally
    //   18	25	461	finally
    //   48	52	461	finally
    //   54	60	461	finally
    //   72	77	461	finally
    //   89	94	461	finally
    //   95	99	461	finally
    //   104	107	461	finally
    //   111	116	461	finally
    //   122	126	461	finally
    //   127	133	461	finally
    //   143	149	461	finally
    //   152	159	461	finally
    //   161	168	461	finally
    //   175	182	461	finally
    //   184	189	461	finally
    //   191	196	461	finally
    //   198	205	461	finally
    //   229	233	461	finally
    //   234	241	461	finally
    //   247	252	461	finally
    //   256	259	461	finally
    //   260	264	461	finally
    //   265	269	461	finally
    //   274	278	461	finally
    //   281	288	461	finally
    //   293	295	461	finally
    //   311	314	461	finally
    //   316	321	461	finally
    //   327	332	461	finally
    //   334	341	461	finally
    //   369	372	461	finally
    //   374	379	461	finally
    //   387	392	461	finally
    //   397	405	461	finally
    //   433	436	461	finally
    //   441	445	461	finally
    //   445	447	461	finally
    //   447	450	461	finally
    //   455	459	461	finally
    //   459	461	461	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */