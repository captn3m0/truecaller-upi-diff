package com.truecaller.insights.core.smscategorizer.db;

import android.arch.persistence.room.c;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

public final class j
  implements i
{
  private final android.arch.persistence.room.f a;
  private final c b;
  private final c c;
  private final c d;
  private final android.arch.persistence.room.j e;
  private final android.arch.persistence.room.j f;
  private final android.arch.persistence.room.j g;
  private final android.arch.persistence.room.j h;
  private final android.arch.persistence.room.j i;
  
  public j(android.arch.persistence.room.f paramf)
  {
    a = paramf;
    Object localObject = new com/truecaller/insights/core/smscategorizer/db/j$1;
    ((j.1)localObject).<init>(this, paramf);
    b = ((c)localObject);
    localObject = new com/truecaller/insights/core/smscategorizer/db/j$2;
    ((j.2)localObject).<init>(this, paramf);
    c = ((c)localObject);
    localObject = new com/truecaller/insights/core/smscategorizer/db/j$3;
    ((j.3)localObject).<init>(this, paramf);
    d = ((c)localObject);
    localObject = new com/truecaller/insights/core/smscategorizer/db/j$4;
    ((j.4)localObject).<init>(this, paramf);
    e = ((android.arch.persistence.room.j)localObject);
    localObject = new com/truecaller/insights/core/smscategorizer/db/j$5;
    ((j.5)localObject).<init>(this, paramf);
    f = ((android.arch.persistence.room.j)localObject);
    localObject = new com/truecaller/insights/core/smscategorizer/db/j$6;
    ((j.6)localObject).<init>(this, paramf);
    g = ((android.arch.persistence.room.j)localObject);
    localObject = new com/truecaller/insights/core/smscategorizer/db/j$7;
    ((j.7)localObject).<init>(this, paramf);
    h = ((android.arch.persistence.room.j)localObject);
    localObject = new com/truecaller/insights/core/smscategorizer/db/j$8;
    ((j.8)localObject).<init>(this, paramf);
    i = ((android.arch.persistence.room.j)localObject);
  }
  
  public final List a()
  {
    android.arch.persistence.room.i locali = android.arch.persistence.room.i.a("SELECT * FROM metaData", 0);
    Cursor localCursor = a.a(locali);
    String str1 = "id";
    try
    {
      int j = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "keyword";
      int k = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "instances";
      int m = localCursor.getColumnIndexOrThrow(str3);
      String str4 = "words_count";
      int n = localCursor.getColumnIndexOrThrow(str4);
      ArrayList localArrayList = new java/util/ArrayList;
      int i1 = localCursor.getCount();
      localArrayList.<init>(i1);
      for (;;)
      {
        boolean bool = localCursor.moveToNext();
        if (!bool) {
          break;
        }
        MetaData localMetaData = new com/truecaller/insights/core/smscategorizer/db/MetaData;
        localMetaData.<init>();
        int i2 = localCursor.getInt(j);
        localMetaData.setId(i2);
        String str5 = localCursor.getString(k);
        localMetaData.setKeyword(str5);
        i2 = localCursor.getInt(m);
        localMetaData.setInstances(i2);
        i2 = localCursor.getInt(n);
        localMetaData.setWordsCount(i2);
        localArrayList.add(localMetaData);
      }
      return localArrayList;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final void a(String paramString)
  {
    android.arch.persistence.db.f localf = e.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int j = 1;
    try
    {
      localf.a(j, paramString);
      localf.a();
      paramString = a;
      paramString.f();
      return;
    }
    finally
    {
      a.e();
      e.a(localf);
    }
  }
  
  public final int b(String paramString)
  {
    android.arch.persistence.db.f localf = f.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int j = 1;
    if (paramString == null) {}
    try
    {
      localf.a(j);
      break label44;
      localf.a(j, paramString);
      label44:
      int k = localf.a();
      localf1 = a;
      localf1.f();
      return k;
    }
    finally
    {
      a.e();
      f.a(localf);
    }
  }
  
  public final List b()
  {
    android.arch.persistence.room.i locali = android.arch.persistence.room.i.a("SELECT * FROM keywordCounts", 0);
    Cursor localCursor = a.a(locali);
    String str1 = "id";
    try
    {
      int j = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "key_name";
      int k = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "ran_value";
      int m = localCursor.getColumnIndexOrThrow(str3);
      String str4 = "pam_value";
      int n = localCursor.getColumnIndexOrThrow(str4);
      String str5 = "ham_value";
      int i1 = localCursor.getColumnIndexOrThrow(str5);
      String str6 = "otp_value";
      int i2 = localCursor.getColumnIndexOrThrow(str6);
      ArrayList localArrayList = new java/util/ArrayList;
      int i3 = localCursor.getCount();
      localArrayList.<init>(i3);
      for (;;)
      {
        boolean bool = localCursor.moveToNext();
        if (!bool) {
          break;
        }
        KeywordCounts localKeywordCounts = new com/truecaller/insights/core/smscategorizer/db/KeywordCounts;
        localKeywordCounts.<init>();
        int i4 = localCursor.getInt(j);
        localKeywordCounts.setId(i4);
        String str7 = localCursor.getString(k);
        keyName = str7;
        i4 = localCursor.getInt(m);
        localKeywordCounts.setRanValue(i4);
        i4 = localCursor.getInt(n);
        localKeywordCounts.setPamValue(i4);
        i4 = localCursor.getInt(i1);
        localKeywordCounts.setHamValue(i4);
        i4 = localCursor.getInt(i2);
        localKeywordCounts.setOtpValue(i4);
        localArrayList.add(localKeywordCounts);
      }
      return localArrayList;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final int c()
  {
    int j = 0;
    Object localObject1 = null;
    android.arch.persistence.room.i locali = android.arch.persistence.room.i.a("SELECT SUM (instances) FROM metaData", 0);
    Cursor localCursor = a.a(locali);
    try
    {
      boolean bool = localCursor.moveToFirst();
      if (bool) {
        j = localCursor.getInt(0);
      }
      return j;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final int c(String paramString)
  {
    android.arch.persistence.db.f localf = g.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int j = 1;
    if (paramString == null) {}
    try
    {
      localf.a(j);
      break label44;
      localf.a(j, paramString);
      label44:
      int k = localf.a();
      localf1 = a;
      localf1.f();
      return k;
    }
    finally
    {
      a.e();
      g.a(localf);
    }
  }
  
  public final int d(String paramString)
  {
    android.arch.persistence.db.f localf = h.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int j = 1;
    if (paramString == null) {}
    try
    {
      localf.a(j);
      break label44;
      localf.a(j, paramString);
      label44:
      int k = localf.a();
      localf1 = a;
      localf1.f();
      return k;
    }
    finally
    {
      a.e();
      h.a(localf);
    }
  }
  
  public final List d()
  {
    Object localObject1 = null;
    android.arch.persistence.room.i locali = android.arch.persistence.room.i.a("SELECT * FROM StopWord", 0);
    Cursor localCursor = a.a(locali);
    try
    {
      ArrayList localArrayList = new java/util/ArrayList;
      int j = localCursor.getCount();
      localArrayList.<init>(j);
      for (;;)
      {
        boolean bool = localCursor.moveToNext();
        if (!bool) {
          break;
        }
        String str = localCursor.getString(0);
        localArrayList.add(str);
      }
      return localArrayList;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final int e(String paramString)
  {
    android.arch.persistence.db.f localf = i.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int j = 1;
    if (paramString == null) {}
    try
    {
      localf.a(j);
      break label44;
      localf.a(j, paramString);
      label44:
      int k = localf.a();
      localf1 = a;
      localf1.f();
      return k;
    }
    finally
    {
      a.e();
      i.a(localf);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */