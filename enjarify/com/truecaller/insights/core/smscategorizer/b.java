package com.truecaller.insights.core.smscategorizer;

import android.util.Patterns;
import c.a.m;
import c.a.y;
import c.u;
import com.truecaller.insights.core.smscategorizer.db.KeywordCounts;
import com.truecaller.insights.core.smscategorizer.db.MetaData;
import com.truecaller.insights.core.smscategorizer.db.g;
import com.truecaller.tracking.events.bb.a;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class b
  implements a
{
  private final List a;
  private final g b;
  
  public b(g paramg)
  {
    b = paramg;
    paramg = b.a();
    a = paramg;
  }
  
  private static String b(String paramString, bb.a parama)
  {
    Object localObject1 = Patterns.EMAIL_ADDRESS;
    paramString = (CharSequence)paramString;
    paramString = ((Pattern)localObject1).matcher(paramString).replaceAll(" EMAIL ");
    c.g.b.k.a(paramString, "emailMatcher.replaceAll(EMAIL_STRING)");
    localObject1 = c.b();
    paramString = (CharSequence)paramString;
    paramString = ((Pattern)localObject1).matcher(paramString).replaceAll(" DATE ");
    c.g.b.k.a(paramString, "dateMatcher.replaceAll(DATE_STRING)");
    localObject1 = Patterns.WEB_URL;
    paramString = (CharSequence)paramString;
    paramString = ((Pattern)localObject1).matcher(paramString).replaceAll(" URL ");
    c.g.b.k.a(paramString, "urlMatcher.replaceAll(URL_STRING)");
    paramString = (CharSequence)paramString;
    Object localObject2 = new c/n/k;
    ((c.n.k)localObject2).<init>(" URL ");
    int i = 0;
    localObject1 = null;
    localObject2 = ((c.n.k)localObject2).a(paramString, 0);
    boolean bool1 = ((List)localObject2).isEmpty();
    int m = 1;
    Object localObject3;
    CharSequence localCharSequence;
    if (!bool1)
    {
      int j = ((List)localObject2).size();
      localObject3 = ((List)localObject2).listIterator(j);
      int n;
      do
      {
        boolean bool3 = ((ListIterator)localObject3).hasPrevious();
        if (!bool3) {
          break;
        }
        localCharSequence = (CharSequence)((ListIterator)localObject3).previous();
        n = localCharSequence.length();
        if (n == 0)
        {
          n = 1;
        }
        else
        {
          n = 0;
          localCharSequence = null;
        }
      } while (n != 0);
      localObject2 = (Iterable)localObject2;
      j = ((ListIterator)localObject3).nextIndex() + m;
      localObject2 = m.d((Iterable)localObject2, j);
    }
    else
    {
      localObject2 = (List)y.a;
    }
    localObject2 = (Collection)localObject2;
    if (localObject2 != null)
    {
      localObject3 = new String[0];
      localObject2 = ((Collection)localObject2).toArray((Object[])localObject3);
      if (localObject2 != null)
      {
        int i2 = localObject2.length - m;
        parama.b(i2);
        localObject2 = c.c();
        localObject3 = new c/n/k;
        ((c.n.k)localObject3).<init>((String)localObject2);
        localObject2 = ((c.n.k)localObject3).a(paramString, 0);
        boolean bool2 = ((List)localObject2).isEmpty();
        if (!bool2)
        {
          int k = ((List)localObject2).size();
          localObject3 = ((List)localObject2).listIterator(k);
          int i1;
          do
          {
            boolean bool4 = ((ListIterator)localObject3).hasPrevious();
            if (!bool4) {
              break;
            }
            localCharSequence = (CharSequence)((ListIterator)localObject3).previous();
            i1 = localCharSequence.length();
            if (i1 == 0)
            {
              i1 = 1;
            }
            else
            {
              i1 = 0;
              localCharSequence = null;
            }
          } while (i1 != 0);
          localObject2 = (Iterable)localObject2;
          k = ((ListIterator)localObject3).nextIndex() + m;
          localObject2 = m.d((Iterable)localObject2, k);
        }
        else
        {
          localObject2 = (List)y.a;
        }
        localObject2 = (Collection)localObject2;
        if (localObject2 != null)
        {
          localObject1 = new String[0];
          localObject1 = ((Collection)localObject2).toArray((Object[])localObject1);
          if (localObject1 != null)
          {
            i = localObject1.length - m;
            parama.a(i);
            parama = c.c();
            localObject1 = new c/n/k;
            ((c.n.k)localObject1).<init>(parama);
            paramString = ((c.n.k)localObject1).a(paramString, " NUMBER ");
            parama = c.d();
            paramString = (CharSequence)paramString;
            paramString = parama.matcher(paramString).replaceAll(" CURRENCY ");
            parama = "currencyMatcher.replaceAll(CURRENCY_STRING)";
            c.g.b.k.a(paramString, parama);
            if (paramString != null)
            {
              paramString = paramString.toLowerCase();
              c.g.b.k.a(paramString, "(this as java.lang.String).toLowerCase()");
              parama = new java/lang/StringBuilder;
              parama.<init>();
              parama.append(paramString);
              parama.append(" z");
              paramString = (CharSequence)parama.toString();
              localObject1 = new c/n/k;
              ((c.n.k)localObject1).<init>("[0-9]");
              return ((c.n.k)localObject1).a(paramString, "");
            }
            paramString = new c/u;
            paramString.<init>("null cannot be cast to non-null type java.lang.String");
            throw paramString;
          }
          paramString = new c/u;
          paramString.<init>("null cannot be cast to non-null type kotlin.Array<T>");
          throw paramString;
        }
        paramString = new c/u;
        paramString.<init>("null cannot be cast to non-null type java.util.Collection<T>");
        throw paramString;
      }
      paramString = new c/u;
      paramString.<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw paramString;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw paramString;
  }
  
  public final int a(String paramString, bb.a parama)
  {
    Object localObject1 = this;
    Object localObject2 = parama;
    Object localObject3 = paramString;
    c.g.b.k.b(paramString, "message");
    Object localObject4 = "metadataBuilder";
    c.g.b.k.b(parama, (String)localObject4);
    int i = 1;
    Object localObject5 = new String[i];
    Object localObject6 = new java/lang/StringBuilder;
    ((StringBuilder)localObject6).<init>("getCalculatedScore start time ");
    long l1 = System.currentTimeMillis();
    ((StringBuilder)localObject6).append(l1);
    localObject6 = ((StringBuilder)localObject6).toString();
    int j = 0;
    Object localObject7 = null;
    localObject5[0] = localObject6;
    localObject3 = b(paramString, parama);
    localObject5 = localObject3;
    localObject5 = (CharSequence)localObject3;
    localObject6 = "[.,;:'\"()?!@+#]?\\s+";
    Object localObject8 = new c/n/k;
    ((c.n.k)localObject8).<init>((String)localObject6);
    localObject5 = ((c.n.k)localObject8).a((CharSequence)localObject5, 0);
    boolean bool1 = ((List)localObject5).isEmpty();
    int k;
    int m;
    if (!bool1)
    {
      k = ((List)localObject5).size();
      localObject6 = ((List)localObject5).listIterator(k);
      do
      {
        boolean bool2 = ((ListIterator)localObject6).hasPrevious();
        if (!bool2) {
          break;
        }
        localObject8 = (CharSequence)((ListIterator)localObject6).previous();
        m = ((CharSequence)localObject8).length();
        if (m == 0)
        {
          m = 1;
        }
        else
        {
          m = 0;
          localObject8 = null;
        }
      } while (m != 0);
      localObject5 = (Iterable)localObject5;
      k = ((ListIterator)localObject6).nextIndex() + i;
      localObject5 = m.d((Iterable)localObject5, k);
    }
    else
    {
      localObject5 = (List)y.a;
    }
    localObject5 = (Collection)localObject5;
    if (localObject5 != null)
    {
      localObject6 = new String[0];
      localObject5 = ((Collection)localObject5).toArray((Object[])localObject6);
      if (localObject5 != null)
      {
        localObject5 = (String[])localObject5;
        k = 4;
        localObject6 = new Double[k];
        double d1 = 0.0D;
        Object localObject9 = Double.valueOf(d1);
        localObject6[0] = localObject9;
        localObject9 = Double.valueOf(d1);
        localObject6[i] = localObject9;
        localObject9 = Double.valueOf(d1);
        int n = 2;
        localObject6[n] = localObject9;
        localObject9 = Double.valueOf(d1);
        int i1 = 3;
        localObject6[i1] = localObject9;
        localObject9 = b.c();
        double d2 = b.b();
        MetaData localMetaData1 = (MetaData)((List)localObject9).get(0);
        int i3 = localMetaData1.getInstances();
        double d3 = i3;
        Double.isNaN(d3);
        d3 /= d2;
        m = ((MetaData)((List)localObject9).get(i)).getInstances();
        d1 = m;
        Double.isNaN(d1);
        d1 /= d2;
        MetaData localMetaData2 = (MetaData)((List)localObject9).get(n);
        n = localMetaData2.getInstances();
        double d4 = d1;
        double d5 = n;
        Double.isNaN(d5);
        d5 /= d2;
        int i4 = ((MetaData)((List)localObject9).get(i1)).getInstances();
        Object localObject10 = localObject3;
        double d6 = i4;
        Double.isNaN(d6);
        d6 /= d2;
        Object localObject11 = localObject1;
        localObject11 = (b)localObject1;
        n = 0;
        int i5 = 0;
        Object localObject12 = null;
        int i6 = 0;
        int i7 = 0;
        for (;;)
        {
          i1 = localObject5.length + -1;
          if (i5 >= i1) {
            break;
          }
          Object localObject13 = new java/lang/StringBuilder;
          ((StringBuilder)localObject13).<init>();
          localObject1 = localObject5[i5];
          ((StringBuilder)localObject13).append((String)localObject1);
          localObject1 = " ";
          ((StringBuilder)localObject13).append((String)localObject1);
          int i8 = i5 + 1;
          d7 = d6;
          localObject4 = localObject5[i8];
          ((StringBuilder)localObject13).append((String)localObject4);
          localObject4 = ((StringBuilder)localObject13).toString();
          localObject3 = b.a((String)localObject4);
          if (localObject3 == null)
          {
            localObject4 = localObject5[i5];
            localObject3 = a;
            boolean bool7 = ((List)localObject3).contains(localObject4);
            if (bool7)
            {
              i5 = i8;
              localObject1 = this;
            }
            else
            {
              i8 = i5;
            }
          }
          else
          {
            localObject3 = b.a((String)localObject4);
            if (localObject3 != null)
            {
              localObject13 = c.a();
              localObject12 = localObject4;
              localObject12 = (CharSequence)localObject4;
              localObject13 = ((Pattern)localObject13).matcher((CharSequence)localObject12);
              boolean bool3 = ((Matcher)localObject13).find();
              if (bool3) {
                i6 += 1;
              } else {
                i7 += 1;
              }
              i = ((String)localObject4).length();
              n += i;
              double d8 = localObject6[0].doubleValue();
              int i12 = ((KeywordCounts)localObject3).getRanValue();
              double d9 = Double.parseDouble(Integer.toString(i12));
              localMetaData2 = (MetaData)((List)localObject9).get(0);
              i = localMetaData2.getWordsCount();
              d10 = d5;
              d5 = i;
              Double.isNaN(d5);
              d9 /= d5;
              d5 = Math.log10(d9);
              localObject4 = Double.valueOf(d8 + d5);
              localObject6[0] = localObject4;
              i = 1;
              localObject7 = localObject6[i];
              d5 = ((Double)localObject7).doubleValue();
              int i2 = ((KeywordCounts)localObject3).getPamValue();
              localObject13 = Integer.toString(i2);
              d8 = Double.parseDouble((String)localObject13);
              i = ((MetaData)((List)localObject9).get(i)).getWordsCount();
              d11 = d3;
              double d12 = i;
              Double.isNaN(d12);
              d8 = Math.log10(d8 / d12);
              localObject4 = Double.valueOf(d5 + d8);
              localObject6[1] = localObject4;
              j = 2;
              d8 = localObject6[j].doubleValue();
              d12 = Double.parseDouble(Integer.toString(((KeywordCounts)localObject3).getHamValue()));
              localObject8 = (MetaData)((List)localObject9).get(j);
              m = ((MetaData)localObject8).getWordsCount();
              Object localObject14 = localObject3;
              d6 = m;
              Double.isNaN(d6);
              d6 = Math.log10(d12 / d6);
              localObject4 = Double.valueOf(d8 + d6);
              localObject6[j] = localObject4;
              i = 3;
              d5 = localObject6[i].doubleValue();
              d8 = Double.parseDouble(Integer.toString(((KeywordCounts)localObject14).getOtpValue()));
              int i11 = ((MetaData)((List)localObject9).get(i)).getWordsCount();
              d12 = i11;
              Double.isNaN(d12);
              d8 = Math.log10(d8 / d12);
              d5 += d8;
              localObject3 = Double.valueOf(d5);
              localObject6[i] = localObject3;
              i = 1;
            }
            else
            {
              d10 = d5;
              d11 = d3;
              i = 1;
            }
            i5 = i8 + 1;
            d6 = d7;
            d3 = d11;
            d5 = d10;
            localObject1 = this;
          }
        }
        double d7 = d6;
        double d10 = d5;
        double d11 = d3;
        i = 1;
        localObject1 = Integer.valueOf(i6);
        localObject1 = ((bb.a)localObject2).b((Integer)localObject1);
        localObject2 = Integer.valueOf(i7);
        localObject1 = ((bb.a)localObject1).a((Integer)localObject2);
        localObject2 = "metadataBuilder.setNumBi…am(numIdentifiedUnigrams)";
        c.g.b.k.a(localObject1, (String)localObject2);
        int i13 = localObject5.length - i;
        ((bb.a)localObject1).c(i13);
        localObject1 = localObject6[i];
        d6 = ((Double)localObject1).doubleValue();
        l1 = 0L;
        d5 = 0.0D;
        boolean bool5 = d6 < d5;
        if (bool5)
        {
          int i9 = ((String)localObject10).length() / 5;
          if (n >= i9)
          {
            i9 = 20;
            if (n >= i9)
            {
              localObject1 = null;
              d6 = localObject6[0].doubleValue();
              d5 = Math.log10(d3);
              d6 += d5;
              localObject4 = Double.valueOf(d6);
              localObject6[0] = localObject4;
              i9 = 1;
              double d13 = localObject6[i9].doubleValue();
              double d14 = Math.log10(d4);
              localObject2 = Double.valueOf(d13 + d14);
              localObject6[i9] = localObject2;
              i9 = 2;
              d13 = localObject6[i9].doubleValue();
              d14 = Math.log10(d10);
              localObject2 = Double.valueOf(d13 + d14);
              localObject6[i9] = localObject2;
              i9 = 3;
              d13 = localObject6[i9].doubleValue();
              d14 = Math.log10(d7);
              localObject2 = Double.valueOf(d13 + d14);
              localObject6[i9] = localObject2;
              long l2 = -4526534890170089472L;
              d13 = -1000000.0D;
              long l3 = l2;
              d14 = d13;
              i13 = 0;
              localObject2 = null;
              i = 0;
              localObject4 = null;
              int i10;
              while (i13 <= i9)
              {
                localObject1 = localObject6[i13];
                d5 = ((Double)localObject1).doubleValue();
                boolean bool6 = d5 < d14;
                if (bool6)
                {
                  localObject1 = localObject6[i13];
                  d6 = ((Double)localObject1).doubleValue();
                  d14 = d6;
                  i = i13;
                }
                i13 += 1;
                i10 = 3;
              }
              localObject1 = localObject6[i];
              double d15 = ((Double)localObject1).doubleValue();
              l1 = 0L;
              d5 = 0.0D;
              boolean bool4 = d15 < d5;
              if (bool4)
              {
                localObject1 = null;
                localObject2 = Double.valueOf(localObject6[0].doubleValue() / d14);
                localObject6[0] = localObject2;
                i10 = 1;
                localObject2 = Double.valueOf(localObject6[i10].doubleValue() / d14);
                localObject6[i10] = localObject2;
                i10 = 2;
                localObject2 = Double.valueOf(localObject6[i10].doubleValue() / d14);
                localObject6[i10] = localObject2;
                i10 = 3;
                d5 = localObject6[i10].doubleValue() / d14;
                localObject2 = Double.valueOf(d5);
                localObject6[i10] = localObject2;
                i13 = 1;
              }
              else
              {
                i10 = 3;
                i13 = 1;
              }
              d14 = localObject6[i13].doubleValue();
              l1 = 4580268907426851324L;
              d5 = 0.016D;
              localObject3 = Double.valueOf(d14 + d5);
              localObject6[i13] = localObject3;
              l3 = 4621819117588971520L;
              d14 = 10.0D;
              i13 = 0;
              localObject2 = null;
              while (i13 <= i10)
              {
                localObject7 = localObject6[i13];
                d5 = ((Double)localObject7).doubleValue();
                bool4 = d5 < d14;
                if (!bool4)
                {
                  localObject4 = localObject6[i13];
                  d6 = ((Double)localObject4).doubleValue();
                  d14 = d6;
                  i = i13;
                }
                i13 += 1;
              }
              localObject1 = new String[1];
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>("getCalculatedScore end ");
              l3 = System.currentTimeMillis();
              ((StringBuilder)localObject2).append(l3);
              localObject2 = ((StringBuilder)localObject2).toString();
              localObject1[0] = localObject2;
              return i;
            }
          }
        }
        return 2;
      }
      localObject1 = new c/u;
      ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw ((Throwable)localObject1);
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw ((Throwable)localObject1);
  }
  
  public final void a(String paramString, int paramInt1, int paramInt2, bb.a parama)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject1 = "metadataBuilder";
    c.g.b.k.b(parama, (String)localObject1);
    if (paramInt1 != paramInt2)
    {
      b.d();
      b.a(paramInt2);
      c.g.b.k.b(paramString, "message");
      c.g.b.k.b(parama, "metadataBuilder");
      paramString = (CharSequence)b(paramString, parama);
      localObject1 = new c/n/k;
      ((c.n.k)localObject1).<init>("[.,;:'\"()?!@+#]?\\s+");
      paramInt1 = 0;
      Object localObject2 = null;
      paramString = ((c.n.k)localObject1).a(paramString, 0);
      boolean bool1 = paramString.isEmpty();
      int j = 1;
      int i;
      CharSequence localCharSequence;
      int k;
      if (!bool1)
      {
        i = paramString.size();
        localObject1 = paramString.listIterator(i);
        do
        {
          boolean bool2 = ((ListIterator)localObject1).hasPrevious();
          if (!bool2) {
            break;
          }
          localCharSequence = (CharSequence)((ListIterator)localObject1).previous();
          k = localCharSequence.length();
          if (k == 0)
          {
            k = 1;
          }
          else
          {
            k = 0;
            localCharSequence = null;
          }
        } while (k != 0);
        paramString = (Iterable)paramString;
        i = ((ListIterator)localObject1).nextIndex() + j;
        paramString = m.d(paramString, i);
      }
      else
      {
        paramString = (List)y.a;
      }
      paramString = (Collection)paramString;
      if (paramString != null)
      {
        localObject1 = new String[0];
        paramString = paramString.toArray((Object[])localObject1);
        if (paramString != null)
        {
          paramString = (String[])paramString;
          i = 0;
          localObject1 = null;
          k = 0;
          localCharSequence = null;
          for (;;)
          {
            int m = paramString.length - j;
            if (paramInt1 >= m) {
              break;
            }
            Object localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            Object localObject4 = paramString[paramInt1];
            ((StringBuilder)localObject3).append((String)localObject4);
            localObject4 = " ";
            ((StringBuilder)localObject3).append((String)localObject4);
            int n = paramInt1 + 1;
            Object localObject5 = paramString[n];
            ((StringBuilder)localObject3).append((String)localObject5);
            localObject3 = ((StringBuilder)localObject3).toString();
            localObject5 = b;
            m = ((g)localObject5).a(paramInt2, (String)localObject3);
            if (m == 0)
            {
              localObject3 = paramString[paramInt1];
              localObject4 = b;
              m = ((g)localObject4).a(paramInt2, (String)localObject3);
              if (m != 0) {
                k += 1;
              }
            }
            else
            {
              i += 1;
              paramInt1 = n;
            }
            paramInt1 += j;
          }
          localObject2 = Integer.valueOf(i);
          localObject2 = parama.b((Integer)localObject2);
          Object localObject6 = Integer.valueOf(k);
          localObject2 = ((bb.a)localObject2).a((Integer)localObject6);
          localObject6 = "metadataBuilder.setNumBi…am(numIdentifiedUnigrams)";
          c.g.b.k.a(localObject2, (String)localObject6);
          int i1 = paramString.length - j;
          ((bb.a)localObject2).c(i1);
        }
        else
        {
          paramString = new c/u;
          paramString.<init>("null cannot be cast to non-null type kotlin.Array<T>");
          throw paramString;
        }
      }
      else
      {
        paramString = new c/u;
        paramString.<init>("null cannot be cast to non-null type java.util.Collection<T>");
        throw paramString;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */