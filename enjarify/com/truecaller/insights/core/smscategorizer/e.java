package com.truecaller.insights.core.smscategorizer;

import c.a.ag;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.tracking.events.ac.a;
import com.truecaller.tracking.events.ad;
import com.truecaller.tracking.events.ad.a;
import com.truecaller.tracking.events.ba;
import com.truecaller.tracking.events.ba.a;
import com.truecaller.tracking.events.bb;
import com.truecaller.tracking.events.bb.a;
import java.util.List;

public final class e
  implements d
{
  public dagger.a a;
  private final b b;
  private final f c;
  
  public e(b paramb, f paramf)
  {
    b = paramb;
    c = paramf;
  }
  
  private static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "";
    case 3: 
      return "OTP";
    case 2: 
      return "Other";
    case 1: 
      return "Spam";
    }
    return "Transaction";
  }
  
  public final int a(String paramString1, String paramString2, boolean paramBoolean, List paramList, ac.a parama)
  {
    k.b(paramString1, "senderId");
    k.b(paramString2, "message");
    k.b(paramList, "eventParticipants");
    k.b(parama, "smsCategorizerCompareEvent");
    paramString1 = bb.g();
    Object localObject1 = a;
    if (localObject1 == null)
    {
      str1 = "scoreCalculatorLazy";
      k.a(str1);
    }
    localObject1 = (a)((dagger.a)localObject1).get();
    String str1 = "metadataBuilder";
    k.a(paramString1, str1);
    int i = ((a)localObject1).a(paramString2, paramString1);
    if (paramBoolean)
    {
      Object localObject2 = a(i);
      paramString1 = paramString1.a();
      k.a(paramString1, "metadataBuilder.build()");
      int j = 5;
      localObject1 = new n[j];
      str1 = null;
      String str2 = String.valueOf(paramString1.e().intValue());
      Object localObject3 = t.a("numUnigram", str2);
      localObject1[0] = localObject3;
      str2 = String.valueOf(paramString1.f().intValue());
      localObject3 = t.a("numBigram", str2);
      localObject1[1] = localObject3;
      str2 = String.valueOf(paramString1.b().intValue());
      localObject3 = t.a("numNumbers", str2);
      localObject1[2] = localObject3;
      int k = paramString1.c().intValue();
      str2 = String.valueOf(k);
      localObject3 = t.a("numUrls", str2);
      localObject1[3] = localObject3;
      int m = 4;
      localObject3 = "numWords";
      int n = paramString1.d().intValue();
      paramString1 = String.valueOf(n);
      paramString1 = t.a(localObject3, paramString1);
      localObject1[m] = paramString1;
      paramString1 = ag.a((n[])localObject1);
      parama.a(paramList);
      localObject2 = (CharSequence)localObject2;
      parama.a((CharSequence)localObject2);
      parama.a();
      parama.a(paramString1);
      paramString1 = new com/truecaller/analytics/e$a;
      localObject2 = "SmsCategorizer";
      paramString1.<init>((String)localObject2);
      switch (i)
      {
      default: 
        break;
      case 3: 
        localObject2 = "Type";
        paramList = "OTP";
        paramString1.a((String)localObject2, paramList);
        break;
      case 2: 
        localObject2 = "Type";
        paramList = "Other";
        paramString1.a((String)localObject2, paramList);
        break;
      case 1: 
        localObject2 = "Type";
        paramList = "Spam";
        paramString1.a((String)localObject2, paramList);
        break;
      case 0: 
        localObject2 = "Type";
        paramList = "Transaction";
        paramString1.a((String)localObject2, paramList);
      }
      localObject2 = b;
      paramString1 = paramString1.a();
      paramList = "event.build()";
      k.a(paramString1, paramList);
      ((b)localObject2).b(paramString1);
    }
    return i;
  }
  
  public final void a(String paramString, int paramInt1, int paramInt2, List paramList)
  {
    k.b(paramString, "message");
    k.b(paramList, "eventParticipants");
    Object localObject1 = bb.g();
    Object localObject2 = a;
    if (localObject2 == null)
    {
      str = "scoreCalculatorLazy";
      k.a(str);
    }
    localObject2 = (a)((dagger.a)localObject2).get();
    String str = "metadataBuilder";
    k.a(localObject1, str);
    ((a)localObject2).a(paramString, paramInt1, paramInt2, (bb.a)localObject1);
    paramString = new com/truecaller/analytics/e$a;
    localObject2 = "SmsCategorizerReclassification";
    paramString.<init>((String)localObject2);
    switch (paramInt2)
    {
    default: 
      break;
    case 3: 
      localObject2 = "ReclassificationType";
      str = "OTP";
      paramString.a((String)localObject2, str);
      break;
    case 2: 
      localObject2 = "ReclassificationType";
      str = "Other";
      paramString.a((String)localObject2, str);
      break;
    case 1: 
      localObject2 = "ReclassificationType";
      str = "Spam";
      paramString.a((String)localObject2, str);
      break;
    case 0: 
      localObject2 = "ReclassificationType";
      str = "Transaction";
      paramString.a((String)localObject2, str);
    }
    localObject2 = b;
    paramString = paramString.a();
    k.a(paramString, "event.build()");
    ((b)localObject2).b(paramString);
    paramString = a(paramInt1);
    Object localObject3 = a(paramInt2);
    bb localbb = ((bb.a)localObject1).a();
    k.a(localbb, "metadataBuilder.build()");
    localObject1 = ad.b();
    paramString = (CharSequence)paramString;
    paramString = ((ad.a)localObject1).a(paramString);
    localObject3 = (CharSequence)localObject3;
    paramString = paramString.b((CharSequence)localObject3);
    localObject3 = ba.b();
    localObject1 = (CharSequence)"RawOccurrences";
    localObject3 = ((ba.a)localObject3).a((CharSequence)localObject1).a().b();
    k.a(localObject3, "SmsCategorizerModel.newB…VERSION)\n        .build()");
    paramString = paramString.a((ba)localObject3).a(localbb).a(paramList).a();
    localObject3 = (ae)c.a();
    paramString = (org.apache.a.d.d)paramString;
    ((ae)localObject3).a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */