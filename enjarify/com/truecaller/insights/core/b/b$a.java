package com.truecaller.insights.core.b;

import c.d.c;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import com.truecaller.insights.models.PdoBinderType.PdoBinder;
import com.truecaller.insights.models.PdoBinderType.a;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

final class b$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  b$a(b paramb, List paramList, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/insights/core/b/b$a;
    b localb = b;
    List localList = c;
    locala.<init>(localb, localList, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = d;
        localObject = b.b.a();
        boolean bool2 = localObject instanceof PdoBinderType.PdoBinder;
        if (bool2)
        {
          try
          {
            localObject = new com/truecaller/insights/core/b/b$a$1;
            bool2 = false;
            ((b.a.1)localObject).<init>(this, null);
            localObject = (m)localObject;
            int j = 3;
            e.b((ag)paramObject, null, (m)localObject, j);
            localObject = new com/truecaller/insights/core/b/b$a$2;
            ((b.a.2)localObject).<init>(this, null);
            localObject = (m)localObject;
            paramObject = e.b((ag)paramObject, null, (m)localObject, j);
          }
          catch (Exception localException)
          {
            com.truecaller.insights.b.a.b("Error while binding");
            paramObject = x.a;
          }
          return paramObject;
        }
        boolean bool3 = localObject instanceof PdoBinderType.a;
        if (bool3)
        {
          com.truecaller.insights.b.a.b("Empty insights binder");
          return x.a;
        }
        paramObject = new c/l;
        ((l)paramObject).<init>();
        throw ((Throwable)paramObject);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.b.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */