package com.truecaller.insights.core.c;

import c.o.b;
import c.x;
import com.truecaller.insights.models.InsightState;
import com.truecaller.insights.models.ParsedDataObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class b$a$1
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  Object c;
  int d;
  private ag f;
  
  b$a$1(b.a parama, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/insights/core/c/b$a$1;
    b.a locala = e;
    local1.<init>(locala, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    int i = d;
    int j = 1;
    boolean bool1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label533;
      }
      throw a;
    case 2: 
      localObject1 = (InsightState)a;
      bool3 = paramObject instanceof o.b;
      if (!bool3) {
        break label235;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label537;
      }
      paramObject = e.b.c;
      localObject1 = "INSIGHTS.PRUNING";
      d = j;
      paramObject = ((com.truecaller.insights.database.c.a)paramObject).a((String)localObject1, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    Object localObject1 = paramObject;
    localObject1 = (InsightState)paramObject;
    paramObject = e.b.c;
    Object localObject2 = ((InsightState)localObject1).getLastUpdatedAt();
    a = localObject1;
    int k = 2;
    d = k;
    paramObject = ((com.truecaller.insights.database.c.a)paramObject).b((Date)localObject2, this);
    if (paramObject == locala) {
      return locala;
    }
    label235:
    paramObject = (List)paramObject;
    localObject2 = paramObject;
    localObject2 = (Collection)paramObject;
    boolean bool3 = ((Collection)localObject2).isEmpty() ^ j;
    if (bool3)
    {
      localObject2 = e.b;
      Object localObject3 = ((InsightState)localObject1).getLastUpdatedData();
      c.g.b.k.b(paramObject, "records");
      Object localObject4 = paramObject;
      localObject4 = (Iterable)paramObject;
      Object localObject5 = new java/util/ArrayList;
      int m = c.a.m.a((Iterable)localObject4, 10);
      ((ArrayList)localObject5).<init>(m);
      localObject5 = (Collection)localObject5;
      localObject4 = ((Iterable)localObject4).iterator();
      for (;;)
      {
        boolean bool4 = ((Iterator)localObject4).hasNext();
        if (!bool4) {
          break;
        }
        ParsedDataObject localParsedDataObject = (ParsedDataObject)((Iterator)localObject4).next();
        com.truecaller.insights.c.c localc = new com/truecaller/insights/c/c;
        com.truecaller.insights.models.a locala1 = d;
        localc.<init>(localParsedDataObject, locala1);
        ((Collection)localObject5).add(localc);
      }
      localObject5 = (List)localObject5;
      localObject4 = new com/twelfthmile/c/a/b;
      localObject2 = (com.twelfthmile.c.a.a)b;
      ((com.twelfthmile.c.a.b)localObject4).<init>((com.twelfthmile.c.a.a)localObject2, (String)localObject3);
      ((com.twelfthmile.c.a.b)localObject4).a((List)localObject5);
      localObject2 = ((com.twelfthmile.c.a.b)localObject4).b();
      c.g.b.k.a(localObject2, "duplicateEngine.serializedLinkTree");
      localObject3 = e.b.c;
      int n = ((List)paramObject).size() - j;
      Date localDate = ((ParsedDataObject)((List)paramObject).get(n)).getCreatedAt();
      a = localObject1;
      b = paramObject;
      c = localObject2;
      int i1 = 3;
      d = i1;
      paramObject = ((com.truecaller.insights.database.c.a)localObject3).a((InsightState)localObject1, localDate, (String)localObject2, this);
      if (paramObject == locala) {
        return locala;
      }
    }
    label533:
    return x.a;
    label537:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.c.b.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */