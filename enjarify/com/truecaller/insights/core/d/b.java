package com.truecaller.insights.core.d;

import android.os.Build.VERSION;
import c.n.m;
import c.u;
import com.truecaller.insights.models.d.b;
import com.truecaller.insights.models.e.b;
import com.truecaller.insights.models.e.c;
import com.truecaller.insights.models.e.d;
import com.truecaller.insights.models.e.e;
import com.truecaller.insights.models.e.f;
import com.truecaller.insights.models.e.g;
import com.truecaller.insights.models.e.h;
import com.truecaller.insights.models.e.i;
import com.truecaller.insights.models.f;
import com.truecaller.insights.models.f.a;
import com.truecaller.insights.models.f.b;
import com.truecaller.insights.models.g.a;
import com.truecaller.log.UnmutedException.InsightsExceptions;
import com.truecaller.log.UnmutedException.InsightsExceptions.Cause;
import com.twelfthmile.malana.compiler.types.MalanaSeed;
import com.twelfthmile.malana.compiler.types.Response;
import com.twelfthmile.malana.controller.Controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class b
  implements a
{
  private final com.truecaller.analytics.b a;
  private final com.truecaller.featuretoggles.e b;
  
  public b(com.truecaller.analytics.b paramb, com.truecaller.featuretoggles.e parame, MalanaSeed paramMalanaSeed)
  {
    a = paramb;
    b = parame;
    int i = Build.VERSION.SDK_INT;
    int j = 19;
    if (i > j)
    {
      Controller.init(paramMalanaSeed);
      return;
    }
    Controller.init(paramMalanaSeed, true);
  }
  
  private static d.b a(com.truecaller.insights.models.c paramc)
  {
    c.g.b.k.b(paramc, "smsMessage");
    Object localObject1 = com.truecaller.insights.d.d.a;
    localObject1 = a;
    c.g.b.k.b(localObject1, "address");
    localObject1 = m.a(m.a((String)localObject1, "-", ""), " ", "");
    Object localObject2 = com.truecaller.insights.d.d.a;
    localObject2 = localObject1;
    localObject2 = (CharSequence)localObject1;
    Object localObject3 = new c/n/k;
    Object localObject4 = "[A-Za-z0-9]*";
    ((c.n.k)localObject3).<init>((String)localObject4);
    boolean bool1 = ((c.n.k)localObject3).a((CharSequence)localObject2);
    if (bool1)
    {
      localObject3 = new c/n/k;
      localObject4 = ".*[A-Za-z].*";
      ((c.n.k)localObject3).<init>((String)localObject4);
      bool3 = ((c.n.k)localObject3).a((CharSequence)localObject2);
      if (bool3)
      {
        bool3 = true;
        break label111;
      }
    }
    boolean bool3 = false;
    localObject2 = null;
    label111:
    if (bool3)
    {
      int j = ((String)localObject1).length();
      int i = 8;
      if (j == i)
      {
        j = ((String)localObject1).length();
        if (localObject1 != null)
        {
          i = 2;
          localObject1 = ((String)localObject1).substring(i, j);
          localObject2 = "(this as java.lang.Strin…ing(startIndex, endIndex)";
          c.g.b.k.a(localObject1, (String)localObject2);
        }
        else
        {
          paramc = new c/u;
          paramc.<init>("null cannot be cast to non-null type java.lang.String");
          throw paramc;
        }
      }
    }
    try
    {
      localObject2 = c;
      localObject3 = d;
      localObject4 = Controller.getGRMs();
      localObject2 = Controller.parse((String)localObject2, (String)localObject1, (Date)localObject3, (ArrayList)localObject4);
      Object localObject5;
      if (localObject2 == null)
      {
        localObject2 = new com/truecaller/insights/models/d$b;
        localObject3 = e.h.a;
        localObject3 = (com.truecaller.insights.models.e)localObject3;
        localObject4 = new com/truecaller/insights/models/f$a;
        localObject5 = new com/truecaller/log/UnmutedException$InsightsExceptions;
        UnmutedException.InsightsExceptions.Cause localCause = UnmutedException.InsightsExceptions.Cause.PARSE_FAILURE;
        ((UnmutedException.InsightsExceptions)localObject5).<init>(localCause);
        localObject5 = (Throwable)localObject5;
        ((f.a)localObject4).<init>((Throwable)localObject5);
        localObject4 = (f)localObject4;
        ((d.b)localObject2).<init>(paramc, (com.truecaller.insights.models.e)localObject3, (String)localObject1, (f)localObject4);
        localObject3 = localObject2;
      }
      else
      {
        localObject3 = ((Response)localObject2).getCategory();
        if (localObject3 != null)
        {
          int k = ((String)localObject3).hashCode();
          boolean bool2;
          switch (k)
          {
          default: 
            break;
          case 1240557924: 
            localObject4 = "GRM_BILL";
            bool2 = ((String)localObject3).equals(localObject4);
            if (bool2)
            {
              localObject3 = new com/truecaller/insights/models/d$b;
              localObject4 = e.b.a;
              localObject4 = (com.truecaller.insights.models.e)localObject4;
              localObject5 = new com/truecaller/insights/models/f$b;
              ((f.b)localObject5).<init>((Response)localObject2);
              localObject5 = (f)localObject5;
              ((d.b)localObject3).<init>(paramc, (com.truecaller.insights.models.e)localObject4, (String)localObject1, (f)localObject5);
            }
            break;
          case 1240550297: 
            localObject4 = "GRM_BANK";
            bool2 = ((String)localObject3).equals(localObject4);
            if (bool2)
            {
              localObject3 = new com/truecaller/insights/models/d$b;
              localObject4 = com.truecaller.insights.models.e.a.a;
              localObject4 = (com.truecaller.insights.models.e)localObject4;
              localObject5 = new com/truecaller/insights/models/f$b;
              ((f.b)localObject5).<init>((Response)localObject2);
              localObject5 = (f)localObject5;
              ((d.b)localObject3).<init>(paramc, (com.truecaller.insights.models.e)localObject4, (String)localObject1, (f)localObject5);
            }
            break;
          case 1009862158: 
            localObject4 = "GRM_OTP";
            bool2 = ((String)localObject3).equals(localObject4);
            if (bool2)
            {
              localObject3 = new com/truecaller/insights/models/d$b;
              localObject4 = e.g.a;
              localObject4 = (com.truecaller.insights.models.e)localObject4;
              localObject5 = new com/truecaller/insights/models/f$b;
              ((f.b)localObject5).<init>((Response)localObject2);
              localObject5 = (f)localObject5;
              ((d.b)localObject3).<init>(paramc, (com.truecaller.insights.models.e)localObject4, (String)localObject1, (f)localObject5);
            }
            break;
          case -186141357: 
            localObject4 = "GRM_NOTIF";
            bool2 = ((String)localObject3).equals(localObject4);
            if (bool2)
            {
              localObject3 = new com/truecaller/insights/models/d$b;
              localObject4 = e.e.a;
              localObject4 = (com.truecaller.insights.models.e)localObject4;
              localObject5 = new com/truecaller/insights/models/f$b;
              ((f.b)localObject5).<init>((Response)localObject2);
              localObject5 = (f)localObject5;
              ((d.b)localObject3).<init>(paramc, (com.truecaller.insights.models.e)localObject4, (String)localObject1, (f)localObject5);
            }
            break;
          case -194258755: 
            localObject4 = "GRM_EVENT";
            bool2 = ((String)localObject3).equals(localObject4);
            if (bool2)
            {
              localObject3 = new com/truecaller/insights/models/d$b;
              localObject4 = e.d.a;
              localObject4 = (com.truecaller.insights.models.e)localObject4;
              localObject5 = new com/truecaller/insights/models/f$b;
              ((f.b)localObject5).<init>((Response)localObject2);
              localObject5 = (f)localObject5;
              ((d.b)localObject3).<init>(paramc, (com.truecaller.insights.models.e)localObject4, (String)localObject1, (f)localObject5);
            }
            break;
          case -1296211247: 
            localObject4 = "GRM_DELIVERY";
            bool2 = ((String)localObject3).equals(localObject4);
            if (bool2)
            {
              localObject3 = new com/truecaller/insights/models/d$b;
              localObject4 = e.c.a;
              localObject4 = (com.truecaller.insights.models.e)localObject4;
              localObject5 = new com/truecaller/insights/models/f$b;
              ((f.b)localObject5).<init>((Response)localObject2);
              localObject5 = (f)localObject5;
              ((d.b)localObject3).<init>(paramc, (com.truecaller.insights.models.e)localObject4, (String)localObject1, (f)localObject5);
            }
            break;
          case -1301422793: 
            localObject4 = "GRM_TRAVEL";
            bool2 = ((String)localObject3).equals(localObject4);
            if (bool2)
            {
              localObject3 = new com/truecaller/insights/models/d$b;
              localObject4 = e.i.a;
              localObject4 = (com.truecaller.insights.models.e)localObject4;
              localObject5 = new com/truecaller/insights/models/f$b;
              ((f.b)localObject5).<init>((Response)localObject2);
              localObject5 = (f)localObject5;
              ((d.b)localObject3).<init>(paramc, (com.truecaller.insights.models.e)localObject4, (String)localObject1, (f)localObject5);
            }
            break;
          case -1455517772: 
            localObject4 = "GRM_OFFERS";
            bool2 = ((String)localObject3).equals(localObject4);
            if (bool2)
            {
              localObject3 = new com/truecaller/insights/models/d$b;
              localObject4 = e.f.a;
              localObject4 = (com.truecaller.insights.models.e)localObject4;
              localObject5 = new com/truecaller/insights/models/f$b;
              ((f.b)localObject5).<init>((Response)localObject2);
              localObject5 = (f)localObject5;
              ((d.b)localObject3).<init>(paramc, (com.truecaller.insights.models.e)localObject4, (String)localObject1, (f)localObject5);
            }
            break;
          }
        }
        localObject2 = new com/truecaller/log/UnmutedException$InsightsExceptions;
        localObject3 = UnmutedException.InsightsExceptions.Cause.PARSER_UNKNOWN_GRM_EXCEPTION;
        ((UnmutedException.InsightsExceptions)localObject2).<init>((UnmutedException.InsightsExceptions.Cause)localObject3);
        localObject2 = (Throwable)localObject2;
        localObject2 = a(paramc, (String)localObject1, (Throwable)localObject2);
        localObject3 = localObject2;
      }
    }
    catch (Exception localException)
    {
      localObject2 = (Throwable)localException;
      localObject3 = a(paramc, (String)localObject1, (Throwable)localObject2);
    }
    return (d.b)localObject3;
  }
  
  private static d.b a(com.truecaller.insights.models.c paramc, String paramString, Throwable paramThrowable)
  {
    com.truecaller.insights.b.a.a(paramThrowable.getLocalizedMessage());
    d.b localb = new com/truecaller/insights/models/d$b;
    com.truecaller.insights.models.e locale = (com.truecaller.insights.models.e)e.h.a;
    Object localObject = new com/truecaller/insights/models/f$a;
    ((f.a)localObject).<init>(paramThrowable);
    localObject = (f)localObject;
    localb.<init>(paramc, locale, paramString, (f)localObject);
    return localb;
  }
  
  public final d.b a(com.truecaller.insights.database.c paramc, com.truecaller.insights.models.c paramc1)
  {
    c.g.b.k.b(paramc, "receiver$0");
    c.g.b.k.b(paramc1, "smsMessage");
    paramc1 = a(paramc1);
    Object localObject = b.Z();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      paramc = a;
      localObject = new com/truecaller/insights/models/g$a;
      ((g.a)localObject).<init>(paramc1);
      paramc.add(localObject);
    }
    return paramc1;
  }
  
  public final void a(com.truecaller.insights.models.e parame)
  {
    c.g.b.k.b(parame, "response");
    Object localObject = new com/truecaller/analytics/e$a;
    ((com.truecaller.analytics.e.a)localObject).<init>("InsightsParser");
    parame = parame.a();
    ((com.truecaller.analytics.e.a)localObject).a("Category", parame);
    parame = a;
    localObject = ((com.truecaller.analytics.e.a)localObject).a();
    c.g.b.k.a(localObject, "event.build()");
    parame.b((com.truecaller.analytics.e)localObject);
  }
  
  public final boolean a(com.truecaller.insights.models.d paramd)
  {
    c.g.b.k.b(paramd, "parseResponse");
    paramd = b;
    Object localObject = e.h.a;
    boolean bool1 = c.g.b.k.a(paramd, localObject);
    if (!bool1)
    {
      localObject = e.f.a;
      boolean bool2 = c.g.b.k.a(paramd, localObject);
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */