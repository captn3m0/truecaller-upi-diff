package com.truecaller.abtest.definitions;

public abstract interface Constants$ActiveExperiments$PremiumButtonColors_16430
{
  public static final String NAME = "premiumButtonColors_16430";
  public static final String VARIANT_A = "orange";
  public static final String VARIANT_B = "blue";
  public static final String VARIANT_KEY = "premiumButtonColors_16430";
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.definitions.Constants.ActiveExperiments.PremiumButtonColors_16430
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */