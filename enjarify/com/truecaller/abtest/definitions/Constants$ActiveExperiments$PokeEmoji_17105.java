package com.truecaller.abtest.definitions;

public abstract interface Constants$ActiveExperiments$PokeEmoji_17105
{
  public static final String NAME = "defaultPokeEmoji_17105";
  public static final String VARIANT_A = "thumbsUp";
  public static final String VARIANT_B = "foldedHands";
  public static final String VARIANT_KEY = "defaultPokeEmoji_17105";
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.definitions.Constants.ActiveExperiments.PokeEmoji_17105
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */