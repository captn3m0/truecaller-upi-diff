package com.truecaller.abtest.definitions;

public abstract interface Constants$ActiveExperiments$ReferralByWhatsapp_16722
{
  public static final String NAME = "referralByWhatsApp_16722";
  public static final String VARIANT_A = "sms";
  public static final String VARIANT_B = "whatsapp";
  public static final String VARIANT_KEY = "referralByWhatsApp_16722";
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.definitions.Constants.ActiveExperiments.ReferralByWhatsapp_16722
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */