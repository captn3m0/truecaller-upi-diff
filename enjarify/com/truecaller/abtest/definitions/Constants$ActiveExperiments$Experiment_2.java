package com.truecaller.abtest.definitions;

public abstract interface Constants$ActiveExperiments$Experiment_2
{
  public static final String NAME = "Truecaller SDK signup flow 17858";
  public static final String VARIANT_A = "Loader Last";
  public static final String VARIANT_B = "Loader First";
  public static final String VARIANT_KEY = "truecaller_sdk_sign_in_flow_17858";
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.definitions.Constants.ActiveExperiments.Experiment_2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */