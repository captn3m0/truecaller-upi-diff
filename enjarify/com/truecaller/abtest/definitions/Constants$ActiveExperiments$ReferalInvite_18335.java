package com.truecaller.abtest.definitions;

public abstract interface Constants$ActiveExperiments$ReferalInvite_18335
{
  public static final String NAME = "referalInvite_18335";
  public static final String VARIANT_A = "inviteEarn";
  public static final String VARIANT_B = "referEarn";
  public static final String VARIANT_CONTROL = "inviteFriends";
  public static final String VARIANT_KEY = "referalInvite_18335";
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.definitions.Constants.ActiveExperiments.ReferalInvite_18335
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */