package com.truecaller.abtest.definitions;

public abstract interface Constants$ActiveExperiments$SingleContactReferralWithInviteMore_17575
{
  public static final String NAME = "inviteMore_17575";
  public static final String VARIANT_A = "bulkInvite";
  public static final String VARIANT_CONTROL = "singleContact";
  public static final String VARIANT_KEY = "inviteMore_17575";
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.definitions.Constants.ActiveExperiments.SingleContactReferralWithInviteMore_17575
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */