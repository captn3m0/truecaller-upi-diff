package com.truecaller.abtest.definitions;

public abstract interface Constants$ActiveExperiments$SubscriptionButtonsOrientation_18492
{
  public static final String NAME = "premiumButtonsType_18492";
  public static final String VARIANT_A = "vertical";
  public static final String VARIANT_CONTROL = "horizontal";
  public static final String VARIANT_KEY = "premiumButtonsType_18492";
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.definitions.Constants.ActiveExperiments.SubscriptionButtonsOrientation_18492
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */