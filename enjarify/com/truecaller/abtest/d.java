package com.truecaller.abtest;

import c.g.b.k;
import c.n.m;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.c;
import com.truecaller.old.data.access.Settings;
import java.util.concurrent.TimeUnit;

public final class d
  implements c
{
  final dagger.a a;
  
  public d(dagger.a parama)
  {
    a = parama;
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "key");
    paramString = am.n(((com.google.firebase.remoteconfig.a)a.get()).a(paramString));
    k.a(paramString, "StringUtils.defaultStrin…fig.get().getString(key))");
    return paramString;
  }
  
  public final void a()
  {
    Object localObject1 = "qaDisableFirebaseConfig";
    boolean bool = Settings.e((String)localObject1);
    long l1;
    if (bool)
    {
      l1 = 0L;
    }
    else
    {
      localObject1 = TimeUnit.HOURS;
      long l2 = 6;
      l1 = ((TimeUnit)localObject1).toSeconds(l2);
    }
    int i = 1;
    Object localObject2 = new String[i];
    String str1 = String.valueOf(l1);
    String str2 = "FirebaseRemoteConfig fetching remote values: with cache expiration: ".concat(str1);
    localObject2[0] = str2;
    try
    {
      localObject2 = a;
      localObject2 = ((dagger.a)localObject2).get();
      localObject2 = (com.google.firebase.remoteconfig.a)localObject2;
      localObject1 = ((com.google.firebase.remoteconfig.a)localObject2).a(l1);
      localObject3 = new com/truecaller/abtest/d$a;
      ((d.a)localObject3).<init>(this);
      localObject3 = (OnCompleteListener)localObject3;
      ((Task)localObject1).a((OnCompleteListener)localObject3);
      return;
    }
    catch (Exception localException)
    {
      Object localObject3;
      localObject1 = localException.getMessage();
      if (localObject1 != null)
      {
        localObject3 = new com/truecaller/log/UnmutedException$c;
        ((UnmutedException.c)localObject3).<init>((String)localObject1);
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject3);
        return;
      }
    }
  }
  
  public final boolean b()
  {
    return m.a(a("inviteMore_17575"), "bulkInvite", true);
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "key");
    paramString = ((com.google.firebase.remoteconfig.a)a.get()).a(paramString);
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    if (localObject != null)
    {
      i = ((CharSequence)localObject).length();
      if (i != 0)
      {
        i = 0;
        localObject = null;
        break label57;
      }
    }
    int i = 1;
    label57:
    if (i == 0) {
      return Boolean.parseBoolean(paramString);
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */