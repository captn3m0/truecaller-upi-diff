package com.truecaller.abtest;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.TextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.truecaller.TrueApp;
import com.truecaller.abtest.definitions.Constants.ActiveExperiments;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Comparator;

public final class b
  extends android.support.v4.app.e
  implements RadioGroup.OnCheckedChangeListener
{
  final c a;
  
  public b()
  {
    c localc = TrueApp.y().a().ao();
    a = localc;
  }
  
  public final void onCheckedChanged(RadioGroup paramRadioGroup, int paramInt)
  {
    Object localObject = ((RadioButton)paramRadioGroup.findViewById(paramInt)).getText();
    paramRadioGroup = (String)paramRadioGroup.getTag();
    localObject = ((CharSequence)localObject).toString();
    com.truecaller.common.b.e.b(paramRadioGroup, (String)localObject);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    b localb = this;
    Object localObject1 = new android/widget/LinearLayout;
    Object localObject2 = getContext();
    ((LinearLayout)localObject1).<init>((Context)localObject2);
    int i = 1;
    ((LinearLayout)localObject1).setOrientation(i);
    Object localObject4 = LayoutInflater.from(getContext());
    localObject2 = new android/support/v7/widget/SwitchCompat;
    Object localObject5 = getContext();
    ((SwitchCompat)localObject2).<init>((Context)localObject5);
    ((SwitchCompat)localObject2).setText("Enable Local config");
    boolean bool1 = false;
    Object localObject6 = null;
    boolean bool2 = com.truecaller.common.b.e.a("qaAbTestEnableLocalConfig", false);
    ((SwitchCompat)localObject2).setChecked(bool2);
    localObject5 = -..Lambda.b.qJ6Nzz09-2_cwosyryeCnbm_mTA.INSTANCE;
    ((SwitchCompat)localObject2).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject5);
    localObject5 = getResources();
    int k = 2131165470;
    int j = ((Resources)localObject5).getDimensionPixelSize(k);
    ((SwitchCompat)localObject2).setPadding(j, j, j, j);
    ((LinearLayout)localObject1).addView((View)localObject2);
    localObject2 = new android/support/v7/widget/SwitchCompat;
    Object localObject7 = getContext();
    ((SwitchCompat)localObject2).<init>((Context)localObject7);
    ((SwitchCompat)localObject2).setText("Disable Firebase Caching");
    boolean bool3 = com.truecaller.common.b.e.a("qaDisableFirebaseConfig", false);
    ((SwitchCompat)localObject2).setChecked(bool3);
    localObject7 = -..Lambda.b.fA4289LS0Bpg0mL7TjNA5JQ3PlY.INSTANCE;
    ((SwitchCompat)localObject2).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject7);
    ((SwitchCompat)localObject2).setPadding(j, j, j, j);
    ((LinearLayout)localObject1).addView((View)localObject2);
    localObject7 = Constants.ActiveExperiments.class.getDeclaredClasses();
    localObject2 = -..Lambda.b._-lXKJHfQsS3CoSlMwAu8CnuOXU.INSTANCE;
    Arrays.sort((Object[])localObject7, (Comparator)localObject2);
    int m = localObject7.length;
    int n = 0;
    while (n < m)
    {
      Object localObject8 = localObject7[n];
      View localView = ((LayoutInflater)localObject4).inflate(2131558904, (ViewGroup)localObject1, false);
      i1 = 2131365420;
      localObject2 = localView.findViewById(i1);
      Object localObject9 = localObject2;
      localObject9 = (RadioGroup)localObject2;
      ((RadioGroup)localObject9).setOnCheckedChangeListener(localb);
      Field[] arrayOfField = ((Class)localObject8).getFields();
      int i2 = arrayOfField.length;
      bool1 = false;
      localObject6 = null;
      i = 0;
      localObject10 = null;
      k = 0;
      while (k < i2)
      {
        localObject2 = arrayOfField[k];
        localObject11 = localObject4;
        try
        {
          localObject4 = ((Field)localObject2).getName();
          localObject2 = ((Field)localObject2).get(null);
          localObject2 = (String)localObject2;
          localObject6 = "VARIANT_KEY";
          bool1 = ((String)localObject4).equals(localObject6);
          if (bool1)
          {
            localObject10 = localObject2;
          }
          else
          {
            localObject6 = "VARIANT";
            boolean bool4 = ((String)localObject4).startsWith((String)localObject6);
            if (!bool4)
            {
              localObject2 = "onCreateDialog:: Ignoring";
              new String[1][0] = localObject2;
            }
            else
            {
              localObject4 = new android/widget/RadioButton;
              localObject6 = getContext();
              ((RadioButton)localObject4).<init>((Context)localObject6);
              ((RadioButton)localObject4).setText((CharSequence)localObject2);
              ((RadioGroup)localObject9).addView((View)localObject4);
            }
          }
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
          localIllegalAccessException.printStackTrace();
        }
        k += 1;
        localObject4 = localObject11;
        bool1 = false;
        localObject6 = null;
      }
      Object localObject11 = localObject4;
      if (localObject10 != null)
      {
        ((RadioGroup)localObject9).setTag(localObject10);
        localObject3 = a.a((String)localObject10);
        i = 0;
        localObject10 = null;
        for (;;)
        {
          int i3 = ((RadioGroup)localObject9).getChildCount();
          if (i >= i3) {
            break;
          }
          localObject4 = (RadioButton)((RadioGroup)localObject9).getChildAt(i);
          localObject6 = ((RadioButton)localObject4).getText();
          bool1 = am.b((CharSequence)localObject6, (CharSequence)localObject3);
          if (bool1)
          {
            bool1 = true;
            ((RadioButton)localObject4).setChecked(bool1);
            break;
          }
          i += 1;
        }
      }
      localObject3 = ((Class)localObject8).getSimpleName();
      localObject10 = (TextView)localView.findViewById(2131363786);
      ((TextView)localObject10).setText((CharSequence)localObject3);
      ((LinearLayout)localObject1).addView(localView);
      n += 1;
      localObject4 = localObject11;
      i = 1;
      bool1 = false;
      localObject6 = null;
      k = 2131165470;
    }
    int i1 = getResources().getDimensionPixelSize(2131165470);
    Object localObject10 = new android/widget/LinearLayout;
    localObject4 = getContext();
    ((LinearLayout)localObject10).<init>((Context)localObject4);
    ((LinearLayout)localObject10).setOrientation(1);
    ((LinearLayout)localObject10).setPadding(i1, i1, i1, i1);
    ((LinearLayout)localObject10).setGravity(17);
    Object localObject3 = new android/widget/EditText;
    localObject4 = getContext();
    ((EditText)localObject3).<init>((Context)localObject4);
    ((EditText)localObject3).setHint("Input remote config key");
    localObject4 = new android/widget/Button;
    localObject6 = getContext();
    ((Button)localObject4).<init>((Context)localObject6);
    ((Button)localObject4).setText("Show");
    localObject6 = new com/truecaller/abtest/-$$Lambda$b$geizoD363OhyzDiLdpsiGjXNq_M;
    ((-..Lambda.b.geizoD363OhyzDiLdpsiGjXNq_M)localObject6).<init>(localb, (EditText)localObject3);
    ((Button)localObject4).setOnClickListener((View.OnClickListener)localObject6);
    ((LinearLayout)localObject10).addView((View)localObject3);
    ((LinearLayout)localObject10).addView((View)localObject4);
    ((LinearLayout)localObject1).addView((View)localObject10);
    localObject3 = new android/widget/TextView;
    localObject10 = getContext();
    ((TextView)localObject3).<init>((Context)localObject10);
    i = com.truecaller.utils.ui.b.a(getContext(), 2130969591);
    ((TextView)localObject3).setTextColor(i);
    localObject10 = FirebaseInstanceId.a().e();
    localObject4 = new com/truecaller/abtest/-$$Lambda$b$ahxe_uilyYYfnQQDhiCJN3Ar9vM;
    ((-..Lambda.b.ahxe_uilyYYfnQQDhiCJN3Ar9vM)localObject4).<init>(localb, (TextView)localObject3);
    ((Task)localObject10).a((OnCompleteListener)localObject4);
    ((TextView)localObject3).setPadding(j, j, j, j);
    ((LinearLayout)localObject1).addView((View)localObject3);
    localObject3 = new android/widget/ScrollView;
    localObject10 = getContext();
    ((ScrollView)localObject3).<init>((Context)localObject10);
    ((ScrollView)localObject3).addView((View)localObject1);
    localObject1 = new android/support/v7/app/AlertDialog$Builder;
    localObject10 = getContext();
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject10);
    ((AlertDialog.Builder)localObject1).setView((View)localObject3);
    ((AlertDialog.Builder)localObject1).setTitle("Active AB Tests");
    return ((AlertDialog.Builder)localObject1).create();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */