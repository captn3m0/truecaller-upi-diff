package com.truecaller;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.wizard.utils.i;

public final class bl
  extends AppCompatDialog
  implements View.OnClickListener
{
  private final int a = 2131886808;
  private final String b;
  private final int c;
  private final int d;
  private final int e;
  
  public bl(Context paramContext, int paramInt)
  {
    super(paramContext);
    paramContext = paramContext.getString(2131886811);
    b = paramContext;
    d = 2131886797;
    e = 2131886794;
    c = paramInt;
  }
  
  public bl(Context paramContext, int paramInt1, int paramInt2, int paramInt3)
  {
    super(paramContext);
    String str1 = paramContext.getString(paramInt1);
    String str2 = paramContext.getString(paramInt2);
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = str1;
    arrayOfObject[1] = str2;
    paramContext = paramContext.getString(2131886807, arrayOfObject);
    b = paramContext;
    c = paramInt3;
    d = 2131886797;
    e = 2131886794;
  }
  
  public final void onClick(View paramView)
  {
    int i = paramView.getId();
    int j = 2131363989;
    if (i == j)
    {
      i.a(getContext());
      dismiss();
      return;
    }
    j = 2131362862;
    if (i == j) {
      dismiss();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    supportRequestWindowFeature(1);
    setContentView(2131558578);
    paramBundle = (TintedImageView)findViewById(2131363301);
    TextView localTextView1 = (TextView)findViewById(2131364888);
    TextView localTextView2 = (TextView)findViewById(2131364609);
    Button localButton1 = (Button)findViewById(2131362862);
    Button localButton2 = (Button)findViewById(2131363989);
    int i = c;
    paramBundle.setImageResource(i);
    int j = a;
    localTextView1.setText(j);
    paramBundle = b;
    localTextView2.setText(paramBundle);
    j = d;
    localButton1.setText(j);
    j = e;
    localButton2.setText(j);
    localButton1.setOnClickListener(this);
    localButton2.setOnClickListener(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.bl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */