package com.truecaller;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final c a;
  private final Provider b;
  
  private p(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static p a(c paramc, Provider paramProvider)
  {
    p localp = new com/truecaller/p;
    localp.<init>(paramc, paramProvider);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */