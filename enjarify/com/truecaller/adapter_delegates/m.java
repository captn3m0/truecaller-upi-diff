package com.truecaller.adapter_delegates;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import c.a.f;
import c.g.a.b;
import c.g.b.k;
import java.util.NoSuchElementException;

public final class m
  implements a, q
{
  private boolean a;
  private final l[] b;
  
  public m(l... paramVarArgs)
  {
    e locale = new com/truecaller/adapter_delegates/e;
    locale.<init>();
    c = locale;
    b = paramVarArgs;
    paramVarArgs = b;
    int i = paramVarArgs.length;
    int j = 1;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramVarArgs = null;
    }
    if (i == 0)
    {
      paramVarArgs = b[0].a;
      i = paramVarArgs.getItemCount();
      l[] arrayOfl = b;
      int k = arrayOfl.length;
      int m = 0;
      while (m < k)
      {
        n localn = a;
        int n = localn.getItemCount();
        if (n != i)
        {
          n = 1;
        }
        else
        {
          n = 0;
          localn = null;
        }
        if (n != 0) {
          break label153;
        }
        m += 1;
      }
      j = 0;
      locale = null;
      label153:
      if (j == 0) {
        return;
      }
      paramVarArgs = new java/lang/IllegalArgumentException;
      paramVarArgs.<init>("All item type data sets should have same size");
      throw ((Throwable)paramVarArgs);
    }
    paramVarArgs = new java/lang/IllegalArgumentException;
    paramVarArgs.<init>("At least one item type required");
    throw ((Throwable)paramVarArgs);
  }
  
  private final l c(int paramInt)
  {
    Object localObject = b;
    int i = localObject.length;
    int j = 0;
    while (j < i)
    {
      locall = localObject[j];
      n localn = a;
      boolean bool = localn.a(paramInt);
      if (bool) {
        break label60;
      }
      j += 1;
    }
    l locall = null;
    label60:
    if (locall != null) {
      return locall;
    }
    localObject = new java/lang/IllegalStateException;
    String str = String.valueOf(paramInt);
    str = "At least one delegate should support position ".concat(str);
    ((IllegalStateException)localObject).<init>(str);
    throw ((Throwable)localObject);
  }
  
  public final int a(int paramInt)
  {
    return paramInt;
  }
  
  public final t a(a parama, s params)
  {
    k.b(parama, "outerDelegate");
    k.b(params, "wrapper");
    return a.a.a(this, parama, params);
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "unwrapper");
    c.a(paramb);
  }
  
  public final void a(boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  public final boolean a(h paramh)
  {
    Object localObject = "event";
    k.b(paramh, (String)localObject);
    int i = b;
    if (i >= 0)
    {
      i = b;
      localObject = ca;
      boolean bool1 = localObject instanceof j;
      if (!bool1)
      {
        i = 0;
        localObject = null;
      }
      localObject = (j)localObject;
      boolean bool2;
      if (localObject != null)
      {
        bool2 = ((j)localObject).a(paramh);
      }
      else
      {
        bool2 = false;
        paramh = null;
      }
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int a_(int paramInt)
  {
    return c.a_(paramInt);
  }
  
  public final boolean b(int paramInt)
  {
    l[] arrayOfl = b;
    int i = arrayOfl.length;
    int j = 0;
    while (j < i)
    {
      l locall = arrayOfl[j];
      int k = b;
      boolean bool = true;
      if (k == paramInt)
      {
        k = 1;
      }
      else
      {
        k = 0;
        locall = null;
      }
      if (k != 0) {
        return bool;
      }
      j += 1;
    }
    return false;
  }
  
  public final int getItemCount()
  {
    boolean bool = a;
    int i = 0;
    if (bool) {
      return 0;
    }
    Object localObject = b;
    String str = "receiver$0";
    k.b(localObject, str);
    int j = localObject.length;
    if (j == 0) {
      i = 1;
    }
    if (i == 0)
    {
      i = f.e((Object[])localObject);
      return a.getItemCount();
    }
    localObject = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject).<init>("Array is empty.");
    throw ((Throwable)localObject);
  }
  
  public final long getItemId(int paramInt)
  {
    return ca.getItemId(paramInt);
  }
  
  public final int getItemViewType(int paramInt)
  {
    return cb;
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    k.b(paramViewHolder, "holder");
    Object localObject = c(paramInt);
    k.b(paramViewHolder, "view");
    localObject = a;
    paramViewHolder = (Object)paramViewHolder;
    ((n)localObject).a(paramViewHolder, paramInt);
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "parent");
    Object localObject1 = b;
    int i = localObject1.length;
    int j = 0;
    while (j < i)
    {
      localObject2 = localObject1[j];
      int k = b;
      if (k == paramInt) {
        k = 1;
      } else {
        k = 0;
      }
      if (k != 0) {
        break label73;
      }
      j += 1;
    }
    Object localObject2 = null;
    label73:
    if (localObject2 != null)
    {
      localObject1 = c;
      if (localObject1 != null)
      {
        paramViewGroup = (RecyclerView.ViewHolder)((b)localObject1).invoke(paramViewGroup);
        if (paramViewGroup != null) {
          return paramViewGroup;
        }
      }
    }
    paramViewGroup = new java/lang/IllegalStateException;
    String str = String.valueOf(paramInt);
    str = "Unsupported view type requested ".concat(str);
    paramViewGroup.<init>(str);
    throw ((Throwable)paramViewGroup);
  }
  
  public final void onViewAttachedToWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    k.b(paramViewHolder, "holder");
  }
  
  public final void onViewDetachedFromWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    k.b(paramViewHolder, "holder");
  }
  
  public final void onViewRecycled(RecyclerView.ViewHolder paramViewHolder)
  {
    k.b(paramViewHolder, "holder");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */