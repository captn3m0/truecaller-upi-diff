package com.truecaller.adapter_delegates;

public final class g
  implements s
{
  private final int a = 0;
  
  public final boolean a(int paramInt1, int paramInt2)
  {
    int i = a;
    paramInt2 += i;
    return (i <= paramInt1) && (paramInt2 > paramInt1);
  }
  
  public final int b(int paramInt1, int paramInt2)
  {
    paramInt2 = a;
    return paramInt1 - paramInt2;
  }
  
  public final int c(int paramInt1, int paramInt2)
  {
    int i = a;
    if (paramInt1 < i) {
      return paramInt1;
    }
    return paramInt1 - paramInt2;
  }
  
  public final int d(int paramInt1, int paramInt2)
  {
    int i = a;
    if (paramInt2 < i) {
      paramInt1 = 0;
    }
    return paramInt2 + paramInt1;
  }
  
  public final int e(int paramInt1, int paramInt2)
  {
    paramInt2 = Math.min(a, paramInt2);
    return paramInt1 + paramInt2;
  }
  
  public final int f(int paramInt1, int paramInt2)
  {
    int i = a;
    if (paramInt1 < i) {
      return paramInt1;
    }
    return paramInt1 + paramInt2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */