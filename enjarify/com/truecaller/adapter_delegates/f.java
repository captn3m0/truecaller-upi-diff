package com.truecaller.adapter_delegates;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import c.g.a.b;
import c.g.b.k;

public final class f
  extends RecyclerView.Adapter
  implements a
{
  private final a a;
  
  public f(a parama)
  {
    a = parama;
  }
  
  public final int a(int paramInt)
  {
    return a.a(paramInt);
  }
  
  public final t a(a parama, s params)
  {
    k.b(parama, "outerDelegate");
    k.b(params, "wrapper");
    return a.a(parama, params);
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "unwrapper");
    a.a(paramb);
  }
  
  public final void a(boolean paramBoolean)
  {
    a.a(paramBoolean);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    return a.a(paramh);
  }
  
  public final int a_(int paramInt)
  {
    return a.a_(paramInt);
  }
  
  public final boolean b(int paramInt)
  {
    return a.b(paramInt);
  }
  
  public final int getItemCount()
  {
    return a.getItemCount();
  }
  
  public final long getItemId(int paramInt)
  {
    return a.getItemId(paramInt);
  }
  
  public final int getItemViewType(int paramInt)
  {
    return a.getItemViewType(paramInt);
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    k.b(paramViewHolder, "holder");
    a.onBindViewHolder(paramViewHolder, paramInt);
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "parent");
    return a.onCreateViewHolder(paramViewGroup, paramInt);
  }
  
  public final void onViewAttachedToWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    k.b(paramViewHolder, "holder");
    a.onViewAttachedToWindow(paramViewHolder);
  }
  
  public final void onViewDetachedFromWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    k.b(paramViewHolder, "holder");
    a.onViewDetachedFromWindow(paramViewHolder);
  }
  
  public final void onViewRecycled(RecyclerView.ViewHolder paramViewHolder)
  {
    k.b(paramViewHolder, "holder");
    a.onViewRecycled(paramViewHolder);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */