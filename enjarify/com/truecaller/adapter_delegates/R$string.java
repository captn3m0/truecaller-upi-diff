package com.truecaller.adapter_delegates;

public final class R$string
{
  public static final int abc_action_bar_home_description = 2131887345;
  public static final int abc_action_bar_up_description = 2131887346;
  public static final int abc_action_menu_overflow_description = 2131887347;
  public static final int abc_action_mode_done = 2131887348;
  public static final int abc_activity_chooser_view_see_all = 2131887349;
  public static final int abc_activitychooserview_choose_application = 2131887350;
  public static final int abc_capital_off = 2131887351;
  public static final int abc_capital_on = 2131887352;
  public static final int abc_font_family_body_1_material = 2131887353;
  public static final int abc_font_family_body_2_material = 2131887354;
  public static final int abc_font_family_button_material = 2131887355;
  public static final int abc_font_family_caption_material = 2131887356;
  public static final int abc_font_family_display_1_material = 2131887357;
  public static final int abc_font_family_display_2_material = 2131887358;
  public static final int abc_font_family_display_3_material = 2131887359;
  public static final int abc_font_family_display_4_material = 2131887360;
  public static final int abc_font_family_headline_material = 2131887361;
  public static final int abc_font_family_menu_material = 2131887362;
  public static final int abc_font_family_subhead_material = 2131887363;
  public static final int abc_font_family_title_material = 2131887364;
  public static final int abc_menu_alt_shortcut_label = 2131887365;
  public static final int abc_menu_ctrl_shortcut_label = 2131887366;
  public static final int abc_menu_delete_shortcut_label = 2131887367;
  public static final int abc_menu_enter_shortcut_label = 2131887368;
  public static final int abc_menu_function_shortcut_label = 2131887369;
  public static final int abc_menu_meta_shortcut_label = 2131887370;
  public static final int abc_menu_shift_shortcut_label = 2131887371;
  public static final int abc_menu_space_shortcut_label = 2131887372;
  public static final int abc_menu_sym_shortcut_label = 2131887373;
  public static final int abc_prepend_shortcut_label = 2131887374;
  public static final int abc_search_hint = 2131887375;
  public static final int abc_searchview_description_clear = 2131887376;
  public static final int abc_searchview_description_query = 2131887377;
  public static final int abc_searchview_description_search = 2131887378;
  public static final int abc_searchview_description_submit = 2131887379;
  public static final int abc_searchview_description_voice = 2131887380;
  public static final int abc_shareactionprovider_share_with = 2131887381;
  public static final int abc_shareactionprovider_share_with_application = 2131887382;
  public static final int abc_toolbar_collapse_description = 2131887383;
  public static final int appbar_scrolling_view_behavior = 2131887453;
  public static final int bottom_sheet_behavior = 2131887536;
  public static final int character_counter_content_description = 2131887657;
  public static final int character_counter_pattern = 2131887658;
  public static final int fab_transformation_scrim_behavior = 2131888032;
  public static final int fab_transformation_sheet_behavior = 2131888033;
  public static final int hide_bottom_view_on_scroll_behavior = 2131888180;
  public static final int mtrl_chip_close_icon_content_description = 2131888387;
  public static final int password_toggle_content_description = 2131888481;
  public static final int path_password_eye = 2131888482;
  public static final int path_password_eye_mask_strike_through = 2131888483;
  public static final int path_password_eye_mask_visible = 2131888484;
  public static final int path_password_strike_through = 2131888485;
  public static final int search_menu_title = 2131888760;
  public static final int status_bar_notification_info_overflow = 2131888855;
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.R.string
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */