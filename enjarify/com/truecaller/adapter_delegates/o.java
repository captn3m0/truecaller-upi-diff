package com.truecaller.adapter_delegates;

public final class o
  implements s
{
  private final int a;
  private final int b;
  private final boolean c = false;
  
  public o()
  {
    this(0, 0, 7);
  }
  
  private o(int paramInt1, int paramInt2)
  {
    int i = 1;
    if (paramInt2 <= i) {
      paramInt2 = 2;
    }
    a = paramInt2;
    if (paramInt1 < 0) {
      paramInt1 = 0;
    }
    b = paramInt1;
  }
  
  private final int g(int paramInt1, int paramInt2)
  {
    int i = b;
    if (paramInt1 > i)
    {
      paramInt1 = paramInt1 - i + -1;
      i = a;
      return Math.min(paramInt1 / i + 1, paramInt2);
    }
    return 0;
  }
  
  public final boolean a(int paramInt1, int paramInt2)
  {
    int i = b;
    if (paramInt1 >= i)
    {
      i = paramInt1 - i;
      int j = a;
      i %= j;
      if (i == 0)
      {
        paramInt1 = g(paramInt1, paramInt2);
        if (paramInt1 < paramInt2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final int b(int paramInt1, int paramInt2)
  {
    return g(paramInt1, paramInt2);
  }
  
  public final int c(int paramInt1, int paramInt2)
  {
    paramInt2 = g(paramInt1, paramInt2);
    return paramInt1 - paramInt2;
  }
  
  public final int d(int paramInt1, int paramInt2)
  {
    if (paramInt2 == 0)
    {
      boolean bool = c;
      if (!bool) {
        return 0;
      }
    }
    int i = b;
    if (paramInt2 < i) {
      return paramInt2;
    }
    i = paramInt2 - i;
    int j = a + -1;
    paramInt1 = Math.min(i / j + 1, paramInt1);
    return paramInt2 + paramInt1;
  }
  
  public final int e(int paramInt1, int paramInt2)
  {
    paramInt2 = b;
    int i = a;
    paramInt1 *= i;
    return paramInt2 + paramInt1;
  }
  
  public final int f(int paramInt1, int paramInt2)
  {
    paramInt2 = b;
    if (paramInt1 < paramInt2) {
      return paramInt1;
    }
    paramInt2 = paramInt1 - paramInt2;
    int i = a + -1;
    paramInt2 = paramInt2 / i + 1;
    return paramInt1 + paramInt2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */