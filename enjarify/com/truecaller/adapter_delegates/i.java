package com.truecaller.adapter_delegates;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.a.a;

public final class i
{
  public static final void a(View paramView, k paramk, RecyclerView.ViewHolder paramViewHolder, a parama, Object paramObject)
  {
    c.g.b.k.b(paramView, "receiver$0");
    c.g.b.k.b(paramk, "receiver");
    c.g.b.k.b(paramViewHolder, "holder");
    c.g.b.k.b(parama, "action");
    Object localObject = new com/truecaller/adapter_delegates/i$a;
    ((i.a)localObject).<init>(paramView, paramk, parama, paramViewHolder, paramObject);
    localObject = (View.OnClickListener)localObject;
    paramView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public static final void a(View paramView, k paramk, RecyclerView.ViewHolder paramViewHolder, String paramString, Object paramObject)
  {
    c.g.b.k.b(paramView, "receiver$0");
    c.g.b.k.b(paramk, "receiver");
    c.g.b.k.b(paramViewHolder, "holder");
    c.g.b.k.b(paramString, "action");
    Object localObject = new com/truecaller/adapter_delegates/i$b;
    ((i.b)localObject).<init>(paramView, paramk, paramString, paramViewHolder, paramObject);
    localObject = (View.OnClickListener)localObject;
    paramView.setOnClickListener((View.OnClickListener)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */