package com.truecaller.adapter_delegates;

import c.g.a.b;
import c.g.b.k;

public final class e
  implements q
{
  private b a;
  
  public e()
  {
    b localb = (b)e.a.a;
    a = localb;
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "unwrapper");
    a = paramb;
  }
  
  public final int a_(int paramInt)
  {
    b localb = a;
    Integer localInteger = Integer.valueOf(paramInt);
    return ((Number)localb.invoke(localInteger)).intValue();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */