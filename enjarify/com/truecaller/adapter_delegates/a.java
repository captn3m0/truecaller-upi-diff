package com.truecaller.adapter_delegates;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;

public abstract interface a
  extends k, q
{
  public abstract int a(int paramInt);
  
  public abstract t a(a parama, s params);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract boolean b(int paramInt);
  
  public abstract int getItemCount();
  
  public abstract long getItemId(int paramInt);
  
  public abstract int getItemViewType(int paramInt);
  
  public abstract void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt);
  
  public abstract RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt);
  
  public abstract void onViewAttachedToWindow(RecyclerView.ViewHolder paramViewHolder);
  
  public abstract void onViewDetachedFromWindow(RecyclerView.ViewHolder paramViewHolder);
  
  public abstract void onViewRecycled(RecyclerView.ViewHolder paramViewHolder);
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */