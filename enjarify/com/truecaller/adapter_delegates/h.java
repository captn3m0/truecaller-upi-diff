package com.truecaller.adapter_delegates;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import c.g.b.k;

public final class h
{
  public static final h.a f;
  public final String a;
  public final int b;
  final long c;
  public final View d;
  public final Object e;
  
  static
  {
    h.a locala = new com/truecaller/adapter_delegates/h$a;
    locala.<init>((byte)0);
    f = locala;
  }
  
  public h(String paramString, int paramInt, long paramLong, View paramView, Object paramObject)
  {
    a = paramString;
    b = paramInt;
    c = paramLong;
    d = paramView;
    e = paramObject;
    int i = b;
    paramInt = -1;
    if (i >= paramInt) {
      return;
    }
    paramString = new java/lang/IllegalStateException;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Illegal position: ");
    int j = b;
    ((StringBuilder)localObject).append(j);
    localObject = ((StringBuilder)localObject).toString();
    paramString.<init>((String)localObject);
    throw ((Throwable)paramString);
  }
  
  public h(String paramString, RecyclerView.ViewHolder paramViewHolder, View paramView, Object paramObject)
  {
    this(paramString, i, l, paramView, paramObject);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof h;
      if (bool2)
      {
        paramObject = (h)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          int i = b;
          int j = b;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            localObject1 = null;
          }
          if (i != 0)
          {
            long l1 = c;
            long l2 = c;
            boolean bool3 = l1 < l2;
            if (!bool3)
            {
              bool3 = true;
            }
            else
            {
              bool3 = false;
              localObject1 = null;
            }
            if (bool3)
            {
              localObject1 = d;
              localObject2 = d;
              bool3 = k.a(localObject1, localObject2);
              if (bool3)
              {
                localObject1 = e;
                paramObject = e;
                boolean bool4 = k.a(localObject1, paramObject);
                if (bool4) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    int k = b;
    int j = (j + k) * 31;
    long l1 = c;
    int m = 32;
    long l2 = l1 >>> m;
    l1 ^= l2;
    int n = (int)l1;
    j = (j + n) * 31;
    Object localObject = d;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = e;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ItemEvent(action=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", position=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", id=");
    long l = c;
    localStringBuilder.append(l);
    localStringBuilder.append(", view=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", data=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */