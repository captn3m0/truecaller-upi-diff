package com.truecaller.adapter_delegates;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import java.util.HashMap;
import java.util.Map;

public final class t
  implements a, q
{
  private final f b;
  private final f c;
  private final f d;
  private boolean e;
  private final a f;
  private final a g;
  private final s h;
  private final boolean i;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[3];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(t.class);
    ((u)localObject).<init>(localb, "uniqueIdGenerator", "getUniqueIdGenerator()Lkotlin/jvm/functions/Function0;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(t.class);
    ((u)localObject).<init>(localb, "innerIdMap", "getInnerIdMap()Lcom/truecaller/adapter_delegates/WrappingDelegator$BidiMap;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(t.class);
    ((u)localObject).<init>(localb, "outerIdMap", "getOuterIdMap()Lcom/truecaller/adapter_delegates/WrappingDelegator$BidiMap;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    a = arrayOfg;
  }
  
  private t(a parama1, a parama2, s params)
  {
    e locale = new com/truecaller/adapter_delegates/e;
    locale.<init>();
    j = locale;
    f = parama1;
    g = parama2;
    h = params;
    i = true;
    parama1 = c.g.a((c.g.a.a)t.d.a);
    b = parama1;
    parama1 = new com/truecaller/adapter_delegates/t$b;
    parama1.<init>(this);
    parama1 = c.g.a((c.g.a.a)parama1);
    c = parama1;
    parama1 = new com/truecaller/adapter_delegates/t$c;
    parama1.<init>(this);
    parama1 = c.g.a((c.g.a.a)parama1);
    d = parama1;
    parama1 = f;
    parama2 = new com/truecaller/adapter_delegates/t$1;
    parama2.<init>(this);
    parama2 = (c.g.a.b)parama2;
    parama1.a(parama2);
    parama1 = g;
    parama2 = new com/truecaller/adapter_delegates/t$2;
    parama2.<init>(this);
    parama2 = (c.g.a.b)parama2;
    parama1.a(parama2);
  }
  
  private final t.a a()
  {
    return (t.a)c.b();
  }
  
  private final t.a b()
  {
    return (t.a)d.b();
  }
  
  private final boolean c(int paramInt)
  {
    s locals = h;
    int k = f.getItemCount();
    g.getItemCount();
    return locals.a(paramInt, k);
  }
  
  public final int a(int paramInt)
  {
    boolean bool = c(paramInt);
    a locala1;
    s locals;
    int k;
    a locala2;
    if (bool)
    {
      locala1 = f;
      locals = h;
      k = locala1.getItemCount();
      locala2 = g;
      locala2.getItemCount();
    }
    for (paramInt = locals.b(paramInt, k);; paramInt = locals.c(paramInt, k))
    {
      return locala1.a(paramInt);
      locala1 = g;
      locals = h;
      a locala3 = f;
      k = locala3.getItemCount();
      locala2 = g;
      locala2.getItemCount();
    }
  }
  
  public final t a(a parama, s params)
  {
    k.b(parama, "outerDelegate");
    k.b(params, "wrapper");
    return a.a.a(this, parama, params);
  }
  
  public final void a(c.g.a.b paramb)
  {
    k.b(paramb, "unwrapper");
    j.a(paramb);
  }
  
  public final void a(boolean paramBoolean)
  {
    e = paramBoolean;
  }
  
  public final boolean a(h paramh)
  {
    Object localObject = "event";
    k.b(paramh, (String)localObject);
    int k = b;
    if (k < 0) {
      return false;
    }
    boolean bool1 = i;
    long l2;
    if (bool1)
    {
      int m = b;
      long l1 = c;
      boolean bool2 = c(m);
      if (bool2) {
        localObject = b();
      } else {
        localObject = a();
      }
      localObject = b;
      Long localLong = Long.valueOf(l1);
      localObject = (Long)((HashMap)localObject).get(localLong);
      if (localObject != null) {
        l2 = ((Long)localObject).longValue();
      } else {
        l2 = -1;
      }
    }
    else
    {
      l2 = c;
    }
    int n = b;
    boolean bool3 = c(n);
    if (bool3)
    {
      locala = f;
      locals = h;
      i1 = locala.getItemCount();
      g.getItemCount();
      n = locals.b(n, i1);
      paramh = h.a(paramh, n, l2);
      return locala.a(paramh);
    }
    a locala = g;
    s locals = h;
    int i1 = f.getItemCount();
    g.getItemCount();
    n = locals.c(n, i1);
    paramh = h.a(paramh, n, l2);
    return locala.a(paramh);
  }
  
  public final int a_(int paramInt)
  {
    return j.a_(paramInt);
  }
  
  public final boolean b(int paramInt)
  {
    a locala = f;
    boolean bool = locala.b(paramInt);
    if (!bool)
    {
      locala = g;
      paramInt = locala.b(paramInt);
      if (paramInt == 0) {
        return false;
      }
    }
    return true;
  }
  
  public final int getItemCount()
  {
    boolean bool = e;
    if (!bool)
    {
      s locals = h;
      int k = f.getItemCount();
      int m = g.getItemCount();
      return locals.d(k, m);
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    boolean bool1 = i;
    if (bool1)
    {
      bool1 = c(paramInt);
      int m;
      if (bool1)
      {
        localObject1 = f;
        locals = h;
        k = ((a)localObject1).getItemCount();
        localObject2 = g;
        ((a)localObject2).getItemCount();
        m = locals.b(paramInt, k);
      }
      else
      {
        localObject1 = g;
        locals = h;
        localObject3 = f;
        k = ((a)localObject3).getItemCount();
        localObject2 = g;
        ((a)localObject2).getItemCount();
        m = locals.c(paramInt, k);
      }
      long l1 = ((a)localObject1).getItemId(m);
      long l2 = -1;
      boolean bool2 = l1 < l2;
      if (!bool2) {
        return l2;
      }
      paramInt = c(paramInt);
      if (paramInt != 0) {
        localObject4 = b();
      } else {
        localObject4 = a();
      }
      Object localObject3 = a;
      Object localObject2 = Long.valueOf(l1);
      localObject3 = (Long)((HashMap)localObject3).get(localObject2);
      if (localObject3 != null) {
        return ((Long)localObject3).longValue();
      }
      localObject3 = (Number)c.invoke();
      long l3 = ((Number)localObject3).longValue();
      Map localMap = (Map)a;
      Long localLong1 = Long.valueOf(l1);
      Long localLong2 = Long.valueOf(l3);
      localMap.put(localLong1, localLong2);
      Object localObject4 = (Map)b;
      localObject2 = Long.valueOf(l3);
      localObject1 = Long.valueOf(l1);
      ((Map)localObject4).put(localObject2, localObject1);
      return ((Number)localObject3).longValue();
    }
    bool1 = c(paramInt);
    if (bool1)
    {
      localObject1 = f;
      locals = h;
      k = ((a)localObject1).getItemCount();
      g.getItemCount();
      paramInt = locals.b(paramInt, k);
      return ((a)localObject1).getItemId(paramInt);
    }
    Object localObject1 = g;
    s locals = h;
    int k = f.getItemCount();
    g.getItemCount();
    paramInt = locals.c(paramInt, k);
    return ((a)localObject1).getItemId(paramInt);
  }
  
  public final int getItemViewType(int paramInt)
  {
    boolean bool = c(paramInt);
    a locala1;
    s locals;
    int k;
    a locala2;
    if (bool)
    {
      locala1 = f;
      locals = h;
      k = locala1.getItemCount();
      locala2 = g;
      locala2.getItemCount();
    }
    for (paramInt = locals.b(paramInt, k);; paramInt = locals.c(paramInt, k))
    {
      return locala1.getItemViewType(paramInt);
      locala1 = g;
      locals = h;
      a locala3 = f;
      k = locala3.getItemCount();
      locala2 = g;
      locala2.getItemCount();
    }
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    Object localObject = "holder";
    k.b(paramViewHolder, (String)localObject);
    boolean bool = c(paramInt);
    s locals;
    int k;
    a locala1;
    if (bool)
    {
      localObject = f;
      locals = h;
      k = ((a)localObject).getItemCount();
      locala1 = g;
      locala1.getItemCount();
    }
    for (paramInt = locals.b(paramInt, k);; paramInt = locals.c(paramInt, k))
    {
      ((a)localObject).onBindViewHolder(paramViewHolder, paramInt);
      return;
      localObject = g;
      locals = h;
      a locala2 = f;
      k = locala2.getItemCount();
      locala1 = g;
      locala1.getItemCount();
    }
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "parent");
    a locala = f;
    boolean bool = locala.b(paramInt);
    if (bool) {
      return f.onCreateViewHolder(paramViewGroup, paramInt);
    }
    return g.onCreateViewHolder(paramViewGroup, paramInt);
  }
  
  public final void onViewAttachedToWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    String str = "holder";
    k.b(paramViewHolder, str);
    int k = paramViewHolder.getItemViewType();
    a locala = f;
    boolean bool = locala.b(k);
    if (bool)
    {
      f.onViewAttachedToWindow(paramViewHolder);
      return;
    }
    g.onViewAttachedToWindow(paramViewHolder);
  }
  
  public final void onViewDetachedFromWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    String str = "holder";
    k.b(paramViewHolder, str);
    int k = paramViewHolder.getItemViewType();
    a locala = f;
    boolean bool = locala.b(k);
    if (bool)
    {
      f.onViewDetachedFromWindow(paramViewHolder);
      return;
    }
    g.onViewDetachedFromWindow(paramViewHolder);
  }
  
  public final void onViewRecycled(RecyclerView.ViewHolder paramViewHolder)
  {
    String str = "holder";
    k.b(paramViewHolder, str);
    int k = paramViewHolder.getItemViewType();
    a locala = f;
    boolean bool = locala.b(k);
    if (bool)
    {
      f.onViewRecycled(paramViewHolder);
      return;
    }
    g.onViewRecycled(paramViewHolder);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */