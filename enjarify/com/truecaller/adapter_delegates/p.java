package com.truecaller.adapter_delegates;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import c.g.b.k;

public final class p
  implements a, b, q
{
  public boolean a;
  private final b b;
  private final int c;
  private final c.g.a.b d;
  private final c.g.a.b e;
  
  public p(b paramb, int paramInt, c.g.a.b paramb1, c.g.a.b paramb2)
  {
    e locale = new com/truecaller/adapter_delegates/e;
    locale.<init>();
    f = locale;
    b = paramb;
    c = paramInt;
    d = paramb1;
    e = paramb2;
  }
  
  public final int a(int paramInt)
  {
    return paramInt;
  }
  
  public final t a(a parama, s params)
  {
    k.b(parama, "outerDelegate");
    k.b(params, "wrapper");
    return a.a.a(this, parama, params);
  }
  
  public final void a(c.g.a.b paramb)
  {
    k.b(paramb, "unwrapper");
    f.a(paramb);
  }
  
  public final void a(Object paramObject)
  {
    b.a(paramObject);
  }
  
  public final void a(Object paramObject, int paramInt)
  {
    b.a(paramObject, paramInt);
  }
  
  public final void a(boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  public final boolean a(h paramh)
  {
    Object localObject = "event";
    k.b(paramh, (String)localObject);
    int i = b;
    if (i >= 0)
    {
      localObject = b;
      boolean bool1 = localObject instanceof j;
      if (!bool1)
      {
        i = 0;
        localObject = null;
      }
      localObject = (j)localObject;
      boolean bool2;
      if (localObject != null)
      {
        bool2 = ((j)localObject).a(paramh);
      }
      else
      {
        bool2 = false;
        paramh = null;
      }
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int a_(int paramInt)
  {
    return f.a_(paramInt);
  }
  
  public final void b(Object paramObject)
  {
    b.b(paramObject);
  }
  
  public final void b(boolean paramBoolean)
  {
    b.b(paramBoolean);
  }
  
  public final boolean b(int paramInt)
  {
    int i = c;
    return i == paramInt;
  }
  
  public final void c(Object paramObject)
  {
    b.c(paramObject);
  }
  
  public final void d(Object paramObject)
  {
    b.d(paramObject);
  }
  
  public final int getItemCount()
  {
    boolean bool = a;
    if (!bool) {
      return b.getItemCount();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return b.getItemId(paramInt);
  }
  
  public final int getItemViewType(int paramInt)
  {
    return c;
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    k.b(paramViewHolder, "holder");
    paramViewHolder = e.invoke(paramViewHolder);
    a(paramViewHolder, paramInt);
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "parent");
    Object localObject1 = d;
    Object localObject2 = LayoutInflater.from(paramViewGroup.getContext());
    int i = c;
    paramViewGroup = ((LayoutInflater)localObject2).inflate(i, paramViewGroup, false);
    k.a(paramViewGroup, "LayoutInflater\n         …layoutRes, parent, false)");
    paramViewGroup = (RecyclerView.ViewHolder)((c.g.a.b)localObject1).invoke(paramViewGroup);
    localObject1 = b;
    localObject2 = e.invoke(paramViewGroup);
    ((b)localObject1).a(localObject2);
    return paramViewGroup;
  }
  
  public final void onViewAttachedToWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    k.b(paramViewHolder, "holder");
    paramViewHolder = e.invoke(paramViewHolder);
    c(paramViewHolder);
  }
  
  public final void onViewDetachedFromWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    k.b(paramViewHolder, "holder");
    paramViewHolder = e.invoke(paramViewHolder);
    d(paramViewHolder);
  }
  
  public final void onViewRecycled(RecyclerView.ViewHolder paramViewHolder)
  {
    k.b(paramViewHolder, "holder");
    paramViewHolder = e.invoke(paramViewHolder);
    b(paramViewHolder);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.adapter_delegates.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */