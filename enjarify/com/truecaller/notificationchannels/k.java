package com.truecaller.notificationchannels;

import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioAttributes.Builder;
import android.net.Uri;
import android.os.Build.VERSION;
import c.a.m;
import c.j.c;
import java.util.List;

public final class k
  extends a
  implements j
{
  private final SharedPreferences c;
  private final String d;
  private final String e;
  private final Context f;
  private final h g;
  
  public k(Context paramContext, h paramh)
  {
    super(paramContext);
    f = paramContext;
    g = paramh;
    paramContext = f.getSharedPreferences("notifications.settings", 0);
    c.g.b.k.a(paramContext, "context.getSharedPrefere…E, Activity.MODE_PRIVATE)");
    c = paramContext;
    d = "spam_sms";
    e = "blocked_sms";
  }
  
  private final NotificationChannel a(String paramString1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2, String paramString2, Uri paramUri)
  {
    NotificationChannel localNotificationChannel = new android/app/NotificationChannel;
    CharSequence localCharSequence = (CharSequence)f.getString(paramInt1);
    localNotificationChannel.<init>(paramString1, localCharSequence, paramInt3);
    paramString1 = f.getString(paramInt2);
    localNotificationChannel.setDescription(paramString1);
    localNotificationChannel.enableLights(paramBoolean1);
    localNotificationChannel.enableVibration(paramBoolean2);
    localNotificationChannel.setLightColor(paramInt4);
    localNotificationChannel.setGroup(paramString2);
    paramString1 = new android/media/AudioAttributes$Builder;
    paramString1.<init>();
    paramString1 = paramString1.setContentType(4).setUsage(5).build();
    localNotificationChannel.setSound(paramUri, paramString1);
    return localNotificationChannel;
  }
  
  private final String a(int paramInt, Uri paramUri, boolean paramBoolean)
  {
    k localk = this;
    int i = paramInt;
    String str1 = m();
    Object localObject1 = c;
    Object localObject2 = "non_spam_sms_settings_hash_key";
    int j = -1;
    int k = ((SharedPreferences)localObject1).getInt((String)localObject2, j);
    if (paramInt != k)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("non_spam_sms_v2");
      localObject2 = c.c;
      int m = c.d().b();
      ((StringBuilder)localObject1).append(m);
      String str2 = ((StringBuilder)localObject1).toString();
      j = R.string.notification_channels_channel_non_spam_sms;
      int n = R.string.notification_channels_channel_description_non_spam_sms;
      int i1 = 5;
      int i2 = 176;
      localObject1 = this;
      localObject2 = str2;
      localObject1 = a(this, str2, j, n, i1, 0, false, paramBoolean, null, paramUri, i2);
      localObject2 = b;
      if (localObject2 != null)
      {
        ((NotificationManager)localObject2).deleteNotificationChannel(str1);
        ((NotificationManager)localObject2).createNotificationChannel((NotificationChannel)localObject1);
      }
      c.edit().putString("non_spam_sms_channel_id_key", str2).putInt("non_spam_sms_settings_hash_key", i).apply();
      return str2;
    }
    return str1;
  }
  
  private Uri k()
  {
    h localh = g;
    if (localh != null) {
      return localh.a();
    }
    return null;
  }
  
  private boolean l()
  {
    h localh = g;
    if (localh != null) {
      return localh.b();
    }
    return true;
  }
  
  private final String m()
  {
    Object localObject = c;
    String str1 = "non_spam_sms_channel_id_key";
    String str2 = "non_spam_sms_v2";
    localObject = ((SharedPreferences)localObject).getString(str1, str2);
    if (localObject == null) {
      localObject = "non_spam_sms_v2";
    }
    return (String)localObject;
  }
  
  public final String a()
  {
    return d;
  }
  
  public final String d()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    if (i >= j)
    {
      String str1 = String.valueOf(h());
      i = str1.hashCode();
      int k = i();
      i += k;
      Object localObject = c;
      String str2 = "non_spam_sms_settings_hash_key";
      int n = -1;
      int m = ((SharedPreferences)localObject).getInt(str2, n);
      if (m == n)
      {
        localObject = m();
        c.edit().putString("non_spam_sms_channel_id_key", (String)localObject).putInt("non_spam_sms_settings_hash_key", i).apply();
        return (String)localObject;
      }
      localObject = h();
      boolean bool = i();
      return a(i, (Uri)localObject, bool);
    }
    return m();
  }
  
  public final List e()
  {
    NotificationChannel[] arrayOfNotificationChannel = new NotificationChannel[3];
    String str1 = d();
    int i = R.string.notification_channels_channel_non_spam_sms;
    int j = R.string.notification_channels_channel_description_non_spam_sms;
    Object localObject = this;
    localObject = a(this, str1, i, j, 5, 0, false, false, null, null, 496);
    arrayOfNotificationChannel[0] = localObject;
    String str2 = d;
    int k = R.string.notification_channels_channel_spam_sms;
    int m = R.string.notification_channels_channel_description_spam_sms;
    localObject = a(this, str2, k, m, 0, 0, false, false, null, null, 504);
    arrayOfNotificationChannel[1] = localObject;
    str2 = e;
    k = R.string.notification_channels_channel_blocked_sms;
    m = R.string.notification_channels_channel_description_blocked_sms;
    localObject = a(this, str2, k, m, 0, 0, false, false, null, null, 472);
    arrayOfNotificationChannel[2] = localObject;
    return m.b(arrayOfNotificationChannel);
  }
  
  public final List f()
  {
    NotificationChannelGroup localNotificationChannelGroup = new android/app/NotificationChannelGroup;
    Object localObject = f;
    int i = R.string.notification_channels_group_sms;
    localObject = (CharSequence)((Context)localObject).getString(i);
    localNotificationChannelGroup.<init>("sms", (CharSequence)localObject);
    return m.a(localNotificationChannelGroup);
  }
  
  public final String g()
  {
    return e;
  }
  
  public final Uri h()
  {
    Uri localUri = k();
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    if (i >= j)
    {
      Object localObject = m();
      NotificationManager localNotificationManager = b;
      if (localNotificationManager != null)
      {
        localObject = localNotificationManager.getNotificationChannel((String)localObject);
        if (localObject != null)
        {
          localObject = ((NotificationChannel)localObject).getSound();
          break label63;
        }
      }
      i = 0;
      localObject = null;
      label63:
      if (localObject != null)
      {
        boolean bool = c.g.b.k.a(localObject, localUri) ^ true;
        if (bool) {
          return (Uri)localObject;
        }
      }
    }
    return localUri;
  }
  
  public final boolean i()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    if (i >= j)
    {
      Object localObject1 = m();
      Object localObject2 = b;
      if (localObject2 != null)
      {
        localObject1 = ((NotificationManager)localObject2).getNotificationChannel((String)localObject1);
        if (localObject1 != null)
        {
          bool1 = ((NotificationChannel)localObject1).shouldVibrate();
          localObject1 = Boolean.valueOf(bool1);
          break label56;
        }
      }
      boolean bool1 = false;
      localObject1 = null;
      label56:
      if (localObject1 != null)
      {
        localObject2 = Boolean.valueOf(l());
        boolean bool2 = c.g.b.k.a(localObject1, localObject2) ^ true;
        if (bool2) {
          return ((Boolean)localObject1).booleanValue();
        }
      }
    }
    return l();
  }
  
  public final void j()
  {
    int i = String.valueOf(k()).hashCode();
    int j = l();
    i += j;
    Uri localUri = k();
    boolean bool = l();
    a(i, localUri, bool);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */