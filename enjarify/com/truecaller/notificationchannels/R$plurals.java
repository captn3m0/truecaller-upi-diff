package com.truecaller.notificationchannels;

public final class R$plurals
{
  public static final int joda_time_android_abbrev_in_num_days = 2131755043;
  public static final int joda_time_android_abbrev_in_num_hours = 2131755044;
  public static final int joda_time_android_abbrev_in_num_minutes = 2131755045;
  public static final int joda_time_android_abbrev_in_num_seconds = 2131755046;
  public static final int joda_time_android_abbrev_num_days_ago = 2131755047;
  public static final int joda_time_android_abbrev_num_hours_ago = 2131755048;
  public static final int joda_time_android_abbrev_num_minutes_ago = 2131755049;
  public static final int joda_time_android_abbrev_num_seconds_ago = 2131755050;
  public static final int joda_time_android_duration_hours = 2131755051;
  public static final int joda_time_android_duration_minutes = 2131755052;
  public static final int joda_time_android_duration_seconds = 2131755053;
  public static final int joda_time_android_in_num_days = 2131755054;
  public static final int joda_time_android_in_num_hours = 2131755055;
  public static final int joda_time_android_in_num_minutes = 2131755056;
  public static final int joda_time_android_in_num_seconds = 2131755057;
  public static final int joda_time_android_num_days_ago = 2131755058;
  public static final int joda_time_android_num_hours_ago = 2131755059;
  public static final int joda_time_android_num_minutes_ago = 2131755060;
  public static final int joda_time_android_num_seconds_ago = 2131755061;
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.R.plurals
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */