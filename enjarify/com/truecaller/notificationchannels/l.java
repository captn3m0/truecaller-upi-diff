package com.truecaller.notificationchannels;

import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private l(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static l a(Provider paramProvider1, Provider paramProvider2)
  {
    l locall = new com/truecaller/notificationchannels/l;
    locall.<init>(paramProvider1, paramProvider2);
    return locall;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */