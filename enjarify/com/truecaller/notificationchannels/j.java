package com.truecaller.notificationchannels;

import android.net.Uri;

public abstract interface j
  extends m
{
  public abstract String a();
  
  public abstract String d();
  
  public abstract String g();
  
  public abstract Uri h();
  
  public abstract boolean i();
  
  public abstract void j();
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */