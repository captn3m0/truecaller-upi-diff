package com.truecaller.notificationchannels;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import c.a.m;
import c.a.y;
import java.util.List;

public final class f
  extends a
  implements e
{
  private final String c;
  private final String d;
  private final String e;
  private final String f;
  private final String g;
  private final Context h;
  
  public f(Context paramContext)
  {
    super(paramContext);
    h = paramContext;
    c = "miscellaneous_channel";
    d = "caller_id";
    e = "backup";
    f = "flash";
    g = "profile_share";
  }
  
  private final NotificationChannel t()
  {
    NotificationChannel localNotificationChannel = new android/app/NotificationChannel;
    Object localObject1 = h;
    int i = R.string.notification_channels_channel_profile_views;
    localObject1 = (CharSequence)((Context)localObject1).getString(i);
    localNotificationChannel.<init>("profile_views", (CharSequence)localObject1, 4);
    Object localObject2 = h;
    int j = R.string.notification_channels_channel_description_profile_views;
    localObject2 = ((Context)localObject2).getString(j);
    localNotificationChannel.setDescription((String)localObject2);
    boolean bool = true;
    localNotificationChannel.enableLights(bool);
    j = Z_();
    localNotificationChannel.setLightColor(j);
    localNotificationChannel.enableVibration(bool);
    localObject2 = new long[3];
    Object tmp90_88 = localObject2;
    tmp90_88[0] = 500L;
    Object tmp96_90 = tmp90_88;
    tmp96_90[1] = 100;
    tmp96_90[2] = 500L;
    localNotificationChannel.setVibrationPattern((long[])localObject2);
    return localNotificationChannel;
  }
  
  private final NotificationChannel u()
  {
    NotificationChannel localNotificationChannel = new android/app/NotificationChannel;
    Object localObject1 = h;
    int i = R.string.notification_channels_channel_engagement_rewards;
    localObject1 = (CharSequence)((Context)localObject1).getString(i);
    localNotificationChannel.<init>("engagement_rewards", (CharSequence)localObject1, 4);
    Object localObject2 = h;
    int j = R.string.notification_channels_channel_engagement_rewards;
    localObject2 = ((Context)localObject2).getString(j);
    localNotificationChannel.setDescription((String)localObject2);
    return localNotificationChannel;
  }
  
  public final String a()
  {
    return c;
  }
  
  public final String d()
  {
    return d;
  }
  
  public final List e()
  {
    NotificationChannel[] arrayOfNotificationChannel = new NotificationChannel[7];
    NotificationChannel localNotificationChannel = new android/app/NotificationChannel;
    Object localObject1 = h;
    int i = R.string.notification_channels_channel_miscellaneous;
    localObject1 = (CharSequence)((Context)localObject1).getString(i);
    i = 2;
    localNotificationChannel.<init>("miscellaneous_channel", (CharSequence)localObject1, i);
    Object localObject2 = h;
    int j = R.string.notification_channels_channel_description_miscellaneous;
    localObject2 = ((Context)localObject2).getString(j);
    localNotificationChannel.setDescription((String)localObject2);
    int k = 1;
    localNotificationChannel.enableLights(k);
    j = Z_();
    localNotificationChannel.setLightColor(j);
    arrayOfNotificationChannel[0] = localNotificationChannel;
    localNotificationChannel = new android/app/NotificationChannel;
    Object localObject3 = h;
    int m = R.string.notification_channels_channel_caller_id;
    localObject3 = (CharSequence)((Context)localObject3).getString(m);
    localNotificationChannel.<init>("caller_id", (CharSequence)localObject3, k);
    localObject1 = h;
    int n = R.string.notification_channels_channel_description_caller_id;
    localObject1 = ((Context)localObject1).getString(n);
    localNotificationChannel.setDescription((String)localObject1);
    arrayOfNotificationChannel[k] = localNotificationChannel;
    localNotificationChannel = new android/app/NotificationChannel;
    localObject3 = h;
    m = R.string.notification_channels_channel_backup;
    localObject3 = (CharSequence)((Context)localObject3).getString(m);
    localNotificationChannel.<init>("backup", (CharSequence)localObject3, i);
    localObject1 = h;
    n = R.string.notification_channels_channel_description_backup;
    localObject1 = ((Context)localObject1).getString(n);
    localNotificationChannel.setDescription((String)localObject1);
    localNotificationChannel.enableLights(k);
    j = Z_();
    localNotificationChannel.setLightColor(j);
    arrayOfNotificationChannel[i] = localNotificationChannel;
    localNotificationChannel = new android/app/NotificationChannel;
    localObject3 = h;
    m = R.string.notification_channels_channel_flash;
    localObject3 = (CharSequence)((Context)localObject3).getString(m);
    localNotificationChannel.<init>("flash", (CharSequence)localObject3, i);
    localObject1 = h;
    i = R.string.notification_channels_channel_description_flash;
    localObject1 = ((Context)localObject1).getString(i);
    localNotificationChannel.setDescription((String)localObject1);
    localNotificationChannel.enableLights(k);
    j = Z_();
    localNotificationChannel.setLightColor(j);
    j = 3;
    arrayOfNotificationChannel[j] = localNotificationChannel;
    localNotificationChannel = new android/app/NotificationChannel;
    localObject3 = h;
    m = R.string.notification_channels_channel_profile_share;
    localObject3 = (CharSequence)((Context)localObject3).getString(m);
    m = 4;
    localNotificationChannel.<init>("profile_share", (CharSequence)localObject3, m);
    Object localObject4 = h;
    n = R.string.notification_channels_channel_description_profile_share;
    localObject4 = ((Context)localObject4).getString(n);
    localNotificationChannel.setDescription((String)localObject4);
    localNotificationChannel.enableLights(k);
    i = Z_();
    localNotificationChannel.setLightColor(i);
    localNotificationChannel.enableVibration(k);
    localObject2 = new long[j];
    Object tmp422_420 = localObject2;
    tmp422_420[0] = 500L;
    Object tmp428_422 = tmp422_420;
    tmp428_422[1] = 100;
    tmp428_422[2] = 500L;
    localNotificationChannel.setVibrationPattern((long[])localObject2);
    arrayOfNotificationChannel[m] = localNotificationChannel;
    localNotificationChannel = t();
    arrayOfNotificationChannel[5] = localNotificationChannel;
    localNotificationChannel = u();
    arrayOfNotificationChannel[6] = localNotificationChannel;
    return m.b(arrayOfNotificationChannel);
  }
  
  public final List f()
  {
    return (List)y.a;
  }
  
  public final String g()
  {
    return e;
  }
  
  public final String h()
  {
    return f;
  }
  
  public final String i()
  {
    return g;
  }
  
  public final String j()
  {
    boolean bool = Y_();
    if (!bool) {
      return null;
    }
    Object localObject = b;
    if (localObject == null) {
      return null;
    }
    String str = "truecaller_pay_v2";
    localObject = ((NotificationManager)localObject).getNotificationChannel(str);
    if (localObject != null) {
      return ((NotificationChannel)localObject).getId();
    }
    return null;
  }
  
  public final String k()
  {
    boolean bool = Y_();
    if (!bool) {
      return null;
    }
    Object localObject = b;
    if (localObject == null) {
      return null;
    }
    String str = "engagement_rewards";
    localObject = ((NotificationManager)localObject).getNotificationChannel(str);
    if (localObject != null) {
      return ((NotificationChannel)localObject).getId();
    }
    return null;
  }
  
  public final void l()
  {
    boolean bool = Y_();
    if (!bool) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      NotificationChannel localNotificationChannel = new android/app/NotificationChannel;
      Object localObject1 = h;
      int i = R.string.notification_channels_channel_truecaller_pay;
      localObject1 = (CharSequence)((Context)localObject1).getString(i);
      localNotificationChannel.<init>("truecaller_pay_v2", (CharSequence)localObject1, 4);
      Object localObject2 = h;
      int j = R.string.notification_channels_channel_description_truecaller_pay;
      localObject2 = ((Context)localObject2).getString(j);
      localNotificationChannel.setDescription((String)localObject2);
      localNotificationChannel.enableLights(true);
      int k = Z_();
      localNotificationChannel.setLightColor(k);
      localNotificationManager.createNotificationChannel(localNotificationChannel);
      return;
    }
  }
  
  public final void m()
  {
    boolean bool = Y_();
    if (!bool) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      localNotificationManager.deleteNotificationChannel("truecaller_pay_v2");
      return;
    }
  }
  
  public final void n()
  {
    boolean bool = Y_();
    if (!bool) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      localNotificationManager.deleteNotificationChannel("dapp_search");
      return;
    }
  }
  
  public final String o()
  {
    boolean bool = Y_();
    if (!bool) {
      return null;
    }
    Object localObject = b;
    if (localObject == null) {
      return null;
    }
    String str = "profile_views";
    localObject = ((NotificationManager)localObject).getNotificationChannel(str);
    if (localObject != null) {
      return ((NotificationChannel)localObject).getId();
    }
    return null;
  }
  
  public final void p()
  {
    boolean bool = Y_();
    if (!bool) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      NotificationChannel localNotificationChannel = t();
      localNotificationManager.createNotificationChannel(localNotificationChannel);
      return;
    }
  }
  
  public final void q()
  {
    boolean bool = Y_();
    if (!bool) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      localNotificationManager.deleteNotificationChannel("profile_views");
      return;
    }
  }
  
  public final void r()
  {
    boolean bool = Y_();
    if (!bool) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      NotificationChannel localNotificationChannel = u();
      localNotificationManager.createNotificationChannel(localNotificationChannel);
      return;
    }
  }
  
  public final void s()
  {
    boolean bool = Y_();
    if (!bool) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      localNotificationManager.deleteNotificationChannel("engagement_rewards");
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */