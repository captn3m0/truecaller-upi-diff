package com.truecaller.notificationchannels;

import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build.VERSION;
import java.util.Iterator;
import java.util.List;

public final class q
  implements p
{
  private final NotificationManager a;
  private final List b;
  
  public q(Context paramContext, f paramf, c paramc, k paramk)
  {
    paramContext = (NotificationManager)paramContext.getSystemService("notification");
    a = paramContext;
    paramContext = new m[3];
    paramf = (m)paramf;
    paramContext[0] = paramf;
    paramc = (m)paramc;
    paramContext[1] = paramc;
    paramk = (m)paramk;
    paramContext[2] = paramk;
    paramContext = c.a.m.b(paramContext);
    b = paramContext;
  }
  
  public final void a()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    if (i < j) {
      return;
    }
    Object localObject = a;
    j = 1;
    int k = 0;
    if (localObject != null)
    {
      localObject = ((NotificationManager)localObject).getNotificationChannels();
      boolean bool2 = ((List)localObject).isEmpty();
      if (!bool2)
      {
        int m = ((List)localObject).size();
        if (m == j)
        {
          localObject = (NotificationChannel)((List)localObject).get(0);
          c.g.b.k.a(localObject, "channel");
          localObject = ((NotificationChannel)localObject).getId();
          String str = "miscellaneous";
          boolean bool1 = c.g.b.k.a(localObject, str);
          if (bool1) {}
        }
        else
        {
          k = 1;
        }
      }
    }
    if (k != 0) {
      return;
    }
    b();
  }
  
  public final void b()
  {
    new String[1][0] = "Updating notification channels";
    Iterator localIterator = ((Iterable)b).iterator();
    boolean bool;
    m localm;
    for (;;)
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localm = (m)localIterator.next();
      localm.b();
    }
    localIterator = ((Iterable)b).iterator();
    for (;;)
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localm = (m)localIterator.next();
      localm.c();
    }
  }
  
  public final void c()
  {
    new String[1][0] = "Recreating notification channels";
    NotificationManager localNotificationManager = a;
    if (localNotificationManager == null) {
      return;
    }
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    if (i < j) {
      return;
    }
    Object localObject1 = localNotificationManager.getNotificationChannelGroups();
    Object localObject2 = "notificationManager.notificationChannelGroups";
    c.g.b.k.a(localObject1, (String)localObject2);
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool1;
    String str1;
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (NotificationChannelGroup)((Iterator)localObject1).next();
      str1 = "it";
      c.g.b.k.a(localObject2, str1);
      localObject2 = ((NotificationChannelGroup)localObject2).getId();
      localNotificationManager.deleteNotificationChannelGroup((String)localObject2);
    }
    localObject1 = localNotificationManager.getNotificationChannels();
    localObject2 = "notificationManager.notificationChannels";
    c.g.b.k.a(localObject1, (String)localObject2);
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (NotificationChannel)((Iterator)localObject1).next();
      c.g.b.k.a(localObject2, "it");
      str1 = ((NotificationChannel)localObject2).getId();
      String str2 = "miscellaneous";
      boolean bool2 = c.g.b.k.a(str1, str2);
      if (!bool2)
      {
        localObject2 = ((NotificationChannel)localObject2).getId();
        localNotificationManager.deleteNotificationChannel((String)localObject2);
      }
    }
    b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */