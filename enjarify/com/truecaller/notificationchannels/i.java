package com.truecaller.notificationchannels;

import android.content.Context;
import dagger.a.c;
import javax.inject.Provider;

public final class i
  implements o
{
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  
  private i(Context paramContext, h paramh)
  {
    paramContext = dagger.a.e.a(paramContext);
    b = paramContext;
    paramContext = g.a(b);
    c = paramContext;
    paramContext = c.a(c);
    d = paramContext;
    paramContext = b;
    Provider localProvider1 = d;
    paramContext = d.a(paramContext, localProvider1);
    e = paramContext;
    paramContext = dagger.a.e.b(paramh);
    f = paramContext;
    paramContext = b;
    paramh = f;
    paramContext = l.a(paramContext, paramh);
    g = paramContext;
    paramContext = b;
    paramh = c;
    localProvider1 = e;
    Provider localProvider2 = g;
    paramContext = s.a(paramContext, paramh, localProvider1, localProvider2);
    h = paramContext;
    paramContext = c.a(h);
    i = paramContext;
    paramContext = c.a(e);
    j = paramContext;
    paramContext = c.a(g);
    k = paramContext;
  }
  
  public static o.a a()
  {
    i.a locala = new com/truecaller/notificationchannels/i$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final p b()
  {
    return (p)i.get();
  }
  
  public final e c()
  {
    return (e)d.get();
  }
  
  public final b d()
  {
    return (b)j.get();
  }
  
  public final j e()
  {
    return (j)k.get();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */