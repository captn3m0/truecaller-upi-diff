package com.truecaller.notificationchannels;

import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import c.a.m;
import c.g.b.k;
import java.util.List;

public final class c
  extends a
  implements b
{
  private final String c;
  private final String d;
  private final String e;
  private final String f;
  private final String g;
  private final Context h;
  private final e i;
  
  public c(Context paramContext, e parame)
  {
    super(paramContext);
    h = paramContext;
    i = parame;
    c = "missed_calls";
    d = "missed_calls_reminder";
    e = "blocked_calls";
    f = "push_caller_id";
    g = "phone_calls";
  }
  
  public final String X_()
  {
    return c;
  }
  
  public final String d()
  {
    return d;
  }
  
  public final List e()
  {
    NotificationChannel[] arrayOfNotificationChannel = new NotificationChannel[5];
    NotificationChannel localNotificationChannel = new android/app/NotificationChannel;
    Object localObject1 = c;
    Object localObject2 = h;
    int j = R.string.notification_channels_channel_missed_calls;
    localObject2 = (CharSequence)((Context)localObject2).getString(j);
    j = 2;
    localNotificationChannel.<init>((String)localObject1, (CharSequence)localObject2, j);
    localObject1 = h;
    int k = R.string.notification_channels_channel_description_missed_calls;
    localObject1 = ((Context)localObject1).getString(k);
    localNotificationChannel.setDescription((String)localObject1);
    boolean bool = true;
    localNotificationChannel.enableLights(bool);
    k = Z_();
    localNotificationChannel.setLightColor(k);
    localNotificationChannel.setGroup("calls");
    arrayOfNotificationChannel[0] = localNotificationChannel;
    localNotificationChannel = new android/app/NotificationChannel;
    localObject2 = d;
    Object localObject3 = h;
    int n = R.string.notification_channels_channel_missed_calls_reminder;
    localObject3 = (CharSequence)((Context)localObject3).getString(n);
    n = 3;
    localNotificationChannel.<init>((String)localObject2, (CharSequence)localObject3, n);
    localObject2 = h;
    int i1 = R.string.notification_channels_channel_description_missed_calls_reminder;
    localObject2 = ((Context)localObject2).getString(i1);
    localNotificationChannel.setDescription((String)localObject2);
    localNotificationChannel.enableLights(bool);
    k = Z_();
    localNotificationChannel.setLightColor(k);
    localNotificationChannel.setGroup("calls");
    arrayOfNotificationChannel[bool] = localNotificationChannel;
    localNotificationChannel = new android/app/NotificationChannel;
    localObject2 = e;
    localObject3 = h;
    int i2 = R.string.notification_channels_channel_blocked_calls;
    localObject3 = (CharSequence)((Context)localObject3).getString(i2);
    localNotificationChannel.<init>((String)localObject2, (CharSequence)localObject3, j);
    localObject2 = h;
    i1 = R.string.notification_channels_channel_description_blocked_calls;
    localObject2 = ((Context)localObject2).getString(i1);
    localNotificationChannel.setDescription((String)localObject2);
    localNotificationChannel.setGroup("calls");
    arrayOfNotificationChannel[j] = localNotificationChannel;
    localNotificationChannel = new android/app/NotificationChannel;
    localObject2 = f;
    localObject3 = h;
    i2 = R.string.notification_channels_channel_push_caller_id;
    localObject3 = (CharSequence)((Context)localObject3).getString(i2);
    i2 = 4;
    localNotificationChannel.<init>((String)localObject2, (CharSequence)localObject3, i2);
    localObject2 = h;
    i1 = R.string.notification_channels_channel_description_push_caller_id;
    localObject2 = ((Context)localObject2).getString(i1);
    localNotificationChannel.setDescription((String)localObject2);
    localNotificationChannel.setGroup("calls");
    arrayOfNotificationChannel[n] = localNotificationChannel;
    localNotificationChannel = new android/app/NotificationChannel;
    localObject2 = g;
    localObject3 = h;
    n = R.string.notification_channels_channel_phone_calls;
    localObject3 = (CharSequence)((Context)localObject3).getString(n);
    localNotificationChannel.<init>((String)localObject2, (CharSequence)localObject3, j);
    localObject2 = h;
    j = R.string.notification_channels_channel_description_phone_calls;
    localObject2 = ((Context)localObject2).getString(j);
    localNotificationChannel.setDescription((String)localObject2);
    localNotificationChannel.enableLights(bool);
    int m = Z_();
    localNotificationChannel.setLightColor(m);
    localNotificationChannel.setGroup("calls");
    arrayOfNotificationChannel[i2] = localNotificationChannel;
    return m.b(arrayOfNotificationChannel);
  }
  
  public final List f()
  {
    NotificationChannelGroup localNotificationChannelGroup = new android/app/NotificationChannelGroup;
    Object localObject = h;
    int j = R.string.notification_channels_group_calls;
    localObject = (CharSequence)((Context)localObject).getString(j);
    localNotificationChannelGroup.<init>("calls", (CharSequence)localObject);
    return m.a(localNotificationChannelGroup);
  }
  
  public final String g()
  {
    return e;
  }
  
  public final String h()
  {
    Object localObject1 = i.a();
    boolean bool = Y_();
    if (!bool) {
      return (String)localObject1;
    }
    Object localObject2 = b;
    if (localObject2 != null)
    {
      String str = "voip";
      localObject2 = ((NotificationManager)localObject2).getNotificationChannel(str);
      if (localObject2 != null)
      {
        localObject2 = ((NotificationChannel)localObject2).getId();
        if (localObject2 != null) {
          localObject1 = localObject2;
        }
      }
    }
    return (String)localObject1;
  }
  
  public final String i()
  {
    return f;
  }
  
  public final String j()
  {
    return g;
  }
  
  public final void k()
  {
    boolean bool1 = Y_();
    if (!bool1) {
      return;
    }
    Object localObject1 = h();
    Object localObject2 = "voip";
    bool1 = k.a(localObject1, localObject2);
    boolean bool2 = true;
    bool1 ^= bool2;
    if (bool1)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        NotificationChannel localNotificationChannel = new android/app/NotificationChannel;
        Object localObject3 = h;
        int j = R.string.notification_channels_channel_voip;
        localObject3 = (CharSequence)((Context)localObject3).getString(j);
        localNotificationChannel.<init>("voip", (CharSequence)localObject3, 2);
        Context localContext = h;
        int k = R.string.notification_channels_channel_description_voip;
        localObject2 = new Object[bool2];
        Object localObject4 = h;
        int m = R.string.voip_text;
        localObject4 = ((Context)localObject4).getString(m);
        localObject2[0] = localObject4;
        localObject2 = localContext.getString(k, (Object[])localObject2);
        localNotificationChannel.setDescription((String)localObject2);
        localNotificationChannel.setGroup("calls");
        ((NotificationManager)localObject1).createNotificationChannel(localNotificationChannel);
        return;
      }
    }
  }
  
  public final void l()
  {
    boolean bool = Y_();
    if (!bool) {
      return;
    }
    Object localObject = h();
    String str = "voip";
    bool = k.a(localObject, str);
    if (bool)
    {
      localObject = b;
      if (localObject != null)
      {
        ((NotificationManager)localObject).deleteNotificationChannel("voip");
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */