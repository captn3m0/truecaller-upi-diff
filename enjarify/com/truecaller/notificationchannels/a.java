package com.truecaller.notificationchannels;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Build.VERSION;
import c.f;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import java.util.List;

public abstract class a
  implements m
{
  final NotificationManager b;
  private final f c;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(a.class);
    ((u)localObject).<init>(localb, "defaultNotificationColor", "getDefaultNotificationColor()I");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public a(Context paramContext)
  {
    Object localObject = (NotificationManager)paramContext.getSystemService("notification");
    b = ((NotificationManager)localObject);
    localObject = new com/truecaller/notificationchannels/a$a;
    ((a.a)localObject).<init>(paramContext);
    paramContext = c.g.a((c.g.a.a)localObject);
    c = paramContext;
  }
  
  protected static boolean Y_()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    return i >= j;
  }
  
  protected final int Z_()
  {
    return ((Number)c.b()).intValue();
  }
  
  public final void b()
  {
    boolean bool = Y_();
    if (!bool) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager == null) {
      return;
    }
    List localList = f();
    localNotificationManager.createNotificationChannelGroups(localList);
  }
  
  public final void c()
  {
    boolean bool = Y_();
    if (!bool) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager == null) {
      return;
    }
    List localList = e();
    localNotificationManager.createNotificationChannels(localList);
  }
  
  public abstract List e();
  
  public abstract List f();
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */