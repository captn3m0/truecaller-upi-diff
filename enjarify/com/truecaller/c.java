package com.truecaller;

import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.support.v4.app.ac;
import android.telephony.TelephonyManager;
import com.google.android.gms.internal.firebase_remote_config.zzeh;
import com.google.android.gms.internal.firebase_remote_config.zzeo;
import com.google.android.gms.internal.firebase_remote_config.zzeq;
import com.google.android.gms.internal.firebase_remote_config.zzeu;
import com.google.android.gms.internal.firebase_remote_config.zzex;
import com.google.firebase.remoteconfig.g.a;
import com.truecaller.analytics.ab;
import com.truecaller.analytics.au;
import com.truecaller.analytics.w;
import com.truecaller.analytics.y;
import com.truecaller.androidactors.aa;
import com.truecaller.androidactors.q;
import com.truecaller.calling.ak;
import com.truecaller.calling.ar;
import com.truecaller.calling.as;
import com.truecaller.calling.dialer.ax;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.common.h.an;
import com.truecaller.common.h.u;
import com.truecaller.data.access.i;
import com.truecaller.filters.FilterManager;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.multisim.ae;
import com.truecaller.network.search.BulkSearcherImpl;
import com.truecaller.scanner.o;
import com.truecaller.util.ag;
import com.truecaller.util.b.at;
import com.truecaller.util.bt;
import com.truecaller.util.bu;
import com.truecaller.util.cn;
import java.util.Arrays;
import java.util.Map;
import org.json.JSONException;

public class c
{
  final TrueApp a;
  
  public c(TrueApp paramTrueApp)
  {
    a = paramTrueApp;
  }
  
  static com.truecaller.abtest.c a(dagger.a parama)
  {
    Object localObject = "qaAbTestEnableLocalConfig";
    boolean bool = com.truecaller.common.b.e.a((String)localObject, false);
    if (bool)
    {
      localObject = new com/truecaller/abtest/a;
      ((com.truecaller.abtest.a)localObject).<init>(parama);
      return (com.truecaller.abtest.c)localObject;
    }
    localObject = new com/truecaller/abtest/d;
    ((com.truecaller.abtest.d)localObject).<init>(parama);
    return (com.truecaller.abtest.c)localObject;
  }
  
  static com.truecaller.aftercall.a a(cn paramcn, com.truecaller.i.c paramc, com.truecaller.util.al paramal, com.truecaller.utils.a parama)
  {
    com.truecaller.aftercall.b localb = new com/truecaller/aftercall/b;
    localb.<init>(paramcn, paramc, paramal, parama);
    return localb;
  }
  
  static au a(com.truecaller.utils.a parama, com.truecaller.analytics.b paramb)
  {
    ab localab = new com/truecaller/analytics/ab;
    localab.<init>(parama, paramb);
    return localab;
  }
  
  static w a(com.truecaller.utils.a parama, com.truecaller.androidactors.f paramf, com.truecaller.analytics.b paramb)
  {
    y localy = new com/truecaller/analytics/y;
    localy.<init>(parama, paramf, paramb);
    return localy;
  }
  
  static com.truecaller.androidactors.k a()
  {
    Object localObject1 = new com/truecaller/a;
    ((a)localObject1).<init>();
    Object localObject2 = a;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/androidactors/m;
      ((com.truecaller.androidactors.m)localObject2).<init>();
      a = ((q)localObject2);
    }
    localObject2 = b;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/a$b;
      localaa = null;
      ((a.b)localObject2).<init>((byte)0);
      b = ((aa)localObject2);
    }
    localObject2 = new com/truecaller/a$a;
    aa localaa = b;
    localObject1 = a;
    ((a.a)localObject2).<init>(localaa, (q)localObject1);
    return (com.truecaller.androidactors.k)localObject2;
  }
  
  static com.truecaller.callerid.a a(com.truecaller.androidactors.f paramf, com.truecaller.utils.d paramd)
  {
    com.truecaller.callerid.b localb = new com/truecaller/callerid/b;
    localb.<init>(paramf, paramd);
    return localb;
  }
  
  static com.truecaller.calling.after_call.a a(com.truecaller.androidactors.k paramk, com.truecaller.androidactors.f paramf, bw parambw, com.truecaller.analytics.b paramb, com.truecaller.i.e parame, an paraman)
  {
    com.truecaller.calling.after_call.a locala = new com/truecaller/calling/after_call/a;
    locala.<init>(paramk, paramf, parambw, paramb, parame, paraman);
    return locala;
  }
  
  static com.truecaller.calling.after_call.b a(Context paramContext, c.d.f paramf, com.truecaller.analytics.b paramb)
  {
    com.truecaller.calling.after_call.b localb = new com/truecaller/calling/after_call/b;
    localb.<init>(paramContext, paramb, paramf);
    return localb;
  }
  
  static ak a(com.truecaller.calling.k paramk, com.truecaller.calling.f paramf, com.truecaller.utils.l paraml, com.truecaller.utils.a parama, com.truecaller.callerid.a.d paramd, u paramu)
  {
    com.truecaller.calling.al localal = new com/truecaller/calling/al;
    localal.<init>(paramk, paramf, paraml, parama, paramd, paramu);
    return localal;
  }
  
  static ar a(com.truecaller.i.c paramc, com.truecaller.multisim.h paramh, com.truecaller.utils.n paramn)
  {
    as localas = new com/truecaller/calling/as;
    localas.<init>(paramc, paramh, paramn);
    return localas;
  }
  
  static ax a(Context paramContext)
  {
    com.truecaller.calling.dialer.b localb = new com/truecaller/calling/dialer/b;
    localb.<init>(paramContext);
    return localb;
  }
  
  static com.truecaller.calling.f a(com.truecaller.androidactors.f paramf, com.truecaller.common.h.d paramd, com.truecaller.i.c paramc)
  {
    com.truecaller.calling.g localg = new com/truecaller/calling/g;
    localg.<init>(paramf, paramd, paramc);
    return localg;
  }
  
  static com.truecaller.calling.k a(com.truecaller.utils.d paramd, com.truecaller.i.c paramc, FilterManager paramFilterManager, com.truecaller.common.account.r paramr, an paraman, cn paramcn, u paramu, com.truecaller.multisim.h paramh, com.truecaller.utils.l paraml, com.truecaller.utils.a parama, CallRecordingManager paramCallRecordingManager, com.truecaller.featuretoggles.e parame, com.truecaller.androidactors.f paramf, com.truecaller.voip.d paramd1)
  {
    com.truecaller.calling.l locall = new com/truecaller/calling/l;
    locall.<init>(paramd, paramc, paraman, paramFilterManager, paramh, paramr, paramu, paramcn, parama, paraml, paramCallRecordingManager, parame, paramf, paramd1);
    return locall;
  }
  
  static com.truecaller.common.f.c a(com.truecaller.androidactors.f paramf1, com.truecaller.androidactors.f paramf2, com.truecaller.androidactors.k paramk, com.truecaller.common.g.a parama, com.truecaller.filters.p paramp, com.truecaller.engagementrewards.c paramc, com.truecaller.engagementrewards.ui.d paramd, com.truecaller.engagementrewards.g paramg, com.truecaller.featuretoggles.e parame)
  {
    com.truecaller.premium.data.p localp = new com/truecaller/premium/data/p;
    localp.<init>(paramf1, paramf2, paramk, parama, paramp, paramc, paramd, paramg, parame);
    return localp;
  }
  
  static com.truecaller.d.b a(com.truecaller.featuretoggles.e parame, com.truecaller.common.g.a parama, TelephonyManager paramTelephonyManager)
  {
    com.truecaller.d.c localc = new com/truecaller/d/c;
    String[] arrayOfString = new String[2];
    String str = paramTelephonyManager.getSimCountryIso();
    arrayOfString[0] = str;
    paramTelephonyManager = paramTelephonyManager.getNetworkCountryIso();
    arrayOfString[1] = paramTelephonyManager;
    paramTelephonyManager = Arrays.asList(arrayOfString);
    localc.<init>(parame, paramTelephonyManager, parama);
    return localc;
  }
  
  static i a(ContentResolver paramContentResolver, u paramu, com.truecaller.data.access.c paramc, c.d.f paramf1, c.d.f paramf2)
  {
    com.truecaller.data.access.j localj = new com/truecaller/data/access/j;
    localj.<init>(paramContentResolver, paramu, paramc, paramf1, paramf2);
    return localj;
  }
  
  static com.truecaller.featuretoggles.p a(com.truecaller.abtest.c paramc)
  {
    return paramc;
  }
  
  static com.truecaller.messaging.j a(com.truecaller.utils.d paramd, com.truecaller.messaging.h paramh)
  {
    com.truecaller.messaging.k localk = new com/truecaller/messaging/k;
    localk.<init>(paramd, paramh);
    return localk;
  }
  
  static ae a(com.truecaller.multisim.h paramh)
  {
    com.truecaller.multisim.af localaf = new com/truecaller/multisim/af;
    localaf.<init>(paramh);
    return localaf;
  }
  
  static com.truecaller.network.a.d a(com.truecaller.common.account.r paramr, com.truecaller.common.g.a parama, com.truecaller.common.network.c paramc)
  {
    com.truecaller.network.a.e locale = new com/truecaller/network/a/e;
    locale.<init>(paramr, parama, paramc);
    return locale;
  }
  
  static com.truecaller.scanner.l a(com.truecaller.utils.d paramd, com.truecaller.common.g.a parama)
  {
    com.truecaller.scanner.l locall = new com/truecaller/scanner/l;
    locall.<init>(paramd, parama);
    return locall;
  }
  
  static com.truecaller.search.local.model.c a(com.truecaller.search.local.model.c paramc)
  {
    return paramc;
  }
  
  static com.truecaller.search.local.model.c a(com.truecaller.search.local.model.g paramg, com.truecaller.common.account.r paramr, com.truecaller.util.al paramal, com.truecaller.messaging.h paramh, com.truecaller.androidactors.f paramf, com.truecaller.presence.r paramr1, bw parambw, com.truecaller.voip.d paramd)
  {
    com.truecaller.search.local.model.d locald = new com/truecaller/search/local/model/d;
    locald.<init>(paramal, paramg, paramr, paramh, paramf, paramr1, parambw, paramd);
    return locald;
  }
  
  static cn a(com.truecaller.util.b paramb)
  {
    br localbr = new com/truecaller/br;
    localbr.<init>(paramb);
    return localbr;
  }
  
  static com.truecaller.network.search.e b(Context paramContext)
  {
    BulkSearcherImpl localBulkSearcherImpl = new com/truecaller/network/search/BulkSearcherImpl;
    localBulkSearcherImpl.<init>(paramContext, 20, "inbox", null);
    return localBulkSearcherImpl;
  }
  
  static com.truecaller.util.b b()
  {
    com.truecaller.util.c localc = new com/truecaller/util/c;
    localc.<init>();
    return localc;
  }
  
  static com.google.firebase.remoteconfig.a c()
  {
    com.google.firebase.remoteconfig.a locala = com.google.firebase.remoteconfig.a.a();
    Object localObject1 = new com/google/firebase/remoteconfig/g$a;
    ((g.a)localObject1).<init>();
    a = false;
    localObject1 = ((g.a)localObject1).a();
    Object localObject2 = g;
    boolean bool = a;
    ((zzeu)localObject2).zzb(bool);
    localObject1 = b;
    int i = 2132082700;
    localObject1 = zzex.zza((Context)localObject1, i);
    try
    {
      localObject2 = zzeo.zzct();
      localObject1 = ((zzeq)localObject2).zzc((Map)localObject1);
      localObject1 = ((zzeq)localObject1).zzcv();
      localObject2 = f;
      ((zzeh)localObject2).zzb((zzeo)localObject1);
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
    return locala;
  }
  
  static com.truecaller.data.access.m c(Context paramContext)
  {
    com.truecaller.data.access.m localm = new com/truecaller/data/access/m;
    localm.<init>(paramContext);
    return localm;
  }
  
  static com.truecaller.data.access.c d(Context paramContext)
  {
    com.truecaller.data.access.c localc = new com/truecaller/data/access/c;
    localc.<init>(paramContext);
    return localc;
  }
  
  static com.truecaller.f.a d()
  {
    com.truecaller.f.b localb = new com/truecaller/f/b;
    localb.<init>();
    return localb;
  }
  
  static ContentResolver e(Context paramContext)
  {
    return paramContext.getContentResolver();
  }
  
  static com.truecaller.scanner.r e()
  {
    com.truecaller.scanner.r localr = new com/truecaller/scanner/r;
    localr.<init>();
    return localr;
  }
  
  static com.truecaller.scanner.n f()
  {
    o localo = new com/truecaller/scanner/o;
    localo.<init>();
    return localo;
  }
  
  static com.truecaller.service.e f(Context paramContext)
  {
    com.truecaller.service.f localf = new com/truecaller/service/f;
    localf.<init>(paramContext);
    return localf;
  }
  
  static androidx.work.p g()
  {
    return androidx.work.p.a();
  }
  
  static com.truecaller.callerid.c g(Context paramContext)
  {
    com.truecaller.callerid.d locald = new com/truecaller/callerid/d;
    locald.<init>(paramContext);
    return locald;
  }
  
  static com.truecaller.util.b.j h(Context paramContext)
  {
    return at.a(paramContext);
  }
  
  static String h()
  {
    TrueApp.y();
    return "Truecaller";
  }
  
  static com.truecaller.util.d.a i(Context paramContext)
  {
    com.truecaller.util.d.b localb = new com/truecaller/util/d/b;
    localb.<init>(paramContext);
    return localb;
  }
  
  static String i()
  {
    TrueApp.y();
    return "10.41.6";
  }
  
  static com.truecaller.util.af j(Context paramContext)
  {
    ag localag = new com/truecaller/util/ag;
    localag.<init>(paramContext);
    return localag;
  }
  
  static ac k(Context paramContext)
  {
    return ac.a(paramContext);
  }
  
  static ClipboardManager l(Context paramContext)
  {
    return (ClipboardManager)paramContext.getSystemService("clipboard");
  }
  
  static bt m(Context paramContext)
  {
    bu localbu = new com/truecaller/util/bu;
    paramContext = paramContext.getSharedPreferences("qa-menu", 0);
    localbu.<init>(paramContext);
    return localbu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */