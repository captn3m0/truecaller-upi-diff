package com.truecaller.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import com.truecaller.common.c.a.a.a;
import com.truecaller.common.c.a.a.b;
import com.truecaller.common.c.a.a.c;
import com.truecaller.common.c.a.a.e;
import com.truecaller.common.c.a.a.f;
import com.truecaller.common.c.a.a.g;
import com.truecaller.common.c.a.a.h;
import com.truecaller.common.c.a.c;
import com.truecaller.content.c.ab;
import com.truecaller.content.c.ag;
import com.truecaller.content.c.ag.a;
import com.truecaller.content.c.p;
import java.util.Collection;
import java.util.HashSet;

public class TruecallerContentProvider
  extends com.truecaller.common.c.a
  implements com.truecaller.common.c.a.e
{
  Handler b;
  private final ThreadLocal c;
  private final t d;
  
  public TruecallerContentProvider()
  {
    Object localObject = new java/lang/ThreadLocal;
    ((ThreadLocal)localObject).<init>();
    c = ((ThreadLocal)localObject);
    localObject = new com/truecaller/content/t;
    ((t)localObject).<init>();
    d = ((t)localObject);
  }
  
  private static Uri a(com.truecaller.common.c.a.d paramd, String paramString1, String paramString2)
  {
    com.truecaller.common.c.a.b localb = paramd.a(paramString1);
    boolean bool = true;
    c = bool;
    a = paramString2;
    localb = localb.a().a(paramString1);
    c = bool;
    a = paramString2;
    b = bool;
    localb = localb.a().a(paramString1);
    c = bool;
    a = paramString2;
    d = bool;
    localb.a();
    return paramd.a(paramString1).b();
  }
  
  public static void c(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("ACTION_RESTORE_AGGREGATION");
    localIntent.putExtra("ARG_DELAY", 100);
    android.support.v4.content.d.a(paramContext).a(localIntent);
  }
  
  private TruecallerContentProvider.AggregationState e()
  {
    TruecallerContentProvider.AggregationState localAggregationState = (TruecallerContentProvider.AggregationState)c.get();
    if (localAggregationState == null) {
      localAggregationState = TruecallerContentProvider.AggregationState.NONE;
    }
    return localAggregationState;
  }
  
  final void a(long paramLong)
  {
    b.sendEmptyMessageDelayed(1, paramLong);
  }
  
  final void a(TruecallerContentProvider.AggregationState paramAggregationState)
  {
    Object localObject = e();
    int i = ((TruecallerContentProvider.AggregationState)localObject).ordinal();
    int j = paramAggregationState.ordinal();
    if (i < j)
    {
      localObject = c;
      ((ThreadLocal)localObject).set(paramAggregationState);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    super.a(paramBoolean);
    Object localObject = e();
    TruecallerContentProvider.AggregationState localAggregationState = TruecallerContentProvider.AggregationState.DELAYED;
    if (localObject != localAggregationState)
    {
      localAggregationState = TruecallerContentProvider.AggregationState.IMMEDIATE;
      if (localObject != localAggregationState) {}
    }
    else
    {
      localObject = c;
      ((ThreadLocal)localObject).remove();
      long l = 100;
      a(l);
    }
  }
  
  public final SQLiteDatabase a_(Context paramContext)
  {
    Object localObject = ag.b();
    com.truecaller.analytics.b localb = com.truecaller.common.b.a.F().v().c();
    localObject = ag.a(paramContext, (ab[])localObject, localb);
    try
    {
      return ((ag)localObject).getWritableDatabase();
    }
    catch (ag.a locala)
    {
      paramContext.deleteDatabase("tc.db");
      com.truecaller.common.b.a.F().a(false);
      throw a;
    }
  }
  
  public final c b(Context paramContext)
  {
    Object localObject1 = (com.truecaller.common.b.a)paramContext.getApplicationContext();
    com.truecaller.featuretoggles.e locale = ((com.truecaller.common.b.a)localObject1).f();
    Object localObject2 = ((com.truecaller.common.b.a)localObject1).g();
    localObject1 = ((com.truecaller.common.b.a)localObject1).u().c();
    Object localObject3 = getClass();
    localObject3 = com.truecaller.common.c.b.b.a(paramContext, (Class)localObject3);
    com.truecaller.common.c.a.d locald = new com/truecaller/common/c/a/d;
    locald.<init>();
    d = ((String)localObject3);
    Object localObject4;
    if (localObject3 != null)
    {
      localObject4 = e;
      if (localObject4 == null)
      {
        localObject4 = "content://";
        localObject3 = String.valueOf(localObject3);
        localObject3 = Uri.parse(((String)localObject4).concat((String)localObject3));
        e = ((Uri)localObject3);
      }
    }
    localObject3 = c;
    if (localObject3 == null)
    {
      c = this;
      localObject3 = new java/util/HashSet;
      ((HashSet)localObject3).<init>();
      localObject4 = a(locald, "history_with_raw_contact", "history_with_raw_contact");
      ((HashSet)localObject3).add(localObject4);
      localObject4 = a(locald, "history_with_aggregated_contact", "history_with_aggregated_contact");
      ((HashSet)localObject3).add(localObject4);
      localObject4 = a(locald, "history_top_called_with_aggregated_contact", "history_top_called_with_aggregated_contact");
      ((HashSet)localObject3).add(localObject4);
      localObject4 = a(locald, "history_with_aggregated_contact_number", "history_with_aggregated_contact_number");
      ((HashSet)localObject3).add(localObject4);
      localObject4 = a(locald, "history_with_call_recording", "history_with_call_recording");
      ((HashSet)localObject3).add(localObject4);
      localObject4 = a(locald, "call_recordings_with_history_event", "call_recordings_with_history_event");
      ((HashSet)localObject3).add(localObject4);
      localObject4 = a(locald, "sorted_contacts_with_data", "sorted_contacts_with_data");
      ((HashSet)localObject3).add(localObject4);
      Object localObject5 = a(locald, "sorted_contacts_shallow", "sorted_contacts_shallow");
      ((HashSet)localObject3).add(localObject5);
      localObject5 = new java/util/HashSet;
      ((HashSet)localObject5).<init>();
      Object localObject6 = TruecallerContract.n.c();
      ((HashSet)localObject5).add(localObject6);
      localObject6 = TruecallerContract.n.b();
      ((HashSet)localObject5).add(localObject6);
      localObject6 = TruecallerContract.n.e();
      ((HashSet)localObject5).add(localObject6);
      localObject6 = TruecallerContract.n.d();
      ((HashSet)localObject5).add(localObject6);
      localObject6 = TruecallerContract.c.b();
      ((HashSet)localObject5).add(localObject6);
      localObject6 = locald.a("aggregated_contact");
      int i = 5;
      e = i;
      ((com.truecaller.common.c.a.b)localObject6).a((Collection)localObject3).a();
      localObject6 = locald.a("aggregated_contact").a((Collection)localObject3);
      boolean bool = true;
      b = bool;
      ((com.truecaller.common.c.a.b)localObject6).a();
      localObject6 = locald.a("aggregated_contact");
      d = bool;
      ((com.truecaller.common.c.a.b)localObject6).a();
      localObject6 = locald.a("aggregated_contact_t9").a(false).b(bool);
      Object localObject7 = new com/truecaller/content/c/p;
      ((p)localObject7).<init>(bool);
      f = ((a.g)localObject7);
      ((com.truecaller.common.c.a.b)localObject6).a();
      localObject6 = locald.a("aggregated_contact_plain_text").a(false).b(bool);
      localObject7 = new com/truecaller/content/c/p;
      ((p)localObject7).<init>(false);
      f = ((a.g)localObject7);
      ((com.truecaller.common.c.a.b)localObject6).a();
      localObject6 = locald.a("raw_contact");
      e = i;
      localObject7 = d;
      g = ((a.f)localObject7);
      j = ((a.b)localObject7);
      i = ((a.e)localObject7);
      ((com.truecaller.common.c.a.b)localObject6).a((Collection)localObject3).a();
      localObject6 = locald.a("raw_contact");
      localObject7 = d;
      i = ((a.e)localObject7);
      localObject6 = ((com.truecaller.common.c.a.b)localObject6).a((Collection)localObject3);
      b = bool;
      ((com.truecaller.common.c.a.b)localObject6).a();
      localObject6 = locald.a("raw_contact");
      d = bool;
      ((com.truecaller.common.c.a.b)localObject6).a();
      localObject6 = new com/truecaller/content/j;
      ((j)localObject6).<init>((com.truecaller.content.d.a)localObject2);
      localObject7 = new com/truecaller/content/k;
      ((k)localObject7).<init>((com.truecaller.content.d.a)localObject2);
      i locali = new com/truecaller/content/i;
      locali.<init>((com.truecaller.content.d.a)localObject2);
      localObject2 = locald.a("history");
      j = ((a.b)localObject6);
      k = ((a.c)localObject7);
      l = locali;
      ((com.truecaller.common.c.a.b)localObject2).a((Collection)localObject3).a();
      localObject2 = locald.a("history").a((Collection)localObject3);
      b = bool;
      l = locali;
      ((com.truecaller.common.c.a.b)localObject2).a();
      localObject2 = locald.a("history");
      d = bool;
      ((com.truecaller.common.c.a.b)localObject2).a();
      localObject2 = locald.a("raw_contact").b();
      ((HashSet)localObject3).add(localObject2);
      localObject2 = a(locald, "raw_contact_data", "raw_contact/data");
      ((HashSet)localObject3).add(localObject2);
      localObject2 = a(locald, "aggregated_contact_data", "aggregated_contact/data");
      ((HashSet)localObject3).add(localObject2);
      localObject2 = new com/truecaller/content/q;
      ((q)localObject2).<init>();
      localObject6 = locald.a("data");
      g = ((a.f)localObject2);
      j = ((a.b)localObject2);
      ((com.truecaller.common.c.a.b)localObject6).a((Collection)localObject3).a();
      localObject6 = locald.a("data");
      g = ((a.f)localObject2);
      j = ((a.b)localObject2);
      localObject2 = ((com.truecaller.common.c.a.b)localObject6).a((Collection)localObject3);
      b = bool;
      ((com.truecaller.common.c.a.b)localObject2).a();
      localObject2 = locald.a("data");
      d = bool;
      ((com.truecaller.common.c.a.b)localObject2).a();
      localObject2 = new com/truecaller/content/d;
      ((d)localObject2).<init>(locale);
      localObject3 = locald.a("msg_conversations");
      a = "msg/msg_conversations";
      localObject3 = ((com.truecaller.common.c.a.b)localObject3).a(bool);
      g = ((a.f)localObject2);
      h = ((a.h)localObject2);
      i = ((a.e)localObject2);
      ((com.truecaller.common.c.a.b)localObject3).a();
      localObject2 = locald.a("msg_thread_stats");
      a = "msg/msg_thread_stats";
      ((com.truecaller.common.c.a.b)localObject2).a();
      localObject2 = locald.a("msg/msg_thread_stats_specific_update");
      localObject3 = new com/truecaller/content/v;
      ((v)localObject3).<init>();
      h = ((a.h)localObject3);
      ((com.truecaller.common.c.a.b)localObject2).b(false).a(bool).a();
      localObject2 = locald.a("msg_conversations_list");
      a = "msg/msg_conversations_list";
      b = bool;
      localObject2 = ((com.truecaller.common.c.a.b)localObject2).a(false);
      localObject3 = new com/truecaller/content/e;
      ((e)localObject3).<init>(locale);
      f = ((a.g)localObject3);
      ((com.truecaller.common.c.a.b)localObject2).a();
      localObject2 = locald.a("msg_conversations_list");
      a = "msg/msg_conversations_list";
      localObject2 = ((com.truecaller.common.c.a.b)localObject2).a(false);
      localObject3 = new com/truecaller/content/e;
      ((e)localObject3).<init>(locale);
      f = ((a.g)localObject3);
      ((com.truecaller.common.c.a.b)localObject2).a();
      localObject2 = locald.a("msg_conversation_transport_info");
      a = "msg/msg_conversation_transport_info";
      ((com.truecaller.common.c.a.b)localObject2).a(false).a();
      localObject2 = new com/truecaller/content/r;
      ((r)localObject2).<init>();
      localObject3 = locald.a("msg_participants");
      a = "msg/msg_participants";
      g = ((a.f)localObject2);
      h = ((a.h)localObject2);
      ((com.truecaller.common.c.a.b)localObject3).b(bool).a();
      localObject2 = locald.a("msg_conversation_participants");
      a = "msg/msg_conversation_participants";
      ((com.truecaller.common.c.a.b)localObject2).a(false).b(bool).a();
      localObject2 = locald.a("msg_participants_with_contact_info");
      a = "msg/msg_participants_with_contact_info";
      localObject3 = new com/truecaller/content/s;
      ((s)localObject3).<init>(paramContext);
      f = ((a.g)localObject3);
      ((com.truecaller.common.c.a.b)localObject2).a(false).a();
      localObject2 = new com/truecaller/content/n;
      ((n)localObject2).<init>();
      localObject3 = locald.a("msg_messages");
      a = "msg/msg_messages";
      g = ((a.f)localObject2);
      localObject6 = new Uri[bool];
      localObject7 = TruecallerContract.g.a();
      localObject6[0] = localObject7;
      ((com.truecaller.common.c.a.b)localObject3).a((Uri[])localObject6).a();
      localObject3 = locald.a("msg_messages");
      a = "msg/msg_messages";
      b = bool;
      h = ((a.h)localObject2);
      i = ((a.e)localObject2);
      localObject2 = new Uri[bool];
      localObject6 = TruecallerContract.g.a();
      localObject2[0] = localObject6;
      ((com.truecaller.common.c.a.b)localObject3).a((Uri[])localObject2).a();
      localObject2 = locald.a("msg_entities");
      a = "msg/msg_entities";
      localObject3 = new Uri[bool];
      localObject6 = TruecallerContract.ab.a();
      localObject3[0] = localObject6;
      localObject2 = ((com.truecaller.common.c.a.b)localObject2).a((Uri[])localObject3);
      localObject3 = new Uri[bool];
      localObject6 = TruecallerContract.g.a();
      localObject3[0] = localObject6;
      ((com.truecaller.common.c.a.b)localObject2).a((Uri[])localObject3).a();
      localObject2 = locald.a("msg_sms_transport_info");
      a = "msg/msg_sms_transport_info";
      ((com.truecaller.common.c.a.b)localObject2).b(false).a();
      localObject2 = locald.a("msg_mms_transport_info");
      a = "msg/msg_mms_transport_info";
      ((com.truecaller.common.c.a.b)localObject2).b(false).a();
      localObject2 = locald.a("msg_im_transport_info");
      a = "msg/msg_im_transport_info";
      localObject2 = ((com.truecaller.common.c.a.b)localObject2).b(bool);
      localObject3 = new Uri[bool];
      localObject6 = TruecallerContract.ab.a();
      localObject3[0] = localObject6;
      localObject2 = ((com.truecaller.common.c.a.b)localObject2).a((Uri[])localObject3);
      localObject3 = new Uri[bool];
      localObject6 = TruecallerContract.g.a();
      localObject3[0] = localObject6;
      ((com.truecaller.common.c.a.b)localObject2).a((Uri[])localObject3).a();
      localObject2 = locald.a("msg_history_transport_info");
      a = "msg/msg_history_transport_info";
      localObject2 = ((com.truecaller.common.c.a.b)localObject2).b(bool);
      localObject3 = new Uri[bool];
      localObject6 = TruecallerContract.ab.a();
      localObject3[0] = localObject6;
      localObject2 = ((com.truecaller.common.c.a.b)localObject2).a((Uri[])localObject3);
      localObject3 = new Uri[bool];
      localObject6 = TruecallerContract.g.a();
      localObject3[0] = localObject6;
      ((com.truecaller.common.c.a.b)localObject2).a((Uri[])localObject3).a();
      localObject2 = locald.a("msg_status_transport_info");
      a = "msg/msg_status_transport_info";
      ((com.truecaller.common.c.a.b)localObject2).b(bool).a(bool).a();
      localObject2 = locald.a("msg_im_reactions");
      a = "msg/msg_im_reactions";
      localObject3 = new Uri[bool];
      localObject6 = TruecallerContract.ab.a();
      localObject3[0] = localObject6;
      ((com.truecaller.common.c.a.b)localObject2).a((Uri[])localObject3).a();
      localObject2 = locald.a("reaction_with_participants").a(false).b(bool);
      localObject3 = new com/truecaller/content/u;
      ((u)localObject3).<init>();
      f = ((a.g)localObject3);
      ((com.truecaller.common.c.a.b)localObject2).a();
      localObject2 = locald.a("msg_messages_by_transport_view");
      a = "msg/msg_messages_by_transport_view";
      ((com.truecaller.common.c.a.b)localObject2).b(bool).a(false).a();
      localObject2 = locald.a("msg/msg_messages_with_entities").a(false).b(bool);
      localObject3 = new com/truecaller/content/c/s;
      ((com.truecaller.content.c.s)localObject3).<init>(paramContext);
      f = ((a.g)localObject3);
      b = bool;
      ((com.truecaller.common.c.a.b)localObject2).a();
      localObject2 = locald.a("msg_messages_with_entities");
      a = "msg/msg_messages_with_entities";
      localObject2 = ((com.truecaller.common.c.a.b)localObject2).a(false).b(bool);
      localObject3 = new com/truecaller/content/c/s;
      ((com.truecaller.content.c.s)localObject3).<init>(paramContext);
      f = ((a.g)localObject3);
      ((com.truecaller.common.c.a.b)localObject2).a();
      paramContext = locald.a("msg_messages_with_entities");
      a = "msg/msg_messages_with_entities_filtered";
      paramContext = paramContext.a(false);
      localObject2 = new com/truecaller/content/c/r;
      ((com.truecaller.content.c.r)localObject2).<init>();
      f = ((a.g)localObject2);
      paramContext.a();
      paramContext = locald.a("msg_im_attachments");
      a = "msg/msg_im_attachments";
      paramContext.a();
      locald.a("msg_im_attachments_entities").a(false).b(bool).a();
      paramContext = locald.a("msg_im_sync_view");
      a = "msg/msg_im_sync_view";
      paramContext.a(false).b(bool).a();
      paramContext = new com/truecaller/content/m;
      paramContext.<init>();
      localObject2 = locald.a("msg_im_users");
      a = "msg/msg_im_users";
      localObject2 = ((com.truecaller.common.c.a.b)localObject2).a(bool).b(bool);
      g = paramContext;
      h = paramContext;
      i = paramContext;
      e = i;
      ((com.truecaller.common.c.a.b)localObject2).a();
      paramContext = locald.a("msg_im_group_participants");
      a = "msg/msg_im_group_participants";
      paramContext = paramContext.a(bool).b(bool);
      e = i;
      localObject2 = new Uri[bool];
      localObject3 = TruecallerContract.t.a();
      localObject2[0] = localObject3;
      paramContext.a((Uri[])localObject2).a();
      paramContext = locald.a("msg_im_group_info");
      a = "msg/msg_im_group_info";
      paramContext = paramContext.a(bool).b(bool);
      e = i;
      localObject2 = new Uri[bool];
      localObject3 = TruecallerContract.g.a();
      localObject2[0] = localObject3;
      paramContext.a((Uri[])localObject2).a();
      paramContext = locald.a("msg_im_group_participants_view");
      a = "msg/msg_im_group_participants_view";
      paramContext = paramContext.a(false).b(bool);
      localObject2 = new com/truecaller/content/l;
      ((l)localObject2).<init>();
      f = ((a.g)localObject2);
      paramContext.a();
      paramContext = locald.a("new_conversation_items").a(false).b(bool);
      localObject2 = new com/truecaller/content/o;
      ((o)localObject2).<init>((com.truecaller.common.g.a)localObject1, locale);
      f = ((a.g)localObject2);
      paramContext.a();
      paramContext = locald.a("conversation_messages").a(bool).b(bool);
      localObject1 = new com/truecaller/content/z;
      ((z)localObject1).<init>();
      f = ((a.g)localObject1);
      paramContext.a();
      paramContext = locald.a("filters");
      a = "filters";
      localObject1 = new com/truecaller/content/g;
      ((g)localObject1).<init>();
      g = ((a.f)localObject1);
      localObject1 = new com/truecaller/content/h;
      ((h)localObject1).<init>();
      h = ((a.h)localObject1);
      localObject1 = new com/truecaller/content/f;
      ((f)localObject1).<init>();
      i = ((a.e)localObject1);
      paramContext = paramContext.a().a("filters");
      a = "filters";
      b = bool;
      paramContext = paramContext.a().a("filters");
      a = "filters";
      d = bool;
      paramContext.a();
      paramContext = locald.a("topspammers");
      a = "topspammers";
      localObject1 = new com/truecaller/content/x;
      ((x)localObject1).<init>();
      j = ((a.b)localObject1);
      localObject1 = new com/truecaller/content/y;
      ((y)localObject1).<init>();
      h = ((a.h)localObject1);
      localObject1 = new com/truecaller/content/w;
      ((w)localObject1).<init>();
      l = ((a.a)localObject1);
      paramContext = paramContext.a().a("topspammers");
      a = "topspammers";
      b = bool;
      paramContext = paramContext.a().a("topspammers");
      a = "topspammers";
      d = bool;
      paramContext.a();
      locald.a("t9_mapping").a(bool).b(bool).a();
      paramContext = locald.a("contact_sorting_index");
      localObject1 = new Uri[bool];
      localObject1[0] = localObject4;
      paramContext.a((Uri[])localObject1).a(bool).b(bool).a();
      paramContext = locald.a("contact_sorting_index");
      a = "contact_sorting_index/fast_scroll";
      paramContext = paramContext.a(false).b(bool);
      localObject1 = new com/truecaller/content/c/o;
      ((com.truecaller.content.c.o)localObject1).<init>();
      f = ((a.g)localObject1);
      paramContext.a();
      paramContext = locald.a("call_recordings");
      a = "call_recordings";
      paramContext.a((Collection)localObject5).a(bool).b(bool).a();
      paramContext = locald.a("profile_view_events");
      a = "profile_view_events";
      paramContext.a(bool).b(bool).a();
      paramContext = locald.a("spam_url_reports");
      a = "spam_url_reports";
      e = i;
      paramContext.a();
      paramContext = locald.a("msg_im_unsupported_events");
      a = "msg/msg_im_unsupported_events";
      paramContext.a(bool).b(bool).a();
      paramContext = locald.a("contact_settings");
      a = "contact_settings";
      paramContext = paramContext.a(bool).b(bool);
      e = i;
      paramContext.a();
      return locald.a();
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Database factory already set");
    throw paramContext;
  }
  
  public final void d()
  {
    super.d();
    Object localObject1 = e();
    Object localObject2 = TruecallerContentProvider.AggregationState.IMMEDIATE;
    if (localObject1 == localObject2)
    {
      localObject1 = d;
      localObject2 = c();
      ((t)localObject1).a((SQLiteDatabase)localObject2);
      c.remove();
      localObject1 = TruecallerContract.a.a();
      a((Uri)localObject1);
    }
  }
  
  public void onBegin()
  {
    super.onBegin();
    c.remove();
    Object localObject = b;
    int i = 1;
    boolean bool = ((Handler)localObject).hasMessages(i);
    if (bool)
    {
      b.removeMessages(i);
      localObject = TruecallerContentProvider.AggregationState.DELAYED;
      a((TruecallerContentProvider.AggregationState)localObject);
    }
  }
  
  public boolean onCreate()
  {
    Object localObject1 = android.support.v4.content.d.a(getContext());
    Object localObject2 = new com/truecaller/content/TruecallerContentProvider$1;
    ((TruecallerContentProvider.1)localObject2).<init>(this);
    Object localObject3 = new android/content/IntentFilter;
    ((IntentFilter)localObject3).<init>("ACTION_RESTORE_AGGREGATION");
    ((android.support.v4.content.d)localObject1).a((BroadcastReceiver)localObject2, (IntentFilter)localObject3);
    localObject1 = new android/os/HandlerThread;
    ((HandlerThread)localObject1).<init>("Aggregation", 10);
    ((HandlerThread)localObject1).start();
    localObject2 = new android/os/Handler;
    localObject1 = ((HandlerThread)localObject1).getLooper();
    localObject3 = new com/truecaller/content/TruecallerContentProvider$a;
    ((TruecallerContentProvider.a)localObject3).<init>(this, (byte)0);
    ((Handler)localObject2).<init>((Looper)localObject1, (Handler.Callback)localObject3);
    b = ((Handler)localObject2);
    localObject1 = getContext();
    localObject2 = getClass();
    TruecallerContract.a(com.truecaller.common.c.b.b.a((Context)localObject1, (Class)localObject2));
    return super.onCreate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContentProvider
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */