package com.truecaller.content;

import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.BaseColumns;

public final class TruecallerContract$n
  implements BaseColumns, TruecallerContract.m
{
  public static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "history");
  }
  
  public static Uri a(int paramInt)
  {
    Uri.Builder localBuilder = TruecallerContract.b.buildUpon();
    String str1 = "history_top_called_with_aggregated_contact";
    localBuilder = localBuilder.appendEncodedPath(str1);
    if (paramInt > 0)
    {
      str1 = "limit";
      String str2 = String.valueOf(paramInt);
      localBuilder = localBuilder.appendQueryParameter(str1, str2);
    }
    return localBuilder.build();
  }
  
  public static Uri b()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "history_with_call_recording");
  }
  
  public static Uri c()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "history_with_raw_contact");
  }
  
  public static Uri d()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "history_with_aggregated_contact");
  }
  
  public static Uri e()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "history_with_aggregated_contact_number");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */