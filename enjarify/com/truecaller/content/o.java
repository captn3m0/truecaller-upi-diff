package com.truecaller.content;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import c.a.f;
import c.u;
import com.google.c.a.k.c;
import com.google.c.a.m.a;
import com.truecaller.common.c.a.a.g;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class o
  implements a.g
{
  private final c.n.k a;
  private final com.truecaller.common.g.a b;
  private final e c;
  
  public o(com.truecaller.common.g.a parama, e parame)
  {
    b = parama;
    c = parame;
    parame = new c/n/k;
    parame.<init>("\\+?[\\d\\s()-]+");
    a = parame;
  }
  
  private static m.a a(String paramString1, String paramString2)
  {
    m.a locala = null;
    try
    {
      com.google.c.a.k localk = com.google.c.a.k.a();
      paramString1 = (CharSequence)paramString1;
      locala = localk.a(paramString1, paramString2);
    }
    catch (IllegalStateException|com.google.c.a.g localIllegalStateException)
    {
      for (;;) {}
    }
    return locala;
  }
  
  private static void a(StringBuilder paramStringBuilder, String paramString, boolean paramBoolean, List paramList)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("\n              SELECT null                                                                                        as conversation_id,\n                     IFNULL(a.contact_name, '')                                                            as name_sorting,\n                     IFNULL(LENGTH(a.contact_name) || '|' || a.contact_name, '0|')                   as names_group,\n                     IFNULL(a.contact_phonebook_id, '')                                                    as phonebook_ids_grouped,\n                     a.contact_image_url                                                                   as image_uri,\n                     LENGTH(d.data1) || '|' || d.data1 as numbers_group,\n                     i.im_peer_id                                                                as im_id,\n                     i.registration_timestamp                                                    as im_registration_timestamp,\n                     0                                                                                           as date_sorting,\n                     CASE\n                        WHEN ? AND i.im_peer_id IS NOT NULL THEN 0\n                        ELSE 1 END                                                          as transport_type,\n                     ");
    localStringBuilder.append(paramString);
    localStringBuilder.append("                                                                    as group_sorting,\n                     NULL                                                                                        as im_group_id,\n                     NULL                                                                                        as im_group_title,\n                     NULL                                                                                        as im_group_avatar\n              FROM data d\n                     LEFT JOIN raw_contact r ON r._id = d.data_raw_contact_id\n                     LEFT JOIN aggregated_contact a on a._id = r.aggregated_contact_id\n                     LEFT JOIN msg_im_users i ON i.normalized_number = d.data1\n              WHERE d.data_type = 4\n              AND r.contact_phonebook_id IS NOT NULL\n        ");
    paramString = localStringBuilder.toString();
    paramStringBuilder.append(paramString);
    a(paramList, paramBoolean);
  }
  
  private final boolean a(String paramString)
  {
    paramString = (CharSequence)paramString;
    return a.a(paramString);
  }
  
  private static boolean a(List paramList, boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (String str = "1";; str = "0") {
      return paramList.add(str);
    }
  }
  
  private static String b(String paramString)
  {
    paramString = (CharSequence)paramString;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = (Appendable)localObject;
    int i = paramString.length();
    int j = 0;
    while (j < i)
    {
      char c1 = paramString.charAt(j);
      boolean bool = Character.isDigit(c1);
      if (bool) {
        ((Appendable)localObject).append(c1);
      }
      j += 1;
    }
    paramString = ((StringBuilder)localObject).toString();
    c.g.b.k.a(paramString, "filterTo(StringBuilder(), predicate).toString()");
    return paramString;
  }
  
  public final Cursor a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    o localo = this;
    Object localObject1 = paramUri;
    Object localObject2 = parama;
    c.g.b.k.b(parama, "provider");
    Object localObject3 = parama1;
    c.g.b.k.b(parama1, "helper");
    c.g.b.k.b(paramUri, "uri");
    Object localObject4 = paramUri.getQueryParameter("query_type");
    if (localObject4 == null) {
      localObject4 = "new_conversation";
    }
    localObject3 = ((Uri)localObject1).getQueryParameter("filter");
    Object localObject5 = ((Uri)localObject1).getQueryParameter("im_enabled");
    boolean bool1;
    if (localObject5 != null)
    {
      bool1 = Boolean.parseBoolean((String)localObject5);
    }
    else
    {
      bool1 = false;
      localObject5 = null;
    }
    if (paramArrayOfString2 != null)
    {
      localObject6 = f.g(paramArrayOfString2);
    }
    else
    {
      localObject6 = new java/util/ArrayList;
      ((ArrayList)localObject6).<init>();
      localObject6 = (List)localObject6;
    }
    Object localObject7 = ((Uri)localObject1).getQueryParameter("conversation_type");
    Object localObject8 = "exclude_im_group_roles";
    localObject1 = ((Uri)localObject1).getQueryParameter((String)localObject8);
    if (localObject1 != null)
    {
      localObject1 = c.n.m.b((String)localObject1);
      if (localObject1 != null)
      {
        i = ((Integer)localObject1).intValue();
        break label173;
      }
    }
    int i = 0;
    localObject1 = null;
    label173:
    localObject8 = new java/lang/StringBuilder;
    ((StringBuilder)localObject8).<init>();
    int k = ((String)localObject4).hashCode();
    int m = -677145915;
    int n = 1;
    int j;
    boolean bool2;
    if (k != m)
    {
      i = 219431106;
      if (k == i)
      {
        localObject1 = "new_conversation";
        j = ((String)localObject4).equals(localObject1);
        if (j != 0)
        {
          localObject1 = "mms_group_type";
          j = c.g.b.k.a(localObject7, localObject1) ^ n;
          if ((j != 0) && (bool1))
          {
            j = 1;
          }
          else
          {
            j = 0;
            localObject1 = null;
          }
          if (j != 0) {
            localObject1 = "\n            CASE\n               WHEN i.im_peer_id IS NOT NULL THEN 2\n               ELSE 4 END\n        ";
          } else {
            localObject1 = "4";
          }
          a((StringBuilder)localObject8, (String)localObject1, bool1, (List)localObject6);
          localObject1 = "im_group_type";
          j = c.g.b.k.a(localObject7, localObject1);
          if (j == 0) {
            break label623;
          }
          ((StringBuilder)localObject8).append(" AND i.im_peer_id IS NOT NULL ");
          localObject1 = b;
          localObject5 = "profileNumber";
          localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject5);
          if (localObject1 == null) {
            break label623;
          }
          ((StringBuilder)localObject8).append(" AND i.normalized_number != ? ");
          localObject5 = "profileNumber";
          c.g.b.k.a(localObject1, (String)localObject5);
          ((List)localObject6).add(localObject1);
          break label623;
        }
      }
    }
    else
    {
      localObject7 = "forward";
      boolean bool3 = ((String)localObject4).equals(localObject7);
      if (bool3)
      {
        ((StringBuilder)localObject8).append("SELECT * FROM (\n");
        ((StringBuilder)localObject8).append("\n             SELECT c._id                                                                                         as conversation_id,\n             GROUP_CONCAT(COALESCE(a.contact_name, ig.title, ''), '|')                                  as name_sorting,\n             GROUP_CONCAT(IFNULL(LENGTH(a.contact_name) || '|' || a.contact_name, '0|'), '')                          as names_group,\n             GROUP_CONCAT(IFNULL(a.contact_phonebook_id, -1), '|')                                                          as phonebook_ids_grouped,\n             a.contact_image_url                                                                                            as image_uri,\n             GROUP_CONCAT(LENGTH(p.normalized_destination) || '|' || p.normalized_destination, '')  as numbers_group,\n             null                                                                                                                 as im_id,\n             null                                                                                                                 as im_registration_timestamp,\n             ts.date_sorting                                                                                             as date_sorting,\n             CASE\n               WHEN c.tc_group_id IS NULL AND (NOT ? OR c.type == 1\n                   OR COUNT(p.tc_im_peer_id) == 0) THEN 1\n               ELSE 0 END                                                                                        as transport_type,\n             CASE\n               WHEN c.tc_group_id IS NULL AND (NOT ? OR c.type == 1\n                   OR COUNT(p.tc_im_peer_id) == 0) THEN 3\n               WHEN ts.date_sorting > ? THEN 0\n               ELSE 1 END                                                                                        as group_sorting,\n             c.tc_group_id                                                                                        as im_group_id,\n             ig.title                                                                                         as im_group_title,\n             ig.avatar                                                                                        as im_group_avatar\n      FROM msg_conversations c\n             INNER JOIN msg_conversation_participants cp ON cp.conversation_id = c._id\n             INNER JOIN msg_thread_stats ts ON ts.conversation_id = c._id\n             INNER JOIN msg_participants p ON p._id = cp.participant_id\n             LEFT JOIN aggregated_contact a ON a._id = p.aggregated_contact_id\n             LEFT JOIN msg_im_group_info ig ON ig.im_group_id = c.tc_group_id\n             WHERE ts.filter = 1\n               AND p.type != 1\n               AND (ig.roles IS NULL OR ig.roles != 0 AND (ig.roles & ?) = 0)\n               AND (? OR c.tc_group_id IS NULL)\n      GROUP BY c._id\n    ");
        a((List)localObject6, bool1);
        a((List)localObject6, bool1);
        localObject7 = org.a.a.b.ay_();
        k = 7;
        localObject7 = ((org.a.a.b)localObject7).c(k);
        String str = "DateTime.now().minusDays(RECENT_CHATS_DAYS)";
        c.g.b.k.a(localObject7, str);
        long l1 = a;
        localObject7 = String.valueOf(l1);
        ((List)localObject6).add(localObject7);
        localObject1 = String.valueOf(j);
        ((List)localObject6).add(localObject1);
        if (bool1)
        {
          localObject1 = c.i();
          bool2 = ((com.truecaller.featuretoggles.b)localObject1).a();
          if (bool2)
          {
            bool2 = true;
            break label547;
          }
        }
        bool2 = false;
        localObject1 = null;
        label547:
        a((List)localObject6, bool2);
        ((StringBuilder)localObject8).append("\nUNION\n");
        a((StringBuilder)localObject8, "4", bool1, (List)localObject6);
        localObject1 = "\n)";
        ((StringBuilder)localObject8).append((String)localObject1);
        break label623;
      }
    }
    localObject1 = new String[n];
    localObject7 = String.valueOf(localObject4);
    localObject5 = "Unknown query type ".concat((String)localObject7);
    localObject1[0] = localObject5;
    AssertionUtil.AlwaysFatal.fail((String[])localObject1);
    label623:
    if (localObject3 != null)
    {
      localObject1 = localObject3;
      localObject1 = (CharSequence)localObject3;
      bool2 = c.n.m.a((CharSequence)localObject1) ^ n;
      if (bool2)
      {
        localObject1 = "new_conversation";
        bool2 = c.g.b.k.a(localObject4, localObject1);
        if (bool2)
        {
          localObject1 = "\nAND\n";
          ((StringBuilder)localObject8).append((String)localObject1);
        }
        else
        {
          localObject1 = "\nWHERE\n";
          ((StringBuilder)localObject8).append((String)localObject1);
        }
        ((StringBuilder)localObject8).append("((");
        localObject1 = p.a();
        Object localObject9 = localObject1;
        localObject9 = (Iterable)localObject1;
        CharSequence localCharSequence = (CharSequence)" OR ";
        localObject1 = new com/truecaller/content/o$a;
        ((o.a)localObject1).<init>((List)localObject6, (String)localObject3);
        Object localObject10 = localObject1;
        localObject10 = (c.g.a.b)localObject1;
        int i1 = 30;
        localObject1 = c.a.m.a((Iterable)localObject9, localCharSequence, null, null, 0, null, (c.g.a.b)localObject10, i1);
        ((StringBuilder)localObject8).append((String)localObject1);
        ((StringBuilder)localObject8).append(")");
        localObject1 = "+";
        bool2 = c.n.m.b((String)localObject3, (String)localObject1, false);
        localObject4 = null;
        if (bool2)
        {
          localObject1 = a((String)localObject3, null);
          if (localObject1 != null)
          {
            localObject5 = com.google.c.a.k.a();
            localObject7 = k.c.a;
            localObject1 = ((com.google.c.a.k)localObject5).a((m.a)localObject1, (k.c)localObject7);
            break label914;
          }
          bool2 = localo.a((String)localObject3);
          if (bool2)
          {
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>("+");
            localObject5 = b((String)localObject3);
            ((StringBuilder)localObject1).append((String)localObject5);
            localObject1 = ((StringBuilder)localObject1).toString();
            break label914;
          }
        }
        bool2 = false;
        localObject1 = null;
        label914:
        char c1 = '%';
        if (localObject1 != null)
        {
          ((StringBuilder)localObject8).append(" OR numbers_group LIKE ?");
          localObject4 = new java/lang/StringBuilder;
          localObject3 = "%";
          ((StringBuilder)localObject4).<init>((String)localObject3);
          ((StringBuilder)localObject4).append((String)localObject1);
          ((StringBuilder)localObject4).append(c1);
          localObject1 = ((StringBuilder)localObject4).toString();
          ((List)localObject6).add(localObject1);
        }
        else
        {
          localObject1 = b;
          localObject7 = "profileCountryIso";
          localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject7);
          if (localObject1 != null)
          {
            if (localObject1 != null)
            {
              localObject1 = ((String)localObject1).toUpperCase();
              localObject7 = "(this as java.lang.String).toUpperCase()";
              c.g.b.k.a(localObject1, (String)localObject7);
            }
            else
            {
              localObject1 = new c/u;
              ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
              throw ((Throwable)localObject1);
            }
          }
          else
          {
            bool2 = false;
            localObject1 = null;
          }
          localObject1 = a((String)localObject3, (String)localObject1);
          if (localObject1 != null)
          {
            long l2 = d;
            localObject4 = String.valueOf(l2);
          }
          else
          {
            bool2 = localo.a((String)localObject3);
            if (bool2) {
              localObject4 = b((String)localObject3);
            }
          }
          if (localObject4 != null)
          {
            ((StringBuilder)localObject8).append(" OR numbers_group LIKE ?");
            localObject1 = new java/lang/StringBuilder;
            localObject3 = "%";
            ((StringBuilder)localObject1).<init>((String)localObject3);
            ((StringBuilder)localObject1).append((String)localObject4);
            ((StringBuilder)localObject1).append(c1);
            localObject1 = ((StringBuilder)localObject1).toString();
            ((List)localObject6).add(localObject1);
          }
        }
        ((StringBuilder)localObject8).append(") ");
        localObject1 = "append(\") \")";
        c.g.b.k.a(localObject8, (String)localObject1);
      }
    }
    ((StringBuilder)localObject8).append("\nGROUP BY numbers_group\nORDER BY group_sorting, date_sorting DESC, name_sorting COLLATE NOCASE;\n");
    localObject1 = new String[n];
    localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>("New Conversation destinations query.\nArgs: ");
    ((StringBuilder)localObject4).append(localObject6);
    localObject3 = "\nQuery:\n";
    ((StringBuilder)localObject4).append((String)localObject3);
    ((StringBuilder)localObject4).append(localObject8);
    localObject4 = ((StringBuilder)localObject4).toString();
    localObject1[0] = localObject4;
    localObject1 = parama.c();
    localObject4 = ((StringBuilder)localObject8).toString();
    Object localObject6 = (Collection)localObject6;
    localObject2 = new String[0];
    localObject2 = ((Collection)localObject6).toArray((Object[])localObject2);
    if (localObject2 != null)
    {
      localObject2 = (String[])localObject2;
      localObject3 = paramCancellationSignal;
      localObject1 = ((SQLiteDatabase)localObject1).rawQuery((String)localObject4, (String[])localObject2, paramCancellationSignal);
      c.g.b.k.a(localObject1, "provider.database.rawQue…ay(), cancellationSignal)");
      return (Cursor)localObject1;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */