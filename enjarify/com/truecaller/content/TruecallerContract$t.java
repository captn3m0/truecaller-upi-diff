package com.truecaller.content;

import android.net.Uri;
import android.net.Uri.Builder;

public final class TruecallerContract$t
{
  static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "msg/msg_im_group_participants_view");
  }
  
  public static Uri a(String paramString)
  {
    return TruecallerContract.b.buildUpon().appendEncodedPath("msg/msg_im_group_participants_view").appendQueryParameter("peer_id", paramString).build();
  }
  
  public static Uri a(String paramString1, String paramString2)
  {
    return TruecallerContract.b.buildUpon().appendEncodedPath("msg/msg_im_group_participants_view").appendQueryParameter("group_id", paramString1).appendQueryParameter("self_peer_id", paramString2).build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */