package com.truecaller.content;

import android.net.Uri;
import android.net.Uri.Builder;

public final class TruecallerContract$c
{
  public static final String[] a = { "recording_path", "history_event_id" };
  
  public static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "call_recordings");
  }
  
  public static Uri b()
  {
    return TruecallerContract.b.buildUpon().appendEncodedPath("call_recordings_with_history_event").build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */