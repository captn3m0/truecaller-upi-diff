package com.truecaller.content;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import c.g.b.k;
import com.truecaller.common.c.a.a.g;
import com.truecaller.common.h.am;
import com.truecaller.featuretoggles.b;
import com.truecaller.utils.extensions.r;
import java.util.Iterator;
import java.util.List;

public final class e
  implements a.g
{
  private final com.truecaller.featuretoggles.e a;
  
  public e(com.truecaller.featuretoggles.e parame)
  {
    a = parame;
  }
  
  private final String a(int paramInt)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SELECT c._id AS _id, c.type AS type, c.tc_group_id AS tc_group_id, c.has_outgoing_messages AS outgoing_message_count, c.white_list_count AS white_list_count, c.blacklist_count AS blacklist_count, c.top_spammer_count AS top_spammer_count, c.phonebook_count AS phonebook_count, c.split_criteria AS split_criteria, c.preferred_transport AS preferred_transport, ts.date_sorting AS date, ts.unread_messages_count AS unread_messages_count, ts.latest_message_id AS latest_message_id, ts.latest_message_status AS latest_message_status, ts.latest_message_media_count AS latest_message_media_count, ts.latest_message_media_type AS latest_message_media_type, ts.latest_sim_token AS latest_sim_token, ts.snippet_text AS snippet_text, ts.actions_dismissed AS actions_dismissed, ts.filter AS filter, ts.latest_message_read_status AS latest_message_read_status, ts.latest_message_delivery_status AS latest_message_delivery_status, ts.latest_message_raw_status AS latest_message_raw_status, ts.latest_message_transport AS latest_message_transport, ig.im_group_id AS im_group_id, ig.title AS im_group_title, ig.avatar AS im_group_avatar, ig.invited_date AS im_group_invited_date, ig.invited_by AS im_group_invited_by, ig.roles AS im_group_roles, ig.actions AS im_group_actions, ig.role_update_restriction_mask AS im_group_role_update_restriction_mask, ig.role_update_mask AS im_group_role_update_mask, ig.self_role_update_mask AS im_group_self_role_update_mask, ig.notification_settings AS im_group_notification_settings, ");
    Object localObject1 = a.e();
    boolean bool = ((b)localObject1).a();
    if (bool) {
      localObject1 = "h.type AS history_type, h.number_type AS history_number_type, h.features AS history_features, ";
    } else {
      localObject1 = "";
    }
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append("MAX(MAX(p.top_spam_score), IFNULL(MAX(a.contact_spam_score), 0)) AS spam_score, GROUP_CONCAT(p._id,'|') AS participants_id, GROUP_CONCAT(p.type,'|') AS participants_type, GROUP_CONCAT(ifnull(length(p.tc_im_peer_id) || '|' || p.tc_im_peer_id,'|'),'') AS participants_im_id, GROUP_CONCAT(ifnull(length(p.raw_destination) || '|' || p.raw_destination,'|'),'') AS participants_raw_destinantion, GROUP_CONCAT(ifnull(length(p.normalized_destination) || '|' || p.normalized_destination,'|'),'') AS participants_normalized_destination, GROUP_CONCAT(ifnull(length(p.country_code) || '|' || p.country_code,'|'),'') AS participants_country_codes, GROUP_CONCAT(ifnull(length(a.tc_id) || '|' || a.tc_id,'|'),'') AS participants_tc_id, GROUP_CONCAT(p.aggregated_contact_id,'|') AS participants_aggregated_contact_id, GROUP_CONCAT(p.filter_action,'|') AS participants_filter_action, GROUP_CONCAT(p.is_top_spammer,'|') AS participants_is_top_spammer, GROUP_CONCAT(ifnull(length(a.contact_name) || '|' || a.contact_name,'|'),'') AS participants_name, GROUP_CONCAT(ifnull(length(a.contact_image_url) || '|' || a.contact_image_url,'|'),'') AS participants_image_url, GROUP_CONCAT(ifnull(a.contact_source, 0), '|') AS participants_source, GROUP_CONCAT(ifnull(a.contact_phonebook_id, -1), '|') AS participants_phonebook_id, GROUP_CONCAT(ifnull(a.contact_badges, 0), '|') AS participants_badges, GROUP_CONCAT(ifnull(length(a.contact_company) || '|' || a.contact_company,'|'),'') AS participants_company, GROUP_CONCAT(ifnull(a.contact_search_time, -1), '|') AS participants_search_time, GROUP_CONCAT(top_spam_score, '|') AS participants_top_spam_score, GROUP_CONCAT(MAX(IFNULL(a.contact_spam_score, 0), top_spam_score), '|') AS participants_spam_score FROM msg_conversations c LEFT JOIN msg_conversation_participants cp on cp.conversation_id = c._id LEFT JOIN msg_participants p on cp.participant_id = p._id LEFT JOIN msg_thread_stats ts on ts.conversation_id = c._id LEFT JOIN aggregated_contact a on p.aggregated_contact_id = a._id LEFT JOIN msg_im_group_info ig on p.normalized_destination = ig.im_group_id AND p.type = 4 ");
    localObject1 = a.e();
    bool = ((b)localObject1).a();
    if (bool) {
      localObject1 = "LEFT JOIN msg_history_transport_info h on ts.latest_message_id = h.message_id ";
    } else {
      localObject1 = "";
    }
    localStringBuilder.append((String)localObject1);
    localObject1 = "WHERE ts.filter = CASE WHEN split_criteria=0 THEN 1 ELSE ";
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" END ");
    Object localObject2 = a.i();
    paramInt = ((b)localObject2).a();
    if (paramInt == 0) {
      localObject2 = "AND c.tc_group_id IS NULL ";
    } else {
      localObject2 = "";
    }
    localStringBuilder.append((String)localObject2);
    localStringBuilder.append("GROUP BY cp.conversation_id");
    return localStringBuilder.toString();
  }
  
  public final Cursor a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    k.b(parama, "provider");
    k.b(parama1, "helper");
    k.b(paramUri, "uri");
    parama1 = parama.c();
    paramCancellationSignal = r.b(paramUri);
    Object localObject1 = paramUri.getQueryParameters("participant_addr");
    Object localObject2 = a.w();
    boolean bool1 = ((b)localObject2).a();
    int j = 0;
    String str1 = null;
    int k = 1;
    if (bool1)
    {
      localObject2 = paramUri.getQueryParameter("filter");
      i = org.c.a.a.a.b.a.a((String)localObject2, 0);
    }
    else
    {
      i = 1;
    }
    switch (i)
    {
    default: 
      i = 1;
      break;
    case 4: 
      i = 1;
      break;
    case 3: 
      i = 3;
      break;
    case 2: 
      i = 2;
    }
    int i1;
    if (localObject1 != null)
    {
      int m = ((List)localObject1).isEmpty();
      if (m == 0)
      {
        paramCancellationSignal = new java/lang/StringBuilder;
        localObject2 = "SELECT ";
        paramCancellationSignal.<init>((String)localObject2);
        String str2;
        if (paramArrayOfString1 != null)
        {
          i = paramArrayOfString1.length;
          if (i == 0)
          {
            i = 1;
          }
          else
          {
            i = 0;
            localObject2 = null;
          }
          i ^= k;
          if (i != 0)
          {
            localObject2 = paramArrayOfString1[0];
            paramCancellationSignal.append((String)localObject2);
            i = paramArrayOfString1.length;
            m = 1;
            while (m < i)
            {
              paramCancellationSignal.append(", ");
              str2 = paramArrayOfString1[m];
              paramCancellationSignal.append(str2);
              m += 1;
            }
          }
        }
        paramArrayOfString1 = "*";
        paramCancellationSignal.append(paramArrayOfString1);
        paramCancellationSignal.append(" FROM ");
        paramArrayOfString1 = new java/lang/StringBuilder;
        paramArrayOfString1.<init>("( ");
        localObject2 = a(k);
        paramArrayOfString1.append((String)localObject2);
        localObject2 = " )";
        paramArrayOfString1.append((String)localObject2);
        paramArrayOfString1 = paramArrayOfString1.toString();
        paramCancellationSignal.append(paramArrayOfString1);
        paramArrayOfString1 = " WHERE _id IN (SELECT conversation_id FROM msg_conversation_participants WHERE conversation_id  IN (SELECT cp.conversation_id FROM msg_participants p LEFT JOIN msg_conversation_participants cp ON cp.participant_id = p._id WHERE p.normalized_destination IN (";
        paramCancellationSignal.append(paramArrayOfString1);
        int i2 = ((List)localObject1).size();
        int i3;
        if (paramArrayOfString2 != null)
        {
          i = paramArrayOfString2.length;
          i2 += i;
        }
        paramArrayOfString1 = new String[i3];
        localObject2 = ((List)localObject1).iterator();
        str3 = (String)((Iterator)localObject2).next();
        paramArrayOfString1[0] = str3;
        str3 = "?";
        paramCancellationSignal.append(str3);
        int n = 1;
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject2).hasNext();
          if (!bool3) {
            break;
          }
          str2 = (String)((Iterator)localObject2).next();
          paramArrayOfString1[n] = str2;
          str2 = ",?";
          paramCancellationSignal.append(str2);
          n += k;
        }
        paramCancellationSignal.append(") GROUP BY conversation_id HAVING count(*)=");
        i = ((List)localObject1).size();
        paramCancellationSignal.append(i);
        paramCancellationSignal.append(") GROUP BY conversation_id HAVING count(*)=");
        i = ((List)localObject1).size();
        paramCancellationSignal.append(i);
        localObject2 = ")";
        paramCancellationSignal.append((String)localObject2);
        if (paramString1 != null)
        {
          localObject2 = " AND (";
          paramCancellationSignal.append((String)localObject2);
          paramCancellationSignal.append(paramString1);
          paramString1 = ")";
          paramCancellationSignal.append(paramString1);
          if (paramArrayOfString2 != null)
          {
            int i4 = ((List)localObject1).size();
            int i5 = paramArrayOfString2.length;
            System.arraycopy(paramArrayOfString2, 0, paramArrayOfString1, i4, i5);
          }
        }
        if (paramString2 != null)
        {
          paramString1 = " ORDER BY ";
          paramCancellationSignal.append(paramString1);
          paramCancellationSignal.append(paramString2);
        }
        paramString1 = paramCancellationSignal.toString();
        parama1 = parama1.rawQuery(paramString1, paramArrayOfString1);
        break label935;
      }
    }
    localObject1 = new java/lang/StringBuilder;
    String str3 = "SELECT ";
    ((StringBuilder)localObject1).<init>(str3);
    if (paramArrayOfString1 != null)
    {
      i1 = paramArrayOfString1.length;
      if (i1 == 0) {
        j = 1;
      }
      j ^= k;
      if (j != 0)
      {
        str1 = ", ";
        paramArrayOfString1 = am.a(paramArrayOfString1, str1);
        ((StringBuilder)localObject1).append(paramArrayOfString1);
        break label752;
      }
    }
    paramArrayOfString1 = "*";
    ((StringBuilder)localObject1).append(paramArrayOfString1);
    label752:
    paramArrayOfString1 = new java/lang/StringBuilder;
    str1 = " FROM (";
    paramArrayOfString1.<init>(str1);
    localObject2 = a(i);
    paramArrayOfString1.append((String)localObject2);
    int i = 41;
    paramArrayOfString1.append(i);
    paramArrayOfString1 = paramArrayOfString1.toString();
    ((StringBuilder)localObject1).append(paramArrayOfString1);
    paramArrayOfString1 = Integer.valueOf(-1);
    boolean bool2 = k.a(paramCancellationSignal, paramArrayOfString1) ^ k;
    if (bool2)
    {
      paramString1 = String.valueOf(paramCancellationSignal);
      paramArrayOfString1 = " WHERE _id = ".concat(paramString1);
      ((StringBuilder)localObject1).append(paramArrayOfString1);
    }
    else
    {
      if (paramString1 != null)
      {
        paramString1 = String.valueOf(paramString1);
        paramArrayOfString1 = " WHERE ".concat(paramString1);
        ((StringBuilder)localObject1).append(paramArrayOfString1);
      }
      if (paramString2 != null)
      {
        paramString1 = String.valueOf(paramString2);
        paramArrayOfString1 = " ORDER BY ".concat(paramString1);
        ((StringBuilder)localObject1).append(paramArrayOfString1);
      }
    }
    paramArrayOfString1 = ((StringBuilder)localObject1).toString();
    parama1 = parama1.rawQuery(paramArrayOfString1, paramArrayOfString2);
    label935:
    parama = parama.getContext();
    if (parama != null)
    {
      parama = parama.getContentResolver();
      parama1.setNotificationUri(parama, paramUri);
    }
    return parama1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */