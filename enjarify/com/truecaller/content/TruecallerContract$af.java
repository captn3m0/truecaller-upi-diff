package com.truecaller.content;

import android.net.Uri;
import android.net.Uri.Builder;

public final class TruecallerContract$af
{
  public static Uri a(String[] paramArrayOfString)
  {
    Uri.Builder localBuilder = TruecallerContract.b.buildUpon();
    String str1 = "msg/msg_participants_with_contact_info";
    localBuilder = localBuilder.appendEncodedPath(str1);
    int i = paramArrayOfString.length;
    int j = 0;
    while (j < i)
    {
      String str2 = paramArrayOfString[j];
      String str3 = "number";
      localBuilder.appendQueryParameter(str3, str2);
      j += 1;
    }
    return localBuilder.build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */