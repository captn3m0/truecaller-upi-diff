package com.truecaller.content;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import com.truecaller.common.c.a.a.g;
import java.util.List;
import org.c.a.a.a.k;

final class s
  implements a.g
{
  private static final String a;
  private final Context b;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SELECT -1 as _id, CASE substr(d.data1, 1, 1) WHEN '+' THEN 0 ELSE 1 END as type, d.data9 as raw_destination, d.data2 as national_destination, d.data1 as normalized_destination, d.data7 as country_code, NULL as tc_im_peer_id, a._id as aggregated_contact_id, a.tc_id as tc_id, CASE f.rule WHEN 0 THEN 1 WHEN 1 THEN 2 ELSE 0 END as filter_action, CASE WHEN  t._id IS NULL THEN 0 ELSE 1 END  as is_top_spammer, IFNULL(t.count, 0)  as top_spam_score, a.contact_name as name, a.contact_image_url as image_url, a.contact_source as source, a.contact_badges as badges, a.contact_company as company_name, IFNULL(a.contact_phonebook_id, -1) as phonebook_id, IFNULL(a.contact_spam_score, 0) as spam_score, i.im_peer_id as tc_im_peer_id, a.contact_search_time as search_time FROM data d LEFT JOIN raw_contact r  ON d.data_raw_contact_id= r._id LEFT JOIN aggregated_contact a ON a._id=aggregated_contact_id LEFT JOIN filters f  ON f.wildcard_type = ");
    int i = NONEtype;
    localStringBuilder.append(i);
    localStringBuilder.append(" AND f.value=d.data1 LEFT JOIN topspammers t ON t.value=data1 LEFT JOIN msg_im_users i ON i.normalized_number=data1 WHERE d.data_type=4 AND d.data1 NOT NULL AND d.data1 != '' AND a.contact_source!=4");
    a = localStringBuilder.toString();
  }
  
  s(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
  }
  
  public final Cursor a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    Object localObject1 = parama.c();
    Object localObject2 = paramUri;
    Object localObject3 = paramUri.getQueryParameters("number");
    if (localObject3 != null)
    {
      boolean bool1 = ((List)localObject3).isEmpty();
      if (!bool1)
      {
        int i = ((List)localObject3).size();
        localObject4 = k.a("?", ",", i);
        int j = i * 4;
        arrayOfString = new String[j];
        k = 0;
        for (;;)
        {
          m = ((List)localObject3).size();
          if (k >= m) {
            break;
          }
          str = (String)((List)localObject3).get(k);
          arrayOfString[k] = str;
          int n = i + k;
          arrayOfString[n] = str;
          n = i * 2 + k;
          arrayOfString[n] = str;
          n = i * 3 + k;
          arrayOfString[n] = str;
          k += 1;
        }
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("SELECT _id as _id, type as type, raw_destination as raw_destination, national_destination as national_destination, normalized_destination as normalized_destination, country_code as country_code, tc_im_peer_id as tc_im_peer_id, aggregated_contact_id as aggregated_contact_id, tc_id as tc_id, filter_action as filter_action, is_top_spammer as is_top_spammer, top_spam_score as top_spam_score, name as name, image_url as image_url, source as source, badges as badges, company_name as company_name, phonebook_id as phonebook_id, spam_score as spam_score, tc_im_peer_id as tc_im_peer_id, search_time as search_time from msg_participants_with_contact_info where normalized_destination IN ");
        ((StringBuilder)localObject3).append("(");
        ((StringBuilder)localObject3).append((String)localObject4);
        ((StringBuilder)localObject3).append(")");
        ((StringBuilder)localObject3).append(" UNION ");
        localObject2 = a;
        ((StringBuilder)localObject3).append((String)localObject2);
        ((StringBuilder)localObject3).append(" AND (data1 IN (");
        ((StringBuilder)localObject3).append((String)localObject4);
        ((StringBuilder)localObject3).append(") OR data9 IN (");
        ((StringBuilder)localObject3).append((String)localObject4);
        ((StringBuilder)localObject3).append(") OR data2 IN (");
        ((StringBuilder)localObject3).append((String)localObject4);
        localObject2 = "))";
        ((StringBuilder)localObject3).append((String)localObject2);
        boolean bool2 = k.b(paramString2);
        if (!bool2)
        {
          localObject2 = " ORDER BY ";
          ((StringBuilder)localObject3).append((String)localObject2);
          ((StringBuilder)localObject3).append(paramString2);
        }
        localObject3 = ((StringBuilder)localObject3).toString();
        return ((SQLiteDatabase)localObject1).rawQuery((String)localObject3, arrayOfString);
      }
    }
    localObject3 = "msg_participants_with_contact_info";
    int k = 0;
    int m = 0;
    String str = null;
    localObject2 = paramArrayOfString1;
    Object localObject4 = paramString1;
    String[] arrayOfString = paramArrayOfString2;
    localObject1 = ((SQLiteDatabase)localObject1).query((String)localObject3, paramArrayOfString1, paramString1, paramArrayOfString2, null, null, paramString2);
    if (localObject1 != null)
    {
      localObject3 = this;
      localObject2 = b.getContentResolver();
      localObject4 = parama1;
      localObject4 = i;
      ((Cursor)localObject1).setNotificationUri((ContentResolver)localObject2, (Uri)localObject4);
    }
    else
    {
      localObject3 = this;
    }
    return (Cursor)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */