package com.truecaller.content;

import android.content.ContentValues;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.text.TextUtils;
import com.truecaller.common.c.a.a.b;
import com.truecaller.common.c.a.a.e;
import com.truecaller.common.c.a.a.f;
import com.truecaller.common.c.b.b;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

public final class t
  implements a.b, a.e, a.f
{
  private static final String a;
  private final Comparator b;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("INSERT OR REPLACE INTO aggregated_contact (tc_id, tc_flag, aggregated_update_timestamp, ");
    String[] arrayOfString = TruecallerContract.d.a;
    String str = TextUtils.join(",", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(") SELECT tc_id, tc_flag, insert_timestamp, ");
    arrayOfString = TruecallerContract.d.a;
    str = TextUtils.join(",", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(" FROM raw_contact WHERE _id=?");
    a = localStringBuilder.toString();
  }
  
  public t()
  {
    t.1 local1 = new com/truecaller/content/t$1;
    local1.<init>(this);
    b = local1;
  }
  
  private static int a(SQLiteDatabase paramSQLiteDatabase, long paramLong1, long paramLong2)
  {
    String str = "UPDATE raw_contact SET aggregated_contact_id=? WHERE _id=?";
    paramSQLiteDatabase = paramSQLiteDatabase.compileStatement(str);
    int i = 1;
    try
    {
      paramSQLiteDatabase.bindLong(i, paramLong1);
      int j = 2;
      paramSQLiteDatabase.bindLong(j, paramLong2);
      j = paramSQLiteDatabase.executeUpdateDelete();
      return j;
    }
    finally
    {
      paramSQLiteDatabase.close();
    }
  }
  
  private static int a(SQLiteDatabase paramSQLiteDatabase, com.truecaller.common.c.a.a parama, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    boolean bool1;
    if (paramString == null)
    {
      bool1 = e;
      if (!bool1) {
        return paramSQLiteDatabase.delete("aggregated_contact", "1", null);
      }
    }
    boolean bool2 = e;
    if (bool2)
    {
      paramString = android.support.v4.a.a.a(paramString, "_id=?");
      bool2 = true;
      parama = new String[bool2];
      bool1 = false;
      paramUri = paramUri.getLastPathSegment();
      parama[0] = paramUri;
      paramArrayOfString = android.support.v4.a.a.a(paramArrayOfString, parama);
    }
    parama = new java/lang/StringBuilder;
    parama.<init>("DELETE FROM aggregated_contact WHERE _id IN (SELECT raw_contact.aggregated_contact_id FROM raw_contact WHERE ");
    parama.append(paramString);
    paramUri = ")";
    parama.append(paramUri);
    parama = parama.toString();
    paramSQLiteDatabase = paramSQLiteDatabase.compileStatement(parama);
    try
    {
      paramSQLiteDatabase.bindAllArgsAsStrings(paramArrayOfString);
      int i = paramSQLiteDatabase.executeUpdateDelete();
      return i;
    }
    finally
    {
      paramSQLiteDatabase.close();
    }
  }
  
  private static void a(ContentValues paramContentValues1, String paramString, ContentValues paramContentValues2, ContentValues paramContentValues3)
  {
    paramContentValues2 = paramContentValues2.get(paramString);
    if (paramContentValues2 == null)
    {
      paramContentValues2 = paramContentValues3.get(paramString);
    }
    else
    {
      boolean bool = paramContentValues2 instanceof String;
      if (bool)
      {
        Object localObject = paramContentValues2;
        localObject = (String)paramContentValues2;
        paramContentValues3 = paramContentValues3.get(paramString);
        bool = TextUtils.isEmpty((CharSequence)localObject);
        if ((bool) && (paramContentValues3 != null)) {
          paramContentValues2 = paramContentValues3;
        }
      }
    }
    b.a(paramContentValues1, paramString, paramContentValues2);
  }
  
  private static boolean a(Collection paramCollection, ContentValues paramContentValues)
  {
    String str = "contact_phonebook_id";
    paramContentValues = paramContentValues.getAsLong(str);
    if (paramContentValues != null)
    {
      paramCollection = paramCollection.iterator();
      do
      {
        boolean bool = paramCollection.hasNext();
        if (!bool) {
          break;
        }
        paramContentValues = (ContentValues)paramCollection.next();
        str = "contact_phonebook_id";
        paramContentValues = paramContentValues.getAsLong(str);
      } while (paramContentValues == null);
      return false;
    }
    return true;
  }
  
  private static int b(SQLiteDatabase paramSQLiteDatabase, com.truecaller.common.c.a.a parama, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    boolean bool1;
    if (paramString == null)
    {
      bool1 = e;
      if (!bool1)
      {
        parama = new android/content/ContentValues;
        parama.<init>();
        parama.putNull("tc_id");
        return paramSQLiteDatabase.update("history", parama, null, null);
      }
    }
    boolean bool2 = e;
    if (bool2)
    {
      paramString = DatabaseUtils.concatenateWhere(paramString, "_id=?");
      bool2 = true;
      parama = new String[bool2];
      bool1 = false;
      paramUri = paramUri.getLastPathSegment();
      parama[0] = paramUri;
      paramArrayOfString = DatabaseUtils.appendSelectionArgs(paramArrayOfString, parama);
    }
    parama = new java/lang/StringBuilder;
    parama.<init>("UPDATE history SET tc_id = NULL WHERE tc_id IN (SELECT tc_id FROM raw_contact WHERE ");
    parama.append(paramString);
    paramUri = ")";
    parama.append(paramUri);
    parama = parama.toString();
    paramSQLiteDatabase = paramSQLiteDatabase.compileStatement(parama);
    try
    {
      paramSQLiteDatabase.bindAllArgsAsStrings(paramArrayOfString);
      int i = paramSQLiteDatabase.executeUpdateDelete();
      return i;
    }
    finally
    {
      paramSQLiteDatabase.close();
    }
  }
  
  private static int b(Integer paramInteger)
  {
    if (paramInteger == null) {
      return 0;
    }
    return paramInteger.intValue();
  }
  
  private static long b(Long paramLong)
  {
    if (paramLong == null) {
      return 0L;
    }
    return paramLong.longValue();
  }
  
  /* Error */
  public static boolean b(SQLiteDatabase paramSQLiteDatabase)
  {
    // Byte code:
    //   0: invokestatic 202	java/lang/System:currentTimeMillis	()J
    //   3: pop2
    //   4: aload_0
    //   5: ldc -52
    //   7: invokevirtual 67	android/database/sqlite/SQLiteDatabase:compileStatement	(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    //   10: astore_1
    //   11: aload_1
    //   12: invokevirtual 79	android/database/sqlite/SQLiteStatement:executeUpdateDelete	()I
    //   15: iconst_0
    //   16: iadd
    //   17: istore_2
    //   18: aload_1
    //   19: invokevirtual 82	android/database/sqlite/SQLiteStatement:close	()V
    //   22: ldc -50
    //   24: astore_1
    //   25: aload_0
    //   26: aload_1
    //   27: invokevirtual 67	android/database/sqlite/SQLiteDatabase:compileStatement	(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    //   30: astore_0
    //   31: aload_0
    //   32: invokevirtual 79	android/database/sqlite/SQLiteStatement:executeUpdateDelete	()I
    //   35: istore_3
    //   36: iload_2
    //   37: iload_3
    //   38: iadd
    //   39: istore_2
    //   40: aload_0
    //   41: invokevirtual 82	android/database/sqlite/SQLiteStatement:close	()V
    //   44: invokestatic 202	java/lang/System:currentTimeMillis	()J
    //   47: pop2
    //   48: iload_2
    //   49: ifle +5 -> 54
    //   52: iconst_1
    //   53: ireturn
    //   54: iconst_0
    //   55: ireturn
    //   56: astore_1
    //   57: aload_0
    //   58: invokevirtual 82	android/database/sqlite/SQLiteStatement:close	()V
    //   61: aload_1
    //   62: athrow
    //   63: astore_0
    //   64: aload_1
    //   65: invokevirtual 82	android/database/sqlite/SQLiteStatement:close	()V
    //   68: aload_0
    //   69: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	70	0	paramSQLiteDatabase	SQLiteDatabase
    //   10	17	1	localObject1	Object
    //   56	9	1	localObject2	Object
    //   17	32	2	i	int
    //   35	4	3	j	int
    // Exception table:
    //   from	to	target	type
    //   31	35	56	finally
    //   11	15	63	finally
  }
  
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    b(parama.c(), parama1, paramUri, paramString, paramArrayOfString);
    SQLiteDatabase localSQLiteDatabase = parama.c();
    int i = a(localSQLiteDatabase, parama1, paramUri, paramString, paramArrayOfString);
    if (i > 0)
    {
      parama = (TruecallerContentProvider)parama;
      parama1 = TruecallerContentProvider.AggregationState.IMMEDIATE;
      parama.a(parama1);
    }
    return -1;
  }
  
  public final Uri a(com.truecaller.common.c.a parama, Uri paramUri1, ContentValues paramContentValues, Uri paramUri2)
  {
    paramUri1 = paramContentValues.getAsLong("aggregated_contact_id");
    if (paramUri1 == null)
    {
      parama = (TruecallerContentProvider)parama;
      paramUri1 = TruecallerContentProvider.AggregationState.IMMEDIATE;
      parama.a(paramUri1);
    }
    return paramUri2;
  }
  
  public final Uri a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues)
  {
    parama1 = "false";
    String str = "aggregation";
    paramUri = paramUri.getQueryParameter(str);
    boolean bool1 = parama1.equals(paramUri);
    if (bool1)
    {
      parama1 = paramContentValues.getAsLong("aggregated_contact_id");
      if (parama1 == null)
      {
        parama1 = new android/content/ContentValues;
        parama1.<init>(paramContentValues);
        parama1.remove("aggregated_contact_id");
        parama = parama.c();
        paramUri = "aggregated_contact";
        str = "_id";
        long l1 = parama.insert(paramUri, str, parama1);
        long l2 = 0L;
        boolean bool2 = l1 < l2;
        if (bool2)
        {
          paramUri = "aggregated_contact_id";
          parama = Long.valueOf(l1);
          paramContentValues.put(paramUri, parama);
        }
      }
    }
    return null;
  }
  
  /* Error */
  public final boolean a(SQLiteDatabase paramSQLiteDatabase)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_2
    //   2: aload_1
    //   3: astore_3
    //   4: iconst_2
    //   5: istore 4
    //   7: iload 4
    //   9: anewarray 105	java/lang/String
    //   12: astore 5
    //   14: iconst_0
    //   15: istore 6
    //   17: aconst_null
    //   18: astore 7
    //   20: iconst_0
    //   21: istore 8
    //   23: aconst_null
    //   24: astore 9
    //   26: iconst_0
    //   27: istore 10
    //   29: iconst_0
    //   30: istore 11
    //   32: iload 8
    //   34: ifne +2335 -> 2369
    //   37: aload 5
    //   39: iconst_0
    //   40: ldc_w 264
    //   43: aastore
    //   44: iload 11
    //   46: invokestatic 267	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   49: astore 12
    //   51: iconst_1
    //   52: istore 13
    //   54: aload 5
    //   56: iload 13
    //   58: aload 12
    //   60: aastore
    //   61: aload_3
    //   62: ldc_w 269
    //   65: aload 5
    //   67: invokevirtual 273	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   70: astore 12
    //   72: aload 12
    //   74: ifnull +2290 -> 2364
    //   77: iconst_1
    //   78: istore 8
    //   80: aload 12
    //   82: invokeinterface 278 1 0
    //   87: istore 14
    //   89: iload 14
    //   91: ifeq +2206 -> 2297
    //   94: ldc -6
    //   96: astore 9
    //   98: aload 12
    //   100: aload 9
    //   102: invokeinterface 282 2 0
    //   107: istore 8
    //   109: aload 12
    //   111: iload 8
    //   113: invokeinterface 286 2 0
    //   118: lstore 15
    //   120: lload 15
    //   122: invokestatic 289	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   125: astore 17
    //   127: ldc_w 291
    //   130: astore 18
    //   132: iload 4
    //   134: anewarray 105	java/lang/String
    //   137: astore 19
    //   139: aload 19
    //   141: iconst_0
    //   142: aload 17
    //   144: aastore
    //   145: aload 19
    //   147: iload 13
    //   149: aload 17
    //   151: aastore
    //   152: aload_3
    //   153: aload 18
    //   155: aload 19
    //   157: invokevirtual 273	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   160: astore 17
    //   162: ldc -26
    //   164: astore 18
    //   166: aload 17
    //   168: aload 18
    //   170: invokeinterface 282 2 0
    //   175: istore 20
    //   177: new 293	java/util/LinkedHashMap
    //   180: astore 19
    //   182: aload 19
    //   184: invokespecial 294	java/util/LinkedHashMap:<init>	()V
    //   187: aload 17
    //   189: invokeinterface 278 1 0
    //   194: istore 21
    //   196: iconst_0
    //   197: istore 22
    //   199: aconst_null
    //   200: astore 23
    //   202: iload 21
    //   204: ifeq +170 -> 374
    //   207: aload 17
    //   209: iload 20
    //   211: invokeinterface 286 2 0
    //   216: lstore 24
    //   218: lload 24
    //   220: invokestatic 258	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   223: astore 26
    //   225: aload 19
    //   227: aload 26
    //   229: invokeinterface 299 2 0
    //   234: astore 26
    //   236: aload 26
    //   238: checkcast 301	java/util/Queue
    //   241: astore 26
    //   243: aload 26
    //   245: ifnonnull +32 -> 277
    //   248: new 303	java/util/LinkedList
    //   251: astore 26
    //   253: aload 26
    //   255: invokespecial 304	java/util/LinkedList:<init>	()V
    //   258: lload 24
    //   260: invokestatic 258	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   263: astore 27
    //   265: aload 19
    //   267: aload 27
    //   269: aload 26
    //   271: invokeinterface 307 3 0
    //   276: pop
    //   277: ldc_w 309
    //   280: astore 27
    //   282: lload 24
    //   284: invokestatic 289	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   287: astore 28
    //   289: aload 27
    //   291: aload 28
    //   293: invokevirtual 312	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   296: astore 27
    //   298: aload_3
    //   299: aload 27
    //   301: aconst_null
    //   302: invokevirtual 273	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   305: astore 27
    //   307: aload 27
    //   309: invokeinterface 278 1 0
    //   314: istore 22
    //   316: iload 22
    //   318: ifeq +33 -> 351
    //   321: new 129	android/content/ContentValues
    //   324: astore 23
    //   326: aload 23
    //   328: invokespecial 165	android/content/ContentValues:<init>	()V
    //   331: aload 27
    //   333: aload 23
    //   335: invokestatic 316	android/database/DatabaseUtils:cursorRowToContentValues	(Landroid/database/Cursor;Landroid/content/ContentValues;)V
    //   338: aload 26
    //   340: aload 23
    //   342: invokeinterface 319 2 0
    //   347: pop
    //   348: goto -41 -> 307
    //   351: aload 27
    //   353: invokeinterface 320 1 0
    //   358: iconst_2
    //   359: istore 4
    //   361: goto -174 -> 187
    //   364: astore_3
    //   365: aload 27
    //   367: invokeinterface 320 1 0
    //   372: aload_3
    //   373: athrow
    //   374: aload 19
    //   376: invokeinterface 322 1 0
    //   381: istore 4
    //   383: iload 4
    //   385: ifne +1732 -> 2117
    //   388: new 129	android/content/ContentValues
    //   391: astore 27
    //   393: aload 27
    //   395: invokespecial 165	android/content/ContentValues:<init>	()V
    //   398: aload 12
    //   400: aload 27
    //   402: invokestatic 316	android/database/DatabaseUtils:cursorRowToContentValues	(Landroid/database/Cursor;Landroid/content/ContentValues;)V
    //   405: iconst_m1
    //   406: i2l
    //   407: lstore 29
    //   409: aload 19
    //   411: invokeinterface 326 1 0
    //   416: astore 18
    //   418: aload 18
    //   420: invokeinterface 329 1 0
    //   425: astore 18
    //   427: lload 29
    //   429: lstore 31
    //   431: aload 18
    //   433: invokeinterface 160 1 0
    //   438: istore 33
    //   440: iload 33
    //   442: ifeq +1409 -> 1851
    //   445: aload 18
    //   447: invokeinterface 164 1 0
    //   452: astore 34
    //   454: aload 34
    //   456: checkcast 331	java/util/Map$Entry
    //   459: astore 34
    //   461: aload 34
    //   463: invokeinterface 334 1 0
    //   468: astore 35
    //   470: aload 35
    //   472: astore 7
    //   474: aload 35
    //   476: checkcast 150	java/util/Collection
    //   479: astore 7
    //   481: aload 7
    //   483: aload 27
    //   485: invokestatic 337	com/truecaller/content/t:a	(Ljava/util/Collection;Landroid/content/ContentValues;)Z
    //   488: istore 6
    //   490: iload 6
    //   492: ifeq +1305 -> 1797
    //   495: aload 23
    //   497: ifnull +59 -> 556
    //   500: aload 34
    //   502: invokeinterface 334 1 0
    //   507: astore 7
    //   509: aload 7
    //   511: checkcast 150	java/util/Collection
    //   514: astore 7
    //   516: aload 7
    //   518: aload 23
    //   520: invokestatic 337	com/truecaller/content/t:a	(Ljava/util/Collection;Landroid/content/ContentValues;)Z
    //   523: istore 6
    //   525: iload 6
    //   527: ifeq +6 -> 533
    //   530: goto +26 -> 556
    //   533: aload 27
    //   535: astore 36
    //   537: aload 5
    //   539: astore 37
    //   541: aload 12
    //   543: astore 38
    //   545: aload 17
    //   547: astore 39
    //   549: aload 18
    //   551: astore 40
    //   553: goto +1264 -> 1817
    //   556: new 339	java/util/TreeSet
    //   559: astore 7
    //   561: aload_2
    //   562: getfield 59	com/truecaller/content/t:b	Ljava/util/Comparator;
    //   565: astore 41
    //   567: aload 7
    //   569: aload 41
    //   571: invokespecial 342	java/util/TreeSet:<init>	(Ljava/util/Comparator;)V
    //   574: aload 7
    //   576: aload 27
    //   578: invokevirtual 343	java/util/TreeSet:add	(Ljava/lang/Object;)Z
    //   581: pop
    //   582: aload 34
    //   584: invokeinterface 334 1 0
    //   589: astore 41
    //   591: aload 41
    //   593: checkcast 150	java/util/Collection
    //   596: astore 41
    //   598: aload 7
    //   600: aload 41
    //   602: invokevirtual 347	java/util/TreeSet:addAll	(Ljava/util/Collection;)Z
    //   605: pop
    //   606: aload 7
    //   608: invokevirtual 350	java/util/TreeSet:pollFirst	()Ljava/lang/Object;
    //   611: astore 41
    //   613: aload 41
    //   615: checkcast 129	android/content/ContentValues
    //   618: astore 41
    //   620: aload 7
    //   622: invokevirtual 351	java/util/TreeSet:iterator	()Ljava/util/Iterator;
    //   625: astore 7
    //   627: aload 7
    //   629: invokeinterface 160 1 0
    //   634: istore 42
    //   636: iload 42
    //   638: ifeq +969 -> 1607
    //   641: aload 7
    //   643: invokeinterface 164 1 0
    //   648: astore 23
    //   650: aload 23
    //   652: checkcast 129	android/content/ContentValues
    //   655: astore 23
    //   657: new 129	android/content/ContentValues
    //   660: astore 28
    //   662: aload 28
    //   664: invokespecial 165	android/content/ContentValues:<init>	()V
    //   667: aload 27
    //   669: astore 36
    //   671: ldc_w 353
    //   674: astore 27
    //   676: aload 41
    //   678: aload 27
    //   680: invokevirtual 357	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   683: astore 27
    //   685: aload 27
    //   687: invokestatic 124	com/truecaller/content/t:b	(Ljava/lang/Integer;)I
    //   690: istore 4
    //   692: aload 5
    //   694: astore 37
    //   696: ldc_w 353
    //   699: astore 5
    //   701: aload 23
    //   703: aload 5
    //   705: invokevirtual 357	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   708: astore 5
    //   710: aload 5
    //   712: invokestatic 124	com/truecaller/content/t:b	(Ljava/lang/Integer;)I
    //   715: istore 43
    //   717: aload 7
    //   719: astore 44
    //   721: aload_2
    //   722: getfield 59	com/truecaller/content/t:b	Ljava/util/Comparator;
    //   725: astore 7
    //   727: aload 7
    //   729: aload 41
    //   731: aload 23
    //   733: invokeinterface 363 3 0
    //   738: istore 6
    //   740: iload 6
    //   742: ifgt +17 -> 759
    //   745: aload 41
    //   747: astore 7
    //   749: aload 18
    //   751: astore 40
    //   753: aload 23
    //   755: astore_2
    //   756: goto +14 -> 770
    //   759: aload 41
    //   761: astore_2
    //   762: aload 18
    //   764: astore 40
    //   766: aload 23
    //   768: astore 7
    //   770: ldc_w 365
    //   773: astore 18
    //   775: aload 41
    //   777: aload 18
    //   779: invokevirtual 368	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   782: astore 18
    //   784: aload 12
    //   786: astore 38
    //   788: ldc_w 365
    //   791: astore 12
    //   793: aload 41
    //   795: aload 12
    //   797: invokevirtual 368	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   800: astore 12
    //   802: aload 18
    //   804: invokestatic 137	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   807: istore 45
    //   809: iload 45
    //   811: ifne +33 -> 844
    //   814: aload 18
    //   816: astore 39
    //   818: ldc_w 370
    //   821: astore 18
    //   823: aload 18
    //   825: aload 12
    //   827: invokevirtual 374	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   830: istore 20
    //   832: iload 20
    //   834: ifeq +6 -> 840
    //   837: goto +7 -> 844
    //   840: aload 39
    //   842: astore 12
    //   844: ldc_w 365
    //   847: astore 18
    //   849: aload 28
    //   851: aload 18
    //   853: aload 12
    //   855: invokevirtual 377	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   858: iload 43
    //   860: iload 4
    //   862: ior
    //   863: istore 43
    //   865: iload 43
    //   867: iconst_1
    //   868: iand
    //   869: istore 46
    //   871: iload 46
    //   873: ifne +15 -> 888
    //   876: iload 43
    //   878: bipush 64
    //   880: iand
    //   881: istore 46
    //   883: iload 46
    //   885: ifeq +13 -> 898
    //   888: iload 43
    //   890: bipush -5
    //   892: iand
    //   893: bipush -9
    //   895: iand
    //   896: istore 43
    //   898: iload 43
    //   900: bipush 8
    //   902: iand
    //   903: istore 46
    //   905: iload 46
    //   907: ifeq +10 -> 917
    //   910: iload 43
    //   912: bipush -5
    //   914: iand
    //   915: istore 43
    //   917: ldc_w 353
    //   920: astore 12
    //   922: iload 43
    //   924: invokestatic 380	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   927: astore 5
    //   929: aload 28
    //   931: aload 12
    //   933: aload 5
    //   935: invokevirtual 383	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   938: ldc_w 385
    //   941: astore 5
    //   943: aload 41
    //   945: aload 5
    //   947: invokevirtual 357	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   950: astore 5
    //   952: aload 5
    //   954: invokestatic 124	com/truecaller/content/t:b	(Ljava/lang/Integer;)I
    //   957: istore 43
    //   959: ldc_w 385
    //   962: astore 12
    //   964: aload 23
    //   966: aload 12
    //   968: invokevirtual 357	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   971: astore 12
    //   973: aload 12
    //   975: invokestatic 124	com/truecaller/content/t:b	(Ljava/lang/Integer;)I
    //   978: istore 46
    //   980: ldc_w 385
    //   983: astore 18
    //   985: iload 43
    //   987: iload 46
    //   989: invokestatic 391	java/lang/Math:max	(II)I
    //   992: istore 43
    //   994: iload 43
    //   996: invokestatic 380	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   999: astore 5
    //   1001: aload 28
    //   1003: aload 18
    //   1005: aload 5
    //   1007: invokevirtual 383	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   1010: iload 4
    //   1012: iconst_1
    //   1013: iand
    //   1014: istore 4
    //   1016: iload 4
    //   1018: ifeq +22 -> 1040
    //   1021: ldc_w 393
    //   1024: astore 27
    //   1026: aload 28
    //   1028: aload 27
    //   1030: aload 41
    //   1032: aload 23
    //   1034: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1037: goto +19 -> 1056
    //   1040: ldc_w 393
    //   1043: astore 27
    //   1045: aload 28
    //   1047: aload 27
    //   1049: aload 23
    //   1051: aload 41
    //   1053: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1056: ldc_w 398
    //   1059: astore 27
    //   1061: ldc_w 398
    //   1064: astore 5
    //   1066: aload 41
    //   1068: aload 5
    //   1070: invokevirtual 357	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   1073: astore 5
    //   1075: aload 5
    //   1077: invokestatic 124	com/truecaller/content/t:b	(Ljava/lang/Integer;)I
    //   1080: istore 43
    //   1082: ldc_w 398
    //   1085: astore 12
    //   1087: aload 23
    //   1089: aload 12
    //   1091: invokevirtual 357	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   1094: astore 12
    //   1096: aload 12
    //   1098: invokestatic 124	com/truecaller/content/t:b	(Ljava/lang/Integer;)I
    //   1101: istore 46
    //   1103: iload 43
    //   1105: iload 46
    //   1107: invokestatic 391	java/lang/Math:max	(II)I
    //   1110: istore 43
    //   1112: iload 43
    //   1114: invokestatic 380	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   1117: astore 5
    //   1119: aload 28
    //   1121: aload 27
    //   1123: aload 5
    //   1125: invokevirtual 383	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   1128: ldc_w 400
    //   1131: astore 27
    //   1133: aload 41
    //   1135: aload 27
    //   1137: invokevirtual 357	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   1140: astore 27
    //   1142: aload 27
    //   1144: invokestatic 124	com/truecaller/content/t:b	(Ljava/lang/Integer;)I
    //   1147: istore 4
    //   1149: ldc_w 400
    //   1152: astore 5
    //   1154: aload 23
    //   1156: aload 5
    //   1158: invokevirtual 357	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   1161: astore 5
    //   1163: aload 5
    //   1165: invokestatic 124	com/truecaller/content/t:b	(Ljava/lang/Integer;)I
    //   1168: istore 43
    //   1170: ldc_w 400
    //   1173: astore 12
    //   1175: iload 4
    //   1177: iload 43
    //   1179: invokestatic 391	java/lang/Math:max	(II)I
    //   1182: istore 4
    //   1184: iload 4
    //   1186: invokestatic 380	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   1189: astore 27
    //   1191: aload 28
    //   1193: aload 12
    //   1195: aload 27
    //   1197: invokevirtual 383	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   1200: ldc_w 402
    //   1203: astore 27
    //   1205: aload 41
    //   1207: aload 27
    //   1209: invokevirtual 357	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   1212: astore 27
    //   1214: aload 27
    //   1216: invokestatic 124	com/truecaller/content/t:b	(Ljava/lang/Integer;)I
    //   1219: istore 4
    //   1221: ldc_w 402
    //   1224: astore 5
    //   1226: aload 23
    //   1228: aload 5
    //   1230: invokevirtual 357	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   1233: astore 5
    //   1235: aload 5
    //   1237: invokestatic 124	com/truecaller/content/t:b	(Ljava/lang/Integer;)I
    //   1240: istore 43
    //   1242: ldc_w 402
    //   1245: astore 12
    //   1247: iload 4
    //   1249: iload 43
    //   1251: ior
    //   1252: istore 4
    //   1254: iload 4
    //   1256: invokestatic 380	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   1259: astore 27
    //   1261: aload 28
    //   1263: aload 12
    //   1265: aload 27
    //   1267: invokevirtual 383	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   1270: ldc_w 404
    //   1273: astore 27
    //   1275: ldc_w 406
    //   1278: astore 5
    //   1280: aload 41
    //   1282: aload 5
    //   1284: invokevirtual 148	android/content/ContentValues:getAsLong	(Ljava/lang/String;)Ljava/lang/Long;
    //   1287: astore 5
    //   1289: aload 5
    //   1291: invokestatic 127	com/truecaller/content/t:b	(Ljava/lang/Long;)J
    //   1294: lstore 47
    //   1296: ldc_w 406
    //   1299: astore 5
    //   1301: aload 23
    //   1303: aload 5
    //   1305: invokevirtual 148	android/content/ContentValues:getAsLong	(Ljava/lang/String;)Ljava/lang/Long;
    //   1308: astore 5
    //   1310: aload 17
    //   1312: astore 39
    //   1314: aload 5
    //   1316: invokestatic 127	com/truecaller/content/t:b	(Ljava/lang/Long;)J
    //   1319: lstore 49
    //   1321: lload 47
    //   1323: lload 49
    //   1325: invokestatic 409	java/lang/Math:max	(JJ)J
    //   1328: lstore 47
    //   1330: lload 47
    //   1332: invokestatic 258	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   1335: astore 5
    //   1337: aload 28
    //   1339: aload 27
    //   1341: aload 5
    //   1343: invokevirtual 262	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   1346: ldc_w 411
    //   1349: astore 27
    //   1351: aload 28
    //   1353: aload 27
    //   1355: aload 7
    //   1357: aload_2
    //   1358: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1361: ldc_w 413
    //   1364: astore 27
    //   1366: aload 28
    //   1368: aload 27
    //   1370: aload 7
    //   1372: aload_2
    //   1373: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1376: ldc_w 415
    //   1379: astore 27
    //   1381: aload 28
    //   1383: aload 27
    //   1385: aload 7
    //   1387: aload_2
    //   1388: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1391: ldc_w 417
    //   1394: astore 27
    //   1396: aload 28
    //   1398: aload 27
    //   1400: aload 7
    //   1402: aload_2
    //   1403: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1406: ldc_w 419
    //   1409: astore 27
    //   1411: aload 28
    //   1413: aload 27
    //   1415: aload 7
    //   1417: aload_2
    //   1418: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1421: ldc_w 421
    //   1424: astore 27
    //   1426: aload 28
    //   1428: aload 27
    //   1430: aload 7
    //   1432: aload_2
    //   1433: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1436: ldc_w 423
    //   1439: astore 27
    //   1441: aload 28
    //   1443: aload 27
    //   1445: aload 7
    //   1447: aload_2
    //   1448: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1451: ldc_w 425
    //   1454: astore 27
    //   1456: aload 28
    //   1458: aload 27
    //   1460: aload 7
    //   1462: aload_2
    //   1463: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1466: ldc_w 427
    //   1469: astore 27
    //   1471: aload 28
    //   1473: aload 27
    //   1475: aload 7
    //   1477: aload_2
    //   1478: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1481: ldc_w 429
    //   1484: astore 27
    //   1486: aload 28
    //   1488: aload 27
    //   1490: aload 7
    //   1492: aload_2
    //   1493: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1496: ldc -112
    //   1498: astore 27
    //   1500: aload 28
    //   1502: aload 27
    //   1504: aload 7
    //   1506: aload_2
    //   1507: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1510: ldc_w 431
    //   1513: astore 27
    //   1515: aload 28
    //   1517: aload 27
    //   1519: aload 7
    //   1521: aload_2
    //   1522: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1525: ldc_w 433
    //   1528: astore 27
    //   1530: aload 28
    //   1532: aload 27
    //   1534: aload 7
    //   1536: aload_2
    //   1537: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1540: ldc_w 435
    //   1543: astore 27
    //   1545: aload 28
    //   1547: aload 27
    //   1549: aload 7
    //   1551: aload_2
    //   1552: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1555: ldc_w 437
    //   1558: astore 27
    //   1560: aload 28
    //   1562: aload 27
    //   1564: aload 7
    //   1566: aload_2
    //   1567: invokestatic 396	com/truecaller/content/t:a	(Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    //   1570: aload 28
    //   1572: astore 41
    //   1574: aload 28
    //   1576: astore 23
    //   1578: aload 36
    //   1580: astore 27
    //   1582: aload 37
    //   1584: astore 5
    //   1586: aload 44
    //   1588: astore 7
    //   1590: aload 40
    //   1592: astore 18
    //   1594: aload 38
    //   1596: astore 12
    //   1598: aload_0
    //   1599: astore_2
    //   1600: goto -973 -> 627
    //   1603: astore_3
    //   1604: goto +676 -> 2280
    //   1607: aload 27
    //   1609: astore 36
    //   1611: aload 5
    //   1613: astore 37
    //   1615: aload 12
    //   1617: astore 38
    //   1619: aload 17
    //   1621: astore 39
    //   1623: aload 18
    //   1625: astore 40
    //   1627: lconst_0
    //   1628: lstore 51
    //   1630: lload 31
    //   1632: lload 51
    //   1634: lcmp
    //   1635: istore 43
    //   1637: iload 43
    //   1639: ifge +36 -> 1675
    //   1642: aload 34
    //   1644: invokeinterface 440 1 0
    //   1649: astore_2
    //   1650: aload_2
    //   1651: checkcast 193	java/lang/Long
    //   1654: astore_2
    //   1655: aload_2
    //   1656: invokevirtual 197	java/lang/Long:longValue	()J
    //   1659: lstore 31
    //   1661: aload_0
    //   1662: astore_2
    //   1663: iconst_0
    //   1664: istore 6
    //   1666: aconst_null
    //   1667: astore 7
    //   1669: iconst_1
    //   1670: istore 13
    //   1672: goto -1241 -> 431
    //   1675: iconst_1
    //   1676: istore 53
    //   1678: iload 53
    //   1680: anewarray 105	java/lang/String
    //   1683: astore 27
    //   1685: aload 34
    //   1687: invokeinterface 440 1 0
    //   1692: astore_2
    //   1693: aload_2
    //   1694: invokestatic 443	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   1697: astore_2
    //   1698: iconst_0
    //   1699: istore 43
    //   1701: aconst_null
    //   1702: astore 5
    //   1704: aload 27
    //   1706: iconst_0
    //   1707: aload_2
    //   1708: aastore
    //   1709: new 129	android/content/ContentValues
    //   1712: astore_2
    //   1713: aload_2
    //   1714: invokespecial 165	android/content/ContentValues:<init>	()V
    //   1717: ldc -26
    //   1719: astore 5
    //   1721: lload 31
    //   1723: invokestatic 258	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   1726: astore 7
    //   1728: aload_2
    //   1729: aload 5
    //   1731: aload 7
    //   1733: invokevirtual 262	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   1736: ldc_w 445
    //   1739: astore 5
    //   1741: ldc_w 447
    //   1744: astore 7
    //   1746: aload_3
    //   1747: aload 5
    //   1749: aload_2
    //   1750: aload 7
    //   1752: aload 27
    //   1754: invokevirtual 176	android/database/sqlite/SQLiteDatabase:update	(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   1757: pop
    //   1758: ldc 90
    //   1760: astore_2
    //   1761: ldc 98
    //   1763: astore 5
    //   1765: aload_3
    //   1766: aload_2
    //   1767: aload 5
    //   1769: aload 27
    //   1771: invokevirtual 96	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   1774: pop
    //   1775: aload 36
    //   1777: astore 27
    //   1779: aload 37
    //   1781: astore 5
    //   1783: aload_0
    //   1784: astore_2
    //   1785: iconst_0
    //   1786: istore 6
    //   1788: aconst_null
    //   1789: astore 7
    //   1791: iconst_1
    //   1792: istore 13
    //   1794: goto -1363 -> 431
    //   1797: aload 27
    //   1799: astore 36
    //   1801: aload 5
    //   1803: astore 37
    //   1805: aload 12
    //   1807: astore 38
    //   1809: aload 17
    //   1811: astore 39
    //   1813: aload 18
    //   1815: astore 40
    //   1817: aload 36
    //   1819: astore 27
    //   1821: aload 37
    //   1823: astore 5
    //   1825: aload 40
    //   1827: astore 18
    //   1829: aload 38
    //   1831: astore 12
    //   1833: aload 39
    //   1835: astore 17
    //   1837: aload_0
    //   1838: astore_2
    //   1839: iconst_0
    //   1840: istore 6
    //   1842: aconst_null
    //   1843: astore 7
    //   1845: iconst_1
    //   1846: istore 13
    //   1848: goto -1417 -> 431
    //   1851: aload 5
    //   1853: astore 37
    //   1855: aload 12
    //   1857: astore 38
    //   1859: aload 17
    //   1861: astore 39
    //   1863: aload 23
    //   1865: ifnull +243 -> 2108
    //   1868: ldc 90
    //   1870: astore_2
    //   1871: ldc 98
    //   1873: astore 27
    //   1875: iconst_1
    //   1876: istore 43
    //   1878: iload 43
    //   1880: anewarray 105	java/lang/String
    //   1883: astore 7
    //   1885: lload 31
    //   1887: invokestatic 289	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   1890: astore 12
    //   1892: iconst_0
    //   1893: istore 13
    //   1895: aconst_null
    //   1896: astore 41
    //   1898: aload 7
    //   1900: iconst_0
    //   1901: aload 12
    //   1903: aastore
    //   1904: aload_3
    //   1905: aload_2
    //   1906: aload 23
    //   1908: aload 27
    //   1910: aload 7
    //   1912: invokevirtual 176	android/database/sqlite/SQLiteDatabase:update	(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   1915: istore 53
    //   1917: iload 53
    //   1919: iload 43
    //   1921: if_icmpne +158 -> 2079
    //   1924: new 129	android/content/ContentValues
    //   1927: astore_2
    //   1928: aload_2
    //   1929: invokespecial 165	android/content/ContentValues:<init>	()V
    //   1932: ldc -26
    //   1934: astore 27
    //   1936: lload 31
    //   1938: invokestatic 258	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   1941: astore 5
    //   1943: aload_2
    //   1944: aload 27
    //   1946: aload 5
    //   1948: invokevirtual 262	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   1951: ldc_w 445
    //   1954: astore 27
    //   1956: ldc 98
    //   1958: astore 5
    //   1960: iconst_1
    //   1961: istore 6
    //   1963: iload 6
    //   1965: anewarray 105	java/lang/String
    //   1968: astore 12
    //   1970: lload 15
    //   1972: invokestatic 289	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   1975: astore 41
    //   1977: iconst_0
    //   1978: istore 14
    //   1980: aconst_null
    //   1981: astore 17
    //   1983: aload 12
    //   1985: iconst_0
    //   1986: aload 41
    //   1988: aastore
    //   1989: aload_3
    //   1990: aload 27
    //   1992: aload_2
    //   1993: aload 5
    //   1995: aload 12
    //   1997: invokevirtual 176	android/database/sqlite/SQLiteDatabase:update	(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   2000: istore 53
    //   2002: iload 53
    //   2004: iload 6
    //   2006: if_icmpne +16 -> 2022
    //   2009: aload 39
    //   2011: invokeinterface 320 1 0
    //   2016: iconst_1
    //   2017: istore 4
    //   2019: goto +178 -> 2197
    //   2022: new 449	android/database/sqlite/SQLiteException
    //   2025: astore_3
    //   2026: new 16	java/lang/StringBuilder
    //   2029: astore_2
    //   2030: ldc_w 451
    //   2033: astore 27
    //   2035: aload_2
    //   2036: aload 27
    //   2038: invokespecial 22	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   2041: aload_2
    //   2042: lload 15
    //   2044: invokevirtual 454	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   2047: pop
    //   2048: ldc_w 456
    //   2051: astore 27
    //   2053: aload_2
    //   2054: aload 27
    //   2056: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2059: pop
    //   2060: aload_2
    //   2061: lload 31
    //   2063: invokevirtual 454	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   2066: pop
    //   2067: aload_2
    //   2068: invokevirtual 47	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   2071: astore_2
    //   2072: aload_3
    //   2073: aload_2
    //   2074: invokespecial 457	android/database/sqlite/SQLiteException:<init>	(Ljava/lang/String;)V
    //   2077: aload_3
    //   2078: athrow
    //   2079: new 449	android/database/sqlite/SQLiteException
    //   2082: astore_3
    //   2083: ldc_w 459
    //   2086: astore_2
    //   2087: lload 31
    //   2089: invokestatic 289	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   2092: astore 27
    //   2094: aload_2
    //   2095: aload 27
    //   2097: invokevirtual 312	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   2100: astore_2
    //   2101: aload_3
    //   2102: aload_2
    //   2103: invokespecial 457	android/database/sqlite/SQLiteException:<init>	(Ljava/lang/String;)V
    //   2106: aload_3
    //   2107: athrow
    //   2108: iconst_0
    //   2109: istore 14
    //   2111: aconst_null
    //   2112: astore 17
    //   2114: goto +21 -> 2135
    //   2117: aload 5
    //   2119: astore 37
    //   2121: aload 12
    //   2123: astore 38
    //   2125: aload 17
    //   2127: astore 39
    //   2129: iconst_0
    //   2130: istore 14
    //   2132: aconst_null
    //   2133: astore 17
    //   2135: getstatic 49	com/truecaller/content/t:a	Ljava/lang/String;
    //   2138: astore_2
    //   2139: aload_3
    //   2140: aload_2
    //   2141: invokevirtual 67	android/database/sqlite/SQLiteDatabase:compileStatement	(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    //   2144: astore_2
    //   2145: iconst_1
    //   2146: istore 4
    //   2148: aload_2
    //   2149: iload 4
    //   2151: lload 15
    //   2153: invokevirtual 74	android/database/sqlite/SQLiteStatement:bindLong	(IJ)V
    //   2156: aload_2
    //   2157: invokevirtual 462	android/database/sqlite/SQLiteStatement:executeInsert	()J
    //   2160: lstore 54
    //   2162: lconst_0
    //   2163: lstore 47
    //   2165: lload 54
    //   2167: lload 47
    //   2169: lcmp
    //   2170: istore 20
    //   2172: iload 20
    //   2174: ifle +57 -> 2231
    //   2177: aload_3
    //   2178: lload 54
    //   2180: lload 15
    //   2182: invokestatic 465	com/truecaller/content/t:a	(Landroid/database/sqlite/SQLiteDatabase;JJ)I
    //   2185: pop
    //   2186: aload_2
    //   2187: invokevirtual 82	android/database/sqlite/SQLiteStatement:close	()V
    //   2190: aload 39
    //   2192: invokeinterface 320 1 0
    //   2197: aload 37
    //   2199: astore 5
    //   2201: aload 38
    //   2203: astore 12
    //   2205: aload_0
    //   2206: astore_2
    //   2207: iconst_2
    //   2208: istore 4
    //   2210: iconst_0
    //   2211: istore 6
    //   2213: aconst_null
    //   2214: astore 7
    //   2216: iconst_0
    //   2217: istore 8
    //   2219: aconst_null
    //   2220: astore 9
    //   2222: iconst_1
    //   2223: istore 10
    //   2225: iconst_1
    //   2226: istore 13
    //   2228: goto -2148 -> 80
    //   2231: new 449	android/database/sqlite/SQLiteException
    //   2234: astore_3
    //   2235: ldc_w 467
    //   2238: astore 27
    //   2240: lload 15
    //   2242: invokestatic 289	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   2245: astore 5
    //   2247: aload 27
    //   2249: aload 5
    //   2251: invokevirtual 312	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   2254: astore 27
    //   2256: aload_3
    //   2257: aload 27
    //   2259: invokespecial 457	android/database/sqlite/SQLiteException:<init>	(Ljava/lang/String;)V
    //   2262: aload_3
    //   2263: athrow
    //   2264: astore_3
    //   2265: aload_2
    //   2266: invokevirtual 82	android/database/sqlite/SQLiteStatement:close	()V
    //   2269: aload_3
    //   2270: athrow
    //   2271: astore_3
    //   2272: goto +12 -> 2284
    //   2275: astore_3
    //   2276: aload 12
    //   2278: astore 38
    //   2280: aload 17
    //   2282: astore 39
    //   2284: aload 39
    //   2286: invokeinterface 320 1 0
    //   2291: aload_3
    //   2292: athrow
    //   2293: astore_3
    //   2294: goto +58 -> 2352
    //   2297: aload 5
    //   2299: astore 37
    //   2301: aload 12
    //   2303: astore 38
    //   2305: iconst_0
    //   2306: istore 14
    //   2308: aconst_null
    //   2309: astore 17
    //   2311: iload 11
    //   2313: bipush 100
    //   2315: iadd
    //   2316: istore 11
    //   2318: aload 12
    //   2320: invokeinterface 320 1 0
    //   2325: aload_0
    //   2326: astore_2
    //   2327: iconst_2
    //   2328: istore 4
    //   2330: iconst_0
    //   2331: istore 6
    //   2333: aconst_null
    //   2334: astore 7
    //   2336: goto -2304 -> 32
    //   2339: astore_3
    //   2340: aload 12
    //   2342: astore 38
    //   2344: goto +11 -> 2355
    //   2347: astore_3
    //   2348: aload 12
    //   2350: astore 38
    //   2352: aload_3
    //   2353: athrow
    //   2354: astore_3
    //   2355: aload 38
    //   2357: invokeinterface 320 1 0
    //   2362: aload_3
    //   2363: athrow
    //   2364: aload_0
    //   2365: astore_2
    //   2366: goto -2334 -> 32
    //   2369: iload 10
    //   2371: ifeq +37 -> 2408
    //   2374: aload_3
    //   2375: ldc_w 469
    //   2378: invokevirtual 67	android/database/sqlite/SQLiteDatabase:compileStatement	(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    //   2381: astore_2
    //   2382: aload_2
    //   2383: invokevirtual 79	android/database/sqlite/SQLiteStatement:executeUpdateDelete	()I
    //   2386: pop
    //   2387: aload_2
    //   2388: invokevirtual 82	android/database/sqlite/SQLiteStatement:close	()V
    //   2391: aload_1
    //   2392: invokestatic 474	com/truecaller/content/r:a	(Landroid/database/sqlite/SQLiteDatabase;)V
    //   2395: goto +13 -> 2408
    //   2398: astore_3
    //   2399: aload_3
    //   2400: astore 27
    //   2402: aload_2
    //   2403: invokevirtual 82	android/database/sqlite/SQLiteStatement:close	()V
    //   2406: aload_3
    //   2407: athrow
    //   2408: iload 10
    //   2410: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	2411	0	this	t
    //   0	2411	1	paramSQLiteDatabase	SQLiteDatabase
    //   1	2402	2	localObject1	Object
    //   3	296	3	localSQLiteDatabase	SQLiteDatabase
    //   364	9	3	localObject2	Object
    //   1603	387	3	localObject3	Object
    //   2025	238	3	localSQLiteException	android.database.sqlite.SQLiteException
    //   2264	6	3	localObject4	Object
    //   2271	1	3	localObject5	Object
    //   2275	17	3	localObject6	Object
    //   2293	1	3	localRuntimeException1	RuntimeException
    //   2339	1	3	localObject7	Object
    //   2347	6	3	localRuntimeException2	RuntimeException
    //   2354	21	3	localObject8	Object
    //   2398	9	3	localObject9	Object
    //   5	355	4	i	int
    //   381	3	4	bool1	boolean
    //   690	1639	4	j	int
    //   12	2286	5	localObject10	Object
    //   15	511	6	bool2	boolean
    //   738	1594	6	k	int
    //   18	2317	7	localObject11	Object
    //   21	2197	8	m	int
    //   24	2197	9	str	String
    //   27	2382	10	bool3	boolean
    //   30	2287	11	n	int
    //   49	2300	12	localObject12	Object
    //   52	2175	13	i1	int
    //   87	2220	14	bool4	boolean
    //   118	2123	15	l1	long
    //   125	2185	17	localObject13	Object
    //   130	1698	18	localObject14	Object
    //   137	273	19	localObject15	Object
    //   175	35	20	i2	int
    //   830	1343	20	bool5	boolean
    //   194	9	21	bool6	boolean
    //   197	120	22	bool7	boolean
    //   200	1707	23	localObject16	Object
    //   216	67	24	l2	long
    //   223	116	26	localObject17	Object
    //   263	2138	27	localObject18	Object
    //   287	1288	28	localObject19	Object
    //   407	21	29	l3	long
    //   429	1659	31	l4	long
    //   438	3	33	bool8	boolean
    //   452	1234	34	localObject20	Object
    //   468	7	35	localObject21	Object
    //   535	1283	36	localObject22	Object
    //   539	1761	37	localObject23	Object
    //   543	1813	38	localObject24	Object
    //   547	1738	39	localObject25	Object
    //   551	1275	40	localObject26	Object
    //   565	1422	41	localObject27	Object
    //   634	3	42	bool9	boolean
    //   715	537	43	i3	int
    //   1635	287	43	i4	int
    //   719	868	44	localObject28	Object
    //   807	3	45	bool10	boolean
    //   869	237	46	i5	int
    //   1294	874	47	l5	long
    //   1319	5	49	l6	long
    //   1628	5	51	l7	long
    //   1676	246	53	i6	int
    //   2000	7	53	i7	int
    //   2160	19	54	l8	long
    // Exception table:
    //   from	to	target	type
    //   307	314	364	finally
    //   321	324	364	finally
    //   326	331	364	finally
    //   333	338	364	finally
    //   340	348	364	finally
    //   795	800	1603	finally
    //   802	807	1603	finally
    //   825	830	1603	finally
    //   853	858	1603	finally
    //   922	927	1603	finally
    //   933	938	1603	finally
    //   945	950	1603	finally
    //   952	957	1603	finally
    //   966	971	1603	finally
    //   973	978	1603	finally
    //   987	992	1603	finally
    //   994	999	1603	finally
    //   1005	1010	1603	finally
    //   1032	1037	1603	finally
    //   1051	1056	1603	finally
    //   1068	1073	1603	finally
    //   1075	1080	1603	finally
    //   1089	1094	1603	finally
    //   1096	1101	1603	finally
    //   1105	1110	1603	finally
    //   1112	1117	1603	finally
    //   1123	1128	1603	finally
    //   1135	1140	1603	finally
    //   1142	1147	1603	finally
    //   1156	1161	1603	finally
    //   1163	1168	1603	finally
    //   1177	1182	1603	finally
    //   1184	1189	1603	finally
    //   1195	1200	1603	finally
    //   1207	1212	1603	finally
    //   1214	1219	1603	finally
    //   1228	1233	1603	finally
    //   1235	1240	1603	finally
    //   1254	1259	1603	finally
    //   1265	1270	1603	finally
    //   1282	1287	1603	finally
    //   1289	1294	1603	finally
    //   1303	1308	1603	finally
    //   2151	2156	2264	finally
    //   2156	2160	2264	finally
    //   2180	2186	2264	finally
    //   2231	2234	2264	finally
    //   2240	2245	2264	finally
    //   2249	2254	2264	finally
    //   2257	2262	2264	finally
    //   2262	2264	2264	finally
    //   1314	1319	2271	finally
    //   1323	1328	2271	finally
    //   1330	1335	2271	finally
    //   1341	1346	2271	finally
    //   1357	1361	2271	finally
    //   1372	1376	2271	finally
    //   1387	1391	2271	finally
    //   1402	1406	2271	finally
    //   1417	1421	2271	finally
    //   1432	1436	2271	finally
    //   1447	1451	2271	finally
    //   1462	1466	2271	finally
    //   1477	1481	2271	finally
    //   1492	1496	2271	finally
    //   1506	1510	2271	finally
    //   1521	1525	2271	finally
    //   1536	1540	2271	finally
    //   1551	1555	2271	finally
    //   1566	1570	2271	finally
    //   1642	1649	2271	finally
    //   1650	1654	2271	finally
    //   1655	1659	2271	finally
    //   1678	1683	2271	finally
    //   1685	1692	2271	finally
    //   1693	1697	2271	finally
    //   1707	1709	2271	finally
    //   1709	1712	2271	finally
    //   1713	1717	2271	finally
    //   1721	1726	2271	finally
    //   1731	1736	2271	finally
    //   1752	1758	2271	finally
    //   1769	1775	2271	finally
    //   1878	1883	2271	finally
    //   1885	1890	2271	finally
    //   1901	1904	2271	finally
    //   1910	1915	2271	finally
    //   1924	1927	2271	finally
    //   1928	1932	2271	finally
    //   1936	1941	2271	finally
    //   1946	1951	2271	finally
    //   1963	1968	2271	finally
    //   1970	1975	2271	finally
    //   1986	1989	2271	finally
    //   1995	2000	2271	finally
    //   2022	2025	2271	finally
    //   2026	2029	2271	finally
    //   2036	2041	2271	finally
    //   2042	2048	2271	finally
    //   2054	2060	2271	finally
    //   2061	2067	2271	finally
    //   2067	2071	2271	finally
    //   2073	2077	2271	finally
    //   2077	2079	2271	finally
    //   2079	2082	2271	finally
    //   2087	2092	2271	finally
    //   2095	2100	2271	finally
    //   2102	2106	2271	finally
    //   2106	2108	2271	finally
    //   2135	2138	2271	finally
    //   2140	2144	2271	finally
    //   2186	2190	2271	finally
    //   2265	2269	2271	finally
    //   2269	2271	2271	finally
    //   168	175	2275	finally
    //   177	180	2275	finally
    //   182	187	2275	finally
    //   187	194	2275	finally
    //   209	216	2275	finally
    //   218	223	2275	finally
    //   227	234	2275	finally
    //   236	241	2275	finally
    //   248	251	2275	finally
    //   253	258	2275	finally
    //   258	263	2275	finally
    //   269	277	2275	finally
    //   282	287	2275	finally
    //   291	296	2275	finally
    //   301	305	2275	finally
    //   351	358	2275	finally
    //   365	372	2275	finally
    //   372	374	2275	finally
    //   374	381	2275	finally
    //   388	391	2275	finally
    //   393	398	2275	finally
    //   400	405	2275	finally
    //   409	416	2275	finally
    //   418	425	2275	finally
    //   431	438	2275	finally
    //   445	452	2275	finally
    //   454	459	2275	finally
    //   461	468	2275	finally
    //   474	479	2275	finally
    //   483	488	2275	finally
    //   500	507	2275	finally
    //   509	514	2275	finally
    //   518	523	2275	finally
    //   556	559	2275	finally
    //   561	565	2275	finally
    //   569	574	2275	finally
    //   576	582	2275	finally
    //   582	589	2275	finally
    //   591	596	2275	finally
    //   600	606	2275	finally
    //   606	611	2275	finally
    //   613	618	2275	finally
    //   620	625	2275	finally
    //   627	634	2275	finally
    //   641	648	2275	finally
    //   650	655	2275	finally
    //   657	660	2275	finally
    //   662	667	2275	finally
    //   678	683	2275	finally
    //   685	690	2275	finally
    //   703	708	2275	finally
    //   710	715	2275	finally
    //   721	725	2275	finally
    //   731	738	2275	finally
    //   777	782	2275	finally
    //   2009	2016	2293	java/lang/RuntimeException
    //   2190	2197	2293	java/lang/RuntimeException
    //   2284	2291	2293	java/lang/RuntimeException
    //   2291	2293	2293	java/lang/RuntimeException
    //   80	87	2339	finally
    //   100	107	2339	finally
    //   111	118	2339	finally
    //   120	125	2339	finally
    //   132	137	2339	finally
    //   142	145	2339	finally
    //   149	152	2339	finally
    //   155	160	2339	finally
    //   80	87	2347	java/lang/RuntimeException
    //   100	107	2347	java/lang/RuntimeException
    //   111	118	2347	java/lang/RuntimeException
    //   120	125	2347	java/lang/RuntimeException
    //   132	137	2347	java/lang/RuntimeException
    //   142	145	2347	java/lang/RuntimeException
    //   149	152	2347	java/lang/RuntimeException
    //   155	160	2347	java/lang/RuntimeException
    //   2009	2016	2354	finally
    //   2190	2197	2354	finally
    //   2284	2291	2354	finally
    //   2291	2293	2354	finally
    //   2352	2354	2354	finally
    //   2382	2387	2398	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */