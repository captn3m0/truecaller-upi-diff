package com.truecaller.content;

import android.content.ContentResolver;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Handler.Callback;
import android.os.Message;

final class TruecallerContentProvider$a
  implements Handler.Callback
{
  private TruecallerContentProvider$a(TruecallerContentProvider paramTruecallerContentProvider) {}
  
  public final boolean handleMessage(Message paramMessage)
  {
    Object localObject1 = a.c();
    ((SQLiteDatabase)localObject1).beginTransaction();
    int i = 1;
    try
    {
      int j = what;
      Object localObject2;
      int k;
      if (j == i)
      {
        paramMessage = a;
        paramMessage = TruecallerContentProvider.a(paramMessage);
        localObject2 = a;
        localObject2 = ((TruecallerContentProvider)localObject2).c();
        boolean bool1 = paramMessage.a((SQLiteDatabase)localObject2);
        if (bool1)
        {
          ((SQLiteDatabase)localObject1).setTransactionSuccessful();
          bool1 = true;
          break label134;
        }
      }
      else
      {
        int m = what;
        k = 2;
        if (m == k)
        {
          paramMessage = a;
          TruecallerContentProvider.a(paramMessage);
          paramMessage = a;
          paramMessage = paramMessage.c();
          bool2 = t.b(paramMessage);
          if (bool2)
          {
            ((SQLiteDatabase)localObject1).setTransactionSuccessful();
            bool2 = true;
            break label134;
          }
        }
      }
      boolean bool2 = false;
      paramMessage = null;
      label134:
      ((SQLiteDatabase)localObject1).endTransaction();
      if (bool2)
      {
        paramMessage = a.getContext().getContentResolver();
        localObject1 = TruecallerContract.b;
        k = 0;
        localObject2 = null;
        paramMessage.notifyChange((Uri)localObject1, null);
      }
    }
    finally
    {
      ((SQLiteDatabase)localObject1).endTransaction();
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContentProvider.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */