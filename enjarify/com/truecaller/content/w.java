package com.truecaller.content;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import com.truecaller.common.c.a.a.a;

final class w
  implements a.a
{
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String paramString, String[] paramArrayOfString, int paramInt)
  {
    parama1 = parama.c();
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      paramUri = new java/lang/StringBuilder;
      str = "normalized_destination IN (SELECT value FROM topspammers WHERE (";
      paramUri.<init>(str);
      paramUri.append(paramString);
      paramString = " COLLATE NOCASE))";
      paramUri.append(paramString);
      paramUri = paramUri.toString();
    }
    else
    {
      bool = false;
      paramUri = null;
    }
    paramString = new android/content/ContentValues;
    int i = 1;
    paramString.<init>(i);
    Object localObject = Boolean.FALSE;
    paramString.put("is_top_spammer", (Boolean)localObject);
    localObject = Integer.valueOf(0);
    paramString.put("top_spam_score", (Integer)localObject);
    String str = "msg_participants";
    int j = parama1.update(str, paramString, paramUri, paramArrayOfString);
    if (j > 0)
    {
      parama1 = TruecallerContract.ae.a();
      parama.a(parama1);
    }
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */