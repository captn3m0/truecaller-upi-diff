package com.truecaller.content;

import android.net.Uri;
import android.net.Uri.Builder;

public final class TruecallerContract$ad
{
  public static Uri a(boolean paramBoolean, String paramString)
  {
    paramString = TruecallerContract.b.buildUpon().appendEncodedPath("new_conversation_items").appendQueryParameter("query_type", "forward").appendQueryParameter("filter", paramString);
    String str = String.valueOf(paramBoolean);
    return paramString.appendQueryParameter("im_enabled", str).appendQueryParameter("exclude_im_group_roles", "2").build();
  }
  
  public static Uri a(boolean paramBoolean, String paramString1, String paramString2)
  {
    paramString1 = TruecallerContract.b.buildUpon().appendEncodedPath("new_conversation_items").appendQueryParameter("query_type", "new_conversation").appendQueryParameter("filter", paramString1);
    String str = String.valueOf(paramBoolean);
    return paramString1.appendQueryParameter("im_enabled", str).appendQueryParameter("conversation_type", paramString2).build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */