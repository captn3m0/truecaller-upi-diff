package com.truecaller.content.c;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import c.a.y;
import c.g.b.k;
import c.n;
import c.t;
import c.u;
import com.truecaller.common.c.a.a.g;
import com.truecaller.utils.extensions.o;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class p
  implements a.g
{
  private final List a;
  private final boolean b;
  
  public p(boolean paramBoolean)
  {
    b = paramBoolean;
    Object localObject = new Character[5];
    Character localCharacter = Character.valueOf('(');
    localObject[0] = localCharacter;
    localCharacter = Character.valueOf(')');
    localObject[1] = localCharacter;
    localCharacter = Character.valueOf('-');
    localObject[2] = localCharacter;
    localCharacter = Character.valueOf(' ');
    localObject[3] = localCharacter;
    localCharacter = Character.valueOf('+');
    localObject[4] = localCharacter;
    localObject = c.a.m.b((Object[])localObject);
    a = ((List)localObject);
  }
  
  private final String a(String paramString)
  {
    paramString = (CharSequence)paramString;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = (Appendable)localObject;
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      List localList = a;
      Character localCharacter = Character.valueOf(j);
      boolean bool = localList.contains(localCharacter);
      if (!bool) {
        ((Appendable)localObject).append(j);
      }
      i += 1;
    }
    paramString = ((StringBuilder)localObject).toString();
    k.a(paramString, "filterNotTo(StringBuilder(), predicate).toString()");
    return paramString;
  }
  
  private final boolean b(String paramString)
  {
    paramString = (CharSequence)a(paramString);
    int i = c.n.m.a(paramString);
    int k = 1;
    i ^= k;
    if (i != 0)
    {
      i = 0;
      for (;;)
      {
        int m = paramString.length();
        if (i >= m) {
          break;
        }
        int n = paramString.charAt(i);
        int i1 = 57;
        int i2 = 48;
        if ((i2 <= n) && (i1 >= n)) {
          n = 1;
        } else {
          n = 0;
        }
        if (n == 0)
        {
          i3 = 0;
          paramString = null;
          break label106;
        }
        int j;
        i += 1;
      }
      int i3 = 1;
      label106:
      if (i3 != 0) {
        return k;
      }
    }
    return false;
  }
  
  public final Cursor a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    p localp = this;
    Object localObject1 = paramUri;
    Object localObject2 = parama;
    k.b(parama, "provider");
    Object localObject3 = parama1;
    k.b(parama1, "helper");
    k.b(paramUri, "uri");
    Object localObject4 = paramUri.getQueryParameter("filter");
    localObject3 = localObject4;
    localObject3 = (CharSequence)localObject4;
    int i = 1;
    if (localObject3 != null)
    {
      bool1 = c.n.m.a((CharSequence)localObject3);
      if (!bool1)
      {
        bool1 = false;
        localObject3 = null;
        break label84;
      }
    }
    boolean bool1 = true;
    label84:
    Object localObject5 = null;
    if (bool1) {
      localObject4 = null;
    }
    if (localObject4 == null) {
      return null;
    }
    localObject1 = ((Uri)localObject1).getQueryParameter("limit");
    localObject3 = localObject1;
    localObject3 = (CharSequence)localObject1;
    if (localObject3 != null)
    {
      bool1 = c.n.m.a((CharSequence)localObject3);
      if (!bool1)
      {
        bool1 = false;
        localObject3 = null;
        break label157;
      }
    }
    bool1 = true;
    label157:
    if (bool1) {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject3 = " LIMIT ";
      localObject1 = String.valueOf(localObject1);
      localObject1 = ((String)localObject3).concat((String)localObject1);
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = "";
    }
    bool1 = b;
    char c1 = '%';
    int k = 2;
    int n;
    Object localObject6;
    int i1;
    char c3;
    Object localObject7;
    label323:
    Object localObject8;
    if (bool1)
    {
      localObject4 = (CharSequence)localObject4;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = (Appendable)localObject3;
      int m = ((CharSequence)localObject4).length();
      n = 0;
      localObject6 = null;
      while (n < m)
      {
        char c2 = ((CharSequence)localObject4).charAt(n);
        i1 = 57;
        c3 = '0';
        if ((c3 > c2) || (i1 < c2))
        {
          i1 = 43;
          if (c2 != i1) {}
        }
        else
        {
          i1 = 1;
          break label323;
        }
        i1 = 0;
        localObject7 = null;
        if (i1 != 0) {
          ((Appendable)localObject3).append(c2);
        }
        n += 1;
      }
      localObject4 = ((StringBuilder)localObject3).toString();
      k.a(localObject4, "filterTo(StringBuilder(), predicate).toString()");
      localObject3 = localObject4;
      localObject3 = (CharSequence)localObject4;
      int j = ((CharSequence)localObject3).length();
      if (j == 0)
      {
        j = 1;
      }
      else
      {
        j = 0;
        localObject3 = null;
      }
      if (j != 0) {
        localObject4 = null;
      }
      if (localObject4 == null) {
        return null;
      }
      localObject3 = "WHERE t9_starts_with LIKE ? OR t9_anywhere LIKE ?";
      localObject5 = new String[k];
      localObject8 = new java/lang/StringBuilder;
      ((StringBuilder)localObject8).<init>();
      ((StringBuilder)localObject8).append((String)localObject4);
      ((StringBuilder)localObject8).append(c1);
      localObject8 = ((StringBuilder)localObject8).toString();
      localObject5[0] = localObject8;
      localObject8 = new java/lang/StringBuilder;
      localObject6 = "%";
      ((StringBuilder)localObject8).<init>((String)localObject6);
      ((StringBuilder)localObject8).append((String)localObject4);
      ((StringBuilder)localObject8).append(c1);
      localObject4 = ((StringBuilder)localObject8).toString();
      localObject5[i] = localObject4;
      localObject4 = c.a.m.b((Object[])localObject5);
      localObject4 = t.a(localObject3, localObject4);
    }
    else
    {
      boolean bool2 = localp.b((String)localObject4);
      if (bool2)
      {
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>("%");
        localObject8 = localp.a((String)localObject4);
        ((StringBuilder)localObject5).append((String)localObject8);
        ((StringBuilder)localObject5).append(c1);
        localObject5 = c.a.m.a(((StringBuilder)localObject5).toString());
        localObject3 = t.a("t9_anywhere LIKE ?", localObject5);
      }
      else
      {
        localObject3 = (List)y.a;
        localObject3 = t.a(null, localObject3);
      }
      localObject5 = (String)a;
      localObject3 = (List)b;
      localObject8 = q.a();
      n = localObject8.length;
      localObject8 = Arrays.copyOf((char[])localObject8, n);
      localObject4 = (CharSequence)o.a((String)localObject4, (char[])localObject8);
      localObject8 = new char[i];
      localObject8[0] = 32;
      localObject4 = c.a.m.k((Iterable)c.n.m.a((CharSequence)localObject4, (char[])localObject8, 0, 6));
      localObject8 = new java/util/ArrayList;
      ((ArrayList)localObject8).<init>(k);
      n = 0;
      localObject6 = null;
      while (n < k)
      {
        localObject9 = "matched_value LIKE ? ESCAPE '\\'";
        ((ArrayList)localObject8).add(localObject9);
        n += 1;
      }
      localObject8 = (List)localObject8;
      localObject6 = localObject8;
      localObject6 = (Iterable)localObject8;
      Object localObject9 = (CharSequence)" OR ";
      c3 = '\000';
      Object localObject10 = null;
      char c4 = '\000';
      String str1 = null;
      int i3 = 62;
      localObject8 = c.a.m.a((Iterable)localObject6, (CharSequence)localObject9, null, null, 0, null, null, i3);
      n = ((List)localObject4).size();
      localObject9 = new java/util/ArrayList;
      ((ArrayList)localObject9).<init>(n);
      i1 = 0;
      localObject7 = null;
      while (i1 < n)
      {
        localObject10 = new java/lang/StringBuilder;
        str1 = "(";
        ((StringBuilder)localObject10).<init>(str1);
        ((StringBuilder)localObject10).append((String)localObject8);
        c4 = ')';
        ((StringBuilder)localObject10).append(c4);
        localObject10 = ((StringBuilder)localObject10).toString();
        ((ArrayList)localObject9).add(localObject10);
        int i2;
        i1 += 1;
      }
      localObject9 = (List)localObject9;
      localObject7 = localObject9;
      localObject7 = (Iterable)localObject9;
      localObject10 = (CharSequence)" AND ";
      c4 = '\000';
      str1 = null;
      i3 = 0;
      int i4 = 0;
      int i5 = 62;
      localObject8 = c.a.m.a((Iterable)localObject7, (CharSequence)localObject10, null, null, 0, null, null, i5);
      localObject4 = (Iterable)localObject4;
      localObject6 = new java/util/ArrayList;
      ((ArrayList)localObject6).<init>();
      localObject6 = (Collection)localObject6;
      localObject4 = ((Iterable)localObject4).iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject4).hasNext();
        if (!bool3) {
          break;
        }
        localObject9 = (String)((Iterator)localObject4).next();
        localObject7 = new String[k];
        localObject10 = new java/lang/StringBuilder;
        ((StringBuilder)localObject10).<init>();
        ((StringBuilder)localObject10).append((String)localObject9);
        ((StringBuilder)localObject10).append(c1);
        localObject10 = ((StringBuilder)localObject10).toString();
        localObject7[0] = localObject10;
        localObject10 = new java/lang/StringBuilder;
        str1 = "% ";
        ((StringBuilder)localObject10).<init>(str1);
        ((StringBuilder)localObject10).append((String)localObject9);
        ((StringBuilder)localObject10).append(c1);
        localObject9 = ((StringBuilder)localObject10).toString();
        localObject7[i] = localObject9;
        localObject9 = (Iterable)c.a.m.b((Object[])localObject7);
        c.a.m.a((Collection)localObject6, (Iterable)localObject9);
      }
      localObject6 = (List)localObject6;
      localObject4 = new String[k];
      localObject4[0] = localObject5;
      localObject4[i] = localObject8;
      localObject4 = c.a.m.e((Object[])localObject4);
      localObject9 = localObject4;
      localObject9 = (Iterable)localObject4;
      localObject7 = (CharSequence)" OR ";
      localObject10 = (CharSequence)"WHERE ";
      c4 = '\000';
      str1 = null;
      i3 = 0;
      i4 = 60;
      localObject4 = c.a.m.a((Iterable)localObject9, (CharSequence)localObject7, (CharSequence)localObject10, null, 0, null, null, i4);
      localObject3 = (Collection)localObject3;
      localObject6 = (Iterable)localObject6;
      localObject3 = c.a.m.c((Collection)localObject3, (Iterable)localObject6);
      localObject4 = t.a(localObject4, localObject3);
    }
    localObject3 = (String)a;
    localObject4 = (List)b;
    localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>("\n            SELECT m.matched_value, a.*\n            FROM (\n                SELECT hit.matched_key, hit.aggregated_contact_id\n                FROM (\n                    SELECT m.rowid as matched_key, MAX(hit_priority) as max_hit_priority, aggregated_contact_id\n                    FROM t9_mapping m\n                    INNER JOIN raw_contact r on m.raw_contact_id = r._id\n                    ");
    ((StringBuilder)localObject5).append((String)localObject3);
    ((StringBuilder)localObject5).append("\n                    GROUP BY aggregated_contact_id\n                ) hit\n                LEFT JOIN contacts_with_call_count most_called on hit.aggregated_contact_id = most_called.aggregated_contact_id\n                ORDER BY hit.max_hit_priority DESC, most_called.calls_count DESC, most_called.recent_call_timestamp DESC ");
    ((StringBuilder)localObject5).append((String)localObject1);
    ((StringBuilder)localObject5).append("\n            ) limited\n            LEFT JOIN t9_mapping m on limited.matched_key = m.rowid\n            LEFT JOIN aggregated_contact a on limited.aggregated_contact_id = a._id\n            ");
    localObject1 = ((StringBuilder)localObject5).toString();
    localObject3 = new String[k];
    localObject3[0] = "OnFilterContactData";
    String str2 = String.valueOf(localObject1);
    localObject5 = "sql = ".concat(str2);
    localObject3[i] = localObject5;
    localObject3 = new String[k];
    localObject3[0] = "OnFilterContactData";
    str2 = String.valueOf(localObject4);
    localObject5 = "args = ".concat(str2);
    localObject3[i] = localObject5;
    localObject2 = parama.c();
    localObject4 = (Collection)localObject4;
    if (localObject4 != null)
    {
      localObject3 = new String[0];
      localObject4 = ((Collection)localObject4).toArray((Object[])localObject3);
      if (localObject4 != null)
      {
        localObject4 = (String[])localObject4;
        localObject3 = paramCancellationSignal;
        return ((SQLiteDatabase)localObject2).rawQuery((String)localObject1, (String[])localObject4, paramCancellationSignal);
      }
      localObject1 = new c/u;
      ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw ((Throwable)localObject1);
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */