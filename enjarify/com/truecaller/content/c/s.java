package com.truecaller.content.c;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import c.a.f;
import c.a.m;
import c.g.b.k;
import c.g.b.z;
import c.u;
import com.truecaller.common.c.a.a.g;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public final class s
  implements a.g
{
  private final Context a;
  
  public s(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Cursor a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    Object localObject1 = paramString1;
    Object localObject2 = paramArrayOfString2;
    Object localObject3 = parama;
    k.b(parama, "provider");
    k.b(parama1, "helper");
    Object localObject4 = paramUri;
    k.b(paramUri, "uri");
    Object localObject5 = t.a(paramUri);
    localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>();
    Object localObject6 = new java/util/ArrayList;
    ((ArrayList)localObject6).<init>();
    localObject6 = (List)localObject6;
    int i = -1;
    Object localObject7 = Integer.valueOf(i);
    boolean bool2 = k.a(localObject5, localObject7);
    boolean bool3 = true;
    bool2 ^= bool3;
    char c = ')';
    if ((bool2) || (paramString1 != null))
    {
      localObject7 = " WHERE ";
      ((StringBuilder)localObject4).append((String)localObject7);
      localObject8 = Integer.valueOf(i);
      boolean bool1 = k.a(localObject5, localObject8) ^ bool3;
      if (bool1)
      {
        localObject8 = "conversation_id = ?";
        ((StringBuilder)localObject4).append((String)localObject8);
        localObject5 = localObject5.toString();
        ((List)localObject6).add(localObject5);
        if (localObject1 != null)
        {
          localObject5 = " AND ";
          ((StringBuilder)localObject4).append((String)localObject5);
        }
      }
      if (localObject1 != null)
      {
        localObject5 = new java/lang/StringBuilder;
        localObject8 = "(";
        ((StringBuilder)localObject5).<init>((String)localObject8);
        ((StringBuilder)localObject5).append((String)localObject1);
        ((StringBuilder)localObject5).append(c);
        localObject1 = ((StringBuilder)localObject5).toString();
        ((StringBuilder)localObject4).append((String)localObject1);
        if (localObject2 != null)
        {
          localObject1 = localObject6;
          localObject1 = (Collection)localObject6;
          m.a((Collection)localObject1, (Object[])localObject2);
        }
      }
    }
    if (paramString2 != null)
    {
      localObject2 = String.valueOf(paramString2);
      localObject1 = " ORDER BY ".concat((String)localObject2);
    }
    else
    {
      localObject1 = "";
    }
    localObject2 = z.a;
    int j = 2;
    Object localObject8 = new Object[j];
    localObject7 = new java/lang/StringBuilder;
    ((StringBuilder)localObject7).<init>();
    localObject4 = ((StringBuilder)localObject4).toString();
    ((StringBuilder)localObject7).append((String)localObject4);
    ((StringBuilder)localObject7).append((String)localObject1);
    localObject4 = ((StringBuilder)localObject7).toString();
    bool2 = false;
    localObject7 = null;
    localObject8[0] = localObject4;
    localObject8[bool3] = localObject1;
    localObject1 = Arrays.copyOf((Object[])localObject8, j);
    localObject1 = String.format("SELECT m.*, p._id AS participant_id, p.type AS me_participant_type, p.tc_im_peer_id AS me_participant_im_id, p.raw_destination AS me_participant_raw_destination, p.normalized_destination AS me_participant_normalized_destination, p.country_code AS me_participant_country_code, p.aggregated_contact_id AS me_participant_aggregated_contact_id, p.filter_action AS me_participant_filter_action, p.is_top_spammer AS me_participant_is_top_spammer, p.top_spam_score AS me_participant_top_spam_score, IFNULL(a.contact_name, ig.title) AS me_participant_name, IFNULL(a.contact_image_url, ig.avatar) AS me_participant_image_url, a.contact_source AS me_participant_source, IFNULL(a.contact_badges, 0) AS me_participant_badges, a.contact_company AS me_participant_company_name, a.contact_search_time AS me_participant_search_time, IFNULL(a.contact_phonebook_id, -1) AS me_participant_phonebook_id, a.tc_id AS me_participant_tc_id, MAX(IFNULL(a.contact_spam_score, 0), p.top_spam_score) AS me_participant_spam_score, GROUP_CONCAT(e._id, '|') AS me_entities_id, GROUP_CONCAT(LENGTH(e.type) || '|' || e.type, \"\") AS me_entities_type, GROUP_CONCAT(LENGTH(e.content) || '|' || e.content, \"\") AS me_entities_content, GROUP_CONCAT(LENGTH(e.thumbnail) || '|' || e.thumbnail, \"\") AS me_entities_thumnail, GROUP_CONCAT(e.width, '|') AS me_entities_width, GROUP_CONCAT(e.height, '|') AS me_entities_height, GROUP_CONCAT(e.duration, '|') AS me_entities_duration, GROUP_CONCAT(e.status, '|') AS me_entities_status, GROUP_CONCAT(e.size, '|') AS me_entities_size, CASE WHEN m.transport = 0 THEN si.raw_id WHEN m.transport = 1 THEN mi.raw_id WHEN m.transport = 2 THEN ii.raw_id WHEN m.transport = 5 THEN hi.raw_id WHEN m.transport = 6 THEN sti.raw_id ELSE NULL END AS me_raw_id, CASE WHEN m.transport = 0 THEN si.raw_status WHEN m.transport = 1 THEN mi.raw_status ELSE NULL END AS me_raw_status, CASE WHEN m.transport = 0 THEN si.raw_thread_id WHEN m.transport = 1 THEN mi.raw_thread_id ELSE -1 END AS me_raw_thread, CASE WHEN m.transport = 0 THEN si.message_uri WHEN m.transport = 1 THEN mi.message_uri ELSE NULL END AS me_message_uri, si.protocol AS me_sms_protocol, si.type AS me_sms_type, si.service_center AS me_sms_service_center, si.subject AS me_sms_subject, si.error_code AS me_sms_error_code, si.reply_path_present AS me_sms_reply_path_present, si.stripped_raw_address AS me_sms_stripped_raw_address, mi.mms_message_id AS me_mms_message_id, mi.transaction_id AS me_mms_transaction_id, mi.subject AS me_mms_subject, mi.subject_charset AS me_mms_subject_charset, mi.content_location AS me_mms_content_location, mi.expiry AS me_mms_expiry, mi.priority AS me_mms_priority, mi.size AS me_mms_size, mi.message_box AS me_mms_message_box, mi.retrieve_status AS me_mms_retrieve_status, mi.response_status AS me_mms_response_status, mi.type AS me_mms_type, mi.delivery_report AS me_mms_delivery_report, mi.delivery_time AS me_mms_delivery_time, mi.version AS me_mms_version, mi.retrieve_text AS me_mms_retrieve_text, mi.retrieve_text_charset AS me_mms_retrieve_text_charset, mi.content_type AS me_mms_content_type, mi.content_class AS me_mms_content_class, mi.response_text AS me_mms_response_text, mi.message_class AS me_mms_message_class, mi.read_report AS me_mms_read_report, mi.read_status AS me_mms_read_status, mi.report_allowed AS me_mms_report_allowed, ii.im_status AS me_im_status, ii.delivery_status AS me_im_delivery_status, ii.read_status AS me_im_read_status, ii.read_sync_status AS me_im_read_sync_status, ii.error_code AS im_error_code, ii.api_version AS im_api_version, ii.random_id AS im_random_id, ii.reactions AS im_reactions, hi.type AS h_type, hi.number_type AS h_number_type, hi.features AS h_features, hi.call_log_id AS h_call_log_id, rm.status AS re_message_status, ra.contact_name AS re_participant_name, rp.normalized_destination AS re_participant_normalized_address, GROUP_CONCAT(re._id, '|') AS re_entities_id, GROUP_CONCAT(LENGTH(re.type) || '|' || re.type, \"\") AS re_entities_type, GROUP_CONCAT(LENGTH(re.content) || '|' || re.content, \"\") AS re_entities_content, GROUP_CONCAT(LENGTH(re.thumbnail) || '|' || re.thumbnail, \"\") AS re_entities_thumbnail FROM (SELECT * FROM msg_messages%s) m JOIN msg_entities e ON m._id = e.message_id LEFT JOIN msg_participants p ON p._id = m.participant_id LEFT JOIN aggregated_contact a ON p.aggregated_contact_id = a._id LEFT JOIN msg_sms_transport_info si ON m.transport = 0 AND si.message_id = m._id LEFT JOIN msg_mms_transport_info mi ON m.transport = 1 AND mi.message_id = m._id LEFT JOIN msg_im_transport_info ii ON m.transport = 2 AND ii.message_id = m._id LEFT JOIN msg_history_transport_info hi ON m.transport = 5 AND hi.message_id = m._id LEFT JOIN msg_status_transport_info sti ON m.transport = 6 AND sti.message_id = m._id LEFT JOIN msg_messages rm ON rm._id = m.reply_to_msg_id LEFT JOIN msg_entities re ON rm._id = re.message_id LEFT JOIN msg_participants rp ON rp._id = rm.participant_id LEFT JOIN aggregated_contact ra ON ra._id = rp.aggregated_contact_id LEFT JOIN msg_im_group_info ig ON ig.im_group_id = p.normalized_destination AND p.type = 4 GROUP BY m._id%s", (Object[])localObject1);
    localObject2 = "java.lang.String.format(format, *args)";
    k.a(localObject1, (String)localObject2);
    if (paramArrayOfString1 != null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("SELECT ");
      int k = 63;
      localObject5 = f.a(paramArrayOfString1, null, null, null, 0, null, k);
      ((StringBuilder)localObject2).append((String)localObject5);
      localObject5 = " FROM (";
      ((StringBuilder)localObject2).append((String)localObject5);
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(c);
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    localObject2 = parama.c();
    localObject6 = (Collection)localObject6;
    localObject5 = new String[0];
    localObject5 = ((Collection)localObject6).toArray((Object[])localObject5);
    if (localObject5 != null)
    {
      localObject5 = (String[])localObject5;
      localObject1 = ((SQLiteDatabase)localObject2).rawQuery((String)localObject1, (String[])localObject5);
      if (localObject1 != null)
      {
        localObject2 = this;
        localObject5 = a.getContentResolver();
        localObject3 = parama1.b();
        ((Cursor)localObject1).setNotificationUri((ContentResolver)localObject5, (Uri)localObject3);
      }
      else
      {
        localObject2 = this;
      }
      k.a(localObject1, "cursor");
      return (Cursor)localObject1;
    }
    localObject2 = this;
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */