package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public final class k
  implements ab
{
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    int i = 27;
    if ((paramInt1 < i) && (paramSQLiteDatabase != null))
    {
      paramSQLiteDatabase.execSQL("CREATE TABLE msg_im_attachments (\n    _id INTEGER PRIMARY KEY AUTOINCREMENT,\n    entity_id INTEGER NOT NULL REFERENCES msg_entities (_id) ON DELETE CASCADE,\n    uri TEXT NOT NULL\n)");
      return;
    }
  }
  
  public final String[] a()
  {
    return new String[] { "CREATE TABLE msg_im_attachments (\n    _id INTEGER PRIMARY KEY AUTOINCREMENT,\n    entity_id INTEGER NOT NULL REFERENCES msg_entities (_id) ON DELETE CASCADE,\n    uri TEXT NOT NULL\n)" };
  }
  
  public final String[] b()
  {
    return new String[] { "CREATE VIEW msg_im_attachments_entities AS SELECT\n  a._id as _id,\n  e._id as entity_id,\n  a.uri as source_uri,\n  e.content as current_uri,\n  e.status as status,\n  e.size as size\nFROM msg_im_attachments a INNER JOIN msg_entities e\nON e._id = a.entity_id\n" };
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */