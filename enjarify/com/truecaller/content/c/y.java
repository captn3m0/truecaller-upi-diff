package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.truecaller.common.c.b.b;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.content.TruecallerContract.j;

public final class y
  implements ab
{
  private static final String a;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CREATE VIEW raw_contact_data AS SELECT raw_contact._id AS _id, raw_contact.tc_id AS tc_id, raw_contact.tc_flag AS tc_flag, raw_contact.insert_timestamp AS insert_timestamp, raw_contact.aggregated_contact_id AS aggregated_contact_id, raw_contact.search_query AS search_query, raw_contact.cache_control AS cache_control, ");
    String[] arrayOfString = TruecallerContract.ah.a;
    String str = b.a("raw_contact", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(", data._id AS data_id, data.tc_id AS data_tc_id, ");
    arrayOfString = TruecallerContract.j.a;
    str = b.a("data", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(" FROM raw_contact LEFT JOIN data ON raw_contact._id=data.data_raw_contact_id");
    a = localStringBuilder.toString();
  }
  
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    while (paramInt1 < paramInt2)
    {
      int i = 2;
      if (paramInt1 == i)
      {
        paramContext = "ALTER TABLE raw_contact ADD COLUMN contact_phonebook_lookup TEXT";
        paramSQLiteDatabase.execSQL(paramContext);
      }
      i = 4;
      if (paramInt1 == i)
      {
        paramContext = "ALTER TABLE raw_contact ADD COLUMN search_query TEXT";
        paramSQLiteDatabase.execSQL(paramContext);
      }
      i = 7;
      if (paramInt1 == i)
      {
        paramContext = "ALTER TABLE raw_contact ADD COLUMN contact_transliterated_name TEXT";
        paramSQLiteDatabase.execSQL(paramContext);
      }
      i = 8;
      if (paramInt1 == i)
      {
        paramContext = "ALTER TABLE raw_contact ADD COLUMN cache_control TEXT";
        paramSQLiteDatabase.execSQL(paramContext);
      }
      i = 10;
      if (paramInt1 == i)
      {
        paramContext = "raw_contact";
        String str = "contact_is_favorite";
        boolean bool = b.b(paramSQLiteDatabase, paramContext, str);
        if (!bool)
        {
          paramSQLiteDatabase.execSQL("ALTER TABLE raw_contact ADD COLUMN contact_is_favorite INT");
          paramContext = "ALTER TABLE raw_contact ADD COLUMN contact_favorite_position INT";
          paramSQLiteDatabase.execSQL(paramContext);
        }
      }
      int j = 11;
      if (paramInt1 == j)
      {
        paramContext = "ALTER TABLE raw_contact ADD COLUMN contact_spam_score INT";
        paramSQLiteDatabase.execSQL(paramContext);
      }
      j = 14;
      if (paramInt1 == j)
      {
        paramContext = "ALTER TABLE raw_contact ADD COLUMN contact_badges INT DEFAULT 0";
        paramSQLiteDatabase.execSQL(paramContext);
      }
      j = 25;
      if (paramInt1 == j)
      {
        paramContext = "ALTER TABLE raw_contact ADD COLUMN tc_flag INT NOT NULL DEFAULT 0";
        paramSQLiteDatabase.execSQL(paramContext);
      }
      j = 35;
      if (paramInt1 == j)
      {
        paramSQLiteDatabase.execSQL("ALTER TABLE raw_contact ADD COLUMN insert_timestamp INT NOT NULL DEFAULT 0");
        paramContext = "UPDATE raw_contact SET insert_timestamp = strftime('%s','now')";
        paramSQLiteDatabase.execSQL(paramContext);
      }
      paramInt1 += 1;
    }
  }
  
  public final String[] a()
  {
    String[] arrayOfString1 = new String[5];
    arrayOfString1[0] = "CREATE TABLE raw_contact(_id INTEGER PRIMARY KEY, aggregated_contact_id INTEGER REFERENCES aggregated_contact(_id) ON DELETE SET NULL ON UPDATE CASCADE, tc_id TEXT UNIQUE NOT NULL, contact_name TEXT, contact_transliterated_name TEXT, contact_is_favorite INT, contact_favorite_position INT, contact_handle TEXT, contact_alt_name TEXT, contact_gender TEXT, contact_about TEXT, contact_image_url TEXT, contact_job_title TEXT, contact_company TEXT, contact_access TEXT, contact_common_connections INT, contact_search_time INT, contact_source INT, contact_default_number TEXT, contact_phonebook_id INT, contact_phonebook_hash INT, contact_phonebook_lookup TEXT,search_query TEXT,cache_control TEXT,contact_spam_score INT,contact_badges INT DEFAULT 0, tc_flag INT NOT NULL DEFAULT 0, insert_timestamp INT NOT NULL DEFAULT 0, contact_im_id TEXT, settings_flag INT NOT NULL DEFAULT 0);";
    String[] arrayOfString2 = { "aggregated_contact_id" };
    String str = b.b("raw_contact", arrayOfString2);
    arrayOfString1[1] = str;
    arrayOfString2 = new String[] { "tc_id" };
    str = b.b("raw_contact", arrayOfString2);
    arrayOfString1[2] = str;
    arrayOfString2 = new String[] { "contact_phonebook_id" };
    str = b.b("raw_contact", arrayOfString2);
    arrayOfString1[3] = str;
    arrayOfString2 = new String[] { "contact_im_id" };
    str = b.b("raw_contact", arrayOfString2);
    arrayOfString1[4] = str;
    return arrayOfString1;
  }
  
  public final String[] b()
  {
    String[] arrayOfString = new String[2];
    String str = a;
    arrayOfString[0] = str;
    arrayOfString[1] = "CREATE TRIGGER IF NOT EXISTS insert_timestamp_trigger AFTER INSERT ON raw_contact BEGIN  UPDATE raw_contact SET insert_timestamp=strftime('%s','now') WHERE _id=NEW._id; END;";
    return arrayOfString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */