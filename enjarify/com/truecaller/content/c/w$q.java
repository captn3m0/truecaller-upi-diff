package com.truecaller.content.c;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

final class w$q
  implements BlockingQueue
{
  private final LinkedBlockingQueue a;
  private final HashMap b;
  
  private w$q()
  {
    Object localObject = new java/util/concurrent/LinkedBlockingQueue;
    ((LinkedBlockingQueue)localObject).<init>();
    a = ((LinkedBlockingQueue)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = ((HashMap)localObject);
  }
  
  private Runnable a(long paramLong, TimeUnit arg3)
  {
    Object localObject1 = a;
    Runnable localRunnable1 = (Runnable)((LinkedBlockingQueue)localObject1).poll(paramLong, ???);
    Object localObject2 = localRunnable1;
    localObject2 = (w)localRunnable1;
    ??? = b;
    if ((??? != null) && (localObject2 != null))
    {
      ??? = w.a((w)localObject2);
      if (??? != null) {
        synchronized (b)
        {
          localObject1 = b;
          localObject2 = w.a((w)localObject2);
          ((HashMap)localObject1).remove(localObject2);
        }
      }
    }
    return localRunnable2;
  }
  
  private boolean a(Runnable paramRunnable)
  {
    Object localObject1 = paramRunnable;
    localObject1 = (w)paramRunnable;
    synchronized (b)
    {
      Object localObject2 = b;
      w.n localn = w.a((w)localObject1);
      boolean bool1 = ((HashMap)localObject2).containsKey(localn);
      if (!bool1)
      {
        localObject2 = w.a();
        localObject2 = d;
        ((AtomicInteger)localObject2).incrementAndGet();
        localObject2 = b;
        localn = w.a((w)localObject1);
        ((HashMap)localObject2).put(localn, localObject1);
        localObject1 = a;
        boolean bool2 = ((LinkedBlockingQueue)localObject1).offer(paramRunnable);
        return bool2;
      }
      paramRunnable = w.a();
      paramRunnable = c;
      paramRunnable.incrementAndGet();
      return true;
    }
  }
  
  public final boolean addAll(Collection paramCollection)
  {
    new String[1][0] = "unexpected call.";
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>();
    throw paramCollection;
  }
  
  public final void clear()
  {
    new String[1][0] = "unexpected call.";
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>();
    throw localUnsupportedOperationException;
  }
  
  public final boolean contains(Object paramObject)
  {
    new String[1][0] = "unexpected call.";
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>();
    throw ((Throwable)paramObject);
  }
  
  public final boolean containsAll(Collection paramCollection)
  {
    new String[1][0] = "unexpected call.";
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>();
    throw paramCollection;
  }
  
  public final int drainTo(Collection paramCollection)
  {
    new String[1][0] = "unexpected call.";
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>();
    throw paramCollection;
  }
  
  public final int drainTo(Collection paramCollection, int paramInt)
  {
    new String[1][0] = "unexpected call.";
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>();
    throw paramCollection;
  }
  
  public final boolean isEmpty()
  {
    return a.isEmpty();
  }
  
  public final Iterator iterator()
  {
    new String[1][0] = "unexpected call.";
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>();
    throw localUnsupportedOperationException;
  }
  
  public final int remainingCapacity()
  {
    new String[1][0] = "unexpected call.";
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>();
    throw localUnsupportedOperationException;
  }
  
  public final boolean remove(Object paramObject)
  {
    new String[1][0] = "unexpected call.";
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>();
    throw ((Throwable)paramObject);
  }
  
  public final boolean removeAll(Collection paramCollection)
  {
    new String[1][0] = "unexpected call.";
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>();
    throw paramCollection;
  }
  
  public final boolean retainAll(Collection paramCollection)
  {
    new String[1][0] = "unexpected call.";
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>();
    throw paramCollection;
  }
  
  public final int size()
  {
    return a.size();
  }
  
  public final Object[] toArray()
  {
    new String[1][0] = "unexpected call.";
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>();
    throw localUnsupportedOperationException;
  }
  
  public final Object[] toArray(Object[] paramArrayOfObject)
  {
    new String[1][0] = "unexpected call.";
    paramArrayOfObject = new java/lang/UnsupportedOperationException;
    paramArrayOfObject.<init>();
    throw paramArrayOfObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.w.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */