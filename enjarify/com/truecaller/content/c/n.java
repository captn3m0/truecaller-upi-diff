package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.truecaller.content.b.f;

final class n
  implements ab
{
  private static final String a;
  private f b;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CREATE VIEW msg_messages_with_entities AS ");
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = "";
    arrayOfObject[1] = "";
    String str = String.format("SELECT m.*, p._id AS participant_id, p.type AS me_participant_type, p.tc_im_peer_id AS me_participant_im_id, p.raw_destination AS me_participant_raw_destination, p.normalized_destination AS me_participant_normalized_destination, p.country_code AS me_participant_country_code, p.aggregated_contact_id AS me_participant_aggregated_contact_id, p.filter_action AS me_participant_filter_action, p.is_top_spammer AS me_participant_is_top_spammer, p.top_spam_score AS me_participant_top_spam_score, IFNULL(a.contact_name, ig.title) AS me_participant_name, IFNULL(a.contact_image_url, ig.avatar) AS me_participant_image_url, a.contact_source AS me_participant_source, IFNULL(a.contact_badges, 0) AS me_participant_badges, a.contact_company AS me_participant_company_name, a.contact_search_time AS me_participant_search_time, IFNULL(a.contact_phonebook_id, -1) AS me_participant_phonebook_id, a.tc_id AS me_participant_tc_id, MAX(IFNULL(a.contact_spam_score, 0), p.top_spam_score) AS me_participant_spam_score, GROUP_CONCAT(e._id, '|') AS me_entities_id, GROUP_CONCAT(LENGTH(e.type) || '|' || e.type, \"\") AS me_entities_type, GROUP_CONCAT(LENGTH(e.content) || '|' || e.content, \"\") AS me_entities_content, GROUP_CONCAT(LENGTH(e.thumbnail) || '|' || e.thumbnail, \"\") AS me_entities_thumnail, GROUP_CONCAT(e.width, '|') AS me_entities_width, GROUP_CONCAT(e.height, '|') AS me_entities_height, GROUP_CONCAT(e.duration, '|') AS me_entities_duration, GROUP_CONCAT(e.status, '|') AS me_entities_status, GROUP_CONCAT(e.size, '|') AS me_entities_size, CASE WHEN m.transport = 0 THEN si.raw_id WHEN m.transport = 1 THEN mi.raw_id WHEN m.transport = 2 THEN ii.raw_id WHEN m.transport = 5 THEN hi.raw_id WHEN m.transport = 6 THEN sti.raw_id ELSE NULL END AS me_raw_id, CASE WHEN m.transport = 0 THEN si.raw_status WHEN m.transport = 1 THEN mi.raw_status ELSE NULL END AS me_raw_status, CASE WHEN m.transport = 0 THEN si.raw_thread_id WHEN m.transport = 1 THEN mi.raw_thread_id ELSE -1 END AS me_raw_thread, CASE WHEN m.transport = 0 THEN si.message_uri WHEN m.transport = 1 THEN mi.message_uri ELSE NULL END AS me_message_uri, si.protocol AS me_sms_protocol, si.type AS me_sms_type, si.service_center AS me_sms_service_center, si.subject AS me_sms_subject, si.error_code AS me_sms_error_code, si.reply_path_present AS me_sms_reply_path_present, si.stripped_raw_address AS me_sms_stripped_raw_address, mi.mms_message_id AS me_mms_message_id, mi.transaction_id AS me_mms_transaction_id, mi.subject AS me_mms_subject, mi.subject_charset AS me_mms_subject_charset, mi.content_location AS me_mms_content_location, mi.expiry AS me_mms_expiry, mi.priority AS me_mms_priority, mi.size AS me_mms_size, mi.message_box AS me_mms_message_box, mi.retrieve_status AS me_mms_retrieve_status, mi.response_status AS me_mms_response_status, mi.type AS me_mms_type, mi.delivery_report AS me_mms_delivery_report, mi.delivery_time AS me_mms_delivery_time, mi.version AS me_mms_version, mi.retrieve_text AS me_mms_retrieve_text, mi.retrieve_text_charset AS me_mms_retrieve_text_charset, mi.content_type AS me_mms_content_type, mi.content_class AS me_mms_content_class, mi.response_text AS me_mms_response_text, mi.message_class AS me_mms_message_class, mi.read_report AS me_mms_read_report, mi.read_status AS me_mms_read_status, mi.report_allowed AS me_mms_report_allowed, ii.im_status AS me_im_status, ii.delivery_status AS me_im_delivery_status, ii.read_status AS me_im_read_status, ii.read_sync_status AS me_im_read_sync_status, ii.error_code AS im_error_code, ii.api_version AS im_api_version, ii.random_id AS im_random_id, ii.reactions AS im_reactions, hi.type AS h_type, hi.number_type AS h_number_type, hi.features AS h_features, hi.call_log_id AS h_call_log_id, rm.status AS re_message_status, ra.contact_name AS re_participant_name, rp.normalized_destination AS re_participant_normalized_address, GROUP_CONCAT(re._id, '|') AS re_entities_id, GROUP_CONCAT(LENGTH(re.type) || '|' || re.type, \"\") AS re_entities_type, GROUP_CONCAT(LENGTH(re.content) || '|' || re.content, \"\") AS re_entities_content, GROUP_CONCAT(LENGTH(re.thumbnail) || '|' || re.thumbnail, \"\") AS re_entities_thumbnail FROM (SELECT * FROM msg_messages%s) m JOIN msg_entities e ON m._id = e.message_id LEFT JOIN msg_participants p ON p._id = m.participant_id LEFT JOIN aggregated_contact a ON p.aggregated_contact_id = a._id LEFT JOIN msg_sms_transport_info si ON m.transport = 0 AND si.message_id = m._id LEFT JOIN msg_mms_transport_info mi ON m.transport = 1 AND mi.message_id = m._id LEFT JOIN msg_im_transport_info ii ON m.transport = 2 AND ii.message_id = m._id LEFT JOIN msg_history_transport_info hi ON m.transport = 5 AND hi.message_id = m._id LEFT JOIN msg_status_transport_info sti ON m.transport = 6 AND sti.message_id = m._id LEFT JOIN msg_messages rm ON rm._id = m.reply_to_msg_id LEFT JOIN msg_entities re ON rm._id = re.message_id LEFT JOIN msg_participants rp ON rp._id = rm.participant_id LEFT JOIN aggregated_contact ra ON ra._id = rp.aggregated_contact_id LEFT JOIN msg_im_group_info ig ON ig.im_group_id = p.normalized_destination AND p.type = 4 GROUP BY m._id%s", arrayOfObject);
    localStringBuilder.append(str);
    a = localStringBuilder.toString();
  }
  
  n(f paramf)
  {
    b = paramf;
  }
  
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    int i = 12;
    String str1;
    if (paramInt1 < i)
    {
      paramSQLiteDatabase.execSQL("CREATE TABLE msg_messages(_id INTEGER PRIMARY KEY AUTOINCREMENT, conversation_id INTEGER NOT NULL, participant_id INTEGER NOT NULL, date INTEGER DEFAULT(0), date_sent INTEGER DEFAULT(0), status INTEGER DEFAULT(0), seen INTEGER DEFAULT(0), read INTEGER DEFAULT(0), locked INTEGER DEFAULT(0), transport INTEGER DEFAULT(3),sim_token TEXT NOT NULL DEFAULT('-1'),scheduled_transport INTEGER DEFAULT(3),analytics_id TEXT, hidden_number INTEGER DEFAULT(0),raw_address TEXT,category INTEGER, sync_status INTEGER DEFAULT(0),classification INTEGER DEFAULT(0),retry_count INTEGER DEFAULT(0),retry_date INTEGER DEFAULT(0),reply_to_msg_id INTEGER DEFAULT(-1))");
      paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_messages_conversation_id_date ON msg_messages (conversation_id, date)");
      paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_messages_participant_id ON msg_messages (participant_id)");
      paramSQLiteDatabase.execSQL("CREATE TABLE msg_entities (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE, type TEXT NOT NULL, content TEXT NOT NULL,thumbnail TEXT NOT NULL DEFAULT(''),width INTEGER DEFAULT(-1),height INTEGER DEFAULT(-1),duration INTEGER DEFAULT(-1),status INTEGER DEFAULT(0),size INTEGER DEFAULT(-1))");
      paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_entities_message_id ON msg_entities(message_id)");
      paramSQLiteDatabase.execSQL("CREATE TABLE msg_sms_transport_info (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE, raw_id INTEGER NOT NULL, raw_status INTEGER NOT NULL, raw_thread_id INTEGER NOT NULL DEFAULT(-1), message_uri TEXT NOT NULL, protocol INTEGER, type INTEGER, service_center TEXT, subject TEXT, error_code INTEGER, reply_path_present INTEGER,stripped_raw_address TEXT)");
      paramSQLiteDatabase.execSQL("CREATE TABLE msg_mms_transport_info (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE, raw_id INTEGER NOT NULL, raw_status INTEGER NOT NULL, raw_thread_id INTEGER NOT NULL DEFAULT(-1), message_uri TEXT NOT NULL, version INTEGER NOT NULL, type INTEGER NOT NULL, mms_message_id TEXT, transaction_id TEXT, subject TEXT, subject_charset INTEGER DEFAULT(0), retrieve_text TEXT, retrieve_text_charset INTEGER DEFAULT(0), content_location TEXT, content_type TEXT, content_class INTEGER DEFAULT(0), expiry INTEGER DEFAULT(0), priority INTEGER DEFAULT(0), size INTEGER DEFAULT(0), retrieve_status INTEGER DEFAULT(0), response_status INTEGER DEFAULT(0),response_text TEXT, message_class TEXT, message_box INTEGER DEFAULT(0),delivery_report INTEGER DEFAULT(0),delivery_time INTEGER DEFAULT(0), read_report INTEGER DEFAULT(0), read_status INTEGER DEFAULT(0), report_allowed INTEGER DEFAULT(0))");
      paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_sms_transport_info_message_id ON msg_sms_transport_info(message_id)");
      str1 = "CREATE INDEX idx_msg_mms_transport_info_message_id ON msg_mms_transport_info(message_id)";
      paramSQLiteDatabase.execSQL(str1);
    }
    paramInt2 = 21;
    String str2;
    if (paramInt1 < paramInt2)
    {
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS msg_im_transport_info");
      paramSQLiteDatabase.execSQL("CREATE TABLE msg_im_transport_info (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE, raw_id TEXT NOT NULL,im_status INTEGER DEFAULT(0),delivery_status INTEGER DEFAULT(0),read_status INTEGER DEFAULT(0),delivery_sync_status INTEGER(0),read_sync_status INTEGER DEFAULT(0),error_code INTEGER DEFAULT(0),api_version INTEGER DEFAULT(0),peer_id TEXT,read_send_time INTEGER DEFAULT(0),random_id INTEGER DEFAULT(0),reactions TEXT)");
      str2 = "CREATE INDEX idx_msg_im_transport_info_message_id ON msg_im_transport_info(message_id)";
      paramSQLiteDatabase.execSQL(str2);
    }
    if (paramInt1 >= i)
    {
      j = 22;
      if (paramInt1 < j)
      {
        str2 = "ALTER TABLE msg_entities ADD COLUMN status INTEGER DEFAULT 0";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= paramInt2)
    {
      j = 28;
      if (paramInt1 < j)
      {
        str2 = "ALTER TABLE msg_im_transport_info ADD COLUMN error_code INTEGER DEFAULT (0)";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= i)
    {
      j = 29;
      if (paramInt1 < j)
      {
        str2 = "ALTER TABLE msg_entities ADD COLUMN size INTEGER DEFAULT (-1)";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= paramInt2)
    {
      j = 31;
      if (paramInt1 < j)
      {
        str2 = "ALTER TABLE msg_im_transport_info ADD COLUMN delivery_sync_status INTEGER DEFAULT (0)";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= i)
    {
      j = 32;
      if (paramInt1 < j)
      {
        str2 = "ALTER TABLE msg_messages ADD COLUMN hidden_number INTEGER DEFAULT (0)";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= i)
    {
      j = 33;
      if (paramInt1 < j)
      {
        str2 = "ALTER TABLE msg_messages ADD COLUMN analytics_id TEXT";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    int j = 40;
    if (paramInt1 < j)
    {
      paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_messages_date ON msg_messages (date)");
      str2 = "CREATE INDEX idx_msg_messages_seen_date ON msg_messages (seen, date)";
      paramSQLiteDatabase.execSQL(str2);
    }
    if (paramInt1 >= i)
    {
      j = 46;
      if (paramInt1 < j)
      {
        paramSQLiteDatabase.execSQL("ALTER TABLE msg_messages ADD COLUMN raw_address TEXT");
        str2 = "ALTER TABLE msg_sms_transport_info ADD COLUMN stripped_raw_address TEXT";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= i)
    {
      j = 47;
      if (paramInt1 < j)
      {
        paramSQLiteDatabase.execSQL("ALTER TABLE msg_messages ADD COLUMN category INTEGER");
        paramSQLiteDatabase.execSQL("ALTER TABLE msg_messages ADD COLUMN sync_status INTEGER DEFAULT (0)");
        str2 = "ALTER TABLE msg_messages ADD COLUMN classification INTEGER DEFAULT (0)";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= paramInt2)
    {
      j = 49;
      if (paramInt1 < j)
      {
        str2 = "ALTER TABLE msg_im_transport_info ADD COLUMN api_version INTEGER DEFAULT(0)";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= i)
    {
      j = 51;
      if (paramInt1 < j)
      {
        str2 = "ALTER TABLE msg_entities ADD COLUMN duration INTEGER DEFAULT (-1)";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= i)
    {
      j = 53;
      if (paramInt1 < j)
      {
        str2 = "ALTER TABLE msg_entities ADD COLUMN thumbnail TEXT NOT NULL DEFAULT ('')";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= paramInt2)
    {
      j = 56;
      if (paramInt1 < j)
      {
        paramSQLiteDatabase.execSQL("ALTER TABLE msg_im_transport_info ADD COLUMN peer_id TEXT");
        str2 = "ALTER TABLE msg_im_transport_info ADD COLUMN read_send_time INTEGER DEFAULT(0)";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    if (paramInt1 >= i)
    {
      i = 70;
      if (paramInt1 < i)
      {
        paramSQLiteDatabase.execSQL("ALTER TABLE msg_messages ADD COLUMN retry_count INTEGER DEFAULT(0)");
        paramContext = "ALTER TABLE msg_messages ADD COLUMN retry_date INTEGER DEFAULT(0)";
        paramSQLiteDatabase.execSQL(paramContext);
      }
    }
    if (paramInt1 >= paramInt2)
    {
      i = 79;
      if (paramInt1 < i)
      {
        paramContext = "ALTER TABLE msg_im_transport_info ADD COLUMN random_id INTEGER DEFAULT(0)";
        paramSQLiteDatabase.execSQL(paramContext);
      }
    }
    i = 84;
    if ((paramInt1 >= paramInt2) && (paramInt1 < i))
    {
      str1 = "ALTER TABLE msg_im_transport_info ADD COLUMN reactions TEXT";
      paramSQLiteDatabase.execSQL(str1);
    }
    if (paramInt1 < i)
    {
      paramContext = b;
      paramContext.a(paramSQLiteDatabase);
    }
  }
  
  public final String[] a()
  {
    String[] tmp5_2 = new String[22];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "CREATE TABLE msg_messages(_id INTEGER PRIMARY KEY AUTOINCREMENT, conversation_id INTEGER NOT NULL, participant_id INTEGER NOT NULL, date INTEGER DEFAULT(0), date_sent INTEGER DEFAULT(0), status INTEGER DEFAULT(0), seen INTEGER DEFAULT(0), read INTEGER DEFAULT(0), locked INTEGER DEFAULT(0), transport INTEGER DEFAULT(3),sim_token TEXT NOT NULL DEFAULT('-1'),scheduled_transport INTEGER DEFAULT(3),analytics_id TEXT, hidden_number INTEGER DEFAULT(0),raw_address TEXT,category INTEGER, sync_status INTEGER DEFAULT(0),classification INTEGER DEFAULT(0),retry_count INTEGER DEFAULT(0),retry_date INTEGER DEFAULT(0),reply_to_msg_id INTEGER DEFAULT(-1))";
    tmp6_5[1] = "CREATE INDEX idx_msg_messages_conversation_id_date ON msg_messages (conversation_id, date)";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "CREATE INDEX idx_msg_messages_participant_id ON msg_messages (participant_id)";
    tmp15_6[3] = "CREATE INDEX idx_msg_messages_date ON msg_messages (date)";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "CREATE INDEX idx_msg_messages_seen_date ON msg_messages (seen, date)";
    tmp24_15[5] = "CREATE TABLE msg_entities (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE, type TEXT NOT NULL, content TEXT NOT NULL,thumbnail TEXT NOT NULL DEFAULT(''),width INTEGER DEFAULT(-1),height INTEGER DEFAULT(-1),duration INTEGER DEFAULT(-1),status INTEGER DEFAULT(0),size INTEGER DEFAULT(-1))";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[6] = "CREATE INDEX idx_msg_entities_message_id ON msg_entities(message_id)";
    tmp33_24[7] = "CREATE TABLE msg_sms_transport_info (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE, raw_id INTEGER NOT NULL, raw_status INTEGER NOT NULL, raw_thread_id INTEGER NOT NULL DEFAULT(-1), message_uri TEXT NOT NULL, protocol INTEGER, type INTEGER, service_center TEXT, subject TEXT, error_code INTEGER, reply_path_present INTEGER,stripped_raw_address TEXT)";
    String[] tmp44_33 = tmp33_24;
    String[] tmp44_33 = tmp33_24;
    tmp44_33[8] = "CREATE TABLE msg_mms_transport_info (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE, raw_id INTEGER NOT NULL, raw_status INTEGER NOT NULL, raw_thread_id INTEGER NOT NULL DEFAULT(-1), message_uri TEXT NOT NULL, version INTEGER NOT NULL, type INTEGER NOT NULL, mms_message_id TEXT, transaction_id TEXT, subject TEXT, subject_charset INTEGER DEFAULT(0), retrieve_text TEXT, retrieve_text_charset INTEGER DEFAULT(0), content_location TEXT, content_type TEXT, content_class INTEGER DEFAULT(0), expiry INTEGER DEFAULT(0), priority INTEGER DEFAULT(0), size INTEGER DEFAULT(0), retrieve_status INTEGER DEFAULT(0), response_status INTEGER DEFAULT(0),response_text TEXT, message_class TEXT, message_box INTEGER DEFAULT(0),delivery_report INTEGER DEFAULT(0),delivery_time INTEGER DEFAULT(0), read_report INTEGER DEFAULT(0), read_status INTEGER DEFAULT(0), report_allowed INTEGER DEFAULT(0))";
    tmp44_33[9] = "CREATE TABLE msg_im_transport_info (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE, raw_id TEXT NOT NULL,im_status INTEGER DEFAULT(0),delivery_status INTEGER DEFAULT(0),read_status INTEGER DEFAULT(0),delivery_sync_status INTEGER(0),read_sync_status INTEGER DEFAULT(0),error_code INTEGER DEFAULT(0),api_version INTEGER DEFAULT(0),peer_id TEXT,read_send_time INTEGER DEFAULT(0),random_id INTEGER DEFAULT(0),reactions TEXT)";
    String[] tmp55_44 = tmp44_33;
    String[] tmp55_44 = tmp44_33;
    tmp55_44[10] = "CREATE INDEX idx_msg_sms_transport_info_message_id ON msg_sms_transport_info(message_id)";
    tmp55_44[11] = "CREATE INDEX idx_msg_mms_transport_info_message_id ON msg_mms_transport_info(message_id)";
    String[] tmp66_55 = tmp55_44;
    String[] tmp66_55 = tmp55_44;
    tmp66_55[12] = "CREATE INDEX idx_msg_im_transport_info_message_id ON msg_im_transport_info(message_id)";
    tmp66_55[13] = "CREATE TABLE msg_history_transport_info (_id INTEGER PRIMARY KEY AUTOINCREMENT, raw_id INTEGER NOT NULL,call_log_id INTEGER DEFAULT NULL,message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE, type INTEGER DEFAULT(0),features INTEGER,number_type TEXT)";
    String[] tmp77_66 = tmp66_55;
    String[] tmp77_66 = tmp66_55;
    tmp77_66[14] = "CREATE INDEX idx_msg_history_transport_info_message_id ON msg_history_transport_info(message_id)";
    tmp77_66[15] = "CREATE TABLE msg_status_transport_info (_id INTEGER PRIMARY KEY AUTOINCREMENT, raw_id TEXT NOT NULL, message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE )";
    String[] tmp88_77 = tmp77_66;
    String[] tmp88_77 = tmp77_66;
    tmp88_77[16] = "CREATE INDEX idx_msg_status_transport_info_message_id ON msg_status_transport_info(message_id)";
    tmp88_77[17] = "CREATE INDEX idx_msg_status_transport_info_raw_id ON msg_status_transport_info(raw_id)";
    String[] tmp99_88 = tmp88_77;
    String[] tmp99_88 = tmp88_77;
    tmp99_88[18] = "CREATE TABLE msg_im_reactions (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE, emoji TEXT,from_peer_id TEXT,status INTEGER DEFAULT(0),send_date INTEGER DEFAULT(0))";
    tmp99_88[19] = "CREATE INDEX idx_msg_im_reactions_status ON msg_im_reactions(status)";
    tmp99_88[20] = "CREATE TABLE msg_im_unsupported_events (_id INTEGER PRIMARY KEY AUTOINCREMENT, event BLOB NOT NULL, api_version INTEGER DEFAULT(0))";
    String[] tmp115_99 = tmp99_88;
    tmp115_99[21] = "CREATE INDEX idx_msg_im_unsupported_events_api_version ON msg_im_unsupported_events(api_version)";
    return tmp115_99;
  }
  
  public final String[] b()
  {
    String[] arrayOfString = new String[6];
    arrayOfString[0] = "CREATE VIEW msg_messages_by_transport_view AS SELECT m._id AS _id, m.date AS date, CASE WHEN m.transport = 1 THEN mms.raw_id WHEN m.transport = 2 THEN im.raw_id WHEN m.transport = 5 THEN h.raw_id ELSE sms.raw_id END AS raw_id, CASE WHEN m.transport = 1 THEN mms.raw_status WHEN m.transport = 2 THEN 0 ELSE sms.raw_status END AS raw_status, CASE WHEN m.transport = 1 THEN mms.raw_thread_id WHEN m.transport = 2 THEN 0 ELSE sms.raw_thread_id END AS raw_thread, CASE WHEN m.transport = 1 THEN mms.response_status ELSE 0 END AS response_status, CASE WHEN m.transport = 1 THEN mms.retrieve_status ELSE 0 END AS retrieve_status, CASE WHEN m.transport = 1 THEN mms.delivery_report ELSE 0 END AS delivery_report, CASE WHEN m.transport = 0 THEN sms.stripped_raw_address ELSE NULL END AS stripped_raw_address, m.transport AS transport, m.seen AS seen, m.read AS read, m.locked AS locked, m.status AS status, m.category AS category, m.sync_status AS sync_status, m.classification AS classification FROM msg_messages m LEFT JOIN msg_sms_transport_info sms ON m._id = sms.message_id AND m.transport=0 LEFT JOIN msg_mms_transport_info mms ON m._id = mms.message_id AND m.transport=1 LEFT JOIN msg_im_transport_info im ON m._id = im.message_id AND m.transport=2 LEFT JOIN msg_history_transport_info h ON m._id = h.message_id AND m.transport=5";
    String str = a;
    arrayOfString[1] = str;
    arrayOfString[2] = "CREATE VIEW msg_conversation_transport_info AS SELECT m._id AS message_id, m.conversation_id AS conversation_id, m.seen AS seen, m.date AS date, m.read AS read, m.locked AS locked, m.status AS status, m.category AS category, m.transport AS transport, CASE WHEN m.transport = 0 THEN sms.raw_id WHEN m.transport = 1 THEN mms.raw_id WHEN m.transport = 2 THEN im.raw_id WHEN m.transport = 5 THEN h.raw_id END AS raw_id, CASE WHEN m.transport = 0 THEN sms.raw_status WHEN m.transport = 1 THEN mms.raw_id END AS raw_status, CASE WHEN m.transport = 0 THEN sms.raw_thread_id WHEN m.transport = 1 THEN mms.raw_thread_id END AS raw_thread, CASE WHEN m.transport = 0 THEN sms.message_uri WHEN m.transport = 1 THEN mms.message_uri END AS message_uri, sms.protocol AS sms_protocol, sms.type AS sms_type, sms.service_center AS sms_service_center, sms.subject AS sms_subject, sms.error_code AS sms_error_code, sms.reply_path_present AS sms_reply_path_present, sms.stripped_raw_address AS sms_stripped_raw_address, mms.mms_message_id AS mms_message_id, mms.transaction_id AS mms_transaction_id, mms.subject AS mms_subject, mms.subject_charset AS mms_subject_charset, mms.content_location AS mms_content_location, mms.expiry AS mms_expiry, mms.priority AS mms_priority, mms.size AS mms_size, mms.retrieve_status AS mms_retrieve_status, mms.response_status AS mms_response_status, mms.message_box AS mms_message_box, mms.type AS mms_type, mms.delivery_report AS mms_delivery_report, mms.delivery_time AS mms_delivery_time, mms.version AS mms_version, mms.retrieve_text AS mms_retrieve_text, mms.retrieve_text_charset AS mms_retrieve_text_charset, mms.content_type AS mms_content_type, mms.content_class AS mms_content_class, mms.response_text AS mms_response_text, mms.message_class AS mms_message_class, mms.read_report AS mms_read_report, mms.read_status AS mms_read_status, mms.report_allowed AS mms_report_allowed, im.im_status AS im_status, im.delivery_status AS im_delivery_status, im.read_status AS im_read_status, im.read_sync_status AS im_read_sync_status, im.error_code AS im_error_code, im.api_version AS im_api_version, im.random_id AS im_random_id, h.type AS h_type, h.number_type AS h_number_type, h.features AS h_features, h.call_log_id AS h_call_log_id FROM msg_messages m LEFT JOIN msg_sms_transport_info sms ON m.transport = 0 AND sms.message_id = m._id LEFT JOIN msg_mms_transport_info mms ON m.transport = 1 AND mms.message_id = m._id LEFT JOIN msg_im_transport_info im on m.transport = 2 AND im.message_id = m._id LEFT JOIN msg_history_transport_info h on m.transport = 5 AND h.message_id = m._id";
    arrayOfString[3] = "CREATE VIEW msg_im_sync_view AS SELECT m.date AS date,p.tc_im_peer_id AS im_peer_id,p.filter_action AS filter_action,im.raw_id AS raw_id,im.delivery_sync_status AS delivery_sync_status,im.read_sync_status AS read_sync_status, im.error_code AS error_code, c.tc_group_id AS im_group_id FROM msg_im_transport_info im LEFT JOIN msg_messages m ON m.transport = 2 AND m._id = im.message_id LEFT JOIN msg_participants p ON m.participant_id = p._id LEFT JOIN msg_conversations c ON m.conversation_id = c._id";
    arrayOfString[4] = "CREATE TRIGGER trigger_im_info_transport_on_reactions_insert AFTER INSERT ON msg_im_reactions BEGIN UPDATE msg_im_transport_info SET reactions=(SELECT GROUP_CONCAT(emoji, '|') FROM msg_im_reactions WHERE new.message_id=message_id ORDER BY send_date) WHERE message_id=new.message_id; END";
    arrayOfString[5] = "CREATE TRIGGER trigger_im_info_transport_on_reactions_delete AFTER DELETE ON msg_im_reactions BEGIN UPDATE msg_im_transport_info SET reactions=(SELECT GROUP_CONCAT(emoji, '|') FROM msg_im_reactions WHERE old.message_id=message_id ORDER BY send_date) WHERE message_id=old.message_id; END";
    return arrayOfString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */