package com.truecaller.content.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.ApplicationInfo;
import com.truecaller.log.f;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class w
  implements SharedPreferences, Runnable
{
  static final Object c;
  private static final int d = Runtime.getRuntime().availableProcessors();
  private static final Map e;
  private static final w.m f;
  private static final w.q j;
  private static final ThreadPoolExecutor k;
  w.e a;
  final WeakHashMap b;
  private final File g;
  private final File h;
  private final w.n i;
  private w.l l;
  private final WeakHashMap m;
  
  static
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    e = (Map)localObject;
    localObject = new com/truecaller/content/c/w$m;
    ((w.m)localObject).<init>((byte)0);
    f = (w.m)localObject;
    localObject = new com/truecaller/content/c/w$q;
    ((w.q)localObject).<init>((byte)0);
    j = (w.q)localObject;
    localObject = new java/util/concurrent/ThreadPoolExecutor;
    int n = d;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    w.q localq = j;
    ThreadFactory localThreadFactory = w.k.a;
    ((ThreadPoolExecutor)localObject).<init>(0, n, 30, localTimeUnit, localq, localThreadFactory);
    k = (ThreadPoolExecutor)localObject;
    localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
  }
  
  public w(Context arg1, String paramString, w.l paraml)
  {
    ??? = w.f.a;
    a = ((w.e)???);
    ??? = w.g.a;
    l = ((w.l)???);
    ??? = new java/util/WeakHashMap;
    ((WeakHashMap)???).<init>();
    b = ((WeakHashMap)???);
    ??? = new java/util/WeakHashMap;
    ((WeakHashMap)???).<init>();
    m = ((WeakHashMap)???);
    ??? = ???.getApplicationInfo();
    ??? = new java/io/File;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    ??? = dataDir;
    ((StringBuilder)localObject2).append(???);
    ((StringBuilder)localObject2).append("/shared_prefs");
    ??? = ((StringBuilder)localObject2).toString();
    ((File)???).<init>(???);
    h = ((File)???);
    ??? = new java/io/File;
    ??? = h;
    ???.<init>((File)???, paramString);
    g = ???;
    l = paraml;
    ??? = g.getAbsolutePath();
    paramString = (w.n)e.get(???);
    int n = 0;
    paraml = null;
    if (paramString == null) {
      synchronized (e)
      {
        paramString = e;
        paramString = paramString.get(???);
        paramString = (w.n)paramString;
        if (paramString == null)
        {
          paramString = e;
          localObject2 = new com/truecaller/content/c/w$n;
          ((w.n)localObject2).<init>((byte)0);
          paramString.put(???, localObject2);
          bool1 = true;
          paramString = (String)localObject2;
          n = 1;
        }
      }
    }
    i = paramString;
    ??? = h;
    boolean bool1 = ???.exists();
    if (!bool1) {
      synchronized (w.class)
      {
        paramString = h;
        boolean bool2 = paramString.exists();
        if (!bool2)
        {
          paramString = h;
          bool2 = paramString.mkdirs();
          if (!bool2)
          {
            paramString = "Impossible to create directories for preferences.";
            new String[1][0] = paramString;
          }
        }
      }
    }
    if (n != 0) {
      b();
    }
    c();
  }
  
  public static void a(SharedPreferences paramSharedPreferences1, SharedPreferences paramSharedPreferences2)
  {
    if ((paramSharedPreferences1 != null) && (paramSharedPreferences2 != null))
    {
      paramSharedPreferences1 = paramSharedPreferences1.getAll();
      if (paramSharedPreferences1 != null)
      {
        boolean bool = paramSharedPreferences1.isEmpty();
        if (!bool)
        {
          paramSharedPreferences2 = paramSharedPreferences2.edit();
          paramSharedPreferences1 = paramSharedPreferences1.entrySet().iterator();
          for (;;)
          {
            bool = paramSharedPreferences1.hasNext();
            if (!bool) {
              break;
            }
            Object localObject1 = (Map.Entry)paramSharedPreferences1.next();
            Object localObject2 = ((Map.Entry)localObject1).getValue();
            int i1 = v.h.a(localObject2);
            int i2 = 2;
            if (i1 != i2)
            {
              i2 = 4;
              if (i1 != i2)
              {
                i2 = 8;
                if (i1 != i2)
                {
                  i2 = 16;
                  if (i1 != i2)
                  {
                    i2 = 32;
                    if (i1 != i2)
                    {
                      i2 = 64;
                      if (i1 == i2)
                      {
                        localObject2 = (String)((Map.Entry)localObject1).getKey();
                        localObject1 = (Set)((Map.Entry)localObject1).getValue();
                        paramSharedPreferences2.putStringSet((String)localObject2, (Set)localObject1);
                      }
                    }
                    else
                    {
                      localObject2 = (String)((Map.Entry)localObject1).getKey();
                      localObject1 = (String)((Map.Entry)localObject1).getValue();
                      paramSharedPreferences2.putString((String)localObject2, (String)localObject1);
                    }
                  }
                  else
                  {
                    localObject2 = (String)((Map.Entry)localObject1).getKey();
                    localObject1 = (Boolean)((Map.Entry)localObject1).getValue();
                    bool = ((Boolean)localObject1).booleanValue();
                    paramSharedPreferences2.putBoolean((String)localObject2, bool);
                  }
                }
                else
                {
                  localObject2 = (String)((Map.Entry)localObject1).getKey();
                  localObject1 = (Float)((Map.Entry)localObject1).getValue();
                  float f1 = ((Float)localObject1).floatValue();
                  paramSharedPreferences2.putFloat((String)localObject2, f1);
                }
              }
              else
              {
                localObject2 = (String)((Map.Entry)localObject1).getKey();
                localObject1 = (Long)((Map.Entry)localObject1).getValue();
                long l1 = ((Long)localObject1).longValue();
                paramSharedPreferences2.putLong((String)localObject2, l1);
              }
            }
            else
            {
              localObject2 = (String)((Map.Entry)localObject1).getKey();
              localObject1 = (Integer)((Map.Entry)localObject1).getValue();
              int n = ((Integer)localObject1).intValue();
              paramSharedPreferences2.putInt((String)localObject2, n);
            }
          }
          paramSharedPreferences2.apply();
        }
      }
    }
  }
  
  private void a(Queue paramQueue)
  {
    Object localObject = b;
    boolean bool1 = ((WeakHashMap)localObject).isEmpty();
    if (!bool1)
    {
      localObject = b.keySet().iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject).hasNext();
        if (!bool2) {
          break;
        }
        w.c localc = (w.c)((Iterator)localObject).next();
        if (localc != null) {
          if (paramQueue == null) {
            localc.c();
          } else {
            localc.a(paramQueue);
          }
        }
      }
    }
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    File localFile = new java/io/File;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = getApplicationInfodataDir;
    localStringBuilder.append(paramContext);
    localStringBuilder.append("/shared_prefs");
    paramContext = localStringBuilder.toString();
    localFile.<init>(paramContext);
    paramContext = new java/io/File;
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append(".xml");
    paramString = localStringBuilder.toString();
    paramContext.<init>(localFile, paramString);
    boolean bool1 = paramContext.exists();
    if (bool1)
    {
      long l1 = paramContext.length();
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  private static boolean a(w.n paramn, File paramFile1, File paramFile2, w.l paraml)
  {
    Object localObject1 = c;
    int n = ((AtomicInteger)localObject1).get();
    long l1 = System.nanoTime();
    File localFile = new java/io/File;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    Object localObject3 = paramFile2.getName();
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append(".bak");
    localObject2 = ((StringBuilder)localObject2).toString();
    localFile.<init>(paramFile1, (String)localObject2);
    localObject2 = new java/io/File;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    ((StringBuilder)localObject3).append(l1);
    ((StringBuilder)localObject3).append("-");
    Object localObject4 = paramFile2.getName();
    ((StringBuilder)localObject3).append((String)localObject4);
    ((StringBuilder)localObject3).append(".temp");
    localObject4 = ((StringBuilder)localObject3).toString();
    ((File)localObject2).<init>(paramFile1, (String)localObject4);
    paramFile1 = Thread.currentThread();
    localObject4 = j;
    int i1 = ((w.q)localObject4).size();
    int i2 = 1;
    int i3 = 4;
    if (i1 > i3)
    {
      i1 = 10;
    }
    else
    {
      i3 = d;
      if (i1 > i3) {
        i1 = 5;
      } else {
        i1 = 1;
      }
    }
    paramFile1.setPriority(i1);
    boolean bool1 = false;
    paramFile1 = null;
    try
    {
      localObject4 = b;
      Collections.unmodifiableMap((Map)localObject4);
      paraml = paraml.a();
      localObject4 = c;
      i1 = ((AtomicInteger)localObject4).get();
      if (i1 == n)
      {
        if (paraml != null)
        {
          n = paraml.length;
          if (n != 0)
          {
            localObject1 = new java/io/FileOutputStream;
            ((FileOutputStream)localObject1).<init>((File)localObject2);
            localObject4 = new java/io/BufferedOutputStream;
            i3 = 32768;
            ((BufferedOutputStream)localObject4).<init>((OutputStream)localObject1, i3);
            ((BufferedOutputStream)localObject4).write(paraml);
            ((BufferedOutputStream)localObject4).flush();
            ((BufferedOutputStream)localObject4).close();
            break label320;
          }
        }
        ((File)localObject2).delete();
        label320:
        bool1 = true;
      }
      else
      {
        paraml = new com/truecaller/content/c/w$j;
        localObject1 = "Collection modified during serialization. Reschedule is needed.";
        paraml.<init>((String)localObject1);
        throw paraml;
      }
    }
    finally
    {
      localObject1 = new String[i2];
      paraml = f.a(paraml);
      localObject1[0] = paraml;
      boolean bool2;
      if (bool1)
      {
        bool2 = localFile.exists();
        if (bool2) {
          bool1 = localFile.delete();
        }
      }
      if (bool1)
      {
        bool2 = paramFile2.exists();
        if (bool2) {
          bool1 = paramFile2.renameTo(localFile);
        }
      }
      if (bool1)
      {
        bool2 = ((File)localObject2).exists();
        if (bool2) {
          try
          {
            bool1 = ((File)localObject2).renameTo(paramFile2);
            paramFile2 = d;
            if (paramFile2 != null)
            {
              paramFile2 = Runtime.getRuntime();
              paraml = d;
              paramFile2.removeShutdownHook(paraml);
              paramFile2 = null;
              d = null;
            }
          }
          finally {}
        }
      }
      boolean bool3 = ((File)localObject2).exists();
      if (bool3) {
        ((File)localObject2).delete();
      }
    }
    return bool1;
  }
  
  /* Error */
  private boolean b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 144	com/truecaller/content/c/w:g	Ljava/io/File;
    //   4: astore_1
    //   5: aload_1
    //   6: invokevirtual 167	java/io/File:exists	()Z
    //   9: istore_2
    //   10: iload_2
    //   11: ifeq +83 -> 94
    //   14: aload_0
    //   15: getfield 144	com/truecaller/content/c/w:g	Ljava/io/File;
    //   18: invokevirtual 304	java/io/File:length	()J
    //   21: lstore_3
    //   22: lload_3
    //   23: l2i
    //   24: newarray <illegal type>
    //   26: astore_1
    //   27: aconst_null
    //   28: astore 5
    //   30: new 404	java/io/FileInputStream
    //   33: astore 6
    //   35: aload_0
    //   36: getfield 144	com/truecaller/content/c/w:g	Ljava/io/File;
    //   39: astore 7
    //   41: aload 6
    //   43: aload 7
    //   45: invokespecial 405	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   48: aload 6
    //   50: invokevirtual 409	java/io/FileInputStream:getChannel	()Ljava/nio/channels/FileChannel;
    //   53: astore 5
    //   55: aload_1
    //   56: invokestatic 415	java/nio/ByteBuffer:wrap	([B)Ljava/nio/ByteBuffer;
    //   59: astore_1
    //   60: aload 5
    //   62: aload_1
    //   63: invokevirtual 421	java/nio/channels/FileChannel:read	(Ljava/nio/ByteBuffer;)I
    //   66: pop
    //   67: aload 5
    //   69: invokevirtual 422	java/nio/channels/FileChannel:close	()V
    //   72: aload 6
    //   74: invokevirtual 423	java/io/FileInputStream:close	()V
    //   77: goto +7 -> 84
    //   80: pop
    //   81: aconst_null
    //   82: astore 6
    //   84: aload 5
    //   86: invokestatic 428	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   89: aload 6
    //   91: invokestatic 428	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   94: iconst_1
    //   95: istore_2
    //   96: aconst_null
    //   97: astore 5
    //   99: aload_0
    //   100: getfield 100	com/truecaller/content/c/w:l	Lcom/truecaller/content/c/w$l;
    //   103: astore 6
    //   105: aload 6
    //   107: invokeinterface 430 1 0
    //   112: astore 6
    //   114: aload 6
    //   116: ifnull +69 -> 185
    //   119: aload 6
    //   121: invokeinterface 431 1 0
    //   126: istore 8
    //   128: iload 8
    //   130: ifle +55 -> 185
    //   133: aload_0
    //   134: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   137: astore 7
    //   139: aload 7
    //   141: getfield 433	com/truecaller/content/c/w$n:a	Ljava/lang/Object;
    //   144: astore 7
    //   146: aload 7
    //   148: monitorenter
    //   149: aload_0
    //   150: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   153: astore 9
    //   155: aload 9
    //   157: getfield 345	com/truecaller/content/c/w$n:b	Ljava/util/Map;
    //   160: astore 9
    //   162: aload 9
    //   164: aload 6
    //   166: invokeinterface 437 2 0
    //   171: aload 7
    //   173: monitorexit
    //   174: goto +11 -> 185
    //   177: astore 6
    //   179: aload 7
    //   181: monitorexit
    //   182: aload 6
    //   184: athrow
    //   185: aload_0
    //   186: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   189: getfield 307	com/truecaller/content/c/w$n:c	Ljava/util/concurrent/atomic/AtomicInteger;
    //   192: invokevirtual 440	java/util/concurrent/atomic/AtomicInteger:incrementAndGet	()I
    //   195: pop
    //   196: aload_0
    //   197: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   200: getfield 307	com/truecaller/content/c/w$n:c	Ljava/util/concurrent/atomic/AtomicInteger;
    //   203: astore 5
    //   205: aload 5
    //   207: monitorenter
    //   208: aload_0
    //   209: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   212: astore 6
    //   214: aload 6
    //   216: getfield 307	com/truecaller/content/c/w$n:c	Ljava/util/concurrent/atomic/AtomicInteger;
    //   219: astore 6
    //   221: aload 6
    //   223: invokevirtual 443	java/lang/Object:notifyAll	()V
    //   226: aload 5
    //   228: monitorexit
    //   229: goto +77 -> 306
    //   232: astore_1
    //   233: aload 5
    //   235: monitorexit
    //   236: aload_1
    //   237: athrow
    //   238: astore_1
    //   239: goto +76 -> 315
    //   242: astore 6
    //   244: iload_2
    //   245: anewarray 174	java/lang/String
    //   248: astore_1
    //   249: aload 6
    //   251: invokestatic 391	com/truecaller/log/f:a	(Ljava/lang/Throwable;)Ljava/lang/String;
    //   254: astore 6
    //   256: aload_1
    //   257: iconst_0
    //   258: aload 6
    //   260: aastore
    //   261: aload_0
    //   262: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   265: getfield 307	com/truecaller/content/c/w$n:c	Ljava/util/concurrent/atomic/AtomicInteger;
    //   268: invokevirtual 440	java/util/concurrent/atomic/AtomicInteger:incrementAndGet	()I
    //   271: pop
    //   272: aload_0
    //   273: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   276: getfield 307	com/truecaller/content/c/w$n:c	Ljava/util/concurrent/atomic/AtomicInteger;
    //   279: astore_1
    //   280: aload_1
    //   281: monitorenter
    //   282: aload_0
    //   283: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   286: astore 6
    //   288: aload 6
    //   290: getfield 307	com/truecaller/content/c/w$n:c	Ljava/util/concurrent/atomic/AtomicInteger;
    //   293: astore 6
    //   295: aload 6
    //   297: invokevirtual 443	java/lang/Object:notifyAll	()V
    //   300: aload_1
    //   301: monitorexit
    //   302: iconst_0
    //   303: istore_2
    //   304: aconst_null
    //   305: astore_1
    //   306: iload_2
    //   307: ireturn
    //   308: astore 5
    //   310: aload_1
    //   311: monitorexit
    //   312: aload 5
    //   314: athrow
    //   315: aload_0
    //   316: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   319: getfield 307	com/truecaller/content/c/w$n:c	Ljava/util/concurrent/atomic/AtomicInteger;
    //   322: invokevirtual 440	java/util/concurrent/atomic/AtomicInteger:incrementAndGet	()I
    //   325: pop
    //   326: aload_0
    //   327: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   330: getfield 307	com/truecaller/content/c/w$n:c	Ljava/util/concurrent/atomic/AtomicInteger;
    //   333: astore 5
    //   335: aload 5
    //   337: monitorenter
    //   338: aload_0
    //   339: getfield 163	com/truecaller/content/c/w:i	Lcom/truecaller/content/c/w$n;
    //   342: astore 6
    //   344: aload 6
    //   346: getfield 307	com/truecaller/content/c/w$n:c	Ljava/util/concurrent/atomic/AtomicInteger;
    //   349: astore 6
    //   351: aload 6
    //   353: invokevirtual 443	java/lang/Object:notifyAll	()V
    //   356: aload 5
    //   358: monitorexit
    //   359: aload_1
    //   360: athrow
    //   361: astore_1
    //   362: aload 5
    //   364: monitorexit
    //   365: aload_1
    //   366: athrow
    //   367: pop
    //   368: goto -284 -> 84
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	371	0	this	w
    //   4	59	1	localObject1	Object
    //   232	5	1	localObject2	Object
    //   238	1	1	localObject3	Object
    //   361	5	1	localObject5	Object
    //   9	298	2	bool	boolean
    //   21	2	3	l1	long
    //   28	206	5	localObject6	Object
    //   308	5	5	localObject7	Object
    //   33	132	6	localObject8	Object
    //   177	6	6	localObject9	Object
    //   212	10	6	localObject10	Object
    //   242	8	6	localThrowable	Throwable
    //   254	98	6	localObject11	Object
    //   39	141	7	localObject12	Object
    //   126	3	8	n	int
    //   153	10	9	localObject13	Object
    // Exception table:
    //   from	to	target	type
    //   30	33	80	finally
    //   35	39	80	finally
    //   43	48	80	finally
    //   149	153	177	finally
    //   155	160	177	finally
    //   164	171	177	finally
    //   171	174	177	finally
    //   179	182	177	finally
    //   208	212	232	finally
    //   214	219	232	finally
    //   221	226	232	finally
    //   226	229	232	finally
    //   233	236	232	finally
    //   244	248	238	finally
    //   249	254	238	finally
    //   258	261	238	finally
    //   99	103	242	finally
    //   105	112	242	finally
    //   119	126	242	finally
    //   133	137	242	finally
    //   139	144	242	finally
    //   146	149	242	finally
    //   182	185	242	finally
    //   282	286	308	finally
    //   288	293	308	finally
    //   295	300	308	finally
    //   300	302	308	finally
    //   310	312	308	finally
    //   338	342	361	finally
    //   344	349	361	finally
    //   351	356	361	finally
    //   356	359	361	finally
    //   362	365	361	finally
    //   48	53	367	finally
    //   55	59	367	finally
    //   62	67	367	finally
    //   67	72	367	finally
    //   72	77	367	finally
  }
  
  private void c()
  {
    ??? = i.c;
    int n = ???.get();
    if (n == 0)
    {
      try
      {
        synchronized (i.c)
        {
          Object localObject1 = i;
          localObject1 = c;
          long l1 = 40;
          localObject1.wait(l1);
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        return;
      }
      throw ((Throwable)localObject2);
    }
  }
  
  private boolean d()
  {
    synchronized (i)
    {
      Object localObject1 = k;
      ((ThreadPoolExecutor)localObject1).execute(this);
      try
      {
        localObject1 = i;
        localObject1.wait();
        return true;
      }
      catch (InterruptedException localInterruptedException)
      {
        localObject1 = null;
        return false;
      }
    }
  }
  
  public final boolean contains(String paramString)
  {
    synchronized (i.a)
    {
      Object localObject2 = i;
      localObject2 = b;
      boolean bool = ((Map)localObject2).containsKey(paramString);
      return bool;
    }
  }
  
  public final SharedPreferences.Editor edit()
  {
    w.d locald = new com/truecaller/content/c/w$d;
    locald.<init>(this, (byte)0);
    return locald;
  }
  
  public final Map getAll()
  {
    synchronized (i.a)
    {
      Object localObject2 = i;
      localObject2 = b;
      localObject2 = Collections.unmodifiableMap((Map)localObject2);
      return (Map)localObject2;
    }
  }
  
  public final boolean getBoolean(String paramString, boolean paramBoolean)
  {
    synchronized (i.a)
    {
      Object localObject2 = i;
      localObject2 = b;
      boolean bool1 = ((Map)localObject2).containsKey(paramString);
      if (bool1)
      {
        localObject2 = i;
        localObject2 = b;
        paramString = ((Map)localObject2).get(paramString);
        bool1 = paramString instanceof Boolean;
        if (bool1)
        {
          paramString = (Boolean)paramString;
          boolean bool2 = paramString.booleanValue();
          return bool2;
        }
      }
      return paramBoolean;
    }
  }
  
  public final float getFloat(String paramString, float paramFloat)
  {
    synchronized (i.a)
    {
      Object localObject2 = i;
      localObject2 = b;
      boolean bool = ((Map)localObject2).containsKey(paramString);
      if (bool)
      {
        localObject2 = i;
        localObject2 = b;
        paramString = ((Map)localObject2).get(paramString);
        bool = paramString instanceof Number;
        if (bool)
        {
          paramString = (Number)paramString;
          float f1 = paramString.floatValue();
          return f1;
        }
      }
      return paramFloat;
    }
  }
  
  public final int getInt(String paramString, int paramInt)
  {
    synchronized (i.a)
    {
      Object localObject2 = i;
      localObject2 = b;
      boolean bool = ((Map)localObject2).containsKey(paramString);
      if (bool)
      {
        localObject2 = i;
        localObject2 = b;
        paramString = ((Map)localObject2).get(paramString);
        bool = paramString instanceof Number;
        if (bool)
        {
          paramString = (Number)paramString;
          int n = paramString.intValue();
          return n;
        }
      }
      return paramInt;
    }
  }
  
  public final long getLong(String paramString, long paramLong)
  {
    synchronized (i.a)
    {
      Object localObject2 = i;
      localObject2 = b;
      boolean bool = ((Map)localObject2).containsKey(paramString);
      if (bool)
      {
        localObject2 = i;
        localObject2 = b;
        paramString = ((Map)localObject2).get(paramString);
        bool = paramString instanceof Number;
        if (bool)
        {
          paramString = (Number)paramString;
          long l1 = paramString.longValue();
          return l1;
        }
      }
      return paramLong;
    }
  }
  
  public final String getString(String paramString1, String paramString2)
  {
    synchronized (i.a)
    {
      Object localObject2 = i;
      localObject2 = b;
      boolean bool = ((Map)localObject2).containsKey(paramString1);
      if (bool)
      {
        paramString2 = i;
        paramString2 = b;
        paramString1 = paramString2.get(paramString1);
        paramString1 = String.valueOf(paramString1);
        return paramString1;
      }
      return paramString2;
    }
  }
  
  public final Set getStringSet(String paramString, Set paramSet)
  {
    synchronized (i.a)
    {
      Object localObject2 = i;
      localObject2 = b;
      boolean bool = ((Map)localObject2).containsKey(paramString);
      if (bool)
      {
        localObject2 = i;
        localObject2 = b;
        paramString = ((Map)localObject2).get(paramString);
        bool = paramString instanceof Set;
        if (bool)
        {
          paramString = (Set)paramString;
          return paramString;
        }
      }
      return paramSet;
    }
  }
  
  public final void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener paramOnSharedPreferenceChangeListener)
  {
    WeakHashMap localWeakHashMap = m;
    Object localObject = c;
    localWeakHashMap.put(paramOnSharedPreferenceChangeListener, localObject);
  }
  
  public final void run()
  {
    ??? = i;
    Object localObject2 = h;
    File localFile = g;
    w.l locall = l;
    boolean bool1 = a((w.n)???, (File)localObject2, localFile, locall);
    if (!bool1)
    {
      localObject2 = k;
      boolean bool2 = ((ThreadPoolExecutor)localObject2).isShutdown();
      if (!bool2)
      {
        fb.incrementAndGet();
        localObject2 = k;
        ((ThreadPoolExecutor)localObject2).execute(this);
      }
    }
    if (bool1) {
      synchronized (i)
      {
        localObject2 = i;
        localObject2.notifyAll();
        ??? = fa;
        ((AtomicInteger)???).incrementAndGet();
      }
    }
  }
  
  public final void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener paramOnSharedPreferenceChangeListener)
  {
    m.remove(paramOnSharedPreferenceChangeListener);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */