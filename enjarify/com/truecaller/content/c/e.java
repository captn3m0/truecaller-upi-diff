package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.common.c.b.b;

public final class e
  implements ab
{
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    String str1 = "context";
    k.b(paramContext, str1);
    paramContext = "db";
    k.b(paramSQLiteDatabase, paramContext);
    int i = 78;
    if (paramInt1 < i)
    {
      String str2 = "contact_sorting_index";
      b.a(paramSQLiteDatabase, "table", str2);
      paramContext = a();
      paramInt1 = paramContext.length;
      paramInt2 = 0;
      str1 = null;
      while (paramInt2 < paramInt1)
      {
        String str3 = paramContext[paramInt2];
        paramSQLiteDatabase.execSQL(str3);
        paramInt2 += 1;
      }
    }
  }
  
  public final String[] a()
  {
    String[] arrayOfString1 = new String[4];
    arrayOfString1[0] = "\n    CREATE TABLE contact_sorting_index (\n        aggregated_contact_id INTEGER NOT NULL,\n        first_name TEXT DEFAULT NULL,\n        last_name TEXT DEFAULT NULL,\n        sorting_key_1 TEXT DEFAULT NULL,\n        sorting_key_2 TEXT DEFAULT NULL,\n        sorting_group_1 TEXT DEFAULT NULL,\n        sorting_group_2 TEXT DEFAULT NULL,\n        contact_update_timestamp INTEGER DEFAULT NULL)\n    ";
    String[] arrayOfString2 = { "sorting_key_1" };
    String str = b.b("contact_sorting_index", arrayOfString2);
    k.a(str, "ProviderUtil.getCreateIn…gTable.SORTING_MODE1_KEY)");
    arrayOfString1[1] = str;
    arrayOfString2 = new String[] { "sorting_key_2" };
    str = b.b("contact_sorting_index", arrayOfString2);
    k.a(str, "ProviderUtil.getCreateIn…gTable.SORTING_MODE2_KEY)");
    arrayOfString1[2] = str;
    arrayOfString2 = new String[] { "aggregated_contact_id" };
    str = b.b("contact_sorting_index", arrayOfString2);
    k.a(str, "ProviderUtil.getCreateIn…le.AGGREGATED_CONTACT_ID)");
    arrayOfString1[3] = str;
    return arrayOfString1;
  }
  
  public final String[] b()
  {
    String[] arrayOfString = new String[2];
    String str = f.a();
    arrayOfString[0] = str;
    str = f.b();
    arrayOfString[1] = str;
    return arrayOfString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */