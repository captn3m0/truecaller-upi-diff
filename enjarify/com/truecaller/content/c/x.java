package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public final class x
  implements ab
{
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramContext = "db";
    k.b(paramSQLiteDatabase, paramContext);
    int i = 44;
    if (paramInt1 < i)
    {
      paramContext = a();
      paramInt1 = paramContext.length;
      paramInt2 = 0;
      while (paramInt2 < paramInt1)
      {
        String str = paramContext[paramInt2];
        paramSQLiteDatabase.execSQL(str);
        paramInt2 += 1;
      }
    }
  }
  
  public final String[] a()
  {
    return new String[] { "CREATE TABLE profile_view_events (tc_id TEXT PRIMARY KEY, timestamp INT NOT NULL )" };
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */