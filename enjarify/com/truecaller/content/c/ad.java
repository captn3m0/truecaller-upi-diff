package com.truecaller.content.c;

import android.database.sqlite.SQLiteDatabase;
import c.a.f;
import c.g.b.k;
import com.truecaller.content.TruecallerContract.an;

public final class ad
  implements ac
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    String str1 = "msg_thread_stats_temp";
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("\n           CREATE TEMP TABLE ");
    ((StringBuilder)localObject).append(str1);
    ((StringBuilder)localObject).append("\n           AS\n           SELECT conversation_id, filter, ");
    String[] arrayOfString = TruecallerContract.an.a;
    k.a(arrayOfString, "MODIFIABLE_COLUMNS");
    String str2 = null;
    StringBuilder localStringBuilder = null;
    String str3 = null;
    int i = 63;
    String str4 = f.a(arrayOfString, null, null, null, 0, null, i);
    ((StringBuilder)localObject).append(str4);
    ((StringBuilder)localObject).append("\n           FROM msg_thread_stats;\n           CREATE INDEX idx_");
    ((StringBuilder)localObject).append(str1);
    ((StringBuilder)localObject).append(" ON ");
    ((StringBuilder)localObject).append(str1);
    ((StringBuilder)localObject).append(" (conversation_id, filter)\n        ");
    localObject = ((StringBuilder)localObject).toString();
    paramSQLiteDatabase.execSQL((String)localObject);
    paramSQLiteDatabase.execSQL("DELETE FROM msg_thread_stats");
    paramSQLiteDatabase.execSQL("\n            INSERT INTO msg_thread_stats (\n                latest_message_id,\n                latest_message_status,\n                unread_messages_count,\n                latest_sim_token,\n                date_sorting,\n                filter,\n                latest_message_transport,\n                conversation_id)\n            SELECT\n                _id,\n                status,\n                SUM(NOT read),\n                sim_token,\n                date,\n                filter,\n                transport,\n                conversation_id\n            FROM (SELECT *, category AS filter FROM msg_messages\n                UNION\n                SELECT *, 1 AS filter FROM msg_messages\n                ORDER BY date ASC)\n            GROUP BY conversation_id, filter\n            ");
    paramSQLiteDatabase.execSQL("\n            UPDATE msg_thread_stats\n                SET\n                    latest_message_media_count = (SELECT COUNT(*)\n                        FROM msg_entities\n                        WHERE message_id = latest_message_id\n                            AND type != 'text/plain'),\n                    latest_message_media_type = (SELECT type\n                        FROM msg_entities\n                        WHERE message_id = latest_message_id\n                            AND type != 'text/plain'\n                        LIMIT 1),\n                    snippet_text = (SELECT content\n                        FROM msg_entities\n                        WHERE message_id = latest_message_id\n                            AND type = 'text/plain'),\n                    latest_message_raw_status = (SELECT IFNULL ((SELECT raw_status\n                        FROM msg_sms_transport_info\n                        WHERE message_id = latest_message_id), 0)),\n                    latest_message_delivery_status = (SELECT IFNULL ((SELECT delivery_status\n                        FROM msg_im_transport_info\n                        WHERE message_id = latest_message_id), 0)),\n                    latest_message_read_status = (SELECT IFNULL ((SELECT read_status\n                        FROM msg_im_transport_info\n                        WHERE message_id = latest_message_id), 0))\n            ");
    localObject = TruecallerContract.an.a;
    str4 = "MODIFIABLE_COLUMNS";
    k.a(localObject, str4);
    int j = localObject.length;
    int k = 0;
    arrayOfString = null;
    while (k < j)
    {
      str2 = localObject[k];
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("\n                UPDATE msg_thread_stats\n                SET ");
      localStringBuilder.append(str2);
      str3 = " = (SELECT ";
      localStringBuilder.append(str3);
      localStringBuilder.append(str2);
      localStringBuilder.append("\n                    FROM ");
      localStringBuilder.append(str1);
      localStringBuilder.append("\n                    WHERE msg_thread_stats.conversation_id = conversation_id\n                        AND msg_thread_stats.filter = filter)\n            ");
      str2 = localStringBuilder.toString();
      paramSQLiteDatabase.execSQL(str2);
      k += 1;
    }
    str1 = String.valueOf(str1);
    str1 = "DROP TABLE ".concat(str1);
    paramSQLiteDatabase.execSQL(str1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */