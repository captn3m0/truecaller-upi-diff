package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.truecaller.common.c.b.b;

public final class h
  implements ab
{
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    while (paramInt1 < paramInt2)
    {
      int i = 2;
      if (paramInt1 == i)
      {
        paramContext = "ALTER TABLE data ADD COLUMN data9 TEXT";
        paramSQLiteDatabase.execSQL(paramContext);
      }
      i = 5;
      if (paramInt1 == i)
      {
        paramContext = "ALTER TABLE data ADD COLUMN data10 TEXT";
        paramSQLiteDatabase.execSQL(paramContext);
      }
      i = 10;
      if (paramInt1 == i)
      {
        paramContext = "data";
        String str = "data_is_primary";
        boolean bool = b.b(paramSQLiteDatabase, paramContext, str);
        if (!bool)
        {
          paramSQLiteDatabase.execSQL("ALTER TABLE data ADD COLUMN data_is_primary INTEGER");
          paramSQLiteDatabase.execSQL("ALTER TABLE data ADD COLUMN data_phonebook_id INTEGER");
          paramContext = "UPDATE data SET data4 = -1 WHERE _id IN (SELECT data._id FROM raw_contact INNER JOIN data ON raw_contact._id = data.data_raw_contact_id WHERE data_type = 4 AND contact_source = 1)";
          paramSQLiteDatabase.execSQL(paramContext);
        }
      }
      paramInt1 += 1;
    }
  }
  
  public final String[] a()
  {
    String[] arrayOfString1 = new String[3];
    arrayOfString1[0] = "CREATE TABLE data(_id INTEGER PRIMARY KEY NOT NULL, data_raw_contact_id INTEGER NOT NULL REFERENCES raw_contact(_id) ON DELETE CASCADE ON UPDATE CASCADE,tc_id TEXT NOT NULL, data_type INTEGER NOT NULL, data_is_primary INTEGER, data_phonebook_id INTEGER, data1 TEXT, data2 TEXT, data3 TEXT, data4 TEXT, data5 TEXT, data6 TEXT, data7 TEXT, data8 TEXT, data9 TEXT, data10 TEXT)";
    String[] arrayOfString2 = { "data_raw_contact_id" };
    String str = b.b("data", arrayOfString2);
    arrayOfString1[1] = str;
    arrayOfString2 = new String[] { "data_type", "data1" };
    str = b.b("data", arrayOfString2);
    arrayOfString1[2] = str;
    return arrayOfString1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */