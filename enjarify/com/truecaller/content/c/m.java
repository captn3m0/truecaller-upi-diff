package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public final class m
  implements ab
{
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    String str = "context";
    k.b(paramContext, str);
    paramContext = "db";
    k.b(paramSQLiteDatabase, paramContext);
    int i = 57;
    if (paramInt1 < i)
    {
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS msg_im_users");
      paramSQLiteDatabase.execSQL("CREATE TABLE msg_im_users(_id INTEGER PRIMARY KEY AUTOINCREMENT,normalized_number TEXT NOT NULL, im_peer_id TEXT, date INTEGER NOT NULL, join_im_notification INTEGER NOT NULL DEFAULT 0, registration_timestamp INTEGER NOT NULL DEFAULT 0, UNIQUE(normalized_number) ON CONFLICT REPLACE)");
      paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_im_users_im_peer_id ON msg_im_users(im_peer_id)");
      str = "CREATE INDEX idx_msg_im_users_normalized_number ON msg_im_users(normalized_number)";
      paramSQLiteDatabase.execSQL(str);
    }
    paramInt2 = 73;
    if ((i <= paramInt1) && (paramInt2 >= paramInt1))
    {
      str = "ALTER TABLE msg_im_users ADD COLUMN join_im_notification INTEGER NOT NULL DEFAULT(0)";
      paramSQLiteDatabase.execSQL(str);
    }
    paramInt2 = 75;
    if (i > paramInt1) {
      return;
    }
    if (paramInt2 >= paramInt1)
    {
      paramSQLiteDatabase.execSQL("ALTER TABLE msg_im_users ADD COLUMN registration_timestamp INTEGER NOT NULL DEFAULT(0)");
      paramContext = "\n                UPDATE msg_im_users SET join_im_notification = 1\n                WHERE im_peer_id IS NOT NULL\n            ";
      paramSQLiteDatabase.execSQL(paramContext);
    }
  }
  
  public final String[] a()
  {
    String[] tmp4_1 = new String[3];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "CREATE TABLE msg_im_users(_id INTEGER PRIMARY KEY AUTOINCREMENT,normalized_number TEXT NOT NULL, im_peer_id TEXT, date INTEGER NOT NULL, join_im_notification INTEGER NOT NULL DEFAULT 0, registration_timestamp INTEGER NOT NULL DEFAULT 0, UNIQUE(normalized_number) ON CONFLICT REPLACE)";
    tmp5_4[1] = "CREATE INDEX idx_msg_im_users_im_peer_id ON msg_im_users(im_peer_id)";
    tmp5_4[2] = "CREATE INDEX idx_msg_im_users_normalized_number ON msg_im_users(normalized_number)";
    return tmp5_4;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */