package com.truecaller.content.c;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import com.truecaller.common.c.a.a.g;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import org.c.a.a.a.k;

public final class r
  implements a.g
{
  public final Cursor a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    parama1 = paramUri.getQueryParameter("filter");
    boolean bool = k.e(parama1);
    paramString1 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool, paramString1);
    paramUri = new java/lang/StringBuilder;
    paramUri.<init>("%");
    paramUri.append(parama1);
    paramUri.append("%");
    parama1 = paramUri.toString();
    parama = parama.c();
    paramString1 = new String[3];
    paramString1[0] = parama1;
    paramString1[1] = parama1;
    paramString1[2] = parama1;
    return parama.rawQuery("SELECT 1 as sort_order_key, * FROM msg_messages_with_entities WHERE me_entities_content LIKE (?) AND transport NOT IN (5, 6) UNION SELECT * FROM (SELECT 0 as sort_order_key, * FROM msg_messages_with_entities WHERE me_participant_name LIKE (?) OR me_participant_normalized_destination LIKE (?) ORDER BY date ASC ) GROUP BY conversation_id ORDER BY sort_order_key ASC, date DESC LIMIT 200", paramString1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */