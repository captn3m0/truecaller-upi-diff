package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public final class b
  implements ab
{
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramContext = "db";
    k.b(paramSQLiteDatabase, paramContext);
    int i = 39;
    if (paramInt1 < i)
    {
      paramContext = a();
      paramInt1 = paramContext.length;
      paramInt2 = 0;
      while (paramInt2 < paramInt1)
      {
        String str = paramContext[paramInt2];
        paramSQLiteDatabase.execSQL(str);
        paramInt2 += 1;
      }
    }
  }
  
  public final String[] a()
  {
    return new String[] { "CREATE TABLE call_recordings (_id INTEGER PRIMARY KEY AUTOINCREMENT, recording_path TEXT NOT NULL,history_event_id TEXT NOT NULL )", "CREATE INDEX IF NOT EXISTS call_recordings_history_event_id ON call_recordings(history_event_id)" };
  }
  
  public final String[] b()
  {
    return new String[] { "CREATE VIEW call_recordings_with_history_event AS SELECT * from history_with_aggregated_contact_number INNER JOIN call_recordings ON event_id=call_recordings.history_event_id" };
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */