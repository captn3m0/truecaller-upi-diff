package com.truecaller.content.c;

import java.util.Map;

final class w$h
  implements w.a, w.o, w.p
{
  public final String a;
  public final Object b;
  
  public w$h(String paramString, Object paramObject)
  {
    a = paramString;
    b = paramObject;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final void a(Map paramMap)
  {
    String str = a;
    Object localObject = b;
    paramMap.put(str, localObject);
  }
  
  public final Object b()
  {
    return b;
  }
  
  public final int c()
  {
    return 1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.w.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */