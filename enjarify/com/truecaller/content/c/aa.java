package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.common.c.b.b;

public final class aa
  implements ab
{
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    String str1 = "context";
    k.b(paramContext, str1);
    paramContext = "db";
    k.b(paramSQLiteDatabase, paramContext);
    int i = 36;
    if (paramInt1 < i)
    {
      String str2 = "t9_mapping";
      b.a(paramSQLiteDatabase, "table", str2);
      paramContext = a();
      paramInt1 = paramContext.length;
      paramInt2 = 0;
      str1 = null;
      while (paramInt2 < paramInt1)
      {
        String str3 = paramContext[paramInt2];
        paramSQLiteDatabase.execSQL(str3);
        paramInt2 += 1;
      }
    }
  }
  
  public final String[] a()
  {
    String[] arrayOfString1 = new String[4];
    arrayOfString1[0] = "\n    CREATE TABLE t9_mapping (\n        data_id INTEGER DEFAULT NULL,\n        raw_contact_id INTEGER NOT NULL,\n        t9_anywhere TEXT DEFAULT NULL,\n        t9_starts_with TEXT DEFAULT NULL,\n        matched_value TEXT DEFAULT NULL,\n        hit_priority INTEGER NOT NULL DEFAULT 0,\n        raw_contact_insert_timestamp INTEGER DEFAULT NULL)\n    ";
    String[] arrayOfString2 = { "t9_starts_with" };
    String str = b.b("t9_mapping", arrayOfString2);
    k.a(str, "ProviderUtil.getCreateIn…pingTable.T9_STARTS_WITH)");
    arrayOfString1[1] = str;
    arrayOfString2 = new String[] { "data_id" };
    str = b.b("t9_mapping", arrayOfString2);
    k.a(str, "ProviderUtil.getCreateIn…, T9MappingTable.DATA_ID)");
    arrayOfString1[2] = str;
    arrayOfString2 = new String[] { "raw_contact_id" };
    str = b.b("t9_mapping", arrayOfString2);
    k.a(str, "ProviderUtil.getCreateIn…pingTable.RAW_CONTACT_ID)");
    arrayOfString1[3] = str;
    return arrayOfString1;
  }
  
  public final String[] b()
  {
    return new String[] { "\n    CREATE VIEW contacts_with_call_count\n    AS SELECT history_aggregated_contact_id as aggregated_contact_id,\n        COUNT(*) as calls_count,\n        MAX(timestamp) as recent_call_timestamp\n        FROM history_with_aggregated_contact\n        WHERE type = 2\n            AND DATE(timestamp / 1000, 'unixepoch') > DATE('now', '-30 days')\n            AND history_aggregated_contact_id IS NOT NULL\n        GROUP BY history_aggregated_contact_id\n    " };
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */