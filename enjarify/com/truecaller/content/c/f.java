package com.truecaller.content.c;

import c.g.b.y;
import com.truecaller.common.c.b.b;
import com.truecaller.content.TruecallerContract.d;
import com.truecaller.content.TruecallerContract.f;
import java.util.ArrayList;
import java.util.Arrays;

public final class f
{
  private static final String a;
  private static final String b;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("\n    CREATE VIEW sorted_contacts_with_data AS\n        SELECT aggregated_contact_data.*,\n                ");
    Object localObject = TruecallerContract.f.a;
    int i = localObject.length;
    localObject = (String[])Arrays.copyOf((Object[])localObject, i);
    String str = b.a("contact_sorting_index", (String[])localObject);
    localStringBuilder.append(str);
    localStringBuilder.append(",\n                contact_settings.hidden_from_identified\n        FROM aggregated_contact_data\n        LEFT JOIN contact_sorting_index\n            ON aggregated_contact_data._id =\n                                                                contact_sorting_index.aggregated_contact_id\n        LEFT JOIN contact_settings\n            ON aggregated_contact_data.tc_id = contact_settings.tc_id\n        WHERE contact_name IS NOT NULL\n            AND contact_name NOT IN ('', 'Truecaller Verification')\n            AND contact_default_number NOT NULL\n            OR contact_phonebook_id IS NOT NULL\n    ");
    a = localStringBuilder.toString();
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("\n    CREATE VIEW sorted_contacts_shallow AS\n        SELECT\n            ");
    localObject = new c/g/b/y;
    ((y)localObject).<init>(3);
    ((y)localObject).b("_id");
    ((y)localObject).b("tc_id");
    String[] arrayOfString = TruecallerContract.d.b;
    ((y)localObject).a(arrayOfString);
    arrayOfString = new String[a.size()];
    localObject = (String[])((y)localObject).a(arrayOfString);
    str = b.a("aggregated_contact", (String[])localObject);
    localStringBuilder.append(str);
    localStringBuilder.append(",\n            ");
    localObject = TruecallerContract.f.a;
    i = localObject.length;
    localObject = (String[])Arrays.copyOf((Object[])localObject, i);
    str = b.a("contact_sorting_index", (String[])localObject);
    localStringBuilder.append(str);
    localStringBuilder.append(",\n            contact_settings.hidden_from_identified\n        FROM aggregated_contact\n        LEFT JOIN contact_sorting_index\n            ON aggregated_contact._id = contact_sorting_index.aggregated_contact_id\n        LEFT JOIN contact_settings\n            ON aggregated_contact.tc_id = contact_settings.tc_id\n        WHERE contact_name IS NOT NULL\n            AND contact_name NOT IN ('', 'Truecaller Verification')\n            AND contact_default_number NOT NULL\n            OR contact_phonebook_id IS NOT NULL\n    ");
    b = localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */