package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import c.g.b.y;
import com.truecaller.content.b.h;
import java.util.ArrayList;

public final class g
  implements ab
{
  private final h a;
  private final h b;
  private final h c;
  private final h d;
  private final h e;
  
  public g(h paramh1, h paramh2, h paramh3, h paramh4, h paramh5)
  {
    a = paramh1;
    b = paramh2;
    c = paramh3;
    d = paramh4;
    e = paramh5;
  }
  
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    String str = "context";
    k.b(paramContext, str);
    paramContext = "db";
    k.b(paramSQLiteDatabase, paramContext);
    int i = 11;
    if (paramInt1 <= i)
    {
      paramContext = a;
      paramContext.a(paramSQLiteDatabase, paramInt1);
    }
    i = 34;
    if (paramInt1 <= i)
    {
      paramContext = "ALTER TABLE msg_conversations ADD COLUMN hidden_number_status INTEGER DEFAULT (2)";
      paramSQLiteDatabase.execSQL(paramContext);
    }
    i = 46;
    if (paramInt1 <= i)
    {
      paramContext = "ALTER TABLE msg_conversations ADD COLUMN filter INTEGER DEFAULT (0)";
      paramSQLiteDatabase.execSQL(paramContext);
    }
    i = 58;
    if (paramInt1 <= i)
    {
      paramContext = b;
      paramContext.a(paramSQLiteDatabase, paramInt1);
    }
    i = 64;
    if (paramInt1 <= i)
    {
      paramContext = c;
      paramContext.a(paramSQLiteDatabase, paramInt1);
    }
    i = 66;
    if (paramInt1 <= i)
    {
      paramContext = d;
      paramContext.a(paramSQLiteDatabase, paramInt1);
    }
    i = 68;
    if (paramInt1 <= i)
    {
      paramContext = e;
      paramContext.a(paramSQLiteDatabase, paramInt1);
    }
    i = 71;
    if (paramInt1 <= i)
    {
      paramContext = "ALTER TABLE msg_conversations ADD COLUMN preferred_transport INTEGER DEFAULT (3)";
      paramSQLiteDatabase.execSQL(paramContext);
    }
  }
  
  public final String[] a()
  {
    return new String[] { "CREATE TABLE msg_conversations (_id INTEGER PRIMARY KEY AUTOINCREMENT, type INTEGER DEFAULT(0), tc_group_id TEXT, has_outgoing_messages INTEGER DEFAULT(0), phonebook_count INTEGER DEFAULT(0), white_list_count INTEGER DEFAULT(0), blacklist_count INTEGER DEFAULT(0), top_spammer_count INTEGER DEFAULT(0), has_spam_messages INTEGER DEFAULT(0), split_criteria INTEGER DEFAULT(0), preferred_transport INTEGER DEFAULT(3))" };
  }
  
  public final String[] b()
  {
    y localy = new c/g/b/y;
    localy.<init>(3);
    String[] tmp13_10 = new String[5];
    String[] tmp14_13 = tmp13_10;
    String[] tmp14_13 = tmp13_10;
    tmp14_13[0] = "\n            CREATE TRIGGER trigger_conversation_outgoing_message_count_on_message_insert\n                AFTER INSERT\n                ON msg_messages\n            BEGIN UPDATE msg_conversations\n                SET has_outgoing_messages = has_outgoing_messages + ((new.status & 3) = 1)\n                WHERE _id = new.conversation_id;\n            END\n        ";
    tmp14_13[1] = "\n            CREATE TRIGGER trigger_conversation_outgoing_message_count_on_message_update\n                AFTER UPDATE OF status\n                ON msg_messages\n            BEGIN UPDATE msg_conversations\n                SET has_outgoing_messages = has_outgoing_messages - ((old.status & 3) = 1) + ((new.status & 3) = 1)\n                WHERE _id = new.conversation_id;\n            END\n        ";
    String[] tmp23_14 = tmp14_13;
    String[] tmp23_14 = tmp14_13;
    tmp23_14[2] = "\n            CREATE TRIGGER trigger_conversation_outgoing_message_count_on_message_delete\n                AFTER DELETE\n                ON msg_messages\n            BEGIN UPDATE msg_conversations\n                SET has_outgoing_messages = has_outgoing_messages - ((old.status & 3) = 1)\n                WHERE _id = old.conversation_id;\n            END\n        ";
    tmp23_14[3] = "\n            CREATE TRIGGER trigger_conversation_delete_on_message_delete\n                AFTER DELETE\n                ON msg_messages\n                WHEN (SELECT NOT EXISTS (\n                    SELECT 1\n                    FROM msg_messages\n                    WHERE conversation_id = old.conversation_id))\n            BEGIN DELETE FROM msg_conversations\n                WHERE _id = old.conversation_id;\n            END\n        ";
    tmp23_14[4] = "\n            CREATE TRIGGER trigger_conversation_delete_on_message_conversation_id_update\n                AFTER UPDATE OF conversation_id\n                ON msg_messages\n                WHEN (SELECT NOT EXISTS (\n                    SELECT 1\n                    FROM msg_messages\n                    WHERE conversation_id = old.conversation_id))\n            BEGIN DELETE FROM msg_conversations\n                WHERE _id = old.conversation_id;\n            END\n        ";
    String[] arrayOfString = tmp23_14;
    localy.a(arrayOfString);
    String[] tmp46_43 = new String[3];
    String[] tmp47_46 = tmp46_43;
    String[] tmp47_46 = tmp46_43;
    tmp47_46[0] = "\n            CREATE TRIGGER trigger_conversation_counts_on_conversation_participant_insert\n                AFTER INSERT\n                ON msg_conversation_participants\n            BEGIN UPDATE msg_conversations\n                SET phonebook_count = phonebook_count + (SELECT phonebook_count\n                        FROM msg_participants\n                        WHERE _id = new.participant_id),\n                    blacklist_count = blacklist_count + (SELECT filter_action = 1\n                        FROM msg_participants\n                        WHERE _id = new.participant_id),\n                    white_list_count = white_list_count + (SELECT filter_action = 2\n                        FROM msg_participants\n                        WHERE _id = new.participant_id),\n                    top_spammer_count = top_spammer_count + (SELECT is_top_spammer\n                    FROM msg_participants\n                    WHERE _id = new.participant_id)\n                WHERE _id = new.conversation_id;\n            END\n        ";
    tmp47_46[1] = "\n            CREATE TRIGGER trigger_conversation_counts_on_participant_update\n                AFTER UPDATE OF phonebook_count, filter_action, is_top_spammer\n                ON msg_participants\n            BEGIN UPDATE msg_conversations\n                SET phonebook_count = phonebook_count - old.phonebook_count + new.phonebook_count,\n                    blacklist_count = blacklist_count - (old.filter_action = 1) + (new.filter_action = 1),\n                    white_list_count = white_list_count - (old.filter_action = 2) + (new.filter_action = 2),\n                    top_spammer_count = top_spammer_count - old.is_top_spammer + new.is_top_spammer\n                WHERE _id IN (SELECT conversation_id\n                    FROM msg_conversation_participants\n                    WHERE participant_id = new._id);\n            END\n        ";
    tmp47_46[2] = "\n            CREATE TRIGGER trigger_conversation_counts_on_conversation_participant_delete\n                AFTER DELETE\n                ON msg_conversation_participants\n            BEGIN UPDATE msg_conversations\n                SET phonebook_count = phonebook_count - (SELECT phonebook_count\n                        FROM msg_participants\n                        WHERE _id = old.participant_id),\n                    blacklist_count = blacklist_count - (SELECT filter_action = 1\n                        FROM msg_participants\n                        WHERE _id = old.participant_id),\n                    white_list_count = white_list_count - (SELECT filter_action = 2\n                        FROM msg_participants\n                        WHERE _id = old.participant_id),\n                    top_spammer_count = top_spammer_count - (SELECT is_top_spammer\n                        FROM msg_participants\n                        WHERE _id = old.participant_id)\n                WHERE _id = old.conversation_id;\n            END\n        ";
    arrayOfString = tmp47_46;
    localy.a(arrayOfString);
    arrayOfString = new String[] { "\n            CREATE TRIGGER trigger_conversation_split_criteria_on_conversation_insert\n                AFTER INSERT\n                ON msg_conversations\n            BEGIN UPDATE msg_conversations\n                SET split_criteria = CASE new.phonebook_count OR new.white_list_count OR new.blacklist_count OR new.has_outgoing_messages OR new.tc_group_id IS NOT NULL\n                    WHEN 1 THEN 0\n                    ELSE 1 END\n                WHERE _id = new._id;\n            END\n        ", "\n            CREATE TRIGGER trigger_conversation_split_criteria_on_conversation_update\n                AFTER UPDATE OF phonebook_count, white_list_count, blacklist_count, has_outgoing_messages, tc_group_id\n                ON msg_conversations\n            BEGIN UPDATE msg_conversations\n                SET split_criteria = CASE new.phonebook_count OR white_list_count OR blacklist_count OR has_outgoing_messages OR new.tc_group_id IS NOT NULL\n                    WHEN 1 THEN 0\n                    ELSE 1 END\n                WHERE _id = new._id;\n            END\n        " };
    localy.a(arrayOfString);
    arrayOfString = new String[a.size()];
    return (String[])localy.a(arrayOfString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */