package com.truecaller.content.c;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.a.a.a.f;
import com.a.a.aa;
import com.truecaller.content.a.a;
import java.util.Iterator;

final class c
  implements ab
{
  private static String a(String paramString, com.a.a.ab paramab)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    Object localObject = "UPDATE ";
    localStringBuilder.<init>((String)localObject);
    localStringBuilder.append(paramString);
    localStringBuilder.append(" SET contact_badges = CASE _id");
    paramString = new java/lang/StringBuilder;
    paramString.<init>();
    paramab = paramab.iterator();
    for (;;)
    {
      boolean bool = paramab.hasNext();
      if (!bool) {
        break;
      }
      localObject = (f)paramab.next();
      long l = b;
      int i = c;
      localStringBuilder.append(" WHEN ");
      localStringBuilder.append(l);
      String str = " THEN ";
      localStringBuilder.append(str);
      localStringBuilder.append(i);
      paramString.append(l);
      localObject = ",";
      paramString.append((String)localObject);
    }
    int j = paramString.length();
    if (j > 0)
    {
      j = paramString.length() + -1;
      paramString.setLength(j);
    }
    localStringBuilder.append(" END WHERE _id IN (");
    localStringBuilder.append(paramString);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    Object localObject1 = paramSQLiteDatabase;
    int i = paramInt1;
    int j = 12;
    Object localObject2;
    if (paramInt1 < j)
    {
      paramSQLiteDatabase.execSQL("UPDATE raw_contact SET contact_spam_score=(SELECT MAX(data3) FROM data WHERE data_raw_contact_id=raw_contact._id AND  data_type=4)");
      localObject2 = "UPDATE aggregated_contact SET contact_spam_score=(SELECT MAX(contact_spam_score) FROM raw_contact WHERE aggregated_contact_id=aggregated_contact._id)";
      paramSQLiteDatabase.execSQL((String)localObject2);
    }
    j = 15;
    if (i < j)
    {
      long l1 = System.currentTimeMillis();
      Object localObject3 = ((SQLiteDatabase)localObject1).rawQuery("SELECT data_raw_contact_id AS raw_id, r.aggregated_contact_id,  data1 FROM data LEFT JOIN raw_contact r ON data_raw_contact_id = r._id WHERE data_type = 2 ORDER BY raw_id", null);
      int k;
      float f1;
      String str1;
      if (localObject3 == null)
      {
        k = 0;
        f1 = 0.0F;
        str1 = null;
      }
      else
      {
        k = ((Cursor)localObject3).getCount();
      }
      if (k > 0)
      {
        aa localaa1 = new com/a/a/aa;
        localaa1.<init>(k);
        aa localaa2 = new com/a/a/aa;
        localaa2.<init>(k);
        int i2;
        int i1;
        for (;;)
        {
          boolean bool1 = ((Cursor)localObject3).moveToNext();
          int n = 2;
          i2 = 1;
          if (!bool1) {
            break;
          }
          long l2 = ((Cursor)localObject3).getLong(0);
          bool1 = ((Cursor)localObject3).isNull(i2);
          long l3 = -1;
          long l4;
          if (bool1) {
            l4 = l3;
          } else {
            l4 = ((Cursor)localObject3).getLong(i2);
          }
          str1 = ((Cursor)localObject3).getString(n);
          m = a.a(str1);
          n = localaa1.a(l2) | m;
          localaa1.a(l2, n);
          boolean bool2 = l4 < l3;
          if (bool2)
          {
            i1 = localaa2.a(l4);
            m |= i1;
            localaa2.a(l4, m);
          }
        }
        ((Cursor)localObject3).close();
        boolean bool3 = localaa1.a();
        if (!bool3)
        {
          localObject3 = a("raw_contact", localaa1);
          ((SQLiteDatabase)localObject1).execSQL((String)localObject3);
        }
        bool3 = localaa2.a();
        if (!bool3)
        {
          localObject3 = a("aggregated_contact", localaa2);
          ((SQLiteDatabase)localObject1).execSQL((String)localObject3);
        }
        ((SQLiteDatabase)localObject1).execSQL("DELETE FROM data WHERE data_type = 2");
        long l5 = System.currentTimeMillis() - l1;
        localObject1 = new String[i2];
        localObject2 = new Object[i1];
        localObject3 = Long.valueOf(l5);
        localObject2[0] = localObject3;
        float f2 = (float)l5;
        int m = 1148846080;
        f1 = 1000.0F;
        f2 /= f1;
        localObject3 = Float.valueOf(f2);
        localObject2[i2] = localObject3;
        String str2 = String.format("Migrating badges took %d millis (%.2f seconds)", (Object[])localObject2);
        localObject1[0] = str2;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */