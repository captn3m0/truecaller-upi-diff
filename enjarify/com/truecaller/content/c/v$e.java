package com.truecaller.content.c;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

final class v$e
  implements v.a, w.o, w.p
{
  public final String a;
  public final Object b;
  
  public v$e(String paramString, Object paramObject)
  {
    a = paramString;
    b = paramObject;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase, long paramLong)
  {
    int i = v.h.a(b);
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    String str1 = a;
    localContentValues.put("key", str1);
    Object localObject1 = Long.valueOf(paramLong);
    localContentValues.put("time", (Long)localObject1);
    Object localObject2 = Integer.valueOf(i);
    localContentValues.put("type", (Integer)localObject2);
    localObject2 = b;
    localObject2 = v.h.a(i, localObject2);
    localContentValues.put("value", (byte[])localObject2);
    localObject1 = "preferences";
    localObject2 = "[key] = ?";
    i = 1;
    String[] arrayOfString = new String[i];
    String str2 = a;
    str1 = null;
    arrayOfString[0] = str2;
    int j = paramSQLiteDatabase.update((String)localObject1, localContentValues, (String)localObject2, arrayOfString);
    if (j == 0)
    {
      localObject1 = "preferences";
      localObject2 = null;
      paramSQLiteDatabase.insert((String)localObject1, null, localContentValues);
    }
  }
  
  public final Object b()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.v.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */