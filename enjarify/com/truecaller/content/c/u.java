package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.g.b.y;
import com.truecaller.content.aa;
import com.truecaller.content.aa.a;
import java.util.ArrayList;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class u
  implements ab
{
  public aa a;
  
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    Object localObject1 = "context";
    k.b(paramContext, (String)localObject1);
    paramContext = "db";
    k.b(paramSQLiteDatabase, paramContext);
    int i = 12;
    if (paramInt1 < i)
    {
      paramSQLiteDatabase.execSQL("CREATE TABLE msg_participants (_id INTEGER PRIMARY KEY AUTOINCREMENT, type INTEGER DEFAULT(0), tc_im_peer_id TEXT, raw_destination TEXT DEFAULT(''), normalized_destination TEXT DEFAULT(''), country_code TEXT DEFAULT('') COLLATE NOCASE, aggregated_contact_id INTEGER NOT NULL DEFAULT(-1), filter_action INTEGER DEFAULT(0), is_top_spammer INTEGER DEFAULT(0),top_spam_score INTEGER DEFAULT(0),phonebook_count INTEGER DEFAULT(0), UNIQUE(normalized_destination) ON CONFLICT FAIL)");
      paramSQLiteDatabase.execSQL("CREATE INDEX msg_participants_normalized_destination_idx ON msg_participants(normalized_destination)");
      paramSQLiteDatabase.execSQL("CREATE TABLE msg_conversation_participants(participant_id INTEGER NOT NULL,filter INTEGER DEFAULT (0),conversation_id INTEGER NOT NULL REFERENCES msg_conversations (_id) ON DELETE CASCADE, UNIQUE(participant_id, conversation_id) ON CONFLICT REPLACE)");
      paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_conversation_participants_conversation_id ON msg_conversation_participants(conversation_id)");
      localObject1 = "CREATE INDEX idx_msg_conversation_participants_participant_id ON msg_conversation_participants(participant_id)";
      paramSQLiteDatabase.execSQL((String)localObject1);
    }
    paramInt2 = 14;
    if (paramInt1 < paramInt2)
    {
      localObject1 = "UPDATE msg_participants SET type=0 WHERE type=1 AND substr(normalized_destination,1,1)='+' ";
      paramSQLiteDatabase.execSQL((String)localObject1);
    }
    paramInt2 = 46;
    if ((i <= paramInt1) && (paramInt2 >= paramInt1))
    {
      localObject1 = "ALTER TABLE msg_conversation_participants ADD COLUMN filter INTEGER DEFAULT (0)";
      paramSQLiteDatabase.execSQL((String)localObject1);
    }
    paramInt2 = 51;
    if ((i <= paramInt1) && (paramInt2 >= paramInt1))
    {
      localObject1 = com.truecaller.common.b.a.F();
      Object localObject2 = "ApplicationBase.getAppBase()";
      k.a(localObject1, (String)localObject2);
      ((com.truecaller.common.b.a)localObject1).u().a(this);
      paramSQLiteDatabase.execSQL("ALTER TABLE msg_participants ADD COLUMN country_code TEXT DEFAULT ('') COLLATE NOCASE");
      localObject1 = a;
      if (localObject1 == null)
      {
        localObject2 = "participantsUpdateHelper";
        k.a((String)localObject2);
      }
      localObject2 = (ag)bg.a;
      f localf = c;
      Object localObject3 = new com/truecaller/content/aa$a;
      ((aa.a)localObject3).<init>((aa)localObject1, null);
      localObject3 = (m)localObject3;
      paramInt2 = 2;
      e.b((ag)localObject2, localf, (m)localObject3, paramInt2);
    }
    paramInt2 = 64;
    if (i > paramInt1) {
      return;
    }
    if (paramInt2 >= paramInt1)
    {
      paramSQLiteDatabase.execSQL("ALTER TABLE msg_participants ADD COLUMN phonebook_count INTEGER DEFAULT (0)");
      paramContext = "\n                UPDATE msg_participants\n                    SET phonebook_count = (SELECT COUNT(*)\n                        FROM data\n                        WHERE data_type = 4\n                            AND data_phonebook_id NOT NULL\n                            AND data1 = normalized_destination)\n            ";
      paramSQLiteDatabase.execSQL(paramContext);
    }
  }
  
  public final String[] a()
  {
    String[] tmp4_1 = new String[5];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "CREATE TABLE msg_participants (_id INTEGER PRIMARY KEY AUTOINCREMENT, type INTEGER DEFAULT(0), tc_im_peer_id TEXT, raw_destination TEXT DEFAULT(''), normalized_destination TEXT DEFAULT(''), country_code TEXT DEFAULT('') COLLATE NOCASE, aggregated_contact_id INTEGER NOT NULL DEFAULT(-1), filter_action INTEGER DEFAULT(0), is_top_spammer INTEGER DEFAULT(0),top_spam_score INTEGER DEFAULT(0),phonebook_count INTEGER DEFAULT(0), UNIQUE(normalized_destination) ON CONFLICT FAIL)";
    tmp5_4[1] = "CREATE INDEX msg_participants_normalized_destination_idx ON msg_participants(normalized_destination)";
    String[] tmp14_5 = tmp5_4;
    String[] tmp14_5 = tmp5_4;
    tmp14_5[2] = "CREATE TABLE msg_conversation_participants(participant_id INTEGER NOT NULL,filter INTEGER DEFAULT (0),conversation_id INTEGER NOT NULL REFERENCES msg_conversations (_id) ON DELETE CASCADE, UNIQUE(participant_id, conversation_id) ON CONFLICT REPLACE)";
    tmp14_5[3] = "CREATE INDEX idx_msg_conversation_participants_conversation_id ON msg_conversation_participants(conversation_id)";
    tmp14_5[4] = "CREATE INDEX idx_msg_conversation_participants_participant_id ON msg_conversation_participants(participant_id)";
    return tmp14_5;
  }
  
  public final String[] b()
  {
    y localy = new c/g/b/y;
    localy.<init>(2);
    localy.b("CREATE VIEW msg_participants_with_contact_info AS SELECT p._id AS _id,cp.conversation_id AS conversation_id, p.type AS type,p.raw_destination AS raw_destination,NULL as national_destination, p.normalized_destination AS normalized_destination,p.country_code AS country_code,p.tc_im_peer_id AS tc_im_peer_id,p.aggregated_contact_id AS aggregated_contact_id,a.tc_id AS tc_id,p.filter_action AS filter_action,p.is_top_spammer AS is_top_spammer,p.top_spam_score AS top_spam_score,a.contact_name AS name,a.contact_image_url AS image_url,a.contact_source AS source,a.contact_badges AS badges,a.contact_company AS company_name,a.contact_search_time AS search_time,IFNULL(a.contact_phonebook_id,-1) AS phonebook_id,MAX(IFNULL(a.contact_spam_score, 0), top_spam_score) AS spam_score FROM msg_participants p LEFT JOIN msg_conversation_participants cp ON p._id = cp.participant_id LEFT JOIN aggregated_contact a ON a._id = p.aggregated_contact_id");
    String[] tmp19_16 = new String[3];
    String[] tmp20_19 = tmp19_16;
    String[] tmp20_19 = tmp19_16;
    tmp20_19[0] = "\n            CREATE TRIGGER trigger_participant_phonebook_count_on_participant_insert\n                AFTER INSERT\n                ON msg_participants\n            BEGIN UPDATE msg_participants\n                SET phonebook_count = (SELECT COUNT(*)\n                    FROM data\n                    WHERE data_type = 4\n                        AND data1 = new.normalized_destination\n                        AND data_phonebook_id NOT NULL)\n                WHERE _id = new._id;\n            END\n        ";
    tmp20_19[1] = "\n            CREATE TRIGGER trigger_participant_phonebook_count_on_data_insert\n                AFTER INSERT\n                ON data\n                WHEN new.data_type = 4 AND new.data_phonebook_id NOT NULL\n            BEGIN UPDATE msg_participants\n                SET phonebook_count = phonebook_count + 1\n                WHERE normalized_destination = new.data1;\n            END\n        ";
    tmp20_19[2] = "\n            CREATE TRIGGER trigger_participant_phonebook_count_on_data_delete\n                AFTER DELETE\n                ON data\n                WHEN old.data_type = 4 AND old.data_phonebook_id NOT NULL\n            BEGIN UPDATE msg_participants\n                SET phonebook_count = phonebook_count - 1\n                WHERE normalized_destination = old.data1;\n            END\n        ";
    String[] arrayOfString = tmp20_19;
    localy.a(arrayOfString);
    arrayOfString = new String[a.size()];
    return (String[])localy.a(arrayOfString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */