package com.truecaller.content.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.truecaller.log.f;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public final class v
  implements w.c, w.l
{
  private static final v.g a;
  private static final AtomicInteger b;
  private static final AtomicInteger c;
  private static final Map d;
  private static final HashMap e;
  private final v.d f;
  private long g;
  
  static
  {
    Object localObject = new com/truecaller/content/c/v$g;
    ((v.g)localObject).<init>((byte)0);
    a = (v.g)localObject;
    localObject = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject).<init>();
    b = (AtomicInteger)localObject;
    localObject = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject).<init>();
    c = (AtomicInteger)localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    d = (Map)localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    e = (HashMap)localObject;
  }
  
  private v(Context paramContext, String paramString)
  {
    Object localObject = new java/io/File;
    ((File)localObject).<init>(paramString);
    paramString = ((File)localObject).getName();
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append(".s3db");
    paramString = ((StringBuilder)localObject).toString();
    localObject = new com/truecaller/content/c/v$d;
    ((v.d)localObject).<init>(paramContext, paramString);
    f = ((v.d)localObject);
  }
  
  public static SharedPreferences a(Context paramContext, String paramString)
  {
    v localv = new com/truecaller/content/c/v;
    localv.<init>(paramContext, paramString);
    w localw = new com/truecaller/content/c/w;
    localw.<init>(paramContext, paramString, localv);
    paramContext = b;
    paramString = w.c;
    paramContext.put(localv, paramString);
    return localw;
  }
  
  private Map e()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    long l1 = g;
    SQLiteDatabase localSQLiteDatabase = f.getReadableDatabase();
    Object localObject1 = String.valueOf(l1);
    String str1 = "preferences";
    String str2 = "[time] > ?";
    Object localObject2 = new String[1];
    localObject2[0] = localObject1;
    int i = 0;
    String str3 = null;
    boolean bool1 = false;
    String str4 = null;
    int j = 0;
    Object localObject3 = null;
    byte[] arrayOfByte = null;
    localObject1 = localSQLiteDatabase;
    localObject1 = localSQLiteDatabase.query(str1, null, str2, (String[])localObject2, null, null, null);
    if (localObject1 != null) {
      try
      {
        boolean bool2 = ((Cursor)localObject1).moveToFirst();
        if (bool2)
        {
          str1 = "key";
          int k = ((Cursor)localObject1).getColumnIndex(str1);
          str3 = "type";
          i = ((Cursor)localObject1).getColumnIndex(str3);
          str2 = "value";
          int m = ((Cursor)localObject1).getColumnIndex(str2);
          localObject2 = "time";
          int n = ((Cursor)localObject1).getColumnIndex((String)localObject2);
          do
          {
            str4 = ((Cursor)localObject1).getString(k);
            j = ((Cursor)localObject1).getInt(i);
            arrayOfByte = ((Cursor)localObject1).getBlob(m);
            long l2 = ((Cursor)localObject1).getLong(n);
            l1 = Math.max(l1, l2);
            localObject3 = v.h.a(j, arrayOfByte);
            localHashMap.put(str4, localObject3);
            bool1 = ((Cursor)localObject1).moveToNext();
          } while (bool1);
        }
      }
      finally
      {
        if (localObject1 != null) {
          ((Cursor)localObject1).close();
        }
      }
    }
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    localSQLiteDatabase.close();
    l1 = System.nanoTime();
    g = l1;
    return localMap;
  }
  
  public final void a(Queue paramQueue)
  {
    v.c localc = new com/truecaller/content/c/v$c;
    Object localObject1 = null;
    localc.<init>((byte)0);
    paramQueue = paramQueue.iterator();
    Object localObject3;
    for (;;)
    {
      boolean bool = paramQueue.hasNext();
      if (!bool) {
        break;
      }
      ??? = (w.a)paramQueue.next();
      int i = ((w.a)???).c();
      int j = 4;
      if (i != j)
      {
        Object localObject4;
        switch (i)
        {
        default: 
          break;
        case 2: 
          localObject3 = b;
          localObject4 = new com/truecaller/content/c/v$f;
          ??? = ((w.o)???).a();
          ((v.f)localObject4).<init>((String)???);
          ((Queue)localObject3).add(localObject4);
          break;
        case 1: 
          localObject3 = b;
          localObject4 = new com/truecaller/content/c/v$e;
          Object localObject5 = ???;
          localObject5 = ((w.o)???).a();
          ??? = ((w.p)???).b();
          ((v.e)localObject4).<init>((String)localObject5, ???);
          ((Queue)localObject3).add(localObject4);
          break;
        }
      }
      else
      {
        b.clear();
        ??? = b;
        localObject3 = new com/truecaller/content/c/v$b;
        ((v.b)localObject3).<init>((byte)0);
        ((Queue)???).add(localObject3);
      }
    }
    aa.incrementAndGet();
    paramQueue = f.a;
    localObject1 = (Queue)d.get(paramQueue);
    if (localObject1 == null) {
      synchronized (d)
      {
        localObject1 = d;
        localObject1 = ((Map)localObject1).get(paramQueue);
        localObject1 = (Queue)localObject1;
        if (localObject1 == null)
        {
          localObject1 = d;
          localObject3 = new java/util/concurrent/ConcurrentLinkedQueue;
          ((ConcurrentLinkedQueue)localObject3).<init>();
          ((Map)localObject1).put(paramQueue, localObject3);
          localObject1 = localObject3;
        }
      }
    }
    ((Queue)localObject1).offer(localc);
  }
  
  public final byte[] a()
  {
    try
    {
      ??? = f;
      Object localObject4;
      int i;
      int j;
      synchronized (b)
      {
        Object localObject2 = a;
        localObject2 = c;
        ((AtomicInteger)localObject2).incrementAndGet();
        localObject2 = f;
        localObject2 = ((v.d)localObject2).getWritableDatabase();
        localObject4 = f;
        localObject4 = a;
        Object localObject5 = d;
        localObject4 = ((Map)localObject5).get(localObject4);
        localObject4 = (Queue)localObject4;
        ((SQLiteDatabase)localObject2).beginTransaction();
        for (;;)
        {
          localObject5 = ((Queue)localObject4).poll();
          localObject5 = (v.c)localObject5;
          if (localObject5 == null) {
            break;
          }
          for (;;)
          {
            Object localObject6 = b;
            localObject6 = ((Queue)localObject6).poll();
            localObject6 = (v.a)localObject6;
            if (localObject6 == null) {
              break;
            }
            long l = a;
            ((v.a)localObject6).a((SQLiteDatabase)localObject2, l);
            localObject6 = a;
            localObject6 = b;
            ((AtomicInteger)localObject6).incrementAndGet();
          }
          localObject5 = c;
          ((AtomicInteger)localObject5).incrementAndGet();
        }
        ((SQLiteDatabase)localObject2).setTransactionSuccessful();
        ((SQLiteDatabase)localObject2).endTransaction();
        ((SQLiteDatabase)localObject2).close();
        localObject2 = b;
        i = ((AtomicInteger)localObject2).get();
        localObject4 = c;
        j = ((AtomicInteger)localObject4).get();
        if (i != j)
        {
          localObject2 = new com/truecaller/content/c/w$j;
          localObject4 = "Reschedule of synchronization job is required. Expected version does not match current.";
          ((w.j)localObject2).<init>((String)localObject4);
          throw ((Throwable)localObject2);
        }
      }
      String[] arrayOfString;
      String str;
      return null;
    }
    finally
    {
      i = 1;
      arrayOfString = new String[i];
      j = 0;
      localObject4 = null;
      str = f.a(localThrowable);
      arrayOfString[0] = str;
    }
  }
  
  public final Map b()
  {
    try
    {
      ??? = f;
      synchronized (b)
      {
        Map localMap = e();
        return localMap;
      }
      String[] arrayOfString;
      String str;
      return null;
    }
    finally
    {
      arrayOfString = new String[1];
      str = f.a(localThrowable);
      arrayOfString[0] = str;
    }
  }
  
  public final void c()
  {
    b.incrementAndGet();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */