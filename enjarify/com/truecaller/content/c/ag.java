package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.truecaller.analytics.e.a;
import com.truecaller.content.b.f;
import com.truecaller.log.AssertionUtil;

public final class ag
  extends SQLiteOpenHelper
{
  private static ag b;
  com.truecaller.analytics.b a;
  private final Context c;
  private final ab[] d;
  private final c e;
  private final ac f;
  
  private ag(Context paramContext, ab[] paramArrayOfab, com.truecaller.analytics.b paramb)
  {
    super(paramContext, "tc.db", null, 111);
    paramContext = paramContext.getApplicationContext();
    c = paramContext;
    d = paramArrayOfab;
    paramContext = new com/truecaller/content/c/c;
    paramContext.<init>();
    e = paramContext;
    a = paramb;
    paramContext = new com/truecaller/content/c/ad;
    paramContext.<init>();
    f = paramContext;
  }
  
  public static ag a(Context paramContext, ab[] paramArrayOfab, com.truecaller.analytics.b paramb)
  {
    synchronized (ag.class)
    {
      ag localag = b;
      if (localag == null)
      {
        localag = new com/truecaller/content/c/ag;
        localag.<init>(paramContext, paramArrayOfab, paramb);
        b = localag;
      }
      paramContext = b;
      return paramContext;
    }
  }
  
  public static void a()
  {
    synchronized (ag.class)
    {
      com.truecaller.common.c.c.b();
      Object localObject1 = null;
      b = null;
      return;
    }
  }
  
  private void a(SQLiteDatabase paramSQLiteDatabase)
  {
    ab[] arrayOfab = d;
    int i = arrayOfab.length;
    int j = 0;
    while (j < i)
    {
      String[] arrayOfString = arrayOfab[j].b();
      int k = arrayOfString.length;
      int m = 0;
      while (m < k)
      {
        String str = arrayOfString[m];
        paramSQLiteDatabase.execSQL(str);
        m += 1;
      }
      j += 1;
    }
  }
  
  public static ab[] b()
  {
    ab[] arrayOfab = new ab[19];
    Object localObject1 = new com/truecaller/content/c/a;
    ((a)localObject1).<init>();
    arrayOfab[0] = localObject1;
    localObject1 = new com/truecaller/content/c/y;
    ((y)localObject1).<init>();
    arrayOfab[1] = localObject1;
    localObject1 = new com/truecaller/content/c/h;
    ((h)localObject1).<init>();
    arrayOfab[2] = localObject1;
    localObject1 = new com/truecaller/content/c/j;
    ((j)localObject1).<init>();
    arrayOfab[3] = localObject1;
    localObject1 = new com/truecaller/content/c/u;
    ((u)localObject1).<init>();
    arrayOfab[4] = localObject1;
    localObject1 = new com/truecaller/content/c/n;
    Object localObject2 = new com/truecaller/content/b/f;
    ((f)localObject2).<init>();
    ((n)localObject1).<init>((f)localObject2);
    arrayOfab[5] = localObject1;
    localObject1 = new com/truecaller/content/c/g;
    com.truecaller.content.b.a locala = new com/truecaller/content/b/a;
    locala.<init>();
    com.truecaller.content.b.b localb = new com/truecaller/content/b/b;
    localb.<init>();
    com.truecaller.content.b.c localc = new com/truecaller/content/b/c;
    localc.<init>();
    com.truecaller.content.b.d locald = new com/truecaller/content/b/d;
    locald.<init>();
    com.truecaller.content.b.e locale = new com/truecaller/content/b/e;
    locale.<init>();
    Object localObject3 = localObject1;
    ((g)localObject1).<init>(locala, localb, localc, locald, locale);
    arrayOfab[6] = localObject1;
    localObject1 = new com/truecaller/content/c/i;
    ((i)localObject1).<init>();
    arrayOfab[7] = localObject1;
    localObject1 = new com/truecaller/content/c/af;
    ((af)localObject1).<init>();
    arrayOfab[8] = localObject1;
    localObject1 = new com/truecaller/content/c/m;
    ((m)localObject1).<init>();
    arrayOfab[9] = localObject1;
    localObject1 = new com/truecaller/content/c/k;
    ((k)localObject1).<init>();
    arrayOfab[10] = localObject1;
    localObject1 = new com/truecaller/content/c/aa;
    ((aa)localObject1).<init>();
    arrayOfab[11] = localObject1;
    localObject1 = new com/truecaller/content/c/b;
    ((b)localObject1).<init>();
    arrayOfab[12] = localObject1;
    localObject1 = new com/truecaller/content/c/x;
    ((x)localObject1).<init>();
    arrayOfab[13] = localObject1;
    localObject1 = new com/truecaller/content/c/z;
    ((z)localObject1).<init>();
    arrayOfab[14] = localObject1;
    localObject1 = new com/truecaller/content/c/e;
    ((e)localObject1).<init>();
    arrayOfab[15] = localObject1;
    localObject1 = new com/truecaller/content/c/ae;
    localObject2 = new com/truecaller/content/c/ad;
    ((ad)localObject2).<init>();
    localObject3 = new com/truecaller/content/b/a/a;
    ((com.truecaller.content.b.a.a)localObject3).<init>();
    ((ae)localObject1).<init>((ac)localObject2, (com.truecaller.content.b.h)localObject3);
    arrayOfab[16] = localObject1;
    localObject1 = new com/truecaller/content/c/l;
    ((l)localObject1).<init>();
    arrayOfab[17] = localObject1;
    localObject1 = new com/truecaller/content/c/d;
    ((d)localObject1).<init>();
    arrayOfab[18] = localObject1;
    return arrayOfab;
  }
  
  public final void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    ab[] arrayOfab = d;
    int i = arrayOfab.length;
    int j = 0;
    while (j < i)
    {
      String[] arrayOfString = arrayOfab[j].a();
      int k = arrayOfString.length;
      int m = 0;
      while (m < k)
      {
        String str = arrayOfString[m];
        paramSQLiteDatabase.execSQL(str);
        m += 1;
      }
      j += 1;
    }
    a(paramSQLiteDatabase);
  }
  
  public final void onOpen(SQLiteDatabase paramSQLiteDatabase)
  {
    super.onOpen(paramSQLiteDatabase);
    com.truecaller.common.c.b.b.a(paramSQLiteDatabase);
    com.truecaller.common.c.b.b.b(paramSQLiteDatabase);
  }
  
  public final void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    Object localObject1 = "view";
    try
    {
      com.truecaller.common.c.b.b.a(paramSQLiteDatabase, (String)localObject1);
      localObject1 = "trigger";
      com.truecaller.common.c.b.b.a(paramSQLiteDatabase, (String)localObject1);
      int i = 85;
      if (paramInt1 < i)
      {
        Object localObject2 = d;
        int j = localObject2.length;
        int k = 0;
        while (k < j)
        {
          Object localObject3 = localObject2[k];
          Context localContext = c;
          ((ab)localObject3).a(localContext, paramSQLiteDatabase, paramInt1, paramInt2);
          k += 1;
        }
        m = 12;
        if (paramInt1 < m)
        {
          localObject2 = c;
          localObject4 = "filterDatabase";
          ((Context)localObject2).deleteDatabase((String)localObject4);
        }
        localObject2 = e;
        Object localObject4 = c;
        ((c)localObject2).a((Context)localObject4, paramSQLiteDatabase, paramInt1, paramInt2);
      }
      int m = paramInt1 + 1;
      i = Math.max(m, i);
      while (i <= paramInt2)
      {
        com.truecaller.content.b.j.a(i, paramSQLiteDatabase);
        i += 1;
      }
      a(paramSQLiteDatabase);
      i = 100;
      if (paramInt1 < i)
      {
        localObject1 = f;
        ((ac)localObject1).a(paramSQLiteDatabase);
      }
      return;
    }
    catch (RuntimeException paramSQLiteDatabase)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramSQLiteDatabase);
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("DbUpgradeFailed");
      Object localObject5 = String.valueOf(paramInt1);
      localObject5 = ((e.a)localObject1).a("VersionFrom", (String)localObject5);
      String str = String.valueOf(paramInt2);
      localObject5 = ((e.a)localObject5).a("VersionTo", str);
      str = paramSQLiteDatabase.getClass().getCanonicalName();
      localObject5 = ((e.a)localObject5).a("ExceptionType", str).a();
      a.b((com.truecaller.analytics.e)localObject5);
      localObject5 = new com/truecaller/content/c/ag$a;
      ((ag.a)localObject5).<init>(this, paramSQLiteDatabase);
      throw ((Throwable)localObject5);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */