package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import c.g.b.y;
import com.truecaller.content.b.h;
import java.util.ArrayList;

public final class ae
  implements ab
{
  private final ac a;
  private final h b;
  
  public ae(ac paramac, h paramh)
  {
    a = paramac;
    b = paramh;
  }
  
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    String str = "context";
    k.b(paramContext, str);
    paramContext = "db";
    k.b(paramSQLiteDatabase, paramContext);
    int i = 72;
    if (paramInt1 <= i)
    {
      paramContext = b;
      paramContext.a(paramSQLiteDatabase);
    }
    i = 81;
    if (paramInt1 <= i)
    {
      paramSQLiteDatabase.execSQL("ALTER TABLE msg_thread_stats ADD COLUMN latest_message_raw_status INTEGER DEFAULT (0)");
      paramSQLiteDatabase.execSQL("ALTER TABLE msg_thread_stats ADD COLUMN latest_message_delivery_status INTEGER DEFAULT (0)");
      paramSQLiteDatabase.execSQL("ALTER TABLE msg_thread_stats ADD COLUMN latest_message_read_status INTEGER DEFAULT (0)");
      paramContext = "ALTER TABLE msg_thread_stats ADD COLUMN latest_message_transport INTEGER DEFAULT (0)";
      paramSQLiteDatabase.execSQL(paramContext);
    }
  }
  
  public final String[] a()
  {
    return new String[] { "\n    CREATE TABLE msg_thread_stats (\n    latest_message_id INTEGER,\n    latest_message_status INTEGER,\n    latest_message_media_count INTEGER DEFAULT(0),\n    latest_message_media_type TEXT,\n    unread_messages_count INTEGER DEFAULT(0),\n    latest_sim_token TEXT DEFAULT('-1'),\n    date_sorting INTEGER DEFAULT(0),\n    snippet_text TEXT DEFAULT(''),\n    actions_dismissed INTEGER DEFAULT(0),\n    filter INTEGER NOT NULL,\n    latest_message_raw_status INTEGER DEFAULT(0),\n    latest_message_delivery_status INTEGER DEFAULT(0),\n    latest_message_read_status INTEGER DEFAULT(0),\n    latest_message_transport INTEGER DEFAULT(0),\n    conversation_id INTEGER NOT NULL REFERENCES msg_conversations (_id) ON DELETE CASCADE,\n    UNIQUE(filter, conversation_id) ON CONFLICT REPLACE)\n", "CREATE INDEX idx_msg_thread_stats_latest_message_id ON msg_thread_stats (latest_message_id)" };
  }
  
  public final String[] b()
  {
    y localy = new c/g/b/y;
    localy.<init>(5);
    String[] arrayOfString = { "\n            CREATE TRIGGER trigger_thread_stats_on_conversation_insert\n                AFTER INSERT\n                ON msg_conversations\n            BEGIN\n                INSERT OR IGNORE\n                    INTO msg_thread_stats (conversation_id, filter)\n                    VALUES (new._id, 1);\n            END\n        " };
    localy.a(arrayOfString);
    String[] tmp28_25 = new String[5];
    String[] tmp29_28 = tmp28_25;
    String[] tmp29_28 = tmp28_25;
    tmp29_28[0] = "\n            CREATE TRIGGER trigger_thread_stats_on_message_insert\n                AFTER INSERT\n                ON msg_messages\n            BEGIN\n                \n    INSERT OR IGNORE\n        INTO msg_thread_stats (conversation_id, filter)\n        VALUES (new.conversation_id, new.category);\n    UPDATE msg_thread_stats\n        SET latest_message_id = new._id\n        WHERE new.conversation_id = conversation_id\n            AND filter IN (new.category, 1)\n            AND new.date >= date_sorting;\n    UPDATE msg_thread_stats\n        SET unread_messages_count = unread_messages_count + (NOT new.read AND new.status & 1 == 0)\n        WHERE new.conversation_id = conversation_id\n            AND filter IN (new.category, 1);\n\n            END\n        ";
    tmp29_28[1] = "\n            CREATE TRIGGER trigger_thread_stats_on_message_category_update\n                AFTER UPDATE OF category, conversation_id\n                ON msg_messages\n            BEGIN\n                \n    INSERT OR IGNORE\n        INTO msg_thread_stats (conversation_id, filter)\n        VALUES (new.conversation_id, new.category);\n    UPDATE msg_thread_stats\n        SET latest_message_id = new._id\n        WHERE new.conversation_id = conversation_id\n            AND filter IN (new.category, 1)\n            AND new.date >= date_sorting;\n    UPDATE msg_thread_stats\n        SET unread_messages_count = unread_messages_count + (NOT new.read AND new.status & 1 == 0)\n        WHERE new.conversation_id = conversation_id\n            AND filter IN (new.category, 1);\n\n                \n    UPDATE msg_thread_stats\n        SET latest_message_id = (SELECT _id\n            FROM msg_messages\n            WHERE msg_thread_stats.conversation_id = conversation_id\n                AND msg_thread_stats.filter IN (category, 1)\n            ORDER BY date DESC\n            LIMIT 1)\n        WHERE latest_message_id = old._id;\n    UPDATE msg_thread_stats\n        SET unread_messages_count = unread_messages_count - (NOT old.read AND old.status & 1 == 0)\n        WHERE old.conversation_id = conversation_id\n            AND filter IN (old.category, 1);\n    DELETE\n        FROM msg_thread_stats\n        WHERE conversation_id = old.conversation_id AND latest_message_id IS NULL;\n\n            END\n        ";
    String[] tmp38_29 = tmp29_28;
    String[] tmp38_29 = tmp29_28;
    tmp38_29[2] = "\n            CREATE TRIGGER trigger_thread_stats_counts_on_message_update\n                AFTER UPDATE OF read, status\n                ON msg_messages\n            BEGIN\n                UPDATE msg_thread_stats\n                    SET unread_messages_count = unread_messages_count - (NOT old.read AND old.status & 1 == 0) + (NOT new.read AND new.status & 1 == 0)\n                    WHERE new.conversation_id = conversation_id\n                        AND filter IN (new.category, 1);\n            END\n        ";
    tmp38_29[3] = "\n            CREATE TRIGGER trigger_thread_stats_on_message_update\n                AFTER UPDATE OF status\n                ON msg_messages\n            BEGIN\n                UPDATE msg_thread_stats\n                    SET latest_message_status = new.status\n                    WHERE new._id = latest_message_id;\n            END\n        ";
    tmp38_29[4] = "\n            CREATE TRIGGER trigger_thread_stats_on_message_delete\n                AFTER DELETE\n                ON msg_messages\n            BEGIN\n                \n    UPDATE msg_thread_stats\n        SET latest_message_id = (SELECT _id\n            FROM msg_messages\n            WHERE msg_thread_stats.conversation_id = conversation_id\n                AND msg_thread_stats.filter IN (category, 1)\n            ORDER BY date DESC\n            LIMIT 1)\n        WHERE latest_message_id = old._id;\n    UPDATE msg_thread_stats\n        SET unread_messages_count = unread_messages_count - (NOT old.read AND old.status & 1 == 0)\n        WHERE old.conversation_id = conversation_id\n            AND filter IN (old.category, 1);\n    DELETE\n        FROM msg_thread_stats\n        WHERE conversation_id = old.conversation_id AND latest_message_id IS NULL;\n\n            END\n        ";
    arrayOfString = tmp38_29;
    localy.a(arrayOfString);
    arrayOfString = new String[] { "\n            CREATE TRIGGER trigger_thread_stats_on_thread_stats_update\n                AFTER UPDATE OF latest_message_id\n                ON msg_thread_stats\n            BEGIN\n                UPDATE msg_thread_stats\n                    SET date_sorting = (SELECT date\n                            FROM msg_messages\n                            WHERE _id = new.latest_message_id),\n                        latest_sim_token = (SELECT sim_token\n                            FROM msg_messages\n                            WHERE _id = new.latest_message_id),\n                        latest_message_status = (SELECT status\n                            FROM msg_messages\n                            WHERE _id = new.latest_message_id),\n                        snippet_text = (SELECT content\n                            FROM msg_entities\n                            WHERE message_id = new.latest_message_id\n                                AND type = 'text/plain'),\n                        latest_message_media_count = (SELECT COUNT(*)\n                            FROM msg_entities\n                            WHERE message_id = new.latest_message_id\n                                AND type != 'text/plain'),\n                        latest_message_media_type = (SELECT type\n                            FROM msg_entities\n                            WHERE message_id = new.latest_message_id\n                                AND type != 'text/plain'\n                            LIMIT 1),\n                        latest_message_transport = (SELECT transport\n                            FROM msg_messages\n                            WHERE _id = new.latest_message_id),\n                        latest_message_raw_status = (SELECT IFNULL ((SELECT raw_status\n                            FROM msg_sms_transport_info\n                            WHERE message_id = new.latest_message_id), 0)),\n                        latest_message_delivery_status = (SELECT IFNULL ((SELECT delivery_status\n                            FROM msg_im_transport_info\n                            WHERE message_id = new.latest_message_id), 0)),\n                        latest_message_read_status = (SELECT IFNULL ((SELECT read_status\n                            FROM msg_im_transport_info\n                            WHERE message_id = new.latest_message_id), 0))\n                    WHERE conversation_id = new.conversation_id\n                        AND filter = new.filter;\n            END\n        " };
    localy.a(arrayOfString);
    String[] tmp76_73 = new String[4];
    String[] tmp77_76 = tmp76_73;
    String[] tmp77_76 = tmp76_73;
    tmp77_76[0] = "\n            CREATE TRIGGER trigger_thread_stats_on_text_entity_insert\n                AFTER INSERT\n                ON msg_entities\n                WHEN new.type = 'text/plain'\n            BEGIN\n                UPDATE msg_thread_stats\n                    SET snippet_text = new.content\n                    WHERE latest_message_id = new.message_id;\n            END\n        ";
    tmp77_76[1] = "\n            CREATE TRIGGER trigger_thread_stats_on_media_entity_insert\n                AFTER INSERT\n                ON msg_entities\n                WHEN new.type != 'text/plain'\n            BEGIN\n                UPDATE msg_thread_stats\n                    SET latest_message_media_count = latest_message_media_count + 1,\n                        latest_message_media_type = new.type\n                    WHERE latest_message_id = new.message_id;\n            END\n        ";
    tmp77_76[2] = "\n            CREATE TRIGGER trigger_thread_stats_on_media_entity_delete\n                AFTER DELETE\n                ON msg_entities\n                WHEN old.type != 'text/plain'\n            BEGIN\n                UPDATE msg_thread_stats\n                    SET latest_message_media_count = latest_message_media_count - 1,\n                        latest_message_media_type = (SELECT type FROM msg_entities\n                        WHERE type != 'text/plain' LIMIT 1)\n                    WHERE latest_message_id = old.message_id;\n            END\n        ";
    String[] tmp90_77 = tmp77_76;
    tmp90_77[3] = "\n            CREATE TRIGGER trigger_thread_stats_on_text_entity_delete\n                AFTER DELETE\n                ON msg_entities\n                WHEN old.type = 'text/plain'\n            BEGIN\n                UPDATE msg_thread_stats\n                    SET snippet_text = NULL\n                    WHERE latest_message_id = old.message_id;\n            END\n        ";
    arrayOfString = tmp90_77;
    localy.a(arrayOfString);
    arrayOfString = new String[] { "\n            CREATE TRIGGER trigger_thread_stats_on_im_receipt_update\n                AFTER UPDATE OF delivery_status, read_status\n                ON msg_im_transport_info\n            BEGIN\n                UPDATE msg_thread_stats\n                    SET latest_message_delivery_status = new.delivery_status,\n                        latest_message_read_status = new.read_status,\n                        latest_message_transport = 2,\n                        latest_message_raw_status = 0\n                    WHERE latest_message_id = new.message_id;\n            END\n        ", "\n            CREATE TRIGGER trigger_thread_stats_on_sms_report_update\n                AFTER INSERT\n                ON msg_sms_transport_info\n            BEGIN\n                UPDATE msg_thread_stats\n                    SET latest_message_raw_status = new.raw_status,\n                        latest_message_transport = 0,\n                        latest_message_delivery_status = 0,\n                        latest_message_read_status = 0\n                    WHERE latest_message_id = new.message_id;\n            END\n        " };
    localy.a(arrayOfString);
    arrayOfString = new String[a.size()];
    return (String[])localy.a(arrayOfString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */