package com.truecaller.content.c;

import android.content.SharedPreferences.Editor;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

final class w$d
  implements SharedPreferences.Editor
{
  private final w.n a;
  private final w.e b;
  private final w c;
  private final Queue d;
  
  private w$d(w paramw)
  {
    ArrayDeque localArrayDeque = new java/util/ArrayDeque;
    localArrayDeque.<init>();
    d = localArrayDeque;
    c = paramw;
    paramw = w.a(c);
    a = paramw;
    paramw = c.a;
    b = paramw;
  }
  
  private boolean a()
  {
    ae.incrementAndGet();
    ??? = d;
    boolean bool1 = ((Queue)???).isEmpty();
    Object localObject3 = null;
    int j;
    if (!bool1)
    {
      ??? = new java/util/ArrayList;
      int i = d.size();
      ((ArrayList)???).<init>(i);
      Object localObject5 = c;
      localQueue = d;
      w.a((w)localObject5, localQueue);
      localObject5 = a.a;
      j = 0;
      localQueue = null;
      try
      {
        Object localObject7;
        for (;;)
        {
          localObject6 = d;
          localObject6 = ((Queue)localObject6).poll();
          localObject6 = (w.a)localObject6;
          if (localObject6 == null) {
            break;
          }
          localObject7 = a;
          localObject7 = b;
          ((w.a)localObject6).a((Map)localObject7);
          j += 1;
          boolean bool2 = localObject6 instanceof w.o;
          if (bool2)
          {
            localObject6 = (w.o)localObject6;
            localObject6 = ((w.o)localObject6).a();
            ((List)???).add(localObject6);
          }
        }
        Object localObject6 = a;
        localObject6 = c;
        ((AtomicInteger)localObject6).addAndGet(j);
        localObject6 = c;
        w.d((w)localObject6);
        localObject5 = c;
        w.a((w)localObject5, (List)???);
        if (j == 0) {
          break label335;
        }
        ??? = a.d;
        if (??? != null) {
          break label335;
        }
        synchronized (a)
        {
          localObject5 = a;
          localObject5 = d;
          if (localObject5 == null)
          {
            localObject5 = Runtime.getRuntime();
            localObject6 = a;
            localObject7 = new java/lang/Thread;
            w localw = c;
            ((Thread)localObject7).<init>(localw);
            d = ((Thread)localObject7);
            ((Runtime)localObject5).addShutdownHook((Thread)localObject7);
          }
        }
        j = 0;
      }
      finally {}
    }
    Queue localQueue = null;
    label335:
    return j != 0;
  }
  
  public final void apply()
  {
    Object localObject = af;
    ((AtomicInteger)localObject).incrementAndGet();
    boolean bool = a();
    if (bool)
    {
      localObject = c;
      w.c((w)localObject);
    }
  }
  
  public final SharedPreferences.Editor clear()
  {
    d.clear();
    Queue localQueue = d;
    w.a locala = b.a(4, null, null);
    localQueue.offer(locala);
    return this;
  }
  
  public final boolean commit()
  {
    Object localObject = ag;
    ((AtomicInteger)localObject).incrementAndGet();
    boolean bool = a();
    if (bool)
    {
      localObject = c;
      bool = w.b((w)localObject);
    }
    else
    {
      bool = false;
      localObject = null;
    }
    return bool;
  }
  
  public final SharedPreferences.Editor putBoolean(String paramString, boolean paramBoolean)
  {
    Queue localQueue = d;
    w.e locale = b;
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    paramString = locale.a(1, paramString, localBoolean);
    localQueue.offer(paramString);
    return this;
  }
  
  public final SharedPreferences.Editor putFloat(String paramString, float paramFloat)
  {
    Queue localQueue = d;
    w.e locale = b;
    Float localFloat = Float.valueOf(paramFloat);
    paramString = locale.a(1, paramString, localFloat);
    localQueue.offer(paramString);
    return this;
  }
  
  public final SharedPreferences.Editor putInt(String paramString, int paramInt)
  {
    Queue localQueue = d;
    w.e locale = b;
    Integer localInteger = Integer.valueOf(paramInt);
    paramString = locale.a(1, paramString, localInteger);
    localQueue.offer(paramString);
    return this;
  }
  
  public final SharedPreferences.Editor putLong(String paramString, long paramLong)
  {
    Queue localQueue = d;
    w.e locale = b;
    Long localLong = Long.valueOf(paramLong);
    paramString = locale.a(1, paramString, localLong);
    localQueue.offer(paramString);
    return this;
  }
  
  public final SharedPreferences.Editor putString(String paramString1, String paramString2)
  {
    Queue localQueue = d;
    paramString1 = b.a(1, paramString1, paramString2);
    localQueue.offer(paramString1);
    return this;
  }
  
  public final SharedPreferences.Editor putStringSet(String paramString, Set paramSet)
  {
    Queue localQueue = d;
    paramString = b.a(1, paramString, paramSet);
    localQueue.offer(paramString);
    return this;
  }
  
  public final SharedPreferences.Editor remove(String paramString)
  {
    Queue localQueue = d;
    paramString = b.a(2, paramString, null);
    localQueue.offer(paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.w.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */