package com.truecaller.content.c;

public final class l
  implements ab
{
  public final String[] a()
  {
    return new String[] { "\n    CREATE TABLE msg_im_group_participants (\n        im_group_id TEXT NOT NULL,\n        im_peer_id TEXT NOT NULL,\n        roles INTEGER NOT NULL DEFAULT 0,\n        UNIQUE(im_group_id, im_peer_id)\n    )\n", "\n    CREATE TABLE msg_im_group_info (\n        im_group_id TEXT PRIMARY KEY,\n        title TEXT,\n        avatar TEXT,\n        invited_date INTEGER NOT NULL,\n        invited_by TEXT,\n        roles INTEGER NOT NULL DEFAULT 0,\n        actions INTEGER NOT NULL DEFAULT 0,\n        role_update_restriction_mask INTEGER NOT NULL DEFAULT 0,\n        role_update_mask INTEGER NOT NULL DEFAULT 0,\n        self_role_update_mask INTEGER NOT NULL DEFAULT 0,\n        notification_settings INTEGER NOT NULL DEFAULT 0\n    )\n" };
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */