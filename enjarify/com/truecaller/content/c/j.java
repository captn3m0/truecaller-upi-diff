package com.truecaller.content.c;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.truecaller.common.c.b.b;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract.c;
import com.truecaller.content.TruecallerContract.d;
import com.truecaller.content.TruecallerContract.j;
import com.truecaller.content.TruecallerContract.m;

final class j
  implements ab
{
  static final String a;
  private static final String b;
  private static final String c;
  private static final String d;
  private static final String e;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("history._id AS _id, history.tc_id AS tc_id, history.tc_flag AS tc_flag, ");
    String[] arrayOfString = TruecallerContract.m.a;
    String str = b.a("history", arrayOfString);
    localStringBuilder.append(str);
    b = localStringBuilder.toString();
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CREATE VIEW history_with_call_recording AS SELECT ");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(",call_recordings._id AS history_call_recording_id,");
    arrayOfString = TruecallerContract.c.a;
    str = b.a("call_recordings", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(" FROM history LEFT JOIN call_recordings ON history.event_id=call_recordings.history_event_id");
    a = localStringBuilder.toString();
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CREATE VIEW history_with_raw_contact AS SELECT history_with_call_recording._id AS _id, history_with_call_recording.tc_id AS tc_id, history_with_call_recording.tc_flag AS tc_flag, ");
    arrayOfString = TruecallerContract.m.a;
    str = b.a("history_with_call_recording", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(", raw_contact._id AS history_raw_contact_id, raw_contact.tc_id AS history_raw_contact_tc_id, ");
    arrayOfString = TruecallerContract.d.a;
    str = b.a("raw_contact", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(" FROM history_with_call_recording LEFT JOIN raw_contact ON history_with_call_recording.tc_id = raw_contact.tc_id");
    c = localStringBuilder.toString();
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CREATE VIEW history_with_aggregated_contact AS SELECT history_with_call_recording._id AS _id, history_with_call_recording.tc_id AS tc_id, history_with_call_recording.tc_flag AS tc_flag, history_with_call_recording.history_call_recording_id AS history_call_recording_id, ");
    arrayOfString = TruecallerContract.m.a;
    str = b.a("history_with_call_recording", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(", ");
    arrayOfString = TruecallerContract.c.a;
    str = b.a("history_with_call_recording", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(", aggregated_contact._id AS history_aggregated_contact_id, aggregated_contact.tc_id AS history_aggregated_contact_tc_id, raw_contact._id AS history_raw_contact_id, raw_contact.tc_id AS history_raw_contact_tc_id, ");
    arrayOfString = TruecallerContract.d.a;
    str = b.a("aggregated_contact", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(" FROM history_with_call_recording LEFT JOIN raw_contact ON history_with_call_recording.tc_id=raw_contact.tc_id LEFT JOIN aggregated_contact ON aggregated_contact._id=raw_contact.aggregated_contact_id");
    d = localStringBuilder.toString();
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CREATE VIEW history_with_aggregated_contact_number AS SELECT history_with_aggregated_contact.*, data._id AS data_id, data.tc_id AS data_tc_id, ");
    arrayOfString = TruecallerContract.j.a;
    str = b.a("data", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(" FROM history_with_aggregated_contact LEFT JOIN raw_contact ON history_with_aggregated_contact.history_aggregated_contact_id=raw_contact.aggregated_contact_id AND raw_contact.contact_source=2 LEFT JOIN data ON data._id= (SELECT _id FROM data WHERE data.data_raw_contact_id");
    localStringBuilder.append("=raw_contact._id AND data.data_type=4 AND data.data1=history_with_aggregated_contact.normalized_number LIMIT 1 )");
    e = localStringBuilder.toString();
  }
  
  private static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    String[] tmp5_2 = new String[17];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "_id";
    tmp6_5[1] = "tc_id";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "normalized_number";
    tmp15_6[3] = "raw_number";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "number_type";
    tmp24_15[5] = "country_code";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[6] = "cached_name";
    tmp33_24[7] = "type";
    String[] tmp44_33 = tmp33_24;
    String[] tmp44_33 = tmp33_24;
    tmp44_33[8] = "action";
    tmp44_33[9] = "call_log_id";
    String[] tmp55_44 = tmp44_33;
    String[] tmp55_44 = tmp44_33;
    tmp55_44[10] = "timestamp";
    tmp55_44[11] = "duration";
    String[] tmp66_55 = tmp55_44;
    String[] tmp66_55 = tmp55_44;
    tmp66_55[12] = "subscription_id";
    tmp66_55[13] = "feature";
    String[] tmp77_66 = tmp66_55;
    String[] tmp77_66 = tmp66_55;
    tmp77_66[14] = "new";
    tmp77_66[15] = "is_read";
    tmp77_66[16] = "subscription_component_name";
    String str = am.a(tmp77_66, ",");
    paramSQLiteDatabase.execSQL("CREATE TEMPORARY TABLE temp_history AS SELECT * FROM history");
    paramSQLiteDatabase.execSQL("DROP TABLE history");
    paramSQLiteDatabase.execSQL("CREATE TABLE history(_id INTEGER PRIMARY KEY, event_id TEXT NOT NULL DEFAULT '',tc_id TEXT, normalized_number TEXT, raw_number TEXT, number_type INT, country_code TEXT, cached_name TEXT,type INT, action INT, call_log_id INT, timestamp INT NOT NULL, duration INT, subscription_id TEXT, feature INT, new INT, is_read INT, subscription_component_name TEXT, tc_flag INT NOT NULL DEFAULT 0);");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("INSERT INTO history (");
    localStringBuilder.append(str);
    localStringBuilder.append(") SELECT ");
    localStringBuilder.append(str);
    localStringBuilder.append(" FROM temp_history");
    str = localStringBuilder.toString();
    paramSQLiteDatabase.execSQL(str);
    paramSQLiteDatabase.execSQL("DROP TABLE temp_history");
  }
  
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    SQLiteDatabase localSQLiteDatabase = paramSQLiteDatabase;
    int i = paramInt1;
    int j = paramInt2;
    while (i < j)
    {
      int k = 3;
      Object localObject1;
      Object localObject2;
      String str12;
      int m;
      if (i == k)
      {
        localSQLiteDatabase.execSQL("CREATE TEMPORARY TABLE temp_history AS SELECT * FROM history");
        localSQLiteDatabase.execSQL("DROP TABLE history");
        localSQLiteDatabase.execSQL("CREATE TABLE history(_id INTEGER PRIMARY KEY, event_id TEXT NOT NULL DEFAULT '',tc_id TEXT, normalized_number TEXT, raw_number TEXT, number_type INT, country_code TEXT, cached_name TEXT,type INT, action INT, call_log_id INT, timestamp INT NOT NULL, duration INT, subscription_id TEXT, feature INT, new INT, is_read INT, subscription_component_name TEXT, tc_flag INT NOT NULL DEFAULT 0);");
        localSQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS history_timestamp ON history(timestamp DESC)");
        localSQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS history_tc_id ON history(tc_id)");
        localSQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS history_normalized_number ON history(normalized_number)");
        localSQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS history_type ON history(type)");
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("INSERT INTO history (");
        String[] tmp93_90 = new String[12];
        String[] tmp94_93 = tmp93_90;
        String[] tmp94_93 = tmp93_90;
        tmp94_93[0] = "_id";
        tmp94_93[1] = "tc_id";
        String[] tmp103_94 = tmp94_93;
        String[] tmp103_94 = tmp94_93;
        tmp103_94[2] = "normalized_number";
        tmp103_94[3] = "raw_number";
        String[] tmp112_103 = tmp103_94;
        String[] tmp112_103 = tmp103_94;
        tmp112_103[4] = "number_type";
        tmp112_103[5] = "country_code";
        String[] tmp121_112 = tmp112_103;
        String[] tmp121_112 = tmp112_103;
        tmp121_112[6] = "cached_name";
        tmp121_112[7] = "type";
        String[] tmp132_121 = tmp121_112;
        String[] tmp132_121 = tmp121_112;
        tmp132_121[8] = "action";
        tmp132_121[9] = "call_log_id";
        tmp132_121[10] = "timestamp";
        String[] tmp148_132 = tmp132_121;
        tmp148_132[11] = "duration";
        localObject2 = am.a(tmp148_132, ",");
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append(") SELECT ");
        String str1 = "tc_id";
        String str2 = "normalized_number";
        String str3 = "national_number";
        String str4 = "number_type";
        String str5 = "country_code";
        String str6 = "cached_name";
        String str7 = "type";
        String str8 = "action";
        String str9 = "call_log_id";
        String str10 = "timestamp";
        String str11 = "duration";
        String[] tmp226_223 = new String[12];
        String[] tmp227_226 = tmp226_223;
        String[] tmp227_226 = tmp226_223;
        tmp227_226[0] = "_id";
        tmp227_226[1] = str1;
        String[] tmp236_227 = tmp227_226;
        String[] tmp236_227 = tmp227_226;
        tmp236_227[2] = str2;
        tmp236_227[3] = str3;
        String[] tmp245_236 = tmp236_227;
        String[] tmp245_236 = tmp236_227;
        tmp245_236[4] = str4;
        tmp245_236[5] = str5;
        String[] tmp254_245 = tmp245_236;
        String[] tmp254_245 = tmp245_236;
        tmp254_245[6] = str6;
        tmp254_245[7] = str7;
        String[] tmp265_254 = tmp254_245;
        String[] tmp265_254 = tmp254_245;
        tmp265_254[8] = str8;
        tmp265_254[9] = str9;
        tmp265_254[10] = str10;
        String[] tmp281_265 = tmp265_254;
        tmp281_265[11] = str11;
        localObject2 = tmp281_265;
        str12 = ",";
        localObject2 = am.a((Object[])localObject2, str12);
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = " FROM temp_history";
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        localSQLiteDatabase.execSQL((String)localObject1);
        localObject1 = "DROP TABLE temp_history";
        localSQLiteDatabase.execSQL((String)localObject1);
        m = paramInt1;
      }
      else
      {
        k = 9;
        if (i == k)
        {
          localSQLiteDatabase.execSQL("ALTER TABLE history ADD COLUMN subscription_id TEXT;");
          localObject1 = "ALTER TABLE history ADD COLUMN sim_index INT;";
          localSQLiteDatabase.execSQL((String)localObject1);
          m = paramInt1;
        }
        else
        {
          k = 10;
          m = 0;
          localObject2 = null;
          if (i == k)
          {
            str12 = "type=4";
            localSQLiteDatabase.delete("history", str12, null);
            localSQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS history_tc_id ON history(tc_id)");
            localSQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS history_normalized_number ON history(normalized_number)");
            localObject1 = "CREATE INDEX IF NOT EXISTS history_type ON history(type)";
            localSQLiteDatabase.execSQL((String)localObject1);
            m = paramInt1;
          }
          else
          {
            k = 11;
            if (i == k)
            {
              localObject1 = "CREATE INDEX IF NOT EXISTS history_action ON history(action)";
              localSQLiteDatabase.execSQL((String)localObject1);
              m = paramInt1;
            }
            else
            {
              k = 12;
              if (i == k)
              {
                localObject1 = "ALTER TABLE history ADD COLUMN feature INT;";
                localSQLiteDatabase.execSQL((String)localObject1);
                m = paramInt1;
              }
              else
              {
                k = 15;
                if (i == k)
                {
                  localSQLiteDatabase.execSQL("ALTER TABLE history ADD COLUMN new INT;");
                  localSQLiteDatabase.execSQL("ALTER TABLE history ADD COLUMN is_read INT;");
                  localObject1 = "ALTER TABLE history ADD COLUMN subscription_component_name TEXT;";
                  localSQLiteDatabase.execSQL((String)localObject1);
                  m = paramInt1;
                }
                else
                {
                  k = 17;
                  if (i == k)
                  {
                    a(paramSQLiteDatabase);
                    m = paramInt1;
                  }
                  else
                  {
                    k = 22;
                    if (i == k)
                    {
                      localObject1 = localSQLiteDatabase.rawQuery("PRAGMA table_info(history)", null);
                      localObject2 = "tc_flag";
                      m = ((Cursor)localObject1).getColumnIndex((String)localObject2);
                      ((Cursor)localObject1).close();
                      if (m < 0) {
                        a(paramSQLiteDatabase);
                      }
                      m = paramInt1;
                    }
                    else
                    {
                      k = 24;
                      if (i == k)
                      {
                        a(paramSQLiteDatabase);
                        m = paramInt1;
                      }
                      else
                      {
                        m = 38;
                        if (i == m)
                        {
                          m = paramInt1;
                          if (paramInt1 > k)
                          {
                            localObject1 = "ALTER TABLE history ADD COLUMN event_id TEXT NOT NULL DEFAULT '';";
                            localSQLiteDatabase.execSQL((String)localObject1);
                          }
                          localSQLiteDatabase.execSQL("UPDATE history SET event_id=lower(hex(randomblob(16)))");
                          localObject1 = "CREATE UNIQUE INDEX IF NOT EXISTS history_event_id ON history(event_id)";
                          localSQLiteDatabase.execSQL((String)localObject1);
                        }
                        else
                        {
                          m = paramInt1;
                          k = 40;
                          if (i == k)
                          {
                            localObject1 = "UPDATE history SET tc_id=NULL WHERE tc_id NOT IN (SELECT tc_id FROM raw_contact)";
                            localSQLiteDatabase.execSQL((String)localObject1);
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      i += 1;
    }
  }
  
  public final String[] a()
  {
    String[] tmp5_2 = new String[7];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "CREATE TABLE history(_id INTEGER PRIMARY KEY, event_id TEXT NOT NULL DEFAULT '',tc_id TEXT, normalized_number TEXT, raw_number TEXT, number_type INT, country_code TEXT, cached_name TEXT,type INT, action INT, call_log_id INT, timestamp INT NOT NULL, duration INT, subscription_id TEXT, feature INT, new INT, is_read INT, subscription_component_name TEXT, tc_flag INT NOT NULL DEFAULT 0);";
    tmp6_5[1] = "CREATE INDEX IF NOT EXISTS history_timestamp ON history(timestamp DESC)";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "CREATE INDEX IF NOT EXISTS history_tc_id ON history(tc_id)";
    tmp15_6[3] = "CREATE INDEX IF NOT EXISTS history_normalized_number ON history(normalized_number)";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "CREATE INDEX IF NOT EXISTS history_type ON history(type)";
    tmp24_15[5] = "CREATE INDEX IF NOT EXISTS history_action ON history(action)";
    tmp24_15[6] = "CREATE UNIQUE INDEX IF NOT EXISTS history_event_id ON history(event_id)";
    return tmp24_15;
  }
  
  public final String[] b()
  {
    String[] arrayOfString = new String[5];
    String str = a;
    arrayOfString[0] = str;
    str = c;
    arrayOfString[1] = str;
    str = d;
    arrayOfString[2] = str;
    arrayOfString[3] = "CREATE VIEW history_top_called_with_aggregated_contact AS SELECT * FROM history_with_aggregated_contact WHERE type=2 AND DATE(timestamp / 1000, 'unixepoch') > DATE('now', '-30 days') GROUP BY normalized_number ORDER BY COUNT(normalized_number) DESC, MAX(timestamp) DESC";
    str = e;
    arrayOfString[4] = str;
    return arrayOfString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */