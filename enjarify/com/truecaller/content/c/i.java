package com.truecaller.content.c;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.truecaller.common.c.b.a;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

final class i
  implements ab
{
  private static final String a;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CREATE TABLE filters(_id INTEGER PRIMARY KEY AUTOINCREMENT, server_id TEXT, value TEXT NOT NULL, label TEXT, rule INTEGER NOT NULL, wildcard_type INTEGER DEFAULT(");
    int i = NONEtype;
    localStringBuilder.append(i);
    localStringBuilder.append("), sync_state INTEGER DEFAULT(1), tracking_type TEXT, tracking_source TEXT, entity_type INTEGER DEFAULT(");
    i = UNKNOWNvalue;
    localStringBuilder.append(i);
    localStringBuilder.append("), UNIQUE (value) ON CONFLICT ABORT )");
    a = localStringBuilder.toString();
  }
  
  private static void a(SQLiteDatabase paramSQLiteDatabase1, SQLiteDatabase paramSQLiteDatabase2)
  {
    int i = 6;
    String[] arrayOfString = new String[i];
    arrayOfString[0] = "value AS value";
    arrayOfString[1] = "snapshot_label AS label";
    arrayOfString[2] = "1 AS rule";
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    int j = NONEtype;
    ((StringBuilder)localObject1).append(j);
    ((StringBuilder)localObject1).append(" AS wildcard_type");
    localObject1 = ((StringBuilder)localObject1).toString();
    arrayOfString[3] = localObject1;
    arrayOfString[4] = "1 AS sync_state";
    localObject1 = "'OTHER' AS tracking_type";
    j = 5;
    arrayOfString[j] = localObject1;
    String str = "whitelisttable";
    Object localObject2 = paramSQLiteDatabase1;
    paramSQLiteDatabase1 = paramSQLiteDatabase1.query(str, arrayOfString, null, null, null, null, null);
    if (paramSQLiteDatabase1 != null) {
      try
      {
        boolean bool1 = paramSQLiteDatabase1.moveToFirst();
        if (bool1)
        {
          localObject1 = new java/util/ArrayList;
          ((ArrayList)localObject1).<init>();
          a.a(paramSQLiteDatabase1, (Collection)localObject1);
          localObject1 = ((Collection)localObject1).iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject1).hasNext();
            if (!bool2) {
              break;
            }
            localObject2 = ((Iterator)localObject1).next();
            localObject2 = (ContentValues)localObject2;
            str = "filters";
            arrayOfString = null;
            paramSQLiteDatabase2.insert(str, null, (ContentValues)localObject2);
          }
        }
        if (paramSQLiteDatabase1 == null) {
          return;
        }
      }
      finally
      {
        if (paramSQLiteDatabase1 != null) {
          paramSQLiteDatabase1.close();
        }
      }
    }
    paramSQLiteDatabase1.close();
    return;
  }
  
  /* Error */
  public final void a(android.content.Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    // Byte code:
    //   0: bipush 12
    //   2: istore 4
    //   4: iload_3
    //   5: iload 4
    //   7: if_icmpge +281 -> 288
    //   10: getstatic 52	com/truecaller/content/c/i:a	Ljava/lang/String;
    //   13: astore 5
    //   15: aload_2
    //   16: aload 5
    //   18: invokevirtual 128	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   21: aload_2
    //   22: ldc -126
    //   24: invokevirtual 128	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   27: aload_2
    //   28: ldc -124
    //   30: invokevirtual 128	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   33: aload_2
    //   34: ldc -122
    //   36: invokevirtual 128	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   39: aload_2
    //   40: ldc -120
    //   42: invokevirtual 128	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   45: aload_2
    //   46: ldc -118
    //   48: invokevirtual 128	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   51: aload_1
    //   52: ldc -116
    //   54: invokevirtual 146	android/content/Context:getDatabasePath	(Ljava/lang/String;)Ljava/io/File;
    //   57: astore 5
    //   59: aload 5
    //   61: invokevirtual 151	java/io/File:exists	()Z
    //   64: istore 6
    //   66: iload 6
    //   68: ifeq +220 -> 288
    //   71: iconst_0
    //   72: istore 6
    //   74: aconst_null
    //   75: astore 5
    //   77: ldc -116
    //   79: astore 7
    //   81: iconst_0
    //   82: istore 8
    //   84: aconst_null
    //   85: astore 9
    //   87: aload_1
    //   88: aload 7
    //   90: iconst_0
    //   91: aconst_null
    //   92: invokevirtual 155	android/content/Context:openOrCreateDatabase	(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;
    //   95: astore_1
    //   96: new 10	java/lang/StringBuilder
    //   99: astore 7
    //   101: ldc -99
    //   103: astore 9
    //   105: aload 7
    //   107: aload 9
    //   109: invokespecial 16	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   112: getstatic 22	com/truecaller/content/TruecallerContract$Filters$WildCardType:NONE	Lcom/truecaller/content/TruecallerContract$Filters$WildCardType;
    //   115: astore 9
    //   117: aload 9
    //   119: getfield 26	com/truecaller/content/TruecallerContract$Filters$WildCardType:type	I
    //   122: istore 8
    //   124: aload 7
    //   126: iload 8
    //   128: invokevirtual 30	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: ldc -97
    //   134: astore 9
    //   136: aload 7
    //   138: aload 9
    //   140: invokevirtual 35	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   143: pop
    //   144: aload 7
    //   146: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   149: astore 7
    //   151: aload_1
    //   152: aload 7
    //   154: aconst_null
    //   155: invokevirtual 163	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   158: astore 7
    //   160: aload 7
    //   162: ifnull +78 -> 240
    //   165: new 115	android/content/ContentValues
    //   168: astore 9
    //   170: aload 9
    //   172: invokespecial 164	android/content/ContentValues:<init>	()V
    //   175: aload 7
    //   177: invokeinterface 167 1 0
    //   182: istore 10
    //   184: iload 10
    //   186: ifeq +32 -> 218
    //   189: aload 9
    //   191: invokevirtual 170	android/content/ContentValues:clear	()V
    //   194: aload 7
    //   196: aload 9
    //   198: invokestatic 176	android/database/DatabaseUtils:cursorRowToContentValues	(Landroid/database/Cursor;Landroid/content/ContentValues;)V
    //   201: ldc 117
    //   203: astore 11
    //   205: aload_2
    //   206: aload 11
    //   208: aconst_null
    //   209: aload 9
    //   211: invokevirtual 121	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    //   214: pop2
    //   215: goto -40 -> 175
    //   218: aload 7
    //   220: invokeinterface 124 1 0
    //   225: goto +15 -> 240
    //   228: astore 5
    //   230: aload 7
    //   232: invokeinterface 124 1 0
    //   237: aload 5
    //   239: athrow
    //   240: aload_1
    //   241: aload_2
    //   242: invokestatic 179	com/truecaller/content/c/i:a	(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/sqlite/SQLiteDatabase;)V
    //   245: goto +29 -> 274
    //   248: astore 5
    //   250: goto +19 -> 269
    //   253: astore_2
    //   254: iconst_0
    //   255: istore 12
    //   257: aconst_null
    //   258: astore_1
    //   259: goto +23 -> 282
    //   262: astore 5
    //   264: iconst_0
    //   265: istore 12
    //   267: aconst_null
    //   268: astore_1
    //   269: aload 5
    //   271: invokestatic 185	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   274: aload_1
    //   275: invokestatic 190	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   278: goto +10 -> 288
    //   281: astore_2
    //   282: aload_1
    //   283: invokestatic 190	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   286: aload_2
    //   287: athrow
    //   288: iload_3
    //   289: iload 4
    //   291: if_icmplt +50 -> 341
    //   294: bipush 43
    //   296: istore 12
    //   298: iload_3
    //   299: iload 12
    //   301: if_icmpge +40 -> 341
    //   304: new 10	java/lang/StringBuilder
    //   307: astore_1
    //   308: aload_1
    //   309: ldc -63
    //   311: invokespecial 16	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   314: getstatic 41	com/truecaller/content/TruecallerContract$Filters$EntityType:UNKNOWN	Lcom/truecaller/content/TruecallerContract$Filters$EntityType;
    //   317: astore 13
    //   319: aload 13
    //   321: getfield 44	com/truecaller/content/TruecallerContract$Filters$EntityType:value	I
    //   324: istore_3
    //   325: aload_1
    //   326: iload_3
    //   327: invokevirtual 30	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   330: pop
    //   331: aload_1
    //   332: invokevirtual 50	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   335: astore_1
    //   336: aload_2
    //   337: aload_1
    //   338: invokevirtual 128	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
    //   341: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	342	0	this	i
    //   0	342	1	paramContext	android.content.Context
    //   0	342	2	paramSQLiteDatabase	SQLiteDatabase
    //   0	342	3	paramInt1	int
    //   0	342	4	paramInt2	int
    //   13	63	5	localObject1	Object
    //   228	10	5	localObject2	Object
    //   248	1	5	localSQLiteException1	android.database.sqlite.SQLiteException
    //   262	8	5	localSQLiteException2	android.database.sqlite.SQLiteException
    //   64	9	6	bool1	boolean
    //   79	152	7	localObject3	Object
    //   82	45	8	i	int
    //   85	125	9	localObject4	Object
    //   182	3	10	bool2	boolean
    //   203	4	11	str	String
    //   255	47	12	j	int
    //   317	3	13	localEntityType	TruecallerContract.Filters.EntityType
    // Exception table:
    //   from	to	target	type
    //   165	168	228	finally
    //   170	175	228	finally
    //   175	182	228	finally
    //   189	194	228	finally
    //   196	201	228	finally
    //   209	215	228	finally
    //   96	99	248	android/database/sqlite/SQLiteException
    //   107	112	248	android/database/sqlite/SQLiteException
    //   112	115	248	android/database/sqlite/SQLiteException
    //   117	122	248	android/database/sqlite/SQLiteException
    //   126	132	248	android/database/sqlite/SQLiteException
    //   138	144	248	android/database/sqlite/SQLiteException
    //   144	149	248	android/database/sqlite/SQLiteException
    //   154	158	248	android/database/sqlite/SQLiteException
    //   218	225	248	android/database/sqlite/SQLiteException
    //   230	237	248	android/database/sqlite/SQLiteException
    //   237	240	248	android/database/sqlite/SQLiteException
    //   241	245	248	android/database/sqlite/SQLiteException
    //   91	95	253	finally
    //   91	95	262	android/database/sqlite/SQLiteException
    //   96	99	281	finally
    //   107	112	281	finally
    //   112	115	281	finally
    //   117	122	281	finally
    //   126	132	281	finally
    //   138	144	281	finally
    //   144	149	281	finally
    //   154	158	281	finally
    //   218	225	281	finally
    //   230	237	281	finally
    //   237	240	281	finally
    //   241	245	281	finally
    //   269	274	281	finally
  }
  
  public final String[] a()
  {
    String[] arrayOfString = new String[6];
    String str = a;
    arrayOfString[0] = str;
    arrayOfString[1] = "CREATE INDEX idx_filters_server_id ON filters (server_id)";
    arrayOfString[2] = "CREATE INDEX idx_filters_value ON filters (value)";
    arrayOfString[3] = "CREATE INDEX idx_filters_label ON filters (label)";
    arrayOfString[4] = "CREATE INDEX idx_filters_rule ON filters (rule)";
    arrayOfString[5] = "CREATE INDEX idx_filters_wildcard_type ON filters (wildcard_type)";
    return arrayOfString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */