package com.truecaller.content.c;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class w$k
  implements ThreadFactory
{
  public static final ThreadFactory a;
  private static final AtomicInteger b;
  
  static
  {
    Object localObject = new com/truecaller/content/c/w$k;
    ((k)localObject).<init>();
    a = (ThreadFactory)localObject;
    localObject = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject).<init>();
    b = (AtomicInteger)localObject;
  }
  
  public final Thread newThread(Runnable paramRunnable)
  {
    int i = b.getAndIncrement();
    Thread localThread = new java/lang/Thread;
    String str = String.valueOf(i);
    str = "preferencesunified-thread-pool-".concat(str);
    localThread.<init>(paramRunnable, str);
    localThread.setPriority(1);
    return localThread;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.w.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */