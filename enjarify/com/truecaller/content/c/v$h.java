package com.truecaller.content.c;

import com.truecaller.log.f;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class v$h
{
  public static int a(Object paramObject)
  {
    boolean bool1 = paramObject instanceof String;
    if (bool1) {
      return 32;
    }
    bool1 = paramObject instanceof Integer;
    if (bool1) {
      return 2;
    }
    bool1 = paramObject instanceof Long;
    if (bool1) {
      return 4;
    }
    bool1 = paramObject instanceof Float;
    if (bool1) {
      return 8;
    }
    bool1 = paramObject instanceof Boolean;
    if (bool1) {
      return 16;
    }
    boolean bool2 = paramObject instanceof Set;
    if (bool2) {
      return 64;
    }
    paramObject = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)paramObject).<init>("Unexpected data type.");
    throw ((Throwable)paramObject);
  }
  
  public static Object a(int paramInt, byte[] paramArrayOfByte)
  {
    int i = paramInt & 0x7E;
    if (i > 0)
    {
      DataInputStream localDataInputStream = new java/io/DataInputStream;
      ByteArrayInputStream localByteArrayInputStream = new java/io/ByteArrayInputStream;
      localByteArrayInputStream.<init>(paramArrayOfByte);
      localDataInputStream.<init>(localByteArrayInputStream);
      try
      {
        return a(localDataInputStream, paramInt);
      }
      finally
      {
        int j = 1;
        paramArrayOfByte = new String[j];
        i = 0;
        localDataInputStream = null;
        String str = f.a(localThrowable);
        paramArrayOfByte[0] = str;
      }
    }
    return null;
  }
  
  private static Object a(DataInputStream paramDataInputStream, int paramInt)
  {
    int i = 2;
    if (paramInt != i)
    {
      i = 4;
      if (paramInt != i)
      {
        i = 8;
        if (paramInt != i)
        {
          i = 16;
          if (paramInt != i)
          {
            i = 32;
            int j = 0;
            String str;
            if (paramInt != i)
            {
              i = 64;
              if (paramInt != i) {
                return null;
              }
              localObject1 = new java/util/HashSet;
              ((HashSet)localObject1).<init>();
              i = paramDataInputStream.readInt();
              while (j < i)
              {
                str = paramDataInputStream.readUTF();
                ((Set)localObject1).add(str);
                j += 1;
              }
              return localObject1;
            }
            Object localObject1 = paramDataInputStream.readUTF();
            Object localObject2 = "--several-chunks-of-the-string--";
            boolean bool = ((String)localObject2).equals(localObject1);
            if (bool)
            {
              paramInt = paramDataInputStream.readInt();
              localObject2 = new java/lang/StringBuilder;
              int k = 32768 * paramInt;
              ((StringBuilder)localObject2).<init>(k);
              while (j < paramInt)
              {
                str = paramDataInputStream.readUTF();
                ((StringBuilder)localObject2).append(str);
                j += 1;
              }
              return ((StringBuilder)localObject2).toString();
            }
            return localObject1;
          }
          return Boolean.valueOf(paramDataInputStream.readBoolean());
        }
        return Float.valueOf(paramDataInputStream.readFloat());
      }
      return Long.valueOf(paramDataInputStream.readLong());
    }
    return Integer.valueOf(paramDataInputStream.readInt());
  }
  
  private static void a(DataOutputStream paramDataOutputStream, int paramInt, Object paramObject)
  {
    int i = 2;
    if (paramInt != i)
    {
      i = 4;
      if (paramInt != i)
      {
        i = 8;
        if (paramInt != i)
        {
          i = 16;
          if (paramInt != i)
          {
            i = 32;
            Object localObject;
            boolean bool;
            if (paramInt != i)
            {
              i = 64;
              if (paramInt == i)
              {
                paramObject = (Set)paramObject;
                paramInt = ((Set)paramObject).size();
                paramDataOutputStream.writeInt(paramInt);
                localObject = ((Set)paramObject).iterator();
                for (;;)
                {
                  bool = ((Iterator)localObject).hasNext();
                  if (!bool) {
                    break;
                  }
                  paramObject = (String)((Iterator)localObject).next();
                  paramDataOutputStream.writeUTF((String)paramObject);
                }
              }
            }
            else
            {
              paramObject = (String)paramObject;
              paramInt = ((String)paramObject).length();
              i = paramInt / (char)-1;
              if (i > 0)
              {
                String str1 = "--several-chunks-of-the-string--";
                paramDataOutputStream.writeUTF(str1);
                i = 32768;
                paramInt = paramInt / i + 1;
                paramDataOutputStream.writeInt(paramInt);
                localObject = new java/util/ArrayList;
                ((ArrayList)localObject).<init>();
                int j = ((String)paramObject).length();
                int k = 0;
                String str2 = null;
                while (j != 0)
                {
                  int m;
                  if (j >= i)
                  {
                    m = k + i;
                    j += 32768;
                    int n = m;
                    m = j;
                    j = n;
                  }
                  else
                  {
                    j = ((String)paramObject).length();
                    ((String)paramObject).substring(k, j);
                    m = 0;
                  }
                  str2 = ((String)paramObject).substring(k, j);
                  ((List)localObject).add(str2);
                  k = j;
                  j = m;
                }
                localObject = ((List)localObject).iterator();
                for (;;)
                {
                  bool = ((Iterator)localObject).hasNext();
                  if (!bool) {
                    break;
                  }
                  paramObject = (String)((Iterator)localObject).next();
                  paramDataOutputStream.writeUTF((String)paramObject);
                }
              }
              paramDataOutputStream.writeUTF((String)paramObject);
            }
          }
          else
          {
            paramObject = (Boolean)paramObject;
            paramInt = ((Boolean)paramObject).booleanValue();
            paramDataOutputStream.writeBoolean(paramInt);
          }
        }
        else
        {
          paramObject = (Float)paramObject;
          float f = ((Float)paramObject).floatValue();
          paramDataOutputStream.writeFloat(f);
        }
      }
      else
      {
        paramObject = (Long)paramObject;
        long l = ((Long)paramObject).longValue();
        paramDataOutputStream.writeLong(l);
      }
    }
    else
    {
      paramObject = (Integer)paramObject;
      paramInt = ((Integer)paramObject).intValue();
      paramDataOutputStream.writeInt(paramInt);
    }
    paramDataOutputStream.flush();
  }
  
  /* Error */
  public static byte[] a(int paramInt, Object paramObject)
  {
    // Byte code:
    //   0: iload_0
    //   1: bipush 126
    //   3: iand
    //   4: istore_2
    //   5: iload_2
    //   6: ifle +63 -> 69
    //   9: new 179	java/io/ByteArrayOutputStream
    //   12: astore_3
    //   13: aload_3
    //   14: invokespecial 180	java/io/ByteArrayOutputStream:<init>	()V
    //   17: new 116	java/io/DataOutputStream
    //   20: astore 4
    //   22: aload 4
    //   24: aload_3
    //   25: invokespecial 183	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   28: aload 4
    //   30: iload_0
    //   31: aload_1
    //   32: invokestatic 186	com/truecaller/content/c/v$h:a	(Ljava/io/DataOutputStream;ILjava/lang/Object;)V
    //   35: goto +29 -> 64
    //   38: astore 5
    //   40: iconst_1
    //   41: istore 6
    //   43: iload 6
    //   45: anewarray 6	java/lang/String
    //   48: astore_1
    //   49: aconst_null
    //   50: astore 4
    //   52: aload 5
    //   54: invokestatic 50	com/truecaller/log/f:a	(Ljava/lang/Throwable;)Ljava/lang/String;
    //   57: astore 5
    //   59: aload_1
    //   60: iconst_0
    //   61: aload 5
    //   63: aastore
    //   64: aload_3
    //   65: invokevirtual 190	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   68: areturn
    //   69: aconst_null
    //   70: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	71	0	paramInt	int
    //   0	71	1	paramObject	Object
    //   4	2	2	i	int
    //   12	53	3	localByteArrayOutputStream	java.io.ByteArrayOutputStream
    //   20	31	4	localDataOutputStream	DataOutputStream
    //   38	15	5	localThrowable	Throwable
    //   57	5	5	str	String
    //   41	3	6	j	int
    // Exception table:
    //   from	to	target	type
    //   31	35	38	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.v.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */