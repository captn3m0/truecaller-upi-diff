package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public final class z
  implements ab
{
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramContext = "db";
    k.b(paramSQLiteDatabase, paramContext);
    int i = 55;
    Object localObject;
    if (paramInt1 < i)
    {
      localObject = a();
      int j = localObject.length;
      int k = 0;
      while (k < j)
      {
        String str = localObject[k];
        paramSQLiteDatabase.execSQL(str);
        k += 1;
      }
    }
    paramInt2 = 59;
    if ((i <= paramInt1) && (paramInt2 >= paramInt1))
    {
      localObject = "ALTER TABLE spam_url_reports ADD COLUMN sync_status INT NOT NULL DEFAULT (0) ";
      paramSQLiteDatabase.execSQL((String)localObject);
    }
    paramInt2 = 62;
    if (i > paramInt1) {
      return;
    }
    if (paramInt2 >= paramInt1)
    {
      paramSQLiteDatabase.execSQL("ALTER TABLE spam_url_reports ADD COLUMN spam_score INT NOT NULL DEFAULT (0) ");
      paramContext = "ALTER TABLE spam_url_reports ADD COLUMN last_updated_at INT NOT NULL DEFAULT (0) ";
      paramSQLiteDatabase.execSQL(paramContext);
    }
  }
  
  public final String[] a()
  {
    return new String[] { "create table spam_url_reports (url TEXT PRIMARY KEY ,spam_count INT NOT NULL, is_spam_reported INT NOT NULL, expires_at INT NOT NULL, sync_status INT NOT NULL DEFAULT (0), spam_score INT NOT NULL DEFAULT (0), last_updated_at INT NOT NULL DEFAULT (0) )" };
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */