package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.truecaller.log.f;
import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;

final class v$d
  extends SQLiteOpenHelper
{
  private static final String c;
  private static final String d;
  private static final String e;
  private static final String f;
  private static final String g;
  private static final String h;
  public final String a;
  public final Object b;
  private final Context i;
  
  static
  {
    Object[] arrayOfObject = new Object[6];
    arrayOfObject[0] = "preferences";
    int j = 1;
    arrayOfObject[j] = "_id";
    int k = 2;
    arrayOfObject[k] = "key";
    arrayOfObject[3] = "value";
    arrayOfObject[4] = "type";
    arrayOfObject[5] = "time";
    c = String.format("CREATE TABLE [%1$s] ( [%2$s] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, [%3$s] NVARCHAR(200) UNIQUE NOT NULL, [%4$s] TEXT, [%5$s] INTEGER NOT NULL,[%6$s] INTEGER NOT NULL )", arrayOfObject);
    arrayOfObject = new Object[k];
    arrayOfObject[0] = "preferences";
    arrayOfObject[j] = "key";
    d = String.format("CREATE UNIQUE INDEX [IDX_%1$s_%2$s] ON [%1$s] ( [%2$s] )", arrayOfObject);
    arrayOfObject = new Object[k];
    arrayOfObject[0] = "preferences";
    arrayOfObject[j] = "time";
    e = String.format("CREATE INDEX [IDX_%1$s_%2$s] ON [%1$s] ( [%2$s] )", arrayOfObject);
    arrayOfObject = new Object[j];
    arrayOfObject[0] = "preferences";
    f = String.format("DROP TABLE [%1$s]", arrayOfObject);
    arrayOfObject = new Object[k];
    arrayOfObject[0] = "preferences";
    arrayOfObject[j] = "time";
    g = String.format("DROP INDEX [IDX_%1$s_%2$s]", arrayOfObject);
    arrayOfObject = new Object[k];
    arrayOfObject[0] = "preferences";
    arrayOfObject[j] = "key";
    h = String.format("DROP INDEX [IDX_%1$s_%2$s]", arrayOfObject);
  }
  
  public v$d(Context arg1, String paramString)
  {
    super(???, paramString, null, j);
    a = paramString;
    i = ???;
    ??? = v.d();
    paramString = a;
    boolean bool = ???.containsKey(paramString);
    if (!bool) {
      synchronized (v.d())
      {
        paramString = v.d();
        str = a;
        Object localObject = new java/lang/Object;
        localObject.<init>();
        paramString.put(str, localObject);
      }
    }
    ??? = v.d();
    paramString = a;
    ??? = ???.get(paramString);
    b = ???;
  }
  
  private SQLiteDatabase a()
  {
    try
    {
      Object localObject1 = i;
      String str1 = a;
      localObject1 = ((Context)localObject1).getDatabasePath(str1);
      localObject1 = ((File)localObject1).getPath();
      int j = 0;
      str1 = null;
      int k = 268435472;
      localObject1 = SQLiteDatabase.openDatabase((String)localObject1, null, k);
      onConfigure((SQLiteDatabase)localObject1);
      j = ((SQLiteDatabase)localObject1).getVersion();
      k = 1;
      if (j != k)
      {
        boolean bool = ((SQLiteDatabase)localObject1).isReadOnly();
        if (!bool)
        {
          ((SQLiteDatabase)localObject1).beginTransaction();
          if (j == 0) {
            try
            {
              onCreate((SQLiteDatabase)localObject1);
            }
            finally
            {
              break label131;
            }
          } else if (j > k) {
            onDowngrade((SQLiteDatabase)localObject1, j, k);
          } else {
            onUpgrade((SQLiteDatabase)localObject1, j, k);
          }
          ((SQLiteDatabase)localObject1).setVersion(k);
          ((SQLiteDatabase)localObject1).setTransactionSuccessful();
          ((SQLiteDatabase)localObject1).endTransaction();
          break label206;
          label131:
          ((SQLiteDatabase)localObject1).endTransaction();
          throw ((Throwable)localObject3);
        }
        else
        {
          SQLiteException localSQLiteException = new android/database/sqlite/SQLiteException;
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          String str2 = "Can't upgrade read-only database from version ";
          localStringBuilder.<init>(str2);
          int m = ((SQLiteDatabase)localObject1).getVersion();
          localStringBuilder.append(m);
          localObject1 = " to 1: ";
          localStringBuilder.append((String)localObject1);
          localObject1 = a;
          localStringBuilder.append((String)localObject1);
          localObject1 = localStringBuilder.toString();
          localSQLiteException.<init>((String)localObject1);
          throw localSQLiteException;
        }
      }
      label206:
      onOpen((SQLiteDatabase)localObject1);
      localObject1 = a((SQLiteDatabase)localObject1);
      return (SQLiteDatabase)localObject1;
    }
    finally {}
  }
  
  private SQLiteDatabase a(SQLiteDatabase paramSQLiteDatabase)
  {
    boolean bool = true;
    try
    {
      Object localObject = getClass();
      str2 = "mDatabase";
      localObject = ((Class)localObject).getField(str2);
      if (localObject != null)
      {
        ((Field)localObject).setAccessible(bool);
        ((Field)localObject).set(this, paramSQLiteDatabase);
      }
    }
    finally
    {
      String[] arrayOfString = new String[bool];
      String str2 = null;
      String str1 = f.a(localThrowable);
      arrayOfString[0] = str1;
    }
    return paramSQLiteDatabase;
  }
  
  /* Error */
  public final SQLiteDatabase getReadableDatabase()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 210	android/database/sqlite/SQLiteOpenHelper:getReadableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   4: areturn
    //   5: astore_1
    //   6: iconst_1
    //   7: anewarray 40	java/lang/String
    //   10: astore_2
    //   11: aload_1
    //   12: invokestatic 206	com/truecaller/log/f:a	(Ljava/lang/Throwable;)Ljava/lang/String;
    //   15: astore_1
    //   16: aload_2
    //   17: iconst_0
    //   18: aload_1
    //   19: aastore
    //   20: aload_0
    //   21: invokespecial 212	com/truecaller/content/c/v$d:a	()Landroid/database/sqlite/SQLiteDatabase;
    //   24: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	25	0	this	d
    //   5	7	1	localThrowable	Throwable
    //   15	4	1	str	String
    //   10	7	2	arrayOfString	String[]
    // Exception table:
    //   from	to	target	type
    //   0	4	5	finally
  }
  
  public final void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    String str = c;
    paramSQLiteDatabase.execSQL(str);
    str = d;
    paramSQLiteDatabase.execSQL(str);
    str = e;
    paramSQLiteDatabase.execSQL(str);
  }
  
  public final void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    String str = g;
    paramSQLiteDatabase.execSQL(str);
    str = h;
    paramSQLiteDatabase.execSQL(str);
    str = f;
    paramSQLiteDatabase.execSQL(str);
    onCreate(paramSQLiteDatabase);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.v.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */