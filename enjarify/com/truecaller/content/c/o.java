package com.truecaller.content.c;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.common.c.a.a.g;

public final class o
  implements a.g
{
  public static final o.a a;
  
  static
  {
    o.a locala = new com/truecaller/content/c/o$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public final Cursor a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    k.b(parama, "provider");
    paramArrayOfString1 = "helper";
    k.b(parama1, paramArrayOfString1);
    k.b(paramUri, "uri");
    parama1 = paramUri.getQueryParameter("sorting_mode");
    if (parama1 != null)
    {
      int i = parama1.hashCode();
      int j = 2013122196;
      if (i == j)
      {
        paramArrayOfString1 = "last_name";
        boolean bool2 = parama1.equals(paramArrayOfString1);
        if (bool2)
        {
          paramArrayOfString1 = "sorting_key_2";
          parama1 = t.a("sorting_group_2", paramArrayOfString1);
          break label100;
        }
      }
    }
    paramArrayOfString1 = "sorting_key_1";
    parama1 = t.a("sorting_group_1", paramArrayOfString1);
    label100:
    paramArrayOfString1 = (String)a;
    parama1 = (String)b;
    paramString1 = paramUri.getQueryParameter("phonebook_filter");
    if (paramString1 != null)
    {
      int k = paramString1.hashCode();
      int m = -1028583081;
      label263:
      boolean bool5;
      if (k != m)
      {
        m = 915036773;
        if (k == m)
        {
          paramArrayOfString2 = "non_phonebook";
          boolean bool1 = paramString1.equals(paramArrayOfString2);
          if (bool1)
          {
            paramString1 = new java/lang/StringBuilder;
            paramString1.<init>("WHERE contact_phonebook_id IS NULL AND (contact_source & 32)!=32 ");
            paramArrayOfString2 = paramUri.getQueryParameter("hidden_from_identified_filter");
            paramString2 = "identified_spam_score_filter";
            paramUri = paramUri.getQueryParameter(paramString2);
            if ((paramArrayOfString2 != null) || (paramUri != null))
            {
              m = 0;
              paramString2 = null;
              boolean bool4 = true;
              if (paramArrayOfString2 != null)
              {
                bool3 = Boolean.parseBoolean(paramArrayOfString2);
                if (bool3 == bool4)
                {
                  bool3 = true;
                  break label263;
                }
              }
              boolean bool3 = false;
              paramArrayOfString2 = null;
              if (paramUri != null)
              {
                bool5 = Boolean.parseBoolean(paramUri);
                if (bool5 == bool4) {
                  m = 1;
                }
              }
              if ((bool3) && (m != 0))
              {
                paramUri = "AND (hidden_from_identified IS NULL OR hidden_from_identified==0) AND (contact_spam_score IS NULL OR contact_spam_score<10)";
                break label324;
              }
              if (bool3)
              {
                paramUri = "AND (hidden_from_identified IS NULL OR hidden_from_identified==0)";
                break label324;
              }
              if (m != 0)
              {
                paramUri = "AND (contact_spam_score IS NULL OR contact_spam_score<10)";
                break label324;
              }
            }
            paramUri = "";
            label324:
            paramString1.append(paramUri);
            paramUri = paramString1.toString();
            break label365;
          }
        }
      }
      else
      {
        paramUri = "phonebook";
        bool5 = paramString1.equals(paramUri);
        if (bool5)
        {
          paramUri = "WHERE contact_phonebook_id IS NOT NULL OR (contact_source & 32)=32";
          break label365;
        }
      }
    }
    paramUri = "";
    label365:
    paramString1 = new java/lang/StringBuilder;
    paramString1.<init>("\n            SELECT ");
    paramString1.append(paramArrayOfString1);
    paramString1.append(" AS group_label, COUNT(*) AS label_count FROM sorted_contacts_shallow\n            ");
    paramString1.append(paramUri);
    paramString1.append("\n            GROUP BY ");
    paramString1.append(paramArrayOfString1);
    paramString1.append(" ORDER BY ");
    paramString1.append(parama1);
    paramString1.append(" IS NULL, ");
    paramString1.append(parama1);
    paramString1.append("\n            ");
    parama1 = paramString1.toString();
    return parama.c().rawQuery(parama1, null, paramCancellationSignal);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */