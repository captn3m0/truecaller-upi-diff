package com.truecaller.content.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.truecaller.common.c.b.b;
import com.truecaller.content.TruecallerContentProvider;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.j;

public class a
  implements ab
{
  private static final String a;
  private static final String b;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CREATE VIEW aggregated_contact_raw_contact AS SELECT aggregated_contact._id AS _id, aggregated_contact.tc_id AS tc_id, aggregated_contact.tc_flag AS tc_flag, aggregated_contact.aggregated_update_timestamp AS aggregated_update_timestamp, ");
    String[] arrayOfString = TruecallerContract.a.a;
    String str = b.a("aggregated_contact", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(", raw_contact._id AS aggregated_raw_contact_id, raw_contact.tc_id AS aggregated_raw_contact_tc_id, raw_contact.contact_source AS aggregated_raw_contact_source, raw_contact.aggregated_contact_id AS aggregated_contact_id, raw_contact.search_query AS search_query, raw_contact");
    localStringBuilder.append(".cache_control AS cache_control FROM aggregated_contact INNER JOIN raw_contact ON aggregated_contact._id=raw_contact.aggregated_contact_id");
    a = localStringBuilder.toString();
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CREATE VIEW aggregated_contact_data AS SELECT aggregated_contact_raw_contact.*, data._id AS data_id, data.tc_id AS data_tc_id, ");
    arrayOfString = TruecallerContract.j.a;
    str = b.a("data", arrayOfString);
    localStringBuilder.append(str);
    localStringBuilder.append(" FROM aggregated_contact_raw_contact LEFT JOIN data ON aggregated_raw_contact_id=data.data_raw_contact_id");
    b = localStringBuilder.toString();
  }
  
  public void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    while (paramInt1 < paramInt2)
    {
      int i = 1;
      String str1;
      if (paramInt1 == i)
      {
        paramSQLiteDatabase.execSQL("CREATE TEMPORARY TABLE temp_aggregated_contact AS SELECT * FROM aggregated_contact");
        paramSQLiteDatabase.execSQL("DROP TABLE aggregated_contact");
        paramSQLiteDatabase.execSQL("CREATE TABLE aggregated_contact(_id INTEGER PRIMARY KEY, tc_id TEXT NOT NULL, contact_name TEXT, contact_handle TEXT, contact_alt_name TEXT, contact_gender TEXT, contact_about TEXT, contact_image_url TEXT, contact_job_title TEXT, contact_company TEXT, contact_access TEXT, contact_common_connections INT, contact_search_time INT, contact_source INT, contact_default_number TEXT, contact_phonebook_id INT, contact_phonebook_hash INT)");
        paramSQLiteDatabase.execSQL("INSERT INTO aggregated_contact SELECT * FROM temp_aggregated_contact");
        str1 = "DROP TABLE temp_aggregated_contact";
        paramSQLiteDatabase.execSQL(str1);
      }
      i = 2;
      if (paramInt1 == i)
      {
        str1 = "ALTER TABLE aggregated_contact ADD COLUMN contact_phonebook_lookup TEXT";
        paramSQLiteDatabase.execSQL(str1);
      }
      i = 4;
      if (paramInt1 == i)
      {
        paramSQLiteDatabase.execSQL("DELETE FROM aggregated_contact");
        str1 = "UPDATE raw_contact SET aggregated_contact_id=NULL";
        paramSQLiteDatabase.execSQL(str1);
        TruecallerContentProvider.c(paramContext);
      }
      i = 7;
      if (paramInt1 == i)
      {
        str1 = "ALTER TABLE aggregated_contact ADD COLUMN contact_transliterated_name TEXT";
        paramSQLiteDatabase.execSQL(str1);
      }
      i = 10;
      if (paramInt1 == i)
      {
        str1 = "aggregated_contact";
        String str2 = "contact_is_favorite";
        boolean bool = b.b(paramSQLiteDatabase, str1, str2);
        if (!bool)
        {
          paramSQLiteDatabase.execSQL("ALTER TABLE aggregated_contact ADD COLUMN contact_is_favorite INT");
          str1 = "ALTER TABLE aggregated_contact ADD COLUMN contact_favorite_position INT";
          paramSQLiteDatabase.execSQL(str1);
        }
      }
      int j = 11;
      if (paramInt1 == j)
      {
        str1 = "ALTER TABLE aggregated_contact ADD COLUMN contact_spam_score INT";
        paramSQLiteDatabase.execSQL(str1);
      }
      j = 14;
      if (paramInt1 == j)
      {
        str1 = "ALTER TABLE aggregated_contact ADD COLUMN contact_badges INT DEFAULT 0";
        paramSQLiteDatabase.execSQL(str1);
      }
      j = 25;
      if (paramInt1 == j)
      {
        str1 = "ALTER TABLE aggregated_contact ADD COLUMN tc_flag INT NOT NULL DEFAULT 0";
        paramSQLiteDatabase.execSQL(str1);
      }
      j = 79;
      if (paramInt1 == j)
      {
        paramSQLiteDatabase.execSQL("ALTER TABLE aggregated_contact ADD COLUMN aggregated_update_timestamp INT NOT NULL DEFAULT 0");
        str1 = "UPDATE aggregated_contact SET aggregated_update_timestamp = ( SELECT MAX(insert_timestamp) FROM raw_contact WHERE aggregated_contact_id = aggregated_contact._id)";
        paramSQLiteDatabase.execSQL(str1);
      }
      paramInt1 += 1;
    }
  }
  
  public final String[] a()
  {
    return new String[] { "CREATE TABLE aggregated_contact(_id INTEGER PRIMARY KEY, tc_id TEXT NOT NULL, contact_name TEXT, contact_transliterated_name TEXT, contact_is_favorite INT, contact_favorite_position INT, contact_handle TEXT, contact_alt_name TEXT, contact_gender TEXT, contact_about TEXT, contact_image_url TEXT, contact_job_title TEXT, contact_company TEXT, contact_access TEXT, contact_common_connections INT, contact_search_time INT, contact_source INT, contact_default_number TEXT, contact_phonebook_id INT, contact_phonebook_hash INT, contact_phonebook_lookup TEXT, contact_spam_score INT, contact_badges INT DEFAULT 0, tc_flag INT NOT NULL DEFAULT 0, aggregated_update_timestamp INT NOT NULL DEFAULT 0, contact_im_id TEXT, settings_flag INT NOT NULL DEFAULT 0)" };
  }
  
  public String[] b()
  {
    String[] arrayOfString = new String[2];
    String str = a;
    arrayOfString[0] = str;
    str = b;
    arrayOfString[1] = str;
    return arrayOfString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */