package com.truecaller.content;

import android.content.ContentUris;
import android.net.Uri;
import android.net.Uri.Builder;

public final class TruecallerContract$g
{
  public static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "msg/msg_conversations_list");
  }
  
  public static Uri a(int paramInt)
  {
    Uri.Builder localBuilder = TruecallerContract.b.buildUpon().appendEncodedPath("msg/msg_conversations_list");
    String str = String.valueOf(paramInt);
    localBuilder.appendQueryParameter("filter", str);
    return localBuilder.build();
  }
  
  public static Uri a(long paramLong)
  {
    return ContentUris.appendId(TruecallerContract.b.buildUpon().appendEncodedPath("msg/msg_conversations_list"), paramLong).build();
  }
  
  public static Uri a(String[] paramArrayOfString, int paramInt)
  {
    Uri.Builder localBuilder = TruecallerContract.b.buildUpon();
    String str1 = "msg/msg_conversations_list";
    localBuilder = localBuilder.appendEncodedPath(str1);
    int i = paramArrayOfString.length;
    int j = 0;
    while (j < i)
    {
      String str2 = paramArrayOfString[j];
      String str3 = "participant_addr";
      str2 = String.valueOf(str2);
      localBuilder.appendQueryParameter(str3, str2);
      j += 1;
    }
    String str4 = String.valueOf(paramInt);
    localBuilder.appendQueryParameter("filter", str4);
    return localBuilder.build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */