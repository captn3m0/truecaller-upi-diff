package com.truecaller.content;

import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d
{
  private final a a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private b(a parama, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static b a(a parama, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    b localb = new com/truecaller/content/b;
    localb.<init>(parama, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */