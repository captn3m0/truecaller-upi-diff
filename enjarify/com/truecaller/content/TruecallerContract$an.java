package com.truecaller.content;

import android.net.Uri;
import android.net.Uri.Builder;

public final class TruecallerContract$an
{
  public static final String[] a = { "actions_dismissed" };
  
  public static Uri a(long paramLong)
  {
    Uri.Builder localBuilder = TruecallerContract.b.buildUpon().appendEncodedPath("msg/msg_thread_stats_specific_update");
    String str = String.valueOf(paramLong);
    return localBuilder.appendQueryParameter("conversation_id", str).build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */