package com.truecaller.content;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.text.TextUtils;
import com.truecaller.common.c.a.a.b;
import com.truecaller.common.c.a.a.f;

final class q
  implements a.b, a.f
{
  public final Uri a(com.truecaller.common.c.a parama, Uri paramUri1, ContentValues paramContentValues, Uri paramUri2)
  {
    paramUri1 = paramContentValues.getAsInteger("data_type");
    int i = paramUri1.intValue();
    int j = 4;
    if (i == j)
    {
      paramUri1 = paramContentValues.getAsString("data1");
      String str = paramContentValues.getAsString("tc_id");
      boolean bool = TextUtils.isEmpty(paramUri1);
      if (!bool)
      {
        bool = TextUtils.isEmpty(str);
        if (!bool)
        {
          SQLiteDatabase localSQLiteDatabase = parama.c();
          SQLiteStatement localSQLiteStatement = localSQLiteDatabase.compileStatement("UPDATE history SET tc_id=? WHERE tc_id IS NULL AND normalized_number=?");
          int k = 1;
          try
          {
            localSQLiteStatement.bindString(k, str);
            j = 2;
            localSQLiteStatement.bindString(j, paramUri1);
            i = localSQLiteStatement.executeUpdateDelete();
            if (i > 0)
            {
              paramUri1 = TruecallerContract.n.a();
              parama.a(paramUri1);
            }
            localSQLiteStatement.close();
            paramUri1 = paramContentValues.getAsLong("data_raw_contact_id");
            if (paramUri1 != null)
            {
              paramUri1 = paramUri1.toString();
              paramContentValues = new String[j];
              j = 0;
              str = null;
              paramContentValues[0] = paramUri1;
              paramContentValues[k] = paramUri1;
              localSQLiteDatabase.execSQL("UPDATE raw_contact SET contact_spam_score=(SELECT MAX(data3) FROM data WHERE data_raw_contact_id=? AND data_type=4) WHERE _id=?", paramContentValues);
              paramUri1 = TruecallerContract.ah.a();
              parama.a(paramUri1);
            }
          }
          finally
          {
            localSQLiteStatement.close();
          }
        }
      }
    }
    return paramUri2;
  }
  
  public final Uri a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues)
  {
    parama = paramContentValues.getAsInteger("data_type");
    if (parama != null)
    {
      int i = parama.intValue();
      int j = 4;
      if (i == j)
      {
        parama = paramContentValues.getAsString("data1");
        boolean bool = TextUtils.isEmpty(parama);
        if (bool)
        {
          parama = "data1";
          paramContentValues.remove(parama);
        }
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */