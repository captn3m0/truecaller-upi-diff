package com.truecaller.content;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.text.TextUtils;
import com.truecaller.common.c.a.a.b;

final class j
  implements a.b
{
  private SQLiteStatement a;
  private com.truecaller.content.d.a b;
  
  public j(com.truecaller.content.d.a parama)
  {
    b = parama;
  }
  
  public final Uri a(com.truecaller.common.c.a parama, Uri paramUri1, ContentValues paramContentValues, Uri paramUri2)
  {
    paramUri1 = paramContentValues.get("tc_id");
    int i;
    SQLiteDatabase localSQLiteDatabase;
    if (paramUri1 == null)
    {
      paramUri1 = paramContentValues.getAsString("normalized_number");
      i = TextUtils.isEmpty(paramUri1);
      if (i == 0)
      {
        localSQLiteDatabase = parama.c();
        parama = a;
        if (parama == null) {
          try
          {
            parama = a;
            if (parama == null)
            {
              parama = "SELECT tc_id FROM data WHERE data1=? AND data_type=4";
              parama = localSQLiteDatabase.compileStatement(parama);
              a = parama;
            }
          }
          finally {}
        }
        parama = a;
        i = 1;
        parama.bindString(i, paramUri1);
      }
    }
    try
    {
      parama = a;
      parama = parama.simpleQueryForString();
      boolean bool = TextUtils.isEmpty(parama);
      if (!bool)
      {
        paramContentValues.clear();
        paramUri1 = "tc_id";
        paramContentValues.put(paramUri1, parama);
        String str1 = "history";
        String str2 = "_id=?";
        String[] arrayOfString = new String[i];
        parama = null;
        paramUri1 = paramUri2.getLastPathSegment();
        arrayOfString[0] = paramUri1;
        int j = 4;
        localSQLiteDatabase.updateWithOnConflict(str1, paramContentValues, str2, arrayOfString, j);
      }
    }
    catch (SQLiteDoneException localSQLiteDoneException)
    {
      for (;;) {}
    }
    b.a(paramContentValues);
    return paramUri2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */