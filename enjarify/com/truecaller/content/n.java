package com.truecaller.content;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.text.TextUtils;
import com.truecaller.common.c.a.a.e;
import com.truecaller.common.c.a.a.f;
import com.truecaller.common.c.a.a.h;
import com.truecaller.log.AssertionUtil;

final class n
  implements a.e, a.f, a.h
{
  private static final String[] a = { "_id" };
  private static final String[] b = { "conversation_id" };
  
  private static Uri a(SQLiteDatabase paramSQLiteDatabase, com.truecaller.common.c.a.a parama, ContentValues paramContentValues, long paramLong)
  {
    Object localObject1 = paramContentValues.getAsLong("_id");
    paramContentValues.remove("_id");
    Object localObject2 = new android/content/ContentValues;
    ((ContentValues)localObject2).<init>(paramContentValues);
    ((ContentValues)localObject2).remove("status");
    int i = 1;
    String[] arrayOfString1 = new String[i];
    String str1 = String.valueOf(paramLong);
    String str2 = null;
    arrayOfString1[0] = str1;
    str1 = "msg_messages";
    Object localObject3 = "status & 2 = 2 AND conversation_id = ?";
    int j = paramSQLiteDatabase.update(str1, paramContentValues, (String)localObject3, arrayOfString1);
    Cursor localCursor = null;
    if (j > 0)
    {
      l1 = -1;
      String str3 = "msg_messages";
      try
      {
        String[] arrayOfString2 = a;
        String str4 = "status & 2 = 2 AND conversation_id = ?";
        localObject3 = paramSQLiteDatabase;
        localCursor = paramSQLiteDatabase.query(str3, arrayOfString2, str4, arrayOfString1, null, null, null);
        boolean bool1 = localCursor.moveToFirst();
        if (bool1) {
          l1 = localCursor.getLong(0);
        }
        if (localCursor != null) {
          localCursor.close();
        }
        if (localObject1 != null)
        {
          long l2 = ((Long)localObject1).longValue();
          boolean bool2 = l2 < l1;
          if (bool2)
          {
            localObject2 = new String[i];
            localObject1 = ((Long)localObject1).toString();
            localObject2[0] = localObject1;
            paramSQLiteDatabase.delete("msg_messages", "(_id=?)", (String[])localObject2);
            str2 = "msg_entities";
            localObject1 = "message_id=?";
            paramSQLiteDatabase.delete(str2, (String)localObject1, (String[])localObject2);
          }
        }
        paramSQLiteDatabase = Long.valueOf(l1);
      }
      finally
      {
        if (localCursor != null) {
          localCursor.close();
        }
      }
    }
    str1 = "msg_messages";
    long l1 = paramSQLiteDatabase.insert(str1, null, paramContentValues);
    paramSQLiteDatabase = Long.valueOf(l1);
    l1 = paramSQLiteDatabase.longValue();
    return parama.a(l1);
  }
  
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    Object localObject1 = paramContentValues;
    String[] arrayOfString1 = paramArrayOfString;
    long l1 = ContentUris.parseId(paramUri);
    int i = 1;
    long l2 = -1;
    boolean bool1 = l1 < l2;
    boolean bool2;
    if (bool1)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localSQLiteDatabase = null;
    }
    Object localObject3 = new String[0];
    AssertionUtil.isTrue(bool2, (String[])localObject3);
    SQLiteDatabase localSQLiteDatabase = parama.c();
    if (arrayOfString1 != null)
    {
      localObject3 = String.valueOf(l1);
      arrayOfString1 = (String[])org.c.a.a.a.a.b(arrayOfString1, localObject3);
    }
    else
    {
      arrayOfString1 = new String[i];
      localObject3 = String.valueOf(l1);
      arrayOfString1[0] = localObject3;
    }
    boolean bool3 = TextUtils.isEmpty(paramString);
    if (bool3)
    {
      localObject3 = "(_id=?)";
    }
    else
    {
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("(");
      localObject4 = paramString;
      ((StringBuilder)localObject3).append(paramString);
      localObject4 = ") AND (_id=?)";
      ((StringBuilder)localObject3).append((String)localObject4);
      localObject3 = ((StringBuilder)localObject3).toString();
    }
    Object localObject4 = "msg_messages";
    int k = localSQLiteDatabase.update((String)localObject4, (ContentValues)localObject1, (String)localObject3, arrayOfString1);
    if (k > 0)
    {
      bool3 = false;
      localObject3 = null;
      String str1 = "msg_messages";
      try
      {
        Object localObject5 = b;
        String str2 = "(_id=?)";
        String[] arrayOfString2 = new String[i];
        localObject4 = String.valueOf(l1);
        arrayOfString2[0] = localObject4;
        localObject4 = localSQLiteDatabase;
        localObject3 = localSQLiteDatabase.query(str1, (String[])localObject5, str2, arrayOfString2, null, null, null);
        bool1 = ((Cursor)localObject3).moveToFirst();
        if (bool1)
        {
          long l3 = ((Cursor)localObject3).getLong(0);
          if (localObject3 != null) {
            ((Cursor)localObject3).close();
          }
          localObject3 = TruecallerContract.ab.a();
          localObject5 = parama;
          parama.a((Uri)localObject3);
          localObject3 = "status";
          localObject1 = ((ContentValues)localObject1).getAsInteger((String)localObject3);
          if (localObject1 != null)
          {
            int m = ((Integer)localObject1).intValue();
            int j = 2;
            m &= j;
            if (m != 0)
            {
              localObject1 = "msg_messages";
              localObject5 = "(status& 2) != 0 AND conversation_id = ? AND _id != ?";
              localObject3 = new String[j];
              localObject4 = String.valueOf(l3);
              localObject3[0] = localObject4;
              String str3 = String.valueOf(l1);
              localObject3[i] = str3;
              localSQLiteDatabase.delete((String)localObject1, (String)localObject5, (String[])localObject3);
            }
          }
        }
        else
        {
          return k;
        }
      }
      finally
      {
        if (localObject3 != null) {
          ((Cursor)localObject3).close();
        }
      }
    }
    return k;
  }
  
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    int i = 1;
    boolean bool1;
    if (paramString == null)
    {
      bool1 = true;
    }
    else
    {
      bool1 = false;
      paramString = null;
    }
    Object localObject1 = { "Selection is not supported for this operation" };
    AssertionUtil.isTrue(bool1, (String[])localObject1);
    long l1 = ContentUris.parseId(paramUri);
    long l2 = -1;
    boolean bool2 = l1 < l2;
    boolean bool3;
    if (bool2)
    {
      bool3 = true;
    }
    else
    {
      bool3 = false;
      localObject1 = null;
    }
    String[] arrayOfString1 = new String[0];
    AssertionUtil.isTrue(bool3, arrayOfString1);
    localObject1 = parama.c();
    boolean bool4 = ((SQLiteDatabase)localObject1).inTransaction();
    Object localObject2 = { "This method can work properly only in transaction" };
    AssertionUtil.isTrue(bool4, (String[])localObject2);
    parama1 = new String[i];
    paramUri = String.valueOf(l1);
    parama1[0] = paramUri;
    paramUri = null;
    String str1 = "msg_messages";
    try
    {
      String[] arrayOfString2 = b;
      String str2 = "(_id=?)";
      localObject2 = localObject1;
      paramUri = ((SQLiteDatabase)localObject1).query(str1, arrayOfString2, str2, parama1, null, null, null);
      bool1 = paramUri.moveToFirst();
      if (bool1) {
        paramUri.getLong(0);
      }
      if (paramUri != null) {
        paramUri.close();
      }
      paramUri = "msg_messages";
      paramString = "_id=?";
      i = ((SQLiteDatabase)localObject1).delete(paramUri, paramString, parama1);
      if (i > 0)
      {
        paramUri = TruecallerContract.ab.a();
        parama.a(paramUri);
      }
      return i;
    }
    finally
    {
      if (paramUri != null) {
        paramUri.close();
      }
    }
  }
  
  public final Uri a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues)
  {
    boolean bool1 = paramContentValues.containsKey("conversation_id");
    Object localObject = new String[0];
    AssertionUtil.isTrue(bool1, (String[])localObject);
    long l1 = paramContentValues.getAsLong("conversation_id").longValue();
    paramUri = parama.c();
    boolean bool2 = paramUri.inTransaction();
    String[] arrayOfString = { "This method can work properly only in transaction" };
    AssertionUtil.isTrue(bool2, arrayOfString);
    Integer localInteger = paramContentValues.getAsInteger("status");
    if (localInteger != null)
    {
      int i = localInteger.intValue();
      int j = 2;
      i &= j;
      if (i == j)
      {
        parama1 = a(paramUri, parama1, paramContentValues, l1);
        paramUri = TruecallerContract.ab.a();
        parama.a(paramUri);
        return parama1;
      }
    }
    localObject = "msg_messages";
    long l2 = paramUri.insertOrThrow((String)localObject, null, paramContentValues);
    l1 = 0L;
    boolean bool3 = l2 < l1;
    if (!bool3)
    {
      localObject = TruecallerContract.ab.a();
      parama.a((Uri)localObject);
      return parama1.a(l2);
    }
    parama = new android/database/sqlite/SQLiteException;
    parama.<init>("Can not insert new message");
    throw parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */