package com.truecaller.content;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import com.truecaller.common.c.a;
import com.truecaller.common.c.a.a.b;
import com.truecaller.log.AssertionUtil.AlwaysFatal;

final class x
  implements a.b
{
  public final Uri a(a parama, Uri paramUri1, ContentValues paramContentValues, Uri paramUri2)
  {
    paramUri2 = paramContentValues.getAsString("value");
    boolean bool = TextUtils.isEmpty(paramUri2);
    int i = 1;
    bool ^= i;
    Object localObject1 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool, (String[])localObject1);
    SQLiteDatabase localSQLiteDatabase = parama.c();
    localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>(i);
    Object localObject2 = Boolean.TRUE;
    ((ContentValues)localObject1).put("is_top_spammer", (Boolean)localObject2);
    localObject2 = "count";
    paramContentValues = paramContentValues.getAsInteger((String)localObject2);
    ((ContentValues)localObject1).put("top_spam_score", paramContentValues);
    paramContentValues = "msg_participants";
    String str = "normalized_destination=? COLLATE NOCASE";
    String[] arrayOfString = new String[i];
    arrayOfString[0] = paramUri2;
    int j = localSQLiteDatabase.update(paramContentValues, (ContentValues)localObject1, str, arrayOfString);
    if (j > 0)
    {
      paramContentValues = TruecallerContract.ae.a();
      parama.a(paramContentValues);
    }
    return paramUri1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */