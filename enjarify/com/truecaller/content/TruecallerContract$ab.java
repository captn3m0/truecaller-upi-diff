package com.truecaller.content;

import android.content.ContentUris;
import android.net.Uri;
import android.net.Uri.Builder;

public final class TruecallerContract$ab
{
  public static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "msg/msg_messages_with_entities");
  }
  
  public static Uri a(long paramLong)
  {
    return ContentUris.appendId(TruecallerContract.b.buildUpon().appendEncodedPath("msg/msg_messages_with_entities"), paramLong).build();
  }
  
  public static Uri a(String paramString)
  {
    return TruecallerContract.b.buildUpon().appendEncodedPath("msg/msg_messages_with_entities_filtered").appendQueryParameter("filter", paramString).build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */