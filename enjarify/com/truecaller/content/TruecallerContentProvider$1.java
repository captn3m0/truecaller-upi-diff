package com.truecaller.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

final class TruecallerContentProvider$1
  extends BroadcastReceiver
{
  TruecallerContentProvider$1(TruecallerContentProvider paramTruecallerContentProvider) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    long l = paramIntent.getLongExtra("ARG_DELAY", 0L);
    TruecallerContentProvider localTruecallerContentProvider = a;
    localTruecallerContentProvider.a(l);
    b.sendEmptyMessageDelayed(2, l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContentProvider.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */