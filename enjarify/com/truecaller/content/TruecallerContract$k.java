package com.truecaller.content;

import android.net.Uri;
import android.provider.BaseColumns;

public final class TruecallerContract$k
  implements BaseColumns, TruecallerContract.j
{
  public static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "data");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */