package com.truecaller.content.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public final class e
  extends h
{
  public final int a()
  {
    return 68;
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("\n            INSERT OR IGNORE INTO msg_thread_stats (\n            conversation_id,\n            filter,\n            latest_message_id,\n            latest_message_status,\n            latest_message_entities_types,\n            latest_sim_token,\n            date_sorting,\n            actions_dismissed,\n            snippet_text,\n            unread_messages_count\n            )\n            SELECT\n            _id,\n            1,\n            latest_message_id,\n            latest_message_status,\n            latest_message_entities_types,\n            latest_sim_token,\n            date_sorting,\n            actions_dismissed,\n            snippet_text,\n            unread_messages_count\n            FROM msg_conversations\n        ");
    paramSQLiteDatabase.execSQL("\n\t        CREATE TEMPORARY TABLE temp_conversations_table AS SELECT\n             _id,\n            type,\n            tc_group_id,\n            has_outgoing_messages,\n\t\t\tphonebook_count,\n\t\t\twhite_list_count,\n\t\t\tblacklist_count,\n\t\t\ttop_spammer_count,\n\t\t\tsplit_criteria,\n\t\t\thas_spam_messages\n            FROM msg_conversations\n        ");
    paramSQLiteDatabase.execSQL("\n            DROP TABLE msg_conversations\n        ");
    paramSQLiteDatabase.execSQL("\n           CREATE TABLE msg_conversations(\n           _id INTEGER PRIMARY KEY AUTOINCREMENT,\n           type INTEGER DEFAULT(0),\n           tc_group_id TEXT,\n           has_outgoing_messages INTEGER DEFAULT(0),\n           phonebook_count INTEGER DEFAULT(0),\n           white_list_count INTEGER DEFAULT(0),\n           blacklist_count INTEGER DEFAULT(0),\n           top_spammer_count INTEGER DEFAULT(0),\n           split_criteria INTEGER DEFAULT(0),\n           has_spam_messages INTEGER DEFAULT(0)\n           )\n        ");
    paramSQLiteDatabase.execSQL("\n           INSERT INTO msg_conversations SELECT * FROM temp_conversations_table\n        ");
    paramSQLiteDatabase.execSQL("\n            DROP TABLE temp_conversations_table\n        ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */