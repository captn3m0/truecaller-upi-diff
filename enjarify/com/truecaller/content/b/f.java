package com.truecaller.content.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public final class f
  extends h
{
  public final int a()
  {
    return 84;
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("\n               CREATE TABLE msg_im_reactions (\n               _id INTEGER PRIMARY KEY AUTOINCREMENT,\n               message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE,\n               emoji TEXT,\n               from_peer_id TEXT,\n               send_date INTEGER DEFAULT(0)\n               )\n            ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */