package com.truecaller.content.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public abstract class h
{
  public abstract int a();
  
  public abstract void a(SQLiteDatabase paramSQLiteDatabase);
  
  public final void a(SQLiteDatabase paramSQLiteDatabase, int paramInt)
  {
    String str = "db";
    k.b(paramSQLiteDatabase, str);
    if (paramInt >= 0)
    {
      int i = a();
      if (paramInt <= i)
      {
        a(paramSQLiteDatabase);
        return;
      }
    }
    paramSQLiteDatabase = new com/truecaller/content/b/g;
    paramSQLiteDatabase.<init>(paramInt);
    throw ((Throwable)paramSQLiteDatabase);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */