package com.truecaller.content.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public final class a
  extends h
{
  public final int a()
  {
    return 11;
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("\n               CREATE TABLE msg_conversations (\n               _id INTEGER PRIMARY KEY AUTOINCREMENT,\n               type INTEGER DEFAULT(0),\n               tc_group_id TEXT,\n               latest_message_id INTEGER,\n               latest_message_status INTEGER,\n               latest_message_entities_types TEXT DEFAULT(''),\n               latest_sim_token TEXT DEFAULT('-1'),\n               date_sorting INTEGER,\n               unread_messages_count INTEGER DEFAULT(0),\n               snippet_text TEXT DEFAULT(''),\n               actions_dismissed INTEGER DEFAULT(0),\n               has_outgoing_messages INTEGER DEFAULT(0)\n               )\n            ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */