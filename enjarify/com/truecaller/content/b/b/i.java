package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public final class i
  implements com.truecaller.content.b.i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_messages ADD COLUMN reply_to_msg_id INTEGER DEFAULT(-1)");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */