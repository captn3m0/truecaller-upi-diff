package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class j
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("\n                UPDATE msg_conversations\n                SET split_criteria = CASE phonebook_count OR white_list_count OR blacklist_count OR has_outgoing_messages\n                    WHEN 1 THEN 0\n                    ELSE 1 END\n            ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */