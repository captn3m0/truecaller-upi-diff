package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import com.truecaller.content.b.i;

public final class k
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    c.g.b.k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_im_reactions ADD COLUMN status INTEGER DEFAULT(0)");
    paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_im_reactions_status ON msg_im_reactions(status)");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */