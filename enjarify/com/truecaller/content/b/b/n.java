package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class n
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("CREATE TABLE msg_im_unsupported_events (\n                _id INTEGER PRIMARY KEY AUTOINCREMENT,\n                event BLOB NOT NULL,\n                api_version INTEGER DEFAULT(0))");
    paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_im_unsupported_events_api_version\n                ON msg_im_unsupported_events (api_version)");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */