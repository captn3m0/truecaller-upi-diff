package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class d
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("ALTER TABLE raw_contact ADD COLUMN settings_flag INT NOT NULL DEFAULT 0");
    paramSQLiteDatabase.execSQL("ALTER TABLE aggregated_contact ADD COLUMN settings_flag INT NOT NULL DEFAULT 0");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */