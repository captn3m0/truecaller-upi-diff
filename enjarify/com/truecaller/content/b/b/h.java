package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class h
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("CREATE INDEX idx_raw_contact_contact_im_id ON raw_contact (contact_im_id)");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */