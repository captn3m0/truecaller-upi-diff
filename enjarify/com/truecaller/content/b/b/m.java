package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class m
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("\n            CREATE TABLE msg_history_transport_info (\n                _id INTEGER PRIMARY KEY AUTOINCREMENT,\n                raw_id INTEGER NOT NULL,\n                call_log_id INTEGER DEFAULT NULL,\n                message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE,\n                type INTEGER DEFAULT(0),\n                features INTEGER,\n                number_type TEXT\n            )\n        ");
    paramSQLiteDatabase.execSQL("\n            CREATE INDEX idx_msg_history_transport_info_message_id ON msg_history_transport_info(message_id)\n        ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */