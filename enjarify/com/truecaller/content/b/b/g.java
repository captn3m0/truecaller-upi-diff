package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class g
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("\n                CREATE TABLE contact_settings (\n                    tc_id TEXT UNIQUE NOT NULL,\n                    hidden_from_identified INT NOT NULL DEFAULT 0\n                )\n        ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */