package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class p
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("\n                CREATE TABLE msg_im_group_info (\n                    im_group_id TEXT PRIMARY KEY,\n                    title TEXT,\n                    avatar TEXT,\n                    invited_date INTEGER NOT NULL,\n                    invited_by TEXT\n                )\n        ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */