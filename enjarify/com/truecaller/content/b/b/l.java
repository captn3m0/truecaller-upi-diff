package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class l
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("DROP INDEX IF EXISTS idx_msg_messages_date");
    paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_messages_conversation_id_date ON msg_messages (conversation_id, date)");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */