package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class o
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("\n                CREATE TABLE msg_im_group_participants (\n                    im_group_id TEXT NOT NULL,\n                    im_peer_id TEXT NOT NULL,\n                    roles INTEGER NOT NULL DEFAULT 0,\n                    UNIQUE(im_group_id, im_peer_id)\n                )\n        ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */