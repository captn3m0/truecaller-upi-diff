package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class e
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("DELETE FROM msg_messages WHERE transport = 6");
    paramSQLiteDatabase.execSQL("\n                CREATE TABLE msg_status_transport_info (\n                    _id INTEGER PRIMARY KEY AUTOINCREMENT,\n                    raw_id TEXT NOT NULL,\n                    message_id INTEGER NOT NULL REFERENCES msg_messages (_id) ON DELETE CASCADE\n                )\n            ");
    paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_status_transport_info_message_id  ON msg_status_transport_info (message_id)");
    paramSQLiteDatabase.execSQL("CREATE INDEX idx_msg_status_transport_info_raw_id  ON msg_status_transport_info (raw_id)");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */