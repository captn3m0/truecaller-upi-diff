package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class c
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("ALTER TABLE raw_contact ADD COLUMN contact_im_id TEXT");
    paramSQLiteDatabase.execSQL("ALTER TABLE aggregated_contact ADD COLUMN contact_im_id TEXT");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */