package com.truecaller.content.b.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;
import com.truecaller.content.b.i;

public final class b
  implements i
{
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_im_group_info ADD COLUMN actions INTEGER NOT NULL DEFAULT 0");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_im_group_info ADD COLUMN role_update_restriction_mask INTEGER NOT NULL DEFAULT 0");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_im_group_info ADD COLUMN role_update_mask INTEGER NOT NULL DEFAULT 0");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_im_group_info ADD COLUMN self_role_update_mask INTEGER NOT NULL DEFAULT 0");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */