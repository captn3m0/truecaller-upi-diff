package com.truecaller.content.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public final class d
  extends h
{
  public final int a()
  {
    return 66;
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("\n               ALTER TABLE msg_conversations ADD COLUMN has_spam_messages INTEGER DEFAULT(0)\n            ");
    paramSQLiteDatabase.execSQL("\n           CREATE TABLE msg_thread_stats (\n           latest_message_id INTEGER,\n           latest_message_status INTEGER,\n           latest_message_entities_types TEXT DEFAULT(''),\n           unread_messages_count INTEGER DEFAULT(0),\n           latest_sim_token TEXT DEFAULT('-1'),\n           date_sorting INTEGER,\n           snippet_text TEXT DEFAULT(''),\n           actions_dismissed INTEGER DEFAULT(0),\n           filter INTEGER NOT NULL,\n           conversation_id INTEGER NOT NULL REFERENCES msg_conversations (_id) ON DELETE CASCADE,\n           UNIQUE(filter, conversation_id) ON CONFLICT REPLACE)\n        ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */