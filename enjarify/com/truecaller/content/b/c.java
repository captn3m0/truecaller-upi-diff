package com.truecaller.content.b;

import android.database.sqlite.SQLiteDatabase;
import c.g.b.k;

public final class c
  extends h
{
  private static void a(SQLiteDatabase paramSQLiteDatabase, String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("\n                UPDATE msg_conversations\n                SET ");
    localStringBuilder.append(paramString1);
    localStringBuilder.append(" = (SELECT SUM(");
    localStringBuilder.append(paramString2);
    localStringBuilder.append(")\n                    FROM msg_participants\n                    WHERE msg_participants._id IN (SELECT participant_id\n                        FROM msg_conversation_participants\n                        WHERE conversation_id = msg_conversations._id))\n            ");
    paramString1 = localStringBuilder.toString();
    paramSQLiteDatabase.execSQL(paramString1);
  }
  
  public final int a()
  {
    return 64;
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    k.b(paramSQLiteDatabase, "db");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_conversations ADD COLUMN phonebook_count INTEGER DEFAULT (0)");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_conversations ADD COLUMN white_list_count INTEGER DEFAULT (0)");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_conversations ADD COLUMN blacklist_count INTEGER DEFAULT (0)");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_conversations ADD COLUMN top_spammer_count INTEGER DEFAULT (0)");
    paramSQLiteDatabase.execSQL("ALTER TABLE msg_conversations ADD COLUMN split_criteria INTEGER DEFAULT (0)");
    paramSQLiteDatabase.execSQL("\n                UPDATE msg_conversations\n                SET has_outgoing_messages = (SELECT COUNT(*)\n                    FROM msg_messages\n                    WHERE (status & 3) = 1\n                    AND conversation_id = msg_conversations._id)\n            ");
    a(paramSQLiteDatabase, "phonebook_count", "phonebook_count");
    a(paramSQLiteDatabase, "white_list_count", "filter_action = 2");
    a(paramSQLiteDatabase, "blacklist_count", "filter_action = 1");
    a(paramSQLiteDatabase, "top_spammer_count", "is_top_spammer");
    paramSQLiteDatabase.execSQL("\n                UPDATE msg_conversations\n                SET split_criteria = CASE phonebook_count OR white_list_count OR blacklist_count OR top_spammer_count OR has_outgoing_messages\n                    WHEN 1 THEN 0\n                    ELSE 1 END\n            ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */