package com.truecaller.content.b;

import android.database.sqlite.SQLiteDatabase;
import com.truecaller.content.b.b.a;
import com.truecaller.content.b.b.b;
import com.truecaller.content.b.b.c;
import com.truecaller.content.b.b.d;
import com.truecaller.content.b.b.e;
import com.truecaller.content.b.b.f;
import com.truecaller.content.b.b.g;
import com.truecaller.content.b.b.h;
import com.truecaller.content.b.b.l;
import com.truecaller.content.b.b.m;
import com.truecaller.content.b.b.n;
import com.truecaller.content.b.b.o;
import com.truecaller.content.b.b.p;

public final class j
{
  public static final j a;
  
  static
  {
    j localj = new com/truecaller/content/b/j;
    localj.<init>();
    a = localj;
  }
  
  public static final void a(int paramInt, SQLiteDatabase paramSQLiteDatabase)
  {
    c.g.b.k.b(paramSQLiteDatabase, "db");
    Object localObject1 = null;
    Object localObject2;
    switch (paramInt)
    {
    default: 
      localObject2 = new java/lang/IllegalStateException;
      paramSQLiteDatabase = "Version migration is not provided".toString();
      ((IllegalStateException)localObject2).<init>(paramSQLiteDatabase);
      throw ((Throwable)localObject2);
    case 111: 
      localObject2 = new com/truecaller/content/b/b/h;
      ((h)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 110: 
      localObject2 = new com/truecaller/content/b/b/g;
      ((g)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 109: 
      localObject2 = new com/truecaller/content/b/b/f;
      ((f)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 108: 
      localObject2 = new com/truecaller/content/b/b/e;
      ((e)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 107: 
      localObject2 = new com/truecaller/content/b/b/d;
      ((d)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 105: 
      localObject2 = new com/truecaller/content/b/b/c;
      ((c)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 104: 
      localObject2 = new com/truecaller/content/b/b/b;
      ((b)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 101: 
      localObject2 = new com/truecaller/content/b/b/a;
      ((a)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 96: 
      localObject2 = new com/truecaller/content/b/b/p;
      ((p)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 95: 
      localObject2 = new com/truecaller/content/b/b/o;
      ((o)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 94: 
      localObject2 = new com/truecaller/content/b/b/n;
      ((n)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 92: 
      localObject2 = new com/truecaller/content/b/b/m;
      ((m)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 90: 
      localObject2 = new com/truecaller/content/b/b/l;
      ((l)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 89: 
      localObject2 = new com/truecaller/content/b/b/k;
      ((com.truecaller.content.b.b.k)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 88: 
      localObject2 = new com/truecaller/content/b/b/j;
      ((com.truecaller.content.b.b.j)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
      break;
    case 85: 
      localObject2 = new com/truecaller/content/b/b/i;
      ((com.truecaller.content.b.b.i)localObject2).<init>();
      localObject1 = localObject2;
      localObject1 = (i)localObject2;
    }
    if (localObject1 != null)
    {
      ((i)localObject1).a(paramSQLiteDatabase);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */