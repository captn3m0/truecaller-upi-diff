package com.truecaller.content;

import android.net.Uri;
import android.net.Uri.Builder;

public final class TruecallerContract$f
{
  public static String[] a = tmp28_15;
  
  static
  {
    String[] tmp5_2 = new String[6];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "first_name";
    tmp6_5[1] = "last_name";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "sorting_key_1";
    tmp15_6[3] = "sorting_key_2";
    tmp15_6[4] = "sorting_group_1";
    String[] tmp28_15 = tmp15_6;
    tmp28_15[5] = "sorting_group_2";
  }
  
  public static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "contact_sorting_index");
  }
  
  public static Uri a(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    paramString1 = a().buildUpon().appendEncodedPath("fast_scroll").appendQueryParameter("sorting_mode", paramString1).appendQueryParameter("phonebook_filter", paramString2);
    String str = String.valueOf(paramBoolean1);
    paramString1 = paramString1.appendQueryParameter("hidden_from_identified_filter", str);
    str = String.valueOf(paramBoolean2);
    return paramString1.appendQueryParameter("identified_spam_score_filter", str).build();
  }
  
  public static Uri b()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "sorted_contacts_with_data");
  }
  
  public static Uri c()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "sorted_contacts_shallow");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */