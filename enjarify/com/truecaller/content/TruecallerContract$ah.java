package com.truecaller.content;

import android.net.Uri;
import android.provider.BaseColumns;

public final class TruecallerContract$ah
  implements BaseColumns, TruecallerContract.d
{
  public static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "raw_contact");
  }
  
  public static Uri b()
  {
    return Uri.withAppendedPath(a(), "data");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */