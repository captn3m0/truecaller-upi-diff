package com.truecaller.content;

import android.net.Uri;
import android.net.Uri.Builder;
import java.util.Iterator;
import java.util.Set;

public final class TruecallerContract$ap
{
  public static Uri a(Set paramSet)
  {
    Uri.Builder localBuilder = TruecallerContract.b.buildUpon();
    Object localObject = "conversation_messages";
    localBuilder = localBuilder.appendEncodedPath((String)localObject);
    paramSet = paramSet.iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      localObject = (Long)paramSet.next();
      String str = "ids";
      localObject = String.valueOf(localObject);
      localBuilder.appendQueryParameter(str, (String)localObject);
    }
    return localBuilder.build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.ap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */