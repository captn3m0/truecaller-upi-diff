package com.truecaller.content;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import c.a.f;
import c.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.common.c.a.a.g;
import java.util.Collection;
import java.util.List;

public final class l
  implements a.g
{
  public final Cursor a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    Object localObject1 = paramUri;
    Object localObject2 = parama;
    k.b(parama, "provider");
    Object localObject3 = parama1;
    k.b(parama1, "helper");
    k.b(paramUri, "uri");
    Object localObject4 = paramUri.getQueryParameter("group_id");
    localObject3 = paramUri.getQueryParameter("peer_id");
    localObject1 = paramUri.getQueryParameter("self_peer_id");
    Object localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>("\n    SELECT\n        gp.im_peer_id as im_peer_id,\n        gp.roles as roles,\n        iu.normalized_number as normalized_number,\n        ac.contact_name as name,\n        ac.contact_image_url as image_url,\n        IFNULL(ac.contact_phonebook_id, -1) as phonebook_id,\n        ac.tc_id as tc_contact_id,\n        d.data9 as raw_number,\n        gp.im_peer_id = ? AS is_self\n    FROM msg_im_group_participants gp\n        LEFT JOIN msg_im_users iu ON gp.im_peer_id = iu.im_peer_id\n        LEFT JOIN data d ON d.data_type = 4\n            AND d.data1 = iu.normalized_number\n        LEFT JOIN raw_contact rc on d.data_raw_contact_id = rc._id\n            OR  gp.im_peer_id = rc.contact_im_id\n        LEFT JOIN aggregated_contact ac on rc.aggregated_contact_id = ac._id\n");
    int i = 1;
    Object localObject6 = new String[i];
    if (localObject1 == null) {
      localObject1 = "";
    }
    localObject6[0] = localObject1;
    localObject1 = m.c((Object[])localObject6);
    if (localObject4 != null)
    {
      localObject3 = "WHERE gp.im_group_id = ?";
      ((StringBuilder)localObject5).append((String)localObject3);
      ((List)localObject1).add(localObject4);
    }
    else
    {
      if (localObject3 == null) {
        break label424;
      }
      localObject4 = "WHERE gp.im_peer_id = ?";
      ((StringBuilder)localObject5).append((String)localObject4);
      ((List)localObject1).add(localObject3);
    }
    localObject4 = " GROUP BY gp.im_peer_id";
    ((StringBuilder)localObject5).append((String)localObject4);
    if (paramString2 != null)
    {
      localObject3 = String.valueOf(paramString2);
      localObject4 = " ORDER BY ".concat((String)localObject3);
      ((StringBuilder)localObject5).append((String)localObject4);
    }
    localObject4 = parama.c();
    localObject3 = ((StringBuilder)localObject5).toString();
    localObject5 = "query.toString()";
    k.a(localObject3, (String)localObject5);
    if (paramArrayOfString1 != null)
    {
      localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>("SELECT ");
      i = 0;
      int j = 63;
      parama1 = paramArrayOfString1;
      paramUri = null;
      paramArrayOfString1 = null;
      paramString2 = null;
      localObject6 = f.a(parama1, null, null, null, 0, null, j);
      ((StringBuilder)localObject5).append((String)localObject6);
      localObject6 = " FROM (";
      ((StringBuilder)localObject5).append((String)localObject6);
      ((StringBuilder)localObject5).append((String)localObject3);
      char c = ')';
      ((StringBuilder)localObject5).append(c);
      localObject3 = ((StringBuilder)localObject5).toString();
    }
    localObject1 = (Collection)localObject1;
    localObject5 = new String[0];
    localObject1 = ((Collection)localObject1).toArray((Object[])localObject5);
    if (localObject1 != null)
    {
      localObject1 = (String[])localObject1;
      localObject1 = ((SQLiteDatabase)localObject4).rawQuery((String)localObject3, (String[])localObject1);
      localObject4 = parama.getContext();
      if (localObject4 != null)
      {
        localObject4 = ((Context)localObject4).getContentResolver();
        if ((localObject4 != null) && (localObject1 != null))
        {
          localObject2 = TruecallerContract.t.a();
          ((Cursor)localObject1).setNotificationUri((ContentResolver)localObject4, (Uri)localObject2);
        }
      }
      return (Cursor)localObject1;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw ((Throwable)localObject1);
    label424:
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */