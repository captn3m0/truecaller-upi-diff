package com.truecaller.content;

import android.content.ContentValues;
import android.net.Uri;
import com.truecaller.common.c.a.a.c;

public final class k
  implements a.c
{
  private final com.truecaller.content.d.a a;
  
  public k(com.truecaller.content.d.a parama)
  {
    a = parama;
  }
  
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues, int paramInt)
  {
    String str = "provider";
    c.g.b.k.b(parama, str);
    c.g.b.k.b(parama1, "helper");
    c.g.b.k.b(paramUri, "uri");
    parama = "values";
    c.g.b.k.b(paramContentValues, parama);
    if (paramInt > 0)
    {
      parama = a;
      parama.a(paramContentValues);
    }
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */