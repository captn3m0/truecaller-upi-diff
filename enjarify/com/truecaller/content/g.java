package com.truecaller.content;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.common.c.a.a.f;
import com.truecaller.log.AssertionUtil.AlwaysFatal;

public final class g
  implements a.f
{
  public final Uri a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues)
  {
    k.b(parama, "provider");
    k.b(parama1, "helper");
    k.b(paramUri, "uri");
    k.b(paramContentValues, "values");
    paramUri = paramContentValues.getAsString("value");
    k.a(paramUri, "values.getAsString(Filters.VALUE)");
    Object localObject1 = paramContentValues.getAsInteger("rule");
    k.a(localObject1, "values.getAsInteger(Filters.RULE)");
    int i = ((Integer)localObject1).intValue();
    String str = paramContentValues.getAsString("tracking_type");
    Object localObject2 = paramUri;
    localObject2 = (CharSequence)paramUri;
    int k = ((CharSequence)localObject2).length();
    int m = 1;
    int n = 0;
    if (k > 0)
    {
      k = 1;
    }
    else
    {
      k = 0;
      localObject2 = null;
    }
    String[] arrayOfString1 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(k, arrayOfString1);
    localObject2 = parama.c();
    long l = -1;
    Object localObject3 = "filters";
    int i1 = 0;
    Object localObject4 = null;
    int i2 = 2;
    try
    {
      l = ((SQLiteDatabase)localObject2).insertWithOnConflict((String)localObject3, null, paramContentValues, i2);
    }
    catch (SQLiteException localSQLiteException)
    {
      localObject3 = "filters";
      localObject4 = "value=?";
      String[] arrayOfString2 = new String[m];
      arrayOfString2[0] = paramUri;
      ((SQLiteDatabase)localObject2).update((String)localObject3, paramContentValues, (String)localObject4, arrayOfString2);
    }
    paramContentValues = new android/content/ContentValues;
    paramContentValues.<init>(m);
    localObject3 = "filter_action";
    i1 = TruecallerContract.ae.a(i);
    localObject4 = Integer.valueOf(i1);
    paramContentValues.put((String)localObject3, (Integer)localObject4);
    if (str != null)
    {
      int i3 = str.hashCode();
      i1 = -467061482;
      int j;
      int i4;
      if (i3 != i1)
      {
        i = 40276826;
        if (i3 != i)
        {
          i = 75532016;
          if (i3 != i) {
            break label461;
          }
          localObject1 = "OTHER";
          j = str.equals(localObject1);
          if (j == 0) {
            break label461;
          }
        }
        else
        {
          localObject1 = "PHONE_NUMBER";
          j = str.equals(localObject1);
          if (j == 0) {
            break label461;
          }
        }
        localObject1 = "msg_participants";
        str = "normalized_destination=?";
        localObject3 = new String[m];
        localObject3[0] = paramUri;
        i4 = ((SQLiteDatabase)localObject2).update((String)localObject1, paramContentValues, str, (String[])localObject3);
        if (i4 > 0) {
          n = 1;
        }
      }
      else
      {
        localObject3 = "COUNTRY_CODE";
        boolean bool = str.equals(localObject3);
        if (bool) {
          if (j != m)
          {
            localObject1 = "msg_participants";
            str = "country_code=? AND normalized_destination NOT IN (SELECT value FROM filters WHERE rule = '1' AND tracking_type = 'PHONE_NUMBER')";
            localObject3 = new String[m];
            localObject3[0] = paramUri;
            i4 = ((SQLiteDatabase)localObject2).update((String)localObject1, paramContentValues, str, (String[])localObject3);
            if (i4 > 0) {
              n = 1;
            }
          }
          else
          {
            parama = new java/lang/IllegalStateException;
            parama.<init>("Country whitelisting is not supported");
            throw ((Throwable)parama);
          }
        }
      }
    }
    label461:
    if (n != 0)
    {
      paramUri = TruecallerContract.ae.a();
      parama.a(paramUri);
      paramUri = TruecallerContract.g.a();
      parama.a(paramUri);
    }
    parama = parama1.a(l);
    k.a(parama, "helper.getContentUri(resultId)");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */