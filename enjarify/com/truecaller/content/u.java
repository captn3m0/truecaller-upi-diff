package com.truecaller.content;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import c.g.b.k;
import com.truecaller.common.c.a.a.g;
import java.util.Arrays;

public final class u
  implements a.g
{
  public final Cursor a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    k.b(parama, "provider");
    paramArrayOfString1 = "helper";
    k.b(parama1, paramArrayOfString1);
    parama1 = "uri";
    k.b(paramUri, parama1);
    if (paramString1 != null)
    {
      int i = 1;
      paramArrayOfString1 = new Object[i];
      paramArrayOfString1[0] = paramString1;
      paramUri = Arrays.copyOf(paramArrayOfString1, i);
      parama1 = String.format("\n            SELECT\n                r._id as im_reaction_id,\n                r.message_id as im_reaction_message_id,\n                r.emoji as im_reaction_emoji,\n                r.from_peer_id as im_reaction_from_peer_id,\n                r.send_date as im_reaction_date,\n                r.status as im_reaction_status,\n\n                m.conversation_id as im_conversation_id,\n                m.status as im_message_status,\n\n                i.normalized_number as im_participant_number,\n\n                ac.contact_name as im_participant_name,\n                ac.contact_image_url as im_participant_image_url,\n                IFNULL(ac.contact_phonebook_id, -1) as im_participant_phonebook_id\n\n            FROM msg_im_reactions r\n                LEFT JOIN msg_messages m ON m._id = r.message_id\n                LEFT JOIN msg_im_users i ON i.im_peer_id = r.from_peer_id\n                LEFT JOIN data d ON d.data_type = 4\n                   AND d.data1 = i.normalized_number\n                LEFT JOIN raw_contact rc on d.data_raw_contact_id = rc._id\n                    OR r.from_peer_id = rc.contact_im_id\n                LEFT JOIN aggregated_contact ac on rc.aggregated_contact_id = ac._id\n            WHERE %s\n            GROUP BY r._id\n        ", paramUri);
      paramUri = "java.lang.String.format(this, *args)";
      k.a(parama1, paramUri);
      if (paramString2 != null)
      {
        paramUri = new java/lang/StringBuilder;
        paramUri.<init>();
        paramUri.append(parama1);
        paramUri.append("ORDER BY ");
        parama1 = paramUri.toString();
        paramUri = new java/lang/StringBuilder;
        paramUri.<init>();
        paramUri.append(parama1);
        paramUri.append(paramString2);
        parama1 = paramUri.toString();
      }
      parama = parama.c().rawQuery(parama1, paramArrayOfString2);
      k.a(parama, "provider.database.rawQue…(sqlQuery, selectionArgs)");
      return parama;
    }
    parama = new java/lang/IllegalArgumentException;
    parama.<init>("Add selection for query: \n            SELECT\n                r._id as im_reaction_id,\n                r.message_id as im_reaction_message_id,\n                r.emoji as im_reaction_emoji,\n                r.from_peer_id as im_reaction_from_peer_id,\n                r.send_date as im_reaction_date,\n                r.status as im_reaction_status,\n\n                m.conversation_id as im_conversation_id,\n                m.status as im_message_status,\n\n                i.normalized_number as im_participant_number,\n\n                ac.contact_name as im_participant_name,\n                ac.contact_image_url as im_participant_image_url,\n                IFNULL(ac.contact_phonebook_id, -1) as im_participant_phonebook_id\n\n            FROM msg_im_reactions r\n                LEFT JOIN msg_messages m ON m._id = r.message_id\n                LEFT JOIN msg_im_users i ON i.im_peer_id = r.from_peer_id\n                LEFT JOIN data d ON d.data_type = 4\n                   AND d.data1 = i.normalized_number\n                LEFT JOIN raw_contact rc on d.data_raw_contact_id = rc._id\n                    OR r.from_peer_id = rc.contact_im_id\n                LEFT JOIN aggregated_contact ac on rc.aggregated_contact_id = ac._id\n            WHERE %s\n            GROUP BY r._id\n        ");
    throw ((Throwable)parama);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */