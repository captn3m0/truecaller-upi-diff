package com.truecaller.content;

import android.net.Uri;
import android.provider.BaseColumns;

public final class TruecallerContract$Filters
  implements BaseColumns, TruecallerContract.l
{
  public static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "filters");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.Filters
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */