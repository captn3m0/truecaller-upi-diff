package com.truecaller.content;

import android.content.ContentUris;
import android.net.Uri;

public final class TruecallerContract$aa
{
  public static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "msg/msg_messages");
  }
  
  public static Uri a(long paramLong)
  {
    return ContentUris.withAppendedId(Uri.withAppendedPath(TruecallerContract.b, "msg/msg_messages"), paramLong);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */