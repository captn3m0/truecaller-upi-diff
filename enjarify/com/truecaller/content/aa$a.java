package com.truecaller.content;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.common.h.u;
import com.truecaller.utils.q;
import com.truecaller.utils.s;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import kotlinx.coroutines.ag;

public final class aa$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  public aa$a(aa paramaa, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/content/aa$a;
    aa localaa = b;
    locala.<init>(localaa, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = b.d.a("participants-country-code");
        bool1 = false;
        localObject1 = null;
        Object localObject2 = new java/util/LinkedHashMap;
        ((LinkedHashMap)localObject2).<init>();
        localObject2 = (Map)localObject2;
        try
        {
          Object localObject3 = b;
          Object localObject4 = a;
          Object localObject5 = TruecallerContract.ae.a();
          localObject3 = "normalized_destination";
          Object localObject6 = { localObject3 };
          Object localObject7 = "type = ? AND (country_code IS NULL OR country_code = \"\")";
          localObject3 = "0";
          Object localObject8 = { localObject3 };
          localObject1 = ((ContentResolver)localObject4).query((Uri)localObject5, (String[])localObject6, (String)localObject7, (String[])localObject8, null);
          int j = 1;
          localObject4 = null;
          if (localObject1 != null) {
            for (;;)
            {
              boolean bool2 = ((Cursor)localObject1).moveToNext();
              if (!bool2) {
                break;
              }
              localObject5 = ((Cursor)localObject1).getString(0);
              localObject6 = "cursor.getString(0)";
              c.g.b.k.a(localObject5, (String)localObject6);
              localObject6 = localObject5;
              localObject6 = (CharSequence)localObject5;
              int m = ((CharSequence)localObject6).length();
              if (m > 0)
              {
                m = 1;
              }
              else
              {
                m = 0;
                localObject6 = null;
              }
              if (m != 0)
              {
                boolean bool4 = ((Map)localObject2).containsKey(localObject5);
                if (!bool4)
                {
                  localObject6 = b;
                  localObject6 = b;
                  localObject6 = ((u)localObject6).e((String)localObject5);
                  if (localObject6 != null)
                  {
                    localObject7 = localObject6;
                    localObject7 = (CharSequence)localObject6;
                    int n = ((CharSequence)localObject7).length();
                    if (n == 0)
                    {
                      n = 1;
                    }
                    else
                    {
                      n = 0;
                      localObject7 = null;
                    }
                    if (n == 0) {
                      ((Map)localObject2).put(localObject5, localObject6);
                    }
                  }
                }
              }
            }
          }
          if (localObject1 != null)
          {
            localObject1 = (Closeable)localObject1;
            com.truecaller.utils.extensions.d.a((Closeable)localObject1);
          }
          bool1 = ((Map)localObject2).isEmpty() ^ j;
          if (bool1)
          {
            localObject1 = new java/util/ArrayList;
            int k = ((Map)localObject2).size();
            ((ArrayList)localObject1).<init>(k);
            localObject1 = (Collection)localObject1;
            localObject2 = ((Map)localObject2).entrySet().iterator();
            for (;;)
            {
              boolean bool3 = ((Iterator)localObject2).hasNext();
              if (!bool3) {
                break;
              }
              localObject5 = (Map.Entry)((Iterator)localObject2).next();
              localObject6 = ContentProviderOperation.newUpdate(TruecallerContract.ae.a());
              localObject8 = ((Map.Entry)localObject5).getValue();
              localObject6 = ((ContentProviderOperation.Builder)localObject6).withValue("country_code", localObject8);
              localObject7 = "normalized_destination = ?";
              localObject8 = new String[j];
              localObject5 = (String)((Map.Entry)localObject5).getKey();
              localObject8[0] = localObject5;
              localObject5 = ((ContentProviderOperation.Builder)localObject6).withSelection((String)localObject7, (String[])localObject8).build();
              ((Collection)localObject1).add(localObject5);
            }
            localObject1 = (List)localObject1;
            try
            {
              localObject2 = b;
              localObject2 = a;
              localObject3 = TruecallerContract.a();
              localObject4 = new java/util/ArrayList;
              localObject1 = (Collection)localObject1;
              ((ArrayList)localObject4).<init>((Collection)localObject1);
              ((ContentResolver)localObject2).applyBatch((String)localObject3, (ArrayList)localObject4);
            }
            catch (OperationApplicationException localOperationApplicationException)
            {
              localObject1 = (Throwable)localOperationApplicationException;
              localObject2 = "Updating participants' country codes failed";
              com.truecaller.log.d.a((Throwable)localObject1, (String)localObject2);
            }
          }
          ((q)paramObject).a();
          return x.a;
        }
        finally
        {
          if (localObject1 != null)
          {
            localObject1 = (Closeable)localObject1;
            com.truecaller.utils.extensions.d.a((Closeable)localObject1);
          }
        }
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.aa.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */