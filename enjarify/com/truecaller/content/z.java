package com.truecaller.content;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.CancellationSignal;
import c.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.common.c.a.a.g;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class z
  implements a.g
{
  public final Cursor a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    k.b(parama, "provider");
    k.b(parama1, "helper");
    k.b(paramUri, "uri");
    parama1 = paramUri.getQueryParameters("ids");
    k.a(parama1, "ids");
    paramUri = parama1;
    boolean bool1 = ((Collection)parama1).isEmpty() ^ true;
    paramArrayOfString1 = null;
    paramString1 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool1, paramString1);
    int i = parama1.size();
    paramUri = am.a("?", ",", i);
    k.a(paramUri, "StringUtils.repeat(\"?\", \",\", ids.size)");
    parama1 = (Iterable)parama1;
    paramString1 = new java/util/ArrayList;
    i = m.a(parama1, 10);
    paramString1.<init>(i);
    paramString1 = (Collection)paramString1;
    parama1 = parama1.iterator();
    for (;;)
    {
      boolean bool2 = parama1.hasNext();
      if (!bool2) {
        break;
      }
      paramArrayOfString2 = ((String)parama1.next()).toString();
      paramString1.add(paramArrayOfString2);
    }
    paramString1 = (Collection)paramString1;
    parama1 = new String[0];
    parama1 = paramString1.toArray(parama1);
    if (parama1 != null)
    {
      parama1 = (String[])parama1;
      paramArrayOfString1 = new java/lang/StringBuilder;
      paramArrayOfString1.<init>("m._id IN(");
      paramArrayOfString1.append(paramUri);
      paramArrayOfString1.append(") AND m.classification=0");
      paramUri = paramArrayOfString1.toString();
      parama = parama.c();
      paramUri = String.valueOf(paramUri);
      paramUri = "\n            SELECT\n            m._id                                      AS message_id,\n            m.status                                   AS message_status,\n            m.raw_address                              AS message_sender_raw_address,\n            m.sim_token                                AS message_sim_token,\n            m.date                                     AS message_date,\n            e.content           AS message_content,\n            c.split_criteria      AS conversation_split_criteria\n            FROM msg_messages m\n                LEFT JOIN msg_entities e ON m._id = e.message_id\n                LEFT JOIN msg_conversations c ON c._id = m.conversation_id\n         WHERE ".concat(paramUri);
      parama = parama.rawQuery(paramUri, parama1);
      k.a(parama, "provider.database.rawQue…messagesSelection\", args)");
      return parama;
    }
    parama = new c/u;
    parama.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */