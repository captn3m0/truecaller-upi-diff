package com.truecaller.content;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import com.truecaller.common.c.a.a.f;
import com.truecaller.common.c.a.a.h;

final class r
  implements a.f, a.h
{
  private static final String[] a = { "_id" };
  private static final String b;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("wildcard_type=");
    int i = NONEtype;
    localStringBuilder.append(i);
    localStringBuilder.append(" AND value=? COLLATE NOCASE");
    b = localStringBuilder.toString();
  }
  
  private static Cursor a(SQLiteDatabase paramSQLiteDatabase, String paramString)
  {
    String[] arrayOfString1 = { "rule" };
    String str = b;
    String[] arrayOfString2 = new String[1];
    arrayOfString2[0] = paramString;
    return paramSQLiteDatabase.query("filters", arrayOfString1, str, arrayOfString2, null, null, null);
  }
  
  private static Long a(SQLiteDatabase paramSQLiteDatabase, String paramString, String[] paramArrayOfString)
  {
    paramSQLiteDatabase = paramSQLiteDatabase.rawQuery(paramString, paramArrayOfString);
    if (paramSQLiteDatabase != null) {
      try
      {
        boolean bool = paramSQLiteDatabase.moveToFirst();
        if (bool)
        {
          bool = false;
          paramString = null;
          long l = paramSQLiteDatabase.getLong(0);
          paramString = Long.valueOf(l);
          return paramString;
        }
      }
      finally
      {
        paramSQLiteDatabase.close();
      }
    }
    return null;
  }
  
  static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.execSQL("UPDATE msg_participants SET aggregated_contact_id = COALESCE((SELECT r.aggregated_contact_id FROM data d LEFT JOIN raw_contact r ON r._id = d.data_raw_contact_id WHERE d.data1 = msg_participants.normalized_destination AND d.data_type=4 LIMIT 1),(SELECT aggregated_contact_id FROM raw_contact WHERE contact_im_id = msg_participants.tc_im_peer_id LIMIT 1), -1)");
  }
  
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    paramContentValues.remove("_id");
    paramContentValues.remove("normalized_destination");
    paramContentValues.remove("raw_destination");
    paramContentValues.remove("type");
    paramContentValues.remove("tc_im_peer_id");
    parama1 = parama.c();
    paramUri = "msg_participants";
    int i = parama1.update(paramUri, paramContentValues, paramString, paramArrayOfString);
    if (i != 0)
    {
      paramUri = TruecallerContract.ae.a();
      parama.a(paramUri);
      paramUri = TruecallerContract.b;
      paramContentValues = "msg/msg_participants_with_contact_info";
      paramUri = Uri.withAppendedPath(paramUri, paramContentValues);
      parama.a(paramUri);
      paramUri = TruecallerContract.ab.a();
      parama.a(paramUri);
    }
    return i;
  }
  
  /* Error */
  public final Uri a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues)
  {
    // Byte code:
    //   0: aload_2
    //   1: astore 5
    //   3: aload 4
    //   5: astore 6
    //   7: aload 4
    //   9: ldc 103
    //   11: invokevirtual 145	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   14: astore 7
    //   16: aload 7
    //   18: invokevirtual 151	java/lang/Integer:intValue	()I
    //   21: istore 8
    //   23: aload 4
    //   25: ldc 100
    //   27: invokevirtual 155	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   30: astore 9
    //   32: iconst_1
    //   33: istore 10
    //   35: iconst_0
    //   36: istore 11
    //   38: iload 8
    //   40: ifne +34 -> 74
    //   43: aload 9
    //   45: iconst_0
    //   46: invokevirtual 159	java/lang/String:charAt	(I)C
    //   49: istore 12
    //   51: bipush 43
    //   53: istore 13
    //   55: iload 12
    //   57: iload 13
    //   59: if_icmpne +6 -> 65
    //   62: goto +12 -> 74
    //   65: iconst_0
    //   66: istore 12
    //   68: aconst_null
    //   69: astore 14
    //   71: goto +6 -> 77
    //   74: iconst_1
    //   75: istore 12
    //   77: iload 10
    //   79: anewarray 16	java/lang/String
    //   82: astore 15
    //   84: aload 9
    //   86: invokestatic 165	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   89: astore 16
    //   91: ldc -94
    //   93: aload 16
    //   95: invokevirtual 168	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   98: astore 17
    //   100: aload 15
    //   102: iconst_0
    //   103: aload 17
    //   105: aastore
    //   106: iload 12
    //   108: aload 15
    //   110: invokestatic 174	com/truecaller/log/AssertionUtil:isTrue	(Z[Ljava/lang/String;)V
    //   113: aload_1
    //   114: invokevirtual 111	com/truecaller/common/c/a:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   117: astore 14
    //   119: iconst_3
    //   120: istore 13
    //   122: aconst_null
    //   123: astore 17
    //   125: iload 8
    //   127: iload 13
    //   129: if_icmpne +172 -> 301
    //   132: ldc -79
    //   134: astore 18
    //   136: ldc -77
    //   138: astore 16
    //   140: iconst_1
    //   141: anewarray 16	java/lang/String
    //   144: dup
    //   145: iconst_0
    //   146: aload 16
    //   148: aastore
    //   149: astore 19
    //   151: ldc -75
    //   153: astore 20
    //   155: iload 10
    //   157: anewarray 16	java/lang/String
    //   160: astore 21
    //   162: aload 21
    //   164: iconst_0
    //   165: aload 9
    //   167: aastore
    //   168: aload 14
    //   170: astore 16
    //   172: aload 14
    //   174: aload 18
    //   176: aload 19
    //   178: aload 20
    //   180: aload 21
    //   182: aconst_null
    //   183: aconst_null
    //   184: aconst_null
    //   185: invokevirtual 65	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   188: astore 16
    //   190: aload 16
    //   192: invokeinterface 75 1 0
    //   197: istore 22
    //   199: iload 22
    //   201: ifeq +55 -> 256
    //   204: aload 16
    //   206: iconst_0
    //   207: invokeinterface 185 2 0
    //   212: astore 7
    //   214: ldc 103
    //   216: astore 9
    //   218: iconst_0
    //   219: invokestatic 188	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   222: astore 18
    //   224: aload 6
    //   226: aload 9
    //   228: aload 18
    //   230: invokevirtual 192	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   233: ldc 100
    //   235: astore 9
    //   237: aload 6
    //   239: aload 9
    //   241: aload 7
    //   243: invokevirtual 195	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   246: aload 7
    //   248: astore 9
    //   250: iconst_0
    //   251: istore 8
    //   253: aconst_null
    //   254: astore 7
    //   256: aload 16
    //   258: ifnull +10 -> 268
    //   261: aload 16
    //   263: invokeinterface 88 1 0
    //   268: aload 16
    //   270: astore 23
    //   272: goto +32 -> 304
    //   275: astore 5
    //   277: aload 16
    //   279: astore 17
    //   281: goto +5 -> 286
    //   284: astore 5
    //   286: aload 17
    //   288: ifnull +10 -> 298
    //   291: aload 17
    //   293: invokeinterface 88 1 0
    //   298: aload 5
    //   300: athrow
    //   301: aconst_null
    //   302: astore 23
    //   304: ldc 113
    //   306: astore 18
    //   308: getstatic 18	com/truecaller/content/r:a	[Ljava/lang/String;
    //   311: astore 19
    //   313: ldc -59
    //   315: astore 20
    //   317: iload 10
    //   319: anewarray 16	java/lang/String
    //   322: astore 21
    //   324: aload 21
    //   326: iconst_0
    //   327: aload 9
    //   329: aastore
    //   330: aload 14
    //   332: astore 16
    //   334: aload 14
    //   336: aload 18
    //   338: aload 19
    //   340: aload 20
    //   342: aload 21
    //   344: aconst_null
    //   345: aconst_null
    //   346: aconst_null
    //   347: invokevirtual 65	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   350: astore 16
    //   352: aload 16
    //   354: invokeinterface 75 1 0
    //   359: istore 22
    //   361: iload 22
    //   363: ifeq +37 -> 400
    //   366: aload 16
    //   368: iconst_0
    //   369: invokeinterface 79 2 0
    //   374: lstore 24
    //   376: aload 5
    //   378: lload 24
    //   380: invokevirtual 202	com/truecaller/common/c/a/a:a	(J)Landroid/net/Uri;
    //   383: astore 5
    //   385: aload 16
    //   387: ifnull +10 -> 397
    //   390: aload 16
    //   392: invokeinterface 88 1 0
    //   397: aload 5
    //   399: areturn
    //   400: aload 16
    //   402: ifnull +10 -> 412
    //   405: aload 16
    //   407: invokeinterface 88 1 0
    //   412: iload 8
    //   414: iload 13
    //   416: if_icmpeq +64 -> 480
    //   419: iload 8
    //   421: tableswitch	default:+23->444, 0:+32->453, 1:+32->453
    //   444: iconst_0
    //   445: istore 13
    //   447: aconst_null
    //   448: astore 15
    //   450: goto +67 -> 517
    //   453: iload 10
    //   455: anewarray 16	java/lang/String
    //   458: astore 16
    //   460: aload 16
    //   462: iconst_0
    //   463: aload 9
    //   465: aastore
    //   466: aload 14
    //   468: ldc -52
    //   470: aload 16
    //   472: invokestatic 207	com/truecaller/content/r:a	(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Long;
    //   475: astore 15
    //   477: goto +40 -> 517
    //   480: aload 6
    //   482: ldc 105
    //   484: invokevirtual 155	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   487: astore 15
    //   489: ldc -47
    //   491: astore 16
    //   493: iload 10
    //   495: anewarray 16	java/lang/String
    //   498: astore 18
    //   500: aload 18
    //   502: iconst_0
    //   503: aload 15
    //   505: aastore
    //   506: aload 14
    //   508: aload 16
    //   510: aload 18
    //   512: invokestatic 207	com/truecaller/content/r:a	(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Long;
    //   515: astore 15
    //   517: aload 15
    //   519: ifnull +16 -> 535
    //   522: ldc -45
    //   524: astore 16
    //   526: aload 6
    //   528: aload 16
    //   530: aload 15
    //   532: invokevirtual 214	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   535: iload 8
    //   537: ifeq +10 -> 547
    //   540: iload 8
    //   542: iload 10
    //   544: if_icmpne +335 -> 879
    //   547: aload 14
    //   549: aload 9
    //   551: invokestatic 217	com/truecaller/content/r:a	(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    //   554: astore 15
    //   556: aload 15
    //   558: invokeinterface 75 1 0
    //   563: istore 26
    //   565: iload 26
    //   567: ifeq +23 -> 590
    //   570: aload 15
    //   572: iconst_0
    //   573: invokeinterface 221 2 0
    //   578: istore 26
    //   580: iload 26
    //   582: invokestatic 223	com/truecaller/content/TruecallerContract$ae:a	(I)I
    //   585: istore 26
    //   587: goto +9 -> 596
    //   590: iconst_0
    //   591: istore 26
    //   593: aconst_null
    //   594: astore 16
    //   596: aload 15
    //   598: invokestatic 228	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   601: iload 26
    //   603: ifne +104 -> 707
    //   606: invokestatic 234	com/truecaller/common/b/a:F	()Lcom/truecaller/common/b/a;
    //   609: invokevirtual 238	com/truecaller/common/b/a:u	()Lcom/truecaller/common/a;
    //   612: invokeinterface 244 1 0
    //   617: aload 9
    //   619: invokeinterface 249 2 0
    //   624: astore 15
    //   626: aload 15
    //   628: invokestatic 254	com/truecaller/common/h/am:c	(Ljava/lang/CharSequence;)Z
    //   631: istore 22
    //   633: iload 22
    //   635: ifeq +72 -> 707
    //   638: aload 14
    //   640: aload 15
    //   642: invokestatic 217	com/truecaller/content/r:a	(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Landroid/database/Cursor;
    //   645: astore 15
    //   647: aload 15
    //   649: invokeinterface 75 1 0
    //   654: istore 22
    //   656: iload 22
    //   658: ifeq +20 -> 678
    //   661: aload 15
    //   663: iconst_0
    //   664: invokeinterface 221 2 0
    //   669: istore 26
    //   671: iload 26
    //   673: invokestatic 223	com/truecaller/content/TruecallerContract$ae:a	(I)I
    //   676: istore 26
    //   678: aload 15
    //   680: invokestatic 228	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   683: goto +24 -> 707
    //   686: astore 5
    //   688: goto +11 -> 699
    //   691: astore 5
    //   693: iconst_0
    //   694: istore 13
    //   696: aconst_null
    //   697: astore 15
    //   699: aload 15
    //   701: invokestatic 228	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   704: aload 5
    //   706: athrow
    //   707: iload 26
    //   709: invokestatic 188	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   712: astore 16
    //   714: aload 6
    //   716: ldc_w 256
    //   719: aload 16
    //   721: invokevirtual 192	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   724: ldc_w 258
    //   727: astore 18
    //   729: ldc_w 260
    //   732: astore 15
    //   734: iconst_1
    //   735: anewarray 16	java/lang/String
    //   738: dup
    //   739: iconst_0
    //   740: aload 15
    //   742: aastore
    //   743: astore 19
    //   745: ldc_w 262
    //   748: astore 20
    //   750: iload 10
    //   752: anewarray 16	java/lang/String
    //   755: astore 21
    //   757: aload 21
    //   759: iconst_0
    //   760: aload 9
    //   762: aastore
    //   763: ldc_w 264
    //   766: astore 23
    //   768: aload 14
    //   770: astore 16
    //   772: aload 14
    //   774: aload 18
    //   776: aload 19
    //   778: aload 20
    //   780: aload 21
    //   782: aconst_null
    //   783: aconst_null
    //   784: aconst_null
    //   785: aload 23
    //   787: invokevirtual 267	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   790: astore 15
    //   792: aload 15
    //   794: invokeinterface 75 1 0
    //   799: istore 26
    //   801: ldc_w 269
    //   804: astore 18
    //   806: iload 26
    //   808: invokestatic 274	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   811: astore 19
    //   813: aload 6
    //   815: aload 18
    //   817: aload 19
    //   819: invokevirtual 277	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Boolean;)V
    //   822: ldc_w 279
    //   825: astore 18
    //   827: iload 26
    //   829: ifeq +16 -> 845
    //   832: aload 15
    //   834: iconst_0
    //   835: invokeinterface 221 2 0
    //   840: istore 26
    //   842: goto +9 -> 851
    //   845: iconst_0
    //   846: istore 26
    //   848: aconst_null
    //   849: astore 16
    //   851: iload 26
    //   853: invokestatic 188	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   856: astore 16
    //   858: aload 6
    //   860: aload 18
    //   862: aload 16
    //   864: invokevirtual 192	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   867: aload 15
    //   869: ifnull +10 -> 879
    //   872: aload 15
    //   874: invokeinterface 88 1 0
    //   879: iload 8
    //   881: ifne +155 -> 1036
    //   884: aload 6
    //   886: ldc 105
    //   888: invokevirtual 155	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   891: astore 7
    //   893: aload 7
    //   895: invokestatic 281	com/truecaller/common/h/am:b	(Ljava/lang/CharSequence;)Z
    //   898: istore 8
    //   900: iload 8
    //   902: ifeq +134 -> 1036
    //   905: ldc -79
    //   907: astore 18
    //   909: ldc_w 283
    //   912: astore 7
    //   914: iconst_1
    //   915: anewarray 16	java/lang/String
    //   918: dup
    //   919: iconst_0
    //   920: aload 7
    //   922: aastore
    //   923: astore 19
    //   925: ldc_w 285
    //   928: astore 20
    //   930: iload 10
    //   932: anewarray 16	java/lang/String
    //   935: astore 21
    //   937: aload 21
    //   939: iconst_0
    //   940: aload 9
    //   942: aastore
    //   943: ldc_w 264
    //   946: astore 23
    //   948: aload 14
    //   950: astore 16
    //   952: aload 14
    //   954: aload 18
    //   956: aload 19
    //   958: aload 20
    //   960: aload 21
    //   962: aconst_null
    //   963: aconst_null
    //   964: aconst_null
    //   965: aload 23
    //   967: invokevirtual 267	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   970: astore 7
    //   972: aload 7
    //   974: invokeinterface 75 1 0
    //   979: istore 27
    //   981: iload 27
    //   983: ifeq +26 -> 1009
    //   986: ldc 105
    //   988: astore 9
    //   990: aload 7
    //   992: iconst_0
    //   993: invokeinterface 185 2 0
    //   998: astore 28
    //   1000: aload 6
    //   1002: aload 9
    //   1004: aload 28
    //   1006: invokevirtual 195	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   1009: aload 7
    //   1011: invokestatic 228	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   1014: goto +22 -> 1036
    //   1017: astore 5
    //   1019: aload 7
    //   1021: astore 17
    //   1023: goto +5 -> 1028
    //   1026: astore 5
    //   1028: aload 17
    //   1030: invokestatic 228	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   1033: aload 5
    //   1035: athrow
    //   1036: ldc 113
    //   1038: astore 7
    //   1040: aload 14
    //   1042: aload 7
    //   1044: aconst_null
    //   1045: aload 6
    //   1047: invokevirtual 289	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    //   1050: lstore 24
    //   1052: iconst_m1
    //   1053: i2l
    //   1054: lstore 29
    //   1056: lload 24
    //   1058: lload 29
    //   1060: lcmp
    //   1061: istore 11
    //   1063: iload 11
    //   1065: ifeq +11 -> 1076
    //   1068: aload 5
    //   1070: lload 24
    //   1072: invokevirtual 202	com/truecaller/common/c/a/a:a	(J)Landroid/net/Uri;
    //   1075: areturn
    //   1076: new 291	android/database/sqlite/SQLiteException
    //   1079: astore 5
    //   1081: aload 5
    //   1083: ldc_w 293
    //   1086: invokespecial 294	android/database/sqlite/SQLiteException:<init>	(Ljava/lang/String;)V
    //   1089: aload 5
    //   1091: athrow
    //   1092: astore 5
    //   1094: goto +11 -> 1105
    //   1097: astore 5
    //   1099: iconst_0
    //   1100: istore 13
    //   1102: aconst_null
    //   1103: astore 15
    //   1105: aload 15
    //   1107: ifnull +10 -> 1117
    //   1110: aload 15
    //   1112: invokeinterface 88 1 0
    //   1117: aload 5
    //   1119: athrow
    //   1120: astore 5
    //   1122: goto +11 -> 1133
    //   1125: astore 5
    //   1127: iconst_0
    //   1128: istore 13
    //   1130: aconst_null
    //   1131: astore 15
    //   1133: aload 15
    //   1135: invokestatic 228	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   1138: aload 5
    //   1140: athrow
    //   1141: astore 5
    //   1143: aload 16
    //   1145: astore 23
    //   1147: goto +5 -> 1152
    //   1150: astore 5
    //   1152: aload 23
    //   1154: ifnull +10 -> 1164
    //   1157: aload 23
    //   1159: invokeinterface 88 1 0
    //   1164: aload 5
    //   1166: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1167	0	this	r
    //   0	1167	1	parama	com.truecaller.common.c.a
    //   0	1167	2	parama1	com.truecaller.common.c.a.a
    //   0	1167	3	paramUri	Uri
    //   0	1167	4	paramContentValues	ContentValues
    //   1	1	5	locala	com.truecaller.common.c.a.a
    //   275	1	5	localObject1	Object
    //   284	93	5	localObject2	Object
    //   383	15	5	localUri	Uri
    //   686	1	5	localObject3	Object
    //   691	14	5	localObject4	Object
    //   1017	1	5	localObject5	Object
    //   1026	43	5	localObject6	Object
    //   1079	11	5	localSQLiteException	android.database.sqlite.SQLiteException
    //   1092	1	5	localObject7	Object
    //   1097	21	5	localObject8	Object
    //   1120	1	5	localObject9	Object
    //   1125	14	5	localObject10	Object
    //   1141	1	5	localObject11	Object
    //   1150	15	5	localObject12	Object
    //   5	1041	6	localContentValues	ContentValues
    //   14	1029	7	localObject13	Object
    //   21	859	8	i	int
    //   898	3	8	bool1	boolean
    //   30	973	9	localObject14	Object
    //   33	898	10	j	int
    //   36	1028	11	bool2	boolean
    //   49	58	12	k	int
    //   53	7	13	m	int
    //   120	1009	13	n	int
    //   69	972	14	localSQLiteDatabase	SQLiteDatabase
    //   82	1052	15	localObject15	Object
    //   89	1055	16	localObject16	Object
    //   98	931	17	localObject17	Object
    //   134	821	18	localObject18	Object
    //   149	808	19	localObject19	Object
    //   153	806	20	str1	String
    //   160	801	21	arrayOfString	String[]
    //   197	460	22	bool3	boolean
    //   270	888	23	localObject20	Object
    //   374	697	24	l1	long
    //   563	3	26	bool4	boolean
    //   578	130	26	i1	int
    //   799	29	26	bool5	boolean
    //   840	12	26	i2	int
    //   979	3	27	bool6	boolean
    //   998	7	28	str2	String
    //   1054	5	29	l2	long
    // Exception table:
    //   from	to	target	type
    //   190	197	275	finally
    //   206	212	275	finally
    //   218	222	275	finally
    //   228	233	275	finally
    //   241	246	275	finally
    //   140	149	284	finally
    //   155	160	284	finally
    //   165	168	284	finally
    //   184	188	284	finally
    //   647	654	686	finally
    //   663	669	686	finally
    //   671	676	686	finally
    //   640	645	691	finally
    //   972	979	1017	finally
    //   992	998	1017	finally
    //   1004	1009	1017	finally
    //   914	923	1026	finally
    //   930	935	1026	finally
    //   940	943	1026	finally
    //   965	970	1026	finally
    //   792	799	1092	finally
    //   806	811	1092	finally
    //   817	822	1092	finally
    //   834	840	1092	finally
    //   851	856	1092	finally
    //   862	867	1092	finally
    //   734	743	1097	finally
    //   750	755	1097	finally
    //   760	763	1097	finally
    //   785	790	1097	finally
    //   556	563	1120	finally
    //   572	578	1120	finally
    //   580	585	1120	finally
    //   549	554	1125	finally
    //   352	359	1141	finally
    //   368	374	1141	finally
    //   378	383	1141	finally
    //   308	311	1150	finally
    //   317	322	1150	finally
    //   327	330	1150	finally
    //   346	350	1150	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */