package com.truecaller.content;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import com.truecaller.common.c.a.a.h;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.Iterator;
import java.util.Set;

public final class v
  implements a.h
{
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    parama1 = paramContentValues.keySet().iterator();
    int i;
    for (;;)
    {
      bool1 = parama1.hasNext();
      i = 1;
      if (!bool1) {
        break;
      }
      paramString = (String)parama1.next();
      localObject1 = TruecallerContract.an.a;
      int j = localObject1.length;
      int k = 0;
      String str = null;
      while (k < j)
      {
        Object localObject2 = localObject1[k];
        boolean bool2 = ((String)localObject2).equals(paramString);
        if (bool2)
        {
          bool3 = true;
          break label103;
        }
        k += 1;
      }
      boolean bool3 = false;
      localObject1 = null;
      label103:
      String[] arrayOfString = new String[i];
      str = "Conversation field '%s' is not approved for direct modification";
      paramArrayOfString = new Object[i];
      paramArrayOfString[0] = paramString;
      paramString = String.format(str, paramArrayOfString);
      arrayOfString[0] = paramString;
      AssertionUtil.AlwaysFatal.isTrue(bool3, arrayOfString);
    }
    parama1 = parama.c();
    paramUri = paramUri.getQueryParameter("conversation_id");
    boolean bool1 = TextUtils.isEmpty(paramUri);
    Object localObject1 = new String[0];
    AssertionUtil.AlwaysFatal.isFalse(bool1, (String[])localObject1);
    paramString = "msg_thread_stats";
    localObject1 = "conversation_id=?";
    paramArrayOfString = new String[i];
    paramArrayOfString[0] = paramUri;
    int m = parama1.update(paramString, paramContentValues, (String)localObject1, paramArrayOfString);
    if (m > 0)
    {
      paramUri = TruecallerContract.g.a();
      parama.a(paramUri);
    }
    return m;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */