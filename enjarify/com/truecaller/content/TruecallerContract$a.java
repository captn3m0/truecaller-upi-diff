package com.truecaller.content;

import android.net.Uri;
import android.net.Uri.Builder;

public final class TruecallerContract$a
  extends TruecallerContract.b
{
  public static Uri a()
  {
    return Uri.withAppendedPath(TruecallerContract.b, "aggregated_contact");
  }
  
  public static Uri a(String paramString)
  {
    return TruecallerContract.b.buildUpon().appendEncodedPath("aggregated_contact_t9").appendQueryParameter("filter", paramString).build();
  }
  
  public static Uri b()
  {
    return Uri.withAppendedPath(a(), "data");
  }
  
  public static Uri b(String paramString)
  {
    return TruecallerContract.b.buildUpon().appendEncodedPath("aggregated_contact_plain_text").appendQueryParameter("filter", paramString).build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */