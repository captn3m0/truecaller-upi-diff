package com.truecaller.content;

import android.text.TextUtils;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public enum TruecallerContract$Filters$WildCardType
{
  public final int patternFlags;
  public final String prefix;
  public final String prefixQuoted;
  public final String suffix;
  public final String suffixQuoted;
  public final int type;
  
  static
  {
    WildCardType localWildCardType = new com/truecaller/content/TruecallerContract$Filters$WildCardType;
    Object localObject1 = localWildCardType;
    localWildCardType.<init>("NONE", 0, "", "", 0, 0);
    NONE = localWildCardType;
    localObject1 = new com/truecaller/content/TruecallerContract$Filters$WildCardType;
    ((WildCardType)localObject1).<init>("START", 1, "^", ".*", 1, 0);
    START = (WildCardType)localObject1;
    localObject1 = new com/truecaller/content/TruecallerContract$Filters$WildCardType;
    Object localObject2 = localObject1;
    ((WildCardType)localObject1).<init>("CONTAIN", 2, ".*", ".*", 2, 2);
    CONTAIN = (WildCardType)localObject1;
    localObject1 = new com/truecaller/content/TruecallerContract$Filters$WildCardType;
    ((WildCardType)localObject1).<init>("END", 3, ".*", "$", 3, 0);
    END = (WildCardType)localObject1;
    localObject1 = new WildCardType[4];
    localObject2 = NONE;
    localObject1[0] = localObject2;
    localObject2 = START;
    localObject1[1] = localObject2;
    localObject2 = CONTAIN;
    localObject1[2] = localObject2;
    localObject2 = END;
    localObject1[3] = localObject2;
    $VALUES = (WildCardType[])localObject1;
  }
  
  private TruecallerContract$Filters$WildCardType(String paramString2, String paramString3, int paramInt2, int paramInt3)
  {
    prefix = paramString2;
    paramString1 = new java/lang/StringBuilder;
    paramString1.<init>();
    paramString1.append(paramString2);
    paramString1.append("\\Q");
    paramString1 = paramString1.toString();
    prefixQuoted = paramString1;
    suffix = paramString3;
    String str = String.valueOf(paramString3);
    paramString1 = "\\E".concat(str);
    suffixQuoted = paramString1;
    type = paramInt2;
    patternFlags = paramInt3;
  }
  
  public static WildCardType valueOfPattern(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1) {
      try
      {
        WildCardType[] arrayOfWildCardType = values();
        int i = 0;
        for (;;)
        {
          int j = arrayOfWildCardType.length;
          if (i >= j) {
            break;
          }
          WildCardType localWildCardType = arrayOfWildCardType[i];
          Object localObject = NONE;
          if (localWildCardType != localObject)
          {
            localObject = prefix;
            boolean bool2 = paramString.startsWith((String)localObject);
            if (bool2)
            {
              localObject = suffix;
              bool2 = paramString.endsWith((String)localObject);
              if (bool2)
              {
                localWildCardType.compilePattern(paramString);
                return localWildCardType;
              }
            }
          }
          i += 1;
        }
        return NONE;
      }
      catch (PatternSyntaxException localPatternSyntaxException) {}
    }
  }
  
  public static WildCardType valueOfType(int paramInt)
  {
    WildCardType[] arrayOfWildCardType = values();
    int i = 0;
    for (;;)
    {
      int j = arrayOfWildCardType.length;
      if (i >= j) {
        break;
      }
      WildCardType localWildCardType = arrayOfWildCardType[i];
      j = type;
      if (j == paramInt) {
        return arrayOfWildCardType[i];
      }
      i += 1;
    }
    return NONE;
  }
  
  public final Pattern compilePattern(String paramString)
  {
    try
    {
      int i = patternFlags;
      return Pattern.compile(paramString, i);
    }
    catch (PatternSyntaxException localPatternSyntaxException)
    {
      String str = stripPattern(paramString);
      boolean bool = TextUtils.equals(paramString, str);
      if (!bool) {
        return Pattern.compile(formatPattern(str));
      }
      throw localPatternSyntaxException;
    }
  }
  
  public final String formatPattern(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = prefix;
    localStringBuilder.append(str);
    paramString = Pattern.quote(paramString);
    localStringBuilder.append(paramString);
    paramString = suffix;
    localStringBuilder.append(paramString);
    return localStringBuilder.toString();
  }
  
  public final String getPrefix()
  {
    return prefix;
  }
  
  public final String getSuffix()
  {
    return suffix;
  }
  
  public final int getType()
  {
    return type;
  }
  
  public final String stripPattern(String paramString)
  {
    Object localObject = NONE;
    if (this != localObject)
    {
      boolean bool1 = TextUtils.isEmpty(paramString);
      if (!bool1)
      {
        localObject = prefixQuoted;
        bool1 = paramString.startsWith((String)localObject);
        if (bool1)
        {
          localObject = prefixQuoted;
          int i = ((String)localObject).length();
          paramString = paramString.substring(i);
        }
        else
        {
          localObject = prefix;
          boolean bool2 = paramString.startsWith((String)localObject);
          if (bool2)
          {
            localObject = prefix;
            int j = ((String)localObject).length();
            paramString = paramString.substring(j);
          }
        }
        localObject = suffixQuoted;
        boolean bool3 = paramString.endsWith((String)localObject);
        String str;
        int n;
        if (bool3)
        {
          int k = paramString.length();
          str = suffixQuoted;
          n = str.length();
          k -= n;
          paramString = paramString.substring(0, k);
        }
        else
        {
          localObject = suffix;
          boolean bool4 = paramString.endsWith((String)localObject);
          if (bool4)
          {
            int m = paramString.length();
            str = suffix;
            n = str.length();
            m -= n;
            paramString = paramString.substring(0, m);
          }
        }
      }
    }
    if (paramString == null) {
      paramString = "";
    }
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.Filters.WildCardType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */