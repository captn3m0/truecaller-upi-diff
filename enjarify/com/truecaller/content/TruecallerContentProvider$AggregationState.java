package com.truecaller.content;

public enum TruecallerContentProvider$AggregationState
{
  static
  {
    Object localObject = new com/truecaller/content/TruecallerContentProvider$AggregationState;
    ((AggregationState)localObject).<init>("NONE", 0);
    NONE = (AggregationState)localObject;
    localObject = new com/truecaller/content/TruecallerContentProvider$AggregationState;
    int i = 1;
    ((AggregationState)localObject).<init>("DELAYED", i);
    DELAYED = (AggregationState)localObject;
    localObject = new com/truecaller/content/TruecallerContentProvider$AggregationState;
    int j = 2;
    ((AggregationState)localObject).<init>("IMMEDIATE", j);
    IMMEDIATE = (AggregationState)localObject;
    localObject = new AggregationState[3];
    AggregationState localAggregationState = NONE;
    localObject[0] = localAggregationState;
    localAggregationState = DELAYED;
    localObject[i] = localAggregationState;
    localAggregationState = IMMEDIATE;
    localObject[j] = localAggregationState;
    $VALUES = (AggregationState[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContentProvider.AggregationState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */