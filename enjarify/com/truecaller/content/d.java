package com.truecaller.content;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import com.truecaller.common.c.a.a.e;
import com.truecaller.common.c.a.a.f;
import com.truecaller.common.c.a.a.h;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.List;
import org.c.a.a.a.k;

final class d
  implements a.e, a.f, a.h
{
  private final e a;
  
  public d(e parame)
  {
    a = parame;
  }
  
  private static long a(SQLiteDatabase paramSQLiteDatabase, String[] paramArrayOfString)
  {
    Cursor localCursor = null;
    try
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      String str1 = "SELECT conversation_id FROM msg_conversation_participants WHERE conversation_id IN (SELECT conversation_id FROM msg_conversation_participants WHERE participant_id IN (";
      localStringBuilder.<init>(str1);
      str1 = null;
      String str2 = paramArrayOfString[0];
      localStringBuilder.append(str2);
      int i = 1;
      for (;;)
      {
        int j = paramArrayOfString.length;
        if (i >= j) {
          break;
        }
        String str3 = ",";
        localStringBuilder.append(str3);
        str3 = paramArrayOfString[i];
        localStringBuilder.append(str3);
        i += 1;
      }
      int k = paramArrayOfString.length;
      paramArrayOfString = String.valueOf(k);
      str2 = ") GROUP BY conversation_id HAVING count(*)=";
      localStringBuilder.append(str2);
      localStringBuilder.append(paramArrayOfString);
      str2 = ") GROUP BY conversation_id HAVING count(*)=";
      localStringBuilder.append(str2);
      localStringBuilder.append(paramArrayOfString);
      paramArrayOfString = localStringBuilder.toString();
      localCursor = paramSQLiteDatabase.rawQuery(paramArrayOfString, null);
      if (localCursor != null)
      {
        boolean bool = localCursor.moveToFirst();
        if (bool)
        {
          long l = localCursor.getLong(0);
          return l;
        }
      }
      return -1;
    }
    finally
    {
      if (localCursor != null) {
        localCursor.close();
      }
    }
  }
  
  private static String[] a(SQLiteDatabase paramSQLiteDatabase, List paramList)
  {
    Object localObject1 = new java/lang/StringBuilder;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>(" IN (");
    Object localObject3 = DatabaseUtils.sqlEscapeString((String)paramList.get(0));
    ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((StringBuilder)localObject1).<init>((String)localObject2);
    int i = 1;
    for (;;)
    {
      int j = paramList.size();
      if (i >= j) {
        break;
      }
      ((StringBuilder)localObject1).append(",");
      localObject3 = DatabaseUtils.sqlEscapeString((String)paramList.get(i));
      ((StringBuilder)localObject1).append((String)localObject3);
      i += 1;
    }
    ((StringBuilder)localObject1).append(")");
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("SELECT _id FROM msg_participants WHERE normalized_destination");
    ((StringBuilder)localObject2).append((String)localObject1);
    ((StringBuilder)localObject2).append(" OR tc_im_peer_id");
    ((StringBuilder)localObject2).append((String)localObject1);
    localObject1 = ((StringBuilder)localObject2).toString();
    i = paramList.size();
    localObject2 = new String[i];
    int k = 0;
    localObject3 = null;
    try
    {
      localObject3 = paramSQLiteDatabase.rawQuery((String)localObject1, null);
      if (localObject3 != null)
      {
        int m = ((Cursor)localObject3).getCount();
        int n = localObject2.length;
        if (m == n)
        {
          m = 0;
          paramSQLiteDatabase = null;
          for (;;)
          {
            boolean bool = ((Cursor)localObject3).moveToNext();
            if (!bool) {
              break;
            }
            int i1 = m + 1;
            localObject1 = ((Cursor)localObject3).getString(0);
            localObject2[m] = localObject1;
            m = i1;
          }
          return (String[])localObject2;
        }
        paramSQLiteDatabase = new android/database/sqlite/SQLiteException;
        localObject1 = new java/lang/StringBuilder;
        localObject2 = "Not all participants was inserted: [";
        ((StringBuilder)localObject1).<init>((String)localObject2);
        i = 59;
        paramList = k.a(paramList, i);
        ((StringBuilder)localObject1).append(paramList);
        paramList = "]";
        ((StringBuilder)localObject1).append(paramList);
        paramList = ((StringBuilder)localObject1).toString();
        paramSQLiteDatabase.<init>(paramList);
        throw paramSQLiteDatabase;
      }
      paramSQLiteDatabase = new android/database/sqlite/SQLiteException;
      paramList = "Can not fetch participants";
      paramSQLiteDatabase.<init>(paramList);
      throw paramSQLiteDatabase;
    }
    finally
    {
      if (localObject3 != null) {
        ((Cursor)localObject3).close();
      }
    }
  }
  
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    int i = paramContentValues.size();
    int j = 1;
    if (i == j)
    {
      parama1 = "preferred_transport";
      boolean bool = paramContentValues.containsKey(parama1);
      if (bool) {
        return parama.c().update("msg_conversations", paramContentValues, paramString, paramArrayOfString);
      }
    }
    AssertionUtil.AlwaysFatal.fail(new String[] { "Should never be updated directly" });
    return 0;
  }
  
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    parama1 = new String[0];
    AssertionUtil.AlwaysFatal.isNull(paramString, parama1);
    return parama.c().delete("msg_conversations", null, null);
  }
  
  public final Uri a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues)
  {
    parama = parama.c();
    paramUri = paramUri.getQueryParameters("addr");
    Object localObject1 = { "You have to provide at least one participant" };
    AssertionUtil.AlwaysFatal.isNotNull(paramUri, (String[])localObject1);
    boolean bool1 = paramUri.isEmpty();
    Object localObject2 = { "You have to provide at least one participant" };
    AssertionUtil.AlwaysFatal.isFalse(bool1, (String[])localObject2);
    paramUri = a(parama, paramUri);
    long l1 = a(parama, paramUri);
    long l2 = -1;
    boolean bool2 = l1 < l2;
    if (bool2) {
      return parama1.a(l1);
    }
    paramContentValues = paramContentValues.getAsString("tc_group_id");
    localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    localObject2 = "type";
    int j = paramUri.length;
    int k = 0;
    int m = 1;
    if ((j == m) && (paramContentValues == null)) {
      m = 0;
    }
    Object localObject3 = Integer.valueOf(m);
    ((ContentValues)localObject1).put((String)localObject2, (Integer)localObject3);
    ((ContentValues)localObject1).put("tc_group_id", paramContentValues);
    paramContentValues = "msg_conversations";
    localObject2 = null;
    long l3 = parama.insert(paramContentValues, null, (ContentValues)localObject1);
    boolean bool3 = l3 < l2;
    if (bool3)
    {
      paramContentValues = new android/content/ContentValues;
      paramContentValues.<init>();
      int i = paramUri.length;
      while (k < i)
      {
        String str1 = paramUri[k];
        localObject3 = Long.valueOf(l3);
        paramContentValues.put("conversation_id", (Long)localObject3);
        String str2 = "participant_id";
        paramContentValues.put(str2, str1);
        str1 = "msg_conversation_participants";
        parama.insert(str1, null, paramContentValues);
        k += 1;
      }
      return parama1.a(l3);
    }
    parama = new android/database/sqlite/SQLiteException;
    parama.<init>("Can not create new conversation");
    throw parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */