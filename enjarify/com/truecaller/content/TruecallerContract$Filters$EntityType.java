package com.truecaller.content;

public enum TruecallerContract$Filters$EntityType
{
  public final int value;
  
  static
  {
    Object localObject = new com/truecaller/content/TruecallerContract$Filters$EntityType;
    ((EntityType)localObject).<init>("UNKNOWN", 0, 0);
    UNKNOWN = (EntityType)localObject;
    localObject = new com/truecaller/content/TruecallerContract$Filters$EntityType;
    int i = 1;
    ((EntityType)localObject).<init>("PERSON", i, i);
    PERSON = (EntityType)localObject;
    localObject = new com/truecaller/content/TruecallerContract$Filters$EntityType;
    int j = 2;
    ((EntityType)localObject).<init>("BUSINESS", j, j);
    BUSINESS = (EntityType)localObject;
    localObject = new EntityType[3];
    EntityType localEntityType = UNKNOWN;
    localObject[0] = localEntityType;
    localEntityType = PERSON;
    localObject[i] = localEntityType;
    localEntityType = BUSINESS;
    localObject[j] = localEntityType;
    $VALUES = (EntityType[])localObject;
  }
  
  private TruecallerContract$Filters$EntityType(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static EntityType fromValue(int paramInt)
  {
    EntityType[] arrayOfEntityType = values();
    int i = 0;
    for (;;)
    {
      int j = arrayOfEntityType.length;
      if (i >= j) {
        break;
      }
      EntityType localEntityType = arrayOfEntityType[i];
      j = value;
      if (j == paramInt) {
        return arrayOfEntityType[i];
      }
      i += 1;
    }
    return UNKNOWN;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract.Filters.EntityType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */