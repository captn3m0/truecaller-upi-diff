package com.truecaller.content;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.common.c.a.a.e;
import com.truecaller.common.c.a.a.f;
import com.truecaller.common.c.a.a.h;
import com.truecaller.log.AssertionUtil.AlwaysFatal;

public final class m
  implements a.e, a.f, a.h
{
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    k.b(parama, "provider");
    k.b(parama1, "helper");
    k.b(paramUri, "uri");
    k.b(paramContentValues, "values");
    parama = new android/database/sqlite/SQLiteException;
    parama.<init>("Update not supported");
    throw ((Throwable)parama);
  }
  
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    k.b(parama, "provider");
    k.b(parama1, "helper");
    k.b(paramUri, "uri");
    parama = new android/database/sqlite/SQLiteException;
    parama.<init>("Delete not supported");
    throw ((Throwable)parama);
  }
  
  public final Uri a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues)
  {
    k.b(parama, "provider");
    k.b(parama1, "helper");
    k.b(paramUri, "uri");
    k.b(paramContentValues, "values");
    parama1 = paramContentValues.getAsString("normalized_number");
    Object localObject1 = parama1;
    localObject1 = (CharSequence)parama1;
    int i = 1;
    if (localObject1 != null)
    {
      j = ((CharSequence)localObject1).length();
      if (j != 0)
      {
        j = 0;
        localObject1 = null;
        break label79;
      }
    }
    int j = 1;
    label79:
    Object localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isFalse(j, (String[])localObject2);
    localObject1 = parama.c();
    localObject2 = "msg_im_users";
    String str = "normalized_number=?";
    String[] arrayOfString1 = new String[i];
    arrayOfString1[0] = parama1;
    int k = ((SQLiteDatabase)localObject1).update((String)localObject2, paramContentValues, str, arrayOfString1);
    if (k == 0)
    {
      localObject1 = parama.c();
      localObject2 = "msg_im_users";
      str = null;
      ((SQLiteDatabase)localObject1).insert((String)localObject2, null, paramContentValues);
    }
    localObject1 = "im_peer_id";
    boolean bool = paramContentValues.containsKey((String)localObject1);
    if (bool)
    {
      k.a(parama1, "normalizedNumber");
      paramContentValues = paramContentValues.getAsString("im_peer_id");
      localObject1 = new android/content/ContentValues;
      ((ContentValues)localObject1).<init>();
      ((ContentValues)localObject1).put("tc_im_peer_id", paramContentValues);
      paramContentValues = parama.c();
      localObject2 = "msg_participants";
      str = "normalized_destination=?";
      String[] arrayOfString2 = new String[i];
      arrayOfString2[0] = parama1;
      int m = paramContentValues.update((String)localObject2, (ContentValues)localObject1, str, arrayOfString2);
      if (m > 0)
      {
        parama1 = TruecallerContract.ae.a();
        parama.a(parama1);
      }
    }
    return paramUri;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */