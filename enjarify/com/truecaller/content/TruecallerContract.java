package com.truecaller.content;

import android.net.Uri;

public final class TruecallerContract
{
  public static String a;
  public static Uri b;
  
  public static String a()
  {
    return a;
  }
  
  public static void a(String paramString)
  {
    a = paramString;
    paramString = String.valueOf(paramString);
    b = Uri.parse("content://".concat(paramString));
  }
  
  public static Uri b()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContract
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */