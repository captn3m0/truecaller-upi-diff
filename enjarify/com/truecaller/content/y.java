package com.truecaller.content;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import com.truecaller.common.c.a.a.h;

final class y
  implements a.h
{
  public final int a(com.truecaller.common.c.a parama, com.truecaller.common.c.a.a parama1, Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    parama = new android/database/sqlite/SQLiteException;
    parama1 = new java/lang/StringBuilder;
    parama1.<init>();
    parama1.append(paramUri);
    parama1.append(" does not support update");
    parama1 = parama1.toString();
    parama.<init>(parama1);
    throw parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */