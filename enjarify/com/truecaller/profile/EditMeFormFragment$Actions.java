package com.truecaller.profile;

import com.truecaller.ui.components.n.b;

public enum EditMeFormFragment$Actions
  implements n.b
{
  private final int _stringId;
  
  static
  {
    Object localObject = new com/truecaller/profile/EditMeFormFragment$Actions;
    ((Actions)localObject).<init>("Camera", 0, 2131886930);
    Camera = (Actions)localObject;
    localObject = new com/truecaller/profile/EditMeFormFragment$Actions;
    int i = 1;
    ((Actions)localObject).<init>("Gallery", i, 2131886931);
    Gallery = (Actions)localObject;
    localObject = new com/truecaller/profile/EditMeFormFragment$Actions;
    int j = 2;
    ((Actions)localObject).<init>("Social", j, 0);
    Social = (Actions)localObject;
    localObject = new com/truecaller/profile/EditMeFormFragment$Actions;
    int k = 3;
    ((Actions)localObject).<init>("Remove", k, 2131886932);
    Remove = (Actions)localObject;
    localObject = new Actions[4];
    Actions localActions = Camera;
    localObject[0] = localActions;
    localActions = Gallery;
    localObject[i] = localActions;
    localActions = Social;
    localObject[j] = localActions;
    localActions = Remove;
    localObject[k] = localActions;
    $VALUES = (Actions[])localObject;
  }
  
  private EditMeFormFragment$Actions(int paramInt1)
  {
    _stringId = paramInt1;
  }
  
  public final int getDetailsId()
  {
    return -1;
  }
  
  public final int getImageId()
  {
    return 0;
  }
  
  public final int getNameId()
  {
    return _stringId;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.EditMeFormFragment.Actions
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */