package com.truecaller.profile.business;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.common.h.n;

final class CreateBusinessProfileActivity$x
  implements DialogInterface.OnClickListener
{
  CreateBusinessProfileActivity$x(CreateBusinessProfileActivity paramCreateBusinessProfileActivity) {}
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    paramDialogInterface = a;
    Object localObject = paramDialogInterface;
    localObject = n.d((Context)paramDialogInterface).toString();
    k.a(localObject, "ImageUtils.getCapturedImageUri(this).toString()");
    CreateBusinessProfileActivity.a(paramDialogInterface, (String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */