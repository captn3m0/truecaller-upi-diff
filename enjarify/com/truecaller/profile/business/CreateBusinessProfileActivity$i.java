package com.truecaller.profile.business;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import c.g.b.k;
import com.truecaller.R.id;

final class CreateBusinessProfileActivity$i
  implements View.OnClickListener
{
  CreateBusinessProfileActivity$i(CreateBusinessProfileActivity paramCreateBusinessProfileActivity) {}
  
  public final void onClick(View paramView)
  {
    paramView = a;
    int i = R.id.addMoreInfoLinear;
    paramView = (LinearLayout)paramView.g(i);
    Object localObject = "addMoreInfoLinear";
    k.a(paramView, (String)localObject);
    int j = paramView.getHeight();
    if (j != 0)
    {
      paramView = a;
      i = R.id.addMoreInfoLinear;
      localObject = (LinearLayout)paramView.g(i);
      k.a(localObject, "addMoreInfoLinear");
      localObject = (View)localObject;
      CreateBusinessProfileActivity.b(paramView, (View)localObject);
      return;
    }
    paramView = a;
    i = R.id.addMoreInfoLinear;
    localObject = (LinearLayout)paramView.g(i);
    k.a(localObject, "addMoreInfoLinear");
    localObject = (View)localObject;
    CreateBusinessProfileActivity.a(paramView, (View)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */