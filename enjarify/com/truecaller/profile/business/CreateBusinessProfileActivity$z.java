package com.truecaller.profile.business;

import android.support.v7.widget.CardView;
import android.view.View;
import c.g.b.k;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition.Builder;
import com.google.android.gms.maps.model.LatLng;
import com.truecaller.R.id;
import com.truecaller.utils.extensions.t;

final class CreateBusinessProfileActivity$z
  implements OnMapReadyCallback
{
  CreateBusinessProfileActivity$z(CreateBusinessProfileActivity paramCreateBusinessProfileActivity, double paramDouble1, double paramDouble2) {}
  
  public final void a(GoogleMap paramGoogleMap)
  {
    Object localObject = a;
    int i = R.id.mapCardView;
    localObject = (CardView)((CreateBusinessProfileActivity)localObject).g(i);
    k.a(localObject, "mapCardView");
    t.a((View)localObject);
    k.a(paramGoogleMap, "it");
    localObject = paramGoogleMap.c();
    k.a(localObject, "it.uiSettings");
    ((UiSettings)localObject).e();
    localObject = new com/google/android/gms/maps/model/CameraPosition$Builder;
    ((CameraPosition.Builder)localObject).<init>();
    localObject = ((CameraPosition.Builder)localObject).a();
    LatLng localLatLng = new com/google/android/gms/maps/model/LatLng;
    double d1 = b;
    double d2 = c;
    localLatLng.<init>(d1, d2);
    localObject = CameraUpdateFactory.a(((CameraPosition.Builder)localObject).a(localLatLng).b());
    paramGoogleMap.a((CameraUpdate)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */