package com.truecaller.profile.business;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;

public final class CreateBusinessProfileActivity$l
  implements AdapterView.OnItemSelectedListener
{
  CreateBusinessProfileActivity$l(CreateBusinessProfileActivity paramCreateBusinessProfileActivity) {}
  
  public final void onItemSelected(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    paramAdapterView = a;
    int i = R.id.sizeEditText;
    paramAdapterView = (TextView)paramAdapterView.g(i);
    k.a(paramAdapterView, "sizeEditText");
    paramAdapterView.setError(null);
  }
  
  public final void onNothingSelected(AdapterView paramAdapterView) {}
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */