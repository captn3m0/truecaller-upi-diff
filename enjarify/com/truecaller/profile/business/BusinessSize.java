package com.truecaller.profile.business;

public enum BusinessSize
{
  private final int option;
  
  static
  {
    BusinessSize[] arrayOfBusinessSize = new BusinessSize[5];
    BusinessSize localBusinessSize = new com/truecaller/profile/business/BusinessSize;
    localBusinessSize.<init>("MICRO", 0, 2131886282);
    MICRO = localBusinessSize;
    arrayOfBusinessSize[0] = localBusinessSize;
    localBusinessSize = new com/truecaller/profile/business/BusinessSize;
    int i = 1;
    localBusinessSize.<init>("SMALL", i, 2131886283);
    SMALL = localBusinessSize;
    arrayOfBusinessSize[i] = localBusinessSize;
    localBusinessSize = new com/truecaller/profile/business/BusinessSize;
    i = 2;
    localBusinessSize.<init>("MEDIUM", i, 2131886281);
    MEDIUM = localBusinessSize;
    arrayOfBusinessSize[i] = localBusinessSize;
    localBusinessSize = new com/truecaller/profile/business/BusinessSize;
    i = 3;
    localBusinessSize.<init>("LARGE", i, 2131886280);
    LARGE = localBusinessSize;
    arrayOfBusinessSize[i] = localBusinessSize;
    localBusinessSize = new com/truecaller/profile/business/BusinessSize;
    i = 4;
    localBusinessSize.<init>("ENTERPRISE", i, 2131886279);
    ENTERPRISE = localBusinessSize;
    arrayOfBusinessSize[i] = localBusinessSize;
    $VALUES = arrayOfBusinessSize;
  }
  
  private BusinessSize(int paramInt1)
  {
    option = paramInt1;
  }
  
  public final int getOption()
  {
    return option;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.BusinessSize
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */