package com.truecaller.profile.business;

import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import c.u;
import c.x;
import com.truecaller.R.id;
import com.truecaller.common.h.ac;
import com.truecaller.profile.business.address.BusinessAddressInput;
import com.truecaller.profile.business.openHours.OpenHoursFragment;
import com.truecaller.profile.business.openHours.f;
import com.truecaller.profile.business.openHours.g;
import com.truecaller.profile.data.dto.Address;
import com.truecaller.profile.data.dto.Branding;
import com.truecaller.profile.data.dto.BusinessData;
import com.truecaller.profile.data.dto.Company;
import com.truecaller.profile.data.dto.OnlineIds;
import com.truecaller.profile.data.dto.OpenHours;
import com.truecaller.profile.data.dto.Profile;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

final class CreateBusinessProfileActivity$m
  implements View.OnClickListener
{
  CreateBusinessProfileActivity$m(CreateBusinessProfileActivity paramCreateBusinessProfileActivity) {}
  
  public final void onClick(View paramView)
  {
    Object localObject1 = this;
    Object localObject2 = CreateBusinessProfileActivity.b(a);
    Object localObject3 = CreateBusinessProfileActivity.c(a);
    Object localObject4 = a;
    int i = R.id.nameEditText;
    localObject4 = (TextInputEditText)((CreateBusinessProfileActivity)localObject4).g(i);
    Object localObject5 = "nameEditText";
    k.a(localObject4, (String)localObject5);
    localObject4 = String.valueOf(((TextInputEditText)localObject4).getText());
    if (localObject4 != null)
    {
      String str1 = c.n.m.b((CharSequence)localObject4).toString();
      Object localObject6 = c.a.m.g((Iterable)CreateBusinessProfileActivity.d(a));
      localObject4 = a;
      i = R.id.descriptionEditText;
      localObject4 = (TextInputEditText)((CreateBusinessProfileActivity)localObject4).g(i);
      localObject5 = "descriptionEditText";
      k.a(localObject4, (String)localObject5);
      localObject4 = String.valueOf(((TextInputEditText)localObject4).getText());
      if (localObject4 != null)
      {
        Object localObject7 = c.n.m.b((CharSequence)localObject4).toString();
        Object localObject8 = c.a.m.b(CreateBusinessProfileActivity.e(a));
        Object localObject9 = CreateBusinessProfileActivity.f(a);
        localObject4 = ga).a;
        if (localObject4 == null)
        {
          localObject5 = "presenter";
          k.a((String)localObject5);
        }
        Object localObject10 = a.c();
        localObject4 = a;
        i = R.id.firstNameEditText;
        localObject4 = (TextInputEditText)((CreateBusinessProfileActivity)localObject4).g(i);
        localObject5 = "firstNameEditText";
        k.a(localObject4, (String)localObject5);
        localObject4 = String.valueOf(((TextInputEditText)localObject4).getText());
        if (localObject4 != null)
        {
          Object localObject11 = c.n.m.b((CharSequence)localObject4).toString();
          localObject4 = a;
          i = R.id.lastNameEditText;
          localObject4 = (TextInputEditText)((CreateBusinessProfileActivity)localObject4).g(i);
          localObject5 = "lastNameEditText";
          k.a(localObject4, (String)localObject5);
          localObject4 = String.valueOf(((TextInputEditText)localObject4).getText());
          if (localObject4 != null)
          {
            Object localObject12 = c.n.m.b((CharSequence)localObject4).toString();
            Object localObject13 = c.a.m.b(CreateBusinessProfileActivity.h(a));
            localObject4 = a;
            i = R.id.emailEditText;
            localObject4 = (TextInputEditText)((CreateBusinessProfileActivity)localObject4).g(i);
            localObject5 = "emailEditText";
            k.a(localObject4, (String)localObject5);
            localObject4 = String.valueOf(((TextInputEditText)localObject4).getText());
            if (localObject4 != null)
            {
              String str2 = c.n.m.b((CharSequence)localObject4).toString();
              localObject4 = a;
              i = R.id.designationEditText;
              localObject4 = (TextInputEditText)((CreateBusinessProfileActivity)localObject4).g(i);
              localObject5 = "designationEditText";
              k.a(localObject4, (String)localObject5);
              localObject4 = String.valueOf(((TextInputEditText)localObject4).getText());
              if (localObject4 != null)
              {
                String str3 = c.n.m.b((CharSequence)localObject4).toString();
                localObject4 = a;
                i = R.id.websiteEditText;
                localObject4 = (TextInputEditText)((CreateBusinessProfileActivity)localObject4).g(i);
                localObject5 = "websiteEditText";
                k.a(localObject4, (String)localObject5);
                localObject4 = String.valueOf(((TextInputEditText)localObject4).getText());
                if (localObject4 != null)
                {
                  Object localObject14 = c.n.m.b((CharSequence)localObject4).toString();
                  localObject4 = a;
                  i = R.id.facebookEditText;
                  localObject4 = (TextInputEditText)((CreateBusinessProfileActivity)localObject4).g(i);
                  localObject5 = "facebookEditText";
                  k.a(localObject4, (String)localObject5);
                  localObject4 = String.valueOf(((TextInputEditText)localObject4).getText());
                  if (localObject4 != null)
                  {
                    Object localObject15 = c.n.m.b((CharSequence)localObject4).toString();
                    localObject4 = a;
                    i = R.id.twitterEditText;
                    localObject4 = (TextInputEditText)((CreateBusinessProfileActivity)localObject4).g(i);
                    localObject5 = "twitterEditText";
                    k.a(localObject4, (String)localObject5);
                    localObject4 = String.valueOf(((TextInputEditText)localObject4).getText());
                    if (localObject4 != null)
                    {
                      Object localObject16 = c.n.m.b((CharSequence)localObject4).toString();
                      localObject4 = new com/truecaller/profile/business/c;
                      localObject5 = localObject4;
                      ((c)localObject4).<init>((String)localObject2, (String)localObject3, str1, (List)localObject6, (String)localObject7, (List)localObject8, (String)localObject9, (List)localObject10, (String)localObject11, (String)localObject12, (List)localObject13, str2, str3, (String)localObject14, (String)localObject15, (String)localObject16);
                      localObject5 = a.a();
                      k.b(localObject4, "input");
                      localObject2 = d;
                      localObject3 = (CharSequence)c;
                      boolean bool1 = c.n.m.a((CharSequence)localObject3);
                      int m = 2131886247;
                      localObject7 = null;
                      localObject8 = null;
                      int n;
                      if (bool1)
                      {
                        localObject4 = (n.a)b;
                        if (localObject4 != null)
                        {
                          n = 2131363787;
                          ((n.a)localObject4).a(n, m);
                          localObject4 = x.a;
                        }
                        localObject4 = x.a;
                      }
                      else
                      {
                        int j = 2131361992;
                        if (localObject2 == null)
                        {
                          localObject4 = (n.a)b;
                          if (localObject4 != null)
                          {
                            ((n.a)localObject4).b(j);
                            localObject4 = x.a;
                          }
                          localObject4 = x.a;
                        }
                        else
                        {
                          localObject9 = (CharSequence)((BusinessAddressInput)localObject2).getStreet();
                          boolean bool4 = c.n.m.a((CharSequence)localObject9);
                          if (bool4)
                          {
                            localObject4 = (n.a)b;
                            if (localObject4 != null)
                            {
                              ((n.a)localObject4).b(j);
                              localObject4 = x.a;
                            }
                            localObject4 = x.a;
                          }
                          else
                          {
                            localObject9 = (CharSequence)((BusinessAddressInput)localObject2).getCity();
                            bool4 = c.n.m.a((CharSequence)localObject9);
                            if (bool4)
                            {
                              localObject4 = (n.a)b;
                              if (localObject4 != null)
                              {
                                ((n.a)localObject4).b(j);
                                localObject4 = x.a;
                              }
                              localObject4 = x.a;
                            }
                            else
                            {
                              localObject9 = (CharSequence)((BusinessAddressInput)localObject2).getCountryCode();
                              bool4 = c.n.m.a((CharSequence)localObject9);
                              if (bool4)
                              {
                                localObject4 = (n.a)b;
                                if (localObject4 != null)
                                {
                                  ((n.a)localObject4).b(j);
                                  localObject4 = x.a;
                                }
                                localObject4 = x.a;
                              }
                              else
                              {
                                localObject3 = (CharSequence)e;
                                boolean bool2 = c.n.m.a((CharSequence)localObject3);
                                if (bool2)
                                {
                                  localObject4 = (n.a)b;
                                  if (localObject4 != null)
                                  {
                                    n = 2131362811;
                                    ((n.a)localObject4).a(n, m);
                                    localObject4 = x.a;
                                  }
                                  localObject4 = x.a;
                                }
                                else
                                {
                                  localObject3 = f;
                                  bool2 = ((List)localObject3).isEmpty();
                                  if (bool2)
                                  {
                                    localObject4 = (n.a)b;
                                    if (localObject4 != null)
                                    {
                                      n = 2131364666;
                                      ((n.a)localObject4).b(n);
                                      localObject4 = x.a;
                                    }
                                    localObject4 = x.a;
                                  }
                                  else
                                  {
                                    localObject3 = g;
                                    if (localObject3 == null)
                                    {
                                      localObject4 = (n.a)b;
                                      if (localObject4 != null)
                                      {
                                        n = 2131364529;
                                        ((n.a)localObject4).b(n);
                                        localObject4 = x.a;
                                      }
                                      localObject4 = x.a;
                                    }
                                    else
                                    {
                                      localObject3 = h;
                                      bool2 = ((List)localObject3).isEmpty();
                                      if (bool2)
                                      {
                                        localObject4 = (n.a)b;
                                        if (localObject4 != null)
                                        {
                                          ((n.a)localObject4).c(0);
                                          localObject4 = x.a;
                                        }
                                        localObject4 = x.a;
                                      }
                                      else
                                      {
                                        localObject3 = h;
                                        int k;
                                        if (localObject3 != null)
                                        {
                                          localObject9 = localObject3;
                                          localObject9 = ((Iterable)localObject3).iterator();
                                          boolean bool6;
                                          label1422:
                                          do
                                          {
                                            boolean bool5 = ((Iterator)localObject9).hasNext();
                                            if (!bool5) {
                                              break;
                                            }
                                            localObject10 = (OpenHours)((Iterator)localObject9).next();
                                            localObject11 = ((OpenHours)localObject10).getWeekday();
                                            bool6 = ((SortedSet)localObject11).isEmpty();
                                            if (bool6)
                                            {
                                              localObject9 = (n.a)b;
                                              if (localObject9 != null)
                                              {
                                                k = ((List)localObject3).indexOf(localObject10);
                                                ((n.a)localObject9).c(k);
                                              }
                                              k = 0;
                                              localObject3 = null;
                                              break label1565;
                                            }
                                            localObject11 = (CharSequence)((OpenHours)localObject10).getOpens();
                                            if (localObject11 != null)
                                            {
                                              bool6 = c.n.m.a((CharSequence)localObject11);
                                              if (!bool6)
                                              {
                                                bool6 = false;
                                                localObject11 = null;
                                                break label1422;
                                              }
                                            }
                                            bool6 = true;
                                            if (bool6)
                                            {
                                              localObject9 = (n.a)b;
                                              if (localObject9 != null)
                                              {
                                                k = ((List)localObject3).indexOf(localObject10);
                                                ((n.a)localObject9).d(k);
                                              }
                                              k = 0;
                                              localObject3 = null;
                                              break label1565;
                                            }
                                            localObject11 = (CharSequence)((OpenHours)localObject10).getCloses();
                                            if (localObject11 != null)
                                            {
                                              bool6 = c.n.m.a((CharSequence)localObject11);
                                              if (!bool6)
                                              {
                                                bool6 = false;
                                                localObject11 = null;
                                                continue;
                                              }
                                            }
                                            bool6 = true;
                                          } while (!bool6);
                                          localObject9 = (n.a)b;
                                          if (localObject9 != null)
                                          {
                                            k = ((List)localObject3).indexOf(localObject10);
                                            ((n.a)localObject9).e(k);
                                          }
                                          k = 0;
                                          localObject3 = null;
                                        }
                                        else
                                        {
                                          k = 1;
                                        }
                                        label1565:
                                        if (k != 0)
                                        {
                                          localObject3 = (CharSequence)i;
                                          boolean bool3 = c.n.m.a((CharSequence)localObject3);
                                          if (bool3)
                                          {
                                            localObject4 = (n.a)b;
                                            if (localObject4 != null)
                                            {
                                              n = 2131363095;
                                              ((n.a)localObject4).a(n, m);
                                              localObject4 = x.a;
                                            }
                                            localObject4 = x.a;
                                          }
                                          else
                                          {
                                            localObject3 = (CharSequence)j;
                                            bool3 = c.n.m.a((CharSequence)localObject3);
                                            if (bool3)
                                            {
                                              localObject4 = (n.a)b;
                                              if (localObject4 != null)
                                              {
                                                n = 2131363587;
                                                ((n.a)localObject4).a(n, m);
                                                localObject4 = x.a;
                                              }
                                              localObject4 = x.a;
                                            }
                                            else
                                            {
                                              localObject3 = (CharSequence)l;
                                              bool3 = c.n.m.a((CharSequence)localObject3);
                                              int i1 = 2131362942;
                                              if (bool3)
                                              {
                                                localObject4 = (n.a)b;
                                                if (localObject4 != null)
                                                {
                                                  ((n.a)localObject4).a(i1, m);
                                                  localObject4 = x.a;
                                                }
                                                localObject4 = x.a;
                                              }
                                              else
                                              {
                                                localObject3 = l;
                                                localObject6 = (n.a)b;
                                                if (localObject6 != null)
                                                {
                                                  bool3 = ((n.a)localObject6).h((String)localObject3);
                                                }
                                                else
                                                {
                                                  bool3 = false;
                                                  localObject3 = null;
                                                }
                                                if (!bool3)
                                                {
                                                  localObject4 = (n.a)b;
                                                  if (localObject4 != null)
                                                  {
                                                    n = 2131886941;
                                                    ((n.a)localObject4).a(i1, n);
                                                    localObject4 = x.a;
                                                  }
                                                  localObject4 = x.a;
                                                }
                                                else
                                                {
                                                  localObject10 = i;
                                                  localObject11 = j;
                                                  localObject13 = k;
                                                  str2 = a;
                                                  str3 = m;
                                                  localObject3 = e;
                                                  localObject6 = f;
                                                  localObject8 = new com/truecaller/profile/data/dto/OnlineIds;
                                                  localObject9 = o;
                                                  localObject12 = l;
                                                  str1 = n;
                                                  localObject7 = p;
                                                  ((OnlineIds)localObject8).<init>((String)localObject9, (String)localObject12, str1, (String)localObject7);
                                                  str1 = c;
                                                  localObject7 = h;
                                                  localObject9 = new com/truecaller/profile/data/dto/Address;
                                                  String str4 = ((BusinessAddressInput)localObject2).getStreet();
                                                  Object localObject17 = ((BusinessAddressInput)localObject2).getZipCode();
                                                  Object localObject18 = ((BusinessAddressInput)localObject2).getCity();
                                                  Object localObject19 = ((BusinessAddressInput)localObject2).getCountryCode();
                                                  Object localObject20 = ((BusinessAddressInput)localObject2).getLatitude();
                                                  Double localDouble = ((BusinessAddressInput)localObject2).getLongitude();
                                                  ((Address)localObject9).<init>(str4, (String)localObject18, (String)localObject17, (String)localObject19, (Double)localObject20, localDouble);
                                                  localObject2 = b;
                                                  localObject12 = d;
                                                  if (localObject12 != null)
                                                  {
                                                    localObject12 = c.a.m.d((Collection)localObject12);
                                                  }
                                                  else
                                                  {
                                                    localObject12 = new java/util/ArrayList;
                                                    ((ArrayList)localObject12).<init>();
                                                    localObject12 = (List)localObject12;
                                                  }
                                                  localObject1 = new com/truecaller/profile/data/dto/Branding;
                                                  ((Branding)localObject1).<init>((String)localObject2, (List)localObject12);
                                                  localObject4 = g;
                                                  localObject2 = new com/truecaller/profile/data/dto/Company;
                                                  str4 = str1;
                                                  localObject18 = localObject7;
                                                  localObject17 = localObject9;
                                                  localObject19 = localObject1;
                                                  localObject20 = localObject4;
                                                  ((Company)localObject2).<init>(str1, (List)localObject7, (Address)localObject9, (Branding)localObject1, (String)localObject4);
                                                  localObject1 = new com/truecaller/profile/data/dto/BusinessData;
                                                  localObject12 = localObject1;
                                                  localObject14 = localObject3;
                                                  localObject15 = localObject6;
                                                  localObject16 = localObject8;
                                                  ((BusinessData)localObject1).<init>((List)localObject13, str2, str3, (String)localObject3, (List)localObject6, (OnlineIds)localObject8, (Company)localObject2);
                                                  localObject4 = new com/truecaller/profile/data/dto/Profile;
                                                  localObject12 = null;
                                                  int i2 = 4;
                                                  str3 = null;
                                                  localObject9 = localObject4;
                                                  localObject13 = localObject1;
                                                  ((Profile)localObject4).<init>((String)localObject10, (String)localObject11, null, (BusinessData)localObject1, i2, null);
                                                  localObject8 = localObject4;
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                      c = ((Profile)localObject8);
                      localObject1 = c;
                      if (localObject1 != null)
                      {
                        localObject1 = k;
                        boolean bool7 = ((ac)localObject1).a();
                        if (bool7)
                        {
                          bool7 = e;
                          if (!bool7)
                          {
                            i3 = 1;
                            break label2263;
                          }
                        }
                        int i3 = 0;
                        label2263:
                        if (i3 != 0)
                        {
                          localObject1 = (n.a)b;
                          if (localObject1 != null) {
                            ((n.a)localObject1).o();
                          }
                          return;
                        }
                        ((o)localObject5).e();
                      }
                      return;
                    }
                    localObject1 = new c/u;
                    ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
                    throw ((Throwable)localObject1);
                  }
                  localObject1 = new c/u;
                  ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
                  throw ((Throwable)localObject1);
                }
                localObject1 = new c/u;
                ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
                throw ((Throwable)localObject1);
              }
              localObject1 = new c/u;
              ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
              throw ((Throwable)localObject1);
            }
            localObject1 = new c/u;
            ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
            throw ((Throwable)localObject1);
          }
          localObject1 = new c/u;
          ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
          throw ((Throwable)localObject1);
        }
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
        throw ((Throwable)localObject1);
      }
      localObject1 = new c/u;
      ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
      throw ((Throwable)localObject1);
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */