package com.truecaller.profile.business;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import c.a.y;
import c.u;
import com.d.b.w;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.truecaller.R.id;
import com.truecaller.common.account.r;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.am;
import com.truecaller.common.h.n;
import com.truecaller.common.tag.TagView;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker.a;
import com.truecaller.profile.business.address.BusinessAddressInput;
import com.truecaller.profile.business.openHours.OpenHoursFragment;
import com.truecaller.profile.data.dto.Address;
import com.truecaller.profile.data.dto.Branding;
import com.truecaller.profile.data.dto.BusinessData;
import com.truecaller.profile.data.dto.Company;
import com.truecaller.profile.data.dto.OnlineIds;
import com.truecaller.profile.data.dto.OpenHours;
import com.truecaller.profile.data.dto.Profile;
import com.truecaller.tag.TagPickActivity;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.components.CircularImageView;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.util.at;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class CreateBusinessProfileActivity
  extends AppCompatActivity
  implements n.a
{
  public static final CreateBusinessProfileActivity.a i;
  public o a;
  public com.truecaller.featuretoggles.e b;
  public com.truecaller.util.g c;
  public com.truecaller.common.g.a d;
  public r e;
  public com.truecaller.analytics.b f;
  public c.d.f g;
  public c.d.f h;
  private a j;
  private d k;
  private OpenHoursFragment l;
  private String m;
  private String n = "";
  private Set o;
  private Long p;
  private com.truecaller.ui.dialogs.k q;
  private HashMap r;
  
  static
  {
    CreateBusinessProfileActivity.a locala = new com/truecaller/profile/business/CreateBusinessProfileActivity$a;
    locala.<init>((byte)0);
    i = locala;
  }
  
  public CreateBusinessProfileActivity()
  {
    Object localObject = new java/util/LinkedHashSet;
    ((LinkedHashSet)localObject).<init>();
    localObject = (Set)localObject;
    o = ((Set)localObject);
  }
  
  public static final Intent a(Context paramContext)
  {
    return CreateBusinessProfileActivity.a.a(paramContext, false, true, false);
  }
  
  private static void a(View paramView, int paramInt1, int paramInt2)
  {
    int[] arrayOfInt = new int[2];
    arrayOfInt[0] = paramInt1;
    arrayOfInt[1] = paramInt2;
    ValueAnimator localValueAnimator = ValueAnimator.ofInt(arrayOfInt);
    Object localObject = new com/truecaller/profile/business/CreateBusinessProfileActivity$d;
    ((CreateBusinessProfileActivity.d)localObject).<init>(paramView);
    localObject = (ValueAnimator.AnimatorUpdateListener)localObject;
    localValueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject);
    paramView = new android/view/animation/DecelerateInterpolator;
    paramView.<init>();
    paramView = (TimeInterpolator)paramView;
    localValueAnimator.setInterpolator(paramView);
    localValueAnimator.start();
  }
  
  public static final Intent b(Context paramContext)
  {
    return CreateBusinessProfileActivity.a.a(paramContext, false, true, false);
  }
  
  private final void i(String paramString)
  {
    int i1 = Color.parseColor(paramString);
    double d1 = com.truecaller.utils.extensions.f.a(i1);
    double d2 = 85.0D;
    boolean bool = d1 < d2;
    PorterDuff.Mode localMode;
    if (bool)
    {
      i2 = 1;
    }
    else
    {
      i2 = 0;
      localMode = null;
    }
    if (i2 != 0) {
      i2 = 2131099827;
    } else {
      i2 = 2131099831;
    }
    Object localObject = this;
    int i2 = android.support.v4.content.b.c((Context)this, i2);
    int i3 = R.id.headerView;
    g(i3).setBackgroundColor(i1);
    i1 = R.id.logoButton;
    ((Button)g(i1)).setTextColor(i2);
    i1 = R.id.backButton;
    ((TintedImageView)g(i1)).setTint(i2);
    i1 = R.id.toolbar;
    paramString = (Toolbar)g(i1);
    c.g.b.k.a(paramString, "toolbar");
    i3 = R.id.toolbar;
    localObject = (Toolbar)g(i3);
    String str = "toolbar";
    c.g.b.k.a(localObject, str);
    localObject = ((Toolbar)localObject).getOverflowIcon();
    if (localObject != null)
    {
      localObject = android.support.v4.graphics.drawable.a.e((Drawable)localObject);
      android.support.v4.graphics.drawable.a.a((Drawable)localObject, i2);
      localMode = PorterDuff.Mode.SRC_IN;
      android.support.v4.graphics.drawable.a.a((Drawable)localObject, localMode);
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    paramString.setOverflowIcon((Drawable)localObject);
  }
  
  private final void j(String paramString)
  {
    ag localag = (ag)bg.a;
    c.d.f localf = g;
    if (localf == null)
    {
      localObject = "asyncCoroutineContext";
      c.g.b.k.a((String)localObject);
    }
    Object localObject = new com/truecaller/profile/business/CreateBusinessProfileActivity$f;
    ((CreateBusinessProfileActivity.f)localObject).<init>(this, paramString, null);
    localObject = (c.g.a.m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (c.g.a.m)localObject, 2);
  }
  
  private void v()
  {
    com.truecaller.ui.dialogs.k localk = q;
    if (localk != null) {
      localk.dismissAllowingStateLoss();
    }
    q = null;
  }
  
  public final o a()
  {
    o localo = a;
    if (localo == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return localo;
  }
  
  public final void a(double paramDouble1, double paramDouble2)
  {
    Object localObject1 = getSupportFragmentManager();
    int i1 = 2131363726;
    localObject1 = ((android.support.v4.app.j)localObject1).a(i1);
    if (localObject1 != null)
    {
      localObject1 = (SupportMapFragment)localObject1;
      Object localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$z;
      ((CreateBusinessProfileActivity.z)localObject2).<init>(this, paramDouble1, paramDouble2);
      localObject2 = (OnMapReadyCallback)localObject2;
      ((SupportMapFragment)localObject1).a((OnMapReadyCallback)localObject2);
      return;
    }
    u localu = new c/u;
    localu.<init>("null cannot be cast to non-null type com.google.android.gms.maps.SupportMapFragment");
    throw localu;
  }
  
  public final void a(int paramInt)
  {
    o localo = a;
    if (localo == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    Object localObject = Integer.valueOf(paramInt);
    f = ((Integer)localObject);
    localObject = (n.a)b;
    if (localObject != null)
    {
      ((n.a)localObject).l();
      return;
    }
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    EditText localEditText = (EditText)findViewById(paramInt1);
    CharSequence localCharSequence = (CharSequence)getString(paramInt2);
    localEditText.setError(localCharSequence);
    localEditText.requestFocus();
  }
  
  public final void a(int paramInt, String paramString)
  {
    c.g.b.k.b(paramString, "picture");
    o.add(paramString);
    d locald = k;
    if (locald == null)
    {
      String str = "picturesAdapter";
      c.g.b.k.a(str);
    }
    c.g.b.k.b(paramString, "picture");
    a.set(paramInt, paramString);
    locald.notifyItemChanged(paramInt);
  }
  
  public final void a(int paramInt, boolean paramBoolean)
  {
    v();
    AlertDialog.Builder localBuilder1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject1 = this;
    localObject1 = (Context)this;
    localBuilder1.<init>((Context)localObject1);
    localBuilder1.setMessage(paramInt);
    Object localObject2;
    if (paramBoolean)
    {
      paramInt = 2131887224;
      localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$s;
      ((CreateBusinessProfileActivity.s)localObject2).<init>(this);
      localObject2 = (DialogInterface.OnClickListener)localObject2;
      AlertDialog.Builder localBuilder2 = localBuilder1.setPositiveButton(paramInt, (DialogInterface.OnClickListener)localObject2);
      paramBoolean = 2131887197;
      localObject1 = (DialogInterface.OnClickListener)CreateBusinessProfileActivity.t.a;
      localBuilder2.setNegativeButton(paramBoolean, (DialogInterface.OnClickListener)localObject1);
    }
    else
    {
      paramInt = 2131887217;
      localObject2 = (DialogInterface.OnClickListener)CreateBusinessProfileActivity.u.a;
      localBuilder1.setNegativeButton(paramInt, (DialogInterface.OnClickListener)localObject2);
    }
    localBuilder1.show();
  }
  
  public final void a(com.truecaller.common.tag.c paramc)
  {
    c.g.b.k.b(paramc, "tag");
    Object localObject = Long.valueOf(a);
    p = ((Long)localObject);
    int i1 = R.id.tagTextView;
    localObject = (TagView)g(i1);
    ((TagView)localObject).setTag(paramc);
    c.g.b.k.a(localObject, "this");
    com.truecaller.utils.extensions.t.a((View)localObject);
    i1 = R.id.tagsEditText;
    localObject = (TextView)g(i1);
    paramc = (CharSequence)b;
    ((TextView)localObject).setText(paramc);
    ((TextView)localObject).setError(null);
  }
  
  public final void a(BusinessAddressInput paramBusinessAddressInput)
  {
    Object localObject = com.truecaller.profile.business.address.b.b;
    localObject = new com/truecaller/profile/business/address/b;
    ((com.truecaller.profile.business.address.b)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramBusinessAddressInput = (Parcelable)paramBusinessAddressInput;
    localBundle.putParcelable("arg_address", paramBusinessAddressInput);
    ((com.truecaller.profile.business.address.b)localObject).setArguments(localBundle);
    paramBusinessAddressInput = getSupportFragmentManager().a();
    int i1 = 2130771992;
    int i2 = 2130771991;
    paramBusinessAddressInput = paramBusinessAddressInput.a(i2, i1, i2, i1);
    localObject = (Fragment)localObject;
    paramBusinessAddressInput = paramBusinessAddressInput.f((Fragment)localObject).b(2131362268, (Fragment)localObject);
    localObject = com.truecaller.profile.business.address.b.class.getName();
    paramBusinessAddressInput.a((String)localObject).d();
  }
  
  public final void a(Profile paramProfile)
  {
    c.g.b.k.b(paramProfile, "profile");
    int i1 = R.id.firstNameEditText;
    Object localObject1 = (TextInputEditText)g(i1);
    Object localObject2 = (CharSequence)paramProfile.getFirstName();
    ((TextInputEditText)localObject1).setText((CharSequence)localObject2);
    i1 = R.id.lastNameEditText;
    localObject1 = (TextInputEditText)g(i1);
    localObject2 = (CharSequence)paramProfile.getLastName();
    ((TextInputEditText)localObject1).setText((CharSequence)localObject2);
    paramProfile = paramProfile.getBusinessData();
    if (paramProfile == null) {
      return;
    }
    i1 = R.id.nameEditText;
    localObject1 = (TextInputEditText)g(i1);
    localObject2 = (CharSequence)paramProfile.getCompany().getName();
    ((TextInputEditText)localObject1).setText((CharSequence)localObject2);
    i1 = R.id.descriptionEditText;
    localObject1 = (TextInputEditText)g(i1);
    localObject2 = (CharSequence)paramProfile.getAbout();
    ((TextInputEditText)localObject1).setText((CharSequence)localObject2);
    i1 = R.id.emailEditText;
    localObject1 = (TextInputEditText)g(i1);
    localObject2 = (CharSequence)paramProfile.getOnlineIds().getEmail();
    ((TextInputEditText)localObject1).setText((CharSequence)localObject2);
    localObject1 = paramProfile.getJobTitle();
    int i2;
    if (localObject1 != null)
    {
      i2 = R.id.designationEditText;
      localObject2 = (TextInputEditText)g(i2);
      localObject1 = (CharSequence)localObject1;
      ((TextInputEditText)localObject2).setText((CharSequence)localObject1);
    }
    localObject1 = paramProfile.getOnlineIds().getUrl();
    if (localObject1 != null)
    {
      i2 = R.id.websiteEditText;
      localObject2 = (TextInputEditText)g(i2);
      localObject1 = (CharSequence)localObject1;
      ((TextInputEditText)localObject2).setText((CharSequence)localObject1);
    }
    localObject1 = paramProfile.getOnlineIds().getFacebookId();
    if (localObject1 != null)
    {
      i2 = R.id.facebookEditText;
      localObject2 = (TextInputEditText)g(i2);
      localObject1 = (CharSequence)localObject1;
      ((TextInputEditText)localObject2).setText((CharSequence)localObject1);
    }
    localObject1 = paramProfile.getOnlineIds().getTwitterId();
    if (localObject1 != null)
    {
      i2 = R.id.twitterEditText;
      localObject2 = (TextInputEditText)g(i2);
      localObject1 = (CharSequence)localObject1;
      ((TextInputEditText)localObject2).setText((CharSequence)localObject1);
    }
    paramProfile = paramProfile.getCompany().getSize();
    if (paramProfile != null)
    {
      i1 = R.id.sizeSpinner;
      localObject1 = (Spinner)g(i1);
      if (paramProfile != null)
      {
        paramProfile = paramProfile.toUpperCase();
        c.g.b.k.a(paramProfile, "(this as java.lang.String).toUpperCase()");
        int i3 = BusinessSize.valueOf(paramProfile).ordinal() + 1;
        ((Spinner)localObject1).setSelection(i3);
        return;
      }
      paramProfile = new c/u;
      paramProfile.<init>("null cannot be cast to non-null type java.lang.String");
      throw paramProfile;
    }
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "color");
    Object localObject = a;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    String str = "color";
    c.g.b.k.b(paramString, str);
    localObject = (n.a)b;
    if (localObject != null)
    {
      ((n.a)localObject).c(paramString);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    c.g.b.k.b(paramString1, "street");
    c.g.b.k.b(paramString3, "city");
    c.g.b.k.b(paramString4, "countryName");
    int i1 = R.id.addressEditText;
    TextView localTextView = (TextView)g(i1);
    c.g.b.k.a(localTextView, "addressEditText");
    String[] arrayOfString = new String[4];
    arrayOfString[0] = paramString1;
    arrayOfString[1] = paramString2;
    arrayOfString[2] = paramString3;
    arrayOfString[3] = paramString4;
    paramString1 = (CharSequence)am.a(arrayOfString);
    localTextView.setText(paramString1);
    int i2 = R.id.addressEditText;
    paramString1 = (TextView)g(i2);
    c.g.b.k.a(paramString1, "addressEditText");
    paramString1.setError(null);
  }
  
  public final void a(List paramList)
  {
    c.g.b.k.b(paramList, "openHours");
    Object localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "openHoursFragment";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = "openHours";
    c.g.b.k.b(paramList, (String)localObject2);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    c.g.b.k.b(paramList, "openHours");
    localObject2 = paramList;
    localObject2 = (Collection)paramList;
    boolean bool1 = ((Collection)localObject2).isEmpty() ^ true;
    if (bool1)
    {
      localObject2 = ((Iterable)c.a.m.f(c.a.m.j((Iterable)a.c()))).iterator();
      Object localObject3;
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject2).hasNext();
        if (!bool2) {
          break;
        }
        localObject3 = (c.a.ab)((Iterator)localObject2).next();
        int i1 = a;
        a.b(i1);
        com.truecaller.profile.business.openHours.e.a locala = (com.truecaller.profile.business.openHours.e.a)b;
        if (locala != null) {
          locala.f(i1);
        }
      }
      localObject2 = a;
      paramList = ((Iterable)((com.truecaller.profile.business.openHours.g)localObject2).a(paramList)).iterator();
      for (;;)
      {
        bool1 = paramList.hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (OpenHours)paramList.next();
        localObject3 = (com.truecaller.profile.business.openHours.e.a)b;
        if (localObject3 != null) {
          ((com.truecaller.profile.business.openHours.e.a)localObject3).a((OpenHours)localObject2);
        }
      }
      paramList = (com.truecaller.profile.business.openHours.e.a)b;
      if (paramList != null)
      {
        paramList.b();
        return;
      }
    }
  }
  
  public final com.truecaller.util.g b()
  {
    com.truecaller.util.g localg = c;
    if (localg == null)
    {
      String str = "bitmapConverter";
      c.g.b.k.a(str);
    }
    return localg;
  }
  
  public final void b(int paramInt)
  {
    TextView localTextView = (TextView)findViewById(paramInt);
    CharSequence localCharSequence = (CharSequence)getString(2131886247);
    localTextView.setError(localCharSequence);
    localTextView.requestFocus();
  }
  
  public final void b(BusinessAddressInput paramBusinessAddressInput)
  {
    c.g.b.k.b(paramBusinessAddressInput, "address");
    o localo = a;
    if (localo == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localo.a(paramBusinessAddressInput);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "logo");
    m = paramString;
    int i1 = R.id.logoButton;
    Object localObject = (Button)g(i1);
    c.g.b.k.a(localObject, "logoButton");
    com.truecaller.utils.extensions.t.b((View)localObject);
    localObject = this;
    paramString = w.a((Context)this).a(paramString);
    c = true;
    paramString = paramString.b();
    i1 = R.id.logoImageView;
    localObject = (CircularImageView)g(i1);
    paramString.a((ImageView)localObject, null);
  }
  
  public final c.d.f c()
  {
    c.d.f localf = h;
    if (localf == null)
    {
      String str = "uiCoroutineContext";
      c.g.b.k.a(str);
    }
    return localf;
  }
  
  public final void c(int paramInt)
  {
    Object localObject = l;
    String str;
    if (localObject == null)
    {
      str = "openHoursFragment";
      c.g.b.k.a(str);
    }
    localObject = b;
    if (localObject == null)
    {
      str = "openHoursAdapter";
      c.g.b.k.a(str);
    }
    e = paramInt;
    b = true;
    ((com.truecaller.profile.business.openHours.d)localObject).notifyItemChanged(paramInt);
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "color");
    n = paramString;
    Object localObject1 = j;
    if (localObject1 == null)
    {
      localObject2 = "colorsAdapter";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = "color";
    c.g.b.k.b(paramString, (String)localObject2);
    int i1 = a.a(paramString);
    int i2 = -1;
    if (i1 == i2)
    {
      i1 = 0;
      localObject2 = null;
    }
    a = i1;
    ((a)localObject1).notifyDataSetChanged();
    int i3 = R.id.colorsRecyclerView;
    localObject1 = (RecyclerView)g(i3);
    localObject2 = j;
    if (localObject2 == null)
    {
      localObject2 = "colorsAdapter";
      c.g.b.k.a((String)localObject2);
    }
    i1 = a.a(paramString);
    ((RecyclerView)localObject1).scrollToPosition(i1);
    i(paramString);
    i3 = Build.VERSION.SDK_INT;
    i1 = 21;
    if (i3 >= i1)
    {
      localObject1 = getWindow();
      localObject2 = "window";
      c.g.b.k.a(localObject1, (String)localObject2);
      int i4 = com.truecaller.utils.extensions.f.b(Color.parseColor(paramString));
      ((Window)localObject1).setStatusBarColor(i4);
    }
  }
  
  public final void d()
  {
    int i1 = R.id.footnoteTextView;
    TextView localTextView = (TextView)g(i1);
    c.g.b.k.a(localTextView, "footnoteTextView");
    localTextView.setVisibility(0);
    i1 = R.id.footnoteTextView;
    localTextView = (TextView)g(i1);
    c.g.b.k.a(localTextView, "footnoteTextView");
    MovementMethod localMovementMethod = LinkMovementMethod.getInstance();
    localTextView.setMovementMethod(localMovementMethod);
  }
  
  public final void d(int paramInt)
  {
    Object localObject = l;
    String str;
    if (localObject == null)
    {
      str = "openHoursFragment";
      c.g.b.k.a(str);
    }
    localObject = b;
    if (localObject == null)
    {
      str = "openHoursAdapter";
      c.g.b.k.a(str);
    }
    e = paramInt;
    c = true;
    ((com.truecaller.profile.business.openHours.d)localObject).notifyItemChanged(paramInt);
  }
  
  public final void d(String paramString)
  {
    c.g.b.k.b(paramString, "logo");
    Object localObject1 = this;
    localObject1 = (Context)this;
    Object localObject2 = Uri.parse(paramString);
    localObject1 = n.a((Context)localObject1, (Uri)localObject2);
    localObject2 = getPackageManager().queryIntentActivities((Intent)localObject1, 0);
    boolean bool = ((List)localObject2).isEmpty();
    if (bool)
    {
      j(paramString);
      return;
    }
    c.g.b.k.a(localObject1, "intent");
    paramString = new android/content/ComponentName;
    c.g.b.k.a(localObject2, "cropApplications");
    String str = dactivityInfo.packageName;
    localObject2 = dactivityInfo.name;
    paramString.<init>(str, (String)localObject2);
    ((Intent)localObject1).setComponent(paramString);
    startActivityForResult((Intent)localObject1, 3);
    overridePendingTransition(0, 0);
  }
  
  public final void e()
  {
    Object localObject = this;
    localObject = (Activity)this;
    Intent localIntent = n.a();
    com.truecaller.common.h.o.a((Activity)localObject, localIntent, 0);
  }
  
  public final void e(int paramInt)
  {
    Object localObject = l;
    String str;
    if (localObject == null)
    {
      str = "openHoursFragment";
      c.g.b.k.a(str);
    }
    localObject = b;
    if (localObject == null)
    {
      str = "openHoursAdapter";
      c.g.b.k.a(str);
    }
    e = paramInt;
    d = true;
    ((com.truecaller.profile.business.openHours.d)localObject).notifyItemChanged(paramInt);
  }
  
  public final void e(String paramString)
  {
    c.g.b.k.b(paramString, "picture");
    Object localObject = a;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    String str = "picture";
    c.g.b.k.b(paramString, str);
    localObject = (n.a)b;
    if (localObject != null)
    {
      ((n.a)localObject).f(paramString);
      return;
    }
  }
  
  public final void f()
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = this;
    localObject = (Context)this;
    localBuilder.<init>((Context)localObject);
    localBuilder = localBuilder.setMessage(2131886268);
    localObject = new com/truecaller/profile/business/CreateBusinessProfileActivity$aa;
    ((CreateBusinessProfileActivity.aa)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    localBuilder = localBuilder.setPositiveButton(2131887217, (DialogInterface.OnClickListener)localObject);
    localObject = (DialogInterface.OnClickListener)CreateBusinessProfileActivity.ab.a;
    localBuilder.setNegativeButton(2131887197, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public final void f(int paramInt)
  {
    v();
    Toast.makeText((Context)this, paramInt, 1).show();
  }
  
  public final void f(String paramString)
  {
    c.g.b.k.b(paramString, "picture");
    Object localObject1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).setMessage(2131886271);
    localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$ac;
    ((CreateBusinessProfileActivity.ac)localObject2).<init>(this, paramString);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    paramString = ((AlertDialog.Builder)localObject1).setPositiveButton(2131887217, (DialogInterface.OnClickListener)localObject2);
    localObject1 = (DialogInterface.OnClickListener)CreateBusinessProfileActivity.ad.a;
    paramString.setNegativeButton(2131887197, (DialogInterface.OnClickListener)localObject1).show();
  }
  
  public final View g(int paramInt)
  {
    Object localObject1 = r;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      r = ((HashMap)localObject1);
    }
    localObject1 = r;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = r;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void g()
  {
    m = null;
    int i1 = R.id.logoButton;
    Button localButton = (Button)g(i1);
    c.g.b.k.a(localButton, "logoButton");
    com.truecaller.utils.extensions.t.a((View)localButton);
    i1 = R.id.logoImageView;
    ((CircularImageView)g(i1)).setImageDrawable(null);
  }
  
  public final void g(String paramString)
  {
    c.g.b.k.b(paramString, "picture");
    o.remove(paramString);
    d locald = k;
    if (locald == null)
    {
      String str = "picturesAdapter";
      c.g.b.k.a(str);
    }
    c.g.b.k.b(paramString, "picture");
    int i1 = a.indexOf(paramString);
    a.set(i1, "");
    locald.notifyItemChanged(i1);
  }
  
  public final void h()
  {
    int i1 = R.id.deleteAddressButton;
    ImageButton localImageButton = (ImageButton)g(i1);
    c.g.b.k.a(localImageButton, "deleteAddressButton");
    com.truecaller.utils.extensions.t.a((View)localImageButton);
    i1 = R.id.deleteAddressButton;
    localImageButton = (ImageButton)g(i1);
    Object localObject = new com/truecaller/profile/business/CreateBusinessProfileActivity$r;
    ((CreateBusinessProfileActivity.r)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    localImageButton.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final boolean h(String paramString)
  {
    c.g.b.k.b(paramString, "email");
    Pattern localPattern = Patterns.EMAIL_ADDRESS;
    paramString = (CharSequence)paramString;
    return localPattern.matcher(paramString).matches();
  }
  
  public final void i()
  {
    int i1 = R.id.addressEditText;
    Object localObject = (TextView)g(i1);
    c.g.b.k.a(localObject, "addressEditText");
    ((TextView)localObject).setText(null);
    i1 = R.id.mapCardView;
    localObject = (CardView)g(i1);
    c.g.b.k.a(localObject, "mapCardView");
    com.truecaller.utils.extensions.t.b((View)localObject);
    i1 = R.id.deleteAddressButton;
    localObject = (ImageButton)g(i1);
    c.g.b.k.a(localObject, "deleteAddressButton");
    com.truecaller.utils.extensions.t.b((View)localObject);
  }
  
  public final void j()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localObject = (n.a)b;
    if (localObject != null)
    {
      ((n.a)localObject).k();
      return;
    }
  }
  
  public final void k()
  {
    android.support.v4.app.j localj = getSupportFragmentManager();
    String str = "supportFragmentManager";
    c.g.b.k.a(localj, str);
    int i1 = localj.e();
    if (i1 > 0)
    {
      localj = getSupportFragmentManager();
      str = com.truecaller.profile.business.address.b.class.getName();
      localj.b(str);
    }
  }
  
  public final void l()
  {
    Object localObject = this;
    localObject = (Activity)this;
    Intent localIntent = n.a();
    com.truecaller.common.h.o.a((Activity)localObject, localIntent, 1);
  }
  
  public final void m()
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = this;
    localObject = (Context)this;
    localBuilder.<init>((Context)localObject);
    localBuilder = localBuilder.setMessage(2131886242);
    localObject = new com/truecaller/profile/business/CreateBusinessProfileActivity$x;
    ((CreateBusinessProfileActivity.x)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    localBuilder = localBuilder.setPositiveButton(2131887217, (DialogInterface.OnClickListener)localObject);
    localObject = (DialogInterface.OnClickListener)CreateBusinessProfileActivity.y.a;
    localBuilder.setNegativeButton(2131887197, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public final void n()
  {
    Object localObject = this;
    localObject = (Context)this;
    Long localLong = p;
    localObject = TagPickActivity.a((Context)localObject, localLong, 4);
    startActivityForResult((Intent)localObject, 2);
  }
  
  public final void o()
  {
    Object localObject1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).setTitle(2131886272).setMessage(2131886273);
    localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$v;
    ((CreateBusinessProfileActivity.v)localObject2).<init>(this);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton(2131887193, (DialogInterface.OnClickListener)localObject2);
    localObject2 = (DialogInterface.OnClickListener)CreateBusinessProfileActivity.w.a;
    int i1 = 2131887197;
    localObject1 = ((AlertDialog.Builder)localObject1).setNegativeButton(i1, (DialogInterface.OnClickListener)localObject2).show();
    int i2 = 16908299;
    localObject1 = (TextView)((AlertDialog)localObject1).findViewById(i2);
    if (localObject1 != null)
    {
      localObject2 = LinkMovementMethod.getInstance();
      ((TextView)localObject1).setMovementMethod((MovementMethod)localObject2);
      return;
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int i1 = -1;
    if (paramInt2 == i1)
    {
      paramInt2 = 2;
      i1 = 0;
      Long localLong = null;
      Object localObject1;
      Object localObject2;
      ag localag;
      c.d.f localf;
      Object localObject3;
      switch (paramInt1)
      {
      default: 
        break;
      case 3: 
        localObject1 = this;
        localObject1 = n.c((Context)this).toString();
        localObject2 = "ImageUtils.getCroppedImageUri(this).toString()";
        c.g.b.k.a(localObject1, (String)localObject2);
        j((String)localObject1);
        break;
      case 2: 
        long l1 = 0L;
        if (paramIntent != null)
        {
          long l2 = paramIntent.getLongExtra("tag_id", l1);
          localLong = Long.valueOf(l2);
        }
        if (localLong != null) {
          l1 = localLong.longValue();
        }
        localObject1 = com.truecaller.common.tag.d.a(l1);
        if (localObject1 != null)
        {
          localObject2 = a;
          if (localObject2 == null)
          {
            paramIntent = "presenter";
            c.g.b.k.a(paramIntent);
          }
          c.g.b.k.a(localObject1, "it");
          paramIntent = "tag";
          c.g.b.k.b(localObject1, paramIntent);
          localObject2 = (n.a)b;
          if (localObject2 != null) {
            ((n.a)localObject2).a((com.truecaller.common.tag.c)localObject1);
          }
        }
        return;
      case 1: 
        if (paramIntent != null)
        {
          localObject1 = paramIntent.getData();
          if (localObject1 != null)
          {
            paramIntent = this;
            paramIntent = (CreateBusinessProfileActivity)this;
            localag = (ag)bg.a;
            localf = g;
            if (localf == null)
            {
              localObject3 = "asyncCoroutineContext";
              c.g.b.k.a((String)localObject3);
            }
            localObject3 = new com/truecaller/profile/business/CreateBusinessProfileActivity$e;
            ((CreateBusinessProfileActivity.e)localObject3).<init>(paramIntent, (Uri)localObject1, null);
            localObject3 = (c.g.a.m)localObject3;
            kotlinx.coroutines.e.b(localag, localf, (c.g.a.m)localObject3, paramInt2);
          }
        }
        return;
      case 0: 
        if (paramIntent != null)
        {
          localObject1 = paramIntent.getData();
          if (localObject1 != null)
          {
            paramIntent = this;
            paramIntent = (CreateBusinessProfileActivity)this;
            localag = (ag)bg.a;
            localf = g;
            if (localf == null)
            {
              localObject3 = "asyncCoroutineContext";
              c.g.b.k.a((String)localObject3);
            }
            localObject3 = new com/truecaller/profile/business/CreateBusinessProfileActivity$b;
            ((CreateBusinessProfileActivity.b)localObject3).<init>(paramIntent, (Uri)localObject1, null);
            localObject3 = (c.g.a.m)localObject3;
            kotlinx.coroutines.e.b(localag, localf, (c.g.a.m)localObject3, paramInt2);
          }
        }
        return;
      }
    }
  }
  
  public final void onBackPressed()
  {
    Object localObject = getSupportFragmentManager();
    String str = "supportFragmentManager";
    c.g.b.k.a(localObject, str);
    int i1 = ((android.support.v4.app.j)localObject).e();
    if (i1 > 0)
    {
      getSupportFragmentManager().c();
      return;
    }
    localObject = a;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((o)localObject).f();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i1 = aresId;
    setTheme(i1);
    i1 = 2131558511;
    setContentView(i1);
    j.a(this).a(this);
    paramBundle = d;
    if (paramBundle == null)
    {
      localObject1 = "coreSettings";
      c.g.b.k.a((String)localObject1);
    }
    com.truecaller.featuretoggles.e.a locala = null;
    boolean bool1 = paramBundle.a("profileBusiness", false);
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "featuresRegistry";
      c.g.b.k.a((String)localObject2);
    }
    localObject1 = ((com.truecaller.featuretoggles.e)localObject1).z();
    boolean bool2 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if ((!bool2) && (!bool1))
    {
      finish();
      return;
    }
    paramBundle = getIntent();
    localObject1 = "arg_from_wizard";
    bool1 = paramBundle.getBooleanExtra((String)localObject1, false);
    if (bool1)
    {
      paramBundle = f;
      if (paramBundle == null)
      {
        localObject1 = "analytics";
        c.g.b.k.a((String)localObject1);
      }
      localObject1 = new com/truecaller/analytics/e$a;
      ((com.truecaller.analytics.e.a)localObject1).<init>("WizardAction");
      localObject3 = "BusinessProfile";
      localObject1 = ((com.truecaller.analytics.e.a)localObject1).a("Action", (String)localObject3).a();
      localObject2 = "AnalyticsEvent.Builder(W…                 .build()";
      c.g.b.k.a(localObject1, (String)localObject2);
      paramBundle.b((com.truecaller.analytics.e)localObject1);
    }
    AvailableTagsDownloadWorker.d.b();
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject1 = "presenter";
      c.g.b.k.a((String)localObject1);
    }
    paramBundle.a(this);
    int i2 = R.id.logoCardView;
    paramBundle = (FrameLayout)g(i2);
    localObject1 = new com/truecaller/profile/business/CreateBusinessProfileActivity$g;
    ((CreateBusinessProfileActivity.g)localObject1).<init>(this);
    localObject1 = (View.OnClickListener)localObject1;
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    i2 = R.id.logoButton;
    paramBundle = (Button)g(i2);
    localObject1 = new com/truecaller/profile/business/CreateBusinessProfileActivity$h;
    ((CreateBusinessProfileActivity.h)localObject1).<init>(this);
    localObject1 = (View.OnClickListener)localObject1;
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    paramBundle = new com/truecaller/profile/business/a;
    localObject1 = this;
    localObject1 = (h)this;
    paramBundle.<init>((h)localObject1);
    j = paramBundle;
    i2 = R.id.colorsRecyclerView;
    paramBundle = (RecyclerView)g(i2);
    bool2 = true;
    paramBundle.setHasFixedSize(bool2);
    i2 = R.id.colorsRecyclerView;
    paramBundle = (RecyclerView)g(i2);
    c.g.b.k.a(paramBundle, "colorsRecyclerView");
    Object localObject2 = j;
    if (localObject2 == null)
    {
      localObject3 = "colorsAdapter";
      c.g.b.k.a((String)localObject3);
    }
    localObject2 = (RecyclerView.Adapter)localObject2;
    paramBundle.setAdapter((RecyclerView.Adapter)localObject2);
    n = "#F2F5F7";
    i2 = R.id.addressEditText;
    paramBundle = (TextView)g(i2);
    localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$j;
    ((CreateBusinessProfileActivity.j)localObject2).<init>(this);
    localObject2 = (View.OnTouchListener)localObject2;
    paramBundle.setOnTouchListener((View.OnTouchListener)localObject2);
    i2 = R.id.mapViewMarker;
    paramBundle = (ImageView)g(i2);
    localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$k;
    ((CreateBusinessProfileActivity.k)localObject2).<init>(this);
    localObject2 = (View.OnClickListener)localObject2;
    paramBundle.setOnClickListener((View.OnClickListener)localObject2);
    paramBundle = new com/truecaller/profile/business/d;
    localObject2 = this;
    localObject2 = (t)this;
    paramBundle.<init>((t)localObject2);
    k = paramBundle;
    i2 = R.id.picturesRecyclerView;
    ((RecyclerView)g(i2)).setHasFixedSize(bool2);
    i2 = R.id.picturesRecyclerView;
    paramBundle = (RecyclerView)g(i2);
    c.g.b.k.a(paramBundle, "picturesRecyclerView");
    localObject2 = k;
    if (localObject2 == null)
    {
      localObject3 = "picturesAdapter";
      c.g.b.k.a((String)localObject3);
    }
    localObject2 = (RecyclerView.Adapter)localObject2;
    paramBundle.setAdapter((RecyclerView.Adapter)localObject2);
    paramBundle = k;
    if (paramBundle == null)
    {
      localObject2 = "picturesAdapter";
      c.g.b.k.a((String)localObject2);
    }
    a.add("");
    a.add("");
    a.add("");
    paramBundle.notifyDataSetChanged();
    i2 = R.id.tagsEditText;
    paramBundle = (TextView)g(i2);
    localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$n;
    ((CreateBusinessProfileActivity.n)localObject2).<init>(this);
    localObject2 = (View.OnTouchListener)localObject2;
    paramBundle.setOnTouchListener((View.OnTouchListener)localObject2);
    i2 = R.id.sizeSpinner;
    paramBundle = (Spinner)g(i2);
    c.g.b.k.a(paramBundle, "sizeSpinner");
    localObject2 = new com/truecaller/profile/business/g;
    Object localObject3 = this;
    localObject3 = (Context)this;
    ((g)localObject2).<init>((Context)localObject3);
    localObject2 = (SpinnerAdapter)localObject2;
    paramBundle.setAdapter((SpinnerAdapter)localObject2);
    i2 = R.id.sizeSpinner;
    paramBundle = (Spinner)g(i2);
    c.g.b.k.a(paramBundle, "sizeSpinner");
    localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$l;
    ((CreateBusinessProfileActivity.l)localObject2).<init>(this);
    localObject2 = (AdapterView.OnItemSelectedListener)localObject2;
    paramBundle.setOnItemSelectedListener((AdapterView.OnItemSelectedListener)localObject2);
    paramBundle = getSupportFragmentManager();
    int i5 = 2131363834;
    paramBundle = paramBundle.a(i5);
    if (paramBundle != null)
    {
      paramBundle = (OpenHoursFragment)paramBundle;
      l = paramBundle;
      i2 = R.id.phoneNumberEditText;
      paramBundle = (TextInputEditText)g(i2);
      localObject2 = d;
      if (localObject2 == null)
      {
        localObject3 = "coreSettings";
        c.g.b.k.a((String)localObject3);
      }
      localObject2 = (CharSequence)at.a((CharSequence)com.truecaller.profile.c.a((com.truecaller.common.g.a)localObject2));
      paramBundle.setText((CharSequence)localObject2);
      i2 = R.id.moreInfoButton;
      paramBundle = (Button)g(i2);
      localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$i;
      ((CreateBusinessProfileActivity.i)localObject2).<init>(this);
      localObject2 = (View.OnClickListener)localObject2;
      paramBundle.setOnClickListener((View.OnClickListener)localObject2);
      i2 = R.id.addressEditText;
      paramBundle = (TextView)g(i2);
      i5 = 2130969592;
      at.e(paramBundle, i5);
      i2 = R.id.aboutTextView;
      at.e((TextView)g(i2), i5);
      i2 = R.id.contactPersonTextView;
      at.e((TextView)g(i2), i5);
      i2 = R.id.moreInfoButton;
      paramBundle = (Button)g(i2);
      i5 = 2130969528;
      at.e(paramBundle, i5);
      i("#F2F5F7");
      i2 = R.id.continueButton;
      paramBundle = (Button)g(i2);
      localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$m;
      ((CreateBusinessProfileActivity.m)localObject2).<init>(this);
      localObject2 = (View.OnClickListener)localObject2;
      paramBundle.setOnClickListener((View.OnClickListener)localObject2);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject2 = "presenter";
        c.g.b.k.a((String)localObject2);
      }
      localObject2 = getIntent();
      boolean bool5 = ((Intent)localObject2).getBooleanExtra("arg_editing", false);
      localObject3 = getIntent();
      boolean bool6 = ((Intent)localObject3).getBooleanExtra("arg_from_wizard", false);
      Intent localIntent = getIntent();
      Object localObject4 = "arg_migrating";
      boolean bool7 = localIntent.getBooleanExtra((String)localObject4, false);
      e = bool5;
      if ((bool5) || (bool7))
      {
        localObject2 = j.a();
        localObject4 = (n.a)b;
        if (localObject4 != null) {
          ((n.a)localObject4).a((Profile)localObject2);
        }
        localObject4 = ((Profile)localObject2).getBusinessData();
        int i8 = 0;
        Object localObject5 = null;
        if (localObject4 != null) {
          localObject4 = ((BusinessData)localObject4).getAvatarUrl();
        } else {
          localObject4 = null;
        }
        Object localObject6 = localObject4;
        localObject6 = (CharSequence)localObject4;
        if (localObject6 != null)
        {
          i9 = ((CharSequence)localObject6).length();
          if (i9 != 0)
          {
            i9 = 0;
            localObject6 = null;
            break label1201;
          }
        }
        int i9 = 1;
        label1201:
        if (i9 == 0)
        {
          localObject6 = (n.a)b;
          if (localObject6 != null) {
            ((n.a)localObject6).b((String)localObject4);
          }
        }
        localObject4 = ((Profile)localObject2).getBusinessData();
        if (localObject4 != null)
        {
          localObject4 = ((BusinessData)localObject4).getCompany();
          if (localObject4 != null)
          {
            localObject4 = ((Company)localObject4).getBranding();
            if (localObject4 != null)
            {
              localObject4 = ((Branding)localObject4).getBackgroundColor();
              if (localObject4 != null)
              {
                localObject6 = (n.a)b;
                if (localObject6 != null) {
                  ((n.a)localObject6).c((String)localObject4);
                }
              }
            }
          }
        }
        localObject4 = (n.a)b;
        if (localObject4 != null)
        {
          localObject6 = ((Profile)localObject2).getBusinessData();
          if (localObject6 != null)
          {
            localObject6 = ((BusinessData)localObject6).getCompany();
            if (localObject6 != null) {
              localObject5 = ((Company)localObject6).getOpenHours();
            }
          }
          if (localObject5 == null) {
            localObject5 = (List)y.a;
          }
          ((n.a)localObject4).a((List)localObject5);
        }
        localObject4 = ((Profile)localObject2).getBusinessData();
        Object localObject7;
        if (localObject4 != null)
        {
          localObject4 = ((BusinessData)localObject4).getCompany();
          if (localObject4 != null)
          {
            localObject4 = ((Company)localObject4).getBranding();
            if (localObject4 != null)
            {
              localObject4 = ((Branding)localObject4).getImageUrls();
              if (localObject4 != null)
              {
                localObject4 = ((Iterable)localObject4).iterator();
                i8 = 0;
                localObject5 = null;
                for (;;)
                {
                  boolean bool9 = ((Iterator)localObject4).hasNext();
                  if (!bool9) {
                    break;
                  }
                  localObject6 = ((Iterator)localObject4).next();
                  int i10 = i8 + 1;
                  if (i8 < 0) {
                    c.a.m.a();
                  }
                  localObject6 = (String)localObject6;
                  localObject7 = (n.a)b;
                  if (localObject7 != null) {
                    ((n.a)localObject7).a(i8, (String)localObject6);
                  }
                  i8 = i10;
                }
              }
            }
          }
        }
        localObject4 = ((Profile)localObject2).getBusinessData();
        if (localObject4 != null)
        {
          localObject4 = ((BusinessData)localObject4).getTags();
          if (localObject4 != null) {}
        }
        else
        {
          localObject4 = (List)y.a;
        }
        localObject5 = localObject4;
        localObject5 = (Collection)localObject4;
        boolean bool8 = ((Collection)localObject5).isEmpty() ^ bool2;
        if (bool8)
        {
          localObject4 = (Long)c.a.m.e((List)localObject4);
          if (localObject4 != null)
          {
            long l1 = ((Number)localObject4).longValue();
            localObject4 = com.truecaller.common.tag.d.a(l1);
            if (localObject4 != null)
            {
              localObject5 = (n.a)b;
              if (localObject5 != null)
              {
                localObject6 = "it";
                c.g.b.k.a(localObject4, (String)localObject6);
                ((n.a)localObject5).a((com.truecaller.common.tag.c)localObject4);
              }
            }
          }
        }
        localObject2 = ((Profile)localObject2).getBusinessData();
        if (localObject2 != null)
        {
          localObject2 = ((BusinessData)localObject2).getCompany();
          if (localObject2 != null)
          {
            localObject2 = ((Company)localObject2).getAddress();
            if (localObject2 != null)
            {
              BusinessAddressInput localBusinessAddressInput = new com/truecaller/profile/business/address/BusinessAddressInput;
              localObject4 = ((Address)localObject2).getStreet();
              if (localObject4 == null) {
                localObject4 = "";
              }
              localObject5 = localObject4;
              localObject6 = ((Address)localObject2).getZipCode();
              localObject4 = ((Address)localObject2).getCity();
              if (localObject4 == null) {
                localObject4 = "";
              }
              Object localObject8 = localObject4;
              localObject4 = ((Address)localObject2).getCountry();
              if (localObject4 == null) {
                localObject4 = "";
              }
              localObject7 = localObject4;
              Double localDouble1 = ((Address)localObject2).getLatitude();
              Double localDouble2 = ((Address)localObject2).getLongitude();
              localObject4 = localBusinessAddressInput;
              localBusinessAddressInput.<init>((String)localObject5, (String)localObject6, (String)localObject8, (String)localObject7, localDouble1, localDouble2);
              paramBundle.a(localBusinessAddressInput);
            }
          }
        }
      }
      int i6 = 2;
      int i3;
      if (!bool7) {
        if (bool6)
        {
          bool2 = false;
          localObject1 = null;
        }
        else
        {
          i3 = 2;
        }
      }
      i = i3;
      localObject1 = k;
      boolean bool3 = ((ac)localObject1).a();
      if (!bool3)
      {
        localObject1 = (n.a)b;
        if (localObject1 != null) {
          ((n.a)localObject1).d();
        }
      }
      int i4 = i;
      if (i4 == i6)
      {
        localObject1 = l;
        locala = t;
        localObject2 = com.truecaller.featuretoggles.e.a;
        int i7 = 66;
        localObject2 = localObject2[i7];
        localObject1 = locala.a((com.truecaller.featuretoggles.e)localObject1, (c.l.g)localObject2);
        boolean bool4 = ((com.truecaller.featuretoggles.b)localObject1).a();
        if (bool4)
        {
          paramBundle = (n.a)b;
          if (paramBundle != null) {
            paramBundle.t();
          }
        }
      }
      i2 = R.id.backButton;
      paramBundle = (TintedImageView)g(i2);
      localObject1 = new com/truecaller/profile/business/CreateBusinessProfileActivity$c;
      ((CreateBusinessProfileActivity.c)localObject1).<init>(this);
      localObject1 = (View.OnClickListener)localObject1;
      paramBundle.setOnClickListener((View.OnClickListener)localObject1);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.profile.business.openHours.OpenHoursFragment");
    throw paramBundle;
  }
  
  public final void onDestroy()
  {
    o localo = a;
    if (localo == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localo.y_();
    super.onDestroy();
  }
  
  public final void p()
  {
    com.truecaller.ui.dialogs.k localk = com.truecaller.ui.dialogs.k.c(2131886255);
    Object localObject = this;
    localObject = (android.support.v4.app.f)this;
    com.truecaller.ui.dialogs.a.a(localk, (android.support.v4.app.f)localObject);
    q = localk;
  }
  
  public final void q()
  {
    v();
    Object localObject = this;
    Toast.makeText((Context)this, 2131886266, 0).show();
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>();
    setResult(-1, (Intent)localObject);
    finish();
  }
  
  public final void r()
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = this;
    localObject = (Context)this;
    localBuilder.<init>((Context)localObject);
    localBuilder = localBuilder.setTitle(2131886223).setMessage(2131886221);
    localObject = new com/truecaller/profile/business/CreateBusinessProfileActivity$o;
    ((CreateBusinessProfileActivity.o)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    localBuilder = localBuilder.setPositiveButton(2131886222, (DialogInterface.OnClickListener)localObject);
    localObject = (DialogInterface.OnClickListener)CreateBusinessProfileActivity.p.a;
    localBuilder.setNegativeButton(2131887197, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public final void s()
  {
    finish();
  }
  
  public final void t()
  {
    int i1 = R.id.toolbar;
    Object localObject1 = (Toolbar)g(i1);
    c.g.b.k.a(localObject1, "toolbar");
    localObject1 = ((Toolbar)localObject1).getMenu().add(2131886234);
    Object localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$q;
    ((CreateBusinessProfileActivity.q)localObject2).<init>(this);
    localObject2 = (MenuItem.OnMenuItemClickListener)localObject2;
    ((MenuItem)localObject1).setOnMenuItemClickListener((MenuItem.OnMenuItemClickListener)localObject2);
  }
  
  public final void u()
  {
    Object localObject = this;
    localObject = com.truecaller.profile.a.c((Context)this);
    startActivity((Intent)localObject);
    finish();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */