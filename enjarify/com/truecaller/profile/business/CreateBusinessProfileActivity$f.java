package com.truecaller.profile.business;

import android.content.Context;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.common.h.n;
import com.truecaller.messaging.data.types.ImageEntity;
import java.io.IOException;
import kotlinx.coroutines.ag;

final class CreateBusinessProfileActivity$f
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag e;
  
  CreateBusinessProfileActivity$f(CreateBusinessProfileActivity paramCreateBusinessProfileActivity, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/profile/business/CreateBusinessProfileActivity$f;
    CreateBusinessProfileActivity localCreateBusinessProfileActivity = c;
    String str = d;
    localf.<init>(localCreateBusinessProfileActivity, str, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    boolean bool1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label376;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        try
        {
          paramObject = (o.b)paramObject;
          paramObject = a;
          throw ((Throwable)paramObject);
        }
        catch (IOException paramObject) {}
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label380;
      }
      paramObject = c;
      paramObject = ((CreateBusinessProfileActivity)paramObject).b();
      localObject1 = d;
      localObject1 = Uri.parse((String)localObject1);
      localObject2 = "Uri.parse(logo)";
      c.g.b.k.a(localObject1, (String)localObject2);
      int k = 800;
      paramObject = ((com.truecaller.util.g)paramObject).a((Uri)localObject1, k);
      if (paramObject != null)
      {
        paramObject = b;
        if (paramObject != null)
        {
          paramObject = ((Uri)paramObject).toString();
          break label183;
        }
      }
      m = 0;
      paramObject = null;
      label183:
      localObject1 = paramObject;
      localObject1 = (CharSequence)paramObject;
      k = 1;
      if (localObject1 != null)
      {
        j = ((CharSequence)localObject1).length();
        if (j != 0)
        {
          j = 0;
          localObject1 = null;
          break label225;
        }
      }
      int j = 1;
      label225:
      if (j != 0)
      {
        m = 0;
        paramObject = null;
      }
      if (paramObject != null)
      {
        localObject1 = c;
        localObject1 = ((CreateBusinessProfileActivity)localObject1).c();
        Object localObject3 = new com/truecaller/profile/business/CreateBusinessProfileActivity$f$a;
        ((CreateBusinessProfileActivity.f.a)localObject3).<init>((String)paramObject, null, this);
        localObject3 = (m)localObject3;
        a = paramObject;
        b = k;
        paramObject = kotlinx.coroutines.g.a((f)localObject1, (m)localObject3, this);
        if (paramObject == locala) {
          return locala;
        }
      }
      break;
    }
    paramObject = c;
    paramObject = (Context)paramObject;
    n.f((Context)paramObject);
    break label376;
    Object localObject1 = c.c();
    Object localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$f$1;
    ((CreateBusinessProfileActivity.f.1)localObject2).<init>(this, null);
    localObject2 = (m)localObject2;
    a = paramObject;
    int m = 2;
    b = m;
    paramObject = kotlinx.coroutines.g.a((f)localObject1, (m)localObject2, this);
    if (paramObject == locala) {
      return locala;
    }
    label376:
    return x.a;
    label380:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */