package com.truecaller.profile.business;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.truecaller.R.id;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class d$a
  extends RecyclerView.ViewHolder
  implements View.OnClickListener, a
{
  private final View b;
  private HashMap c;
  
  public d$a(d paramd, View paramView)
  {
    super(paramView);
    b = paramView;
    int i = R.id.rootView;
    paramd = (CardView)a(i);
    paramView = this;
    paramView = (View.OnClickListener)this;
    paramd.setOnClickListener(paramView);
  }
  
  public final View a()
  {
    return b;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void onClick(View paramView)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    paramView = a;
    int i = getAdapterPosition();
    paramView = (CharSequence)d.a(paramView, i);
    int j = paramView.length();
    if (j == 0)
    {
      j = 1;
    }
    else
    {
      j = 0;
      paramView = null;
    }
    if (j != 0)
    {
      paramView = d.a(a);
      i = getAdapterPosition();
      paramView.a(i);
      return;
    }
    paramView = d.a(a);
    localObject = a;
    int k = getAdapterPosition();
    localObject = d.a((d)localObject, k);
    paramView.e((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */