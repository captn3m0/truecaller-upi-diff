package com.truecaller.profile.business;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.utils.ui.b;
import java.util.List;

public final class g
  extends ArrayAdapter
{
  public g(Context paramContext)
  {
    super(paramContext, 17367049, (List)localObject1);
  }
  
  private final int a(int paramInt)
  {
    if (paramInt != 0) {
      return b.a(getContext(), 2130969591);
    }
    return b.a(getContext(), 2130969590);
  }
  
  public final View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    k.b(paramViewGroup, "parent");
    paramView = super.getView(paramInt, paramView, paramViewGroup);
    paramViewGroup = (TextView)paramView.findViewById(16908308);
    Object localObject1 = paramViewGroup.getContext();
    Object localObject2 = getItem(paramInt);
    k.a(localObject2, "getItem(position)");
    int i = ((Number)localObject2).intValue();
    localObject1 = (CharSequence)((Context)localObject1).getString(i);
    paramViewGroup.setText((CharSequence)localObject1);
    paramInt = a(paramInt);
    paramViewGroup.setTextColor(paramInt);
    k.a(paramView, "rootView");
    return paramView;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    k.b(paramViewGroup, "parent");
    paramView = super.getView(paramInt, paramView, paramViewGroup);
    paramViewGroup = (TextView)paramView.findViewById(16908308);
    Object localObject1 = paramViewGroup.getContext();
    Object localObject2 = getItem(paramInt);
    k.a(localObject2, "getItem(position)");
    int i = ((Number)localObject2).intValue();
    localObject1 = (CharSequence)((Context)localObject1).getString(i);
    paramViewGroup.setText((CharSequence)localObject1);
    localObject1 = paramViewGroup.getContext();
    k.a(localObject1, "context");
    float f = ((Context)localObject1).getResources().getDimension(2131165358);
    paramViewGroup.setTextSize(0, f);
    paramInt = a(paramInt);
    paramViewGroup.setTextColor(paramInt);
    k.a(paramView, "rootView");
    paramInt = paramView.getPaddingTop();
    int j = paramView.getPaddingRight();
    int k = paramView.getPaddingBottom();
    paramView.setPaddingRelative(0, paramInt, j, k);
    return paramView;
  }
  
  public final boolean isEnabled(int paramInt)
  {
    return paramInt != 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */