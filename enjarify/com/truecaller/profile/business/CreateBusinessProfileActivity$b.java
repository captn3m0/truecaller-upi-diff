package com.truecaller.profile.business;

import android.net.Uri;
import c.d.c;
import c.g.a.m;
import c.x;
import kotlinx.coroutines.ag;

final class CreateBusinessProfileActivity$b
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  Object f;
  Object g;
  int h;
  private ag k;
  
  CreateBusinessProfileActivity$b(CreateBusinessProfileActivity paramCreateBusinessProfileActivity, Uri paramUri, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/profile/business/CreateBusinessProfileActivity$b;
    CreateBusinessProfileActivity localCreateBusinessProfileActivity = i;
    Uri localUri = j;
    localb.<init>(localCreateBusinessProfileActivity, localUri, paramc);
    paramObject = (ag)paramObject;
    k = ((ag)paramObject);
    return localb;
  }
  
  /* Error */
  public final Object a(Object paramObject)
  {
    // Byte code:
    //   0: getstatic 50	c/d/a/a:a	Lc/d/a/a;
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 52	com/truecaller/profile/business/CreateBusinessProfileActivity$b:h	I
    //   8: istore_3
    //   9: iload_3
    //   10: tableswitch	default:+26->36, 0:+126->136, 1:+60->70, 2:+38->48
    //   36: new 54	java/lang/IllegalStateException
    //   39: astore_1
    //   40: aload_1
    //   41: ldc 56
    //   43: invokespecial 59	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   46: aload_1
    //   47: athrow
    //   48: aload_1
    //   49: instanceof 61
    //   52: istore 4
    //   54: iload 4
    //   56: ifne +6 -> 62
    //   59: goto +449 -> 508
    //   62: aload_1
    //   63: checkcast 61	c/o$b
    //   66: getfield 64	c/o$b:a	Ljava/lang/Throwable;
    //   69: athrow
    //   70: aload_0
    //   71: getfield 66	com/truecaller/profile/business/CreateBusinessProfileActivity$b:f	Ljava/lang/Object;
    //   74: checkcast 68	java/lang/Throwable
    //   77: astore 5
    //   79: aload_0
    //   80: getfield 70	com/truecaller/profile/business/CreateBusinessProfileActivity$b:e	Ljava/lang/Object;
    //   83: checkcast 72	java/io/Closeable
    //   86: astore 6
    //   88: aload_0
    //   89: getfield 74	com/truecaller/profile/business/CreateBusinessProfileActivity$b:c	Ljava/lang/Object;
    //   92: checkcast 68	java/lang/Throwable
    //   95: astore 7
    //   97: aload_0
    //   98: getfield 76	com/truecaller/profile/business/CreateBusinessProfileActivity$b:b	Ljava/lang/Object;
    //   101: checkcast 72	java/io/Closeable
    //   104: astore 8
    //   106: aload_1
    //   107: instanceof 61
    //   110: istore 9
    //   112: iload 9
    //   114: ifne +6 -> 120
    //   117: goto +257 -> 374
    //   120: aload_1
    //   121: checkcast 61	c/o$b
    //   124: astore_1
    //   125: aload_1
    //   126: getfield 64	c/o$b:a	Ljava/lang/Throwable;
    //   129: astore_1
    //   130: aload_1
    //   131: athrow
    //   132: astore_1
    //   133: goto +259 -> 392
    //   136: aload_1
    //   137: instanceof 61
    //   140: istore_3
    //   141: iload_3
    //   142: ifne +370 -> 512
    //   145: aload_0
    //   146: getfield 24	com/truecaller/profile/business/CreateBusinessProfileActivity$b:i	Lcom/truecaller/profile/business/CreateBusinessProfileActivity;
    //   149: astore_1
    //   150: aload_1
    //   151: checkcast 78	android/content/Context
    //   154: astore_1
    //   155: aload_1
    //   156: invokestatic 83	com/truecaller/common/h/n:b	(Landroid/content/Context;)Landroid/net/Uri;
    //   159: astore_1
    //   160: aload_0
    //   161: getfield 24	com/truecaller/profile/business/CreateBusinessProfileActivity$b:i	Lcom/truecaller/profile/business/CreateBusinessProfileActivity;
    //   164: astore 5
    //   166: aload 5
    //   168: invokevirtual 89	com/truecaller/profile/business/CreateBusinessProfileActivity:getContentResolver	()Landroid/content/ContentResolver;
    //   171: astore 5
    //   173: aload_0
    //   174: getfield 26	com/truecaller/profile/business/CreateBusinessProfileActivity$b:j	Landroid/net/Uri;
    //   177: astore 6
    //   179: aload 5
    //   181: aload 6
    //   183: invokevirtual 95	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   186: astore 5
    //   188: aload 5
    //   190: ifnull +318 -> 508
    //   193: aload 5
    //   195: astore 8
    //   197: aload 5
    //   199: checkcast 72	java/io/Closeable
    //   202: astore 8
    //   204: aload 8
    //   206: astore 5
    //   208: aload 8
    //   210: checkcast 97	java/io/InputStream
    //   213: astore 5
    //   215: aload_0
    //   216: getfield 24	com/truecaller/profile/business/CreateBusinessProfileActivity$b:i	Lcom/truecaller/profile/business/CreateBusinessProfileActivity;
    //   219: astore 6
    //   221: aload 6
    //   223: invokevirtual 89	com/truecaller/profile/business/CreateBusinessProfileActivity:getContentResolver	()Landroid/content/ContentResolver;
    //   226: astore 6
    //   228: aload 6
    //   230: aload_1
    //   231: invokevirtual 101	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
    //   234: astore 6
    //   236: aload 6
    //   238: ifnull +178 -> 416
    //   241: aload 6
    //   243: checkcast 72	java/io/Closeable
    //   246: astore 6
    //   248: aload 6
    //   250: astore 7
    //   252: aload 6
    //   254: checkcast 103	java/io/OutputStream
    //   257: astore 7
    //   259: aload 5
    //   261: aload 7
    //   263: invokestatic 108	c/f/a:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   266: pop2
    //   267: aload_0
    //   268: getfield 24	com/truecaller/profile/business/CreateBusinessProfileActivity$b:i	Lcom/truecaller/profile/business/CreateBusinessProfileActivity;
    //   271: astore 10
    //   273: aload 10
    //   275: invokevirtual 111	com/truecaller/profile/business/CreateBusinessProfileActivity:c	()Lc/d/f;
    //   278: astore 10
    //   280: new 113	com/truecaller/profile/business/CreateBusinessProfileActivity$b$a
    //   283: astore 11
    //   285: aload 11
    //   287: aconst_null
    //   288: aload 5
    //   290: aload_0
    //   291: aload_1
    //   292: invokespecial 116	com/truecaller/profile/business/CreateBusinessProfileActivity$b$a:<init>	(Lc/d/c;Ljava/io/InputStream;Lcom/truecaller/profile/business/CreateBusinessProfileActivity$b;Landroid/net/Uri;)V
    //   295: aload 11
    //   297: checkcast 6	c/g/a/m
    //   300: astore 11
    //   302: aload_0
    //   303: aload_1
    //   304: putfield 118	com/truecaller/profile/business/CreateBusinessProfileActivity$b:a	Ljava/lang/Object;
    //   307: aload_0
    //   308: aload 8
    //   310: putfield 76	com/truecaller/profile/business/CreateBusinessProfileActivity$b:b	Ljava/lang/Object;
    //   313: aload_0
    //   314: aconst_null
    //   315: putfield 74	com/truecaller/profile/business/CreateBusinessProfileActivity$b:c	Ljava/lang/Object;
    //   318: aload_0
    //   319: aload 5
    //   321: putfield 120	com/truecaller/profile/business/CreateBusinessProfileActivity$b:d	Ljava/lang/Object;
    //   324: aload_0
    //   325: aload 6
    //   327: putfield 70	com/truecaller/profile/business/CreateBusinessProfileActivity$b:e	Ljava/lang/Object;
    //   330: aload_0
    //   331: aconst_null
    //   332: putfield 66	com/truecaller/profile/business/CreateBusinessProfileActivity$b:f	Ljava/lang/Object;
    //   335: aload_0
    //   336: aload 7
    //   338: putfield 122	com/truecaller/profile/business/CreateBusinessProfileActivity$b:g	Ljava/lang/Object;
    //   341: iconst_1
    //   342: istore 12
    //   344: aload_0
    //   345: iload 12
    //   347: putfield 52	com/truecaller/profile/business/CreateBusinessProfileActivity$b:h	I
    //   350: aload 10
    //   352: aload 11
    //   354: aload_0
    //   355: invokestatic 128	kotlinx/coroutines/g:a	(Lc/d/f;Lc/g/a/m;Lc/d/c;)Ljava/lang/Object;
    //   358: astore_1
    //   359: aload_1
    //   360: aload_2
    //   361: if_acmpne +5 -> 366
    //   364: aload_2
    //   365: areturn
    //   366: iconst_0
    //   367: istore_3
    //   368: aconst_null
    //   369: astore 5
    //   371: aconst_null
    //   372: astore 7
    //   374: getstatic 133	c/x:a	Lc/x;
    //   377: astore_1
    //   378: aload 6
    //   380: aload 5
    //   382: invokestatic 138	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   385: goto +34 -> 419
    //   388: astore_1
    //   389: aconst_null
    //   390: astore 7
    //   392: aload_1
    //   393: athrow
    //   394: astore 13
    //   396: aload_1
    //   397: astore 5
    //   399: aload 13
    //   401: astore_1
    //   402: aload 6
    //   404: aload 5
    //   406: invokestatic 138	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   409: aload 13
    //   411: athrow
    //   412: astore_1
    //   413: goto +16 -> 429
    //   416: aconst_null
    //   417: astore 7
    //   419: aload 8
    //   421: aload 7
    //   423: invokestatic 138	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   426: goto +82 -> 508
    //   429: aload_1
    //   430: athrow
    //   431: astore 5
    //   433: aload_1
    //   434: astore 7
    //   436: aload 5
    //   438: astore_1
    //   439: aload 8
    //   441: aload 7
    //   443: invokestatic 138	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   446: aload 5
    //   448: athrow
    //   449: astore_1
    //   450: aload_0
    //   451: getfield 24	com/truecaller/profile/business/CreateBusinessProfileActivity$b:i	Lcom/truecaller/profile/business/CreateBusinessProfileActivity;
    //   454: invokevirtual 111	com/truecaller/profile/business/CreateBusinessProfileActivity:c	()Lc/d/f;
    //   457: astore 5
    //   459: new 140	com/truecaller/profile/business/CreateBusinessProfileActivity$b$1
    //   462: astore 6
    //   464: aload 6
    //   466: aload_0
    //   467: aconst_null
    //   468: invokespecial 143	com/truecaller/profile/business/CreateBusinessProfileActivity$b$1:<init>	(Lcom/truecaller/profile/business/CreateBusinessProfileActivity$b;Lc/d/c;)V
    //   471: aload 6
    //   473: checkcast 6	c/g/a/m
    //   476: astore 6
    //   478: aload_0
    //   479: aload_1
    //   480: putfield 118	com/truecaller/profile/business/CreateBusinessProfileActivity$b:a	Ljava/lang/Object;
    //   483: iconst_2
    //   484: istore 12
    //   486: aload_0
    //   487: iload 12
    //   489: putfield 52	com/truecaller/profile/business/CreateBusinessProfileActivity$b:h	I
    //   492: aload 5
    //   494: aload 6
    //   496: aload_0
    //   497: invokestatic 128	kotlinx/coroutines/g:a	(Lc/d/f;Lc/g/a/m;Lc/d/c;)Ljava/lang/Object;
    //   500: astore_1
    //   501: aload_1
    //   502: aload_2
    //   503: if_acmpne +5 -> 508
    //   506: aload_2
    //   507: areturn
    //   508: getstatic 133	c/x:a	Lc/x;
    //   511: areturn
    //   512: aload_1
    //   513: checkcast 61	c/o$b
    //   516: getfield 64	c/o$b:a	Ljava/lang/Throwable;
    //   519: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	520	0	this	b
    //   0	520	1	paramObject	Object
    //   3	504	2	locala	c.d.a.a
    //   8	2	3	m	int
    //   140	228	3	bool1	boolean
    //   52	3	4	bool2	boolean
    //   77	328	5	localObject1	Object
    //   431	16	5	localObject2	Object
    //   457	36	5	localf	c.d.f
    //   86	409	6	localObject3	Object
    //   95	347	7	localObject4	Object
    //   104	336	8	localObject5	Object
    //   110	3	9	bool3	boolean
    //   271	80	10	localObject6	Object
    //   283	70	11	localObject7	Object
    //   342	146	12	n	int
    //   394	16	13	localObject8	Object
    // Exception table:
    //   from	to	target	type
    //   120	124	132	finally
    //   125	129	132	finally
    //   130	132	132	finally
    //   374	377	132	finally
    //   252	257	388	finally
    //   261	267	388	finally
    //   267	271	388	finally
    //   273	278	388	finally
    //   280	283	388	finally
    //   291	295	388	finally
    //   295	300	388	finally
    //   303	307	388	finally
    //   308	313	388	finally
    //   314	318	388	finally
    //   319	324	388	finally
    //   325	330	388	finally
    //   331	335	388	finally
    //   336	341	388	finally
    //   345	350	388	finally
    //   354	358	388	finally
    //   392	394	394	finally
    //   208	213	412	finally
    //   215	219	412	finally
    //   221	226	412	finally
    //   230	234	412	finally
    //   241	246	412	finally
    //   380	385	412	finally
    //   404	409	412	finally
    //   409	412	412	finally
    //   429	431	431	finally
    //   145	149	449	java/io/IOException
    //   150	154	449	java/io/IOException
    //   155	159	449	java/io/IOException
    //   160	164	449	java/io/IOException
    //   166	171	449	java/io/IOException
    //   173	177	449	java/io/IOException
    //   181	186	449	java/io/IOException
    //   197	202	449	java/io/IOException
    //   421	426	449	java/io/IOException
    //   441	446	449	java/io/IOException
    //   446	449	449	java/io/IOException
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */