package com.truecaller.profile.business;

import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.profile.data.dto.Branding;
import com.truecaller.profile.data.dto.BusinessData;
import com.truecaller.profile.data.dto.Company;
import com.truecaller.profile.data.dto.OnlineIds;

public final class r
  implements e
{
  private final b b;
  
  public r(b paramb)
  {
    b = paramb;
  }
  
  public final void a(int paramInt, BusinessData paramBusinessData)
  {
    k.b(paramBusinessData, "data");
    b localb = b;
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("BusinessProfileSaved");
    Object localObject2 = f.a(paramInt);
    localObject2 = ((e.a)localObject1).a("Action", (String)localObject2);
    boolean bool1 = f.a((CharSequence)paramBusinessData.getAvatarUrl());
    localObject2 = ((e.a)localObject2).a("Logo", bool1);
    String str = paramBusinessData.getCompany().getBranding().getBackgroundColor();
    localObject2 = ((e.a)localObject2).a("Color", str);
    localObject1 = "Size";
    str = paramBusinessData.getCompany().getSize();
    if (str == null) {
      str = "";
    }
    localObject2 = ((e.a)localObject2).a((String)localObject1, str);
    bool1 = f.a((CharSequence)paramBusinessData.getOnlineIds().getUrl());
    localObject2 = ((e.a)localObject2).a("HasWebsite", bool1);
    bool1 = f.a((CharSequence)paramBusinessData.getOnlineIds().getFacebookId());
    localObject2 = ((e.a)localObject2).a("HasFacebook", bool1);
    bool1 = f.a((CharSequence)paramBusinessData.getOnlineIds().getTwitterId());
    localObject2 = ((e.a)localObject2).a("HasTwitter", bool1);
    boolean bool2 = f.a((CharSequence)paramBusinessData.getJobTitle());
    localObject2 = ((e.a)localObject2).a("HasContactPersonsDesignation", bool2).a();
    k.a(localObject2, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.b((com.truecaller.analytics.e)localObject2);
  }
  
  public final void a(int paramInt, String paramString)
  {
    k.b(paramString, "cause");
    b localb = b;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("BusinessProfileSaveFailed");
    Object localObject = f.a(paramInt);
    localObject = locala.a("Action", (String)localObject).a("Cause", paramString).a();
    k.a(localObject, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.b((com.truecaller.analytics.e)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */