package com.truecaller.profile.business;

import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.messaging.data.types.ImageEntity;
import kotlinx.coroutines.ag;

final class CreateBusinessProfileActivity$e
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag e;
  
  CreateBusinessProfileActivity$e(CreateBusinessProfileActivity paramCreateBusinessProfileActivity, Uri paramUri, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/profile/business/CreateBusinessProfileActivity$e;
    CreateBusinessProfileActivity localCreateBusinessProfileActivity = c;
    Uri localUri = d;
    locale.<init>(localCreateBusinessProfileActivity, localUri, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label246;
      }
      paramObject = c.b();
      Uri localUri = d;
      paramObject = ((com.truecaller.util.g)paramObject).a(localUri);
      bool1 = false;
      localUri = null;
      if (paramObject != null)
      {
        paramObject = b;
        if (paramObject != null)
        {
          paramObject = ((Uri)paramObject).toString();
          break label126;
        }
      }
      paramObject = null;
      label126:
      Object localObject1 = paramObject;
      localObject1 = (CharSequence)paramObject;
      int j = 1;
      if (localObject1 != null)
      {
        k = ((CharSequence)localObject1).length();
        if (k != 0)
        {
          k = 0;
          localObject1 = null;
          break label172;
        }
      }
      int k = 1;
      label172:
      if (k != 0) {
        paramObject = null;
      }
      if (paramObject != null)
      {
        localObject1 = c.c();
        Object localObject2 = new com/truecaller/profile/business/CreateBusinessProfileActivity$e$a;
        ((CreateBusinessProfileActivity.e.a)localObject2).<init>((String)paramObject, null, this);
        localObject2 = (m)localObject2;
        a = paramObject;
        b = j;
        paramObject = kotlinx.coroutines.g.a((f)localObject1, (m)localObject2, this);
        if (paramObject == locala) {
          return locala;
        }
      }
      break;
    }
    return x.a;
    label246:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */