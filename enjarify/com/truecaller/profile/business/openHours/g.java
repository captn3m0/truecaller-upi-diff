package com.truecaller.profile.business.openHours;

import com.truecaller.profile.data.dto.OpenHours;
import java.util.List;
import java.util.SortedSet;

public abstract interface g
{
  public abstract OpenHours a();
  
  public abstract OpenHours a(int paramInt);
  
  public abstract OpenHours a(int paramInt, b paramb);
  
  public abstract OpenHours a(int paramInt, SortedSet paramSortedSet);
  
  public abstract List a(List paramList);
  
  public abstract OpenHours b(int paramInt, b paramb);
  
  public abstract void b(int paramInt);
  
  public abstract boolean b();
  
  public abstract List c();
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */