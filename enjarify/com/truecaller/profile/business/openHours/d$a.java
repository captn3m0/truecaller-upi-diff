package com.truecaller.profile.business.openHours;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class d$a
  extends RecyclerView.ViewHolder
  implements View.OnClickListener, View.OnTouchListener, a
{
  private final View b;
  private HashMap c;
  
  public d$a(d paramd, View paramView)
  {
    super(paramView);
    b = paramView;
    int i = R.id.openDaysEditText;
    paramd = (TextView)a(i);
    paramView = this;
    paramView = (View.OnTouchListener)this;
    paramd.setOnTouchListener(paramView);
    i = R.id.opensAtEditText;
    ((TextView)a(i)).setOnTouchListener(paramView);
    i = R.id.closesAtEditText;
    ((TextView)a(i)).setOnTouchListener(paramView);
    i = R.id.removeButton;
    paramd = (ImageButton)a(i);
    paramView = this;
    paramView = (View.OnClickListener)this;
    paramd.setOnClickListener(paramView);
  }
  
  public final View a()
  {
    return b;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "view");
    int i = R.id.removeButton;
    ImageButton localImageButton = (ImageButton)a(i);
    boolean bool = k.a(paramView, localImageButton);
    if (bool)
    {
      paramView = d.a(a);
      i = getAdapterPosition();
      paramView.d(i);
    }
  }
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    k.b(paramView, "view");
    String str = "event";
    k.b(paramMotionEvent, str);
    int i = paramMotionEvent.getAction();
    int m = 1;
    if (i == m)
    {
      i = R.id.openDaysEditText;
      paramMotionEvent = (TextView)a(i);
      boolean bool1 = k.a(paramView, paramMotionEvent);
      int j;
      if (bool1)
      {
        paramView = d.a(a);
        j = getAdapterPosition();
        paramView.a(j);
      }
      else
      {
        j = R.id.opensAtEditText;
        paramMotionEvent = (TextView)a(j);
        boolean bool2 = k.a(paramView, paramMotionEvent);
        int k;
        if (bool2)
        {
          paramView = d.a(a);
          k = getAdapterPosition();
          paramView.b(k);
        }
        else
        {
          k = R.id.closesAtEditText;
          paramMotionEvent = (TextView)a(k);
          boolean bool3 = k.a(paramView, paramMotionEvent);
          if (bool3)
          {
            paramView = d.a(a);
            k = getAdapterPosition();
            paramView.c(k);
          }
        }
      }
    }
    return m;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */