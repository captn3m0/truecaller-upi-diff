package com.truecaller.profile.business.openHours;

import c.g.b.k;
import com.truecaller.log.AssertionUtil;
import com.truecaller.profile.data.dto.OpenHours;
import java.text.DateFormatSymbols;
import java.util.List;
import java.util.Locale;

public final class a
{
  public static final b a(OpenHours paramOpenHours)
  {
    String str = "receiver$0";
    k.b(paramOpenHours, str);
    paramOpenHours = paramOpenHours.getOpens();
    if (paramOpenHours != null) {
      return a(paramOpenHours);
    }
    return null;
  }
  
  private static final b a(String paramString)
  {
    paramString = (CharSequence)paramString;
    Object localObject1 = { ":" };
    int i = 0;
    b localb = null;
    paramString = c.n.m.c(paramString, (String[])localObject1, false, 6);
    int j = paramString.size();
    int k = 2;
    if (j != k) {
      return null;
    }
    try
    {
      localObject1 = c.a.m.d(paramString);
      localObject1 = (CharSequence)localObject1;
      j = ((CharSequence)localObject1).length();
      k = 1;
      if (j > 0)
      {
        j = 1;
      }
      else
      {
        j = 0;
        localObject1 = null;
      }
      if (j != 0)
      {
        localObject1 = c.a.m.d(paramString);
        localObject1 = (String)localObject1;
        j = Integer.parseInt((String)localObject1);
        Object localObject2 = c.a.m.f(paramString);
        localObject2 = (CharSequence)localObject2;
        int m = ((CharSequence)localObject2).length();
        if (m > 0) {
          i = 1;
        }
        if (i != 0)
        {
          paramString = c.a.m.f(paramString);
          paramString = (String)paramString;
          int n = Integer.parseInt(paramString);
          localb = new com/truecaller/profile/business/openHours/b;
          localb.<init>(j, n);
          return localb;
        }
        return null;
      }
      return null;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localNumberFormatException);
    }
    return null;
  }
  
  static final String a(int paramInt)
  {
    DateFormatSymbols localDateFormatSymbols = new java/text/DateFormatSymbols;
    Locale localLocale = Locale.getDefault();
    localDateFormatSymbols.<init>(localLocale);
    return localDateFormatSymbols.getWeekdays()[paramInt];
  }
  
  public static final b b(OpenHours paramOpenHours)
  {
    String str = "receiver$0";
    k.b(paramOpenHours, str);
    paramOpenHours = paramOpenHours.getCloses();
    if (paramOpenHours != null) {
      return a(paramOpenHours);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */