package com.truecaller.profile.business.openHours;

import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import java.util.SortedSet;

final class OpenHoursFragment$c
  implements DialogInterface.OnMultiChoiceClickListener
{
  OpenHoursFragment$c(SortedSet paramSortedSet) {}
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramDialogInterface = a;
      localInteger = Integer.valueOf(paramInt + 1);
      paramDialogInterface.add(localInteger);
      return;
    }
    paramDialogInterface = a;
    Integer localInteger = Integer.valueOf(paramInt + 1);
    paramDialogInterface.remove(localInteger);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.OpenHoursFragment.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */