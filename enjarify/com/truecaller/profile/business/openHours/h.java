package com.truecaller.profile.business.openHours;

import c.g.b.k;
import com.truecaller.profile.data.dto.OpenHours;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public final class h
  implements g
{
  private final List a;
  
  public h()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    localObject = (List)localObject;
    a = ((List)localObject);
  }
  
  public final OpenHours a()
  {
    OpenHours localOpenHours = new com/truecaller/profile/data/dto/OpenHours;
    Object localObject1 = new java/util/TreeSet;
    ((TreeSet)localObject1).<init>();
    Object localObject2 = localObject1;
    localObject2 = (SortedSet)localObject1;
    localObject1 = localOpenHours;
    localOpenHours.<init>((SortedSet)localObject2, null, null, 6, null);
    a.add(localOpenHours);
    return localOpenHours;
  }
  
  public final OpenHours a(int paramInt)
  {
    return (OpenHours)a.get(paramInt);
  }
  
  public final OpenHours a(int paramInt, b paramb)
  {
    k.b(paramb, "opensAt");
    OpenHours localOpenHours = a(paramInt);
    k.b(localOpenHours, "receiver$0");
    k.b(paramb, "time");
    String str = paramb.a();
    paramb = OpenHours.copy$default(localOpenHours, null, str, null, 5, null);
    a.set(paramInt, paramb);
    return paramb;
  }
  
  public final OpenHours a(int paramInt, SortedSet paramSortedSet)
  {
    k.b(paramSortedSet, "daysOfTheWeek");
    Object localObject = a.get(paramInt);
    paramSortedSet = OpenHours.copy$default((OpenHours)localObject, paramSortedSet, null, null, 6, null);
    a.set(paramInt, paramSortedSet);
    return paramSortedSet;
  }
  
  public final List a(List paramList)
  {
    k.b(paramList, "openHours");
    a.clear();
    List localList = a;
    paramList = (Collection)paramList;
    localList.addAll(paramList);
    return a;
  }
  
  public final OpenHours b(int paramInt, b paramb)
  {
    k.b(paramb, "closesAt");
    OpenHours localOpenHours = a(paramInt);
    k.b(localOpenHours, "receiver$0");
    k.b(paramb, "time");
    String str = paramb.a();
    paramb = OpenHours.copy$default(localOpenHours, null, null, str, 3, null);
    a.set(paramInt, paramb);
    return paramb;
  }
  
  public final void b(int paramInt)
  {
    a.remove(paramInt);
  }
  
  public final boolean b()
  {
    Iterator localIterator = ((Iterable)a).iterator();
    boolean bool2;
    label97:
    int i;
    do
    {
      boolean bool1 = localIterator.hasNext();
      bool2 = true;
      if (!bool1) {
        break;
      }
      Object localObject = (OpenHours)localIterator.next();
      SortedSet localSortedSet = ((OpenHours)localObject).getWeekday();
      int j = localSortedSet.isEmpty();
      CharSequence localCharSequence = (CharSequence)((OpenHours)localObject).getOpens();
      if (localCharSequence != null)
      {
        k = localCharSequence.length();
        if (k != 0)
        {
          k = 0;
          localCharSequence = null;
          break label97;
        }
      }
      int k = 1;
      j |= k;
      localObject = (CharSequence)((OpenHours)localObject).getCloses();
      if (localObject != null)
      {
        i = ((CharSequence)localObject).length();
        if (i != 0) {
          bool2 = false;
        }
      }
      i = j | bool2;
    } while (i == 0);
    return false;
    return bool2;
  }
  
  public final List c()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */