package com.truecaller.profile.business.openHours;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import c.g.b.k;
import com.truecaller.profile.data.dto.OpenHours;
import java.util.SortedSet;

final class OpenHoursFragment$d
  implements DialogInterface.OnClickListener
{
  OpenHoursFragment$d(OpenHoursFragment paramOpenHoursFragment, int paramInt, SortedSet paramSortedSet) {}
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    paramDialogInterface = a.a();
    paramInt = b;
    Object localObject = c;
    k.b(localObject, "daysOfTheWeek");
    localObject = a.a(paramInt, (SortedSet)localObject);
    e.a locala = (e.a)b;
    if (locala != null) {
      locala.a(paramInt, (OpenHours)localObject);
    }
    paramDialogInterface.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.OpenHoursFragment.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */