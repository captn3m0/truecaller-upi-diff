package com.truecaller.profile.business.openHours;

import android.app.TimePickerDialog.OnTimeSetListener;
import android.widget.TimePicker;
import c.g.b.k;

final class OpenHoursFragment$b
  implements TimePickerDialog.OnTimeSetListener
{
  OpenHoursFragment$b(OpenHoursFragment paramOpenHoursFragment, int paramInt) {}
  
  public final void onTimeSet(TimePicker paramTimePicker, int paramInt1, int paramInt2)
  {
    paramTimePicker = new com/truecaller/profile/business/openHours/b;
    paramTimePicker.<init>(paramInt1, paramInt2);
    f localf = a.a();
    paramInt2 = b;
    k.b(paramTimePicker, "closesAt");
    paramTimePicker = a.b(paramInt2, paramTimePicker);
    e.a locala = (e.a)b;
    if (locala != null) {
      locala.c(paramInt2, paramTimePicker);
    }
    localf.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.OpenHoursFragment.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */