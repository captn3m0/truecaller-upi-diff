package com.truecaller.profile.business.openHours;

import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.profile.business.i;
import com.truecaller.profile.business.j;
import com.truecaller.profile.data.dto.OpenHours;
import com.truecaller.util.at;
import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;

public final class OpenHoursFragment
  extends Fragment
  implements e.a
{
  public f a;
  public d b;
  private HashMap c;
  
  private static String[] d()
  {
    Object localObject = DateFormatSymbols.getInstance(Locale.getDefault());
    k.a(localObject, "DateFormatSymbols.getInstance(Locale.getDefault())");
    localObject = ((DateFormatSymbols)localObject).getWeekdays();
    int i = localObject.length;
    localObject = Arrays.copyOfRange((Object[])localObject, 1, i);
    k.a(localObject, "Arrays.copyOfRange(weekdays, 1, weekdays.size)");
    return (String[])localObject;
  }
  
  private View g(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final f a()
  {
    f localf = a;
    if (localf == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localf;
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    Object localObject2 = (e.a)b;
    if (localObject2 != null)
    {
      localObject1 = a.a(paramInt).getWeekday();
      ((e.a)localObject2).a(paramInt, (SortedSet)localObject1);
      return;
    }
  }
  
  public final void a(int paramInt1, int paramInt2, int paramInt3)
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    TimePickerDialog localTimePickerDialog = new android/app/TimePickerDialog;
    Object localObject1 = new com/truecaller/profile/business/openHours/OpenHoursFragment$f;
    ((OpenHoursFragment.f)localObject1).<init>(this, paramInt1);
    Object localObject2 = localObject1;
    localObject2 = (TimePickerDialog.OnTimeSetListener)localObject1;
    boolean bool = DateFormat.is24HourFormat((Context)getActivity());
    localObject1 = localTimePickerDialog;
    localTimePickerDialog.<init>(localContext, 2131952157, (TimePickerDialog.OnTimeSetListener)localObject2, paramInt2, paramInt3, bool);
    localTimePickerDialog.show();
  }
  
  public final void a(int paramInt, OpenHours paramOpenHours)
  {
    k.b(paramOpenHours, "openHour");
    d locald = b;
    if (locald == null)
    {
      String str = "openHoursAdapter";
      k.a(str);
    }
    locald.a(paramInt, paramOpenHours);
  }
  
  public final void a(int paramInt, SortedSet paramSortedSet)
  {
    k.b(paramSortedSet, "daysOfTheWeek");
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "context ?: return");
    Object localObject2 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
    int i = 2131886259;
    localObject1 = ((AlertDialog.Builder)localObject2).setTitle(i);
    localObject2 = (CharSequence[])d();
    Object localObject3 = d();
    int j = localObject3.length;
    boolean[] arrayOfBoolean = new boolean[j];
    int k = 0;
    int m = localObject3.length;
    while (k < m)
    {
      int n = k + 1;
      Integer localInteger = Integer.valueOf(n);
      boolean bool = paramSortedSet.contains(localInteger);
      arrayOfBoolean[k] = bool;
      k = n;
    }
    localObject3 = new com/truecaller/profile/business/openHours/OpenHoursFragment$c;
    ((OpenHoursFragment.c)localObject3).<init>(paramSortedSet);
    localObject3 = (DialogInterface.OnMultiChoiceClickListener)localObject3;
    localObject1 = ((AlertDialog.Builder)localObject1).setMultiChoiceItems((CharSequence[])localObject2, arrayOfBoolean, (DialogInterface.OnMultiChoiceClickListener)localObject3);
    localObject3 = new com/truecaller/profile/business/openHours/OpenHoursFragment$d;
    ((OpenHoursFragment.d)localObject3).<init>(this, paramInt, paramSortedSet);
    localObject3 = (DialogInterface.OnClickListener)localObject3;
    AlertDialog.Builder localBuilder = ((AlertDialog.Builder)localObject1).setPositiveButton(2131887217, (DialogInterface.OnClickListener)localObject3);
    localObject1 = (DialogInterface.OnClickListener)OpenHoursFragment.e.a;
    localBuilder.setNegativeButton(2131887197, (DialogInterface.OnClickListener)localObject1).show();
  }
  
  public final void a(OpenHours paramOpenHours)
  {
    k.b(paramOpenHours, "openHour");
    d locald = b;
    if (locald == null)
    {
      String str = "openHoursAdapter";
      k.a(str);
    }
    k.b(paramOpenHours, "openHour");
    a.add(paramOpenHours);
    int i = a.indexOf(paramOpenHours);
    locald.notifyItemInserted(i);
  }
  
  public final void b()
  {
    int i = R.id.newOpenHourButton;
    Button localButton = (Button)g(i);
    k.a(localButton, "newOpenHourButton");
    localButton.setVisibility(0);
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    Object localObject2 = a.a(a.a(paramInt));
    localObject1 = (e.a)b;
    if (localObject1 != null)
    {
      int i;
      if (localObject2 != null) {
        i = b;
      } else {
        i = 8;
      }
      int j;
      if (localObject2 != null)
      {
        j = c;
      }
      else
      {
        j = 0;
        localObject2 = null;
      }
      ((e.a)localObject1).a(paramInt, i, j);
      return;
    }
  }
  
  public final void b(int paramInt1, int paramInt2, int paramInt3)
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    TimePickerDialog localTimePickerDialog = new android/app/TimePickerDialog;
    Object localObject1 = new com/truecaller/profile/business/openHours/OpenHoursFragment$b;
    ((OpenHoursFragment.b)localObject1).<init>(this, paramInt1);
    Object localObject2 = localObject1;
    localObject2 = (TimePickerDialog.OnTimeSetListener)localObject1;
    boolean bool = DateFormat.is24HourFormat((Context)getActivity());
    localObject1 = localTimePickerDialog;
    localTimePickerDialog.<init>(localContext, 2131952157, (TimePickerDialog.OnTimeSetListener)localObject2, paramInt2, paramInt3, bool);
    localTimePickerDialog.show();
  }
  
  public final void b(int paramInt, OpenHours paramOpenHours)
  {
    k.b(paramOpenHours, "openHour");
    d locald = b;
    if (locald == null)
    {
      String str = "openHoursAdapter";
      k.a(str);
    }
    locald.a(paramInt, paramOpenHours);
  }
  
  public final void c()
  {
    int i = R.id.newOpenHourButton;
    Button localButton = (Button)g(i);
    k.a(localButton, "newOpenHourButton");
    localButton.setVisibility(8);
  }
  
  public final void c(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    Object localObject2 = a.b(a.a(paramInt));
    localObject1 = (e.a)b;
    if (localObject1 != null)
    {
      int i;
      if (localObject2 != null) {
        i = b;
      } else {
        i = 18;
      }
      int j;
      if (localObject2 != null)
      {
        j = c;
      }
      else
      {
        j = 0;
        localObject2 = null;
      }
      ((e.a)localObject1).b(paramInt, i, j);
      return;
    }
  }
  
  public final void c(int paramInt, OpenHours paramOpenHours)
  {
    k.b(paramOpenHours, "openHour");
    d locald = b;
    if (locald == null)
    {
      String str = "openHoursAdapter";
      k.a(str);
    }
    locald.a(paramInt, paramOpenHours);
  }
  
  public final void d(int paramInt)
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localObject = (e.a)b;
    if (localObject != null)
    {
      ((e.a)localObject).e(paramInt);
      return;
    }
  }
  
  public final void e(int paramInt)
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "context ?: return");
    Object localObject2 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
    localObject1 = ((AlertDialog.Builder)localObject2).setMessage(2131886269);
    Object localObject3 = new com/truecaller/profile/business/openHours/OpenHoursFragment$g;
    ((OpenHoursFragment.g)localObject3).<init>(this, paramInt);
    localObject3 = (DialogInterface.OnClickListener)localObject3;
    AlertDialog.Builder localBuilder = ((AlertDialog.Builder)localObject1).setPositiveButton(2131887217, (DialogInterface.OnClickListener)localObject3);
    localObject2 = (DialogInterface.OnClickListener)OpenHoursFragment.h.a;
    localBuilder.setNegativeButton(2131887197, (DialogInterface.OnClickListener)localObject2).show();
  }
  
  public final void f(int paramInt)
  {
    d locald = b;
    if (locald == null)
    {
      String str = "openHoursAdapter";
      k.a(str);
    }
    a.remove(paramInt);
    locald.notifyItemRemoved(paramInt);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      paramBundle = j.a((Context)paramBundle);
      if (paramBundle != null)
      {
        paramBundle.a(this);
        return;
      }
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(2131558515, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…_hours, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    paramView = new com/truecaller/profile/business/openHours/d;
    paramBundle = this;
    paramBundle = (c)this;
    paramView.<init>(paramBundle);
    b = paramView;
    int i = R.id.openHoursRecyclerView;
    paramView = (RecyclerView)g(i);
    int j = 0;
    paramView.setHasFixedSize(false);
    i = R.id.openHoursRecyclerView;
    paramView = (RecyclerView)g(i);
    Object localObject = "openHoursRecyclerView";
    k.a(paramView, (String)localObject);
    paramView.setNestedScrollingEnabled(false);
    i = R.id.openHoursRecyclerView;
    paramView = (RecyclerView)g(i);
    k.a(paramView, "openHoursRecyclerView");
    paramBundle = b;
    if (paramBundle == null)
    {
      localObject = "openHoursAdapter";
      k.a((String)localObject);
    }
    paramBundle = (RecyclerView.Adapter)paramBundle;
    paramView.setAdapter(paramBundle);
    i = R.id.openingHoursTextView;
    paramView = (TextView)g(i);
    j = 2130969592;
    at.e(paramView, j);
    i = R.id.newOpenHourButton;
    paramView = (Button)g(i);
    paramBundle = new com/truecaller/profile/business/openHours/OpenHoursFragment$a;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = (e.a)b;
    if (paramBundle != null)
    {
      localObject = a.a();
      paramBundle.a((OpenHours)localObject);
    }
    paramView = (e.a)b;
    if (paramView != null)
    {
      paramView.c();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.OpenHoursFragment
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */