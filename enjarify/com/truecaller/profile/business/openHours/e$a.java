package com.truecaller.profile.business.openHours;

import com.truecaller.profile.data.dto.OpenHours;
import java.util.SortedSet;

public abstract interface e$a
  extends c
{
  public abstract void a(int paramInt1, int paramInt2, int paramInt3);
  
  public abstract void a(int paramInt, OpenHours paramOpenHours);
  
  public abstract void a(int paramInt, SortedSet paramSortedSet);
  
  public abstract void a(OpenHours paramOpenHours);
  
  public abstract void b();
  
  public abstract void b(int paramInt1, int paramInt2, int paramInt3);
  
  public abstract void b(int paramInt, OpenHours paramOpenHours);
  
  public abstract void c();
  
  public abstract void c(int paramInt, OpenHours paramOpenHours);
  
  public abstract void e(int paramInt);
  
  public abstract void f(int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */