package com.truecaller.profile.business.openHours;

import c.f;
import c.g.a.a;
import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import java.util.Calendar;

public final class b
{
  public static final b.a d;
  final int b;
  final int c;
  private final int e;
  private final f f;
  private final f g;
  
  static
  {
    Object localObject1 = new c.l.g[2];
    Object localObject2 = new c/g/b/u;
    c.l.b localb1 = w.a(b.class);
    ((u)localObject2).<init>(localb1, "internalFormat", "getInternalFormat()Ljava/lang/String;");
    localObject2 = (c.l.g)w.a((t)localObject2);
    localObject1[0] = localObject2;
    localObject2 = new c/g/b/u;
    c.l.b localb2 = w.a(b.class);
    ((u)localObject2).<init>(localb2, "localFormat", "getLocalFormat()Ljava/lang/String;");
    localObject2 = (c.l.g)w.a((t)localObject2);
    localObject1[1] = localObject2;
    a = (c.l.g[])localObject1;
    localObject1 = new com/truecaller/profile/business/openHours/b$a;
    ((b.a)localObject1).<init>((byte)0);
    d = (b.a)localObject1;
  }
  
  public b(int paramInt1, int paramInt2)
  {
    b = paramInt1;
    c = paramInt2;
    paramInt1 = b * 60;
    paramInt2 = c;
    paramInt1 += paramInt2;
    e = paramInt1;
    Object localObject = new com/truecaller/profile/business/openHours/b$b;
    ((b.b)localObject).<init>(this);
    localObject = c.g.a((a)localObject);
    f = ((f)localObject);
    localObject = new com/truecaller/profile/business/openHours/b$c;
    ((b.c)localObject).<init>(this);
    localObject = c.g.a((a)localObject);
    g = ((f)localObject);
  }
  
  public static final b a(Calendar paramCalendar)
  {
    k.b(paramCalendar, "time");
    b localb = new com/truecaller/profile/business/openHours/b;
    int i = paramCalendar.get(11);
    int j = paramCalendar.get(12);
    localb.<init>(i, j);
    return localb;
  }
  
  public final int a(b paramb)
  {
    k.b(paramb, "other");
    int i = e;
    int j = e;
    return k.a(i, j);
  }
  
  public final String a()
  {
    return (String)f.b();
  }
  
  public final String b()
  {
    return (String)g.b();
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof b;
      if (bool2)
      {
        paramObject = (b)paramObject;
        int i = b;
        int j = b;
        if (i == j) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0)
        {
          i = c;
          int k = c;
          if (i == k)
          {
            k = 1;
          }
          else
          {
            k = 0;
            paramObject = null;
          }
          if (k != 0) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = b * 31;
    int j = c;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("HourMinute(hour=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", minute=");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */