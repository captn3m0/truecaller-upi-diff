package com.truecaller.profile.business.openHours;

import android.support.v7.widget.RecyclerView.Adapter;
import c.g.b.k;
import com.truecaller.profile.data.dto.OpenHours;
import java.util.ArrayList;
import java.util.List;

public final class d
  extends RecyclerView.Adapter
{
  final List a;
  public boolean b;
  public boolean c;
  public boolean d;
  public int e;
  private final c f;
  
  public d(c paramc)
  {
    f = paramc;
    paramc = new java/util/ArrayList;
    paramc.<init>();
    paramc = (List)paramc;
    a = paramc;
  }
  
  public final void a(int paramInt, OpenHours paramOpenHours)
  {
    k.b(paramOpenHours, "openHour");
    a.set(paramInt, paramOpenHours);
    notifyItemChanged(paramInt);
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.openHours.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */