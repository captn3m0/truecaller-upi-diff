package com.truecaller.profile.business.a;

import c.o.b;
import c.x;
import com.truecaller.profile.business.p.c;
import com.truecaller.profile.data.dto.Address;
import com.truecaller.profile.data.dto.Branding;
import com.truecaller.profile.data.dto.BusinessData;
import com.truecaller.profile.data.dto.Company;
import com.truecaller.profile.data.dto.OnlineIds;
import com.truecaller.profile.data.dto.Profile;
import com.truecaller.profile.data.i;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

final class e$e
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  e$e(e parame, Profile paramProfile, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/profile/business/a/e$e;
    e locale1 = b;
    Profile localProfile = c;
    locale.<init>(locale1, localProfile, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = this;
    Object localObject2 = paramObject;
    Object localObject3 = c.d.a.a.a;
    int i = a;
    Object localObject4;
    switch (i)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label926;
      }
      localObject2 = b.b;
      localObject4 = c;
      localObject2 = ((com.truecaller.profile.data.c)localObject2).a((Profile)localObject4);
      j = 1;
      a = j;
      localObject2 = ((ao)localObject2).a(this);
      if (localObject2 == localObject3) {
        return localObject3;
      }
      break;
    }
    localObject2 = (com.truecaller.common.profile.f)localObject2;
    boolean bool1 = a;
    if (bool1)
    {
      localObject2 = b;
      localObject3 = c;
      localObject4 = c;
      Object localObject5 = ((Profile)localObject3).getFirstName();
      String str1 = ((Profile)localObject3).getLastName();
      i.a((com.truecaller.common.g.a)localObject4, (String)localObject5, str1);
      localObject3 = ((Profile)localObject3).getBusinessData();
      if (localObject3 != null)
      {
        localObject4 = c;
        localObject5 = ((BusinessData)localObject3).getCompany().getAddress();
        str1 = null;
        if (localObject5 != null) {
          localObject5 = ((Address)localObject5).getStreet();
        } else {
          localObject5 = null;
        }
        Object localObject6 = ((BusinessData)localObject3).getCompany().getAddress();
        if (localObject6 != null) {
          localObject6 = ((Address)localObject6).getCity();
        } else {
          localObject6 = null;
        }
        Object localObject7 = ((BusinessData)localObject3).getCompany().getAddress();
        if (localObject7 != null) {
          localObject7 = ((Address)localObject7).getZipCode();
        } else {
          localObject7 = null;
        }
        Object localObject8 = ((BusinessData)localObject3).getCompany().getAddress();
        if (localObject8 != null) {
          localObject8 = ((Address)localObject8).getLatitude();
        } else {
          localObject8 = null;
        }
        Object localObject9 = ((BusinessData)localObject3).getCompany().getAddress();
        if (localObject9 != null) {
          localObject9 = ((Address)localObject9).getLongitude();
        } else {
          localObject9 = null;
        }
        String str2 = ((BusinessData)localObject3).getCompany().getName();
        OnlineIds localOnlineIds = ((BusinessData)localObject3).getOnlineIds();
        String str3 = ((BusinessData)localObject3).getAvatarUrl();
        String str4 = ((BusinessData)localObject3).getCompany().getBranding().getBackgroundColor();
        String str5 = ((BusinessData)localObject3).getJobTitle();
        Object localObject10 = (Long)c.a.m.e(((BusinessData)localObject3).getTags());
        if (localObject10 != null)
        {
          long l = ((Long)localObject10).longValue();
          str1 = String.valueOf(l);
        }
        localObject10 = ((BusinessData)localObject3).getAbout();
        localObject1 = ((BusinessData)localObject3).getCompany().getSize();
        paramObject = localObject9;
        localObject9 = d;
        Object localObject11 = ((BusinessData)localObject3).getCompany();
        Object localObject12 = localObject8;
        localObject8 = ((Company)localObject11).getOpenHours();
        localObject8 = ((com.google.gson.f)localObject9).b(localObject8);
        c.g.b.k.a(localObject8, "gson.toJson(company.openHours)");
        localObject2 = d;
        localObject3 = ((BusinessData)localObject3).getCompany().getBranding().getImageUrls();
        localObject2 = ((com.google.gson.f)localObject2).b(localObject3);
        c.g.b.k.a(localObject2, "gson.toJson(company.branding.imageUrls)");
        localObject11 = Boolean.TRUE;
        c.g.b.k.b(localObject4, "receiver$0");
        c.g.b.k.b(str2, "companyName");
        c.g.b.k.b(localOnlineIds, "onlineIds");
        c.g.b.k.b(str4, "backgroundColor");
        c.g.b.k.b(localObject8, "openingHours");
        c.g.b.k.b(localObject2, "imageUrls");
        localObject9 = "profileGender";
        String str6 = "1";
        ((com.truecaller.common.g.a)localObject4).a((String)localObject9, "N");
        ((com.truecaller.common.g.a)localObject4).a("profileStreet", (String)localObject5);
        ((com.truecaller.common.g.a)localObject4).a("profileCity", (String)localObject6);
        ((com.truecaller.common.g.a)localObject4).a("profileZip", (String)localObject7);
        localObject5 = localOnlineIds.getFacebookId();
        ((com.truecaller.common.g.a)localObject4).a("profileFacebook", (String)localObject5);
        localObject5 = localOnlineIds.getTwitterId();
        ((com.truecaller.common.g.a)localObject4).a("profileTwitter", (String)localObject5);
        localObject5 = localOnlineIds.getEmail();
        ((com.truecaller.common.g.a)localObject4).a("profileEmail", (String)localObject5);
        localObject5 = localOnlineIds.getUrl();
        ((com.truecaller.common.g.a)localObject4).a("profileWeb", (String)localObject5);
        ((com.truecaller.common.g.a)localObject4).a("profileAvatar", str3);
        ((com.truecaller.common.g.a)localObject4).a("profileBackgroundColor", str4);
        ((com.truecaller.common.g.a)localObject4).a("profileCompanyName", str2);
        ((com.truecaller.common.g.a)localObject4).a("profileCompanyJob", str5);
        ((com.truecaller.common.g.a)localObject4).a("profileTag", str1);
        ((com.truecaller.common.g.a)localObject4).a("profileStatus", (String)localObject10);
        localObject3 = "profileSize";
        ((com.truecaller.common.g.a)localObject4).a((String)localObject3, (String)localObject1);
        ((com.truecaller.common.g.a)localObject4).a("profileOpeningHours", (String)localObject8);
        ((com.truecaller.common.g.a)localObject4).a("profileImageUrls", (String)localObject2);
        localObject2 = str6;
        ((com.truecaller.common.g.a)localObject4).a("profileAcceptAuto", str6);
        localObject1 = "profileBusiness";
        boolean bool2 = com.truecaller.utils.extensions.c.a((Boolean)localObject11);
        ((com.truecaller.common.g.a)localObject4).b((String)localObject1, bool2);
        double d1;
        if (localObject12 != null)
        {
          localObject8 = localObject12;
          localObject8 = (Number)localObject12;
          d1 = ((Number)localObject8).doubleValue();
          localObject3 = "profileLatitude";
          ((com.truecaller.common.g.a)localObject4).a((String)localObject3, d1);
        }
        if (paramObject != null)
        {
          localObject9 = paramObject;
          localObject9 = (Number)paramObject;
          d1 = ((Number)localObject9).doubleValue();
          localObject3 = "profileLongitude";
          ((com.truecaller.common.g.a)localObject4).a((String)localObject3, d1);
        }
      }
      return x.a;
    }
    localObject1 = new com/truecaller/profile/business/p$c;
    ((p.c)localObject1).<init>((com.truecaller.common.profile.f)localObject2);
    throw ((Throwable)localObject1);
    label926:
    localObject1 = paramObject;
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.a.e.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */