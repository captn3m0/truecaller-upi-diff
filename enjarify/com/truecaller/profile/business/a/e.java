package com.truecaller.profile.business.a;

import c.a.y;
import c.g.b.k;
import c.o.b;
import com.google.gson.t;
import com.truecaller.log.AssertionUtil;
import com.truecaller.profile.data.dto.Address;
import com.truecaller.profile.data.dto.Branding;
import com.truecaller.profile.data.dto.BusinessData;
import com.truecaller.profile.data.dto.Company;
import com.truecaller.profile.data.dto.OnlineIds;
import com.truecaller.profile.data.dto.Profile;
import java.lang.reflect.Type;
import java.util.List;
import kotlinx.coroutines.g;

public final class e
  implements d
{
  final a a;
  final com.truecaller.profile.data.c b;
  final com.truecaller.common.g.a c;
  final com.google.gson.f d;
  private final c.d.f e;
  
  public e(a parama, com.truecaller.profile.data.c paramc, com.truecaller.common.g.a parama1, com.google.gson.f paramf, c.d.f paramf1)
  {
    a = parama;
    b = paramc;
    c = parama1;
    d = paramf;
    e = paramf1;
  }
  
  private final List b()
  {
    Object localObject1 = c;
    Object localObject2 = "profileOpeningHours";
    localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject2);
    if (localObject1 != null)
    {
      try
      {
        localObject2 = d;
        Object localObject3 = new com/truecaller/profile/business/a/e$d;
        ((e.d)localObject3).<init>();
        localObject3 = b;
        localObject1 = ((com.google.gson.f)localObject2).a((String)localObject1, (Type)localObject3);
        localObject1 = (List)localObject1;
      }
      catch (t localt)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localt);
        localObject1 = null;
      }
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = (List)y.a;
    }
    return (List)localObject1;
  }
  
  private final List c()
  {
    Object localObject1 = c;
    Object localObject2 = "profileImageUrls";
    localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject2);
    if (localObject1 != null)
    {
      try
      {
        localObject2 = d;
        Object localObject3 = new com/truecaller/profile/business/a/e$c;
        ((e.c)localObject3).<init>();
        localObject3 = b;
        localObject1 = ((com.google.gson.f)localObject2).a((String)localObject1, (Type)localObject3);
        localObject1 = (List)localObject1;
      }
      catch (t localt)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localt);
        localObject1 = null;
      }
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = (List)y.a;
    }
    return (List)localObject1;
  }
  
  public final Profile a()
  {
    e locale = this;
    Profile localProfile = new com/truecaller/profile/data/dto/Profile;
    Object localObject1 = c;
    Object localObject2 = "profileFirstName";
    localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject2);
    if (localObject1 == null) {
      localObject1 = "";
    }
    localObject2 = localObject1;
    localObject1 = c;
    Object localObject3 = "profileLastName";
    localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject3);
    if (localObject1 == null) {
      localObject1 = "";
    }
    localObject3 = localObject1;
    BusinessData localBusinessData = new com/truecaller/profile/data/dto/BusinessData;
    localObject1 = y.a;
    Object localObject4 = localObject1;
    localObject4 = (List)localObject1;
    String str1 = c.a("profileAvatar");
    String str2 = c.a("profileCompanyJob");
    localObject1 = c;
    Object localObject5 = "profileStatus";
    localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject5);
    if (localObject1 == null) {
      localObject1 = "";
    }
    Object localObject6 = localObject1;
    localObject1 = c;
    localObject5 = "profileTag";
    localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject5);
    if (localObject1 != null) {
      localObject1 = c.n.m.d((String)localObject1);
    } else {
      localObject1 = null;
    }
    List localList1 = c.a.m.b(localObject1);
    OnlineIds localOnlineIds = new com/truecaller/profile/data/dto/OnlineIds;
    localObject1 = c.a("profileFacebook");
    Object localObject7 = c;
    Object localObject8 = "profileEmail";
    localObject7 = ((com.truecaller.common.g.a)localObject7).a((String)localObject8);
    if (localObject7 == null) {
      localObject7 = "";
    }
    localObject5 = c.a("profileWeb");
    localObject8 = c;
    Object localObject9 = ((com.truecaller.common.g.a)localObject8).a("profileTwitter");
    localOnlineIds.<init>((String)localObject1, (String)localObject7, (String)localObject5, (String)localObject9);
    localObject1 = new com/truecaller/profile/data/dto/Company;
    localObject9 = c;
    localObject5 = "profileCompanyName";
    localObject9 = ((com.truecaller.common.g.a)localObject9).a((String)localObject5);
    if (localObject9 == null) {
      localObject9 = "";
    }
    Object localObject10 = localObject9;
    List localList2 = b();
    Object localObject11 = c.a("profileStreet");
    String str3 = c.a("profileCity");
    String str4 = c.a("profileCountryIso");
    double d1 = c.c("profileLatitude");
    localObject5 = Double.valueOf(d1);
    localObject9 = localObject5;
    localObject9 = (Number)localObject5;
    double d2 = ((Number)localObject9).doubleValue();
    double d3 = 0.0D;
    int i = 1;
    localObject7 = null;
    boolean bool = d2 < d3;
    if (!bool)
    {
      bool = true;
    }
    else
    {
      bool = false;
      localObject8 = null;
    }
    Object localObject12;
    if (!bool) {
      localObject12 = localObject5;
    } else {
      localObject12 = null;
    }
    localObject5 = Double.valueOf(c.c("profileLongitude"));
    localObject8 = localObject5;
    localObject8 = (Number)localObject5;
    double d4 = ((Number)localObject8).doubleValue();
    bool = d4 < d3;
    if (!bool)
    {
      bool = true;
    }
    else
    {
      bool = false;
      localObject8 = null;
    }
    Object localObject13;
    if (!bool) {
      localObject13 = localObject5;
    } else {
      localObject13 = null;
    }
    localObject5 = localObject11;
    localObject5 = (CharSequence)localObject11;
    if (localObject5 != null)
    {
      j = ((CharSequence)localObject5).length();
      if (j != 0)
      {
        j = 0;
        localObject5 = null;
        break label548;
      }
    }
    int j = 1;
    label548:
    if (j == 0)
    {
      localObject5 = str3;
      localObject5 = (CharSequence)str3;
      if (localObject5 != null)
      {
        j = ((CharSequence)localObject5).length();
        if (j != 0)
        {
          j = 0;
          localObject5 = null;
          break label598;
        }
      }
      j = 1;
      label598:
      if (j == 0)
      {
        localObject5 = str4;
        localObject5 = (CharSequence)str4;
        if (localObject5 != null)
        {
          j = ((CharSequence)localObject5).length();
          if (j != 0)
          {
            i = 0;
            localObject9 = null;
          }
        }
        if (i == 0)
        {
          localObject9 = new com/truecaller/profile/data/dto/Address;
          localObject5 = c;
          localObject7 = "profileZip";
          String str5 = ((com.truecaller.common.g.a)localObject5).a((String)localObject7);
          localObject14 = localObject9;
          ((Address)localObject9).<init>((String)localObject11, str3, str5, str4, (Double)localObject12, (Double)localObject13);
          break label700;
        }
      }
    }
    Object localObject14 = null;
    label700:
    localObject9 = new com/truecaller/profile/data/dto/Branding;
    localObject5 = c;
    localObject7 = "profileBackgroundColor";
    localObject5 = ((com.truecaller.common.g.a)localObject5).a((String)localObject7);
    if (localObject5 == null) {
      localObject5 = "#F2F5F7";
    }
    localObject7 = c.a.m.g((Iterable)c());
    ((Branding)localObject9).<init>((String)localObject5, (List)localObject7);
    str3 = c.a("profileSize");
    localObject11 = localObject9;
    ((Company)localObject1).<init>((String)localObject10, localList2, (Address)localObject14, (Branding)localObject9, str3);
    localObject8 = localBusinessData;
    localBusinessData.<init>((List)localObject4, str1, str2, (String)localObject6, localList1, localOnlineIds, (Company)localObject1);
    localObject1 = localProfile;
    localProfile.<init>((String)localObject2, (String)localObject3, null, localBusinessData, 4, null);
    return localProfile;
  }
  
  public final Object a(c.d.c paramc)
  {
    c.d.f localf = e;
    Object localObject = new com/truecaller/profile/business/a/e$a;
    ((e.a)localObject).<init>(this, null);
    localObject = (c.g.a.m)localObject;
    return g.a(localf, (c.g.a.m)localObject, paramc);
  }
  
  public final Object a(Profile paramProfile, c.d.c paramc)
  {
    c.d.f localf = e;
    Object localObject = new com/truecaller/profile/business/a/e$e;
    ((e.e)localObject).<init>(this, paramProfile, null);
    localObject = (c.g.a.m)localObject;
    return g.a(localf, (c.g.a.m)localObject, paramc);
  }
  
  public final Object a(String paramString, c.d.c paramc)
  {
    c.d.f localf = e;
    Object localObject = new com/truecaller/profile/business/a/e$b;
    ((e.b)localObject).<init>(this, paramString, null);
    localObject = (c.g.a.m)localObject;
    return g.a(localf, (c.g.a.m)localObject, paramc);
  }
  
  public final Object b(String paramString, c.d.c paramc)
  {
    boolean bool1 = paramc instanceof e.f;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (e.f)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label77;
      }
    }
    Object localObject1 = new com/truecaller/profile/business/a/e$f;
    ((e.f)localObject1).<init>(this, (c.d.c)paramc);
    label77:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    int j = b;
    int m;
    switch (j)
    {
    default: 
      paramString = new java/lang/IllegalStateException;
      paramString.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramString;
    case 1: 
      m = paramc instanceof o.b;
      if (m != 0) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label239;
      }
      paramc = e;
      Object localObject2 = new com/truecaller/profile/business/a/e$g;
      ((e.g)localObject2).<init>(this, paramString, null);
      localObject2 = (c.g.a.m)localObject2;
      d = this;
      e = paramString;
      m = 1;
      b = m;
      paramc = g.a(paramc, (c.g.a.m)localObject2, (c.d.c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      break;
    }
    k.a(paramc, "withContext(async) {\n   …e.code())\n        }\n    }");
    return paramc;
    label239:
    throw a;
  }
  
  public final Object c(String paramString, c.d.c paramc)
  {
    boolean bool1 = paramc instanceof e.h;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (e.h)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label77;
      }
    }
    Object localObject1 = new com/truecaller/profile/business/a/e$h;
    ((e.h)localObject1).<init>(this, (c.d.c)paramc);
    label77:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    int j = b;
    int m;
    switch (j)
    {
    default: 
      paramString = new java/lang/IllegalStateException;
      paramString.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramString;
    case 1: 
      m = paramc instanceof o.b;
      if (m != 0) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label239;
      }
      paramc = e;
      Object localObject2 = new com/truecaller/profile/business/a/e$i;
      ((e.i)localObject2).<init>(this, paramString, null);
      localObject2 = (c.g.a.m)localObject2;
      d = this;
      e = paramString;
      m = 1;
      b = m;
      paramc = g.a(paramc, (c.g.a.m)localObject2, (c.d.c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      break;
    }
    k.a(paramc, "withContext(async) {\n   …e.code())\n        }\n    }");
    return paramc;
    label239:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */