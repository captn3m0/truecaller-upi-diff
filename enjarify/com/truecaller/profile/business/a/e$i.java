package com.truecaller.profile.business.a;

import android.net.Uri;
import android.webkit.URLUtil;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.common.network.util.g;
import com.truecaller.profile.business.p.e;
import e.b;
import e.r;
import java.io.File;
import kotlinx.coroutines.ag;
import okhttp3.ac;
import okhttp3.ae;
import okhttp3.w;

final class e$i
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  e$i(e parame, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    i locali = new com/truecaller/profile/business/a/e$i;
    e locale = b;
    String str = c;
    locali.<init>(locale, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locali;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = c;
        boolean bool2 = URLUtil.isNetworkUrl((String)paramObject);
        if (bool2) {
          return c;
        }
        paramObject = g.c;
        localObject1 = new java/io/File;
        Object localObject2 = Uri.parse(c);
        String str = "Uri.parse(picture)";
        c.g.b.k.a(localObject2, str);
        localObject2 = ((Uri)localObject2).getPath();
        ((File)localObject1).<init>((String)localObject2);
        paramObject = ac.a((w)paramObject, (File)localObject1);
        localObject1 = b.a;
        c.g.b.k.a(paramObject, "request");
        paramObject = ((a)localObject1).b((ac)paramObject).c();
        localObject1 = (ae)((r)paramObject).e();
        localObject2 = "response";
        c.g.b.k.a(paramObject, (String)localObject2);
        boolean bool3 = ((r)paramObject).d();
        if ((bool3) && (localObject1 != null)) {
          return ((ae)localObject1).g();
        }
        localObject1 = new com/truecaller/profile/business/p$e;
        int j = ((r)paramObject).b();
        ((p.e)localObject1).<init>(j);
        throw ((Throwable)localObject1);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (i)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((i)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.a.e.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */