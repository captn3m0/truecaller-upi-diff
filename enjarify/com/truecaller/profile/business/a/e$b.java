package com.truecaller.profile.business.a;

import android.webkit.URLUtil;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.profile.business.p.b;
import e.b;
import e.r;
import kotlinx.coroutines.ag;
import okhttp3.ac;
import okhttp3.w;

final class e$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  e$b(e parame, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/profile/business/a/e$b;
    e locale = b;
    String str = c;
    localb.<init>(locale, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = c;
        boolean bool2 = URLUtil.isNetworkUrl((String)paramObject);
        if (!bool2) {
          return x.a;
        }
        paramObject = w.b("text/plain");
        localObject = c;
        paramObject = ac.a((w)paramObject, (String)localObject);
        localObject = b.a;
        String str = "request";
        c.g.b.k.a(paramObject, str);
        paramObject = ((a)localObject).c((ac)paramObject).c();
        localObject = "response";
        c.g.b.k.a(paramObject, (String)localObject);
        bool1 = ((r)paramObject).d();
        if (bool1) {
          return x.a;
        }
        localObject = new com/truecaller/profile/business/p$b;
        int j = ((r)paramObject).b();
        ((p.b)localObject).<init>(j);
        throw ((Throwable)localObject);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.a.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */