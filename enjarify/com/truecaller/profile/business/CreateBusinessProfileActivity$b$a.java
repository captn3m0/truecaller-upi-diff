package com.truecaller.profile.business;

import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.io.InputStream;
import kotlinx.coroutines.ag;

final class CreateBusinessProfileActivity$b$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  CreateBusinessProfileActivity$b$a(c paramc, InputStream paramInputStream, CreateBusinessProfileActivity.b paramb, Uri paramUri)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/profile/business/CreateBusinessProfileActivity$b$a;
    InputStream localInputStream = b;
    CreateBusinessProfileActivity.b localb = c;
    Uri localUri = d;
    locala.<init>(paramc, localInputStream, localb, localUri);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = c.i.a();
        localObject = d.toString();
        c.g.b.k.a(localObject, "tempCaptureUri.toString()");
        String str = "logo";
        c.g.b.k.b(localObject, str);
        paramObject = (n.a)b;
        if (paramObject != null) {
          ((n.a)paramObject).d((String)localObject);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */