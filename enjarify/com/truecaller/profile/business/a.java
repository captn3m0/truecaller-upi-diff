package com.truecaller.profile.business;

import android.support.v7.widget.RecyclerView.Adapter;
import c.g.b.k;
import java.util.List;

public final class a
  extends RecyclerView.Adapter
{
  int a;
  private final h b;
  
  public a(h paramh)
  {
    b = paramh;
  }
  
  public static int a(String paramString)
  {
    k.b(paramString, "color");
    return b.a().indexOf(paramString);
  }
  
  public static String a(int paramInt)
  {
    return (String)b.a().get(paramInt);
  }
  
  public final int getItemCount()
  {
    return b.a().size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */