package com.truecaller.profile.business;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class CreateBusinessProfileActivity$e$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  CreateBusinessProfileActivity$e$a(String paramString, c paramc, CreateBusinessProfileActivity.e parame)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/profile/business/CreateBusinessProfileActivity$e$a;
    String str = b;
    CreateBusinessProfileActivity.e locale = c;
    locala.<init>(str, paramc, locale);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = c.c.a();
        localObject1 = b;
        c.g.b.k.a(localObject1, "it");
        c.g.b.k.b(localObject1, "picture");
        Object localObject2 = f;
        if (localObject2 != null)
        {
          localObject2 = (Number)localObject2;
          int j = ((Number)localObject2).intValue();
          f = null;
          paramObject = (n.a)b;
          if (paramObject != null) {
            ((n.a)paramObject).a(j, (String)localObject1);
          }
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */