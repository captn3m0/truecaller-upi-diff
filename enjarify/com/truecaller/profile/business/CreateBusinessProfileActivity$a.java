package com.truecaller.profile.business;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class CreateBusinessProfileActivity$a
{
  public static Intent a(Context paramContext, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, CreateBusinessProfileActivity.class);
    localIntent.putExtra("arg_from_wizard", paramBoolean1);
    localIntent.putExtra("arg_editing", paramBoolean2);
    localIntent.putExtra("arg_migrating", paramBoolean3);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */