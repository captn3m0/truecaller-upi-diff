package com.truecaller.profile.business;

import com.truecaller.profile.BusinessProfileOnboardingActivity;
import com.truecaller.profile.business.address.b;
import com.truecaller.profile.business.address.c;
import com.truecaller.profile.business.openHours.OpenHoursFragment;

public abstract interface i
{
  public abstract void a(BusinessProfileOnboardingActivity paramBusinessProfileOnboardingActivity);
  
  public abstract void a(CreateBusinessProfileActivity paramCreateBusinessProfileActivity);
  
  public abstract void a(b paramb);
  
  public abstract void a(c paramc);
  
  public abstract void a(OpenHoursFragment paramOpenHoursFragment);
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */