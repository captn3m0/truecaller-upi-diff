package com.truecaller.profile.business;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import c.g.b.k;
import com.truecaller.R.id;
import java.util.HashMap;

public final class a$a
  extends RecyclerView.ViewHolder
  implements View.OnClickListener, kotlinx.a.a.a
{
  private final View b;
  private HashMap c;
  
  public a$a(a parama, View paramView)
  {
    super(paramView);
    b = paramView;
    int i = R.id.rootView;
    parama = (FrameLayout)a(i);
    paramView = this;
    paramView = (View.OnClickListener)this;
    parama.setOnClickListener(paramView);
  }
  
  public final View a()
  {
    return b;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "view");
    paramView = a.a(a);
    String str = a.a(getAdapterPosition());
    paramView.a(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */