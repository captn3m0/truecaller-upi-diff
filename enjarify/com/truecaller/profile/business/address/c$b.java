package com.truecaller.profile.business.address;

import android.view.View;
import android.view.View.OnClickListener;

final class c$b
  implements View.OnClickListener
{
  c$b(c paramc) {}
  
  public final void onClick(View paramView)
  {
    paramView = a.a();
    GeocodedBusinessAddress localGeocodedBusinessAddress = d;
    if (localGeocodedBusinessAddress != null)
    {
      double d1 = localGeocodedBusinessAddress.getLongitude();
      double d2 = 0.0D;
      boolean bool = d1 < d2;
      if (bool)
      {
        d1 = localGeocodedBusinessAddress.getLatitude();
        bool = d1 < d2;
        if (bool)
        {
          i = 1;
          break label61;
        }
      }
      int i = 0;
      label61:
      if (i != 0)
      {
        paramView = (d.a)b;
        if (paramView != null) {
          paramView.a(localGeocodedBusinessAddress);
        }
        return;
      }
    }
    paramView = (d.a)b;
    if (paramView != null)
    {
      paramView.d();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */