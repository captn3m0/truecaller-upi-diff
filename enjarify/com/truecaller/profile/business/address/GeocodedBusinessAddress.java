package com.truecaller.profile.business.address;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class GeocodedBusinessAddress
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String city;
  private final String countryCode;
  private final double latitude;
  private final double longitude;
  private final String street;
  private final String zipCode;
  
  static
  {
    GeocodedBusinessAddress.a locala = new com/truecaller/profile/business/address/GeocodedBusinessAddress$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public GeocodedBusinessAddress(String paramString1, String paramString2, String paramString3, String paramString4, double paramDouble1, double paramDouble2)
  {
    street = paramString1;
    zipCode = paramString2;
    city = paramString3;
    countryCode = paramString4;
    latitude = paramDouble1;
    longitude = paramDouble2;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final String getCity()
  {
    return city;
  }
  
  public final String getCountryCode()
  {
    return countryCode;
  }
  
  public final double getLatitude()
  {
    return latitude;
  }
  
  public final double getLongitude()
  {
    return longitude;
  }
  
  public final String getStreet()
  {
    return street;
  }
  
  public final String getZipCode()
  {
    return zipCode;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = street;
    paramParcel.writeString(str);
    str = zipCode;
    paramParcel.writeString(str);
    str = city;
    paramParcel.writeString(str);
    str = countryCode;
    paramParcel.writeString(str);
    double d = latitude;
    paramParcel.writeDouble(d);
    d = longitude;
    paramParcel.writeDouble(d);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.GeocodedBusinessAddress
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */