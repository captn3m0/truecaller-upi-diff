package com.truecaller.profile.business.address;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import c.g.b.k;
import c.x;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.truecaller.R.id;

final class b$e
  implements OnMapReadyCallback
{
  b$e(b paramb) {}
  
  public final void a(GoogleMap paramGoogleMap)
  {
    b.a(a, paramGoogleMap);
    k.a(paramGoogleMap, "it");
    paramGoogleMap = paramGoogleMap.c();
    k.a(paramGoogleMap, "it.uiSettings");
    paramGoogleMap.e();
    paramGoogleMap = a;
    int i = R.id.mapOverlayView;
    paramGoogleMap = paramGoogleMap.b(i);
    Object localObject = new com/truecaller/profile/business/address/b$e$1;
    ((b.e.1)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramGoogleMap.setOnClickListener((View.OnClickListener)localObject);
    paramGoogleMap = a;
    i = R.id.mapLocationButton;
    paramGoogleMap = (Button)paramGoogleMap.b(i);
    localObject = new com/truecaller/profile/business/address/b$e$2;
    ((b.e.2)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramGoogleMap.setOnClickListener((View.OnClickListener)localObject);
    paramGoogleMap = a.a();
    localObject = d;
    if (localObject != null)
    {
      f.a locala = (f.a)b;
      if (locala != null)
      {
        locala.a((LatLng)localObject);
        localObject = x.a;
      }
      else
      {
        i = 0;
        localObject = null;
      }
      if (localObject != null) {}
    }
    else
    {
      paramGoogleMap = (f.a)b;
      if (paramGoogleMap != null)
      {
        paramGoogleMap.b();
        paramGoogleMap = x.a;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */