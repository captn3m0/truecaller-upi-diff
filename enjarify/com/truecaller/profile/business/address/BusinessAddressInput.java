package com.truecaller.profile.business.address;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class BusinessAddressInput
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String city;
  private final String countryCode;
  private final Double latitude;
  private final Double longitude;
  private final String street;
  private final String zipCode;
  
  static
  {
    BusinessAddressInput.a locala = new com/truecaller/profile/business/address/BusinessAddressInput$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public BusinessAddressInput(String paramString1, String paramString2, String paramString3, String paramString4, Double paramDouble1, Double paramDouble2)
  {
    street = paramString1;
    zipCode = paramString2;
    city = paramString3;
    countryCode = paramString4;
    latitude = paramDouble1;
    longitude = paramDouble2;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final String getCity()
  {
    return city;
  }
  
  public final String getCountryCode()
  {
    return countryCode;
  }
  
  public final Double getLatitude()
  {
    return latitude;
  }
  
  public final Double getLongitude()
  {
    return longitude;
  }
  
  public final String getStreet()
  {
    return street;
  }
  
  public final String getZipCode()
  {
    return zipCode;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = street;
    paramParcel.writeString((String)localObject);
    localObject = zipCode;
    paramParcel.writeString((String)localObject);
    localObject = city;
    paramParcel.writeString((String)localObject);
    localObject = countryCode;
    paramParcel.writeString((String)localObject);
    localObject = latitude;
    int i = 1;
    if (localObject != null)
    {
      paramParcel.writeInt(i);
      double d1 = ((Double)localObject).doubleValue();
      paramParcel.writeDouble(d1);
    }
    else
    {
      paramParcel.writeInt(0);
    }
    localObject = longitude;
    if (localObject != null)
    {
      paramParcel.writeInt(i);
      double d2 = ((Double)localObject).doubleValue();
      paramParcel.writeDouble(d2);
      return;
    }
    paramParcel.writeInt(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.BusinessAddressInput
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */