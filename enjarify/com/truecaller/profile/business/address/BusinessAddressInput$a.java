package com.truecaller.profile.business.address;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class BusinessAddressInput$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    BusinessAddressInput localBusinessAddressInput = new com/truecaller/profile/business/address/BusinessAddressInput;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    int i = paramParcel.readInt();
    Object localObject1 = null;
    if (i != 0)
    {
      double d1 = paramParcel.readDouble();
      localObject2 = Double.valueOf(d1);
      localObject3 = localObject2;
    }
    else
    {
      localObject3 = null;
    }
    i = paramParcel.readInt();
    if (i != 0)
    {
      double d2 = paramParcel.readDouble();
      paramParcel = Double.valueOf(d2);
    }
    else
    {
      paramParcel = null;
    }
    Object localObject2 = localBusinessAddressInput;
    localObject1 = localObject3;
    Object localObject3 = paramParcel;
    localBusinessAddressInput.<init>(str1, str2, str3, str4, (Double)localObject1, paramParcel);
    return localBusinessAddressInput;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new BusinessAddressInput[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.BusinessAddressInput.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */