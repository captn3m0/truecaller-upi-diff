package com.truecaller.profile.business.address;

import c.g.b.k;
import com.google.android.gms.maps.model.LatLng;
import com.truecaller.bb;

public final class g
  extends bb
{
  BusinessAddressInput a;
  String c;
  LatLng d;
  final h e;
  
  public g(h paramh)
  {
    e = paramh;
  }
  
  public final void a()
  {
    f.a locala = (f.a)b;
    if (locala != null)
    {
      LatLng localLatLng = d;
      locala.b(localLatLng);
      return;
    }
  }
  
  public final void a(f.a parama)
  {
    Object localObject1 = "presenterView";
    k.b(parama, (String)localObject1);
    super.a(parama);
    parama = a;
    if (parama != null)
    {
      localObject1 = e;
      Object localObject2 = parama.getCountryCode();
      localObject1 = ((h)localObject1).a((String)localObject2);
      if (localObject1 != null)
      {
        localObject2 = parama.getCountryCode();
        c = ((String)localObject2);
        localObject2 = parama.getLatitude();
        Object localObject3;
        if (localObject2 != null)
        {
          localObject2 = parama.getLongitude();
          if (localObject2 != null)
          {
            localObject2 = new com/google/android/gms/maps/model/LatLng;
            localObject3 = parama.getLatitude();
            double d1 = ((Double)localObject3).doubleValue();
            Double localDouble = parama.getLongitude();
            double d2 = localDouble.doubleValue();
            ((LatLng)localObject2).<init>(d1, d2);
            d = ((LatLng)localObject2);
          }
        }
        localObject2 = (f.a)b;
        if (localObject2 != null)
        {
          localObject3 = parama.getStreet();
          String str = parama.getZipCode();
          parama = parama.getCity();
          ((f.a)localObject2).a((String)localObject3, str, parama);
        }
        parama = (f.a)b;
        if (parama != null)
        {
          parama.a((String)localObject1);
          return;
        }
      }
      return;
    }
    parama = e.a();
    localObject1 = e.b();
    if ((parama != null) && (localObject1 != null))
    {
      c = parama;
      parama = (f.a)b;
      if (parama != null)
      {
        parama.a((String)localObject1);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */