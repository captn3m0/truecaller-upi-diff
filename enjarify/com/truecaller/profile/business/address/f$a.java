package com.truecaller.profile.business.address;

import com.google.android.gms.maps.model.LatLng;

public abstract interface f$a
{
  public abstract void a(int paramInt);
  
  public abstract void a(LatLng paramLatLng);
  
  public abstract void a(BusinessAddressInput paramBusinessAddressInput);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString1, String paramString2, String paramString3);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void b();
  
  public abstract void b(LatLng paramLatLng);
  
  public abstract void b(String paramString);
  
  public abstract void c();
  
  public abstract void c(LatLng paramLatLng);
  
  public abstract void e();
  
  public abstract void f();
  
  public abstract void g();
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */