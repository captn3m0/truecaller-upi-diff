package com.truecaller.profile.business.address;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class GeocodedBusinessAddress$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    GeocodedBusinessAddress localGeocodedBusinessAddress = new com/truecaller/profile/business/address/GeocodedBusinessAddress;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    double d1 = paramParcel.readDouble();
    double d2 = paramParcel.readDouble();
    localGeocodedBusinessAddress.<init>(str1, str2, str3, str4, d1, d2);
    return localGeocodedBusinessAddress;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new GeocodedBusinessAddress[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.GeocodedBusinessAddress.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */