package com.truecaller.profile.business.address;

import android.content.Context;
import android.location.Geocoder;
import c.d.c;
import c.d.f;
import c.g.a.m;
import com.truecaller.common.network.country.CountryListDto.a;
import kotlinx.coroutines.g;

public final class i
  implements h
{
  final Geocoder a;
  private final Context b;
  private final f c;
  
  public i(Context paramContext, Geocoder paramGeocoder, f paramf)
  {
    b = paramContext;
    a = paramGeocoder;
    c = paramf;
  }
  
  public final Object a(double paramDouble1, double paramDouble2, c paramc)
  {
    f localf = c;
    Object localObject = new com/truecaller/profile/business/address/i$a;
    ((i.a)localObject).<init>(this, paramDouble1, paramDouble2, null);
    localObject = (m)localObject;
    return g.a(localf, (m)localObject, paramc);
  }
  
  public final String a()
  {
    CountryListDto.a locala = com.truecaller.common.h.h.c(b);
    if (locala != null) {
      return c;
    }
    return null;
  }
  
  public final String a(String paramString)
  {
    paramString = com.truecaller.common.h.h.a(paramString);
    if (paramString != null) {
      return b;
    }
    return null;
  }
  
  public final String b()
  {
    CountryListDto.a locala = com.truecaller.common.h.h.c(b);
    if (locala != null) {
      return b;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */