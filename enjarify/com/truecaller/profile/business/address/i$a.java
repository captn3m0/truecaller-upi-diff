package com.truecaller.profile.business.address;

import android.location.Address;
import android.location.Geocoder;
import c.d.a.a;
import c.d.c;
import c.g.a.b;
import c.m.l;
import c.o.b;
import c.u;
import c.x;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class i$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag e;
  
  i$a(i parami, double paramDouble1, double paramDouble2, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/profile/business/address/i$a;
    i locali = b;
    double d1 = c;
    double d2 = d;
    locala.<init>(locali, d1, d2, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = paramObject;
    Object localObject2 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        localObject2 = b.a;
        double d1 = c;
        double d2 = d;
        int j = 10;
        localObject1 = ((Geocoder)localObject2).getFromLocation(d1, d2, j);
        bool1 = false;
        localObject2 = null;
        if (localObject1 != null)
        {
          Object localObject3 = localObject1;
          localObject3 = (Collection)localObject1;
          boolean bool2 = ((Collection)localObject3).isEmpty();
          int k = 1;
          bool2 ^= k;
          if (bool2)
          {
            localObject3 = localObject1;
            localObject3 = (Iterable)localObject1;
            Object localObject4 = c.a.m.n((Iterable)localObject3);
            Object localObject5 = (b)i.c.a;
            localObject4 = l.c((c.m.i)localObject4, (b)localObject5).a();
            int n;
            label217:
            do
            {
              bool3 = ((Iterator)localObject4).hasNext();
              j = 0;
              if (!bool3) {
                break;
              }
              localObject5 = ((Iterator)localObject4).next();
              localObject6 = localObject5;
              localObject6 = (CharSequence)localObject5;
              if (localObject6 != null)
              {
                n = ((CharSequence)localObject6).length();
                if (n != 0)
                {
                  n = 0;
                  localObject6 = null;
                  break label217;
                }
              }
              n = 1;
              n ^= k;
            } while (n == 0);
            break label238;
            boolean bool3 = false;
            localObject5 = null;
            label238:
            Object localObject7 = localObject5;
            localObject7 = (String)localObject5;
            localObject4 = c.a.m.n((Iterable)localObject3);
            localObject5 = (b)i.d.a;
            localObject4 = l.c((c.m.i)localObject4, (b)localObject5).a();
            int i1;
            label349:
            do
            {
              bool3 = ((Iterator)localObject4).hasNext();
              if (!bool3) {
                break;
              }
              localObject5 = ((Iterator)localObject4).next();
              localObject6 = localObject5;
              localObject6 = (CharSequence)localObject5;
              if (localObject6 != null)
              {
                i1 = ((CharSequence)localObject6).length();
                if (i1 != 0)
                {
                  i1 = 0;
                  localObject6 = null;
                  break label349;
                }
              }
              i1 = 1;
              i1 ^= k;
            } while (i1 == 0);
            break label370;
            bool3 = false;
            localObject5 = null;
            label370:
            Object localObject8 = localObject5;
            localObject8 = (String)localObject5;
            localObject3 = c.a.m.n((Iterable)localObject3);
            localObject4 = (b)i.b.a;
            localObject3 = l.c((c.m.i)localObject3, (b)localObject4).a();
            int m;
            label481:
            do
            {
              bool4 = ((Iterator)localObject3).hasNext();
              if (!bool4) {
                break;
              }
              localObject4 = ((Iterator)localObject3).next();
              localObject5 = localObject4;
              localObject5 = (CharSequence)localObject4;
              if (localObject5 != null)
              {
                m = ((CharSequence)localObject5).length();
                if (m != 0)
                {
                  m = 0;
                  localObject5 = null;
                  break label481;
                }
              }
              m = 1;
              m ^= k;
            } while (m == 0);
            break label502;
            boolean bool4 = false;
            localObject4 = null;
            label502:
            Object localObject9 = localObject4;
            localObject9 = (String)localObject4;
            localObject3 = c.a.m.d((List)localObject1);
            String str = "addresses.first()";
            c.g.b.k.a(localObject3, str);
            localObject3 = ((Address)localObject3).getCountryCode();
            if (localObject3 != null) {
              if (localObject3 != null)
              {
                localObject2 = ((String)localObject3).toLowerCase();
                localObject3 = "(this as java.lang.String).toLowerCase()";
                c.g.b.k.a(localObject2, (String)localObject3);
              }
              else
              {
                localObject1 = new c/u;
                ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
                throw ((Throwable)localObject1);
              }
            }
            Object localObject10 = localObject2;
            localObject2 = c.a.m.d((List)localObject1);
            c.g.b.k.a(localObject2, "addresses.first()");
            double d3 = ((Address)localObject2).getLatitude();
            localObject1 = c.a.m.d((List)localObject1);
            c.g.b.k.a(localObject1, "addresses.first()");
            double d4 = ((Address)localObject1).getLongitude();
            localObject1 = new com/truecaller/profile/business/address/GeocodedBusinessAddress;
            Object localObject6 = localObject1;
            ((GeocodedBusinessAddress)localObject1).<init>((String)localObject7, (String)localObject8, (String)localObject9, (String)localObject10, d3, d4);
            return localObject1;
          }
        }
        return null;
      }
      throw a;
    }
    localObject1 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)localObject1);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */