package com.truecaller.profile.business.address;

import com.google.android.gms.maps.model.LatLng;

public abstract interface d$a
{
  public abstract void a(int paramInt);
  
  public abstract void a(LatLng paramLatLng);
  
  public abstract void a(GeocodedBusinessAddress paramGeocodedBusinessAddress);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void b();
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */