package com.truecaller.profile.business.address;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.o;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import c.u;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition.Builder;
import com.google.android.gms.maps.model.LatLng;
import com.truecaller.R.id;
import com.truecaller.profile.business.i;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class b
  extends Fragment
  implements f.a, k
{
  public static final b.a b;
  public g a;
  private View c;
  private a d;
  private GoogleMap e;
  private HashMap f;
  
  static
  {
    b.a locala = new com/truecaller/profile/business/address/b$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private final void d(LatLng paramLatLng)
  {
    Object localObject = new com/google/android/gms/maps/model/CameraPosition$Builder;
    ((CameraPosition.Builder)localObject).<init>();
    paramLatLng = ((CameraPosition.Builder)localObject).a().a(paramLatLng).b();
    localObject = e;
    if (localObject != null)
    {
      paramLatLng = CameraUpdateFactory.a(paramLatLng);
      ((GoogleMap)localObject).a(paramLatLng);
      return;
    }
  }
  
  public final g a()
  {
    g localg = a;
    if (localg == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return localg;
  }
  
  public final void a(int paramInt)
  {
    Object localObject = c;
    if (localObject == null)
    {
      String str = "rootView";
      c.g.b.k.a(str);
    }
    EditText localEditText = (EditText)((View)localObject).findViewById(paramInt);
    localObject = (CharSequence)getString(2131886247);
    localEditText.setError((CharSequence)localObject);
    localEditText.requestFocus();
  }
  
  public final void a(LatLng paramLatLng)
  {
    c.g.b.k.b(paramLatLng, "latLng");
    d(paramLatLng);
    int i = R.id.mapViewMarker;
    paramLatLng = (ImageView)b(i);
    c.g.b.k.a(paramLatLng, "mapViewMarker");
    t.a((View)paramLatLng);
    i = R.id.mapLocationButton;
    paramLatLng = (Button)b(i);
    c.g.b.k.a(paramLatLng, "mapLocationButton");
    t.b((View)paramLatLng);
  }
  
  public final void a(BusinessAddressInput paramBusinessAddressInput)
  {
    c.g.b.k.b(paramBusinessAddressInput, "address");
    a locala = d;
    if (locala == null)
    {
      String str = "businessAddressListener";
      c.g.b.k.a(str);
    }
    locala.b(paramBusinessAddressInput);
    paramBusinessAddressInput = getView();
    if (paramBusinessAddressInput != null)
    {
      t.a(paramBusinessAddressInput, false, 2);
      return;
    }
  }
  
  public final void a(GeocodedBusinessAddress paramGeocodedBusinessAddress)
  {
    c.g.b.k.b(paramGeocodedBusinessAddress, "address");
    g localg = a;
    if (localg == null)
    {
      localObject1 = "presenter";
      c.g.b.k.a((String)localObject1);
    }
    c.g.b.k.b(paramGeocodedBusinessAddress, "address");
    Object localObject1 = paramGeocodedBusinessAddress.getCountryCode();
    c = ((String)localObject1);
    localObject1 = new com/google/android/gms/maps/model/LatLng;
    double d1 = paramGeocodedBusinessAddress.getLatitude();
    double d2 = paramGeocodedBusinessAddress.getLongitude();
    ((LatLng)localObject1).<init>(d1, d2);
    d = ((LatLng)localObject1);
    localObject1 = (f.a)b;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = paramGeocodedBusinessAddress.getStreet();
      String str1 = paramGeocodedBusinessAddress.getZipCode();
      String str2 = paramGeocodedBusinessAddress.getCity();
      Object localObject3 = e;
      String str3 = paramGeocodedBusinessAddress.getCountryCode();
      localObject3 = ((h)localObject3).a(str3);
      ((f.a)localObject1).a((String)localObject2, str1, str2, (String)localObject3);
    }
    localObject1 = (f.a)b;
    if (localObject1 != null)
    {
      localObject2 = new com/google/android/gms/maps/model/LatLng;
      double d3 = paramGeocodedBusinessAddress.getLatitude();
      double d4 = paramGeocodedBusinessAddress.getLongitude();
      ((LatLng)localObject2).<init>(d3, d4);
      ((f.a)localObject1).c((LatLng)localObject2);
    }
    paramGeocodedBusinessAddress = (f.a)b;
    if (paramGeocodedBusinessAddress != null)
    {
      paramGeocodedBusinessAddress.e();
      return;
    }
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "countryName");
    int i = R.id.countryEditText;
    TextView localTextView = (TextView)b(i);
    c.g.b.k.a(localTextView, "countryEditText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    int i = R.id.streetEditText;
    TextInputEditText localTextInputEditText = (TextInputEditText)b(i);
    paramString1 = (CharSequence)paramString1;
    localTextInputEditText.setText(paramString1);
    int j = R.id.zipCodeEditText;
    paramString1 = (TextInputEditText)b(j);
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
    j = R.id.cityEditText;
    paramString1 = (TextInputEditText)b(j);
    paramString3 = (CharSequence)paramString3;
    paramString1.setText(paramString3);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    int i = R.id.streetEditText;
    TextInputEditText localTextInputEditText = (TextInputEditText)b(i);
    paramString1 = (CharSequence)paramString1;
    localTextInputEditText.setText(paramString1);
    int j = R.id.zipCodeEditText;
    paramString1 = (TextInputEditText)b(j);
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
    j = R.id.cityEditText;
    paramString1 = (TextInputEditText)b(j);
    paramString3 = (CharSequence)paramString3;
    paramString1.setText(paramString3);
    j = R.id.countryEditText;
    paramString1 = (TextView)b(j);
    c.g.b.k.a(paramString1, "countryEditText");
    paramString4 = (CharSequence)paramString4;
    paramString1.setText(paramString4);
  }
  
  public final View b(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    int i = R.id.mapViewMarker;
    Object localObject = (ImageView)b(i);
    c.g.b.k.a(localObject, "mapViewMarker");
    t.b((View)localObject);
    i = R.id.mapLocationButton;
    localObject = (Button)b(i);
    c.g.b.k.a(localObject, "mapLocationButton");
    t.a((View)localObject);
  }
  
  public final void b(LatLng paramLatLng)
  {
    Object localObject = c.b;
    localObject = new com/truecaller/profile/business/address/c;
    ((c)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramLatLng = (Parcelable)paramLatLng;
    localBundle.putParcelable("arg_latlng", paramLatLng);
    ((c)localObject).setArguments(localBundle);
    paramLatLng = getChildFragmentManager().a().a(0, 0, 0, 2130771992);
    localObject = (Fragment)localObject;
    paramLatLng = paramLatLng.b(2131362268, (Fragment)localObject);
    localObject = c.class.getName();
    paramLatLng.a((String)localObject).d();
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "countryName");
    int i = R.id.countryEditText;
    TextView localTextView = (TextView)b(i);
    String str = "countryEditText";
    c.g.b.k.a(localTextView, str);
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    int j = R.id.countryEditText;
    paramString = (TextView)b(j);
    c.g.b.k.a(paramString, "countryEditText");
    i = 0;
    localTextView = null;
    paramString.setError(null);
    paramString = getView();
    if (paramString != null)
    {
      t.a(paramString, false, 2);
      return;
    }
  }
  
  public final void c()
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      Object localObject2 = new com/truecaller/common/ui/a/b$a;
      localObject1 = (Context)localObject1;
      ((com.truecaller.common.ui.a.b.a)localObject2).<init>((Context)localObject1);
      localObject1 = ((com.truecaller.common.ui.a.b.a)localObject2).a().b();
      localObject2 = new com/truecaller/profile/business/address/b$f;
      ((b.f)localObject2).<init>(this);
      localObject2 = (AdapterView.OnItemClickListener)localObject2;
      ((com.truecaller.common.ui.a.b.a)localObject1).a((AdapterView.OnItemClickListener)localObject2).c();
      return;
    }
  }
  
  public final void c(LatLng paramLatLng)
  {
    c.g.b.k.b(paramLatLng, "latLng");
    Object localObject = e;
    if (localObject != null)
    {
      d(paramLatLng);
      int i = R.id.mapViewMarker;
      paramLatLng = (ImageView)b(i);
      c.g.b.k.a(paramLatLng, "mapViewMarker");
      t.a((View)paramLatLng);
      i = R.id.mapLocationButton;
      paramLatLng = (Button)b(i);
      localObject = "mapLocationButton";
      c.g.b.k.a(paramLatLng, (String)localObject);
      paramLatLng = (View)paramLatLng;
      t.b(paramLatLng);
    }
  }
  
  public final void d()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localObject = (f.a)b;
    if (localObject != null)
    {
      ((f.a)localObject).e();
      return;
    }
  }
  
  public final void e()
  {
    android.support.v4.app.j localj = getChildFragmentManager();
    String str = "childFragmentManager";
    c.g.b.k.a(localj, str);
    int i = localj.e();
    if (i > 0)
    {
      localj = getChildFragmentManager();
      str = c.class.getName();
      localj.b(str);
    }
  }
  
  public final void f()
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "rootView";
      c.g.b.k.a((String)localObject2);
    }
    localObject1 = (TextView)((View)localObject1).findViewById(2131362610);
    Object localObject2 = (CharSequence)getString(2131886247);
    ((TextView)localObject1).setError((CharSequence)localObject2);
    ((TextView)localObject1).requestFocus();
  }
  
  public final void g()
  {
    Object localObject = d;
    if (localObject == null)
    {
      String str = "businessAddressListener";
      c.g.b.k.a(str);
    }
    ((a)localObject).j();
    localObject = getView();
    if (localObject != null)
    {
      t.a((View)localObject, false, 2);
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = (a)paramContext;
    d = paramContext;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      paramBundle = com.truecaller.profile.business.j.a((Context)paramBundle);
      if (paramBundle != null) {
        paramBundle.a(this);
      }
    }
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      c.g.b.k.a((String)localObject);
    }
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "arg_address";
      localObject = (BusinessAddressInput)((Bundle)localObject).getParcelable(str);
    }
    else
    {
      localObject = null;
    }
    a = ((BusinessAddressInput)localObject);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramBundle = "inflater";
    c.g.b.k.b(paramLayoutInflater, paramBundle);
    int i = 2131558513;
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    paramViewGroup = "inflater.inflate(R.layou…ddress, container, false)";
    c.g.b.k.a(paramLayoutInflater, paramViewGroup);
    c = paramLayoutInflater;
    paramLayoutInflater = c;
    if (paramLayoutInflater == null)
    {
      paramViewGroup = "rootView";
      c.g.b.k.a(paramViewGroup);
    }
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    ((g)localObject).y_();
    localObject = f;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    c.g.b.k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    int i = 2;
    t.a(paramView, false, i);
    int j = R.id.countryEditText;
    paramView = (TextView)b(j);
    paramBundle = new com/truecaller/profile/business/address/b$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnTouchListener)paramBundle;
    paramView.setOnTouchListener(paramBundle);
    paramView = getChildFragmentManager();
    int k = 2131363726;
    paramView = paramView.a(k);
    if (paramView != null)
    {
      paramView = (SupportMapFragment)paramView;
      paramBundle = new com/truecaller/profile/business/address/b$e;
      paramBundle.<init>(this);
      paramBundle = (OnMapReadyCallback)paramBundle;
      paramView.a(paramBundle);
      paramView = a;
      if (paramView == null)
      {
        paramBundle = "presenter";
        c.g.b.k.a(paramBundle);
      }
      paramBundle = this;
      paramBundle = (f.a)this;
      paramView.a(paramBundle);
      j = R.id.submitButton;
      paramView = (ImageButton)b(j);
      paramBundle = new com/truecaller/profile/business/address/b$b;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      j = R.id.cancelButton;
      paramView = (ImageButton)b(j);
      paramBundle = new com/truecaller/profile/business/address/b$c;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.google.android.gms.maps.SupportMapFragment");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */