package com.truecaller.profile.business.address;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import c.u;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition.Builder;
import com.google.android.gms.maps.model.LatLng;
import com.truecaller.R.id;
import com.truecaller.common.h.am;
import com.truecaller.profile.business.i;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class c
  extends Fragment
  implements d.a
{
  public static final c.a b;
  public e a;
  private View c;
  private k d;
  private GoogleMap e;
  private HashMap f;
  
  static
  {
    c.a locala = new com/truecaller/profile/business/address/c$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View b(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  private void f()
  {
    int i = R.id.loadingProgressBar;
    FrameLayout localFrameLayout = (FrameLayout)b(i);
    c.g.b.k.a(localFrameLayout, "loadingProgressBar");
    t.c((View)localFrameLayout);
  }
  
  public final e a()
  {
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locale;
  }
  
  public final void a(int paramInt)
  {
    f();
    int i = R.id.addressTextView;
    TextView localTextView = (TextView)b(i);
    c.g.b.k.a(localTextView, "addressTextView");
    CharSequence localCharSequence = (CharSequence)getString(paramInt);
    localTextView.setText(localCharSequence);
  }
  
  public final void a(LatLng paramLatLng)
  {
    c.g.b.k.b(paramLatLng, "latLng");
    Object localObject = new com/google/android/gms/maps/model/CameraPosition$Builder;
    ((CameraPosition.Builder)localObject).<init>();
    paramLatLng = ((CameraPosition.Builder)localObject).a().a(paramLatLng).b();
    localObject = e;
    if (localObject != null)
    {
      paramLatLng = CameraUpdateFactory.a(paramLatLng);
      ((GoogleMap)localObject).a(paramLatLng);
      return;
    }
  }
  
  public final void a(GeocodedBusinessAddress paramGeocodedBusinessAddress)
  {
    c.g.b.k.b(paramGeocodedBusinessAddress, "address");
    k localk = d;
    if (localk == null)
    {
      String str = "listener";
      c.g.b.k.a(str);
    }
    localk.a(paramGeocodedBusinessAddress);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    f();
    int i = R.id.addressTextView;
    TextView localTextView = (TextView)b(i);
    c.g.b.k.a(localTextView, "addressTextView");
    String[] arrayOfString = new String[4];
    arrayOfString[0] = paramString1;
    arrayOfString[1] = paramString2;
    arrayOfString[2] = paramString3;
    arrayOfString[3] = paramString4;
    paramString1 = (CharSequence)am.a(arrayOfString);
    localTextView.setText(paramString1);
  }
  
  public final void b()
  {
    GoogleMap localGoogleMap = e;
    if (localGoogleMap != null)
    {
      Object localObject = new com/truecaller/profile/business/address/c$d;
      ((c.d)localObject).<init>(localGoogleMap, this);
      localObject = (GoogleMap.OnCameraIdleListener)localObject;
      localGoogleMap.a((GoogleMap.OnCameraIdleListener)localObject);
      return;
    }
  }
  
  public final void c()
  {
    int i = R.id.loadingProgressBar;
    FrameLayout localFrameLayout = (FrameLayout)b(i);
    c.g.b.k.a(localFrameLayout, "loadingProgressBar");
    t.a((View)localFrameLayout);
  }
  
  public final void d()
  {
    View localView = c;
    if (localView == null)
    {
      String str = "rootView";
      c.g.b.k.a(str);
    }
    Snackbar.a(localView, 2131886254, -1).c();
  }
  
  public final void e()
  {
    k localk = d;
    if (localk == null)
    {
      String str = "listener";
      c.g.b.k.a(str);
    }
    localk.d();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      paramBundle = com.truecaller.profile.business.j.a((Context)paramBundle);
      if (paramBundle != null) {
        paramBundle.a(this);
      }
    }
    paramBundle = getParentFragment();
    boolean bool;
    if (paramBundle != null)
    {
      localObject = "it";
      c.g.b.k.a(paramBundle, (String)localObject);
      bool = paramBundle instanceof k;
      if (bool)
      {
        paramBundle = (k)paramBundle;
        d = paramBundle;
      }
    }
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      c.g.b.k.a((String)localObject);
    }
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "arg_latlng";
      localObject = (LatLng)((Bundle)localObject).getParcelable(str);
    }
    else
    {
      bool = false;
      localObject = null;
    }
    c = ((LatLng)localObject);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramBundle = "inflater";
    c.g.b.k.b(paramLayoutInflater, paramBundle);
    int i = 2131558514;
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    paramViewGroup = "inflater.inflate(R.layou…cation, container, false)";
    c.g.b.k.a(paramLayoutInflater, paramViewGroup);
    c = paramLayoutInflater;
    paramLayoutInflater = c;
    if (paramLayoutInflater == null)
    {
      paramViewGroup = "rootView";
      c.g.b.k.a(paramViewGroup);
    }
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    ((e)localObject).y_();
    localObject = f;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    c.g.b.k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramBundle = a;
    if (paramBundle == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    paramBundle.a(this);
    paramBundle = null;
    int i = 2;
    t.a(paramView, false, i);
    paramView = getChildFragmentManager();
    int j = 2131363726;
    paramView = paramView.a(j);
    if (paramView != null)
    {
      paramView = (SupportMapFragment)paramView;
      paramBundle = new com/truecaller/profile/business/address/c$e;
      paramBundle.<init>(this);
      paramBundle = (OnMapReadyCallback)paramBundle;
      paramView.a(paramBundle);
      int k = R.id.submitButton;
      paramView = (ImageButton)b(k);
      paramBundle = new com/truecaller/profile/business/address/c$b;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      k = R.id.cancelButton;
      paramView = (ImageButton)b(k);
      paramBundle = new com/truecaller/profile/business/address/c$c;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.google.android.gms.maps.SupportMapFragment");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.address.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */