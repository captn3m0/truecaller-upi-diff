package com.truecaller.profile.business;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final k a;
  private final Provider b;
  
  private m(k paramk, Provider paramProvider)
  {
    a = paramk;
    b = paramProvider;
  }
  
  public static m a(k paramk, Provider paramProvider)
  {
    m localm = new com/truecaller/profile/business/m;
    localm.<init>(paramk, paramProvider);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */