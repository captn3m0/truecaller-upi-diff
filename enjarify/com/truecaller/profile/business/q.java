package com.truecaller.profile.business;

import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.g.a;
import com.truecaller.common.h.ac;
import com.truecaller.profile.BusinessProfileOnboardingActivity;
import com.truecaller.profile.business.a.d;
import com.truecaller.profile.business.address.h;
import com.truecaller.profile.business.address.j;
import com.truecaller.profile.business.openHours.OpenHoursFragment;
import javax.inject.Provider;

public final class q
  implements i
{
  private final bp a;
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  
  private q(k paramk, bp parambp)
  {
    a = parambp;
    Object localObject = dagger.a.c.a(com.truecaller.profile.business.a.c.a());
    b = ((Provider)localObject);
    localObject = new com/truecaller/profile/business/q$g;
    ((q.g)localObject).<init>(parambp);
    c = ((Provider)localObject);
    localObject = new com/truecaller/profile/business/q$f;
    ((q.f)localObject).<init>(parambp);
    d = ((Provider)localObject);
    localObject = dagger.a.c.a(l.a(paramk));
    e = ((Provider)localObject);
    localObject = new com/truecaller/profile/business/q$e;
    ((q.e)localObject).<init>(parambp);
    f = ((Provider)localObject);
    localObject = b;
    Provider localProvider1 = c;
    Provider localProvider2 = d;
    Provider localProvider3 = e;
    Provider localProvider4 = f;
    localObject = com.truecaller.profile.business.a.f.a((Provider)localObject, localProvider1, localProvider2, localProvider3, localProvider4);
    g = ((Provider)localObject);
    localObject = dagger.a.c.a(g);
    h = ((Provider)localObject);
    localObject = new com/truecaller/profile/business/q$c;
    ((q.c)localObject).<init>(parambp);
    i = ((Provider)localObject);
    localObject = i;
    paramk = dagger.a.c.a(m.a(paramk, (Provider)localObject));
    j = paramk;
    paramk = new com/truecaller/profile/business/q$d;
    paramk.<init>(parambp);
    k = paramk;
    paramk = i;
    localObject = j;
    localProvider1 = k;
    paramk = j.a(paramk, (Provider)localObject, localProvider1);
    l = paramk;
    paramk = dagger.a.c.a(l);
    m = paramk;
    paramk = new com/truecaller/profile/business/q$b;
    paramk.<init>(parambp);
    n = paramk;
    paramk = s.a(n);
    o = paramk;
    paramk = dagger.a.c.a(o);
    p = paramk;
    paramk = dagger.a.c.a(com.truecaller.profile.business.openHours.i.a());
    q = paramk;
  }
  
  public static q.a a()
  {
    q.a locala = new com/truecaller/profile/business/q$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final void a(BusinessProfileOnboardingActivity paramBusinessProfileOnboardingActivity)
  {
    Object localObject = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.aF(), "Cannot return null from a non-@Nullable component method");
    a = ((com.truecaller.featuretoggles.e)localObject);
    localObject = (com.truecaller.analytics.b)dagger.a.g.a(a.c(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.analytics.b)localObject);
  }
  
  public final void a(CreateBusinessProfileActivity paramCreateBusinessProfileActivity)
  {
    o localo = new com/truecaller/profile/business/o;
    Object localObject1 = h.get();
    Object localObject2 = localObject1;
    localObject2 = (d)localObject1;
    localObject1 = m.get();
    Object localObject3 = localObject1;
    localObject3 = (h)localObject1;
    localObject1 = p.get();
    Object localObject4 = localObject1;
    localObject4 = (e)localObject1;
    localObject1 = dagger.a.g.a(a.J(), "Cannot return null from a non-@Nullable component method");
    Object localObject5 = localObject1;
    localObject5 = (ac)localObject1;
    localObject1 = dagger.a.g.a(a.bl(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject1;
    localObject6 = (c.d.f)localObject1;
    localObject1 = dagger.a.g.a(a.aF(), "Cannot return null from a non-@Nullable component method");
    Object localObject7 = localObject1;
    localObject7 = (com.truecaller.featuretoggles.e)localObject1;
    localObject1 = localo;
    localo.<init>((d)localObject2, (h)localObject3, (e)localObject4, (ac)localObject5, (c.d.f)localObject6, (com.truecaller.featuretoggles.e)localObject7);
    a = localo;
    localObject1 = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.aF(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.featuretoggles.e)localObject1);
    localObject1 = (com.truecaller.util.g)dagger.a.g.a(a.aQ(), "Cannot return null from a non-@Nullable component method");
    c = ((com.truecaller.util.g)localObject1);
    localObject1 = (a)dagger.a.g.a(a.I(), "Cannot return null from a non-@Nullable component method");
    d = ((a)localObject1);
    localObject1 = (r)dagger.a.g.a(a.T(), "Cannot return null from a non-@Nullable component method");
    e = ((r)localObject1);
    localObject1 = (com.truecaller.analytics.b)dagger.a.g.a(a.c(), "Cannot return null from a non-@Nullable component method");
    f = ((com.truecaller.analytics.b)localObject1);
    localObject1 = (c.d.f)dagger.a.g.a(a.bm(), "Cannot return null from a non-@Nullable component method");
    g = ((c.d.f)localObject1);
    localObject1 = (c.d.f)dagger.a.g.a(a.bl(), "Cannot return null from a non-@Nullable component method");
    h = ((c.d.f)localObject1);
  }
  
  public final void a(com.truecaller.profile.business.address.b paramb)
  {
    com.truecaller.profile.business.address.g localg = new com/truecaller/profile/business/address/g;
    h localh = (h)m.get();
    localg.<init>(localh);
    a = localg;
  }
  
  public final void a(com.truecaller.profile.business.address.c paramc)
  {
    com.truecaller.profile.business.address.e locale = new com/truecaller/profile/business/address/e;
    h localh = (h)m.get();
    c.d.f localf = (c.d.f)dagger.a.g.a(a.bl(), "Cannot return null from a non-@Nullable component method");
    locale.<init>(localh, localf);
    a = locale;
  }
  
  public final void a(OpenHoursFragment paramOpenHoursFragment)
  {
    com.truecaller.profile.business.openHours.f localf = new com/truecaller/profile/business/openHours/f;
    com.truecaller.profile.business.openHours.g localg = (com.truecaller.profile.business.openHours.g)q.get();
    localf.<init>(localg);
    a = localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */