package com.truecaller.profile.business;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import c.g.b.k;
import c.u;

final class CreateBusinessProfileActivity$d
  implements ValueAnimator.AnimatorUpdateListener
{
  CreateBusinessProfileActivity$d(View paramView) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    ViewGroup.LayoutParams localLayoutParams = a.getLayoutParams();
    String str = "animation";
    k.a(paramValueAnimator, str);
    paramValueAnimator = paramValueAnimator.getAnimatedValue();
    if (paramValueAnimator != null)
    {
      int i = ((Integer)paramValueAnimator).intValue();
      height = i;
      a.requestLayout();
      return;
    }
    paramValueAnimator = new c/u;
    paramValueAnimator.<init>("null cannot be cast to non-null type kotlin.Int");
    throw paramValueAnimator;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.CreateBusinessProfileActivity.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */