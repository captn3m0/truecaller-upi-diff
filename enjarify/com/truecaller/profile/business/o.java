package com.truecaller.profile.business;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.g.b.v.c;
import com.truecaller.ba;
import com.truecaller.common.h.ac;
import com.truecaller.profile.business.a.d;
import com.truecaller.profile.business.address.BusinessAddressInput;
import com.truecaller.profile.business.address.h;
import com.truecaller.profile.data.dto.Branding;
import com.truecaller.profile.data.dto.BusinessData;
import com.truecaller.profile.data.dto.Company;
import com.truecaller.profile.data.dto.Profile;
import java.util.ArrayList;
import java.util.List;

public final class o
  extends ba
{
  Profile c;
  BusinessAddressInput d;
  boolean e;
  Integer f;
  boolean g;
  final List h;
  int i;
  final d j;
  final ac k;
  final com.truecaller.featuretoggles.e l;
  private final h m;
  private final e n;
  private final f o;
  
  public o(d paramd, h paramh, e parame, ac paramac, f paramf, com.truecaller.featuretoggles.e parame1)
  {
    super(paramf);
    j = paramd;
    m = paramh;
    n = parame;
    k = paramac;
    o = paramf;
    l = parame1;
    paramd = new java/util/ArrayList;
    paramd.<init>();
    paramd = (List)paramd;
    h = paramd;
    i = 2;
  }
  
  public final void a()
  {
    n.a locala = (n.a)b;
    if (locala != null)
    {
      BusinessAddressInput localBusinessAddressInput = d;
      locala.a(localBusinessAddressInput);
      return;
    }
  }
  
  public final void a(BusinessAddressInput paramBusinessAddressInput)
  {
    k.b(paramBusinessAddressInput, "address");
    d = paramBusinessAddressInput;
    Object localObject1 = m;
    Object localObject2 = paramBusinessAddressInput.getCountryCode();
    localObject1 = ((h)localObject1).a((String)localObject2);
    if (localObject1 != null)
    {
      localObject2 = (n.a)b;
      if (localObject2 != null)
      {
        String str1 = paramBusinessAddressInput.getStreet();
        String str2 = paramBusinessAddressInput.getZipCode();
        String str3 = paramBusinessAddressInput.getCity();
        ((n.a)localObject2).a(str1, str2, str3, (String)localObject1);
      }
    }
    localObject1 = paramBusinessAddressInput.getLatitude();
    if (localObject1 != null)
    {
      localObject1 = paramBusinessAddressInput.getLongitude();
      if (localObject1 != null)
      {
        localObject1 = (n.a)b;
        if (localObject1 != null)
        {
          localObject2 = paramBusinessAddressInput.getLatitude();
          double d1 = ((Double)localObject2).doubleValue();
          paramBusinessAddressInput = paramBusinessAddressInput.getLongitude();
          double d2 = paramBusinessAddressInput.doubleValue();
          ((n.a)localObject1).a(d1, d2);
        }
      }
    }
    paramBusinessAddressInput = (n.a)b;
    if (paramBusinessAddressInput != null) {
      paramBusinessAddressInput.h();
    }
    paramBusinessAddressInput = (n.a)b;
    if (paramBusinessAddressInput != null)
    {
      paramBusinessAddressInput.k();
      return;
    }
  }
  
  public final void e()
  {
    Object localObject1 = (n.a)b;
    if (localObject1 != null) {
      ((n.a)localObject1).p();
    }
    v.c localc = new c/g/b/v$c;
    localc.<init>();
    localObject1 = c;
    int i1 = 2131886241;
    if (localObject1 == null)
    {
      localObject1 = this;
      localObject1 = (n.a)b;
      if (localObject1 != null)
      {
        n.a.a.a((n.a)localObject1, i1);
        return;
      }
      return;
    }
    a = localObject1;
    BusinessData localBusinessData = ((Profile)a).getBusinessData();
    localObject1 = null;
    Object localObject2;
    Object localObject3;
    if (localBusinessData != null)
    {
      localObject2 = localBusinessData.getCompany();
      localObject3 = localObject2;
    }
    else
    {
      localObject3 = null;
    }
    Object localObject4;
    if (localObject3 != null)
    {
      localObject2 = ((Company)localObject3).getBranding();
      localObject4 = localObject2;
    }
    else
    {
      localObject4 = null;
    }
    if ((localBusinessData != null) && (localObject3 != null) && (localObject4 != null))
    {
      Object localObject5 = new com/truecaller/profile/business/o$a;
      localObject2 = this;
      ((o.a)localObject5).<init>(this, localBusinessData, (Branding)localObject4, localc, (Company)localObject3, null);
      localObject5 = (m)localObject5;
      kotlinx.coroutines.e.b(this, null, (m)localObject5, 3);
      return;
    }
    localObject1 = (n.a)b;
    if (localObject1 != null)
    {
      n.a.a.a((n.a)localObject1, i1);
      return;
    }
  }
  
  public final void f()
  {
    n.a locala = (n.a)b;
    if (locala != null)
    {
      locala.r();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.business.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */