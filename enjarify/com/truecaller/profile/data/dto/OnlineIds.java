package com.truecaller.profile.data.dto;

import c.g.b.k;

public final class OnlineIds
{
  private final String email;
  private final String facebookId;
  private final String twitterId;
  private final String url;
  
  public OnlineIds(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    facebookId = paramString1;
    email = paramString2;
    url = paramString3;
    twitterId = paramString4;
  }
  
  public final String component1()
  {
    return facebookId;
  }
  
  public final String component2()
  {
    return email;
  }
  
  public final String component3()
  {
    return url;
  }
  
  public final String component4()
  {
    return twitterId;
  }
  
  public final OnlineIds copy(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    OnlineIds localOnlineIds = new com/truecaller/profile/data/dto/OnlineIds;
    localOnlineIds.<init>(paramString1, paramString2, paramString3, paramString4);
    return localOnlineIds;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof OnlineIds;
      if (bool1)
      {
        paramObject = (OnlineIds)paramObject;
        String str1 = facebookId;
        String str2 = facebookId;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = email;
          str2 = email;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = url;
            str2 = url;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = twitterId;
              paramObject = twitterId;
              boolean bool2 = k.a(str1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final String getEmail()
  {
    return email;
  }
  
  public final String getFacebookId()
  {
    return facebookId;
  }
  
  public final String getTwitterId()
  {
    return twitterId;
  }
  
  public final String getUrl()
  {
    return url;
  }
  
  public final int hashCode()
  {
    String str1 = facebookId;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = email;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = url;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = twitterId;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("OnlineIds(facebookId=");
    String str = facebookId;
    localStringBuilder.append(str);
    localStringBuilder.append(", email=");
    str = email;
    localStringBuilder.append(str);
    localStringBuilder.append(", url=");
    str = url;
    localStringBuilder.append(str);
    localStringBuilder.append(", twitterId=");
    str = twitterId;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.OnlineIds
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */