package com.truecaller.profile.data.dto;

import c.g.b.k;

public final class PhoneNumber
{
  private final String countryCode;
  private final long number;
  
  public PhoneNumber(long paramLong, String paramString)
  {
    number = paramLong;
    countryCode = paramString;
  }
  
  public final long component1()
  {
    return number;
  }
  
  public final String component2()
  {
    return countryCode;
  }
  
  public final PhoneNumber copy(long paramLong, String paramString)
  {
    k.b(paramString, "countryCode");
    PhoneNumber localPhoneNumber = new com/truecaller/profile/data/dto/PhoneNumber;
    localPhoneNumber.<init>(paramLong, paramString);
    return localPhoneNumber;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof PhoneNumber;
      if (bool2)
      {
        paramObject = (PhoneNumber)paramObject;
        long l1 = number;
        long l2 = number;
        bool2 = l1 < l2;
        String str;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          str = null;
        }
        if (bool2)
        {
          str = countryCode;
          paramObject = countryCode;
          boolean bool3 = k.a(str, paramObject);
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getCountryCode()
  {
    return countryCode;
  }
  
  public final long getNumber()
  {
    return number;
  }
  
  public final int hashCode()
  {
    long l1 = number;
    int i = 32;
    long l2 = l1 >>> i;
    l1 ^= l2;
    int j = (int)l1 * 31;
    String str = countryCode;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    return j + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PhoneNumber(number=");
    long l = number;
    localStringBuilder.append(l);
    localStringBuilder.append(", countryCode=");
    String str = countryCode;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.PhoneNumber
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */