package com.truecaller.profile.data.dto;

import c.g.b.k;
import java.util.List;

public final class PersonalDataResponse
{
  private final String about;
  private final Address address;
  private final String avatarUrl;
  private final String birthday;
  private final String companyName;
  private final String gender;
  private final String jobTitle;
  private final OnlineIds onlineIds;
  private final List phoneNumbers;
  private final String privacy;
  private final List tags;
  private final String type;
  
  public PersonalDataResponse(List paramList1, String paramString1, String paramString2, String paramString3, List paramList2, OnlineIds paramOnlineIds, String paramString4, String paramString5, Address paramAddress, String paramString6, String paramString7, String paramString8)
  {
    phoneNumbers = paramList1;
    avatarUrl = paramString1;
    jobTitle = paramString2;
    about = paramString3;
    tags = paramList2;
    onlineIds = paramOnlineIds;
    type = paramString4;
    gender = paramString5;
    address = paramAddress;
    companyName = paramString6;
    birthday = paramString7;
    privacy = paramString8;
  }
  
  public final List component1()
  {
    return phoneNumbers;
  }
  
  public final String component10()
  {
    return companyName;
  }
  
  public final String component11()
  {
    return birthday;
  }
  
  public final String component12()
  {
    return privacy;
  }
  
  public final String component2()
  {
    return avatarUrl;
  }
  
  public final String component3()
  {
    return jobTitle;
  }
  
  public final String component4()
  {
    return about;
  }
  
  public final List component5()
  {
    return tags;
  }
  
  public final OnlineIds component6()
  {
    return onlineIds;
  }
  
  public final String component7()
  {
    return type;
  }
  
  public final String component8()
  {
    return gender;
  }
  
  public final Address component9()
  {
    return address;
  }
  
  public final PersonalDataResponse copy(List paramList1, String paramString1, String paramString2, String paramString3, List paramList2, OnlineIds paramOnlineIds, String paramString4, String paramString5, Address paramAddress, String paramString6, String paramString7, String paramString8)
  {
    k.b(paramList1, "phoneNumbers");
    k.b(paramList2, "tags");
    k.b(paramOnlineIds, "onlineIds");
    k.b(paramString5, "gender");
    k.b(paramAddress, "address");
    k.b(paramString8, "privacy");
    PersonalDataResponse localPersonalDataResponse = new com/truecaller/profile/data/dto/PersonalDataResponse;
    localPersonalDataResponse.<init>(paramList1, paramString1, paramString2, paramString3, paramList2, paramOnlineIds, paramString4, paramString5, paramAddress, paramString6, paramString7, paramString8);
    return localPersonalDataResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PersonalDataResponse;
      if (bool1)
      {
        paramObject = (PersonalDataResponse)paramObject;
        Object localObject1 = phoneNumbers;
        Object localObject2 = phoneNumbers;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = avatarUrl;
          localObject2 = avatarUrl;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = jobTitle;
            localObject2 = jobTitle;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = about;
              localObject2 = about;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = tags;
                localObject2 = tags;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = onlineIds;
                  localObject2 = onlineIds;
                  bool1 = k.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = type;
                    localObject2 = type;
                    bool1 = k.a(localObject1, localObject2);
                    if (bool1)
                    {
                      localObject1 = gender;
                      localObject2 = gender;
                      bool1 = k.a(localObject1, localObject2);
                      if (bool1)
                      {
                        localObject1 = address;
                        localObject2 = address;
                        bool1 = k.a(localObject1, localObject2);
                        if (bool1)
                        {
                          localObject1 = companyName;
                          localObject2 = companyName;
                          bool1 = k.a(localObject1, localObject2);
                          if (bool1)
                          {
                            localObject1 = birthday;
                            localObject2 = birthday;
                            bool1 = k.a(localObject1, localObject2);
                            if (bool1)
                            {
                              localObject1 = privacy;
                              paramObject = privacy;
                              boolean bool2 = k.a(localObject1, paramObject);
                              if (bool2) {
                                break label288;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label288:
    return true;
  }
  
  public final String getAbout()
  {
    return about;
  }
  
  public final Address getAddress()
  {
    return address;
  }
  
  public final String getAvatarUrl()
  {
    return avatarUrl;
  }
  
  public final String getBirthday()
  {
    return birthday;
  }
  
  public final String getCompanyName()
  {
    return companyName;
  }
  
  public final String getGender()
  {
    return gender;
  }
  
  public final String getJobTitle()
  {
    return jobTitle;
  }
  
  public final OnlineIds getOnlineIds()
  {
    return onlineIds;
  }
  
  public final List getPhoneNumbers()
  {
    return phoneNumbers;
  }
  
  public final String getPrivacy()
  {
    return privacy;
  }
  
  public final List getTags()
  {
    return tags;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    List localList = phoneNumbers;
    int i = 0;
    if (localList != null)
    {
      j = localList.hashCode();
    }
    else
    {
      j = 0;
      localList = null;
    }
    j *= 31;
    Object localObject = avatarUrl;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = jobTitle;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = about;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = tags;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = onlineIds;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = type;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = gender;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = address;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = companyName;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = birthday;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = privacy;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PersonalDataResponse(phoneNumbers=");
    Object localObject = phoneNumbers;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", avatarUrl=");
    localObject = avatarUrl;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", jobTitle=");
    localObject = jobTitle;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", about=");
    localObject = about;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", tags=");
    localObject = tags;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", onlineIds=");
    localObject = onlineIds;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", type=");
    localObject = type;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", gender=");
    localObject = gender;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", address=");
    localObject = address;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", companyName=");
    localObject = companyName;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", birthday=");
    localObject = birthday;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", privacy=");
    localObject = privacy;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.PersonalDataResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */