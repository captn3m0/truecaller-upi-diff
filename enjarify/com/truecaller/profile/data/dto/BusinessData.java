package com.truecaller.profile.data.dto;

import c.g.b.k;
import java.util.List;

public final class BusinessData
{
  private final String about;
  private final String avatarUrl;
  private final Company company;
  private final String jobTitle;
  private final OnlineIds onlineIds;
  private final List phoneNumbers;
  private final List tags;
  
  public BusinessData(List paramList1, String paramString1, String paramString2, String paramString3, List paramList2, OnlineIds paramOnlineIds, Company paramCompany)
  {
    phoneNumbers = paramList1;
    avatarUrl = paramString1;
    jobTitle = paramString2;
    about = paramString3;
    tags = paramList2;
    onlineIds = paramOnlineIds;
    company = paramCompany;
  }
  
  public final List component1()
  {
    return phoneNumbers;
  }
  
  public final String component2()
  {
    return avatarUrl;
  }
  
  public final String component3()
  {
    return jobTitle;
  }
  
  public final String component4()
  {
    return about;
  }
  
  public final List component5()
  {
    return tags;
  }
  
  public final OnlineIds component6()
  {
    return onlineIds;
  }
  
  public final Company component7()
  {
    return company;
  }
  
  public final BusinessData copy(List paramList1, String paramString1, String paramString2, String paramString3, List paramList2, OnlineIds paramOnlineIds, Company paramCompany)
  {
    k.b(paramList1, "phoneNumbers");
    k.b(paramList2, "tags");
    k.b(paramOnlineIds, "onlineIds");
    k.b(paramCompany, "company");
    BusinessData localBusinessData = new com/truecaller/profile/data/dto/BusinessData;
    localBusinessData.<init>(paramList1, paramString1, paramString2, paramString3, paramList2, paramOnlineIds, paramCompany);
    return localBusinessData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BusinessData;
      if (bool1)
      {
        paramObject = (BusinessData)paramObject;
        Object localObject1 = phoneNumbers;
        Object localObject2 = phoneNumbers;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = avatarUrl;
          localObject2 = avatarUrl;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = jobTitle;
            localObject2 = jobTitle;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = about;
              localObject2 = about;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = tags;
                localObject2 = tags;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = onlineIds;
                  localObject2 = onlineIds;
                  bool1 = k.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = company;
                    paramObject = company;
                    boolean bool2 = k.a(localObject1, paramObject);
                    if (bool2) {
                      break label178;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label178:
    return true;
  }
  
  public final String getAbout()
  {
    return about;
  }
  
  public final String getAvatarUrl()
  {
    return avatarUrl;
  }
  
  public final Company getCompany()
  {
    return company;
  }
  
  public final String getJobTitle()
  {
    return jobTitle;
  }
  
  public final OnlineIds getOnlineIds()
  {
    return onlineIds;
  }
  
  public final List getPhoneNumbers()
  {
    return phoneNumbers;
  }
  
  public final List getTags()
  {
    return tags;
  }
  
  public final int hashCode()
  {
    List localList = phoneNumbers;
    int i = 0;
    if (localList != null)
    {
      j = localList.hashCode();
    }
    else
    {
      j = 0;
      localList = null;
    }
    j *= 31;
    Object localObject = avatarUrl;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = jobTitle;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = about;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = tags;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = onlineIds;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = company;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BusinessData(phoneNumbers=");
    Object localObject = phoneNumbers;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", avatarUrl=");
    localObject = avatarUrl;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", jobTitle=");
    localObject = jobTitle;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", about=");
    localObject = about;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", tags=");
    localObject = tags;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", onlineIds=");
    localObject = onlineIds;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", company=");
    localObject = company;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.BusinessData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */