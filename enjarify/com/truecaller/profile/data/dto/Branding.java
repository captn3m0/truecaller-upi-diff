package com.truecaller.profile.data.dto;

import c.g.b.k;
import java.util.List;

public final class Branding
{
  private final String backgroundColor;
  private final List imageUrls;
  
  public Branding(String paramString, List paramList)
  {
    backgroundColor = paramString;
    imageUrls = paramList;
  }
  
  public final String component1()
  {
    return backgroundColor;
  }
  
  public final List component2()
  {
    return imageUrls;
  }
  
  public final Branding copy(String paramString, List paramList)
  {
    k.b(paramString, "backgroundColor");
    k.b(paramList, "imageUrls");
    Branding localBranding = new com/truecaller/profile/data/dto/Branding;
    localBranding.<init>(paramString, paramList);
    return localBranding;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Branding;
      if (bool1)
      {
        paramObject = (Branding)paramObject;
        Object localObject = backgroundColor;
        String str = backgroundColor;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = imageUrls;
          paramObject = imageUrls;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getBackgroundColor()
  {
    return backgroundColor;
  }
  
  public final List getImageUrls()
  {
    return imageUrls;
  }
  
  public final int hashCode()
  {
    String str = backgroundColor;
    int i = 0;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    List localList = imageUrls;
    if (localList != null) {
      i = localList.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Branding(backgroundColor=");
    Object localObject = backgroundColor;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", imageUrls=");
    localObject = imageUrls;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.Branding
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */