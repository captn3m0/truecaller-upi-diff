package com.truecaller.profile.data.dto;

import c.g.b.k;

public final class Profile
{
  private final BusinessData businessData;
  private final String firstName;
  private final String lastName;
  private final PersonalData personalData;
  
  public Profile(String paramString1, String paramString2, PersonalData paramPersonalData, BusinessData paramBusinessData)
  {
    firstName = paramString1;
    lastName = paramString2;
    personalData = paramPersonalData;
    businessData = paramBusinessData;
  }
  
  public final String component1()
  {
    return firstName;
  }
  
  public final String component2()
  {
    return lastName;
  }
  
  public final PersonalData component3()
  {
    return personalData;
  }
  
  public final BusinessData component4()
  {
    return businessData;
  }
  
  public final Profile copy(String paramString1, String paramString2, PersonalData paramPersonalData, BusinessData paramBusinessData)
  {
    k.b(paramString1, "firstName");
    k.b(paramString2, "lastName");
    Profile localProfile = new com/truecaller/profile/data/dto/Profile;
    localProfile.<init>(paramString1, paramString2, paramPersonalData, paramBusinessData);
    return localProfile;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Profile;
      if (bool1)
      {
        paramObject = (Profile)paramObject;
        Object localObject1 = firstName;
        Object localObject2 = firstName;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = lastName;
          localObject2 = lastName;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = personalData;
            localObject2 = personalData;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = businessData;
              paramObject = businessData;
              boolean bool2 = k.a(localObject1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final BusinessData getBusinessData()
  {
    return businessData;
  }
  
  public final String getFirstName()
  {
    return firstName;
  }
  
  public final String getLastName()
  {
    return lastName;
  }
  
  public final PersonalData getPersonalData()
  {
    return personalData;
  }
  
  public final int hashCode()
  {
    String str = firstName;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = lastName;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = personalData;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = businessData;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Profile(firstName=");
    Object localObject = firstName;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", lastName=");
    localObject = lastName;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", personalData=");
    localObject = personalData;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", businessData=");
    localObject = businessData;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.Profile
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */