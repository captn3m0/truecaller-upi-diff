package com.truecaller.profile.data.dto;

import c.g.b.k;
import java.util.List;

public final class BusinessDataResponse
{
  private final String about;
  private final String avatarUrl;
  private final Company company;
  private final boolean isAnonymous;
  private final String jobTitle;
  private final OnlineIds onlineIds;
  private final List phoneNumbers;
  private final List tags;
  private final String type;
  
  public BusinessDataResponse(List paramList1, String paramString1, String paramString2, boolean paramBoolean, String paramString3, List paramList2, OnlineIds paramOnlineIds, Company paramCompany, String paramString4)
  {
    phoneNumbers = paramList1;
    avatarUrl = paramString1;
    jobTitle = paramString2;
    isAnonymous = paramBoolean;
    about = paramString3;
    tags = paramList2;
    onlineIds = paramOnlineIds;
    company = paramCompany;
    type = paramString4;
  }
  
  public final List component1()
  {
    return phoneNumbers;
  }
  
  public final String component2()
  {
    return avatarUrl;
  }
  
  public final String component3()
  {
    return jobTitle;
  }
  
  public final boolean component4()
  {
    return isAnonymous;
  }
  
  public final String component5()
  {
    return about;
  }
  
  public final List component6()
  {
    return tags;
  }
  
  public final OnlineIds component7()
  {
    return onlineIds;
  }
  
  public final Company component8()
  {
    return company;
  }
  
  public final String component9()
  {
    return type;
  }
  
  public final BusinessDataResponse copy(List paramList1, String paramString1, String paramString2, boolean paramBoolean, String paramString3, List paramList2, OnlineIds paramOnlineIds, Company paramCompany, String paramString4)
  {
    k.b(paramList1, "phoneNumbers");
    k.b(paramList2, "tags");
    k.b(paramOnlineIds, "onlineIds");
    k.b(paramCompany, "company");
    BusinessDataResponse localBusinessDataResponse = new com/truecaller/profile/data/dto/BusinessDataResponse;
    localBusinessDataResponse.<init>(paramList1, paramString1, paramString2, paramBoolean, paramString3, paramList2, paramOnlineIds, paramCompany, paramString4);
    return localBusinessDataResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof BusinessDataResponse;
      if (bool2)
      {
        paramObject = (BusinessDataResponse)paramObject;
        Object localObject1 = phoneNumbers;
        Object localObject2 = phoneNumbers;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = avatarUrl;
          localObject2 = avatarUrl;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = jobTitle;
            localObject2 = jobTitle;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              bool2 = isAnonymous;
              boolean bool3 = isAnonymous;
              if (bool2 == bool3)
              {
                bool2 = true;
              }
              else
              {
                bool2 = false;
                localObject1 = null;
              }
              if (bool2)
              {
                localObject1 = about;
                localObject2 = about;
                bool2 = k.a(localObject1, localObject2);
                if (bool2)
                {
                  localObject1 = tags;
                  localObject2 = tags;
                  bool2 = k.a(localObject1, localObject2);
                  if (bool2)
                  {
                    localObject1 = onlineIds;
                    localObject2 = onlineIds;
                    bool2 = k.a(localObject1, localObject2);
                    if (bool2)
                    {
                      localObject1 = company;
                      localObject2 = company;
                      bool2 = k.a(localObject1, localObject2);
                      if (bool2)
                      {
                        localObject1 = type;
                        paramObject = type;
                        boolean bool4 = k.a(localObject1, paramObject);
                        if (bool4) {
                          return bool1;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getAbout()
  {
    return about;
  }
  
  public final String getAvatarUrl()
  {
    return avatarUrl;
  }
  
  public final Company getCompany()
  {
    return company;
  }
  
  public final String getJobTitle()
  {
    return jobTitle;
  }
  
  public final OnlineIds getOnlineIds()
  {
    return onlineIds;
  }
  
  public final List getPhoneNumbers()
  {
    return phoneNumbers;
  }
  
  public final List getTags()
  {
    return tags;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    List localList = phoneNumbers;
    int i = 0;
    if (localList != null)
    {
      j = localList.hashCode();
    }
    else
    {
      j = 0;
      localList = null;
    }
    j *= 31;
    Object localObject = avatarUrl;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = jobTitle;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    int m = isAnonymous;
    if (m != 0) {
      m = 1;
    }
    j = (j + m) * 31;
    localObject = about;
    int n;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    j = (j + n) * 31;
    localObject = tags;
    int i1;
    if (localObject != null)
    {
      i1 = localObject.hashCode();
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    j = (j + i1) * 31;
    localObject = onlineIds;
    int i2;
    if (localObject != null)
    {
      i2 = localObject.hashCode();
    }
    else
    {
      i2 = 0;
      localObject = null;
    }
    j = (j + i2) * 31;
    localObject = company;
    int i3;
    if (localObject != null)
    {
      i3 = localObject.hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    j = (j + i3) * 31;
    localObject = type;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final boolean isAnonymous()
  {
    return isAnonymous;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BusinessDataResponse(phoneNumbers=");
    Object localObject = phoneNumbers;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", avatarUrl=");
    localObject = avatarUrl;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", jobTitle=");
    localObject = jobTitle;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", isAnonymous=");
    boolean bool = isAnonymous;
    localStringBuilder.append(bool);
    localStringBuilder.append(", about=");
    localObject = about;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", tags=");
    localObject = tags;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", onlineIds=");
    localObject = onlineIds;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", company=");
    localObject = company;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", type=");
    localObject = type;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.BusinessDataResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */