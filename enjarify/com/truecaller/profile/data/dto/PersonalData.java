package com.truecaller.profile.data.dto;

import c.g.b.k;
import java.util.List;

public final class PersonalData
{
  private final String about;
  private final Address address;
  private final String avatarUrl;
  private final String companyName;
  private final String gender;
  private final String jobTitle;
  private final OnlineIds onlineIds;
  private final List phoneNumbers;
  private final String privacy;
  private final List tags;
  
  public PersonalData(List paramList1, String paramString1, Address paramAddress, OnlineIds paramOnlineIds, String paramString2, List paramList2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    phoneNumbers = paramList1;
    gender = paramString1;
    address = paramAddress;
    onlineIds = paramOnlineIds;
    avatarUrl = paramString2;
    tags = paramList2;
    companyName = paramString3;
    jobTitle = paramString4;
    privacy = paramString5;
    about = paramString6;
  }
  
  public final List component1()
  {
    return phoneNumbers;
  }
  
  public final String component10()
  {
    return about;
  }
  
  public final String component2()
  {
    return gender;
  }
  
  public final Address component3()
  {
    return address;
  }
  
  public final OnlineIds component4()
  {
    return onlineIds;
  }
  
  public final String component5()
  {
    return avatarUrl;
  }
  
  public final List component6()
  {
    return tags;
  }
  
  public final String component7()
  {
    return companyName;
  }
  
  public final String component8()
  {
    return jobTitle;
  }
  
  public final String component9()
  {
    return privacy;
  }
  
  public final PersonalData copy(List paramList1, String paramString1, Address paramAddress, OnlineIds paramOnlineIds, String paramString2, List paramList2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    k.b(paramList1, "phoneNumbers");
    k.b(paramString1, "gender");
    k.b(paramAddress, "address");
    k.b(paramOnlineIds, "onlineIds");
    k.b(paramList2, "tags");
    k.b(paramString5, "privacy");
    PersonalData localPersonalData = new com/truecaller/profile/data/dto/PersonalData;
    localPersonalData.<init>(paramList1, paramString1, paramAddress, paramOnlineIds, paramString2, paramList2, paramString3, paramString4, paramString5, paramString6);
    return localPersonalData;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PersonalData;
      if (bool1)
      {
        paramObject = (PersonalData)paramObject;
        Object localObject1 = phoneNumbers;
        Object localObject2 = phoneNumbers;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = gender;
          localObject2 = gender;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = address;
            localObject2 = address;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = onlineIds;
              localObject2 = onlineIds;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = avatarUrl;
                localObject2 = avatarUrl;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = tags;
                  localObject2 = tags;
                  bool1 = k.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = companyName;
                    localObject2 = companyName;
                    bool1 = k.a(localObject1, localObject2);
                    if (bool1)
                    {
                      localObject1 = jobTitle;
                      localObject2 = jobTitle;
                      bool1 = k.a(localObject1, localObject2);
                      if (bool1)
                      {
                        localObject1 = privacy;
                        localObject2 = privacy;
                        bool1 = k.a(localObject1, localObject2);
                        if (bool1)
                        {
                          localObject1 = about;
                          paramObject = about;
                          boolean bool2 = k.a(localObject1, paramObject);
                          if (bool2) {
                            break label244;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label244:
    return true;
  }
  
  public final String getAbout()
  {
    return about;
  }
  
  public final Address getAddress()
  {
    return address;
  }
  
  public final String getAvatarUrl()
  {
    return avatarUrl;
  }
  
  public final String getCompanyName()
  {
    return companyName;
  }
  
  public final String getGender()
  {
    return gender;
  }
  
  public final String getJobTitle()
  {
    return jobTitle;
  }
  
  public final OnlineIds getOnlineIds()
  {
    return onlineIds;
  }
  
  public final List getPhoneNumbers()
  {
    return phoneNumbers;
  }
  
  public final String getPrivacy()
  {
    return privacy;
  }
  
  public final List getTags()
  {
    return tags;
  }
  
  public final int hashCode()
  {
    List localList = phoneNumbers;
    int i = 0;
    if (localList != null)
    {
      j = localList.hashCode();
    }
    else
    {
      j = 0;
      localList = null;
    }
    j *= 31;
    Object localObject = gender;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = address;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = onlineIds;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = avatarUrl;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = tags;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = companyName;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = jobTitle;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = privacy;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = about;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PersonalData(phoneNumbers=");
    Object localObject = phoneNumbers;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", gender=");
    localObject = gender;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", address=");
    localObject = address;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", onlineIds=");
    localObject = onlineIds;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", avatarUrl=");
    localObject = avatarUrl;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", tags=");
    localObject = tags;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", companyName=");
    localObject = companyName;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", jobTitle=");
    localObject = jobTitle;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", privacy=");
    localObject = privacy;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", about=");
    localObject = about;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.PersonalData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */