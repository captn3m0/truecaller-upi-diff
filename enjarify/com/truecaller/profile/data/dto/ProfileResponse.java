package com.truecaller.profile.data.dto;

import c.g.b.k;

public final class ProfileResponse
{
  private final BusinessDataResponse businessData;
  private final String firstName;
  private final String lastName;
  private final PersonalDataResponse personalData;
  private final long userId;
  
  public ProfileResponse(long paramLong, String paramString1, String paramString2, PersonalDataResponse paramPersonalDataResponse, BusinessDataResponse paramBusinessDataResponse)
  {
    userId = paramLong;
    firstName = paramString1;
    lastName = paramString2;
    personalData = paramPersonalDataResponse;
    businessData = paramBusinessDataResponse;
  }
  
  public final long component1()
  {
    return userId;
  }
  
  public final String component2()
  {
    return firstName;
  }
  
  public final String component3()
  {
    return lastName;
  }
  
  public final PersonalDataResponse component4()
  {
    return personalData;
  }
  
  public final BusinessDataResponse component5()
  {
    return businessData;
  }
  
  public final ProfileResponse copy(long paramLong, String paramString1, String paramString2, PersonalDataResponse paramPersonalDataResponse, BusinessDataResponse paramBusinessDataResponse)
  {
    ProfileResponse localProfileResponse = new com/truecaller/profile/data/dto/ProfileResponse;
    localProfileResponse.<init>(paramLong, paramString1, paramString2, paramPersonalDataResponse, paramBusinessDataResponse);
    return localProfileResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ProfileResponse;
      if (bool2)
      {
        paramObject = (ProfileResponse)paramObject;
        long l1 = userId;
        long l2 = userId;
        bool2 = l1 < l2;
        Object localObject1;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localObject1 = null;
        }
        if (bool2)
        {
          localObject1 = firstName;
          Object localObject2 = firstName;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = lastName;
            localObject2 = lastName;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              localObject1 = personalData;
              localObject2 = personalData;
              bool2 = k.a(localObject1, localObject2);
              if (bool2)
              {
                localObject1 = businessData;
                paramObject = businessData;
                boolean bool3 = k.a(localObject1, paramObject);
                if (bool3) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final BusinessDataResponse getBusinessData()
  {
    return businessData;
  }
  
  public final String getFirstName()
  {
    return firstName;
  }
  
  public final String getLastName()
  {
    return lastName;
  }
  
  public final PersonalDataResponse getPersonalData()
  {
    return personalData;
  }
  
  public final long getUserId()
  {
    return userId;
  }
  
  public final int hashCode()
  {
    long l1 = userId;
    long l2 = l1 >>> 32;
    l1 ^= l2;
    int i = (int)l1 * 31;
    Object localObject = firstName;
    int j = 0;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = lastName;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = personalData;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = businessData;
    if (localObject != null) {
      j = localObject.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ProfileResponse(userId=");
    long l = userId;
    localStringBuilder.append(l);
    localStringBuilder.append(", firstName=");
    Object localObject = firstName;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", lastName=");
    localObject = lastName;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", personalData=");
    localObject = personalData;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", businessData=");
    localObject = businessData;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.ProfileResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */