package com.truecaller.profile.data.dto;

import c.g.b.k;
import java.util.List;

public final class Company
{
  private final Address address;
  private final Branding branding;
  private final String name;
  private final List openHours;
  private final String size;
  
  public Company(String paramString1, List paramList, Address paramAddress, Branding paramBranding, String paramString2)
  {
    name = paramString1;
    openHours = paramList;
    address = paramAddress;
    branding = paramBranding;
    size = paramString2;
  }
  
  public final String component1()
  {
    return name;
  }
  
  public final List component2()
  {
    return openHours;
  }
  
  public final Address component3()
  {
    return address;
  }
  
  public final Branding component4()
  {
    return branding;
  }
  
  public final String component5()
  {
    return size;
  }
  
  public final Company copy(String paramString1, List paramList, Address paramAddress, Branding paramBranding, String paramString2)
  {
    k.b(paramString1, "name");
    k.b(paramList, "openHours");
    k.b(paramBranding, "branding");
    Company localCompany = new com/truecaller/profile/data/dto/Company;
    localCompany.<init>(paramString1, paramList, paramAddress, paramBranding, paramString2);
    return localCompany;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Company;
      if (bool1)
      {
        paramObject = (Company)paramObject;
        Object localObject1 = name;
        Object localObject2 = name;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = openHours;
          localObject2 = openHours;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = address;
            localObject2 = address;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = branding;
              localObject2 = branding;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = size;
                paramObject = size;
                boolean bool2 = k.a(localObject1, paramObject);
                if (bool2) {
                  break label134;
                }
              }
            }
          }
        }
      }
      return false;
    }
    label134:
    return true;
  }
  
  public final Address getAddress()
  {
    return address;
  }
  
  public final Branding getBranding()
  {
    return branding;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final List getOpenHours()
  {
    return openHours;
  }
  
  public final String getSize()
  {
    return size;
  }
  
  public final int hashCode()
  {
    String str = name;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = openHours;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = address;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = branding;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = size;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Company(name=");
    Object localObject = name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", openHours=");
    localObject = openHours;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", address=");
    localObject = address;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", branding=");
    localObject = branding;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", size=");
    localObject = size;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.Company
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */