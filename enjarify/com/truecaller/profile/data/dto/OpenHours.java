package com.truecaller.profile.data.dto;

import c.g.b.k;
import java.util.SortedSet;

public final class OpenHours
{
  private final String closes;
  private final String opens;
  private final SortedSet weekday;
  
  public OpenHours(SortedSet paramSortedSet, String paramString1, String paramString2)
  {
    weekday = paramSortedSet;
    opens = paramString1;
    closes = paramString2;
  }
  
  public final SortedSet component1()
  {
    return weekday;
  }
  
  public final String component2()
  {
    return opens;
  }
  
  public final String component3()
  {
    return closes;
  }
  
  public final OpenHours copy(SortedSet paramSortedSet, String paramString1, String paramString2)
  {
    k.b(paramSortedSet, "weekday");
    OpenHours localOpenHours = new com/truecaller/profile/data/dto/OpenHours;
    localOpenHours.<init>(paramSortedSet, paramString1, paramString2);
    return localOpenHours;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof OpenHours;
      if (bool1)
      {
        paramObject = (OpenHours)paramObject;
        Object localObject1 = weekday;
        Object localObject2 = weekday;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = opens;
          localObject2 = opens;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = closes;
            paramObject = closes;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getCloses()
  {
    return closes;
  }
  
  public final String getOpens()
  {
    return opens;
  }
  
  public final SortedSet getWeekday()
  {
    return weekday;
  }
  
  public final int hashCode()
  {
    SortedSet localSortedSet = weekday;
    int i = 0;
    if (localSortedSet != null)
    {
      j = localSortedSet.hashCode();
    }
    else
    {
      j = 0;
      localSortedSet = null;
    }
    j *= 31;
    String str = opens;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    int j = (j + k) * 31;
    str = closes;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("OpenHours(weekday=");
    Object localObject = weekday;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", opens=");
    localObject = opens;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", closes=");
    localObject = closes;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.OpenHours
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */