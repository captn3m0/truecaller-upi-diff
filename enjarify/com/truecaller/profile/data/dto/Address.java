package com.truecaller.profile.data.dto;

import c.g.b.k;

public final class Address
{
  private final String city;
  private final String country;
  private final Double latitude;
  private final Double longitude;
  private final String street;
  private final String zipCode;
  
  public Address(String paramString1, String paramString2, String paramString3, String paramString4, Double paramDouble1, Double paramDouble2)
  {
    street = paramString1;
    city = paramString2;
    zipCode = paramString3;
    country = paramString4;
    latitude = paramDouble1;
    longitude = paramDouble2;
  }
  
  public final String component1()
  {
    return street;
  }
  
  public final String component2()
  {
    return city;
  }
  
  public final String component3()
  {
    return zipCode;
  }
  
  public final String component4()
  {
    return country;
  }
  
  public final Double component5()
  {
    return latitude;
  }
  
  public final Double component6()
  {
    return longitude;
  }
  
  public final Address copy(String paramString1, String paramString2, String paramString3, String paramString4, Double paramDouble1, Double paramDouble2)
  {
    Address localAddress = new com/truecaller/profile/data/dto/Address;
    localAddress.<init>(paramString1, paramString2, paramString3, paramString4, paramDouble1, paramDouble2);
    return localAddress;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Address;
      if (bool1)
      {
        paramObject = (Address)paramObject;
        Object localObject1 = street;
        Object localObject2 = street;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = city;
          localObject2 = city;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = zipCode;
            localObject2 = zipCode;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = country;
              localObject2 = country;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = latitude;
                localObject2 = latitude;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = longitude;
                  paramObject = longitude;
                  boolean bool2 = k.a(localObject1, paramObject);
                  if (bool2) {
                    break label156;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label156:
    return true;
  }
  
  public final String getCity()
  {
    return city;
  }
  
  public final String getCountry()
  {
    return country;
  }
  
  public final Double getLatitude()
  {
    return latitude;
  }
  
  public final Double getLongitude()
  {
    return longitude;
  }
  
  public final String getStreet()
  {
    return street;
  }
  
  public final String getZipCode()
  {
    return zipCode;
  }
  
  public final int hashCode()
  {
    String str = street;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = city;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = zipCode;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = country;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = latitude;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = longitude;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Address(street=");
    Object localObject = street;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", city=");
    localObject = city;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", zipCode=");
    localObject = zipCode;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", country=");
    localObject = country;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", latitude=");
    localObject = latitude;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", longitude=");
    localObject = longitude;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.dto.Address
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */