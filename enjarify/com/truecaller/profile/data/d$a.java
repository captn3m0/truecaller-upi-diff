package com.truecaller.profile.data;

import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import e.b;
import e.r;
import java.io.IOException;
import kotlinx.coroutines.ag;

final class d$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag b;
  
  d$a(c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/profile/data/d$a;
    locala.<init>(paramc);
    paramObject = (ag)paramObject;
    b = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        boolean bool2;
        try
        {
          paramObject = KnownEndpoints.IMAGES;
          localObject = a.class;
          paramObject = h.a((KnownEndpoints)paramObject, (Class)localObject);
          paramObject = (a)paramObject;
          paramObject = ((a)paramObject).a();
          paramObject = ((b)paramObject).c();
          if (paramObject != null)
          {
            bool2 = ((r)paramObject).d();
            paramObject = Boolean.valueOf(bool2);
          }
          else
          {
            bool2 = false;
            paramObject = null;
          }
          bool2 = com.truecaller.utils.extensions.c.a((Boolean)paramObject);
        }
        catch (IOException localIOException)
        {
          bool2 = false;
          paramObject = null;
        }
        return Boolean.valueOf(bool2);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */