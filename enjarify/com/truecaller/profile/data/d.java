package com.truecaller.profile.data;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.network.e.a;
import com.truecaller.profile.data.dto.Profile;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.e;
import okhttp3.ac;

public final class d
  implements c, ag
{
  final a a;
  private final f b;
  
  public d(a parama, f paramf)
  {
    a = parama;
    b = paramf;
  }
  
  public final f V_()
  {
    return b;
  }
  
  public final ao a()
  {
    Object localObject = new com/truecaller/profile/data/d$b;
    ((d.b)localObject).<init>(this, null);
    localObject = (m)localObject;
    return e.a(this, null, (m)localObject, 3);
  }
  
  public final ao a(Profile paramProfile)
  {
    k.b(paramProfile, "profile");
    Object localObject = new com/truecaller/profile/data/d$c;
    ((d.c)localObject).<init>(this, paramProfile, null);
    localObject = (m)localObject;
    return e.a(this, null, (m)localObject, 3);
  }
  
  public final ao a(ac paramac)
  {
    k.b(paramac, "avatar");
    Object localObject = new com/truecaller/profile/data/d$d;
    ((d.d)localObject).<init>(paramac, null);
    localObject = (m)localObject;
    return e.a(this, null, (m)localObject, 3);
  }
  
  public final ao b()
  {
    Object localObject = new com/truecaller/profile/data/d$a;
    ((d.a)localObject).<init>(null);
    localObject = (m)localObject;
    return e.a(this, null, (m)localObject, 3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */