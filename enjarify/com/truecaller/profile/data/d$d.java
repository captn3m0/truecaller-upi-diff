package com.truecaller.profile.data;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import e.b;
import e.r;
import java.io.IOException;
import kotlinx.coroutines.ag;
import okhttp3.ac;
import okhttp3.ae;

final class d$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  d$d(ac paramac, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/profile/data/d$d;
    ac localac = b;
    locald.<init>(localac, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = (a)h.a(KnownEndpoints.IMAGES, a.class);
        localObject = b;
        paramObject = ((a)paramObject).a((ac)localObject);
        bool1 = false;
        localObject = null;
      }
    }
    try
    {
      paramObject = ((b)paramObject).c();
      String str = "response";
      c.g.b.k.a(paramObject, str);
      boolean bool2 = ((r)paramObject).d();
      if (bool2)
      {
        paramObject = ((r)paramObject).e();
        paramObject = (ae)paramObject;
        if (paramObject != null)
        {
          paramObject = ((ae)paramObject).g();
          localObject = paramObject;
        }
      }
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    return localObject;
    throw a;
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.d.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */