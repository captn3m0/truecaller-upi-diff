package com.truecaller.profile.data;

import c.a.y;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.common.profile.ProfileSaveErrorResponse;
import com.truecaller.common.profile.f.a;
import com.truecaller.common.profile.f.b;
import com.truecaller.common.profile.f.c;
import com.truecaller.common.profile.f.d;
import com.truecaller.common.profile.f.e;
import com.truecaller.common.profile.f.f;
import com.truecaller.common.profile.f.g;
import com.truecaller.profile.data.dto.Profile;
import e.b;
import e.r;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;
import kotlinx.coroutines.ag;
import okhttp3.ae;

final class d$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  d$c(d paramd, Profile paramProfile, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/profile/data/d$c;
    d locald = b;
    Profile localProfile = c;
    localc.<init>(locald, localProfile, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        try
        {
          paramObject = b;
          paramObject = a;
          localObject = c;
          paramObject = ((com.truecaller.network.e.a)paramObject).a((Profile)localObject);
          paramObject = ((b)paramObject).c();
          int j;
          if (paramObject != null)
          {
            j = ((r)paramObject).b();
            int k = 204;
            if (j != k)
            {
              k = 400;
              if (j != k)
              {
                k = 403;
                if (j != k)
                {
                  k = 422;
                  int m;
                  if (j != k)
                  {
                    k = 500;
                    if (j != k)
                    {
                      localObject = new com/truecaller/common/profile/f$f;
                      m = ((r)paramObject).b();
                      ((f.f)localObject).<init>(m);
                      localObject = (com.truecaller.common.profile.f)localObject;
                    }
                    else
                    {
                      paramObject = f.c.c;
                      localObject = paramObject;
                      localObject = (com.truecaller.common.profile.f)paramObject;
                    }
                  }
                  else
                  {
                    localObject = ProfileSaveErrorResponse.Companion;
                    localObject = "response";
                    c.g.b.k.b(paramObject, (String)localObject);
                    localObject = ProfileSaveErrorResponse.access$getGson$cp();
                    paramObject = ((r)paramObject).f();
                    if (paramObject != null)
                    {
                      paramObject = ((ae)paramObject).f();
                    }
                    else
                    {
                      m = 0;
                      paramObject = null;
                    }
                    Type localType = ProfileSaveErrorResponse.access$getType$cp();
                    paramObject = ((com.google.gson.f)localObject).a((Reader)paramObject, localType);
                    localObject = "gson.fromJson(response.e…           type\n        )";
                    c.g.b.k.a(paramObject, (String)localObject);
                    paramObject = (ProfileSaveErrorResponse)paramObject;
                    paramObject = ((ProfileSaveErrorResponse)paramObject).getErrors();
                    if (paramObject == null)
                    {
                      paramObject = y.a;
                      paramObject = (List)paramObject;
                    }
                    localObject = new com/truecaller/common/profile/f$g;
                    ((f.g)localObject).<init>((List)paramObject);
                    localObject = (com.truecaller.common.profile.f)localObject;
                  }
                }
                else
                {
                  paramObject = f.b.c;
                  localObject = paramObject;
                  localObject = (com.truecaller.common.profile.f)paramObject;
                }
              }
              else
              {
                paramObject = f.a.c;
                localObject = paramObject;
                localObject = (com.truecaller.common.profile.f)paramObject;
              }
            }
            else
            {
              paramObject = f.e.c;
              localObject = paramObject;
              localObject = (com.truecaller.common.profile.f)paramObject;
            }
            if (localObject != null) {}
          }
          else
          {
            paramObject = new com/truecaller/common/profile/f$f;
            j = 0;
            localObject = null;
            ((f.f)paramObject).<init>(0);
            localObject = paramObject;
            localObject = (com.truecaller.common.profile.f)paramObject;
          }
        }
        catch (IOException localIOException)
        {
          paramObject = f.d.c;
          localObject = paramObject;
          localObject = (com.truecaller.common.profile.f)paramObject;
        }
        return localObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */