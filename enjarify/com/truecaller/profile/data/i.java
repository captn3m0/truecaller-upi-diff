package com.truecaller.profile.data;

import c.g.b.k;
import com.truecaller.common.g.a;
import com.truecaller.profile.data.dto.OnlineIds;
import com.truecaller.utils.extensions.c;

public final class i
{
  public static final void a(a parama)
  {
    k.b(parama, "receiver$0");
    parama.d("profileTwitter");
    parama.d("profileBackgroundColor");
    parama.d("profileSize");
    parama.d("profileOpeningHours");
    parama.d("profileImageUrls");
    parama.d("profileLatitude");
    parama.d("profileLongitude");
  }
  
  public static final void a(a parama, String paramString1, String paramString2)
  {
    k.b(parama, "receiver$0");
    k.b(paramString1, "firstName");
    k.b(paramString2, "lastName");
    parama.a("profileFirstName", paramString1);
    parama.a("profileLastName", paramString2);
  }
  
  public static final void a(a parama, String paramString1, String paramString2, String paramString3)
  {
    k.b(parama, "receiver$0");
    k.b(paramString1, "phoneNumber");
    k.b(paramString2, "countryCode");
    parama.a("profileNumber", paramString1);
    parama.a("profileCountryIso", paramString2);
    parama.a("profileNationalNumber", paramString3);
  }
  
  public static final void a(a parama, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, OnlineIds paramOnlineIds, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, Boolean paramBoolean)
  {
    k.b(parama, "receiver$0");
    k.b(paramOnlineIds, "onlineIds");
    parama.a("profileGender", paramString1);
    parama.a("profileStreet", paramString2);
    parama.a("profileCity", paramString3);
    parama.a("profileZip", paramString4);
    paramString2 = paramOnlineIds.getFacebookId();
    parama.a("profileFacebook", paramString2);
    paramString2 = paramOnlineIds.getEmail();
    parama.a("profileEmail", paramString2);
    paramString2 = paramOnlineIds.getUrl();
    parama.a("profileWeb", paramString2);
    parama.a("profileAvatar", paramString6);
    parama.a("profileCompanyName", paramString5);
    parama.a("profileCompanyJob", paramString7);
    parama.a("profileTag", paramString8);
    parama.a("profileStatus", paramString9);
    parama.a("profileAcceptAuto", paramString10);
    boolean bool = c.a(paramBoolean);
    parama.b("profileBusiness", bool);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */