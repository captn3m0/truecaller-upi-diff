package com.truecaller.profile.data;

import c.g.b.k;
import c.o.b;
import com.truecaller.common.account.r;
import com.truecaller.common.profile.f.f;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.h;
import com.truecaller.profile.data.dto.Address;
import com.truecaller.profile.data.dto.BusinessData;
import com.truecaller.profile.data.dto.BusinessDataResponse;
import com.truecaller.profile.data.dto.Company;
import com.truecaller.profile.data.dto.OnlineIds;
import com.truecaller.profile.data.dto.PersonalData;
import com.truecaller.profile.data.dto.PersonalDataResponse;
import com.truecaller.profile.data.dto.PhoneNumber;
import com.truecaller.profile.data.dto.Profile;
import com.truecaller.profile.data.dto.ProfileResponse;
import java.util.List;
import java.util.Map;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bg;
import okhttp3.ac;

public final class f
  implements com.truecaller.common.profile.e
{
  final c.d.f a;
  private final c b;
  private final com.truecaller.common.g.a c;
  private final r d;
  private final com.truecaller.common.profile.b e;
  private final com.truecaller.common.h.u f;
  private final c.d.f g;
  
  public f(c paramc, com.truecaller.common.g.a parama, r paramr, com.truecaller.common.profile.b paramb, com.truecaller.common.h.u paramu, c.d.f paramf1, c.d.f paramf2)
  {
    b = paramc;
    c = parama;
    d = paramr;
    e = paramb;
    f = paramu;
    a = paramf1;
    g = paramf2;
  }
  
  private final Long a(Long paramLong, boolean paramBoolean)
  {
    if (paramBoolean) {
      return paramLong;
    }
    paramLong = c;
    String str = "profileTag";
    paramLong = paramLong.a(str);
    if (paramLong != null) {
      return c.n.m.d(paramLong);
    }
    return null;
  }
  
  private static String a(long paramLong)
  {
    String str = String.valueOf(paramLong);
    return "+".concat(str);
  }
  
  private final String a(PhoneNumber paramPhoneNumber)
  {
    com.truecaller.common.h.u localu = f;
    String str = String.valueOf(paramPhoneNumber.getNumber());
    paramPhoneNumber = paramPhoneNumber.getCountryCode();
    return localu.a(str, paramPhoneNumber);
  }
  
  private static String a(String paramString)
  {
    String str = "Private";
    boolean bool = k.a(paramString, str);
    if (bool) {
      return "0";
    }
    return "1";
  }
  
  public final Object a(c.d.c paramc)
  {
    f localf = this;
    Object localObject1 = paramc;
    boolean bool1 = paramc instanceof f.a;
    if (bool1)
    {
      localObject2 = paramc;
      localObject2 = (f.a)paramc;
      int j = b;
      k = -1 << -1;
      j &= k;
      if (j != 0)
      {
        m = b - k;
        b = m;
        break label83;
      }
    }
    Object localObject2 = new com/truecaller/profile/data/f$a;
    ((f.a)localObject2).<init>(localf, (c.d.c)localObject1);
    label83:
    localObject1 = a;
    Object localObject3 = c.d.a.a.a;
    int k = b;
    int n = 1;
    switch (k)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 1: 
      localObject2 = (f)d;
      boolean bool2 = localObject1 instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool3 = localObject1 instanceof o.b;
      if (bool3) {
        break label968;
      }
      localObject1 = b.a();
      d = localf;
      b = n;
      localObject1 = ((ao)localObject1).a((c.d.c)localObject2);
      if (localObject1 == localObject3) {
        return localObject3;
      }
      localObject2 = localf;
    }
    localObject1 = (h)localObject1;
    localObject3 = b;
    boolean bool3 = false;
    if (localObject3 != null)
    {
      Object localObject4 = c;
      long l1 = ((ProfileResponse)localObject3).getUserId();
      k.b(localObject4, "receiver$0");
      String str1 = "profileUserId";
      ((com.truecaller.common.g.a)localObject4).b(str1, l1);
      localObject4 = c;
      String str2 = ((ProfileResponse)localObject3).getFirstName();
      if (str2 == null) {
        str2 = "";
      }
      Object localObject5 = ((ProfileResponse)localObject3).getLastName();
      if (localObject5 == null) {
        localObject5 = "";
      }
      i.a((com.truecaller.common.g.a)localObject4, str2, (String)localObject5);
      localObject4 = ((ProfileResponse)localObject3).getPersonalData();
      str2 = null;
      long l2;
      String str3;
      com.truecaller.common.g.a locala;
      String str4;
      Object localObject6;
      Object localObject7;
      Object localObject8;
      String str5;
      OnlineIds localOnlineIds;
      String str6;
      String str7;
      long l3;
      String str8;
      String str9;
      Boolean localBoolean;
      if (localObject4 != null)
      {
        localObject3 = ((ProfileResponse)localObject3).getPersonalData();
        i.a(c);
        localObject4 = (PhoneNumber)((PersonalDataResponse)localObject3).getPhoneNumbers().get(0);
        localObject5 = c;
        l2 = ((PhoneNumber)localObject4).getNumber();
        str1 = a(l2);
        str3 = ((PhoneNumber)localObject4).getCountryCode();
        localObject4 = ((f)localObject2).a((PhoneNumber)localObject4);
        i.a((com.truecaller.common.g.a)localObject5, str1, str3, (String)localObject4);
        locala = c;
        str4 = ((PersonalDataResponse)localObject3).getGender();
        localObject6 = ((PersonalDataResponse)localObject3).getAddress().getStreet();
        localObject7 = ((PersonalDataResponse)localObject3).getAddress().getCity();
        localObject8 = ((PersonalDataResponse)localObject3).getAddress().getZipCode();
        str5 = ((PersonalDataResponse)localObject3).getCompanyName();
        localOnlineIds = ((PersonalDataResponse)localObject3).getOnlineIds();
        str6 = ((PersonalDataResponse)localObject3).getAvatarUrl();
        str7 = ((PersonalDataResponse)localObject3).getJobTitle();
        localObject4 = (Long)c.a.m.e(((PersonalDataResponse)localObject3).getTags());
        if (localObject4 != null)
        {
          l3 = ((Long)localObject4).longValue();
          str2 = String.valueOf(l3);
        }
        str8 = ((PersonalDataResponse)localObject3).getAbout();
        localObject3 = ((PersonalDataResponse)localObject3).getPrivacy();
        str9 = a((String)localObject3);
        localBoolean = Boolean.FALSE;
        i.a(locala, str4, (String)localObject6, (String)localObject7, (String)localObject8, str5, localOnlineIds, str6, str7, str2, str8, str9, localBoolean);
      }
      else
      {
        localObject4 = ((ProfileResponse)localObject3).getBusinessData();
        if (localObject4 != null)
        {
          localObject3 = ((ProfileResponse)localObject3).getBusinessData();
          localObject4 = (PhoneNumber)((BusinessDataResponse)localObject3).getPhoneNumbers().get(0);
          localObject5 = c;
          l2 = ((PhoneNumber)localObject4).getNumber();
          str1 = a(l2);
          str3 = ((PhoneNumber)localObject4).getCountryCode();
          localObject4 = ((f)localObject2).a((PhoneNumber)localObject4);
          i.a((com.truecaller.common.g.a)localObject5, str1, str3, (String)localObject4);
          locala = c;
          str4 = "N";
          localObject4 = ((BusinessDataResponse)localObject3).getCompany().getAddress();
          if (localObject4 != null)
          {
            localObject4 = ((Address)localObject4).getStreet();
            localObject6 = localObject4;
          }
          else
          {
            localObject6 = null;
          }
          localObject4 = ((BusinessDataResponse)localObject3).getCompany().getAddress();
          if (localObject4 != null)
          {
            localObject4 = ((Address)localObject4).getCity();
            localObject7 = localObject4;
          }
          else
          {
            localObject7 = null;
          }
          localObject4 = ((BusinessDataResponse)localObject3).getCompany().getAddress();
          if (localObject4 != null)
          {
            localObject4 = ((Address)localObject4).getZipCode();
            localObject8 = localObject4;
          }
          else
          {
            localObject8 = null;
          }
          str5 = ((BusinessDataResponse)localObject3).getCompany().getName();
          localOnlineIds = ((BusinessDataResponse)localObject3).getOnlineIds();
          str6 = ((BusinessDataResponse)localObject3).getAvatarUrl();
          str7 = ((BusinessDataResponse)localObject3).getJobTitle();
          localObject4 = (Long)c.a.m.e(((BusinessDataResponse)localObject3).getTags());
          if (localObject4 != null)
          {
            l3 = ((Long)localObject4).longValue();
            str2 = String.valueOf(l3);
          }
          str8 = ((BusinessDataResponse)localObject3).getAbout();
          str9 = "0";
          localBoolean = Boolean.TRUE;
          i.a(locala, str4, (String)localObject6, (String)localObject7, (String)localObject8, str5, localOnlineIds, str6, str7, str2, str8, str9, localBoolean);
        }
        else
        {
          localObject1 = new java/lang/IllegalStateException;
          ((IllegalStateException)localObject1).<init>("Either personal or business data should not be empty");
          throw ((Throwable)localObject1);
        }
      }
    }
    localObject2 = e;
    ((com.truecaller.common.profile.b)localObject2).a();
    localObject1 = (Number)Integer.valueOf(a);
    int m = ((Number)localObject1).intValue();
    int i = 200;
    if (m != i)
    {
      i = 404;
      if (m != i) {}
    }
    else
    {
      bool3 = true;
    }
    return Boolean.valueOf(bool3);
    label968:
    throw a;
  }
  
  public final Object a(boolean paramBoolean1, ac paramac, boolean paramBoolean2, Long paramLong, Map paramMap, boolean paramBoolean3, c.d.c paramc)
  {
    f localf = this;
    boolean bool1 = paramBoolean1;
    Object localObject1 = paramac;
    Object localObject2 = paramc;
    boolean bool2 = paramc instanceof f.c;
    if (bool2)
    {
      localObject3 = paramc;
      localObject3 = (f.c)paramc;
      int j = b;
      k = -1 << -1;
      j &= k;
      if (j != 0)
      {
        int m = b - k;
        b = m;
        break label97;
      }
    }
    Object localObject3 = new com/truecaller/profile/data/f$c;
    ((f.c)localObject3).<init>(localf, (c.d.c)localObject2);
    label97:
    localObject2 = a;
    Object localObject4 = c.d.a.a.a;
    int k = b;
    int n = 1;
    String str1 = null;
    boolean bool6;
    boolean bool7;
    boolean bool8;
    boolean bool10;
    switch (k)
    {
    default: 
      localObject5 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject5).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject5);
    case 2: 
      localObject5 = (Profile)i;
      localObject1 = (f)d;
      bool2 = localObject2 instanceof o.b;
      if (!bool2) {
        break label1000;
      }
      throw a;
    case 1: 
      bool1 = l;
      localObject1 = (Map)g;
      localObject6 = (Long)f;
      bool5 = k;
      localObject7 = (ac)e;
      bool6 = j;
      localObject8 = (f)d;
      bool7 = localObject2 instanceof o.b;
      if (!bool7)
      {
        bool8 = bool1;
        bool1 = bool6;
        localObject9 = localObject2;
        localObject2 = localObject1;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      boolean bool3 = localObject2 instanceof o.b;
      if (bool3) {
        break label1539;
      }
      if (!bool1) {
        break label522;
      }
      d = localf;
      j = bool1;
      e = localObject1;
      bool4 = paramBoolean2;
      k = paramBoolean2;
      localObject6 = paramLong;
      f = paramLong;
      localObject10 = paramMap;
      g = paramMap;
      bool9 = paramBoolean3;
      l = paramBoolean3;
      b = n;
      localObject9 = localf.a((ac)localObject1, (c.d.c)localObject3);
      if (localObject9 == localObject4) {
        return localObject4;
      }
      localObject8 = localf;
      bool10 = paramBoolean3;
      localObject7 = localObject1;
      bool8 = paramBoolean3;
      bool5 = paramBoolean2;
      localObject2 = paramMap;
    }
    Object localObject9 = (String)localObject9;
    if (localObject9 != null)
    {
      localObject11 = c;
      localObject12 = "profileAvatar";
      ((com.truecaller.common.g.a)localObject11).a((String)localObject12, (String)localObject9);
      if (localObject9 != null)
      {
        bool10 = bool5;
        bool5 = bool8;
        localObject1 = localObject8;
        localObject8 = localObject2;
        bool4 = bool10;
        break label585;
      }
    }
    Object localObject5 = new com/truecaller/common/profile/f$f;
    ((f.f)localObject5).<init>();
    return localObject5;
    label522:
    boolean bool4 = paramBoolean2;
    Object localObject6 = paramLong;
    Object localObject10 = paramMap;
    boolean bool9 = paramBoolean3;
    localObject9 = c;
    Object localObject8 = "profileAvatar";
    localObject9 = ((com.truecaller.common.g.a)localObject9).a((String)localObject8);
    if (localObject9 == null) {
      localObject9 = "";
    }
    localObject8 = localObject10;
    boolean bool5 = bool9;
    Object localObject7 = localObject1;
    localObject1 = localf;
    label585:
    Object localObject11 = d.b();
    boolean bool11 = false;
    Object localObject12 = null;
    Object localObject13;
    if (localObject11 != null)
    {
      localObject13 = localObject11;
      localObject13 = (CharSequence)localObject11;
      int i1 = ((CharSequence)localObject13).length();
      if (i1 == 0)
      {
        i1 = 1;
      }
      else
      {
        i1 = 0;
        localObject13 = null;
      }
      if (i1 == 0)
      {
        localObject13 = "+";
        boolean bool12 = c.n.m.b((String)localObject11, (String)localObject13, false);
        if (bool12) {
          if (localObject11 != null)
          {
            localObject11 = ((String)localObject11).substring(n);
            localObject14 = "(this as java.lang.String).substring(startIndex)";
            k.a(localObject11, (String)localObject14);
          }
          else
          {
            localObject5 = new c/u;
            ((c.u)localObject5).<init>("null cannot be cast to non-null type java.lang.String");
            throw ((Throwable)localObject5);
          }
        }
        localObject14 = c.n.m.d((String)localObject11);
        break label733;
      }
    }
    n = 0;
    Object localObject14 = null;
    label733:
    if (localObject14 != null)
    {
      localObject14 = (Number)localObject14;
      long l1 = ((Number)localObject14).longValue();
      if (bool5)
      {
        localObject14 = new com/truecaller/profile/data/b;
        localObject11 = c;
        ((b)localObject14).<init>((Map)localObject8, (com.truecaller.common.g.a)localObject11, l1);
        localObject11 = c.a.m.b(((f)localObject1).a((Long)localObject6, bool4));
        bool11 = false;
        localObject12 = null;
        localObject14 = ((b)localObject14).a((String)localObject9, (List)localObject11, false);
      }
      else
      {
        localObject14 = new com/truecaller/profile/data/b;
        localObject11 = c;
        ((b)localObject14).<init>((Map)localObject8, (com.truecaller.common.g.a)localObject11, l1);
        localObject11 = c.a.m.b(((f)localObject1).a((Long)localObject6, bool4));
        localObject12 = a;
        localObject13 = "profileBusiness";
        bool11 = ((com.truecaller.common.g.a)localObject12).b((String)localObject13);
        localObject14 = ((b)localObject14).a((String)localObject9, (List)localObject11, bool11);
      }
      localObject11 = b.a((Profile)localObject14);
      d = localObject1;
      j = bool1;
      e = localObject7;
      k = bool4;
      f = localObject6;
      g = localObject8;
      l = bool5;
      h = localObject9;
      i = localObject14;
      int i = 2;
      b = i;
      localObject2 = ((ao)localObject11).a((c.d.c)localObject3);
      if (localObject2 == localObject4) {
        return localObject4;
      }
      localObject5 = localObject14;
      label1000:
      localObject2 = (com.truecaller.common.profile.f)localObject2;
      bool2 = a;
      if (bool2)
      {
        localObject3 = c;
        localObject4 = ((Profile)localObject5).getFirstName();
        localObject6 = ((Profile)localObject5).getLastName();
        i.a((com.truecaller.common.g.a)localObject3, (String)localObject4, (String)localObject6);
        localObject3 = ((Profile)localObject5).getPersonalData();
        String str2;
        String str3;
        long l2;
        String str4;
        String str5;
        Boolean localBoolean;
        if (localObject3 != null)
        {
          localObject5 = ((Profile)localObject5).getPersonalData();
          localObject3 = c;
          i.a((com.truecaller.common.g.a)localObject3);
          localObject10 = c;
          localObject7 = ((PersonalData)localObject5).getGender();
          localObject9 = ((PersonalData)localObject5).getAddress().getStreet();
          localObject8 = ((PersonalData)localObject5).getAddress().getCity();
          localObject11 = ((PersonalData)localObject5).getAddress().getZipCode();
          localObject12 = ((PersonalData)localObject5).getCompanyName();
          localObject13 = ((PersonalData)localObject5).getOnlineIds();
          str2 = ((PersonalData)localObject5).getAvatarUrl();
          str3 = ((PersonalData)localObject5).getJobTitle();
          localObject1 = (Long)c.a.m.e(((PersonalData)localObject5).getTags());
          if (localObject1 != null)
          {
            l2 = ((Long)localObject1).longValue();
            str1 = String.valueOf(l2);
          }
          str4 = ((PersonalData)localObject5).getAbout();
          localObject5 = ((PersonalData)localObject5).getPrivacy();
          str5 = a((String)localObject5);
          localBoolean = Boolean.FALSE;
          i.a((com.truecaller.common.g.a)localObject10, (String)localObject7, (String)localObject9, (String)localObject8, (String)localObject11, (String)localObject12, (OnlineIds)localObject13, str2, str3, str1, str4, str5, localBoolean);
        }
        else
        {
          localObject3 = ((Profile)localObject5).getBusinessData();
          if (localObject3 != null)
          {
            localObject5 = ((Profile)localObject5).getBusinessData();
            localObject10 = c;
            localObject7 = "N";
            localObject1 = ((BusinessData)localObject5).getCompany().getAddress();
            if (localObject1 != null)
            {
              localObject1 = ((Address)localObject1).getStreet();
              localObject9 = localObject1;
            }
            else
            {
              bool6 = false;
              localObject9 = null;
            }
            localObject1 = ((BusinessData)localObject5).getCompany().getAddress();
            if (localObject1 != null)
            {
              localObject1 = ((Address)localObject1).getCity();
              localObject8 = localObject1;
            }
            else
            {
              localObject8 = null;
            }
            localObject1 = ((BusinessData)localObject5).getCompany().getAddress();
            if (localObject1 != null)
            {
              localObject1 = ((Address)localObject1).getZipCode();
              localObject11 = localObject1;
            }
            else
            {
              bool7 = false;
              localObject11 = null;
            }
            localObject12 = ((BusinessData)localObject5).getCompany().getName();
            localObject13 = ((BusinessData)localObject5).getOnlineIds();
            str2 = ((BusinessData)localObject5).getAvatarUrl();
            str3 = ((BusinessData)localObject5).getJobTitle();
            localObject1 = (Long)c.a.m.e(((BusinessData)localObject5).getTags());
            if (localObject1 != null)
            {
              l2 = ((Long)localObject1).longValue();
              str1 = String.valueOf(l2);
            }
            str4 = ((BusinessData)localObject5).getAbout();
            str5 = "0";
            localBoolean = Boolean.TRUE;
            i.a((com.truecaller.common.g.a)localObject10, (String)localObject7, (String)localObject9, (String)localObject8, (String)localObject11, (String)localObject12, (OnlineIds)localObject13, str2, str3, str1, str4, str5, localBoolean);
          }
          else
          {
            localObject5 = new java/lang/IllegalStateException;
            ((IllegalStateException)localObject5).<init>("Either personal or business data should not be empty");
            throw ((Throwable)localObject5);
          }
        }
      }
      return localObject2;
    }
    localObject5 = new com/truecaller/log/UnmutedException$h;
    ((UnmutedException.h)localObject5).<init>();
    AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject5);
    localObject5 = new com/truecaller/common/profile/f$f;
    ((f.f)localObject5).<init>();
    return localObject5;
    label1539:
    throw a;
  }
  
  public final void a(com.truecaller.common.profile.a parama)
  {
    ag localag = (ag)bg.a;
    c.d.f localf = g;
    Object localObject = new com/truecaller/profile/data/f$b;
    ((f.b)localObject).<init>(this, parama, null);
    localObject = (c.g.a.m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (c.g.a.m)localObject, 2);
  }
  
  public final void a(boolean paramBoolean1, ac paramac, boolean paramBoolean2, Long paramLong, Map paramMap, boolean paramBoolean3, com.truecaller.common.profile.h paramh)
  {
    ag localag = (ag)bg.a;
    c.d.f localf = g;
    Object localObject = new com/truecaller/profile/data/f$d;
    ((f.d)localObject).<init>(this, paramBoolean1, paramac, paramBoolean2, paramLong, paramMap, paramBoolean3, paramh, null);
    localObject = (c.g.a.m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (c.g.a.m)localObject, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */