package com.truecaller.profile.data;

import c.a.m;
import c.a.y;
import c.g.b.k;
import com.truecaller.common.g.a;
import com.truecaller.common.network.g.c;
import com.truecaller.profile.data.dto.Address;
import com.truecaller.profile.data.dto.Branding;
import com.truecaller.profile.data.dto.BusinessData;
import com.truecaller.profile.data.dto.Company;
import com.truecaller.profile.data.dto.OnlineIds;
import com.truecaller.profile.data.dto.PersonalData;
import com.truecaller.profile.data.dto.Profile;
import java.util.List;
import java.util.Map;

public final class b
{
  final a a;
  private final Map b;
  private final long c;
  
  public b(Map paramMap, a parama, long paramLong)
  {
    b = paramMap;
    a = parama;
    c = paramLong;
  }
  
  private final Address a(boolean paramBoolean)
  {
    String str1 = a("profileStreet");
    String str2 = a("profileCity");
    String str3 = a("profileZip");
    String str4 = a("profileCountryIso");
    Object localObject2;
    if (paramBoolean)
    {
      localObject1 = b("profileLatitude");
      localObject2 = localObject1;
    }
    else
    {
      localObject2 = null;
    }
    Object localObject4;
    if (paramBoolean)
    {
      localObject3 = b("profileLongitude");
      localObject4 = localObject3;
    }
    else
    {
      localObject4 = null;
    }
    Object localObject3 = new com/truecaller/profile/data/dto/Address;
    Object localObject1 = localObject3;
    ((Address)localObject3).<init>(str1, str2, str3, str4, (Double)localObject2, (Double)localObject4);
    return (Address)localObject3;
  }
  
  private final OnlineIds a()
  {
    String str1 = a("profileFacebook");
    String str2 = a("profileTwitter");
    String str3 = a("profileEmail");
    String str4 = a("profileWeb");
    OnlineIds localOnlineIds = new com/truecaller/profile/data/dto/OnlineIds;
    localOnlineIds.<init>(str1, str3, str4, str2);
    return localOnlineIds;
  }
  
  private final String a(String paramString)
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = c.a(paramString);
      localObject1 = (String)((Map)localObject1).get(localObject2);
    }
    else
    {
      localObject1 = null;
    }
    Object localObject2 = a;
    String str = "";
    paramString = ((a)localObject2).b(paramString, str);
    if (localObject1 != null) {
      paramString = (String)localObject1;
    }
    k.a(paramString, "StringUtils.defaultStrin…String(key, \"\")\n        )");
    return paramString;
  }
  
  private final Double b(String paramString)
  {
    paramString = Double.valueOf(a.c(paramString));
    Object localObject = paramString;
    localObject = (Number)paramString;
    double d1 = ((Number)localObject).doubleValue();
    double d2 = 0.0D;
    boolean bool = d1 < d2;
    int i;
    if (bool)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      return paramString;
    }
    return null;
  }
  
  public final Profile a(String paramString, List paramList, boolean paramBoolean)
  {
    b localb = this;
    k.b(paramString, "avatarUrl");
    k.b(paramList, "tags");
    String str1 = a("profileFirstName");
    String str2 = a("profileLastName");
    Object localObject1 = a("profileGender");
    boolean bool1 = true;
    Object localObject2 = null;
    if (localObject1 != null)
    {
      localObject3 = localObject1;
      localObject3 = (CharSequence)localObject1;
      int i = ((CharSequence)localObject3).length();
      if (i == 0)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localObject3 = null;
      }
      if (i == 0) {}
    }
    else
    {
      localObject1 = "N";
    }
    Object localObject3 = localObject1;
    String str3 = localb.a("profileCompanyName");
    String str4 = localb.a("profileCompanyJob");
    String str5 = localb.a("profileStatus");
    localObject1 = localb.a("profileAcceptAuto");
    Object localObject4 = "1";
    boolean bool2 = k.a(localObject1, localObject4);
    if (!bool2)
    {
      localObject4 = "2";
      boolean bool3 = k.a(localObject1, localObject4);
      if (!bool3)
      {
        localObject1 = "Private";
        break label186;
      }
    }
    localObject1 = "Public";
    label186:
    Object localObject5 = localObject1;
    if (paramBoolean)
    {
      localObject6 = new com/truecaller/profile/data/dto/Company;
      localObject1 = y.a;
      Object localObject7 = localObject1;
      localObject7 = (List)localObject1;
      Address localAddress = localb.a(bool1);
      localObject1 = new com/truecaller/profile/data/dto/Branding;
      localObject8 = localb.a("profileBackgroundColor");
      localObject2 = (List)y.a;
      ((Branding)localObject1).<init>((String)localObject8, (List)localObject2);
      String str6 = localb.a("profileSize");
      localObject9 = localObject6;
      ((Company)localObject6).<init>(str3, (List)localObject7, localAddress, (Branding)localObject1, str6);
      localObject5 = new com/truecaller/profile/data/dto/BusinessData;
      localObject8 = m.a(Long.valueOf(c));
      localObject9 = a();
      localObject1 = localObject5;
      localObject2 = paramString;
      localObject3 = str4;
      localObject4 = str5;
      ((BusinessData)localObject5).<init>((List)localObject8, paramString, str4, str5, paramList, (OnlineIds)localObject9, (Company)localObject6);
      localObject6 = new com/truecaller/profile/data/dto/Profile;
      localObject1 = localObject6;
      localObject8 = str1;
      localObject2 = str2;
      localObject4 = localObject5;
      ((Profile)localObject6).<init>(str1, str2, null, (BusinessData)localObject5, 4, null);
      return (Profile)localObject6;
    }
    Object localObject9 = new com/truecaller/profile/data/dto/PersonalData;
    Object localObject8 = m.a(Long.valueOf(c));
    localObject4 = localb.a(false);
    Object localObject6 = a();
    localObject1 = localObject9;
    localObject2 = localObject3;
    localObject3 = localObject4;
    localObject4 = localObject6;
    localObject6 = str3;
    ((PersonalData)localObject9).<init>((List)localObject8, (String)localObject2, (Address)localObject3, (OnlineIds)localObject4, paramString, paramList, str3, str4, (String)localObject5, str5);
    localObject6 = new com/truecaller/profile/data/dto/Profile;
    localObject1 = localObject6;
    localObject8 = str1;
    localObject2 = str2;
    localObject3 = localObject9;
    ((Profile)localObject6).<init>(str1, str2, (PersonalData)localObject9, null, 8, null);
    return (Profile)localObject6;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */