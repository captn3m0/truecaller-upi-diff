package com.truecaller.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.analytics.e.a;
import com.truecaller.profile.business.i;
import com.truecaller.profile.business.j;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.HashMap;

public final class BusinessProfileOnboardingActivity
  extends AppCompatActivity
{
  public static final BusinessProfileOnboardingActivity.a c;
  public com.truecaller.featuretoggles.e a;
  public com.truecaller.analytics.b b;
  private HashMap d;
  
  static
  {
    BusinessProfileOnboardingActivity.a locala = new com/truecaller/profile/BusinessProfileOnboardingActivity$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public static final Intent a(Context paramContext)
  {
    return BusinessProfileOnboardingActivity.a.a(paramContext, false, true);
  }
  
  public static final Intent a(Context paramContext, boolean paramBoolean)
  {
    return BusinessProfileOnboardingActivity.a.a(paramContext, paramBoolean, false);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = aresId;
    setTheme(i);
    i = 2131558512;
    setContentView(i);
    j.a(this).a(this);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "featuresRegistry";
      k.a((String)localObject);
    }
    paramBundle = paramBundle.z();
    boolean bool1 = paramBundle.a();
    if (!bool1)
    {
      finish();
      return;
    }
    paramBundle = getIntent();
    Object localObject = "arg_from_wizard";
    String str1 = null;
    bool1 = paramBundle.getBooleanExtra((String)localObject, false);
    if (bool1)
    {
      paramBundle = b;
      if (paramBundle == null)
      {
        localObject = "analytics";
        k.a((String)localObject);
      }
      localObject = new com/truecaller/analytics/e$a;
      ((e.a)localObject).<init>("WizardAction");
      String str2 = "BusinessProfileOnboarding";
      localObject = ((e.a)localObject).a("Action", str2).a();
      str1 = "AnalyticsEvent.Builder(W…                 .build()";
      k.a(localObject, str1);
      paramBundle.b((com.truecaller.analytics.e)localObject);
    }
    int j = R.id.toolbar;
    paramBundle = (Toolbar)a(j);
    setSupportActionBar(paramBundle);
    paramBundle = getSupportActionBar();
    boolean bool2 = true;
    if (paramBundle != null) {
      paramBundle.setDisplayHomeAsUpEnabled(bool2);
    }
    paramBundle = getSupportActionBar();
    if (paramBundle != null) {
      paramBundle.setDisplayShowHomeEnabled(bool2);
    }
    j = R.id.toolbar;
    paramBundle = (Toolbar)a(j);
    if (paramBundle != null)
    {
      localObject = new com/truecaller/profile/BusinessProfileOnboardingActivity$c;
      ((BusinessProfileOnboardingActivity.c)localObject).<init>(this);
      localObject = (View.OnClickListener)localObject;
      paramBundle.setNavigationOnClickListener((View.OnClickListener)localObject);
    }
    j = R.id.continueButton;
    paramBundle = (Button)a(j);
    localObject = new com/truecaller/profile/BusinessProfileOnboardingActivity$b;
    ((BusinessProfileOnboardingActivity.b)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.BusinessProfileOnboardingActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */