package com.truecaller.profile;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog.Builder;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.truecaller.ui.components.l;
import com.truecaller.util.aw;
import com.truecaller.utils.extensions.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class EditMeFormFragment$3
  implements Runnable
{
  EditMeFormFragment$3(EditMeFormFragment paramEditMeFormFragment, Uri paramUri) {}
  
  public final void run()
  {
    Object localObject1 = b;
    boolean bool1 = ((EditMeFormFragment)localObject1).s();
    if (!bool1) {
      return;
    }
    localObject1 = b.getContext();
    Object localObject3 = com.truecaller.common.h.n.b(b.getContext());
    localObject1 = com.truecaller.common.h.n.a((Context)localObject1, (Uri)localObject3);
    localObject3 = b.getActivity();
    Object localObject4 = ((f)localObject3).getPackageManager();
    Object localObject5 = null;
    Object localObject6 = ((PackageManager)localObject4).queryIntentActivities((Intent)localObject1, 0);
    int i = ((List)localObject6).size();
    if (i == 0) {
      try
      {
        localObject1 = a;
        localObject1 = ((Uri)localObject1).getPath();
        localObject1 = aw.b((String)localObject1);
        if (localObject1 == null)
        {
          localObject1 = b;
          j = 2131886538;
          localObject1 = ((EditMeFormFragment)localObject1).getString(j);
          localObject1 = Toast.makeText((Context)localObject3, (CharSequence)localObject1, 0);
          ((Toast)localObject1).show();
          return;
        }
        localObject3 = b;
        int j = 800;
        localObject1 = b.b((Bitmap)localObject1, j, j);
        EditMeFormFragment.a((EditMeFormFragment)localObject3, (Bitmap)localObject1);
        return;
      }
      finally
      {
        com.truecaller.common.h.n.f(b.getContext());
      }
    }
    int k = 1;
    if (i == k)
    {
      localObject3 = new android/content/Intent;
      ((Intent)localObject3).<init>(localIntent1);
      localObject2 = (ResolveInfo)((List)localObject6).get(0);
      localObject4 = new android/content/ComponentName;
      localObject5 = activityInfo.packageName;
      localObject2 = activityInfo.name;
      ((ComponentName)localObject4).<init>((String)localObject5, (String)localObject2);
      ((Intent)localObject3).setComponent((ComponentName)localObject4);
      b.startActivityForResult((Intent)localObject3, 3);
      return;
    }
    localObject5 = new java/util/ArrayList;
    ((ArrayList)localObject5).<init>();
    localObject6 = ((List)localObject6).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject6).hasNext();
      if (!bool2) {
        break;
      }
      Object localObject7 = (ResolveInfo)((Iterator)localObject6).next();
      Intent localIntent2 = new android/content/Intent;
      localIntent2.<init>((Intent)localObject2);
      Object localObject8 = new android/content/ComponentName;
      String str1 = activityInfo.packageName;
      String str2 = activityInfo.name;
      ((ComponentName)localObject8).<init>(str1, str2);
      localIntent2.setComponent((ComponentName)localObject8);
      localObject7 = activityInfo.applicationInfo;
      localObject7 = ((PackageManager)localObject4).getApplicationLabel((ApplicationInfo)localObject7).toString();
      localObject8 = new com/truecaller/ui/components/n;
      str1 = null;
      ((com.truecaller.ui.components.n)localObject8).<init>((String)localObject7, null, localIntent2);
      ((ArrayList)localObject5).add(localObject8);
    }
    Object localObject2 = new android/support/v7/app/AlertDialog$Builder;
    localObject4 = b.getContext();
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject4);
    localObject2 = ((AlertDialog.Builder)localObject2).setTitle(2131887194);
    localObject4 = new com/truecaller/ui/components/l;
    localObject6 = b.getContext();
    ((l)localObject4).<init>((Context)localObject6, (List)localObject5);
    localObject6 = new com/truecaller/profile/-$$Lambda$EditMeFormFragment$3$ouEVBLhVP-9erTwbrO7xrLXbPqc;
    ((-..Lambda.EditMeFormFragment.3.ouEVBLhVP-9erTwbrO7xrLXbPqc)localObject6).<init>(this, (ArrayList)localObject5);
    ((AlertDialog.Builder)localObject2).setAdapter((ListAdapter)localObject4, (DialogInterface.OnClickListener)localObject6).setCancelable(k).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.EditMeFormFragment.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */