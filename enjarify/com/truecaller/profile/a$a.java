package com.truecaller.profile;

import android.support.v4.view.o;
import android.view.View;
import android.view.ViewGroup;

final class a$a
  extends o
{
  private a$a(a parama) {}
  
  public final void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    paramObject = (View)paramObject;
    paramViewGroup.removeView((View)paramObject);
  }
  
  public final int getCount()
  {
    return 2;
  }
  
  public final CharSequence getPageTitle(int paramInt)
  {
    a locala = a;
    if (paramInt == 0) {
      paramInt = 2131886379;
    } else {
      paramInt = 2131886378;
    }
    return locala.getString(paramInt);
  }
  
  public final Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
  {
    if (paramInt == 0)
    {
      localView = a.d(a);
      paramViewGroup.addView(localView);
      return a.d(a);
    }
    View localView = a.e(a);
    paramViewGroup.addView(localView);
    return a.e(a);
  }
  
  public final boolean isViewFromObject(View paramView, Object paramObject)
  {
    return paramView == paramObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */