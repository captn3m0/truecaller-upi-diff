package com.truecaller.profile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog.Builder;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.g.a;
import com.truecaller.common.h.am;
import com.truecaller.common.h.o;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.SingleActivity;
import com.truecaller.ui.SingleActivity.FragmentSingle;
import com.truecaller.ui.components.CircularImageView;
import com.truecaller.ui.components.m;
import com.truecaller.ui.components.m.a;
import com.truecaller.ui.components.n.b;
import com.truecaller.ui.x;
import com.truecaller.util.at;
import com.truecaller.util.aw;
import com.truecaller.util.aw.e;
import com.truecaller.util.e.e;
import com.truecaller.utils.l;
import com.truecaller.utils.ui.b;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public final class EditMeFormFragment
  extends x
  implements View.OnClickListener, m.a, aw.e
{
  public static final int[] a;
  public static final int[] b;
  public static final int[] c;
  public static final int[] d;
  public static final int[] e;
  private l f;
  private a g;
  private Dialog h;
  private Bitmap i;
  private boolean l;
  private int m;
  private CircularImageView n;
  private TreeMap o;
  private Handler p;
  private Dialog q;
  
  static
  {
    int[] arrayOfInt1 = new int[4];
    arrayOfInt1[0] = 2131363094;
    arrayOfInt1[1] = 2131363586;
    arrayOfInt1[2] = 2131362537;
    arrayOfInt1[3] = 2131362536;
    a = arrayOfInt1;
    arrayOfInt1 = new int[3];
    arrayOfInt1[0] = 2131364599;
    arrayOfInt1[1] = 2131365579;
    arrayOfInt1[2] = 2131362471;
    b = arrayOfInt1;
    int j = 1;
    int[] arrayOfInt2 = new int[j];
    arrayOfInt2[0] = 2131362940;
    c = arrayOfInt2;
    arrayOfInt2 = new int[j];
    arrayOfInt2[0] = 2131365528;
    d = arrayOfInt2;
    arrayOfInt1 = new int[j];
    arrayOfInt1[0] = 2131362118;
    e = arrayOfInt1;
  }
  
  public EditMeFormFragment()
  {
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    p = localHandler;
  }
  
  private static Intent a(Activity paramActivity, int paramInt, TreeMap paramTreeMap, CountryListDto.a parama, boolean paramBoolean, Bitmap paramBitmap)
  {
    Object localObject1 = SingleActivity.FragmentSingle.EDIT_ME_FORM;
    localObject1 = SingleActivity.a(paramActivity, (SingleActivity.FragmentSingle)localObject1);
    Object localObject2 = "ARG_FORM_TYPE";
    Intent localIntent = ((Intent)localObject1).putExtra((String)localObject2, paramInt).putExtra("ARG_INITIAL_DATA", paramTreeMap);
    paramTreeMap = "ARG_SELECTED_COUNTRY";
    localObject1 = null;
    if (parama == null)
    {
      parama = null;
    }
    else
    {
      localObject2 = new com/google/gson/f;
      ((com.google.gson.f)localObject2).<init>();
      parama = ((com.google.gson.f)localObject2).b(parama);
    }
    localIntent = localIntent.putExtra(paramTreeMap, parama).putExtra("ARG_PHOTO_EDITED", paramBoolean);
    paramTreeMap = "ARG_PHOTO_PATH";
    if (paramBitmap != null) {
      localObject1 = a(paramActivity, paramBitmap);
    }
    return localIntent.putExtra(paramTreeMap, (String)localObject1);
  }
  
  public static Intent a(Activity paramActivity, TreeMap paramTreeMap, boolean paramBoolean, Bitmap paramBitmap)
  {
    return a(paramActivity, 1, paramTreeMap, null, paramBoolean, paramBitmap);
  }
  
  public static Intent a(Context paramContext)
  {
    SingleActivity.FragmentSingle localFragmentSingle = SingleActivity.FragmentSingle.EDIT_ME;
    return SingleActivity.c(paramContext, localFragmentSingle).putExtra("ARG_SHOW_PHOTO_SELECTOR", true);
  }
  
  private Contact a(Map paramMap, boolean paramBoolean)
  {
    int j = 2;
    Object localObject1 = new CharSequence[j];
    Object localObject2 = Integer.valueOf(2131363094);
    localObject2 = (CharSequence)paramMap.get(localObject2);
    localObject1[0] = localObject2;
    localObject2 = Integer.valueOf(2131363586);
    paramMap = (CharSequence)paramMap.get(localObject2);
    int k = 1;
    localObject1[k] = paramMap;
    paramMap = am.a(" ", (CharSequence[])localObject1);
    Contact localContact = new com/truecaller/data/entity/Contact;
    localContact.<init>();
    localContact.l(paramMap);
    paramMap = g;
    localObject1 = "profileNumber";
    paramMap = paramMap.a((String)localObject1);
    localContact.g(paramMap);
    if (paramBoolean)
    {
      paramMap = g;
      String str = "profileAvatar";
      paramMap = paramMap.a(str);
      localContact.j(paramMap);
    }
    return localContact;
  }
  
  private static String a(Context paramContext, Bitmap paramBitmap)
  {
    try
    {
      Object localObject1 = paramContext.getExternalCacheDir();
      if (localObject1 == null) {
        localObject1 = paramContext.getCacheDir();
      }
      paramContext = new java/io/File;
      Object localObject2 = "profile_tmp.png";
      paramContext.<init>((File)localObject1, (String)localObject2);
      localObject1 = new java/io/FileOutputStream;
      localObject2 = paramContext.getPath();
      int j = 0;
      ((FileOutputStream)localObject1).<init>((String)localObject2, false);
      localObject2 = Bitmap.CompressFormat.JPEG;
      j = 85;
      paramBitmap.compress((Bitmap.CompressFormat)localObject2, j, (OutputStream)localObject1);
      ((FileOutputStream)localObject1).close();
      return paramContext.getPath();
    }
    catch (Exception localException)
    {
      localException;
    }
    return null;
  }
  
  private TreeMap a(int[] paramArrayOfInt)
  {
    TreeMap localTreeMap = new java/util/TreeMap;
    localTreeMap.<init>();
    int j = paramArrayOfInt.length;
    int k = 0;
    while (k < j)
    {
      int i1 = paramArrayOfInt[k];
      Integer localInteger = Integer.valueOf(i1);
      View localView = getView();
      String str = ((EditText)localView.findViewById(i1)).getText().toString();
      localTreeMap.put(localInteger, str);
      k += 1;
    }
    return localTreeMap;
  }
  
  private void a(Bitmap paramBitmap)
  {
    i = paramBitmap;
    l = true;
    boolean bool = s();
    if (bool) {
      t();
    }
  }
  
  private static void a(Fragment paramFragment, int paramInt, TreeMap paramTreeMap, CountryListDto.a parama, boolean paramBoolean, Bitmap paramBitmap)
  {
    Intent localIntent = a(paramFragment.getActivity(), paramInt, paramTreeMap, parama, paramBoolean, paramBitmap);
    paramFragment.startActivityForResult(localIntent, 30);
  }
  
  public static void a(Fragment paramFragment, TreeMap paramTreeMap)
  {
    a(paramFragment, 3, paramTreeMap, null, false, null);
  }
  
  public static void a(Fragment paramFragment, TreeMap paramTreeMap, CountryListDto.a parama)
  {
    a(paramFragment, 2, paramTreeMap, parama, false, null);
  }
  
  public static void a(Fragment paramFragment, TreeMap paramTreeMap, boolean paramBoolean, Bitmap paramBitmap)
  {
    a(paramFragment, 1, paramTreeMap, null, paramBoolean, paramBitmap);
  }
  
  private void a(int[] paramArrayOfInt, TreeMap paramTreeMap)
  {
    int j = paramArrayOfInt.length;
    int k = 0;
    while (k < j)
    {
      int i1 = paramArrayOfInt[k];
      EditText localEditText = (EditText)getView().findViewById(i1);
      Object localObject = Integer.valueOf(i1);
      localObject = (CharSequence)paramTreeMap.get(localObject);
      localEditText.setText((CharSequence)localObject);
      k += 1;
    }
  }
  
  public static void b(Fragment paramFragment, TreeMap paramTreeMap)
  {
    a(paramFragment, 4, paramTreeMap, null, false, null);
  }
  
  public static void c(Fragment paramFragment, TreeMap paramTreeMap)
  {
    a(paramFragment, 5, paramTreeMap, null, false, null);
  }
  
  private void d(int paramInt)
  {
    e locale = c(paramInt);
    EditMeFormFragment.2 local2 = new com/truecaller/profile/EditMeFormFragment$2;
    local2.<init>(this);
    locale.b(this, local2);
  }
  
  private void h()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    boolean bool1 = m();
    int k = 3;
    int i1 = 2;
    EditMeFormFragment.Actions localActions1 = null;
    boolean bool2 = true;
    Object localObject2;
    if (!bool1)
    {
      localObject2 = new n.b[k];
      localObject3 = EditMeFormFragment.Actions.Social;
      localObject2[0] = localObject3;
      localObject3 = EditMeFormFragment.Actions.Camera;
      localObject2[bool2] = localObject3;
      localObject3 = EditMeFormFragment.Actions.Gallery;
      localObject2[i1] = localObject3;
      localObject2 = com.truecaller.ui.components.n.a((n.b[])localObject2);
    }
    else
    {
      int j = 4;
      localObject2 = new n.b[j];
      EditMeFormFragment.Actions localActions2 = EditMeFormFragment.Actions.Social;
      localObject2[0] = localActions2;
      localActions1 = EditMeFormFragment.Actions.Camera;
      localObject2[bool2] = localActions1;
      localActions1 = EditMeFormFragment.Actions.Gallery;
      localObject2[i1] = localActions1;
      localObject4 = EditMeFormFragment.Actions.Remove;
      localObject2[k] = localObject4;
      localObject2 = com.truecaller.ui.components.n.a((n.b[])localObject2);
    }
    Object localObject3 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject3).<init>((Context)localObject1);
    localObject1 = ((AlertDialog.Builder)localObject3).setTitle(2131886765);
    localObject3 = new com/truecaller/ui/components/m;
    Object localObject4 = getContext();
    ((m)localObject3).<init>((Context)localObject4, (List)localObject2, this);
    localObject4 = new com/truecaller/profile/-$$Lambda$EditMeFormFragment$vTrtPE1LYv3KZgPIWKRwXee9EZU;
    ((-..Lambda.EditMeFormFragment.vTrtPE1LYv3KZgPIWKRwXee9EZU)localObject4).<init>(this, (List)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).setAdapter((ListAdapter)localObject3, (DialogInterface.OnClickListener)localObject4).setCancelable(bool2).create();
    h = ((Dialog)localObject1);
    h.show();
  }
  
  private void i()
  {
    Intent localIntent = com.truecaller.common.h.n.a(getContext());
    o.a(this, localIntent, 1);
  }
  
  private boolean m()
  {
    boolean bool = l;
    if (bool)
    {
      Bitmap localBitmap = i;
      return localBitmap != null;
    }
    return am.a(g.a("profileAvatar"));
  }
  
  private void n()
  {
    Intent localIntent = com.truecaller.common.h.n.a();
    String str = getString(2131887194);
    localIntent = Intent.createChooser(localIntent, str);
    o.a(this, localIntent, 2);
  }
  
  private void t()
  {
    boolean bool1 = l;
    if (bool1)
    {
      localObject = i;
      if (localObject != null)
      {
        n.setImageBitmap((Bitmap)localObject);
        return;
      }
    }
    Object localObject = o;
    boolean bool2 = l ^ true;
    localObject = a((Map)localObject, bool2);
    n.a((Contact)localObject);
  }
  
  public final void a()
  {
    h = null;
    n = null;
    q = null;
    p = null;
  }
  
  protected final void a(Uri paramUri)
  {
    android.support.v4.app.f localf = getActivity();
    if (localf == null) {
      return;
    }
    EditMeFormFragment.3 local3 = new com/truecaller/profile/EditMeFormFragment$3;
    local3.<init>(this, paramUri);
    localf.runOnUiThread(local3);
  }
  
  public final void a(ImageView paramImageView) {}
  
  public final void a(ImageView paramImageView, Bitmap paramBitmap, String paramString)
  {
    boolean bool = s();
    if (!bool) {
      return;
    }
    i = paramBitmap;
    l = true;
    a(paramBitmap);
  }
  
  protected final void aa_()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf == null) {
      return;
    }
    -..Lambda.EditMeFormFragment.uNRU-HQVecN4JWEhn0yvj0WF9cM localuNRU-HQVecN4JWEhn0yvj0WF9cM = new com/truecaller/profile/-$$Lambda$EditMeFormFragment$uNRU-HQVecN4JWEhn0yvj0WF9cM;
    localuNRU-HQVecN4JWEhn0yvj0WF9cM.<init>(this);
    localf.runOnUiThread(localuNRU-HQVecN4JWEhn0yvj0WF9cM);
  }
  
  public final void b()
  {
    d(1);
    h.hide();
  }
  
  public final void b(ImageView paramImageView) {}
  
  public final void c()
  {
    h.hide();
    d(4);
  }
  
  protected final void g()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf == null) {
      return;
    }
    -..Lambda.EditMeFormFragment.Rnob2YPAVZYQuZBfYwZdFoOeboI localRnob2YPAVZYQuZBfYwZdFoOeboI = new com/truecaller/profile/-$$Lambda$EditMeFormFragment$Rnob2YPAVZYQuZBfYwZdFoOeboI;
    localRnob2YPAVZYQuZBfYwZdFoOeboI.<init>(this);
    localf.runOnUiThread(localRnob2YPAVZYQuZBfYwZdFoOeboI);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int j = -1;
    Object localObject;
    switch (paramInt1)
    {
    default: 
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
      break;
    case 3: 
      if (paramInt2 == j)
      {
        localObject = aw.b(com.truecaller.common.h.n.c(getContext()).getPath());
        a((Bitmap)localObject);
      }
      com.truecaller.common.h.n.f(getContext());
      return;
    case 2: 
      if (paramInt2 == j)
      {
        Uri localUri1 = paramIntent.getData();
        Uri localUri2 = com.truecaller.common.h.n.b(getContext());
        EditMeFormFragment.4 local4 = new com/truecaller/profile/EditMeFormFragment$4;
        android.support.v4.app.f localf = getActivity();
        local4.<init>(this, localf, localUri1, localUri2, localUri2);
        return;
      }
      break;
    case 1: 
      if (paramInt2 == j)
      {
        localObject = com.truecaller.common.h.n.b(getContext());
        a((Uri)localObject);
        return;
      }
      break;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = ((TrueApp)paramContext.getApplicationContext()).a();
    l locall = paramContext.bw();
    f = locall;
    paramContext = paramContext.I();
    g = paramContext;
  }
  
  public final void onClick(View paramView)
  {
    h();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    if (paramBundle == null) {
      return;
    }
    paramBundle = paramBundle.getIntent();
    int j = paramBundle.getIntExtra("ARG_FORM_TYPE", 1);
    m = j;
    Object localObject = "ARG_PHOTO_EDITED";
    boolean bool = paramBundle.getBooleanExtra((String)localObject, false);
    l = bool;
    bool = l;
    if (bool)
    {
      localObject = "ARG_PHOTO_PATH";
      paramBundle = paramBundle.getStringExtra((String)localObject);
      if (paramBundle != null)
      {
        localObject = BitmapFactory.decodeFile(paramBundle);
        i = ((Bitmap)localObject);
        localObject = new java/io/File;
        ((File)localObject).<init>(paramBundle);
        ((File)localObject).delete();
      }
    }
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenuInflater.inflate(2131623950, paramMenu);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int j = m;
    switch (j)
    {
    default: 
      paramLayoutInflater = null;
      break;
    case 5: 
      j = 2131559188;
      paramLayoutInflater = paramLayoutInflater.inflate(j, paramViewGroup, false);
      break;
    case 4: 
      j = 2131559192;
      paramLayoutInflater = paramLayoutInflater.inflate(j, paramViewGroup, false);
      break;
    case 3: 
      j = 2131559190;
      paramLayoutInflater = paramLayoutInflater.inflate(j, paramViewGroup, false);
      break;
    case 2: 
      j = 2131559189;
      paramLayoutInflater = paramLayoutInflater.inflate(j, paramViewGroup, false);
      break;
    case 1: 
      j = 2131559191;
      paramLayoutInflater = paramLayoutInflater.inflate(j, paramViewGroup, false);
      int k = 2131363998;
      paramViewGroup = (CircularImageView)paramLayoutInflater.findViewById(k);
      n = paramViewGroup;
    }
    return paramLayoutInflater;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    com.truecaller.common.h.n.f(getContext());
    Dialog localDialog = q;
    if (localDialog != null)
    {
      localDialog.hide();
      localDialog = null;
      q = null;
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    boolean bool1 = super.onOptionsItemSelected(paramMenuItem);
    int i1 = 1;
    if (bool1) {
      return i1;
    }
    int i2 = paramMenuItem.getItemId();
    int j = 2131361930;
    int i6 = 0;
    Object localObject1 = null;
    if (i2 == j)
    {
      i2 = m;
      if (i2 != i1)
      {
        j = 3;
        if (i2 != j)
        {
          i6 = 1;
        }
        else
        {
          paramMenuItem = at.f(o(), 2131362940);
          boolean bool2 = am.a(paramMenuItem);
          if (bool2)
          {
            boolean bool3 = am.c(paramMenuItem);
            if (!bool3)
            {
              int i3 = 2131886941;
              b(i3);
              break label195;
            }
          }
          i6 = 1;
        }
      }
      else
      {
        paramMenuItem = o();
        int k = 2131363094;
        paramMenuItem = at.f(paramMenuItem, k);
        boolean bool4 = am.a(paramMenuItem);
        if (!bool4)
        {
          int i4 = 2131886944;
          b(i4);
        }
        else
        {
          paramMenuItem = o();
          k = 2131363586;
          paramMenuItem = at.f(paramMenuItem, k);
          boolean bool5 = am.a(paramMenuItem);
          if (!bool5)
          {
            int i5 = 2131886950;
            b(i5);
          }
          else
          {
            i6 = 1;
          }
        }
      }
      label195:
      if (i6 != 0)
      {
        paramMenuItem = new android/content/Intent;
        paramMenuItem.<init>();
        Object localObject2 = "RESULT_DATA";
        i6 = m;
        switch (i6)
        {
        default: 
          i6 = 0;
          localObject1 = null;
          break;
        case 5: 
          localObject1 = e;
          localObject1 = a((int[])localObject1);
          break;
        case 4: 
          localObject1 = d;
          localObject1 = a((int[])localObject1);
          break;
        case 3: 
          localObject1 = c;
          localObject1 = a((int[])localObject1);
          break;
        case 2: 
          localObject1 = b;
          localObject1 = a((int[])localObject1);
          break;
        case 1: 
          localObject1 = a;
          localObject1 = a((int[])localObject1);
        }
        paramMenuItem.putExtra((String)localObject2, (Serializable)localObject1);
        localObject2 = getActivity();
        i6 = m;
        if (i6 == i1)
        {
          localObject1 = "RESULT_PHOTO_EDITED";
          boolean bool7 = l;
          paramMenuItem.putExtra((String)localObject1, bool7);
          boolean bool6 = l;
          if (bool6)
          {
            localObject1 = i;
            if (localObject1 != null)
            {
              localObject1 = a((Context)localObject2, (Bitmap)localObject1);
              String str = "RESULT_PHOTO_PATH";
              paramMenuItem.putExtra(str, (String)localObject1);
            }
          }
        }
        int i7 = -1;
        ((android.support.v4.app.f)localObject2).setResult(i7, paramMenuItem);
        ((android.support.v4.app.f)localObject2).finish();
      }
      return i1;
    }
    return false;
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int j = 1;
    l locall;
    if (paramInt != j)
    {
      switch (paramInt)
      {
      default: 
        break;
      case 202: 
        paramInt = paramArrayOfInt.length;
        if (paramInt <= 0) {
          break;
        }
        locall = null;
        paramInt = paramArrayOfInt[0];
        if (paramInt != 0) {
          break;
        }
        i();
        break;
      case 201: 
        locall = f;
        paramArrayOfString = new String[] { "android.permission.READ_EXTERNAL_STORAGE" };
        paramInt = locall.a(paramArrayOfString);
        if (paramInt == 0) {
          break;
        }
        n();
        return;
      }
    }
    else
    {
      locall = f;
      paramArrayOfString = new String[] { "android.permission.GET_ACCOUNTS" };
      paramInt = locall.a(paramArrayOfString);
      if (paramInt != 0)
      {
        d(4);
        return;
      }
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = getActivity();
    if (paramBundle == null) {
      return;
    }
    boolean bool1 = true;
    setHasOptionsMenu(bool1);
    Object localObject1 = k();
    int k = 16842808;
    paramBundle = b.a(paramBundle, 2131233794, k);
    ((ActionBar)localObject1).setHomeAsUpIndicator(paramBundle);
    paramBundle = getActivity().getIntent();
    localObject1 = new java/util/TreeMap;
    Map localMap = (Map)paramBundle.getSerializableExtra("ARG_INITIAL_DATA");
    ((TreeMap)localObject1).<init>(localMap);
    o = ((TreeMap)localObject1);
    int i1 = m;
    localMap = null;
    int i2;
    Object localObject2;
    int j;
    switch (i1)
    {
    default: 
      break;
    case 5: 
      k().setTitle(2131886933);
      paramBundle = e;
      localObject1 = o;
      a(paramBundle, (TreeMap)localObject1);
      i2 = 2131362118;
      paramBundle = (EditText)paramView.findViewById(i2);
      paramView = (TextView)paramView.findViewById(2131362120);
      i1 = 2131886934;
      localObject2 = new Object[bool1];
      k = paramBundle.length();
      k = 160 - k;
      Integer localInteger = Integer.valueOf(k);
      localObject2[0] = localInteger;
      localObject2 = getString(i1, (Object[])localObject2);
      paramView.setText((CharSequence)localObject2);
      localObject2 = new com/truecaller/profile/EditMeFormFragment$1;
      ((EditMeFormFragment.1)localObject2).<init>(this, paramView);
      paramBundle.addTextChangedListener((TextWatcher)localObject2);
      break;
    case 4: 
      k().setTitle(2131886965);
      paramView = d;
      paramBundle = o;
      a(paramView, paramBundle);
      return;
    case 3: 
      k().setTitle(2131886940);
      paramView = c;
      paramBundle = o;
      a(paramView, paramBundle);
      return;
    case 2: 
      localObject2 = k();
      i1 = 2131886929;
      ((ActionBar)localObject2).setTitle(i1);
      localObject2 = "ARG_SELECTED_COUNTRY";
      bool1 = paramBundle.hasExtra((String)localObject2);
      if (bool1)
      {
        localObject2 = new com/google/gson/f;
        ((com.google.gson.f)localObject2).<init>();
        paramBundle = paramBundle.getStringExtra("ARG_SELECTED_COUNTRY");
        localObject1 = CountryListDto.a.class;
        paramBundle = (CountryListDto.a)((com.google.gson.f)localObject2).a(paramBundle, (Class)localObject1);
        j = 2131362609;
        paramView = (EditText)paramView.findViewById(j);
        paramBundle = b;
        paramView.setText(paramBundle);
      }
      paramView = b;
      paramBundle = o;
      a(paramView, paramBundle);
      return;
    case 1: 
      paramBundle = k();
      j = 0;
      localObject2 = null;
      paramBundle.setTitle(null);
      i2 = 2131363940;
      paramView.findViewById(i2).setOnClickListener(this);
      t();
      paramView = a;
      paramBundle = o;
      a(paramView, paramBundle);
      paramView = getActivity().getIntent();
      paramBundle = "ARG_SHOW_PHOTO_SELECTOR";
      boolean bool2 = paramView.getBooleanExtra(paramBundle, false);
      if (bool2)
      {
        h();
        return;
      }
      break;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.EditMeFormFragment
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */