package com.truecaller.profile;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.k;
import com.truecaller.bp;
import com.truecaller.common.h.ah;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.common.tag.d;
import com.truecaller.data.entity.Contact;
import com.truecaller.featuretoggles.b;
import com.truecaller.old.data.access.Settings;
import com.truecaller.tag.TagPickActivity;
import com.truecaller.ui.SingleActivity;
import com.truecaller.ui.SingleActivity.FragmentSingle;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.components.CallerButtonBase;
import com.truecaller.ui.components.CircularImageView;
import com.truecaller.ui.components.NewComboBase;
import com.truecaller.ui.components.n;
import com.truecaller.ui.w;
import com.truecaller.util.at;
import com.truecaller.util.aw;
import com.truecaller.util.aw.e;
import com.truecaller.util.e.g;
import com.truecaller.utils.l;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public final class a
  extends w
  implements View.OnClickListener, aw.e
{
  private CallerButtonBase A;
  private NewComboBase B;
  private TextView C;
  private TextView D;
  private TextView E;
  private boolean F;
  private boolean G;
  private boolean H;
  private k I;
  private com.truecaller.androidactors.a J;
  private com.truecaller.androidactors.f K;
  private com.truecaller.common.profile.e L;
  private com.truecaller.androidactors.f M;
  private com.truecaller.common.h.ac N;
  private l O;
  private com.truecaller.utils.i P;
  private com.truecaller.common.g.a Q;
  private com.truecaller.featuretoggles.e R;
  private AlertDialog S;
  TreeMap a;
  aw b;
  private TreeMap i;
  private CountryListDto.a l;
  private Map m;
  private Bitmap n;
  private boolean o;
  private boolean p;
  private View q;
  private Button r;
  private Button s;
  private CallerButtonBase t;
  private CallerButtonBase u;
  private CallerButtonBase v;
  private CallerButtonBase w;
  private CallerButtonBase x;
  private View y;
  private View z;
  
  public a()
  {
    TreeMap localTreeMap = new java/util/TreeMap;
    localTreeMap.<init>();
    i = localTreeMap;
    localTreeMap = new java/util/TreeMap;
    localTreeMap.<init>();
    a = localTreeMap;
  }
  
  private Contact a(Map paramMap, boolean paramBoolean)
  {
    int j = 2;
    Object localObject1 = new CharSequence[j];
    Object localObject2 = Integer.valueOf(2131363094);
    localObject2 = (CharSequence)paramMap.get(localObject2);
    localObject1[0] = localObject2;
    localObject2 = Integer.valueOf(2131363586);
    paramMap = (CharSequence)paramMap.get(localObject2);
    int k = 1;
    localObject1[k] = paramMap;
    paramMap = am.a(" ", (CharSequence[])localObject1);
    Contact localContact = new com/truecaller/data/entity/Contact;
    localContact.<init>();
    localContact.l(paramMap);
    paramMap = Q;
    localObject1 = "profileNumber";
    paramMap = paramMap.a((String)localObject1);
    localContact.g(paramMap);
    if (paramBoolean)
    {
      paramMap = Q;
      String str = "profileAvatar";
      paramMap = paramMap.a(str);
      localContact.j(paramMap);
    }
    return localContact;
  }
  
  private void a(int paramInt, boolean paramBoolean)
  {
    com.truecaller.util.e.e locale = c(paramInt);
    a.1 local1 = new com/truecaller/profile/a$1;
    local1.<init>(this, paramBoolean);
    locale.b(this, local1);
  }
  
  public static void a(Context paramContext)
  {
    Object localObject = SingleActivity.FragmentSingle.EDIT_ME;
    localObject = SingleActivity.c(paramContext, (SingleActivity.FragmentSingle)localObject);
    paramContext.startActivity((Intent)localObject);
  }
  
  private static void a(CallerButtonBase paramCallerButtonBase, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramCallerButtonBase.setHeadingTextStyle(2131952076);
      paramCallerButtonBase.setDetailsTextStyle(2131952074);
      paramCallerButtonBase.setRightImage(2131234073);
      return;
    }
    paramCallerButtonBase.setHeadingTextStyle(2131952077);
    paramCallerButtonBase.setDetailsTextStyle(2131952075);
    paramCallerButtonBase.setRightImage(2131233817);
  }
  
  private static void a(boolean paramBoolean, Button paramButton)
  {
    if (paramButton != null)
    {
      if (paramBoolean) {
        paramBoolean = 2131234071;
      } else {
        paramBoolean = false;
      }
      paramButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, paramBoolean, 0);
    }
  }
  
  public static Intent b(Context paramContext)
  {
    SingleActivity.FragmentSingle localFragmentSingle = SingleActivity.FragmentSingle.EDIT_ME;
    return SingleActivity.c(paramContext, localFragmentSingle);
  }
  
  public static Intent c(Context paramContext)
  {
    SingleActivity.FragmentSingle localFragmentSingle = SingleActivity.FragmentSingle.EDIT_ME;
    paramContext = SingleActivity.c(paramContext, localFragmentSingle);
    paramContext.putExtra("conversionFromBusiness", true);
    return paramContext;
  }
  
  private void d(int paramInt)
  {
    Object localObject1 = P;
    boolean bool1 = ((com.truecaller.utils.i)localObject1).a();
    if (!bool1)
    {
      com.truecaller.utils.extensions.i.a(requireContext());
      return;
    }
    localObject1 = c(paramInt);
    Object localObject2 = ((com.truecaller.util.e.e)localObject1).f();
    boolean bool2 = ((g)localObject2).a();
    if (bool2)
    {
      localObject2 = new android/support/v7/app/AlertDialog$Builder;
      android.support.v4.app.f localf = getActivity();
      ((AlertDialog.Builder)localObject2).<init>(localf);
      localObject2 = ((AlertDialog.Builder)localObject2).setMessage(2131887283).setCancelable(false);
      -..Lambda.a.QKGMB69WjwzCT48Rk8IspF8c7vI localQKGMB69WjwzCT48Rk8IspF8c7vI = new com/truecaller/profile/-$$Lambda$a$QKGMB69WjwzCT48Rk8IspF8c7vI;
      localQKGMB69WjwzCT48Rk8IspF8c7vI.<init>(this, (com.truecaller.util.e.e)localObject1, paramInt);
      AlertDialog localAlertDialog = ((AlertDialog.Builder)localObject2).setPositiveButton(2131887235, localQKGMB69WjwzCT48Rk8IspF8c7vI).setNegativeButton(2131887214, null).show();
      S = localAlertDialog;
      return;
    }
    p = true;
    ((com.truecaller.util.e.e)localObject1).d();
  }
  
  private void g()
  {
    int j = 2;
    Object localObject1 = new CharSequence[j];
    Object localObject2 = a;
    Integer localInteger1 = Integer.valueOf(2131363094);
    localObject2 = (CharSequence)((TreeMap)localObject2).get(localInteger1);
    localObject1[0] = localObject2;
    localObject2 = a;
    Integer localInteger2 = Integer.valueOf(2131363586);
    localObject2 = (CharSequence)((TreeMap)localObject2).get(localInteger2);
    int k = 1;
    localObject1[k] = localObject2;
    String str = am.a(" ", (CharSequence[])localObject1);
    at.b(C, str);
    l().setTitle(str);
    CharSequence[] arrayOfCharSequence = new CharSequence[j];
    localObject1 = a;
    localObject2 = Integer.valueOf(2131362536);
    localObject1 = (CharSequence)((TreeMap)localObject1).get(localObject2);
    arrayOfCharSequence[0] = localObject1;
    localObject1 = a;
    localObject2 = Integer.valueOf(2131362537);
    localObject1 = (CharSequence)((TreeMap)localObject1).get(localObject2);
    arrayOfCharSequence[k] = localObject1;
    str = am.a(" @ ", arrayOfCharSequence);
    at.b(E, str);
    at.b(D, "");
    b();
    c();
    u();
  }
  
  private Map h()
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    Object localObject2 = a.entrySet().iterator();
    Object localObject3;
    Object localObject4;
    for (;;)
    {
      bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      localObject4 = i;
      Object localObject5 = ((Map.Entry)localObject3).getKey();
      localObject4 = (String)((TreeMap)localObject4).get(localObject5);
      localObject3 = ((Map.Entry)localObject3).getValue();
      ((Map)localObject1).put(localObject4, localObject3);
    }
    localObject2 = "profileAcceptAuto";
    boolean bool = G;
    if (bool)
    {
      localObject3 = "0";
    }
    else
    {
      localObject3 = Q;
      localObject4 = "profileAcceptAuto";
      localObject3 = ((com.truecaller.common.g.a)localObject3).a((String)localObject4);
    }
    ((Map)localObject1).put(localObject2, localObject3);
    localObject2 = B;
    if (localObject2 == null)
    {
      localObject2 = "profileGender";
      localObject3 = Q;
      localObject4 = "profileGender";
      localObject3 = ((com.truecaller.common.g.a)localObject3).a((String)localObject4);
      ((Map)localObject1).put(localObject2, localObject3);
    }
    else
    {
      localObject3 = "profileGender";
      localObject2 = String.valueOf(((NewComboBase)localObject2).getSelection().q());
      ((Map)localObject1).put(localObject3, localObject2);
    }
    localObject2 = new java/util/HashMap;
    ((HashMap)localObject2).<init>();
    com.truecaller.common.network.g.c.a((Map)localObject1, (Map)localObject2);
    localObject1 = "avatar_enabled";
    bool = t();
    if (bool) {
      localObject3 = "1";
    } else {
      localObject3 = "0";
    }
    ((Map)localObject2).put(localObject1, localObject3);
    return (Map)localObject2;
  }
  
  private Long i()
  {
    Object localObject = a;
    int j = 2131364648;
    Integer localInteger1 = Integer.valueOf(j);
    localObject = (CharSequence)((TreeMap)localObject).get(localInteger1);
    boolean bool = am.a((CharSequence)localObject);
    if (bool)
    {
      localObject = a;
      Integer localInteger2 = Integer.valueOf(j);
      return Long.valueOf(Long.parseLong((String)((TreeMap)localObject).get(localInteger2)));
    }
    return null;
  }
  
  private void m()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    localBuilder.<init>((Context)localObject1);
    localObject1 = localBuilder.setTitle(2131886236).setMessage(2131886235);
    Object localObject2 = new com/truecaller/profile/-$$Lambda$a$aEPpS74x3tJT1f3Fysq9UAwUQ-8;
    ((-..Lambda.a.aEPpS74x3tJT1f3Fysq9UAwUQ-8)localObject2).<init>(this);
    localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton(2131887201, (DialogInterface.OnClickListener)localObject2);
    localObject2 = -..Lambda.a.wzOzCkm6aIOh8qTxWJzxNbQQkMs.INSTANCE;
    ((AlertDialog.Builder)localObject1).setNegativeButton(2131887197, (DialogInterface.OnClickListener)localObject2).show();
  }
  
  private void n()
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    boolean bool1 = G;
    if (bool1)
    {
      bool1 = H;
      if (!bool1)
      {
        m();
        return;
      }
    }
    localObject1 = n;
    Object localObject2;
    if (localObject1 == null)
    {
      bool1 = false;
      localObject1 = null;
      localObject2 = null;
    }
    else
    {
      com.truecaller.common.network.util.e locale = new com/truecaller/common/network/util/e;
      locale.<init>((Bitmap)localObject1);
      localObject2 = locale;
    }
    com.truecaller.common.profile.e locale1 = L;
    boolean bool2 = o;
    Long localLong = i();
    Map localMap = h();
    -..Lambda.a.k2K1Ue8kQ4muKVYmOvsvfE7wp98 localk2K1Ue8kQ4muKVYmOvsvfE7wp98 = new com/truecaller/profile/-$$Lambda$a$k2K1Ue8kQ4muKVYmOvsvfE7wp98;
    localk2K1Ue8kQ4muKVYmOvsvfE7wp98.<init>(this);
    locale1.a(bool2, (okhttp3.ac)localObject2, true, localLong, localMap, true, localk2K1Ue8kQ4muKVYmOvsvfE7wp98);
  }
  
  private boolean t()
  {
    boolean bool = o;
    if (bool)
    {
      Bitmap localBitmap = n;
      return localBitmap != null;
    }
    return am.a(Q.a("profileAvatar"));
  }
  
  private void u()
  {
    boolean bool1 = o;
    if (bool1)
    {
      localObject = n;
      if (localObject != null)
      {
        localObject = g;
        Bitmap localBitmap = n;
        ((CircularImageView)localObject).setImageBitmap(localBitmap);
        localBitmap = n;
        a(null, localBitmap);
        return;
      }
    }
    Object localObject = a;
    boolean bool2 = o ^ true;
    localObject = a((Map)localObject, bool2);
    g.a((Contact)localObject);
  }
  
  public final boolean W_()
  {
    Object localObject1 = m;
    if (localObject1 != null)
    {
      localObject2 = h();
      boolean bool = ((Map)localObject1).equals(localObject2);
      if (bool)
      {
        bool = o;
        if (!bool)
        {
          bool = G;
          if (!bool) {
            return false;
          }
        }
      }
    }
    localObject1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = getActivity();
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).setMessage(2131886954);
    Object localObject3 = new com/truecaller/profile/-$$Lambda$a$w2WQs9O2jB0TNoojKWRCQLw3ELA;
    ((-..Lambda.a.w2WQs9O2jB0TNoojKWRCQLw3ELA)localObject3).<init>(this);
    localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject3);
    localObject3 = new com/truecaller/profile/-$$Lambda$a$8_-sPOhcnPRDam0m_PlrumlVYpM;
    ((-..Lambda.a.8_-sPOhcnPRDam0m_PlrumlVYpM)localObject3).<init>(this);
    localObject1 = ((AlertDialog.Builder)localObject1).setNegativeButton(2131887214, (DialogInterface.OnClickListener)localObject3).setCancelable(false).show();
    S = ((AlertDialog)localObject1);
    return true;
  }
  
  public final void a()
  {
    super.a();
    q = null;
    r = null;
    s = null;
    t = null;
    u = null;
    v = null;
    w = null;
    z = null;
    A = null;
    B = null;
  }
  
  public final void a(int paramInt)
  {
    super.a(paramInt);
    int j = 1;
    Button localButton;
    if (paramInt != j)
    {
      int k = 4;
      if (paramInt == k)
      {
        localButton = r;
        a(j, localButton);
      }
    }
    else
    {
      localButton = s;
      a(j, localButton);
    }
    boolean bool = p;
    a(paramInt, bool);
    p = false;
  }
  
  public final boolean a(String paramString)
  {
    boolean bool1 = o;
    if (!bool1)
    {
      boolean bool2 = super.a(paramString);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  final void b()
  {
    Object localObject1 = a;
    int j = 2131364599;
    Object localObject2 = Integer.valueOf(j);
    localObject1 = (CharSequence)((TreeMap)localObject1).get(localObject2);
    boolean bool1 = am.a((CharSequence)localObject1);
    int k = 2131362471;
    int i1 = 2131365579;
    boolean bool2 = true;
    if (!bool1)
    {
      localObject1 = a;
      localObject3 = Integer.valueOf(i1);
      localObject1 = (CharSequence)((TreeMap)localObject1).get(localObject3);
      bool1 = am.a((CharSequence)localObject1);
      if (!bool1)
      {
        localObject1 = a;
        localObject3 = Integer.valueOf(k);
        localObject1 = (CharSequence)((TreeMap)localObject1).get(localObject3);
        bool1 = am.a((CharSequence)localObject1);
        if (!bool1)
        {
          a(u, false);
          localObject1 = u;
          localObject4 = getString(2131886926);
          ((CallerButtonBase)localObject1).setHeadingText((CharSequence)localObject4);
          localObject1 = u;
          j = 2131886958;
          localObject4 = getString(j);
          ((CallerButtonBase)localObject1).setDetailsText((CharSequence)localObject4);
          break label340;
        }
      }
    }
    a(u, bool2);
    localObject1 = u;
    int i2 = 2;
    String[] arrayOfString = new String[i2];
    TreeMap localTreeMap = a;
    Object localObject4 = Integer.valueOf(j);
    localObject4 = (String)localTreeMap.get(localObject4);
    arrayOfString[0] = localObject4;
    Object localObject3 = new CharSequence[i2];
    localTreeMap = a;
    Object localObject5 = Integer.valueOf(i1);
    localObject5 = (CharSequence)localTreeMap.get(localObject5);
    localObject3[0] = localObject5;
    localObject5 = a;
    localObject2 = Integer.valueOf(k);
    localObject2 = (CharSequence)((TreeMap)localObject5).get(localObject2);
    localObject3[bool2] = localObject2;
    localObject4 = am.a(" ", (CharSequence[])localObject3);
    arrayOfString[bool2] = localObject4;
    localObject4 = am.a(arrayOfString);
    ((CallerButtonBase)localObject1).setHeadingText((CharSequence)localObject4);
    localObject1 = u;
    j = 2131886929;
    localObject4 = getString(j);
    ((CallerButtonBase)localObject1).setDetailsText((CharSequence)localObject4);
    label340:
    localObject1 = a;
    j = 2131362940;
    localObject2 = Integer.valueOf(j);
    localObject1 = (CharSequence)((TreeMap)localObject1).get(localObject2);
    bool1 = am.a((CharSequence)localObject1);
    k = 2131886940;
    if (bool1)
    {
      a(v, bool2);
      localObject1 = v;
      localObject5 = a;
      localObject4 = Integer.valueOf(j);
      localObject4 = (CharSequence)((TreeMap)localObject5).get(localObject4);
      ((CallerButtonBase)localObject1).setHeadingText((CharSequence)localObject4);
      localObject1 = v;
      localObject4 = getString(k);
      ((CallerButtonBase)localObject1).setDetailsText((CharSequence)localObject4);
    }
    else
    {
      a(v, false);
      localObject1 = v;
      localObject4 = getString(k);
      ((CallerButtonBase)localObject1).setHeadingText((CharSequence)localObject4);
      localObject1 = v;
      j = 2131886959;
      localObject4 = getString(j);
      ((CallerButtonBase)localObject1).setDetailsText((CharSequence)localObject4);
    }
    localObject1 = a;
    j = 2131365528;
    localObject2 = Integer.valueOf(j);
    localObject1 = (CharSequence)((TreeMap)localObject1).get(localObject2);
    bool1 = am.a((CharSequence)localObject1);
    if (bool1)
    {
      a(w, bool2);
      localObject1 = w;
      localObject2 = a;
      localObject4 = Integer.valueOf(j);
      localObject4 = (CharSequence)((TreeMap)localObject2).get(localObject4);
      ((CallerButtonBase)localObject1).setHeadingText((CharSequence)localObject4);
      localObject1 = w;
      j = 2131886965;
      localObject4 = getString(j);
      ((CallerButtonBase)localObject1).setDetailsText((CharSequence)localObject4);
    }
    else
    {
      a(w, false);
      localObject1 = w;
      localObject4 = getString(2131886928);
      ((CallerButtonBase)localObject1).setHeadingText((CharSequence)localObject4);
      localObject1 = w;
      j = 2131886960;
      localObject4 = getString(j);
      ((CallerButtonBase)localObject1).setDetailsText((CharSequence)localObject4);
    }
    localObject1 = a;
    j = 2131364648;
    localObject2 = Integer.valueOf(j);
    localObject1 = (CharSequence)((TreeMap)localObject1).get(localObject2);
    bool1 = am.a((CharSequence)localObject1);
    k = 0;
    localObject2 = null;
    if (bool1)
    {
      localObject1 = a;
      localObject4 = Integer.valueOf(j);
      long l1 = Long.valueOf((String)((TreeMap)localObject1).get(localObject4)).longValue();
      localObject1 = d.a(l1);
    }
    else
    {
      bool1 = false;
      localObject1 = null;
    }
    localObject4 = x;
    ((CallerButtonBase)localObject4).setTag((com.truecaller.common.tag.c)localObject1);
    if (localObject1 != null)
    {
      a(x, bool2);
      x.setHeadingText(null);
      return;
    }
    a(x, false);
    localObject1 = x;
    localObject4 = getString(2131886963);
    ((CallerButtonBase)localObject1).setHeadingText((CharSequence)localObject4);
  }
  
  final void b(String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(3);
    Object localObject = new com/truecaller/ui/components/n;
    String str = getString(2131886948);
    ((n)localObject).<init>(str, "", "N");
    localArrayList.add(localObject);
    localObject = new com/truecaller/ui/components/n;
    str = getString(2131886947);
    ((n)localObject).<init>(str, "", "M");
    localArrayList.add(localObject);
    localObject = new com/truecaller/ui/components/n;
    str = getString(2131886946);
    ((n)localObject).<init>(str, "", "F");
    localArrayList.add(localObject);
    B.setData(localArrayList);
    localObject = B;
    paramString = at.a(localArrayList, paramString);
    ((NewComboBase)localObject).setSelection(paramString);
  }
  
  final void c()
  {
    Object localObject1 = a;
    int j = 2131362118;
    Object localObject2 = Integer.valueOf(j);
    localObject1 = (CharSequence)((TreeMap)localObject1).get(localObject2);
    boolean bool1 = am.a((CharSequence)localObject1);
    boolean bool2 = true;
    if (bool1)
    {
      a(A, bool2);
      A.setSingleLine(false);
      A.setMaxLines(-1 >>> 1);
      localObject1 = A;
      localObject2 = a;
      localObject3 = Integer.valueOf(j);
      localObject3 = (CharSequence)((TreeMap)localObject2).get(localObject3);
      ((CallerButtonBase)localObject1).setHeadingText((CharSequence)localObject3);
      A.setHeadingTextStyle(2131952073);
      return;
    }
    a(A, false);
    A.setSingleLine(bool2);
    localObject1 = A;
    Object localObject3 = getString(2131886927);
    ((CallerButtonBase)localObject1).setHeadingText((CharSequence)localObject3);
  }
  
  public final void d()
  {
    Button localButton = s;
    a(true, localButton);
  }
  
  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int j = -1;
    int k = 30;
    Object localObject1;
    Object localObject2;
    Object localObject3;
    if ((paramInt1 == k) && (paramInt2 == j))
    {
      localObject1 = new java/util/TreeMap;
      localObject2 = (Map)paramIntent.getSerializableExtra("RESULT_DATA");
      ((TreeMap)localObject1).<init>((Map)localObject2);
      localObject1 = ((TreeMap)localObject1).entrySet().iterator();
      for (;;)
      {
        paramInt2 = ((Iterator)localObject1).hasNext();
        if (paramInt2 == 0) {
          break;
        }
        localObject2 = (Map.Entry)((Iterator)localObject1).next();
        localObject3 = a;
        Object localObject4 = ((Map.Entry)localObject2).getKey();
        localObject2 = ((Map.Entry)localObject2).getValue();
        ((TreeMap)localObject3).put(localObject4, localObject2);
      }
      localObject1 = "RESULT_COUNTRY";
      paramInt1 = paramIntent.hasExtra((String)localObject1);
      if (paramInt1 != 0)
      {
        localObject1 = new com/google/gson/f;
        ((com.google.gson.f)localObject1).<init>();
        localObject2 = paramIntent.getStringExtra("RESULT_COUNTRY");
        localObject3 = CountryListDto.a.class;
        localObject1 = (CountryListDto.a)((com.google.gson.f)localObject1).a((String)localObject2, (Class)localObject3);
        l = ((CountryListDto.a)localObject1);
      }
      localObject1 = "RESULT_PHOTO_EDITED";
      paramInt2 = 0;
      localObject2 = null;
      paramInt1 = paramIntent.getBooleanExtra((String)localObject1, false);
      if (paramInt1 != 0)
      {
        paramInt1 = 1;
        o = paramInt1;
        localObject2 = paramIntent.getStringExtra("RESULT_PHOTO_PATH");
        if (localObject2 == null)
        {
          paramInt2 = 0;
          localObject2 = null;
          n = null;
        }
        else
        {
          paramIntent = BitmapFactory.decodeFile((String)localObject2);
          n = paramIntent;
          paramIntent = new java/io/File;
          paramIntent.<init>((String)localObject2);
          paramIntent.delete();
        }
        localObject2 = getActivity();
        paramIntent = a;
        localObject1 = a(paramIntent, paramInt1);
        localObject2 = aw.b((Context)localObject2);
        ((aw)localObject2).a((Contact)localObject1);
        u();
      }
      g();
      return;
    }
    k = 101;
    if ((paramInt1 == k) && (paramInt2 == j))
    {
      localObject1 = "tag_id";
      long l1 = Long.MIN_VALUE;
      long l2 = paramIntent.getLongExtra((String)localObject1, l1);
      int i1 = 2131364648;
      boolean bool = l2 < l1;
      if (bool)
      {
        localObject3 = a;
        paramIntent = Integer.valueOf(i1);
        localObject1 = String.valueOf(l2);
        ((TreeMap)localObject3).put(paramIntent, localObject1);
      }
      else
      {
        localObject1 = a;
        localObject2 = Integer.valueOf(i1);
        ((TreeMap)localObject1).remove(localObject2);
      }
      g();
      return;
    }
    if ((paramInt1 == 0) && (paramInt2 == j))
    {
      localObject1 = new android/content/Intent;
      localObject2 = getActivity();
      paramIntent = TruecallerInit.class;
      ((Intent)localObject1).<init>((Context)localObject2, paramIntent);
      paramInt2 = 335577088;
      ((Intent)localObject1).addFlags(paramInt2);
      startActivity((Intent)localObject1);
    }
  }
  
  public final void onClick(View paramView)
  {
    int j = paramView.getId();
    int i1 = 2131363198;
    if (j == i1)
    {
      d(4);
      return;
    }
    i1 = 2131363027;
    int i7 = 1;
    if (j == i1)
    {
      d(i7);
      return;
    }
    i1 = 2131363930;
    Object localObject1 = null;
    Object localObject2;
    Object localObject3;
    if (j == i1)
    {
      paramView = new android/support/v7/app/AlertDialog$Builder;
      localObject2 = getContext();
      paramView.<init>((Context)localObject2);
      paramView = paramView.setIcon(2131234119).setTitle(2131886956);
      localObject3 = new Object[i7];
      String str = c.a(Q);
      localObject3[0] = str;
      localObject2 = getString(2131886955, (Object[])localObject3);
      paramView = paramView.setMessage((CharSequence)localObject2).setNegativeButton(2131887197, null);
      localObject3 = new com/truecaller/profile/-$$Lambda$a$3688M3PJlGQVqag5OuTUFY0gzyU;
      ((-..Lambda.a.3688M3PJlGQVqag5OuTUFY0gzyU)localObject3).<init>(this, false);
      paramView.setPositiveButton(2131887202, (DialogInterface.OnClickListener)localObject3).show();
      return;
    }
    i1 = 2131361990;
    int k;
    if (j == i1)
    {
      k = F;
      if (k == 0)
      {
        paramView = a;
        localObject2 = l;
        EditMeFormFragment.a(this, paramView, (CountryListDto.a)localObject2);
        F = i7;
      }
    }
    else
    {
      i1 = 2131362941;
      if (k == i1)
      {
        k = F;
        if (k == 0)
        {
          paramView = a;
          EditMeFormFragment.a(this, paramView);
          F = i7;
        }
      }
      else
      {
        int i2 = 2131365532;
        if (k == i2)
        {
          k = F;
          if (k == 0)
          {
            paramView = a;
            EditMeFormFragment.b(this, paramView);
            F = i7;
          }
        }
        else
        {
          int i3 = 2131362119;
          if (k == i3)
          {
            k = F;
            if (k == 0)
            {
              paramView = a;
              EditMeFormFragment.c(this, paramView);
              F = i7;
            }
          }
          else
          {
            int i4 = 2131363991;
            if (k == i4)
            {
              k = F;
              if (k == 0)
              {
                paramView = a;
                boolean bool = o;
                localObject1 = n;
                EditMeFormFragment.a(this, paramView, bool, (Bitmap)localObject1);
                F = i7;
              }
              return;
            }
            int i5 = 2131364648;
            if (k == i5)
            {
              paramView = a;
              localObject3 = Integer.valueOf(i5);
              paramView = (CharSequence)paramView.get(localObject3);
              k = am.a(paramView);
              if (k != 0)
              {
                paramView = a;
                localObject2 = Integer.valueOf(i5);
                paramView = (String)paramView.get(localObject2);
                localObject1 = Long.valueOf(paramView);
              }
              paramView = TagPickActivity.a(getActivity(), (Long)localObject1, 3);
              startActivityForResult(paramView, 101);
              return;
            }
            int i6 = 2131362272;
            if (k == i6)
            {
              paramView = BusinessProfileOnboardingActivity.a(getActivity());
              startActivityForResult(paramView, 0);
            }
          }
        }
      }
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = TrueApp.y().a();
    Object localObject1 = paramBundle.w();
    K = ((com.truecaller.androidactors.f)localObject1);
    localObject1 = paramBundle.m();
    I = ((k)localObject1);
    localObject1 = paramBundle.bf();
    L = ((com.truecaller.common.profile.e)localObject1);
    localObject1 = paramBundle.bi();
    M = ((com.truecaller.androidactors.f)localObject1);
    localObject1 = paramBundle.J();
    N = ((com.truecaller.common.h.ac)localObject1);
    localObject1 = paramBundle.bw();
    O = ((l)localObject1);
    localObject1 = paramBundle.v();
    P = ((com.truecaller.utils.i)localObject1);
    localObject1 = paramBundle.I();
    Q = ((com.truecaller.common.g.a)localObject1);
    paramBundle = paramBundle.aF();
    R = paramBundle;
    paramBundle = i;
    localObject1 = Integer.valueOf(2131363094);
    paramBundle.put(localObject1, "profileFirstName");
    paramBundle = i;
    localObject1 = Integer.valueOf(2131363586);
    paramBundle.put(localObject1, "profileLastName");
    paramBundle = i;
    localObject1 = Integer.valueOf(2131362471);
    paramBundle.put(localObject1, "profileCity");
    paramBundle = i;
    localObject1 = Integer.valueOf(2131365579);
    paramBundle.put(localObject1, "profileZip");
    paramBundle = i;
    int j = 2131364599;
    Object localObject2 = Integer.valueOf(j);
    paramBundle.put(localObject2, "profileStreet");
    paramBundle = i;
    localObject2 = Integer.valueOf(2131362940);
    paramBundle.put(localObject2, "profileEmail");
    paramBundle = i;
    localObject2 = Integer.valueOf(2131365528);
    paramBundle.put(localObject2, "profileWeb");
    paramBundle = i;
    localObject2 = Integer.valueOf(2131363026);
    paramBundle.put(localObject2, "profileFacebook");
    paramBundle = i;
    localObject2 = Integer.valueOf(2131362537);
    paramBundle.put(localObject2, "profileCompanyName");
    paramBundle = i;
    localObject2 = Integer.valueOf(2131362536);
    paramBundle.put(localObject2, "profileCompanyJob");
    paramBundle = i;
    int k = 2131362118;
    Integer localInteger1 = Integer.valueOf(k);
    paramBundle.put(localInteger1, "profileStatus");
    paramBundle = i;
    localInteger1 = Integer.valueOf(2131363167);
    paramBundle.put(localInteger1, "profileGender");
    paramBundle = i;
    int i1 = 2131364648;
    Integer localInteger2 = Integer.valueOf(i1);
    paramBundle.put(localInteger2, "profileTag");
    paramBundle = getArguments();
    boolean bool1 = true;
    boolean bool2 = false;
    Object localObject3 = null;
    Object localObject4;
    if (paramBundle != null)
    {
      localObject4 = "conversionFromBusiness";
      bool3 = paramBundle.getBoolean((String)localObject4, false);
      if (bool3) {
        bool2 = true;
      }
    }
    G = bool2;
    paramBundle = i.entrySet().iterator();
    for (;;)
    {
      bool2 = paramBundle.hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (Map.Entry)paramBundle.next();
      localObject4 = a;
      Object localObject5 = ((Map.Entry)localObject3).getKey();
      localObject3 = Settings.b((String)((Map.Entry)localObject3).getValue());
      ((TreeMap)localObject4).put(localObject5, localObject3);
    }
    boolean bool3 = G;
    if (bool3)
    {
      paramBundle = a;
      localObject1 = Integer.valueOf(j);
      localObject3 = "";
      paramBundle.put(localObject1, localObject3);
      paramBundle = a;
      localObject1 = Integer.valueOf(k);
      localObject2 = "";
      paramBundle.put(localObject1, localObject2);
      paramBundle = a;
      localObject1 = Integer.valueOf(i1);
      paramBundle.remove(localObject1);
      p = bool1;
    }
    paramBundle = h.c(getContext());
    l = paramBundle;
    paramBundle = h();
    m = paramBundle;
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenuInflater.inflate(2131623951, paramMenu);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    setHasOptionsMenu(true);
    paramBundle = aw.b(getActivity());
    int j = 2131230873;
    paramBundle = paramBundle.a(j, j);
    b = paramBundle;
    return paramLayoutInflater.inflate(2131559185, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    com.truecaller.androidactors.a locala = J;
    if (locala != null)
    {
      locala.a();
      locala = null;
      J = null;
    }
    super.onDestroyView();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    boolean bool1 = super.onOptionsItemSelected(paramMenuItem);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    int k = paramMenuItem.getItemId();
    int j = 2131361930;
    if (k == j)
    {
      n();
      return bool2;
    }
    return false;
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    l locall = O;
    paramArrayOfString = new String[] { "android.permission.GET_ACCOUNTS" };
    paramInt = locall.a(paramArrayOfString);
    if (paramInt != 0)
    {
      paramInt = 4;
      boolean bool = true;
      a(paramInt, bool);
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    F = false;
  }
  
  public final void onStop()
  {
    super.onStop();
    AlertDialog localAlertDialog = S;
    if (localAlertDialog != null)
    {
      localAlertDialog.dismiss();
      localAlertDialog = null;
      S = null;
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int j = 2131364644;
    paramBundle = (TabLayout)paramView.findViewById(j);
    int k = 2131362929;
    ViewPager localViewPager = (ViewPager)paramView.findViewById(k);
    Object localObject1 = (TextView)paramView.findViewById(2131364001);
    C = ((TextView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131364000);
    D = ((TextView)localObject1);
    localObject1 = (TextView)paramView.findViewById(2131363999);
    E = ((TextView)localObject1);
    int i1 = 2131362927;
    localObject1 = (ImageView)paramView.findViewById(i1);
    paramView.findViewById(2131363991).setOnClickListener(this);
    paramView = getActivity().getLayoutInflater();
    Object localObject2 = (ViewGroup)getView();
    paramView = paramView.inflate(2131559187, (ViewGroup)localObject2, false);
    q = paramView;
    paramView = (Button)q.findViewById(2131363027);
    s = paramView;
    paramView = (Button)q.findViewById(2131363198);
    r = paramView;
    paramView = (CallerButtonBase)q.findViewById(2131363930);
    t = paramView;
    paramView = (CallerButtonBase)q.findViewById(2131361990);
    u = paramView;
    paramView = (CallerButtonBase)q.findViewById(2131362941);
    v = paramView;
    paramView = (CallerButtonBase)q.findViewById(2131365532);
    w = paramView;
    paramView = (CallerButtonBase)q.findViewById(2131364648);
    x = paramView;
    paramView = x;
    int i2 = 0;
    localObject2 = null;
    paramView.setDetailsText(null);
    paramView = q;
    int i3 = 2131362272;
    paramView = paramView.findViewById(i3);
    y = paramView;
    r.setOnClickListener(this);
    s.setOnClickListener(this);
    t.setOnClickListener(this);
    u.setOnClickListener(this);
    v.setOnClickListener(this);
    w.setOnClickListener(this);
    x.setOnClickListener(this);
    y.setOnClickListener(this);
    paramView = l;
    if (paramView != null)
    {
      paramView = c;
    }
    else
    {
      bool2 = false;
      paramView = null;
    }
    CallerButtonBase localCallerButtonBase = t;
    paramView = ah.a(c.a(Q), paramView);
    localCallerButtonBase.setHeadingText(paramView);
    paramView = t;
    i3 = 1;
    a(paramView, i3);
    paramView = q.findViewById(2131362928);
    com.truecaller.common.h.ac localac = N;
    boolean bool3 = localac.a();
    int i5 = 8;
    if (bool3)
    {
      bool3 = false;
      localac = null;
    }
    else
    {
      i4 = 8;
    }
    paramView.setVisibility(i4);
    paramView = R.z();
    boolean bool2 = paramView.a();
    if (bool2)
    {
      bool2 = G;
      if (!bool2) {
        i5 = 0;
      }
    }
    y.setVisibility(i5);
    paramView = getActivity().getLayoutInflater();
    int i4 = 2131559186;
    paramView = paramView.inflate(i4, null);
    z = paramView;
    paramView = (CallerButtonBase)z.findViewById(2131362119);
    A = paramView;
    paramView = z;
    i2 = 2131363167;
    paramView = (NewComboBase)paramView.findViewById(i2);
    B = paramView;
    A.setOnClickListener(this);
    paramView = Q;
    localObject2 = "profileGender";
    paramView = paramView.a((String)localObject2);
    b(paramView);
    paramView = new com/truecaller/profile/a$a;
    paramView.<init>(this, (byte)0);
    localViewPager.setAdapter(paramView);
    paramBundle.setupWithViewPager(localViewPager);
    localViewPager.setOffscreenPageLimit(i3);
    localViewPager.setCurrentItem(0);
    f.setVisibility(0);
    f.setClickable(i3);
    ((ImageView)localObject1).setVisibility(0);
    g();
    paramView = getActivity().getIntent();
    paramBundle = "ARG_SHOW_PHOTO_SELECTOR";
    bool2 = paramView.getBooleanExtra(paramBundle, false);
    if (bool2)
    {
      paramView = getActivity();
      paramBundle = a;
      boolean bool1 = o;
      localObject1 = n;
      paramView = EditMeFormFragment.a(paramView, paramBundle, bool1, (Bitmap)localObject1);
      paramBundle = "ARG_SHOW_PHOTO_SELECTOR";
      paramView = paramView.putExtra(paramBundle, i3);
      j = 30;
      startActivityForResult(paramView, j);
      F = i3;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */