package com.truecaller.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;
import com.truecaller.R.id;
import java.util.HashMap;

public final class b
  extends Fragment
{
  private HashMap a;
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558760, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = getActivity();
    boolean bool = paramView instanceof AppCompatActivity;
    if (!bool) {
      paramView = null;
    }
    paramView = (AppCompatActivity)paramView;
    if (paramView != null)
    {
      paramBundle = paramView;
      paramBundle = (f)paramView;
      int i = R.id.toolbar;
      paramBundle = (Toolbar)paramBundle.findViewById(i);
      paramView.setSupportActionBar(paramBundle);
      paramBundle = paramView.getSupportActionBar();
      i = 1;
      if (paramBundle != null) {
        paramBundle.setDisplayHomeAsUpEnabled(i);
      }
      paramBundle = paramView.getSupportActionBar();
      if (paramBundle != null) {
        paramBundle.setHomeButtonEnabled(i);
      }
      paramView = paramView.getSupportActionBar();
      if (paramView != null)
      {
        paramBundle = (CharSequence)"John Doe";
        paramView.setTitle(paramBundle);
        return;
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */