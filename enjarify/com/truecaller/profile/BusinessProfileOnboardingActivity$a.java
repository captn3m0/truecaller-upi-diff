package com.truecaller.profile;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class BusinessProfileOnboardingActivity$a
{
  public static Intent a(Context paramContext, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, BusinessProfileOnboardingActivity.class);
    localIntent.putExtra("arg_from_wizard", paramBoolean1);
    localIntent.putExtra("arg_migrating", paramBoolean2);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.BusinessProfileOnboardingActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */