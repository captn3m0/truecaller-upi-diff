package com.truecaller.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public final class ProfileActivity
  extends AppCompatActivity
{
  public final void onCreate(Bundle paramBundle)
  {
    ThemeManager.Theme localTheme = ThemeManager.a();
    int i = resId;
    setTheme(i);
    super.onCreate(paramBundle);
    if (paramBundle == null)
    {
      paramBundle = getSupportFragmentManager().a();
      i = 16908290;
      Object localObject = new com/truecaller/profile/b;
      ((b)localObject).<init>();
      localObject = (Fragment)localObject;
      paramBundle = paramBundle.b(i, (Fragment)localObject);
      paramBundle.c();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.ProfileActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */