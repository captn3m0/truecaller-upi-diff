package com.truecaller.profile;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

final class EditMeFormFragment$1
  implements TextWatcher
{
  EditMeFormFragment$1(EditMeFormFragment paramEditMeFormFragment, TextView paramTextView) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    int i = paramEditable.length();
    int j = 160;
    if (i > j)
    {
      paramEditable.delete(j, i);
      i = paramEditable.length();
    }
    paramEditable = a;
    EditMeFormFragment localEditMeFormFragment = b;
    Object[] arrayOfObject = new Object[1];
    Object localObject = Integer.valueOf(j - i);
    arrayOfObject[0] = localObject;
    localObject = localEditMeFormFragment.getString(2131886934, arrayOfObject);
    paramEditable.setText((CharSequence)localObject);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.profile.EditMeFormFragment.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */