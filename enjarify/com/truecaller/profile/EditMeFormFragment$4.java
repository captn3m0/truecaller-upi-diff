package com.truecaller.profile;

import android.content.Context;
import android.net.Uri;
import com.truecaller.util.aw.d;

final class EditMeFormFragment$4
  extends aw.d
{
  EditMeFormFragment$4(EditMeFormFragment paramEditMeFormFragment, Context paramContext, Uri paramUri1, Uri paramUri2, Uri paramUri3)
  {
    super(paramContext, paramUri1, paramUri2);
  }
  
  /* Error */
  public final Object doInBackground(Object... paramVarArgs)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 10	com/truecaller/profile/EditMeFormFragment$4:c	Lcom/truecaller/profile/EditMeFormFragment;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 22	com/truecaller/profile/EditMeFormFragment:aa_	()V
    //   9: aload_0
    //   10: aload_1
    //   11: invokespecial 26	com/truecaller/util/aw$d:doInBackground	([Ljava/lang/Object;)Ljava/lang/Object;
    //   14: pop
    //   15: aload_0
    //   16: getfield 10	com/truecaller/profile/EditMeFormFragment$4:c	Lcom/truecaller/profile/EditMeFormFragment;
    //   19: astore_1
    //   20: aload_0
    //   21: getfield 12	com/truecaller/profile/EditMeFormFragment$4:a	Landroid/net/Uri;
    //   24: astore_2
    //   25: aload_1
    //   26: aload_2
    //   27: invokevirtual 29	com/truecaller/profile/EditMeFormFragment:a	(Landroid/net/Uri;)V
    //   30: goto +12 -> 42
    //   33: astore_1
    //   34: goto +17 -> 51
    //   37: astore_1
    //   38: aload_1
    //   39: invokevirtual 34	java/lang/SecurityException:printStackTrace	()V
    //   42: aload_0
    //   43: getfield 10	com/truecaller/profile/EditMeFormFragment$4:c	Lcom/truecaller/profile/EditMeFormFragment;
    //   46: invokevirtual 37	com/truecaller/profile/EditMeFormFragment:g	()V
    //   49: aconst_null
    //   50: areturn
    //   51: aload_0
    //   52: getfield 10	com/truecaller/profile/EditMeFormFragment$4:c	Lcom/truecaller/profile/EditMeFormFragment;
    //   55: invokevirtual 37	com/truecaller/profile/EditMeFormFragment:g	()V
    //   58: aload_1
    //   59: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	60	0	this	4
    //   0	60	1	paramVarArgs	Object[]
    //   4	23	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   10	15	33	finally
    //   15	19	33	finally
    //   20	24	33	finally
    //   26	30	33	finally
    //   38	42	33	finally
    //   10	15	37	java/lang/SecurityException
    //   15	19	37	java/lang/SecurityException
    //   20	24	37	java/lang/SecurityException
    //   26	30	37	java/lang/SecurityException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.EditMeFormFragment.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */