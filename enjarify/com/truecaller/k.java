package com.truecaller;

import android.content.Context;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class k
  implements d
{
  private final c a;
  private final Provider b;
  
  private k(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static com.truecaller.data.access.c a(Context paramContext)
  {
    return (com.truecaller.data.access.c)g.a(c.d(paramContext), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static k a(c paramc, Provider paramProvider)
  {
    k localk = new com/truecaller/k;
    localk.<init>(paramc, paramProvider);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */