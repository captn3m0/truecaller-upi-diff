package com.truecaller;

import android.content.Context;
import com.truecaller.util.b.j;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class ag
  implements d
{
  private final c a;
  private final Provider b;
  
  private ag(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static ag a(c paramc, Provider paramProvider)
  {
    ag localag = new com/truecaller/ag;
    localag.<init>(paramc, paramProvider);
    return localag;
  }
  
  public static j a(Context paramContext)
  {
    return (j)g.a(c.h(paramContext), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */