package com.truecaller;

import android.content.Context;
import c.g.b.k;
import com.bumptech.glide.c.a;
import com.bumptech.glide.load.b.b.a.a;

public final class TruecallerGlideModule
  extends a
{
  public final void a(Context paramContext, com.bumptech.glide.f paramf)
  {
    k.b(paramContext, "context");
    k.b(paramf, "builder");
    Object localObject = new com/bumptech/glide/load/b/b/f;
    ((com.bumptech.glide.load.b.b.f)localObject).<init>(paramContext, (byte)0);
    localObject = (a.a)localObject;
    paramf.a((a.a)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.TruecallerGlideModule
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */