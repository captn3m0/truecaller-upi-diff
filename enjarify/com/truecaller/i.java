package com.truecaller;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private i(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static i a(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    i locali = new com/truecaller/i;
    locali.<init>(paramc, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */