package com.truecaller.a;

import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.common.f.c;

public final class g
  implements f
{
  final c a;
  private final com.truecaller.androidactors.f b;
  private final com.truecaller.androidactors.k c;
  
  public g(com.truecaller.androidactors.f paramf, com.truecaller.androidactors.k paramk, c paramc)
  {
    b = paramf;
    c = paramk;
    a = paramc;
  }
  
  public final void a(String paramString, f.a parama)
  {
    c.g.b.k.b(paramString, "webId");
    c.g.b.k.b(parama, "callback");
    paramString = ((b)b.a()).a(paramString);
    i locali = c.a();
    Object localObject = new com/truecaller/a/g$a;
    ((g.a)localObject).<init>(parama);
    localObject = (ac)localObject;
    paramString.a(locali, (ac)localObject);
  }
  
  public final void a(String paramString1, String paramString2, f.b paramb)
  {
    c.g.b.k.b(paramString1, "receiver");
    c.g.b.k.b(paramString2, "name");
    c.g.b.k.b(paramb, "callback");
    paramString1 = ((b)b.a()).a(paramString1, paramString2);
    i locali = c.a();
    Object localObject = new com/truecaller/a/g$b;
    ((g.b)localObject).<init>(this, paramb, paramString2);
    localObject = (ac)localObject;
    paramString1.a(locali, (ac)localObject);
  }
  
  public final void b(String paramString, f.a parama)
  {
    c.g.b.k.b(paramString, "webId");
    c.g.b.k.b(parama, "callback");
    paramString = ((b)b.a()).b(paramString);
    i locali = c.a();
    Object localObject = new com/truecaller/a/g$c;
    ((g.c)localObject).<init>(parama);
    localObject = (ac)localObject;
    paramString.a(locali, (ac)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */