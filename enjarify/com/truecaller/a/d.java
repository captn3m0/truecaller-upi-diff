package com.truecaller.a;

import c.g.b.k;
import com.google.gson.o;
import com.truecaller.androidactors.w;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import e.r;
import java.io.IOException;

public final class d
  implements b
{
  public final w a(String paramString)
  {
    k.b(paramString, "webId");
    Object localObject = KnownEndpoints.CONTACTREQUEST;
    Class localClass = i.class;
    localObject = (i)h.a((KnownEndpoints)localObject, localClass);
    paramString = ((i)localObject).a(paramString);
    try
    {
      paramString = paramString.c();
      i = paramString.b();
      paramString = Integer.valueOf(i);
      paramString = w.b(paramString);
      localObject = "Promise.wrap<Int>(response.code())";
      k.a(paramString, (String)localObject);
    }
    catch (IOException localIOException)
    {
      int i = -1;
      paramString = w.b(Integer.valueOf(i));
      localObject = "Promise.wrap<Int>(UNKNOWN_ERROR)";
      k.a(paramString, (String)localObject);
    }
    return paramString;
  }
  
  public final w a(String paramString1, String paramString2)
  {
    k.b(paramString1, "receiver");
    k.b(paramString2, "name");
    o localo = new com/google/gson/o;
    localo.<init>();
    localo.a("receiverName", paramString2);
    paramString2 = KnownEndpoints.CONTACTREQUEST;
    Class localClass = i.class;
    paramString2 = (i)h.a(paramString2, localClass);
    paramString1 = paramString2.a(paramString1, localo);
    try
    {
      paramString1 = paramString1.c();
      i = paramString1.b();
      paramString1 = Integer.valueOf(i);
      paramString1 = w.b(paramString1);
      paramString2 = "Promise.wrap<Int>(response.code())";
      k.a(paramString1, paramString2);
    }
    catch (IOException localIOException)
    {
      int i = -1;
      paramString1 = w.b(Integer.valueOf(i));
      paramString2 = "Promise.wrap<Int>(UNKNOWN_ERROR)";
      k.a(paramString1, paramString2);
    }
    return paramString1;
  }
  
  public final w b(String paramString)
  {
    k.b(paramString, "webId");
    Object localObject = KnownEndpoints.CONTACTREQUEST;
    Class localClass = i.class;
    localObject = (i)h.a((KnownEndpoints)localObject, localClass);
    paramString = ((i)localObject).b(paramString);
    try
    {
      paramString = paramString.c();
      i = paramString.b();
      paramString = Integer.valueOf(i);
      paramString = w.b(paramString);
      localObject = "Promise.wrap<Int>(response.code())";
      k.a(paramString, (String)localObject);
    }
    catch (IOException localIOException)
    {
      int i = -1;
      paramString = w.b(Integer.valueOf(i));
      localObject = "Promise.wrap<Int>(UNKNOWN_ERROR)";
      k.a(paramString, (String)localObject);
    }
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */