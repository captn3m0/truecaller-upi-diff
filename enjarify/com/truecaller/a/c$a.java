package com.truecaller.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class c$a
  extends u
{
  private final String b;
  
  private c$a(e parame, String paramString)
  {
    super(parame);
    b = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".acceptContactRequest(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.a.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */