package com.truecaller.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;

public final class c
  implements b
{
  private final v a;
  
  public c(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return b.class.equals(paramClass);
  }
  
  public final w a(String paramString)
  {
    v localv = a;
    c.a locala = new com/truecaller/a/c$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramString, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w a(String paramString1, String paramString2)
  {
    v localv = a;
    c.b localb = new com/truecaller/a/c$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramString1, paramString2, (byte)0);
    return w.a(localv, localb);
  }
  
  public final w b(String paramString)
  {
    v localv = a;
    c.c localc = new com/truecaller/a/c$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramString, (byte)0);
    return w.a(localv, localc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */