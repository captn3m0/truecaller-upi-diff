package com.truecaller;

import com.truecaller.clevertap.j;
import com.truecaller.fcm.FcmMessageListenerService;
import com.truecaller.fcm.d;
import com.truecaller.fcm.f;
import com.truecaller.fcm.g;
import com.truecaller.messaging.transport.im.ap;
import javax.inject.Provider;

final class be$j
  implements com.truecaller.fcm.a
{
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  
  private be$j(be parambe, com.truecaller.fcm.b paramb)
  {
    parambe = dagger.a.c.a(d.a(paramb));
    b = parambe;
    parambe = b;
    parambe = dagger.a.c.a(g.a(paramb, parambe));
    c = parambe;
    parambe = b;
    parambe = dagger.a.c.a(f.a(paramb, parambe));
    d = parambe;
    parambe = be.o(a);
    Provider localProvider1 = c;
    Provider localProvider2 = d;
    parambe = dagger.a.c.a(com.truecaller.fcm.e.a(paramb, parambe, localProvider1, localProvider2));
    e = parambe;
    parambe = be.o(a);
    parambe = dagger.a.c.a(com.truecaller.fcm.c.a(paramb, parambe));
    f = parambe;
  }
  
  public final void a(FcmMessageListenerService paramFcmMessageListenerService)
  {
    Object localObject = (com.truecaller.flashsdk.b.a)e.get();
    b = ((com.truecaller.flashsdk.b.a)localObject);
    localObject = (j)f.get();
    c = ((j)localObject);
    localObject = (ap)be.N(a).get();
    d = ((ap)localObject);
    localObject = (com.truecaller.b.c)be.O(a).get();
    e = ((com.truecaller.b.c)localObject);
    localObject = (com.truecaller.featuretoggles.e)be.a(a).get();
    f = ((com.truecaller.featuretoggles.e)localObject);
    localObject = (com.truecaller.push.e)be.P(a).get();
    g = ((com.truecaller.push.e)localObject);
    localObject = (com.truecaller.push.b)be.Q(a).get();
    h = ((com.truecaller.push.b)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */