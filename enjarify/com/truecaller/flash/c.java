package com.truecaller.flash;

import c.g.b.k;
import com.truecaller.common.h.aa;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.flashsdk.core.i;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public final class c
  implements b
{
  private final String a;
  private final i b;
  private final com.truecaller.callhistory.a c;
  private final com.truecaller.common.g.a d;
  
  public c(String paramString, i parami, com.truecaller.callhistory.a parama, com.truecaller.common.g.a parama1)
  {
    a = paramString;
    b = parami;
    c = parama;
    d = parama1;
  }
  
  private final List a(Collection paramCollection)
  {
    paramCollection = (Iterable)c.a.m.e((Iterable)paramCollection);
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramCollection = paramCollection.iterator();
    boolean bool1;
    boolean bool3;
    boolean bool4;
    Object localObject2;
    Object localObject4;
    int j;
    Object localObject5;
    for (;;)
    {
      bool1 = paramCollection.hasNext();
      bool3 = true;
      bool4 = false;
      localObject2 = null;
      if (!bool1) {
        break;
      }
      localObject3 = paramCollection.next();
      localObject4 = localObject3;
      localObject4 = ((Contact)localObject3).A();
      j = ((List)localObject4).size();
      if (j <= 0)
      {
        bool3 = false;
        localObject5 = null;
      }
      if (bool3) {
        ((Collection)localObject1).add(localObject3);
      }
    }
    localObject1 = (Iterable)localObject1;
    paramCollection = new java/util/ArrayList;
    paramCollection.<init>();
    paramCollection = (Collection)paramCollection;
    localObject1 = ((Iterable)localObject1).iterator();
    Object localObject6;
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = ((Iterator)localObject1).next();
      localObject4 = localObject3;
      localObject4 = (Contact)localObject3;
      localObject6 = ((Contact)localObject4).A();
      Object localObject7 = "contact.numbers";
      k.a(localObject6, (String)localObject7);
      localObject6 = (Number)c.a.m.e((List)localObject6);
      if (localObject6 != null)
      {
        localObject7 = b;
        localObject6 = ((Number)localObject6).a();
        boolean bool5 = ((i)localObject7).b((String)localObject6);
        if (bool5)
        {
          localObject4 = ((Contact)localObject4).A();
          k.a(localObject4, "contact.numbers");
          localObject4 = c.a.m.d((List)localObject4);
          k.a(localObject4, "contact.numbers.first()");
          localObject4 = ((Number)localObject4).a();
          localObject6 = a;
          localObject4 = aa.c((String)localObject4, (String)localObject6);
          localObject6 = "PhoneNumberNormalizer.no…alizedNumber, countryIso)";
          k.a(localObject4, (String)localObject6);
          localObject4 = c.n.m.d((String)localObject4);
          if (localObject4 != null)
          {
            j = 1;
            break label321;
          }
        }
      }
      j = 0;
      localObject4 = null;
      label321:
      if (j != 0) {
        paramCollection.add(localObject3);
      }
    }
    paramCollection = (Iterable)paramCollection;
    localObject1 = new java/util/ArrayList;
    int i = c.a.m.a(paramCollection, 10);
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool2 = paramCollection.hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (Contact)paramCollection.next();
      localObject5 = new com/truecaller/flashsdk/models/a;
      localObject2 = ((Contact)localObject3).s();
      localObject4 = ((Contact)localObject3).A();
      k.a(localObject4, "it.numbers");
      localObject4 = c.a.m.d((List)localObject4);
      k.a(localObject4, "it.numbers.first()");
      localObject4 = ((Number)localObject4).a();
      localObject6 = a;
      localObject4 = aa.c((String)localObject4, (String)localObject6);
      localObject3 = ((Contact)localObject3).v();
      ((com.truecaller.flashsdk.models.a)localObject5).<init>((String)localObject2, (String)localObject4, (String)localObject3);
      ((Collection)localObject1).add(localObject5);
    }
    localObject1 = (Iterable)localObject1;
    paramCollection = new java/util/HashSet;
    paramCollection.<init>();
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject5 = ((Iterator)localObject1).next();
      localObject2 = localObject5;
      localObject2 = ((com.truecaller.flashsdk.models.a)localObject5).b();
      bool4 = paramCollection.add(localObject2);
      if (bool4) {
        ((ArrayList)localObject3).add(localObject5);
      }
    }
    return (List)localObject3;
  }
  
  /* Error */
  private List b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 35	com/truecaller/flash/c:c	Lcom/truecaller/callhistory/a;
    //   4: astore_1
    //   5: getstatic 149	com/truecaller/callhistory/FilterType:NONE	Lcom/truecaller/callhistory/FilterType;
    //   8: astore_2
    //   9: bipush 20
    //   11: istore_3
    //   12: iload_3
    //   13: invokestatic 156	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   16: astore 4
    //   18: aload_1
    //   19: aload_2
    //   20: aload 4
    //   22: invokeinterface 161 3 0
    //   27: invokevirtual 165	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   30: checkcast 73	java/util/List
    //   33: astore_1
    //   34: aload_1
    //   35: ifnonnull +10 -> 45
    //   38: getstatic 170	c/a/y:a	Lc/a/y;
    //   41: checkcast 73	java/util/List
    //   44: astore_1
    //   45: aload_1
    //   46: checkcast 39	java/lang/Iterable
    //   49: astore_1
    //   50: new 47	java/util/ArrayList
    //   53: astore_2
    //   54: bipush 10
    //   56: istore_3
    //   57: aload_1
    //   58: iload_3
    //   59: invokestatic 118	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   62: istore 5
    //   64: aload_2
    //   65: iload 5
    //   67: invokespecial 121	java/util/ArrayList:<init>	(I)V
    //   70: aload_2
    //   71: checkcast 50	java/util/Collection
    //   74: astore_2
    //   75: aload_1
    //   76: invokeinterface 54 1 0
    //   81: astore_1
    //   82: aload_1
    //   83: invokeinterface 60 1 0
    //   88: istore 5
    //   90: iload 5
    //   92: ifeq +44 -> 136
    //   95: aload_1
    //   96: invokeinterface 65 1 0
    //   101: checkcast 172	com/truecaller/data/entity/HistoryEvent
    //   104: astore 6
    //   106: ldc -82
    //   108: astore 7
    //   110: aload 6
    //   112: aload 7
    //   114: invokestatic 85	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   117: aload 6
    //   119: invokevirtual 177	com/truecaller/data/entity/HistoryEvent:s	()Lcom/truecaller/data/entity/Contact;
    //   122: astore 6
    //   124: aload_2
    //   125: aload 6
    //   127: invokeinterface 81 2 0
    //   132: pop
    //   133: goto -51 -> 82
    //   136: aload_2
    //   137: checkcast 73	java/util/List
    //   140: checkcast 50	java/util/Collection
    //   143: astore_2
    //   144: aload_0
    //   145: aload_2
    //   146: invokespecial 180	com/truecaller/flash/c:a	(Ljava/util/Collection;)Ljava/util/List;
    //   149: astore_1
    //   150: aload_0
    //   151: getfield 35	com/truecaller/flash/c:c	Lcom/truecaller/callhistory/a;
    //   154: bipush 100
    //   156: invokeinterface 184 2 0
    //   161: invokevirtual 165	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   164: checkcast 186	com/truecaller/callhistory/z
    //   167: astore_2
    //   168: iconst_0
    //   169: istore 5
    //   171: aconst_null
    //   172: astore 6
    //   174: aload_2
    //   175: ifnull +136 -> 311
    //   178: aload_2
    //   179: checkcast 188	android/database/Cursor
    //   182: astore_2
    //   183: aload_2
    //   184: astore 7
    //   186: aload_2
    //   187: checkcast 190	java/io/Closeable
    //   190: astore 7
    //   192: new 47	java/util/ArrayList
    //   195: astore 8
    //   197: aload 8
    //   199: invokespecial 48	java/util/ArrayList:<init>	()V
    //   202: aload 8
    //   204: checkcast 50	java/util/Collection
    //   207: astore 8
    //   209: aload_2
    //   210: invokeinterface 193 1 0
    //   215: istore 9
    //   217: iload 9
    //   219: ifeq +55 -> 274
    //   222: aload_2
    //   223: astore 10
    //   225: aload_2
    //   226: checkcast 186	com/truecaller/callhistory/z
    //   229: astore 10
    //   231: aload 10
    //   233: invokeinterface 196 1 0
    //   238: astore 10
    //   240: aload 10
    //   242: ifnull +13 -> 255
    //   245: aload 10
    //   247: invokevirtual 177	com/truecaller/data/entity/HistoryEvent:s	()Lcom/truecaller/data/entity/Contact;
    //   250: astore 10
    //   252: goto +9 -> 261
    //   255: iconst_0
    //   256: istore 9
    //   258: aconst_null
    //   259: astore 10
    //   261: aload 8
    //   263: aload 10
    //   265: invokeinterface 81 2 0
    //   270: pop
    //   271: goto -62 -> 209
    //   274: aload 8
    //   276: astore_2
    //   277: aload 8
    //   279: checkcast 73	java/util/List
    //   282: astore_2
    //   283: aload 7
    //   285: aconst_null
    //   286: invokestatic 201	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   289: goto +27 -> 316
    //   292: astore_1
    //   293: goto +9 -> 302
    //   296: astore_1
    //   297: aload_1
    //   298: astore 6
    //   300: aload_1
    //   301: athrow
    //   302: aload 7
    //   304: aload 6
    //   306: invokestatic 201	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   309: aload_1
    //   310: athrow
    //   311: iconst_0
    //   312: istore 11
    //   314: aconst_null
    //   315: astore_2
    //   316: aload_2
    //   317: ifnonnull +10 -> 327
    //   320: getstatic 170	c/a/y:a	Lc/a/y;
    //   323: checkcast 73	java/util/List
    //   326: astore_2
    //   327: aload_2
    //   328: checkcast 50	java/util/Collection
    //   331: astore_2
    //   332: aload_0
    //   333: aload_2
    //   334: invokespecial 180	com/truecaller/flash/c:a	(Ljava/util/Collection;)Ljava/util/List;
    //   337: astore_2
    //   338: aload_1
    //   339: checkcast 39	java/lang/Iterable
    //   342: astore_1
    //   343: iconst_2
    //   344: istore 5
    //   346: aload_1
    //   347: iload 5
    //   349: invokestatic 205	c/a/m:d	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   352: checkcast 50	java/util/Collection
    //   355: astore_1
    //   356: aload_2
    //   357: checkcast 39	java/lang/Iterable
    //   360: iload_3
    //   361: invokestatic 205	c/a/m:d	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   364: checkcast 39	java/lang/Iterable
    //   367: astore_2
    //   368: aload_1
    //   369: aload_2
    //   370: invokestatic 208	c/a/m:c	(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;
    //   373: checkcast 39	java/lang/Iterable
    //   376: astore_1
    //   377: new 138	java/util/HashSet
    //   380: astore_2
    //   381: aload_2
    //   382: invokespecial 139	java/util/HashSet:<init>	()V
    //   385: new 47	java/util/ArrayList
    //   388: astore 6
    //   390: aload 6
    //   392: invokespecial 48	java/util/ArrayList:<init>	()V
    //   395: aload_1
    //   396: invokeinterface 54 1 0
    //   401: astore_1
    //   402: aload_1
    //   403: invokeinterface 60 1 0
    //   408: istore 12
    //   410: iload 12
    //   412: ifeq +49 -> 461
    //   415: aload_1
    //   416: invokeinterface 65 1 0
    //   421: astore 7
    //   423: aload 7
    //   425: astore 8
    //   427: aload 7
    //   429: checkcast 123	com/truecaller/flashsdk/models/a
    //   432: invokevirtual 141	com/truecaller/flashsdk/models/a:b	()Ljava/lang/String;
    //   435: astore 8
    //   437: aload_2
    //   438: aload 8
    //   440: invokevirtual 142	java/util/HashSet:add	(Ljava/lang/Object;)Z
    //   443: istore 13
    //   445: iload 13
    //   447: ifeq -45 -> 402
    //   450: aload 6
    //   452: aload 7
    //   454: invokevirtual 143	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   457: pop
    //   458: goto -56 -> 402
    //   461: aload 6
    //   463: checkcast 73	java/util/List
    //   466: checkcast 39	java/lang/Iterable
    //   469: astore 6
    //   471: aload 6
    //   473: iload_3
    //   474: invokestatic 205	c/a/m:d	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   477: astore_1
    //   478: aload_1
    //   479: invokeinterface 211 1 0
    //   484: istore 11
    //   486: iload 11
    //   488: ifeq +132 -> 620
    //   491: aload_0
    //   492: getfield 37	com/truecaller/flash/c:d	Lcom/truecaller/common/g/a;
    //   495: astore_2
    //   496: ldc -43
    //   498: astore 4
    //   500: aload_2
    //   501: aload 4
    //   503: invokeinterface 218 2 0
    //   508: astore_2
    //   509: aload_2
    //   510: ifnonnull +10 -> 520
    //   513: getstatic 170	c/a/y:a	Lc/a/y;
    //   516: checkcast 73	java/util/List
    //   519: areturn
    //   520: aload_2
    //   521: ldc -36
    //   523: invokestatic 85	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   526: aload_0
    //   527: getfield 31	com/truecaller/flash/c:a	Ljava/lang/String;
    //   530: astore 4
    //   532: aload_2
    //   533: aload 4
    //   535: invokestatic 107	com/truecaller/common/h/aa:c	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   538: astore_2
    //   539: aload_2
    //   540: ldc 109
    //   542: invokestatic 85	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   545: aload_2
    //   546: invokestatic 114	c/n/m:d	(Ljava/lang/String;)Ljava/lang/Long;
    //   549: astore 4
    //   551: aload 4
    //   553: ifnull +67 -> 620
    //   556: new 123	com/truecaller/flashsdk/models/a
    //   559: astore_1
    //   560: iconst_1
    //   561: anewarray 224	java/lang/CharSequence
    //   564: astore 6
    //   566: aload_0
    //   567: getfield 37	com/truecaller/flash/c:d	Lcom/truecaller/common/g/a;
    //   570: invokestatic 229	com/truecaller/profile/c:b	(Lcom/truecaller/common/g/a;)Ljava/lang/String;
    //   573: checkcast 224	java/lang/CharSequence
    //   576: astore 8
    //   578: aload 6
    //   580: iconst_0
    //   581: aload 8
    //   583: aastore
    //   584: ldc -34
    //   586: aload 6
    //   588: invokestatic 234	com/truecaller/common/h/am:a	(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/String;
    //   591: astore 4
    //   593: aload_0
    //   594: getfield 37	com/truecaller/flash/c:d	Lcom/truecaller/common/g/a;
    //   597: ldc -20
    //   599: invokeinterface 218 2 0
    //   604: astore 6
    //   606: aload_1
    //   607: aload 4
    //   609: aload_2
    //   610: aload 6
    //   612: invokespecial 136	com/truecaller/flashsdk/models/a:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   615: aload_1
    //   616: invokestatic 239	c/a/m:a	(Ljava/lang/Object;)Ljava/util/List;
    //   619: areturn
    //   620: aload_1
    //   621: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	622	0	this	c
    //   4	146	1	localObject1	Object
    //   292	1	1	localObject2	Object
    //   296	43	1	localObject3	Object
    //   342	279	1	localObject4	Object
    //   8	602	2	localObject5	Object
    //   11	463	3	i	int
    //   16	592	4	localObject6	Object
    //   62	4	5	j	int
    //   88	82	5	bool1	boolean
    //   344	4	5	k	int
    //   104	507	6	localObject7	Object
    //   108	345	7	localObject8	Object
    //   195	387	8	localObject9	Object
    //   215	42	9	bool2	boolean
    //   223	41	10	localObject10	Object
    //   312	175	11	bool3	boolean
    //   408	3	12	bool4	boolean
    //   443	3	13	bool5	boolean
    // Exception table:
    //   from	to	target	type
    //   300	302	292	finally
    //   192	195	296	finally
    //   197	202	296	finally
    //   202	207	296	finally
    //   209	215	296	finally
    //   225	229	296	finally
    //   231	238	296	finally
    //   245	250	296	finally
    //   263	271	296	finally
    //   277	282	296	finally
  }
  
  public final List a()
  {
    AssertionUtil.notOnMainThread(new String[] { "Please run off the main thread!" });
    return b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */