package com.truecaller.flash;

import c.d.c;
import c.o.b;
import c.x;
import com.google.f.r;
import com.truecaller.api.services.presence.v1.models.b;
import com.truecaller.flashsdk.models.e;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class n$a$1
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  n$a$1(n.a parama, ArrayList paramArrayList, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/flash/n$a$1;
    n.a locala = b;
    ArrayList localArrayList = c;
    local1.<init>(locala, localArrayList, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = (Iterable)c;
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        localObject1 = (Collection)localObject1;
        paramObject = ((Iterable)paramObject).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)paramObject).hasNext();
          if (!bool2) {
            break;
          }
          Object localObject2 = (com.truecaller.presence.a)((Iterator)paramObject).next();
          b localb = c;
          if (localb == null)
          {
            bool2 = false;
            localObject2 = null;
          }
          else
          {
            e locale = new com/truecaller/flashsdk/models/e;
            localObject2 = c.n.m.a(a, "+", "");
            r localr = localb.b();
            String str = "flash.version";
            c.g.b.k.a(localr, str);
            int j = localr.getValue();
            boolean bool3 = localb.a();
            locale.<init>((String)localObject2, j, bool3);
            localObject2 = locale;
          }
          if (localObject2 != null) {
            ((Collection)localObject1).add(localObject2);
          }
        }
        return (List)localObject1;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.n.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */