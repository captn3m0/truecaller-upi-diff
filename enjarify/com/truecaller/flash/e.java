package com.truecaller.flash;

import c.g.b.k;
import c.n.m;
import com.truecaller.callhistory.a;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.Sender;

public final class e
  implements d
{
  private final a a;
  
  public e(a parama)
  {
    a = parama;
  }
  
  public final void a(Flash paramFlash)
  {
    Object localObject1 = "flash";
    k.b(paramFlash, (String)localObject1);
    boolean bool1 = paramFlash.k();
    int j = 1;
    int k;
    if (bool1)
    {
      k = 1;
    }
    else
    {
      int i = 2;
      k = 2;
    }
    localObject1 = paramFlash.e();
    k.a(localObject1, "flash.history");
    localObject1 = (CharSequence)localObject1;
    Object localObject2 = (CharSequence)" ";
    boolean bool2 = m.a((CharSequence)localObject1, (CharSequence)localObject2, false);
    if (!bool2)
    {
      localObject1 = new String[j];
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("should not be in db for ");
      localObject2 = paramFlash.e();
      ((StringBuilder)localObject3).append((String)localObject2);
      localObject3 = ((StringBuilder)localObject3).toString();
      localObject1[0] = localObject3;
      bool2 = paramFlash.k();
      long l1;
      long l2;
      if (bool2)
      {
        localObject1 = paramFlash.a();
        k.a(localObject1, "flash.sender");
        localObject1 = String.valueOf(((Sender)localObject1).a().longValue());
        l1 = System.currentTimeMillis();
        l2 = l1;
        localObject3 = localObject1;
        localObject1 = this;
      }
      else
      {
        localObject1 = String.valueOf(paramFlash.b());
        l1 = paramFlash.g();
        l2 = l1;
        localObject3 = localObject1;
        localObject1 = this;
      }
      a locala = a;
      HistoryEvent localHistoryEvent = new com/truecaller/data/entity/HistoryEvent;
      CallLogFlashItem localCallLogFlashItem = new com/truecaller/flash/CallLogFlashItem;
      long l3 = paramFlash.g();
      localObject3 = String.valueOf(localObject3);
      String str = "+".concat((String)localObject3);
      localObject2 = localCallLogFlashItem;
      localObject3 = locala;
      int m = 480;
      localCallLogFlashItem.<init>(l3, str, l2, 0L, k, 0, 0, null, null, m, null);
      localHistoryEvent.<init>(localCallLogFlashItem);
      locala.a(localHistoryEvent);
      return;
    }
    localObject1 = this;
    new String[1][0] = "should be already in the db";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */