package com.truecaller.flash;

import c.g.b.k;

public final class CallLogFlashItem
{
  private int action;
  private long callLogId;
  private String componentName;
  private long duration;
  private int features;
  private transient Integer flag;
  private String number;
  private long timestamp;
  private int type;
  
  public CallLogFlashItem(long paramLong1, String paramString1, long paramLong2, long paramLong3, int paramInt1, int paramInt2, int paramInt3, String paramString2, Integer paramInteger)
  {
    callLogId = paramLong1;
    number = paramString1;
    timestamp = paramLong2;
    duration = paramLong3;
    type = paramInt1;
    action = paramInt2;
    features = paramInt3;
    componentName = paramString2;
    flag = paramInteger;
  }
  
  public final long component1()
  {
    return callLogId;
  }
  
  public final String component2()
  {
    return number;
  }
  
  public final long component3()
  {
    return timestamp;
  }
  
  public final long component4()
  {
    return duration;
  }
  
  public final int component5()
  {
    return type;
  }
  
  public final int component6()
  {
    return action;
  }
  
  public final int component7()
  {
    return features;
  }
  
  public final String component8()
  {
    return componentName;
  }
  
  public final Integer component9()
  {
    return flag;
  }
  
  public final CallLogFlashItem copy(long paramLong1, String paramString1, long paramLong2, long paramLong3, int paramInt1, int paramInt2, int paramInt3, String paramString2, Integer paramInteger)
  {
    k.b(paramString1, "number");
    CallLogFlashItem localCallLogFlashItem = new com/truecaller/flash/CallLogFlashItem;
    localCallLogFlashItem.<init>(paramLong1, paramString1, paramLong2, paramLong3, paramInt1, paramInt2, paramInt3, paramString2, paramInteger);
    return localCallLogFlashItem;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof CallLogFlashItem;
      if (bool2)
      {
        paramObject = (CallLogFlashItem)paramObject;
        long l1 = callLogId;
        long l2 = callLogId;
        bool2 = l1 < l2;
        Object localObject;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localObject = null;
        }
        if (bool2)
        {
          localObject = number;
          String str = number;
          bool2 = k.a(localObject, str);
          if (bool2)
          {
            l1 = timestamp;
            l2 = timestamp;
            bool2 = l1 < l2;
            if (!bool2)
            {
              bool2 = true;
            }
            else
            {
              bool2 = false;
              localObject = null;
            }
            if (bool2)
            {
              l1 = duration;
              l2 = duration;
              bool2 = l1 < l2;
              if (!bool2)
              {
                bool2 = true;
              }
              else
              {
                bool2 = false;
                localObject = null;
              }
              if (bool2)
              {
                int i = type;
                int j = type;
                if (i == j)
                {
                  i = 1;
                }
                else
                {
                  i = 0;
                  localObject = null;
                }
                if (i != 0)
                {
                  i = action;
                  j = action;
                  if (i == j)
                  {
                    i = 1;
                  }
                  else
                  {
                    i = 0;
                    localObject = null;
                  }
                  if (i != 0)
                  {
                    i = features;
                    j = features;
                    if (i == j)
                    {
                      i = 1;
                    }
                    else
                    {
                      i = 0;
                      localObject = null;
                    }
                    if (i != 0)
                    {
                      localObject = componentName;
                      str = componentName;
                      boolean bool3 = k.a(localObject, str);
                      if (bool3)
                      {
                        localObject = flag;
                        paramObject = flag;
                        boolean bool4 = k.a(localObject, paramObject);
                        if (bool4) {
                          return bool1;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int getAction()
  {
    return action;
  }
  
  public final long getCallLogId()
  {
    return callLogId;
  }
  
  public final String getComponentName()
  {
    return componentName;
  }
  
  public final long getDuration()
  {
    return duration;
  }
  
  public final int getFeatures()
  {
    return features;
  }
  
  public final Integer getFlag()
  {
    return flag;
  }
  
  public final String getNumber()
  {
    return number;
  }
  
  public final long getTimestamp()
  {
    return timestamp;
  }
  
  public final int getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    long l1 = callLogId;
    int i = 32;
    long l2 = l1 >>> i;
    l1 ^= l2;
    int j = (int)l1 * 31;
    Object localObject = number;
    int k = 0;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    long l3 = timestamp;
    long l4 = l3 >>> i;
    int m = (int)(l3 ^ l4);
    j = (j + m) * 31;
    l3 = duration;
    l4 = l3 >>> i;
    l3 ^= l4;
    m = (int)l3;
    j = (j + m) * 31;
    m = type;
    j = (j + m) * 31;
    m = action;
    j = (j + m) * 31;
    m = features;
    j = (j + m) * 31;
    localObject = componentName;
    if (localObject != null)
    {
      m = localObject.hashCode();
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = (j + m) * 31;
    localObject = flag;
    if (localObject != null) {
      k = localObject.hashCode();
    }
    return j + k;
  }
  
  public final void setAction(int paramInt)
  {
    action = paramInt;
  }
  
  public final void setCallLogId(long paramLong)
  {
    callLogId = paramLong;
  }
  
  public final void setComponentName(String paramString)
  {
    componentName = paramString;
  }
  
  public final void setDuration(long paramLong)
  {
    duration = paramLong;
  }
  
  public final void setFeatures(int paramInt)
  {
    features = paramInt;
  }
  
  public final void setFlag(Integer paramInteger)
  {
    flag = paramInteger;
  }
  
  public final void setNumber(String paramString)
  {
    k.b(paramString, "<set-?>");
    number = paramString;
  }
  
  public final void setTimestamp(long paramLong)
  {
    timestamp = paramLong;
  }
  
  public final void setType(int paramInt)
  {
    type = paramInt;
  }
  
  public final String toString()
  {
    String str = number;
    if (str == null) {
      str = "null";
    } else {
      str = "<non-null raw number>";
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{CallLogFlashItem:{callLogId=");
    long l = callLogId;
    localStringBuilder.append(l);
    localStringBuilder.append(", number= ");
    localStringBuilder.append(str);
    localStringBuilder.append(", timestamp=");
    l = timestamp;
    localStringBuilder.append(l);
    localStringBuilder.append(", duration=");
    l = duration;
    localStringBuilder.append(l);
    localStringBuilder.append(", type=");
    int i = type;
    localStringBuilder.append(i);
    localStringBuilder.append(", action=");
    i = action;
    localStringBuilder.append(i);
    localStringBuilder.append(", features=");
    i = features;
    localStringBuilder.append(i);
    localStringBuilder.append(", phoneAccountComponentName=");
    str = componentName;
    localStringBuilder.append(str);
    localStringBuilder.append(' ');
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.CallLogFlashItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */