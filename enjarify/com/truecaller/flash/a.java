package com.truecaller.flash;

import android.os.Bundle;
import android.text.TextUtils;
import c.g.b.k;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.analytics.bb;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.am;
import com.truecaller.flashsdk.core.s;
import com.truecaller.tracking.events.l;
import com.truecaller.tracking.events.l.a;
import com.truecaller.tracking.events.m;
import com.truecaller.tracking.events.m.a;
import com.truecaller.tracking.events.n;
import com.truecaller.tracking.events.n.a;
import java.util.Iterator;
import java.util.Set;
import org.apache.a.d.d;

public final class a
  implements s
{
  private final f a;
  private final b b;
  
  public a(f paramf, b paramb)
  {
    a = paramf;
    b = paramb;
  }
  
  public final void a(String paramString, Bundle paramBundle)
  {
    k.b(paramString, "eventName");
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool1)
    {
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("FLASH_USED");
      ((e.a)localObject1).a("FlashActionType", paramString);
      e.a locala = new com/truecaller/analytics/e$a;
      locala.<init>(paramString);
      if (paramBundle != null)
      {
        int i = paramString.hashCode();
        Object localObject3;
        Object localObject4;
        boolean bool5;
        switch (i)
        {
        default: 
          break;
        case 1973848558: 
          localObject2 = "ANDROID_FLASH_VIEW_PROFILE";
          bool2 = paramString.equals(localObject2);
          if (!bool2) {
            break label1468;
          }
          break;
        case 1929709946: 
          localObject2 = "ANDROID_FLASH_REPLIED";
          bool2 = paramString.equals(localObject2);
          if (!bool2) {
            break label1468;
          }
          break;
        case 967132857: 
          localObject2 = "ANDROID_FLASH_CLOSE";
          bool2 = paramString.equals(localObject2);
          if (!bool2) {
            break label1468;
          }
          break;
        case 392911307: 
          localObject2 = "ANDROID_FLASH_TAPPED";
          bool2 = paramString.equals(localObject2);
          if (!bool2) {
            break label1468;
          }
          localObject2 = paramBundle.getString("flash_context");
          if (localObject2 == null) {
            break label1468;
          }
          localObject2 = paramBundle.getString("flash_context");
          if (localObject2 == null) {
            break label1468;
          }
          int j = ((String)localObject2).hashCode();
          switch (j)
          {
          default: 
            break;
          case 1198114018: 
            localObject3 = "callMeBackPopupOutApp";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "callMeBackPopupOutApp";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
            break;
          case 1018734170: 
            localObject3 = "afterCall";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "afterCall";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
            break;
          case 740154499: 
            localObject3 = "conversation";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "conversation";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
            break;
          case 628280070: 
            localObject3 = "deepLink";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "deepLink";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
            break;
          case 595233003: 
            localObject3 = "notification";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "notification";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
            break;
          case -674115797: 
            localObject3 = "globalSearch";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "globalSearch";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
            break;
          case -845664406: 
            localObject3 = "incomingFlash";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "incomingFlash";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
            break;
          case -1206196785: 
            localObject3 = "flashShare";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "flashShare";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
            break;
          case -1522107352: 
            localObject3 = "callMeBackNotification";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "callMeBackNotification";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
            break;
          case -1768263699: 
            localObject3 = "callMeBackPopupInApp";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "callMeBackPopupInApp";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
            break;
          case -1880932050: 
            localObject3 = "searchResults";
            bool2 = ((String)localObject2).equals(localObject3);
            if (!bool2) {
              break;
            }
            localObject2 = a;
            localObject3 = "searchResults";
            localObject4 = "flashButton";
            bb.a((f)localObject2, (String)localObject3, (String)localObject4);
          }
          break;
        case 263288744: 
          localObject2 = "ANDROID_FLASH_OPENED";
          bool2 = paramString.equals(localObject2);
          if (!bool2) {
            break label1468;
          }
          break;
        case 199987674: 
          localObject2 = "ANDROID_FLASH_MISSED";
          bool2 = paramString.equals(localObject2);
          if (!bool2) {
            break label1468;
          }
          break;
        case 170215063: 
          localObject2 = "ANDROID_FLASH_SENT";
          bool2 = paramString.equals(localObject2);
          if (!bool2) {
            break label1468;
          }
          localObject2 = n.b();
          localObject3 = (CharSequence)paramBundle.getString("type");
          localObject3 = ((n.a)localObject2).d((CharSequence)localObject3);
          localObject4 = (CharSequence)paramBundle.getString("flash_message_id");
          localObject3 = ((n.a)localObject3).a((CharSequence)localObject4);
          boolean bool4 = paramBundle.getBoolean("flash_from_phonebook");
          localObject3 = ((n.a)localObject3).a(bool4);
          localObject4 = (CharSequence)paramBundle.getString("flash_receiver_id");
          localObject3 = ((n.a)localObject3).b((CharSequence)localObject4);
          localObject4 = (CharSequence)paramBundle.getString("flash_context");
          localObject3 = ((n.a)localObject3).c((CharSequence)localObject4);
          localObject4 = (CharSequence)paramBundle.getString("flash_reply_id");
          localObject3 = ((n.a)localObject3).e((CharSequence)localObject4);
          int k = Integer.parseInt(paramBundle.getString("history_length"));
          localObject3 = ((n.a)localObject3).a(k);
          k.a(localObject3, "flashInitiatedEvent.setC…alytics.HISTORY_LENGTH)))");
          localObject4 = (CharSequence)paramBundle.getString("flash_thread_id");
          ((n.a)localObject3).f((CharSequence)localObject4);
          localObject3 = (ae)a.a();
          localObject2 = (d)((n.a)localObject2).a();
          ((ae)localObject3).a((d)localObject2);
          break;
        case -687160992: 
          localObject2 = "ANDROID_FLASH_RECEIVED";
          bool2 = paramString.equals(localObject2);
          if (!bool2) {
            break label1468;
          }
          localObject2 = m.b();
          localObject3 = (CharSequence)paramBundle.getString("type");
          localObject3 = ((m.a)localObject2).d((CharSequence)localObject3);
          localObject4 = (CharSequence)paramBundle.getString("flash_message_id");
          localObject3 = ((m.a)localObject3).a((CharSequence)localObject4);
          bool5 = paramBundle.getBoolean("flash_from_phonebook");
          localObject3 = ((m.a)localObject3).a(bool5);
          localObject4 = (CharSequence)paramBundle.getString("flash_sender_id");
          localObject3 = ((m.a)localObject3).b((CharSequence)localObject4);
          k.a(localObject3, "flashIncomingEvent.setCo…alytics.FLASH_SENDER_ID))");
          localObject4 = (CharSequence)paramBundle.getString("flash_thread_id");
          ((m.a)localObject3).c((CharSequence)localObject4);
          localObject3 = (ae)a.a();
          localObject2 = (d)((m.a)localObject2).a();
          ((ae)localObject3).a((d)localObject2);
          break;
        }
        Object localObject2 = "ANDROID_FLASH_BLOCK_USER";
        boolean bool2 = paramString.equals(localObject2);
        if (bool2)
        {
          localObject2 = l.b();
          localObject3 = (CharSequence)paramBundle.getString("type");
          localObject3 = ((l.a)localObject2).d((CharSequence)localObject3);
          localObject4 = (CharSequence)paramBundle.getString("flash_message_id");
          localObject3 = ((l.a)localObject3).a((CharSequence)localObject4);
          bool5 = paramBundle.getBoolean("flash_from_phonebook");
          localObject3 = ((l.a)localObject3).a(bool5);
          bool5 = paramBundle.getBoolean("flash_missed");
          localObject3 = ((l.a)localObject3).b(bool5);
          localObject4 = (CharSequence)paramBundle.getString("flash_sender_id");
          localObject3 = ((l.a)localObject3).b((CharSequence)localObject4);
          localObject4 = (CharSequence)paramBundle.getString("flash_thread_id");
          localObject3 = ((l.a)localObject3).c((CharSequence)localObject4);
          k.a(localObject3, "flashActionEvent.setCont…alytics.FLASH_THREAD_ID))");
          localObject4 = (CharSequence)paramBundle.getString("flash_action_name");
          ((l.a)localObject3).e((CharSequence)localObject4);
          localObject3 = (ae)a.a();
          localObject2 = (d)((l.a)localObject2).a();
          ((ae)localObject3).a((d)localObject2);
        }
        label1468:
        localObject2 = paramBundle.keySet().iterator();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject2).hasNext();
          if (!bool3) {
            break;
          }
          localObject3 = (String)((Iterator)localObject2).next();
          localObject4 = paramBundle.get((String)localObject3);
          if (localObject4 != null)
          {
            localObject4 = localObject3;
            localObject4 = (CharSequence)localObject3;
            CharSequence localCharSequence = (CharSequence)"type";
            boolean bool6 = am.a((CharSequence)localObject4, localCharSequence);
            if (!bool6)
            {
              localCharSequence = (CharSequence)"history_length";
              bool6 = am.a((CharSequence)localObject4, localCharSequence);
              if (!bool6)
              {
                localCharSequence = (CharSequence)"flash_context";
                bool6 = am.a((CharSequence)localObject4, localCharSequence);
                if (!bool6)
                {
                  localCharSequence = (CharSequence)"CampaignDescription";
                  bool6 = am.a((CharSequence)localObject4, localCharSequence);
                  if (!bool6)
                  {
                    localCharSequence = (CharSequence)"FlashFromHistory";
                    bool6 = am.a((CharSequence)localObject4, localCharSequence);
                    if (!bool6)
                    {
                      localCharSequence = (CharSequence)"flash_waiting_timer";
                      bool6 = am.a((CharSequence)localObject4, localCharSequence);
                      if (!bool6)
                      {
                        localCharSequence = (CharSequence)"sentFailed";
                        bool6 = am.a((CharSequence)localObject4, localCharSequence);
                        if (!bool6)
                        {
                          localCharSequence = (CharSequence)"ANDROID_FLASH_CUSTOM_BUTTON_CLICKED";
                          bool5 = am.a((CharSequence)localObject4, localCharSequence);
                          if (!bool5) {
                            continue;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            localObject4 = paramBundle.getString((String)localObject3);
            ((e.a)localObject1).a((String)localObject3, (String)localObject4);
            localObject4 = paramBundle.getString((String)localObject3);
            locala.a((String)localObject3, (String)localObject4);
          }
        }
        localObject2 = "ANDROID_FLASH_SENT_FAILED";
        boolean bool7 = k.a(paramString, localObject2);
        if (bool7)
        {
          localObject2 = paramBundle.getString("flash_thread_id");
          ((e.a)localObject1).a("flash_thread_id", (String)localObject2);
          paramString = "flash_thread_id";
          localObject2 = "flash_thread_id";
          paramBundle = paramBundle.getString((String)localObject2);
          locala.a(paramString, paramBundle);
        }
      }
      paramString = b;
      paramBundle = ((e.a)localObject1).a();
      k.a(paramBundle, "event.build()");
      paramString.a(paramBundle);
      paramString = b;
      paramBundle = locala.a();
      localObject1 = "eventOld.build()";
      k.a(paramBundle, (String)localObject1);
      paramString.a(paramBundle);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */