package com.truecaller.flash;

import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d
{
  private final f a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private k(f paramf, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramf;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static k a(f paramf, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    k localk = new com/truecaller/flash/k;
    localk.<init>(paramf, paramProvider1, paramProvider2, paramProvider3);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */