package com.truecaller.flash;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final f a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private h(f paramf, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramf;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static h a(f paramf, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    h localh = new com/truecaller/flash/h;
    localh.<init>(paramf, paramProvider1, paramProvider2, paramProvider3);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */