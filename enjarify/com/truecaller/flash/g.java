package com.truecaller.flash;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final f a;
  private final Provider b;
  private final Provider c;
  
  private g(f paramf, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramf;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static g a(f paramf, Provider paramProvider1, Provider paramProvider2)
  {
    g localg = new com/truecaller/flash/g;
    localg.<init>(paramf, paramProvider1, paramProvider2);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */