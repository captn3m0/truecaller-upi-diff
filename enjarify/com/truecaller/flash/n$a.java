package com.truecaller.flash;

import android.content.Intent;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.flashsdk.core.b;
import java.util.ArrayList;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.e;

final class n$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private ag f;
  
  n$a(n paramn, Intent paramIntent, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/flash/n$a;
    n localn = d;
    Intent localIntent = e;
    locala.<init>(localn, localIntent, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = c;
    int j;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject1 = (b)b;
      j = paramObject instanceof o.b;
      if (j != 0) {
        throw a;
      }
      break;
    case 0: 
      j = paramObject instanceof o.b;
      if (j != 0) {
        break label235;
      }
      paramObject = f;
      Object localObject2 = e;
      Object localObject3 = "com.truecaller.datamanager.EXTRA_PRESENCE";
      localObject2 = ((Intent)localObject2).getSerializableExtra((String)localObject3);
      if (localObject2 == null) {
        break label223;
      }
      localObject2 = (ArrayList)localObject2;
      localObject3 = d.a;
      f localf = d.b;
      Object localObject4 = new com/truecaller/flash/n$a$1;
      ((n.a.1)localObject4).<init>(this, (ArrayList)localObject2, null);
      localObject4 = (m)localObject4;
      int k = 2;
      paramObject = e.a((ag)paramObject, localf, (m)localObject4, k);
      a = localObject2;
      b = localObject3;
      j = 1;
      c = j;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      localObject1 = localObject3;
    }
    paramObject = (List)paramObject;
    ((b)localObject1).a((List)paramObject);
    return x.a;
    label223:
    paramObject = new c/u;
    ((u)paramObject).<init>("null cannot be cast to non-null type kotlin.collections.ArrayList<com.truecaller.presence.Presence> /* = java.util.ArrayList<com.truecaller.presence.Presence> */");
    throw ((Throwable)paramObject);
    label235:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.n.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */