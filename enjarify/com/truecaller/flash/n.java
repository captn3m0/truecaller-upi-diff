package com.truecaller.flash;

import android.content.Intent;
import c.d.f;
import c.g.b.k;
import com.truecaller.flashsdk.core.b;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class n
  implements m
{
  final b a;
  final f b;
  private final f c;
  
  public n(b paramb, f paramf1, f paramf2)
  {
    a = paramb;
    b = paramf1;
    c = paramf2;
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    Object localObject1 = "com.truecaller.datamanager.EXTRA_PRESENCE";
    boolean bool = paramIntent.hasExtra((String)localObject1);
    if (!bool) {
      return;
    }
    localObject1 = (ag)bg.a;
    f localf = c;
    Object localObject2 = new com/truecaller/flash/n$a;
    ((n.a)localObject2).<init>(this, paramIntent, null);
    localObject2 = (c.g.a.m)localObject2;
    e.b((ag)localObject1, localf, (c.g.a.m)localObject2, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */