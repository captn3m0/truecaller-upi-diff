package com.truecaller.flash;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final f a;
  private final Provider b;
  
  private i(f paramf, Provider paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static i a(f paramf, Provider paramProvider)
  {
    i locali = new com/truecaller/flash/i;
    locali.<init>(paramf, paramProvider);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */