package com.truecaller.incallui.service;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.z.d;
import android.support.v4.app.z.e;
import android.support.v4.app.z.g;
import android.telecom.Call;
import android.telecom.Call.Callback;
import android.telecom.Call.Details;
import android.telecom.CallAudioState;
import android.telecom.InCallService;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.incallui.R.color;
import com.truecaller.incallui.R.drawable;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.string;
import com.truecaller.incallui.a.i.a;
import com.truecaller.incallui.callui.InCallUIActivity;
import com.truecaller.incallui.callui.InCallUIActivity.a;
import com.truecaller.utils.extensions.n;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.a.h;

public final class InCallUIService
  extends InCallService
  implements a.c
{
  public a.a a;
  public com.truecaller.notificationchannels.b b;
  final h c;
  private Call d;
  private AudioManager e;
  private PowerManager.WakeLock f;
  private z.d g;
  private int h;
  private final InCallUIService.a i;
  
  public InCallUIService()
  {
    Object localObject = kotlinx.coroutines.a.i.a(-1);
    c = ((h)localObject);
    int j = R.id.incallui_service_ongoing_call_notification;
    h = j;
    localObject = new com/truecaller/incallui/service/InCallUIService$a;
    ((InCallUIService.a)localObject).<init>(this);
    i = ((InCallUIService.a)localObject);
  }
  
  private final z.d b(String paramString)
  {
    z.d locald = new android/support/v4/app/z$d;
    Object localObject = this;
    localObject = (Context)this;
    locald.<init>((Context)localObject, paramString);
    int j = R.drawable.ic_button_incallui_answer;
    paramString = locald.a(j).b().d();
    int k = R.color.incallui_header_color;
    k = android.support.v4.content.b.c((Context)localObject, k);
    paramString = paramString.f(k).c("INCALLUI_NOTIFICATION_GROUP").e(2);
    k.a(paramString, "NotificationCompat.Build…ationCompat.PRIORITY_MAX)");
    return paramString;
  }
  
  private final void s()
  {
    Object localObject = g;
    if (localObject != null)
    {
      int j = h;
      localObject = ((z.d)localObject).h();
      startForeground(j, (Notification)localObject);
      return;
    }
  }
  
  public final h a()
  {
    return c;
  }
  
  public final void a(char paramChar)
  {
    Call localCall = d;
    if (localCall != null)
    {
      localCall.playDtmfTone(paramChar);
      return;
    }
  }
  
  public final void a(int paramInt, Long paramLong)
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = this;
    localObject2 = (Context)this;
    Object localObject3 = InCallUIActivity.b;
    localObject3 = InCallUIActivity.a.a((Context)localObject2, (Call)localObject1);
    localObject3 = PendingIntent.getActivity((Context)localObject2, 0, (Intent)localObject3, 0);
    Object localObject4 = b;
    if (localObject4 == null)
    {
      String str1 = "notificationChannelProvider";
      k.a(str1);
    }
    localObject4 = ((com.truecaller.notificationchannels.b)localObject4).j();
    localObject4 = b((String)localObject4);
    Object localObject5 = (CharSequence)getString(paramInt);
    localObject5 = ((z.d)localObject4).b((CharSequence)localObject5).a((PendingIntent)localObject3);
    int j = R.drawable.ic_button_incallui_hangup;
    int k = R.string.incallui_notification_button_hang_up;
    localObject4 = (CharSequence)getString(k);
    int m = R.id.incallui_incoming_notification_action_hang_up;
    Object localObject6 = InCallUIActivity.b;
    k.b(localObject2, "context");
    localObject6 = "call";
    k.b(localObject1, (String)localObject6);
    int n = Build.VERSION.SDK_INT;
    int i1 = 23;
    boolean bool;
    if (n < i1)
    {
      bool = false;
      localObject1 = null;
    }
    else
    {
      localObject6 = new android/content/Intent;
      ((Intent)localObject6).<init>((Context)localObject2, InCallUIActivity.class);
      localObject6 = ((Intent)localObject6).setAction("com.truecaller.incallui.callui.ACTION_HANG_UP_CALL");
      i1 = 268435456;
      localObject6 = ((Intent)localObject6).setFlags(i1);
      localObject1 = ((Call)localObject1).getDetails();
      String str2 = "call.details";
      k.a(localObject1, str2);
      localObject1 = ((Call.Details)localObject1).getHandle();
      localObject1 = ((Intent)localObject6).setData((Uri)localObject1);
    }
    localObject1 = PendingIntent.getActivity((Context)localObject2, m, (Intent)localObject1, 0);
    localObject5 = ((z.d)localObject5).a(j, (CharSequence)localObject4, (PendingIntent)localObject1);
    if (paramLong != null)
    {
      bool = true;
      localObject1 = ((z.d)localObject5).a(bool);
      long l = paramLong.longValue();
      ((z.d)localObject1).a(l);
    }
    g = ((z.d)localObject5);
    paramInt = R.id.incallui_service_ongoing_call_notification;
    h = paramInt;
    s();
  }
  
  public final void a(Bitmap paramBitmap)
  {
    k.b(paramBitmap, "icon");
    z.d locald = g;
    if (locald != null) {
      locald.a(paramBitmap);
    }
    s();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    z.d locald = g;
    if (locald != null)
    {
      paramString = (CharSequence)paramString;
      locald.a(paramString);
    }
    s();
  }
  
  public final void b()
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = this;
    localObject2 = (Context)this;
    Object localObject3 = InCallUIActivity.b;
    localObject3 = InCallUIActivity.a.a((Context)localObject2, (Call)localObject1);
    localObject3 = PendingIntent.getActivity((Context)localObject2, 0, (Intent)localObject3, 0);
    Object localObject4 = b;
    if (localObject4 == null)
    {
      localObject5 = "notificationChannelProvider";
      k.a((String)localObject5);
    }
    localObject4 = ((com.truecaller.notificationchannels.b)localObject4).i();
    localObject4 = b((String)localObject4);
    int j = R.string.incallui_notification_incoming_content;
    Object localObject5 = (CharSequence)getString(j);
    localObject4 = ((z.d)localObject4).b((CharSequence)localObject5).a((PendingIntent)localObject3).a("call");
    localObject5 = new android/support/v4/app/z$e;
    ((z.e)localObject5).<init>();
    localObject5 = (z.g)localObject5;
    localObject4 = ((z.d)localObject4).a((z.g)localObject5);
    j = R.drawable.ic_button_incallui_close;
    int k = R.string.incallui_notification_button_decline;
    CharSequence localCharSequence = (CharSequence)getString(k);
    int m = R.id.incallui_incoming_notification_action_decline;
    Object localObject6 = InCallUIActivity.b;
    k.b(localObject2, "context");
    localObject6 = "call";
    k.b(localObject1, (String)localObject6);
    int n = Build.VERSION.SDK_INT;
    Intent localIntent = null;
    int i1 = 268435456;
    int i2 = 23;
    if (n < i2)
    {
      n = 0;
      localObject6 = null;
    }
    else
    {
      localObject6 = new android/content/Intent;
      ((Intent)localObject6).<init>((Context)localObject2, InCallUIActivity.class);
      localObject6 = ((Intent)localObject6).setAction("com.truecaller.incallui.callui.ACTION_DECLINE_CALL").setFlags(i1);
      Object localObject7 = ((Call)localObject1).getDetails();
      String str = "call.details";
      k.a(localObject7, str);
      localObject7 = ((Call.Details)localObject7).getHandle();
      localObject6 = ((Intent)localObject6).setData((Uri)localObject7);
    }
    PendingIntent localPendingIntent = PendingIntent.getActivity((Context)localObject2, m, (Intent)localObject6, 0);
    localObject4 = ((z.d)localObject4).a(j, localCharSequence, localPendingIntent);
    j = R.drawable.ic_button_incallui_answer;
    k = R.string.incallui_notification_button_answer;
    localCharSequence = (CharSequence)getString(k);
    m = R.id.incallui_incoming_notification_action_answer;
    localObject6 = InCallUIActivity.b;
    k.b(localObject2, "context");
    localObject6 = "call";
    k.b(localObject1, (String)localObject6);
    n = Build.VERSION.SDK_INT;
    if (n >= i2)
    {
      localObject6 = new android/content/Intent;
      ((Intent)localObject6).<init>((Context)localObject2, InCallUIActivity.class);
      localObject6 = ((Intent)localObject6).setAction("com.truecaller.incallui.callui.ACTION_ANSWER_CALL").setFlags(i1);
      localObject1 = ((Call)localObject1).getDetails();
      k.a(localObject1, "call.details");
      localObject1 = ((Call.Details)localObject1).getHandle();
      localIntent = ((Intent)localObject6).setData((Uri)localObject1);
    }
    localObject1 = PendingIntent.getActivity((Context)localObject2, m, localIntent, 0);
    localObject1 = ((z.d)localObject4).a(j, localCharSequence, (PendingIntent)localObject1).c().c((PendingIntent)localObject3);
    g = ((z.d)localObject1);
    int i3 = R.id.incallui_service_incoming_call_notification;
    h = i3;
    s();
  }
  
  public final void c()
  {
    Object localObject = d;
    if (localObject == null) {
      return;
    }
    localObject = InCallUIActivity.a.a((Context)this, (Call)localObject);
    startActivity((Intent)localObject);
  }
  
  public final void d()
  {
    Call localCall = d;
    if (localCall != null)
    {
      localCall.answer(0);
      return;
    }
  }
  
  public final void e()
  {
    PowerManager.WakeLock localWakeLock = f;
    if (localWakeLock != null)
    {
      boolean bool = localWakeLock.isHeld();
      if (!bool)
      {
        TimeUnit localTimeUnit = TimeUnit.HOURS;
        long l1 = 5;
        long l2 = localTimeUnit.toMillis(l1);
        localWakeLock.acquire(l2);
      }
      return;
    }
  }
  
  public final boolean f()
  {
    ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = new android/app/ActivityManager$RunningAppProcessInfo;
    localRunningAppProcessInfo.<init>();
    ActivityManager.getMyMemoryState(localRunningAppProcessInfo);
    int j = importance;
    int k = 100;
    return j != k;
  }
  
  public final void g()
  {
    Object localObject = this;
    localObject = (Context)this;
    int j = R.string.incallui_status_call_connected;
    Toast.makeText((Context)localObject, j, 0).show();
  }
  
  public final boolean h()
  {
    CallAudioState localCallAudioState = getCallAudioState();
    k.a(localCallAudioState, "callAudioState");
    return localCallAudioState.isMuted();
  }
  
  public final void i()
  {
    setMuted(true);
  }
  
  public final void j()
  {
    setMuted(false);
  }
  
  public final boolean k()
  {
    AudioManager localAudioManager = e;
    if (localAudioManager != null) {
      return localAudioManager.isSpeakerphoneOn();
    }
    return false;
  }
  
  public final void l()
  {
    AudioManager localAudioManager = e;
    if (localAudioManager != null)
    {
      localAudioManager.setSpeakerphoneOn(true);
      return;
    }
  }
  
  public final void m()
  {
    AudioManager localAudioManager = e;
    if (localAudioManager != null)
    {
      localAudioManager.setSpeakerphoneOn(false);
      return;
    }
  }
  
  public final void n()
  {
    Call localCall = d;
    if (localCall != null)
    {
      localCall.hold();
      return;
    }
  }
  
  public final void o()
  {
    Call localCall = d;
    if (localCall != null)
    {
      localCall.unhold();
      return;
    }
  }
  
  public final void onCallAdded(Call paramCall)
  {
    k.b(paramCall, "call");
    Object localObject1 = d;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = (Call.Callback)i;
      ((Call)localObject1).unregisterCallback((Call.Callback)localObject2);
    }
    if (paramCall != null)
    {
      localObject1 = (Call.Callback)i;
      paramCall.registerCallback((Call.Callback)localObject1);
      localObject1 = c;
      int j = paramCall.getState();
      localObject2 = Integer.valueOf(j);
      ((h)localObject1).d_(localObject2);
    }
    d = paramCall;
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    paramCall = paramCall.getDetails();
    k.a(paramCall, "call.details");
    paramCall = paramCall.getHandle();
    k.a(paramCall, "call.details.handle");
    paramCall = paramCall.getSchemeSpecificPart();
    k.a(paramCall, "call.details.handle.schemeSpecificPart");
    ((a.a)localObject1).a(paramCall);
  }
  
  public final void onCallRemoved(Call paramCall)
  {
    String str = "call";
    k.b(paramCall, str);
    paramCall = a;
    if (paramCall == null)
    {
      str = "presenter";
      k.a(str);
    }
    paramCall.a();
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject = com.truecaller.incallui.a.i.a;
    i.a.a().a(this);
    localObject = getSystemService("audio");
    if (localObject != null)
    {
      localObject = (AudioManager)localObject;
      e = ((AudioManager)localObject);
      localObject = e;
      if (localObject != null)
      {
        int j = 2;
        ((AudioManager)localObject).setMode(j);
      }
      localObject = n.a(com.truecaller.utils.extensions.i.g(this));
      f = ((PowerManager.WakeLock)localObject);
      localObject = a;
      if (localObject == null)
      {
        String str = "presenter";
        k.a(str);
      }
      ((a.a)localObject).a(this);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type android.media.AudioManager");
    throw ((Throwable)localObject);
  }
  
  public final void onDestroy()
  {
    a.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.y_();
    super.onDestroy();
  }
  
  public final void p()
  {
    Call localCall = d;
    if (localCall != null)
    {
      localCall.disconnect();
      return;
    }
  }
  
  public final void q()
  {
    PowerManager.WakeLock localWakeLock = f;
    if (localWakeLock != null)
    {
      boolean bool = localWakeLock.isHeld();
      if (bool) {
        localWakeLock.release();
      }
      return;
    }
  }
  
  public final void r()
  {
    stopForeground(true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.service.InCallUIService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */