package com.truecaller.incallui.service;

import android.graphics.Bitmap;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class b$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  b$c(b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/incallui/service/b$c;
    b localb = c;
    localc.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label198;
      }
      paramObject = b.c(c);
      if (paramObject == null) {
        break label194;
      }
      paramObject = b;
      if (paramObject == null) {
        break label194;
      }
      f localf = b.g(c);
      Object localObject2 = new com/truecaller/incallui/service/b$c$a;
      ((b.c.a)localObject2).<init>(this, (String)paramObject, null);
      localObject2 = (m)localObject2;
      a = paramObject;
      int j = 1;
      b = j;
      paramObject = g.a(localf, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Bitmap)paramObject;
    if (paramObject == null) {
      return x.a;
    }
    localObject1 = b.e(c);
    if (localObject1 != null) {
      ((a.c)localObject1).a((Bitmap)paramObject);
    }
    return x.a;
    label194:
    return x.a;
    label198:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.service.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */