package com.truecaller.incallui.service;

import c.g.b.k;
import kotlinx.coroutines.a.h;

public final class d
  implements c
{
  private a.b a;
  
  public final h a()
  {
    a.b localb = a;
    if (localb != null) {
      return localb.b();
    }
    return null;
  }
  
  public final void a(char paramChar)
  {
    a.b localb = a;
    if (localb != null)
    {
      localb.a(paramChar);
      return;
    }
  }
  
  public final void a(a.b paramb)
  {
    k.b(paramb, "callback");
    a = paramb;
  }
  
  public final void a(boolean paramBoolean)
  {
    a.b localb = a;
    if (localb != null)
    {
      localb.a(paramBoolean);
      return;
    }
  }
  
  public final h b()
  {
    a.b localb = a;
    if (localb != null) {
      return localb.c();
    }
    return null;
  }
  
  public final void b(boolean paramBoolean)
  {
    a.b localb = a;
    if (localb != null)
    {
      localb.b(paramBoolean);
      return;
    }
  }
  
  public final Long c()
  {
    a.b localb = a;
    if (localb != null) {
      return Long.valueOf(localb.d());
    }
    return null;
  }
  
  public final void c(boolean paramBoolean)
  {
    a.b localb = a;
    if (localb != null)
    {
      localb.c(paramBoolean);
      return;
    }
  }
  
  public final Boolean d()
  {
    a.b localb = a;
    if (localb != null) {
      return localb.e();
    }
    return null;
  }
  
  public final Boolean e()
  {
    a.b localb = a;
    if (localb != null) {
      return localb.f();
    }
    return null;
  }
  
  public final String f()
  {
    a.b localb = a;
    if (localb != null) {
      return localb.g();
    }
    return null;
  }
  
  public final void g()
  {
    a.b localb = a;
    if (localb != null)
    {
      localb.h();
      return;
    }
  }
  
  public final void h()
  {
    a.b localb = a;
    if (localb != null)
    {
      localb.i();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.service.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */