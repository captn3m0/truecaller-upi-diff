package com.truecaller.incallui.service;

import android.graphics.Bitmap;
import kotlinx.coroutines.a.h;

public abstract interface a$c
{
  public abstract h a();
  
  public abstract void a(char paramChar);
  
  public abstract void a(int paramInt, Long paramLong);
  
  public abstract void a(Bitmap paramBitmap);
  
  public abstract void a(String paramString);
  
  public abstract void b();
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract boolean f();
  
  public abstract void g();
  
  public abstract boolean h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract boolean k();
  
  public abstract void l();
  
  public abstract void m();
  
  public abstract void n();
  
  public abstract void o();
  
  public abstract void p();
  
  public abstract void q();
  
  public abstract void r();
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.service.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */