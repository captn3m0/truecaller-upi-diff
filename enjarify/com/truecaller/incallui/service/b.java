package com.truecaller.incallui.service;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.incallui.a.d;
import com.truecaller.incallui.a.g;
import com.truecaller.utils.extensions.j;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.a.i;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class b
  extends ba
  implements a.a
{
  private final h c;
  private com.truecaller.incallui.callui.a d;
  private long e;
  private String f;
  private final c g;
  private final g h;
  private final d i;
  private final com.truecaller.utils.a j;
  private final f k;
  private final f l;
  
  public b(c paramc, g paramg, d paramd, com.truecaller.utils.a parama, f paramf1, f paramf2)
  {
    super(paramf1);
    g = paramc;
    h = paramg;
    i = paramd;
    j = parama;
    k = paramf1;
    l = paramf2;
    paramc = i.a(-1);
    c = paramc;
    f = "";
  }
  
  private final void b(String paramString)
  {
    a.c localc = (a.c)b;
    if (localc != null)
    {
      Object localObject = d;
      if (localObject != null)
      {
        localObject = a;
        if (localObject != null) {
          paramString = (String)localObject;
        }
      }
      localc.a(paramString);
    }
    j();
  }
  
  private final void j()
  {
    Object localObject = d;
    String str = null;
    if (localObject != null) {
      localObject = g;
    } else {
      localObject = null;
    }
    if (localObject != null)
    {
      k();
      return;
    }
    localObject = d;
    if (localObject != null) {
      str = b;
    }
    if (str != null)
    {
      l();
      return;
    }
  }
  
  private final bn k()
  {
    Object localObject = new com/truecaller/incallui/service/b$d;
    ((b.d)localObject).<init>(this, null);
    localObject = (m)localObject;
    return e.b(this, null, (m)localObject, 3);
  }
  
  private final bn l()
  {
    Object localObject = new com/truecaller/incallui/service/b$c;
    ((b.c)localObject).<init>(this, null);
    localObject = (m)localObject;
    return e.b(this, null, (m)localObject, 3);
  }
  
  public final void a()
  {
    a.c localc = (a.c)b;
    if (localc != null) {
      localc.q();
    }
    localc = (a.c)b;
    if (localc != null)
    {
      localc.r();
      return;
    }
  }
  
  public final void a(char paramChar)
  {
    Object localObject = f;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(paramChar);
    localObject = localStringBuilder.toString();
    f = ((String)localObject);
    localObject = (a.c)b;
    if (localObject != null)
    {
      ((a.c)localObject).a(paramChar);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "number");
    Object localObject1 = b();
    if (localObject1 != null)
    {
      Object localObject2 = new com/truecaller/incallui/service/b$a;
      ((b.a)localObject2).<init>(this, paramString, null);
      localObject2 = (m)localObject2;
      j.a(this, (h)localObject1, (m)localObject2);
    }
    localObject1 = new com/truecaller/incallui/service/b$b;
    ((b.b)localObject1).<init>(this, paramString, null);
    localObject1 = (m)localObject1;
    e.b(this, null, (m)localObject1, 3);
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localc = (a.c)b;
      if (localc != null) {
        localc.i();
      }
      return;
    }
    a.c localc = (a.c)b;
    if (localc != null)
    {
      localc.j();
      return;
    }
  }
  
  public final h b()
  {
    a.c localc = (a.c)b;
    if (localc != null) {
      return localc.a();
    }
    return null;
  }
  
  public final void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localc = (a.c)b;
      if (localc != null) {
        localc.l();
      }
      return;
    }
    a.c localc = (a.c)b;
    if (localc != null)
    {
      localc.m();
      return;
    }
  }
  
  public final h c()
  {
    return c;
  }
  
  public final void c(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localc = (a.c)b;
      if (localc != null) {
        localc.n();
      }
      return;
    }
    a.c localc = (a.c)b;
    if (localc != null)
    {
      localc.o();
      return;
    }
  }
  
  public final long d()
  {
    return e;
  }
  
  public final Boolean e()
  {
    a.c localc = (a.c)b;
    if (localc != null) {
      return Boolean.valueOf(localc.h());
    }
    return null;
  }
  
  public final Boolean f()
  {
    a.c localc = (a.c)b;
    if (localc != null) {
      return Boolean.valueOf(localc.k());
    }
    return null;
  }
  
  public final String g()
  {
    return f;
  }
  
  public final void h()
  {
    a.c localc = (a.c)b;
    if (localc != null)
    {
      localc.d();
      return;
    }
  }
  
  public final void i()
  {
    a.c localc = (a.c)b;
    if (localc != null)
    {
      localc.p();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.service.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */