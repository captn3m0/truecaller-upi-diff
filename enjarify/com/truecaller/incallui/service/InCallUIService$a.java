package com.truecaller.incallui.service;

import android.telecom.Call;
import android.telecom.Call.Callback;
import c.g.b.k;
import kotlinx.coroutines.a.h;

public final class InCallUIService$a
  extends Call.Callback
{
  InCallUIService$a(InCallUIService paramInCallUIService) {}
  
  public final void onStateChanged(Call paramCall, int paramInt)
  {
    k.b(paramCall, "call");
    paramCall = a.c;
    Integer localInteger = Integer.valueOf(paramInt);
    paramCall.d_(localInteger);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.service.InCallUIService.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */