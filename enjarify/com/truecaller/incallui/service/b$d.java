package com.truecaller.incallui.service;

import android.graphics.Bitmap;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class b$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  b$d(b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/incallui/service/b$d;
    b localb = b;
    locald.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label162;
      }
      paramObject = b.g(b);
      Object localObject2 = new com/truecaller/incallui/service/b$d$a;
      ((b.d.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      int j = 1;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Bitmap)paramObject;
    if (paramObject == null) {
      return x.a;
    }
    localObject1 = b.e(b);
    if (localObject1 != null) {
      ((a.c)localObject1).a((Bitmap)paramObject);
    }
    return x.a;
    label162:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.service.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */