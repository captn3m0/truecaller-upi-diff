package com.truecaller.incallui;

import android.net.Uri;
import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.bp;
import com.truecaller.common.h.u;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.incallui.a.h;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.search.j;
import com.truecaller.network.search.l;
import com.truecaller.network.search.n;
import com.truecaller.util.ce;
import com.truecaller.util.w;
import java.io.IOException;
import java.util.UUID;
import kotlinx.coroutines.ag;

final class b$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  b$a(b paramb, String paramString, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/incallui/b$a;
    b localb = b;
    String str = c;
    locala.<init>(localb, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = null;
        try
        {
          localObject1 = b;
          localObject1 = ((b)localObject1).a();
          localObject1 = ((bp)localObject1).V();
          Object localObject2 = "graph.phoneNumberHelper()";
          c.g.b.k.a(localObject1, (String)localObject2);
          localObject2 = c;
          localObject1 = ((u)localObject1).b((String)localObject2);
          if (localObject1 == null) {
            localObject1 = c;
          }
          localObject2 = b;
          localObject2 = ((b)localObject2).a();
          localObject2 = ((bp)localObject2).u();
          Object localObject3 = "graph.searchManager()";
          c.g.b.k.a(localObject2, (String)localObject3);
          localObject3 = UUID.randomUUID();
          String str1 = "UUID.randomUUID()";
          c.g.b.k.a(localObject3, str1);
          str1 = "voip";
          localObject2 = ((l)localObject2).a((UUID)localObject3, str1);
          localObject2 = ((j)localObject2).b();
          localObject2 = ((j)localObject2).a((String)localObject1);
          localObject2 = ((j)localObject2).a();
          int j = 4;
          localObject2 = ((j)localObject2).a(j);
          localObject2 = ((j)localObject2).f();
          if (localObject2 != null)
          {
            localObject2 = ((n)localObject2).a();
            if (localObject2 != null)
            {
              localObject1 = w.a((Contact)localObject2, (String)localObject1);
              com.truecaller.common.tag.c localc = ce.a((Contact)localObject2);
              h localh = new com/truecaller/incallui/a/h;
              localObject3 = "contact";
              c.g.b.k.a(localObject2, (String)localObject3);
              str1 = ((Contact)localObject2).s();
              localObject3 = "contact.displayNameOrNumber";
              c.g.b.k.a(str1, (String)localObject3);
              j = 0;
              localObject3 = null;
              localObject3 = ((Contact)localObject2).a(false);
              Object localObject4;
              if (localObject3 != null)
              {
                localObject3 = ((Uri)localObject3).toString();
                localObject4 = localObject3;
              }
              else
              {
                localObject4 = null;
              }
              localObject3 = ((Contact)localObject2).q();
              if (localObject3 == null) {
                localObject3 = c;
              }
              Object localObject5 = localObject3;
              String str2 = ((Contact)localObject2).c();
              localObject3 = "contact.shortFormattedAddress";
              c.g.b.k.a(str2, (String)localObject3);
              Object localObject6;
              if (localObject1 != null)
              {
                localObject1 = ((Number)localObject1).g();
                localObject6 = localObject1;
              }
              else
              {
                localObject6 = null;
              }
              localObject1 = b;
              boolean bool2 = b.a((b)localObject1, (Contact)localObject2);
              localObject1 = b;
              Integer localInteger = b.b((b)localObject1, (Contact)localObject2);
              boolean bool3 = ((Contact)localObject2).Z();
              localObject3 = localh;
              localh.<init>(str1, (String)localObject4, (String)localObject5, str2, (String)localObject6, localc, bool2, localInteger, bool3);
              paramObject = localh;
            }
          }
        }
        catch (IOException localIOException)
        {
          localObject1 = (Throwable)localIOException;
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
        }
        return paramObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */