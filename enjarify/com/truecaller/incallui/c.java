package com.truecaller.incallui;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import c.g.b.k;
import c.l.g;
import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.incallui.service.InCallUIService;

public final class c
  implements a
{
  private final r b;
  private final e c;
  
  public c(r paramr, e parame)
  {
    b = paramr;
    c = parame;
  }
  
  private static boolean a()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 23;
    return i >= j;
  }
  
  private static void c(Context paramContext)
  {
    new String[1][0] = "Disable InCallUI service";
    ComponentName localComponentName = new android/content/ComponentName;
    localComponentName.<init>(paramContext, InCallUIService.class);
    paramContext.getPackageManager().setComponentEnabledSetting(localComponentName, 2, 1);
  }
  
  public final void a(Context paramContext)
  {
    Object localObject1 = "context";
    k.b(paramContext, (String)localObject1);
    boolean bool = a();
    if (!bool) {
      return;
    }
    localObject1 = c;
    e.a locala = B;
    Object localObject2 = e.a;
    int i = 86;
    localObject2 = localObject2[i];
    localObject1 = locala.a((e)localObject1, (g)localObject2);
    bool = ((b)localObject1).a();
    if (bool)
    {
      localObject1 = b;
      bool = ((r)localObject1).c();
      if (bool)
      {
        new String[1][0] = "Enable InCallUI service";
        localObject1 = new android/content/ComponentName;
        ((ComponentName)localObject1).<init>(paramContext, InCallUIService.class);
        paramContext = paramContext.getPackageManager();
        int j = 1;
        paramContext.setComponentEnabledSetting((ComponentName)localObject1, j, j);
        return;
      }
    }
    c(paramContext);
  }
  
  public final void b(Context paramContext)
  {
    String str = "context";
    k.b(paramContext, str);
    boolean bool = a();
    if (!bool) {
      return;
    }
    c(paramContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */