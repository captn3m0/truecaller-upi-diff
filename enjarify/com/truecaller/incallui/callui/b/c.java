package com.truecaller.incallui.callui.b;

import c.d.f;
import c.g.a.m;
import com.truecaller.ba;
import com.truecaller.incallui.a.b;
import com.truecaller.incallui.a.o;
import com.truecaller.utils.a;
import com.truecaller.utils.extensions.j;

public final class c
  extends ba
  implements b.a
{
  private final com.truecaller.incallui.service.c c;
  private final o d;
  private final a e;
  private final b f;
  private final f g;
  
  public c(com.truecaller.incallui.service.c paramc, o paramo, a parama, b paramb, f paramf)
  {
    super(paramf);
    c = paramc;
    d = paramo;
    e = parama;
    f = paramb;
    g = paramf;
  }
  
  private final void a(int paramInt1, int paramInt2)
  {
    b.b localb = (b.b)b;
    if (localb != null) {
      localb.m();
    }
    localb = (b.b)b;
    if (localb != null)
    {
      localb.a(paramInt1, paramInt2);
      return;
    }
  }
  
  public final void S_()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.i();
      return;
    }
  }
  
  public final void T_()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.l();
      return;
    }
  }
  
  public final void a()
  {
    c.h();
  }
  
  public final void a(String paramString)
  {
    if (paramString != null)
    {
      localObject = (b.b)b;
      if (localObject != null) {
        ((b.b)localObject).a(paramString);
      }
    }
    paramString = c.b();
    if (paramString == null) {
      return;
    }
    Object localObject = new com/truecaller/incallui/callui/b/c$b;
    ((c.b)localObject).<init>(this, null);
    localObject = (m)localObject;
    j.a(this, paramString, (m)localObject);
  }
  
  public final void a(boolean paramBoolean)
  {
    c.a(paramBoolean);
  }
  
  public final void b(boolean paramBoolean)
  {
    c.b(paramBoolean);
  }
  
  public final void c(boolean paramBoolean)
  {
    c.c(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */