package com.truecaller.incallui.callui.b;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.constraint.motion.MotionLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v4.content.b;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.incallui.R.color;
import com.truecaller.incallui.R.drawable;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.layout;
import com.truecaller.incallui.a.i;
import com.truecaller.incallui.a.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class a
  extends Fragment
  implements b.b
{
  public static final a.a b;
  public b.a a;
  private MotionLayout c;
  private FloatingActionButton d;
  private ToggleButton e;
  private Button f;
  private ToggleButton g;
  private ToggleButton h;
  private ImageView i;
  private ImageButton j;
  private TextView k;
  private ImageView l;
  private View m;
  private View n;
  private Chronometer o;
  private TextView p;
  private HashMap q;
  
  static
  {
    a.a locala = new com/truecaller/incallui/callui/b/a$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public final b.a a()
  {
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(int paramInt)
  {
    ImageView localImageView = l;
    if (localImageView == null)
    {
      String str = "truecallerLogoView";
      k.a(str);
    }
    localImageView.setImageResource(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    String str1 = "activity ?: return";
    k.a(localObject, str1);
    paramInt2 = b.c((Context)localObject, paramInt2);
    localObject = p;
    if (localObject == null)
    {
      str1 = "statusTextView";
      k.a(str1);
    }
    ((TextView)localObject).setText(paramInt1);
    TextView localTextView = p;
    if (localTextView == null)
    {
      localObject = "statusTextView";
      k.a((String)localObject);
    }
    localTextView.setTextColor(paramInt2);
    localTextView = p;
    if (localTextView == null)
    {
      String str2 = "statusTextView";
      k.a(str2);
    }
    t.a((View)localTextView);
  }
  
  public final void a(long paramLong)
  {
    Chronometer localChronometer1 = o;
    String str1;
    if (localChronometer1 == null)
    {
      str1 = "chronometerView";
      k.a(str1);
    }
    t.a((View)localChronometer1);
    localChronometer1 = o;
    if (localChronometer1 == null)
    {
      str1 = "chronometerView";
      k.a(str1);
    }
    localChronometer1.setBase(paramLong);
    Chronometer localChronometer2 = o;
    if (localChronometer2 == null)
    {
      String str2 = "chronometerView";
      k.a(str2);
    }
    localChronometer2.start();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "profileName");
    Object localObject = k;
    if (localObject == null)
    {
      String str = "profileNameTextView";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    ((TextView)localObject).setText(paramString);
    paramString = k;
    if (paramString == null)
    {
      localObject = "profileNameTextView";
      k.a((String)localObject);
    }
    paramString.setSelected(true);
  }
  
  public final void b()
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = "activity ?: return";
    k.a(localObject1, (String)localObject2);
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 21;
    Object localObject3;
    if (i1 >= i2)
    {
      localObject2 = ((f)localObject1).getWindow();
      k.a(localObject2, "context.window");
      localObject3 = localObject1;
      localObject3 = (Context)localObject1;
      int i3 = R.color.incallui_status_bar_spam_color;
      i2 = b.c((Context)localObject3, i3);
      ((Window)localObject2).setStatusBarColor(i2);
    }
    localObject2 = m;
    if (localObject2 == null)
    {
      localObject3 = "headerCoverView";
      k.a((String)localObject3);
    }
    localObject1 = (Context)localObject1;
    i2 = R.color.incallui_spam_color;
    int i4 = b.c((Context)localObject1, i2);
    ((View)localObject2).setBackgroundColor(i4);
    localObject1 = n;
    if (localObject1 == null)
    {
      localObject2 = "headerArcView";
      k.a((String)localObject2);
    }
    i1 = R.drawable.background_incallui_spam_header_view;
    ((View)localObject1).setBackgroundResource(i1);
    localObject1 = i;
    if (localObject1 == null)
    {
      localObject2 = "profilePictureImageView";
      k.a((String)localObject2);
    }
    i1 = 0;
    localObject2 = null;
    ((ImageView)localObject1).setImageDrawable(null);
    localObject1 = i;
    if (localObject1 == null)
    {
      localObject2 = "profilePictureImageView";
      k.a((String)localObject2);
    }
    i1 = R.drawable.ic_avatar_incallui_spam;
    ((ImageView)localObject1).setImageResource(i1);
    localObject1 = j;
    if (localObject1 == null)
    {
      localObject2 = "minimiseButton";
      k.a((String)localObject2);
    }
    i1 = R.drawable.background_incallui_minimise_spam_call;
    ((ImageButton)localObject1).setBackgroundResource(i1);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "profilePictureUrl");
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    String str = "activity ?: return";
    k.a(localObject, str);
    paramString = w.a((Context)localObject).a(paramString);
    int i1 = R.drawable.ic_avatar_incallui_default;
    paramString = paramString.a(i1);
    localObject = (ai)aq.d.b();
    paramString = paramString.a((ai)localObject);
    i1 = 1;
    c = i1;
    paramString = paramString.b();
    localObject = i;
    if (localObject == null)
    {
      str = "profilePictureImageView";
      k.a(str);
    }
    paramString.a((ImageView)localObject, null);
  }
  
  public final void c()
  {
    TextView localTextView = p;
    if (localTextView == null)
    {
      String str = "statusTextView";
      k.a(str);
    }
    t.c((View)localTextView);
  }
  
  public final void d()
  {
    ToggleButton localToggleButton = e;
    if (localToggleButton == null)
    {
      String str = "muteToggleButton";
      k.a(str);
    }
    localToggleButton.setChecked(true);
  }
  
  public final void e()
  {
    ToggleButton localToggleButton = e;
    if (localToggleButton == null)
    {
      String str = "muteToggleButton";
      k.a(str);
    }
    localToggleButton.setChecked(false);
  }
  
  public final void f()
  {
    ToggleButton localToggleButton = g;
    if (localToggleButton == null)
    {
      String str = "speakerToggleButton";
      k.a(str);
    }
    localToggleButton.setChecked(true);
  }
  
  public final void g()
  {
    ToggleButton localToggleButton = g;
    if (localToggleButton == null)
    {
      String str = "speakerToggleButton";
      k.a(str);
    }
    localToggleButton.setChecked(false);
  }
  
  public final void h()
  {
    ToggleButton localToggleButton = h;
    if (localToggleButton == null)
    {
      String str = "holdToggleButton";
      k.a(str);
    }
    localToggleButton.setEnabled(true);
  }
  
  public final void i()
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      localObject1 = ((f)localObject1).getSupportFragmentManager();
      if (localObject1 != null)
      {
        Object localObject2 = ((j)localObject1).a("KEYPAD_FRAGMENT_TAG");
        if (localObject2 == null)
        {
          localObject1 = ((j)localObject1).a();
          int i1 = R.id.view_keypad;
          localObject3 = com.truecaller.incallui.callui.b.a.a.b;
          localObject3 = new com/truecaller/incallui/callui/b/a/a;
          ((com.truecaller.incallui.callui.b.a.a)localObject3).<init>();
          localObject3 = (Fragment)localObject3;
          ((o)localObject1).a(i1, (Fragment)localObject3, "KEYPAD_FRAGMENT_TAG").a("KEYPAD_FRAGMENT_TAG").d();
          return;
        }
        localObject2 = ((j)localObject1).a();
        Object localObject3 = "KEYPAD_FRAGMENT_TAG";
        localObject1 = ((j)localObject1).a((String)localObject3);
        if (localObject1 != null)
        {
          ((o)localObject2).e((Fragment)localObject1).a("KEYPAD_FRAGMENT_TAG").d();
          return;
        }
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type android.support.v4.app.Fragment");
        throw ((Throwable)localObject1);
      }
    }
  }
  
  public final void j()
  {
    ToggleButton localToggleButton = h;
    if (localToggleButton == null)
    {
      String str = "holdToggleButton";
      k.a(str);
    }
    localToggleButton.setChecked(true);
  }
  
  public final void k()
  {
    ToggleButton localToggleButton = h;
    if (localToggleButton == null)
    {
      String str = "holdToggleButton";
      k.a(str);
    }
    localToggleButton.setChecked(false);
  }
  
  public final void l()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void m()
  {
    Chronometer localChronometer = o;
    String str;
    if (localChronometer == null)
    {
      str = "chronometerView";
      k.a(str);
    }
    t.c((View)localChronometer);
    localChronometer = o;
    if (localChronometer == null)
    {
      str = "chronometerView";
      k.a(str);
    }
    localChronometer.stop();
  }
  
  public final void n()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((f)localObject).getSupportFragmentManager();
      if (localObject != null)
      {
        int i1 = ((j)localObject).e();
        if (i1 == 0) {
          return;
        }
        ((j)localObject).b("KEYPAD_FRAGMENT_TAG");
        return;
      }
    }
  }
  
  public final void o()
  {
    MotionLayout localMotionLayout = c;
    String str;
    if (localMotionLayout == null)
    {
      str = "motionLayoutView";
      k.a(str);
    }
    int i1 = R.id.outgoing_incallui_ended_start_set;
    int i2 = R.id.outgoing_incallui_ended_end_set;
    localMotionLayout.a(i1, i2);
    localMotionLayout = c;
    if (localMotionLayout == null)
    {
      str = "motionLayoutView";
      k.a(str);
    }
    localMotionLayout.c();
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = i.a;
    i.a.a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i1 = R.layout.fragment_incallui_ongoing;
    paramLayoutInflater = paramLayoutInflater.inflate(i1, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…ngoing, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((b.a)localObject).y_();
    super.onDestroyView();
    localObject = q;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i1 = R.id.motion_layout;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.motion_layout)");
    paramBundle = (MotionLayout)paramBundle;
    c = paramBundle;
    i1 = R.id.button_end_call;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_end_call)");
    paramBundle = (FloatingActionButton)paramBundle;
    d = paramBundle;
    i1 = R.id.toggle_mute;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.toggle_mute)");
    paramBundle = (ToggleButton)paramBundle;
    e = paramBundle;
    i1 = R.id.button_keypad;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_keypad)");
    paramBundle = (Button)paramBundle;
    f = paramBundle;
    i1 = R.id.toggle_speaker;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.toggle_speaker)");
    paramBundle = (ToggleButton)paramBundle;
    g = paramBundle;
    i1 = R.id.toggle_hold;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.toggle_hold)");
    paramBundle = (ToggleButton)paramBundle;
    h = paramBundle;
    i1 = R.id.text_profile_name;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_profile_name)");
    paramBundle = (TextView)paramBundle;
    k = paramBundle;
    i1 = R.id.image_profile_picture;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.image_profile_picture)");
    paramBundle = (ImageView)paramBundle;
    i = paramBundle;
    i1 = R.id.button_minimise;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_minimise)");
    paramBundle = (ImageButton)paramBundle;
    j = paramBundle;
    i1 = R.id.image_logo;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.image_logo)");
    paramBundle = (ImageView)paramBundle;
    l = paramBundle;
    i1 = R.id.view_header_cover;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.view_header_cover)");
    m = paramBundle;
    i1 = R.id.view_header;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.view_header)");
    n = paramBundle;
    i1 = R.id.chronometer;
    paramBundle = paramView.findViewById(i1);
    String str1 = "view.findViewById(R.id.chronometer)";
    k.a(paramBundle, str1);
    paramBundle = (Chronometer)paramBundle;
    o = paramBundle;
    i1 = R.id.text_status;
    paramView = paramView.findViewById(i1);
    paramBundle = "view.findViewById(R.id.text_status)";
    k.a(paramView, paramBundle);
    paramView = (TextView)paramView;
    p = paramView;
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = getArguments();
    str1 = null;
    if (paramBundle != null)
    {
      String str2 = "ARGUMENT_CALLER_NUMBER";
      str1 = paramBundle.getString(str2, null);
    }
    paramView.a(str1);
    paramView = d;
    if (paramView == null)
    {
      paramBundle = "endCallButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/b/a$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = e;
    if (paramView == null)
    {
      paramBundle = "muteToggleButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/b/a$c;
    paramBundle.<init>(this);
    paramBundle = (CompoundButton.OnCheckedChangeListener)paramBundle;
    paramView.setOnCheckedChangeListener(paramBundle);
    paramView = f;
    if (paramView == null)
    {
      paramBundle = "keypadButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/b/a$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = g;
    if (paramView == null)
    {
      paramBundle = "speakerToggleButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/b/a$e;
    paramBundle.<init>(this);
    paramBundle = (CompoundButton.OnCheckedChangeListener)paramBundle;
    paramView.setOnCheckedChangeListener(paramBundle);
    paramView = h;
    if (paramView == null)
    {
      paramBundle = "holdToggleButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/b/a$f;
    paramBundle.<init>(this);
    paramBundle = (CompoundButton.OnCheckedChangeListener)paramBundle;
    paramView.setOnCheckedChangeListener(paramBundle);
    paramView = j;
    if (paramView == null)
    {
      paramBundle = "minimiseButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/b/a$g;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */