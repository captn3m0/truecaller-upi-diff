package com.truecaller.incallui.callui.b.a;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.incallui.R.anim;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.layout;
import com.truecaller.incallui.a.i;
import com.truecaller.incallui.a.i.a;
import java.util.HashMap;

public final class a
  extends Fragment
  implements c.b
{
  public static final a.a b;
  public c.a a;
  private ImageButton c;
  private FrameLayout d;
  private GridLayout e;
  private TextView f;
  private HashMap g;
  
  static
  {
    a.a locala = new com/truecaller/incallui/callui/b/a/a$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public final c.a a()
  {
    c.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "input");
    TextView localTextView = f;
    if (localTextView == null)
    {
      String str = "keypadInputText";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    localTextView.append(paramString);
  }
  
  public final void b()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((f)localObject).getSupportFragmentManager();
      if (localObject != null)
      {
        int i = ((j)localObject).e();
        if (i == 0) {
          return;
        }
        ((j)localObject).b("KEYPAD_FRAGMENT_TAG");
        return;
      }
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = i.a;
    i.a.a().a(this);
  }
  
  public final Animation onCreateAnimation(int paramInt1, boolean paramBoolean, int paramInt2)
  {
    if (paramBoolean)
    {
      localObject = (Context)getActivity();
      paramBoolean = R.anim.fast_slide_in_up;
      localObject = AnimationUtils.loadAnimation((Context)localObject, paramBoolean);
      k.a(localObject, "AnimationUtils.loadAnima… R.anim.fast_slide_in_up)");
      return (Animation)localObject;
    }
    Object localObject = (Context)getActivity();
    paramBoolean = R.anim.fast_slide_out_down;
    localObject = AnimationUtils.loadAnimation((Context)localObject, paramBoolean);
    k.a(localObject, "AnimationUtils.loadAnima…anim.fast_slide_out_down)");
    return (Animation)localObject;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_keypad;
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…keypad, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((c.a)localObject).y_();
    super.onDestroyView();
    localObject = g;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.button_close;
    paramBundle = paramView.findViewById(i);
    k.a(paramBundle, "view.findViewById(R.id.button_close)");
    paramBundle = (ImageButton)paramBundle;
    c = paramBundle;
    i = R.id.view_outside_area;
    paramBundle = paramView.findViewById(i);
    k.a(paramBundle, "view.findViewById(R.id.view_outside_area)");
    paramBundle = (FrameLayout)paramBundle;
    d = paramBundle;
    i = R.id.grid_keypad;
    paramBundle = paramView.findViewById(i);
    Object localObject1 = "view.findViewById(R.id.grid_keypad)";
    k.a(paramBundle, (String)localObject1);
    paramBundle = (GridLayout)paramBundle;
    e = paramBundle;
    i = R.id.text_keypad_input;
    paramView = paramView.findViewById(i);
    paramBundle = "view.findViewById(R.id.text_keypad_input)";
    k.a(paramView, paramBundle);
    paramView = (TextView)paramView;
    f = paramView;
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = c;
    if (paramView == null)
    {
      paramBundle = "closeButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/b/a/a$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = d;
    if (paramView == null)
    {
      paramBundle = "outsideAreaView";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/b/a/a$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    int j = 0;
    paramView = null;
    paramBundle = e;
    if (paramBundle == null)
    {
      localObject1 = "keypadView";
      k.a((String)localObject1);
    }
    i = paramBundle.getChildCount();
    while (j < i)
    {
      localObject1 = e;
      if (localObject1 == null)
      {
        localObject2 = "keypadView";
        k.a((String)localObject2);
      }
      localObject1 = ((GridLayout)localObject1).getChildAt(j);
      Object localObject2 = new com/truecaller/incallui/callui/b/a/a$d;
      ((a.d)localObject2).<init>(this, j);
      localObject2 = (View.OnClickListener)localObject2;
      ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.b.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */