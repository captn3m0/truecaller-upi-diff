package com.truecaller.incallui.callui;

import c.g.b.k;
import com.truecaller.common.tag.c;

public final class a
{
  public final String a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  public final c f;
  public final Integer g;
  
  public a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, c paramc, Integer paramInteger)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramString4;
    e = paramString5;
    f = paramc;
    g = paramInteger;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = c;
            localObject2 = c;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = d;
              localObject2 = d;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = e;
                localObject2 = e;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = f;
                  localObject2 = f;
                  bool1 = k.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = g;
                    paramObject = g;
                    boolean bool2 = k.a(localObject1, paramObject);
                    if (bool2) {
                      break label178;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label178:
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = b;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = d;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = e;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = f;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = g;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CallerInfo(profileName=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", profilePictureUrl=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", phoneNumberForDisplay=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", shortFormattedAddress=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", carrier=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", tag=");
    localObject = f;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", spamScore=");
    localObject = g;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */