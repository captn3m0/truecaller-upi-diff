package com.truecaller.incallui.callui;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.utils.extensions.j;
import kotlinx.coroutines.a.h;

public final class c
  extends ba
  implements b.a
{
  private final com.truecaller.incallui.service.c c;
  private final f d;
  
  public c(com.truecaller.incallui.service.c paramc, f paramf)
  {
    super(paramf);
    c = paramc;
    d = paramf;
  }
  
  public final void O_()
  {
    c.h();
    b.b localb = (b.b)b;
    if (localb != null) {
      localb.a();
    }
    localb = (b.b)b;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void P_()
  {
    c.h();
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void a()
  {
    c.g();
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.a();
      return;
    }
  }
  
  public final void a(int paramInt)
  {
    if (paramInt > 0)
    {
      localb = (b.b)b;
      if (localb != null) {
        localb.b();
      }
      return;
    }
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "number");
    h localh = c.a();
    if (localh == null) {
      return;
    }
    Object localObject = new com/truecaller/incallui/callui/c$a;
    ((c.a)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    j.a(this, localh, (m)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */