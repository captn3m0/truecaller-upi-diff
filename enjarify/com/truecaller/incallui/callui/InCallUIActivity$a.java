package com.truecaller.incallui.callui;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.telecom.Call;
import android.telecom.Call.Details;
import c.g.b.k;

public final class InCallUIActivity$a
{
  public static Intent a(Context paramContext, Call paramCall)
  {
    k.b(paramContext, "context");
    Object localObject = "call";
    k.b(paramCall, (String)localObject);
    int i = Build.VERSION.SDK_INT;
    int j = 23;
    if (i < j) {
      return null;
    }
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>(paramContext, InCallUIActivity.class);
    paramContext = ((Intent)localObject).setFlags(268435456).addFlags(262144);
    paramCall = paramCall.getDetails();
    k.a(paramCall, "call.details");
    paramCall = paramCall.getHandle();
    return paramContext.setData(paramCall);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.InCallUIActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */