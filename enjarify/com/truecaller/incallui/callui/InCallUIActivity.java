package com.truecaller.incallui.callui;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v4.content.b;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import c.g.b.k;
import c.u;
import com.truecaller.incallui.R.color;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.layout;
import com.truecaller.incallui.a.i.a;

public final class InCallUIActivity
  extends AppCompatActivity
  implements b.b
{
  public static final InCallUIActivity.a b;
  public b.a a;
  
  static
  {
    InCallUIActivity.a locala = new com/truecaller/incallui/callui/InCallUIActivity$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private final void a(Intent paramIntent)
  {
    boolean bool;
    if (paramIntent != null)
    {
      paramIntent = paramIntent.getAction();
    }
    else
    {
      bool = false;
      paramIntent = null;
    }
    if (paramIntent == null) {
      return;
    }
    int i = paramIntent.hashCode();
    int j = 361822499;
    String str;
    if (i != j)
    {
      j = 1285183569;
      if (i != j)
      {
        j = 1965691843;
        if (i == j)
        {
          str = "com.truecaller.incallui.callui.ACTION_DECLINE_CALL";
          bool = paramIntent.equals(str);
          if (bool)
          {
            paramIntent = a;
            if (paramIntent == null)
            {
              str = "presenter";
              k.a(str);
            }
            paramIntent.O_();
          }
        }
      }
      else
      {
        str = "com.truecaller.incallui.callui.ACTION_HANG_UP_CALL";
        bool = paramIntent.equals(str);
        if (bool)
        {
          paramIntent = a;
          if (paramIntent == null)
          {
            str = "presenter";
            k.a(str);
          }
          paramIntent.P_();
        }
      }
    }
    else
    {
      str = "com.truecaller.incallui.callui.ACTION_ANSWER_CALL";
      bool = paramIntent.equals(str);
      if (bool)
      {
        paramIntent = a;
        if (paramIntent == null)
        {
          str = "presenter";
          k.a(str);
        }
        paramIntent.a();
        return;
      }
    }
  }
  
  public final void a()
  {
    NotificationManager localNotificationManager = com.truecaller.utils.extensions.i.f(this);
    int i = R.id.incallui_service_incoming_call_notification;
    localNotificationManager.cancel(i);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "number");
    o localo = getSupportFragmentManager().a();
    Object localObject = com.truecaller.incallui.callui.a.a.b;
    localObject = new com/truecaller/incallui/callui/a/a;
    ((com.truecaller.incallui.callui.a.a)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("ARGUMENT_CALLER_NUMBER", paramString);
    ((com.truecaller.incallui.callui.a.a)localObject).setArguments(localBundle);
    localObject = (Fragment)localObject;
    localo.b(16908290, (Fragment)localObject).d();
  }
  
  public final void b()
  {
    getSupportFragmentManager().c();
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "number");
    Object localObject1 = getSupportFragmentManager();
    String str = "OUTGOING_CALL_FRAGMENT_TAG";
    localObject1 = ((j)localObject1).a(str);
    if (localObject1 == null)
    {
      localObject1 = getSupportFragmentManager().a();
      Object localObject2 = com.truecaller.incallui.callui.b.a.b;
      localObject2 = new com/truecaller/incallui/callui/b/a;
      ((com.truecaller.incallui.callui.b.a)localObject2).<init>();
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      localBundle.putString("ARGUMENT_CALLER_NUMBER", paramString);
      ((com.truecaller.incallui.callui.b.a)localObject2).setArguments(localBundle);
      localObject2 = (Fragment)localObject2;
      ((o)localObject1).a(16908290, (Fragment)localObject2, "OUTGOING_CALL_FRAGMENT_TAG").d();
      return;
    }
    paramString = getSupportFragmentManager().a();
    localObject1 = getSupportFragmentManager();
    str = "OUTGOING_CALL_FRAGMENT_TAG";
    localObject1 = ((j)localObject1).a(str);
    if (localObject1 != null)
    {
      paramString.e((Fragment)localObject1).d();
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type android.support.v4.app.Fragment");
    throw paramString;
  }
  
  public final void c()
  {
    finish();
  }
  
  public final void onBackPressed()
  {
    b.a locala = a;
    if (locala == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    Object localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    int i = ((j)localObject).e();
    locala.a(i);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.layout.activity_incallui;
    setContentView(i);
    com.truecaller.utils.extensions.a.a(this, true);
    paramBundle = com.truecaller.incallui.a.i.a;
    paramBundle = i.a.a();
    paramBundle.a(this);
    i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      paramBundle = getWindow();
      k.a(paramBundle, "window");
      localObject = this;
      localObject = (Context)this;
      int k = R.color.incallui_status_bar_color;
      j = b.c((Context)localObject, k);
      paramBundle.setStatusBarColor(j);
    }
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    paramBundle.a(this);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    Object localObject = getIntent();
    if (localObject != null)
    {
      localObject = ((Intent)localObject).getData();
      if (localObject != null)
      {
        localObject = ((Uri)localObject).getSchemeSpecificPart();
        if (localObject != null)
        {
          paramBundle.a((String)localObject);
          paramBundle = getIntent();
          a(paramBundle);
          return;
        }
      }
    }
  }
  
  public final void onDestroy()
  {
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.y_();
    super.onDestroy();
  }
  
  public final void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    a(paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.InCallUIActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */