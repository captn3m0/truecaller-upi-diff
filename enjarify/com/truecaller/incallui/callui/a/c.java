package com.truecaller.incallui.callui.a;

import c.d.f;
import c.g.a.m;
import com.truecaller.ba;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.string;
import com.truecaller.incallui.a.b;
import com.truecaller.incallui.a.n;
import com.truecaller.incallui.a.o;
import com.truecaller.utils.extensions.j;
import kotlinx.coroutines.e;

public final class c
  extends ba
  implements b.a
{
  private final com.truecaller.incallui.service.c c;
  private final o d;
  private final n e;
  private final b f;
  private final f g;
  
  public c(com.truecaller.incallui.service.c paramc, o paramo, n paramn, b paramb, f paramf)
  {
    super(paramf);
    c = paramc;
    d = paramo;
    e = paramn;
    f = paramb;
    g = paramf;
  }
  
  public final void Q_()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.d();
      return;
    }
  }
  
  public final void R_()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.e();
      return;
    }
  }
  
  public final void a()
  {
    c.h();
  }
  
  public final void a(float paramFloat, int paramInt1, int paramInt2, int paramInt3)
  {
    double d1 = paramFloat;
    double d2 = 0.95D;
    boolean bool = d1 < d2;
    Object localObject;
    if (bool)
    {
      i = R.id.incoming_incallui_answer_end_set;
      if (paramInt1 != i)
      {
        i = 0;
        paramFloat = 0.0F;
        localObject = null;
        break label52;
      }
    }
    int i = 1;
    paramFloat = Float.MIN_VALUE;
    label52:
    paramInt1 = R.id.incoming_incallui_answer_start_set;
    if (paramInt2 == paramInt1)
    {
      paramInt1 = R.id.incoming_incallui_answer_end_set;
      if ((paramInt3 == paramInt1) && (i != 0))
      {
        localObject = new com/truecaller/incallui/callui/a/c$a;
        paramInt1 = 0;
        ((c.a)localObject).<init>(this, null);
        localObject = (m)localObject;
        paramInt2 = 3;
        e.b(this, null, (m)localObject, paramInt2);
      }
    }
  }
  
  public final void a(String paramString)
  {
    if (paramString != null)
    {
      localObject = (b.b)b;
      if (localObject != null) {
        ((b.b)localObject).a(paramString);
      }
    }
    paramString = c.b();
    if (paramString == null) {
      return;
    }
    Object localObject = new com/truecaller/incallui/callui/a/c$c;
    ((c.c)localObject).<init>(this, null);
    localObject = (m)localObject;
    j.a(this, paramString, (m)localObject);
  }
  
  public final void a(String paramString, int paramInt)
  {
    if (paramString == null) {
      return;
    }
    int i = R.string.incallui_reject_message_custom_option;
    Integer localInteger;
    if (paramInt != i)
    {
      localInteger = Integer.valueOf(paramInt);
    }
    else
    {
      paramInt = 0;
      localInteger = null;
    }
    e.a(paramString, localInteger);
    c.h();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */