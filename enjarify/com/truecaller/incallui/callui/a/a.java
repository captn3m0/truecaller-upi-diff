package com.truecaller.incallui.callui.a;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.ColorStateList;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.constraint.motion.MotionLayout;
import android.support.constraint.motion.MotionLayout.d;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.content.b;
import android.support.v4.view.r;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.common.tag.TagView;
import com.truecaller.common.tag.c;
import com.truecaller.incallui.R.anim;
import com.truecaller.incallui.R.array;
import com.truecaller.incallui.R.color;
import com.truecaller.incallui.R.drawable;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.layout;
import com.truecaller.incallui.R.string;
import com.truecaller.incallui.a.i;
import com.truecaller.incallui.a.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class a
  extends Fragment
  implements b.b
{
  public static final a.a b;
  public b.a a;
  private MotionLayout c;
  private FloatingActionButton d;
  private FloatingActionButton e;
  private FloatingActionButton f;
  private TextView g;
  private ImageView h;
  private TextView i;
  private TagView j;
  private ImageButton k;
  private View l;
  private ImageView m;
  private View n;
  private View o;
  private TextView p;
  private TextView q;
  private String r;
  private HashMap s;
  
  static
  {
    a.a locala = new com/truecaller/incallui/callui/a/a$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public final b.a a()
  {
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(int paramInt)
  {
    ImageView localImageView = m;
    if (localImageView == null)
    {
      String str = "truecallerLogoView";
      k.a(str);
    }
    localImageView.setImageResource(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    String str1 = "activity ?: return";
    k.a(localObject, str1);
    paramInt2 = b.c((Context)localObject, paramInt2);
    localObject = q;
    if (localObject == null)
    {
      str1 = "statusTextView";
      k.a(str1);
    }
    ((TextView)localObject).setText(paramInt1);
    TextView localTextView = q;
    if (localTextView == null)
    {
      localObject = "statusTextView";
      k.a((String)localObject);
    }
    localTextView.setTextColor(paramInt2);
    localTextView = q;
    if (localTextView == null)
    {
      String str2 = "statusTextView";
      k.a(str2);
    }
    t.a((View)localTextView);
  }
  
  public final void a(c paramc)
  {
    k.b(paramc, "tag");
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    Object localObject2 = j;
    if (localObject2 == null)
    {
      String str = "tagView";
      k.a(str);
    }
    ((TagView)localObject2).setTag(paramc);
    paramc = j;
    if (paramc == null)
    {
      localObject2 = "tagView";
      k.a((String)localObject2);
    }
    localObject1 = (Context)localObject1;
    int i1 = R.color.incallui_action_background_color;
    int i2 = b.c((Context)localObject1, i1);
    paramc.setBackgroundColor(i2);
    paramc = j;
    if (paramc == null)
    {
      localObject1 = "tagView";
      k.a((String)localObject1);
    }
    t.a((View)paramc);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "profileName");
    Object localObject = g;
    if (localObject == null)
    {
      String str = "profileNameTextView";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    ((TextView)localObject).setText(paramString);
    paramString = g;
    if (paramString == null)
    {
      localObject = "profileNameTextView";
      k.a((String)localObject);
    }
    paramString.setSelected(true);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "phoneNumberForDisplay");
    k.b(paramString3, "shortFormattedAddress");
    TextView localTextView = i;
    if (localTextView == null)
    {
      localObject = "aboutTextView";
      k.a((String)localObject);
    }
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    ((StringBuilder)localObject).append(paramString1);
    if (paramString2 != null)
    {
      paramString1 = " · ";
      ((StringBuilder)localObject).append(paramString1);
      ((StringBuilder)localObject).append(paramString2);
    }
    paramString1 = paramString3;
    paramString1 = (CharSequence)paramString3;
    boolean bool = c.n.m.a(paramString1) ^ true;
    if (bool)
    {
      paramString1 = "\n";
      ((StringBuilder)localObject).append(paramString1);
      ((StringBuilder)localObject).append(paramString3);
    }
    paramString1 = (CharSequence)((StringBuilder)localObject).toString();
    localTextView.setText(paramString1);
  }
  
  public final void b()
  {
    TagView localTagView = j;
    if (localTagView == null)
    {
      String str = "tagView";
      k.a(str);
    }
    t.b((View)localTagView);
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = "activity ?: return";
    k.a(localObject1, (String)localObject2);
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 21;
    if (i1 >= i2)
    {
      localObject2 = ((f)localObject1).getWindow();
      k.a(localObject2, "context.window");
      localObject3 = localObject1;
      localObject3 = (Context)localObject1;
      i3 = R.color.incallui_status_bar_spam_color;
      i2 = b.c((Context)localObject3, i3);
      ((Window)localObject2).setStatusBarColor(i2);
    }
    localObject2 = n;
    if (localObject2 == null)
    {
      localObject3 = "headerCoverView";
      k.a((String)localObject3);
    }
    localObject1 = (Context)localObject1;
    i2 = R.color.incallui_spam_color;
    int i4 = b.c((Context)localObject1, i2);
    ((View)localObject2).setBackgroundColor(i4);
    localObject1 = o;
    if (localObject1 == null)
    {
      localObject2 = "headerArcView";
      k.a((String)localObject2);
    }
    i1 = R.drawable.background_incallui_spam_header_view;
    ((View)localObject1).setBackgroundResource(i1);
    localObject1 = h;
    if (localObject1 == null)
    {
      localObject2 = "profilePictureImageView";
      k.a((String)localObject2);
    }
    i1 = 0;
    localObject2 = null;
    ((ImageView)localObject1).setImageDrawable(null);
    localObject1 = h;
    if (localObject1 == null)
    {
      localObject2 = "profilePictureImageView";
      k.a((String)localObject2);
    }
    i1 = R.drawable.ic_avatar_incallui_spam;
    ((ImageView)localObject1).setImageResource(i1);
    localObject1 = k;
    if (localObject1 == null)
    {
      localObject2 = "minimiseButton";
      k.a((String)localObject2);
    }
    i1 = R.drawable.background_incallui_minimise_spam_call;
    ((ImageButton)localObject1).setBackgroundResource(i1);
    localObject1 = p;
    if (localObject1 == null)
    {
      localObject2 = "spamScoreTextView";
      k.a((String)localObject2);
    }
    i1 = R.string.incallui_spam_reports_score;
    i2 = 1;
    Object localObject3 = new Object[i2];
    int i3 = 0;
    Object localObject4 = Integer.valueOf(paramInt);
    localObject3[0] = localObject4;
    localObject4 = (CharSequence)getString(i1, (Object[])localObject3);
    ((TextView)localObject1).setText((CharSequence)localObject4);
    localObject4 = p;
    if (localObject4 == null)
    {
      localObject1 = "spamScoreTextView";
      k.a((String)localObject1);
    }
    t.a((View)localObject4);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "profilePictureUrl");
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    String str = "activity ?: return";
    k.a(localObject, str);
    paramString = w.a((Context)localObject).a(paramString);
    int i1 = R.drawable.ic_avatar_incallui_default;
    paramString = paramString.a(i1);
    localObject = (ai)aq.d.b();
    paramString = paramString.a((ai)localObject);
    i1 = 1;
    c = i1;
    paramString = paramString.b();
    localObject = h;
    if (localObject == null)
    {
      str = "profilePictureImageView";
      k.a(str);
    }
    paramString.a((ImageView)localObject, null);
  }
  
  public final void c()
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    Object localObject2 = d;
    if (localObject2 == null)
    {
      String str = "acceptCallButton";
      k.a(str);
    }
    localObject2 = (View)localObject2;
    localObject1 = (Context)localObject1;
    int i1 = R.color.incallui_action_end_call_background_color;
    int i2 = b.c((Context)localObject1, i1);
    localObject1 = ColorStateList.valueOf(i2);
    r.a((View)localObject2, (ColorStateList)localObject1);
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject2 = "acceptCallButton";
      k.a((String)localObject2);
    }
    int i3 = R.drawable.ic_button_incallui_hangup;
    ((FloatingActionButton)localObject1).setImageResource(i3);
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "motionLayoutView";
      k.a((String)localObject2);
    }
    ((MotionLayout)localObject1).c();
    localObject2 = null;
    ((MotionLayout)localObject1).setTransitionListener(null);
    i3 = R.id.incoming_incallui_accepted_start_set;
    i1 = R.id.incoming_incallui_accepted_end_set;
    ((MotionLayout)localObject1).a(i3, i1);
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "motionLayoutView";
      k.a((String)localObject2);
    }
    ((MotionLayout)localObject1).c();
  }
  
  public final void d()
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    Object localObject2 = new Integer[4];
    Object localObject3 = Integer.valueOf(R.string.incallui_reject_message_first_option);
    localObject2[0] = localObject3;
    localObject3 = Integer.valueOf(R.string.incallui_reject_message_second_option);
    localObject2[1] = localObject3;
    localObject3 = Integer.valueOf(R.string.incallui_reject_message_third_option);
    localObject2[2] = localObject3;
    localObject3 = Integer.valueOf(R.string.incallui_reject_message_custom_option);
    localObject2[3] = localObject3;
    localObject2 = c.a.m.b((Object[])localObject2);
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    localObject1 = (Context)localObject1;
    localBuilder.<init>((Context)localObject1);
    int i1 = R.array.incallui_button_message_options;
    localObject3 = new com/truecaller/incallui/callui/a/a$f;
    ((a.f)localObject3).<init>(this, (List)localObject2);
    localObject3 = (DialogInterface.OnClickListener)localObject3;
    localBuilder.setItems(i1, (DialogInterface.OnClickListener)localObject3).show();
  }
  
  public final void e()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void f()
  {
    MotionLayout localMotionLayout = c;
    String str;
    if (localMotionLayout == null)
    {
      str = "motionLayoutView";
      k.a(str);
    }
    int i1 = R.id.incoming_incallui_ended_start_set;
    int i2 = R.id.incoming_incallui_ended_end_set;
    localMotionLayout.a(i1, i2);
    localMotionLayout = c;
    if (localMotionLayout == null)
    {
      str = "motionLayoutView";
      k.a(str);
    }
    localMotionLayout.c();
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = i.a;
    i.a.a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i1 = R.layout.fragment_incallui_incoming;
    paramLayoutInflater = paramLayoutInflater.inflate(i1, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…coming, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((b.a)localObject).y_();
    super.onDestroyView();
    localObject = s;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i1 = R.id.motion_layout;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.motion_layout)");
    paramBundle = (MotionLayout)paramBundle;
    c = paramBundle;
    i1 = R.id.button_accept_call;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_accept_call)");
    paramBundle = (FloatingActionButton)paramBundle;
    d = paramBundle;
    i1 = R.id.button_reject_call;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_reject_call)");
    paramBundle = (FloatingActionButton)paramBundle;
    e = paramBundle;
    i1 = R.id.button_message;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_message)");
    paramBundle = (FloatingActionButton)paramBundle;
    f = paramBundle;
    i1 = R.id.text_profile_name;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_profile_name)");
    paramBundle = (TextView)paramBundle;
    g = paramBundle;
    i1 = R.id.image_profile_picture;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.image_profile_picture)");
    paramBundle = (ImageView)paramBundle;
    h = paramBundle;
    i1 = R.id.text_about;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.text_about)");
    paramBundle = (TextView)paramBundle;
    i = paramBundle;
    i1 = R.id.tag_view;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.tag_view)");
    paramBundle = (TagView)paramBundle;
    j = paramBundle;
    i1 = R.id.button_minimise;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.button_minimise)");
    paramBundle = (ImageButton)paramBundle;
    k = paramBundle;
    i1 = R.id.view_answer_arrows;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.view_answer_arrows)");
    l = paramBundle;
    i1 = R.id.image_logo;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.image_logo)");
    paramBundle = (ImageView)paramBundle;
    m = paramBundle;
    i1 = R.id.view_header_cover;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.view_header_cover)");
    n = paramBundle;
    i1 = R.id.view_header;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.view_header)");
    o = paramBundle;
    i1 = R.id.text_spam_score;
    paramBundle = paramView.findViewById(i1);
    String str = "view.findViewById(R.id.text_spam_score)";
    k.a(paramBundle, str);
    paramBundle = (TextView)paramBundle;
    p = paramBundle;
    i1 = R.id.text_status;
    paramView = paramView.findViewById(i1);
    paramBundle = "view.findViewById(R.id.text_status)";
    k.a(paramView, paramBundle);
    paramView = (TextView)paramView;
    q = paramView;
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = getArguments();
    i1 = 0;
    paramBundle = null;
    if (paramView != null)
    {
      str = "ARGUMENT_CALLER_NUMBER";
      paramBundle = paramView.getString(str, null);
    }
    r = paramBundle;
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = r;
    paramView.a(paramBundle);
    paramView = l;
    if (paramView == null)
    {
      paramBundle = "answerArrowsView";
      k.a(paramBundle);
    }
    paramBundle = requireContext();
    int i2 = R.anim.anim_incallui_answer_arrows;
    paramBundle = AnimationUtils.loadAnimation(paramBundle, i2);
    paramView.startAnimation(paramBundle);
    paramView = e;
    if (paramView == null)
    {
      paramBundle = "rejectCallButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/a/a$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = f;
    if (paramView == null)
    {
      paramBundle = "rejectMessageButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/a/a$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = k;
    if (paramView == null)
    {
      paramBundle = "minimiseButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/a/a$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = c;
    if (paramView == null)
    {
      paramBundle = "motionLayoutView";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/incallui/callui/a/a$e;
    paramBundle.<init>(this);
    paramBundle = (MotionLayout.d)paramBundle;
    paramView.setTransitionListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */