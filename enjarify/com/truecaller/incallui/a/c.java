package com.truecaller.incallui.a;

import android.content.Context;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build.VERSION;
import android.os.VibrationEffect;
import android.os.Vibrator;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.i;

public final class c
  implements b
{
  private final ToneGenerator a;
  private final Vibrator b;
  private final AudioManager c;
  
  public c(Context paramContext)
  {
    Vibrator localVibrator = i.h(paramContext);
    b = localVibrator;
    paramContext = i.i(paramContext);
    c = paramContext;
    try
    {
      paramContext = new android/media/ToneGenerator;
      localVibrator = null;
      int i = 100;
      paramContext.<init>(0, i);
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeException);
      paramContext = null;
    }
    a = paramContext;
  }
  
  public final void a()
  {
    Object localObject = b;
    boolean bool = ((Vibrator)localObject).hasVibrator();
    if (!bool) {
      return;
    }
    localObject = c;
    int i = ((AudioManager)localObject).getRingerMode();
    if (i == 0) {
      return;
    }
    i = Build.VERSION.SDK_INT;
    int j = 26;
    long l = 400L;
    if (i >= j)
    {
      localObject = b;
      VibrationEffect localVibrationEffect = VibrationEffect.createOneShot(l, -1);
      ((Vibrator)localObject).vibrate(localVibrationEffect);
      return;
    }
    b.vibrate(l);
  }
  
  public final void a(int paramInt)
  {
    int i = 7;
    Object localObject;
    if (paramInt != i)
    {
      i = 9;
      if (paramInt != i)
      {
        paramInt = 0;
        localObject = null;
      }
      else
      {
        paramInt = 35;
        localObject = Integer.valueOf(paramInt);
      }
    }
    else
    {
      paramInt = 96;
      localObject = Integer.valueOf(paramInt);
    }
    if (localObject == null)
    {
      localObject = a;
      if (localObject != null) {
        ((ToneGenerator)localObject).stopTone();
      }
      return;
    }
    ToneGenerator localToneGenerator = a;
    if (localToneGenerator != null)
    {
      paramInt = ((Integer)localObject).intValue();
      localToneGenerator.startTone(paramInt, 200);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */