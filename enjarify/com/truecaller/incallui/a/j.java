package com.truecaller.incallui.a;

import com.truecaller.incallui.a;
import com.truecaller.incallui.a.a;

public abstract class j
{
  public static final j.a a;
  
  static
  {
    j.a locala = new com/truecaller/incallui/a/j$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final g a()
  {
    Object localObject = a.a;
    localObject = a.a.a();
    if (localObject != null) {
      return (g)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Caller info provider should be set");
    throw ((Throwable)localObject);
  }
  
  public static final o b()
  {
    Object localObject = a.a;
    localObject = a.a.b();
    if (localObject != null) {
      return (o)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Logo helper should be set");
    throw ((Throwable)localObject);
  }
  
  public static final n c()
  {
    Object localObject = a.a;
    localObject = a.a.c();
    if (localObject != null) {
      return (n)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Reject with message helper should be set");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */