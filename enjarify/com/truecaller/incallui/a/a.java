package com.truecaller.incallui.a;

import android.content.Context;
import com.truecaller.incallui.callui.InCallUIActivity;
import com.truecaller.incallui.service.InCallUIService;
import com.truecaller.incallui.service.e;
import com.truecaller.utils.t;
import javax.inject.Provider;

public final class a
  implements r
{
  private final t b;
  private final com.truecaller.common.a c;
  private final com.truecaller.notificationchannels.n d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  
  private a(com.truecaller.common.a parama, com.truecaller.notificationchannels.n paramn, t paramt)
  {
    b = paramt;
    c = parama;
    d = paramn;
    paramn = dagger.a.c.a(e.a());
    e = paramn;
    paramn = new com/truecaller/incallui/a/a$b;
    paramn.<init>(parama);
    f = paramn;
    paramn = f.a(f);
    g = paramn;
    paramn = dagger.a.c.a(g);
    h = paramn;
    paramn = new com/truecaller/incallui/a/a$d;
    paramn.<init>(parama);
    i = paramn;
    paramn = new com/truecaller/incallui/a/a$c;
    paramn.<init>(parama);
    j = paramn;
    parama = i;
    paramn = j;
    parama = com.truecaller.incallui.d.a(parama, paramn);
    k = parama;
    parama = dagger.a.c.a(k);
    l = parama;
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/incallui/a/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  private c c()
  {
    c localc = new com/truecaller/incallui/a/c;
    Context localContext = (Context)dagger.a.g.a(c.b(), "Cannot return null from a non-@Nullable component method");
    localc.<init>(localContext);
    return localc;
  }
  
  public final void a(InCallUIActivity paramInCallUIActivity)
  {
    com.truecaller.incallui.callui.c localc = new com/truecaller/incallui/callui/c;
    com.truecaller.incallui.service.c localc1 = (com.truecaller.incallui.service.c)e.get();
    c.d.f localf = (c.d.f)dagger.a.g.a(c.r(), "Cannot return null from a non-@Nullable component method");
    localc.<init>(localc1, localf);
    a = localc;
  }
  
  public final void a(com.truecaller.incallui.callui.a.a parama)
  {
    com.truecaller.incallui.callui.a.c localc = new com/truecaller/incallui/callui/a/c;
    Object localObject1 = e.get();
    Object localObject2 = localObject1;
    localObject2 = (com.truecaller.incallui.service.c)localObject1;
    o localo = m.a();
    n localn = l.a();
    c localc1 = c();
    localObject1 = dagger.a.g.a(c.r(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject1;
    localObject3 = (c.d.f)localObject1;
    localObject1 = localc;
    localc.<init>((com.truecaller.incallui.service.c)localObject2, localo, localn, localc1, (c.d.f)localObject3);
    a = localc;
  }
  
  public final void a(com.truecaller.incallui.callui.b.a.a parama)
  {
    com.truecaller.incallui.callui.b.a.d locald = new com/truecaller/incallui/callui/b/a/d;
    com.truecaller.incallui.service.c localc = (com.truecaller.incallui.service.c)e.get();
    q localq = new com/truecaller/incallui/a/q;
    localq.<init>();
    locald.<init>(localc, localq);
    a = locald;
  }
  
  public final void a(com.truecaller.incallui.callui.b.a parama)
  {
    com.truecaller.incallui.callui.b.c localc = new com/truecaller/incallui/callui/b/c;
    Object localObject1 = e.get();
    Object localObject2 = localObject1;
    localObject2 = (com.truecaller.incallui.service.c)localObject1;
    o localo = m.a();
    localObject1 = dagger.a.g.a(b.d(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject1;
    localObject3 = (com.truecaller.utils.a)localObject1;
    c localc1 = c();
    localObject1 = dagger.a.g.a(c.r(), "Cannot return null from a non-@Nullable component method");
    Object localObject4 = localObject1;
    localObject4 = (c.d.f)localObject1;
    localObject1 = localc;
    localc.<init>((com.truecaller.incallui.service.c)localObject2, localo, (com.truecaller.utils.a)localObject3, localc1, (c.d.f)localObject4);
    a = localc;
  }
  
  public final void a(InCallUIService paramInCallUIService)
  {
    com.truecaller.incallui.service.b localb = new com/truecaller/incallui/service/b;
    Object localObject1 = e.get();
    Object localObject2 = localObject1;
    localObject2 = (com.truecaller.incallui.service.c)localObject1;
    g localg = k.a();
    localObject1 = h.get();
    Object localObject3 = localObject1;
    localObject3 = (d)localObject1;
    localObject1 = dagger.a.g.a(b.d(), "Cannot return null from a non-@Nullable component method");
    Object localObject4 = localObject1;
    localObject4 = (com.truecaller.utils.a)localObject1;
    localObject1 = dagger.a.g.a(c.r(), "Cannot return null from a non-@Nullable component method");
    Object localObject5 = localObject1;
    localObject5 = (c.d.f)localObject1;
    localObject1 = dagger.a.g.a(c.t(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject1;
    localObject6 = (c.d.f)localObject1;
    localObject1 = localb;
    localb.<init>((com.truecaller.incallui.service.c)localObject2, localg, (d)localObject3, (com.truecaller.utils.a)localObject4, (c.d.f)localObject5, (c.d.f)localObject6);
    a = localb;
    localObject1 = (com.truecaller.notificationchannels.b)dagger.a.g.a(d.d(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.notificationchannels.b)localObject1);
  }
  
  public final com.truecaller.incallui.a b()
  {
    return (com.truecaller.incallui.a)l.get();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */