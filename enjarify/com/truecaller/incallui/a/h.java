package com.truecaller.incallui.a;

import c.g.b.k;
import com.truecaller.common.tag.c;

public final class h
{
  public final String a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  public final c f;
  public final Integer g;
  private final boolean h;
  private final boolean i;
  
  public h(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, c paramc, boolean paramBoolean1, Integer paramInteger, boolean paramBoolean2)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramString4;
    e = paramString5;
    f = paramc;
    h = paramBoolean1;
    g = paramInteger;
    i = paramBoolean2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof h;
      if (bool2)
      {
        paramObject = (h)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              localObject1 = d;
              localObject2 = d;
              bool2 = k.a(localObject1, localObject2);
              if (bool2)
              {
                localObject1 = e;
                localObject2 = e;
                bool2 = k.a(localObject1, localObject2);
                if (bool2)
                {
                  localObject1 = f;
                  localObject2 = f;
                  bool2 = k.a(localObject1, localObject2);
                  if (bool2)
                  {
                    bool2 = h;
                    boolean bool3 = h;
                    if (bool2 == bool3)
                    {
                      bool2 = true;
                    }
                    else
                    {
                      bool2 = false;
                      localObject1 = null;
                    }
                    if (bool2)
                    {
                      localObject1 = g;
                      localObject2 = g;
                      bool2 = k.a(localObject1, localObject2);
                      if (bool2)
                      {
                        bool2 = i;
                        boolean bool4 = i;
                        if (bool2 == bool4)
                        {
                          bool4 = true;
                        }
                        else
                        {
                          bool4 = false;
                          paramObject = null;
                        }
                        if (bool4) {
                          return bool1;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int j = 0;
    if (str != null)
    {
      m = str.hashCode();
    }
    else
    {
      m = 0;
      str = null;
    }
    m *= 31;
    Object localObject = b;
    int n;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    int m = (m + n) * 31;
    localObject = c;
    int i1;
    if (localObject != null)
    {
      i1 = localObject.hashCode();
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    m = (m + i1) * 31;
    localObject = d;
    int i2;
    if (localObject != null)
    {
      i2 = localObject.hashCode();
    }
    else
    {
      i2 = 0;
      localObject = null;
    }
    m = (m + i2) * 31;
    localObject = e;
    int i3;
    if (localObject != null)
    {
      i3 = localObject.hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    m = (m + i3) * 31;
    localObject = f;
    int i4;
    if (localObject != null)
    {
      i4 = localObject.hashCode();
    }
    else
    {
      i4 = 0;
      localObject = null;
    }
    m = (m + i4) * 31;
    int i5 = h;
    if (i5 != 0) {
      i5 = 1;
    }
    m = (m + i5) * 31;
    localObject = g;
    if (localObject != null) {
      j = localObject.hashCode();
    }
    m = (m + j) * 31;
    int k = i;
    if (k != 0) {
      k = 1;
    }
    return m + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("InCallUICallerInfoResult(profileName=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", profilePictureUrl=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", phoneNumberForDisplay=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", shortFormattedAddress=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", carrier=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", tag=");
    localObject = f;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", blocked=");
    boolean bool = h;
    localStringBuilder.append(bool);
    localStringBuilder.append(", spamScore=");
    localObject = g;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", isPhonebookContact=");
    bool = i;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */