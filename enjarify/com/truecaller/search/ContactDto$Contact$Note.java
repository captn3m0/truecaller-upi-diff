package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ContactDto$Contact$Note
  extends ContactDto.Row
{
  public static final Parcelable.Creator CREATOR;
  public String note;
  
  static
  {
    ContactDto.Contact.Note.1 local1 = new com/truecaller/search/ContactDto$Contact$Note$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ContactDto$Contact$Note() {}
  
  private ContactDto$Contact$Note(Parcel paramParcel, boolean paramBoolean)
  {
    super(paramParcel);
    String str = paramParcel.readString();
    note = str;
    if (paramBoolean) {
      paramParcel.recycle();
    }
  }
  
  public ContactDto$Contact$Note(Note paramNote)
  {
    this(paramNote, true);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Note{note='");
    String str1 = note;
    localStringBuilder.append(str1);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", rowId='");
    long l = rowId;
    localStringBuilder.append(l);
    localStringBuilder.append(c);
    localStringBuilder.append(", tcId='");
    String str2 = tcId;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", isPrimary='");
    boolean bool = isPrimary;
    localStringBuilder.append(bool);
    localStringBuilder.append(c);
    localStringBuilder.append(", phonebookId='");
    l = phonebookId;
    localStringBuilder.append(l);
    localStringBuilder.append(c);
    localStringBuilder.append(", source='");
    int i = source;
    localStringBuilder.append(i);
    localStringBuilder.append(c);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    String str = note;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Note
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */