package com.truecaller.search;

import java.util.List;

public class ContactDto$Contact$BusinessProfile$OpenHours
{
  public String closes;
  public String opens;
  public List weekdays;
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("OpenHours{weekdays=");
    Object localObject = weekdays;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", opens='");
    localObject = opens;
    localStringBuilder.append((String)localObject);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", closes='");
    String str = closes;
    localStringBuilder.append(str);
    localStringBuilder.append(c);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.BusinessProfile.OpenHours
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */