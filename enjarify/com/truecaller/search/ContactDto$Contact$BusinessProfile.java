package com.truecaller.search;

import java.util.List;

public class ContactDto$Contact$BusinessProfile
{
  public String backgroundColor;
  public String branch;
  public String companySize;
  public String department;
  public List imageUrls;
  public String landLine;
  public List openHours;
  public String score;
  public String swishNumber;
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BusinessProfile{companySize='");
    Object localObject = companySize;
    localStringBuilder.append((String)localObject);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", branch='");
    String str = branch;
    localStringBuilder.append(str);
    localStringBuilder.append(c);
    localStringBuilder.append(", department='");
    str = department;
    localStringBuilder.append(str);
    localStringBuilder.append(c);
    localStringBuilder.append(", swishNumber='");
    str = swishNumber;
    localStringBuilder.append(str);
    localStringBuilder.append(c);
    localStringBuilder.append(", landLine='");
    str = landLine;
    localStringBuilder.append(str);
    localStringBuilder.append(c);
    localStringBuilder.append(", backgroundColor='");
    str = backgroundColor;
    localStringBuilder.append(str);
    localStringBuilder.append(c);
    localStringBuilder.append(", imageUrls=");
    localObject = imageUrls;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", score=");
    localObject = score;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", openHours=");
    localObject = openHours;
    localStringBuilder.append(localObject);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.BusinessProfile
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */