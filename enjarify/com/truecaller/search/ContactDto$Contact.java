package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.List;

public class ContactDto$Contact
  extends ContactDto.Row
{
  public static final Parcelable.Creator CREATOR;
  public String about;
  public String access;
  public List addresses;
  public transient long aggregatedRowId;
  public String altName;
  public List badges;
  public transient ContactDto.Contact.Business business;
  public ContactDto.Contact.BusinessProfile businessProfileNetworkResponse;
  public transient String cacheControl;
  public transient int commonConnections;
  public String companyName;
  public transient String defaultNumber;
  public transient int favoritePosition = -1;
  public String gender;
  public String handle;
  public String id;
  public String imId;
  public String image;
  public List internetAddresses;
  public transient boolean isFavorite;
  public String jobTitle;
  public String name;
  public transient ContactDto.Contact.Note note;
  public transient long phonebookHash;
  public transient long phonebookId;
  public transient String phonebookLookupKey;
  public List phones;
  public Number score;
  public transient String searchQuery;
  public transient long searchTime;
  public transient int source;
  public List sources;
  public transient ContactDto.Contact.StructuredName structuredName;
  public transient ContactDto.Contact.Style style;
  public List tags;
  public transient int tcFlag;
  public String transliteratedName;
  
  static
  {
    ContactDto.Contact.1 local1 = new com/truecaller/search/ContactDto$Contact$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ContactDto$Contact() {}
  
  private ContactDto$Contact(Parcel paramParcel, boolean paramBoolean)
  {
    super(paramParcel);
    Object localObject = paramParcel.readString();
    id = ((String)localObject);
    localObject = paramParcel.readString();
    name = ((String)localObject);
    localObject = paramParcel.readString();
    transliteratedName = ((String)localObject);
    localObject = paramParcel.readString();
    handle = ((String)localObject);
    localObject = paramParcel.readString();
    altName = ((String)localObject);
    localObject = paramParcel.readString();
    gender = ((String)localObject);
    localObject = paramParcel.readString();
    about = ((String)localObject);
    localObject = paramParcel.readString();
    image = ((String)localObject);
    localObject = paramParcel.readString();
    jobTitle = ((String)localObject);
    localObject = paramParcel.readString();
    companyName = ((String)localObject);
    localObject = paramParcel.readString();
    access = ((String)localObject);
    localObject = readNumber(paramParcel);
    score = ((Number)localObject);
    localObject = ContactDto.Contact.PhoneNumber.CREATOR;
    localObject = readList(paramParcel, (Parcelable.Creator)localObject);
    phones = ((List)localObject);
    localObject = ContactDto.Contact.Address.CREATOR;
    localObject = readList(paramParcel, (Parcelable.Creator)localObject);
    addresses = ((List)localObject);
    localObject = ContactDto.Contact.InternetAddress.CREATOR;
    localObject = readList(paramParcel, (Parcelable.Creator)localObject);
    internetAddresses = ((List)localObject);
    localObject = readStringList(paramParcel);
    badges = ((List)localObject);
    localObject = ContactDto.Contact.Tag.CREATOR;
    localObject = readList(paramParcel, (Parcelable.Creator)localObject);
    tags = ((List)localObject);
    localObject = ContactDto.Contact.Source.CREATOR;
    localObject = readList(paramParcel, (Parcelable.Creator)localObject);
    sources = ((List)localObject);
    long l = paramParcel.readLong();
    searchTime = l;
    localObject = paramParcel.readString();
    searchQuery = ((String)localObject);
    localObject = paramParcel.readString();
    cacheControl = ((String)localObject);
    int i = paramParcel.readInt();
    source = i;
    i = paramParcel.readInt();
    commonConnections = i;
    l = paramParcel.readLong();
    aggregatedRowId = l;
    l = paramParcel.readLong();
    phonebookId = l;
    l = paramParcel.readLong();
    phonebookHash = l;
    localObject = paramParcel.readString();
    phonebookLookupKey = ((String)localObject);
    localObject = paramParcel.readString();
    defaultNumber = ((String)localObject);
    i = paramParcel.readInt();
    int j = 1;
    if (i != j) {
      j = 0;
    }
    isFavorite = j;
    i = paramParcel.readInt();
    favoritePosition = i;
    i = paramParcel.readInt();
    tcFlag = i;
    localObject = ContactDto.Contact.StructuredName.class.getClassLoader();
    localObject = (ContactDto.Contact.StructuredName)paramParcel.readParcelable((ClassLoader)localObject);
    structuredName = ((ContactDto.Contact.StructuredName)localObject);
    localObject = ContactDto.Contact.Note.class.getClassLoader();
    localObject = (ContactDto.Contact.Note)paramParcel.readParcelable((ClassLoader)localObject);
    note = ((ContactDto.Contact.Note)localObject);
    localObject = ContactDto.Contact.Business.class.getClassLoader();
    localObject = (ContactDto.Contact.Business)paramParcel.readParcelable((ClassLoader)localObject);
    business = ((ContactDto.Contact.Business)localObject);
    localObject = ContactDto.Contact.Style.class.getClassLoader();
    localObject = (ContactDto.Contact.Style)paramParcel.readParcelable((ClassLoader)localObject);
    style = ((ContactDto.Contact.Style)localObject);
    if (paramBoolean) {
      paramParcel.recycle();
    }
  }
  
  public ContactDto$Contact(Contact paramContact)
  {
    this(paramContact, true);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Contact{id='");
    Object localObject1 = id;
    localStringBuilder.append((String)localObject1);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", phones=");
    Object localObject2 = phones;
    localStringBuilder.append(localObject2);
    localStringBuilder.append(", addresses=");
    localObject2 = addresses;
    localStringBuilder.append(localObject2);
    localStringBuilder.append(", internetAddresses=");
    localObject2 = internetAddresses;
    localStringBuilder.append(localObject2);
    localStringBuilder.append(", tags=");
    localObject2 = tags;
    localStringBuilder.append(localObject2);
    localStringBuilder.append(", sources=");
    localObject2 = sources;
    localStringBuilder.append(localObject2);
    localStringBuilder.append(", source=");
    int j = source;
    localStringBuilder.append(j);
    localStringBuilder.append(", commonConnections=");
    j = commonConnections;
    localStringBuilder.append(j);
    localStringBuilder.append(", searchTime=");
    long l = searchTime;
    localStringBuilder.append(l);
    localStringBuilder.append(", aggregatedRowId=");
    l = aggregatedRowId;
    localStringBuilder.append(l);
    localStringBuilder.append(", phonebookId=");
    l = phonebookId;
    localStringBuilder.append(l);
    localStringBuilder.append(", phonebookHash=");
    l = phonebookHash;
    localStringBuilder.append(l);
    localStringBuilder.append(", cacheControl='");
    localObject2 = cacheControl;
    localStringBuilder.append((String)localObject2);
    localStringBuilder.append(c);
    localStringBuilder.append(", isFavorite=");
    boolean bool = isFavorite;
    localStringBuilder.append(bool);
    localStringBuilder.append(", favoritePosition=");
    int i = favoritePosition;
    localStringBuilder.append(i);
    localStringBuilder.append(", tcFlag=");
    i = tcFlag;
    localStringBuilder.append(i);
    localStringBuilder.append(", structuredName=");
    localObject1 = structuredName;
    localStringBuilder.append(localObject1);
    localStringBuilder.append(", note=");
    localObject1 = note;
    localStringBuilder.append(localObject1);
    localStringBuilder.append(", business=");
    localObject1 = business;
    localStringBuilder.append(localObject1);
    localStringBuilder.append(", style=");
    localObject1 = style;
    localStringBuilder.append(localObject1);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    Object localObject = id;
    paramParcel.writeString((String)localObject);
    localObject = name;
    paramParcel.writeString((String)localObject);
    localObject = transliteratedName;
    paramParcel.writeString((String)localObject);
    localObject = handle;
    paramParcel.writeString((String)localObject);
    localObject = altName;
    paramParcel.writeString((String)localObject);
    localObject = gender;
    paramParcel.writeString((String)localObject);
    localObject = about;
    paramParcel.writeString((String)localObject);
    localObject = image;
    paramParcel.writeString((String)localObject);
    localObject = jobTitle;
    paramParcel.writeString((String)localObject);
    localObject = companyName;
    paramParcel.writeString((String)localObject);
    localObject = access;
    paramParcel.writeString((String)localObject);
    localObject = score;
    writeNumber(paramParcel, (Number)localObject);
    localObject = phones;
    paramParcel.writeTypedList((List)localObject);
    localObject = addresses;
    paramParcel.writeTypedList((List)localObject);
    localObject = internetAddresses;
    paramParcel.writeTypedList((List)localObject);
    localObject = badges;
    paramParcel.writeStringList((List)localObject);
    localObject = tags;
    paramParcel.writeTypedList((List)localObject);
    localObject = sources;
    paramParcel.writeTypedList((List)localObject);
    long l = searchTime;
    paramParcel.writeLong(l);
    localObject = searchQuery;
    paramParcel.writeString((String)localObject);
    localObject = cacheControl;
    paramParcel.writeString((String)localObject);
    int i = source;
    paramParcel.writeInt(i);
    i = commonConnections;
    paramParcel.writeInt(i);
    l = aggregatedRowId;
    paramParcel.writeLong(l);
    l = phonebookId;
    paramParcel.writeLong(l);
    l = phonebookHash;
    paramParcel.writeLong(l);
    localObject = phonebookLookupKey;
    paramParcel.writeString((String)localObject);
    localObject = defaultNumber;
    paramParcel.writeString((String)localObject);
    int j = isFavorite;
    paramParcel.writeInt(j);
    int k = favoritePosition;
    paramParcel.writeInt(k);
    k = tcFlag;
    paramParcel.writeInt(k);
    localObject = structuredName;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = note;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = business;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = style;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */