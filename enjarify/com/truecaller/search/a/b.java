package com.truecaller.search.a;

import c.a.ag;
import c.n;
import c.t;
import com.truecaller.ads.g;
import com.truecaller.ads.k.a;
import com.truecaller.ads.k.b;
import com.truecaller.ads.k.c;
import com.truecaller.ads.k.d;
import com.truecaller.ads.provider.f;
import com.truecaller.ads.provider.holders.e;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public final class b
  implements g, a
{
  private final HashMap a;
  private final Set b;
  private c c;
  private final Map d;
  private final f e;
  
  public b(f paramf)
  {
    e = paramf;
    paramf = new java/util/HashMap;
    paramf.<init>();
    a = paramf;
    paramf = new java/util/LinkedHashSet;
    paramf.<init>();
    paramf = (Set)paramf;
    b = paramf;
    paramf = new n[12];
    n localn = t.a("emergency", "/43067329/A*Fast_emergency*Native*GPS");
    paramf[0] = localn;
    localn = t.a("bank_balance", "/43067329/A*Fast_bank_balance*Native*GPS");
    paramf[1] = localn;
    localn = t.a("airlines", "/43067329/A*Fast_airlines*Native*GPS");
    paramf[2] = localn;
    localn = t.a("indian_railways", "/43067329/A*Fast_indian_railway*Native*GPS");
    paramf[3] = localn;
    localn = t.a("packers", "/43067329/A*Fast_courier*Native*GPS");
    paramf[4] = localn;
    localn = t.a("electronics", "/43067329/A*Fast_electronics*Native*GPS");
    paramf[5] = localn;
    localn = t.a("banks", "/43067329/A*Fast_banks*Native*GPS");
    paramf[6] = localn;
    localn = t.a("dth", "/43067329/A*Fast_dth*Native*GPS");
    paramf[7] = localn;
    localn = t.a("automobiles", "/43067329/A*Fast_automobiles*Native*GPS");
    paramf[8] = localn;
    localn = t.a("health", "/43067329/A*Fast_healthcare*Native*GPS");
    paramf[9] = localn;
    localn = t.a("hotels", "/43067329/A*Fast_hotel*Native*GPS");
    paramf[10] = localn;
    localn = t.a("insurance", "/43067329/A*Fast_insurance*Native*GPS");
    paramf[11] = localn;
    paramf = ag.a(paramf);
    d = paramf;
  }
  
  private static com.truecaller.ads.k b(String paramString)
  {
    return k.d.a().a(paramString).b("SEARCHRESULTS").c("globalSearch").e();
  }
  
  public final e a(String paramString, int paramInt)
  {
    c.g.b.k.b(paramString, "adId");
    Object localObject = (Map)a;
    boolean bool = ((Map)localObject).containsKey(paramString);
    if (bool) {
      return (e)a.get(paramString);
    }
    localObject = e;
    com.truecaller.ads.k localk = b(paramString);
    e locale = ((f)localObject).a(localk, paramInt);
    if (locale != null)
    {
      localObject = (Map)a;
      ((Map)localObject).put(paramString, locale);
    }
    return locale;
  }
  
  public final void a()
  {
    c localc = c;
    if (localc != null)
    {
      localc.b();
      return;
    }
  }
  
  public final void a(int paramInt) {}
  
  public final void a(e parame, int paramInt)
  {
    c.g.b.k.b(parame, "ad");
  }
  
  public final void a(c paramc)
  {
    c.g.b.k.b(paramc, "adsHelperListener");
    c = paramc;
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "adId");
    f localf = e;
    com.truecaller.ads.k localk = b(paramString);
    Object localObject = this;
    localObject = (g)this;
    localf.a(localk, (g)localObject);
    b.add(paramString);
  }
  
  public final void b()
  {
    Object localObject1 = b.iterator();
    boolean bool;
    for (;;)
    {
      bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      f localf = e;
      localObject2 = b((String)localObject2);
      Object localObject3 = this;
      localObject3 = (g)this;
      localf.b((com.truecaller.ads.k)localObject2, (g)localObject3);
    }
    localObject1 = a.values();
    Object localObject2 = "ads.values";
    c.g.b.k.a(localObject1, (String)localObject2);
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (e)((Iterator)localObject1).next();
      ((e)localObject2).d();
    }
    c = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */