package com.truecaller.search;

import android.content.Context;
import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.data.access.m;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.g;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.search.d.b;
import com.truecaller.network.search.n;
import com.truecaller.tracking.events.al;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ay;
import com.truecaller.tracking.events.ay.a;
import com.truecaller.tracking.events.bc;
import com.truecaller.tracking.events.bc.a;
import com.truecaller.tracking.events.z;
import com.truecaller.tracking.events.z.a;
import com.truecaller.util.ce;
import com.truecaller.utils.i;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public abstract class a
  extends com.truecaller.old.a.a
{
  private final Context a;
  private final Contact c;
  private final FilterManager d;
  private final f e;
  private final String f;
  private final UUID g;
  private final int h;
  private com.truecaller.network.search.d i;
  private i j;
  
  a(Context paramContext, com.truecaller.old.a.c paramc, FilterManager paramFilterManager, f paramf, Contact paramContact, String paramString, UUID paramUUID, int paramInt)
  {
    super(paramc, false, null);
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    d = paramFilterManager;
    e = paramf;
    c = paramContact;
    f = paramString;
    g = paramUUID;
    h = paramInt;
    paramContext = ((bk)a).a().v();
    j = paramContext;
  }
  
  private void a(Contact paramContact, List paramList)
  {
    boolean bool1 = paramContact.S();
    if (bool1)
    {
      bool1 = paramContact.O();
      if (bool1)
      {
        Object localObject1 = paramContact.n();
        bool1 = k.a((String)localObject1);
        if (!bool1)
        {
          bool1 = a(paramContact);
          if (!bool1)
          {
            paramContact = "validCacheResult";
            a(paramContact);
            return;
          }
        }
        localObject1 = j;
        bool1 = ((i)localObject1).a();
        String str1 = null;
        int k = 1;
        if (!bool1)
        {
          paramList = new String[k];
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>("Cannot refresh ");
          ((StringBuilder)localObject1).append(paramContact);
          ((StringBuilder)localObject1).append(", internet not OK");
          paramContact = ((StringBuilder)localObject1).toString();
          paramList[0] = paramContact;
          a("noConnection");
          return;
        }
        localObject1 = new String[k];
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append(paramContact);
        Object localObject3 = " is stale, attempt to refresh it";
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject2 = ((StringBuilder)localObject2).toString();
        localObject1[0] = localObject2;
        bool1 = false;
        localObject1 = null;
        localObject2 = paramContact.A().iterator();
        boolean bool3;
        do
        {
          boolean bool2 = ((Iterator)localObject2).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = (Number)((Iterator)localObject2).next();
          String str2 = ((Number)localObject3).o();
          bool3 = TextUtils.isEmpty(str2);
        } while (bool3);
        localObject1 = localObject3;
        if (localObject1 == null)
        {
          paramList = new String[k];
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>("Cannot refresh ");
          ((StringBuilder)localObject1).append(paramContact);
          ((StringBuilder)localObject1).append(", no searchable number");
          paramContact = ((StringBuilder)localObject1).toString();
          paramList[0] = paramContact;
          return;
        }
        paramContact = ((Number)localObject1).o();
        paramList.add(paramContact);
        paramContact = i;
        paramList = ((Number)localObject1).a();
        str1 = ((Number)localObject1).d();
        localObject1 = ((Number)localObject1).l();
        d.b localb = new com/truecaller/network/search/d$b;
        localb.<init>(paramList, str1, (String)localObject1);
        b.add(localb);
        return;
      }
    }
  }
  
  private void a(String paramString)
  {
    Object localObject1 = z.b();
    Object localObject2 = g.toString();
    localObject2 = ((z.a)localObject1).a((CharSequence)localObject2);
    Object localObject3 = f;
    localObject2 = ((z.a)localObject2).d((CharSequence)localObject3);
    localObject3 = String.valueOf(h);
    ((z.a)localObject2).c((CharSequence)localObject3);
    localObject2 = null;
    ((z.a)localObject1).b(null);
    boolean bool1 = false;
    localObject3 = null;
    ((z.a)localObject1).a(false);
    ((z.a)localObject1).b(false);
    Object localObject4 = d;
    Object localObject5 = c.G();
    localObject4 = ((FilterManager)localObject4).a((String)localObject5);
    localObject5 = new java/util/ArrayList;
    ((ArrayList)localObject5).<init>();
    Object localObject6 = al.b();
    boolean bool2 = am.b(c.z());
    int i1 = 1;
    bool2 ^= i1;
    localObject6 = ((al.a)localObject6).b(bool2);
    Object localObject7 = c;
    int k = ((Contact)localObject7).getSource() & 0x2;
    if (k != 0)
    {
      k = 1;
    }
    else
    {
      k = 0;
      localObject7 = null;
    }
    localObject6 = ((al.a)localObject6).a(k);
    localObject7 = c;
    int m = ((Contact)localObject7).getSource() & 0x40;
    if (m != 0)
    {
      m = 1;
    }
    else
    {
      m = 0;
      localObject7 = null;
    }
    localObject7 = Boolean.valueOf(m);
    localObject6 = ((al.a)localObject6).e((Boolean)localObject7);
    int n = c.I();
    localObject7 = Integer.valueOf(Math.max(0, n));
    localObject6 = ((al.a)localObject6).a((Integer)localObject7);
    boolean bool3 = c.U();
    localObject7 = Boolean.valueOf(bool3);
    localObject6 = ((al.a)localObject6).d((Boolean)localObject7);
    localObject7 = j;
    Object localObject8 = FilterManager.ActionSource.CUSTOM_BLACKLIST;
    if (localObject7 == localObject8)
    {
      bool3 = true;
    }
    else
    {
      bool3 = false;
      localObject7 = null;
    }
    localObject7 = Boolean.valueOf(bool3);
    localObject6 = ((al.a)localObject6).a((Boolean)localObject7);
    localObject7 = j;
    localObject8 = FilterManager.ActionSource.CUSTOM_WHITELIST;
    if (localObject7 == localObject8)
    {
      bool3 = true;
    }
    else
    {
      bool3 = false;
      localObject7 = null;
    }
    localObject7 = Boolean.valueOf(bool3);
    localObject6 = ((al.a)localObject6).c((Boolean)localObject7);
    localObject4 = j;
    localObject7 = FilterManager.ActionSource.TOP_SPAMMER;
    if (localObject4 == localObject7) {
      bool1 = true;
    }
    localObject3 = Boolean.valueOf(bool1);
    localObject3 = ((al.a)localObject6).b((Boolean)localObject3).a();
    localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject6 = new java/util/ArrayList;
    ((ArrayList)localObject6).<init>();
    localObject7 = new java/util/ArrayList;
    ((ArrayList)localObject7).<init>();
    localObject8 = c.J().iterator();
    Object localObject9;
    for (;;)
    {
      bool4 = ((Iterator)localObject8).hasNext();
      if (!bool4) {
        break;
      }
      localObject9 = (Tag)((Iterator)localObject8).next();
      int i2 = ((Tag)localObject9).getSource();
      if (i2 == i1)
      {
        localObject9 = ((Tag)localObject9).a();
        ((List)localObject4).add(localObject9);
      }
      else
      {
        localObject9 = ((Tag)localObject9).a();
        ((List)localObject6).add(localObject9);
      }
    }
    localObject8 = ce.a(c);
    if (localObject8 != null)
    {
      long l = a;
      localObject8 = String.valueOf(l);
      ((List)localObject7).add(localObject8);
    }
    localObject8 = bc.b();
    boolean bool4 = ((List)localObject4).isEmpty();
    if (bool4) {
      localObject4 = null;
    }
    localObject4 = ((bc.a)localObject8).a((List)localObject4);
    boolean bool5 = ((List)localObject6).isEmpty();
    if (bool5)
    {
      bool6 = false;
      localObject6 = null;
    }
    localObject4 = ((bc.a)localObject4).b((List)localObject6);
    boolean bool6 = ((List)localObject7).isEmpty();
    if (bool6)
    {
      bool3 = false;
      localObject7 = null;
    }
    localObject4 = ((bc.a)localObject4).c((List)localObject7).a();
    localObject6 = c.A();
    localObject7 = c.G();
    localObject6 = ((List)localObject6).iterator();
    bool5 = false;
    localObject8 = null;
    for (;;)
    {
      bool4 = ((Iterator)localObject6).hasNext();
      if (!bool4) {
        break;
      }
      localObject9 = (Number)((Iterator)localObject6).next();
      int i3 = ((Number)localObject9).getSource() & i1;
      if (i3 != 0)
      {
        localObject8 = ((Number)localObject9).b();
        localObject7 = ((Number)localObject9).o();
      }
    }
    localObject6 = ay.b().a((CharSequence)localObject7);
    localObject4 = ((ay.a)localObject6).a((bc)localObject4);
    localObject3 = ((ay.a)localObject4).a((al)localObject3);
    paramString = ((ay.a)localObject3).b(paramString).c((CharSequence)localObject8).a();
    ((List)localObject5).add(paramString);
    ((z.a)localObject1).a((List)localObject5);
    ((z.a)localObject1).b(null);
    try
    {
      paramString = e;
      paramString = paramString.a();
      paramString = (ae)paramString;
      localObject1 = ((z.a)localObject1).a();
      paramString.a((org.apache.a.d.d)localObject1);
      return;
    }
    catch (org.apache.a.a locala)
    {
      AssertionUtil.reportThrowableButNeverCrash(locala;
    }
  }
  
  protected List a(Context paramContext, List paramList)
  {
    try
    {
      paramContext = i;
      paramContext = paramContext.a();
      localStringBuilder = null;
      c = false;
      d = false;
      paramContext = paramContext.b();
      if (paramContext == null) {
        break label80;
      }
      return c;
    }
    catch (RuntimeException paramContext) {}catch (IOException paramContext) {}
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    String str = "Searching for ";
    localStringBuilder.<init>(str);
    localStringBuilder.append(paramList);
    localStringBuilder.append(" failed");
    paramList = localStringBuilder.toString();
    com.truecaller.log.d.a(paramContext, paramList);
    label80:
    return Collections.emptyList();
  }
  
  public abstract boolean a(Contact paramContact);
  
  protected Object doInBackground(Object... paramVarArgs)
  {
    try
    {
      paramVarArgs = new com/truecaller/network/search/d;
      Object localObject1 = a;
      Object localObject2 = g;
      Object localObject3 = f;
      paramVarArgs.<init>((Context)localObject1, (UUID)localObject2, (String)localObject3);
      int k = h;
      e = k;
      i = paramVarArgs;
      paramVarArgs = TrueApp.x();
      localObject1 = new com/truecaller/data/access/c;
      ((com.truecaller.data.access.c)localObject1).<init>(paramVarArgs);
      localObject2 = new com/truecaller/data/access/m;
      ((m)localObject2).<init>(paramVarArgs);
      localObject3 = c;
      localObject3 = ((Contact)localObject3).getId();
      Object localObject4 = new java/util/ArrayList;
      ((ArrayList)localObject4).<init>();
      int m = 1;
      Object localObject5;
      if (localObject3 != null)
      {
        long l1 = ((Long)localObject3).longValue();
        long l2 = 0L;
        boolean bool2 = l1 < l2;
        if (bool2)
        {
          localObject5 = c;
          bool3 = com.truecaller.data.access.c.b((Contact)localObject5);
          if (bool3)
          {
            l1 = ((Long)localObject3).longValue();
            localObject3 = ((m)localObject2).a(l1);
            localObject3 = ((List)localObject3).iterator();
            for (;;)
            {
              bool3 = ((Iterator)localObject3).hasNext();
              if (!bool3) {
                break;
              }
              localObject5 = ((Iterator)localObject3).next();
              localObject5 = (Contact)localObject5;
              a((Contact)localObject5, (List)localObject4);
            }
            n = 1;
            break label229;
          }
          localObject3 = c;
          a((Contact)localObject3, (List)localObject4);
        }
      }
      int n = 0;
      localObject3 = null;
      label229:
      boolean bool3 = ((List)localObject4).isEmpty();
      if (!bool3)
      {
        paramVarArgs = a(paramVarArgs, (List)localObject4);
        boolean bool4 = paramVarArgs.isEmpty();
        if (!bool4)
        {
          bool4 = ((m)localObject2).b(paramVarArgs);
          if (bool4)
          {
            localObject4 = new String[m];
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localObject5 = "Stored ";
            localStringBuilder.<init>((String)localObject5);
            int i1 = paramVarArgs.size();
            localStringBuilder.append(i1);
            paramVarArgs = " refreshed contact(s)";
            localStringBuilder.append(paramVarArgs);
            paramVarArgs = localStringBuilder.toString();
            localObject4[0] = paramVarArgs;
            if (n != 0)
            {
              paramVarArgs = c;
              return ((com.truecaller.data.access.c)localObject1).a(paramVarArgs);
            }
            paramVarArgs = c;
            paramVarArgs = paramVarArgs.getTcId();
            boolean bool1 = TextUtils.isEmpty(paramVarArgs);
            if (!bool1) {
              return ((m)localObject2).a(paramVarArgs);
            }
          }
          else
          {
            localObject1 = new java/lang/RuntimeException;
            localObject2 = "Error storing ";
            paramVarArgs = String.valueOf(paramVarArgs);
            paramVarArgs = ((String)localObject2).concat(paramVarArgs);
            ((RuntimeException)localObject1).<init>(paramVarArgs);
            throw ((Throwable)localObject1);
          }
        }
      }
      else
      {
        paramVarArgs = new String[m];
        localObject1 = new java/lang/StringBuilder;
        localObject2 = "No contacts were refreshed for ";
        ((StringBuilder)localObject1).<init>((String)localObject2);
        localObject2 = c;
        ((StringBuilder)localObject1).append(localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        paramVarArgs[0] = localObject1;
      }
    }
    catch (RuntimeException paramVarArgs)
    {
      com.truecaller.log.d.a(paramVarArgs);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */