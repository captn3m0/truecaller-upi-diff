package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ContactDto$Contact$Source$1
  implements Parcelable.Creator
{
  public final ContactDto.Contact.Source createFromParcel(Parcel paramParcel)
  {
    ContactDto.Contact.Source localSource = new com/truecaller/search/ContactDto$Contact$Source;
    localSource.<init>(paramParcel, false, null);
    return localSource;
  }
  
  public final ContactDto.Contact.Source[] newArray(int paramInt)
  {
    return new ContactDto.Contact.Source[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Source.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */