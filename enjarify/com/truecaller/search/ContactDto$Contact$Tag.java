package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ContactDto$Contact$Tag
  extends ContactDto.Row
{
  public static final Parcelable.Creator CREATOR;
  public String tag;
  
  static
  {
    ContactDto.Contact.Tag.1 local1 = new com/truecaller/search/ContactDto$Contact$Tag$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ContactDto$Contact$Tag() {}
  
  private ContactDto$Contact$Tag(Parcel paramParcel, boolean paramBoolean)
  {
    super(paramParcel);
    String str = paramParcel.readString();
    tag = str;
    if (paramBoolean) {
      paramParcel.recycle();
    }
  }
  
  public ContactDto$Contact$Tag(Tag paramTag)
  {
    this(paramTag, true);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Tag{rowId=");
    long l = rowId;
    localStringBuilder.append(l);
    localStringBuilder.append(", tcId='");
    String str = tcId;
    localStringBuilder.append(str);
    localStringBuilder.append('\'');
    localStringBuilder.append(", isPrimary=");
    boolean bool = isPrimary;
    localStringBuilder.append(bool);
    localStringBuilder.append(", phonebookId=");
    l = phonebookId;
    localStringBuilder.append(l);
    localStringBuilder.append(", source=");
    int i = source;
    localStringBuilder.append(i);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    String str = tag;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Tag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */