package com.truecaller.search;

import com.google.gson.g;
import com.truecaller.common.network.util.KnownEndpoints;
import java.util.concurrent.TimeUnit;

public final class i
{
  private static final e.a.a.a a;
  
  static
  {
    g localg = new com/google/gson/g;
    localg.<init>();
    Object localObject = new com/truecaller/search/i$a;
    ((i.a)localObject).<init>((byte)0);
    localObject = ((i.a)localObject).a();
    a = e.a.a.a.a(localg.a(ContactDto.Contact.Tag.class, localObject).a());
  }
  
  public static j a()
  {
    return a(0, null, false);
  }
  
  public static j a(int paramInt, TimeUnit paramTimeUnit, boolean paramBoolean)
  {
    com.truecaller.common.network.util.a locala1 = new com/truecaller/common/network/util/a;
    locala1.<init>();
    KnownEndpoints localKnownEndpoints = KnownEndpoints.SEARCH;
    com.truecaller.common.network.util.a locala2 = locala1.a(localKnownEndpoints).a(paramInt, paramTimeUnit, paramBoolean);
    paramTimeUnit = a;
    return (j)locala2.a(paramTimeUnit).a(j.class).b(j.class);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */