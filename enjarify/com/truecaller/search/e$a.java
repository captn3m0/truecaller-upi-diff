package com.truecaller.search;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;
import android.text.TextUtils;
import com.truecaller.data.entity.Number;
import com.truecaller.log.d;

class e$a
  extends e
{
  static final a a;
  private static final String[] b = { "number" };
  
  static
  {
    a locala = new com/truecaller/search/e$a;
    locala.<init>();
    a = locala;
  }
  
  static String b(Context paramContext, String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      Uri localUri1 = ContactsContract.PhoneLookup.CONTENT_FILTER_URI;
      paramString = Uri.encode(paramString);
      Uri localUri2 = Uri.withAppendedPath(localUri1, paramString);
      try
      {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        String[] arrayOfString = b;
        paramContext = localContentResolver.query(localUri2, arrayOfString, null, null, null);
        if (paramContext != null) {
          try
          {
            boolean bool2 = paramContext.moveToNext();
            if (bool2)
            {
              bool2 = false;
              paramString = null;
              paramString = paramContext.getString(0);
              return paramString;
            }
          }
          finally
          {
            paramContext.close();
          }
        }
        return null;
      }
      catch (RuntimeException paramContext)
      {
        d.a(paramContext);
      }
    }
  }
  
  boolean a(Context paramContext, Number paramNumber)
  {
    if (paramNumber != null)
    {
      paramNumber = paramNumber.o();
      boolean bool = a(paramContext, paramNumber);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  final boolean a(Context paramContext, String paramString)
  {
    paramContext = b(paramContext, paramString);
    return paramContext != null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */