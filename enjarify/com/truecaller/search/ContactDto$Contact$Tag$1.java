package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ContactDto$Contact$Tag$1
  implements Parcelable.Creator
{
  public final ContactDto.Contact.Tag createFromParcel(Parcel paramParcel)
  {
    ContactDto.Contact.Tag localTag = new com/truecaller/search/ContactDto$Contact$Tag;
    localTag.<init>(paramParcel, false, null);
    return localTag;
  }
  
  public final ContactDto.Contact.Tag[] newArray(int paramInt)
  {
    return new ContactDto.Contact.Tag[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Tag.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */