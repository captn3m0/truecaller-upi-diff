package com.truecaller.search;

import android.content.Context;
import com.truecaller.androidactors.f;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.FilterManager;
import com.truecaller.old.a.c;
import com.truecaller.util.w;
import java.util.UUID;

public abstract class h
  extends g
{
  private final String a;
  
  public h(Context paramContext, c paramc, FilterManager paramFilterManager, f paramf, Contact paramContact, String paramString1, String paramString2, UUID paramUUID, b paramb)
  {
    super(paramContext, paramc, paramFilterManager, paramf, paramContact, 1, paramString2, paramUUID, 10, paramb);
    a = paramString1;
  }
  
  public final boolean a(Contact paramContact)
  {
    Object localObject = a;
    localObject = w.a(paramContact, (String)localObject);
    if (localObject != null)
    {
      boolean bool = super.a(paramContact);
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */