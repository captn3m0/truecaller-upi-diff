package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ContactDto$Contact$InternetAddress$1
  implements Parcelable.Creator
{
  public final ContactDto.Contact.InternetAddress createFromParcel(Parcel paramParcel)
  {
    ContactDto.Contact.InternetAddress localInternetAddress = new com/truecaller/search/ContactDto$Contact$InternetAddress;
    localInternetAddress.<init>(paramParcel, false, null);
    return localInternetAddress;
  }
  
  public final ContactDto.Contact.InternetAddress[] newArray(int paramInt)
  {
    return new ContactDto.Contact.InternetAddress[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.InternetAddress.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */