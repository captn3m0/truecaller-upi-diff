package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ContactDto$Contact$Address$1
  implements Parcelable.Creator
{
  public final ContactDto.Contact.Address createFromParcel(Parcel paramParcel)
  {
    ContactDto.Contact.Address localAddress = new com/truecaller/search/ContactDto$Contact$Address;
    localAddress.<init>(paramParcel, false, null);
    return localAddress;
  }
  
  public final ContactDto.Contact.Address[] newArray(int paramInt)
  {
    return new ContactDto.Contact.Address[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Address.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */