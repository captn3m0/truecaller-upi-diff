package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ContactDto$Contact$Note$1
  implements Parcelable.Creator
{
  public final ContactDto.Contact.Note createFromParcel(Parcel paramParcel)
  {
    ContactDto.Contact.Note localNote = new com/truecaller/search/ContactDto$Contact$Note;
    localNote.<init>(paramParcel, false, null);
    return localNote;
  }
  
  public final ContactDto.Contact.Note[] newArray(int paramInt)
  {
    return new ContactDto.Contact.Note[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Note.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */