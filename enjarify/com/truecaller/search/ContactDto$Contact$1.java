package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ContactDto$Contact$1
  implements Parcelable.Creator
{
  public final ContactDto.Contact createFromParcel(Parcel paramParcel)
  {
    ContactDto.Contact localContact = new com/truecaller/search/ContactDto$Contact;
    localContact.<init>(paramParcel, false, null);
    return localContact;
  }
  
  public final ContactDto.Contact[] newArray(int paramInt)
  {
    return new ContactDto.Contact[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */