package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ContactDto$Contact$Address
  extends ContactDto.Row
{
  public static final Parcelable.Creator CREATOR;
  public String area;
  public String city;
  public String countryCode;
  public Number latitude;
  public Number longitude;
  public String street;
  public String timeZone;
  public String type;
  public String zipCode;
  
  static
  {
    ContactDto.Contact.Address.1 local1 = new com/truecaller/search/ContactDto$Contact$Address$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ContactDto$Contact$Address() {}
  
  private ContactDto$Contact$Address(Parcel paramParcel, boolean paramBoolean)
  {
    super(paramParcel);
    Object localObject = paramParcel.readString();
    type = ((String)localObject);
    localObject = paramParcel.readString();
    street = ((String)localObject);
    localObject = paramParcel.readString();
    zipCode = ((String)localObject);
    localObject = paramParcel.readString();
    city = ((String)localObject);
    localObject = paramParcel.readString();
    area = ((String)localObject);
    localObject = paramParcel.readString();
    countryCode = ((String)localObject);
    localObject = paramParcel.readString();
    timeZone = ((String)localObject);
    localObject = readNumber(paramParcel);
    latitude = ((Number)localObject);
    localObject = readNumber(paramParcel);
    longitude = ((Number)localObject);
    if (paramBoolean) {
      paramParcel.recycle();
    }
  }
  
  public ContactDto$Contact$Address(Address paramAddress)
  {
    this(paramAddress, true);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Address{type='");
    String str1 = type;
    localStringBuilder.append(str1);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", countryCode='");
    String str2 = countryCode;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", timeZone='");
    str2 = timeZone;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", rowId=");
    long l1 = rowId;
    localStringBuilder.append(l1);
    localStringBuilder.append(", tcId='");
    str2 = tcId;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", isPrimary=");
    boolean bool = isPrimary;
    localStringBuilder.append(bool);
    localStringBuilder.append(", phonebookId=");
    long l2 = phonebookId;
    localStringBuilder.append(l2);
    localStringBuilder.append(", source=");
    int i = source;
    localStringBuilder.append(i);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    Object localObject = type;
    paramParcel.writeString((String)localObject);
    localObject = street;
    paramParcel.writeString((String)localObject);
    localObject = zipCode;
    paramParcel.writeString((String)localObject);
    localObject = city;
    paramParcel.writeString((String)localObject);
    localObject = area;
    paramParcel.writeString((String)localObject);
    localObject = countryCode;
    paramParcel.writeString((String)localObject);
    localObject = timeZone;
    paramParcel.writeString((String)localObject);
    localObject = latitude;
    writeNumber(paramParcel, (Number)localObject);
    localObject = longitude;
    writeNumber(paramParcel, (Number)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Address
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */