package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ContactDto$Contact$Source
  extends ContactDto.Row
{
  public static final Parcelable.Creator CREATOR;
  public String caption;
  public Map extra;
  public String id;
  public String logo;
  public String url;
  
  static
  {
    ContactDto.Contact.Source.1 local1 = new com/truecaller/search/ContactDto$Contact$Source$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ContactDto$Contact$Source() {}
  
  private ContactDto$Contact$Source(Parcel paramParcel, boolean paramBoolean)
  {
    super(paramParcel);
    String str1 = paramParcel.readString();
    id = str1;
    str1 = paramParcel.readString();
    url = str1;
    str1 = paramParcel.readString();
    logo = str1;
    str1 = paramParcel.readString();
    caption = str1;
    int i = paramParcel.readInt();
    if (i >= 0)
    {
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      extra = localHashMap;
      int j = 0;
      localHashMap = null;
      while (j < i)
      {
        Map localMap = extra;
        String str2 = paramParcel.readString();
        String str3 = paramParcel.readString();
        localMap.put(str2, str3);
        j += 1;
      }
    }
    if (paramBoolean) {
      paramParcel.recycle();
    }
  }
  
  public ContactDto$Contact$Source(Source paramSource)
  {
    this(paramSource, true);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Source{id='");
    String str1 = id;
    localStringBuilder.append(str1);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", rowId=");
    long l1 = rowId;
    localStringBuilder.append(l1);
    localStringBuilder.append(", tcId='");
    String str2 = tcId;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", isPrimary=");
    boolean bool = isPrimary;
    localStringBuilder.append(bool);
    localStringBuilder.append(", phonebookId=");
    long l2 = phonebookId;
    localStringBuilder.append(l2);
    localStringBuilder.append(", source=");
    int i = source;
    localStringBuilder.append(i);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    Object localObject1 = id;
    paramParcel.writeString((String)localObject1);
    localObject1 = url;
    paramParcel.writeString((String)localObject1);
    localObject1 = logo;
    paramParcel.writeString((String)localObject1);
    localObject1 = caption;
    paramParcel.writeString((String)localObject1);
    localObject1 = extra;
    if (localObject1 == null) {
      paramInt = -1;
    } else {
      paramInt = ((Map)localObject1).size();
    }
    paramParcel.writeInt(paramInt);
    localObject1 = extra;
    if (localObject1 != null)
    {
      localObject1 = ((Map)localObject1).entrySet().iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        Object localObject2 = (Map.Entry)((Iterator)localObject1).next();
        String str = (String)((Map.Entry)localObject2).getKey();
        paramParcel.writeString(str);
        localObject2 = (String)((Map.Entry)localObject2).getValue();
        paramParcel.writeString((String)localObject2);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Source
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */