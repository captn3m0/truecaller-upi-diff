package com.truecaller.search;

import java.util.ArrayList;
import java.util.List;

public class KeyedContactDto
{
  public List data;
  
  public KeyedContactDto()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    data = localArrayList;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("KeyedContactDto{data=");
    List localList = data;
    localStringBuilder.append(localList);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.KeyedContactDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */