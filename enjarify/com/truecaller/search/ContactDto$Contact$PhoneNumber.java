package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ContactDto$Contact$PhoneNumber
  extends ContactDto.Row
{
  public static final Parcelable.Creator CREATOR;
  public static int EMPTY_TEL_TYPE = 255;
  public String carrier;
  public String countryCode;
  public String dialingCode;
  public String e164Format;
  public String id;
  public String nationalFormat;
  public String numberType;
  public transient String rawNumberFormat;
  public String spamScore;
  public String spamType;
  public String telType;
  public transient String telTypeLabel;
  public String type;
  
  static
  {
    ContactDto.Contact.PhoneNumber.1 local1 = new com/truecaller/search/ContactDto$Contact$PhoneNumber$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ContactDto$Contact$PhoneNumber() {}
  
  private ContactDto$Contact$PhoneNumber(Parcel paramParcel, boolean paramBoolean)
  {
    super(paramParcel);
    String str = paramParcel.readString();
    id = str;
    str = paramParcel.readString();
    type = str;
    str = paramParcel.readString();
    e164Format = str;
    str = paramParcel.readString();
    nationalFormat = str;
    str = paramParcel.readString();
    dialingCode = str;
    str = paramParcel.readString();
    countryCode = str;
    str = paramParcel.readString();
    numberType = str;
    str = paramParcel.readString();
    carrier = str;
    str = paramParcel.readString();
    telType = str;
    str = paramParcel.readString();
    spamScore = str;
    str = paramParcel.readString();
    spamType = str;
    str = paramParcel.readString();
    rawNumberFormat = str;
    str = paramParcel.readString();
    telTypeLabel = str;
    if (paramBoolean) {
      paramParcel.recycle();
    }
  }
  
  public ContactDto$Contact$PhoneNumber(PhoneNumber paramPhoneNumber)
  {
    this(paramPhoneNumber, true);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PhoneNumber{id='");
    String str1 = id;
    localStringBuilder.append(str1);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", type='");
    String str2 = type;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", countryCode='");
    str2 = countryCode;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", numberType='");
    str2 = numberType;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", telType='");
    str2 = telType;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", spamScore='");
    str2 = spamScore;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", spamType='");
    str2 = spamType;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", telTypeLabel='");
    str2 = telTypeLabel;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", rowId=");
    long l1 = rowId;
    localStringBuilder.append(l1);
    localStringBuilder.append(", tcId='");
    str2 = tcId;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", isPrimary=");
    boolean bool = isPrimary;
    localStringBuilder.append(bool);
    localStringBuilder.append(", phonebookId=");
    long l2 = phonebookId;
    localStringBuilder.append(l2);
    localStringBuilder.append(", source=");
    int i = source;
    localStringBuilder.append(i);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    String str = id;
    paramParcel.writeString(str);
    str = type;
    paramParcel.writeString(str);
    str = e164Format;
    paramParcel.writeString(str);
    str = nationalFormat;
    paramParcel.writeString(str);
    str = dialingCode;
    paramParcel.writeString(str);
    str = countryCode;
    paramParcel.writeString(str);
    str = numberType;
    paramParcel.writeString(str);
    str = carrier;
    paramParcel.writeString(str);
    str = telType;
    paramParcel.writeString(str);
    str = spamScore;
    paramParcel.writeString(str);
    str = spamType;
    paramParcel.writeString(str);
    str = rawNumberFormat;
    paramParcel.writeString(str);
    str = telTypeLabel;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.PhoneNumber
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */