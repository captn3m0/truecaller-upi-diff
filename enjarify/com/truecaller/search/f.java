package com.truecaller.search;

import android.content.Context;
import android.text.TextUtils;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.utils.l;
import java.util.Iterator;
import java.util.List;

public final class f
{
  public static boolean a(Context paramContext, Contact paramContact)
  {
    Object localObject1 = ((bk)paramContext.getApplicationContext()).a().bw();
    String[] arrayOfString = { "android.permission.READ_CONTACTS" };
    boolean bool1 = ((l)localObject1).a(arrayOfString);
    arrayOfString = null;
    if (!bool1) {
      return false;
    }
    try
    {
      localObject1 = e.a();
      paramContact = paramContact.A();
      paramContact = paramContact.iterator();
      boolean bool2;
      do
      {
        Object localObject2;
        boolean bool3;
        do
        {
          bool2 = paramContact.hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = paramContact.next();
          localObject2 = (Number)localObject2;
          localObject2 = ((Number)localObject2).a();
          bool3 = TextUtils.isEmpty((CharSequence)localObject2);
        } while (bool3);
        bool2 = ((e)localObject1).a(paramContext, (String)localObject2);
      } while (!bool2);
      return true;
      return false;
    }
    catch (SecurityException localSecurityException) {}
    return false;
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    Object localObject = ((bk)paramContext.getApplicationContext()).a().bw();
    String[] arrayOfString = { "android.permission.READ_CONTACTS" };
    boolean bool1 = ((l)localObject).a(arrayOfString);
    if (bool1)
    {
      localObject = e.a();
      boolean bool2 = ((e)localObject).a(paramContext, paramString);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */