package com.truecaller.search;

public class KeyedContactDto$KeyedContact
{
  public String key;
  public ContactDto.Contact value;
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("KeyedContact{value=");
    ContactDto.Contact localContact = value;
    localStringBuilder.append(localContact);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.KeyedContactDto.KeyedContact
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */