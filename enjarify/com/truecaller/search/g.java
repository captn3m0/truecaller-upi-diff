package com.truecaller.search;

import android.content.Context;
import com.truecaller.androidactors.f;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.FilterManager;
import com.truecaller.old.a.c;
import java.util.List;
import java.util.UUID;

public abstract class g
  extends a
{
  private final int a;
  private final b c;
  
  public g(Context paramContext, c paramc, FilterManager paramFilterManager, f paramf, Contact paramContact, int paramInt1, String paramString, UUID paramUUID, int paramInt2, b paramb)
  {
    super(paramContext, paramc, paramFilterManager, paramf, paramContact, paramString, paramUUID, paramInt2);
    a = paramInt1;
    c = paramb;
  }
  
  protected final List a(Context paramContext, List paramList)
  {
    int i = a;
    int j = paramList.size();
    if (i < j)
    {
      j = a;
      paramList = paramList.subList(0, j);
      return super.a(paramContext, paramList);
    }
    return super.a(paramContext, paramList);
  }
  
  public boolean a(Contact paramContact)
  {
    b localb = c;
    long l = paramContact.H();
    int i = paramContact.getSource();
    return localb.a(l, i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */