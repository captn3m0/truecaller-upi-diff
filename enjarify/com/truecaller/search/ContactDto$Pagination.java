package com.truecaller.search;

public class ContactDto$Pagination
{
  public String next;
  public String pageId;
  public String prev;
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Pagination{prev='");
    String str1 = prev;
    localStringBuilder.append(str1);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", next='");
    String str2 = next;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", pageId='");
    str2 = pageId;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Pagination
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */