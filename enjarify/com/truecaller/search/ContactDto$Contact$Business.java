package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ContactDto$Contact$Business
  extends ContactDto.Row
{
  public static final Parcelable.Creator CREATOR;
  public String branch;
  public String companySize;
  public String department;
  public String landline;
  public String openingHours;
  public String score;
  public String swishNumber;
  
  static
  {
    ContactDto.Contact.Business.1 local1 = new com/truecaller/search/ContactDto$Contact$Business$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ContactDto$Contact$Business() {}
  
  private ContactDto$Contact$Business(Parcel paramParcel, boolean paramBoolean)
  {
    super(paramParcel);
    String str = paramParcel.readString();
    branch = str;
    str = paramParcel.readString();
    department = str;
    str = paramParcel.readString();
    companySize = str;
    str = paramParcel.readString();
    openingHours = str;
    str = paramParcel.readString();
    landline = str;
    str = paramParcel.readString();
    score = str;
    str = paramParcel.readString();
    swishNumber = str;
    if (paramBoolean) {
      paramParcel.recycle();
    }
  }
  
  public ContactDto$Contact$Business(Business paramBusiness)
  {
    this(paramBusiness, true);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Business{branch='");
    String str1 = branch;
    localStringBuilder.append(str1);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", department='");
    String str2 = department;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", companySize='");
    str2 = companySize;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", openingHours='");
    str2 = openingHours;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", landline='");
    str2 = landline;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", score='");
    str2 = score;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", swishNumber='");
    str2 = swishNumber;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    String str = branch;
    paramParcel.writeString(str);
    str = department;
    paramParcel.writeString(str);
    str = companySize;
    paramParcel.writeString(str);
    str = openingHours;
    paramParcel.writeString(str);
    str = landline;
    paramParcel.writeString(str);
    str = score;
    paramParcel.writeString(str);
    str = swishNumber;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Business
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */