package com.truecaller.search.global;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import c.g.b.k;
import com.truecaller.ui.components.d;
import com.truecaller.ui.components.d.c;

public final class aj
  extends d
{
  private final af c;
  
  public aj(af paramaf)
  {
    c = paramaf;
  }
  
  public final d.c a(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "parent");
    LayoutInflater localLayoutInflater = LayoutInflater.from(paramViewGroup.getContext());
    switch (paramInt)
    {
    default: 
      paramViewGroup = new java/lang/IllegalArgumentException;
      localObject = String.valueOf(paramInt);
      localObject = "Cannot create viewholder for view type ".concat((String)localObject);
      paramViewGroup.<init>((String)localObject);
      throw ((Throwable)paramViewGroup);
    case 2131363188: 
    case 2131363189: 
    case 2131363190: 
      localObject = new com/truecaller/search/global/aj$a;
      paramViewGroup = localLayoutInflater.inflate(2131559221, paramViewGroup, false);
      k.a(paramViewGroup, "inflater.inflate(R.layou…show_more, parent, false)");
      ((aj.a)localObject).<init>(paramViewGroup);
      return (d.c)localObject;
    case 2131363187: 
      localObject = new com/truecaller/search/global/b;
      paramViewGroup = localLayoutInflater.inflate(2131559016, paramViewGroup, false);
      ((b)localObject).<init>(paramViewGroup);
      return (d.c)localObject;
    case 2131363183: 
    case 2131363184: 
    case 2131363185: 
      localObject = new com/truecaller/search/global/aj$a;
      paramViewGroup = localLayoutInflater.inflate(2131559212, paramViewGroup, false);
      k.a(paramViewGroup, "inflater.inflate(R.layou…ult_label, parent, false)");
      ((aj.a)localObject).<init>(paramViewGroup);
      return (d.c)localObject;
    case 2131363182: 
      localObject = new com/truecaller/search/global/aj$a;
      paramViewGroup = localLayoutInflater.inflate(2131559030, paramViewGroup, false);
      k.a(paramViewGroup, "inflater.inflate(R.layou…indicator, parent, false)");
      ((aj.a)localObject).<init>(paramViewGroup);
      return (d.c)localObject;
    case 2131363180: 
    case 2131363181: 
    case 2131363186: 
      localObject = new com/truecaller/search/global/ai;
      paramViewGroup = localLayoutInflater.inflate(2131559005, paramViewGroup, false);
      k.a(paramViewGroup, "inflater.inflate(R.layou…_call_log, parent, false)");
      ((ai)localObject).<init>(paramViewGroup);
      return (d.c)localObject;
    }
    Object localObject = new com/truecaller/search/global/e;
    paramViewGroup = localLayoutInflater.inflate(2131558491, paramViewGroup, false);
    ((e)localObject).<init>(paramViewGroup);
    return (d.c)localObject;
  }
  
  public final void a(d.c paramc, int paramInt)
  {
    k.b(paramc, "holder");
    af localaf = c;
    paramc = (am)paramc;
    localaf.a(paramc, paramInt);
  }
  
  public final int getItemCount()
  {
    return c.a();
  }
  
  public final int getItemViewType(int paramInt)
  {
    return c.a(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */