package com.truecaller.search.global;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.view.View;
import android.widget.FrameLayout;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public class GlobalSearchResultActivity
  extends com.truecaller.ui.n
{
  private n a;
  
  public void onBackPressed()
  {
    n localn = a;
    if (localn != null)
    {
      a.h();
      return;
    }
    super.onBackPressed();
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    Settings.c(this);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    int i = aresId;
    setTheme(i);
    super.onCreate(paramBundle);
    Object localObject = new android/widget/FrameLayout;
    ((FrameLayout)localObject).<init>(this);
    int j = 2131362594;
    ((FrameLayout)localObject).setId(j);
    setContentView((View)localObject);
    if (paramBundle == null)
    {
      paramBundle = new com/truecaller/search/global/n;
      paramBundle.<init>();
      a = paramBundle;
      paramBundle = getSupportFragmentManager().a();
      localObject = a;
      paramBundle.b(j, (Fragment)localObject, "SEARCH_RESULT_TAG").c();
      return;
    }
    paramBundle = (n)getSupportFragmentManager().a("SEARCH_RESULT_TAG");
    a = paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.GlobalSearchResultActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */