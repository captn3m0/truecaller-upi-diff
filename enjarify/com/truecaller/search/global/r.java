package com.truecaller.search.global;

import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d
{
  private final p a;
  private final Provider b;
  
  private r(p paramp, Provider paramProvider)
  {
    a = paramp;
    b = paramProvider;
  }
  
  public static r a(p paramp, Provider paramProvider)
  {
    r localr = new com/truecaller/search/global/r;
    localr.<init>(paramp, paramProvider);
    return localr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */