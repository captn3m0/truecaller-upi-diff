package com.truecaller.search.global;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

final class n$3
  implements TextWatcher
{
  n$3(n paramn) {}
  
  public final void afterTextChanged(Editable paramEditable) {}
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    paramCharSequence = a.h.getText().toString().trim();
    a.a.b(paramCharSequence);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.n.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */