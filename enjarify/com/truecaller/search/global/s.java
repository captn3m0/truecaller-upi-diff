package com.truecaller.search.global;

import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d
{
  private final p a;
  private final Provider b;
  
  private s(p paramp, Provider paramProvider)
  {
    a = paramp;
    b = paramProvider;
  }
  
  public static s a(p paramp, Provider paramProvider)
  {
    s locals = new com/truecaller/search/global/s;
    locals.<init>(paramp, paramProvider);
    return locals;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */