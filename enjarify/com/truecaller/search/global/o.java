package com.truecaller.search.global;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.ads.AdLayoutType;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.ads.a.d;
import com.truecaller.ads.a.e;
import com.truecaller.ads.k.a;
import com.truecaller.ads.k.b;
import com.truecaller.ads.k.c;
import com.truecaller.analytics.az;
import com.truecaller.analytics.b;
import com.truecaller.analytics.bc;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callhistory.z;
import com.truecaller.i.c;
import com.truecaller.ui.ab;
import com.truecaller.ui.components.FeedbackItemView;
import com.truecaller.ui.components.FeedbackItemView.DisplaySource;
import com.truecaller.ui.components.FeedbackItemView.a;
import com.truecaller.ui.components.j;
import com.truecaller.ui.components.s;
import com.truecaller.ui.dialogs.d.a;
import com.truecaller.ui.q;
import com.truecaller.ui.r;
import com.truecaller.util.at;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class o
  extends r
  implements az, ab, d.a
{
  af a;
  private RecyclerView b;
  private TextView c;
  private com.truecaller.ui.components.p d;
  private j e;
  private e f;
  private com.truecaller.ads.a.a g;
  private RecyclerView.AdapterDataObserver h;
  private RecyclerView.Adapter i;
  private com.truecaller.androidactors.f l;
  private com.truecaller.androidactors.k m;
  private com.truecaller.androidactors.a n;
  private c o;
  private com.truecaller.i.a p;
  private final ContentObserver q;
  
  public o()
  {
    o.1 local1 = new com/truecaller/search/global/o$1;
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    local1.<init>(this, localHandler);
    q = local1;
  }
  
  private void a(z paramz)
  {
    n = null;
    Object localObject1 = d;
    if (localObject1 == null) {
      return;
    }
    localObject1 = c;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = q;
      ((Cursor)localObject1).unregisterContentObserver((ContentObserver)localObject2);
    }
    d.a(paramz);
    localObject1 = b.getAdapter();
    if (localObject1 == null)
    {
      localObject1 = b;
      localObject2 = i;
      ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
    }
    else
    {
      localObject1 = d;
      ((com.truecaller.ui.components.p)localObject1).notifyDataSetChanged();
    }
    if (paramz != null)
    {
      localObject1 = q;
      paramz.registerContentObserver((ContentObserver)localObject1);
    }
    C_();
    paramz = e;
    a(paramz);
  }
  
  private void g()
  {
    Object localObject = n;
    if (localObject != null) {
      ((com.truecaller.androidactors.a)localObject).a();
    }
    C_();
    localObject = ((com.truecaller.callhistory.a)l.a()).b(5);
    i locali = m.a();
    -..Lambda.o.NCoW_XBmYxdX7U8PF80bVAdxOgk localNCoW_XBmYxdX7U8PF80bVAdxOgk = new com/truecaller/search/global/-$$Lambda$o$NCoW_XBmYxdX7U8PF80bVAdxOgk;
    localNCoW_XBmYxdX7U8PF80bVAdxOgk.<init>(this);
    localObject = ((w)localObject).a(locali, localNCoW_XBmYxdX7U8PF80bVAdxOgk);
    n = ((com.truecaller.androidactors.a)localObject);
    localObject = e;
    a((j)localObject);
  }
  
  public final void C_()
  {
    boolean bool1 = isFinishing();
    if (!bool1)
    {
      bool1 = false;
      c(false);
      at.a(c, false);
      at.a(u(), false);
      ImageView localImageView = t();
      at.a(localImageView, false);
      Object localObject = n;
      boolean bool3 = true;
      if (localObject == null)
      {
        localObject = d;
        int j = ((com.truecaller.ui.components.p)localObject).getItemCount();
        if (j != 0) {
          return;
        }
        localObject = o;
        String str = "initialCallLogSyncComplete";
        boolean bool2 = ((c)localObject).b(str);
        if (bool2)
        {
          at.a(c, bool3);
          at.a(u(), bool3);
          at.a(t(), bool3);
          return;
        }
      }
      c(bool3);
    }
  }
  
  public final void a()
  {
    super.a();
    Object localObject1 = d;
    Object localObject2 = h;
    ((com.truecaller.ui.components.p)localObject1).unregisterAdapterDataObserver((RecyclerView.AdapterDataObserver)localObject2);
    f.d();
    localObject1 = d.c;
    if (localObject1 != null)
    {
      localObject2 = q;
      ((Cursor)localObject1).unregisterContentObserver((ContentObserver)localObject2);
    }
    localObject1 = d;
    localObject2 = null;
    ((com.truecaller.ui.components.p)localObject1).a(null);
    h = null;
    d = null;
    g = null;
    f = null;
    localObject1 = n;
    if (localObject1 != null)
    {
      ((com.truecaller.androidactors.a)localObject1).a();
      n = null;
    }
  }
  
  public final void a(String paramString)
  {
    paramString = TrueApp.y().a().c();
    bc localbc = new com/truecaller/analytics/bc;
    localbc.<init>("globalSearchHistory");
    paramString.a(localbc);
  }
  
  public final void a(boolean paramBoolean)
  {
    paramBoolean = isVisible();
    if (paramBoolean)
    {
      localObject = f;
      boolean bool = true;
      ((e)localObject).a(bool);
    }
    Object localObject = TimeUnit.SECONDS;
    com.truecaller.i.a locala = p;
    String str = "adFeatureRetentionTime";
    long l1 = 0L;
    long l2 = locala.a(str, l1);
    l2 = ((TimeUnit)localObject).toMillis(l2);
    paramBoolean = l2 < l1;
    if (!paramBoolean)
    {
      f.b();
      return;
    }
    f.a(l2);
  }
  
  public final TextView b()
  {
    return c;
  }
  
  public final FeedbackItemView.DisplaySource d()
  {
    return FeedbackItemView.DisplaySource.GLOBAL_SEARCH_HISTORY;
  }
  
  public final int f()
  {
    return 8;
  }
  
  public final void m()
  {
    RecyclerView localRecyclerView = b;
    if (localRecyclerView != null) {
      localRecyclerView.smoothScrollToPosition(0);
    }
  }
  
  public final void n()
  {
    boolean bool = isVisible();
    if (bool)
    {
      f.a(false);
      e locale = f;
      locale.e();
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = ((bk)paramContext.getApplicationContext()).a();
    Object localObject = paramContext.m();
    m = ((com.truecaller.androidactors.k)localObject);
    localObject = paramContext.ad();
    l = ((com.truecaller.androidactors.f)localObject);
    localObject = paramContext.D();
    o = ((c)localObject);
    paramContext = paramContext.as();
    p = paramContext;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity().getIntent();
    bp localbp = ((bk)getContext().getApplicationContext()).a();
    l.a locala = l.a().a(localbp);
    p localp = new com/truecaller/search/global/p;
    paramBundle = (CompositeAdapterDelegate.SearchResultOrder)paramBundle.getSerializableExtra("ARG_RESULT_ORDER");
    android.support.v4.app.f localf = getActivity();
    boolean bool = localbp.aP().a();
    localp.<init>(paramBundle, localf, bool);
    locala.a(localp).a();
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Object localObject1 = ((bk)paramViewGroup.getContext().getApplicationContext()).a();
    Object localObject2 = new com/truecaller/ui/components/p;
    Object localObject3 = getActivity();
    ((com.truecaller.ui.components.p)localObject2).<init>((Context)localObject3);
    d = ((com.truecaller.ui.components.p)localObject2);
    localObject2 = new com/truecaller/ui/components/j;
    localObject3 = d;
    ((j)localObject2).<init>((RecyclerView.Adapter)localObject3);
    e = ((j)localObject2);
    localObject2 = new com/truecaller/ads/a/f;
    localObject3 = ((bp)localObject1).aq();
    Object localObject4 = com.truecaller.ads.k.a().a("/43067329/A*Search_history*Native*GPS").b("HISTORY").c("searchHistory");
    int j = 1;
    CustomTemplate[] arrayOfCustomTemplate = new CustomTemplate[j];
    CustomTemplate localCustomTemplate = CustomTemplate.NATIVE_CONTENT_DUAL_TRACKER;
    arrayOfCustomTemplate[0] = localCustomTemplate;
    localObject4 = ((k.b)localObject4).a(arrayOfCustomTemplate).e();
    localObject1 = ((bp)localObject1).bl();
    ((com.truecaller.ads.a.f)localObject2).<init>((com.truecaller.ads.provider.f)localObject3, (com.truecaller.ads.k)localObject4, (c.d.f)localObject1);
    f = ((e)localObject2);
    localObject1 = new com/truecaller/ads/a/g;
    j localj = e;
    AdLayoutType localAdLayoutType = AdLayoutType.SMALL;
    d locald = new com/truecaller/ads/a/d;
    locald.<init>(j);
    e locale = f;
    ((com.truecaller.ads.a.g)localObject1).<init>(2131558487, localj, localAdLayoutType, locald, locale);
    g = ((com.truecaller.ads.a.a)localObject1);
    localObject1 = new com/truecaller/search/global/o$a;
    localObject2 = g;
    ((o.a)localObject1).<init>((RecyclerView.Adapter)localObject2);
    localObject2 = new com/truecaller/search/global/-$$Lambda$o$JrWH38iMTSM2kBz74WzdG0wu6FA;
    ((-..Lambda.o.JrWH38iMTSM2kBz74WzdG0wu6FA)localObject2).<init>(this);
    a = ((View.OnClickListener)localObject2);
    localObject3 = paramLayoutInflater;
    localObject4 = paramViewGroup;
    localObject2 = paramLayoutInflater.inflate(2131559201, paramViewGroup, false);
    localObject3 = (RecyclerView)((View)localObject2).findViewById(2131364092);
    b = ((RecyclerView)localObject3);
    localObject3 = b;
    localObject4 = new com/truecaller/search/global/o$2;
    ((o.2)localObject4).<init>(this);
    ((RecyclerView)localObject3).addOnScrollListener((RecyclerView.OnScrollListener)localObject4);
    localObject3 = (TextView)((View)localObject2).findViewById(2131363652);
    c = ((TextView)localObject3);
    i = ((RecyclerView.Adapter)localObject1);
    localObject1 = e;
    localObject3 = new com/truecaller/search/global/o$3;
    ((o.3)localObject3).<init>(this);
    c = ((FeedbackItemView.a)localObject3);
    return (View)localObject2;
  }
  
  public final void onHiddenChanged(boolean paramBoolean)
  {
    super.onHiddenChanged(paramBoolean);
    e locale1 = f;
    paramBoolean ^= true;
    locale1.a(paramBoolean);
    paramBoolean = isVisible();
    if (paramBoolean)
    {
      e locale2 = f;
      locale2.e();
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    g();
    j localj = e;
    if (localj != null)
    {
      FeedbackItemView localFeedbackItemView = b;
      if (localFeedbackItemView != null)
      {
        b.b();
        localFeedbackItemView = null;
        b = null;
      }
    }
  }
  
  public final void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Object localObject = b.getLayoutManager();
    if (localObject != null)
    {
      localObject = "STATE_LAYOUT_MANAGER";
      Parcelable localParcelable = b.getLayoutManager().onSaveInstanceState();
      paramBundle.putParcelable((String)localObject, localParcelable);
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramView = getString(2131886588);
    paramBundle = getString(2131886589);
    a(paramView, paramBundle, 2130969016);
    paramView = b;
    paramBundle = new com/truecaller/search/global/o$4;
    android.support.v4.app.f localf = getActivity();
    paramBundle.<init>(this, localf);
    paramView.setLayoutManager(paramBundle);
    b.setItemAnimator(null);
    paramView = new com/truecaller/ui/components/s;
    paramView.<init>();
    paramBundle = d;
    a.add(paramBundle);
    paramView = new com/truecaller/search/global/o$5;
    paramView.<init>(this);
    h = paramView;
    paramView = d;
    paramBundle = h;
    paramView.registerAdapterDataObserver(paramBundle);
    paramView = d;
    paramBundle = new com/truecaller/search/global/-$$Lambda$o$6lWXa8MGDOQu0UqswDGAKBpZUDA;
    paramBundle.<init>(this);
    a = paramBundle;
    paramView = new com/truecaller/ui/q;
    paramBundle = getContext();
    paramView.<init>(paramBundle, 2131559205);
    a = false;
    paramView.a();
    b.addItemDecoration(paramView);
    C_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */