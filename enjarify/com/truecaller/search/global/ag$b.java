package com.truecaller.search.global;

import java.lang.ref.WeakReference;

final class ag$b
  implements Runnable
{
  WeakReference a;
  
  ag$b(ag paramag)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramag);
    a = localWeakReference;
  }
  
  public final void run()
  {
    ag localag = (ag)a.get();
    if (localag != null) {
      localag.d(null);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.ag.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */