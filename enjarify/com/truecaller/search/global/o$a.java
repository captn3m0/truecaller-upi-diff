package com.truecaller.search.global;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.truecaller.ui.b;
import java.util.List;

final class o$a
  extends b
  implements View.OnClickListener
{
  View.OnClickListener a;
  
  public o$a(RecyclerView.Adapter paramAdapter)
  {
    super(paramAdapter);
  }
  
  private void a(RecyclerView.ViewHolder paramViewHolder)
  {
    itemView.setOnClickListener(this);
  }
  
  public final boolean d(int paramInt)
  {
    int i = 2131365484;
    return paramInt == i;
  }
  
  public final int getItemCount()
  {
    int i = super.getItemCount();
    if (i == 0) {
      return 0;
    }
    return i + 1;
  }
  
  public final long getItemId(int paramInt)
  {
    int i = super.getItemCount();
    if (paramInt == i) {
      return -1;
    }
    return super.getItemId(paramInt);
  }
  
  public final int getItemViewType(int paramInt)
  {
    int i = super.getItemCount();
    if (paramInt == i) {
      return 2131365484;
    }
    return super.getItemViewType(paramInt);
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    int i = paramViewHolder.getItemViewType();
    int j = 2131365484;
    if (i == j)
    {
      a(paramViewHolder);
      return;
    }
    super.onBindViewHolder(paramViewHolder, paramInt);
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt, List paramList)
  {
    int i = paramViewHolder.getItemViewType();
    int j = 2131365484;
    if (i == j)
    {
      a(paramViewHolder);
      return;
    }
    super.onBindViewHolder(paramViewHolder, paramInt, paramList);
  }
  
  public final void onClick(View paramView)
  {
    a.onClick(paramView);
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    LayoutInflater localLayoutInflater = LayoutInflater.from(paramViewGroup.getContext());
    int i = 2131365484;
    if (paramInt == i)
    {
      o.a.a locala = new com/truecaller/search/global/o$a$a;
      paramViewGroup = localLayoutInflater.inflate(2131559163, paramViewGroup, false);
      locala.<init>(paramViewGroup);
      return locala;
    }
    return super.onCreateViewHolder(paramViewGroup, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.o.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */