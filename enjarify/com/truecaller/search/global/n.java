package com.truecaller.search.global;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.truecaller.bp;
import com.truecaller.common.ui.a.b.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.essentialnumber.LocalServicesCategoryActivity;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.scanner.NumberDetectorProcessor.ScanType;
import com.truecaller.scanner.NumberScannerActivity;
import com.truecaller.scanner.s;
import com.truecaller.service.DataManagerService;
import com.truecaller.service.h;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.WizardActivity;
import com.truecaller.ui.components.EditBase;
import com.truecaller.ui.components.EditBase.a;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.q;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;
import com.truecaller.wizard.b.c;
import java.util.List;

public final class n
  extends Fragment
  implements ah
{
  af a;
  aj b;
  protected ViewGroup c;
  protected View d;
  protected EditBase e;
  protected ViewGroup f;
  protected View g;
  protected EditText h;
  protected TextView i;
  protected RecyclerView j;
  protected TextView k;
  private Toolbar l;
  private Toolbar m;
  private View n;
  private TextView o;
  private TextView p;
  private FrameLayout q;
  private boolean r;
  private h s;
  private Runnable t;
  
  private static Intent a(Activity paramActivity, String paramString1, String paramString2, boolean paramBoolean1, CompositeAdapterDelegate.SearchResultOrder paramSearchResultOrder, boolean paramBoolean2)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramActivity, GlobalSearchResultActivity.class);
    return localIntent.putExtra("ARG_SEARCH_TEXT", paramString1).putExtra("ARG_SEARCH_COUNTRY", paramString2).putExtra("ARG_FORCE_NUMBER_SEARCH", paramBoolean1).putExtra("ARG_SHOW_KEYBOARD", paramBoolean2).putExtra("ARG_RESULT_ORDER", paramSearchResultOrder);
  }
  
  public static Intent a(Context paramContext, String paramString)
  {
    return TruecallerInit.a(paramContext, "search", paramString).putExtra("ARG_SEARCH_RESULTS", true);
  }
  
  public static void a(Activity paramActivity)
  {
    CompositeAdapterDelegate.SearchResultOrder localSearchResultOrder = CompositeAdapterDelegate.SearchResultOrder.ORDER_CMT;
    a(paramActivity, null, null, false, localSearchResultOrder);
  }
  
  public static void a(Activity paramActivity, CompositeAdapterDelegate.SearchResultOrder paramSearchResultOrder)
  {
    a(paramActivity, null, null, false, paramSearchResultOrder);
  }
  
  public static void a(Activity paramActivity, String paramString)
  {
    CompositeAdapterDelegate.SearchResultOrder localSearchResultOrder = CompositeAdapterDelegate.SearchResultOrder.ORDER_CMT;
    a(paramActivity, paramString, null, false, localSearchResultOrder);
  }
  
  public static void a(Activity paramActivity, String paramString1, String paramString2)
  {
    CompositeAdapterDelegate.SearchResultOrder localSearchResultOrder = CompositeAdapterDelegate.SearchResultOrder.ORDER_CMT;
    a(paramActivity, paramString1, paramString2, false, localSearchResultOrder);
  }
  
  public static void a(Activity paramActivity, String paramString1, String paramString2, boolean paramBoolean, CompositeAdapterDelegate.SearchResultOrder paramSearchResultOrder)
  {
    boolean bool;
    if (paramString1 == null) {
      bool = true;
    } else {
      bool = false;
    }
    b(paramActivity, paramString1, paramString2, paramBoolean, paramSearchResultOrder, bool);
  }
  
  private void a(Toolbar paramToolbar)
  {
    ((AppCompatActivity)getActivity()).setSupportActionBar(paramToolbar);
    Object localObject = ((AppCompatActivity)getActivity()).getSupportActionBar();
    if (localObject != null)
    {
      ((ActionBar)localObject).setDisplayHomeAsUpEnabled(true);
      ((ActionBar)localObject).setDisplayShowTitleEnabled(false);
    }
    localObject = new com/truecaller/search/global/-$$Lambda$n$gmIZgT8nY7ZP1DRFqfL3pICEgdc;
    ((-..Lambda.n.gmIZgT8nY7ZP1DRFqfL3pICEgdc)localObject).<init>(this);
    paramToolbar.setNavigationOnClickListener((View.OnClickListener)localObject);
  }
  
  private static void b(Activity paramActivity, String paramString1, String paramString2, boolean paramBoolean1, CompositeAdapterDelegate.SearchResultOrder paramSearchResultOrder, boolean paramBoolean2)
  {
    paramString1 = a(paramActivity, paramString1, paramString2, paramBoolean1, paramSearchResultOrder, paramBoolean2);
    paramActivity.startActivity(paramString1);
  }
  
  public final void a()
  {
    n.4 local4 = new com/truecaller/search/global/n$4;
    local4.<init>(this);
    e.setCustomSelectionActionModeCallback(local4);
    h.setCustomSelectionActionModeCallback(local4);
  }
  
  public final void a(long paramLong1, long paramLong2)
  {
    Intent localIntent = new android/content/Intent;
    f localf = getActivity();
    localIntent.<init>(localf, ConversationActivity.class);
    localIntent.putExtra("conversation_id", paramLong1);
    localIntent.putExtra("message_id", paramLong2);
    startActivity(localIntent);
  }
  
  public final void a(Contact paramContact)
  {
    f localf = getActivity();
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.SearchResult;
    boolean bool = true;
    DetailsFragment.b(localf, paramContact, localSourceType, bool, bool);
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    k.setText(paramCharSequence);
  }
  
  public final void a(String paramString)
  {
    i.setText(paramString);
  }
  
  public final void a(List paramList)
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject1 = getActivity();
    localBuilder.<init>((Context)localObject1);
    localBuilder = localBuilder.setTitle(2131888737);
    localObject1 = new com/truecaller/scanner/s;
    Object localObject2 = getActivity();
    ((s)localObject1).<init>((Context)localObject2, paramList);
    localObject2 = new com/truecaller/search/global/-$$Lambda$n$HK1iJzxI99kXxXIdwKFA239DyCM;
    ((-..Lambda.n.HK1iJzxI99kXxXIdwKFA239DyCM)localObject2).<init>(this, paramList);
    localBuilder.setAdapter((ListAdapter)localObject1, (DialogInterface.OnClickListener)localObject2).create().show();
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localObject1 = new com/truecaller/search/global/o;
      ((o)localObject1).<init>();
      getChildFragmentManager().a().b(2131363274, (Fragment)localObject1, "TAG_HISTORY_FRAGMENT").f();
      localObject2 = a;
      a = ((af)localObject2);
      return;
    }
    Object localObject1 = getChildFragmentManager();
    Object localObject2 = "TAG_HISTORY_FRAGMENT";
    localObject1 = ((j)localObject1).a((String)localObject2);
    if (localObject1 != null)
    {
      localObject2 = getChildFragmentManager().a();
      localObject1 = ((android.support.v4.app.o)localObject2).a((Fragment)localObject1);
      ((android.support.v4.app.o)localObject1).f();
    }
  }
  
  public final void b()
  {
    Object localObject = getContext();
    NumberDetectorProcessor.ScanType localScanType = NumberDetectorProcessor.ScanType.SCAN_PHONE;
    localObject = NumberScannerActivity.a((Context)localObject, localScanType);
    startActivityForResult((Intent)localObject, 100);
  }
  
  public final void b(Contact paramContact)
  {
    f localf = getActivity();
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.SearchResult;
    DetailsFragment.b(localf, paramContact, localSourceType, true, false);
  }
  
  public final void b(CharSequence paramCharSequence)
  {
    o.setText(paramCharSequence);
  }
  
  public final void b(String paramString)
  {
    Toast.makeText(getContext(), paramString, 0).show();
  }
  
  public final void b(boolean paramBoolean)
  {
    e.setIsScannerEnabled(paramBoolean);
  }
  
  public final void c()
  {
    t.a(e, false, 2);
  }
  
  public final void c(CharSequence paramCharSequence)
  {
    p.setText(paramCharSequence);
  }
  
  public final void c(String paramString)
  {
    e.setText(paramString);
    paramString = e;
    int i1 = paramString.getText().length();
    paramString.setSelection(i1);
  }
  
  public final void c(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localObject = l;
      a((Toolbar)localObject);
    }
    Object localObject = n;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    ((View)localObject).setVisibility(paramBoolean);
  }
  
  public final void d()
  {
    t.a(e, true, 500L);
  }
  
  public final void d(String paramString)
  {
    f localf = getActivity();
    CompositeAdapterDelegate.SearchResultOrder localSearchResultOrder = CompositeAdapterDelegate.SearchResultOrder.ORDER_CMT;
    a(localf, paramString, null, true, localSearchResultOrder);
    f();
  }
  
  public final void d(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localToolbar = m;
      a(localToolbar);
    }
    Toolbar localToolbar = m;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localToolbar.setVisibility(paramBoolean);
  }
  
  public final void e()
  {
    j.scrollToPosition(0);
  }
  
  public final void e(boolean paramBoolean)
  {
    TextView localTextView = k;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localTextView.setVisibility(paramBoolean);
  }
  
  public final void f()
  {
    boolean bool1 = r;
    boolean bool2 = true;
    if (!bool1)
    {
      f localf = getActivity();
      if (localf != null)
      {
        localf = getActivity();
        bool1 = localf.isFinishing();
        if (!bool1)
        {
          bool1 = false;
          localf = null;
          break label46;
        }
      }
    }
    bool1 = true;
    label46:
    if (bool1) {
      return;
    }
    r = bool2;
    getActivity().finish();
  }
  
  public final void f(boolean paramBoolean)
  {
    RecyclerView localRecyclerView = j;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localRecyclerView.setVisibility(paramBoolean);
  }
  
  public final void g()
  {
    b.a locala = new com/truecaller/common/ui/a/b$a;
    Object localObject = getActivity();
    locala.<init>((Context)localObject);
    b = 2131886529;
    d = 2131887027;
    c = 2131231072;
    localObject = new com/truecaller/search/global/-$$Lambda$n$E3zD-vrKhDj0n06izB108AaLf5k;
    ((-..Lambda.n.E3zD-vrKhDj0n06izB108AaLf5k)localObject).<init>(this);
    e = ((AdapterView.OnItemClickListener)localObject);
    locala.c();
  }
  
  public final void g(boolean paramBoolean)
  {
    FrameLayout localFrameLayout = q;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localFrameLayout.setVisibility(paramBoolean);
  }
  
  public final void h(boolean paramBoolean)
  {
    g.setSelected(paramBoolean);
  }
  
  public final boolean h()
  {
    return g.isSelected();
  }
  
  public final void i()
  {
    b.notifyDataSetChanged();
  }
  
  public final void i(boolean paramBoolean)
  {
    View localView = d;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public final void j()
  {
    Object localObject = g;
    int i1 = 3;
    float[] arrayOfFloat = new float[i1];
    float[] tmp12_11 = arrayOfFloat;
    tmp12_11[0] = 1.0F;
    float[] tmp18_12 = tmp12_11;
    tmp18_12[1] = 0.2F;
    tmp18_12[2] = 1.0F;
    localObject = com.truecaller.util.bk.a(localObject, "alpha", arrayOfFloat);
    AccelerateDecelerateInterpolator localAccelerateDecelerateInterpolator = new android/view/animation/AccelerateDecelerateInterpolator;
    localAccelerateDecelerateInterpolator.<init>();
    localObject = ((com.truecaller.util.bk)localObject).a(localAccelerateDecelerateInterpolator).a(300L).b(1000L);
    a.setRepeatCount(i1);
    a.start();
  }
  
  public final void k()
  {
    Object localObject = t;
    if (localObject == null)
    {
      localObject = new com/truecaller/search/global/-$$Lambda$n$OHKAZTrkE0jBfXo8yxOVlJbQKuM;
      ((-..Lambda.n.OHKAZTrkE0jBfXo8yxOVlJbQKuM)localObject).<init>(this);
      t = ((Runnable)localObject);
    }
    localObject = e;
    Runnable localRunnable = t;
    ((EditBase)localObject).post(localRunnable);
  }
  
  public final void l()
  {
    c.a(getContext(), WizardActivity.class, "globalSearch");
    c();
    f();
  }
  
  public final boolean m()
  {
    EditBase localEditBase = e;
    int i1 = localEditBase.getVisibility();
    return i1 == 0;
  }
  
  public final void n()
  {
    Intent localIntent = LocalServicesCategoryActivity.a(getContext(), "globalSearch");
    startActivity(localIntent);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    a.a(paramInt1, paramInt2, paramIntent);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity().getIntent();
    Object localObject = ((com.truecaller.bk)getContext().getApplicationContext()).a();
    l.a locala = l.a().a((bp)localObject);
    p localp = new com/truecaller/search/global/p;
    paramBundle = (CompositeAdapterDelegate.SearchResultOrder)paramBundle.getSerializableExtra("ARG_RESULT_ORDER");
    f localf = getActivity();
    boolean bool = ((bp)localObject).aP().a();
    localp.<init>(paramBundle, localf, bool);
    locala.a(localp).a().a(this);
    paramBundle = new com/truecaller/service/h;
    localObject = getContext();
    paramBundle.<init>((Context)localObject, DataManagerService.class);
    s = paramBundle;
    paramBundle = new com/truecaller/search/global/aj;
    localObject = a;
    paramBundle.<init>((af)localObject);
    b = paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131559202, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    a.y_();
    s.b();
  }
  
  public final void onPause()
  {
    super.onPause();
    a.e();
  }
  
  public final void onResume()
  {
    super.onResume();
    a.c();
  }
  
  public final void onStart()
  {
    super.onStart();
    s.a();
    a.f();
  }
  
  public final void onStop()
  {
    super.onStop();
    Runnable localRunnable = t;
    if (localRunnable != null)
    {
      EditBase localEditBase = e;
      localEditBase.removeCallbacks(localRunnable);
    }
    s.b();
    a.g();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (Toolbar)paramView.findViewById(2131364269);
    l = paramBundle;
    paramBundle = paramView.findViewById(2131364270);
    n = paramBundle;
    paramBundle = (Toolbar)paramView.findViewById(2131363778);
    m = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131364892);
    o = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131364611);
    p = paramBundle;
    paramBundle = (ViewGroup)paramView.findViewById(2131364188);
    c = paramBundle;
    paramBundle = paramView.findViewById(2131364283);
    d = paramBundle;
    paramBundle = (EditBase)paramView.findViewById(2131364260);
    e = paramBundle;
    paramBundle = (ViewGroup)paramView.findViewById(2131363666);
    f = paramBundle;
    paramBundle = paramView.findViewById(2131362332);
    g = paramBundle;
    paramBundle = (EditText)paramView.findViewById(2131361991);
    h = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131364247);
    i = paramBundle;
    paramBundle = (RecyclerView)paramView.findViewById(2131364092);
    j = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131363652);
    k = paramBundle;
    paramView = (FrameLayout)paramView.findViewById(2131363692);
    q = paramView;
    paramView = l;
    a(paramView);
    paramView = q;
    paramBundle = new com/truecaller/search/global/-$$Lambda$n$84BJZ59TKQzZTSNfpC9elmsLy5o;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = (TextView)q.findViewById(2131363693);
    int i1 = 2130969528;
    at.e(paramView, i1);
    paramView = i;
    Object localObject = new com/truecaller/search/global/-$$Lambda$n$U9X4fDtuggFRL-sXDoaBvyrda38;
    ((-..Lambda.n.U9X4fDtuggFRL-sXDoaBvyrda38)localObject).<init>(this);
    paramView.setOnClickListener((View.OnClickListener)localObject);
    at.d(i, i1);
    paramView = g;
    paramBundle = new com/truecaller/search/global/-$$Lambda$n$eKAdyWIOUpiOU3wcrrUTei67czg;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = getContext();
    paramBundle = (ImageView)g;
    b.a(paramView, paramBundle, 2130969017);
    paramView = j;
    paramBundle = new android/support/v7/widget/LinearLayoutManager;
    localObject = getContext();
    paramBundle.<init>((Context)localObject);
    paramView.setLayoutManager(paramBundle);
    paramView = new com/truecaller/search/global/-$$Lambda$n$r30Acr7lQ15uQJrRBJ9fPweaw50;
    paramView.<init>(this);
    e.setOnEditorActionListener(paramView);
    paramBundle = e;
    localObject = new com/truecaller/search/global/-$$Lambda$n$uRvBiZr-tUgNiuPBox1DzHJOa20;
    ((-..Lambda.n.uRvBiZr-tUgNiuPBox1DzHJOa20)localObject).<init>(this);
    paramBundle.setOnScannerClickListener((EditBase.a)localObject);
    h.setOnEditorActionListener(paramView);
    paramView = e;
    paramBundle = new com/truecaller/search/global/n$1;
    paramBundle.<init>(this);
    paramView.addTextChangedListener(paramBundle);
    paramView = j;
    paramBundle = new com/truecaller/search/global/n$2;
    paramBundle.<init>(this);
    paramView.addOnScrollListener(paramBundle);
    paramView = h;
    paramBundle = new com/truecaller/search/global/n$3;
    paramBundle.<init>(this);
    paramView.addTextChangedListener(paramBundle);
    paramView = new com/truecaller/ui/q;
    paramBundle = getContext();
    paramView.<init>(paramBundle, 2131559205, 0);
    a = false;
    paramView.a();
    j.addItemDecoration(paramView);
    paramView = b;
    paramBundle = new com/truecaller/search/global/-$$Lambda$n$tvOD2d9fA9LrpWsbGCNBmf5lgEc;
    paramBundle.<init>(this);
    a = paramBundle;
    j.setAdapter(paramView);
    a.a(this);
    paramView = getActivity().getIntent();
    a.a(paramView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */