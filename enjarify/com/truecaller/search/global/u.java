package com.truecaller.search.global;

import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d
{
  private final p a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  
  private u(p paramp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8)
  {
    a = paramp;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
  }
  
  public static u a(p paramp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8)
  {
    u localu = new com/truecaller/search/global/u;
    localu.<init>(paramp, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8);
    return localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */