package com.truecaller.search.global;

public abstract interface CompositeAdapterDelegate
{
  public abstract c a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(af paramaf);
  
  public abstract c b();
  
  public abstract void b(int paramInt);
  
  public abstract c c();
  
  public abstract void c(int paramInt);
  
  public abstract c d();
  
  public abstract c e();
  
  public abstract CompositeAdapterDelegate.SearchResultOrder f();
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.CompositeAdapterDelegate
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */