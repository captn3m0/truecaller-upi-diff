package com.truecaller.search.global;

import android.content.Context;
import android.os.Handler;
import com.truecaller.data.access.c;
import com.truecaller.filters.FilterManager;
import com.truecaller.flashsdk.core.i;
import com.truecaller.messaging.conversation.bs;
import com.truecaller.messaging.conversation.bt;
import com.truecaller.multisim.ae;
import com.truecaller.multisim.h;
import com.truecaller.search.local.b.e;
import com.truecaller.utils.n;
import com.truecaller.utils.o;
import java.text.NumberFormat;

class p
{
  final int a = 3;
  final CompositeAdapterDelegate.SearchResultOrder b;
  final Handler c;
  final Context d;
  final boolean e;
  
  public p(CompositeAdapterDelegate.SearchResultOrder paramSearchResultOrder, Context paramContext, boolean paramBoolean)
  {
    b = paramSearchResultOrder;
    d = paramContext;
    paramSearchResultOrder = new android/os/Handler;
    paramSearchResultOrder.<init>();
    c = paramSearchResultOrder;
    e = paramBoolean;
  }
  
  static i a(com.truecaller.flashsdk.core.b paramb)
  {
    return paramb.e();
  }
  
  static bs a(o paramo, h paramh, ae paramae)
  {
    bt localbt = new com/truecaller/messaging/conversation/bt;
    boolean bool = paramh.j();
    localbt.<init>(paramo, bool, paramae);
    return localbt;
  }
  
  static com.truecaller.search.a.a a(com.truecaller.ads.provider.f paramf)
  {
    com.truecaller.search.a.b localb = new com/truecaller/search/a/b;
    localb.<init>(paramf);
    return localb;
  }
  
  static a a(n paramn)
  {
    a locala = new com/truecaller/search/global/a;
    locala.<init>(paramn);
    return locala;
  }
  
  static al a(FilterManager paramFilterManager, c paramc, c.d.f paramf1, c.d.f paramf2)
  {
    al localal = new com/truecaller/search/global/al;
    localal.<init>(paramFilterManager, paramc, paramf1, paramf2);
    return localal;
  }
  
  static e a()
  {
    e locale = new com/truecaller/search/local/b/e;
    locale.<init>();
    return locale;
  }
  
  static d b(n paramn)
  {
    d locald = new com/truecaller/search/global/d;
    locald.<init>(paramn);
    return locald;
  }
  
  static f b()
  {
    f localf = new com/truecaller/search/global/f;
    localf.<init>();
    return localf;
  }
  
  static NumberFormat c()
  {
    return NumberFormat.getInstance();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */