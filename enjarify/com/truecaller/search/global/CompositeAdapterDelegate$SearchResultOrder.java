package com.truecaller.search.global;

public enum CompositeAdapterDelegate$SearchResultOrder
{
  static
  {
    Object localObject = new com/truecaller/search/global/CompositeAdapterDelegate$SearchResultOrder;
    ((SearchResultOrder)localObject).<init>("ORDER_CMT", 0);
    ORDER_CMT = (SearchResultOrder)localObject;
    localObject = new com/truecaller/search/global/CompositeAdapterDelegate$SearchResultOrder;
    int i = 1;
    ((SearchResultOrder)localObject).<init>("ORDER_TCM", i);
    ORDER_TCM = (SearchResultOrder)localObject;
    localObject = new com/truecaller/search/global/CompositeAdapterDelegate$SearchResultOrder;
    int j = 2;
    ((SearchResultOrder)localObject).<init>("ORDER_MCT", j);
    ORDER_MCT = (SearchResultOrder)localObject;
    localObject = new com/truecaller/search/global/CompositeAdapterDelegate$SearchResultOrder;
    int k = 3;
    ((SearchResultOrder)localObject).<init>("ORDER_CTM", k);
    ORDER_CTM = (SearchResultOrder)localObject;
    localObject = new com/truecaller/search/global/CompositeAdapterDelegate$SearchResultOrder;
    int m = 4;
    ((SearchResultOrder)localObject).<init>("ORDER_MTC", m);
    ORDER_MTC = (SearchResultOrder)localObject;
    localObject = new SearchResultOrder[5];
    SearchResultOrder localSearchResultOrder = ORDER_CMT;
    localObject[0] = localSearchResultOrder;
    localSearchResultOrder = ORDER_TCM;
    localObject[i] = localSearchResultOrder;
    localSearchResultOrder = ORDER_MCT;
    localObject[j] = localSearchResultOrder;
    localSearchResultOrder = ORDER_CTM;
    localObject[k] = localSearchResultOrder;
    localSearchResultOrder = ORDER_MTC;
    localObject[m] = localSearchResultOrder;
    $VALUES = (SearchResultOrder[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.CompositeAdapterDelegate.SearchResultOrder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */