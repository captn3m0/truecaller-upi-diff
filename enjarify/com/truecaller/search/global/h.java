package com.truecaller.search.global;

import com.truecaller.log.AssertionUtil;
import com.truecaller.util.al;

final class h
  implements CompositeAdapterDelegate
{
  private final j a;
  private final k b;
  private final an c;
  private final a d;
  private final d e;
  private final f f;
  private final CompositeAdapterDelegate.SearchResultOrder g;
  private c h;
  private final al i;
  private final com.truecaller.util.b.j j;
  
  h(j paramj, k paramk, an paraman, a parama, d paramd, f paramf, CompositeAdapterDelegate.SearchResultOrder paramSearchResultOrder, al paramal, com.truecaller.util.b.j paramj1)
  {
    a = paramj;
    b = paramk;
    c = paraman;
    d = parama;
    e = paramd;
    f = paramf;
    g = paramSearchResultOrder;
    i = paramal;
    j = paramj1;
    paramj = h.1.a;
    paramk = g;
    int k = paramk.ordinal();
    int m = paramj[k];
    switch (m)
    {
    default: 
      paramj = a;
      break;
    case 5: 
      paramj = g();
      break;
    case 3: 
    case 4: 
      paramj = b;
      break;
    case 1: 
    case 2: 
      paramj = a;
    }
    h = paramj;
    paramj = h;
    paramk = new String[] { "Main Adapter is not assigned." };
    AssertionUtil.isNotNull(paramj, paramk);
    paramj = h.1.a;
    paramk = g;
    k = paramk.ordinal();
    m = paramj[k];
    switch (m)
    {
    default: 
      m = 0;
      paramj = null;
      break;
    case 5: 
      paramj = a;
      paramk = b;
      paramj.a(paramk);
      paramj = a;
      break;
    case 4: 
      paramj = g();
      paramk = a;
      paramj.a(paramk);
      paramj = g();
      break;
    case 3: 
      paramj = a;
      paramk = g();
      paramj.a(paramk);
      paramj = a;
      break;
    case 2: 
      paramj = g();
      paramk = b;
      paramj.a(paramk);
      paramj = g();
      break;
    case 1: 
      paramj = b;
      paramk = g();
      paramj.a(paramk);
      paramj = b;
    }
    e.a(paramj);
    paramj = h;
    paramk = e;
    paramj.a(paramk);
  }
  
  private g g()
  {
    Object localObject = j;
    boolean bool = ((com.truecaller.util.b.j)localObject).b();
    if (!bool) {
      return f;
    }
    localObject = i;
    bool = ((al)localObject).a();
    if (bool) {
      return c;
    }
    return d;
  }
  
  public final c a()
  {
    return h;
  }
  
  public final void a(int paramInt)
  {
    a.c(paramInt);
  }
  
  public final void a(af paramaf)
  {
    a.a(paramaf);
    b.a(paramaf);
    c.a(paramaf);
    e.a(paramaf);
    e.a(paramaf);
  }
  
  public final c b()
  {
    return a;
  }
  
  public final void b(int paramInt)
  {
    b.c(paramInt);
  }
  
  public final c c()
  {
    return b;
  }
  
  public final void c(int paramInt)
  {
    c.c(paramInt);
  }
  
  public final c d()
  {
    return g();
  }
  
  public final c e()
  {
    return e;
  }
  
  public final CompositeAdapterDelegate.SearchResultOrder f()
  {
    return g;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */