package com.truecaller.search.global;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final p a;
  private final Provider b;
  
  private w(p paramp, Provider paramProvider)
  {
    a = paramp;
    b = paramProvider;
  }
  
  public static w a(p paramp, Provider paramProvider)
  {
    w localw = new com/truecaller/search/global/w;
    localw.<init>(paramp, paramProvider);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */