package com.truecaller.search.global;

import android.view.View;
import android.view.ViewGroup;
import com.truecaller.ui.components.d.c;

final class e
  extends d.c
  implements am
{
  final ViewGroup a;
  private String c;
  private boolean d;
  
  e(View paramView)
  {
    super(paramView);
    paramView = (ViewGroup)paramView.findViewById(2131362562);
    a = paramView;
  }
  
  public final String a()
  {
    return c;
  }
  
  public final boolean b()
  {
    return d;
  }
  
  public final void c_(String paramString)
  {
    c = paramString;
  }
  
  public final void c_(boolean paramBoolean)
  {
    d = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */