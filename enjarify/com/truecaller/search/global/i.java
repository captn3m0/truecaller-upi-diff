package com.truecaller.search.global;

import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.g;

public final class i
{
  final Contact a;
  final String b;
  final g c;
  
  public i(Contact paramContact, String paramString, g paramg)
  {
    a = paramContact;
    b = paramString;
    c = paramg;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof i;
      if (bool1)
      {
        paramObject = (i)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = c;
            paramObject = c;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final int hashCode()
  {
    Contact localContact = a;
    int i = 0;
    if (localContact != null)
    {
      j = localContact.hashCode();
    }
    else
    {
      j = 0;
      localContact = null;
    }
    j *= 31;
    Object localObject = b;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = c;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ContactWithFilterMatch(contact=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", matchedValue=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", filterMatch=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */