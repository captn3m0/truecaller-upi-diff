package com.truecaller.search.global;

import c.d.a.a;
import c.g.a.m;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.g;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class al$b$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  al$b$a(al.b paramb, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/search/global/al$b$a;
    al.b localb = b;
    locala.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = (Iterable)b.c;
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        localObject1 = (Collection)localObject1;
        paramObject = ((Iterable)paramObject).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)paramObject).hasNext();
          if (!bool2) {
            break;
          }
          Object localObject2 = (n)((Iterator)paramObject).next();
          Contact localContact = (Contact)a;
          localObject2 = (String)b;
          localContact = al.b(b.b).a(localContact);
          Object localObject3 = null;
          if (localContact != null)
          {
            c.g.b.k.a(localContact, "aggregatedContactDao.get…?: return@mapNotNull null");
            Object localObject4 = localContact.r();
            if (localObject4 != null)
            {
              localObject3 = al.c(b.b);
              c.g.b.k.a(localObject4, "it");
              String str1 = ((Number)localObject4).d();
              String str2 = ((Number)localObject4).a();
              localObject4 = ((Number)localObject4).l();
              boolean bool3 = true;
              localObject3 = ((FilterManager)localObject3).a(str1, str2, (String)localObject4, bool3);
            }
            localObject4 = new com/truecaller/search/global/i;
            ((i)localObject4).<init>(localContact, (String)localObject2, (g)localObject3);
            localObject3 = localObject4;
          }
          if (localObject3 != null) {
            ((Collection)localObject1).add(localObject3);
          }
        }
        return (List)localObject1;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.al.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */