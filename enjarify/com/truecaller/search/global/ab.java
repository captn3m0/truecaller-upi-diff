package com.truecaller.search.global;

import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d
{
  private final p a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private ab(p paramp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramp;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static ab a(p paramp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    ab localab = new com/truecaller/search/global/ab;
    localab.<init>(paramp, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localab;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */