package com.truecaller.search.global;

import dagger.a.d;
import javax.inject.Provider;

public final class ae
  implements d
{
  private final p a;
  private final Provider b;
  
  private ae(p paramp, Provider paramProvider)
  {
    a = paramp;
    b = paramProvider;
  }
  
  public static ae a(p paramp, Provider paramProvider)
  {
    ae localae = new com/truecaller/search/global/ae;
    localae.<init>(paramp, paramProvider);
    return localae;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */