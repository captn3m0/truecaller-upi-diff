package com.truecaller.search.global;

import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d
{
  private final p a;
  private final Provider b;
  
  private v(p paramp, Provider paramProvider)
  {
    a = paramp;
    b = paramProvider;
  }
  
  public static v a(p paramp, Provider paramProvider)
  {
    v localv = new com/truecaller/search/global/v;
    localv.<init>(paramp, paramProvider);
    return localv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */