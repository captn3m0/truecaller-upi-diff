package com.truecaller.search.global;

import com.truecaller.log.AssertionUtil;
import com.truecaller.ui.components.d.c;
import java.util.Locale;

abstract class g
  implements c
{
  protected af a;
  private int b = 0;
  private int c;
  private int d;
  private boolean e;
  private c f;
  
  g(int paramInt)
  {
    c = paramInt;
    paramInt = Math.min(b, paramInt);
    d = paramInt;
  }
  
  private int e(int paramInt)
  {
    int i = g();
    i = paramInt - i;
    if (i < 0)
    {
      Object localObject1 = Locale.ENGLISH;
      int j = 4;
      Object[] arrayOfObject = new Object[j];
      Object localObject2 = getClass().getSimpleName();
      arrayOfObject[0] = localObject2;
      Object localObject3 = Integer.valueOf(paramInt);
      arrayOfObject[1] = localObject3;
      localObject2 = Integer.valueOf(g());
      arrayOfObject[2] = localObject2;
      paramInt = 3;
      boolean bool = e;
      localObject2 = Boolean.valueOf(bool);
      arrayOfObject[paramInt] = localObject2;
      localObject3 = String.format((Locale)localObject1, "::getInnerPos:: %s  Original Adapter pos: %d adapter count: %d  is loading shown: %s", arrayOfObject);
      localObject1 = new java/lang/IllegalArgumentException;
      String str = " Position is -1. ";
      localObject3 = String.valueOf(localObject3);
      localObject3 = str.concat((String)localObject3);
      ((IllegalArgumentException)localObject1).<init>((String)localObject3);
      localObject3 = new String[0];
      AssertionUtil.shouldNeverHappen((Throwable)localObject1, (String[])localObject3);
    }
    return i;
  }
  
  private int g()
  {
    int i = h();
    int k = 1;
    if (i == 0)
    {
      i = e;
      if (i != 0) {
        return k;
      }
    }
    i = e;
    int m = d;
    int j;
    i += m;
    int n = i();
    j += n;
    int i1 = d;
    if (i1 == 0)
    {
      boolean bool = e;
      if (!bool)
      {
        int i2 = c();
        if (i2 != 0) {
          break label77;
        }
      }
    }
    k = 0;
    label77:
    return j + k;
  }
  
  private boolean h()
  {
    int i = c;
    int j = -1 >>> 1;
    return i == j;
  }
  
  private boolean i()
  {
    int i = b;
    int j = c;
    return i > j;
  }
  
  private void j()
  {
    int i = b;
    int j = c;
    i = Math.min(i, j);
    d = i;
  }
  
  abstract int a();
  
  public final int a(int paramInt)
  {
    int i = 1;
    if (paramInt >= 0)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localStringBuilder = null;
    }
    String[] arrayOfString = new String[i];
    String str1 = String.valueOf(paramInt);
    String str2 = " Position was ".concat(str1);
    arrayOfString[0] = str2;
    AssertionUtil.isTrue(bool2, arrayOfString);
    boolean bool2 = e;
    if (bool2)
    {
      k = d;
      if (paramInt == k) {
        return d();
      }
    }
    int k = c();
    if (paramInt == 0)
    {
      int n = d;
      if ((n == 0) && (k > 0)) {
        return k;
      }
    }
    boolean bool3 = e;
    int m;
    if (bool3) {
      m = d + i;
    } else {
      m = d;
    }
    if (paramInt == m)
    {
      boolean bool1 = i();
      if (bool1) {
        return b();
      }
    }
    int j = d;
    if (paramInt < j) {
      return a();
    }
    Object localObject1 = f;
    if (localObject1 != null)
    {
      m = e(paramInt);
      j = ((c)localObject1).a(m);
      if (j > 0) {
        return j;
      }
    }
    localObject1 = new java/lang/IllegalStateException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("View type for position ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" not handled. In adapter: ");
    localStringBuilder.append(this);
    Object localObject2 = localStringBuilder.toString();
    ((IllegalStateException)localObject1).<init>((String)localObject2);
    localObject2 = new String[0];
    AssertionUtil.shouldNeverHappen((Throwable)localObject1, (String[])localObject2);
    return 0;
  }
  
  public final void a(af paramaf)
  {
    a = paramaf;
  }
  
  public final void a(am paramam, int paramInt)
  {
    int i = a(paramInt);
    boolean bool2 = e;
    int k = 1;
    Object localObject1;
    if (!bool2)
    {
      localObject1 = a;
      if (localObject1 != null)
      {
        j = 2131363182;
        if (i != j)
        {
          j = g() - k;
          if (paramInt == j)
          {
            localObject1 = a;
            ((af)localObject1).a(this);
          }
        }
      }
    }
    int j = g() - k;
    if (paramInt != j) {
      k = 0;
    }
    paramam.c_(k);
    if (paramInt == 0)
    {
      localObject1 = e();
    }
    else
    {
      j = 0;
      localObject1 = null;
    }
    paramam.c_((String)localObject1);
    j = d();
    if (i != j)
    {
      j = c();
      if (i != j)
      {
        j = b();
        if (i != j)
        {
          j = a();
          if (i == j)
          {
            localObject2 = paramam;
            localObject2 = (d.c)paramam;
            boolean bool1 = a((d.c)localObject2, paramInt);
            if (bool1) {
              return;
            }
          }
          Object localObject2 = f;
          if (localObject2 != null)
          {
            paramInt = e(paramInt);
            ((c)localObject2).a(paramam, paramInt);
          }
          return;
        }
      }
    }
  }
  
  public final void a(c paramc)
  {
    boolean bool;
    if (paramc == this) {
      bool = true;
    } else {
      bool = false;
    }
    String[] arrayOfString = { "You cannot nest an adapter inside itself" };
    AssertionUtil.isFalse(bool, arrayOfString);
    f = paramc;
  }
  
  public final void a(boolean paramBoolean)
  {
    e = paramBoolean;
  }
  
  abstract boolean a(d.c paramc, int paramInt);
  
  abstract int b();
  
  public final int b(int paramInt)
  {
    int i = g();
    if (paramInt < i) {
      return paramInt;
    }
    Object localObject1 = f;
    if (localObject1 != null)
    {
      int j = g();
      paramInt -= j;
      return ((c)localObject1).b(paramInt);
    }
    localObject1 = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Position ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append("cannot be mapped as the delegate position.");
    Object localObject2 = localStringBuilder.toString();
    ((IllegalArgumentException)localObject1).<init>((String)localObject2);
    localObject2 = new String[0];
    AssertionUtil.shouldNeverHappen((Throwable)localObject1, (String[])localObject2);
    return -1;
  }
  
  abstract int c();
  
  public final void c(int paramInt)
  {
    b = paramInt;
    j();
  }
  
  abstract int d();
  
  public final void d(int paramInt)
  {
    c = paramInt;
    j();
  }
  
  abstract String e();
  
  public final int f()
  {
    int i = g();
    boolean bool = h();
    if (!bool)
    {
      localc = f;
      if (localc != null)
      {
        j = localc.f();
        break label37;
      }
    }
    int j = 0;
    c localc = null;
    label37:
    return i + j;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */