package com.truecaller.search.global;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final p a;
  private final Provider b;
  
  private q(p paramp, Provider paramProvider)
  {
    a = paramp;
    b = paramProvider;
  }
  
  public static q a(p paramp, Provider paramProvider)
  {
    q localq = new com/truecaller/search/global/q;
    localq.<init>(paramp, paramProvider);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */