package com.truecaller.search.global;

import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private y(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static y a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    y localy = new com/truecaller/search/global/y;
    localy.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */