package com.truecaller.search.global;

import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d
{
  private final p a;
  private final Provider b;
  
  private z(p paramp, Provider paramProvider)
  {
    a = paramp;
    b = paramProvider;
  }
  
  public static z a(p paramp, Provider paramProvider)
  {
    z localz = new com/truecaller/search/global/z;
    localz.<init>(paramp, paramProvider);
    return localz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */