package com.truecaller.search.global;

import android.view.View;
import android.widget.TextView;
import com.truecaller.ui.components.d.c;

abstract class ak
  extends d.c
  implements am
{
  final TextView a;
  private String c;
  private boolean d;
  
  ak(View paramView)
  {
    super(paramView);
    paramView = (TextView)paramView.findViewById(2131363718);
    a = paramView;
  }
  
  public final String a()
  {
    return c;
  }
  
  public final boolean b()
  {
    return d;
  }
  
  public final void c_(String paramString)
  {
    c = paramString;
  }
  
  public final void c_(boolean paramBoolean)
  {
    d = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */