package com.truecaller.search.global;

import com.truecaller.data.entity.Contact;
import java.util.List;

abstract interface ah
{
  public abstract void a();
  
  public abstract void a(long paramLong1, long paramLong2);
  
  public abstract void a(Contact paramContact);
  
  public abstract void a(CharSequence paramCharSequence);
  
  public abstract void a(String paramString);
  
  public abstract void a(List paramList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(Contact paramContact);
  
  public abstract void b(CharSequence paramCharSequence);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(CharSequence paramCharSequence);
  
  public abstract void c(String paramString);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d();
  
  public abstract void d(String paramString);
  
  public abstract void d(boolean paramBoolean);
  
  public abstract void e();
  
  public abstract void e(boolean paramBoolean);
  
  public abstract void f();
  
  public abstract void f(boolean paramBoolean);
  
  public abstract void g();
  
  public abstract void g(boolean paramBoolean);
  
  public abstract void h(boolean paramBoolean);
  
  public abstract boolean h();
  
  public abstract void i();
  
  public abstract void i(boolean paramBoolean);
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract boolean m();
  
  public abstract void n();
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */