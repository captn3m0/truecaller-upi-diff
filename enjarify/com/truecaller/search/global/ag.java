package com.truecaller.search.global;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.truecaller.ads.AdLayoutType;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.calling.r;
import com.truecaller.calling.x;
import com.truecaller.common.h.am;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.data.access.i.a;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.FilterManager.FilterAction;
import com.truecaller.filters.p;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.conversation.bs;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.history.HistoryTransportInfo;
import com.truecaller.network.search.l;
import com.truecaller.search.local.model.c.a;
import com.truecaller.tracking.events.k;
import com.truecaller.tracking.events.k.a;
import com.truecaller.util.ad;
import com.truecaller.util.at;
import com.truecaller.util.q;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

final class ag
  extends af
  implements i.a, com.truecaller.search.a.c, al.a
{
  private CancellationSignal A;
  private c B;
  private boolean C;
  private final List D;
  private final List E;
  private final List F;
  private final boolean G;
  private boolean H;
  private String I;
  private String J;
  private String K;
  private CountryListDto.a L;
  private CountryListDto.a M;
  private String N;
  private String O;
  private CountryListDto.a P;
  private AsyncTask Q;
  private final Runnable R;
  private com.truecaller.androidactors.a S;
  private final f T;
  private final com.truecaller.common.g.a U;
  private final com.truecaller.util.af V;
  private final com.truecaller.flashsdk.core.i W;
  private final com.truecaller.search.local.b.e X;
  private final bs Y;
  final com.truecaller.data.access.i a;
  final f c;
  final com.truecaller.androidactors.i d;
  final Handler e;
  final com.truecaller.utils.o f;
  final NumberFormat g;
  final Date h;
  final Runnable i;
  com.truecaller.androidactors.a j;
  boolean k;
  private final com.truecaller.utils.d l;
  private final com.truecaller.util.al m;
  private final l n;
  private final CompositeAdapterDelegate o;
  private final int p;
  private final b q;
  private final com.truecaller.search.local.model.c r;
  private final ad s;
  private final h t;
  private final com.truecaller.i.e u;
  private final al v;
  private final p w;
  private final com.truecaller.search.a.a x;
  private final com.truecaller.util.b.j y;
  private final com.truecaller.data.entity.g z;
  
  ag(com.truecaller.utils.d paramd, com.truecaller.util.al paramal, l paraml, com.truecaller.data.access.i parami, f paramf1, com.truecaller.androidactors.i parami1, com.truecaller.utils.o paramo, int paramInt, com.truecaller.search.local.b.e parame, CompositeAdapterDelegate paramCompositeAdapterDelegate, com.truecaller.search.local.model.c paramc, Handler paramHandler, NumberFormat paramNumberFormat, ad paramad, b paramb, h paramh, al paramal1, p paramp, com.truecaller.search.a.a parama, com.truecaller.util.b.j paramj, com.truecaller.i.e parame1, boolean paramBoolean, f paramf2, com.truecaller.common.g.a parama1, com.truecaller.util.af paramaf, com.truecaller.flashsdk.core.i parami2, com.truecaller.data.entity.g paramg, bs parambs)
  {
    Object localObject = new android/os/CancellationSignal;
    ((CancellationSignal)localObject).<init>();
    A = ((CancellationSignal)localObject);
    localObject = Calendar.getInstance(Locale.US);
    ((Calendar)localObject).set(1, 2015);
    ((Calendar)localObject).set(2, 8);
    ((Calendar)localObject).set(5, 7);
    ((Calendar)localObject).set(11, 0);
    ((Calendar)localObject).set(12, 0);
    localObject = ((Calendar)localObject).getTime();
    h = ((Date)localObject);
    C = false;
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    D = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    E = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    F = ((List)localObject);
    H = false;
    I = "";
    J = "";
    localObject = new com/truecaller/search/global/ag$b;
    ((ag.b)localObject).<init>(this);
    R = ((Runnable)localObject);
    localObject = new com/truecaller/search/global/ag$a;
    ((ag.a)localObject).<init>(this);
    i = ((Runnable)localObject);
    k = false;
    localObject = paramd;
    l = paramd;
    localObject = paramal;
    m = paramal;
    localObject = paraml;
    n = paraml;
    localObject = parami;
    a = parami;
    localObject = paramf1;
    c = paramf1;
    localObject = parami1;
    d = parami1;
    localObject = paramo;
    f = paramo;
    p = paramInt;
    localObject = parame;
    X = parame;
    localObject = paramCompositeAdapterDelegate;
    o = paramCompositeAdapterDelegate;
    localObject = paramc;
    r = paramc;
    localObject = paramNumberFormat;
    g = paramNumberFormat;
    localObject = paramad;
    s = paramad;
    localObject = paramb;
    q = paramb;
    localObject = paramh;
    t = paramh;
    localObject = paramal1;
    v = paramal1;
    localObject = paramp;
    w = paramp;
    o.a(this);
    localObject = o.a();
    B = ((c)localObject);
    localObject = paramHandler;
    e = paramHandler;
    localObject = parama;
    x = parama;
    localObject = paramj;
    y = paramj;
    G = paramBoolean;
    localObject = parame1;
    u = parame1;
    localObject = paramf2;
    T = paramf2;
    localObject = parama1;
    U = parama1;
    localObject = paramaf;
    V = paramaf;
    localObject = parami2;
    W = parami2;
    localObject = paramg;
    z = paramg;
    localObject = parambs;
    Y = parambs;
  }
  
  private Contact a(int paramInt1, int paramInt2)
  {
    Object localObject1;
    if (paramInt2 < 0)
    {
      localObject1 = new java/lang/IllegalStateException;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Adapter position was -1 nesting order: ");
      Object localObject3 = o.f();
      ((StringBuilder)localObject2).append(localObject3);
      ((StringBuilder)localObject2).append(" showing all results ? ");
      boolean bool = C;
      ((StringBuilder)localObject2).append(bool);
      ((StringBuilder)localObject2).append(" main adapter: ");
      localObject3 = o.a().getClass().getSimpleName();
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      ((IllegalStateException)localObject1).<init>((String)localObject2);
      localObject2 = new String[0];
      AssertionUtil.shouldNeverHappen((Throwable)localObject1, (String[])localObject2);
      return null;
    }
    int i1 = 2131363180;
    if (paramInt1 != i1)
    {
      i1 = 2131363186;
      if (paramInt1 != i1) {
        return null;
      }
      localObject1 = E;
      paramInt2 = B.b(paramInt2);
      return (Contact)((List)localObject1).get(paramInt2);
    }
    paramInt1 = B.b(paramInt2);
    return F.get(paramInt1)).a;
  }
  
  private String a(boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    com.truecaller.utils.o localo;
    Object localObject;
    if (paramBoolean1)
    {
      localo = f;
      Object[] arrayOfObject = new Object[1];
      localObject = Integer.valueOf(paramInt);
      arrayOfObject[0] = localObject;
      return localo.a(2131886146, arrayOfObject);
    }
    if (paramBoolean2)
    {
      localo = f;
      localObject = new Object[0];
      return localo.a(2131886143, (Object[])localObject);
    }
    return null;
  }
  
  private void a(CountryListDto.a parama, boolean paramBoolean)
  {
    Object localObject = b;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localObject, arrayOfString);
    localObject = L;
    if (localObject == null)
    {
      localObject = s.b();
      L = ((CountryListDto.a)localObject);
    }
    if (parama == null) {
      parama = L;
    }
    M = parama;
    if (paramBoolean)
    {
      parama = M;
      L = parama;
    }
    parama = (ah)b;
    String str = s().toUpperCase();
    parama.a(str);
  }
  
  private void a(Contact paramContact, String paramString)
  {
    if (paramContact != null)
    {
      Object localObject = b;
      if (localObject != null)
      {
        localObject = (ah)b;
        ((ah)localObject).c();
        boolean bool = paramContact.S();
        if (bool)
        {
          localObject = (ah)b;
          ((ah)localObject).b(paramContact);
        }
        else
        {
          localObject = (ah)b;
          ((ah)localObject).a(paramContact);
        }
        int i2 = k;
        int i1 = 2;
        if (i2 == i1) {
          paramString = "cloudContact";
        }
        e(paramString);
        return;
      }
    }
  }
  
  private void a(String paramString, com.google.gson.o paramo)
  {
    com.truecaller.analytics.ae localae = (com.truecaller.analytics.ae)T.a();
    paramString = k.b().a(paramString);
    paramo = paramo.toString();
    paramString = paramString.b(paramo).a();
    localae.a(paramString);
  }
  
  private void a(boolean paramBoolean)
  {
    boolean bool1 = C;
    Object localObject;
    if (!bool1)
    {
      localObject = I;
      int i1 = am.f((CharSequence)localObject);
      int i2 = 2;
      if (i1 > i2)
      {
        localObject = E;
        boolean bool2 = ((List)localObject).isEmpty();
        if (!bool2) {}
      }
    }
    else
    {
      localObject = o.d();
      ((c)localObject).a(paramBoolean);
      ah localah = (ah)b;
      localah.i();
    }
  }
  
  private void b(List paramList)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    E.clear();
    E.addAll(paramList);
    paramList = o;
    int i1 = E.size();
    paramList.c(i1);
    ((ah)b).i();
  }
  
  private void b(boolean paramBoolean)
  {
    Object localObject = e;
    Runnable localRunnable = i;
    ((Handler)localObject).removeCallbacks(localRunnable);
    if (paramBoolean)
    {
      Handler localHandler = e;
      localObject = i;
      localHandler.post((Runnable)localObject);
    }
  }
  
  private void c(List paramList)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    F.clear();
    F.addAll(paramList);
    paramList = o;
    int i1 = F.size();
    paramList.a(i1);
    ((ah)b).i();
  }
  
  private void d(int paramInt)
  {
    boolean bool = true;
    C = bool;
    ((ah)b).c();
    ((ah)b).d(bool);
    ((ah)b).c(false);
    ah localah = (ah)b;
    Object localObject = f;
    Object[] arrayOfObject1 = new Object[bool];
    String str1 = I;
    arrayOfObject1[0] = str1;
    localObject = ((com.truecaller.utils.o)localObject).a(2131888159, arrayOfObject1);
    localah.b((CharSequence)localObject);
    localah = (ah)b;
    localObject = f;
    Object[] arrayOfObject2 = new Object[bool];
    arrayOfObject1 = new Object[0];
    String str2 = ((com.truecaller.utils.o)localObject).a(paramInt, arrayOfObject1);
    arrayOfObject2[0] = str2;
    str2 = ((com.truecaller.utils.o)localObject).a(2131888158, arrayOfObject2);
    localah.c(str2);
    ((ah)b).e();
    ((ah)b).i();
  }
  
  private void d(List paramList)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    D.clear();
    D.addAll(paramList);
    paramList = o;
    int i1 = D.size();
    paramList.b(i1);
    ((ah)b).i();
  }
  
  private void e(String paramString)
  {
    b localb = q;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("ViewAction");
    paramString = locala.a("Action", "details").a("Context", "searchResults").a("SubAction", paramString).a();
    localb.a(paramString);
  }
  
  private void p()
  {
    q();
    r();
    Object localObject = y;
    boolean bool1 = ((com.truecaller.util.b.j)localObject).b();
    if (!bool1) {
      return;
    }
    localObject = Q;
    if (localObject != null)
    {
      boolean bool2 = true;
      ((AsyncTask)localObject).cancel(bool2);
      bool1 = false;
      localObject = null;
      Q = null;
    }
    localObject = e;
    Runnable localRunnable = R;
    ((Handler)localObject).removeCallbacks(localRunnable);
    localObject = m;
    bool1 = ((com.truecaller.util.al)localObject).a();
    if (bool1)
    {
      localObject = e;
      localRunnable = R;
      long l1 = 1000L;
      ((Handler)localObject).postDelayed(localRunnable, l1);
    }
  }
  
  private void q()
  {
    A.cancel();
    Object localObject = I;
    boolean bool = am.b((CharSequence)localObject);
    if (bool)
    {
      localObject = Collections.emptyList();
      c((List)localObject);
      return;
    }
    localObject = a;
    String str = I;
    Integer localInteger = Integer.valueOf(100);
    localObject = ((com.truecaller.data.access.i)localObject).a(str, localInteger, this);
    A = ((CancellationSignal)localObject);
  }
  
  private void r()
  {
    Object localObject1 = I;
    boolean bool = am.b((CharSequence)localObject1);
    if (bool)
    {
      localObject1 = Collections.emptyList();
      d((List)localObject1);
      return;
    }
    localObject1 = (com.truecaller.messaging.data.o)c.a();
    Object localObject2 = I;
    localObject1 = ((com.truecaller.messaging.data.o)localObject1).a((String)localObject2);
    localObject2 = d;
    -..Lambda.MAGAF96Ypd7iZ37BYmFbZQmy98k localMAGAF96Ypd7iZ37BYmFbZQmy98k = new com/truecaller/search/global/-$$Lambda$MAGAF96Ypd7iZ37BYmFbZQmy98k;
    localMAGAF96Ypd7iZ37BYmFbZQmy98k.<init>(this);
    localObject1 = ((com.truecaller.androidactors.w)localObject1).a((com.truecaller.androidactors.i)localObject2, localMAGAF96Ypd7iZ37BYmFbZQmy98k);
    S = ((com.truecaller.androidactors.a)localObject1);
  }
  
  private String s()
  {
    CountryListDto.a locala = M;
    if (locala == null) {
      return "";
    }
    return c;
  }
  
  private void t()
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    k = false;
    b(false);
    ((ah)b).f(false);
    ((ah)b).e(false);
    ((ah)b).a(true);
  }
  
  public final int a()
  {
    return B.f();
  }
  
  public final int a(int paramInt)
  {
    return B.a(paramInt);
  }
  
  final void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i1 = 100;
    if (paramInt1 == i1)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        Object localObject1 = paramIntent.getExtras();
        if (localObject1 != null)
        {
          Object localObject2 = "extra_results";
          localObject1 = ((Bundle)localObject1).getStringArrayList((String)localObject2);
          if (localObject1 != null)
          {
            localObject2 = b;
            if (localObject2 != null)
            {
              paramInt2 = ((List)localObject1).size();
              int i2 = 1;
              if (paramInt2 > i2)
              {
                ((ah)b).a((List)localObject1);
                return;
              }
              localObject2 = (ah)b;
              i2 = 0;
              localObject1 = (String)((List)localObject1).get(0);
              ((ah)localObject2).d((String)localObject1);
              localObject1 = new com/truecaller/analytics/e$a;
              ((e.a)localObject1).<init>("SEARCH_scanNumber");
              localObject1 = ((e.a)localObject1).a("scanType", "singleScan").a();
              q.a((com.truecaller.analytics.e)localObject1);
              localObject1 = new com/google/gson/o;
              ((com.google.gson.o)localObject1).<init>();
              paramIntent = "singleScan";
              ((com.google.gson.o)localObject1).a("scanType", paramIntent);
              localObject2 = "SEARCH_scanNumber";
              a((String)localObject2, (com.google.gson.o)localObject1);
            }
          }
        }
      }
    }
  }
  
  final void a(Intent paramIntent)
  {
    Object localObject1 = b;
    boolean bool1 = false;
    ah localah = null;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    localObject1 = paramIntent.getStringExtra("ARG_SEARCH_COUNTRY");
    boolean bool2 = am.b((CharSequence)localObject1);
    boolean bool3 = true;
    if (!bool2)
    {
      localObject2 = "ARG_SEARCH_COUNTRY";
      paramIntent.removeExtra((String)localObject2);
      bool2 = am.b((CharSequence)localObject1);
      if (bool2)
      {
        localObject1 = s.b();
        a((CountryListDto.a)localObject1, bool3);
      }
      else
      {
        localObject2 = s;
        localObject1 = ((ad)localObject2).b((String)localObject1);
        a((CountryListDto.a)localObject1, false);
      }
    }
    localObject1 = paramIntent.getStringExtra("ARG_SEARCH_TEXT");
    bool1 = am.b((CharSequence)localObject1);
    if (!bool1)
    {
      localah = (ah)b;
      localah.c((String)localObject1);
      a((String)localObject1);
      localObject1 = "ARG_SEARCH_TEXT";
      paramIntent.removeExtra((String)localObject1);
    }
    else
    {
      t();
    }
    localObject1 = "ARG_SHOW_KEYBOARD";
    boolean bool4 = paramIntent.getBooleanExtra((String)localObject1, bool3);
    boolean bool5 = C;
    if ((!bool5) && (bool4))
    {
      paramIntent = (ah)b;
      paramIntent.d();
    }
    paramIntent = t;
    bool4 = paramIntent.n();
    if (bool4) {
      return;
    }
    t.o();
    ((ah)b).j();
  }
  
  final void a(CountryListDto.a parama)
  {
    Object localObject = M;
    boolean bool1 = true;
    a(parama, bool1);
    boolean bool2;
    if (localObject != null)
    {
      parama = c;
      localObject = s();
      bool2 = am.a(parama, (CharSequence)localObject);
      if (bool2) {}
    }
    else
    {
      parama = I;
      bool2 = am.b(parama);
      if (!bool2)
      {
        bool2 = false;
        K = null;
        parama = Collections.emptyList();
        b(parama);
        p();
      }
      parama = new com/truecaller/analytics/e$a;
      parama.<init>("SEARCHVIEW_Filtered");
      String str = "CountryChanged";
      parama = parama.a("Filter_Action", str).a();
      localObject = q;
      ((b)localObject).a(parama);
    }
  }
  
  final void a(com.truecaller.messaging.data.a.j paramj)
  {
    if (paramj == null)
    {
      paramj = Collections.emptyList();
      d(paramj);
      return;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    int i1 = paramj.getCount();
    localArrayList.<init>(i1);
    for (;;)
    {
      boolean bool = paramj.moveToNext();
      if (!bool) {
        break;
      }
      Message localMessage = paramj.b();
      localArrayList.add(localMessage);
    }
    q.a(paramj);
    d(localArrayList);
  }
  
  public final void a(c paramc)
  {
    Object localObject = o.d();
    if (paramc == localObject)
    {
      paramc = K;
      if (paramc != null)
      {
        boolean bool = C;
        if (bool)
        {
          paramc = e;
          localObject = new com/truecaller/search/global/-$$Lambda$ag$kEwicQQJCgrgBhQexxZsdzR06ck;
          ((-..Lambda.ag.kEwicQQJCgrgBhQexxZsdzR06ck)localObject).<init>(this);
          paramc.post((Runnable)localObject);
        }
      }
    }
  }
  
  final void a(e parame, int paramInt, String paramString)
  {
    Object localObject1 = x;
    Object localObject2 = ((com.truecaller.search.a.a)localObject1).a(paramString, paramInt);
    paramString = a;
    paramString.removeAllViews();
    if (localObject2 != null)
    {
      paramString = (Activity)itemView.getContext();
      localObject1 = AdLayoutType.SMALL;
      localObject2 = com.truecaller.ads.d.a(paramString, (com.truecaller.ads.a)localObject1, (com.truecaller.ads.provider.holders.e)localObject2);
      a.addView((View)localObject2);
      a.setVisibility(0);
      return;
    }
    a.setVisibility(8);
  }
  
  final void a(String paramString)
  {
    Object localObject1 = b;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    localObject1 = I;
    paramString = am.j(paramString);
    I = paramString;
    paramString = I;
    boolean bool1 = am.b(paramString);
    if (!bool1)
    {
      bool1 = am.b((CharSequence)localObject1);
      if (!bool1)
      {
        int i1 = ((String)localObject1).charAt(0);
        localObject1 = I;
        int i2 = ((String)localObject1).charAt(0);
        if (i1 == i2) {}
      }
      else
      {
        paramString = new com/truecaller/analytics/e$a;
        paramString.<init>("SEARCHVIEW_SearchPerformed");
        localObject1 = I;
        bool3 = am.b((String)localObject1);
        if (bool3)
        {
          localObject1 = "Search_Type";
          localObject2 = "NumberSearch";
          paramString.a((String)localObject1, (String)localObject2);
        }
        else
        {
          localObject1 = "Search_Type";
          localObject2 = "NameSearch";
          paramString.a((String)localObject1, (String)localObject2);
        }
        localObject1 = q;
        paramString = paramString.a();
        ((b)localObject1).a(paramString);
      }
    }
    paramString = I;
    boolean bool2 = am.b(paramString);
    boolean bool3 = true;
    if (bool2)
    {
      localObject2 = b;
      if (localObject2 != null)
      {
        boolean bool4 = k;
        if (!bool4)
        {
          k = bool3;
          b(bool3);
          ((ah)b).f(false);
          localObject2 = (ah)b;
          ((ah)localObject2).a(false);
        }
      }
    }
    else
    {
      localObject2 = b;
      if (localObject2 != null)
      {
        k = false;
        b(false);
        ((ah)b).f(bool3);
        ((ah)b).e(false);
        localObject2 = (ah)b;
        ((ah)localObject2).a(false);
      }
    }
    localObject2 = (ah)b;
    if (bool2)
    {
      boolean bool5 = H;
      if (bool5) {}
    }
    else
    {
      bool3 = false;
      localObject1 = null;
    }
    ((ah)localObject2).g(bool3);
    if (bool2)
    {
      bool2 = false;
      paramString = null;
    }
    else
    {
      paramString = s;
      localObject1 = I;
      paramString = paramString.a((String)localObject1);
    }
    a(paramString, false);
    p();
  }
  
  public final void a(Throwable paramThrowable)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = Q;
      if ((localObject != null) && (paramThrowable != null))
      {
        N = null;
        paramThrowable = (ah)b;
        localObject = f;
        Object[] arrayOfObject = new Object[0];
        localObject = ((com.truecaller.utils.o)localObject).a(2131886537, arrayOfObject);
        paramThrowable.b((String)localObject);
        a(false);
        return;
      }
    }
    paramThrowable = b;
    if (paramThrowable != null)
    {
      a(false);
      paramThrowable = Collections.emptyList();
      b(paramThrowable);
    }
  }
  
  public final void a(List paramList)
  {
    c(paramList);
  }
  
  public final void a(List paramList, String paramString1, String paramString2)
  {
    Object localObject = b;
    if (localObject == null)
    {
      localObject = Q;
      if (localObject != null) {
        return;
      }
    }
    localObject = null;
    a(false);
    if (paramString1 == null)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
    }
    else
    {
      localObject = new java/util/ArrayList;
      List localList = E;
      ((ArrayList)localObject).<init>(localList);
    }
    ((List)localObject).addAll(paramList);
    K = paramString2;
    if (paramString1 == null)
    {
      paramList = t;
      paramList.p();
    }
    b((List)localObject);
  }
  
  public final boolean a(am.a parama, int paramInt)
  {
    ag localag = this;
    am.a locala = parama;
    Object localObject1 = F.get(paramInt);
    Object localObject2 = localObject1;
    localObject2 = (i)localObject1;
    Contact localContact = a;
    localObject1 = localContact.A();
    boolean bool1 = ((List)localObject1).isEmpty();
    boolean bool2 = true;
    com.truecaller.search.local.b.e locale = null;
    if (bool1)
    {
      localObject1 = localContact.p();
      if (localObject1 != null)
      {
        localObject1 = z;
        localObject3 = new String[bool2];
        localObject4 = localContact.p();
        localObject3[0] = localObject4;
        localObject1 = ((com.truecaller.data.entity.g)localObject1).b((String[])localObject3);
        localContact.a((Number)localObject1);
      }
    }
    localObject1 = c;
    boolean bool3 = localContact.U();
    if (!bool3) {
      if (localObject1 != null)
      {
        localObject3 = j;
        localObject4 = FilterManager.ActionSource.TOP_SPAMMER;
        if (localObject3 == localObject4) {}
      }
      else
      {
        bool3 = false;
        localObject3 = null;
        break label176;
      }
    }
    bool3 = true;
    label176:
    if (localObject1 != null)
    {
      i1 = m;
    }
    else
    {
      i1 = 0;
      localObject4 = null;
    }
    int i2 = localContact.I();
    int i1 = Math.max(i1, i2);
    if (localObject1 != null)
    {
      localObject1 = h;
      localObject5 = FilterManager.FilterAction.FILTER_BLACKLISTED;
      if (localObject1 == localObject5)
      {
        bool1 = true;
        break label249;
      }
    }
    bool1 = false;
    localObject1 = null;
    label249:
    Object localObject5 = localContact.s();
    String str = at.a((CharSequence)localObject5);
    locala.b_(str);
    locala.a(localContact);
    int i3 = com.truecaller.util.w.a(f, false);
    locala.a(i3);
    int i4 = k;
    int i5 = 2;
    if (i4 == i5) {
      i4 = 1;
    } else {
      i4 = 0;
    }
    if (i4 != 0)
    {
      bool5 = localContact.Z();
      if ((!bool5) && (i3 == 0))
      {
        i3 = 1;
        break label358;
      }
    }
    i3 = 0;
    label358:
    locala.b(i3);
    boolean bool4 = r.b(localContact);
    if (bool4)
    {
      bool4 = localContact.aa();
      if (bool4)
      {
        bool4 = true;
        break label400;
      }
    }
    bool4 = false;
    label400:
    locala.a(bool4);
    localObject1 = a(bool3, bool1, i1);
    locala.b((String)localObject1);
    i5 = 0;
    if (localObject1 != null)
    {
      locala.a(null);
      locala.b_(bool2);
    }
    else
    {
      localObject1 = r.b(localContact);
      locala.a((c.a)localObject1);
      locala.b_(false);
    }
    locale = X;
    Object localObject3 = I;
    boolean bool6 = true;
    localObject1 = parama;
    Object localObject4 = localObject5;
    localObject5 = str;
    boolean bool7 = false;
    str = null;
    bool4 = false;
    boolean bool5 = bool6;
    bool1 = com.truecaller.calling.ae.a(parama, locale, (String)localObject3, (String)localObject4, (String)localObject5, false, false, bool6);
    if (bool1)
    {
      locala.e_(null);
    }
    else
    {
      localObject4 = b;
      localObject5 = at.a((CharSequence)localObject4);
      locala.e_((String)localObject5);
      locale = X;
      localObject3 = I;
      bool7 = true;
      bool4 = true;
      com.truecaller.calling.ae.a(parama, locale, (String)localObject3, (String)localObject4, (String)localObject5, bool7, bool4);
    }
    localObject1 = W;
    localObject1 = r.a(localContact, (com.truecaller.flashsdk.core.i)localObject1, "globalSearch");
    locala.a((x)localObject1);
    return bool2;
  }
  
  public final long b(int paramInt)
  {
    return 0L;
  }
  
  public final void b()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = o.e();
      int i1 = 1;
      ((c)localObject).c(i1);
      localObject = (ah)b;
      ((ah)localObject).i();
    }
  }
  
  final void b(String paramString)
  {
    Object localObject1 = b;
    b localb = null;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    localObject1 = J;
    boolean bool1 = am.b(paramString);
    if (!bool1)
    {
      bool1 = am.b((CharSequence)localObject1);
      if (!bool1)
      {
        int i1 = paramString.charAt(0);
        int i2 = ((String)localObject1).charAt(0);
        if (i1 == i2) {}
      }
      else
      {
        localObject1 = new com/truecaller/analytics/e$a;
        ((e.a)localObject1).<init>("SEARCHVIEW_Filtered");
        localObject2 = "AddressField";
        localObject1 = ((e.a)localObject1).a("Filter_Action", (String)localObject2).a();
        localb = q;
        localb.a((com.truecaller.analytics.e)localObject1);
      }
    }
    J = paramString;
    paramString = I;
    boolean bool2 = am.b(paramString);
    if (!bool2)
    {
      bool2 = false;
      K = null;
      paramString = Collections.emptyList();
      b(paramString);
      p();
    }
  }
  
  public final void b(Throwable paramThrowable)
  {
    String[] arrayOfString = new String[1];
    paramThrowable = String.valueOf(paramThrowable);
    paramThrowable = "GSearch, onError = ".concat(paramThrowable);
    arrayOfString[0] = paramThrowable;
  }
  
  public final boolean b(am.a parama, int paramInt)
  {
    ag localag = this;
    Object localObject1 = D;
    int i1 = paramInt;
    localObject1 = ((List)localObject1).get(paramInt);
    Object localObject2 = localObject1;
    localObject2 = (Message)localObject1;
    localObject1 = c;
    Object localObject3 = m;
    boolean bool1 = localObject3 instanceof HistoryTransportInfo;
    int i4 = 1;
    if (bool1)
    {
      int i2 = f;
      localObject4 = (HistoryTransportInfo)m;
      if (i2 != i4)
      {
        int i5 = 8;
        if (i2 != i5)
        {
          localObject3 = Y;
          i6 = d;
          localObject3 = ((bs)localObject3).b(i6);
        }
        else
        {
          localObject3 = Y;
          i6 = d;
          localObject3 = ((bs)localObject3).c(i6);
          localObject5 = localObject3;
          break label196;
        }
      }
      else
      {
        localObject3 = Y;
        i6 = d;
        localObject3 = ((bs)localObject3).a(i6);
        localObject5 = localObject3;
        break label196;
      }
    }
    else
    {
      localObject3 = ((Message)localObject2).j();
    }
    Object localObject5 = localObject3;
    label196:
    localObject3 = m;
    boolean bool2 = am.e((CharSequence)localObject3);
    if (bool2) {
      localObject3 = m;
    } else {
      localObject3 = f;
    }
    Object localObject6 = localObject3;
    Object localObject7 = ((Participant)localObject1).a();
    parama.b_((String)localObject7);
    localObject3 = m;
    long l1 = p;
    Object localObject4 = n;
    localObject3 = ((com.truecaller.util.al)localObject3).a(l1, (String)localObject4, i4);
    parama.a(localObject3);
    int i3 = r;
    int i6 = 0;
    localObject4 = null;
    i3 = com.truecaller.util.w.a(i3, false);
    parama.a(i3);
    boolean bool3 = ((Participant)localObject1).e();
    parama.a(bool3);
    bool3 = ((Participant)localObject1).g();
    p localp = w;
    boolean bool4 = localp.g();
    bool4 = ((Participant)localObject1).a(bool4);
    int i7 = q;
    localObject3 = a(bool3, bool4, i7);
    parama.b((String)localObject3);
    if (localObject3 != null)
    {
      parama.a(null);
      parama.b_(i4);
    }
    else
    {
      localObject3 = r;
      localObject1 = ((com.truecaller.search.local.model.c)localObject3).a((Participant)localObject1);
      parama.a((c.a)localObject1);
      parama.b_(false);
    }
    parama.e_((String)localObject5);
    localObject3 = X;
    localObject4 = I;
    localObject1 = parama;
    com.truecaller.calling.ae.a(parama, (com.truecaller.search.local.b.e)localObject3, (String)localObject4, (String)localObject6, (String)localObject7, false, false, false);
    localObject3 = X;
    localObject4 = I;
    localObject6 = localObject5;
    localObject7 = localObject5;
    com.truecaller.calling.ae.a(parama, (com.truecaller.search.local.b.e)localObject3, (String)localObject4, (String)localObject5, (String)localObject5, false, false);
    localObject1 = V;
    long l2 = e.a;
    localObject1 = ((com.truecaller.util.af)localObject1).g(l2).toString();
    parama.a((String)localObject1);
    parama.a(null);
    return i4;
  }
  
  final void c()
  {
    Object localObject = b;
    if (localObject != null)
    {
      bool = k;
      if (bool)
      {
        bool = true;
        b(bool);
      }
    }
    localObject = F;
    boolean bool = ((List)localObject).isEmpty();
    if (!bool)
    {
      localObject = I;
      a((String)localObject);
    }
  }
  
  final void c(int paramInt)
  {
    Object localObject1 = b;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localObject1, arrayOfString);
    localObject1 = B;
    int i1 = ((c)localObject1).a(paramInt);
    switch (i1)
    {
    default: 
      int i2 = -1 >>> 1;
      switch (i1)
      {
      default: 
        break;
      case 2131363190: 
        o.d().d(i2);
        localObject2 = o.d();
        B = ((c)localObject2);
        d(2131888166);
        return;
      case 2131363189: 
        o.c().d(i2);
        localObject2 = o.c();
        B = ((c)localObject2);
        d(2131888165);
        return;
      case 2131363188: 
        o.b().d(i2);
        localObject2 = o.b();
        B = ((c)localObject2);
        d(2131888163);
        return;
      case 2131363187: 
        localObject2 = b;
        localObject1 = new String[0];
        AssertionUtil.isNotNull(localObject2, (String[])localObject1);
        localObject2 = (ah)b;
        ((ah)localObject2).l();
        return;
      }
      localObject2 = a(i1, paramInt);
      a((Contact)localObject2, "truecaller");
      return;
    case 2131363181: 
      localObject1 = D;
      paramInt = B.b(paramInt);
      localObject2 = (Message)((List)localObject1).get(paramInt);
      localObject1 = (ah)b;
      long l1 = b;
      long l2 = a;
      ((ah)localObject1).a(l1, l2);
      localObject2 = q;
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("ViewAction");
      localObject1 = ((e.a)localObject1).a("Action", "message").a("Context", "searchResults").a();
      ((b)localObject2).a((com.truecaller.analytics.e)localObject1);
      return;
    }
    Object localObject2 = a(i1, paramInt);
    a((Contact)localObject2, "phoneBook");
  }
  
  final void c(String paramString)
  {
    Object localObject = b;
    if (localObject != null)
    {
      ((ah)b).d(paramString);
      paramString = new com/truecaller/analytics/e$a;
      paramString.<init>("SEARCH_scanNumber");
      paramString = paramString.a("scanType", "multiScan").a();
      q.a(paramString);
      paramString = new com/google/gson/o;
      paramString.<init>();
      String str = "multiScan";
      paramString.a("scanType", str);
      localObject = "SEARCH_scanNumber";
      a((String)localObject, paramString);
    }
  }
  
  final boolean c(am.a parama, int paramInt)
  {
    Object localObject1 = E;
    Object localObject2 = (Contact)((List)localObject1).get(paramInt);
    boolean bool1 = ((Contact)localObject2).U();
    int i1 = ((Contact)localObject2).I();
    String str1 = ((Contact)localObject2).s();
    String str2 = at.a(str1);
    parama.b_(str2);
    parama.a(localObject2);
    int i2 = f;
    com.truecaller.search.local.b.e locale = null;
    i2 = com.truecaller.util.w.a(i2, false);
    parama.a(i2);
    boolean bool2 = r.b((Contact)localObject2);
    boolean bool3 = true;
    if (bool2)
    {
      bool2 = ((Contact)localObject2).aa();
      if (bool2)
      {
        bool2 = true;
        break label123;
      }
    }
    bool2 = false;
    label123:
    parama.a(bool2);
    localObject1 = a(bool1, false, i1);
    parama.b((String)localObject1);
    i1 = 0;
    String str3 = null;
    if (localObject1 != null)
    {
      parama.a(null);
      parama.b_(bool3);
    }
    else
    {
      localObject1 = r.b((Contact)localObject2);
      parama.a((c.a)localObject1);
      parama.b_(false);
    }
    localObject1 = ((Contact)localObject2).g();
    if (localObject1 != null) {
      str3 = ((Address)localObject1).getDisplayableAddress();
    }
    parama.e_(str3);
    locale = X;
    String str4 = I;
    com.truecaller.calling.ae.a(parama, locale, str4, str1, str2, false, false, false);
    localObject1 = W;
    localObject2 = r.a((Contact)localObject2, (com.truecaller.flashsdk.core.i)localObject1, "globalSearch");
    parama.a((x)localObject2);
    return bool3;
  }
  
  final void d(String paramString)
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    if (paramString == null)
    {
      localObject1 = N;
      localObject2 = I;
      bool1 = am.a((CharSequence)localObject1, (CharSequence)localObject2);
      if (bool1)
      {
        localObject1 = O;
        localObject2 = J;
        bool1 = am.a((CharSequence)localObject1, (CharSequence)localObject2);
        if (bool1)
        {
          localObject1 = P;
          if (localObject1 == null)
          {
            localObject1 = M;
            if (localObject1 != null) {}
          }
          else
          {
            localObject2 = M;
            bool1 = localObject1.equals(localObject2);
            if (bool1) {
              return;
            }
          }
        }
      }
      localObject1 = I;
      N = ((String)localObject1);
      localObject1 = J;
      O = ((String)localObject1);
      localObject1 = M;
      P = ((CountryListDto.a)localObject1);
    }
    boolean bool1 = false;
    localObject1 = null;
    K = null;
    Object localObject2 = I;
    boolean bool2 = am.b((CharSequence)localObject2);
    if (!bool2)
    {
      localObject2 = I;
      int i1 = ((String)localObject2).length();
      int i2 = 3;
      if (i1 >= i2)
      {
        localObject2 = n;
        Object localObject3 = UUID.randomUUID();
        String str = "globalSearch";
        localObject2 = ((l)localObject2).a((UUID)localObject3, str);
        b = false;
        c = false;
        d = false;
        localObject3 = J;
        j = ((String)localObject3);
        localObject3 = I;
        i = ((String)localObject3);
        k = paramString;
        int i3 = 4;
        h = i3;
        paramString = M;
        if (paramString == null)
        {
          i3 = 0;
          paramString = null;
        }
        else
        {
          paramString = c;
        }
        paramString = ((com.truecaller.network.search.j)localObject2).b(paramString);
        i1 = 1;
        paramString = paramString.a(null, i1, false, this);
        Q = paramString;
        a(i1);
        return;
      }
    }
    o.d().a(false);
    paramString = Collections.emptyList();
    b(paramString);
    N = null;
  }
  
  final void e()
  {
    b(false);
  }
  
  final void f()
  {
    r.b();
  }
  
  final void g()
  {
    r.c();
  }
  
  final void h()
  {
    Object localObject = b;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localObject, arrayOfString);
    boolean bool = C;
    if (bool)
    {
      localObject = o.a();
      B = ((c)localObject);
      localObject = o.b();
      int i1 = p;
      ((c)localObject).d(i1);
      localObject = o.c();
      i1 = p;
      ((c)localObject).d(i1);
      localObject = o.d();
      i1 = p;
      ((c)localObject).d(i1);
      ((ah)b).c(true);
      ((ah)b).d(false);
      ((ah)b).e();
      ((ah)b).i();
      C = false;
      return;
    }
    localObject = (ah)b;
    bool = ((ah)localObject).m();
    if (bool)
    {
      ((ah)b).f();
      return;
    }
    t();
  }
  
  final void i()
  {
    Object localObject = b;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localObject, arrayOfString);
    ((ah)b).g();
  }
  
  final void j()
  {
    Object localObject1 = b;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    localObject1 = (ah)b;
    boolean bool = ((ah)localObject1).h() ^ true;
    ((ah)b).h(bool);
    ((ah)b).i(bool);
    localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("SEARCHVIEW_Location");
    String str = "Location_Action";
    if (bool) {
      localObject1 = "Selected";
    } else {
      localObject1 = "Deselected";
    }
    localObject1 = ((e.a)localObject2).a(str, (String)localObject1).a();
    q.a((com.truecaller.analytics.e)localObject1);
  }
  
  final void k()
  {
    Object localObject1 = b;
    Object localObject2 = null;
    Object localObject3 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject3);
    localObject1 = I;
    boolean bool = am.b((CharSequence)localObject1);
    if (bool)
    {
      localObject1 = (ah)b;
      localObject3 = f;
      localObject2 = new Object[0];
      localObject2 = ((com.truecaller.utils.o)localObject3).a(2131887029, (Object[])localObject2);
      ((ah)localObject1).b((String)localObject2);
      return;
    }
    ((ah)b).c();
  }
  
  final void l()
  {
    Object localObject = b;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localObject, arrayOfString);
    ((ah)b).c();
  }
  
  final void m()
  {
    u.b("general_hasShownScannerTooltip", true);
  }
  
  final void n()
  {
    boolean bool = G;
    if (bool)
    {
      Object localObject = b;
      if (localObject != null)
      {
        localObject = (ah)b;
        ((ah)localObject).b();
      }
    }
  }
  
  final void o()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (ah)b;
      ((ah)localObject).n();
    }
  }
  
  public final void y_()
  {
    super.y_();
    Object localObject1 = Q;
    boolean bool = true;
    if (localObject1 != null)
    {
      ((AsyncTask)localObject1).cancel(bool);
      i1 = 0;
      localObject1 = null;
      Q = null;
    }
    localObject1 = e;
    Object localObject2 = R;
    ((Handler)localObject1).removeCallbacks((Runnable)localObject2);
    localObject1 = e;
    localObject2 = i;
    ((Handler)localObject1).removeCallbacks((Runnable)localObject2);
    int i1 = 2;
    localObject1 = new com.truecaller.androidactors.a[i1];
    com.truecaller.androidactors.a locala1 = S;
    localObject1[0] = locala1;
    localObject2 = j;
    localObject1[bool] = localObject2;
    localObject1 = Arrays.asList((Object[])localObject1).iterator();
    for (;;)
    {
      bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      com.truecaller.androidactors.a locala2 = (com.truecaller.androidactors.a)((Iterator)localObject1).next();
      if (locala2 != null) {
        locala2.a();
      }
    }
    x.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */