package com.truecaller.search.global;

import android.view.View;
import com.truecaller.calling.ad;
import com.truecaller.calling.an;
import com.truecaller.calling.ao;
import com.truecaller.calling.at;
import com.truecaller.calling.au;
import com.truecaller.calling.ax;
import com.truecaller.calling.ay;
import com.truecaller.calling.az;
import com.truecaller.calling.ba;
import com.truecaller.calling.bb;
import com.truecaller.calling.bc;
import com.truecaller.calling.bd;
import com.truecaller.calling.c;
import com.truecaller.calling.d;
import com.truecaller.calling.n;
import com.truecaller.calling.o;
import com.truecaller.calling.p;
import com.truecaller.calling.s;
import com.truecaller.calling.t;
import com.truecaller.calling.u;
import com.truecaller.calling.v;
import com.truecaller.calling.w;
import com.truecaller.calling.x;
import com.truecaller.calling.z;
import com.truecaller.search.local.model.c.a;
import com.truecaller.ui.components.d.c;
import com.truecaller.ui.q.a;

public final class ai
  extends d.c
  implements an, ao, ax, az, bb, bc, bd, c, n, p, t, u, v, w, am.a, q.a
{
  private ai(View paramView, o paramo, ba paramba, ad paramad)
  {
    super(paramView);
    a = paramo;
    c = paramo;
    d = paramo;
    Object localObject = new com/truecaller/calling/z;
    ((z)localObject).<init>(paramView);
    e = ((z)localObject);
    f = paramba;
    localObject = new com/truecaller/calling/d;
    ((d)localObject).<init>(paramView);
    g = ((d)localObject);
    h = paramba;
    i = paramba;
    j = paramad;
    k = paramad;
    l = paramo;
    paramba = new com/truecaller/calling/at;
    paramba.<init>();
    m = paramba;
    paramba = new com/truecaller/calling/ay;
    paramba.<init>(paramView);
    n = paramba;
    paramba = new com/truecaller/calling/s;
    paramba.<init>(paramView);
    o = paramba;
    p = paramo;
  }
  
  public final String a()
  {
    return m.b;
  }
  
  public final void a(int paramInt)
  {
    d.a(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    k.a(paramInt1, paramInt2);
  }
  
  public final void a(x paramx)
  {
    e.a(paramx);
  }
  
  public final void a(c.a parama)
  {
    g.a(parama);
  }
  
  public final void a(Object paramObject)
  {
    c.a(paramObject);
  }
  
  public final void a(String paramString)
  {
    o.a(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    f.a(paramBoolean);
  }
  
  public final void a_(int paramInt1, int paramInt2)
  {
    i.a_(paramInt1, paramInt2);
  }
  
  public final void a_(boolean paramBoolean)
  {
    a.a_(paramBoolean);
  }
  
  public final void b(String paramString)
  {
    n.b(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    p.b(paramBoolean);
  }
  
  public final boolean b()
  {
    return m.a;
  }
  
  public final void b_(String paramString)
  {
    h.b_(paramString);
  }
  
  public final void b_(boolean paramBoolean)
  {
    l.b_(paramBoolean);
  }
  
  public final void c_(String paramString)
  {
    m.c_(paramString);
  }
  
  public final void c_(boolean paramBoolean)
  {
    m.a = paramBoolean;
  }
  
  public final void e_(String paramString)
  {
    j.e_(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.global.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */