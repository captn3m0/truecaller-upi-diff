package com.truecaller.search.local.b;

import com.truecaller.old.data.access.Settings;
import java.util.ArrayList;
import java.util.List;

public final class d
{
  private static final int[] a;
  private static f b;
  
  static
  {
    int[] arrayOfInt = new int[''];
    a = arrayOfInt;
    arrayOfInt[63] = 10;
    arrayOfInt = a;
    arrayOfInt[33] = 11;
    arrayOfInt[45] = 12;
    arrayOfInt[95] = 13;
    arrayOfInt[64] = 14;
    arrayOfInt[39] = 15;
    arrayOfInt[59] = 16;
    arrayOfInt[46] = 17;
    arrayOfInt[58] = 18;
    arrayOfInt[44] = 19;
  }
  
  public static int a(char paramChar)
  {
    boolean bool = Character.isDigit(paramChar);
    if (bool) {
      return Character.getNumericValue(paramChar);
    }
    Object localObject = a;
    char c2 = localObject.length;
    if (paramChar < c2)
    {
      c1 = localObject[paramChar];
      if (c1 != 0)
      {
        c1 = '\001';
        break label43;
      }
    }
    char c1 = '\000';
    localObject = null;
    label43:
    if (c1 != 0) {
      return a[paramChar];
    }
    c1 = '*';
    if (c1 == paramChar) {
      return -2;
    }
    c1 = '#';
    if (c1 == paramChar) {
      return -3;
    }
    localObject = a();
    c1 = ((f)localObject).a(paramChar);
    if (c1 != paramChar) {
      return Character.getNumericValue(c1);
    }
    return -1;
  }
  
  private static f a()
  {
    f localf = b;
    if (localf == null)
    {
      localf = f.a(Settings.k());
      b = localf;
    }
    return b;
  }
  
  public static List a(String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramString.length();
    localArrayList.<init>(i);
    i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int n = 42;
      int i3 = -1 >>> 1;
      if (j == n)
      {
        i3 = -2;
      }
      else
      {
        int i1 = 35;
        if (j == i1)
        {
          i3 = -3;
        }
        else
        {
          f localf = a();
          int i2 = localf.a(j);
          int k;
          if (i2 != j)
          {
            k = Character.getNumericValue(i2);
            if (k >= 0) {
              i3 = k;
            }
          }
          else
          {
            boolean bool = Character.isDigit(k);
            if (bool) {
              i3 = Character.getNumericValue(k);
            }
          }
        }
      }
      int m = 9;
      if (i3 <= m)
      {
        Integer localInteger = Integer.valueOf(i3);
        localArrayList.add(localInteger);
      }
      i += 1;
    }
    return localArrayList;
  }
  
  public static void b(String paramString)
  {
    b = f.a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */