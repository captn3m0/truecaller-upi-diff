package com.truecaller.search.local.b;

import android.text.TextUtils;
import com.a.a.n;
import com.a.a.o;
import com.truecaller.search.local.b.a.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class c
{
  private static final o a;
  private static final o b;
  private static final HashMap c;
  private static final boolean[] d;
  private static final boolean[] e;
  
  static
  {
    Object localObject = new com/a/a/n;
    double d1 = 0.99D;
    int i = 500;
    ((n)localObject).<init>(i, d1);
    a = (o)localObject;
    localObject = new com/a/a/n;
    ((n)localObject).<init>(i, d1);
    b = (o)localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>(1500, 0.5F);
    c = (HashMap)localObject;
    int j = 128;
    d = new boolean[j];
    e = new boolean[j];
    localObject = d;
    int k = 1;
    localObject[10] = k;
    localObject[13] = k;
    localObject[40] = k;
    localObject[41] = k;
    localObject[91] = k;
    localObject[93] = k;
    localObject[123] = k;
    localObject[125] = k;
    localObject[60] = k;
    localObject[62] = k;
    localObject[92] = k;
    localObject[47] = k;
    localObject[34] = k;
    localObject[38] = k;
    int m = 45;
    localObject[m] = k;
    i = 95;
    localObject[i] = k;
    int n = 64;
    localObject[n] = k;
    int i1 = 59;
    localObject[i1] = k;
    int i2 = 46;
    localObject[i2] = k;
    int i3 = 58;
    localObject[i3] = k;
    int i4 = 44;
    localObject[i4] = k;
    localObject = e;
    localObject[63] = k;
    localObject[33] = k;
    localObject[m] = k;
    localObject[i] = k;
    localObject[n] = k;
    localObject[39] = k;
    localObject[i1] = k;
    localObject[i2] = k;
    localObject[i3] = k;
    localObject[i4] = k;
  }
  
  public static String a(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = paramString.length();
    localStringBuilder.<init>(i);
    i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      boolean bool = b(j);
      if (!bool) {
        localStringBuilder.append(j);
      }
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static void a()
  {
    synchronized (a)
    {
      ??? = a;
      ((o)???).a();
      synchronized (b)
      {
        ??? = b;
        ((o)???).a();
        synchronized (c)
        {
          ??? = c;
          ((HashMap)???).clear();
          return;
        }
      }
    }
  }
  
  public static c.a[] a(String paramString, List arg1)
  {
    if (paramString == null) {
      paramString = "";
    }
    synchronized (c)
    {
      Object localObject2 = c;
      localObject2 = ((HashMap)localObject2).get(paramString);
      localObject2 = (c.a[])localObject2;
      if (localObject2 == null)
      {
        ???.clear();
        int i = 0;
        ??? = null;
        int j = 0;
        localObject2 = null;
        int k = 0;
        c.a locala = null;
        for (;;)
        {
          int m = paramString.length();
          if (j >= m) {
            break;
          }
          m = paramString.charAt(j);
          boolean bool = c(m);
          Object localObject3;
          Object localObject4;
          if (bool)
          {
            if (j > k)
            {
              localObject3 = new com/truecaller/search/local/b/c$a;
              localObject4 = paramString.substring(k, j);
              ((c.a)localObject3).<init>((String)localObject4, k, j);
              ???.add(localObject3);
            }
            k = j;
          }
          else
          {
            bool = b(m);
            if (bool)
            {
              if (j > k)
              {
                localObject3 = new com/truecaller/search/local/b/c$a;
                localObject4 = paramString.substring(k, j);
                ((c.a)localObject3).<init>((String)localObject4, k, j);
                ???.add(localObject3);
              }
              k = j + 1;
            }
            else
            {
              bool = f.a(m);
              if (!bool)
              {
                bool = b.a(m);
                if (!bool)
                {
                  bool = false;
                  localObject4 = null;
                  break label240;
                }
              }
              bool = true;
              label240:
              if (bool)
              {
                if (j > k)
                {
                  localObject4 = new com/truecaller/search/local/b/c$a;
                  String str = paramString.substring(k, j);
                  ((c.a)localObject4).<init>(str, k, j);
                  ???.add(localObject4);
                }
                locala = new com/truecaller/search/local/b/c$a;
                localObject3 = String.valueOf(m);
                int i1 = j + 1;
                locala.<init>((String)localObject3, j, i1);
                ???.add(locala);
                k = i1;
              }
            }
          }
          j += 1;
        }
        j = paramString.length();
        if (j > k) {
          if (k == 0)
          {
            localObject2 = new com/truecaller/search/local/b/c$a;
            k = paramString.length();
            ((c.a)localObject2).<init>(paramString, 0, k);
            ???.add(localObject2);
          }
          else
          {
            ??? = new com/truecaller/search/local/b/c$a;
            j = paramString.length();
            localObject2 = paramString.substring(k, j);
            int n = paramString.length();
            ((c.a)???).<init>((String)localObject2, k, n);
            ???.add(???);
          }
        }
        i = ???.size();
        ??? = new c.a[i];
        ??? = ???.toArray((Object[])???);
        localObject2 = ???;
        localObject2 = (c.a[])???;
        synchronized (c)
        {
          ??? = c;
          ((HashMap)???).put(paramString, localObject2);
        }
      }
      return (c.a[])localObject2;
    }
  }
  
  /* Error */
  public static String[] a(char paramChar)
  {
    // Byte code:
    //   0: iload_0
    //   1: invokestatic 131	com/truecaller/search/local/b/a/f:a	(C)Z
    //   4: istore_1
    //   5: aconst_null
    //   6: astore_2
    //   7: iload_1
    //   8: ifeq +97 -> 105
    //   11: getstatic 25	com/truecaller/search/local/b/c:a	Lcom/a/a/o;
    //   14: astore_3
    //   15: aload_3
    //   16: monitorenter
    //   17: getstatic 25	com/truecaller/search/local/b/c:a	Lcom/a/a/o;
    //   20: astore 4
    //   22: aload 4
    //   24: iload_0
    //   25: invokeinterface 152 2 0
    //   30: astore 4
    //   32: aload 4
    //   34: checkcast 154	[Ljava/lang/String;
    //   37: astore 4
    //   39: aload_3
    //   40: monitorexit
    //   41: aload 4
    //   43: ifnonnull +52 -> 95
    //   46: iload_0
    //   47: invokestatic 156	com/truecaller/search/local/b/a/f:b	(C)Ljava/lang/String;
    //   50: astore_2
    //   51: aload_2
    //   52: ifnull +43 -> 95
    //   55: aload_2
    //   56: ldc -98
    //   58: invokevirtual 162	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   61: astore 4
    //   63: getstatic 25	com/truecaller/search/local/b/c:a	Lcom/a/a/o;
    //   66: astore_3
    //   67: aload_3
    //   68: monitorenter
    //   69: getstatic 25	com/truecaller/search/local/b/c:a	Lcom/a/a/o;
    //   72: astore_2
    //   73: aload_2
    //   74: iload_0
    //   75: aload 4
    //   77: invokeinterface 165 3 0
    //   82: pop
    //   83: aload_3
    //   84: monitorexit
    //   85: goto +10 -> 95
    //   88: astore 5
    //   90: aload_3
    //   91: monitorexit
    //   92: aload 5
    //   94: athrow
    //   95: aload 4
    //   97: areturn
    //   98: astore 5
    //   100: aload_3
    //   101: monitorexit
    //   102: aload 5
    //   104: athrow
    //   105: iload_0
    //   106: invokestatic 134	com/truecaller/search/local/b/b:a	(C)Z
    //   109: istore_1
    //   110: iload_1
    //   111: ifeq +83 -> 194
    //   114: getstatic 27	com/truecaller/search/local/b/c:b	Lcom/a/a/o;
    //   117: astore_3
    //   118: aload_3
    //   119: monitorenter
    //   120: getstatic 27	com/truecaller/search/local/b/c:b	Lcom/a/a/o;
    //   123: astore_2
    //   124: aload_2
    //   125: iload_0
    //   126: invokeinterface 152 2 0
    //   131: astore_2
    //   132: aload_2
    //   133: checkcast 154	[Ljava/lang/String;
    //   136: astore_2
    //   137: aload_3
    //   138: monitorexit
    //   139: aload_2
    //   140: ifnonnull +45 -> 185
    //   143: iload_0
    //   144: invokestatic 168	com/truecaller/search/local/b/b:c	(C)[Ljava/lang/String;
    //   147: astore_2
    //   148: aload_2
    //   149: ifnull +36 -> 185
    //   152: getstatic 27	com/truecaller/search/local/b/c:b	Lcom/a/a/o;
    //   155: astore_3
    //   156: aload_3
    //   157: monitorenter
    //   158: getstatic 27	com/truecaller/search/local/b/c:b	Lcom/a/a/o;
    //   161: astore 4
    //   163: aload 4
    //   165: iload_0
    //   166: aload_2
    //   167: invokeinterface 165 3 0
    //   172: pop
    //   173: aload_3
    //   174: monitorexit
    //   175: goto +10 -> 185
    //   178: astore 5
    //   180: aload_3
    //   181: monitorexit
    //   182: aload 5
    //   184: athrow
    //   185: aload_2
    //   186: areturn
    //   187: astore 5
    //   189: aload_3
    //   190: monitorexit
    //   191: aload 5
    //   193: athrow
    //   194: aconst_null
    //   195: areturn
    //   196: pop
    //   197: goto -146 -> 51
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	200	0	paramChar	char
    //   4	107	1	bool	boolean
    //   6	180	2	localObject1	Object
    //   14	176	3	localo	o
    //   20	144	4	localObject2	Object
    //   88	5	5	localObject3	Object
    //   98	5	5	localObject4	Object
    //   178	5	5	localObject5	Object
    //   187	5	5	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   69	72	88	finally
    //   75	83	88	finally
    //   83	85	88	finally
    //   90	92	88	finally
    //   17	20	98	finally
    //   24	30	98	finally
    //   32	37	98	finally
    //   39	41	98	finally
    //   100	102	98	finally
    //   158	161	178	finally
    //   166	173	178	finally
    //   173	175	178	finally
    //   180	182	178	finally
    //   120	123	187	finally
    //   125	131	187	finally
    //   132	136	187	finally
    //   137	139	187	finally
    //   189	191	187	finally
    //   46	50	196	finally
  }
  
  public static char b(String paramString)
  {
    if (paramString == null) {
      return '\000';
    }
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      boolean bool = b(j);
      if (!bool) {
        return j;
      }
      i += 1;
    }
    return '\000';
  }
  
  public static boolean b(char paramChar)
  {
    char c1 = ' ';
    if (paramChar > c1)
    {
      boolean[] arrayOfBoolean = d;
      char c2 = arrayOfBoolean.length;
      if (paramChar < c2)
      {
        c1 = arrayOfBoolean[paramChar];
        if (c1 != 0) {}
      }
      else
      {
        c1 = 65288;
        if (paramChar != c1)
        {
          c1 = 65289;
          if (paramChar != c1)
          {
            c1 = 65308;
            if (paramChar != c1)
            {
              c1 = 65310;
              if (paramChar != c1)
              {
                c1 = '“';
                if (paramChar != c1)
                {
                  c1 = '”';
                  if (paramChar != c1)
                  {
                    c1 = 65292;
                    if (paramChar != c1)
                    {
                      paramChar = Character.isWhitespace(paramChar);
                      if (paramChar == 0) {
                        return false;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return true;
  }
  
  public static char c(String paramString)
  {
    if (paramString == null) {
      return '\000';
    }
    char c1 = b(paramString);
    if (c1 == 0) {
      return '\000';
    }
    boolean bool = b.a(c1);
    if (bool) {
      return b.b(c1);
    }
    bool = f.a(c1);
    if (bool)
    {
      paramString = f.b(c1);
      bool = TextUtils.isEmpty(paramString);
      if (bool) {
        return '\000';
      }
      return Character.toUpperCase(paramString.charAt(0));
    }
    return Character.toUpperCase(a.a(c1));
  }
  
  public static boolean c(char paramChar)
  {
    boolean[] arrayOfBoolean = e;
    char c1 = arrayOfBoolean.length;
    if (paramChar < c1)
    {
      paramChar = arrayOfBoolean[paramChar];
      if (paramChar != 0) {
        return true;
      }
    }
    return false;
  }
  
  public static String d(String paramString)
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    paramString = a(paramString, (List)localObject);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    int i = paramString.length;
    int j = 0;
    while (j < i)
    {
      String str = paramString[j];
      String[] arrayOfString = b;
      int k = arrayOfString.length;
      if (k > 0)
      {
        str = b[0];
        ((StringBuilder)localObject).append(str);
      }
      j += 1;
    }
    return ((StringBuilder)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */