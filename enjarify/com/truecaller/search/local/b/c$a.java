package com.truecaller.search.local.b;

import java.util.ArrayList;
import java.util.List;

public final class c$a
{
  public final String a;
  public final String[] b;
  public final List c;
  public int d;
  public int e;
  
  public c$a(String paramString, int paramInt1, int paramInt2)
  {
    d = paramInt1;
    e = paramInt2;
    a = paramString;
    paramInt1 = paramString.length();
    paramInt2 = 1;
    int i = 0;
    if (paramInt1 == paramInt2)
    {
      paramInt1 = paramString.charAt(0);
      arrayOfString = c.a(paramInt1);
    }
    else
    {
      paramInt1 = 0;
      arrayOfString = null;
    }
    if (arrayOfString == null)
    {
      arrayOfString = new String[paramInt2];
      paramString = a.a(paramString);
      arrayOfString[0] = paramString;
      b = arrayOfString;
    }
    else
    {
      int j = arrayOfString.length;
      paramString = new String[j];
      b = paramString;
      paramString = b;
      paramInt2 = arrayOfString.length;
      System.arraycopy(arrayOfString, 0, paramString, 0, paramInt2);
    }
    paramString = new java/util/ArrayList;
    String[] arrayOfString = b;
    paramInt1 = arrayOfString.length;
    paramString.<init>(paramInt1);
    c = paramString;
    paramString = b;
    paramInt1 = paramString.length;
    while (i < paramInt1)
    {
      Object localObject = paramString[i];
      List localList = c;
      localObject = d.a((String)localObject);
      localList.add(localObject);
      i += 1;
    }
  }
  
  public final String toString()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.b.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */