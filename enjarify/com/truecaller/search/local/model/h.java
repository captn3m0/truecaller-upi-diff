package com.truecaller.search.local.model;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.ContactsContract.Contacts;
import android.text.TextUtils;
import android.util.LruCache;
import android.util.Pair;
import c.g.b.k;
import com.a.a.ad;
import com.a.a.ae;
import com.a.a.af;
import com.google.f.r.a;
import com.truecaller.TrueApp;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.b.a;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.old.data.access.Settings;
import com.truecaller.presence.a.a;
import com.truecaller.search.local.model.a.o;
import com.truecaller.util.ck;
import com.truecaller.util.s;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import org.a.a.x;

public final class h
  extends g
  implements Handler.Callback
{
  private final Handler a;
  private final Handler b;
  private final android.support.v4.content.d c;
  private final Context d;
  private final BroadcastReceiver e;
  private final IntentFilter f;
  private final e g;
  private final f h;
  private volatile SortedSet i;
  private volatile SortedSet j;
  private List k;
  private final AtomicInteger l;
  private final AtomicInteger m;
  private final Map n;
  private final LruCache o;
  private final AtomicInteger p;
  private Collection q;
  private g.a r;
  private boolean s;
  private boolean t;
  private boolean u;
  
  h(Context paramContext)
  {
    Object localObject1 = new android/os/Handler;
    Object localObject2 = Looper.getMainLooper();
    ((Handler)localObject1).<init>((Looper)localObject2, this);
    a = ((Handler)localObject1);
    localObject1 = new android/content/IntentFilter;
    ((IntentFilter)localObject1).<init>("com.truecaller.actions.BULK_SEARCH_COMPLETE");
    f = ((IntentFilter)localObject1);
    localObject1 = s.a();
    i = ((SortedSet)localObject1);
    localObject1 = s.a();
    j = ((SortedSet)localObject1);
    localObject1 = Collections.emptyList();
    k = ((List)localObject1);
    localObject1 = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject1).<init>(0);
    l = ((AtomicInteger)localObject1);
    localObject1 = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject1).<init>(0);
    m = ((AtomicInteger)localObject1);
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    n = ((Map)localObject1);
    localObject1 = new android/util/LruCache;
    ((LruCache)localObject1).<init>(50);
    o = ((LruCache)localObject1);
    localObject1 = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject1).<init>(0);
    p = ((AtomicInteger)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    q = ((Collection)localObject1);
    localObject1 = g.a.a;
    r = ((g.a)localObject1);
    s = false;
    t = false;
    localObject1 = new android/os/HandlerThread;
    ((HandlerThread)localObject1).<init>("DataManager loader");
    ((HandlerThread)localObject1).start();
    Object localObject3 = new String[1];
    Object localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>("Data manager created ");
    long l1 = SystemClock.elapsedRealtime();
    long l2 = TrueApp.a;
    l1 -= l2;
    ((StringBuilder)localObject4).append(l1);
    ((StringBuilder)localObject4).append("ms after app start");
    localObject4 = ((StringBuilder)localObject4).toString();
    localObject3[0] = localObject4;
    localObject2 = android.support.v4.content.d.a(paramContext);
    c = ((android.support.v4.content.d)localObject2);
    localObject2 = new com/truecaller/search/local/a/c;
    ((com.truecaller.search.local.a.c)localObject2).<init>(paramContext);
    com.truecaller.search.local.a.d locald = new com/truecaller/search/local/a/d;
    locald.<init>(paramContext);
    Object localObject5 = new com/truecaller/search/local/model/e;
    Looper localLooper = a.getLooper();
    localObject3 = localObject5;
    localObject4 = paramContext;
    ((e)localObject5).<init>(paramContext, localLooper, this, (com.truecaller.search.local.a.c)localObject2, locald);
    g = ((e)localObject5);
    localObject5 = new com/truecaller/search/local/model/f;
    localLooper = a.getLooper();
    localObject3 = localObject5;
    ((f)localObject5).<init>(paramContext, localLooper, this, (com.truecaller.search.local.a.c)localObject2, locald);
    h = ((f)localObject5);
    localObject2 = new android/os/Handler;
    localObject1 = ((HandlerThread)localObject1).getLooper();
    ((Handler)localObject2).<init>((Looper)localObject1, this);
    b = ((Handler)localObject2);
    d = paramContext;
    paramContext = new com/truecaller/search/local/model/h$1;
    paramContext.<init>(this);
    e = paramContext;
  }
  
  private void a(h.b paramb, boolean paramBoolean, long paramLong)
  {
    if (paramBoolean) {}
    try
    {
      try
      {
        Object localObject1 = a;
        int i1 = 10;
        int i2 = (int)paramLong;
        i3 = 0;
        localObject1 = ((Handler)localObject1).obtainMessage(i1, i2, 0, paramb);
        ((Message)localObject1).sendToTarget();
        long l1 = 5000L;
        paramb.wait(l1);
        return;
      }
      finally {}
      Handler localHandler = a;
      int i3 = 14;
      paramb = localHandler.obtainMessage(i3, paramb);
      paramb.sendToTarget();
      return;
    }
    catch (InterruptedException localInterruptedException) {}
  }
  
  private void b(Runnable paramRunnable)
  {
    boolean bool = k();
    if (!bool)
    {
      a.obtainMessage(16, paramRunnable).sendToTarget();
      return;
    }
    bool = t;
    if (!bool)
    {
      bool = l();
      if (bool)
      {
        q.add(paramRunnable);
        return;
      }
    }
    try
    {
      paramRunnable.run();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      com.truecaller.log.d.a(localRuntimeException, "Error when calling callback");
    }
  }
  
  private void g()
  {
    boolean bool = k();
    if (!bool)
    {
      a.sendEmptyMessage(4);
      return;
    }
    Object localObject = b;
    int i1 = 9;
    ((Handler)localObject).removeMessages(i1);
    localObject = b;
    h.c localc = new com/truecaller/search/local/model/h$c;
    localc.<init>(this, (byte)0);
    ((Handler)localObject).obtainMessage(i1, localc).sendToTarget();
    localObject = g.a.c;
    r = ((g.a)localObject);
    m.incrementAndGet();
  }
  
  private void h()
  {
    boolean bool1 = k();
    if (!bool1)
    {
      a.sendEmptyMessage(2);
      return;
    }
    Object localObject1 = b;
    int i2 = 9;
    ((Handler)localObject1).removeMessages(i2);
    int i1 = Settings.m();
    if (i1 >= 0)
    {
      boolean bool3 = l();
      if (!bool3)
      {
        localObject2 = b;
        h.f localf = new com/truecaller/search/local/model/h$f;
        localf.<init>(this, (byte)0);
        localObject2 = ((Handler)localObject2).obtainMessage(i2, localf);
        ((Message)localObject2).sendToTarget();
        long l1 = i1;
        long l2 = 2000L;
        boolean bool2 = l1 < l2;
        if (!bool2)
        {
          localObject1 = b;
          localObject2 = new com/truecaller/search/local/model/h$g;
          ((h.g)localObject2).<init>(this, (byte)0);
          localObject1 = ((Handler)localObject1).obtainMessage(i2, localObject2);
          ((Message)localObject1).sendToTarget();
        }
      }
    }
    localObject1 = b;
    Object localObject2 = new com/truecaller/search/local/model/h$e;
    ((h.e)localObject2).<init>(this, (byte)0);
    ((Handler)localObject1).obtainMessage(i2, localObject2).sendToTarget();
    localObject1 = g.a.b;
    r = ((g.a)localObject1);
    n();
    l.incrementAndGet();
  }
  
  private void i()
  {
    Object localObject1 = q;
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    q = ((Collection)localObject2);
    localObject1 = ((Collection)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Runnable)((Iterator)localObject1).next();
      try
      {
        ((Runnable)localObject2).run();
      }
      catch (Exception localException)
      {
        String str = "Failed to call runnable";
        com.truecaller.log.d.a(localException, str);
      }
    }
  }
  
  private void j()
  {
    ck.a(a.getLooper(), "This method must be run on the coordinator");
  }
  
  private boolean k()
  {
    return ck.a(a.getLooper());
  }
  
  private boolean l()
  {
    g.a locala1 = r;
    g.a locala2 = g.a.b;
    return locala1 == locala2;
  }
  
  private void m()
  {
    boolean bool1 = k();
    if (!bool1)
    {
      a.sendEmptyMessage(12);
      return;
    }
    bool1 = l();
    if (!bool1)
    {
      h();
      return;
    }
    LinkedHashMap localLinkedHashMap = new java/util/LinkedHashMap;
    localLinkedHashMap.<init>(3);
    int i1 = 2;
    Object localObject1 = new a[i1];
    Object localObject2 = g;
    localObject1[0] = localObject2;
    localObject2 = h;
    int i2 = 1;
    localObject1[i2] = localObject2;
    int i3 = 0;
    localObject2 = null;
    long l1;
    Object localObject3;
    Object localObject4;
    for (;;)
    {
      l1 = 0L;
      if (i3 >= i1) {
        break;
      }
      localObject3 = localObject1[i3];
      boolean bool3 = d;
      if (bool3)
      {
        bool3 = localObject3 instanceof b;
        if (bool3)
        {
          localObject4 = localObject3;
          localObject4 = (b)localObject3;
          bool3 = ((b)localObject4).g();
          if (!bool3) {
            l1 = 1000L;
          }
        }
        localObject4 = Long.valueOf(l1);
        localObject4 = (List)localLinkedHashMap.get(localObject4);
        if (localObject4 == null)
        {
          localObject4 = new java/util/ArrayList;
          ((ArrayList)localObject4).<init>(i1);
          Long localLong = Long.valueOf(l1);
          localLinkedHashMap.put(localLong, localObject4);
        }
        ((List)localObject4).add(localObject3);
      }
      i3 += 1;
    }
    boolean bool2 = localLinkedHashMap.isEmpty();
    if (!bool2)
    {
      Iterator localIterator = localLinkedHashMap.keySet().iterator();
      for (;;)
      {
        boolean bool4 = localIterator.hasNext();
        if (!bool4) {
          break;
        }
        localObject1 = (Long)localIterator.next();
        localObject2 = (List)localLinkedHashMap.get(localObject1);
        if (localObject2 != null)
        {
          localObject3 = new com/truecaller/search/local/model/h$h;
          int i4 = ((List)localObject2).size();
          localObject4 = new a[i4];
          localObject2 = (a[])((List)localObject2).toArray((Object[])localObject4);
          ((h.h)localObject3).<init>(this, (a[])localObject2, (byte)0);
          long l2 = ((Long)localObject1).longValue();
          i3 = 9;
          boolean bool5 = l2 < l1;
          if (!bool5)
          {
            localObject1 = b.obtainMessage(i3, localObject3);
            ((Message)localObject1).sendToTarget();
          }
          else
          {
            localObject4 = b;
            localObject2 = ((Handler)localObject4).obtainMessage(i3, localObject3);
            localObject3 = b;
            l2 = ((Long)localObject1).longValue();
            ((Handler)localObject3).sendMessageDelayed((Message)localObject2, l2);
          }
        }
      }
    }
    n();
  }
  
  private void n()
  {
    g.d = false;
    h.d = false;
  }
  
  public final j a(long paramLong)
  {
    return (j)g.h.b(paramLong);
  }
  
  public final j a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return null;
    }
    com.truecaller.search.local.model.a.f localf = (com.truecaller.search.local.model.a.f)h.l.a(paramString);
    if (localf != null)
    {
      j localj = localf.b();
      if (localj != null) {
        return localf.b();
      }
    }
    return g.a(paramString);
  }
  
  public final void a()
  {
    boolean bool = k();
    if (!bool)
    {
      a.sendEmptyMessage(8);
      return;
    }
    bool = u;
    if (!bool) {
      g();
    }
  }
  
  public final void a(a parama)
  {
    j();
    boolean bool = l();
    if (!bool) {
      return;
    }
    bool = parama instanceof f;
    int i1 = 1;
    if (!bool)
    {
      bool = parama instanceof e;
      if (!bool)
      {
        d = false;
        localObject = new com/truecaller/search/local/model/h$h;
        a[] arrayOfa1 = new a[i1];
        arrayOfa1[0] = parama;
        ((h.h)localObject).<init>(this, arrayOfa1, (byte)0);
        break label131;
      }
    }
    parama = h;
    d = false;
    Object localObject = g;
    d = false;
    h.h localh = new com/truecaller/search/local/model/h$h;
    int i2 = 2;
    a[] arrayOfa2 = new a[i2];
    arrayOfa2[0] = parama;
    arrayOfa2[i1] = localObject;
    localh.<init>(this, arrayOfa2, (byte)0);
    localObject = localh;
    label131:
    b.obtainMessage(9, localObject).sendToTarget();
  }
  
  public final void a(Runnable paramRunnable)
  {
    boolean bool = k();
    if (!bool)
    {
      a.obtainMessage(16, paramRunnable).sendToTarget();
      return;
    }
    bool = s;
    if (bool)
    {
      paramRunnable.run();
      return;
    }
    bool = l();
    if (!bool) {
      h();
    }
    b(paramRunnable);
  }
  
  public final void a(String paramString, e.r paramr)
  {
    o.put(paramString, paramr);
  }
  
  public final void a(String paramString, org.a.a.b paramb)
  {
    synchronized (n)
    {
      Object localObject = n;
      boolean bool = ((Map)localObject).containsKey(paramString);
      if (bool)
      {
        localObject = n;
        localObject = ((Map)localObject).get(paramString);
        localObject = (com.truecaller.presence.a)localObject;
        localObject = ((com.truecaller.presence.a)localObject).b();
        c = paramb;
        paramb = ((a.a)localObject).a();
        localObject = n;
        ((Map)localObject).put(paramString, paramb);
      }
      return;
    }
  }
  
  public final void a(Collection paramCollection)
  {
    boolean bool1 = k();
    if (!bool1)
    {
      a.obtainMessage(15, paramCollection).sendToTarget();
      return;
    }
    synchronized (n)
    {
      Object localObject2 = paramCollection.iterator();
      Object localObject3;
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject2).hasNext();
        if (!bool2) {
          break;
        }
        localObject3 = ((Iterator)localObject2).next();
        localObject3 = (com.truecaller.presence.a)localObject3;
        Object localObject4 = n;
        Object localObject5 = a;
        localObject4 = ((Map)localObject4).get(localObject5);
        localObject4 = (com.truecaller.presence.a)localObject4;
        Object localObject6;
        if (localObject4 != null)
        {
          localObject5 = d;
          if (localObject5 != null)
          {
            localObject5 = d;
            localObject6 = d;
            bool3 = ((org.a.a.b)localObject5).b((x)localObject6);
            if (bool3)
            {
              bool3 = true;
              break label156;
            }
          }
        }
        boolean bool3 = false;
        localObject5 = null;
        label156:
        if (bool3)
        {
          localObject4 = ((com.truecaller.presence.a)localObject4).b();
          localObject5 = b;
          a = ((Availability)localObject5);
          localObject5 = c;
          b = ((com.truecaller.api.services.presence.v1.models.b)localObject5);
          localObject5 = f;
          e = ((com.truecaller.api.services.presence.v1.models.i)localObject5);
          localObject5 = e;
          d = ((com.truecaller.api.services.presence.v1.models.d)localObject5);
          localObject5 = a;
          localObject6 = "number";
          k.b(localObject5, (String)localObject6);
          g = ((String)localObject5);
          localObject4 = ((a.a)localObject4).a();
          localObject5 = n;
          localObject3 = a;
          ((Map)localObject5).put(localObject3, localObject4);
        }
        else
        {
          localObject4 = n;
          localObject5 = a;
          ((Map)localObject4).put(localObject5, localObject3);
        }
      }
      int i1 = paramCollection.size();
      if (i1 > 0)
      {
        ??? = new java/util/ArrayList;
        ((ArrayList)???).<init>(paramCollection);
        paramCollection = c;
        localObject2 = new android/content/Intent;
        ((Intent)localObject2).<init>("com.truecaller.datamanager.STATUSES_CHANGED");
        localObject3 = "com.truecaller.datamanager.EXTRA_PRESENCE";
        ??? = ((Intent)localObject2).putExtra((String)localObject3, (Serializable)???);
        paramCollection.a((Intent)???);
      }
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = "You must call this method on the main thread";
    ck.b((String)localObject);
    boolean bool = u;
    if (bool == paramBoolean) {
      return;
    }
    u = paramBoolean;
    int i1 = 2;
    localObject = new String[i1];
    IntentFilter localIntentFilter = null;
    String str = "DataManager is now %s";
    localObject[0] = str;
    int i2 = 1;
    if (paramBoolean) {
      str = "active";
    } else {
      str = "inactive";
    }
    localObject[i2] = str;
    if (paramBoolean)
    {
      g.a();
      h.a();
      locald = c;
      localObject = e;
      localIntentFilter = f;
      locald.a((BroadcastReceiver)localObject, localIntentFilter);
      m();
      return;
    }
    g.b();
    h.b();
    android.support.v4.content.d locald = c;
    localObject = e;
    locald.a((BroadcastReceiver)localObject);
  }
  
  public final boolean a(long paramLong, String paramString)
  {
    Object localObject1 = b.getLooper();
    boolean bool1 = ck.a((Looper)localObject1);
    boolean bool2 = true;
    Object localObject2;
    Object localObject3;
    if (!bool1)
    {
      localObject1 = b;
      localObject2 = new android/util/Pair;
      localObject3 = Long.valueOf(paramLong);
      ((Pair)localObject2).<init>(localObject3, paramString);
      ((Handler)localObject1).obtainMessage(17, localObject2).sendToTarget();
      return bool2;
    }
    localObject1 = d.getContentResolver();
    try
    {
      localObject2 = ContactsContract.Contacts.getLookupUri(paramLong, paramString);
      Uri localUri = null;
      int i1 = ((ContentResolver)localObject1).delete((Uri)localObject2, null, null);
      localUri = TruecallerContract.ah.a();
      String str = "contact_phonebook_id=? AND contact_phonebook_lookup=?";
      int i2 = 2;
      String[] arrayOfString = new String[i2];
      localObject3 = String.valueOf(paramLong);
      arrayOfString[0] = localObject3;
      arrayOfString[bool2] = paramString;
      ((ContentResolver)localObject1).delete(localUri, str, arrayOfString);
      localObject3 = g;
      e.a locala = new com/truecaller/search/local/model/e$a;
      paramString = new com/a/a/ae;
      localObject3 = h;
      paramString.<init>((ad)localObject3);
      locala.<init>(paramString);
      localObject3 = new com/truecaller/search/local/model/h$b;
      ((h.b)localObject3).<init>(this, (byte)0);
      b = locala;
      long l1 = -1;
      a((h.b)localObject3, bool2, l1);
      if (i1 > 0) {
        return bool2;
      }
      return false;
    }
    catch (Exception localException)
    {
      com.truecaller.log.d.a(localException, "Failed to delete contact");
    }
    return false;
  }
  
  public final com.truecaller.presence.a b(String paramString)
  {
    synchronized (n)
    {
      Map localMap2 = n;
      paramString = localMap2.get(paramString);
      paramString = (com.truecaller.presence.a)paramString;
      return paramString;
    }
  }
  
  public final SortedSet b()
  {
    return j;
  }
  
  public final com.truecaller.api.services.presence.v1.models.b c(String paramString)
  {
    Object localObject1 = b(paramString);
    if (paramString != null)
    {
      Object localObject2;
      if (localObject1 != null)
      {
        localObject2 = c;
        if (localObject2 != null) {}
      }
      else
      {
        localObject2 = com.truecaller.flashsdk.core.c.a();
        String str1 = "+";
        String str2 = "";
        paramString = paramString.replace(str1, str2);
        paramString = ((com.truecaller.flashsdk.core.b)localObject2).h(paramString);
        boolean bool = c;
        if (bool)
        {
          localObject1 = com.truecaller.api.services.presence.v1.models.b.c().a(true);
          localObject2 = com.google.f.r.newBuilder();
          int i1 = b;
          paramString = ((r.a)localObject2).setValue(i1);
          return (com.truecaller.api.services.presence.v1.models.b)((b.a)localObject1).a(paramString).build();
        }
      }
    }
    if (localObject1 != null) {
      return c;
    }
    return null;
  }
  
  public final SortedSet c()
  {
    return i;
  }
  
  public final e.r d(String paramString)
  {
    return (e.r)o.get(paramString);
  }
  
  public final List d()
  {
    return k;
  }
  
  final f e()
  {
    return h;
  }
  
  public final SortedSet f()
  {
    TreeSet localTreeSet = new java/util/TreeSet;
    h.d locald = new com/truecaller/search/local/model/h$d;
    locald.<init>((byte)0);
    localTreeSet.<init>(locald);
    return localTreeSet;
  }
  
  public final boolean handleMessage(Message paramMessage)
  {
    int i1 = what;
    int i2 = 2;
    boolean bool1 = true;
    if (i1 != i2)
    {
      i2 = 4;
      if (i1 != i2)
      {
        i2 = 12;
        if (i1 != i2)
        {
          i2 = 0;
          String str1 = null;
          long l1;
          Object localObject1;
          label297:
          Object localObject2;
          switch (i1)
          {
          default: 
            switch (i1)
            {
            default: 
              return false;
            case 17: 
              paramMessage = (Pair)obj;
              l1 = ((Long)first).longValue();
              paramMessage = (String)second;
              a(l1, paramMessage);
              return bool1;
            case 16: 
              paramMessage = (Runnable)obj;
              a(paramMessage);
              return bool1;
            case 15: 
              paramMessage = (Collection)obj;
              a(paramMessage);
              return bool1;
            }
            paramMessage = (h.b)obj;
            localObject1 = new String[bool1];
            paramMessage = String.valueOf(paramMessage);
            paramMessage = "Load failed for cache data ".concat(paramMessage);
            localObject1[0] = paramMessage;
            p.incrementAndGet();
            return bool1;
          case 10: 
            paramMessage = obj;
            localObject1 = paramMessage;
            localObject1 = (h.b)paramMessage;
            j();
            try
            {
              boolean bool2 = t;
              if (bool2)
              {
                bool2 = ((h.b)localObject1).c();
                if (!bool2) {}
              }
              else
              {
                bool2 = t;
                if (bool2) {
                  break label297;
                }
                bool2 = ((h.b)localObject1).d();
                if (bool2) {
                  break label297;
                }
              }
              try
              {
                localObject1.notifyAll();
              }
              finally {}
              Object localObject3;
              try
              {
                ((h.b)localObject1).b();
              }
              catch (Exception paramMessage)
              {
                localObject2 = new java/lang/StringBuilder;
                localObject3 = "Failed to set cache data for ";
                ((StringBuilder)localObject2).<init>((String)localObject3);
                localObject3 = localObject1.getClass();
                localObject3 = ((Class)localObject3).getSimpleName();
                ((StringBuilder)localObject2).append((String)localObject3);
                localObject2 = ((StringBuilder)localObject2).toString();
                com.truecaller.log.d.a(paramMessage, (String)localObject2);
              }
              try
              {
                localObject1.notifyAll();
                bool2 = localObject1 instanceof h.e;
                if (bool2)
                {
                  boolean bool3 = t;
                  if (!bool3)
                  {
                    localObject2 = new java/lang/Thread;
                    localObject3 = new com/truecaller/search/local/model/h$2;
                    ((h.2)localObject3).<init>(this);
                    ((Thread)localObject2).<init>((Runnable)localObject3);
                    ((Thread)localObject2).start();
                  }
                  t = bool1;
                  localObject2 = g.h;
                  int i3 = ((af)localObject2).a();
                  Settings.c(i3);
                }
                boolean bool4 = ((h.b)localObject1).d();
                if (bool4)
                {
                  bool4 = localObject1 instanceof h.c;
                  if (bool4)
                  {
                    s = false;
                    t = false;
                  }
                  else
                  {
                    s = bool1;
                  }
                }
                if (!bool2)
                {
                  bool2 = localObject1 instanceof h.c;
                  if (!bool2) {}
                }
                else
                {
                  i();
                }
                bool2 = g;
                if (!bool2)
                {
                  paramMessage = c;
                  localObject1 = new android/content/Intent;
                  str1 = "com.truecaller.datamanager.DATA_CHANGED";
                  ((Intent)localObject1).<init>(str1);
                  paramMessage.a((Intent)localObject1);
                }
                return bool1;
              }
              finally {}
              paramMessage = (h.a)obj;
            }
            finally
            {
              try
              {
                localObject1.notifyAll();
                throw paramMessage;
              }
              finally {}
            }
          case 9: 
            localObject1 = b.getLooper();
            localObject2 = "This method must be run on the worker";
            ck.a((Looper)localObject1, (String)localObject2);
            long l2 = -1;
            try
            {
              long l3 = SystemClock.elapsedRealtime();
              paramMessage.a();
              l1 = SystemClock.elapsedRealtime();
              l2 = l1 - l3;
              i2 = 1;
            }
            catch (RuntimeException localRuntimeException)
            {
              String str2 = "Failed to load data for";
              com.truecaller.log.d.a(localRuntimeException, str2);
            }
            a(paramMessage, i2, l2);
            return bool1;
          }
          a();
          return bool1;
        }
        m();
        return bool1;
      }
      g();
      return bool1;
    }
    h();
    return bool1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */