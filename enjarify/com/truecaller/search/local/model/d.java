package com.truecaller.search.local.model;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import c.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.h;
import com.truecaller.presence.a;
import com.truecaller.presence.q;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class d
  extends BroadcastReceiver
  implements c, Runnable
{
  private final Handler a;
  private final ArrayList b;
  private boolean c;
  private final g d;
  private final com.truecaller.common.account.r e;
  private final h f;
  private final com.truecaller.androidactors.f g;
  private final com.truecaller.presence.r h;
  private final bw i;
  private final com.truecaller.voip.d j;
  
  public d(al paramal, g paramg, com.truecaller.common.account.r paramr, h paramh, com.truecaller.androidactors.f paramf, com.truecaller.presence.r paramr1, bw parambw, com.truecaller.voip.d paramd)
  {
    d = paramg;
    e = paramr;
    f = paramh;
    g = paramf;
    h = paramr1;
    i = parambw;
    j = paramd;
    paramg = new android/os/Handler;
    paramr = Looper.getMainLooper();
    paramg.<init>(paramr);
    a = paramg;
    paramg = new java/util/ArrayList;
    paramg.<init>();
    b = paramg;
    paramg = this;
    paramg = (BroadcastReceiver)this;
    paramr = new String[] { "com.truecaller.datamanager.STATUSES_CHANGED" };
    paramal.a(paramg, paramr);
  }
  
  private a a(String paramString)
  {
    boolean bool = a();
    if ((bool) && (paramString != null)) {
      return d.b(paramString);
    }
    return null;
  }
  
  private final void a(long paramLong)
  {
    Handler localHandler = a;
    Object localObject = this;
    localObject = (Runnable)this;
    localHandler.removeCallbacks((Runnable)localObject);
    a.postDelayed((Runnable)localObject, paramLong);
  }
  
  private static List c(Contact paramContact)
  {
    paramContact = paramContact.A();
    k.a(paramContact, "numbers");
    paramContact = (Iterable)paramContact;
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramContact = paramContact.iterator();
    for (;;)
    {
      boolean bool = paramContact.hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (Number)paramContact.next();
      String str = "it";
      k.a(localObject2, str);
      localObject2 = ((Number)localObject2).a();
      if (localObject2 != null) {
        ((Collection)localObject1).add(localObject2);
      }
    }
    return (List)localObject1;
  }
  
  private final void d()
  {
    boolean bool = e();
    if (!bool) {
      return;
    }
    long l = h.b();
    a(l);
  }
  
  private final boolean e()
  {
    boolean bool = c;
    if (bool)
    {
      bool = a();
      if (!bool)
      {
        Object localObject = f;
        bool = ((h)localObject).m();
        if (!bool)
        {
          localObject = i;
          bool = ((bw)localObject).a();
          if (!bool)
          {
            localObject = j;
            bool = ((com.truecaller.voip.d)localObject).a();
            if (!bool) {
              break label68;
            }
          }
        }
      }
      return true;
    }
    label68:
    return false;
  }
  
  public final a a(Contact paramContact)
  {
    k.b(paramContact, "contact");
    paramContact = (Collection)c(paramContact);
    int k = 0;
    String[] arrayOfString = new String[0];
    paramContact = paramContact.toArray(arrayOfString);
    if (paramContact != null)
    {
      paramContact = (String[])paramContact;
      k = paramContact.length;
      paramContact = (String[])Arrays.copyOf(paramContact, k);
      return b(paramContact);
    }
    paramContact = new c/u;
    paramContact.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramContact;
  }
  
  public final c.a a(Participant paramParticipant)
  {
    if (paramParticipant != null)
    {
      int k = c;
      if (k == 0)
      {
        String[] arrayOfString = new String[1];
        paramParticipant = f;
        k.a(paramParticipant, "participant.normalizedAddress");
        arrayOfString[0] = paramParticipant;
        return a(arrayOfString);
      }
    }
    return null;
  }
  
  public final c.a a(String... paramVarArgs)
  {
    Object localObject = "normalizedNumbers";
    k.b(paramVarArgs, (String)localObject);
    int k = paramVarArgs.length;
    if (k == 0)
    {
      k = 1;
    }
    else
    {
      k = 0;
      localObject = null;
    }
    if (k != 0) {
      return null;
    }
    d();
    localObject = new com/truecaller/search/local/model/d$a;
    int m = paramVarArgs.length;
    paramVarArgs = (String[])Arrays.copyOf(paramVarArgs, m);
    ((d.a)localObject).<init>(this, paramVarArgs);
    return (c.a)localObject;
  }
  
  public final boolean a()
  {
    h localh = f;
    boolean bool = localh.j();
    if (!bool)
    {
      localh = f;
      bool = localh.k();
      if (bool)
      {
        localh = f;
        bool = localh.l();
        if (bool) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final a b(String... paramVarArgs)
  {
    Object localObject1 = "normalizedPhoneNumbers";
    k.b(paramVarArgs, (String)localObject1);
    boolean bool = a();
    if (!bool) {
      return null;
    }
    int k = paramVarArgs.length;
    int m = 0;
    int n = 1;
    if (k == n)
    {
      paramVarArgs = paramVarArgs[0];
      return a(paramVarArgs);
    }
    localObject1 = q.a();
    n = paramVarArgs.length;
    while (m < n)
    {
      Object localObject2 = paramVarArgs[m];
      localObject2 = a((String)localObject2);
      if (localObject2 != null)
      {
        Object localObject3 = b;
        if (localObject3 != null)
        {
          localObject1 = q.a((a)localObject1, (a)localObject2);
          localObject2 = b;
          if (localObject2 == null) {
            break;
          }
          localObject2 = b;
          if (localObject2 != null) {
            localObject2 = ((Availability)localObject2).a();
          } else {
            localObject2 = null;
          }
          localObject3 = Availability.Status.BUSY;
          if (localObject2 == localObject3) {
            break;
          }
        }
      }
      m += 1;
    }
    return (a)localObject1;
  }
  
  public final c.a b(Contact paramContact)
  {
    k.b(paramContact, "contact");
    paramContact = (Collection)c(paramContact);
    int k = 0;
    String[] arrayOfString = new String[0];
    paramContact = paramContact.toArray(arrayOfString);
    if (paramContact != null)
    {
      paramContact = (String[])paramContact;
      k = paramContact.length;
      paramContact = (String[])Arrays.copyOf(paramContact, k);
      return a(paramContact);
    }
    paramContact = new c/u;
    paramContact.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramContact;
  }
  
  public final void b()
  {
    c = true;
    d();
  }
  
  public final void c()
  {
    c = false;
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = ((Iterable)m.f((Iterable)b)).iterator();
    for (;;)
    {
      boolean bool1 = paramContext.hasNext();
      if (!bool1) {
        break;
      }
      paramIntent = (d.a)paramContext.next();
      boolean bool2 = a();
      Object localObject;
      if (bool2)
      {
        localObject = a;
        int k = localObject.length;
        localObject = (String[])Arrays.copyOf((Object[])localObject, k);
        localObject = b((String[])localObject);
        paramIntent.a((a)localObject);
      }
      else
      {
        bool2 = false;
        localObject = null;
        paramIntent.a(null);
      }
    }
  }
  
  public final void run()
  {
    boolean bool1 = e();
    if (bool1)
    {
      Object localObject1 = e;
      bool1 = ((com.truecaller.common.account.r)localObject1).c();
      if (bool1)
      {
        localObject1 = (Iterable)b;
        Object localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        localObject2 = (Collection)localObject2;
        localObject1 = ((Iterable)localObject1).iterator();
        boolean bool2;
        Object localObject3;
        for (;;)
        {
          bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = ((Iterator)localObject1).next();
          Object localObject4 = localObject3;
          localObject4 = (d.a)localObject3;
          boolean bool3 = ((d.a)localObject4).a();
          if (bool3) {
            ((Collection)localObject2).add(localObject3);
          }
        }
        localObject2 = (Iterable)localObject2;
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        localObject1 = (Collection)localObject1;
        localObject2 = ((Iterable)localObject2).iterator();
        for (;;)
        {
          bool2 = ((Iterator)localObject2).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = (Iterable)c.a.f.h(nexta);
          m.a((Collection)localObject1, (Iterable)localObject3);
        }
        localObject1 = (Collection)localObject1;
        boolean bool4 = ((Collection)localObject1).isEmpty() ^ true;
        if (bool4)
        {
          new String[1][0] = "Updating availability for bound AvailabilityHandle instances";
          ((com.truecaller.presence.c)g.a()).a((Collection)localObject1);
          long l = h.a();
          a(l);
          return;
        }
        new String[1][0] = "No bound AvailabilityHandle instances found, stopping updates";
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */