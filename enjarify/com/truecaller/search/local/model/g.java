package com.truecaller.search.local.model;

import android.content.Context;
import com.truecaller.presence.a;
import e.r;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;

public abstract class g
  implements a.a
{
  private static volatile g a;
  
  public static g a(Context paramContext)
  {
    Object localObject = a;
    if (localObject == null) {
      synchronized (g.class)
      {
        localObject = a;
        if (localObject == null)
        {
          localObject = new com/truecaller/search/local/model/h;
          paramContext = paramContext.getApplicationContext();
          ((h)localObject).<init>(paramContext);
          a = (g)localObject;
        }
      }
    }
    return (g)localObject;
  }
  
  public abstract j a(long paramLong);
  
  public abstract j a(String paramString);
  
  public abstract void a();
  
  public abstract void a(Runnable paramRunnable);
  
  public abstract void a(String paramString, r paramr);
  
  public abstract void a(String paramString, org.a.a.b paramb);
  
  public abstract void a(Collection paramCollection);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract boolean a(long paramLong, String paramString);
  
  public abstract a b(String paramString);
  
  public abstract SortedSet b();
  
  public abstract com.truecaller.api.services.presence.v1.models.b c(String paramString);
  
  public abstract SortedSet c();
  
  public abstract r d(String paramString);
  
  public abstract List d();
  
  abstract f e();
  
  public abstract SortedSet f();
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */