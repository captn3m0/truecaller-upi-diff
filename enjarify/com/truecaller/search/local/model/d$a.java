package com.truecaller.search.local.model;

import c.g.b.k;
import com.truecaller.presence.a;
import java.util.Arrays;

public final class d$a
  implements c.a, c.b
{
  final String[] a;
  private c.b c;
  
  public d$a(d paramd, String[] paramArrayOfString)
  {
    a = paramArrayOfString;
  }
  
  public final void a(a parama)
  {
    c.b localb = c;
    if (localb != null)
    {
      localb.a(parama);
      return;
    }
  }
  
  public final void a(c.b paramb)
  {
    k.b(paramb, "listener");
    c = paramb;
    d.a(b, this);
    paramb = b;
    String[] arrayOfString = a;
    int i = arrayOfString.length;
    arrayOfString = (String[])Arrays.copyOf(arrayOfString, i);
    paramb = paramb.b(arrayOfString);
    a(paramb);
  }
  
  public final boolean a()
  {
    c.b localb = c;
    return localb != null;
  }
  
  public final void b()
  {
    c = null;
    d.b(b, this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */