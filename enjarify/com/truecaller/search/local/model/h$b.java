package com.truecaller.search.local.model;

import java.util.Collections;
import java.util.List;
import java.util.SortedSet;

class h$b
{
  public e.a b;
  public f.a c;
  public SortedSet d;
  public SortedSet e;
  public List f;
  public boolean g = false;
  
  private h$b(h paramh) {}
  
  protected void a(a parama, Object paramObject)
  {
    parama.a(paramObject);
  }
  
  final void b()
  {
    Object localObject1 = b;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject1 = h.b(h);
      localObject2 = b;
      a((a)localObject1, localObject2);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = h.c(h);
      localObject2 = c;
      a((a)localObject1, localObject2);
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject2 = h;
      localObject1 = Collections.unmodifiableSortedSet((SortedSet)localObject1);
      h.a((h)localObject2, (SortedSet)localObject1);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = h;
      localObject1 = Collections.unmodifiableSortedSet((SortedSet)localObject1);
      h.b((h)localObject2, (SortedSet)localObject1);
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject2 = h;
      localObject1 = Collections.unmodifiableList((List)localObject1);
      h.a((h)localObject2, (List)localObject1);
    }
  }
  
  protected boolean c()
  {
    return true;
  }
  
  protected boolean d()
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */