package com.truecaller.search.local.model;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Looper;
import android.os.SystemClock;
import com.a.a.ae;
import com.a.a.af;
import com.truecaller.search.local.a.a;
import com.truecaller.search.local.a.c;
import com.truecaller.search.local.a.d;

public final class e
  extends b
{
  private static final e.a i;
  private static final String[] j = tmp78_67;
  volatile af h;
  private final c k;
  private final d l;
  private final android.support.v4.f.g m;
  
  static
  {
    e.a locala = new com/truecaller/search/local/model/e$a;
    locala.<init>();
    i = locala;
    String[] tmp17_14 = new String[15];
    String[] tmp18_17 = tmp17_14;
    String[] tmp18_17 = tmp17_14;
    tmp18_17[0] = "_id";
    tmp18_17[1] = "tc_id";
    String[] tmp27_18 = tmp18_17;
    String[] tmp27_18 = tmp18_17;
    tmp27_18[2] = "contact_name";
    tmp27_18[3] = "contact_alt_name";
    String[] tmp36_27 = tmp27_18;
    String[] tmp36_27 = tmp27_18;
    tmp36_27[4] = "contact_about";
    tmp36_27[5] = "contact_image_url";
    String[] tmp45_36 = tmp36_27;
    String[] tmp45_36 = tmp36_27;
    tmp45_36[6] = "contact_job_title";
    tmp45_36[7] = "contact_company";
    String[] tmp56_45 = tmp45_36;
    String[] tmp56_45 = tmp45_36;
    tmp56_45[8] = "contact_source";
    tmp56_45[9] = "contact_phonebook_id";
    String[] tmp67_56 = tmp56_45;
    String[] tmp67_56 = tmp56_45;
    tmp67_56[10] = "contact_phonebook_lookup";
    tmp67_56[11] = "contact_access";
    String[] tmp78_67 = tmp67_56;
    String[] tmp78_67 = tmp67_56;
    tmp78_67[12] = "contact_is_favorite";
    tmp78_67[13] = "contact_badges";
    tmp78_67[14] = "tc_flag";
  }
  
  e(Context paramContext, Looper paramLooper, a.a parama, c paramc, d paramd)
  {
    super(paramContext, paramLooper, parama, localUri);
    paramContext = new android/support/v4/f/g;
    paramContext.<init>(10);
    m = paramContext;
    k = paramc;
    l = paramd;
    paramContext = i;
    b(paramContext);
  }
  
  private static Cursor a(a parama)
  {
    long l1 = SystemClock.elapsedRealtime();
    Object localObject1 = parama.getReadableDatabase();
    Object localObject2 = j;
    localObject1 = ((SQLiteDatabase)localObject1).query("aggregated_contact", (String[])localObject2, null, null, null, null, null);
    String[] arrayOfString = new String[1];
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Queried contacts in ");
    parama = parama.getClass().getSimpleName();
    ((StringBuilder)localObject2).append(parama);
    ((StringBuilder)localObject2).append(" in ");
    long l2 = SystemClock.elapsedRealtime() - l1;
    ((StringBuilder)localObject2).append(l2);
    ((StringBuilder)localObject2).append(" ms");
    parama = ((StringBuilder)localObject2).toString();
    arrayOfString[0] = parama;
    return (Cursor)localObject1;
  }
  
  private static e.a a(Context paramContext, Cursor paramCursor)
  {
    Object localObject1 = paramCursor;
    int n = 0;
    Object localObject2 = null;
    if (paramCursor == null) {
      return null;
    }
    Object localObject3 = "tc_id";
    try
    {
      int i1 = paramCursor.getColumnIndex((String)localObject3);
      String str1 = "_id";
      int i2 = paramCursor.getColumnIndex(str1);
      String str2 = "contact_name";
      int i5 = paramCursor.getColumnIndex(str2);
      String str3 = "contact_alt_name";
      int i6 = paramCursor.getColumnIndex(str3);
      String str4 = "contact_about";
      int i7 = paramCursor.getColumnIndex(str4);
      String str5 = "contact_image_url";
      int i8 = paramCursor.getColumnIndex(str5);
      String str6 = "contact_job_title";
      int i9 = paramCursor.getColumnIndex(str6);
      String str7 = "contact_company";
      int i10 = paramCursor.getColumnIndex(str7);
      String str8 = "contact_source";
      int i11 = paramCursor.getColumnIndex(str8);
      String str9 = "contact_phonebook_id";
      int i12 = paramCursor.getColumnIndex(str9);
      String str10 = "contact_phonebook_lookup";
      int i13 = paramCursor.getColumnIndex(str10);
      String str11 = "contact_access";
      int i14 = paramCursor.getColumnIndex(str11);
      String str12 = "contact_is_favorite";
      int i15 = paramCursor.getColumnIndex(str12);
      Object localObject4 = "contact_badges";
      int i16 = paramCursor.getColumnIndex((String)localObject4);
      localObject2 = "tc_flag";
      try
      {
        n = paramCursor.getColumnIndex((String)localObject2);
        int i17 = n;
        localObject2 = new com/a/a/ae;
        int i18 = i16;
        i16 = paramCursor.getCount();
        ((ae)localObject2).<init>(i16);
        localObject4 = g.a(paramContext);
        for (;;)
        {
          int i19 = paramCursor.moveToNext();
          if (i19 == 0) {
            break;
          }
          i19 = i15;
          str12 = ((Cursor)localObject1).getString(i1);
          int i20 = i11;
          int i21 = i12;
          long l1 = ((Cursor)localObject1).getLong(i2);
          int i22 = ((af)localObject2).a(l1);
          if (i22 == 0)
          {
            i22 = i1;
            localObject3 = new com/truecaller/search/local/model/j$a;
            ((j.a)localObject3).<init>((g)localObject4);
            c = str12;
            b = l1;
            str8 = ((Cursor)localObject1).getString(i5);
            d = str8;
            str8 = ((Cursor)localObject1).getString(i6);
            e = str8;
            str8 = ((Cursor)localObject1).getString(i7);
            f = str8;
            str8 = ((Cursor)localObject1).getString(i8);
            j = str8;
            str8 = ((Cursor)localObject1).getString(i9);
            g = str8;
            str8 = ((Cursor)localObject1).getString(i10);
            h = str8;
            str8 = ((Cursor)localObject1).getString(i13);
            i = str8;
            i12 = ((Cursor)localObject1).getInt(i11);
            k = i12;
            i15 = i2;
            i20 = i5;
            i12 = i21;
            long l2 = ((Cursor)localObject1).getLong(i21);
            m = l2;
            str1 = ((Cursor)localObject1).getString(i14);
            str2 = "private";
            int i3 = str2.equalsIgnoreCase(str1);
            n = i3;
            i3 = i19;
            i5 = ((Cursor)localObject1).getInt(i19);
            if (i5 != 0)
            {
              i5 = 1;
            }
            else
            {
              i5 = 0;
              str2 = null;
            }
            o = i5;
            i5 = i18;
            i18 = ((Cursor)localObject1).getInt(i18);
            i19 = i3;
            int i4 = l | i18;
            l = i4;
            i18 = i5;
            i4 = i17;
            i5 = ((Cursor)localObject1).getInt(i17);
            p = i5;
            localObject3 = ((j.a)localObject3).a();
            l2 = c;
            ((af)localObject2).a(l2, localObject3);
            i4 = i15;
            i15 = i19;
            i5 = i20;
            i1 = i22;
          }
        }
        localObject1 = new com/truecaller/search/local/model/e$a;
        ((e.a)localObject1).<init>((af)localObject2);
        return (e.a)localObject1;
      }
      catch (Exception localException1)
      {
        localObject1 = null;
      }
      return null;
    }
    catch (Exception localException2)
    {
      localObject1 = null;
    }
  }
  
  public final j a(String paramString)
  {
    synchronized (m)
    {
      android.support.v4.f.g localg2 = m;
      paramString = localg2.get(paramString);
      paramString = (j)paramString;
      return paramString;
    }
  }
  
  protected final boolean g()
  {
    return true;
  }
  
  protected final Cursor h()
  {
    return a(k);
  }
  
  protected final Cursor i()
  {
    return a(l);
  }
  
  protected final Cursor j()
  {
    ContentResolver localContentResolver = e.getContentResolver();
    Uri localUri = g;
    String[] arrayOfString = j;
    return localContentResolver.query(localUri, arrayOfString, null, null, null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */