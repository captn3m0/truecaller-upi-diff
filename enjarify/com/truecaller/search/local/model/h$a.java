package com.truecaller.search.local.model;

import android.os.SystemClock;
import android.util.Pair;
import java.util.List;
import java.util.SortedSet;

abstract class h$a
  extends h.b
{
  private h$a(h paramh)
  {
    super(paramh, (byte)0);
  }
  
  protected abstract Object a(a parama);
  
  public final void a()
  {
    long l1 = SystemClock.elapsedRealtime();
    Object localObject = h.b(a);
    localObject = (e.a)a((a)localObject);
    b = ((e.a)localObject);
    localObject = h.c(a);
    localObject = (f.a)a((a)localObject);
    c = ((f.a)localObject);
    localObject = b;
    if (localObject != null)
    {
      localObject = h.a(a, this);
      SortedSet localSortedSet = (SortedSet)second;
      d = localSortedSet;
      localObject = (SortedSet)first;
      e = ((SortedSet)localObject);
      localObject = h.a(d);
      f = ((List)localObject);
    }
    localObject = new String[1];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str1 = getClass().getSimpleName();
    localStringBuilder.append(str1);
    localStringBuilder.append(" loaded data in ");
    long l2 = SystemClock.elapsedRealtime() - l1;
    localStringBuilder.append(l2);
    String str2 = localStringBuilder.toString();
    localObject[0] = str2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */