package com.truecaller.search.local.model;

import android.text.TextUtils;
import com.a.a.u;
import com.a.a.v;
import com.truecaller.common.h.ab;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class i
{
  protected final boolean a = false;
  private final v b;
  
  public i()
  {
    this((byte)0);
  }
  
  private i(byte paramByte)
  {
    u localu = new com/a/a/u;
    localu.<init>();
    b = localu;
  }
  
  public i(int paramInt)
  {
    u localu = new com/a/a/u;
    localu.<init>(paramInt, 0.99D);
    b = localu;
  }
  
  protected i(v paramv)
  {
    b = paramv;
  }
  
  private static int b(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    int j = 0;
    if (bool) {
      return 0;
    }
    int i = paramString.length();
    int k = i + -7;
    k = Math.max(0, k);
    i += -1;
    while (i >= k)
    {
      j *= 31;
      int m = paramString.charAt(i);
      j += m;
      i += -1;
    }
    return j;
  }
  
  public final i.a a(String paramString)
  {
    Object localObject1 = b;
    int i = b(paramString);
    localObject1 = ((v)localObject1).a(i);
    i = 0;
    if (localObject1 == null) {
      return null;
    }
    boolean bool1 = localObject1 instanceof i.a;
    Object localObject2;
    if (bool1)
    {
      localObject1 = (i.a)localObject1;
      localObject2 = ((i.a)localObject1).a();
      boolean bool2 = ab.a((String)localObject2, paramString, false);
      if (bool2) {
        return (i.a)localObject1;
      }
    }
    else
    {
      localObject1 = (Set)localObject1;
      localObject2 = ((Set)localObject1).iterator();
      boolean bool3;
      Object localObject3;
      boolean bool5;
      do
      {
        bool3 = ((Iterator)localObject2).hasNext();
        if (!bool3) {
          break;
        }
        localObject3 = (i.a)((Iterator)localObject2).next();
        String str = ((i.a)localObject3).a();
        boolean bool4 = true;
        bool5 = ab.a(str, paramString, bool4);
      } while (!bool5);
      return (i.a)localObject3;
      localObject1 = ((Set)localObject1).iterator();
      do
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (i.a)((Iterator)localObject1).next();
        localObject3 = ((i.a)localObject2).a();
        bool3 = ab.a((String)localObject3, paramString, false);
      } while (!bool3);
      return (i.a)localObject2;
    }
    return null;
  }
  
  public final i a()
  {
    i.b localb = new com/truecaller/search/local/model/i$b;
    v localv = b;
    localb.<init>(localv);
    return localb;
  }
  
  public void a(i.a parama)
  {
    String str = parama.a();
    int i = b(str);
    boolean bool1 = a;
    if (bool1)
    {
      b.a(i, parama);
      return;
    }
    Object localObject = b.a(i);
    boolean bool2 = localObject instanceof i.a;
    if (bool2)
    {
      TreeSet localTreeSet = new java/util/TreeSet;
      localTreeSet.<init>();
      localTreeSet.add(localObject);
      localTreeSet.add(parama);
      b.a(i, localTreeSet);
      return;
    }
    bool2 = localObject instanceof Collection;
    if (bool2)
    {
      ((Set)localObject).add(parama);
      return;
    }
    b.a(i, parama);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */