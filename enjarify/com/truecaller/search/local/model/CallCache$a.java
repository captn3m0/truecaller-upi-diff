package com.truecaller.search.local.model;

import com.a.a.ae;
import com.a.a.af;
import com.truecaller.util.s;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;

public final class CallCache$a
{
  public final SortedSet a;
  public final List b;
  public final i c;
  public final af d;
  
  public CallCache$a()
  {
    Object localObject = s.a();
    a = ((SortedSet)localObject);
    localObject = Collections.emptyList();
    b = ((List)localObject);
    localObject = new com/truecaller/search/local/model/i;
    ((i)localObject).<init>();
    localObject = ((i)localObject).a();
    c = ((i)localObject);
    localObject = new com/a/a/ae;
    ((ae)localObject).<init>();
    d = ((af)localObject);
  }
  
  public CallCache$a(SortedSet paramSortedSet, List paramList, i parami, af paramaf)
  {
    a = paramSortedSet;
    b = paramList;
    c = parami;
    d = paramaf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.CallCache.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */