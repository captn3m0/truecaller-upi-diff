package com.truecaller.search.local.model;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class b
  extends a
  implements Handler.Callback
{
  protected final Context e;
  protected final Handler f;
  public final Uri g;
  private final AtomicInteger h;
  private boolean i;
  
  b(Context paramContext, Looper paramLooper, a.a parama, Uri paramUri)
  {
    super(parama);
    parama = new java/util/concurrent/atomic/AtomicInteger;
    parama.<init>(0);
    h = parama;
    i = false;
    parama = new android/os/Handler;
    parama.<init>(paramLooper, this);
    f = parama;
    parama = paramContext.getApplicationContext();
    e = parama;
    g = paramUri;
    paramContext = paramContext.getContentResolver();
    parama = new com/truecaller/search/local/model/b$1;
    Handler localHandler = new android/os/Handler;
    localHandler.<init>(paramLooper);
    parama.<init>(this, localHandler, paramUri);
    paramContext.registerContentObserver(paramUri, false, parama);
  }
  
  /* Error */
  private Object a(b.a parama)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 36	com/truecaller/search/local/model/b:f	Landroid/os/Handler;
    //   4: astore_2
    //   5: sipush 8193
    //   8: istore_3
    //   9: aload_2
    //   10: iload_3
    //   11: invokevirtual 68	android/os/Handler:removeMessages	(I)V
    //   14: invokestatic 74	android/os/SystemClock:elapsedRealtime	()J
    //   17: lstore 4
    //   19: iconst_0
    //   20: istore 6
    //   22: aload_1
    //   23: invokeinterface 80 1 0
    //   28: astore_1
    //   29: aload_1
    //   30: ifnonnull +20 -> 50
    //   33: aload_1
    //   34: ifnull +9 -> 43
    //   37: aload_1
    //   38: invokeinterface 86 1 0
    //   43: aload_0
    //   44: iconst_0
    //   45: putfield 29	com/truecaller/search/local/model/b:i	Z
    //   48: aconst_null
    //   49: areturn
    //   50: iconst_1
    //   51: istore 6
    //   53: iload 6
    //   55: anewarray 89	java/lang/String
    //   58: astore 7
    //   60: new 91	java/lang/StringBuilder
    //   63: astore 8
    //   65: ldc 93
    //   67: astore 9
    //   69: aload 8
    //   71: aload 9
    //   73: invokespecial 96	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   76: aload_0
    //   77: invokevirtual 102	java/lang/Object:getClass	()Ljava/lang/Class;
    //   80: astore 9
    //   82: aload 9
    //   84: invokevirtual 108	java/lang/Class:getSimpleName	()Ljava/lang/String;
    //   87: astore 9
    //   89: aload 8
    //   91: aload 9
    //   93: invokevirtual 112	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: ldc 114
    //   99: astore 9
    //   101: aload 8
    //   103: aload 9
    //   105: invokevirtual 112	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   108: pop
    //   109: invokestatic 74	android/os/SystemClock:elapsedRealtime	()J
    //   112: lload 4
    //   114: lsub
    //   115: lstore 10
    //   117: aload 8
    //   119: lload 10
    //   121: invokevirtual 117	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   124: pop
    //   125: aload 8
    //   127: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   130: astore_2
    //   131: aload 7
    //   133: iconst_0
    //   134: aload_2
    //   135: aastore
    //   136: invokestatic 74	android/os/SystemClock:elapsedRealtime	()J
    //   139: lstore 4
    //   141: aload_0
    //   142: aload_1
    //   143: invokevirtual 124	com/truecaller/search/local/model/b:a	(Landroid/database/Cursor;)Ljava/lang/Object;
    //   146: astore 7
    //   148: iload 6
    //   150: anewarray 89	java/lang/String
    //   153: astore 8
    //   155: new 91	java/lang/StringBuilder
    //   158: astore 9
    //   160: ldc 93
    //   162: astore 12
    //   164: aload 9
    //   166: aload 12
    //   168: invokespecial 96	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   171: aload_0
    //   172: invokevirtual 102	java/lang/Object:getClass	()Ljava/lang/Class;
    //   175: astore 12
    //   177: aload 12
    //   179: invokevirtual 108	java/lang/Class:getSimpleName	()Ljava/lang/String;
    //   182: astore 12
    //   184: aload 9
    //   186: aload 12
    //   188: invokevirtual 112	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   191: pop
    //   192: ldc 126
    //   194: astore 12
    //   196: aload 9
    //   198: aload 12
    //   200: invokevirtual 112	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   203: pop
    //   204: invokestatic 74	android/os/SystemClock:elapsedRealtime	()J
    //   207: lload 4
    //   209: lsub
    //   210: lstore 13
    //   212: aload 9
    //   214: lload 13
    //   216: invokevirtual 117	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   219: pop
    //   220: aload 9
    //   222: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   225: astore_2
    //   226: aload 8
    //   228: iconst_0
    //   229: aload_2
    //   230: aastore
    //   231: aload_0
    //   232: getfield 129	com/truecaller/search/local/model/a:d	Z
    //   235: istore 15
    //   237: iload 15
    //   239: ifeq +11 -> 250
    //   242: aload 7
    //   244: ifnull +6 -> 250
    //   247: goto +6 -> 253
    //   250: iconst_0
    //   251: istore 6
    //   253: aload_0
    //   254: iload 6
    //   256: putfield 129	com/truecaller/search/local/model/a:d	Z
    //   259: aload_1
    //   260: ifnull +9 -> 269
    //   263: aload_1
    //   264: invokeinterface 86 1 0
    //   269: aload_0
    //   270: iconst_0
    //   271: putfield 29	com/truecaller/search/local/model/b:i	Z
    //   274: aload 7
    //   276: areturn
    //   277: astore_2
    //   278: goto +6 -> 284
    //   281: astore_2
    //   282: aconst_null
    //   283: astore_1
    //   284: aload_1
    //   285: ifnull +9 -> 294
    //   288: aload_1
    //   289: invokeinterface 86 1 0
    //   294: aload_0
    //   295: iconst_0
    //   296: putfield 29	com/truecaller/search/local/model/b:i	Z
    //   299: aload_2
    //   300: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	301	0	this	b
    //   0	301	1	parama	b.a
    //   4	226	2	localObject1	Object
    //   277	1	2	localObject2	Object
    //   281	19	2	localObject3	Object
    //   8	3	3	j	int
    //   17	191	4	l1	long
    //   20	235	6	bool1	boolean
    //   58	217	7	localObject4	Object
    //   63	164	8	localObject5	Object
    //   67	154	9	localObject6	Object
    //   115	5	10	l2	long
    //   162	37	12	localObject7	Object
    //   210	5	13	l3	long
    //   235	3	15	bool2	boolean
    // Exception table:
    //   from	to	target	type
    //   53	58	277	finally
    //   60	63	277	finally
    //   71	76	277	finally
    //   76	80	277	finally
    //   82	87	277	finally
    //   91	97	277	finally
    //   103	109	277	finally
    //   109	112	277	finally
    //   119	125	277	finally
    //   125	130	277	finally
    //   134	136	277	finally
    //   136	139	277	finally
    //   142	146	277	finally
    //   148	153	277	finally
    //   155	158	277	finally
    //   166	171	277	finally
    //   171	175	277	finally
    //   177	182	277	finally
    //   186	192	277	finally
    //   198	204	277	finally
    //   204	207	277	finally
    //   214	220	277	finally
    //   220	225	277	finally
    //   229	231	277	finally
    //   231	235	277	finally
    //   254	259	277	finally
    //   22	28	281	finally
  }
  
  protected abstract Object a(Cursor paramCursor);
  
  public final void b()
  {
    super.b();
    f.removeMessages(8193);
  }
  
  protected final Object c()
  {
    -..Lambda.bVYCWtOJRXbNkp_wK1n12YYvLAE localbVYCWtOJRXbNkp_wK1n12YYvLAE = new com/truecaller/search/local/model/-$$Lambda$bVYCWtOJRXbNkp_wK1n12YYvLAE;
    localbVYCWtOJRXbNkp_wK1n12YYvLAE.<init>(this);
    return a(localbVYCWtOJRXbNkp_wK1n12YYvLAE);
  }
  
  protected final Object d()
  {
    -..Lambda.yVG5IOAde7FxXBy0CCDgCimB0q0 localyVG5IOAde7FxXBy0CCDgCimB0q0 = new com/truecaller/search/local/model/-$$Lambda$yVG5IOAde7FxXBy0CCDgCimB0q0;
    localyVG5IOAde7FxXBy0CCDgCimB0q0.<init>(this);
    return a(localyVG5IOAde7FxXBy0CCDgCimB0q0);
  }
  
  protected final Object e()
  {
    Object localObject = new com/truecaller/search/local/model/-$$Lambda$s2ZP48lW4NLgs0nSQxEIalDOCy4;
    ((-..Lambda.s2ZP48lW4NLgs0nSQxEIalDOCy4)localObject).<init>(this);
    localObject = a((b.a)localObject);
    h.incrementAndGet();
    return localObject;
  }
  
  protected abstract Object f();
  
  protected boolean g()
  {
    return false;
  }
  
  protected Cursor h()
  {
    return null;
  }
  
  public boolean handleMessage(Message paramMessage)
  {
    int j = what;
    String str1 = null;
    int k = 8193;
    if (j != k) {
      return false;
    }
    boolean bool = d;
    k = 1;
    if (bool)
    {
      paramMessage = new String[2];
      String str2 = "[%s] Notifying cache observer of change";
      paramMessage[0] = str2;
      str1 = getClass().getSimpleName();
      paramMessage[k] = str1;
      bool = c;
      if (bool)
      {
        paramMessage = a;
        paramMessage.a(this);
      }
      i = k;
    }
    return k;
  }
  
  protected Cursor i()
  {
    return null;
  }
  
  protected abstract Cursor j();
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */