package com.truecaller.search.local.model;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Looper;
import android.os.SystemClock;
import com.a.a.af;
import com.truecaller.common.h.am;
import com.truecaller.search.local.a.a;
import com.truecaller.search.local.a.c;
import com.truecaller.search.local.a.d;
import java.util.Comparator;

public final class f
  extends b
{
  public static final Comparator h;
  private static final f.a m;
  private static final String[] n;
  private static final String[] o = tmp153_152;
  volatile af i;
  volatile af j;
  volatile af k;
  volatile i l;
  private final c p;
  private final d q;
  private volatile af r;
  
  static
  {
    Object localObject = new com/truecaller/search/local/model/f$b;
    ((f.b)localObject).<init>();
    h = (Comparator)localObject;
    localObject = new com/truecaller/search/local/model/f$a;
    ((f.a)localObject).<init>();
    m = (f.a)localObject;
    String[] tmp29_26 = new String[22];
    String[] tmp30_29 = tmp29_26;
    String[] tmp30_29 = tmp29_26;
    tmp30_29[0] = "_id";
    tmp30_29[1] = "contact_name";
    String[] tmp39_30 = tmp30_29;
    String[] tmp39_30 = tmp30_29;
    tmp39_30[2] = "contact_alt_name";
    tmp39_30[3] = "contact_transliterated_name";
    String[] tmp48_39 = tmp39_30;
    String[] tmp48_39 = tmp39_30;
    tmp48_39[4] = "contact_about";
    tmp48_39[5] = "contact_job_title";
    String[] tmp57_48 = tmp48_39;
    String[] tmp57_48 = tmp48_39;
    tmp57_48[6] = "contact_company";
    tmp57_48[7] = "data_id";
    String[] tmp68_57 = tmp57_48;
    String[] tmp68_57 = tmp57_48;
    tmp68_57[8] = "data_is_primary";
    tmp68_57[9] = "data_phonebook_id";
    String[] tmp79_68 = tmp68_57;
    String[] tmp79_68 = tmp68_57;
    tmp79_68[10] = "data_raw_contact_id";
    tmp79_68[11] = "data_type";
    String[] tmp90_79 = tmp79_68;
    String[] tmp90_79 = tmp79_68;
    tmp90_79[12] = "data1";
    tmp90_79[13] = "data2";
    String[] tmp101_90 = tmp90_79;
    String[] tmp101_90 = tmp90_79;
    tmp101_90[14] = "data3";
    tmp101_90[15] = "data4";
    String[] tmp112_101 = tmp101_90;
    String[] tmp112_101 = tmp101_90;
    tmp112_101[16] = "data5";
    tmp112_101[17] = "data6";
    String[] tmp123_112 = tmp112_101;
    String[] tmp123_112 = tmp112_101;
    tmp123_112[18] = "data7";
    tmp123_112[19] = "data8";
    tmp123_112[20] = "data9";
    String[] tmp139_123 = tmp123_112;
    tmp139_123[21] = "data10";
    n = tmp139_123;
    String[] tmp152_149 = new String[3];
    String[] tmp153_152 = tmp152_149;
    String[] tmp153_152 = tmp152_149;
    tmp153_152[0] = "4";
    tmp153_152[1] = "3";
    tmp153_152[2] = "1";
  }
  
  public f(Context paramContext, Looper paramLooper, a.a parama, c paramc, d paramd)
  {
    super(paramContext, paramLooper, parama, localUri);
    p = paramc;
    q = paramd;
    paramContext = m;
    b(paramContext);
  }
  
  private static Cursor a(a parama)
  {
    long l1 = SystemClock.elapsedRealtime();
    Object localObject1 = parama.getReadableDatabase();
    Object localObject2 = n;
    localObject1 = ((SQLiteDatabase)localObject1).query("aggregated_contact_data_table", (String[])localObject2, null, null, null, null, null);
    String[] arrayOfString = new String[1];
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Queried ");
    parama = parama.getClass().getSimpleName();
    ((StringBuilder)localObject2).append(parama);
    ((StringBuilder)localObject2).append(" in ");
    long l2 = SystemClock.elapsedRealtime() - l1;
    ((StringBuilder)localObject2).append(l2);
    parama = ((StringBuilder)localObject2).toString();
    arrayOfString[0] = parama;
    return (Cursor)localObject1;
  }
  
  /* Error */
  private static f.a a(Context paramContext, Cursor paramCursor)
  {
    // Byte code:
    //   0: aload_1
    //   1: astore_2
    //   2: iconst_0
    //   3: istore_3
    //   4: aconst_null
    //   5: astore 4
    //   7: aload_1
    //   8: ifnonnull +5 -> 13
    //   11: aconst_null
    //   12: areturn
    //   13: aload_0
    //   14: invokestatic 165	com/truecaller/search/local/model/g:a	(Landroid/content/Context;)Lcom/truecaller/search/local/model/g;
    //   17: astore 5
    //   19: iconst_m1
    //   20: istore 6
    //   22: aload_1
    //   23: invokeinterface 171 1 0
    //   28: istore 6
    //   30: new 173	com/a/a/y
    //   33: astore 7
    //   35: ldc2_w 176
    //   38: dstore 8
    //   40: aload 7
    //   42: iload 6
    //   44: dload 8
    //   46: invokespecial 180	com/a/a/y:<init>	(ID)V
    //   49: new 182	java/util/TreeMap
    //   52: astore 10
    //   54: new 184	com/truecaller/search/local/model/f$c
    //   57: astore 11
    //   59: aload 11
    //   61: invokespecial 185	com/truecaller/search/local/model/f$c:<init>	()V
    //   64: aload 10
    //   66: aload 11
    //   68: invokespecial 188	java/util/TreeMap:<init>	(Ljava/util/Comparator;)V
    //   71: new 190	com/a/a/ae
    //   74: astore 12
    //   76: aload 12
    //   78: iload 6
    //   80: dload 8
    //   82: invokespecial 191	com/a/a/ae:<init>	(ID)V
    //   85: new 190	com/a/a/ae
    //   88: astore 13
    //   90: iload 6
    //   92: iconst_4
    //   93: idiv
    //   94: istore 14
    //   96: aload 13
    //   98: iload 14
    //   100: dload 8
    //   102: invokespecial 191	com/a/a/ae:<init>	(ID)V
    //   105: new 190	com/a/a/ae
    //   108: astore 15
    //   110: iload 6
    //   112: iconst_4
    //   113: idiv
    //   114: istore 14
    //   116: aload 15
    //   118: iload 14
    //   120: dload 8
    //   122: invokespecial 191	com/a/a/ae:<init>	(ID)V
    //   125: new 190	com/a/a/ae
    //   128: astore 16
    //   130: iload 6
    //   132: iconst_4
    //   133: idiv
    //   134: istore 14
    //   136: aload 16
    //   138: iload 14
    //   140: dload 8
    //   142: invokespecial 191	com/a/a/ae:<init>	(ID)V
    //   145: new 193	com/truecaller/search/local/model/i
    //   148: astore 17
    //   150: iload 6
    //   152: iconst_5
    //   153: idiv
    //   154: istore 18
    //   156: aload 17
    //   158: iload 18
    //   160: invokespecial 196	com/truecaller/search/local/model/i:<init>	(I)V
    //   163: new 198	com/truecaller/search/local/model/a/b$a
    //   166: astore 19
    //   168: aload 19
    //   170: aload_1
    //   171: aload 5
    //   173: invokespecial 201	com/truecaller/search/local/model/a/b$a:<init>	(Landroid/database/Cursor;Lcom/truecaller/search/local/model/g;)V
    //   176: iconst_0
    //   177: istore 14
    //   179: aconst_null
    //   180: astore 11
    //   182: aload 19
    //   184: getfield 204	com/truecaller/search/local/model/a/b$a:a	Landroid/database/Cursor;
    //   187: astore 20
    //   189: aload 20
    //   191: invokeinterface 208 1 0
    //   196: istore 21
    //   198: iload 21
    //   200: ifeq +1261 -> 1461
    //   203: aload 19
    //   205: invokevirtual 211	com/truecaller/search/local/model/a/b$a:a	()Lcom/truecaller/search/local/model/a/b;
    //   208: astore 20
    //   210: aload 20
    //   212: ifnull +1219 -> 1431
    //   215: aload 20
    //   217: instanceof 213
    //   220: istore 22
    //   222: iload 22
    //   224: ifeq +162 -> 386
    //   227: aload 20
    //   229: astore 23
    //   231: aload 20
    //   233: checkcast 213	com/truecaller/search/local/model/a/f
    //   236: astore 23
    //   238: aload 5
    //   240: astore 24
    //   242: aload 20
    //   244: checkcast 215	com/truecaller/search/local/model/a/b
    //   247: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   250: lstore 25
    //   252: lload 25
    //   254: invokestatic 225	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   257: astore 4
    //   259: aload 10
    //   261: aload 4
    //   263: invokevirtual 229	java/util/TreeMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   266: astore 4
    //   268: aload 4
    //   270: checkcast 231	java/util/Map
    //   273: astore 4
    //   275: aload 4
    //   277: ifnonnull +47 -> 324
    //   280: new 233	java/util/HashMap
    //   283: astore 4
    //   285: aload 4
    //   287: invokespecial 234	java/util/HashMap:<init>	()V
    //   290: iload 6
    //   292: istore 27
    //   294: aload 20
    //   296: checkcast 215	com/truecaller/search/local/model/a/b
    //   299: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   302: lstore 28
    //   304: lload 28
    //   306: invokestatic 225	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   309: astore 5
    //   311: aload 10
    //   313: aload 5
    //   315: aload 4
    //   317: invokevirtual 238	java/util/TreeMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   320: pop
    //   321: goto +7 -> 328
    //   324: iload 6
    //   326: istore 27
    //   328: aload 4
    //   330: aload 23
    //   332: invokeinterface 242 2 0
    //   337: istore 30
    //   339: iload 30
    //   341: ifeq +30 -> 371
    //   344: aload 4
    //   346: aload 23
    //   348: invokeinterface 245 2 0
    //   353: astore 5
    //   355: aload 5
    //   357: checkcast 213	com/truecaller/search/local/model/a/f
    //   360: astore 5
    //   362: aload 5
    //   364: aload 23
    //   366: invokestatic 248	com/truecaller/search/local/model/a/f:a	(Lcom/truecaller/search/local/model/a/f;Lcom/truecaller/search/local/model/a/f;)Lcom/truecaller/search/local/model/a/f;
    //   369: astore 23
    //   371: aload 4
    //   373: aload 23
    //   375: aload 23
    //   377: invokeinterface 249 3 0
    //   382: pop
    //   383: goto +186 -> 569
    //   386: aload 5
    //   388: astore 24
    //   390: iload 6
    //   392: istore 27
    //   394: aload 20
    //   396: instanceof 251
    //   399: istore_3
    //   400: iload_3
    //   401: ifeq +99 -> 500
    //   404: aload 20
    //   406: checkcast 215	com/truecaller/search/local/model/a/b
    //   409: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   412: lstore 25
    //   414: aload 15
    //   416: lload 25
    //   418: invokeinterface 256 3 0
    //   423: astore 4
    //   425: aload 4
    //   427: checkcast 258	java/util/SortedSet
    //   430: astore 4
    //   432: aload 4
    //   434: ifnonnull +42 -> 476
    //   437: new 260	java/util/TreeSet
    //   440: astore 4
    //   442: getstatic 30	com/truecaller/search/local/model/f:h	Ljava/util/Comparator;
    //   445: astore 5
    //   447: aload 4
    //   449: aload 5
    //   451: invokespecial 261	java/util/TreeSet:<init>	(Ljava/util/Comparator;)V
    //   454: aload 20
    //   456: checkcast 215	com/truecaller/search/local/model/a/b
    //   459: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   462: lstore 28
    //   464: aload 15
    //   466: lload 28
    //   468: aload 4
    //   470: invokeinterface 264 4 0
    //   475: pop
    //   476: aload 20
    //   478: astore 5
    //   480: aload 20
    //   482: checkcast 251	com/truecaller/search/local/model/a/t
    //   485: astore 5
    //   487: aload 4
    //   489: aload 5
    //   491: invokeinterface 267 2 0
    //   496: pop
    //   497: goto +72 -> 569
    //   500: aload 20
    //   502: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   505: lstore 25
    //   507: aload 13
    //   509: lload 25
    //   511: invokeinterface 256 3 0
    //   516: astore 4
    //   518: aload 4
    //   520: checkcast 269	java/util/Set
    //   523: astore 4
    //   525: aload 4
    //   527: ifnonnull +32 -> 559
    //   530: new 271	java/util/HashSet
    //   533: astore 4
    //   535: aload 4
    //   537: invokespecial 272	java/util/HashSet:<init>	()V
    //   540: aload 20
    //   542: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   545: lstore 28
    //   547: aload 13
    //   549: lload 28
    //   551: aload 4
    //   553: invokeinterface 264 4 0
    //   558: pop
    //   559: aload 4
    //   561: aload 20
    //   563: invokeinterface 273 2 0
    //   568: pop
    //   569: aload 20
    //   571: checkcast 215	com/truecaller/search/local/model/a/b
    //   574: getfield 276	com/truecaller/search/local/model/a/b:c	J
    //   577: lstore 25
    //   579: aload 12
    //   581: lload 25
    //   583: aload 20
    //   585: invokeinterface 264 4 0
    //   590: pop
    //   591: iload 14
    //   593: iconst_1
    //   594: iadd
    //   595: istore 22
    //   597: aload 20
    //   599: checkcast 215	com/truecaller/search/local/model/a/b
    //   602: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   605: lstore 25
    //   607: aload 7
    //   609: lload 25
    //   611: invokeinterface 281 3 0
    //   616: istore_3
    //   617: iload_3
    //   618: ifne +721 -> 1339
    //   621: aload 20
    //   623: checkcast 215	com/truecaller/search/local/model/a/b
    //   626: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   629: lstore 25
    //   631: aload 15
    //   633: lload 25
    //   635: invokeinterface 256 3 0
    //   640: astore 4
    //   642: aload 4
    //   644: checkcast 258	java/util/SortedSet
    //   647: astore 4
    //   649: aload 4
    //   651: ifnonnull +42 -> 693
    //   654: new 260	java/util/TreeSet
    //   657: astore 4
    //   659: getstatic 30	com/truecaller/search/local/model/f:h	Ljava/util/Comparator;
    //   662: astore 5
    //   664: aload 4
    //   666: aload 5
    //   668: invokespecial 261	java/util/TreeSet:<init>	(Ljava/util/Comparator;)V
    //   671: aload 20
    //   673: checkcast 215	com/truecaller/search/local/model/a/b
    //   676: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   679: lstore 28
    //   681: aload 15
    //   683: lload 28
    //   685: aload 4
    //   687: invokeinterface 264 4 0
    //   692: pop
    //   693: ldc 39
    //   695: astore 5
    //   697: aload_2
    //   698: aload 5
    //   700: invokeinterface 285 2 0
    //   705: istore 30
    //   707: aload_2
    //   708: iload 30
    //   710: invokeinterface 289 2 0
    //   715: astore 5
    //   717: aload 5
    //   719: invokestatic 295	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   722: istore 6
    //   724: iload 6
    //   726: ifne +89 -> 815
    //   729: new 297	com/truecaller/search/local/model/a/g
    //   732: astore 31
    //   734: aload 24
    //   736: astore 11
    //   738: aload 31
    //   740: aload 24
    //   742: invokespecial 300	com/truecaller/search/local/model/a/g:<init>	(Lcom/truecaller/search/local/model/g;)V
    //   745: aload 31
    //   747: aload 5
    //   749: putfield 303	com/truecaller/search/local/model/a/g:h	Ljava/lang/String;
    //   752: aload 13
    //   754: astore 5
    //   756: aload 15
    //   758: astore 24
    //   760: aload 20
    //   762: checkcast 215	com/truecaller/search/local/model/a/b
    //   765: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   768: lstore 32
    //   770: aload 31
    //   772: lload 32
    //   774: putfield 304	com/truecaller/search/local/model/a/g:d	J
    //   777: aload 4
    //   779: aload 31
    //   781: invokeinterface 267 2 0
    //   786: pop
    //   787: aload 31
    //   789: getfield 305	com/truecaller/search/local/model/a/g:c	J
    //   792: lstore 32
    //   794: aload 12
    //   796: lload 32
    //   798: aload 31
    //   800: invokeinterface 264 4 0
    //   805: pop
    //   806: iload 22
    //   808: iconst_1
    //   809: iadd
    //   810: istore 22
    //   812: goto +15 -> 827
    //   815: aload 13
    //   817: astore 5
    //   819: aload 24
    //   821: astore 11
    //   823: aload 15
    //   825: astore 24
    //   827: ldc 41
    //   829: astore 31
    //   831: aload_2
    //   832: aload 31
    //   834: invokeinterface 285 2 0
    //   839: istore 6
    //   841: aload_2
    //   842: iload 6
    //   844: invokeinterface 289 2 0
    //   849: astore 31
    //   851: aload 31
    //   853: invokestatic 295	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   856: istore 34
    //   858: iload 34
    //   860: ifne +81 -> 941
    //   863: new 307	com/truecaller/search/local/model/a/c
    //   866: astore 13
    //   868: iconst_1
    //   869: istore 35
    //   871: aload 13
    //   873: aload 11
    //   875: aload 31
    //   877: iload 35
    //   879: invokespecial 310	com/truecaller/search/local/model/a/c:<init>	(Lcom/truecaller/search/local/model/g;Ljava/lang/String;I)V
    //   882: aload 5
    //   884: astore 15
    //   886: aload 20
    //   888: checkcast 215	com/truecaller/search/local/model/a/b
    //   891: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   894: lstore 28
    //   896: aload 13
    //   898: lload 28
    //   900: putfield 311	com/truecaller/search/local/model/a/c:d	J
    //   903: aload 4
    //   905: aload 13
    //   907: invokeinterface 267 2 0
    //   912: pop
    //   913: aload 13
    //   915: getfield 312	com/truecaller/search/local/model/a/c:c	J
    //   918: lstore 28
    //   920: aload 12
    //   922: lload 28
    //   924: aload 13
    //   926: invokeinterface 264 4 0
    //   931: pop
    //   932: iload 22
    //   934: iconst_1
    //   935: iadd
    //   936: istore 22
    //   938: goto +7 -> 945
    //   941: aload 5
    //   943: astore 15
    //   945: ldc 43
    //   947: astore 5
    //   949: aload_2
    //   950: aload 5
    //   952: invokeinterface 285 2 0
    //   957: istore 30
    //   959: aload_2
    //   960: iload 30
    //   962: invokeinterface 289 2 0
    //   967: astore 5
    //   969: aload 5
    //   971: invokestatic 295	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   974: istore 6
    //   976: iload 6
    //   978: ifne +81 -> 1059
    //   981: new 307	com/truecaller/search/local/model/a/c
    //   984: astore 31
    //   986: iconst_2
    //   987: istore 34
    //   989: aload 31
    //   991: aload 11
    //   993: aload 5
    //   995: iload 34
    //   997: invokespecial 310	com/truecaller/search/local/model/a/c:<init>	(Lcom/truecaller/search/local/model/g;Ljava/lang/String;I)V
    //   1000: aload 15
    //   1002: astore 5
    //   1004: aload 20
    //   1006: checkcast 215	com/truecaller/search/local/model/a/b
    //   1009: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   1012: lstore 32
    //   1014: aload 31
    //   1016: lload 32
    //   1018: putfield 311	com/truecaller/search/local/model/a/c:d	J
    //   1021: aload 4
    //   1023: aload 31
    //   1025: invokeinterface 267 2 0
    //   1030: pop
    //   1031: aload 31
    //   1033: getfield 312	com/truecaller/search/local/model/a/c:c	J
    //   1036: lstore 32
    //   1038: aload 12
    //   1040: lload 32
    //   1042: aload 31
    //   1044: invokeinterface 264 4 0
    //   1049: pop
    //   1050: iload 22
    //   1052: iconst_1
    //   1053: iadd
    //   1054: istore 22
    //   1056: goto +7 -> 1063
    //   1059: aload 15
    //   1061: astore 5
    //   1063: ldc 45
    //   1065: astore 31
    //   1067: aload_2
    //   1068: aload 31
    //   1070: invokeinterface 285 2 0
    //   1075: istore 6
    //   1077: aload_2
    //   1078: iload 6
    //   1080: invokeinterface 289 2 0
    //   1085: astore 31
    //   1087: aload 31
    //   1089: invokestatic 295	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   1092: istore 34
    //   1094: iload 34
    //   1096: ifne +76 -> 1172
    //   1099: new 315	com/truecaller/search/local/model/a/d
    //   1102: astore 13
    //   1104: aload 13
    //   1106: aload 11
    //   1108: aload 31
    //   1110: invokespecial 318	com/truecaller/search/local/model/a/d:<init>	(Lcom/truecaller/search/local/model/g;Ljava/lang/String;)V
    //   1113: aload 5
    //   1115: astore 15
    //   1117: aload 20
    //   1119: checkcast 215	com/truecaller/search/local/model/a/b
    //   1122: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   1125: lstore 28
    //   1127: aload 13
    //   1129: lload 28
    //   1131: putfield 319	com/truecaller/search/local/model/a/d:d	J
    //   1134: aload 4
    //   1136: aload 13
    //   1138: invokeinterface 267 2 0
    //   1143: pop
    //   1144: aload 13
    //   1146: getfield 320	com/truecaller/search/local/model/a/d:c	J
    //   1149: lstore 28
    //   1151: aload 12
    //   1153: lload 28
    //   1155: aload 13
    //   1157: invokeinterface 264 4 0
    //   1162: pop
    //   1163: iload 22
    //   1165: iconst_1
    //   1166: iadd
    //   1167: istore 22
    //   1169: goto +7 -> 1176
    //   1172: aload 5
    //   1174: astore 15
    //   1176: ldc 49
    //   1178: astore 5
    //   1180: aload_2
    //   1181: aload 5
    //   1183: invokeinterface 285 2 0
    //   1188: istore 30
    //   1190: aload_2
    //   1191: iload 30
    //   1193: invokeinterface 289 2 0
    //   1198: astore 5
    //   1200: ldc 47
    //   1202: astore 31
    //   1204: aload_2
    //   1205: aload 31
    //   1207: invokeinterface 285 2 0
    //   1212: istore 6
    //   1214: aload_2
    //   1215: iload 6
    //   1217: invokeinterface 289 2 0
    //   1222: astore 31
    //   1224: aload 5
    //   1226: invokestatic 295	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   1229: istore 34
    //   1231: iload 34
    //   1233: ifeq +15 -> 1248
    //   1236: aload 31
    //   1238: invokestatic 295	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   1241: istore 34
    //   1243: iload 34
    //   1245: ifne +71 -> 1316
    //   1248: new 322	com/truecaller/search/local/model/a/e
    //   1251: astore 13
    //   1253: aload 13
    //   1255: aload 11
    //   1257: aload 5
    //   1259: aload 31
    //   1261: invokespecial 325	com/truecaller/search/local/model/a/e:<init>	(Lcom/truecaller/search/local/model/g;Ljava/lang/String;Ljava/lang/String;)V
    //   1264: aload 20
    //   1266: checkcast 215	com/truecaller/search/local/model/a/b
    //   1269: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   1272: lstore 28
    //   1274: aload 13
    //   1276: lload 28
    //   1278: putfield 326	com/truecaller/search/local/model/a/e:d	J
    //   1281: aload 4
    //   1283: aload 13
    //   1285: invokeinterface 267 2 0
    //   1290: pop
    //   1291: aload 13
    //   1293: getfield 327	com/truecaller/search/local/model/a/e:c	J
    //   1296: lstore 25
    //   1298: aload 12
    //   1300: lload 25
    //   1302: aload 13
    //   1304: invokeinterface 264 4 0
    //   1309: pop
    //   1310: iload 22
    //   1312: iconst_1
    //   1313: iadd
    //   1314: istore 22
    //   1316: aload 20
    //   1318: checkcast 215	com/truecaller/search/local/model/a/b
    //   1321: getfield 219	com/truecaller/search/local/model/a/b:d	J
    //   1324: lstore 25
    //   1326: aload 7
    //   1328: lload 25
    //   1330: invokeinterface 329 3 0
    //   1335: pop
    //   1336: goto +15 -> 1351
    //   1339: aload 24
    //   1341: astore 11
    //   1343: aload 15
    //   1345: astore 24
    //   1347: aload 13
    //   1349: astore 15
    //   1351: aload 11
    //   1353: astore 5
    //   1355: aload 15
    //   1357: astore 13
    //   1359: aload 24
    //   1361: astore 15
    //   1363: iload 27
    //   1365: istore 6
    //   1367: iconst_0
    //   1368: istore_3
    //   1369: aconst_null
    //   1370: astore 4
    //   1372: iload 22
    //   1374: istore 14
    //   1376: goto -1194 -> 182
    //   1379: astore 36
    //   1381: aload 36
    //   1383: astore 4
    //   1385: goto +22 -> 1407
    //   1388: astore 36
    //   1390: aload 36
    //   1392: astore 4
    //   1394: goto +30 -> 1424
    //   1397: astore 36
    //   1399: aload 36
    //   1401: astore 4
    //   1403: iload 14
    //   1405: istore 22
    //   1407: iload 27
    //   1409: istore 6
    //   1411: goto +428 -> 1839
    //   1414: astore 36
    //   1416: aload 36
    //   1418: astore 4
    //   1420: iload 14
    //   1422: istore 22
    //   1424: iload 27
    //   1426: istore 6
    //   1428: goto +504 -> 1932
    //   1431: aload 15
    //   1433: astore 24
    //   1435: iconst_0
    //   1436: istore_3
    //   1437: aconst_null
    //   1438: astore 4
    //   1440: goto -1258 -> 182
    //   1443: astore 36
    //   1445: iload 6
    //   1447: istore 27
    //   1449: goto +345 -> 1794
    //   1452: astore 36
    //   1454: iload 6
    //   1456: istore 27
    //   1458: goto +349 -> 1807
    //   1461: iload 6
    //   1463: istore 27
    //   1465: aload 15
    //   1467: astore 24
    //   1469: aload 13
    //   1471: astore 15
    //   1473: aload 10
    //   1475: invokevirtual 333	java/util/TreeMap:entrySet	()Ljava/util/Set;
    //   1478: astore 4
    //   1480: aload 4
    //   1482: invokeinterface 337 1 0
    //   1487: astore 4
    //   1489: aload 4
    //   1491: invokeinterface 342 1 0
    //   1496: istore 30
    //   1498: iload 30
    //   1500: ifeq +153 -> 1653
    //   1503: aload 4
    //   1505: invokeinterface 346 1 0
    //   1510: astore 5
    //   1512: aload 5
    //   1514: checkcast 348	java/util/Map$Entry
    //   1517: astore 5
    //   1519: aload 5
    //   1521: invokeinterface 351 1 0
    //   1526: astore 31
    //   1528: aload 31
    //   1530: checkcast 221	java/lang/Long
    //   1533: astore 31
    //   1535: aload 31
    //   1537: invokevirtual 354	java/lang/Long:longValue	()J
    //   1540: lstore 37
    //   1542: aload 5
    //   1544: invokeinterface 357 1 0
    //   1549: astore 7
    //   1551: aload 7
    //   1553: checkcast 231	java/util/Map
    //   1556: astore 7
    //   1558: aload 7
    //   1560: invokeinterface 360 1 0
    //   1565: astore 7
    //   1567: aload 16
    //   1569: lload 37
    //   1571: aload 7
    //   1573: invokeinterface 264 4 0
    //   1578: pop
    //   1579: aload 5
    //   1581: invokeinterface 357 1 0
    //   1586: astore 5
    //   1588: aload 5
    //   1590: checkcast 231	java/util/Map
    //   1593: astore 5
    //   1595: aload 5
    //   1597: invokeinterface 360 1 0
    //   1602: astore 5
    //   1604: aload 5
    //   1606: invokeinterface 337 1 0
    //   1611: astore 5
    //   1613: aload 5
    //   1615: invokeinterface 342 1 0
    //   1620: istore 6
    //   1622: iload 6
    //   1624: ifeq -135 -> 1489
    //   1627: aload 5
    //   1629: invokeinterface 346 1 0
    //   1634: astore 31
    //   1636: aload 31
    //   1638: checkcast 213	com/truecaller/search/local/model/a/f
    //   1641: astore 31
    //   1643: aload 17
    //   1645: aload 31
    //   1647: invokevirtual 363	com/truecaller/search/local/model/i:a	(Lcom/truecaller/search/local/model/i$a;)V
    //   1650: goto -37 -> 1613
    //   1653: iconst_1
    //   1654: istore_3
    //   1655: iload_3
    //   1656: anewarray 81	java/lang/String
    //   1659: astore 4
    //   1661: new 130	java/lang/StringBuilder
    //   1664: astore 5
    //   1666: ldc_w 365
    //   1669: astore 31
    //   1671: aload 5
    //   1673: aload 31
    //   1675: invokespecial 135	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1678: iload 27
    //   1680: istore 6
    //   1682: aload 5
    //   1684: iload 27
    //   1686: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1689: pop
    //   1690: ldc_w 370
    //   1693: astore 23
    //   1695: aload 5
    //   1697: aload 23
    //   1699: invokevirtual 151	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1702: pop
    //   1703: aload 5
    //   1705: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1708: astore 5
    //   1710: iconst_0
    //   1711: istore 22
    //   1713: aconst_null
    //   1714: astore 23
    //   1716: aload 4
    //   1718: iconst_0
    //   1719: aload 5
    //   1721: aastore
    //   1722: new 32	com/truecaller/search/local/model/f$a
    //   1725: astore 4
    //   1727: aload 17
    //   1729: invokevirtual 373	com/truecaller/search/local/model/i:a	()Lcom/truecaller/search/local/model/i;
    //   1732: astore 5
    //   1734: aload 4
    //   1736: astore 20
    //   1738: aload 15
    //   1740: astore 13
    //   1742: aload 24
    //   1744: astore 15
    //   1746: aload 5
    //   1748: astore 24
    //   1750: aload 4
    //   1752: aload 12
    //   1754: aload 13
    //   1756: aload 15
    //   1758: aload 16
    //   1760: aload 5
    //   1762: invokespecial 376	com/truecaller/search/local/model/f$a:<init>	(Lcom/a/a/af;Lcom/a/a/af;Lcom/a/a/af;Lcom/a/a/af;Lcom/truecaller/search/local/model/i;)V
    //   1765: aload_1
    //   1766: invokeinterface 379 1 0
    //   1771: aload 4
    //   1773: areturn
    //   1774: astore 36
    //   1776: iload 27
    //   1778: istore 6
    //   1780: goto +14 -> 1794
    //   1783: astore 36
    //   1785: iload 27
    //   1787: istore 6
    //   1789: goto +18 -> 1807
    //   1792: astore 36
    //   1794: aload 36
    //   1796: astore 4
    //   1798: iload 14
    //   1800: istore 22
    //   1802: goto +37 -> 1839
    //   1805: astore 36
    //   1807: aload 36
    //   1809: astore 4
    //   1811: iload 14
    //   1813: istore 22
    //   1815: goto +117 -> 1932
    //   1818: astore 36
    //   1820: aload 36
    //   1822: astore 4
    //   1824: goto +189 -> 2013
    //   1827: astore 36
    //   1829: iconst_0
    //   1830: istore 22
    //   1832: aconst_null
    //   1833: astore 23
    //   1835: aload 36
    //   1837: astore 4
    //   1839: new 130	java/lang/StringBuilder
    //   1842: astore 5
    //   1844: ldc_w 381
    //   1847: astore 7
    //   1849: aload 5
    //   1851: aload 7
    //   1853: invokespecial 135	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1856: aload 5
    //   1858: iload 22
    //   1860: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1863: pop
    //   1864: ldc_w 383
    //   1867: astore 23
    //   1869: aload 5
    //   1871: aload 23
    //   1873: invokevirtual 151	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1876: pop
    //   1877: aload 5
    //   1879: iload 6
    //   1881: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1884: pop
    //   1885: ldc_w 385
    //   1888: astore 31
    //   1890: aload 5
    //   1892: aload 31
    //   1894: invokevirtual 151	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1897: pop
    //   1898: aload 5
    //   1900: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1903: astore 5
    //   1905: aload 4
    //   1907: aload 5
    //   1909: invokestatic 390	com/truecaller/log/d:a	(Ljava/lang/Throwable;Ljava/lang/String;)V
    //   1912: aload_1
    //   1913: invokeinterface 379 1 0
    //   1918: aconst_null
    //   1919: areturn
    //   1920: astore 36
    //   1922: iconst_0
    //   1923: istore 22
    //   1925: aconst_null
    //   1926: astore 23
    //   1928: aload 36
    //   1930: astore 4
    //   1932: new 130	java/lang/StringBuilder
    //   1935: astore 5
    //   1937: ldc_w 392
    //   1940: astore 7
    //   1942: aload 5
    //   1944: aload 7
    //   1946: invokespecial 135	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1949: aload 5
    //   1951: iload 22
    //   1953: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1956: pop
    //   1957: ldc_w 383
    //   1960: astore 23
    //   1962: aload 5
    //   1964: aload 23
    //   1966: invokevirtual 151	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1969: pop
    //   1970: aload 5
    //   1972: iload 6
    //   1974: invokevirtual 368	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1977: pop
    //   1978: ldc_w 385
    //   1981: astore 31
    //   1983: aload 5
    //   1985: aload 31
    //   1987: invokevirtual 151	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1990: pop
    //   1991: aload 5
    //   1993: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1996: astore 5
    //   1998: aload 4
    //   2000: aload 5
    //   2002: invokestatic 390	com/truecaller/log/d:a	(Ljava/lang/Throwable;Ljava/lang/String;)V
    //   2005: aload_1
    //   2006: invokeinterface 379 1 0
    //   2011: aconst_null
    //   2012: areturn
    //   2013: aload_1
    //   2014: invokeinterface 379 1 0
    //   2019: aload 36
    //   2021: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	2022	0	paramContext	Context
    //   0	2022	1	paramCursor	Cursor
    //   1	1214	2	localCursor	Cursor
    //   3	1653	3	bool1	boolean
    //   5	1994	4	localObject1	Object
    //   17	1984	5	localObject2	Object
    //   20	371	6	i1	int
    //   722	3	6	bool2	boolean
    //   839	4	6	i2	int
    //   974	3	6	bool3	boolean
    //   1075	387	6	i3	int
    //   1620	353	6	i4	int
    //   33	1912	7	localObject3	Object
    //   38	103	8	d	double
    //   52	1422	10	localTreeMap	java.util.TreeMap
    //   57	1295	11	localObject4	Object
    //   74	1679	12	localae1	com.a.a.ae
    //   88	1667	13	localObject5	Object
    //   94	1718	14	i5	int
    //   108	1649	15	localObject6	Object
    //   128	1631	16	localae2	com.a.a.ae
    //   148	1580	17	locali	i
    //   154	5	18	i6	int
    //   166	38	19	locala	com.truecaller.search.local.model.a.b.a
    //   187	1550	20	localObject7	Object
    //   196	3	21	bool4	boolean
    //   220	3	22	bool5	boolean
    //   595	1357	22	i7	int
    //   229	1736	23	localObject8	Object
    //   240	1509	24	localObject9	Object
    //   250	1079	25	l1	long
    //   292	1494	27	i8	int
    //   302	975	28	l2	long
    //   337	3	30	bool6	boolean
    //   705	487	30	i9	int
    //   1496	3	30	bool7	boolean
    //   732	1254	31	localObject10	Object
    //   768	273	32	l3	long
    //   856	3	34	bool8	boolean
    //   987	9	34	i10	int
    //   1092	152	34	bool9	boolean
    //   869	9	35	i11	int
    //   1379	3	36	localException1	Exception
    //   1388	3	36	localIllegalStateException1	IllegalStateException
    //   1397	3	36	localException2	Exception
    //   1414	3	36	localIllegalStateException2	IllegalStateException
    //   1443	1	36	localException3	Exception
    //   1452	1	36	localIllegalStateException3	IllegalStateException
    //   1774	1	36	localException4	Exception
    //   1783	1	36	localIllegalStateException4	IllegalStateException
    //   1792	3	36	localException5	Exception
    //   1805	3	36	localIllegalStateException5	IllegalStateException
    //   1818	3	36	localObject11	Object
    //   1827	9	36	localException6	Exception
    //   1920	100	36	localIllegalStateException6	IllegalStateException
    //   1540	30	37	l4	long
    // Exception table:
    //   from	to	target	type
    //   597	605	1379	java/lang/Exception
    //   609	616	1379	java/lang/Exception
    //   621	629	1379	java/lang/Exception
    //   633	640	1379	java/lang/Exception
    //   642	647	1379	java/lang/Exception
    //   654	657	1379	java/lang/Exception
    //   659	662	1379	java/lang/Exception
    //   666	671	1379	java/lang/Exception
    //   671	679	1379	java/lang/Exception
    //   685	693	1379	java/lang/Exception
    //   698	705	1379	java/lang/Exception
    //   708	715	1379	java/lang/Exception
    //   717	722	1379	java/lang/Exception
    //   729	732	1379	java/lang/Exception
    //   740	745	1379	java/lang/Exception
    //   747	752	1379	java/lang/Exception
    //   760	768	1379	java/lang/Exception
    //   772	777	1379	java/lang/Exception
    //   779	787	1379	java/lang/Exception
    //   787	792	1379	java/lang/Exception
    //   798	806	1379	java/lang/Exception
    //   832	839	1379	java/lang/Exception
    //   842	849	1379	java/lang/Exception
    //   851	856	1379	java/lang/Exception
    //   863	866	1379	java/lang/Exception
    //   877	882	1379	java/lang/Exception
    //   886	894	1379	java/lang/Exception
    //   898	903	1379	java/lang/Exception
    //   905	913	1379	java/lang/Exception
    //   913	918	1379	java/lang/Exception
    //   924	932	1379	java/lang/Exception
    //   950	957	1379	java/lang/Exception
    //   960	967	1379	java/lang/Exception
    //   969	974	1379	java/lang/Exception
    //   981	984	1379	java/lang/Exception
    //   995	1000	1379	java/lang/Exception
    //   1004	1012	1379	java/lang/Exception
    //   1016	1021	1379	java/lang/Exception
    //   1023	1031	1379	java/lang/Exception
    //   1031	1036	1379	java/lang/Exception
    //   1042	1050	1379	java/lang/Exception
    //   1068	1075	1379	java/lang/Exception
    //   1078	1085	1379	java/lang/Exception
    //   1087	1092	1379	java/lang/Exception
    //   1099	1102	1379	java/lang/Exception
    //   1108	1113	1379	java/lang/Exception
    //   1117	1125	1379	java/lang/Exception
    //   1129	1134	1379	java/lang/Exception
    //   1136	1144	1379	java/lang/Exception
    //   1144	1149	1379	java/lang/Exception
    //   1155	1163	1379	java/lang/Exception
    //   1181	1188	1379	java/lang/Exception
    //   1191	1198	1379	java/lang/Exception
    //   1205	1212	1379	java/lang/Exception
    //   1215	1222	1379	java/lang/Exception
    //   1224	1229	1379	java/lang/Exception
    //   1236	1241	1379	java/lang/Exception
    //   1248	1251	1379	java/lang/Exception
    //   1259	1264	1379	java/lang/Exception
    //   1264	1272	1379	java/lang/Exception
    //   1276	1281	1379	java/lang/Exception
    //   1283	1291	1379	java/lang/Exception
    //   1291	1296	1379	java/lang/Exception
    //   1302	1310	1379	java/lang/Exception
    //   1316	1324	1379	java/lang/Exception
    //   1328	1336	1379	java/lang/Exception
    //   597	605	1388	java/lang/IllegalStateException
    //   609	616	1388	java/lang/IllegalStateException
    //   621	629	1388	java/lang/IllegalStateException
    //   633	640	1388	java/lang/IllegalStateException
    //   642	647	1388	java/lang/IllegalStateException
    //   654	657	1388	java/lang/IllegalStateException
    //   659	662	1388	java/lang/IllegalStateException
    //   666	671	1388	java/lang/IllegalStateException
    //   671	679	1388	java/lang/IllegalStateException
    //   685	693	1388	java/lang/IllegalStateException
    //   698	705	1388	java/lang/IllegalStateException
    //   708	715	1388	java/lang/IllegalStateException
    //   717	722	1388	java/lang/IllegalStateException
    //   729	732	1388	java/lang/IllegalStateException
    //   740	745	1388	java/lang/IllegalStateException
    //   747	752	1388	java/lang/IllegalStateException
    //   760	768	1388	java/lang/IllegalStateException
    //   772	777	1388	java/lang/IllegalStateException
    //   779	787	1388	java/lang/IllegalStateException
    //   787	792	1388	java/lang/IllegalStateException
    //   798	806	1388	java/lang/IllegalStateException
    //   832	839	1388	java/lang/IllegalStateException
    //   842	849	1388	java/lang/IllegalStateException
    //   851	856	1388	java/lang/IllegalStateException
    //   863	866	1388	java/lang/IllegalStateException
    //   877	882	1388	java/lang/IllegalStateException
    //   886	894	1388	java/lang/IllegalStateException
    //   898	903	1388	java/lang/IllegalStateException
    //   905	913	1388	java/lang/IllegalStateException
    //   913	918	1388	java/lang/IllegalStateException
    //   924	932	1388	java/lang/IllegalStateException
    //   950	957	1388	java/lang/IllegalStateException
    //   960	967	1388	java/lang/IllegalStateException
    //   969	974	1388	java/lang/IllegalStateException
    //   981	984	1388	java/lang/IllegalStateException
    //   995	1000	1388	java/lang/IllegalStateException
    //   1004	1012	1388	java/lang/IllegalStateException
    //   1016	1021	1388	java/lang/IllegalStateException
    //   1023	1031	1388	java/lang/IllegalStateException
    //   1031	1036	1388	java/lang/IllegalStateException
    //   1042	1050	1388	java/lang/IllegalStateException
    //   1068	1075	1388	java/lang/IllegalStateException
    //   1078	1085	1388	java/lang/IllegalStateException
    //   1087	1092	1388	java/lang/IllegalStateException
    //   1099	1102	1388	java/lang/IllegalStateException
    //   1108	1113	1388	java/lang/IllegalStateException
    //   1117	1125	1388	java/lang/IllegalStateException
    //   1129	1134	1388	java/lang/IllegalStateException
    //   1136	1144	1388	java/lang/IllegalStateException
    //   1144	1149	1388	java/lang/IllegalStateException
    //   1155	1163	1388	java/lang/IllegalStateException
    //   1181	1188	1388	java/lang/IllegalStateException
    //   1191	1198	1388	java/lang/IllegalStateException
    //   1205	1212	1388	java/lang/IllegalStateException
    //   1215	1222	1388	java/lang/IllegalStateException
    //   1224	1229	1388	java/lang/IllegalStateException
    //   1236	1241	1388	java/lang/IllegalStateException
    //   1248	1251	1388	java/lang/IllegalStateException
    //   1259	1264	1388	java/lang/IllegalStateException
    //   1264	1272	1388	java/lang/IllegalStateException
    //   1276	1281	1388	java/lang/IllegalStateException
    //   1283	1291	1388	java/lang/IllegalStateException
    //   1291	1296	1388	java/lang/IllegalStateException
    //   1302	1310	1388	java/lang/IllegalStateException
    //   1316	1324	1388	java/lang/IllegalStateException
    //   1328	1336	1388	java/lang/IllegalStateException
    //   294	302	1397	java/lang/Exception
    //   304	309	1397	java/lang/Exception
    //   315	321	1397	java/lang/Exception
    //   330	337	1397	java/lang/Exception
    //   346	353	1397	java/lang/Exception
    //   355	360	1397	java/lang/Exception
    //   364	369	1397	java/lang/Exception
    //   375	383	1397	java/lang/Exception
    //   404	412	1397	java/lang/Exception
    //   416	423	1397	java/lang/Exception
    //   425	430	1397	java/lang/Exception
    //   437	440	1397	java/lang/Exception
    //   442	445	1397	java/lang/Exception
    //   449	454	1397	java/lang/Exception
    //   454	462	1397	java/lang/Exception
    //   468	476	1397	java/lang/Exception
    //   480	485	1397	java/lang/Exception
    //   489	497	1397	java/lang/Exception
    //   500	505	1397	java/lang/Exception
    //   509	516	1397	java/lang/Exception
    //   518	523	1397	java/lang/Exception
    //   530	533	1397	java/lang/Exception
    //   535	540	1397	java/lang/Exception
    //   540	545	1397	java/lang/Exception
    //   551	559	1397	java/lang/Exception
    //   561	569	1397	java/lang/Exception
    //   569	577	1397	java/lang/Exception
    //   583	591	1397	java/lang/Exception
    //   1503	1510	1397	java/lang/Exception
    //   1512	1517	1397	java/lang/Exception
    //   1519	1526	1397	java/lang/Exception
    //   1528	1533	1397	java/lang/Exception
    //   1535	1540	1397	java/lang/Exception
    //   1542	1549	1397	java/lang/Exception
    //   1551	1556	1397	java/lang/Exception
    //   1558	1565	1397	java/lang/Exception
    //   1571	1579	1397	java/lang/Exception
    //   1579	1586	1397	java/lang/Exception
    //   1588	1593	1397	java/lang/Exception
    //   1595	1602	1397	java/lang/Exception
    //   1604	1611	1397	java/lang/Exception
    //   1613	1620	1397	java/lang/Exception
    //   1627	1634	1397	java/lang/Exception
    //   1636	1641	1397	java/lang/Exception
    //   1645	1650	1397	java/lang/Exception
    //   294	302	1414	java/lang/IllegalStateException
    //   304	309	1414	java/lang/IllegalStateException
    //   315	321	1414	java/lang/IllegalStateException
    //   330	337	1414	java/lang/IllegalStateException
    //   346	353	1414	java/lang/IllegalStateException
    //   355	360	1414	java/lang/IllegalStateException
    //   364	369	1414	java/lang/IllegalStateException
    //   375	383	1414	java/lang/IllegalStateException
    //   404	412	1414	java/lang/IllegalStateException
    //   416	423	1414	java/lang/IllegalStateException
    //   425	430	1414	java/lang/IllegalStateException
    //   437	440	1414	java/lang/IllegalStateException
    //   442	445	1414	java/lang/IllegalStateException
    //   449	454	1414	java/lang/IllegalStateException
    //   454	462	1414	java/lang/IllegalStateException
    //   468	476	1414	java/lang/IllegalStateException
    //   480	485	1414	java/lang/IllegalStateException
    //   489	497	1414	java/lang/IllegalStateException
    //   500	505	1414	java/lang/IllegalStateException
    //   509	516	1414	java/lang/IllegalStateException
    //   518	523	1414	java/lang/IllegalStateException
    //   530	533	1414	java/lang/IllegalStateException
    //   535	540	1414	java/lang/IllegalStateException
    //   540	545	1414	java/lang/IllegalStateException
    //   551	559	1414	java/lang/IllegalStateException
    //   561	569	1414	java/lang/IllegalStateException
    //   569	577	1414	java/lang/IllegalStateException
    //   583	591	1414	java/lang/IllegalStateException
    //   1503	1510	1414	java/lang/IllegalStateException
    //   1512	1517	1414	java/lang/IllegalStateException
    //   1519	1526	1414	java/lang/IllegalStateException
    //   1528	1533	1414	java/lang/IllegalStateException
    //   1535	1540	1414	java/lang/IllegalStateException
    //   1542	1549	1414	java/lang/IllegalStateException
    //   1551	1556	1414	java/lang/IllegalStateException
    //   1558	1565	1414	java/lang/IllegalStateException
    //   1571	1579	1414	java/lang/IllegalStateException
    //   1579	1586	1414	java/lang/IllegalStateException
    //   1588	1593	1414	java/lang/IllegalStateException
    //   1595	1602	1414	java/lang/IllegalStateException
    //   1604	1611	1414	java/lang/IllegalStateException
    //   1613	1620	1414	java/lang/IllegalStateException
    //   1627	1634	1414	java/lang/IllegalStateException
    //   1636	1641	1414	java/lang/IllegalStateException
    //   1645	1650	1414	java/lang/IllegalStateException
    //   203	208	1443	java/lang/Exception
    //   231	236	1443	java/lang/Exception
    //   242	250	1443	java/lang/Exception
    //   252	257	1443	java/lang/Exception
    //   261	266	1443	java/lang/Exception
    //   268	273	1443	java/lang/Exception
    //   280	283	1443	java/lang/Exception
    //   285	290	1443	java/lang/Exception
    //   203	208	1452	java/lang/IllegalStateException
    //   231	236	1452	java/lang/IllegalStateException
    //   242	250	1452	java/lang/IllegalStateException
    //   252	257	1452	java/lang/IllegalStateException
    //   261	266	1452	java/lang/IllegalStateException
    //   268	273	1452	java/lang/IllegalStateException
    //   280	283	1452	java/lang/IllegalStateException
    //   285	290	1452	java/lang/IllegalStateException
    //   1473	1478	1774	java/lang/Exception
    //   1480	1487	1774	java/lang/Exception
    //   1489	1496	1774	java/lang/Exception
    //   1655	1659	1774	java/lang/Exception
    //   1661	1664	1774	java/lang/Exception
    //   1673	1678	1774	java/lang/Exception
    //   1473	1478	1783	java/lang/IllegalStateException
    //   1480	1487	1783	java/lang/IllegalStateException
    //   1489	1496	1783	java/lang/IllegalStateException
    //   1655	1659	1783	java/lang/IllegalStateException
    //   1661	1664	1783	java/lang/IllegalStateException
    //   1673	1678	1783	java/lang/IllegalStateException
    //   182	187	1792	java/lang/Exception
    //   189	196	1792	java/lang/Exception
    //   1684	1690	1792	java/lang/Exception
    //   1697	1703	1792	java/lang/Exception
    //   1703	1708	1792	java/lang/Exception
    //   1719	1722	1792	java/lang/Exception
    //   1722	1725	1792	java/lang/Exception
    //   1727	1732	1792	java/lang/Exception
    //   1760	1765	1792	java/lang/Exception
    //   182	187	1805	java/lang/IllegalStateException
    //   189	196	1805	java/lang/IllegalStateException
    //   1684	1690	1805	java/lang/IllegalStateException
    //   1697	1703	1805	java/lang/IllegalStateException
    //   1703	1708	1805	java/lang/IllegalStateException
    //   1719	1722	1805	java/lang/IllegalStateException
    //   1722	1725	1805	java/lang/IllegalStateException
    //   1727	1732	1805	java/lang/IllegalStateException
    //   1760	1765	1805	java/lang/IllegalStateException
    //   22	28	1818	finally
    //   30	33	1818	finally
    //   44	49	1818	finally
    //   49	52	1818	finally
    //   54	57	1818	finally
    //   59	64	1818	finally
    //   66	71	1818	finally
    //   71	74	1818	finally
    //   80	85	1818	finally
    //   85	88	1818	finally
    //   92	94	1818	finally
    //   100	105	1818	finally
    //   105	108	1818	finally
    //   112	114	1818	finally
    //   120	125	1818	finally
    //   125	128	1818	finally
    //   132	134	1818	finally
    //   140	145	1818	finally
    //   145	148	1818	finally
    //   152	154	1818	finally
    //   158	163	1818	finally
    //   163	166	1818	finally
    //   171	176	1818	finally
    //   182	187	1818	finally
    //   189	196	1818	finally
    //   203	208	1818	finally
    //   231	236	1818	finally
    //   242	250	1818	finally
    //   252	257	1818	finally
    //   261	266	1818	finally
    //   268	273	1818	finally
    //   280	283	1818	finally
    //   285	290	1818	finally
    //   294	302	1818	finally
    //   304	309	1818	finally
    //   315	321	1818	finally
    //   330	337	1818	finally
    //   346	353	1818	finally
    //   355	360	1818	finally
    //   364	369	1818	finally
    //   375	383	1818	finally
    //   404	412	1818	finally
    //   416	423	1818	finally
    //   425	430	1818	finally
    //   437	440	1818	finally
    //   442	445	1818	finally
    //   449	454	1818	finally
    //   454	462	1818	finally
    //   468	476	1818	finally
    //   480	485	1818	finally
    //   489	497	1818	finally
    //   500	505	1818	finally
    //   509	516	1818	finally
    //   518	523	1818	finally
    //   530	533	1818	finally
    //   535	540	1818	finally
    //   540	545	1818	finally
    //   551	559	1818	finally
    //   561	569	1818	finally
    //   569	577	1818	finally
    //   583	591	1818	finally
    //   597	605	1818	finally
    //   609	616	1818	finally
    //   621	629	1818	finally
    //   633	640	1818	finally
    //   642	647	1818	finally
    //   654	657	1818	finally
    //   659	662	1818	finally
    //   666	671	1818	finally
    //   671	679	1818	finally
    //   685	693	1818	finally
    //   698	705	1818	finally
    //   708	715	1818	finally
    //   717	722	1818	finally
    //   729	732	1818	finally
    //   740	745	1818	finally
    //   747	752	1818	finally
    //   760	768	1818	finally
    //   772	777	1818	finally
    //   779	787	1818	finally
    //   787	792	1818	finally
    //   798	806	1818	finally
    //   832	839	1818	finally
    //   842	849	1818	finally
    //   851	856	1818	finally
    //   863	866	1818	finally
    //   877	882	1818	finally
    //   886	894	1818	finally
    //   898	903	1818	finally
    //   905	913	1818	finally
    //   913	918	1818	finally
    //   924	932	1818	finally
    //   950	957	1818	finally
    //   960	967	1818	finally
    //   969	974	1818	finally
    //   981	984	1818	finally
    //   995	1000	1818	finally
    //   1004	1012	1818	finally
    //   1016	1021	1818	finally
    //   1023	1031	1818	finally
    //   1031	1036	1818	finally
    //   1042	1050	1818	finally
    //   1068	1075	1818	finally
    //   1078	1085	1818	finally
    //   1087	1092	1818	finally
    //   1099	1102	1818	finally
    //   1108	1113	1818	finally
    //   1117	1125	1818	finally
    //   1129	1134	1818	finally
    //   1136	1144	1818	finally
    //   1144	1149	1818	finally
    //   1155	1163	1818	finally
    //   1181	1188	1818	finally
    //   1191	1198	1818	finally
    //   1205	1212	1818	finally
    //   1215	1222	1818	finally
    //   1224	1229	1818	finally
    //   1236	1241	1818	finally
    //   1248	1251	1818	finally
    //   1259	1264	1818	finally
    //   1264	1272	1818	finally
    //   1276	1281	1818	finally
    //   1283	1291	1818	finally
    //   1291	1296	1818	finally
    //   1302	1310	1818	finally
    //   1316	1324	1818	finally
    //   1328	1336	1818	finally
    //   1473	1478	1818	finally
    //   1480	1487	1818	finally
    //   1489	1496	1818	finally
    //   1503	1510	1818	finally
    //   1512	1517	1818	finally
    //   1519	1526	1818	finally
    //   1528	1533	1818	finally
    //   1535	1540	1818	finally
    //   1542	1549	1818	finally
    //   1551	1556	1818	finally
    //   1558	1565	1818	finally
    //   1571	1579	1818	finally
    //   1579	1586	1818	finally
    //   1588	1593	1818	finally
    //   1595	1602	1818	finally
    //   1604	1611	1818	finally
    //   1613	1620	1818	finally
    //   1627	1634	1818	finally
    //   1636	1641	1818	finally
    //   1645	1650	1818	finally
    //   1655	1659	1818	finally
    //   1661	1664	1818	finally
    //   1673	1678	1818	finally
    //   1684	1690	1818	finally
    //   1697	1703	1818	finally
    //   1703	1708	1818	finally
    //   1719	1722	1818	finally
    //   1722	1725	1818	finally
    //   1727	1732	1818	finally
    //   1760	1765	1818	finally
    //   1839	1842	1818	finally
    //   1851	1856	1818	finally
    //   1858	1864	1818	finally
    //   1871	1877	1818	finally
    //   1879	1885	1818	finally
    //   1892	1898	1818	finally
    //   1898	1903	1818	finally
    //   1907	1912	1818	finally
    //   1932	1935	1818	finally
    //   1944	1949	1818	finally
    //   1951	1957	1818	finally
    //   1964	1970	1818	finally
    //   1972	1978	1818	finally
    //   1985	1991	1818	finally
    //   1991	1996	1818	finally
    //   2000	2005	1818	finally
    //   22	28	1827	java/lang/Exception
    //   30	33	1827	java/lang/Exception
    //   44	49	1827	java/lang/Exception
    //   49	52	1827	java/lang/Exception
    //   54	57	1827	java/lang/Exception
    //   59	64	1827	java/lang/Exception
    //   66	71	1827	java/lang/Exception
    //   71	74	1827	java/lang/Exception
    //   80	85	1827	java/lang/Exception
    //   85	88	1827	java/lang/Exception
    //   92	94	1827	java/lang/Exception
    //   100	105	1827	java/lang/Exception
    //   105	108	1827	java/lang/Exception
    //   112	114	1827	java/lang/Exception
    //   120	125	1827	java/lang/Exception
    //   125	128	1827	java/lang/Exception
    //   132	134	1827	java/lang/Exception
    //   140	145	1827	java/lang/Exception
    //   145	148	1827	java/lang/Exception
    //   152	154	1827	java/lang/Exception
    //   158	163	1827	java/lang/Exception
    //   163	166	1827	java/lang/Exception
    //   171	176	1827	java/lang/Exception
    //   22	28	1920	java/lang/IllegalStateException
    //   30	33	1920	java/lang/IllegalStateException
    //   44	49	1920	java/lang/IllegalStateException
    //   49	52	1920	java/lang/IllegalStateException
    //   54	57	1920	java/lang/IllegalStateException
    //   59	64	1920	java/lang/IllegalStateException
    //   66	71	1920	java/lang/IllegalStateException
    //   71	74	1920	java/lang/IllegalStateException
    //   80	85	1920	java/lang/IllegalStateException
    //   85	88	1920	java/lang/IllegalStateException
    //   92	94	1920	java/lang/IllegalStateException
    //   100	105	1920	java/lang/IllegalStateException
    //   105	108	1920	java/lang/IllegalStateException
    //   112	114	1920	java/lang/IllegalStateException
    //   120	125	1920	java/lang/IllegalStateException
    //   125	128	1920	java/lang/IllegalStateException
    //   132	134	1920	java/lang/IllegalStateException
    //   140	145	1920	java/lang/IllegalStateException
    //   145	148	1920	java/lang/IllegalStateException
    //   152	154	1920	java/lang/IllegalStateException
    //   158	163	1920	java/lang/IllegalStateException
    //   163	166	1920	java/lang/IllegalStateException
    //   171	176	1920	java/lang/IllegalStateException
  }
  
  protected final boolean g()
  {
    return true;
  }
  
  protected final Cursor h()
  {
    return a(p);
  }
  
  protected final Cursor i()
  {
    return a(q);
  }
  
  protected final Cursor j()
  {
    long l1 = SystemClock.elapsedRealtime();
    int i1 = o.length;
    Object localObject1 = am.a("?", ",", i1);
    Object localObject2 = e.getContentResolver();
    Uri localUri = g;
    String[] arrayOfString1 = n;
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("data_type IN (");
    ((StringBuilder)localObject3).append((String)localObject1);
    ((StringBuilder)localObject3).append(")");
    String str1 = ((StringBuilder)localObject3).toString();
    String[] arrayOfString2 = o;
    localObject1 = ((ContentResolver)localObject2).query(localUri, arrayOfString1, str1, arrayOfString2, "_id ASC");
    localObject3 = new String[1];
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Full query on data table took ");
    long l2 = SystemClock.elapsedRealtime() - l1;
    ((StringBuilder)localObject2).append(l2);
    String str2 = ((StringBuilder)localObject2).toString();
    localObject3[0] = str2;
    return (Cursor)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */