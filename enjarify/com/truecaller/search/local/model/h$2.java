package com.truecaller.search.local.model;

import android.os.SystemClock;
import com.a.a.af;
import com.a.a.ai;
import com.truecaller.search.local.b.c;
import com.truecaller.search.local.model.a.o;
import com.truecaller.search.local.model.a.t;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

final class h$2
  implements Runnable
{
  h$2(h paramh) {}
  
  public final void run()
  {
    long l1 = SystemClock.elapsedRealtime();
    Object localObject1 = new java/util/ArrayList;
    int i = 10;
    ((ArrayList)localObject1).<init>(i);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject2 = ba).h.b().iterator();
    boolean bool1 = ((Iterator)localObject2).hasNext();
    if (bool1)
    {
      Object localObject3 = (j)nextb;
      Object localObject4 = ((j)localObject3).a().iterator();
      boolean bool2 = ((Iterator)localObject4).hasNext();
      if (bool2)
      {
        Iterator localIterator = ((t)((Iterator)localObject4).next()).a((List)localObject1).iterator();
        for (;;)
        {
          boolean bool3 = localIterator.hasNext();
          if (!bool3) {
            break;
          }
          String str1 = (String)localIterator.next();
          c.a(str1, localArrayList);
        }
      }
      localObject3 = ((j)localObject3).b().iterator();
      for (;;)
      {
        boolean bool4 = ((Iterator)localObject3).hasNext();
        if (!bool4) {
          break;
        }
        localObject4 = ((o)((Iterator)localObject3).next()).a();
        c.a((String)localObject4, localArrayList);
      }
    }
    localObject1 = new String[1];
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Cached all word phonetics in ");
    long l2 = SystemClock.elapsedRealtime() - l1;
    ((StringBuilder)localObject2).append(l2);
    ((StringBuilder)localObject2).append("ms");
    String str2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = str2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.h.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */