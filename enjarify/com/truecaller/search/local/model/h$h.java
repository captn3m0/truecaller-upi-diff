package com.truecaller.search.local.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

final class h$h
  extends h.a
{
  private final Set j;
  
  private h$h(h paramh, a... paramVarArgs)
  {
    super(paramh, (byte)0);
    paramh = new java/util/HashSet;
    paramVarArgs = Arrays.asList(paramVarArgs);
    paramh.<init>(paramVarArgs);
    j = paramh;
  }
  
  protected final Object a(a parama)
  {
    Set localSet = j;
    boolean bool = localSet.contains(parama);
    if (bool) {
      return parama.e();
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.h.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */