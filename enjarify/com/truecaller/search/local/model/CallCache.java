package com.truecaller.search.local.model;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import com.a.a.af;
import com.truecaller.multisim.h;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;

public final class CallCache
  extends b
{
  static final Comparator h;
  private static final CallCache.a m;
  private static final String[] n;
  protected volatile SortedSet i;
  protected volatile List j;
  protected volatile i k;
  protected volatile af l;
  private final h o;
  
  static
  {
    Object localObject = new com/truecaller/search/local/model/CallCache$a;
    ((CallCache.a)localObject).<init>();
    m = (CallCache.a)localObject;
    String[] tmp17_14 = new String[15];
    String[] tmp18_17 = tmp17_14;
    String[] tmp18_17 = tmp17_14;
    tmp18_17[0] = "_id";
    tmp18_17[1] = "call_log_id";
    String[] tmp27_18 = tmp18_17;
    String[] tmp27_18 = tmp18_17;
    tmp27_18[2] = "timestamp";
    tmp27_18[3] = "type";
    String[] tmp36_27 = tmp27_18;
    String[] tmp36_27 = tmp27_18;
    tmp36_27[4] = "duration";
    tmp36_27[5] = "raw_number";
    String[] tmp45_36 = tmp36_27;
    String[] tmp45_36 = tmp36_27;
    tmp45_36[6] = "normalized_number";
    tmp45_36[7] = "country_code";
    String[] tmp56_45 = tmp45_36;
    String[] tmp56_45 = tmp45_36;
    tmp56_45[8] = "action";
    tmp56_45[9] = "feature";
    String[] tmp67_56 = tmp56_45;
    String[] tmp67_56 = tmp56_45;
    tmp67_56[10] = "new";
    tmp67_56[11] = "is_read";
    String[] tmp78_67 = tmp67_56;
    String[] tmp78_67 = tmp67_56;
    tmp78_67[12] = "subscription_component_name";
    tmp78_67[13] = "subscription_id";
    tmp78_67[14] = "tc_flag";
    n = tmp78_67;
    localObject = new com/truecaller/search/local/model/CallCache$b;
    ((CallCache.b)localObject).<init>((byte)0);
    h = (Comparator)localObject;
  }
  
  private Cursor a(int paramInt)
  {
    String[] tmp4_1 = new String[4];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "1";
    tmp5_4[1] = "3";
    tmp5_4[2] = "2";
    String[] tmp18_5 = tmp5_4;
    tmp18_5[3] = "3";
    String[] arrayOfString1 = tmp18_5;
    Object localObject = g.buildUpon();
    String str = String.valueOf(paramInt);
    Uri localUri = ((Uri.Builder)localObject).appendQueryParameter("limit", str).build();
    localObject = e.getContentResolver();
    String[] arrayOfString2 = n;
    return ((ContentResolver)localObject).query(localUri, arrayOfString2, "type IN (?, ?, ?) AND tc_flag != ?", arrayOfString1, "timestamp DESC");
  }
  
  /* Error */
  private CallCache.a b(Cursor paramCursor)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_2
    //   2: aload_1
    //   3: astore_3
    //   4: aload_0
    //   5: getfield 107	com/truecaller/search/local/model/CallCache:e	Landroid/content/Context;
    //   8: invokestatic 127	com/truecaller/search/local/model/g:a	(Landroid/content/Context;)Lcom/truecaller/search/local/model/g;
    //   11: astore 4
    //   13: iconst_1
    //   14: istore 5
    //   16: iload 5
    //   18: anewarray 60	java/lang/String
    //   21: astore 6
    //   23: new 130	java/lang/StringBuilder
    //   26: astore 7
    //   28: ldc -124
    //   30: astore 8
    //   32: aload 7
    //   34: aload 8
    //   36: invokespecial 135	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   39: aload_1
    //   40: invokeinterface 141 1 0
    //   45: istore 9
    //   47: aload 7
    //   49: iload 9
    //   51: invokevirtual 145	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   54: pop
    //   55: ldc -109
    //   57: astore 8
    //   59: aload 7
    //   61: aload 8
    //   63: invokevirtual 150	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload 7
    //   69: invokevirtual 154	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   72: astore 7
    //   74: iconst_0
    //   75: istore 10
    //   77: aconst_null
    //   78: astore 11
    //   80: aload 6
    //   82: iconst_0
    //   83: aload 7
    //   85: aastore
    //   86: ldc 30
    //   88: astore 6
    //   90: aload_1
    //   91: aload 6
    //   93: invokeinterface 158 2 0
    //   98: istore 12
    //   100: ldc 32
    //   102: astore 6
    //   104: aload_1
    //   105: aload 6
    //   107: invokeinterface 158 2 0
    //   112: istore 13
    //   114: ldc 34
    //   116: astore 6
    //   118: aload_1
    //   119: aload 6
    //   121: invokeinterface 158 2 0
    //   126: istore 14
    //   128: ldc 36
    //   130: astore 6
    //   132: aload_1
    //   133: aload 6
    //   135: invokeinterface 158 2 0
    //   140: istore 15
    //   142: ldc 38
    //   144: astore 6
    //   146: aload_1
    //   147: aload 6
    //   149: invokeinterface 158 2 0
    //   154: istore 16
    //   156: ldc 40
    //   158: astore 6
    //   160: aload_1
    //   161: aload 6
    //   163: invokeinterface 158 2 0
    //   168: istore 17
    //   170: ldc 42
    //   172: astore 6
    //   174: aload_1
    //   175: aload 6
    //   177: invokeinterface 158 2 0
    //   182: istore 9
    //   184: ldc 56
    //   186: astore 6
    //   188: aload_1
    //   189: aload 6
    //   191: invokeinterface 158 2 0
    //   196: istore 18
    //   198: ldc 44
    //   200: astore 6
    //   202: aload_1
    //   203: aload 6
    //   205: invokeinterface 158 2 0
    //   210: istore 19
    //   212: ldc 46
    //   214: astore 6
    //   216: aload_1
    //   217: aload 6
    //   219: invokeinterface 158 2 0
    //   224: istore 20
    //   226: ldc 48
    //   228: astore 11
    //   230: aload_1
    //   231: aload 11
    //   233: invokeinterface 158 2 0
    //   238: istore 10
    //   240: ldc 50
    //   242: astore 21
    //   244: aload_1
    //   245: aload 21
    //   247: invokeinterface 158 2 0
    //   252: istore 22
    //   254: ldc 52
    //   256: astore 23
    //   258: aload_1
    //   259: aload 23
    //   261: invokeinterface 158 2 0
    //   266: istore 5
    //   268: iload 20
    //   270: istore 24
    //   272: ldc 54
    //   274: astore 6
    //   276: aload_1
    //   277: aload 6
    //   279: invokeinterface 158 2 0
    //   284: istore 20
    //   286: iload 20
    //   288: istore 25
    //   290: ldc 58
    //   292: astore 6
    //   294: aload_1
    //   295: aload 6
    //   297: invokeinterface 158 2 0
    //   302: istore 20
    //   304: iload 5
    //   306: istore 26
    //   308: new 160	java/util/TreeSet
    //   311: astore 23
    //   313: iload 20
    //   315: istore 27
    //   317: getstatic 69	com/truecaller/search/local/model/CallCache:h	Ljava/util/Comparator;
    //   320: astore 6
    //   322: aload 23
    //   324: aload 6
    //   326: invokespecial 163	java/util/TreeSet:<init>	(Ljava/util/Comparator;)V
    //   329: new 165	com/a/a/ae
    //   332: astore 6
    //   334: iload 19
    //   336: istore 28
    //   338: aload_1
    //   339: invokeinterface 141 1 0
    //   344: istore 19
    //   346: iload 9
    //   348: istore 29
    //   350: iload 18
    //   352: istore 30
    //   354: ldc2_w 168
    //   357: dstore 31
    //   359: aload 6
    //   361: iload 19
    //   363: dload 31
    //   365: invokespecial 172	com/a/a/ae:<init>	(ID)V
    //   368: aload_0
    //   369: getfield 174	com/truecaller/search/local/model/CallCache:o	Lcom/truecaller/multisim/h;
    //   372: astore 7
    //   374: aload 7
    //   376: invokeinterface 179 1 0
    //   381: pop
    //   382: iconst_0
    //   383: istore 19
    //   385: aconst_null
    //   386: astore 7
    //   388: iconst_0
    //   389: istore 9
    //   391: aconst_null
    //   392: astore 8
    //   394: aload_1
    //   395: invokeinterface 182 1 0
    //   400: istore 18
    //   402: iload 18
    //   404: ifeq +1292 -> 1696
    //   407: aload_3
    //   408: iload 15
    //   410: invokeinterface 186 2 0
    //   415: istore 18
    //   417: iconst_m1
    //   418: istore 33
    //   420: iload 18
    //   422: tableswitch	default:+26->448, 1:+44->466, 2:+38->460, 3:+32->454
    //   448: iconst_m1
    //   449: istore 18
    //   451: goto +18 -> 469
    //   454: iconst_3
    //   455: istore 18
    //   457: goto +12 -> 469
    //   460: iconst_2
    //   461: istore 18
    //   463: goto +6 -> 469
    //   466: iconst_1
    //   467: istore 18
    //   469: iload 18
    //   471: iload 33
    //   473: if_icmpeq +1051 -> 1524
    //   476: iload 22
    //   478: istore 33
    //   480: aload 23
    //   482: astore 34
    //   484: aload_3
    //   485: iload 12
    //   487: invokeinterface 192 2 0
    //   492: lstore 35
    //   494: iconst_m1
    //   495: i2l
    //   496: lstore 37
    //   498: lload 35
    //   500: lload 37
    //   502: lcmp
    //   503: istore 39
    //   505: iload 39
    //   507: ifeq +934 -> 1441
    //   510: aload_3
    //   511: iload 13
    //   513: invokeinterface 196 2 0
    //   518: istore 19
    //   520: iload 19
    //   522: ifeq +10 -> 532
    //   525: lload 37
    //   527: lstore 40
    //   529: goto +13 -> 542
    //   532: aload_3
    //   533: iload 13
    //   535: invokeinterface 192 2 0
    //   540: lstore 40
    //   542: lconst_0
    //   543: lstore 42
    //   545: lload 40
    //   547: lload 42
    //   549: lcmp
    //   550: istore 19
    //   552: iload 19
    //   554: ifne +7 -> 561
    //   557: lload 37
    //   559: lstore 40
    //   561: new 198	com/truecaller/search/local/model/CallCache$Call
    //   564: astore 7
    //   566: aload_3
    //   567: iload 14
    //   569: invokeinterface 192 2 0
    //   574: lstore 37
    //   576: aload_3
    //   577: iload 16
    //   579: invokeinterface 192 2 0
    //   584: lstore 42
    //   586: iload 33
    //   588: istore 44
    //   590: aload 6
    //   592: astore 45
    //   594: iload 25
    //   596: istore 46
    //   598: iload 27
    //   600: istore 33
    //   602: aload 7
    //   604: astore 6
    //   606: aload 7
    //   608: astore_2
    //   609: aload 4
    //   611: astore 7
    //   613: aload 4
    //   615: astore 47
    //   617: aload 8
    //   619: astore 48
    //   621: iload 29
    //   623: istore 49
    //   625: iload 30
    //   627: istore 50
    //   629: iload 18
    //   631: istore 29
    //   633: iload 16
    //   635: istore 30
    //   637: lload 35
    //   639: lstore 51
    //   641: iload 17
    //   643: istore 22
    //   645: iload 14
    //   647: istore 5
    //   649: iload 15
    //   651: istore 39
    //   653: iload 13
    //   655: istore 53
    //   657: iload 18
    //   659: istore 13
    //   661: iload 10
    //   663: istore 54
    //   665: iload 12
    //   667: istore 55
    //   669: iload 14
    //   671: istore 29
    //   673: iconst_0
    //   674: istore 5
    //   676: aconst_null
    //   677: astore 23
    //   679: aload 6
    //   681: aload 4
    //   683: lload 35
    //   685: lload 40
    //   687: lload 37
    //   689: iload 18
    //   691: lload 42
    //   693: invokespecial 201	com/truecaller/search/local/model/CallCache$Call:<init>	(Lcom/truecaller/search/local/model/g;JJJIJ)V
    //   696: aload_3
    //   697: iload 17
    //   699: invokeinterface 204 2 0
    //   704: astore 6
    //   706: aload_2
    //   707: aload 6
    //   709: putfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   712: aload_3
    //   713: iload 49
    //   715: invokeinterface 204 2 0
    //   720: astore 6
    //   722: aload_2
    //   723: aload 6
    //   725: putfield 209	com/truecaller/search/local/model/CallCache$Call:i	Ljava/lang/String;
    //   728: iload 28
    //   730: istore 20
    //   732: aload_3
    //   733: iload 28
    //   735: invokeinterface 204 2 0
    //   740: astore 7
    //   742: aload_2
    //   743: aload 7
    //   745: putfield 211	com/truecaller/search/local/model/CallCache$Call:j	Ljava/lang/String;
    //   748: iload 50
    //   750: istore 9
    //   752: aload_3
    //   753: iload 50
    //   755: invokeinterface 204 2 0
    //   760: astore 7
    //   762: ldc -43
    //   764: astore 56
    //   766: aload 7
    //   768: ifnonnull +7 -> 775
    //   771: aload 56
    //   773: astore 7
    //   775: aload_2
    //   776: aload 7
    //   778: putfield 216	com/truecaller/search/local/model/CallCache$Call:p	Ljava/lang/String;
    //   781: iload 27
    //   783: istore 18
    //   785: aload_3
    //   786: iload 27
    //   788: invokeinterface 186 2 0
    //   793: istore 19
    //   795: aload_2
    //   796: iload 19
    //   798: putfield 220	com/truecaller/search/local/model/CallCache$Call:q	I
    //   801: aload_2
    //   802: getfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   805: astore 7
    //   807: aload 7
    //   809: ifnonnull +16 -> 825
    //   812: ldc -34
    //   814: astore 7
    //   816: aload_2
    //   817: aload 7
    //   819: putfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   822: goto +116 -> 938
    //   825: aload_2
    //   826: getfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   829: astore 7
    //   831: aload 7
    //   833: invokevirtual 225	java/lang/String:length	()I
    //   836: istore 19
    //   838: iconst_2
    //   839: istore 16
    //   841: iload 19
    //   843: iload 16
    //   845: if_icmpne +93 -> 938
    //   848: aload_2
    //   849: getfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   852: astore 7
    //   854: aload 7
    //   856: iconst_0
    //   857: invokevirtual 229	java/lang/String:charAt	(I)C
    //   860: istore 19
    //   862: bipush 45
    //   864: istore 16
    //   866: iload 19
    //   868: iload 16
    //   870: if_icmpne +68 -> 938
    //   873: aload_2
    //   874: getfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   877: astore 7
    //   879: iconst_1
    //   880: istore 16
    //   882: aload 7
    //   884: iload 16
    //   886: invokevirtual 229	java/lang/String:charAt	(I)C
    //   889: istore 19
    //   891: bipush 48
    //   893: istore 17
    //   895: iload 19
    //   897: iload 17
    //   899: if_icmplt +39 -> 938
    //   902: aload_2
    //   903: getfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   906: astore 7
    //   908: aload 7
    //   910: iload 16
    //   912: invokevirtual 229	java/lang/String:charAt	(I)C
    //   915: istore 19
    //   917: bipush 57
    //   919: istore 16
    //   921: iload 19
    //   923: iload 16
    //   925: if_icmpgt +13 -> 938
    //   928: ldc -34
    //   930: astore 7
    //   932: aload_2
    //   933: aload 7
    //   935: putfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   938: aload_2
    //   939: getfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   942: astore 7
    //   944: aload 7
    //   946: invokestatic 237	com/truecaller/util/bj:a	(Ljava/lang/String;)Ljava/lang/String;
    //   949: astore 7
    //   951: aload_2
    //   952: aload 7
    //   954: putfield 239	com/truecaller/search/local/model/CallCache$Call:g	Ljava/lang/String;
    //   957: iload 24
    //   959: istore 16
    //   961: aload_3
    //   962: iload 24
    //   964: invokeinterface 186 2 0
    //   969: istore 19
    //   971: aload_2
    //   972: iload 19
    //   974: putfield 241	com/truecaller/search/local/model/CallCache$Call:k	I
    //   977: iload 54
    //   979: istore 17
    //   981: aload_3
    //   982: iload 54
    //   984: invokeinterface 186 2 0
    //   989: istore 19
    //   991: aload_2
    //   992: iload 19
    //   994: putfield 243	com/truecaller/search/local/model/CallCache$Call:l	I
    //   997: iload 44
    //   999: istore 14
    //   1001: aload_3
    //   1002: iload 44
    //   1004: invokeinterface 186 2 0
    //   1009: istore 19
    //   1011: aload_2
    //   1012: iload 19
    //   1014: putfield 245	com/truecaller/search/local/model/CallCache$Call:m	I
    //   1017: iload 26
    //   1019: istore 15
    //   1021: aload_3
    //   1022: iload 26
    //   1024: invokeinterface 186 2 0
    //   1029: istore 19
    //   1031: aload_2
    //   1032: iload 19
    //   1034: putfield 247	com/truecaller/search/local/model/CallCache$Call:n	I
    //   1037: iload 46
    //   1039: istore 13
    //   1041: aload_3
    //   1042: iload 46
    //   1044: invokeinterface 204 2 0
    //   1049: astore 7
    //   1051: aload_2
    //   1052: aload 7
    //   1054: putfield 249	com/truecaller/search/local/model/CallCache$Call:o	Ljava/lang/String;
    //   1057: aload 48
    //   1059: astore 11
    //   1061: aload 48
    //   1063: ifnull +196 -> 1259
    //   1066: aload 48
    //   1068: getfield 253	com/truecaller/search/local/model/CallCache$Call:d	Lorg/a/a/b;
    //   1071: astore 7
    //   1073: aload_2
    //   1074: getfield 253	com/truecaller/search/local/model/CallCache$Call:d	Lorg/a/a/b;
    //   1077: astore 57
    //   1079: aload 7
    //   1081: aload 57
    //   1083: invokevirtual 258	org/a/a/b:d	(Lorg/a/a/x;)Z
    //   1086: istore 19
    //   1088: iload 19
    //   1090: ifeq +158 -> 1248
    //   1093: aload 48
    //   1095: getfield 260	com/truecaller/search/local/model/CallCache$Call:e	I
    //   1098: istore 19
    //   1100: aload_2
    //   1101: getfield 260	com/truecaller/search/local/model/CallCache$Call:e	I
    //   1104: istore 12
    //   1106: iload 19
    //   1108: iload 12
    //   1110: if_icmpne +138 -> 1248
    //   1113: iload 20
    //   1115: istore 28
    //   1117: aload 48
    //   1119: getfield 264	com/truecaller/search/local/model/CallCache$Call:f	J
    //   1122: lstore 58
    //   1124: iload 9
    //   1126: istore 50
    //   1128: aload_2
    //   1129: getfield 264	com/truecaller/search/local/model/CallCache$Call:f	J
    //   1132: lstore 60
    //   1134: lload 58
    //   1136: lload 60
    //   1138: lcmp
    //   1139: istore 12
    //   1141: iload 12
    //   1143: ifne +124 -> 1267
    //   1146: aload 48
    //   1148: getfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   1151: astore_3
    //   1152: aload_2
    //   1153: getfield 207	com/truecaller/search/local/model/CallCache$Call:h	Ljava/lang/String;
    //   1156: astore 6
    //   1158: aload_3
    //   1159: aload 6
    //   1161: invokestatic 270	android/text/TextUtils:equals	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   1164: istore 62
    //   1166: iload 62
    //   1168: ifne +6 -> 1174
    //   1171: goto +96 -> 1267
    //   1174: iload 18
    //   1176: istore 27
    //   1178: aload 48
    //   1180: astore 8
    //   1182: iload 55
    //   1184: istore 12
    //   1186: iload 30
    //   1188: istore 16
    //   1190: aload 34
    //   1192: astore 23
    //   1194: iload 39
    //   1196: istore 15
    //   1198: aload 45
    //   1200: astore 6
    //   1202: iload 9
    //   1204: istore 30
    //   1206: aload_0
    //   1207: astore_2
    //   1208: aload_1
    //   1209: astore_3
    //   1210: iconst_1
    //   1211: istore 19
    //   1213: iload 54
    //   1215: istore 10
    //   1217: iload 22
    //   1219: istore 17
    //   1221: iload 44
    //   1223: istore 22
    //   1225: iload 29
    //   1227: istore 14
    //   1229: iload 49
    //   1231: istore 29
    //   1233: aload 47
    //   1235: astore 4
    //   1237: iload 46
    //   1239: istore 25
    //   1241: iload 53
    //   1243: istore 13
    //   1245: goto -851 -> 394
    //   1248: iload 20
    //   1250: istore 28
    //   1252: iload 9
    //   1254: istore 50
    //   1256: goto +11 -> 1267
    //   1259: iload 20
    //   1261: istore 28
    //   1263: iload 9
    //   1265: istore 50
    //   1267: aload_2
    //   1268: getfield 241	com/truecaller/search/local/model/CallCache$Call:k	I
    //   1271: istore 62
    //   1273: iconst_1
    //   1274: istore 20
    //   1276: iload 62
    //   1278: iload 20
    //   1280: if_icmpeq +30 -> 1310
    //   1283: aload_2
    //   1284: getfield 241	com/truecaller/search/local/model/CallCache$Call:k	I
    //   1287: istore 62
    //   1289: iconst_3
    //   1290: istore 9
    //   1292: iload 62
    //   1294: iload 9
    //   1296: if_icmpne +6 -> 1302
    //   1299: goto +11 -> 1310
    //   1302: iconst_0
    //   1303: istore 62
    //   1305: aconst_null
    //   1306: astore_3
    //   1307: goto +6 -> 1313
    //   1310: iconst_1
    //   1311: istore 62
    //   1313: iload 62
    //   1315: ifne +38 -> 1353
    //   1318: aload 45
    //   1320: astore_3
    //   1321: lload 51
    //   1323: lstore 60
    //   1325: aload 45
    //   1327: lload 51
    //   1329: aload_2
    //   1330: invokeinterface 275 4 0
    //   1335: pop
    //   1336: aload 34
    //   1338: astore 57
    //   1340: aload 34
    //   1342: aload_2
    //   1343: invokevirtual 279	java/util/TreeSet:add	(Ljava/lang/Object;)Z
    //   1346: pop
    //   1347: aload_2
    //   1348: astore 8
    //   1350: goto +14 -> 1364
    //   1353: aload 34
    //   1355: astore 57
    //   1357: aload 45
    //   1359: astore_3
    //   1360: aload 11
    //   1362: astore 8
    //   1364: aload_3
    //   1365: astore 6
    //   1367: iload 18
    //   1369: istore 27
    //   1371: iload 16
    //   1373: istore 24
    //   1375: iload 17
    //   1377: istore 10
    //   1379: iload 15
    //   1381: istore 26
    //   1383: aload 57
    //   1385: astore 23
    //   1387: iload 22
    //   1389: istore 17
    //   1391: iload 55
    //   1393: istore 12
    //   1395: iload 30
    //   1397: istore 16
    //   1399: iload 39
    //   1401: istore 15
    //   1403: iload 50
    //   1405: istore 30
    //   1407: aload_0
    //   1408: astore_2
    //   1409: aload_1
    //   1410: astore_3
    //   1411: iconst_1
    //   1412: istore 19
    //   1414: iload 14
    //   1416: istore 22
    //   1418: iload 29
    //   1420: istore 14
    //   1422: iload 49
    //   1424: istore 29
    //   1426: aload 47
    //   1428: astore 4
    //   1430: iload 13
    //   1432: istore 25
    //   1434: iload 53
    //   1436: istore 13
    //   1438: goto -1044 -> 394
    //   1441: aload 6
    //   1443: astore_3
    //   1444: iload 17
    //   1446: istore 22
    //   1448: iload 15
    //   1450: istore 39
    //   1452: iload 13
    //   1454: istore 53
    //   1456: iload 10
    //   1458: istore 17
    //   1460: iload 12
    //   1462: istore 55
    //   1464: iload 25
    //   1466: istore 13
    //   1468: iload 26
    //   1470: istore 15
    //   1472: iload 27
    //   1474: istore 18
    //   1476: iload 30
    //   1478: istore 50
    //   1480: aload 23
    //   1482: astore 57
    //   1484: iconst_1
    //   1485: istore 20
    //   1487: iconst_0
    //   1488: istore 5
    //   1490: aconst_null
    //   1491: astore 23
    //   1493: aload 4
    //   1495: astore 47
    //   1497: aload 8
    //   1499: astore 11
    //   1501: iload 16
    //   1503: istore 30
    //   1505: iload 24
    //   1507: istore 16
    //   1509: iload 29
    //   1511: istore 49
    //   1513: iload 14
    //   1515: istore 29
    //   1517: iload 33
    //   1519: istore 14
    //   1521: goto +83 -> 1604
    //   1524: aload 6
    //   1526: astore_3
    //   1527: iload 15
    //   1529: istore 39
    //   1531: iload 13
    //   1533: istore 53
    //   1535: iload 12
    //   1537: istore 55
    //   1539: aload 23
    //   1541: astore 57
    //   1543: iload 25
    //   1545: istore 13
    //   1547: iload 26
    //   1549: istore 15
    //   1551: iload 27
    //   1553: istore 18
    //   1555: iload 30
    //   1557: istore 50
    //   1559: iconst_1
    //   1560: istore 20
    //   1562: iconst_0
    //   1563: istore 5
    //   1565: aconst_null
    //   1566: astore 23
    //   1568: aload 4
    //   1570: astore 47
    //   1572: iload 16
    //   1574: istore 30
    //   1576: iload 24
    //   1578: istore 16
    //   1580: iload 29
    //   1582: istore 49
    //   1584: iload 14
    //   1586: istore 29
    //   1588: iload 22
    //   1590: istore 14
    //   1592: iload 17
    //   1594: istore 22
    //   1596: iload 10
    //   1598: istore 17
    //   1600: aload 8
    //   1602: astore 11
    //   1604: aload_3
    //   1605: astore 6
    //   1607: iload 18
    //   1609: istore 27
    //   1611: iload 16
    //   1613: istore 24
    //   1615: iload 15
    //   1617: istore 26
    //   1619: aload 11
    //   1621: astore 8
    //   1623: aload 57
    //   1625: astore 23
    //   1627: iload 55
    //   1629: istore 12
    //   1631: iload 30
    //   1633: istore 16
    //   1635: iload 39
    //   1637: istore 15
    //   1639: iload 50
    //   1641: istore 30
    //   1643: aload_0
    //   1644: astore_2
    //   1645: aload_1
    //   1646: astore_3
    //   1647: iload 17
    //   1649: istore 10
    //   1651: iload 22
    //   1653: istore 17
    //   1655: iload 14
    //   1657: istore 22
    //   1659: iload 29
    //   1661: istore 14
    //   1663: iload 49
    //   1665: istore 29
    //   1667: aload 47
    //   1669: astore 4
    //   1671: iload 13
    //   1673: istore 25
    //   1675: iload 53
    //   1677: istore 13
    //   1679: goto -1285 -> 394
    //   1682: astore 4
    //   1684: aload_0
    //   1685: astore_2
    //   1686: goto +320 -> 2006
    //   1689: astore 4
    //   1691: aload_0
    //   1692: astore_2
    //   1693: goto +295 -> 1988
    //   1696: aload 6
    //   1698: astore_3
    //   1699: aload 23
    //   1701: astore 57
    //   1703: iconst_1
    //   1704: istore 20
    //   1706: iconst_3
    //   1707: istore 9
    //   1709: iconst_0
    //   1710: istore 5
    //   1712: aconst_null
    //   1713: astore 23
    //   1715: iload 19
    //   1717: ifne +42 -> 1759
    //   1720: aload 6
    //   1722: invokeinterface 281 1 0
    //   1727: istore 49
    //   1729: aload_0
    //   1730: astore_2
    //   1731: iconst_3
    //   1732: istore 19
    //   1734: aload_0
    //   1735: getfield 283	com/truecaller/search/local/model/CallCache:l	Lcom/a/a/af;
    //   1738: astore 8
    //   1740: aload 8
    //   1742: invokeinterface 281 1 0
    //   1747: istore 9
    //   1749: iload 49
    //   1751: iload 9
    //   1753: if_icmpeq +14 -> 1767
    //   1756: goto +8 -> 1764
    //   1759: aload_0
    //   1760: astore_2
    //   1761: iconst_3
    //   1762: istore 19
    //   1764: iconst_1
    //   1765: istore 5
    //   1767: iload 5
    //   1769: ifeq +163 -> 1932
    //   1772: new 285	com/truecaller/search/local/model/i
    //   1775: astore 4
    //   1777: aload_1
    //   1778: invokeinterface 141 1 0
    //   1783: istore 20
    //   1785: iload 20
    //   1787: iload 19
    //   1789: idiv
    //   1790: istore 20
    //   1792: aload 4
    //   1794: iload 20
    //   1796: invokespecial 288	com/truecaller/search/local/model/i:<init>	(I)V
    //   1799: new 290	java/util/ArrayList
    //   1802: astore 6
    //   1804: aload 6
    //   1806: invokespecial 291	java/util/ArrayList:<init>	()V
    //   1809: aload 57
    //   1811: invokevirtual 295	java/util/TreeSet:iterator	()Ljava/util/Iterator;
    //   1814: astore 7
    //   1816: aload 7
    //   1818: invokeinterface 300 1 0
    //   1823: istore 9
    //   1825: iload 9
    //   1827: ifeq +58 -> 1885
    //   1830: aload 7
    //   1832: invokeinterface 304 1 0
    //   1837: astore 8
    //   1839: aload 8
    //   1841: checkcast 198	com/truecaller/search/local/model/CallCache$Call
    //   1844: astore 8
    //   1846: aload 8
    //   1848: getfield 239	com/truecaller/search/local/model/CallCache$Call:g	Ljava/lang/String;
    //   1851: astore 56
    //   1853: aload 4
    //   1855: aload 56
    //   1857: invokevirtual 307	com/truecaller/search/local/model/i:a	(Ljava/lang/String;)Lcom/truecaller/search/local/model/i$a;
    //   1860: astore 56
    //   1862: aload 56
    //   1864: ifnonnull +11 -> 1875
    //   1867: aload 6
    //   1869: aload 8
    //   1871: invokevirtual 308	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   1874: pop
    //   1875: aload 4
    //   1877: aload 8
    //   1879: invokevirtual 311	com/truecaller/search/local/model/i:a	(Lcom/truecaller/search/local/model/i$a;)V
    //   1882: goto -66 -> 1816
    //   1885: new 22	com/truecaller/search/local/model/CallCache$a
    //   1888: astore 7
    //   1890: aload 57
    //   1892: invokestatic 317	java/util/Collections:unmodifiableSortedSet	(Ljava/util/SortedSet;)Ljava/util/SortedSet;
    //   1895: astore 8
    //   1897: aload 6
    //   1899: invokestatic 321	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
    //   1902: astore 6
    //   1904: aload 4
    //   1906: invokevirtual 324	com/truecaller/search/local/model/i:a	()Lcom/truecaller/search/local/model/i;
    //   1909: astore 4
    //   1911: aload 7
    //   1913: aload 8
    //   1915: aload 6
    //   1917: aload 4
    //   1919: aload_3
    //   1920: invokespecial 327	com/truecaller/search/local/model/CallCache$a:<init>	(Ljava/util/SortedSet;Ljava/util/List;Lcom/truecaller/search/local/model/i;Lcom/a/a/af;)V
    //   1923: aload_1
    //   1924: invokeinterface 330 1 0
    //   1929: aload 7
    //   1931: areturn
    //   1932: new 22	com/truecaller/search/local/model/CallCache$a
    //   1935: astore 4
    //   1937: aload_2
    //   1938: getfield 332	com/truecaller/search/local/model/CallCache:i	Ljava/util/SortedSet;
    //   1941: astore_3
    //   1942: aload_2
    //   1943: getfield 334	com/truecaller/search/local/model/CallCache:j	Ljava/util/List;
    //   1946: astore 6
    //   1948: aload_2
    //   1949: getfield 336	com/truecaller/search/local/model/CallCache:k	Lcom/truecaller/search/local/model/i;
    //   1952: astore 7
    //   1954: aload_2
    //   1955: getfield 283	com/truecaller/search/local/model/CallCache:l	Lcom/a/a/af;
    //   1958: astore 8
    //   1960: aload 4
    //   1962: aload_3
    //   1963: aload 6
    //   1965: aload 7
    //   1967: aload 8
    //   1969: invokespecial 327	com/truecaller/search/local/model/CallCache$a:<init>	(Ljava/util/SortedSet;Ljava/util/List;Lcom/truecaller/search/local/model/i;Lcom/a/a/af;)V
    //   1972: aload_1
    //   1973: invokeinterface 330 1 0
    //   1978: aload 4
    //   1980: areturn
    //   1981: astore 4
    //   1983: goto +23 -> 2006
    //   1986: astore 4
    //   1988: ldc_w 338
    //   1991: astore_3
    //   1992: aload 4
    //   1994: aload_3
    //   1995: invokestatic 343	com/truecaller/log/d:a	(Ljava/lang/Throwable;Ljava/lang/String;)V
    //   1998: aload_1
    //   1999: invokeinterface 330 1 0
    //   2004: aconst_null
    //   2005: areturn
    //   2006: aload_1
    //   2007: invokeinterface 330 1 0
    //   2012: aload 4
    //   2014: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	2015	0	this	CallCache
    //   0	2015	1	paramCursor	Cursor
    //   1	1954	2	localObject1	Object
    //   3	1992	3	localObject2	Object
    //   11	1659	4	localObject3	Object
    //   1682	1	4	localObject4	Object
    //   1689	1	4	localException1	Exception
    //   1775	204	4	localObject5	Object
    //   1981	1	4	localObject6	Object
    //   1986	27	4	localException2	Exception
    //   14	1754	5	i1	int
    //   21	1943	6	localObject7	Object
    //   26	1940	7	localObject8	Object
    //   30	1938	8	localObject9	Object
    //   45	1709	9	i2	int
    //   1823	3	9	bool1	boolean
    //   75	1575	10	i3	int
    //   78	1542	11	localObject10	Object
    //   98	1013	12	i4	int
    //   1139	491	12	i5	int
    //   112	1566	13	i6	int
    //   126	1536	14	i7	int
    //   140	1498	15	i8	int
    //   154	1480	16	i9	int
    //   168	1486	17	i10	int
    //   196	155	18	i11	int
    //   400	3	18	bool2	boolean
    //   415	1193	18	i12	int
    //   210	174	19	i13	int
    //   518	35	19	bool3	boolean
    //   793	240	19	i14	int
    //   1086	3	19	bool4	boolean
    //   1098	692	19	i15	int
    //   224	1571	20	i16	int
    //   242	4	21	str	String
    //   252	1406	22	i17	int
    //   256	1458	23	localObject11	Object
    //   270	1344	24	i18	int
    //   288	1386	25	i19	int
    //   306	1312	26	i20	int
    //   315	1295	27	i21	int
    //   336	926	28	i22	int
    //   348	1318	29	i23	int
    //   352	1290	30	i24	int
    //   357	7	31	d	double
    //   418	1100	33	i25	int
    //   482	872	34	localObject12	Object
    //   492	192	35	l1	long
    //   496	192	37	l2	long
    //   503	1133	39	i26	int
    //   527	159	40	l3	long
    //   543	149	42	l4	long
    //   588	634	44	i27	int
    //   592	766	45	localObject13	Object
    //   596	642	46	i28	int
    //   615	1053	47	localObject14	Object
    //   619	560	48	localObject15	Object
    //   623	1131	49	i29	int
    //   627	1013	50	i30	int
    //   639	689	51	l5	long
    //   655	1021	53	i31	int
    //   663	551	54	i32	int
    //   667	961	55	i33	int
    //   764	1099	56	localObject16	Object
    //   1077	814	57	localObject17	Object
    //   1122	13	58	l6	long
    //   1132	192	60	l7	long
    //   1164	3	62	bool5	boolean
    //   1271	43	62	i34	int
    // Exception table:
    //   from	to	target	type
    //   408	415	1682	finally
    //   485	492	1682	finally
    //   511	518	1682	finally
    //   533	540	1682	finally
    //   561	564	1682	finally
    //   567	574	1682	finally
    //   577	584	1682	finally
    //   691	696	1682	finally
    //   697	704	1682	finally
    //   707	712	1682	finally
    //   713	720	1682	finally
    //   723	728	1682	finally
    //   733	740	1682	finally
    //   743	748	1682	finally
    //   753	760	1682	finally
    //   776	781	1682	finally
    //   786	793	1682	finally
    //   796	801	1682	finally
    //   801	805	1682	finally
    //   817	822	1682	finally
    //   825	829	1682	finally
    //   831	836	1682	finally
    //   848	852	1682	finally
    //   856	860	1682	finally
    //   873	877	1682	finally
    //   884	889	1682	finally
    //   902	906	1682	finally
    //   910	915	1682	finally
    //   933	938	1682	finally
    //   938	942	1682	finally
    //   944	949	1682	finally
    //   952	957	1682	finally
    //   962	969	1682	finally
    //   972	977	1682	finally
    //   982	989	1682	finally
    //   992	997	1682	finally
    //   1002	1009	1682	finally
    //   1012	1017	1682	finally
    //   1022	1029	1682	finally
    //   1032	1037	1682	finally
    //   1042	1049	1682	finally
    //   1052	1057	1682	finally
    //   1066	1071	1682	finally
    //   1073	1077	1682	finally
    //   1081	1086	1682	finally
    //   1093	1098	1682	finally
    //   1100	1104	1682	finally
    //   1117	1122	1682	finally
    //   1128	1132	1682	finally
    //   1146	1151	1682	finally
    //   1152	1156	1682	finally
    //   1159	1164	1682	finally
    //   1267	1271	1682	finally
    //   1283	1287	1682	finally
    //   1329	1336	1682	finally
    //   1342	1347	1682	finally
    //   1720	1727	1682	finally
    //   408	415	1689	java/lang/Exception
    //   485	492	1689	java/lang/Exception
    //   511	518	1689	java/lang/Exception
    //   533	540	1689	java/lang/Exception
    //   561	564	1689	java/lang/Exception
    //   567	574	1689	java/lang/Exception
    //   577	584	1689	java/lang/Exception
    //   691	696	1689	java/lang/Exception
    //   697	704	1689	java/lang/Exception
    //   707	712	1689	java/lang/Exception
    //   713	720	1689	java/lang/Exception
    //   723	728	1689	java/lang/Exception
    //   733	740	1689	java/lang/Exception
    //   743	748	1689	java/lang/Exception
    //   753	760	1689	java/lang/Exception
    //   776	781	1689	java/lang/Exception
    //   786	793	1689	java/lang/Exception
    //   796	801	1689	java/lang/Exception
    //   801	805	1689	java/lang/Exception
    //   817	822	1689	java/lang/Exception
    //   825	829	1689	java/lang/Exception
    //   831	836	1689	java/lang/Exception
    //   848	852	1689	java/lang/Exception
    //   856	860	1689	java/lang/Exception
    //   873	877	1689	java/lang/Exception
    //   884	889	1689	java/lang/Exception
    //   902	906	1689	java/lang/Exception
    //   910	915	1689	java/lang/Exception
    //   933	938	1689	java/lang/Exception
    //   938	942	1689	java/lang/Exception
    //   944	949	1689	java/lang/Exception
    //   952	957	1689	java/lang/Exception
    //   962	969	1689	java/lang/Exception
    //   972	977	1689	java/lang/Exception
    //   982	989	1689	java/lang/Exception
    //   992	997	1689	java/lang/Exception
    //   1002	1009	1689	java/lang/Exception
    //   1012	1017	1689	java/lang/Exception
    //   1022	1029	1689	java/lang/Exception
    //   1032	1037	1689	java/lang/Exception
    //   1042	1049	1689	java/lang/Exception
    //   1052	1057	1689	java/lang/Exception
    //   1066	1071	1689	java/lang/Exception
    //   1073	1077	1689	java/lang/Exception
    //   1081	1086	1689	java/lang/Exception
    //   1093	1098	1689	java/lang/Exception
    //   1100	1104	1689	java/lang/Exception
    //   1117	1122	1689	java/lang/Exception
    //   1128	1132	1689	java/lang/Exception
    //   1146	1151	1689	java/lang/Exception
    //   1152	1156	1689	java/lang/Exception
    //   1159	1164	1689	java/lang/Exception
    //   1267	1271	1689	java/lang/Exception
    //   1283	1287	1689	java/lang/Exception
    //   1329	1336	1689	java/lang/Exception
    //   1342	1347	1689	java/lang/Exception
    //   1720	1727	1689	java/lang/Exception
    //   16	21	1981	finally
    //   23	26	1981	finally
    //   34	39	1981	finally
    //   39	45	1981	finally
    //   49	55	1981	finally
    //   61	67	1981	finally
    //   67	72	1981	finally
    //   83	86	1981	finally
    //   91	98	1981	finally
    //   105	112	1981	finally
    //   119	126	1981	finally
    //   133	140	1981	finally
    //   147	154	1981	finally
    //   161	168	1981	finally
    //   175	182	1981	finally
    //   189	196	1981	finally
    //   203	210	1981	finally
    //   217	224	1981	finally
    //   231	238	1981	finally
    //   245	252	1981	finally
    //   259	266	1981	finally
    //   277	284	1981	finally
    //   295	302	1981	finally
    //   308	311	1981	finally
    //   317	320	1981	finally
    //   324	329	1981	finally
    //   329	332	1981	finally
    //   338	344	1981	finally
    //   363	368	1981	finally
    //   368	372	1981	finally
    //   374	382	1981	finally
    //   394	400	1981	finally
    //   1734	1738	1981	finally
    //   1740	1747	1981	finally
    //   1772	1775	1981	finally
    //   1777	1783	1981	finally
    //   1787	1790	1981	finally
    //   1794	1799	1981	finally
    //   1799	1802	1981	finally
    //   1804	1809	1981	finally
    //   1809	1814	1981	finally
    //   1816	1823	1981	finally
    //   1830	1837	1981	finally
    //   1839	1844	1981	finally
    //   1846	1851	1981	finally
    //   1855	1860	1981	finally
    //   1869	1875	1981	finally
    //   1877	1882	1981	finally
    //   1885	1888	1981	finally
    //   1890	1895	1981	finally
    //   1897	1902	1981	finally
    //   1904	1909	1981	finally
    //   1919	1923	1981	finally
    //   1932	1935	1981	finally
    //   1937	1941	1981	finally
    //   1942	1946	1981	finally
    //   1948	1952	1981	finally
    //   1954	1958	1981	finally
    //   1967	1972	1981	finally
    //   1994	1998	1981	finally
    //   16	21	1986	java/lang/Exception
    //   23	26	1986	java/lang/Exception
    //   34	39	1986	java/lang/Exception
    //   39	45	1986	java/lang/Exception
    //   49	55	1986	java/lang/Exception
    //   61	67	1986	java/lang/Exception
    //   67	72	1986	java/lang/Exception
    //   83	86	1986	java/lang/Exception
    //   91	98	1986	java/lang/Exception
    //   105	112	1986	java/lang/Exception
    //   119	126	1986	java/lang/Exception
    //   133	140	1986	java/lang/Exception
    //   147	154	1986	java/lang/Exception
    //   161	168	1986	java/lang/Exception
    //   175	182	1986	java/lang/Exception
    //   189	196	1986	java/lang/Exception
    //   203	210	1986	java/lang/Exception
    //   217	224	1986	java/lang/Exception
    //   231	238	1986	java/lang/Exception
    //   245	252	1986	java/lang/Exception
    //   259	266	1986	java/lang/Exception
    //   277	284	1986	java/lang/Exception
    //   295	302	1986	java/lang/Exception
    //   308	311	1986	java/lang/Exception
    //   317	320	1986	java/lang/Exception
    //   324	329	1986	java/lang/Exception
    //   329	332	1986	java/lang/Exception
    //   338	344	1986	java/lang/Exception
    //   363	368	1986	java/lang/Exception
    //   368	372	1986	java/lang/Exception
    //   374	382	1986	java/lang/Exception
    //   394	400	1986	java/lang/Exception
    //   1734	1738	1986	java/lang/Exception
    //   1740	1747	1986	java/lang/Exception
    //   1772	1775	1986	java/lang/Exception
    //   1777	1783	1986	java/lang/Exception
    //   1787	1790	1986	java/lang/Exception
    //   1794	1799	1986	java/lang/Exception
    //   1799	1802	1986	java/lang/Exception
    //   1804	1809	1986	java/lang/Exception
    //   1809	1814	1986	java/lang/Exception
    //   1816	1823	1986	java/lang/Exception
    //   1830	1837	1986	java/lang/Exception
    //   1839	1844	1986	java/lang/Exception
    //   1846	1851	1986	java/lang/Exception
    //   1855	1860	1986	java/lang/Exception
    //   1869	1875	1986	java/lang/Exception
    //   1877	1882	1986	java/lang/Exception
    //   1885	1888	1986	java/lang/Exception
    //   1890	1895	1986	java/lang/Exception
    //   1897	1902	1986	java/lang/Exception
    //   1904	1909	1986	java/lang/Exception
    //   1919	1923	1986	java/lang/Exception
    //   1932	1935	1986	java/lang/Exception
    //   1937	1941	1986	java/lang/Exception
    //   1942	1946	1986	java/lang/Exception
    //   1948	1952	1986	java/lang/Exception
    //   1954	1958	1986	java/lang/Exception
    //   1967	1972	1986	java/lang/Exception
  }
  
  protected final boolean g()
  {
    return true;
  }
  
  protected final Cursor h()
  {
    return a(20);
  }
  
  protected final Cursor j()
  {
    return a(500);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.CallCache
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */