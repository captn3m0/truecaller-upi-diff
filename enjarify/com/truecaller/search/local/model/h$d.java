package com.truecaller.search.local.model;

import android.text.TextUtils;
import com.truecaller.old.data.access.Settings;
import com.truecaller.search.local.b.f;
import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;
import java.util.Map;

final class h$d
  implements Comparator
{
  final String a;
  final Collator b;
  final Map c;
  
  private h$d()
  {
    Object localObject = Settings.k();
    a = ((String)localObject);
    localObject = a;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (bool)
    {
      bool = false;
      localObject = null;
    }
    else
    {
      localObject = new java/util/Locale;
      String str = a;
      ((Locale)localObject).<init>(str);
      localObject = Collator.getInstance((Locale)localObject);
    }
    b = ((Collator)localObject);
    localObject = aa).b;
    c = ((Map)localObject);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (d)paramObject;
        localObject1 = a;
        localObject2 = a;
        boolean bool2 = TextUtils.equals((CharSequence)localObject1, (CharSequence)localObject2);
        if (bool2)
        {
          localObject1 = c;
          paramObject = c;
          if (localObject1 == paramObject) {
            return bool1;
          }
        }
        return false;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.h.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */