package com.truecaller.search.local.model.a;

import android.text.TextUtils;
import com.truecaller.search.local.model.g;
import java.util.List;

public final class c
  extends b
  implements l
{
  public final int a;
  public final String h;
  
  public c(g paramg, String paramString, int paramInt)
  {
    super(paramg);
    h = paramString;
    a = paramInt;
  }
  
  public final String a()
  {
    return h;
  }
  
  public final List a(List paramList)
  {
    String str = h;
    paramList.add(str);
    return paramList;
  }
  
  public final int c()
  {
    return 2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = super.equals(paramObject);
    if (bool1)
    {
      bool1 = paramObject instanceof l;
      if (bool1)
      {
        String str = h;
        paramObject = ((l)paramObject).a();
        boolean bool2 = TextUtils.equals(str, (CharSequence)paramObject);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = super.hashCode() * 31;
    int j = h.hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */