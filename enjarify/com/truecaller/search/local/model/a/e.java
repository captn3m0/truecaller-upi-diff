package com.truecaller.search.local.model.a;

import android.text.TextUtils;
import com.truecaller.search.local.model.g;
import java.util.List;

public final class e
  extends b
  implements n
{
  public final String a;
  public final String h;
  public final String i;
  
  public e(g paramg, String paramString1, String paramString2)
  {
    super(paramg);
    a = paramString1;
    h = paramString2;
    paramg = d();
    paramString1 = a();
    boolean bool = TextUtils.isEmpty(paramg);
    int j;
    if (bool)
    {
      bool = TextUtils.isEmpty(paramString1);
      if (bool)
      {
        j = 0;
        paramg = null;
        break label123;
      }
    }
    bool = TextUtils.isEmpty(paramg);
    if (bool)
    {
      paramg = paramString1;
    }
    else
    {
      bool = TextUtils.isEmpty(paramString1);
      if (!bool)
      {
        paramString2 = "%s, %s";
        int k = 2;
        Object[] arrayOfObject = new Object[k];
        arrayOfObject[0] = paramg;
        j = 1;
        arrayOfObject[j] = paramString1;
        paramg = String.format(paramString2, arrayOfObject);
      }
    }
    label123:
    i = paramg;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final List a(List paramList)
  {
    String str = e();
    if (str != null) {
      paramList.add(str);
    }
    return paramList;
  }
  
  public final int c()
  {
    return 4;
  }
  
  public final String d()
  {
    return h;
  }
  
  public final String e()
  {
    return i;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = super.equals(paramObject);
    if (bool1)
    {
      bool1 = paramObject instanceof n;
      boolean bool2 = true;
      if (bool1)
      {
        String str1 = a();
        paramObject = (n)paramObject;
        String str2 = ((n)paramObject).a();
        bool1 = TextUtils.equals(str1, str2);
        if (bool1)
        {
          str1 = d();
          paramObject = ((n)paramObject).d();
          bool3 = TextUtils.equals(str1, (CharSequence)paramObject);
          if (bool3)
          {
            bool3 = true;
            break label93;
          }
        }
      }
      boolean bool3 = false;
      paramObject = null;
      label93:
      if (bool3) {
        return bool2;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int j = super.hashCode();
    String str1 = d();
    String str2 = a();
    int k = 0;
    if (str1 == null)
    {
      m = 0;
      str1 = null;
    }
    else
    {
      m = str1.hashCode();
    }
    int m = (m + 527) * 31;
    if (str2 != null) {
      k = str2.hashCode();
    }
    return (m + k) * 31 + j;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */