package com.truecaller.search.local.model.a;

import android.database.Cursor;
import android.text.TextUtils;
import com.truecaller.search.local.model.g;

public final class a$a
  implements k
{
  private final int a;
  private final int b;
  private final int c;
  private final Cursor d;
  private final g e;
  
  public a$a(Cursor paramCursor, g paramg)
  {
    d = paramCursor;
    e = paramg;
    int i = d.getColumnIndex("data1");
    a = i;
    i = d.getColumnIndex("data3");
    b = i;
    i = d.getColumnIndex("data2");
    c = i;
  }
  
  public final b a()
  {
    Object localObject1 = d;
    int i = a;
    boolean bool1 = ((Cursor)localObject1).isNull(i);
    i = 0;
    a locala = null;
    if (bool1) {
      return null;
    }
    localObject1 = d;
    int j = c;
    localObject1 = ((Cursor)localObject1).getString(j);
    Object localObject2 = "email";
    bool1 = ((String)localObject2).equals(localObject1);
    if (!bool1) {
      return null;
    }
    localObject1 = d;
    j = a;
    localObject1 = ((Cursor)localObject1).getString(j);
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
    if (bool2) {
      return null;
    }
    locala = new com/truecaller/search/local/model/a/a;
    localObject2 = e;
    locala.<init>((g)localObject2, (String)localObject1);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */