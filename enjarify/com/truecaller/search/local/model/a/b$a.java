package com.truecaller.search.local.model.a;

import android.database.Cursor;
import com.truecaller.search.local.model.g;
import java.util.HashMap;

public final class b$a
  implements k
{
  public final Cursor a;
  private final HashMap b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  
  public b$a(Cursor paramCursor, g paramg)
  {
    a = paramCursor;
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Integer localInteger = Integer.valueOf(4);
    Object localObject = new com/truecaller/search/local/model/a/f$a;
    ((f.a)localObject).<init>(paramCursor, paramg);
    localHashMap.put(localInteger, localObject);
    localInteger = Integer.valueOf(3);
    localObject = new com/truecaller/search/local/model/a/a$a;
    ((a.a)localObject).<init>(paramCursor, paramg);
    localHashMap.put(localInteger, localObject);
    localInteger = Integer.valueOf(1);
    localObject = new com/truecaller/search/local/model/a/h$a;
    ((h.a)localObject).<init>(paramCursor, paramg);
    localHashMap.put(localInteger, localObject);
    b = localHashMap;
    int i = paramCursor.getColumnIndex("data_id");
    c = i;
    i = paramCursor.getColumnIndex("data_type");
    d = i;
    i = paramCursor.getColumnIndex("_id");
    e = i;
    i = paramCursor.getColumnIndex("data_raw_contact_id");
    f = i;
    i = paramCursor.getColumnIndex("data_is_primary");
    g = i;
    int j = paramCursor.getColumnIndex("data_phonebook_id");
    h = j;
  }
  
  public final b a()
  {
    Cursor localCursor1 = a;
    int i = c;
    long l1 = localCursor1.getLong(i);
    Cursor localCursor2 = a;
    int j = e;
    long l2 = localCursor2.getLong(j);
    Object localObject = a;
    int k = d;
    int m = ((Cursor)localObject).getInt(k);
    HashMap localHashMap = b;
    localObject = Integer.valueOf(m);
    localObject = (k)localHashMap.get(localObject);
    k = 0;
    localHashMap = null;
    if (localObject == null) {
      return null;
    }
    localObject = ((k)localObject).a();
    if (localObject == null) {
      return null;
    }
    c = l1;
    localCursor1 = a;
    i = f;
    l1 = localCursor1.getLong(i);
    e = l1;
    localCursor1 = a;
    i = g;
    int n = localCursor1.getInt(i);
    i = 1;
    if (n != i) {
      i = 0;
    }
    g = i;
    localCursor1 = a;
    i = h;
    boolean bool = localCursor1.isNull(i);
    if (bool)
    {
      l1 = -1;
    }
    else
    {
      localCursor1 = a;
      i = h;
      l1 = localCursor1.getLong(i);
    }
    f = l1;
    d = l2;
    return (b)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */