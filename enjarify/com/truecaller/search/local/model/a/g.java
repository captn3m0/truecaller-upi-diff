package com.truecaller.search.local.model.a;

import android.content.res.Resources;
import android.text.TextUtils;
import java.util.List;

public final class g
  extends b
  implements p
{
  public static final String a = Resources.getSystem().getString(17039374);
  public String h;
  
  public g(com.truecaller.search.local.model.g paramg)
  {
    super(paramg);
  }
  
  public final String a()
  {
    return h;
  }
  
  public final List a(List paramList)
  {
    String str = a();
    paramList.add(str);
    return paramList;
  }
  
  public final int c()
  {
    return 1;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = super.equals(paramObject);
    if (bool1)
    {
      bool1 = paramObject instanceof p;
      boolean bool2 = true;
      if (bool1)
      {
        String str = a();
        paramObject = ((p)paramObject).a();
        bool3 = TextUtils.equals(str, (CharSequence)paramObject);
        if (bool3)
        {
          bool3 = true;
          break label63;
        }
      }
      boolean bool3 = false;
      paramObject = null;
      label63:
      if (bool3) {
        return bool2;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = super.hashCode();
    return (a().hashCode() + 527) * 31 + i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */