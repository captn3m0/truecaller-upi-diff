package com.truecaller.search.local.model.a;

import android.text.TextUtils;
import com.truecaller.search.local.model.g;
import java.util.List;

public final class h
  extends b
  implements q
{
  public final String a;
  public final String h;
  public final String i;
  public final String j;
  public final String k;
  public final String l;
  public final String m;
  
  public h(g paramg, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    super(paramg);
    a = paramString1;
    h = paramString2;
    i = paramString3;
    j = paramString4;
    k = paramString5;
    l = paramString6;
    m = paramString7;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final List a(List paramList)
  {
    String str = a();
    paramList.add(str);
    return paramList;
  }
  
  public final int c()
  {
    return 3;
  }
  
  public final String d()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = super.equals(paramObject);
    if (bool1)
    {
      bool1 = paramObject instanceof q;
      boolean bool2 = true;
      if (bool1)
      {
        String str = a();
        paramObject = ((q)paramObject).a();
        bool3 = TextUtils.equals(str, (CharSequence)paramObject);
        if (bool3)
        {
          bool3 = true;
          break label63;
        }
      }
      boolean bool3 = false;
      paramObject = null;
      label63:
      if (bool3) {
        return bool2;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int n = super.hashCode();
    return (a().hashCode() + 527) * 31 + n;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */