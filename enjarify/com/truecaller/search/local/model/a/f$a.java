package com.truecaller.search.local.model.a;

import android.database.Cursor;
import com.google.c.a.k.d;
import com.truecaller.common.h.ab;
import com.truecaller.log.AssertionUtil;
import com.truecaller.search.local.model.g;

public final class f$a
  implements k
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final Cursor h;
  private final g i;
  
  public f$a(Cursor paramCursor, g paramg)
  {
    h = paramCursor;
    i = paramg;
    int j = h.getColumnIndex("data1");
    a = j;
    j = h.getColumnIndex("data9");
    b = j;
    j = h.getColumnIndex("data4");
    c = j;
    j = h.getColumnIndex("data5");
    d = j;
    j = h.getColumnIndex("data8");
    e = j;
    j = h.getColumnIndex("data10");
    f = j;
    j = h.getColumnIndex("data3");
    g = j;
  }
  
  public final b a()
  {
    Object localObject1 = h;
    int j = b;
    boolean bool = ((Cursor)localObject1).isNull(j);
    Object localObject2;
    if (bool)
    {
      localObject1 = h;
      j = a;
      localObject1 = ((Cursor)localObject1).getString(j);
      localObject2 = localObject1;
    }
    else
    {
      localObject1 = h;
      j = b;
      localObject1 = ((Cursor)localObject1).getString(j);
      localObject2 = localObject1;
    }
    if (localObject2 == null)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("RAW_NUMBER shouldn't be null");
      return null;
    }
    localObject1 = new com/truecaller/search/local/model/a/f;
    g localg = i;
    Object localObject3 = h;
    int k = a;
    String str1 = ((Cursor)localObject3).getString(k);
    localObject3 = h;
    int m = e;
    localObject3 = ((Cursor)localObject3).getString(m);
    k.d locald = k.d.l;
    locald = ab.a((String)localObject3, locald);
    localObject3 = h;
    int n = c;
    n = ((Cursor)localObject3).getInt(n);
    localObject3 = h;
    int i1 = d;
    String str2 = ((Cursor)localObject3).getString(i1);
    localObject3 = h;
    int i2 = f;
    String str3 = ((Cursor)localObject3).getString(i2);
    localObject3 = h;
    int i3 = g;
    i3 = ((Cursor)localObject3).getInt(i3);
    localObject3 = localObject1;
    ((f)localObject1).<init>(localg, str1, (String)localObject2, locald, n, str2, str3, i3);
    return (b)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */