package com.truecaller.search.local.model.a;

import android.text.TextUtils;
import com.truecaller.search.local.model.g;
import java.util.List;

public final class a
  extends b
  implements j
{
  public final String a;
  
  public a(g paramg, String paramString)
  {
    super(paramg);
    a = paramString;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final List a(List paramList)
  {
    String str = a;
    paramList.add(str);
    return paramList;
  }
  
  public final int c()
  {
    return 5;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = super.equals(paramObject);
    if (bool1)
    {
      bool1 = paramObject instanceof j;
      boolean bool2 = true;
      if (bool1)
      {
        String str = a();
        paramObject = ((j)paramObject).a();
        bool3 = TextUtils.equals(str, (CharSequence)paramObject);
        if (bool3)
        {
          bool3 = true;
          break label63;
        }
      }
      boolean bool3 = false;
      paramObject = null;
      label63:
      if (bool3) {
        return bool2;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = super.hashCode() * 31;
    int j = a().hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */