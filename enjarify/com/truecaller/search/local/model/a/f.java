package com.truecaller.search.local.model.a;

import android.text.TextUtils;
import com.google.c.a.k.d;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.am;
import com.truecaller.search.ContactDto.Contact.PhoneNumber;
import com.truecaller.search.local.model.g;
import com.truecaller.util.bj;

public final class f
  extends b
  implements o
{
  public final String a;
  public final String h;
  public final String i;
  public final k.d j;
  public final int k;
  public final String l;
  public final String m;
  public final int n;
  
  public f(g paramg, String paramString1, String paramString2, k.d paramd, int paramInt1, String paramString3, String paramString4, int paramInt2)
  {
    super(paramg);
    h = paramString1;
    i = paramString2;
    paramg = bj.a(paramString2);
    a = paramg;
    j = paramd;
    k = paramInt1;
    l = paramString3;
    m = paramString4;
    n = paramInt2;
  }
  
  public static f a(f paramf1, f paramf2)
  {
    if (paramf2 == null) {
      return paramf1;
    }
    if (paramf1 == null) {
      return paramf2;
    }
    Object localObject = h;
    String str1 = h;
    boolean bool1 = am.a((CharSequence)localObject, str1);
    if (!bool1) {
      return paramf1;
    }
    int i1 = k;
    str1 = l;
    k.d locald1 = j;
    int i2 = k;
    int i3 = ContactDto.Contact.PhoneNumber.EMPTY_TEL_TYPE;
    int i4;
    String str2;
    k.d locald2;
    if (i2 != i3)
    {
      i1 = k;
      str1 = l;
      locald1 = j;
      i4 = i1;
      str2 = str1;
      locald2 = locald1;
    }
    else
    {
      i4 = i1;
      str2 = str1;
      locald2 = locald1;
    }
    localObject = new com/truecaller/search/local/model/a/f;
    g localg = b;
    String str3 = h;
    String str4 = i;
    str1 = m;
    boolean bool2 = TextUtils.isEmpty(str1);
    if (bool2) {
      str1 = m;
    } else {
      str1 = m;
    }
    String str5 = str1;
    int i5 = n;
    if (i5 <= 0) {
      i5 = n;
    }
    ((f)localObject).<init>(localg, str3, str4, locald2, i4, str2, str5, i5);
    long l1 = f;
    long l2 = -1;
    boolean bool3 = l1 < l2;
    if (!bool3) {
      paramf1 = paramf2;
    }
    l1 = c;
    c = l1;
    l1 = f;
    f = l1;
    boolean bool4 = g;
    g = bool4;
    l1 = d;
    d = l1;
    long l3 = e;
    e = l3;
    return (f)localObject;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final int c()
  {
    return 7;
  }
  
  public final String d()
  {
    return h;
  }
  
  public final String e()
  {
    return i;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof f;
    if (bool1)
    {
      long l1 = d;
      Object localObject = paramObject;
      localObject = (f)paramObject;
      long l2 = d;
      bool1 = l1 < l2;
      if (!bool1)
      {
        bool1 = paramObject instanceof o;
        boolean bool2;
        if (!bool1)
        {
          bool2 = false;
          paramObject = null;
        }
        else
        {
          paramObject = (o)paramObject;
          localObject = a();
          paramObject = ((o)paramObject).a();
          bool2 = bj.a((String)localObject, (String)paramObject);
        }
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final k.d f()
  {
    return j;
  }
  
  public final int g()
  {
    return k;
  }
  
  public final int h()
  {
    return n;
  }
  
  public final int hashCode()
  {
    long l1 = d;
    long l2 = l1 >>> 32;
    int i1 = ((int)(l1 ^ l2) + 527) * 31;
    int i2 = bj.e(a());
    return (i1 + i2) * 31 + 0;
  }
  
  public final String i()
  {
    return l;
  }
  
  public final String j()
  {
    return aa.e(h);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */