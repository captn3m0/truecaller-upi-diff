package com.truecaller.search.local.model.a;

import com.truecaller.search.local.model.g;
import com.truecaller.search.local.model.j;

public abstract class b
  implements i, Comparable
{
  protected final g b;
  public long c;
  public long d;
  public long e;
  public long f = -1;
  public boolean g;
  
  public b(g paramg)
  {
    b = paramg;
  }
  
  private static int a(long paramLong1, long paramLong2)
  {
    boolean bool = paramLong1 < paramLong2;
    if (bool) {
      return -1;
    }
    bool = paramLong1 < paramLong2;
    if (bool) {
      return 1;
    }
    return 0;
  }
  
  public final j b()
  {
    g localg = b;
    long l = d;
    return localg.a(l);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof b;
      if (bool1)
      {
        long l1 = c;
        paramObject = (b)paramObject;
        long l2 = c;
        boolean bool2 = l1 < l2;
        if (!bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public int hashCode()
  {
    long l1 = c;
    long l2 = l1 >>> 32;
    return (int)(l1 ^ l2) + 527;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */