package com.truecaller.search.local.model.a;

import android.text.TextUtils;
import com.truecaller.search.local.model.g;
import java.util.List;

public final class d
  extends b
  implements m
{
  public final String a;
  
  public d(g paramg, String paramString)
  {
    super(paramg);
    a = paramString;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final List a(List paramList)
  {
    String str = a;
    paramList.add(str);
    return paramList;
  }
  
  public final int c()
  {
    return 6;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = super.equals(paramObject);
    if (bool1)
    {
      bool1 = paramObject instanceof m;
      if (bool1)
      {
        String str = a;
        paramObject = ((m)paramObject).a();
        boolean bool2 = TextUtils.equals(str, (CharSequence)paramObject);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = super.hashCode() * 31;
    int j = a.hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */