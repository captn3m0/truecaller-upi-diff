package com.truecaller.search.local.model.a;

import android.database.Cursor;
import android.text.TextUtils;
import com.truecaller.common.h.am;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.search.local.model.g;

public final class h$a
  implements k
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final Cursor g;
  private final g h;
  
  public h$a(Cursor paramCursor, g paramg)
  {
    g = paramCursor;
    h = paramg;
    int i = g.getColumnIndex("data1");
    a = i;
    i = g.getColumnIndex("data2");
    b = i;
    i = g.getColumnIndex("data3");
    c = i;
    i = g.getColumnIndex("data4");
    d = i;
    i = g.getColumnIndex("data7");
    e = i;
    i = g.getColumnIndex("data8");
    f = i;
  }
  
  public final b a()
  {
    Object localObject1 = g;
    int i = b;
    String str1 = ((Cursor)localObject1).getString(i);
    localObject1 = g;
    i = c;
    String str2 = ((Cursor)localObject1).getString(i);
    localObject1 = g;
    i = f;
    String str3 = ((Cursor)localObject1).getString(i);
    localObject1 = g;
    i = a;
    String str4 = ((Cursor)localObject1).getString(i);
    localObject1 = g;
    i = d;
    String str5 = ((Cursor)localObject1).getString(i);
    localObject1 = com.truecaller.common.h.h.a(am.m(str5));
    if (localObject1 == null) {
      localObject1 = "";
    } else {
      localObject1 = am.a(b);
    }
    boolean bool1 = TextUtils.isEmpty(str1);
    int k = 2;
    int m = 1;
    boolean bool2 = false;
    Object localObject2 = null;
    if (bool1)
    {
      bool1 = TextUtils.isEmpty(str2);
      if (bool1)
      {
        bool1 = TextUtils.isEmpty(str3);
        if (bool1)
        {
          localObject3 = new String[k];
          localObject3[0] = str4;
          localObject3[m] = localObject1;
          localObject1 = am.a((String[])localObject3);
          localObject2 = localObject1;
          break label287;
        }
      }
    }
    int j = 3;
    Object localObject3 = new String[j];
    localObject3[0] = str4;
    String str6 = " ";
    CharSequence[] arrayOfCharSequence = new CharSequence[k];
    arrayOfCharSequence[0] = str1;
    bool2 = TextUtils.isEmpty(str2);
    if (bool2) {
      localObject2 = str3;
    } else {
      localObject2 = str2;
    }
    arrayOfCharSequence[m] = localObject2;
    localObject2 = am.a(str6, arrayOfCharSequence);
    localObject3[m] = localObject2;
    localObject3[k] = localObject1;
    localObject1 = am.a((String[])localObject3);
    localObject2 = localObject1;
    label287:
    boolean bool3 = TextUtils.isEmpty((CharSequence)localObject2);
    if (bool3) {
      return null;
    }
    localObject1 = new com/truecaller/search/local/model/a/h;
    g localg = h;
    localObject3 = g;
    k = e;
    str6 = ((Cursor)localObject3).getString(k);
    ((h)localObject1).<init>(localg, (String)localObject2, str4, str1, str2, str5, str3, str6);
    return (b)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.a.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */