package com.truecaller.search.local.model;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

final class b$1
  extends ContentObserver
{
  b$1(b paramb, Handler paramHandler, Uri paramUri)
  {
    super(paramHandler);
  }
  
  public final boolean deliverSelfNotifications()
  {
    return true;
  }
  
  public final void onChange(boolean paramBoolean)
  {
    paramBoolean = true;
    Object localObject1 = new String[paramBoolean];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("[");
    Object localObject3 = b.getClass().getSimpleName();
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append("] Uri ");
    localObject3 = a;
    ((StringBuilder)localObject2).append(localObject3);
    ((StringBuilder)localObject2).append(" changed");
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject3 = null;
    localObject1[0] = localObject2;
    localObject1 = b;
    boolean bool = b;
    if (!bool) {
      return;
    }
    localObject1 = b;
    d = paramBoolean;
    bool = c;
    if (bool)
    {
      localObject1 = b;
      bool = b.a((b)localObject1);
      int i = 8193;
      if (!bool)
      {
        localObject1 = b;
        bool = ((b)localObject1).g();
        if (bool)
        {
          arrayOfString = new String[paramBoolean];
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>("[");
          str = b.getClass().getSimpleName();
          ((StringBuilder)localObject1).append(str);
          ((StringBuilder)localObject1).append("] Starting reload immediately");
          localObject1 = ((StringBuilder)localObject1).toString();
          arrayOfString[0] = localObject1;
          b.f.sendEmptyMessage(i);
          return;
        }
      }
      String[] arrayOfString = new String[paramBoolean];
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("[");
      String str = b.getClass().getSimpleName();
      ((StringBuilder)localObject1).append(str);
      ((StringBuilder)localObject1).append("] Throttling reload for 1000");
      localObject1 = ((StringBuilder)localObject1).toString();
      arrayOfString[0] = localObject1;
      b.f.removeMessages(i);
      b.f.sendEmptyMessageDelayed(i, 1000L);
      return;
    }
    localObject1 = new String[2];
    localObject1[0] = "[%s] Notifications disabled, just marking as requires reload";
    localObject2 = b.getClass().getSimpleName();
    localObject1[paramBoolean] = localObject2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */