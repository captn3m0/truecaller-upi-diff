package com.truecaller.search.local.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.truecaller.common.b.a;
import com.truecaller.search.local.model.a.s;
import org.a.a.b;

public class CallCache$Call
  implements Parcelable, s, i.a, Comparable
{
  public static final Parcelable.Creator CREATOR;
  protected final g a;
  public final long b;
  public final long c;
  public final b d;
  public final int e;
  public final long f;
  public String g;
  public String h;
  public String i;
  public String j;
  public int k;
  public int l;
  public int m;
  public int n;
  public String o;
  public String p = "-1";
  public int q;
  int r;
  
  static
  {
    CallCache.Call.1 local1 = new com/truecaller/search/local/model/CallCache$Call$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  protected CallCache$Call(Parcel paramParcel)
  {
    long l1 = paramParcel.readLong();
    b = l1;
    l1 = paramParcel.readLong();
    c = l1;
    Object localObject = new org/a/a/b;
    long l2 = paramParcel.readLong();
    ((b)localObject).<init>(l2);
    d = ((b)localObject);
    int i1 = paramParcel.readInt();
    e = i1;
    l1 = paramParcel.readLong();
    f = l1;
    localObject = paramParcel.readString();
    g = ((String)localObject);
    localObject = paramParcel.readString();
    h = ((String)localObject);
    localObject = paramParcel.readString();
    i = ((String)localObject);
    localObject = paramParcel.readString();
    j = ((String)localObject);
    i1 = paramParcel.readInt();
    k = i1;
    i1 = paramParcel.readInt();
    l = i1;
    i1 = paramParcel.readInt();
    m = i1;
    i1 = paramParcel.readInt();
    n = i1;
    localObject = paramParcel.readString();
    o = ((String)localObject);
    i1 = paramParcel.readInt();
    r = i1;
    localObject = paramParcel.readString();
    p = ((String)localObject);
    int i2 = paramParcel.readInt();
    q = i2;
    paramParcel = g.a(a.F());
    a = paramParcel;
  }
  
  public CallCache$Call(g paramg, long paramLong1, long paramLong2, long paramLong3, int paramInt, long paramLong4)
  {
    a = paramg;
    b = paramLong1;
    c = paramLong2;
    paramg = new org/a/a/b;
    paramg.<init>(paramLong3);
    d = paramg;
    e = paramInt;
    f = paramLong4;
  }
  
  public final String a()
  {
    return g;
  }
  
  public final j b()
  {
    g localg = a;
    String str = g;
    return localg.a(str);
  }
  
  public final int c()
  {
    return -1 >>> 1;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Class localClass1 = getClass();
      Class localClass2 = paramObject.getClass();
      if (localClass1 == localClass2)
      {
        paramObject = (Call)paramObject;
        long l1 = b;
        long l2 = b;
        boolean bool2 = l1 < l2;
        if (!bool2) {
          return bool1;
        }
        return false;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    long l1 = b;
    long l2 = l1 >>> 32;
    return (int)(l1 ^ l2);
  }
  
  public String toString()
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Call{date=");
    Object localObject2 = d;
    ((StringBuilder)localObject1).append(localObject2);
    ((StringBuilder)localObject1).append(", duration=");
    long l1 = f;
    ((StringBuilder)localObject1).append(l1);
    ((StringBuilder)localObject1).append(", rawNumber='");
    localObject2 = h;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    if (localObject1 == null) {
      return "null";
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("<non-null raw number>', count=");
    int i1 = r;
    ((StringBuilder)localObject1).append(i1);
    ((StringBuilder)localObject1).append('}');
    return ((StringBuilder)localObject1).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l1 = b;
    paramParcel.writeLong(l1);
    l1 = c;
    paramParcel.writeLong(l1);
    l1 = d.a;
    paramParcel.writeLong(l1);
    paramInt = e;
    paramParcel.writeInt(paramInt);
    l1 = f;
    paramParcel.writeLong(l1);
    String str = g;
    paramParcel.writeString(str);
    str = h;
    paramParcel.writeString(str);
    str = i;
    paramParcel.writeString(str);
    str = j;
    paramParcel.writeString(str);
    paramInt = k;
    paramParcel.writeInt(paramInt);
    paramInt = l;
    paramParcel.writeInt(paramInt);
    paramInt = m;
    paramParcel.writeInt(paramInt);
    paramInt = n;
    paramParcel.writeInt(paramInt);
    str = o;
    paramParcel.writeString(str);
    paramInt = r;
    paramParcel.writeInt(paramInt);
    str = p;
    paramParcel.writeString(str);
    paramInt = q;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.CallCache.Call
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */