package com.truecaller.search.local.model;

import android.net.Uri;
import android.text.TextUtils;
import com.a.a.af;
import com.google.c.a.k.d;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.content.TruecallerContract;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Link;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Source;
import com.truecaller.presence.a;
import com.truecaller.search.local.model.a.h;
import com.truecaller.search.local.model.a.i;
import com.truecaller.search.local.model.a.n;
import com.truecaller.search.local.model.a.o;
import com.truecaller.search.local.model.a.p;
import com.truecaller.search.local.model.a.r;
import com.truecaller.util.s;
import com.truecaller.util.w;
import com.truecaller.util.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

public final class j
{
  public final String a;
  public final String b;
  public final long c;
  protected final g d;
  public int e;
  public String f;
  public final int g;
  public final long h;
  public final boolean i;
  public final boolean j;
  public final int k;
  private String l;
  private String m;
  private String n;
  private String o;
  private final String p;
  private final int q;
  
  protected j(j.a parama)
  {
    Object localObject = a;
    d = ((g)localObject);
    localObject = c;
    b = ((String)localObject);
    long l1 = b;
    c = l1;
    localObject = d;
    f = ((String)localObject);
    localObject = e;
    l = ((String)localObject);
    localObject = f;
    m = ((String)localObject);
    localObject = g;
    n = ((String)localObject);
    localObject = h;
    o = ((String)localObject);
    localObject = i;
    a = ((String)localObject);
    localObject = j;
    p = ((String)localObject);
    l1 = m;
    h = l1;
    int i1 = k;
    g = i1;
    boolean bool = n;
    i = bool;
    bool = o;
    j = bool;
    int i2 = l;
    q = i2;
    int i3 = p;
    k = i3;
  }
  
  private Set i()
  {
    Object localObject = d.e();
    long l1 = c;
    localObject = (Set)j.b(l1);
    if (localObject == null) {
      localObject = s.a();
    }
    return (Set)localObject;
  }
  
  public final Object a(Class paramClass)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    int i1 = 3;
    Object localObject2 = new Set[i1];
    Object localObject3 = a();
    int i2 = 0;
    localObject2[0] = localObject3;
    localObject3 = i();
    localObject2[1] = localObject3;
    localObject3 = b();
    int i3 = 2;
    localObject2[i3] = localObject3;
    boolean bool2;
    while (i2 < i1)
    {
      localObject3 = localObject2[i2].iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject3).hasNext();
        if (!bool1) {
          break;
        }
        i locali = (i)((Iterator)localObject3).next();
        bool2 = paramClass.isInstance(locali);
        if (bool2) {
          ((ArrayList)localObject1).add(locali);
        }
      }
      i2 += 1;
    }
    paramClass = ((List)localObject1).iterator();
    localObject1 = null;
    i1 = 0;
    Object localObject4 = null;
    for (;;)
    {
      boolean bool3 = paramClass.hasNext();
      if (!bool3) {
        break;
      }
      localObject4 = paramClass.next();
      bool3 = localObject4 instanceof com.truecaller.search.local.model.a.b;
      if (bool3)
      {
        localObject3 = localObject4;
        localObject3 = (com.truecaller.search.local.model.a.b)localObject4;
        boolean bool4 = g;
        if (bool4) {
          return localObject4;
        }
      }
      if (bool3)
      {
        localObject2 = localObject4;
        localObject2 = (com.truecaller.search.local.model.a.b)localObject4;
        long l1 = f;
        long l2 = 0L;
        bool2 = l1 < l2;
        if (bool2) {
          localObject1 = localObject4;
        }
      }
    }
    if (localObject1 != null) {
      return localObject1;
    }
    return localObject4;
  }
  
  public final SortedSet a()
  {
    Object localObject = d.e();
    long l1 = c;
    localObject = (SortedSet)i.b(l1);
    if (localObject == null) {
      localObject = s.a();
    }
    return (SortedSet)localObject;
  }
  
  public final Set b()
  {
    Object localObject = d.e();
    long l1 = c;
    localObject = (Set)k.b(l1);
    if (localObject == null) {
      localObject = s.a();
    }
    return (Set)localObject;
  }
  
  public final Uri c()
  {
    long l1 = h;
    String str = p;
    return y.a(l1, str, true);
  }
  
  public final boolean d()
  {
    int i1 = g & 0x2;
    return i1 != 0;
  }
  
  public final boolean e()
  {
    String str = f;
    boolean bool = TextUtils.isEmpty(str);
    return !bool;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof j;
    if (bool1)
    {
      String str1 = paramObject.getClass().getName();
      String str2 = getClass().getName();
      bool1 = str1.equals(str2);
      if (bool1)
      {
        long l1 = c;
        paramObject = (j)paramObject;
        long l2 = c;
        boolean bool2 = l1 < l2;
        if (!bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final Contact f()
  {
    Contact localContact = new com/truecaller/data/entity/Contact;
    localContact.<init>();
    Object localObject1 = p;
    localContact.j((String)localObject1);
    localObject1 = b;
    localContact.setTcId((String)localObject1);
    int i1 = k;
    k = i1;
    localObject1 = Long.valueOf(c);
    localContact.a((Long)localObject1);
    long l1 = h;
    localObject1 = Long.valueOf(l1);
    localContact.c((Long)localObject1);
    localObject1 = TruecallerContract.b;
    long l2 = c;
    Object localObject2 = String.valueOf(l2);
    localObject1 = Uri.withAppendedPath((Uri)localObject1, (String)localObject2);
    c = ((Uri)localObject1);
    localObject1 = a;
    localContact.m((String)localObject1);
    i1 = q;
    f = i1;
    localObject1 = new com/truecaller/data/entity/Source;
    ((Source)localObject1).<init>();
    int i2 = g;
    ((Source)localObject1).setSource(i2);
    localContact.a((Source)localObject1);
    localObject1 = a();
    localObject2 = i();
    Object localObject3 = b();
    Object localObject4 = new java/util/ArrayList;
    int i4 = ((SortedSet)localObject1).size();
    int i5 = ((Set)localObject2).size();
    i4 += i5;
    i5 = ((Set)localObject3).size();
    i4 += i5;
    ((ArrayList)localObject4).<init>(i4);
    ((ArrayList)localObject4).addAll((Collection)localObject2);
    ((ArrayList)localObject4).addAll((Collection)localObject3);
    localObject1 = ((SortedSet)localObject1).iterator();
    boolean bool2;
    for (;;)
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (i)((Iterator)localObject1).next();
      ((ArrayList)localObject4).add(localObject2);
    }
    localObject1 = ((ArrayList)localObject4).iterator();
    for (;;)
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (i)((Iterator)localObject1).next();
      boolean bool3 = localObject2 instanceof o;
      if (bool3)
      {
        localObject2 = (o)localObject2;
        localObject3 = new com/truecaller/data/entity/Number;
        localObject4 = ((o)localObject2).e();
        ((Number)localObject3).<init>((String)localObject4);
        int i6 = ((o)localObject2).g();
        ((Number)localObject3).b(i6);
        localObject4 = ((o)localObject2).i();
        ((Number)localObject3).e((String)localObject4);
        localObject4 = ((o)localObject2).f();
        ((Number)localObject3).a((k.d)localObject4);
        int i3 = ((o)localObject2).h();
        ((Number)localObject3).a(i3);
        localContact.a((Number)localObject3);
      }
      else
      {
        bool3 = localObject2 instanceof com.truecaller.search.local.model.a.j;
        if (bool3)
        {
          localObject2 = Collections.singleton(((com.truecaller.search.local.model.a.j)localObject2).a());
          w.a(localContact, (Collection)localObject2);
        }
        else
        {
          bool3 = localObject2 instanceof com.truecaller.search.local.model.a.q;
          if (bool3)
          {
            localObject3 = new com/truecaller/data/entity/Address;
            ((Address)localObject3).<init>();
            localObject2 = (h)localObject2;
            localObject4 = h;
            ((Address)localObject3).setStreet((String)localObject4);
            localObject4 = i;
            ((Address)localObject3).setZipCode((String)localObject4);
            localObject4 = j;
            ((Address)localObject3).setCity((String)localObject4);
            localObject4 = k;
            ((Address)localObject3).setCountryCode((String)localObject4);
            localObject2 = l;
            ((Address)localObject3).setArea((String)localObject2);
            localContact.a((Address)localObject3);
          }
          else
          {
            bool3 = localObject2 instanceof n;
            if (bool3)
            {
              localObject2 = (n)localObject2;
              localObject3 = ((n)localObject2).d();
              localContact.k((String)localObject3);
              localObject2 = ((n)localObject2).a();
              localContact.f((String)localObject2);
            }
            else
            {
              bool3 = localObject2 instanceof p;
              if (bool3)
              {
                localObject2 = ((p)localObject2).a();
                localContact.l((String)localObject2);
              }
              else
              {
                bool3 = localObject2 instanceof r;
                if (bool3)
                {
                  localObject3 = new com/truecaller/data/entity/Link;
                  ((Link)localObject3).<init>();
                  localObject2 = ((r)localObject2).a();
                  ((Link)localObject3).setInfo((String)localObject2);
                  localObject2 = "link";
                  ((Link)localObject3).setService((String)localObject2);
                  localContact.a((Link)localObject3);
                }
              }
            }
          }
        }
      }
    }
    i1 = g & 0x1;
    if (i1 != 0)
    {
      bool1 = localContact.O();
      if (bool1) {
        localObject1 = "public";
      } else {
        localObject1 = "private";
      }
      localContact.b((String)localObject1);
    }
    localObject1 = localContact.z();
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    if (bool1)
    {
      localObject1 = f;
      localContact.l((String)localObject1);
    }
    return localContact;
  }
  
  public final a g()
  {
    a locala = com.truecaller.presence.q.a();
    Iterator localIterator = b().iterator();
    Object localObject1;
    Object localObject2;
    do
    {
      do
      {
        do
        {
          boolean bool = localIterator.hasNext();
          if (!bool) {
            break;
          }
          localObject1 = (o)localIterator.next();
          localObject2 = d;
          localObject1 = ((o)localObject1).d();
          localObject1 = ((g)localObject2).b((String)localObject1);
        } while (localObject1 == null);
        localObject2 = b;
      } while (localObject2 == null);
      locala = com.truecaller.presence.q.a(locala, (a)localObject1);
      localObject1 = b.a();
      localObject2 = Availability.Status.BUSY;
    } while (localObject1 != localObject2);
    return locala;
  }
  
  public final List h()
  {
    Iterator localIterator = b().iterator();
    ArrayList localArrayList = null;
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (o)localIterator.next();
      Object localObject2 = d;
      String str = ((o)localObject1).d();
      localObject2 = ((g)localObject2).c(str);
      if (localObject2 != null)
      {
        boolean bool2 = a;
        if (bool2)
        {
          if (localArrayList == null)
          {
            localArrayList = new java/util/ArrayList;
            localArrayList.<init>();
          }
          localObject1 = ((o)localObject1).d();
          localArrayList.add(localObject1);
        }
      }
    }
    return localArrayList;
  }
  
  public final int hashCode()
  {
    int i1 = getClass().getName().hashCode();
    long l1 = c;
    long l2 = l1 >>> 32;
    int i2 = (int)(l1 ^ l2);
    return i1 + i2;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SearchableContact{tcId='");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append('\'');
    localStringBuilder.append(", aggrId=");
    long l1 = c;
    localStringBuilder.append(l1);
    localStringBuilder.append(", frequency=");
    int i1 = e;
    localStringBuilder.append(i1);
    localStringBuilder.append(", mBadges=");
    i1 = q;
    localStringBuilder.append(i1);
    localStringBuilder.append(", source=");
    i1 = g;
    localStringBuilder.append(i1);
    localStringBuilder.append(", phonebookId=");
    l1 = h;
    localStringBuilder.append(l1);
    localStringBuilder.append(", isPrivate=");
    boolean bool = i;
    localStringBuilder.append(bool);
    localStringBuilder.append(", isFavorite=");
    bool = j;
    localStringBuilder.append(bool);
    localStringBuilder.append(", tcFlag=");
    int i2 = k;
    localStringBuilder.append(i2);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */