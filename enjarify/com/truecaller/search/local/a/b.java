package com.truecaller.search.local.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import c.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.content.c.a;
import java.util.ArrayList;
import java.util.Collection;

public final class b
  extends a
{
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramContext = new java/lang/UnsupportedOperationException;
    paramContext.<init>("Cannot upgrade the cache tables");
    throw paramContext;
  }
  
  public final String[] b()
  {
    int i = 3;
    String[][] arrayOfString = new String[i][];
    Object localObject1 = { "CREATE TABLE raw_contact(_id INTEGER PRIMARY KEY, aggregated_contact_id INTEGER REFERENCES aggregated_contact(_id) ON DELETE SET NULL ON UPDATE CASCADE, tc_id TEXT UNIQUE NOT NULL, contact_name TEXT, contact_transliterated_name TEXT, contact_is_favorite INT, contact_favorite_position INT, contact_handle TEXT, contact_alt_name TEXT, contact_gender TEXT, contact_about TEXT, contact_image_url TEXT, contact_job_title TEXT, contact_company TEXT, contact_access TEXT, contact_common_connections INT, contact_search_time INT, contact_source INT, contact_default_number TEXT, contact_phonebook_id INT, contact_phonebook_hash INT, contact_phonebook_lookup TEXT,search_query TEXT,cache_control TEXT,contact_spam_score INT,contact_badges INT DEFAULT 0, tc_flag INT NOT NULL DEFAULT 0, insert_timestamp INT NOT NULL DEFAULT 0, contact_im_id TEXT, settings_flag INT NOT NULL DEFAULT 0);", "CREATE TABLE data(_id INTEGER PRIMARY KEY NOT NULL, data_raw_contact_id INTEGER NOT NULL REFERENCES raw_contact(_id) ON DELETE CASCADE ON UPDATE CASCADE,tc_id TEXT NOT NULL, data_type INTEGER NOT NULL, data_is_primary INTEGER, data_phonebook_id INTEGER, data1 TEXT, data2 TEXT, data3 TEXT, data4 TEXT, data5 TEXT, data6 TEXT, data7 TEXT, data8 TEXT, data9 TEXT, data10 TEXT)" };
    arrayOfString[0] = localObject1;
    localObject1 = super.b();
    arrayOfString[1] = localObject1;
    Object localObject2 = "DROP TABLE IF EXISTS raw_contact";
    String str = "DROP TABLE IF EXISTS data";
    String[] tmp47_44 = new String[5];
    String[] tmp48_47 = tmp47_44;
    String[] tmp48_47 = tmp47_44;
    tmp48_47[0] = "CREATE TABLE aggregated_contact_data_table AS SELECT * FROM aggregated_contact_data";
    tmp48_47[1] = "DROP VIEW IF EXISTS aggregated_contact_raw_contact";
    String[] tmp57_48 = tmp48_47;
    String[] tmp57_48 = tmp48_47;
    tmp57_48[2] = "DROP VIEW IF EXISTS aggregated_contact_data";
    tmp57_48[3] = localObject2;
    tmp57_48[4] = str;
    localObject1 = tmp57_48;
    arrayOfString[2] = localObject1;
    k.b(arrayOfString, "arrays");
    k.b(arrayOfString, "receiver$0");
    localObject1 = arrayOfString;
    localObject1 = (Object[])arrayOfString;
    int j = 0;
    int k = 0;
    String[] arrayOfString1 = null;
    while (j < i)
    {
      localObject2 = (Object[])localObject1[j];
      int m = localObject2.length;
      k += m;
      j += 1;
    }
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>(k);
    j = 0;
    while (j < i)
    {
      arrayOfString1 = arrayOfString[j];
      localObject2 = localObject1;
      localObject2 = (Collection)localObject1;
      m.a((Collection)localObject2, arrayOfString1);
      j += 1;
    }
    localObject1 = (Collection)localObject1;
    Object localObject3 = new String[0];
    localObject3 = ((Collection)localObject1).toArray((Object[])localObject3);
    if (localObject3 != null) {
      return (String[])localObject3;
    }
    localObject3 = new c/u;
    ((u)localObject3).<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw ((Throwable)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */