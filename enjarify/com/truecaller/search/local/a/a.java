package com.truecaller.search.local.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.truecaller.content.c.ab;

public abstract class a
  extends SQLiteOpenHelper
{
  private final ab[] a;
  
  a(Context paramContext, String paramString)
  {
    super(paramContext, paramString, null, 111);
    paramContext = new ab[1];
    paramString = new com/truecaller/search/local/a/b;
    paramString.<init>();
    paramContext[0] = paramString;
    a = paramContext;
  }
  
  private void a(SQLiteDatabase paramSQLiteDatabase)
  {
    ab[] arrayOfab = a;
    int i = arrayOfab.length;
    int j = 0;
    while (j < i)
    {
      String[] arrayOfString = arrayOfab[j].a();
      int k = arrayOfString.length;
      int m = 0;
      while (m < k)
      {
        String str = arrayOfString[m];
        paramSQLiteDatabase.execSQL(str);
        m += 1;
      }
      j += 1;
    }
  }
  
  private void b(SQLiteDatabase paramSQLiteDatabase)
  {
    ab[] arrayOfab = a;
    int i = arrayOfab.length;
    int j = 0;
    while (j < i)
    {
      String[] arrayOfString = arrayOfab[j].b();
      int k = arrayOfString.length;
      int m = 0;
      while (m < k)
      {
        String str = arrayOfString[m];
        paramSQLiteDatabase.execSQL(str);
        m += 1;
      }
      j += 1;
    }
  }
  
  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    a(paramSQLiteDatabase);
    b(paramSQLiteDatabase);
  }
  
  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramSQLiteDatabase.beginTransaction();
    try
    {
      com.truecaller.common.c.b.b.c(paramSQLiteDatabase);
      a(paramSQLiteDatabase);
      b(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */