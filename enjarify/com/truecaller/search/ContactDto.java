package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable;
import com.truecaller.ads.campaigns.b;
import java.util.ArrayList;
import java.util.List;

public class ContactDto
{
  public b campaigns;
  public List data;
  public ContactDto.Pagination pagination;
  
  public ContactDto()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    data = localArrayList;
  }
  
  private static Parcel readableParcel(Parcelable paramParcelable)
  {
    Parcel localParcel = Parcel.obtain();
    paramParcelable.writeToParcel(localParcel, 0);
    localParcel.setDataPosition(0);
    return localParcel;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ContactDto{data=");
    Object localObject = data;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", campaigns=");
    localObject = campaigns;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", pagination=");
    localObject = pagination;
    localStringBuilder.append(localObject);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */