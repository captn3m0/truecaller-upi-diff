package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ContactDto$Contact$Style
  extends ContactDto.Row
{
  public static final Parcelable.Creator CREATOR;
  public String backgroundColor;
  public String imageUrls;
  
  static
  {
    ContactDto.Contact.Style.1 local1 = new com/truecaller/search/ContactDto$Contact$Style$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ContactDto$Contact$Style() {}
  
  private ContactDto$Contact$Style(Parcel paramParcel, boolean paramBoolean)
  {
    super(paramParcel);
    String str = paramParcel.readString();
    backgroundColor = str;
    str = paramParcel.readString();
    imageUrls = str;
    if (paramBoolean) {
      paramParcel.recycle();
    }
  }
  
  public ContactDto$Contact$Style(Style paramStyle)
  {
    this(paramStyle, true);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Style{backgroundColor='");
    String str1 = backgroundColor;
    localStringBuilder.append(str1);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", imageUrls='");
    String str2 = imageUrls;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    String str = backgroundColor;
    paramParcel.writeString(str);
    str = imageUrls;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Style
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */