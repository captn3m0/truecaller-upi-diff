package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ContactDto$Contact$StructuredName
  extends ContactDto.Row
{
  public static final Parcelable.Creator CREATOR;
  public String familyName;
  public String givenName;
  public String middleName;
  
  static
  {
    ContactDto.Contact.StructuredName.1 local1 = new com/truecaller/search/ContactDto$Contact$StructuredName$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ContactDto$Contact$StructuredName() {}
  
  private ContactDto$Contact$StructuredName(Parcel paramParcel, boolean paramBoolean)
  {
    super(paramParcel);
    String str = paramParcel.readString();
    givenName = str;
    str = paramParcel.readString();
    familyName = str;
    str = paramParcel.readString();
    middleName = str;
    if (paramBoolean) {
      paramParcel.recycle();
    }
  }
  
  public ContactDto$Contact$StructuredName(StructuredName paramStructuredName)
  {
    this(paramStructuredName, true);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("StructuredName{givenName='");
    String str1 = givenName;
    localStringBuilder.append(str1);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", familyName='");
    String str2 = familyName;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", middleName='");
    str2 = middleName;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    String str = givenName;
    paramParcel.writeString(str);
    str = familyName;
    paramParcel.writeString(str);
    str = middleName;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.StructuredName
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */