package com.truecaller.search;

import android.content.ContentValues;
import android.text.TextUtils;
import com.google.gson.f;
import com.truecaller.log.AssertionUtil.AlwaysFatal;

final class l$b
  implements l.a
{
  public final ContentValues a(Object paramObject)
  {
    boolean bool = paramObject instanceof ContactDto.Contact.PhoneNumber;
    Object localObject2;
    if (bool)
    {
      paramObject = (ContactDto.Contact.PhoneNumber)paramObject;
      localObject1 = new android/content/ContentValues;
      ((ContentValues)localObject1).<init>();
      localObject2 = Integer.valueOf(4);
      ((ContentValues)localObject1).put("data_type", (Integer)localObject2);
      localObject2 = rawNumberFormat;
      l.a((ContentValues)localObject1, "data9", (String)localObject2);
      localObject2 = e164Format;
      l.a((ContentValues)localObject1, "data1", (String)localObject2);
      localObject2 = nationalFormat;
      l.a((ContentValues)localObject1, "data2", (String)localObject2);
      localObject2 = dialingCode;
      l.a((ContentValues)localObject1, "data6", (String)localObject2);
      localObject2 = countryCode;
      l.a((ContentValues)localObject1, "data7", (String)localObject2);
      localObject2 = numberType;
      l.a((ContentValues)localObject1, "data8", (String)localObject2);
      localObject2 = carrier;
      l.a((ContentValues)localObject1, "data10", (String)localObject2);
      localObject2 = telType;
      l.a((ContentValues)localObject1, "data4", (String)localObject2);
      localObject2 = telTypeLabel;
      l.a((ContentValues)localObject1, "data5", (String)localObject2);
      paramObject = spamScore;
      l.a((ContentValues)localObject1, "data3", (String)paramObject);
      return (ContentValues)localObject1;
    }
    bool = paramObject instanceof ContactDto.Contact.Address;
    if (bool)
    {
      paramObject = (ContactDto.Contact.Address)paramObject;
      localObject1 = new android/content/ContentValues;
      ((ContentValues)localObject1).<init>();
      localObject2 = Integer.valueOf(1);
      ((ContentValues)localObject1).put("data_type", (Integer)localObject2);
      localObject2 = type;
      l.a((ContentValues)localObject1, "data5", (String)localObject2);
      localObject2 = street;
      l.a((ContentValues)localObject1, "data1", (String)localObject2);
      localObject2 = zipCode;
      l.a((ContentValues)localObject1, "data2", (String)localObject2);
      localObject2 = city;
      l.a((ContentValues)localObject1, "data3", (String)localObject2);
      localObject2 = area;
      l.a((ContentValues)localObject1, "data8", (String)localObject2);
      localObject2 = countryCode;
      l.a((ContentValues)localObject1, "data4", (String)localObject2);
      paramObject = timeZone;
      l.a((ContentValues)localObject1, "data7", (String)paramObject);
      return (ContentValues)localObject1;
    }
    bool = paramObject instanceof ContactDto.Contact.InternetAddress;
    if (bool)
    {
      paramObject = (ContactDto.Contact.InternetAddress)paramObject;
      localObject1 = new android/content/ContentValues;
      ((ContentValues)localObject1).<init>();
      localObject2 = Integer.valueOf(3);
      ((ContentValues)localObject1).put("data_type", (Integer)localObject2);
      localObject2 = id;
      l.a((ContentValues)localObject1, "data1", (String)localObject2);
      localObject2 = service;
      l.a((ContentValues)localObject1, "data2", (String)localObject2);
      paramObject = caption;
      l.a((ContentValues)localObject1, "data3", (String)paramObject);
      return (ContentValues)localObject1;
    }
    bool = paramObject instanceof ContactDto.Contact.Source;
    if (bool)
    {
      paramObject = (ContactDto.Contact.Source)paramObject;
      localObject1 = new android/content/ContentValues;
      ((ContentValues)localObject1).<init>();
      int i = 5;
      localObject2 = Integer.valueOf(i);
      ((ContentValues)localObject1).put("data_type", (Integer)localObject2);
      localObject2 = id;
      l.a((ContentValues)localObject1, "data1", (String)localObject2);
      localObject2 = url;
      l.a((ContentValues)localObject1, "data2", (String)localObject2);
      localObject2 = logo;
      l.a((ContentValues)localObject1, "data3", (String)localObject2);
      localObject2 = caption;
      l.a((ContentValues)localObject1, "data4", (String)localObject2);
      localObject3 = extra;
      if (localObject3 != null)
      {
        localObject3 = new com/google/gson/f;
        ((f)localObject3).<init>();
        localObject2 = "data5";
        paramObject = extra;
        paramObject = ((f)localObject3).b(paramObject);
        l.a((ContentValues)localObject1, (String)localObject2, (String)paramObject);
      }
      return (ContentValues)localObject1;
    }
    bool = paramObject instanceof ContactDto.Contact.Tag;
    Object localObject3 = null;
    if (bool)
    {
      paramObject = (ContactDto.Contact.Tag)paramObject;
      localObject1 = tag;
      bool = TextUtils.isEmpty((CharSequence)localObject1);
      if (bool) {
        return null;
      }
      localObject1 = new android/content/ContentValues;
      ((ContentValues)localObject1).<init>();
      localObject2 = Integer.valueOf(6);
      ((ContentValues)localObject1).put("data_type", (Integer)localObject2);
      paramObject = tag;
      ((ContentValues)localObject1).put("data1", (String)paramObject);
      return (ContentValues)localObject1;
    }
    bool = paramObject instanceof ContactDto.Contact.Business;
    if (bool)
    {
      paramObject = (ContactDto.Contact.Business)paramObject;
      localObject1 = new android/content/ContentValues;
      ((ContentValues)localObject1).<init>();
      localObject2 = Integer.valueOf(9);
      ((ContentValues)localObject1).put("data_type", (Integer)localObject2);
      localObject2 = branch;
      ((ContentValues)localObject1).put("data1", (String)localObject2);
      localObject2 = department;
      ((ContentValues)localObject1).put("data2", (String)localObject2);
      localObject2 = companySize;
      ((ContentValues)localObject1).put("data3", (String)localObject2);
      localObject2 = openingHours;
      ((ContentValues)localObject1).put("data4", (String)localObject2);
      localObject2 = landline;
      ((ContentValues)localObject1).put("data5", (String)localObject2);
      localObject2 = score;
      ((ContentValues)localObject1).put("data6", (String)localObject2);
      paramObject = swishNumber;
      ((ContentValues)localObject1).put("data7", (String)paramObject);
      return (ContentValues)localObject1;
    }
    bool = paramObject instanceof ContactDto.Contact.Style;
    if (bool)
    {
      paramObject = (ContactDto.Contact.Style)paramObject;
      localObject1 = new android/content/ContentValues;
      ((ContentValues)localObject1).<init>();
      localObject2 = Integer.valueOf(10);
      ((ContentValues)localObject1).put("data_type", (Integer)localObject2);
      localObject2 = backgroundColor;
      ((ContentValues)localObject1).put("data1", (String)localObject2);
      paramObject = imageUrls;
      ((ContentValues)localObject1).put("data2", (String)paramObject);
      return (ContentValues)localObject1;
    }
    Object localObject1 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(false, (String[])localObject1);
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.l.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */