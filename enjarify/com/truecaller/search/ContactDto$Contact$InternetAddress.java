package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ContactDto$Contact$InternetAddress
  extends ContactDto.Row
{
  public static final Parcelable.Creator CREATOR;
  public String caption;
  public String id;
  public String service;
  public String type;
  
  static
  {
    ContactDto.Contact.InternetAddress.1 local1 = new com/truecaller/search/ContactDto$Contact$InternetAddress$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ContactDto$Contact$InternetAddress() {}
  
  private ContactDto$Contact$InternetAddress(Parcel paramParcel, boolean paramBoolean)
  {
    super(paramParcel);
    String str = paramParcel.readString();
    type = str;
    str = paramParcel.readString();
    id = str;
    str = paramParcel.readString();
    service = str;
    str = paramParcel.readString();
    caption = str;
    if (paramBoolean) {
      paramParcel.recycle();
    }
  }
  
  public ContactDto$Contact$InternetAddress(InternetAddress paramInternetAddress)
  {
    this(paramInternetAddress, true);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("InternetAddress{type='");
    String str1 = type;
    localStringBuilder.append(str1);
    char c = '\'';
    localStringBuilder.append(c);
    localStringBuilder.append(", id='");
    String str2 = id;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", rowId=");
    long l1 = rowId;
    localStringBuilder.append(l1);
    localStringBuilder.append(", tcId='");
    str2 = tcId;
    localStringBuilder.append(str2);
    localStringBuilder.append(c);
    localStringBuilder.append(", isPrimary=");
    boolean bool = isPrimary;
    localStringBuilder.append(bool);
    localStringBuilder.append(", phonebookId=");
    long l2 = phonebookId;
    localStringBuilder.append(l2);
    localStringBuilder.append(", source=");
    int i = source;
    localStringBuilder.append(i);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    String str = type;
    paramParcel.writeString(str);
    str = id;
    paramParcel.writeString(str);
    str = service;
    paramParcel.writeString(str);
    str = caption;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.InternetAddress
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */