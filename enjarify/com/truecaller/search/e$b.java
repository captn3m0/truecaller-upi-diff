package com.truecaller.search;

import android.content.Context;
import android.text.TextUtils;
import com.google.c.a.g;
import com.google.c.a.k;
import com.google.c.a.m.a;
import com.google.c.a.m.a.a;
import com.truecaller.common.b.a;
import com.truecaller.data.entity.Number;

final class e$b
  extends e.a
{
  static final b b;
  
  static
  {
    b localb = new com/truecaller/search/e$b;
    localb.<init>();
    b = localb;
  }
  
  final boolean a(Context paramContext, Number paramNumber)
  {
    if (paramNumber != null)
    {
      Object localObject1 = paramNumber.o();
      boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool1)
      {
        paramContext = b(paramContext, (String)localObject1);
        boolean bool2 = TextUtils.isEmpty(paramContext);
        if (!bool2)
        {
          localObject1 = a.F().H();
          paramNumber = paramNumber.d();
          bool1 = TextUtils.isEmpty(paramNumber);
          boolean bool3 = true;
          if (!bool1)
          {
            bool1 = TextUtils.isEmpty((CharSequence)localObject1);
            if (!bool1)
            {
              Object localObject2 = k.a();
              try
              {
                paramContext = ((k)localObject2).b(paramContext, (String)localObject1);
                paramContext = m;
                Object localObject3 = m.a.a.d;
                if (paramContext != localObject3)
                {
                  paramNumber = ((k)localObject2).b(paramNumber, (String)localObject1);
                  paramNumber = m;
                  localObject1 = new String[bool3];
                  localObject2 = new java/lang/StringBuilder;
                  localObject3 = "Incoming/outgoing CC-source:";
                  ((StringBuilder)localObject2).<init>((String)localObject3);
                  ((StringBuilder)localObject2).append(paramNumber);
                  localObject3 = ", contact CC-source:";
                  ((StringBuilder)localObject2).append((String)localObject3);
                  ((StringBuilder)localObject2).append(paramContext);
                  localObject2 = ((StringBuilder)localObject2).toString();
                  localObject1[0] = localObject2;
                  if (paramNumber == paramContext) {
                    return bool3;
                  }
                  localObject1 = m.a.a.a;
                  if (paramNumber != localObject1)
                  {
                    localObject1 = m.a.a.b;
                    if (paramNumber != localObject1) {}
                  }
                  else
                  {
                    paramNumber = m.a.a.a;
                    if (paramContext == paramNumber) {
                      break label225;
                    }
                    paramNumber = m.a.a.b;
                    if (paramContext == paramNumber) {
                      break label225;
                    }
                  }
                  return false;
                  label225:
                  return bool3;
                }
              }
              catch (g localg) {}
            }
          }
          return bool3;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */