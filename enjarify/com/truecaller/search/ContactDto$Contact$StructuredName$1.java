package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ContactDto$Contact$StructuredName$1
  implements Parcelable.Creator
{
  public final ContactDto.Contact.StructuredName createFromParcel(Parcel paramParcel)
  {
    ContactDto.Contact.StructuredName localStructuredName = new com/truecaller/search/ContactDto$Contact$StructuredName;
    localStructuredName.<init>(paramParcel, false, null);
    return localStructuredName;
  }
  
  public final ContactDto.Contact.StructuredName[] newArray(int paramInt)
  {
    return new ContactDto.Contact.StructuredName[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.StructuredName.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */