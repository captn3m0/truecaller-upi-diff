package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ContactDto$Contact$Style$1
  implements Parcelable.Creator
{
  public final ContactDto.Contact.Style createFromParcel(Parcel paramParcel)
  {
    ContactDto.Contact.Style localStyle = new com/truecaller/search/ContactDto$Contact$Style;
    localStyle.<init>(paramParcel, false, null);
    return localStyle;
  }
  
  public final ContactDto.Contact.Style[] newArray(int paramInt)
  {
    return new ContactDto.Contact.Style[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Style.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */