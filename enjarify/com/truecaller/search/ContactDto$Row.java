package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;
import org.c.a.a.a.b.a;

public abstract class ContactDto$Row
  implements Parcelable
{
  public transient boolean isPrimary;
  public transient long phonebookId;
  public transient long rowId;
  public transient int source;
  public transient String tcId;
  
  protected ContactDto$Row() {}
  
  ContactDto$Row(Parcel paramParcel)
  {
    long l = paramParcel.readLong();
    rowId = l;
    String str = paramParcel.readString();
    tcId = str;
    int i = paramParcel.readInt();
    int j = 1;
    if (i != j) {
      j = 0;
    }
    isPrimary = j;
    l = paramParcel.readLong();
    phonebookId = l;
    int k = paramParcel.readInt();
    source = k;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  List readList(Parcel paramParcel, Parcelable.Creator paramCreator)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramParcel.readTypedList(localArrayList, paramCreator);
    boolean bool = localArrayList.isEmpty();
    if (bool) {
      return null;
    }
    return localArrayList;
  }
  
  Number readNumber(Parcel paramParcel)
  {
    paramParcel = paramParcel.readString();
    if (paramParcel == null) {
      return null;
    }
    return a.b(paramParcel);
  }
  
  List readStringList(Parcel paramParcel)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramParcel.readStringList(localArrayList);
    boolean bool = localArrayList.isEmpty();
    if (bool) {
      return null;
    }
    return localArrayList;
  }
  
  void writeNumber(Parcel paramParcel, Number paramNumber)
  {
    if (paramNumber == null) {
      paramNumber = null;
    } else {
      paramNumber = String.valueOf(paramNumber);
    }
    paramParcel.writeString(paramNumber);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l = rowId;
    paramParcel.writeLong(l);
    String str = tcId;
    paramParcel.writeString(str);
    paramInt = isPrimary;
    paramParcel.writeInt(paramInt);
    l = phonebookId;
    paramParcel.writeLong(l);
    paramInt = source;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Row
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */