package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ContactDto$Contact$Business$1
  implements Parcelable.Creator
{
  public final ContactDto.Contact.Business createFromParcel(Parcel paramParcel)
  {
    ContactDto.Contact.Business localBusiness = new com/truecaller/search/ContactDto$Contact$Business;
    localBusiness.<init>(paramParcel, false, null);
    return localBusiness;
  }
  
  public final ContactDto.Contact.Business[] newArray(int paramInt)
  {
    return new ContactDto.Contact.Business[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.Business.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */