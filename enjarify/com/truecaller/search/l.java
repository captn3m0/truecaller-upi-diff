package com.truecaller.search;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.content.TruecallerContract.k;
import com.truecaller.content.a.a;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public final class l
{
  private static ContentValues a(ContactDto.Contact paramContact)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Object localObject = id;
    a(localContentValues, "tc_id", (String)localObject);
    localObject = name;
    a(localContentValues, "contact_name", (String)localObject);
    localObject = transliteratedName;
    a(localContentValues, "contact_transliterated_name", (String)localObject);
    localObject = handle;
    a(localContentValues, "contact_handle", (String)localObject);
    localObject = altName;
    a(localContentValues, "contact_alt_name", (String)localObject);
    localObject = gender;
    a(localContentValues, "contact_gender", (String)localObject);
    localObject = about;
    a(localContentValues, "contact_about", (String)localObject);
    localObject = image;
    a(localContentValues, "contact_image_url", (String)localObject);
    localObject = jobTitle;
    a(localContentValues, "contact_job_title", (String)localObject);
    localObject = companyName;
    a(localContentValues, "contact_company", (String)localObject);
    localObject = access;
    a(localContentValues, "contact_access", (String)localObject);
    localObject = imId;
    a(localContentValues, "contact_im_id", (String)localObject);
    localObject = Integer.valueOf(a.a(badges));
    localContentValues.put("contact_badges", (Integer)localObject);
    localObject = Integer.valueOf(source);
    localContentValues.put("contact_source", (Integer)localObject);
    localObject = Integer.valueOf(commonConnections);
    localContentValues.put("contact_common_connections", (Integer)localObject);
    localObject = Long.valueOf(searchTime);
    localContentValues.put("contact_search_time", (Long)localObject);
    long l1 = aggregatedRowId;
    a(localContentValues, "aggregated_contact_id", l1);
    l1 = phonebookId;
    a(localContentValues, "contact_phonebook_id", l1);
    l1 = phonebookHash;
    a(localContentValues, "contact_phonebook_hash", l1);
    localObject = searchQuery;
    a(localContentValues, "search_query", (String)localObject);
    localObject = cacheControl;
    a(localContentValues, "cache_control", (String)localObject);
    localObject = phonebookLookupKey;
    a(localContentValues, "contact_phonebook_lookup", (String)localObject);
    String str = "contact_default_number";
    localObject = defaultNumber;
    a(localContentValues, str, (String)localObject);
    int i = Integer.bitCount(source);
    int j = 1;
    if (i == j)
    {
      i = 1;
    }
    else
    {
      i = 0;
      str = null;
    }
    String[] arrayOfString = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(i, arrayOfString);
    long l2 = searchTime;
    long l3 = 0L;
    boolean bool1 = l2 < l3;
    if (bool1)
    {
      bool1 = true;
    }
    else
    {
      bool1 = false;
      str = null;
    }
    arrayOfString = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool1, arrayOfString);
    str = id;
    if (str != null)
    {
      bool1 = true;
    }
    else
    {
      bool1 = false;
      str = null;
    }
    arrayOfString = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool1, arrayOfString);
    str = phonebookLookupKey;
    if (str == null)
    {
      bool1 = true;
    }
    else
    {
      bool1 = false;
      str = null;
    }
    l2 = phonebookId;
    boolean bool3 = l2 < l3;
    if (!bool3)
    {
      bool3 = true;
    }
    else
    {
      bool3 = false;
      paramContact = null;
    }
    boolean bool2;
    if (bool1 != bool3)
    {
      bool2 = false;
      localObject = null;
    }
    paramContact = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool2, paramContact);
    return localContentValues;
  }
  
  private static void a(ContentValues paramContentValues, String paramString, long paramLong)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (bool)
    {
      Long localLong = Long.valueOf(paramLong);
      paramContentValues.put(paramString, localLong);
    }
  }
  
  static void a(ContentValues paramContentValues, String paramString1, String paramString2)
  {
    if (paramString2 != null) {
      paramContentValues.put(paramString1, paramString2);
    }
  }
  
  public static void a(Context paramContext, ArrayList paramArrayList, List paramList)
  {
    boolean bool = paramArrayList.isEmpty();
    if (!bool)
    {
      try
      {
        paramContext = paramContext.getContentResolver();
        String str = TruecallerContract.a();
        paramContext = paramContext.applyBatch(str, paramArrayList);
        int j = paramList.size();
        int i = paramContext.length;
        j = Math.min(j, i);
        i = 0;
        str = null;
        while (i < j)
        {
          Object localObject = paramContext[i];
          localObject = uri;
          if (localObject != null)
          {
            localObject = paramList.get(i);
            localObject = (ContactDto.Row)localObject;
            if (localObject != null)
            {
              Uri localUri = paramContext[i];
              localUri = uri;
              long l = ContentUris.parseId(localUri);
              rowId = l;
            }
          }
          i += 1;
        }
        return;
      }
      catch (RemoteException paramContext) {}catch (OperationApplicationException paramContext) {}
      d.a(paramContext);
    }
  }
  
  public static void a(List paramList, String paramString1, String paramString2, long paramLong)
  {
    boolean bool1 = TextUtils.isEmpty(paramString1);
    if (!bool1)
    {
      Object localObject1 = UUID.randomUUID().toString();
      ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newInsert(TruecallerContract.ah.a()).withValue("tc_id", localObject1);
      Object localObject2 = Long.valueOf(paramLong);
      localObject2 = localBuilder.withValue("contact_search_time", localObject2).withValue("search_query", paramString1).withValue("contact_default_number", paramString2);
      Object localObject3 = "contact_source";
      int i = 4;
      Object localObject4 = Integer.valueOf(i);
      localObject2 = ((ContentProviderOperation.Builder)localObject2).withValue((String)localObject3, localObject4).build();
      paramList.add(localObject2);
      boolean bool2 = TextUtils.isEmpty(paramString2);
      if (!bool2)
      {
        int j = paramList.size() + -1;
        localObject3 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
        localObject4 = "data_raw_contact_id";
        localObject2 = ((ContentProviderOperation.Builder)localObject3).withValueBackReference((String)localObject4, j).withValue("tc_id", localObject1);
        localObject1 = Integer.valueOf(i);
        localObject2 = ((ContentProviderOperation.Builder)localObject2).withValue("data_type", localObject1);
        localObject3 = "data1";
        paramString1 = ((ContentProviderOperation.Builder)localObject2).withValue((String)localObject3, paramString2).withValue("data9", paramString1);
        paramString2 = "data4";
        j = 2;
        localObject2 = Integer.valueOf(j);
        paramString1 = paramString1.withValue(paramString2, localObject2).build();
        paramList.add(paramString1);
      }
    }
  }
  
  public static void a(List paramList1, List paramList2, ContactDto.Contact paramContact)
  {
    Object localObject1 = ContentProviderOperation.newInsert(TruecallerContract.ah.a());
    Object localObject2 = a(paramContact);
    localObject1 = ((ContentProviderOperation.Builder)localObject1).withValues((ContentValues)localObject2).build();
    paramList1.add(localObject1);
    int i = paramList1.size() + -1;
    String str = id;
    Object localObject3 = phones;
    l.b localb = new com/truecaller/search/l$b;
    localb.<init>((byte)0);
    localObject2 = paramList1;
    a(paramList1, paramList2, (Iterable)localObject3, i, str, localb);
    localObject3 = addresses;
    localb = new com/truecaller/search/l$b;
    localb.<init>((byte)0);
    a(paramList1, paramList2, (Iterable)localObject3, i, str, localb);
    localObject3 = internetAddresses;
    localb = new com/truecaller/search/l$b;
    localb.<init>((byte)0);
    a(paramList1, paramList2, (Iterable)localObject3, i, str, localb);
    localObject3 = sources;
    localb = new com/truecaller/search/l$b;
    localb.<init>((byte)0);
    a(paramList1, paramList2, (Iterable)localObject3, i, str, localb);
    localObject3 = tags;
    localb = new com/truecaller/search/l$b;
    localb.<init>((byte)0);
    a(paramList1, paramList2, (Iterable)localObject3, i, str, localb);
    localObject3 = business;
    localb = new com/truecaller/search/l$b;
    localb.<init>((byte)0);
    a(paramList1, paramList2, (ContactDto.Row)localObject3, i, str, localb);
    localObject3 = style;
    localb = new com/truecaller/search/l$b;
    localb.<init>((byte)0);
    a(paramList1, paramList2, (ContactDto.Row)localObject3, i, str, localb);
  }
  
  private static void a(List paramList1, List paramList2, ContactDto.Row paramRow, int paramInt, String paramString, l.a parama)
  {
    if (paramRow != null) {
      paramRow = Collections.singletonList(paramRow);
    } else {
      paramRow = null;
    }
    a(paramList1, paramList2, paramRow, paramInt, paramString, parama);
  }
  
  public static void a(List paramList1, List paramList2, ContactDto paramContactDto)
  {
    if (paramContactDto != null)
    {
      Object localObject = data;
      if (localObject != null)
      {
        paramContactDto = data.iterator();
        for (;;)
        {
          boolean bool = paramContactDto.hasNext();
          if (!bool) {
            break;
          }
          localObject = (ContactDto.Contact)paramContactDto.next();
          a(paramList1, paramList2, (ContactDto.Contact)localObject);
        }
      }
    }
  }
  
  private static void a(List paramList1, List paramList2, Iterable paramIterable, int paramInt, String paramString, l.a parama)
  {
    if (paramIterable == null) {
      return;
    }
    paramIterable = paramIterable.iterator();
    for (;;)
    {
      boolean bool = paramIterable.hasNext();
      if (!bool) {
        break;
      }
      ContactDto.Row localRow = (ContactDto.Row)paramIterable.next();
      Object localObject = parama.a(localRow);
      if (localObject != null)
      {
        localObject = ContentProviderOperation.newInsert(TruecallerContract.k.a()).withValues((ContentValues)localObject).withValue("tc_id", paramString);
        String str = "data_raw_contact_id";
        localObject = ((ContentProviderOperation.Builder)localObject).withValueBackReference(str, paramInt).build();
        paramList1.add(localObject);
        if (paramList2 != null) {
          paramList2.add(localRow);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */