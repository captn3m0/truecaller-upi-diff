package com.truecaller.search;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ContactDto$Contact$PhoneNumber$1
  implements Parcelable.Creator
{
  public final ContactDto.Contact.PhoneNumber createFromParcel(Parcel paramParcel)
  {
    ContactDto.Contact.PhoneNumber localPhoneNumber = new com/truecaller/search/ContactDto$Contact$PhoneNumber;
    localPhoneNumber.<init>(paramParcel, false, null);
    return localPhoneNumber;
  }
  
  public final ContactDto.Contact.PhoneNumber[] newArray(int paramInt)
  {
    return new ContactDto.Contact.PhoneNumber[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.ContactDto.Contact.PhoneNumber.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */