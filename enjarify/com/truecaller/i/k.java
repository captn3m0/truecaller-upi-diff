package com.truecaller.i;

import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d
{
  private final h a;
  private final Provider b;
  
  private k(h paramh, Provider paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static k a(h paramh, Provider paramProvider)
  {
    k localk = new com/truecaller/i/k;
    localk.<init>(paramh, paramProvider);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.i.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */