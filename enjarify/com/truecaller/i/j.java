package com.truecaller.i;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final h a;
  private final Provider b;
  
  private j(h paramh, Provider paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static j a(h paramh, Provider paramProvider)
  {
    j localj = new com/truecaller/i/j;
    localj.<init>(paramh, paramProvider);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.i.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */