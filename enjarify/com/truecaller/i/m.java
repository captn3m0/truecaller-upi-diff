package com.truecaller.i;

import android.content.Context;
import android.content.SharedPreferences;
import c.g.b.k;
import com.truecaller.utils.a.a;

public abstract class m
  extends a
{
  public m(SharedPreferences paramSharedPreferences)
  {
    super(paramSharedPreferences);
  }
  
  public int a()
  {
    c.m localm = new c/m;
    String str = String.valueOf("To support settings migration implement [migrateFrom], [currentVersion] and [name]. Migrations should start with moving values to separate file");
    str = "An operation is not implemented: ".concat(str);
    localm.<init>(str);
    throw ((Throwable)localm);
  }
  
  public final int a(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    long l = paramInt;
    return (int)a(paramString, l);
  }
  
  public void a(int paramInt, Context paramContext)
  {
    k.b(paramContext, "context");
    c.m localm = new c/m;
    paramContext = String.valueOf("To support settings migration implement [migrateFrom], [currentVersion] and [name]. Migrations should start with moving values to separate file");
    paramContext = "An operation is not implemented: ".concat(paramContext);
    localm.<init>(paramContext);
    throw ((Throwable)localm);
  }
  
  public String b()
  {
    c.m localm = new c/m;
    String str = String.valueOf("To support settings migration implement [migrateFrom], [currentVersion] and [name]. Migrations should start with moving values to separate file");
    str = "An operation is not implemented: ".concat(str);
    localm.<init>(str);
    throw ((Throwable)localm);
  }
  
  public final void b(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    long l = paramInt;
    b(paramString, l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.i.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */