package com.truecaller.deeplink;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.aj;
import android.support.v4.f.j;
import android.text.TextUtils;
import android.widget.Toast;
import c.n;
import com.airbnb.deeplinkdispatch.DeepLinkResult;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.am;
import com.truecaller.common.h.k;
import com.truecaller.data.access.i;
import com.truecaller.data.entity.Contact;
import com.truecaller.flashsdk.core.c;
import com.truecaller.flashsdk.models.e;
import com.truecaller.messaging.sharing.SharingActivity;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.premium.data.SubscriptionPromoEventMetaData;
import com.truecaller.sdk.ai;
import com.truecaller.truepay.app.deeplink.TcPaySdkDeeplinkModuleLoader;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.clicktocall.CallConfirmationActivity;
import java.util.UUID;

public class DeepLinkHandlerActivity
  extends Activity
{
  /* Error */
  private j a(Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: invokevirtual 12	com/truecaller/deeplink/DeepLinkHandlerActivity:getContentResolver	()Landroid/content/ContentResolver;
    //   6: astore_3
    //   7: iconst_0
    //   8: istore 4
    //   10: aload_1
    //   11: astore 5
    //   13: aload_3
    //   14: aload_1
    //   15: aconst_null
    //   16: aconst_null
    //   17: aconst_null
    //   18: aconst_null
    //   19: invokevirtual 18	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   22: astore_1
    //   23: aload_1
    //   24: ifnull +102 -> 126
    //   27: aload_1
    //   28: invokeinterface 24 1 0
    //   33: istore 6
    //   35: iload 6
    //   37: ifeq +89 -> 126
    //   40: ldc 26
    //   42: astore_3
    //   43: aload_1
    //   44: aload_3
    //   45: invokeinterface 30 2 0
    //   50: istore 6
    //   52: aload_1
    //   53: iload 6
    //   55: invokeinterface 34 2 0
    //   60: astore_3
    //   61: ldc 36
    //   63: astore 5
    //   65: aload_1
    //   66: aload 5
    //   68: invokeinterface 30 2 0
    //   73: istore 7
    //   75: iconst_m1
    //   76: istore 4
    //   78: iload 7
    //   80: iload 4
    //   82: if_icmpeq +49 -> 131
    //   85: ldc 36
    //   87: astore 5
    //   89: aload_1
    //   90: aload 5
    //   92: invokeinterface 30 2 0
    //   97: istore 7
    //   99: aload_1
    //   100: iload 7
    //   102: invokeinterface 34 2 0
    //   107: astore_2
    //   108: goto +23 -> 131
    //   111: astore 5
    //   113: goto +46 -> 159
    //   116: astore 5
    //   118: iconst_0
    //   119: istore 6
    //   121: aconst_null
    //   122: astore_3
    //   123: goto +36 -> 159
    //   126: iconst_0
    //   127: istore 6
    //   129: aconst_null
    //   130: astore_3
    //   131: aload_1
    //   132: ifnull +39 -> 171
    //   135: aload_1
    //   136: invokeinterface 39 1 0
    //   141: goto +30 -> 171
    //   144: astore_2
    //   145: aconst_null
    //   146: astore_1
    //   147: goto +37 -> 184
    //   150: astore 5
    //   152: aconst_null
    //   153: astore_1
    //   154: iconst_0
    //   155: istore 6
    //   157: aconst_null
    //   158: astore_3
    //   159: aload 5
    //   161: invokestatic 45	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   164: aload_1
    //   165: ifnull +6 -> 171
    //   168: goto -33 -> 135
    //   171: new 47	android/support/v4/f/j
    //   174: astore_1
    //   175: aload_1
    //   176: aload_3
    //   177: aload_2
    //   178: invokespecial 50	android/support/v4/f/j:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   181: aload_1
    //   182: areturn
    //   183: astore_2
    //   184: aload_1
    //   185: ifnull +9 -> 194
    //   188: aload_1
    //   189: invokeinterface 39 1 0
    //   194: aload_2
    //   195: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	196	0	this	DeepLinkHandlerActivity
    //   0	196	1	paramUri	Uri
    //   1	107	2	str	String
    //   144	34	2	localObject1	Object
    //   183	12	2	localObject2	Object
    //   6	171	3	localObject3	Object
    //   8	75	4	i	int
    //   11	80	5	localObject4	Object
    //   111	1	5	localSQLException1	android.database.SQLException
    //   116	1	5	localSQLException2	android.database.SQLException
    //   150	10	5	localSQLException3	android.database.SQLException
    //   33	3	6	bool	boolean
    //   50	106	6	j	int
    //   73	28	7	k	int
    // Exception table:
    //   from	to	target	type
    //   66	73	111	android/database/SQLException
    //   90	97	111	android/database/SQLException
    //   100	107	111	android/database/SQLException
    //   27	33	116	android/database/SQLException
    //   44	50	116	android/database/SQLException
    //   53	60	116	android/database/SQLException
    //   2	6	144	finally
    //   18	22	144	finally
    //   2	6	150	android/database/SQLException
    //   18	22	150	android/database/SQLException
    //   27	33	183	finally
    //   44	50	183	finally
    //   53	60	183	finally
    //   66	73	183	finally
    //   90	97	183	finally
    //   100	107	183	finally
    //   159	164	183	finally
  }
  
  private void a()
  {
    Object localObject1 = new com/truecaller/deeplink/b;
    Object localObject2 = new com/truecaller/deeplink/a;
    ((a)localObject2).<init>();
    Object localObject3 = new com/truecaller/truepay/app/deeplink/TcPaySdkDeeplinkModuleLoader;
    ((TcPaySdkDeeplinkModuleLoader)localObject3).<init>();
    Object localObject4 = new com/truecaller/sdk/ai;
    ((ai)localObject4).<init>();
    Object localObject5 = new com/truecaller/credit/app/b/a;
    ((com.truecaller.credit.app.b.a)localObject5).<init>();
    ((b)localObject1).<init>((a)localObject2, (TcPaySdkDeeplinkModuleLoader)localObject3, (ai)localObject4, (com.truecaller.credit.app.b.a)localObject5);
    localObject1 = ((b)localObject1).a(this);
    boolean bool1;
    if (localObject1 != null)
    {
      bool1 = ((DeepLinkResult)localObject1).isSuccessful();
      if (bool1)
      {
        finish();
        return;
      }
    }
    localObject1 = getIntent();
    boolean bool2 = a((Intent)localObject1);
    if (bool2)
    {
      finish();
      return;
    }
    localObject2 = ((Intent)localObject1).getData();
    if (localObject2 != null)
    {
      localObject3 = ((Uri)localObject2).getHost();
      boolean bool4 = TextUtils.isEmpty((CharSequence)localObject3);
      if (!bool4)
      {
        int j = -1;
        int k = ((String)localObject3).hashCode();
        boolean bool6;
        switch (k)
        {
        default: 
          break;
        case 1465777723: 
          localObject5 = "invite_from_fb";
          bool6 = ((String)localObject3).equals(localObject5);
          if (bool6) {
            j = 1;
          }
          break;
        case 954925063: 
          localObject5 = "message";
          bool6 = ((String)localObject3).equals(localObject5);
          if (bool6) {
            j = 2;
          }
          break;
        case 97513456: 
          localObject5 = "flash";
          bool6 = ((String)localObject3).equals(localObject5);
          if (bool6) {
            j = 6;
          }
          break;
        case 110760: 
          localObject5 = "pay";
          bool6 = ((String)localObject3).equals(localObject5);
          if (bool6) {
            j = 4;
          }
          break;
        case -318452137: 
          localObject5 = "premium";
          bool6 = ((String)localObject3).equals(localObject5);
          if (bool6)
          {
            j = 0;
            localObject4 = null;
          }
          break;
        case -606747530: 
          localObject5 = "call_confirmation";
          bool6 = ((String)localObject3).equals(localObject5);
          if (bool6) {
            j = 3;
          }
          break;
        case -806191449: 
          localObject5 = "recharge";
          bool6 = ((String)localObject3).equals(localObject5);
          if (bool6) {
            j = 5;
          }
          break;
        }
        boolean bool3;
        switch (j)
        {
        default: 
          localObject3 = new android/content/Intent;
          localObject4 = TruecallerInit.class;
          ((Intent)localObject3).<init>(this, (Class)localObject4);
          localObject1 = ((Intent)localObject1).getAction();
          localObject1 = ((Intent)localObject3).setAction((String)localObject1).setData((Uri)localObject2);
          int i = 268468224;
          localObject1 = ((Intent)localObject1).setFlags(i);
          startActivity((Intent)localObject1);
          break;
        case 6: 
          localObject3 = ((Uri)localObject2).getQueryParameter("recipient");
          localObject4 = ((Uri)localObject2).getQueryParameter("name");
          localObject5 = ((Uri)localObject2).getQueryParameter("text");
          String str1 = ((Uri)localObject2).getQueryParameter("promo");
          Object localObject6 = ((Uri)localObject2).getQueryParameter("image");
          String str2 = ((Uri)localObject2).getQueryParameter("video");
          localObject1 = ((Uri)localObject2).getQueryParameter("imagemode");
          String str3 = ((Uri)localObject2).getQueryParameter("background");
          bool3 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool3)
          {
            bool3 = TextUtils.isEmpty((CharSequence)localObject6);
            if (bool3)
            {
              bool3 = TextUtils.isEmpty(str2);
              if (bool3) {}
            }
            else
            {
              bool1 = Boolean.parseBoolean((String)localObject1);
              if (bool1)
              {
                bool7 = true;
                break label574;
              }
            }
          }
          boolean bool7 = false;
          localObject1 = this;
          localObject2 = localObject3;
          localObject3 = localObject4;
          localObject4 = localObject5;
          localObject5 = localObject6;
          localObject6 = str3;
          a((String)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, str2, str1, bool7, str3);
          break;
        case 5: 
          localObject1 = ((Uri)localObject2).getQueryParameter("recipient");
          localObject3 = "amount";
          ((Uri)localObject2).getQueryParameter((String)localObject3);
          bool3 = am.c((CharSequence)localObject1);
          if (bool3)
          {
            localObject1 = ((String)localObject1).replace("+", "");
            localObject2 = " ";
            localObject3 = "";
            localObject1 = ((String)localObject1).replace((CharSequence)localObject2, (CharSequence)localObject3);
            ((String)localObject1).trim();
          }
          break;
        case 4: 
          localObject1 = TrueApp.y();
          bool1 = ((TrueApp)localObject1).isTcPayEnabled();
          if (bool1)
          {
            localObject1 = ((Uri)localObject2).getQueryParameter("recipient");
            localObject3 = ((Uri)localObject2).getQueryParameter("amount");
            localObject4 = "comment";
            localObject5 = ((Uri)localObject2).getQueryParameter((String)localObject4);
            bool3 = am.c((CharSequence)localObject1);
            if (bool3)
            {
              localObject1 = ((String)localObject1).replace("+", "");
              localObject2 = " ";
              localObject1 = ((String)localObject1).replace((CharSequence)localObject2, "").trim();
              localObject4 = localObject1;
            }
            else
            {
              localObject4 = localObject1;
            }
            localObject1 = this;
            localObject2 = localObject3;
            localObject3 = localObject4;
            j = 0;
            localObject4 = null;
            TransactionActivity.startForSend(this, (String)localObject2, (String)localObject3, null, (String)localObject5, null);
          }
          break;
        case 3: 
          localObject1 = new android/content/Intent;
          localObject3 = CallConfirmationActivity.class;
          ((Intent)localObject1).<init>(this, (Class)localObject3);
          int m = 67108864;
          localObject1 = ((Intent)localObject1).addFlags(m).setData((Uri)localObject2);
          startActivity((Intent)localObject1);
          break;
        case 2: 
          localObject1 = ((Uri)localObject2).getQueryParameter("recipient");
          localObject2 = ((Uri)localObject2).getQueryParameter("text");
          localObject3 = new android/content/Intent;
          ((Intent)localObject3).<init>(this, SharingActivity.class);
          localObject4 = "android.intent.action.SENDTO";
          ((Intent)localObject3).setAction((String)localObject4);
          boolean bool5 = am.c((CharSequence)localObject1);
          if (bool5)
          {
            localObject4 = "smsto:";
            localObject1 = String.valueOf(localObject1);
            localObject1 = Uri.parse(((String)localObject4).concat((String)localObject1));
            ((Intent)localObject3).setData((Uri)localObject1);
          }
          if (localObject2 != null)
          {
            localObject1 = "android.intent.extra.TEXT";
            ((Intent)localObject3).putExtra((String)localObject1, (String)localObject2);
          }
          startActivity((Intent)localObject3);
          break;
        case 1: 
          localObject1 = "deepLinkFB";
          TruecallerInit.b(this, (String)localObject1);
          break;
        case 0: 
          label574:
          localObject1 = ((Uri)localObject2).getQueryParameter("c");
          localObject2 = ((Uri)localObject2).getQueryParameter("p");
          TruecallerInit.b(this, "deepLink");
          localObject3 = new com/truecaller/premium/data/SubscriptionPromoEventMetaData;
          localObject4 = UUID.randomUUID().toString();
          ((SubscriptionPromoEventMetaData)localObject3).<init>((String)localObject4, (String)localObject1);
          ((TrueApp)getApplicationContext()).a();
          localObject1 = PremiumPresenterView.LaunchContext.DEEP_LINK;
          br.b(this, (String)localObject2, (PremiumPresenterView.LaunchContext)localObject1, (SubscriptionPromoEventMetaData)localObject3);
        }
      }
    }
    finish();
  }
  
  private void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, boolean paramBoolean, String paramString7)
  {
    boolean bool1 = Settings.g();
    if (bool1)
    {
      bool1 = am.b(paramString1);
      if (!bool1)
      {
        localObject1 = paramString1.trim();
        int i = ((String)localObject1).length();
        int j = 7;
        if (i > j)
        {
          localObject1 = paramString1.trim();
          localObject2 = ((TrueApp)getApplicationContext()).a();
          localObject3 = k.f(this);
          localObject3 = aa.c((String)localObject1, (String)localObject3);
          localObject2 = ((bp)localObject2).aV().h((String)localObject3);
          boolean bool2 = c;
          if (bool2) {
            try
            {
              bool2 = am.b(paramString4);
              if (!bool2)
              {
                bool2 = am.b(paramString5);
                if (!bool2)
                {
                  bool2 = am.b(paramString3);
                  if (!bool2)
                  {
                    bool2 = am.b(paramString7);
                    if (!bool2)
                    {
                      localObject2 = c.a();
                      l = Long.parseLong((String)localObject1);
                      str = "deepLink";
                      localObject1 = localObject2;
                      localObject2 = this;
                      ((com.truecaller.flashsdk.core.b)localObject1).a(this, l, paramString2, str, paramString4, paramString5, paramString3, paramBoolean, paramString7);
                      return;
                    }
                  }
                }
              }
              localObject2 = c.a();
              long l = Long.parseLong((String)localObject1);
              str = "deepLink";
              localObject1 = localObject2;
              localObject2 = this;
              ((com.truecaller.flashsdk.core.b)localObject1).a(this, l, paramString2, str);
              return;
            }
            catch (NumberFormatException localNumberFormatException) {}
          }
        }
      }
      Object localObject1 = c.a();
      Object localObject2 = this;
      Object localObject3 = paramString4;
      String str = paramString6;
      localObject1 = ((com.truecaller.flashsdk.core.b)localObject1).a(this, paramString4, paramString5, paramString3, paramString6, paramBoolean, paramString7);
      localObject2 = aj.a(this);
      localObject3 = TruecallerInit.a(this, "flashShare");
      ((aj)localObject2).a((Intent)localObject3).a((Intent)localObject1).a();
      return;
    }
    Toast.makeText(this, 2131887959, 0).show();
  }
  
  private boolean a(Intent paramIntent)
  {
    Object localObject1 = paramIntent.getType();
    paramIntent = paramIntent.getData();
    boolean bool1 = am.b((CharSequence)localObject1);
    if ((!bool1) && (paramIntent != null))
    {
      Object localObject2 = paramIntent.toString();
      bool1 = am.b((CharSequence)localObject2);
      if (!bool1)
      {
        paramIntent = a(paramIntent);
        localObject2 = getString(2131888085);
        bool1 = am.a((CharSequence)localObject1, (CharSequence)localObject2);
        boolean bool3 = true;
        boolean bool4;
        if (bool1)
        {
          localObject1 = (CharSequence)a;
          bool4 = am.c((CharSequence)localObject1);
          if (bool4)
          {
            localObject1 = (CharSequence)b;
            bool4 = am.c((CharSequence)localObject1);
            if (bool4)
            {
              String str = ((String)a).trim();
              paramIntent = b;
              Object localObject3 = paramIntent;
              localObject3 = (String)paramIntent;
              a(str, (String)localObject3, null, null, null, null, false, null);
              return bool3;
            }
          }
        }
        else
        {
          localObject2 = getString(2131888510);
          bool1 = am.a((CharSequence)localObject1, (CharSequence)localObject2);
          int m = 10;
          if (bool1)
          {
            localObject1 = (CharSequence)a;
            bool4 = am.c((CharSequence)localObject1);
            if (bool4)
            {
              localObject1 = k.f(this);
              paramIntent = aa.c((String)a, (String)localObject1);
              int j = paramIntent.length();
              if (j >= m)
              {
                j = paramIntent.length() - m;
                paramIntent.substring(j);
              }
            }
            return bool3;
          }
          localObject2 = getString(2131888519);
          bool1 = am.a((CharSequence)localObject1, (CharSequence)localObject2);
          if (bool1)
          {
            localObject1 = TrueApp.y();
            boolean bool5 = ((TrueApp)localObject1).isTcPayEnabled();
            if (!bool5) {
              return false;
            }
            localObject1 = (CharSequence)a;
            bool5 = am.c((CharSequence)localObject1);
            if (bool5)
            {
              localObject1 = k.f(this);
              paramIntent = aa.c((String)a, (String)localObject1);
              int k = paramIntent.length();
              if (k >= m)
              {
                k = paramIntent.length() - m;
                paramIntent = paramIntent.substring(k);
              }
              k = 0;
              localObject1 = null;
              TransactionActivity.startForSend(this, paramIntent, null);
            }
            return bool3;
          }
          int i = 2131888509;
          localObject2 = getString(i);
          boolean bool6 = am.a((CharSequence)localObject1, (CharSequence)localObject2);
          if (bool6)
          {
            localObject1 = TrueApp.y();
            bool6 = ((TrueApp)localObject1).isTcPayEnabled();
            if (!bool6) {
              return false;
            }
            localObject1 = ((TrueApp)getApplicationContext()).a();
            localObject2 = (CharSequence)a;
            boolean bool2 = am.c((CharSequence)localObject2);
            if (bool2)
            {
              localObject1 = ((bp)localObject1).br();
              localObject2 = (String)a;
              localObject1 = (Contact)aa;
              if (localObject1 != null)
              {
                paramIntent = (String)a;
                localObject1 = ((Contact)localObject1).t();
                TransactionActivity.startForRequest(this, paramIntent, (String)localObject1);
                return bool3;
              }
            }
          }
        }
        return false;
      }
    }
    return false;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a();
  }
  
  protected void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    setIntent(paramIntent);
    a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.deeplink.DeepLinkHandlerActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */