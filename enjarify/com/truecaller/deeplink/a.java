package com.truecaller.deeplink;

import com.airbnb.deeplinkdispatch.DeepLinkEntry;
import com.airbnb.deeplinkdispatch.DeepLinkEntry.Type;
import com.airbnb.deeplinkdispatch.Parser;
import com.truecaller.ads.leadgen.LeadgenDeeplink;
import com.truecaller.swish.SwishResultActivity;
import com.truecaller.ui.TruecallerInit;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class a
  implements Parser
{
  public static final List a;
  
  static
  {
    DeepLinkEntry[] arrayOfDeepLinkEntry = new DeepLinkEntry[5];
    DeepLinkEntry localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    DeepLinkEntry.Type localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://home/{view}/{subview}", localType, TruecallerInit.class, null);
    arrayOfDeepLinkEntry[0] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://balance_check", localType, TruecallerInit.class, null);
    arrayOfDeepLinkEntry[1] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://swish", localType, SwishResultActivity.class, null);
    arrayOfDeepLinkEntry[2] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://home/{view}", localType, TruecallerInit.class, null);
    arrayOfDeepLinkEntry[3] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.METHOD;
    localDeepLinkEntry.<init>("truecaller://leadgen/{leadgenId}", localType, LeadgenDeeplink.class, "createDeeplink");
    arrayOfDeepLinkEntry[4] = localDeepLinkEntry;
    a = Collections.unmodifiableList(Arrays.asList(arrayOfDeepLinkEntry));
  }
  
  public final DeepLinkEntry parseUri(String paramString)
  {
    Iterator localIterator = a.iterator();
    DeepLinkEntry localDeepLinkEntry;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localDeepLinkEntry = (DeepLinkEntry)localIterator.next();
      bool2 = localDeepLinkEntry.matches(paramString);
    } while (!bool2);
    return localDeepLinkEntry;
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.deeplink.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */