package com.truecaller.deeplink;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.d;
import com.airbnb.deeplinkdispatch.DeepLinkEntry;
import com.airbnb.deeplinkdispatch.DeepLinkResult;
import com.airbnb.deeplinkdispatch.Parser;
import com.truecaller.sdk.ai;
import com.truecaller.truepay.app.deeplink.TcPaySdkDeeplinkModuleLoader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public final class b
{
  private static final String a = "b";
  private final List b;
  
  public b(a parama, TcPaySdkDeeplinkModuleLoader paramTcPaySdkDeeplinkModuleLoader, ai paramai, com.truecaller.credit.app.b.a parama1)
  {
    Parser[] arrayOfParser = new Parser[4];
    arrayOfParser[0] = parama;
    arrayOfParser[1] = paramTcPaySdkDeeplinkModuleLoader;
    arrayOfParser[2] = paramai;
    arrayOfParser[3] = parama1;
    parama = Arrays.asList(arrayOfParser);
    b = parama;
  }
  
  private DeepLinkEntry a(String paramString)
  {
    Iterator localIterator = b.iterator();
    DeepLinkEntry localDeepLinkEntry;
    do
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localDeepLinkEntry = ((Parser)localIterator.next()).parseUri(paramString);
    } while (localDeepLinkEntry == null);
    return localDeepLinkEntry;
    return null;
  }
  
  /* Error */
  private DeepLinkResult a(Activity paramActivity, Intent paramIntent)
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +1154 -> 1155
    //   4: aload_2
    //   5: ifnull +1138 -> 1143
    //   8: aload_2
    //   9: invokevirtual 54	android/content/Intent:getData	()Landroid/net/Uri;
    //   12: astore_3
    //   13: aconst_null
    //   14: astore 4
    //   16: aload_3
    //   17: ifnonnull +12 -> 29
    //   20: aload_1
    //   21: iconst_0
    //   22: aconst_null
    //   23: ldc 56
    //   25: invokestatic 59	com/truecaller/deeplink/b:a	(Landroid/content/Context;ZLandroid/net/Uri;Ljava/lang/String;)Lcom/airbnb/deeplinkdispatch/DeepLinkResult;
    //   28: areturn
    //   29: aload_3
    //   30: invokevirtual 65	android/net/Uri:toString	()Ljava/lang/String;
    //   33: astore 5
    //   35: aload_0
    //   36: aload 5
    //   38: invokespecial 67	com/truecaller/deeplink/b:a	(Ljava/lang/String;)Lcom/airbnb/deeplinkdispatch/DeepLinkEntry;
    //   41: astore 6
    //   43: aload 6
    //   45: ifnull +1062 -> 1107
    //   48: aload 5
    //   50: invokestatic 73	com/airbnb/deeplinkdispatch/DeepLinkUri:parse	(Ljava/lang/String;)Lcom/airbnb/deeplinkdispatch/DeepLinkUri;
    //   53: astore 7
    //   55: aload 6
    //   57: aload 5
    //   59: invokevirtual 79	com/airbnb/deeplinkdispatch/DeepLinkEntry:getParameters	(Ljava/lang/String;)Ljava/util/Map;
    //   62: astore 5
    //   64: aload 7
    //   66: invokevirtual 83	com/airbnb/deeplinkdispatch/DeepLinkUri:queryParameterNames	()Ljava/util/Set;
    //   69: invokeinterface 86 1 0
    //   74: astore 8
    //   76: aload 8
    //   78: invokeinterface 40 1 0
    //   83: istore 9
    //   85: iload 9
    //   87: ifeq +80 -> 167
    //   90: aload 8
    //   92: invokeinterface 44 1 0
    //   97: checkcast 88	java/lang/String
    //   100: astore 10
    //   102: aload 7
    //   104: aload 10
    //   106: invokevirtual 92	com/airbnb/deeplinkdispatch/DeepLinkUri:queryParameterValues	(Ljava/lang/String;)Ljava/util/List;
    //   109: invokeinterface 34 1 0
    //   114: astore 11
    //   116: aload 11
    //   118: invokeinterface 40 1 0
    //   123: istore 12
    //   125: iload 12
    //   127: ifeq -51 -> 76
    //   130: aload 11
    //   132: invokeinterface 44 1 0
    //   137: checkcast 88	java/lang/String
    //   140: astore 13
    //   142: aload 5
    //   144: aload 10
    //   146: invokeinterface 98 2 0
    //   151: pop
    //   152: aload 5
    //   154: aload 10
    //   156: aload 13
    //   158: invokeinterface 102 3 0
    //   163: pop
    //   164: goto -48 -> 116
    //   167: aload_3
    //   168: invokevirtual 65	android/net/Uri:toString	()Ljava/lang/String;
    //   171: astore 8
    //   173: aload 5
    //   175: ldc 104
    //   177: aload 8
    //   179: invokeinterface 102 3 0
    //   184: pop
    //   185: aload_2
    //   186: invokevirtual 108	android/content/Intent:getExtras	()Landroid/os/Bundle;
    //   189: astore 7
    //   191: aload 7
    //   193: ifnull +24 -> 217
    //   196: new 110	android/os/Bundle
    //   199: astore 7
    //   201: aload_2
    //   202: invokevirtual 108	android/content/Intent:getExtras	()Landroid/os/Bundle;
    //   205: astore 8
    //   207: aload 7
    //   209: aload 8
    //   211: invokespecial 113	android/os/Bundle:<init>	(Landroid/os/Bundle;)V
    //   214: goto +13 -> 227
    //   217: new 110	android/os/Bundle
    //   220: astore 7
    //   222: aload 7
    //   224: invokespecial 114	android/os/Bundle:<init>	()V
    //   227: aload 5
    //   229: invokeinterface 117 1 0
    //   234: invokeinterface 86 1 0
    //   239: astore 5
    //   241: aload 5
    //   243: invokeinterface 40 1 0
    //   248: istore 14
    //   250: iload 14
    //   252: ifeq +51 -> 303
    //   255: aload 5
    //   257: invokeinterface 44 1 0
    //   262: checkcast 119	java/util/Map$Entry
    //   265: astore 8
    //   267: aload 8
    //   269: invokeinterface 122 1 0
    //   274: checkcast 88	java/lang/String
    //   277: astore 10
    //   279: aload 8
    //   281: invokeinterface 125 1 0
    //   286: checkcast 88	java/lang/String
    //   289: astore 8
    //   291: aload 7
    //   293: aload 10
    //   295: aload 8
    //   297: invokevirtual 129	android/os/Bundle:putString	(Ljava/lang/String;Ljava/lang/String;)V
    //   300: goto -59 -> 241
    //   303: aload 6
    //   305: invokevirtual 133	com/airbnb/deeplinkdispatch/DeepLinkEntry:getActivityClass	()Ljava/lang/Class;
    //   308: astore 5
    //   310: aload 6
    //   312: invokevirtual 137	com/airbnb/deeplinkdispatch/DeepLinkEntry:getType	()Lcom/airbnb/deeplinkdispatch/DeepLinkEntry$Type;
    //   315: astore 8
    //   317: getstatic 143	com/airbnb/deeplinkdispatch/DeepLinkEntry$Type:CLASS	Lcom/airbnb/deeplinkdispatch/DeepLinkEntry$Type;
    //   320: astore 10
    //   322: iconst_1
    //   323: istore 15
    //   325: aload 8
    //   327: aload 10
    //   329: if_acmpne +22 -> 351
    //   332: new 50	android/content/Intent
    //   335: astore 8
    //   337: aload 8
    //   339: aload_1
    //   340: aload 5
    //   342: invokespecial 146	android/content/Intent:<init>	(Landroid/content/Context;Ljava/lang/Class;)V
    //   345: aconst_null
    //   346: astore 5
    //   348: goto +515 -> 863
    //   351: aload 6
    //   353: invokevirtual 149	com/airbnb/deeplinkdispatch/DeepLinkEntry:getMethod	()Ljava/lang/String;
    //   356: astore 8
    //   358: iload 15
    //   360: anewarray 151	java/lang/Class
    //   363: astore 10
    //   365: ldc -103
    //   367: astore 13
    //   369: aload 10
    //   371: iconst_0
    //   372: aload 13
    //   374: aastore
    //   375: aload 5
    //   377: aload 8
    //   379: aload 10
    //   381: invokevirtual 156	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   384: astore 8
    //   386: aload 8
    //   388: invokevirtual 161	java/lang/reflect/Method:getReturnType	()Ljava/lang/Class;
    //   391: astore 10
    //   393: ldc -93
    //   395: astore 13
    //   397: aload 10
    //   399: aload 13
    //   401: invokevirtual 166	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   404: istore 9
    //   406: iload 9
    //   408: ifeq +152 -> 560
    //   411: iload 15
    //   413: anewarray 4	java/lang/Object
    //   416: astore 10
    //   418: aload 10
    //   420: iconst_0
    //   421: aload_1
    //   422: aastore
    //   423: aload 8
    //   425: aload 5
    //   427: aload 10
    //   429: invokevirtual 170	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   432: astore 8
    //   434: aload 8
    //   436: checkcast 163	android/support/v4/app/aj
    //   439: astore 8
    //   441: aload 8
    //   443: getfield 173	android/support/v4/app/aj:a	Ljava/util/ArrayList;
    //   446: astore 10
    //   448: aload 10
    //   450: invokevirtual 179	java/util/ArrayList:size	()I
    //   453: istore 9
    //   455: iload 9
    //   457: ifne +62 -> 519
    //   460: new 181	java/lang/StringBuilder
    //   463: astore 10
    //   465: ldc -73
    //   467: astore 13
    //   469: aload 10
    //   471: aload 13
    //   473: invokespecial 186	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   476: aload 6
    //   478: invokevirtual 149	com/airbnb/deeplinkdispatch/DeepLinkEntry:getMethod	()Ljava/lang/String;
    //   481: astore 13
    //   483: aload 10
    //   485: aload 13
    //   487: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   490: pop
    //   491: ldc -64
    //   493: astore 13
    //   495: aload 10
    //   497: aload 13
    //   499: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   502: pop
    //   503: aload 10
    //   505: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   508: astore 10
    //   510: aload_1
    //   511: iconst_0
    //   512: aload_3
    //   513: aload 10
    //   515: invokestatic 59	com/truecaller/deeplink/b:a	(Landroid/content/Context;ZLandroid/net/Uri;Ljava/lang/String;)Lcom/airbnb/deeplinkdispatch/DeepLinkResult;
    //   518: areturn
    //   519: aload 8
    //   521: getfield 173	android/support/v4/app/aj:a	Ljava/util/ArrayList;
    //   524: astore 10
    //   526: aload 10
    //   528: invokevirtual 179	java/util/ArrayList:size	()I
    //   531: iload 15
    //   533: isub
    //   534: istore 9
    //   536: aload 8
    //   538: iload 9
    //   540: invokevirtual 196	android/support/v4/app/aj:a	(I)Landroid/content/Intent;
    //   543: astore 5
    //   545: aload 8
    //   547: astore 16
    //   549: aload 5
    //   551: astore 8
    //   553: aload 16
    //   555: astore 5
    //   557: goto +306 -> 863
    //   560: iload 15
    //   562: anewarray 4	java/lang/Object
    //   565: astore 10
    //   567: aload 10
    //   569: iconst_0
    //   570: aload_1
    //   571: aastore
    //   572: aload 8
    //   574: aload 5
    //   576: aload 10
    //   578: invokevirtual 170	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   581: astore 8
    //   583: aload 8
    //   585: checkcast 50	android/content/Intent
    //   588: astore 8
    //   590: aconst_null
    //   591: astore 5
    //   593: goto +270 -> 863
    //   596: pop
    //   597: iconst_0
    //   598: istore 14
    //   600: aconst_null
    //   601: astore 8
    //   603: aload 6
    //   605: invokevirtual 149	com/airbnb/deeplinkdispatch/DeepLinkEntry:getMethod	()Ljava/lang/String;
    //   608: astore 10
    //   610: iconst_2
    //   611: istore 12
    //   613: iload 12
    //   615: anewarray 151	java/lang/Class
    //   618: astore 17
    //   620: ldc -103
    //   622: astore 18
    //   624: aload 17
    //   626: iconst_0
    //   627: aload 18
    //   629: aastore
    //   630: ldc 110
    //   632: astore 18
    //   634: aload 17
    //   636: iload 15
    //   638: aload 18
    //   640: aastore
    //   641: aload 5
    //   643: aload 10
    //   645: aload 17
    //   647: invokevirtual 156	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   650: astore 10
    //   652: aload 10
    //   654: invokevirtual 161	java/lang/reflect/Method:getReturnType	()Ljava/lang/Class;
    //   657: astore 17
    //   659: ldc -93
    //   661: astore 18
    //   663: aload 17
    //   665: aload 18
    //   667: invokevirtual 166	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   670: istore 19
    //   672: iload 19
    //   674: ifeq +140 -> 814
    //   677: iload 12
    //   679: anewarray 4	java/lang/Object
    //   682: astore 8
    //   684: aload 8
    //   686: iconst_0
    //   687: aload_1
    //   688: aastore
    //   689: aload 8
    //   691: iload 15
    //   693: aload 7
    //   695: aastore
    //   696: aload 10
    //   698: aload 5
    //   700: aload 8
    //   702: invokevirtual 170	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   705: astore 5
    //   707: aload 5
    //   709: checkcast 163	android/support/v4/app/aj
    //   712: astore 5
    //   714: aload 5
    //   716: getfield 173	android/support/v4/app/aj:a	Ljava/util/ArrayList;
    //   719: astore 8
    //   721: aload 8
    //   723: invokevirtual 179	java/util/ArrayList:size	()I
    //   726: istore 14
    //   728: iload 14
    //   730: ifne +55 -> 785
    //   733: new 181	java/lang/StringBuilder
    //   736: astore_2
    //   737: ldc -73
    //   739: astore 4
    //   741: aload_2
    //   742: aload 4
    //   744: invokespecial 186	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   747: aload 6
    //   749: invokevirtual 149	com/airbnb/deeplinkdispatch/DeepLinkEntry:getMethod	()Ljava/lang/String;
    //   752: astore 4
    //   754: aload_2
    //   755: aload 4
    //   757: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   760: pop
    //   761: ldc -64
    //   763: astore 4
    //   765: aload_2
    //   766: aload 4
    //   768: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   771: pop
    //   772: aload_2
    //   773: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   776: astore_2
    //   777: aload_1
    //   778: iconst_0
    //   779: aload_3
    //   780: aload_2
    //   781: invokestatic 59	com/truecaller/deeplink/b:a	(Landroid/content/Context;ZLandroid/net/Uri;Ljava/lang/String;)Lcom/airbnb/deeplinkdispatch/DeepLinkResult;
    //   784: areturn
    //   785: aload 5
    //   787: getfield 173	android/support/v4/app/aj:a	Ljava/util/ArrayList;
    //   790: astore 8
    //   792: aload 8
    //   794: invokevirtual 179	java/util/ArrayList:size	()I
    //   797: iload 15
    //   799: isub
    //   800: istore 14
    //   802: aload 5
    //   804: iload 14
    //   806: invokevirtual 196	android/support/v4/app/aj:a	(I)Landroid/content/Intent;
    //   809: astore 8
    //   811: goto +52 -> 863
    //   814: iload 12
    //   816: anewarray 4	java/lang/Object
    //   819: astore 13
    //   821: aload 13
    //   823: iconst_0
    //   824: aload_1
    //   825: aastore
    //   826: aload 13
    //   828: iload 15
    //   830: aload 7
    //   832: aastore
    //   833: aload 10
    //   835: aload 5
    //   837: aload 13
    //   839: invokevirtual 170	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   842: astore 5
    //   844: aload 5
    //   846: checkcast 50	android/content/Intent
    //   849: astore 5
    //   851: aload 8
    //   853: astore 16
    //   855: aload 5
    //   857: astore 8
    //   859: aload 16
    //   861: astore 5
    //   863: aload 8
    //   865: invokevirtual 199	android/content/Intent:getAction	()Ljava/lang/String;
    //   868: astore 10
    //   870: aload 10
    //   872: ifnonnull +17 -> 889
    //   875: aload_2
    //   876: invokevirtual 199	android/content/Intent:getAction	()Ljava/lang/String;
    //   879: astore 10
    //   881: aload 8
    //   883: aload 10
    //   885: invokevirtual 203	android/content/Intent:setAction	(Ljava/lang/String;)Landroid/content/Intent;
    //   888: pop
    //   889: aload 8
    //   891: invokevirtual 54	android/content/Intent:getData	()Landroid/net/Uri;
    //   894: astore 10
    //   896: aload 10
    //   898: ifnonnull +15 -> 913
    //   901: aload_2
    //   902: invokevirtual 54	android/content/Intent:getData	()Landroid/net/Uri;
    //   905: astore_2
    //   906: aload 8
    //   908: aload_2
    //   909: invokevirtual 207	android/content/Intent:setData	(Landroid/net/Uri;)Landroid/content/Intent;
    //   912: pop
    //   913: aload 8
    //   915: aload 7
    //   917: invokevirtual 211	android/content/Intent:putExtras	(Landroid/os/Bundle;)Landroid/content/Intent;
    //   920: pop
    //   921: ldc -43
    //   923: astore_2
    //   924: aload 8
    //   926: aload_2
    //   927: iload 15
    //   929: invokevirtual 217	android/content/Intent:putExtra	(Ljava/lang/String;Z)Landroid/content/Intent;
    //   932: pop
    //   933: ldc -37
    //   935: astore_2
    //   936: aload 8
    //   938: aload_2
    //   939: aload_3
    //   940: invokevirtual 222	android/content/Intent:putExtra	(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    //   943: pop
    //   944: aload_1
    //   945: invokevirtual 228	android/app/Activity:getCallingActivity	()Landroid/content/ComponentName;
    //   948: astore_2
    //   949: aload_2
    //   950: ifnull +15 -> 965
    //   953: ldc -27
    //   955: istore 20
    //   957: aload 8
    //   959: iload 20
    //   961: invokevirtual 233	android/content/Intent:setFlags	(I)Landroid/content/Intent;
    //   964: pop
    //   965: aload 5
    //   967: ifnull +11 -> 978
    //   970: aload 5
    //   972: invokevirtual 235	android/support/v4/app/aj:a	()V
    //   975: goto +9 -> 984
    //   978: aload_1
    //   979: aload 8
    //   981: invokevirtual 239	android/app/Activity:startActivity	(Landroid/content/Intent;)V
    //   984: aload_1
    //   985: iload 15
    //   987: aload_3
    //   988: aconst_null
    //   989: invokestatic 59	com/truecaller/deeplink/b:a	(Landroid/content/Context;ZLandroid/net/Uri;Ljava/lang/String;)Lcom/airbnb/deeplinkdispatch/DeepLinkResult;
    //   992: areturn
    //   993: pop
    //   994: new 181	java/lang/StringBuilder
    //   997: astore_2
    //   998: aload_2
    //   999: ldc -73
    //   1001: invokespecial 186	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1004: aload 6
    //   1006: invokevirtual 149	com/airbnb/deeplinkdispatch/DeepLinkEntry:getMethod	()Ljava/lang/String;
    //   1009: astore 4
    //   1011: aload_2
    //   1012: aload 4
    //   1014: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1017: pop
    //   1018: aload_2
    //   1019: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1022: astore_2
    //   1023: aload_1
    //   1024: iconst_0
    //   1025: aload_3
    //   1026: aload_2
    //   1027: invokestatic 59	com/truecaller/deeplink/b:a	(Landroid/content/Context;ZLandroid/net/Uri;Ljava/lang/String;)Lcom/airbnb/deeplinkdispatch/DeepLinkResult;
    //   1030: areturn
    //   1031: pop
    //   1032: new 181	java/lang/StringBuilder
    //   1035: astore_2
    //   1036: aload_2
    //   1037: ldc -73
    //   1039: invokespecial 186	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1042: aload 6
    //   1044: invokevirtual 149	com/airbnb/deeplinkdispatch/DeepLinkEntry:getMethod	()Ljava/lang/String;
    //   1047: astore 4
    //   1049: aload_2
    //   1050: aload 4
    //   1052: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1055: pop
    //   1056: aload_2
    //   1057: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1060: astore_2
    //   1061: aload_1
    //   1062: iconst_0
    //   1063: aload_3
    //   1064: aload_2
    //   1065: invokestatic 59	com/truecaller/deeplink/b:a	(Landroid/content/Context;ZLandroid/net/Uri;Ljava/lang/String;)Lcom/airbnb/deeplinkdispatch/DeepLinkResult;
    //   1068: areturn
    //   1069: pop
    //   1070: new 181	java/lang/StringBuilder
    //   1073: astore_2
    //   1074: aload_2
    //   1075: ldc -15
    //   1077: invokespecial 186	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1080: aload 6
    //   1082: invokevirtual 149	com/airbnb/deeplinkdispatch/DeepLinkEntry:getMethod	()Ljava/lang/String;
    //   1085: astore 4
    //   1087: aload_2
    //   1088: aload 4
    //   1090: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1093: pop
    //   1094: aload_2
    //   1095: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1098: astore_2
    //   1099: aload_1
    //   1100: iconst_0
    //   1101: aload_3
    //   1102: aload_2
    //   1103: invokestatic 59	com/truecaller/deeplink/b:a	(Landroid/content/Context;ZLandroid/net/Uri;Ljava/lang/String;)Lcom/airbnb/deeplinkdispatch/DeepLinkResult;
    //   1106: areturn
    //   1107: new 181	java/lang/StringBuilder
    //   1110: astore_2
    //   1111: aload_2
    //   1112: ldc -13
    //   1114: invokespecial 186	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1117: aload_3
    //   1118: invokevirtual 65	android/net/Uri:toString	()Ljava/lang/String;
    //   1121: astore 4
    //   1123: aload_2
    //   1124: aload 4
    //   1126: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1129: pop
    //   1130: aload_2
    //   1131: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1134: astore_2
    //   1135: aload_1
    //   1136: iconst_0
    //   1137: aload_3
    //   1138: aload_2
    //   1139: invokestatic 59	com/truecaller/deeplink/b:a	(Landroid/content/Context;ZLandroid/net/Uri;Ljava/lang/String;)Lcom/airbnb/deeplinkdispatch/DeepLinkResult;
    //   1142: areturn
    //   1143: new 245	java/lang/NullPointerException
    //   1146: astore_1
    //   1147: aload_1
    //   1148: ldc -9
    //   1150: invokespecial 248	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
    //   1153: aload_1
    //   1154: athrow
    //   1155: new 245	java/lang/NullPointerException
    //   1158: astore_1
    //   1159: aload_1
    //   1160: ldc -6
    //   1162: invokespecial 248	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
    //   1165: aload_1
    //   1166: athrow
    //   1167: pop
    //   1168: goto -565 -> 603
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1171	0	this	b
    //   0	1171	1	paramActivity	Activity
    //   0	1171	2	paramIntent	Intent
    //   12	1126	3	localUri	Uri
    //   14	1111	4	str	String
    //   33	938	5	localObject1	Object
    //   41	1040	6	localDeepLinkEntry	DeepLinkEntry
    //   53	863	7	localObject2	Object
    //   74	906	8	localObject3	Object
    //   83	324	9	bool1	boolean
    //   453	86	9	i	int
    //   100	797	10	localObject4	Object
    //   114	17	11	localIterator	Iterator
    //   123	3	12	bool2	boolean
    //   611	204	12	j	int
    //   140	698	13	localObject5	Object
    //   248	351	14	bool3	boolean
    //   726	79	14	k	int
    //   323	663	15	m	int
    //   547	313	16	localObject6	Object
    //   618	46	17	localObject7	Object
    //   622	44	18	localClass	Class
    //   670	3	19	bool4	boolean
    //   955	5	20	n	int
    //   596	1	24	localNoSuchMethodException1	NoSuchMethodException
    //   993	1	25	localInvocationTargetException	java.lang.reflect.InvocationTargetException
    //   1031	1	26	localIllegalAccessException	IllegalAccessException
    //   1069	1	27	localNoSuchMethodException2	NoSuchMethodException
    //   1167	1	28	localNoSuchMethodException3	NoSuchMethodException
    // Exception table:
    //   from	to	target	type
    //   351	356	596	java/lang/NoSuchMethodException
    //   358	363	596	java/lang/NoSuchMethodException
    //   372	375	596	java/lang/NoSuchMethodException
    //   379	384	596	java/lang/NoSuchMethodException
    //   386	391	596	java/lang/NoSuchMethodException
    //   399	404	596	java/lang/NoSuchMethodException
    //   411	416	596	java/lang/NoSuchMethodException
    //   421	423	596	java/lang/NoSuchMethodException
    //   427	432	596	java/lang/NoSuchMethodException
    //   434	439	596	java/lang/NoSuchMethodException
    //   560	565	596	java/lang/NoSuchMethodException
    //   570	572	596	java/lang/NoSuchMethodException
    //   576	581	596	java/lang/NoSuchMethodException
    //   583	588	596	java/lang/NoSuchMethodException
    //   303	308	993	java/lang/reflect/InvocationTargetException
    //   310	315	993	java/lang/reflect/InvocationTargetException
    //   317	320	993	java/lang/reflect/InvocationTargetException
    //   332	335	993	java/lang/reflect/InvocationTargetException
    //   340	345	993	java/lang/reflect/InvocationTargetException
    //   351	356	993	java/lang/reflect/InvocationTargetException
    //   358	363	993	java/lang/reflect/InvocationTargetException
    //   372	375	993	java/lang/reflect/InvocationTargetException
    //   379	384	993	java/lang/reflect/InvocationTargetException
    //   386	391	993	java/lang/reflect/InvocationTargetException
    //   399	404	993	java/lang/reflect/InvocationTargetException
    //   411	416	993	java/lang/reflect/InvocationTargetException
    //   421	423	993	java/lang/reflect/InvocationTargetException
    //   427	432	993	java/lang/reflect/InvocationTargetException
    //   434	439	993	java/lang/reflect/InvocationTargetException
    //   441	446	993	java/lang/reflect/InvocationTargetException
    //   448	453	993	java/lang/reflect/InvocationTargetException
    //   460	463	993	java/lang/reflect/InvocationTargetException
    //   471	476	993	java/lang/reflect/InvocationTargetException
    //   476	481	993	java/lang/reflect/InvocationTargetException
    //   485	491	993	java/lang/reflect/InvocationTargetException
    //   497	503	993	java/lang/reflect/InvocationTargetException
    //   503	508	993	java/lang/reflect/InvocationTargetException
    //   513	518	993	java/lang/reflect/InvocationTargetException
    //   519	524	993	java/lang/reflect/InvocationTargetException
    //   526	531	993	java/lang/reflect/InvocationTargetException
    //   538	543	993	java/lang/reflect/InvocationTargetException
    //   560	565	993	java/lang/reflect/InvocationTargetException
    //   570	572	993	java/lang/reflect/InvocationTargetException
    //   576	581	993	java/lang/reflect/InvocationTargetException
    //   583	588	993	java/lang/reflect/InvocationTargetException
    //   603	608	993	java/lang/reflect/InvocationTargetException
    //   613	618	993	java/lang/reflect/InvocationTargetException
    //   627	630	993	java/lang/reflect/InvocationTargetException
    //   638	641	993	java/lang/reflect/InvocationTargetException
    //   645	650	993	java/lang/reflect/InvocationTargetException
    //   652	657	993	java/lang/reflect/InvocationTargetException
    //   665	670	993	java/lang/reflect/InvocationTargetException
    //   677	682	993	java/lang/reflect/InvocationTargetException
    //   687	689	993	java/lang/reflect/InvocationTargetException
    //   693	696	993	java/lang/reflect/InvocationTargetException
    //   700	705	993	java/lang/reflect/InvocationTargetException
    //   707	712	993	java/lang/reflect/InvocationTargetException
    //   714	719	993	java/lang/reflect/InvocationTargetException
    //   721	726	993	java/lang/reflect/InvocationTargetException
    //   733	736	993	java/lang/reflect/InvocationTargetException
    //   742	747	993	java/lang/reflect/InvocationTargetException
    //   747	752	993	java/lang/reflect/InvocationTargetException
    //   755	761	993	java/lang/reflect/InvocationTargetException
    //   766	772	993	java/lang/reflect/InvocationTargetException
    //   772	776	993	java/lang/reflect/InvocationTargetException
    //   780	784	993	java/lang/reflect/InvocationTargetException
    //   785	790	993	java/lang/reflect/InvocationTargetException
    //   792	797	993	java/lang/reflect/InvocationTargetException
    //   804	809	993	java/lang/reflect/InvocationTargetException
    //   814	819	993	java/lang/reflect/InvocationTargetException
    //   824	826	993	java/lang/reflect/InvocationTargetException
    //   830	833	993	java/lang/reflect/InvocationTargetException
    //   837	842	993	java/lang/reflect/InvocationTargetException
    //   844	849	993	java/lang/reflect/InvocationTargetException
    //   863	868	993	java/lang/reflect/InvocationTargetException
    //   875	879	993	java/lang/reflect/InvocationTargetException
    //   883	889	993	java/lang/reflect/InvocationTargetException
    //   889	894	993	java/lang/reflect/InvocationTargetException
    //   901	905	993	java/lang/reflect/InvocationTargetException
    //   908	913	993	java/lang/reflect/InvocationTargetException
    //   915	921	993	java/lang/reflect/InvocationTargetException
    //   927	933	993	java/lang/reflect/InvocationTargetException
    //   939	944	993	java/lang/reflect/InvocationTargetException
    //   944	948	993	java/lang/reflect/InvocationTargetException
    //   959	965	993	java/lang/reflect/InvocationTargetException
    //   970	975	993	java/lang/reflect/InvocationTargetException
    //   979	984	993	java/lang/reflect/InvocationTargetException
    //   988	992	993	java/lang/reflect/InvocationTargetException
    //   303	308	1031	java/lang/IllegalAccessException
    //   310	315	1031	java/lang/IllegalAccessException
    //   317	320	1031	java/lang/IllegalAccessException
    //   332	335	1031	java/lang/IllegalAccessException
    //   340	345	1031	java/lang/IllegalAccessException
    //   351	356	1031	java/lang/IllegalAccessException
    //   358	363	1031	java/lang/IllegalAccessException
    //   372	375	1031	java/lang/IllegalAccessException
    //   379	384	1031	java/lang/IllegalAccessException
    //   386	391	1031	java/lang/IllegalAccessException
    //   399	404	1031	java/lang/IllegalAccessException
    //   411	416	1031	java/lang/IllegalAccessException
    //   421	423	1031	java/lang/IllegalAccessException
    //   427	432	1031	java/lang/IllegalAccessException
    //   434	439	1031	java/lang/IllegalAccessException
    //   441	446	1031	java/lang/IllegalAccessException
    //   448	453	1031	java/lang/IllegalAccessException
    //   460	463	1031	java/lang/IllegalAccessException
    //   471	476	1031	java/lang/IllegalAccessException
    //   476	481	1031	java/lang/IllegalAccessException
    //   485	491	1031	java/lang/IllegalAccessException
    //   497	503	1031	java/lang/IllegalAccessException
    //   503	508	1031	java/lang/IllegalAccessException
    //   513	518	1031	java/lang/IllegalAccessException
    //   519	524	1031	java/lang/IllegalAccessException
    //   526	531	1031	java/lang/IllegalAccessException
    //   538	543	1031	java/lang/IllegalAccessException
    //   560	565	1031	java/lang/IllegalAccessException
    //   570	572	1031	java/lang/IllegalAccessException
    //   576	581	1031	java/lang/IllegalAccessException
    //   583	588	1031	java/lang/IllegalAccessException
    //   603	608	1031	java/lang/IllegalAccessException
    //   613	618	1031	java/lang/IllegalAccessException
    //   627	630	1031	java/lang/IllegalAccessException
    //   638	641	1031	java/lang/IllegalAccessException
    //   645	650	1031	java/lang/IllegalAccessException
    //   652	657	1031	java/lang/IllegalAccessException
    //   665	670	1031	java/lang/IllegalAccessException
    //   677	682	1031	java/lang/IllegalAccessException
    //   687	689	1031	java/lang/IllegalAccessException
    //   693	696	1031	java/lang/IllegalAccessException
    //   700	705	1031	java/lang/IllegalAccessException
    //   707	712	1031	java/lang/IllegalAccessException
    //   714	719	1031	java/lang/IllegalAccessException
    //   721	726	1031	java/lang/IllegalAccessException
    //   733	736	1031	java/lang/IllegalAccessException
    //   742	747	1031	java/lang/IllegalAccessException
    //   747	752	1031	java/lang/IllegalAccessException
    //   755	761	1031	java/lang/IllegalAccessException
    //   766	772	1031	java/lang/IllegalAccessException
    //   772	776	1031	java/lang/IllegalAccessException
    //   780	784	1031	java/lang/IllegalAccessException
    //   785	790	1031	java/lang/IllegalAccessException
    //   792	797	1031	java/lang/IllegalAccessException
    //   804	809	1031	java/lang/IllegalAccessException
    //   814	819	1031	java/lang/IllegalAccessException
    //   824	826	1031	java/lang/IllegalAccessException
    //   830	833	1031	java/lang/IllegalAccessException
    //   837	842	1031	java/lang/IllegalAccessException
    //   844	849	1031	java/lang/IllegalAccessException
    //   863	868	1031	java/lang/IllegalAccessException
    //   875	879	1031	java/lang/IllegalAccessException
    //   883	889	1031	java/lang/IllegalAccessException
    //   889	894	1031	java/lang/IllegalAccessException
    //   901	905	1031	java/lang/IllegalAccessException
    //   908	913	1031	java/lang/IllegalAccessException
    //   915	921	1031	java/lang/IllegalAccessException
    //   927	933	1031	java/lang/IllegalAccessException
    //   939	944	1031	java/lang/IllegalAccessException
    //   944	948	1031	java/lang/IllegalAccessException
    //   959	965	1031	java/lang/IllegalAccessException
    //   970	975	1031	java/lang/IllegalAccessException
    //   979	984	1031	java/lang/IllegalAccessException
    //   988	992	1031	java/lang/IllegalAccessException
    //   303	308	1069	java/lang/NoSuchMethodException
    //   310	315	1069	java/lang/NoSuchMethodException
    //   317	320	1069	java/lang/NoSuchMethodException
    //   332	335	1069	java/lang/NoSuchMethodException
    //   340	345	1069	java/lang/NoSuchMethodException
    //   603	608	1069	java/lang/NoSuchMethodException
    //   613	618	1069	java/lang/NoSuchMethodException
    //   627	630	1069	java/lang/NoSuchMethodException
    //   638	641	1069	java/lang/NoSuchMethodException
    //   645	650	1069	java/lang/NoSuchMethodException
    //   652	657	1069	java/lang/NoSuchMethodException
    //   665	670	1069	java/lang/NoSuchMethodException
    //   677	682	1069	java/lang/NoSuchMethodException
    //   687	689	1069	java/lang/NoSuchMethodException
    //   693	696	1069	java/lang/NoSuchMethodException
    //   700	705	1069	java/lang/NoSuchMethodException
    //   707	712	1069	java/lang/NoSuchMethodException
    //   714	719	1069	java/lang/NoSuchMethodException
    //   721	726	1069	java/lang/NoSuchMethodException
    //   733	736	1069	java/lang/NoSuchMethodException
    //   742	747	1069	java/lang/NoSuchMethodException
    //   747	752	1069	java/lang/NoSuchMethodException
    //   755	761	1069	java/lang/NoSuchMethodException
    //   766	772	1069	java/lang/NoSuchMethodException
    //   772	776	1069	java/lang/NoSuchMethodException
    //   780	784	1069	java/lang/NoSuchMethodException
    //   785	790	1069	java/lang/NoSuchMethodException
    //   792	797	1069	java/lang/NoSuchMethodException
    //   804	809	1069	java/lang/NoSuchMethodException
    //   814	819	1069	java/lang/NoSuchMethodException
    //   824	826	1069	java/lang/NoSuchMethodException
    //   830	833	1069	java/lang/NoSuchMethodException
    //   837	842	1069	java/lang/NoSuchMethodException
    //   844	849	1069	java/lang/NoSuchMethodException
    //   863	868	1069	java/lang/NoSuchMethodException
    //   875	879	1069	java/lang/NoSuchMethodException
    //   883	889	1069	java/lang/NoSuchMethodException
    //   889	894	1069	java/lang/NoSuchMethodException
    //   901	905	1069	java/lang/NoSuchMethodException
    //   908	913	1069	java/lang/NoSuchMethodException
    //   915	921	1069	java/lang/NoSuchMethodException
    //   927	933	1069	java/lang/NoSuchMethodException
    //   939	944	1069	java/lang/NoSuchMethodException
    //   944	948	1069	java/lang/NoSuchMethodException
    //   959	965	1069	java/lang/NoSuchMethodException
    //   970	975	1069	java/lang/NoSuchMethodException
    //   979	984	1069	java/lang/NoSuchMethodException
    //   988	992	1069	java/lang/NoSuchMethodException
    //   441	446	1167	java/lang/NoSuchMethodException
    //   448	453	1167	java/lang/NoSuchMethodException
    //   460	463	1167	java/lang/NoSuchMethodException
    //   471	476	1167	java/lang/NoSuchMethodException
    //   476	481	1167	java/lang/NoSuchMethodException
    //   485	491	1167	java/lang/NoSuchMethodException
    //   497	503	1167	java/lang/NoSuchMethodException
    //   503	508	1167	java/lang/NoSuchMethodException
    //   513	518	1167	java/lang/NoSuchMethodException
    //   519	524	1167	java/lang/NoSuchMethodException
    //   526	531	1167	java/lang/NoSuchMethodException
    //   538	543	1167	java/lang/NoSuchMethodException
  }
  
  private static DeepLinkResult a(Context paramContext, boolean paramBoolean, Uri paramUri, String paramString)
  {
    boolean bool = paramBoolean ^ true;
    b(paramContext, bool, paramUri, paramString);
    paramContext = new com/airbnb/deeplinkdispatch/DeepLinkResult;
    if (paramUri != null) {
      paramUri = paramUri.toString();
    } else {
      paramUri = null;
    }
    paramContext.<init>(paramBoolean, paramUri, paramString);
    return paramContext;
  }
  
  private static void b(Context paramContext, boolean paramBoolean, Uri paramUri, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.setAction("com.airbnb.deeplinkdispatch.DEEPLINK_ACTION");
    String str1 = "com.airbnb.deeplinkdispatch.EXTRA_URI";
    if (paramUri != null) {
      paramUri = paramUri.toString();
    } else {
      paramUri = "";
    }
    localIntent.putExtra(str1, paramUri);
    paramUri = "com.airbnb.deeplinkdispatch.EXTRA_SUCCESSFUL";
    boolean bool = paramBoolean ^ true;
    localIntent.putExtra(paramUri, bool);
    if (paramBoolean)
    {
      String str2 = "com.airbnb.deeplinkdispatch.EXTRA_ERROR_MESSAGE";
      localIntent.putExtra(str2, paramString);
    }
    d.a(paramContext).a(localIntent);
  }
  
  public final DeepLinkResult a(Activity paramActivity)
  {
    Intent localIntent = paramActivity.getIntent();
    return a(paramActivity, localIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.deeplink.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */