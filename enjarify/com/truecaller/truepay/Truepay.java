package com.truecaller.truepay;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import com.truecaller.truepay.app.a.a.b.a;
import com.truecaller.truepay.app.a.b.br;
import com.truecaller.truepay.app.a.b.ct;
import com.truecaller.truepay.app.a.b.dc;
import com.truecaller.truepay.app.a.b.dj;
import com.truecaller.truepay.app.a.b.x;
import com.truecaller.truepay.app.ui.dashboard.views.b.d;
import com.truecaller.truepay.app.ui.payments.views.b.j;
import com.truecaller.truepay.app.ui.registration.d.u;
import com.truecaller.truepay.app.ui.registration.views.b.h;
import com.truecaller.truepay.app.utils.PaymentPresence;
import com.truecaller.truepay.app.utils.ag;
import com.truecaller.truepay.app.utils.n;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import dagger.a.g;

public class Truepay
{
  private static com.truecaller.truepay.app.a.a.a applicationComponent;
  public com.truecaller.truepay.data.b.a analyticLoggerHelper;
  public Application application;
  public c creditHelper;
  public n dynamicShortcutHelper;
  public com.truecaller.featuretoggles.e featuresRegistry;
  private TcPaySDKListener listener;
  public com.truecaller.truepay.app.utils.ab payAppUpdateManager;
  public ag paymentPresenceHelper;
  public com.truecaller.truepay.data.e.e prefSecretToken;
  public com.truecaller.truepay.data.e.e prefTempToken;
  public com.truecaller.truepay.data.e.e prefUserUuid;
  public com.truecaller.common.background.b scheduler;
  public com.truecaller.truepay.data.e.c securePreferences;
  public f userRegisteredListener;
  
  public static com.truecaller.truepay.app.a.a.a getApplicationComponent()
  {
    return applicationComponent;
  }
  
  private com.truecaller.truepay.app.ui.base.views.fragments.b getBankingHomeFragment()
  {
    com.truecaller.featuretoggles.b localb = featuresRegistry.ad();
    boolean bool = localb.a();
    if (bool) {
      return com.truecaller.truepay.app.ui.homescreen.views.d.a.k();
    }
    return d.b();
  }
  
  private com.truecaller.truepay.app.ui.base.views.fragments.b getBankingIntroFragmentV2()
  {
    return h.d("banking");
  }
  
  public static Truepay getInstance()
  {
    return Truepay.a.a;
  }
  
  private com.truecaller.truepay.app.ui.base.views.fragments.b getPaymentsIntroFragmentV2()
  {
    return h.d("payments");
  }
  
  private void initDagger(Application paramApplication)
  {
    Object localObject1 = paramApplication;
    Object localObject2 = com.truecaller.truepay.app.a.a.b.aw();
    Object localObject3 = new com/truecaller/truepay/app/a/b/c;
    ((com.truecaller.truepay.app.a.b.c)localObject3).<init>(paramApplication);
    localObject3 = (com.truecaller.truepay.app.a.b.c)g.a(localObject3);
    a = ((com.truecaller.truepay.app.a.b.c)localObject3);
    localObject3 = (t)g.a(com.truecaller.utils.c.a().a(paramApplication).a());
    l = ((t)localObject3);
    localObject1 = (com.truecaller.common.a)g.a(((com.truecaller.common.b.a)paramApplication).u());
    m = ((com.truecaller.common.a)localObject1);
    localObject1 = a;
    localObject3 = com.truecaller.truepay.app.a.b.c.class;
    g.a(localObject1, (Class)localObject3);
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/app/a/b/dj;
      ((dj)localObject1).<init>();
      b = ((dj)localObject1);
    }
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/app/a/b/ab;
      ((com.truecaller.truepay.app.a.b.ab)localObject1).<init>();
      c = ((com.truecaller.truepay.app.a.b.ab)localObject1);
    }
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/app/a/b/br;
      ((br)localObject1).<init>();
      d = ((br)localObject1);
    }
    localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/app/a/b/ct;
      ((ct)localObject1).<init>();
      e = ((ct)localObject1);
    }
    localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/app/a/b/a;
      ((com.truecaller.truepay.app.a.b.a)localObject1).<init>();
      f = ((com.truecaller.truepay.app.a.b.a)localObject1);
    }
    localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/app/ui/registration/d/u;
      ((u)localObject1).<init>();
      g = ((u)localObject1);
    }
    localObject1 = h;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/app/ui/npci/a/a;
      ((com.truecaller.truepay.app.ui.npci.a.a)localObject1).<init>();
      h = ((com.truecaller.truepay.app.ui.npci.a.a)localObject1);
    }
    localObject1 = i;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/app/a/b/x;
      ((x)localObject1).<init>();
      i = ((x)localObject1);
    }
    localObject1 = j;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/app/a/b/dc;
      ((dc)localObject1).<init>();
      j = ((dc)localObject1);
    }
    localObject1 = k;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/data/a/ab;
      ((com.truecaller.truepay.data.a.ab)localObject1).<init>();
      k = ((com.truecaller.truepay.data.a.ab)localObject1);
    }
    g.a(l, t.class);
    g.a(m, com.truecaller.common.a.class);
    localObject1 = new com/truecaller/truepay/app/a/a/b;
    com.truecaller.truepay.app.a.b.c localc = a;
    dj localdj = b;
    com.truecaller.truepay.app.a.b.ab localab = c;
    br localbr = d;
    ct localct = e;
    com.truecaller.truepay.app.a.b.a locala = f;
    u localu = g;
    com.truecaller.truepay.app.ui.npci.a.a locala1 = h;
    x localx = i;
    dc localdc = j;
    com.truecaller.truepay.data.a.ab localab1 = k;
    t localt = l;
    localObject2 = m;
    ((com.truecaller.truepay.app.a.a.b)localObject1).<init>(localc, localdj, localab, localbr, localct, locala, localu, locala1, localx, localdc, localab1, localt, (com.truecaller.common.a)localObject2, (byte)0);
    applicationComponent = (com.truecaller.truepay.app.a.a.a)localObject1;
    localObject2 = this;
    ((com.truecaller.truepay.app.a.a.a)localObject1).a(this);
  }
  
  private void initScheduler()
  {
    com.truecaller.common.background.b localb = scheduler;
    int[] arrayOfInt = new int[2];
    int[] tmp10_9 = arrayOfInt;
    tmp10_9[0] = '丣';
    tmp10_9[1] = '两';
    localb.a(20001, arrayOfInt);
  }
  
  private void initShortcuts()
  {
    dynamicShortcutHelper.a();
    n localn = dynamicShortcutHelper;
    Object localObject1 = application;
    int i = R.string.shortcut_banking_short_label;
    localObject1 = ((Application)localObject1).getString(i);
    i = R.drawable.ic_shortcut_banking;
    int j = 1;
    Object localObject2 = new Intent[j];
    Intent localIntent = new android/content/Intent;
    Uri localUri = Uri.parse("truecaller://home/tabs/banking").buildUpon().appendQueryParameter("deeplink_source", "app_shortcut").build();
    localIntent.<init>("android.intent.action.VIEW", localUri);
    localObject2[0] = localIntent;
    localn.a((String)localObject1, i, 4, (Intent[])localObject2);
    localn = dynamicShortcutHelper;
    localObject1 = application;
    i = R.string.shortcut_payments_short_label;
    localObject1 = ((Application)localObject1).getString(i);
    i = R.drawable.ic_shortcut_payments;
    Intent[] arrayOfIntent = new Intent[j];
    localObject2 = new android/content/Intent;
    localUri = Uri.parse("truecaller://home/tabs/payments").buildUpon().appendQueryParameter("deeplink_source", "app_shortcut").build();
    ((Intent)localObject2).<init>("android.intent.action.VIEW", localUri);
    arrayOfIntent[0] = localObject2;
    localn.a((String)localObject1, i, 5, arrayOfIntent);
    dynamicShortcutHelper.b();
  }
  
  private void initStrictMode() {}
  
  private void initTLog()
  {
    com.truecaller.log.f.a = false;
  }
  
  private void initTcPaySDKListener(Application paramApplication)
  {
    if (paramApplication != null)
    {
      boolean bool = paramApplication instanceof TcPaySDKListener;
      if (bool)
      {
        paramApplication = (TcPaySDKListener)paramApplication;
        listener = paramApplication;
        return;
      }
    }
    paramApplication = new java/lang/IllegalStateException;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Application must implement ");
    String str = TcPaySDKListener.class.getSimpleName();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    paramApplication.<init>((String)localObject);
    throw paramApplication;
  }
  
  private void initUserRegisteredListener()
  {
    boolean bool = isRegistrationComplete();
    if (bool)
    {
      userRegisteredListener.b();
      return;
    }
    userRegisteredListener.a();
  }
  
  public static void initialize(Application paramApplication)
  {
    getInstance().initDagger(paramApplication);
    getInstance().initTLog();
    getInstance().initStrictMode();
    getInstance().initScheduler();
    getInstance().initTcPaySDKListener(paramApplication);
    getInstance().initShortcuts();
    getInstance().initUserRegisteredListener();
  }
  
  private boolean isRegistrationSuccess()
  {
    Object localObject = securePreferences;
    String str = "j<@$f)qntd=?5e!y";
    localObject = ((com.truecaller.truepay.data.e.c)localObject).d(str);
    int i = ((Integer)localObject).intValue();
    int j = 104;
    return j == i;
  }
  
  public com.truecaller.truepay.data.b.a getAnalyticLoggerHelper()
  {
    return analyticLoggerHelper;
  }
  
  public com.truecaller.truepay.app.ui.base.views.fragments.b getBankingFragment()
  {
    boolean bool = isRegistrationComplete();
    if (bool) {
      return getBankingHomeFragment();
    }
    return getBankingIntroFragmentV2();
  }
  
  public TcPaySDKListener getListener()
  {
    return listener;
  }
  
  public com.truecaller.truepay.app.ui.base.views.fragments.b getPaymentsFragment()
  {
    com.truecaller.featuretoggles.b localb = featuresRegistry.o();
    boolean bool = localb.a();
    if (bool)
    {
      bool = isRegistrationComplete();
      if (bool) {
        return getPaymentsHomeFragment();
      }
      return getPaymentsIntroFragmentV2();
    }
    return com.truecaller.truepay.app.ui.dashboard.views.b.e.b();
  }
  
  public com.truecaller.truepay.app.ui.base.views.fragments.b getPaymentsHomeFragment()
  {
    return j.b();
  }
  
  public PaymentPresence getPresenceInfo()
  {
    Object localObject = applicationComponent;
    if (localObject != null)
    {
      localObject = paymentPresenceHelper;
      if (localObject != null) {
        return ((ag)localObject).a();
      }
    }
    localObject = new com/truecaller/truepay/app/utils/PaymentPresence;
    ((PaymentPresence)localObject).<init>(false, 1, 0);
    return (PaymentPresence)localObject;
  }
  
  public void handleAppUpdate(boolean paramBoolean)
  {
    payAppUpdateManager.a(paramBoolean);
  }
  
  public void incrementBankingClicked()
  {
    Object localObject = applicationComponent;
    if (localObject != null)
    {
      int i = securePreferences.d("P)7`@F]CxES%Xuf!").intValue();
      com.truecaller.truepay.data.e.c localc = securePreferences;
      String str = "P)7`@F]CxES%Xuf!";
      i += 1;
      localObject = Integer.valueOf(i);
      localc.a(str, (Integer)localObject);
    }
  }
  
  public void incrementPaymentsClicked()
  {
    Object localObject = applicationComponent;
    if (localObject != null)
    {
      int i = securePreferences.d("DR!~Tm9k7N>2jv`,").intValue();
      com.truecaller.truepay.data.e.c localc = securePreferences;
      String str = "DR!~Tm9k7N>2jv`,";
      i += 1;
      localObject = Integer.valueOf(i);
      localc.a(str, (Integer)localObject);
    }
  }
  
  public boolean isRegistrationComplete()
  {
    Object localObject = applicationComponent;
    if (localObject != null)
    {
      localObject = prefUserUuid;
      boolean bool = ((com.truecaller.truepay.data.e.e)localObject).b();
      if (bool)
      {
        localObject = prefSecretToken;
        bool = ((com.truecaller.truepay.data.e.e)localObject).b();
        if (bool)
        {
          bool = isRegistrationSuccess();
          if (bool) {
            return true;
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public void openBankingTab(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.setAction("android.intent.action.VIEW");
    Uri localUri = Uri.parse("truecaller://home/tabs/banking");
    localIntent.setData(localUri);
    paramContext.startActivity(localIntent);
  }
  
  public void openPaymentsTab(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.setAction("android.intent.action.VIEW");
    Uri localUri = Uri.parse("truecaller://home/tabs/payments");
    localIntent.setData(localUri);
    paramContext.startActivity(localIntent);
  }
  
  public void setCreditHelper(c paramc)
  {
    creditHelper = paramc;
  }
  
  public boolean shouldShowBankingAsNew()
  {
    Object localObject = applicationComponent;
    if (localObject != null)
    {
      localObject = securePreferences;
      String str = "P)7`@F]CxES%Xuf!";
      localObject = ((com.truecaller.truepay.data.e.c)localObject).d(str);
      int i = ((Integer)localObject).intValue();
      int j = 3;
      return i < j;
    }
    return false;
  }
  
  public boolean shouldShowPaymentsAsNew()
  {
    Object localObject = applicationComponent;
    if (localObject != null)
    {
      localObject = securePreferences;
      String str = "DR!~Tm9k7N>2jv`,";
      localObject = ((com.truecaller.truepay.data.e.c)localObject).d(str);
      int i = ((Integer)localObject).intValue();
      int j = 3;
      return i < j;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.Truepay
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */