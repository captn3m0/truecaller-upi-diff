package com.truecaller.truepay;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import com.truecaller.log.d;
import com.truecaller.truepay.app.ui.scan.views.activities.MerchantActivity;

public final class g
  implements f
{
  private final Context a;
  
  public g(Context paramContext)
  {
    a = paramContext;
  }
  
  public final void a()
  {
    Object localObject1 = UserRegistered.class;
    try
    {
      localObject1 = ((Class)localObject1).getCanonicalName();
      int i = 1;
      int j = 2;
      Object localObject2;
      ComponentName localComponentName;
      Object localObject3;
      if (localObject1 != null)
      {
        localObject2 = a;
        localObject2 = ((Context)localObject2).getPackageManager();
        localComponentName = new android/content/ComponentName;
        localObject3 = a;
        localObject3 = ((Context)localObject3).getPackageName();
        localComponentName.<init>((String)localObject3, (String)localObject1);
        ((PackageManager)localObject2).setComponentEnabledSetting(localComponentName, j, i);
      }
      localObject1 = MerchantActivity.class;
      localObject1 = ((Class)localObject1).getCanonicalName();
      if (localObject1 != null)
      {
        localObject2 = a;
        localObject2 = ((Context)localObject2).getPackageManager();
        localComponentName = new android/content/ComponentName;
        localObject3 = a;
        localObject3 = ((Context)localObject3).getPackageName();
        localComponentName.<init>((String)localObject3, (String)localObject1);
        ((PackageManager)localObject2).setComponentEnabledSetting(localComponentName, j, i);
      }
      return;
    }
    catch (Exception localException)
    {
      d.a((Throwable)localException);
    }
  }
  
  public final void b()
  {
    Object localObject1 = UserRegistered.class;
    try
    {
      localObject1 = ((Class)localObject1).getCanonicalName();
      int i = 1;
      Object localObject2;
      ComponentName localComponentName;
      Object localObject3;
      if (localObject1 != null)
      {
        localObject2 = a;
        localObject2 = ((Context)localObject2).getPackageManager();
        localComponentName = new android/content/ComponentName;
        localObject3 = a;
        localObject3 = ((Context)localObject3).getPackageName();
        localComponentName.<init>((String)localObject3, (String)localObject1);
        ((PackageManager)localObject2).setComponentEnabledSetting(localComponentName, i, i);
      }
      localObject1 = MerchantActivity.class;
      localObject1 = ((Class)localObject1).getCanonicalName();
      if (localObject1 != null)
      {
        localObject2 = a;
        localObject2 = ((Context)localObject2).getPackageManager();
        localComponentName = new android/content/ComponentName;
        localObject3 = a;
        localObject3 = ((Context)localObject3).getPackageName();
        localComponentName.<init>((String)localObject3, (String)localObject1);
        ((PackageManager)localObject2).setComponentEnabledSetting(localComponentName, i, i);
      }
      return;
    }
    catch (Exception localException)
    {
      d.a((Throwable)localException);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */