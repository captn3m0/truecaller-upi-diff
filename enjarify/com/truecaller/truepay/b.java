package com.truecaller.truepay;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import c.g.b.k;
import com.truecaller.truepay.app.utils.ax;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public final class b
  implements a
{
  private final Context a;
  private final ax b;
  private final com.truecaller.truepay.data.b.a c;
  
  public b(Context paramContext, ax paramax, com.truecaller.truepay.data.b.a parama)
  {
    a = paramContext;
    b = paramax;
    c = parama;
  }
  
  public final boolean a()
  {
    Object localObject1 = new java/util/HashSet;
    ((HashSet)localObject1).<init>();
    Object localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>();
    Object localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>();
    Object localObject4 = b.d();
    boolean bool1 = false;
    Object localObject5 = null;
    if (localObject4 == null) {
      return false;
    }
    k.a(localObject4, "utilityHelper.readClever…nfoList() ?: return false");
    Object localObject6 = a.getPackageManager();
    int i = 128;
    localObject6 = ((PackageManager)localObject6).getInstalledApplications(i);
    if (localObject6 == null) {
      return false;
    }
    localObject5 = new java/util/ArrayList;
    ((ArrayList)localObject5).<init>();
    localObject6 = ((List)localObject6).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject6).hasNext();
      if (!bool2) {
        break;
      }
      String str = nextpackageName;
      ((ArrayList)localObject5).add(str);
    }
    localObject6 = localObject5;
    localObject6 = (Collection)localObject5;
    ((List)localObject4).retainAll((Collection)localObject6);
    localObject4 = ((ArrayList)localObject5).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject4).hasNext();
      if (!bool1) {
        break;
      }
      localObject5 = (String)((Iterator)localObject4).next();
      int j = ((String)localObject5).hashCode();
      switch (j)
      {
      default: 
        break;
      case 2009581522: 
        localObject6 = "com.bankofbaroda.mconnect";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Baroda M-connect";
          ((HashSet)localObject2).add(localObject5);
        }
        break;
      case 1780684690: 
        localObject6 = "com.amazon.anow";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 1668864089: 
        localObject6 = "com.india.foodpanda.android";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 1647580510: 
        localObject6 = "in.swiggy.android";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 1644257669: 
        localObject6 = "com.bsb.hike";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 1559336780: 
        localObject6 = "com.oyo.consumer";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 1540779641: 
        localObject6 = "com.urbanclap.urbanclap";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 1474562920: 
        localObject6 = "com.goibibo";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 1337821420: 
        localObject6 = "com.booking";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 1236136879: 
        localObject6 = "com.myntra.android";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 1227213004: 
        localObject6 = "com.grofers.customerapp";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 1202759330: 
        localObject6 = "com.axis.mobile";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Axis mobile banking";
          ((HashSet)localObject2).add(localObject5);
        }
        break;
      case 1170339061: 
        localObject6 = "com.google.android.apps.nbu.paisa.user";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "GooglePay";
          ((HashSet)localObject1).add(localObject5);
        }
        break;
      case 997756941: 
        localObject6 = "com.gaana";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 994781143: 
        localObject6 = "com.sbi.SBIFreedomPlus";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "SBI-mobile banking";
          ((HashSet)localObject2).add(localObject5);
        }
        break;
      case 832961721: 
        localObject6 = "com.daamitt.walnut.app";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 738564857: 
        localObject6 = "com.bt.bms";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 716913649: 
        localObject6 = "com.amazon.avod.thirdpartyclient";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 714499313: 
        localObject6 = "com.facebook.katana";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 709623460: 
        localObject6 = "com.app.abhibus";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 663154607: 
        localObject6 = "com.application.zomato";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Food Delivery";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case 647915941: 
        localObject6 = "com.mventus.selfcare.activity";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 639310365: 
        localObject6 = "com.ubercab";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 562119001: 
        localObject6 = "com.jio.myjio";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 540665142: 
        localObject6 = "cris.org.in.prs.ima";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 500802662: 
        localObject6 = "com.netflix.mediaclient";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 499619142: 
        localObject6 = "com.cleartrip.android";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 437353897: 
        localObject6 = "com.sbi.erupee";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "SBI-buddy";
          ((HashSet)localObject2).add(localObject5);
        }
        break;
      case 409596831: 
        localObject6 = "com.saavn.android";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 213684625: 
        localObject6 = "com.trivago";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Hotel booking";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case 186255355: 
        localObject6 = "com.snapwork.hdfcbank";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 123107788: 
        localObject6 = "com.makemytrip";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case 121348070: 
        localObject6 = "net.one97.paytm";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Paytm";
          ((HashSet)localObject1).add(localObject5);
        }
        break;
      case 31553495: 
        localObject6 = "com.csam.icici.bank.imobile";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "ICICI-mobile banking";
          ((HashSet)localObject2).add(localObject5);
        }
        break;
      case 23930373: 
        localObject6 = "com.ixigo.flight.ixiflight";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Bookings(travel,hotel)";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -115512358: 
        localObject6 = "com.housejoy.consumer.activity";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case -116840477: 
        localObject6 = "com.ideacellular.myidea";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case -381266646: 
        localObject6 = "com.bigbasket.mobileapp";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case -387596570: 
        localObject6 = "com.olacabs.customer";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Cab Hailing";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -478337907: 
        localObject6 = "com.ixigo.train.ixitrain";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case -557749138: 
        localObject6 = "in.startv.hotstar";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case -662003450: 
        localObject6 = "com.instagram.android";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case -730285761: 
        localObject6 = "com.snapwork.hdfc";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "HDFC-mobile banking";
          ((HashSet)localObject2).add(localObject5);
        }
        break;
      case -754988891: 
        localObject6 = "com.sonyliv";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case -802359706: 
        localObject6 = "in.org.npci.upiapp";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Bhim-UPI";
          ((HashSet)localObject1).add(localObject5);
        }
        break;
      case -942668205: 
        localObject6 = "com.flipkart.android";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case -1084602842: 
        localObject6 = "in.mohalla.sharechat";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Social";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -1170413645: 
        localObject6 = "in.amazon.mShop.android.shopping";
        bool1 = ((String)localObject5).equals(localObject6);
        if (!bool1) {}
        break;
      case -1180520466: 
        localObject6 = "com.net.pvr";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Movie Ticketing";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -1237820313: 
        localObject6 = "com.bsbportal.music";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Music";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -1238810178: 
        localObject6 = "in.co.bankofbaroda.mpassbook";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Baroda M-passbook";
          ((HashSet)localObject2).add(localObject5);
        }
        break;
      case -1418358261: 
        localObject6 = "com.jabong.android";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "E-commerce";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -1456982352: 
        localObject6 = "com.godrej.naturesbasketltd";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Grocery Shopping";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -1547699361: 
        localObject6 = "com.whatsapp";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Messenger";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -1638634281: 
        localObject6 = "com.myairtelapp";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Telecom";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -1813392172: 
        localObject6 = "com.dunzo.user";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Hyperlocal";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -1868210007: 
        localObject6 = "com.phonepe.app";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "PhonePe";
          ((HashSet)localObject1).add(localObject5);
        }
        break;
      case -1902529607: 
        localObject6 = "in.redbus.android";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Train/Bus booking";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -2075712516: 
        localObject6 = "com.google.android.youtube";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Streaming";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      case -2124967473: 
        localObject6 = "com.whizdm.moneyview";
        bool1 = ((String)localObject5).equals(localObject6);
        if (bool1)
        {
          localObject5 = "Expense manager";
          ((HashSet)localObject3).add(localObject5);
        }
        break;
      }
    }
    localObject4 = c;
    if (localObject4 != null)
    {
      localObject5 = "PaymentAppsInstalled";
      localObject6 = new java/util/ArrayList;
      localObject1 = (Collection)localObject1;
      ((ArrayList)localObject6).<init>((Collection)localObject1);
      ((com.truecaller.truepay.data.b.a)localObject4).a((String)localObject5, localObject6);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject4 = "BankingAppsInstalled";
      localObject5 = new java/util/ArrayList;
      localObject2 = (Collection)localObject2;
      ((ArrayList)localObject5).<init>((Collection)localObject2);
      ((com.truecaller.truepay.data.b.a)localObject1).a((String)localObject4, localObject5);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = "OtherAppsInstalled";
      localObject4 = new java/util/ArrayList;
      localObject3 = (Collection)localObject3;
      ((ArrayList)localObject4).<init>((Collection)localObject3);
      ((com.truecaller.truepay.data.b.a)localObject1).a((String)localObject2, localObject4);
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */