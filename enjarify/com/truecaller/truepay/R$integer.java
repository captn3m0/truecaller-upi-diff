package com.truecaller.truepay;

public final class R$integer
{
  public static final int abc_config_activityDefaultDur = 2131427328;
  public static final int abc_config_activityShortDur = 2131427329;
  public static final int app_bar_elevation_anim_duration = 2131427330;
  public static final int bottom_sheet_slide_duration = 2131427331;
  public static final int cancel_button_image_alpha = 2131427332;
  public static final int config_tooltipAnimTime = 2131427333;
  public static final int default_circle_indicator_orientation = 2131427334;
  public static final int design_snackbar_text_max_lines = 2131427335;
  public static final int design_tab_indicator_anim_duration_ms = 2131427336;
  public static final int google_play_services_version = 2131427338;
  public static final int grid_span = 2131427339;
  public static final int hide_password_duration = 2131427340;
  public static final int mtrl_btn_anim_delay_ms = 2131427342;
  public static final int mtrl_btn_anim_duration_ms = 2131427343;
  public static final int mtrl_chip_anim_duration = 2131427344;
  public static final int mtrl_tab_indicator_anim_duration_ms = 2131427345;
  public static final int pay_entry_max_amount_length = 2131427346;
  public static final int pay_entry_max_message_length = 2131427347;
  public static final int peek_notification_animation_duration = 2131427348;
  public static final int peek_notification_autohide_timeout = 2131427349;
  public static final int show_password_duration = 2131427350;
  public static final int status_bar_notification_info_maxnum = 2131427351;
  public static final int viewfinder_border_length = 2131427352;
  public static final int viewfinder_border_width = 2131427353;
  public static final int weekday_textsize_big = 2131427354;
  public static final int weekday_textsize_small = 2131427355;
  public static final int weekday_textview_height = 2131427356;
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.R.integer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */