package com.truecaller.truepay;

import android.arch.lifecycle.LiveData;

public abstract interface c
{
  public abstract Object a(c.d.c paramc);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean, String paramString1, String paramString2);
  
  public abstract Object c();
  
  public abstract LiveData d();
  
  public abstract void e();
  
  public abstract void f();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */