package com.truecaller.truepay;

public abstract interface PayTempTokenCallBack
{
  public abstract void onFailure();
  
  public abstract void onSuccess(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.PayTempTokenCallBack
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */