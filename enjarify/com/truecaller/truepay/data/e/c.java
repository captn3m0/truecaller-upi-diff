package com.truecaller.truepay.data.e;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.truecaller.truepay.app.utils.p;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

public final class c
{
  private final SharedPreferences a;
  
  public c(SharedPreferences paramSharedPreferences)
  {
    a = paramSharedPreferences;
  }
  
  public final String a(String paramString1, String paramString2)
  {
    Object localObject = a;
    int i = 0;
    String str1 = null;
    paramString1 = ((SharedPreferences)localObject).getString(paramString1, null);
    if (paramString1 == null) {
      return paramString2;
    }
    try
    {
      localObject = p.b();
      i = 16;
      localObject = ((String)localObject).substring(0, i);
      String str2 = p.c();
      str1 = str2.substring(0, i);
      return p.b((String)localObject, paramString1, str1);
    }
    catch (IllegalArgumentException paramString1) {}catch (UnsupportedEncodingException paramString1) {}catch (GeneralSecurityException paramString1) {}
    paramString1.printStackTrace();
    return paramString2;
  }
  
  public final void a()
  {
    a.edit().clear().apply();
  }
  
  public final void a(String paramString, Boolean paramBoolean)
  {
    paramBoolean = String.valueOf(paramBoolean);
    b(paramString, paramBoolean);
  }
  
  public final void a(String paramString, Integer paramInteger)
  {
    paramInteger = String.valueOf(paramInteger);
    b(paramString, paramInteger);
  }
  
  public final void a(String paramString, Long paramLong)
  {
    paramLong = String.valueOf(paramLong);
    b(paramString, paramLong);
  }
  
  public final boolean a(String paramString)
  {
    return a.contains(paramString);
  }
  
  public final void b(String paramString)
  {
    a.edit().remove(paramString).apply();
  }
  
  public final void b(String paramString1, String paramString2)
  {
    try
    {
      boolean bool = TextUtils.isEmpty(paramString2);
      if (bool) {
        paramString2 = "";
      }
      Object localObject = p.b();
      int i = 16;
      localObject = ((String)localObject).substring(0, i);
      String str1 = p.c();
      String str2 = str1.substring(0, i);
      paramString2 = p.a((String)localObject, paramString2, str2);
      localObject = a;
      localObject = ((SharedPreferences)localObject).edit();
      paramString1 = ((SharedPreferences.Editor)localObject).putString(paramString1, paramString2);
      paramString1.apply();
      return;
    }
    catch (GeneralSecurityException localGeneralSecurityException) {}
  }
  
  public final String c(String paramString)
  {
    return a(paramString, "");
  }
  
  public final Integer d(String paramString)
  {
    return Integer.valueOf(a(paramString, "0"));
  }
  
  public final Boolean e(String paramString)
  {
    return Boolean.valueOf(a(paramString, "false"));
  }
  
  public final Long f(String paramString)
  {
    return Long.valueOf(a(paramString, "0"));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */