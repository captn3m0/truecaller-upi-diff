package com.truecaller.truepay.data.e;

public final class e
{
  private final c a;
  private final String b;
  private final String c;
  
  public e(c paramc, String paramString1, String paramString2)
  {
    a = paramc;
    b = paramString1;
    c = paramString2;
  }
  
  public final String a()
  {
    c localc = a;
    String str1 = b;
    String str2 = c;
    return localc.a(str1, str2);
  }
  
  public final void a(String paramString)
  {
    c localc = a;
    String str = b;
    localc.b(str, paramString);
  }
  
  public final boolean b()
  {
    c localc = a;
    String str = b;
    return localc.a(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.e.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */