package com.truecaller.truepay.data.e;

public final class b
{
  private final c a;
  private final String b;
  
  public b(c paramc, String paramString)
  {
    a = paramc;
    b = paramString;
  }
  
  public final Long a()
  {
    c localc = a;
    String str = b;
    return localc.f(str);
  }
  
  public final void a(Long paramLong)
  {
    c localc = a;
    String str = b;
    localc.a(str, paramLong);
  }
  
  public final boolean b()
  {
    c localc = a;
    String str = b;
    return localc.a(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */