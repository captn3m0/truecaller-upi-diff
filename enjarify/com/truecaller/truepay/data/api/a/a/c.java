package com.truecaller.truepay.data.api.a.a;

import c.g.b.k;
import c.n.m;
import java.net.URL;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.u;
import okhttp3.v;
import okhttp3.v.a;

public final class c
  implements v
{
  public static final c.a a;
  
  static
  {
    c.a locala = new com/truecaller/truepay/data/api/a/a/c$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    Object localObject1 = parama.a().e();
    String str1 = parama.a().a().f();
    Object localObject2 = new java/net/URL;
    String str2 = "https://platform.api.tcpay.in/";
    ((URL)localObject2).<init>(str2);
    localObject2 = ((URL)localObject2).getHost();
    boolean bool1 = true;
    boolean bool2 = m.a(str1, (String)localObject2, bool1);
    if (bool2)
    {
      str1 = "accept";
      localObject2 = "application/vnd.tcpay.v3+json";
      ((ab.a)localObject1).b(str1, (String)localObject2);
    }
    else
    {
      str1 = parama.a().a().f();
      localObject2 = new java/net/URL;
      String str3 = "https://registration.api.tcpay.in/";
      ((URL)localObject2).<init>(str3);
      localObject2 = ((URL)localObject2).getHost();
      bool2 = m.a(str1, (String)localObject2, bool1);
      if (bool2)
      {
        str1 = "accept";
        localObject2 = "application/vnd.tcpay.v2+json";
        ((ab.a)localObject1).b(str1, (String)localObject2);
      }
      else
      {
        str1 = "accept";
        localObject2 = "application/vnd.tcpay.v1+json";
        ((ab.a)localObject1).b(str1, (String)localObject2);
      }
    }
    localObject1 = ((ab.a)localObject1).a();
    parama = parama.a((ab)localObject1);
    k.a(parama, "chain.proceed(requestBuilder.build())");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */