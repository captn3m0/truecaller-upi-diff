package com.truecaller.truepay.data.api.a.a;

import android.content.Context;
import android.widget.Toast;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.R.string;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;

final class a$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  a$b(a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/truepay/data/api/a/a/a$b;
    a locala = b;
    localb.<init>(locala, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = a.a(b);
        localObject = a.b(b);
        int j = R.string.something_went_wrong;
        Object[] arrayOfObject = new Object[0];
        localObject = (CharSequence)((n)localObject).a(j, arrayOfObject);
        Toast.makeText((Context)paramObject, (CharSequence)localObject, 1).show();
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.a.a.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */