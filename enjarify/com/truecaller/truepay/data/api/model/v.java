package com.truecaller.truepay.data.api.model;

import c.g.b.k;

public final class v
{
  private String a;
  
  public v(String paramString)
  {
    a = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof v;
      if (bool1)
      {
        paramObject = (v)paramObject;
        String str = a;
        paramObject = a;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("InviteContactRequestDO(msisdn=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.model.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */