package com.truecaller.truepay.data.api.model;

import c.g.b.k;

public final class PromoBannerDO
{
  private final String bannerId;
  private final String deepLink;
  private final String expiresAt;
  private final String imageUrl;
  
  public PromoBannerDO()
  {
    this(null, null, null, null, 15, null);
  }
  
  public PromoBannerDO(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    imageUrl = paramString1;
    expiresAt = paramString2;
    deepLink = paramString3;
    bannerId = paramString4;
  }
  
  public final String component1()
  {
    return imageUrl;
  }
  
  public final String component2()
  {
    return expiresAt;
  }
  
  public final String component3()
  {
    return deepLink;
  }
  
  public final String component4()
  {
    return bannerId;
  }
  
  public final PromoBannerDO copy(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "imageUrl");
    k.b(paramString2, "expiresAt");
    k.b(paramString3, "deepLink");
    k.b(paramString4, "bannerId");
    PromoBannerDO localPromoBannerDO = new com/truecaller/truepay/data/api/model/PromoBannerDO;
    localPromoBannerDO.<init>(paramString1, paramString2, paramString3, paramString4);
    return localPromoBannerDO;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PromoBannerDO;
      if (bool1)
      {
        paramObject = (PromoBannerDO)paramObject;
        String str1 = imageUrl;
        String str2 = imageUrl;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = expiresAt;
          str2 = expiresAt;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = deepLink;
            str2 = deepLink;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = bannerId;
              paramObject = bannerId;
              boolean bool2 = k.a(str1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final String getBannerId()
  {
    return bannerId;
  }
  
  public final String getDeepLink()
  {
    return deepLink;
  }
  
  public final String getExpiresAt()
  {
    return expiresAt;
  }
  
  public final String getImageUrl()
  {
    return imageUrl;
  }
  
  public final int hashCode()
  {
    String str1 = imageUrl;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = expiresAt;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = deepLink;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = bannerId;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PromoBannerDO(imageUrl=");
    String str = imageUrl;
    localStringBuilder.append(str);
    localStringBuilder.append(", expiresAt=");
    str = expiresAt;
    localStringBuilder.append(str);
    localStringBuilder.append(", deepLink=");
    str = deepLink;
    localStringBuilder.append(str);
    localStringBuilder.append(", bannerId=");
    str = bannerId;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.model.PromoBannerDO
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */