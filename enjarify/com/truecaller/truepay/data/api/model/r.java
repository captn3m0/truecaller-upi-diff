package com.truecaller.truepay.data.api.model;

import java.util.ArrayList;

public final class r
{
  public boolean a;
  public boolean b;
  public boolean c;
  public String d;
  public String e;
  public boolean f;
  public String g;
  public boolean h;
  public ArrayList i;
  public String j;
  
  public final String a()
  {
    return d;
  }
  
  public final boolean b()
  {
    return f;
  }
  
  public final String c()
  {
    return g;
  }
  
  public final boolean d()
  {
    return h;
  }
  
  public final ArrayList e()
  {
    return i;
  }
  
  public final String f()
  {
    return j;
  }
  
  public final String g()
  {
    return e;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.model.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */