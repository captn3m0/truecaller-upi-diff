package com.truecaller.truepay.data.api.model;

import c.g.b.k;

public final class ResolveRequestUser
{
  private final String device_id;
  private final String msisdn;
  private final String profile_id;
  
  public ResolveRequestUser(String paramString1, String paramString2, String paramString3)
  {
    device_id = paramString1;
    profile_id = paramString2;
    msisdn = paramString3;
  }
  
  public final String component1()
  {
    return device_id;
  }
  
  public final String component2()
  {
    return profile_id;
  }
  
  public final String component3()
  {
    return msisdn;
  }
  
  public final ResolveRequestUser copy(String paramString1, String paramString2, String paramString3)
  {
    ResolveRequestUser localResolveRequestUser = new com/truecaller/truepay/data/api/model/ResolveRequestUser;
    localResolveRequestUser.<init>(paramString1, paramString2, paramString3);
    return localResolveRequestUser;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ResolveRequestUser;
      if (bool1)
      {
        paramObject = (ResolveRequestUser)paramObject;
        String str1 = device_id;
        String str2 = device_id;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = profile_id;
          str2 = profile_id;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = msisdn;
            paramObject = msisdn;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getDevice_id()
  {
    return device_id;
  }
  
  public final String getMsisdn()
  {
    return msisdn;
  }
  
  public final String getProfile_id()
  {
    return profile_id;
  }
  
  public final int hashCode()
  {
    String str1 = device_id;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = profile_id;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = msisdn;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ResolveRequestUser(device_id=");
    String str = device_id;
    localStringBuilder.append(str);
    localStringBuilder.append(", profile_id=");
    str = profile_id;
    localStringBuilder.append(str);
    localStringBuilder.append(", msisdn=");
    str = msisdn;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.model.ResolveRequestUser
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */