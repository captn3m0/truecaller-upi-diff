package com.truecaller.truepay.data.api.model;

import java.util.ArrayList;

public final class q
{
  public String a;
  public String b;
  public boolean c;
  public String d;
  public boolean e;
  public ArrayList f;
  private boolean g;
  private boolean h;
  private boolean i;
  private String j;
  
  public q() {}
  
  public q(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString1, boolean paramBoolean4, String paramString2, boolean paramBoolean5, ArrayList paramArrayList, String paramString3, String paramString4)
  {
    g = paramBoolean1;
    h = paramBoolean2;
    i = paramBoolean3;
    a = paramString1;
    c = paramBoolean4;
    d = paramString2;
    e = paramBoolean5;
    f = paramArrayList;
    j = paramString3;
    b = paramString4;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final boolean b()
  {
    return c;
  }
  
  public final String c()
  {
    return d;
  }
  
  public final boolean d()
  {
    return e;
  }
  
  public final ArrayList e()
  {
    return f;
  }
  
  public final String f()
  {
    return j;
  }
  
  public final String g()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.model.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */