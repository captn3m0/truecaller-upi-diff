package com.truecaller.truepay.data.api.model;

import c.g.b.k;

public final class ResolveRequestVpa
{
  private final String msisdn;
  private final String vpa;
  
  public ResolveRequestVpa(String paramString1, String paramString2)
  {
    vpa = paramString1;
    msisdn = paramString2;
  }
  
  public final String component1()
  {
    return vpa;
  }
  
  public final String component2()
  {
    return msisdn;
  }
  
  public final ResolveRequestVpa copy(String paramString1, String paramString2)
  {
    ResolveRequestVpa localResolveRequestVpa = new com/truecaller/truepay/data/api/model/ResolveRequestVpa;
    localResolveRequestVpa.<init>(paramString1, paramString2);
    return localResolveRequestVpa;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ResolveRequestVpa;
      if (bool1)
      {
        paramObject = (ResolveRequestVpa)paramObject;
        String str1 = vpa;
        String str2 = vpa;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = msisdn;
          paramObject = msisdn;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getMsisdn()
  {
    return msisdn;
  }
  
  public final String getVpa()
  {
    return vpa;
  }
  
  public final int hashCode()
  {
    String str1 = vpa;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = msisdn;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ResolveRequestVpa(vpa=");
    String str = vpa;
    localStringBuilder.append(str);
    localStringBuilder.append(", msisdn=");
    str = msisdn;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.model.ResolveRequestVpa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */