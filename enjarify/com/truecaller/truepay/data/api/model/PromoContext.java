package com.truecaller.truepay.data.api.model;

import c.g.b.k;

public final class PromoContext
{
  private final String context;
  
  public PromoContext()
  {
    this(null, 1, null);
  }
  
  public PromoContext(String paramString)
  {
    context = paramString;
  }
  
  public final String component1()
  {
    return context;
  }
  
  public final PromoContext copy(String paramString)
  {
    k.b(paramString, "context");
    PromoContext localPromoContext = new com/truecaller/truepay/data/api/model/PromoContext;
    localPromoContext.<init>(paramString);
    return localPromoContext;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof PromoContext;
      if (bool1)
      {
        paramObject = (PromoContext)paramObject;
        String str = context;
        paramObject = context;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getContext()
  {
    return context;
  }
  
  public final int hashCode()
  {
    String str = context;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PromoContext(context=");
    String str = context;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.model.PromoContext
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */