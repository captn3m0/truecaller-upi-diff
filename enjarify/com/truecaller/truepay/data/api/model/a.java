package com.truecaller.truepay.data.api.model;

import com.truecaller.truepay.app.utils.au;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class a
  implements Serializable
{
  public String a;
  public String b;
  public String c;
  public String d;
  public boolean e;
  public boolean f;
  public boolean g;
  public String h;
  public String i;
  public com.truecaller.truepay.data.d.a j;
  public String k;
  public boolean l;
  public boolean m;
  public String n;
  private String o;
  private ArrayList p;
  
  public final void a(com.truecaller.truepay.data.d.a parama)
  {
    j = parama;
  }
  
  public final void a(String paramString)
  {
    h = paramString;
  }
  
  public final void a(ArrayList paramArrayList)
  {
    p = paramArrayList;
  }
  
  public final void a(boolean paramBoolean)
  {
    g = paramBoolean;
  }
  
  public final boolean a()
  {
    return g;
  }
  
  public final String b()
  {
    return h;
  }
  
  public final void b(String paramString)
  {
    b = paramString;
  }
  
  public final void b(boolean paramBoolean)
  {
    e = paramBoolean;
  }
  
  public final String c()
  {
    return b;
  }
  
  public final void c(String paramString)
  {
    c = paramString;
  }
  
  public final void c(boolean paramBoolean)
  {
    f = paramBoolean;
  }
  
  public final String d()
  {
    return c;
  }
  
  public final void d(String paramString)
  {
    d = paramString;
  }
  
  public final void d(boolean paramBoolean)
  {
    m = paramBoolean;
  }
  
  public final String e()
  {
    return d;
  }
  
  public final void e(String paramString)
  {
    o = paramString;
  }
  
  public final void e(boolean paramBoolean)
  {
    l = paramBoolean;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = paramObject instanceof a;
    if (bool)
    {
      paramObject = (a)paramObject;
      String str1 = au.a(k);
      String str2 = au.a(c);
      bool = str1.equalsIgnoreCase(str2);
      if (bool)
      {
        str1 = d;
        str2 = d;
        bool = str1.equalsIgnoreCase(str2);
        if (bool)
        {
          paramObject = b;
          str1 = b;
          return ((String)paramObject).equalsIgnoreCase(str1);
        }
      }
    }
    return false;
  }
  
  public final String f()
  {
    return o;
  }
  
  public final void f(String paramString)
  {
    a = paramString;
  }
  
  public final void g(String paramString)
  {
    k = paramString;
  }
  
  public final boolean g()
  {
    return e;
  }
  
  public final ArrayList h()
  {
    return p;
  }
  
  public int hashCode()
  {
    Object[] arrayOfObject = new Object[5];
    String str = a;
    arrayOfObject[0] = str;
    str = b;
    arrayOfObject[1] = str;
    str = c;
    arrayOfObject[2] = str;
    str = d;
    arrayOfObject[3] = str;
    str = k;
    arrayOfObject[4] = str;
    return Objects.hash(arrayOfObject);
  }
  
  public final boolean i()
  {
    return f;
  }
  
  public final String j()
  {
    return a;
  }
  
  public final boolean k()
  {
    return m;
  }
  
  public final com.truecaller.truepay.data.d.a l()
  {
    return j;
  }
  
  public final String m()
  {
    return k;
  }
  
  public final boolean n()
  {
    return l;
  }
  
  public final String o()
  {
    return n;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.model.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */