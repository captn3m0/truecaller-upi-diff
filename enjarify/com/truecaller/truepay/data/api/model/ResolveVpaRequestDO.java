package com.truecaller.truepay.data.api.model;

import c.g.b.k;

public final class ResolveVpaRequestDO
{
  private final ResolveRequestVpa resolve_details;
  private final ResolveRequestUser user_details;
  
  public ResolveVpaRequestDO(ResolveRequestVpa paramResolveRequestVpa, ResolveRequestUser paramResolveRequestUser)
  {
    resolve_details = paramResolveRequestVpa;
    user_details = paramResolveRequestUser;
  }
  
  public final ResolveRequestVpa component1()
  {
    return resolve_details;
  }
  
  public final ResolveRequestUser component2()
  {
    return user_details;
  }
  
  public final ResolveVpaRequestDO copy(ResolveRequestVpa paramResolveRequestVpa, ResolveRequestUser paramResolveRequestUser)
  {
    k.b(paramResolveRequestVpa, "resolve_details");
    ResolveVpaRequestDO localResolveVpaRequestDO = new com/truecaller/truepay/data/api/model/ResolveVpaRequestDO;
    localResolveVpaRequestDO.<init>(paramResolveRequestVpa, paramResolveRequestUser);
    return localResolveVpaRequestDO;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ResolveVpaRequestDO;
      if (bool1)
      {
        paramObject = (ResolveVpaRequestDO)paramObject;
        Object localObject = resolve_details;
        ResolveRequestVpa localResolveRequestVpa = resolve_details;
        bool1 = k.a(localObject, localResolveRequestVpa);
        if (bool1)
        {
          localObject = user_details;
          paramObject = user_details;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final ResolveRequestVpa getResolve_details()
  {
    return resolve_details;
  }
  
  public final ResolveRequestUser getUser_details()
  {
    return user_details;
  }
  
  public final int hashCode()
  {
    ResolveRequestVpa localResolveRequestVpa = resolve_details;
    int i = 0;
    int j;
    if (localResolveRequestVpa != null)
    {
      j = localResolveRequestVpa.hashCode();
    }
    else
    {
      j = 0;
      localResolveRequestVpa = null;
    }
    j *= 31;
    ResolveRequestUser localResolveRequestUser = user_details;
    if (localResolveRequestUser != null) {
      i = localResolveRequestUser.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ResolveVpaRequestDO(resolve_details=");
    Object localObject = resolve_details;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", user_details=");
    localObject = user_details;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.model.ResolveVpaRequestDO
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */