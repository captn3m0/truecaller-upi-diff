package com.truecaller.truepay.data.api;

import com.truecaller.truepay.app.ui.accounts.b.a;
import com.truecaller.truepay.app.ui.accounts.b.e;
import com.truecaller.truepay.app.ui.balancecheck.model.ConfirmBalanceCheckRequest;
import com.truecaller.truepay.app.ui.balancecheck.model.InitiateBalanceCheckRequest;
import com.truecaller.truepay.app.ui.registration.c.k;
import com.truecaller.truepay.data.api.model.ah;
import com.truecaller.truepay.data.api.model.ai;
import com.truecaller.truepay.data.api.model.aj;
import com.truecaller.truepay.data.api.model.n;
import com.truecaller.truepay.data.api.model.u;
import io.reactivex.o;

public abstract interface g
{
  public abstract e.b a(com.truecaller.truepay.app.ui.accounts.b.c paramc);
  
  public abstract e.b a(ConfirmBalanceCheckRequest paramConfirmBalanceCheckRequest);
  
  public abstract e.b a(InitiateBalanceCheckRequest paramInitiateBalanceCheckRequest);
  
  public abstract o a();
  
  public abstract o a(a parama);
  
  public abstract o a(com.truecaller.truepay.app.ui.accounts.b.b paramb);
  
  public abstract o a(com.truecaller.truepay.app.ui.accounts.b.d paramd);
  
  public abstract o a(e parame);
  
  public abstract o a(com.truecaller.truepay.app.ui.dashboard.b.c paramc);
  
  public abstract o a(com.truecaller.truepay.app.ui.dashboard.b.d paramd);
  
  public abstract o a(com.truecaller.truepay.app.ui.history.models.d paramd);
  
  public abstract o a(com.truecaller.truepay.app.ui.history.models.f paramf);
  
  public abstract o a(com.truecaller.truepay.app.ui.history.models.l paraml);
  
  public abstract o a(com.truecaller.truepay.app.ui.registration.c.c paramc);
  
  public abstract o a(com.truecaller.truepay.app.ui.registration.c.f paramf);
  
  public abstract o a(com.truecaller.truepay.app.ui.registration.c.g paramg);
  
  public abstract o a(com.truecaller.truepay.app.ui.registration.c.i parami);
  
  public abstract o a(k paramk);
  
  public abstract o a(com.truecaller.truepay.app.ui.registration.c.l paraml);
  
  public abstract o a(com.truecaller.truepay.app.ui.transaction.b.d paramd);
  
  public abstract o a(com.truecaller.truepay.app.ui.transaction.b.j paramj);
  
  public abstract o a(ah paramah);
  
  public abstract o a(ai paramai);
  
  public abstract o a(aj paramaj);
  
  public abstract o a(com.truecaller.truepay.data.api.model.i parami);
  
  public abstract o a(com.truecaller.truepay.data.api.model.j paramj);
  
  public abstract o a(n paramn);
  
  public abstract o a(u paramu);
  
  public abstract e.b b(com.truecaller.truepay.app.ui.transaction.b.d paramd);
  
  public abstract e.b b(com.truecaller.truepay.app.ui.transaction.b.j paramj);
  
  public abstract o b();
  
  public abstract o b(ah paramah);
  
  public abstract o c();
  
  public abstract o c(ah paramah);
  
  public abstract o d();
  
  public abstract o d(ah paramah);
  
  public abstract o e();
  
  public abstract o f();
  
  public abstract o g();
  
  public abstract o h();
  
  public abstract e.b i();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.api.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */