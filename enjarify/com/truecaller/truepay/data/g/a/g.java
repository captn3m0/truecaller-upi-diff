package com.truecaller.truepay.data.g.a;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import com.truecaller.truepay.SmsBankData;
import com.truecaller.truepay.data.f.ah;
import com.truecaller.truepay.data.provider.a.b;
import com.truecaller.truepay.data.provider.b.c;
import io.reactivex.a;
import io.reactivex.o;

public final class g
  implements ah
{
  private final ContentResolver a;
  
  public g(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  public final io.reactivex.d a(String paramString)
  {
    -..Lambda.g.us6UBCYAYfq9y0UhZ-mOGpnRP7g localus6UBCYAYfq9y0UhZ-mOGpnRP7g = new com/truecaller/truepay/data/g/a/-$$Lambda$g$us6UBCYAYfq9y0UhZ-mOGpnRP7g;
    localus6UBCYAYfq9y0UhZ-mOGpnRP7g.<init>(this, paramString);
    paramString = a.c;
    return io.reactivex.d.a(localus6UBCYAYfq9y0UhZ-mOGpnRP7g, paramString);
  }
  
  public final o a(SmsBankData paramSmsBankData)
  {
    b localb = new com/truecaller/truepay/data/provider/a/b;
    localb.<init>();
    Object localObject1 = paramSmsBankData.getBankSymbol();
    localObject1 = localb.b((String)localObject1);
    Object localObject2 = paramSmsBankData.getBankName();
    localObject1 = ((b)localObject1).a((String)localObject2);
    int i = paramSmsBankData.getSimSlotIndex();
    localObject1 = ((b)localObject1).a(i);
    localObject2 = paramSmsBankData.getSmsCount();
    ((b)localObject1).a((Integer)localObject2);
    localObject1 = a;
    localb.a((ContentResolver)localObject1);
    return o.a(paramSmsBankData);
  }
  
  public final o a(SmsBankData paramSmsBankData, String paramString, int paramInt)
  {
    Object localObject1 = new com/truecaller/truepay/data/provider/a/b;
    ((b)localObject1).<init>();
    paramString = ((b)localObject1).b(paramString);
    Object localObject2 = paramSmsBankData.getBankName();
    paramString = paramString.a((String)localObject2);
    int i = paramSmsBankData.getSimSlotIndex();
    paramString = paramString.a(i);
    Object localObject3 = Integer.valueOf(paramInt);
    paramString.a((Integer)localObject3);
    paramString = new com/truecaller/truepay/data/provider/a/d;
    paramString.<init>();
    paramInt = 1;
    localObject2 = new String[paramInt];
    String str = paramSmsBankData.getBankName();
    localObject2[0] = str;
    localObject2 = (com.truecaller.truepay.data.provider.a.d)paramString.a((String[])localObject2).d();
    localObject3 = new String[paramInt];
    str = paramSmsBankData.getBankSymbol();
    localObject3[0] = str;
    ((com.truecaller.truepay.data.provider.a.d)localObject2).c((String[])localObject3);
    localObject3 = a;
    localObject2 = ((b)localObject1).b();
    localObject1 = ((b)localObject1).c();
    str = a.toString();
    paramString = paramString.e();
    ((ContentResolver)localObject3).update((Uri)localObject2, (ContentValues)localObject1, str, paramString);
    return o.a(paramSmsBankData);
  }
  
  /* Error */
  public final java.util.HashMap a()
  {
    // Byte code:
    //   0: iconst_2
    //   1: istore_1
    //   2: iload_1
    //   3: newarray <illegal type>
    //   5: astore_2
    //   6: aload_2
    //   7: dup
    //   8: iconst_0
    //   9: iconst_0
    //   10: iastore
    //   11: iconst_1
    //   12: iconst_1
    //   13: iastore
    //   14: new 196	java/util/HashMap
    //   17: astore_3
    //   18: aload_3
    //   19: invokespecial 197	java/util/HashMap:<init>	()V
    //   22: iconst_0
    //   23: istore 4
    //   25: iload 4
    //   27: iload_1
    //   28: if_icmpge +260 -> 288
    //   31: aload_2
    //   32: iload 4
    //   34: iaload
    //   35: istore 5
    //   37: new 16	com/truecaller/truepay/data/provider/a/d
    //   40: astore 6
    //   42: aload 6
    //   44: invokespecial 17	com/truecaller/truepay/data/provider/a/d:<init>	()V
    //   47: iconst_1
    //   48: istore 7
    //   50: iload 7
    //   52: newarray <illegal type>
    //   54: astore 8
    //   56: aload 8
    //   58: iconst_0
    //   59: iload 5
    //   61: iastore
    //   62: aload 6
    //   64: aload 8
    //   66: invokevirtual 200	com/truecaller/truepay/data/provider/a/d:a	([I)Lcom/truecaller/truepay/data/provider/a/d;
    //   69: pop
    //   70: aload_0
    //   71: getfield 14	com/truecaller/truepay/data/g/a/g:a	Landroid/content/ContentResolver;
    //   74: astore 8
    //   76: aload 6
    //   78: aload 8
    //   80: invokevirtual 33	com/truecaller/truepay/data/provider/a/d:a	(Landroid/content/ContentResolver;)Lcom/truecaller/truepay/data/provider/a/c;
    //   83: astore 6
    //   85: aconst_null
    //   86: astore 8
    //   88: aload 6
    //   90: invokevirtual 39	com/truecaller/truepay/data/provider/a/c:getCount	()I
    //   93: istore 9
    //   95: iload 9
    //   97: ifle +110 -> 207
    //   100: new 202	java/util/HashSet
    //   103: astore 10
    //   105: aload 10
    //   107: invokespecial 203	java/util/HashSet:<init>	()V
    //   110: aload 6
    //   112: invokevirtual 43	com/truecaller/truepay/data/provider/a/c:moveToFirst	()Z
    //   115: pop
    //   116: aload 6
    //   118: invokevirtual 46	com/truecaller/truepay/data/provider/a/c:isAfterLast	()Z
    //   121: istore 11
    //   123: iload 11
    //   125: ifne +33 -> 158
    //   128: ldc 59
    //   130: astore 12
    //   132: aload 6
    //   134: aload 12
    //   136: invokevirtual 54	com/truecaller/truepay/data/provider/a/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   139: astore 12
    //   141: aload 10
    //   143: aload 12
    //   145: invokevirtual 207	java/util/HashSet:add	(Ljava/lang/Object;)Z
    //   148: pop
    //   149: aload 6
    //   151: invokevirtual 86	com/truecaller/truepay/data/provider/a/c:moveToNext	()Z
    //   154: pop
    //   155: goto -39 -> 116
    //   158: getstatic 213	java/util/Locale:ENGLISH	Ljava/util/Locale;
    //   161: astore 12
    //   163: ldc -41
    //   165: astore 13
    //   167: iload 7
    //   169: anewarray 4	java/lang/Object
    //   172: astore 14
    //   174: iload 5
    //   176: invokestatic 163	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   179: astore 15
    //   181: aload 14
    //   183: iconst_0
    //   184: aload 15
    //   186: aastore
    //   187: aload 12
    //   189: aload 13
    //   191: aload 14
    //   193: invokestatic 219	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   196: astore 15
    //   198: aload_3
    //   199: aload 15
    //   201: aload 10
    //   203: invokevirtual 223	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   206: pop
    //   207: aload 6
    //   209: ifnull +70 -> 279
    //   212: aload 6
    //   214: invokevirtual 91	com/truecaller/truepay/data/provider/a/c:close	()V
    //   217: goto +62 -> 279
    //   220: astore 15
    //   222: goto +12 -> 234
    //   225: astore 15
    //   227: aload 15
    //   229: astore 8
    //   231: aload 15
    //   233: athrow
    //   234: aload 6
    //   236: ifnull +33 -> 269
    //   239: aload 8
    //   241: ifnull +23 -> 264
    //   244: aload 6
    //   246: invokevirtual 91	com/truecaller/truepay/data/provider/a/c:close	()V
    //   249: goto +20 -> 269
    //   252: astore 6
    //   254: aload 8
    //   256: aload 6
    //   258: invokevirtual 97	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   261: goto +8 -> 269
    //   264: aload 6
    //   266: invokevirtual 91	com/truecaller/truepay/data/provider/a/c:close	()V
    //   269: aload 15
    //   271: athrow
    //   272: astore 15
    //   274: aload 15
    //   276: invokestatic 226	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   279: iload 4
    //   281: iconst_1
    //   282: iadd
    //   283: istore 4
    //   285: goto -260 -> 25
    //   288: aload_3
    //   289: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	290	0	this	g
    //   1	28	1	i	int
    //   5	27	2	arrayOfInt	int[]
    //   17	272	3	localHashMap	java.util.HashMap
    //   23	261	4	j	int
    //   35	140	5	k	int
    //   40	205	6	localObject1	Object
    //   252	13	6	localThrowable	Throwable
    //   48	120	7	m	int
    //   54	201	8	localObject2	Object
    //   93	3	9	n	int
    //   103	99	10	localHashSet	java.util.HashSet
    //   121	3	11	bool	boolean
    //   130	58	12	localObject3	Object
    //   165	25	13	str	String
    //   172	20	14	arrayOfObject	Object[]
    //   179	21	15	localObject4	Object
    //   220	1	15	localObject5	Object
    //   225	45	15	localObject6	Object
    //   272	3	15	localException	Exception
    // Exception table:
    //   from	to	target	type
    //   231	234	220	finally
    //   88	93	225	finally
    //   100	103	225	finally
    //   105	110	225	finally
    //   110	116	225	finally
    //   116	121	225	finally
    //   134	139	225	finally
    //   143	149	225	finally
    //   149	155	225	finally
    //   158	161	225	finally
    //   167	172	225	finally
    //   174	179	225	finally
    //   184	187	225	finally
    //   191	196	225	finally
    //   201	207	225	finally
    //   244	249	252	finally
    //   70	74	272	java/lang/Exception
    //   78	83	272	java/lang/Exception
    //   212	217	272	java/lang/Exception
    //   256	261	272	java/lang/Exception
    //   264	269	272	java/lang/Exception
    //   269	272	272	java/lang/Exception
  }
  
  /* Error */
  public final java.util.HashMap b()
  {
    // Byte code:
    //   0: new 196	java/util/HashMap
    //   3: astore_1
    //   4: aload_1
    //   5: invokespecial 197	java/util/HashMap:<init>	()V
    //   8: new 16	com/truecaller/truepay/data/provider/a/d
    //   11: astore_2
    //   12: aload_2
    //   13: invokespecial 17	com/truecaller/truepay/data/provider/a/d:<init>	()V
    //   16: aload_2
    //   17: invokevirtual 229	com/truecaller/truepay/data/provider/a/d:b	()Lcom/truecaller/truepay/data/provider/a/d;
    //   20: pop
    //   21: aload_0
    //   22: getfield 14	com/truecaller/truepay/data/g/a/g:a	Landroid/content/ContentResolver;
    //   25: astore_3
    //   26: aload_2
    //   27: aload_3
    //   28: invokevirtual 33	com/truecaller/truepay/data/provider/a/d:a	(Landroid/content/ContentResolver;)Lcom/truecaller/truepay/data/provider/a/c;
    //   31: astore_2
    //   32: aconst_null
    //   33: astore_3
    //   34: aload_2
    //   35: invokevirtual 39	com/truecaller/truepay/data/provider/a/c:getCount	()I
    //   38: istore 4
    //   40: iload 4
    //   42: ifle +90 -> 132
    //   45: aload_2
    //   46: invokevirtual 43	com/truecaller/truepay/data/provider/a/c:moveToFirst	()Z
    //   49: istore 4
    //   51: iload 4
    //   53: ifeq +79 -> 132
    //   56: ldc 59
    //   58: astore 5
    //   60: ldc 59
    //   62: astore 6
    //   64: aload_2
    //   65: aload 6
    //   67: invokevirtual 54	com/truecaller/truepay/data/provider/a/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   70: astore 6
    //   72: aload_1
    //   73: aload 5
    //   75: aload 6
    //   77: invokevirtual 223	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   80: pop
    //   81: ldc 51
    //   83: astore 5
    //   85: ldc 51
    //   87: astore 6
    //   89: aload_2
    //   90: aload 6
    //   92: invokevirtual 54	com/truecaller/truepay/data/provider/a/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   95: astore 6
    //   97: aload_1
    //   98: aload 5
    //   100: aload 6
    //   102: invokevirtual 223	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   105: pop
    //   106: ldc -25
    //   108: astore 5
    //   110: aload_2
    //   111: invokevirtual 64	com/truecaller/truepay/data/provider/a/c:a	()I
    //   114: istore 7
    //   116: iload 7
    //   118: invokestatic 163	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   121: astore 6
    //   123: aload_1
    //   124: aload 5
    //   126: aload 6
    //   128: invokevirtual 223	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   131: pop
    //   132: aload_2
    //   133: ifnull +54 -> 187
    //   136: aload_2
    //   137: invokevirtual 91	com/truecaller/truepay/data/provider/a/c:close	()V
    //   140: goto +47 -> 187
    //   143: astore 5
    //   145: goto +6 -> 151
    //   148: astore_3
    //   149: aload_3
    //   150: athrow
    //   151: aload_2
    //   152: ifnull +27 -> 179
    //   155: aload_3
    //   156: ifnull +19 -> 175
    //   159: aload_2
    //   160: invokevirtual 91	com/truecaller/truepay/data/provider/a/c:close	()V
    //   163: goto +16 -> 179
    //   166: astore_2
    //   167: aload_3
    //   168: aload_2
    //   169: invokevirtual 97	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   172: goto +7 -> 179
    //   175: aload_2
    //   176: invokevirtual 91	com/truecaller/truepay/data/provider/a/c:close	()V
    //   179: aload 5
    //   181: athrow
    //   182: astore_2
    //   183: aload_2
    //   184: invokestatic 226	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   187: aload_1
    //   188: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	189	0	this	g
    //   3	185	1	localHashMap	java.util.HashMap
    //   11	149	2	localObject1	Object
    //   166	10	2	localThrowable	Throwable
    //   182	2	2	localException	Exception
    //   25	9	3	localContentResolver	ContentResolver
    //   148	20	3	localObject2	Object
    //   38	3	4	i	int
    //   49	3	4	bool	boolean
    //   58	67	5	str	String
    //   143	37	5	localObject3	Object
    //   62	65	6	localObject4	Object
    //   114	3	7	j	int
    // Exception table:
    //   from	to	target	type
    //   149	151	143	finally
    //   34	38	148	finally
    //   45	49	148	finally
    //   65	70	148	finally
    //   75	81	148	finally
    //   90	95	148	finally
    //   100	106	148	finally
    //   110	114	148	finally
    //   116	121	148	finally
    //   126	132	148	finally
    //   159	163	166	finally
    //   21	25	182	java/lang/Exception
    //   27	31	182	java/lang/Exception
    //   136	140	182	java/lang/Exception
    //   168	172	182	java/lang/Exception
    //   175	179	182	java/lang/Exception
    //   179	182	182	java/lang/Exception
  }
  
  /* Error */
  public final java.util.List c()
  {
    // Byte code:
    //   0: new 233	java/util/ArrayList
    //   3: astore_1
    //   4: aload_1
    //   5: invokespecial 234	java/util/ArrayList:<init>	()V
    //   8: new 16	com/truecaller/truepay/data/provider/a/d
    //   11: astore_2
    //   12: aload_2
    //   13: invokespecial 17	com/truecaller/truepay/data/provider/a/d:<init>	()V
    //   16: aload_2
    //   17: invokevirtual 229	com/truecaller/truepay/data/provider/a/d:b	()Lcom/truecaller/truepay/data/provider/a/d;
    //   20: pop
    //   21: aload_0
    //   22: getfield 14	com/truecaller/truepay/data/g/a/g:a	Landroid/content/ContentResolver;
    //   25: astore_3
    //   26: aload_2
    //   27: aload_3
    //   28: invokevirtual 33	com/truecaller/truepay/data/provider/a/d:a	(Landroid/content/ContentResolver;)Lcom/truecaller/truepay/data/provider/a/c;
    //   31: astore_2
    //   32: aconst_null
    //   33: astore_3
    //   34: aload_2
    //   35: invokevirtual 39	com/truecaller/truepay/data/provider/a/c:getCount	()I
    //   38: istore 4
    //   40: iload 4
    //   42: ifle +102 -> 144
    //   45: aload_2
    //   46: invokevirtual 43	com/truecaller/truepay/data/provider/a/c:moveToFirst	()Z
    //   49: istore 4
    //   51: iload 4
    //   53: ifeq +91 -> 144
    //   56: new 48	com/truecaller/truepay/SmsBankData$a
    //   59: astore 5
    //   61: aload 5
    //   63: invokespecial 49	com/truecaller/truepay/SmsBankData$a:<init>	()V
    //   66: ldc 51
    //   68: astore 6
    //   70: aload_2
    //   71: aload 6
    //   73: invokevirtual 54	com/truecaller/truepay/data/provider/a/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   76: astore 6
    //   78: aload 5
    //   80: aload 6
    //   82: putfield 57	com/truecaller/truepay/SmsBankData$a:b	Ljava/lang/String;
    //   85: ldc 59
    //   87: astore 6
    //   89: aload_2
    //   90: aload 6
    //   92: invokevirtual 54	com/truecaller/truepay/data/provider/a/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   95: astore 6
    //   97: aload 5
    //   99: aload 6
    //   101: putfield 62	com/truecaller/truepay/SmsBankData$a:c	Ljava/lang/String;
    //   104: aload_2
    //   105: invokevirtual 64	com/truecaller/truepay/data/provider/a/c:a	()I
    //   108: istore 7
    //   110: aload 5
    //   112: iload 7
    //   114: putfield 67	com/truecaller/truepay/SmsBankData$a:a	I
    //   117: aload 5
    //   119: invokevirtual 78	com/truecaller/truepay/SmsBankData$a:a	()Lcom/truecaller/truepay/SmsBankData;
    //   122: astore 5
    //   124: aload_1
    //   125: aload 5
    //   127: invokeinterface 237 2 0
    //   132: pop
    //   133: aload_2
    //   134: invokevirtual 86	com/truecaller/truepay/data/provider/a/c:moveToNext	()Z
    //   137: istore 4
    //   139: iload 4
    //   141: ifne -85 -> 56
    //   144: aload_2
    //   145: ifnull +54 -> 199
    //   148: aload_2
    //   149: invokevirtual 91	com/truecaller/truepay/data/provider/a/c:close	()V
    //   152: goto +47 -> 199
    //   155: astore 5
    //   157: goto +6 -> 163
    //   160: astore_3
    //   161: aload_3
    //   162: athrow
    //   163: aload_2
    //   164: ifnull +27 -> 191
    //   167: aload_3
    //   168: ifnull +19 -> 187
    //   171: aload_2
    //   172: invokevirtual 91	com/truecaller/truepay/data/provider/a/c:close	()V
    //   175: goto +16 -> 191
    //   178: astore_2
    //   179: aload_3
    //   180: aload_2
    //   181: invokevirtual 97	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   184: goto +7 -> 191
    //   187: aload_2
    //   188: invokevirtual 91	com/truecaller/truepay/data/provider/a/c:close	()V
    //   191: aload 5
    //   193: athrow
    //   194: astore_2
    //   195: aload_2
    //   196: invokestatic 226	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   199: aload_1
    //   200: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	201	0	this	g
    //   3	197	1	localArrayList	java.util.ArrayList
    //   11	161	2	localObject1	Object
    //   178	10	2	localThrowable	Throwable
    //   194	2	2	localException	Exception
    //   25	9	3	localContentResolver	ContentResolver
    //   160	20	3	localObject2	Object
    //   38	3	4	i	int
    //   49	91	4	bool	boolean
    //   59	67	5	localObject3	Object
    //   155	37	5	localObject4	Object
    //   68	32	6	str	String
    //   108	5	7	j	int
    // Exception table:
    //   from	to	target	type
    //   161	163	155	finally
    //   34	38	160	finally
    //   45	49	160	finally
    //   56	59	160	finally
    //   61	66	160	finally
    //   71	76	160	finally
    //   80	85	160	finally
    //   90	95	160	finally
    //   99	104	160	finally
    //   104	108	160	finally
    //   112	117	160	finally
    //   117	122	160	finally
    //   125	133	160	finally
    //   133	137	160	finally
    //   171	175	178	finally
    //   21	25	194	java/lang/Exception
    //   27	31	194	java/lang/Exception
    //   148	152	194	java/lang/Exception
    //   180	184	194	java/lang/Exception
    //   187	191	194	java/lang/Exception
    //   191	194	194	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.g.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */