package com.truecaller.truepay.data.g.a;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import com.truecaller.truepay.app.ui.transaction.b.b.a;
import com.truecaller.truepay.data.f.i;
import io.reactivex.a;
import io.reactivex.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class f
  implements i
{
  final ContentResolver a;
  
  public f(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  public final o a()
  {
    f.1 local1 = new com/truecaller/truepay/data/g/a/f$1;
    local1.<init>(this);
    return o.a(local1);
  }
  
  public final o a(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    com.truecaller.truepay.data.provider.c.b localb = new com/truecaller/truepay/data/provider/c/b;
    localb.<init>();
    Object localObject1 = g;
    localObject1 = localb.g((String)localObject1);
    Object localObject2 = c;
    localObject1 = ((com.truecaller.truepay.data.provider.c.b)localObject1).a((Boolean)localObject2);
    localObject2 = f;
    localObject1 = ((com.truecaller.truepay.data.provider.c.b)localObject1).f((String)localObject2);
    localObject2 = j;
    localObject1 = ((com.truecaller.truepay.data.provider.c.b)localObject1).i((String)localObject2);
    localObject2 = h;
    localObject1 = ((com.truecaller.truepay.data.provider.c.b)localObject1).h((String)localObject2);
    localObject2 = e;
    localObject1 = ((com.truecaller.truepay.data.provider.c.b)localObject1).e((String)localObject2);
    localObject2 = k;
    localObject1 = ((com.truecaller.truepay.data.provider.c.b)localObject1).k((String)localObject2);
    localObject2 = d;
    localObject1 = ((com.truecaller.truepay.data.provider.c.b)localObject1).d((String)localObject2);
    localObject2 = i;
    localObject1 = ((com.truecaller.truepay.data.provider.c.b)localObject1).j((String)localObject2);
    localObject2 = b;
    localObject1 = ((com.truecaller.truepay.data.provider.c.b)localObject1).b((String)localObject2);
    localObject2 = a;
    ((com.truecaller.truepay.data.provider.c.b)localObject1).a((String)localObject2);
    localObject1 = a;
    localb.a((ContentResolver)localObject1);
    return o.a(paramb);
  }
  
  public final o a(String paramString)
  {
    com.truecaller.truepay.data.provider.c.d locald = new com/truecaller/truepay/data/provider/c/d;
    locald.<init>();
    int i = 1;
    String[] arrayOfString = new String[i];
    arrayOfString[0] = paramString;
    locald.f(arrayOfString);
    paramString = a;
    int j = locald.b(paramString);
    if (j != 0)
    {
      paramString = new com/truecaller/truepay/app/ui/transaction/b/b$a;
      paramString.<init>();
      return o.a(paramString.a());
    }
    return null;
  }
  
  public final void a(List paramList)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    paramList = paramList.iterator();
    Object localObject4;
    for (;;)
    {
      bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (com.truecaller.truepay.app.ui.transaction.b.b)paramList.next();
      localObject3 = new com/truecaller/truepay/data/provider/c/b;
      ((com.truecaller.truepay.data.provider.c.b)localObject3).<init>();
      localObject4 = g;
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)localObject3).g((String)localObject4);
      Object localObject5 = c;
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)localObject4).a((Boolean)localObject5);
      localObject5 = f;
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)localObject4).f((String)localObject5);
      localObject5 = j;
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)localObject4).i((String)localObject5);
      localObject5 = h;
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)localObject4).h((String)localObject5);
      localObject5 = e;
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)localObject4).e((String)localObject5);
      localObject5 = k;
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)localObject4).k((String)localObject5);
      localObject5 = d;
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)localObject4).d((String)localObject5);
      localObject5 = i;
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)localObject4).j((String)localObject5);
      localObject5 = b;
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)localObject4).b((String)localObject5);
      localObject2 = a;
      ((com.truecaller.truepay.data.provider.c.b)localObject4).a((String)localObject2);
      ((List)localObject1).add(localObject3);
    }
    int j = ((List)localObject1).size();
    paramList = new ContentValues[j];
    boolean bool1 = false;
    Object localObject2 = null;
    int k = 0;
    Object localObject3 = null;
    for (;;)
    {
      int m = ((List)localObject1).size();
      if (k >= m) {
        break;
      }
      localObject4 = ((com.truecaller.truepay.data.provider.c.b)((List)localObject1).get(k)).c();
      paramList[k] = localObject4;
      k += 1;
    }
    boolean bool2 = ((List)localObject1).isEmpty();
    int i;
    if (!bool2)
    {
      localObject3 = a;
      localObject1 = ((com.truecaller.truepay.data.provider.c.b)((List)localObject1).get(0)).b();
      i = ((ContentResolver)localObject3).bulkInsert((Uri)localObject1, paramList);
    }
    if (i != 0)
    {
      paramList = "Save benfies Successfull";
      new String[1][0] = paramList;
    }
  }
  
  public final io.reactivex.d b(String paramString)
  {
    f.2 local2 = new com/truecaller/truepay/data/g/a/f$2;
    local2.<init>(this, paramString);
    paramString = a.c;
    return io.reactivex.d.a(local2, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.g.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */