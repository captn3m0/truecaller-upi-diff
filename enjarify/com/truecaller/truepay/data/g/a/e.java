package com.truecaller.truepay.data.g.a;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.history.models.i;
import com.truecaller.truepay.app.ui.history.models.k;
import com.truecaller.truepay.app.ui.history.models.n;
import com.truecaller.truepay.app.utils.j;
import com.truecaller.truepay.data.e.b;
import com.truecaller.truepay.data.f.w;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class e
  implements w
{
  private com.truecaller.truepay.data.c.g a;
  private ContentResolver b;
  private final com.truecaller.truepay.data.provider.e.a c;
  private String d;
  private b e;
  
  public e(com.truecaller.truepay.data.c.g paramg, ContentResolver paramContentResolver, b paramb, com.truecaller.truepay.data.provider.e.a parama)
  {
    a = paramg;
    b = paramContentResolver;
    c = parama;
    e = paramb;
  }
  
  private static String a(int paramInt, h paramh)
  {
    Object localObject = a;
    if (localObject != null)
    {
      localObject = a;
      boolean bool = ((List)localObject).isEmpty();
      if (!bool)
      {
        localObject = a;
        int i = ((List)localObject).size();
        if (i >= paramInt)
        {
          localObject = a.get(paramInt);
          if (localObject != null) {
            return a.get(paramInt)).b;
          }
        }
      }
    }
    return null;
  }
  
  private static String b(int paramInt, h paramh)
  {
    Object localObject = a;
    if (localObject != null)
    {
      localObject = a;
      boolean bool = ((List)localObject).isEmpty();
      if (!bool)
      {
        localObject = a;
        int i = ((List)localObject).size();
        if (i >= paramInt)
        {
          localObject = a.get(paramInt);
          if (localObject != null) {
            return a.get(paramInt)).a;
          }
        }
      }
    }
    return null;
  }
  
  public final int a()
  {
    com.truecaller.truepay.data.provider.e.g localg = new com/truecaller/truepay/data/provider/e/g;
    localg.<init>();
    ContentResolver localContentResolver = b;
    return localg.c(localContentResolver);
  }
  
  public final io.reactivex.o a(i parami, String paramString)
  {
    parami = new com/truecaller/truepay/data/g/a/-$$Lambda$e$IaQAMF9PfqYKIEUf3V3-aRcbu3o;
    parami.<init>(this, paramString);
    return io.reactivex.o.a(parami);
  }
  
  public final void a(List paramList)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    int i = paramList.isEmpty();
    int k = 0;
    Object localObject3;
    Object localObject4;
    if (i == 0)
    {
      localObject2 = e;
      localObject3 = get0b;
      localObject4 = "yyyy-MM-dd HH:mm:ss Z";
      long l1 = j.a((String)localObject3, (String)localObject4);
      localObject3 = Long.valueOf(l1);
      ((b)localObject2).a((Long)localObject3);
    }
    paramList = paramList.iterator();
    for (;;)
    {
      i = paramList.hasNext();
      if (i == 0) {
        break;
      }
      localObject2 = (h)paramList.next();
      localObject3 = new com/truecaller/truepay/data/provider/e/e;
      ((com.truecaller.truepay.data.provider.e.e)localObject3).<init>();
      double d1 = Double.parseDouble(l);
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject3).a(d1);
      Object localObject5 = f;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).q((String)localObject5);
      localObject5 = m;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).p((String)localObject5);
      localObject5 = r;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).r((String)localObject5);
      localObject5 = p.f;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).i((String)localObject5);
      localObject5 = p.e;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).k((String)localObject5);
      localObject5 = p.h;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).l((String)localObject5);
      localObject5 = p.d;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).e((String)localObject5);
      localObject5 = k;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).a((String)localObject5);
      localObject5 = j;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).d((String)localObject5);
      localObject5 = n;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).b((String)localObject5);
      localObject5 = p.a;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).f((String)localObject5);
      localObject5 = p.c;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).g((String)localObject5);
      localObject5 = i;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).h((String)localObject5);
      localObject5 = e;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).j((String)localObject5);
      localObject5 = j.a(b);
      int m = 1;
      if (localObject5 == null)
      {
        localObject5 = new String[m];
        Object localObject6 = new java/lang/StringBuilder;
        ((StringBuilder)localObject6).<init>("Malformed trnx time received from server for trnx id ");
        String str = j;
        ((StringBuilder)localObject6).append(str);
        ((StringBuilder)localObject6).append(": ");
        str = b;
        ((StringBuilder)localObject6).append(str);
        localObject6 = ((StringBuilder)localObject6).toString();
        localObject5[0] = localObject6;
        localObject5 = new java/util/Date;
        l2 = 0L;
        ((Date)localObject5).<init>(l2);
      }
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).a((Date)localObject5);
      localObject5 = p.g;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).m((String)localObject5);
      localObject5 = g;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).o((String)localObject5);
      localObject5 = m;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).p((String)localObject5);
      localObject5 = f;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).q((String)localObject5);
      localObject5 = h;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).n((String)localObject5);
      localObject5 = d;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).s((String)localObject5);
      localObject5 = o.a;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).t((String)localObject5);
      localObject5 = o.b;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).u((String)localObject5);
      localObject5 = o.c;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).v((String)localObject5);
      localObject5 = o.d;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).w((String)localObject5);
      localObject5 = o.g;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).x((String)localObject5);
      localObject5 = o.f;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).y((String)localObject5);
      localObject5 = o.e;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).z((String)localObject5);
      long l2 = v;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).a(l2);
      localObject5 = s;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).B((String)localObject5);
      localObject5 = t;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).C((String)localObject5);
      localObject5 = c;
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).A((String)localObject5);
      localObject5 = b(0, (h)localObject2);
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).D((String)localObject5);
      localObject5 = b(m, (h)localObject2);
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).F((String)localObject5);
      localObject5 = a(0, (h)localObject2);
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).E((String)localObject5);
      localObject5 = a(m, (h)localObject2);
      localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).G((String)localObject5);
      localObject5 = u;
      ((com.truecaller.truepay.data.provider.e.e)localObject4).ae((String)localObject5);
      localObject4 = w;
      if (localObject4 != null)
      {
        localObject4 = w.a;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject3).H((String)localObject4);
        localObject5 = w.j;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).Q((String)localObject5);
        localObject5 = w.k;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).R((String)localObject5);
        localObject5 = w.h;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).O((String)localObject5);
        localObject5 = w.i;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).P((String)localObject5);
        localObject5 = w.d;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).K((String)localObject5);
        localObject5 = w.b;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).I((String)localObject5);
        localObject5 = w.c;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).J((String)localObject5);
        localObject5 = w.e;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).L((String)localObject5);
        localObject5 = w.p;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).W((String)localObject5);
        localObject5 = w.o;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).V((String)localObject5);
        localObject5 = w.l;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).S((String)localObject5);
        localObject5 = w.n;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).U((String)localObject5);
        localObject5 = w.g;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).N((String)localObject5);
        localObject5 = w.q;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).X((String)localObject5);
        localObject5 = w.r;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).Y((String)localObject5);
        localObject5 = w.f;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).M((String)localObject5);
        localObject5 = w.m;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).T((String)localObject5);
        localObject5 = w.s;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).Z((String)localObject5);
        localObject5 = w.t;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).aa((String)localObject5);
        localObject5 = w.u;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).ab((String)localObject5);
        localObject5 = w.v;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).ac((String)localObject5);
        localObject5 = w.w;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).ad((String)localObject5);
        localObject5 = w.z;
        localObject4 = ((com.truecaller.truepay.data.provider.e.e)localObject4).ah((String)localObject5);
        localObject5 = w.y;
        ((com.truecaller.truepay.data.provider.e.e)localObject4).ag((String)localObject5);
        localObject4 = w.x;
        if (localObject4 != null)
        {
          localObject2 = w.x;
          ((com.truecaller.truepay.data.provider.e.e)localObject3).af((String)localObject2);
        }
      }
      ((List)localObject1).add(localObject3);
    }
    int n = ((List)localObject1).size();
    paramList = new ContentValues[n];
    i = 0;
    Object localObject2 = null;
    for (;;)
    {
      int i1 = ((List)localObject1).size();
      if (i >= i1) {
        break;
      }
      localObject3 = ((com.truecaller.truepay.data.provider.e.e)((List)localObject1).get(i)).c();
      paramList[i] = localObject3;
      int j;
      i += 1;
    }
    boolean bool = ((List)localObject1).isEmpty();
    if (!bool)
    {
      localObject2 = b;
      localObject1 = ((com.truecaller.truepay.data.provider.e.e)((List)localObject1).get(0)).b();
      k = ((ContentResolver)localObject2).bulkInsert((Uri)localObject1, paramList);
    }
    if (k != 0)
    {
      paramList = "Save transaction logs Successfull";
      new String[1][0] = paramList;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.g.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */