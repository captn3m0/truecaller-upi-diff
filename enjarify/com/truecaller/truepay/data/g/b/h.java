package com.truecaller.truepay.data.g.b;

import com.truecaller.truepay.app.ui.transaction.b.d;
import com.truecaller.truepay.app.ui.transaction.b.j;
import com.truecaller.truepay.data.api.g;
import com.truecaller.truepay.data.api.model.aa;
import com.truecaller.truepay.data.api.model.ab;
import com.truecaller.truepay.data.api.model.ah;
import com.truecaller.truepay.data.api.model.u;
import com.truecaller.truepay.data.d.c;
import com.truecaller.truepay.data.d.c.a;
import com.truecaller.truepay.data.f.z;
import io.reactivex.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class h
  implements z
{
  g a;
  
  public h(g paramg)
  {
    a = paramg;
  }
  
  private static List a(com.truecaller.truepay.data.api.model.h paramh)
  {
    Object localObject1 = ca;
    if (localObject1 != null)
    {
      localObject1 = new java/util/ArrayList;
      int i = ca.size();
      ((ArrayList)localObject1).<init>(i);
      Iterator localIterator = ca.iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        Object localObject2 = (ab)localIterator.next();
        Object localObject3 = new com/truecaller/truepay/data/d/c$a;
        ((c.a)localObject3).<init>();
        Object localObject4 = d;
        a = ((String)localObject4);
        localObject4 = a;
        i = ((String)localObject4);
        localObject4 = k;
        e = ((String)localObject4);
        localObject4 = h;
        g = ((String)localObject4);
        localObject4 = c;
        k = ((String)localObject4);
        localObject4 = i;
        b = ((String)localObject4);
        localObject4 = g;
        j = ((String)localObject4);
        localObject4 = e;
        d = ((String)localObject4);
        localObject4 = l;
        c = ((String)localObject4);
        localObject4 = j;
        h = ((String)localObject4);
        localObject4 = f;
        f = ((String)localObject4);
        localObject4 = e;
        d = ((String)localObject4);
        localObject2 = b;
        l = ((String)localObject2);
        localObject2 = new com/truecaller/truepay/data/d/c;
        String str1 = a;
        String str2 = b;
        String str3 = c;
        String str4 = d;
        String str5 = e;
        String str6 = f;
        String str7 = g;
        String str8 = h;
        String str9 = i;
        String str10 = j;
        String str11 = k;
        localObject4 = d;
        localObject3 = l;
        Object localObject5 = localObject4;
        localObject4 = localObject2;
        ((c)localObject2).<init>(str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, (String)localObject5, (String)localObject3, (byte)0);
        ((List)localObject1).add(localObject2);
      }
      return (List)localObject1;
    }
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    return (List)localObject1;
  }
  
  public final o a()
  {
    o localo = a.d();
    -..Lambda.h.d5agDJuYStvLLYRFuVL8tTds3e8 locald5agDJuYStvLLYRFuVL8tTds3e8 = new com/truecaller/truepay/data/g/b/-$$Lambda$h$d5agDJuYStvLLYRFuVL8tTds3e8;
    locald5agDJuYStvLLYRFuVL8tTds3e8.<init>(this);
    return localo.a(locald5agDJuYStvLLYRFuVL8tTds3e8);
  }
  
  public final o a(d paramd)
  {
    return a.a(paramd);
  }
  
  public final o a(j paramj)
  {
    return a.a(paramj);
  }
  
  public final o a(ah paramah)
  {
    String str1 = a;
    String str2 = "reject_request";
    boolean bool = str1.equalsIgnoreCase(str2);
    if (bool) {
      return a.a(paramah);
    }
    str1 = a;
    str2 = "accept_request";
    bool = str1.equalsIgnoreCase(str2);
    if (bool) {
      return a.b(paramah);
    }
    new String[1][0] = "Invalid response to collect request";
    return null;
  }
  
  public final o a(u paramu)
  {
    return a.a(paramu);
  }
  
  public final o b(ah paramah)
  {
    return a.c(paramah);
  }
  
  public final o c(ah paramah)
  {
    return a.d(paramah);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.g.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */