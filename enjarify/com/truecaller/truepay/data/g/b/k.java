package com.truecaller.truepay.data.g.b;

import com.truecaller.truepay.data.api.j;
import com.truecaller.truepay.data.api.model.ResolveVpaRequestDO;
import com.truecaller.truepay.data.f.ac;
import io.reactivex.o;

public final class k
  implements ac
{
  private final j a;
  
  public k(j paramj)
  {
    a = paramj;
  }
  
  public final o a(ResolveVpaRequestDO paramResolveVpaRequestDO)
  {
    c.g.b.k.b(paramResolveVpaRequestDO, "vpaRequestDO");
    return a.a(paramResolveVpaRequestDO);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.g.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */