package com.truecaller.truepay.data.c;

import com.truecaller.truepay.app.ui.history.models.a;
import com.truecaller.truepay.app.ui.history.models.k;
import com.truecaller.truepay.app.ui.history.models.n;
import com.truecaller.truepay.app.ui.history.models.o;
import com.truecaller.truepay.data.api.model.s;
import com.truecaller.truepay.data.api.model.t;
import com.truecaller.truepay.data.provider.b.b;
import com.truecaller.truepay.data.provider.e.f;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class h
  implements g
{
  public final com.truecaller.truepay.app.ui.history.models.h a(f paramf)
  {
    Object localObject1 = paramf;
    com.truecaller.truepay.app.ui.history.models.h localh = new com/truecaller/truepay/app/ui/history/models/h;
    Object localObject2 = paramf.e("transaction_timestamp");
    if (localObject2 != null)
    {
      String str1 = com.truecaller.truepay.app.utils.j.a((Date)localObject2);
      String str2 = paramf.b("status");
      String str3 = paramf.b("bank_rrn");
      String str4 = paramf.b("remarks");
      String str5 = paramf.c();
      localObject2 = "transaction_type";
      String str6 = paramf.b((String)localObject2);
      if (str6 != null)
      {
        localObject2 = "transaction_id";
        String str7 = paramf.b((String)localObject2);
        if (str7 != null)
        {
          String str8 = paramf.b("receiver_user_id");
          localObject2 = "t_id";
          String str9 = paramf.b((String)localObject2);
          if (str9 != null)
          {
            localObject2 = "amount";
            int i = paramf.a((String)localObject2);
            boolean bool = paramf.isNull(i);
            double d;
            if (bool)
            {
              i = 0;
              localObject2 = null;
            }
            else
            {
              d = paramf.getDouble(i);
              localObject2 = Double.valueOf(d);
            }
            if (localObject2 != null)
            {
              d = ((Double)localObject2).doubleValue();
              String str10 = String.valueOf(d);
              String str11 = ((f)localObject1).b("category");
              localObject2 = "seq_no";
              Object localObject3 = ((f)localObject1).b((String)localObject2);
              if (localObject3 != null)
              {
                Object localObject4 = new com/truecaller/truepay/app/ui/history/models/n;
                ((n)localObject4).<init>();
                localObject2 = paramf.e();
                b = ((String)localObject2);
                localObject2 = ((f)localObject1).b("receiver_bank_name");
                g = ((String)localObject2);
                localObject2 = paramf.f();
                c = ((String)localObject2);
                localObject2 = paramf.d();
                a = ((String)localObject2);
                localObject2 = ((f)localObject1).b("receiver_sub_type");
                d = ((String)localObject2);
                localObject2 = ((f)localObject1).b("receiver_bank_acc_num");
                e = ((String)localObject2);
                Object localObject5 = ((f)localObject1).b("payment_flow");
                localObject2 = new java/util/ArrayList;
                ((ArrayList)localObject2).<init>();
                Object localObject6 = localObject5;
                localObject5 = new com/truecaller/truepay/app/ui/history/models/a;
                ((a)localObject5).<init>();
                Object localObject7 = localObject4;
                localObject4 = new com/truecaller/truepay/app/ui/history/models/a;
                ((a)localObject4).<init>();
                Object localObject8 = localObject3;
                localObject3 = ((f)localObject1).b("action_data_1_title");
                if (localObject3 != null)
                {
                  localObject3 = ((f)localObject1).b("action_data_1_type");
                  if (localObject3 != null)
                  {
                    localObject3 = ((f)localObject1).b("action_data_1_title");
                    a = ((String)localObject3);
                    localObject3 = ((f)localObject1).b("action_data_1_type");
                    b = ((String)localObject3);
                  }
                }
                ((List)localObject2).add(localObject5);
                localObject3 = ((f)localObject1).b("action_data_2_title");
                if (localObject3 != null)
                {
                  localObject3 = ((f)localObject1).b("action_data_2_type");
                  if (localObject3 != null)
                  {
                    localObject3 = ((f)localObject1).b("action_data_2_title");
                    a = ((String)localObject3);
                    localObject3 = ((f)localObject1).b("action_data_2_type");
                    b = ((String)localObject3);
                  }
                }
                ((List)localObject2).add(localObject4);
                Object localObject9 = ((f)localObject1).b("dispute_id");
                Object localObject10 = ((f)localObject1).b("dispute_status_check_at");
                localObject5 = new com/truecaller/truepay/app/ui/history/models/k;
                ((k)localObject5).<init>();
                localObject3 = ((f)localObject1).b("initiator_bank_acc_num");
                f = ((String)localObject3);
                localObject3 = ((f)localObject1).b("initiator_bank_id");
                e = ((String)localObject3);
                localObject3 = ((f)localObject1).b("initiator_bank_name");
                h = ((String)localObject3);
                localObject3 = ((f)localObject1).b("initiator_bank_symbol");
                g = ((String)localObject3);
                localObject3 = paramf.a();
                d = ((String)localObject3);
                localObject3 = paramf.b();
                c = ((String)localObject3);
                localObject3 = ((f)localObject1).b("initiator_user_id");
                a = ((String)localObject3);
                Object localObject11 = ((f)localObject1).b("sr_number");
                Object localObject12 = paramf.g();
                String str12 = paramf.h();
                localObject3 = ((f)localObject1).e("updated_at");
                if (localObject3 != null)
                {
                  long l = ((Date)localObject3).getTime() / 1000L;
                  localObject3 = new com/truecaller/truepay/app/ui/history/models/o;
                  Object localObject13 = localObject3;
                  ((o)localObject3).<init>();
                  localObject4 = ((f)localObject1).b("utility_initiated_at");
                  a = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_loc_name");
                  j = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_location_code");
                  k = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_operator_name");
                  h = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_operator_type");
                  i = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_recharge_message");
                  d = ((String)localObject4);
                  localObject4 = paramf.i();
                  b = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_ref_id");
                  e = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_refund_message");
                  p = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_refund_status");
                  m = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_refund_rrn");
                  l = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_refund_time");
                  n = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_type");
                  f = ((String)localObject4);
                  localObject4 = paramf.j();
                  q = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_vendor_trnx_id");
                  g = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_recharge_status");
                  c = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_refund_response_code");
                  o = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("operator_symbol");
                  r = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("bbps_transaction_id");
                  s = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("operator_icon_url");
                  t = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("operator_logo_url");
                  u = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("vendor_logo_url");
                  v = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("bill_due_date");
                  w = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_booking_info");
                  x = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_remarks");
                  y = ((String)localObject4);
                  localObject4 = ((f)localObject1).b("utility_email");
                  z = ((String)localObject4);
                  String str13 = ((f)localObject1).b("repeat_transaction_params");
                  localObject1 = localObject2;
                  localObject2 = localh;
                  localObject3 = localObject8;
                  localObject4 = localObject7;
                  Object localObject14 = localObject5;
                  localObject5 = localObject6;
                  localObject6 = localObject1;
                  localObject7 = localObject9;
                  localObject8 = localObject10;
                  localObject9 = localObject14;
                  localObject10 = localObject11;
                  localObject11 = localObject12;
                  localObject12 = str12;
                  localh.<init>(str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, (String)localObject3, (n)localObject4, (String)localObject5, (List)localObject1, (String)localObject7, (String)localObject8, (k)localObject14, (String)localObject10, (String)localObject11, str12, l, (o)localObject13, str13);
                  return localh;
                }
                localObject1 = new java/lang/NullPointerException;
                ((NullPointerException)localObject1).<init>("The value of 'updated_at' in the database was null, which is not allowed according to the model definition");
                throw ((Throwable)localObject1);
              }
              localObject1 = new java/lang/NullPointerException;
              ((NullPointerException)localObject1).<init>("The value of 'seq_no' in the database was null, which is not allowed according to the model definition");
              throw ((Throwable)localObject1);
            }
            localObject1 = new java/lang/NullPointerException;
            ((NullPointerException)localObject1).<init>("The value of 'amount' in the database was null, which is not allowed according to the model definition");
            throw ((Throwable)localObject1);
          }
          localObject1 = new java/lang/NullPointerException;
          ((NullPointerException)localObject1).<init>("The value of 't_id' in the database was null, which is not allowed according to the model definition");
          throw ((Throwable)localObject1);
        }
        localObject1 = new java/lang/NullPointerException;
        ((NullPointerException)localObject1).<init>("The value of 'transaction_id' in the database was null, which is not allowed according to the model definition");
        throw ((Throwable)localObject1);
      }
      localObject1 = new java/lang/NullPointerException;
      ((NullPointerException)localObject1).<init>("The value of 'transaction_type' in the database was null, which is not allowed according to the model definition");
      throw ((Throwable)localObject1);
    }
    localObject1 = new java/lang/NullPointerException;
    ((NullPointerException)localObject1).<init>("The value of 'transaction_timestamp' in the database was null, which is not allowed according to the model definition");
    throw ((Throwable)localObject1);
  }
  
  public final List a(t paramt)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = paramt.c();
    if (localObject2 != null)
    {
      localObject2 = ca;
      if (localObject2 != null) {
        for (localObject2 = ca.iterator();; localObject2 = paramt)
        {
          boolean bool = ((Iterator)localObject2).hasNext();
          if (!bool) {
            break;
          }
          com.truecaller.truepay.app.ui.history.models.j localj = (com.truecaller.truepay.app.ui.history.models.j)((Iterator)localObject2).next();
          Object localObject3 = new com/truecaller/truepay/app/ui/history/models/h;
          Object localObject4 = localObject3;
          String str1 = a;
          String str2 = b;
          String str3 = c;
          String str4 = e;
          String str5 = f;
          String str6 = g;
          String str7 = h;
          String str8 = i;
          String str9 = j;
          String str10 = k;
          String str11 = l;
          paramt = (t)localObject2;
          localObject2 = m;
          Object localObject5 = localObject1;
          localObject1 = localObject3;
          localObject3 = localObject2;
          n localn = n;
          String str12 = d;
          List localList = p;
          String str13 = q;
          String str14 = r;
          k localk = o;
          String str15 = s;
          String str16 = t;
          localObject2 = u;
          long l = v;
          o localo = x;
          localObject1 = w;
          ((com.truecaller.truepay.app.ui.history.models.h)localObject4).<init>(str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, (String)localObject3, localn, str12, localList, str13, str14, localk, str15, str16, (String)localObject2, l, localo, (String)localObject1);
          localObject1 = localObject5;
          localObject2 = localObject4;
          ((List)localObject5).add(localObject4);
        }
      }
    }
    return (List)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.c.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */