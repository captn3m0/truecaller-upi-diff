package com.truecaller.truepay.data.a;

import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d
{
  private final v a;
  private final Provider b;
  
  private y(v paramv, Provider paramProvider)
  {
    a = paramv;
    b = paramProvider;
  }
  
  public static y a(v paramv, Provider paramProvider)
  {
    y localy = new com/truecaller/truepay/data/a/y;
    localy.<init>(paramv, paramProvider);
    return localy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */