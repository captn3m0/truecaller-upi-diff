package com.truecaller.truepay.data.a;

import dagger.a.d;
import javax.inject.Provider;

public final class ag
  implements d
{
  private final ae a;
  private final Provider b;
  
  private ag(ae paramae, Provider paramProvider)
  {
    a = paramae;
    b = paramProvider;
  }
  
  public static ag a(ae paramae, Provider paramProvider)
  {
    ag localag = new com/truecaller/truepay/data/a/ag;
    localag.<init>(paramae, paramProvider);
    return localag;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */