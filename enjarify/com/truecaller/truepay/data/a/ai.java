package com.truecaller.truepay.data.a;

import dagger.a.d;
import javax.inject.Provider;

public final class ai
  implements d
{
  private final ae a;
  private final Provider b;
  
  private ai(ae paramae, Provider paramProvider)
  {
    a = paramae;
    b = paramProvider;
  }
  
  public static ai a(ae paramae, Provider paramProvider)
  {
    ai localai = new com/truecaller/truepay/data/a/ai;
    localai.<init>(paramae, paramProvider);
    return localai;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */