package com.truecaller.truepay.data.a;

import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d
{
  private final o a;
  private final Provider b;
  
  private s(o paramo, Provider paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static s a(o paramo, Provider paramProvider)
  {
    s locals = new com/truecaller/truepay/data/a/s;
    locals.<init>(paramo, paramProvider);
    return locals;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */