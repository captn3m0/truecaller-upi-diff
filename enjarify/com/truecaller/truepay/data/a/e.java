package com.truecaller.truepay.data.a;

import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d
{
  private final a a;
  private final Provider b;
  
  private e(a parama, Provider paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static e a(a parama, Provider paramProvider)
  {
    e locale = new com/truecaller/truepay/data/a/e;
    locale.<init>(parama, paramProvider);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */