package com.truecaller.truepay.data.a;

import c.g.b.k;
import com.truecaller.truepay.data.f.ag;
import com.truecaller.truepay.data.f.ah;
import dagger.a.d;
import javax.inject.Provider;

public final class ad
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ad(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ad a(ab paramab, Provider paramProvider)
  {
    ad localad = new com/truecaller/truepay/data/a/ad;
    localad.<init>(paramab, paramProvider);
    return localad;
  }
  
  public static ag a(com.truecaller.truepay.data.g.a.g paramg)
  {
    k.b(paramg, "smsDataSource");
    ag localag = new com/truecaller/truepay/data/f/ag;
    paramg = (ah)paramg;
    localag.<init>(paramg);
    return (ag)dagger.a.g.a(localag, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */