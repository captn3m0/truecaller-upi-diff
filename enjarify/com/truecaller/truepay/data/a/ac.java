package com.truecaller.truepay.data.a;

import android.content.Context;
import c.g.b.k;
import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ac(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ac a(ab paramab, Provider paramProvider)
  {
    ac localac = new com/truecaller/truepay/data/a/ac;
    localac.<init>(paramab, paramProvider);
    return localac;
  }
  
  public static com.truecaller.truepay.data.g.a.g a(Context paramContext)
  {
    k.b(paramContext, "context");
    com.truecaller.truepay.data.g.a.g localg = new com/truecaller/truepay/data/g/a/g;
    paramContext = paramContext.getContentResolver();
    localg.<init>(paramContext);
    return (com.truecaller.truepay.data.g.a.g)dagger.a.g.a(localg, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */