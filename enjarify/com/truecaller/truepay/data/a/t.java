package com.truecaller.truepay.data.a;

import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d
{
  private final o a;
  private final Provider b;
  
  private t(o paramo, Provider paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static t a(o paramo, Provider paramProvider)
  {
    t localt = new com/truecaller/truepay/data/a/t;
    localt.<init>(paramo, paramProvider);
    return localt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */