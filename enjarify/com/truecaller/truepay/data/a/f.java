package com.truecaller.truepay.data.a;

import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d
{
  private final a a;
  private final Provider b;
  private final Provider c;
  
  private f(a parama, Provider paramProvider1, Provider paramProvider2)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static f a(a parama, Provider paramProvider1, Provider paramProvider2)
  {
    f localf = new com/truecaller/truepay/data/a/f;
    localf.<init>(parama, paramProvider1, paramProvider2);
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */