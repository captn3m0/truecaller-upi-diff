package com.truecaller.truepay.data.a;

import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d
{
  private final ae a;
  private final Provider b;
  
  private ah(ae paramae, Provider paramProvider)
  {
    a = paramae;
    b = paramProvider;
  }
  
  public static ah a(ae paramae, Provider paramProvider)
  {
    ah localah = new com/truecaller/truepay/data/a/ah;
    localah.<init>(paramae, paramProvider);
    return localah;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */