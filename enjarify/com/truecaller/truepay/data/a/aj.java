package com.truecaller.truepay.data.a;

import dagger.a.d;
import javax.inject.Provider;

public final class aj
  implements d
{
  private final ae a;
  private final Provider b;
  private final Provider c;
  
  private aj(ae paramae, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramae;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static aj a(ae paramae, Provider paramProvider1, Provider paramProvider2)
  {
    aj localaj = new com/truecaller/truepay/data/a/aj;
    localaj.<init>(paramae, paramProvider1, paramProvider2);
    return localaj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.a.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */