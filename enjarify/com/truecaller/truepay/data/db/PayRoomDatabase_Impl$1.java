package com.truecaller.truepay.data.db;

import android.arch.persistence.room.b.b.a;
import android.arch.persistence.room.f.b;
import android.arch.persistence.room.h.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class PayRoomDatabase_Impl$1
  extends h.a
{
  PayRoomDatabase_Impl$1(PayRoomDatabase_Impl paramPayRoomDatabase_Impl)
  {
    super(3);
  }
  
  public final void a(android.arch.persistence.db.b paramb)
  {
    paramb.c("DROP TABLE IF EXISTS `new_badge_icon`");
    paramb.c("DROP TABLE IF EXISTS `bank_list`");
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    paramb.c("CREATE TABLE IF NOT EXISTS `new_badge_icon` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `utilityType` TEXT NOT NULL, `showBadge` INTEGER NOT NULL)");
    paramb.c("CREATE TABLE IF NOT EXISTS `bank_list` (`bank_symbol` TEXT NOT NULL, `account_provider_id` TEXT, `name` TEXT NOT NULL, `id` TEXT NOT NULL, `iin` TEXT, `is_popular` INTEGER NOT NULL, `popularity_index` TEXT NOT NULL, `upi_pin_required` INTEGER NOT NULL, `mandatory_psp` TEXT NOT NULL, `sim_index` INTEGER NOT NULL, `sms_count` INTEGER NOT NULL, `popular` INTEGER NOT NULL, PRIMARY KEY(`bank_symbol`))");
    paramb.c("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    paramb.c("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"15b6e4fd1dd6b5aa7b6ebb6914e2f642\")");
  }
  
  public final void c(android.arch.persistence.db.b paramb)
  {
    PayRoomDatabase_Impl.a(b, paramb);
    PayRoomDatabase_Impl.b(b, paramb);
    List localList1 = PayRoomDatabase_Impl.d(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = PayRoomDatabase_Impl.e(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)PayRoomDatabase_Impl.f(b).get(i);
        localb.b(paramb);
        i += 1;
      }
    }
  }
  
  public final void d(android.arch.persistence.db.b paramb)
  {
    List localList1 = PayRoomDatabase_Impl.a(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = PayRoomDatabase_Impl.b(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)PayRoomDatabase_Impl.c(b).get(i);
        localb.a(paramb);
        i += 1;
      }
    }
  }
  
  public final void e(android.arch.persistence.db.b paramb)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>(3);
    Object localObject2 = new android/arch/persistence/room/b/b$a;
    int i = 1;
    ((b.a)localObject2).<init>("id", "INTEGER", i, i);
    ((HashMap)localObject1).put("id", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("utilityType", "TEXT", i, 0);
    ((HashMap)localObject1).put("utilityType", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("showBadge", "INTEGER", i, 0);
    ((HashMap)localObject1).put("showBadge", localObject2);
    Object localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>(0);
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>(0);
    android.arch.persistence.room.b.b localb = new android/arch/persistence/room/b/b;
    String str = "new_badge_icon";
    localb.<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
    localObject1 = android.arch.persistence.room.b.b.a(paramb, "new_badge_icon");
    boolean bool1 = localb.equals(localObject1);
    if (bool1)
    {
      localObject1 = new java/util/HashMap;
      int j = 12;
      ((HashMap)localObject1).<init>(j);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("bank_symbol", "TEXT", i, i);
      ((HashMap)localObject1).put("bank_symbol", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("account_provider_id", "TEXT", false, 0);
      ((HashMap)localObject1).put("account_provider_id", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("name", "TEXT", i, 0);
      ((HashMap)localObject1).put("name", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("id", "TEXT", i, 0);
      ((HashMap)localObject1).put("id", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("iin", "TEXT", false, 0);
      ((HashMap)localObject1).put("iin", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("is_popular", "INTEGER", i, 0);
      ((HashMap)localObject1).put("is_popular", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("popularity_index", "TEXT", i, 0);
      ((HashMap)localObject1).put("popularity_index", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("upi_pin_required", "INTEGER", i, 0);
      ((HashMap)localObject1).put("upi_pin_required", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("mandatory_psp", "TEXT", i, 0);
      ((HashMap)localObject1).put("mandatory_psp", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("sim_index", "INTEGER", i, 0);
      ((HashMap)localObject1).put("sim_index", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("sms_count", "INTEGER", i, 0);
      ((HashMap)localObject1).put("sms_count", localObject2);
      localObject2 = new android/arch/persistence/room/b/b$a;
      ((b.a)localObject2).<init>("popular", "INTEGER", i, 0);
      ((HashMap)localObject1).put("popular", localObject2);
      localObject3 = new java/util/HashSet;
      ((HashSet)localObject3).<init>(0);
      localObject2 = new java/util/HashSet;
      ((HashSet)localObject2).<init>(0);
      localb = new android/arch/persistence/room/b/b;
      str = "bank_list";
      localb.<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
      localObject1 = "bank_list";
      paramb = android.arch.persistence.room.b.b.a(paramb, (String)localObject1);
      boolean bool2 = localb.equals(paramb);
      if (bool2) {
        return;
      }
      localObject1 = new java/lang/IllegalStateException;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Migration didn't properly handle bank_list(com.truecaller.truepay.app.ui.registrationv2.data.BankData).\n Expected:\n");
      ((StringBuilder)localObject3).append(localb);
      ((StringBuilder)localObject3).append("\n Found:\n");
      ((StringBuilder)localObject3).append(paramb);
      paramb = ((StringBuilder)localObject3).toString();
      ((IllegalStateException)localObject1).<init>(paramb);
      throw ((Throwable)localObject1);
    }
    paramb = new java/lang/IllegalStateException;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Migration didn't properly handle new_badge_icon(com.truecaller.truepay.data.db.NewBadgeIcon).\n Expected:\n");
    ((StringBuilder)localObject3).append(localb);
    ((StringBuilder)localObject3).append("\n Found:\n");
    ((StringBuilder)localObject3).append(localObject1);
    localObject1 = ((StringBuilder)localObject3).toString();
    paramb.<init>((String)localObject1);
    throw paramb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.db.PayRoomDatabase_Impl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */