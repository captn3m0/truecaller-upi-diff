package com.truecaller.truepay.data.db;

import android.arch.persistence.room.i;
import android.arch.persistence.room.j;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

public final class b
  implements a
{
  private final android.arch.persistence.room.f a;
  private final android.arch.persistence.room.c b;
  private final j c;
  
  public b(android.arch.persistence.room.f paramf)
  {
    a = paramf;
    Object localObject = new com/truecaller/truepay/data/db/b$1;
    ((b.1)localObject).<init>(this, paramf);
    b = ((android.arch.persistence.room.c)localObject);
    localObject = new com/truecaller/truepay/data/db/b$2;
    ((b.2)localObject).<init>(this, paramf);
    c = ((j)localObject);
  }
  
  public final List a()
  {
    Object localObject1 = null;
    i locali = i.a("SELECT * FROM new_badge_icon", 0);
    Cursor localCursor = a.a(locali);
    String str1 = "id";
    try
    {
      int i = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "utilityType";
      int j = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "showBadge";
      int k = localCursor.getColumnIndexOrThrow(str3);
      ArrayList localArrayList = new java/util/ArrayList;
      int m = localCursor.getCount();
      localArrayList.<init>(m);
      for (;;)
      {
        boolean bool = localCursor.moveToNext();
        if (!bool) {
          break;
        }
        int n = localCursor.getInt(i);
        String str4 = localCursor.getString(j);
        int i1 = localCursor.getInt(k);
        if (i1 != 0) {
          i1 = 1;
        } else {
          i1 = 0;
        }
        c localc = new com/truecaller/truepay/data/db/c;
        localc.<init>(n, str4, i1);
        localArrayList.add(localc);
      }
      return localArrayList;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final List a(List paramList)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = b;
      paramList = ((android.arch.persistence.room.c)localObject).b(paramList);
      localObject = a;
      ((android.arch.persistence.room.f)localObject).f();
      return paramList;
    }
    finally
    {
      a.e();
    }
  }
  
  public final void a(String paramString)
  {
    android.arch.persistence.db.f localf = c.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int i = 1;
    long l = 0L;
    try
    {
      localf.a(i, l);
      i = 2;
      if (paramString == null) {
        localf.a(i);
      } else {
        localf.a(i, paramString);
      }
      localf.a();
      paramString = a;
      paramString.f();
      return;
    }
    finally
    {
      a.e();
      c.a(localf);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.db.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */