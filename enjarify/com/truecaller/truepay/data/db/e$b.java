package com.truecaller.truepay.data.db;

import android.arch.persistence.db.b;
import android.arch.persistence.room.a.a;
import c.g.b.k;

public final class e$b
  extends a
{
  e$b()
  {
    super(1, 3);
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "database");
    paramb.c("INSERT INTO new_badge_icon(utilityType, showBadge) VALUES ('postpaid',1), ('electricity',1), ('gas',1), ('landline',1), ('water',1)");
    paramb.c("CREATE TABLE IF NOT EXISTS bank_list (bank_symbol TEXT NOT NULL, account_provider_id TEXT, name TEXT NOT NULL, id TEXT NOT NULL, iin TEXT, is_popular INTEGER NOT NULL, popularity_index TEXT NOT NULL, upi_pin_required INTEGER NOT NULL, mandatory_psp TEXT NOT NULL, sim_index INTEGER NOT NULL, sms_count INTEGER NOT NULL, popular INTEGER NOT NULL, PRIMARY KEY(bank_symbol))");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.db.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */