package com.truecaller.truepay.data.db;

import android.arch.persistence.db.b;
import android.arch.persistence.room.f.b;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import javax.inject.Provider;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class PayRoomDatabase$a
  extends f.b
{
  final Provider a;
  private final f b;
  
  public PayRoomDatabase$a(Provider paramProvider, f paramf)
  {
    a = paramProvider;
    b = paramf;
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "db");
    paramb = (ag)bg.a;
    f localf = b;
    Object localObject = new com/truecaller/truepay/data/db/PayRoomDatabase$a$a;
    ((PayRoomDatabase.a.a)localObject).<init>(this, null);
    localObject = (m)localObject;
    e.b(paramb, localf, (m)localObject, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.db.PayRoomDatabase.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */