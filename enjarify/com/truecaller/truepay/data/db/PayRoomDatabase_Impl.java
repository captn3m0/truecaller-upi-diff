package com.truecaller.truepay.data.db;

import android.arch.persistence.db.c;
import android.arch.persistence.db.c.a;
import android.arch.persistence.db.c.b;
import android.arch.persistence.db.c.b.a;
import android.arch.persistence.db.c.c;
import android.arch.persistence.room.d;
import android.arch.persistence.room.h;
import android.arch.persistence.room.h.a;

public class PayRoomDatabase_Impl
  extends PayRoomDatabase
{
  private volatile a g;
  private volatile com.truecaller.truepay.app.ui.registrationv2.data.a.a h;
  
  public final d a()
  {
    d locald = new android/arch/persistence/room/d;
    String[] arrayOfString = { "new_badge_icon", "bank_list" };
    locald.<init>(this, arrayOfString);
    return locald;
  }
  
  public final c b(android.arch.persistence.room.a parama)
  {
    Object localObject1 = new android/arch/persistence/room/h;
    Object localObject2 = new com/truecaller/truepay/data/db/PayRoomDatabase_Impl$1;
    ((PayRoomDatabase_Impl.1)localObject2).<init>(this);
    ((h)localObject1).<init>(parama, (h.a)localObject2, "15b6e4fd1dd6b5aa7b6ebb6914e2f642", "bcefe24b86d6732ad581c676604fe5ba");
    localObject2 = c.b.a(b);
    String str = c;
    b = str;
    c = ((c.a)localObject1);
    localObject1 = ((c.b.a)localObject2).a();
    return a.a((c.b)localObject1);
  }
  
  public final a h()
  {
    Object localObject1 = g;
    if (localObject1 != null) {
      return g;
    }
    try
    {
      localObject1 = g;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/truepay/data/db/b;
        ((b)localObject1).<init>(this);
        g = ((a)localObject1);
      }
      localObject1 = g;
      return (a)localObject1;
    }
    finally {}
  }
  
  public final com.truecaller.truepay.app.ui.registrationv2.data.a.a i()
  {
    Object localObject1 = h;
    if (localObject1 != null) {
      return h;
    }
    try
    {
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/truepay/app/ui/registrationv2/data/a/b;
        ((com.truecaller.truepay.app.ui.registrationv2.data.a.b)localObject1).<init>(this);
        h = ((com.truecaller.truepay.app.ui.registrationv2.data.a.a)localObject1);
      }
      localObject1 = h;
      return (com.truecaller.truepay.app.ui.registrationv2.data.a.a)localObject1;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.db.PayRoomDatabase_Impl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */