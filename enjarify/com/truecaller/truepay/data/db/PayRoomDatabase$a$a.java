package com.truecaller.truepay.data.db;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.util.List;
import javax.inject.Provider;
import kotlinx.coroutines.ag;

final class PayRoomDatabase$a$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  PayRoomDatabase$a$a(PayRoomDatabase.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/truepay/data/db/PayRoomDatabase$a$a;
    PayRoomDatabase.a locala1 = b;
    locala.<init>(locala1, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = ((PayRoomDatabase)b.a.get()).h();
        localObject = d.a();
        ((a)paramObject).a((List)localObject);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.db.PayRoomDatabase.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */