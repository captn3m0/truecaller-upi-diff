package com.truecaller.truepay.data.db;

import c.g.b.k;

public final class c
{
  int a;
  public String b;
  public boolean c;
  
  public c(int paramInt, String paramString, boolean paramBoolean)
  {
    a = paramInt;
    b = paramString;
    c = paramBoolean;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof c;
      if (bool2)
      {
        paramObject = (c)paramObject;
        int i = a;
        int j = a;
        String str1;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          str1 = null;
        }
        if (i != 0)
        {
          str1 = b;
          String str2 = b;
          boolean bool3 = k.a(str1, str2);
          if (bool3)
          {
            bool3 = c;
            boolean bool4 = c;
            if (bool3 == bool4)
            {
              bool4 = true;
            }
            else
            {
              bool4 = false;
              paramObject = null;
            }
            if (bool4) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    String str = b;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    i = (i + j) * 31;
    int k = c;
    if (k != 0) {
      k = 1;
    }
    return i + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("NewBadgeIcon(id=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", utilityType=");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", showBadge=");
    boolean bool = c;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.db.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */