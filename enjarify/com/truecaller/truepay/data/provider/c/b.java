package com.truecaller.truepay.data.provider.c;

import android.content.ContentValues;
import android.net.Uri;

public final class b
  extends com.truecaller.truepay.data.provider.b.a
{
  public final Uri a()
  {
    return a.a;
  }
  
  public final b a(Boolean paramBoolean)
  {
    a.put("favourite", paramBoolean);
    return this;
  }
  
  public final b a(String paramString)
  {
    a.put("beneficiary_aadhar_number", paramString);
    return this;
  }
  
  public final b b(String paramString)
  {
    a.put("beneficiary_acc_number", paramString);
    return this;
  }
  
  public final b d(String paramString)
  {
    a.put("beneficiary_ifsc", paramString);
    return this;
  }
  
  public final b e(String paramString)
  {
    a.put("beneficiary_msisdn", paramString);
    return this;
  }
  
  public final b f(String paramString)
  {
    if (paramString != null)
    {
      a.put("beneficiary_name", paramString);
      return this;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("beneficiaryName must not be null");
    throw paramString;
  }
  
  public final b g(String paramString)
  {
    if (paramString != null)
    {
      a.put("beneficiary_type", paramString);
      return this;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("beneficiaryType must not be null");
    throw paramString;
  }
  
  public final b h(String paramString)
  {
    a.put("beneficiary_vpa", paramString);
    return this;
  }
  
  public final b i(String paramString)
  {
    a.put("beneficiary_nickname", paramString);
    return this;
  }
  
  public final b j(String paramString)
  {
    a.put("beneficiary_id", paramString);
    return this;
  }
  
  public final b k(String paramString)
  {
    a.put("beneficiary_iin", paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */