package com.truecaller.truepay.data.provider.c;

import android.content.ContentResolver;
import android.net.Uri;

public final class d
  extends com.truecaller.truepay.data.provider.b.c
{
  public final Uri a()
  {
    return a.a;
  }
  
  public final c a(ContentResolver paramContentResolver)
  {
    Uri localUri = g();
    String str1 = a.toString();
    String[] arrayOfString = e();
    String str2 = f();
    Object localObject = paramContentResolver;
    paramContentResolver = paramContentResolver.query(localUri, null, str1, arrayOfString, str2);
    if (paramContentResolver == null) {
      return null;
    }
    localObject = new com/truecaller/truepay/data/provider/c/c;
    ((c)localObject).<init>(paramContentResolver);
    return (c)localObject;
  }
  
  public final d a(String... paramVarArgs)
  {
    a("beneficiary_acc_number", paramVarArgs);
    return this;
  }
  
  public final d b(String... paramVarArgs)
  {
    a("beneficiary_msisdn", paramVarArgs);
    return this;
  }
  
  public final d c(String... paramVarArgs)
  {
    a("beneficiary_name", paramVarArgs);
    return this;
  }
  
  public final d d(String... paramVarArgs)
  {
    a("beneficiary_vpa", paramVarArgs);
    return this;
  }
  
  public final d e(String... paramVarArgs)
  {
    a("beneficiary_nickname", paramVarArgs);
    return this;
  }
  
  public final d f(String... paramVarArgs)
  {
    a("beneficiary_id", paramVarArgs);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */