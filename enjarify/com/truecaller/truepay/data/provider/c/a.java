package com.truecaller.truepay.data.provider.c;

import android.net.Uri;
import android.provider.BaseColumns;

public final class a
  implements BaseColumns
{
  public static final Uri a = Uri.parse("content://com.truecaller.truepay.data.provider/beneficiaries");
  public static final String[] b = tmp68_52;
  
  static
  {
    String[] tmp13_10 = new String[12];
    String[] tmp14_13 = tmp13_10;
    String[] tmp14_13 = tmp13_10;
    tmp14_13[0] = "_id";
    tmp14_13[1] = "beneficiary_aadhar_number";
    String[] tmp23_14 = tmp14_13;
    String[] tmp23_14 = tmp14_13;
    tmp23_14[2] = "beneficiary_acc_number";
    tmp23_14[3] = "favourite";
    String[] tmp32_23 = tmp23_14;
    String[] tmp32_23 = tmp23_14;
    tmp32_23[4] = "beneficiary_ifsc";
    tmp32_23[5] = "beneficiary_msisdn";
    String[] tmp41_32 = tmp32_23;
    String[] tmp41_32 = tmp32_23;
    tmp41_32[6] = "beneficiary_name";
    tmp41_32[7] = "beneficiary_type";
    String[] tmp52_41 = tmp41_32;
    String[] tmp52_41 = tmp41_32;
    tmp52_41[8] = "beneficiary_vpa";
    tmp52_41[9] = "beneficiary_nickname";
    tmp52_41[10] = "beneficiary_id";
    String[] tmp68_52 = tmp52_41;
    tmp68_52[11] = "beneficiary_iin";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */