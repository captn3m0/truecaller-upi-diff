package com.truecaller.truepay.data.provider.c;

import android.database.Cursor;
import com.truecaller.truepay.data.provider.b.b;

public final class c
  extends b
{
  public c(Cursor paramCursor)
  {
    super(paramCursor);
  }
  
  public final String a()
  {
    Object localObject = b("beneficiary_name");
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new java/lang/NullPointerException;
    ((NullPointerException)localObject).<init>("The value of 'beneficiary_name' in the database was null, which is not allowed according to the model definition");
    throw ((Throwable)localObject);
  }
  
  public final String b()
  {
    Object localObject = b("beneficiary_type");
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new java/lang/NullPointerException;
    ((NullPointerException)localObject).<init>("The value of 'beneficiary_type' in the database was null, which is not allowed according to the model definition");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */