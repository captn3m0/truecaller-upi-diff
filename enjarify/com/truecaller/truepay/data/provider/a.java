package com.truecaller.truepay.data.provider;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.truecaller.truepay.data.provider.b.e;

public class a
  extends SQLiteOpenHelper
{
  private static final String a = "a";
  private static a b;
  private final Context c;
  private final e d;
  
  private a(Context paramContext, DatabaseErrorHandler paramDatabaseErrorHandler)
  {
    super(paramContext, "TcPayDatabase.db", null, 16, paramDatabaseErrorHandler);
    c = paramContext;
    paramContext = new com/truecaller/truepay/data/provider/b;
    paramContext.<init>();
    d = paramContext;
  }
  
  public static a a(Context paramContext)
  {
    a locala = b;
    if (locala == null)
    {
      paramContext = paramContext.getApplicationContext();
      locala = new com/truecaller/truepay/data/provider/a;
      DefaultDatabaseErrorHandler localDefaultDatabaseErrorHandler = new android/database/DefaultDatabaseErrorHandler;
      localDefaultDatabaseErrorHandler.<init>();
      locala.<init>(paramContext, localDefaultDatabaseErrorHandler);
      b = locala;
    }
    return b;
  }
  
  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS beneficiaries ( _id INTEGER PRIMARY KEY AUTOINCREMENT, beneficiary_aadhar_number TEXT, beneficiary_acc_number TEXT, favourite INTEGER DEFAULT 0, beneficiary_ifsc TEXT, beneficiary_msisdn TEXT, beneficiary_name TEXT NOT NULL, beneficiary_type TEXT NOT NULL, beneficiary_vpa TEXT, beneficiary_nickname TEXT, beneficiary_id TEXT, beneficiary_iin TEXT , CONSTRAINT unique_id UNIQUE (beneficiary_id) ON CONFLICT REPLACE );");
    paramSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS contacts ( _id INTEGER PRIMARY KEY AUTOINCREMENT, full_name TEXT, msisdn TEXT NOT NULL, payments_enabled INTEGER DEFAULT 0, photo_thumbnail_uri TEXT, normalized_number TEXT NOT NULL, lookup_key TEXT NOT NULL, user_id TEXT, vpa TEXT, bank_registered_name TEXT , CONSTRAINT unique_msisdn UNIQUE (msisdn) ON CONFLICT REPLACE );");
    paramSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS transactions ( _id INTEGER PRIMARY KEY AUTOINCREMENT, t_id TEXT NOT NULL, seq_no TEXT NOT NULL, transaction_id TEXT NOT NULL, initiator_msisdn TEXT, initiator_user_id TEXT, initiator_vpa TEXT, receiver_user_id TEXT, transaction_type TEXT NOT NULL, initiator_bank_acc_num TEXT, amount REAL NOT NULL, status TEXT, transaction_timestamp INTEGER NOT NULL, initiator_bank_id TEXT, initiator_bank_name TEXT, initiator_bank_symbol TEXT, transaction_message TEXT, remarks TEXT, category TEXT, bank_rrn TEXT, upi_txn_id TEXT, dispute_id TEXT, request_expired INTEGER, payment_flow TEXT, receiver_vpa TEXT, receiver_msisdn TEXT, receiver_name TEXT, receiver_sub_type TEXT, receiver_bank_name TEXT, receiver_bank_symbol TEXT, receiver_bank_acc_num TEXT, dispute_status_check_at TEXT, updated_at INTEGER NOT NULL, sr_number TEXT, user_remarks TEXT, closure_remarks TEXT, action_data_1_title TEXT, action_data_1_type TEXT, action_data_2_title TEXT, action_data_2_type TEXT, utility_initiated_at TEXT, utility_recharge_number TEXT, utility_recharge_status TEXT, utility_recharge_message TEXT, utility_ref_id TEXT, utility_type TEXT, utility_vendor_trnx_id TEXT, utility_operator_name TEXT, utility_operator_type TEXT, utility_loc_name TEXT, utility_location_code TEXT, utility_refund_rrn TEXT, utility_refund_status TEXT, utility_refund_time TEXT, utility_refund_response_code TEXT, utility_refund_message TEXT, utility_refund_vpa TEXT, operator_symbol TEXT, bbps_transaction_id TEXT, operator_icon_url TEXT ,operator_logo_url TEXT ,vendor_logo_url TEXT ,bill_due_date TEXT ,utility_booking_info TEXT ,repeat_transaction_params TEXT ,utility_remarks TEXT ,utility_email TEXT , CONSTRAINT unique_msisdn UNIQUE (t_id) ON CONFLICT REPLACE );");
    paramSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS bank_sms ( _id INTEGER PRIMARY KEY AUTOINCREMENT, sim_slot_index INTEGER NOT NULL, bank_name TEXT, bank_symbol TEXT, sms_count INTEGER  );");
  }
  
  public void onDowngrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    e locale = d;
    Context localContext = c;
    locale.a(localContext, paramSQLiteDatabase, paramInt1, paramInt2);
  }
  
  public void onOpen(SQLiteDatabase paramSQLiteDatabase)
  {
    super.onOpen(paramSQLiteDatabase);
    boolean bool = paramSQLiteDatabase.isReadOnly();
    if (!bool)
    {
      bool = true;
      paramSQLiteDatabase.setForeignKeyConstraintsEnabled(bool);
    }
  }
  
  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    d.a(paramSQLiteDatabase, paramInt1, paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */