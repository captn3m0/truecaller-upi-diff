package com.truecaller.truepay.data.provider;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import com.truecaller.truepay.data.provider.b.d;
import com.truecaller.truepay.data.provider.b.d.a;

public class TruecallerPayProvider
  extends d
{
  private static final String b = "TruecallerPayProvider";
  private static final UriMatcher c;
  
  static
  {
    UriMatcher localUriMatcher = new android/content/UriMatcher;
    localUriMatcher.<init>(-1);
    c = localUriMatcher;
    localUriMatcher.addURI("com.truecaller.truepay.data.provider", "beneficiaries", 0);
    c.addURI("com.truecaller.truepay.data.provider", "beneficiaries/#", 1);
    c.addURI("com.truecaller.truepay.data.provider", "contacts", 2);
    c.addURI("com.truecaller.truepay.data.provider", "contacts/#", 3);
    c.addURI("com.truecaller.truepay.data.provider", "transactions", 4);
    c.addURI("com.truecaller.truepay.data.provider", "transactions/#", 5);
    c.addURI("com.truecaller.truepay.data.provider", "bank_sms", 6);
    c.addURI("com.truecaller.truepay.data.provider", "bank_sms/#", 7);
  }
  
  public final SQLiteOpenHelper a()
  {
    return a.a(getContext());
  }
  
  public final d.a a(Uri paramUri, String paramString)
  {
    Object localObject1 = new com/truecaller/truepay/data/provider/b/d$a;
    ((d.a)localObject1).<init>();
    Object localObject2 = c;
    int i = ((UriMatcher)localObject2).match(paramUri);
    String str;
    switch (i)
    {
    default: 
      paramString = new java/lang/IllegalArgumentException;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("The uri '");
      ((StringBuilder)localObject1).append(paramUri);
      ((StringBuilder)localObject1).append("' is not supported by this ContentProvider");
      paramUri = ((StringBuilder)localObject1).toString();
      paramString.<init>(paramUri);
      throw paramString;
    case 6: 
    case 7: 
      a = "bank_sms";
      c = "_id";
      b = "bank_sms";
      str = com.truecaller.truepay.data.provider.a.a.b;
      e = str;
      break;
    case 4: 
    case 5: 
      a = "transactions";
      c = "_id";
      b = "transactions";
      str = "transactions.transaction_timestamp DESC";
      e = str;
      break;
    case 2: 
    case 3: 
      a = "contacts";
      c = "_id";
      b = "contacts";
      str = "contacts.full_name";
      e = str;
      break;
    case 0: 
    case 1: 
      a = "beneficiaries";
      c = "_id";
      b = "beneficiaries";
      str = "beneficiaries.beneficiary_name";
      e = str;
    }
    int j = 1;
    if (i != j)
    {
      j = 3;
      if (i != j)
      {
        j = 5;
        if (i != j)
        {
          j = 7;
          if (i != j)
          {
            paramUri = null;
            break label280;
          }
        }
      }
    }
    paramUri = paramUri.getLastPathSegment();
    label280:
    if (paramUri != null)
    {
      if (paramString != null)
      {
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        str = a;
        ((StringBuilder)localObject2).append(str);
        ((StringBuilder)localObject2).append(".");
        str = c;
        ((StringBuilder)localObject2).append(str);
        str = "=";
        ((StringBuilder)localObject2).append(str);
        ((StringBuilder)localObject2).append(paramUri);
        ((StringBuilder)localObject2).append(" and (");
        ((StringBuilder)localObject2).append(paramString);
        ((StringBuilder)localObject2).append(")");
        paramUri = ((StringBuilder)localObject2).toString();
        d = paramUri;
      }
      else
      {
        paramString = new java/lang/StringBuilder;
        paramString.<init>();
        localObject2 = a;
        paramString.append((String)localObject2);
        paramString.append(".");
        localObject2 = c;
        paramString.append((String)localObject2);
        localObject2 = "=";
        paramString.append((String)localObject2);
        paramString.append(paramUri);
        paramUri = paramString.toString();
        d = paramUri;
      }
    }
    else {
      d = paramString;
    }
    return (d.a)localObject1;
  }
  
  public int bulkInsert(Uri paramUri, ContentValues[] paramArrayOfContentValues)
  {
    return super.bulkInsert(paramUri, paramArrayOfContentValues);
  }
  
  public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    return super.delete(paramUri, paramString, paramArrayOfString);
  }
  
  public String getType(Uri paramUri)
  {
    UriMatcher localUriMatcher = c;
    int i = localUriMatcher.match(paramUri);
    switch (i)
    {
    default: 
      return null;
    case 7: 
      return "vnd.android.cursor.item/bank_sms";
    case 6: 
      return "vnd.android.cursor.dir/bank_sms";
    case 5: 
      return "vnd.android.cursor.item/transactions";
    case 4: 
      return "vnd.android.cursor.dir/transactions";
    case 3: 
      return "vnd.android.cursor.item/contacts";
    case 2: 
      return "vnd.android.cursor.dir/contacts";
    case 1: 
      return "vnd.android.cursor.item/beneficiaries";
    }
    return "vnd.android.cursor.dir/beneficiaries";
  }
  
  public Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    return super.insert(paramUri, paramContentValues);
  }
  
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    return super.query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2);
  }
  
  public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    return super.update(paramUri, paramContentValues, paramString, paramArrayOfString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.TruecallerPayProvider
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */