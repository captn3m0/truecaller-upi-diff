package com.truecaller.truepay.data.provider.e;

import android.database.Cursor;
import com.truecaller.truepay.data.provider.b.b;

public final class f
  extends b
{
  public f(Cursor paramCursor)
  {
    super(paramCursor);
  }
  
  public final String a()
  {
    return f(b("initiator_msisdn"));
  }
  
  public final String b()
  {
    return f(b("initiator_vpa"));
  }
  
  public final String c()
  {
    return f(b("transaction_message"));
  }
  
  public final String d()
  {
    return f(b("receiver_vpa"));
  }
  
  public final String e()
  {
    return f(b("receiver_msisdn"));
  }
  
  public final String f()
  {
    return f(b("receiver_name"));
  }
  
  public final String g()
  {
    return f(b("user_remarks"));
  }
  
  public final String h()
  {
    return f(b("closure_remarks"));
  }
  
  public final String i()
  {
    return f(b("utility_recharge_number"));
  }
  
  public final String j()
  {
    return f(b("utility_refund_vpa"));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.e.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */