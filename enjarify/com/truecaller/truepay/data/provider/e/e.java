package com.truecaller.truepay.data.provider.e;

import android.content.ContentValues;
import android.net.Uri;
import com.truecaller.truepay.data.provider.b.a;
import java.util.Date;

public final class e
  extends a
{
  public final e A(String paramString)
  {
    a.put("dispute_status_check_at", paramString);
    return this;
  }
  
  public final e B(String paramString)
  {
    a.put("sr_number", paramString);
    return this;
  }
  
  public final e C(String paramString)
  {
    paramString = c(paramString);
    a.put("user_remarks", paramString);
    return this;
  }
  
  public final e D(String paramString)
  {
    a.put("action_data_1_title", paramString);
    return this;
  }
  
  public final e E(String paramString)
  {
    a.put("action_data_1_type", paramString);
    return this;
  }
  
  public final e F(String paramString)
  {
    a.put("action_data_2_title", paramString);
    return this;
  }
  
  public final e G(String paramString)
  {
    a.put("action_data_2_type", paramString);
    return this;
  }
  
  public final e H(String paramString)
  {
    a.put("utility_initiated_at", paramString);
    return this;
  }
  
  public final e I(String paramString)
  {
    paramString = c(paramString);
    a.put("utility_recharge_number", paramString);
    return this;
  }
  
  public final e J(String paramString)
  {
    a.put("utility_recharge_status", paramString);
    return this;
  }
  
  public final e K(String paramString)
  {
    a.put("utility_recharge_message", paramString);
    return this;
  }
  
  public final e L(String paramString)
  {
    a.put("utility_ref_id", paramString);
    return this;
  }
  
  public final e M(String paramString)
  {
    a.put("utility_type", paramString);
    return this;
  }
  
  public final e N(String paramString)
  {
    a.put("utility_vendor_trnx_id", paramString);
    return this;
  }
  
  public final e O(String paramString)
  {
    a.put("utility_operator_name", paramString);
    return this;
  }
  
  public final e P(String paramString)
  {
    a.put("utility_operator_type", paramString);
    return this;
  }
  
  public final e Q(String paramString)
  {
    a.put("utility_loc_name", paramString);
    return this;
  }
  
  public final e R(String paramString)
  {
    a.put("utility_location_code", paramString);
    return this;
  }
  
  public final e S(String paramString)
  {
    a.put("utility_refund_rrn", paramString);
    return this;
  }
  
  public final e T(String paramString)
  {
    a.put("utility_refund_status", paramString);
    return this;
  }
  
  public final e U(String paramString)
  {
    a.put("utility_refund_time", paramString);
    return this;
  }
  
  public final e V(String paramString)
  {
    a.put("utility_refund_response_code", paramString);
    return this;
  }
  
  public final e W(String paramString)
  {
    a.put("utility_refund_message", paramString);
    return this;
  }
  
  public final e X(String paramString)
  {
    paramString = c(paramString);
    a.put("utility_refund_vpa", paramString);
    return this;
  }
  
  public final e Y(String paramString)
  {
    a.put("operator_symbol", paramString);
    return this;
  }
  
  public final e Z(String paramString)
  {
    a.put("bbps_transaction_id", paramString);
    return this;
  }
  
  public final Uri a()
  {
    return d.a;
  }
  
  public final e a(double paramDouble)
  {
    ContentValues localContentValues = a;
    Double localDouble = Double.valueOf(paramDouble);
    localContentValues.put("amount", localDouble);
    return this;
  }
  
  public final e a(long paramLong)
  {
    ContentValues localContentValues = a;
    Long localLong = Long.valueOf(paramLong);
    localContentValues.put("updated_at", localLong);
    return this;
  }
  
  public final e a(String paramString)
  {
    if (paramString != null)
    {
      a.put("t_id", paramString);
      return this;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("tId must not be null");
    throw paramString;
  }
  
  public final e a(Date paramDate)
  {
    if (paramDate != null)
    {
      ContentValues localContentValues = a;
      paramDate = Long.valueOf(paramDate.getTime());
      localContentValues.put("transaction_timestamp", paramDate);
      return this;
    }
    paramDate = new java/lang/IllegalArgumentException;
    paramDate.<init>("transactionTimestamp must not be null");
    throw paramDate;
  }
  
  public final e aa(String paramString)
  {
    a.put("operator_icon_url", paramString);
    return this;
  }
  
  public final e ab(String paramString)
  {
    a.put("operator_logo_url", paramString);
    return this;
  }
  
  public final e ac(String paramString)
  {
    a.put("vendor_logo_url", paramString);
    return this;
  }
  
  public final e ad(String paramString)
  {
    a.put("bill_due_date", paramString);
    return this;
  }
  
  public final e ae(String paramString)
  {
    a.put("repeat_transaction_params", paramString);
    return this;
  }
  
  public final e af(String paramString)
  {
    a.put("utility_booking_info", paramString);
    return this;
  }
  
  public final e ag(String paramString)
  {
    a.put("utility_remarks", paramString);
    return this;
  }
  
  public final e ah(String paramString)
  {
    a.put("utility_email", paramString);
    return this;
  }
  
  public final e b(String paramString)
  {
    if (paramString != null)
    {
      a.put("seq_no", paramString);
      return this;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("seqNo must not be null");
    throw paramString;
  }
  
  public final e d(String paramString)
  {
    if (paramString != null)
    {
      a.put("transaction_id", paramString);
      return this;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("transactionId must not be null");
    throw paramString;
  }
  
  public final e e(String paramString)
  {
    paramString = c(paramString);
    a.put("initiator_msisdn", paramString);
    return this;
  }
  
  public final e f(String paramString)
  {
    a.put("initiator_user_id", paramString);
    return this;
  }
  
  public final e g(String paramString)
  {
    paramString = c(paramString);
    a.put("initiator_vpa", paramString);
    return this;
  }
  
  public final e h(String paramString)
  {
    if (paramString != null)
    {
      a.put("transaction_type", paramString);
      return this;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("transactionType must not be null");
    throw paramString;
  }
  
  public final e i(String paramString)
  {
    a.put("initiator_bank_acc_num", paramString);
    return this;
  }
  
  public final e j(String paramString)
  {
    a.put("status", paramString);
    return this;
  }
  
  public final e k(String paramString)
  {
    a.put("initiator_bank_id", paramString);
    return this;
  }
  
  public final e l(String paramString)
  {
    a.put("initiator_bank_name", paramString);
    return this;
  }
  
  public final e m(String paramString)
  {
    a.put("initiator_bank_symbol", paramString);
    return this;
  }
  
  public final e n(String paramString)
  {
    paramString = c(paramString);
    a.put("transaction_message", paramString);
    return this;
  }
  
  public final e o(String paramString)
  {
    a.put("remarks", paramString);
    return this;
  }
  
  public final e p(String paramString)
  {
    a.put("category", paramString);
    return this;
  }
  
  public final e q(String paramString)
  {
    a.put("bank_rrn", paramString);
    return this;
  }
  
  public final e r(String paramString)
  {
    a.put("dispute_id", paramString);
    return this;
  }
  
  public final e s(String paramString)
  {
    a.put("payment_flow", paramString);
    return this;
  }
  
  public final e t(String paramString)
  {
    paramString = c(paramString);
    a.put("receiver_vpa", paramString);
    return this;
  }
  
  public final e u(String paramString)
  {
    paramString = c(paramString);
    a.put("receiver_msisdn", paramString);
    return this;
  }
  
  public final e v(String paramString)
  {
    paramString = c(paramString);
    a.put("receiver_name", paramString);
    return this;
  }
  
  public final e w(String paramString)
  {
    a.put("receiver_sub_type", paramString);
    return this;
  }
  
  public final e x(String paramString)
  {
    a.put("receiver_bank_name", paramString);
    return this;
  }
  
  public final e y(String paramString)
  {
    a.put("receiver_bank_symbol", paramString);
    return this;
  }
  
  public final e z(String paramString)
  {
    a.put("receiver_bank_acc_num", paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.e.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */