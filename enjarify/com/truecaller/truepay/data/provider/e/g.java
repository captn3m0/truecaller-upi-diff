package com.truecaller.truepay.data.provider.e;

import android.net.Uri;
import com.truecaller.truepay.data.provider.b.c;

public final class g
  extends c
{
  public final Uri a()
  {
    return d.a;
  }
  
  public final g a(String... paramVarArgs)
  {
    a("transaction_type", paramVarArgs);
    return this;
  }
  
  public final g b(String... paramVarArgs)
  {
    a("utility_type", paramVarArgs);
    return this;
  }
  
  public final g c(String... paramVarArgs)
  {
    a("utility_operator_type", paramVarArgs);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.e.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */