package com.truecaller.truepay.data.provider.e;

import android.net.Uri;
import android.provider.BaseColumns;

public final class d
  implements BaseColumns
{
  public static final Uri a = Uri.parse("content://com.truecaller.truepay.data.provider/transactions");
  public static final String[] b = tmp360_349;
  
  static
  {
    String[] tmp13_10 = new String[67];
    String[] tmp14_13 = tmp13_10;
    String[] tmp14_13 = tmp13_10;
    tmp14_13[0] = "_id";
    tmp14_13[1] = "t_id";
    String[] tmp23_14 = tmp14_13;
    String[] tmp23_14 = tmp14_13;
    tmp23_14[2] = "seq_no";
    tmp23_14[3] = "transaction_id";
    String[] tmp32_23 = tmp23_14;
    String[] tmp32_23 = tmp23_14;
    tmp32_23[4] = "initiator_msisdn";
    tmp32_23[5] = "initiator_user_id";
    String[] tmp41_32 = tmp32_23;
    String[] tmp41_32 = tmp32_23;
    tmp41_32[6] = "initiator_vpa";
    tmp41_32[7] = "receiver_user_id";
    String[] tmp52_41 = tmp41_32;
    String[] tmp52_41 = tmp41_32;
    tmp52_41[8] = "transaction_type";
    tmp52_41[9] = "initiator_bank_acc_num";
    String[] tmp63_52 = tmp52_41;
    String[] tmp63_52 = tmp52_41;
    tmp63_52[10] = "amount";
    tmp63_52[11] = "status";
    String[] tmp74_63 = tmp63_52;
    String[] tmp74_63 = tmp63_52;
    tmp74_63[12] = "transaction_timestamp";
    tmp74_63[13] = "initiator_bank_id";
    String[] tmp85_74 = tmp74_63;
    String[] tmp85_74 = tmp74_63;
    tmp85_74[14] = "initiator_bank_name";
    tmp85_74[15] = "initiator_bank_symbol";
    String[] tmp96_85 = tmp85_74;
    String[] tmp96_85 = tmp85_74;
    tmp96_85[16] = "transaction_message";
    tmp96_85[17] = "remarks";
    String[] tmp107_96 = tmp96_85;
    String[] tmp107_96 = tmp96_85;
    tmp107_96[18] = "category";
    tmp107_96[19] = "bank_rrn";
    String[] tmp118_107 = tmp107_96;
    String[] tmp118_107 = tmp107_96;
    tmp118_107[20] = "upi_txn_id";
    tmp118_107[21] = "dispute_id";
    String[] tmp129_118 = tmp118_107;
    String[] tmp129_118 = tmp118_107;
    tmp129_118[22] = "request_expired";
    tmp129_118[23] = "payment_flow";
    String[] tmp140_129 = tmp129_118;
    String[] tmp140_129 = tmp129_118;
    tmp140_129[24] = "receiver_vpa";
    tmp140_129[25] = "receiver_msisdn";
    String[] tmp151_140 = tmp140_129;
    String[] tmp151_140 = tmp140_129;
    tmp151_140[26] = "receiver_name";
    tmp151_140[27] = "receiver_sub_type";
    String[] tmp162_151 = tmp151_140;
    String[] tmp162_151 = tmp151_140;
    tmp162_151[28] = "receiver_bank_name";
    tmp162_151[29] = "receiver_bank_symbol";
    String[] tmp173_162 = tmp162_151;
    String[] tmp173_162 = tmp162_151;
    tmp173_162[30] = "receiver_bank_acc_num";
    tmp173_162[31] = "dispute_status_check_at";
    String[] tmp184_173 = tmp173_162;
    String[] tmp184_173 = tmp173_162;
    tmp184_173[32] = "updated_at";
    tmp184_173[33] = "sr_number";
    String[] tmp195_184 = tmp184_173;
    String[] tmp195_184 = tmp184_173;
    tmp195_184[34] = "user_remarks";
    tmp195_184[35] = "closure_remarks";
    String[] tmp206_195 = tmp195_184;
    String[] tmp206_195 = tmp195_184;
    tmp206_195[36] = "action_data_1_title";
    tmp206_195[37] = "action_data_1_type";
    String[] tmp217_206 = tmp206_195;
    String[] tmp217_206 = tmp206_195;
    tmp217_206[38] = "action_data_2_title";
    tmp217_206[39] = "action_data_2_type";
    String[] tmp228_217 = tmp217_206;
    String[] tmp228_217 = tmp217_206;
    tmp228_217[40] = "utility_initiated_at";
    tmp228_217[41] = "utility_recharge_number";
    String[] tmp239_228 = tmp228_217;
    String[] tmp239_228 = tmp228_217;
    tmp239_228[42] = "utility_recharge_status";
    tmp239_228[43] = "utility_recharge_message";
    String[] tmp250_239 = tmp239_228;
    String[] tmp250_239 = tmp239_228;
    tmp250_239[44] = "utility_ref_id";
    tmp250_239[45] = "utility_type";
    String[] tmp261_250 = tmp250_239;
    String[] tmp261_250 = tmp250_239;
    tmp261_250[46] = "utility_vendor_trnx_id";
    tmp261_250[47] = "utility_operator_name";
    String[] tmp272_261 = tmp261_250;
    String[] tmp272_261 = tmp261_250;
    tmp272_261[48] = "utility_operator_type";
    tmp272_261[49] = "utility_loc_name";
    String[] tmp283_272 = tmp272_261;
    String[] tmp283_272 = tmp272_261;
    tmp283_272[50] = "utility_location_code";
    tmp283_272[51] = "utility_refund_rrn";
    String[] tmp294_283 = tmp283_272;
    String[] tmp294_283 = tmp283_272;
    tmp294_283[52] = "utility_refund_status";
    tmp294_283[53] = "utility_refund_time";
    String[] tmp305_294 = tmp294_283;
    String[] tmp305_294 = tmp294_283;
    tmp305_294[54] = "utility_refund_response_code";
    tmp305_294[55] = "utility_refund_message";
    String[] tmp316_305 = tmp305_294;
    String[] tmp316_305 = tmp305_294;
    tmp316_305[56] = "utility_refund_vpa";
    tmp316_305[57] = "operator_symbol";
    String[] tmp327_316 = tmp316_305;
    String[] tmp327_316 = tmp316_305;
    tmp327_316[58] = "bbps_transaction_id";
    tmp327_316[59] = "operator_icon_url";
    String[] tmp338_327 = tmp327_316;
    String[] tmp338_327 = tmp327_316;
    tmp338_327[60] = "operator_logo_url";
    tmp338_327[61] = "vendor_logo_url";
    String[] tmp349_338 = tmp338_327;
    String[] tmp349_338 = tmp338_327;
    tmp349_338[62] = "bill_due_date";
    tmp349_338[63] = "repeat_transaction_params";
    String[] tmp360_349 = tmp349_338;
    String[] tmp360_349 = tmp349_338;
    tmp360_349[64] = "utility_booking_info";
    tmp360_349[65] = "utility_remarks";
    tmp360_349[66] = "utility_email";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.e.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */