package com.truecaller.truepay.data.provider.e;

import c.g.b.k;
import c.u;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class b
  implements a
{
  public final String[] a(String[] paramArrayOfString, String paramString)
  {
    k.b(paramArrayOfString, "allColumns");
    int i = 0;
    Object localObject1 = null;
    if (paramString == null) {
      return null;
    }
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (List)localObject2;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    String str1 = "distinct ";
    localStringBuilder.<init>(str1);
    int j = paramArrayOfString.length;
    int k = 0;
    while (k < j)
    {
      String str2 = paramArrayOfString[k];
      boolean bool2 = k.a(str2, paramString);
      if (bool2)
      {
        localObject1 = str2;
        break;
      }
      k += 1;
    }
    localStringBuilder.append((String)localObject1);
    localObject1 = localStringBuilder.toString();
    ((List)localObject2).add(localObject1);
    i = paramArrayOfString.length;
    int m = 0;
    localStringBuilder = null;
    while (m < i)
    {
      str1 = paramArrayOfString[m];
      boolean bool1 = k.a(str1, paramString) ^ true;
      if (bool1) {
        ((List)localObject2).add(str1);
      }
      m += 1;
    }
    localObject2 = (Collection)localObject2;
    paramArrayOfString = new String[0];
    paramArrayOfString = ((Collection)localObject2).toArray(paramArrayOfString);
    if (paramArrayOfString != null) {
      return (String[])paramArrayOfString;
    }
    paramArrayOfString = new c/u;
    paramArrayOfString.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramArrayOfString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */