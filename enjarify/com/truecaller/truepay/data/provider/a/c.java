package com.truecaller.truepay.data.provider.a;

import android.database.Cursor;
import com.truecaller.truepay.data.provider.b.b;

public final class c
  extends b
{
  public c(Cursor paramCursor)
  {
    super(paramCursor);
  }
  
  public final int a()
  {
    Object localObject = c("sim_slot_index");
    if (localObject != null) {
      return ((Integer)localObject).intValue();
    }
    localObject = new java/lang/NullPointerException;
    ((NullPointerException)localObject).<init>("The value of 'sim_slot_index' in the database was null, which is not allowed according to the model definition");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */