package com.truecaller.truepay.data.provider.a;

import android.content.ContentResolver;
import android.net.Uri;

public final class d
  extends com.truecaller.truepay.data.provider.b.c
{
  public final Uri a()
  {
    return a.a;
  }
  
  public final c a(ContentResolver paramContentResolver)
  {
    Uri localUri = g();
    String str1 = a.toString();
    String[] arrayOfString = e();
    String str2 = f();
    Object localObject = paramContentResolver;
    paramContentResolver = paramContentResolver.query(localUri, null, str1, arrayOfString, str2);
    if (paramContentResolver == null) {
      return null;
    }
    localObject = new com/truecaller/truepay/data/provider/a/c;
    ((c)localObject).<init>(paramContentResolver);
    return (c)localObject;
  }
  
  public final d a(int... paramVarArgs)
  {
    String str = "sim_slot_index";
    int i = 1;
    Object[] arrayOfObject = new Object[i];
    int j = 0;
    while (j <= 0)
    {
      int k = paramVarArgs[0];
      Integer localInteger = Integer.valueOf(k);
      arrayOfObject[0] = localInteger;
      j += 1;
    }
    a(str, arrayOfObject);
    return this;
  }
  
  public final d a(String... paramVarArgs)
  {
    a("bank_name", paramVarArgs);
    return this;
  }
  
  public final d b()
  {
    a("sms_count");
    return this;
  }
  
  public final d b(String... paramVarArgs)
  {
    a("bank_name", paramVarArgs);
    return this;
  }
  
  public final d c(String... paramVarArgs)
  {
    a("bank_symbol", paramVarArgs);
    return this;
  }
  
  public final d d(String... paramVarArgs)
  {
    a("bank_symbol", paramVarArgs);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */