package com.truecaller.truepay.data.provider.a;

import android.content.ContentValues;
import android.net.Uri;

public final class b
  extends com.truecaller.truepay.data.provider.b.a
{
  public final Uri a()
  {
    return a.a;
  }
  
  public final b a(int paramInt)
  {
    ContentValues localContentValues = a;
    Integer localInteger = Integer.valueOf(paramInt);
    localContentValues.put("sim_slot_index", localInteger);
    return this;
  }
  
  public final b a(Integer paramInteger)
  {
    a.put("sms_count", paramInteger);
    return this;
  }
  
  public final b a(String paramString)
  {
    a.put("bank_name", paramString);
    return this;
  }
  
  public final b b(String paramString)
  {
    a.put("bank_symbol", paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */