package com.truecaller.truepay.data.provider.a;

import android.net.Uri;
import android.provider.BaseColumns;

public final class a
  implements BaseColumns
{
  public static final Uri a = Uri.parse("content://com.truecaller.truepay.data.provider/bank_sms");
  public static final String b = null;
  public static final String[] c = tmp26_17;
  
  static
  {
    String[] tmp16_13 = new String[5];
    String[] tmp17_16 = tmp16_13;
    String[] tmp17_16 = tmp16_13;
    tmp17_16[0] = "_id";
    tmp17_16[1] = "sim_slot_index";
    String[] tmp26_17 = tmp17_16;
    String[] tmp26_17 = tmp17_16;
    tmp26_17[2] = "bank_name";
    tmp26_17[3] = "bank_symbol";
    tmp26_17[4] = "sms_count";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */