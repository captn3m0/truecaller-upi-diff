package com.truecaller.truepay.data.provider.b;

import android.database.Cursor;
import android.database.CursorWrapper;
import com.truecaller.truepay.app.utils.p;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.HashMap;

public abstract class b
  extends CursorWrapper
{
  private final HashMap a;
  
  public b(Cursor paramCursor)
  {
    super(paramCursor);
    HashMap localHashMap = new java/util/HashMap;
    int i = paramCursor.getColumnCount() * 4 / 3;
    localHashMap.<init>(i, 0.75F);
    a = localHashMap;
  }
  
  protected static String f(String paramString)
  {
    if (paramString != null)
    {
      boolean bool = paramString.isEmpty();
      if (bool) {}
    }
    try
    {
      String str1 = p.b();
      int i = 16;
      str1 = str1.substring(0, i);
      String str2 = p.c();
      String str3 = str2.substring(0, i);
      paramString = p.b(str1, paramString, str3);
    }
    catch (GeneralSecurityException|UnsupportedEncodingException localGeneralSecurityException)
    {
      for (;;) {}
    }
    return paramString;
  }
  
  public final int a(String paramString)
  {
    Integer localInteger = (Integer)a.get(paramString);
    if (localInteger == null)
    {
      int i = getColumnIndexOrThrow(paramString);
      localInteger = Integer.valueOf(i);
      HashMap localHashMap = a;
      localHashMap.put(paramString, localInteger);
    }
    return localInteger.intValue();
  }
  
  public final String b(String paramString)
  {
    int i = a(paramString);
    boolean bool = isNull(i);
    if (bool) {
      return null;
    }
    return getString(i);
  }
  
  public final Integer c(String paramString)
  {
    int i = a(paramString);
    boolean bool = isNull(i);
    if (bool) {
      return null;
    }
    return Integer.valueOf(getInt(i));
  }
  
  public final Boolean d(String paramString)
  {
    int i = a(paramString);
    boolean bool = isNull(i);
    if (bool) {
      return null;
    }
    i = getInt(i);
    if (i != 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramString = null;
    }
    return Boolean.valueOf(i);
  }
  
  public final Date e(String paramString)
  {
    int i = a(paramString);
    boolean bool = isNull(i);
    if (bool) {
      return null;
    }
    Date localDate = new java/util/Date;
    long l = getLong(i);
    localDate.<init>(l);
    return localDate;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */