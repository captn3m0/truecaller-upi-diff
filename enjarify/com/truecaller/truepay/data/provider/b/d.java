package com.truecaller.truepay.data.provider.b;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.net.Uri.Builder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public abstract class d
  extends ContentProvider
{
  protected SQLiteOpenHelper a;
  
  public static Uri a(Uri paramUri, boolean paramBoolean)
  {
    paramUri = paramUri.buildUpon();
    String str = String.valueOf(paramBoolean);
    return paramUri.appendQueryParameter("QUERY_NOTIFY", str).build();
  }
  
  public static Uri b(Uri paramUri, String paramString)
  {
    return paramUri.buildUpon().appendQueryParameter("QUERY_GROUP_BY", paramString).build();
  }
  
  public static Uri c(Uri paramUri, String paramString)
  {
    return paramUri.buildUpon().appendQueryParameter("QUERY_HAVING", paramString).build();
  }
  
  public static Uri d(Uri paramUri, String paramString)
  {
    return paramUri.buildUpon().appendQueryParameter("QUERY_LIMIT", paramString).build();
  }
  
  protected abstract SQLiteOpenHelper a();
  
  protected abstract d.a a(Uri paramUri, String paramString);
  
  public ContentProviderResult[] applyBatch(ArrayList paramArrayList)
  {
    Object localObject1 = new java/util/HashSet;
    int i = paramArrayList.size();
    ((HashSet)localObject1).<init>(i);
    Object localObject2 = paramArrayList.iterator();
    Object localObject3;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = ((ContentProviderOperation)((Iterator)localObject2).next()).getUri();
      ((HashSet)localObject1).add(localObject3);
    }
    localObject2 = a.getWritableDatabase();
    ((SQLiteDatabase)localObject2).beginTransaction();
    try
    {
      int j = paramArrayList.size();
      localObject3 = new ContentProviderResult[j];
      int k = 0;
      Object localObject4 = null;
      paramArrayList = paramArrayList.iterator();
      boolean bool2;
      Object localObject5;
      for (;;)
      {
        bool2 = paramArrayList.hasNext();
        if (!bool2) {
          break;
        }
        localObject5 = paramArrayList.next();
        localObject5 = (ContentProviderOperation)localObject5;
        ContentProviderResult localContentProviderResult = ((ContentProviderOperation)localObject5).apply(this, (ContentProviderResult[])localObject3, k);
        localObject3[k] = localContentProviderResult;
        bool2 = ((ContentProviderOperation)localObject5).isYieldAllowed();
        if (bool2) {
          ((SQLiteDatabase)localObject2).yieldIfContendedSafely();
        }
        k += 1;
      }
      ((SQLiteDatabase)localObject2).setTransactionSuccessful();
      paramArrayList = ((HashSet)localObject1).iterator();
      for (;;)
      {
        boolean bool3 = paramArrayList.hasNext();
        if (!bool3) {
          break;
        }
        localObject1 = paramArrayList.next();
        localObject1 = (Uri)localObject1;
        localObject4 = getContext();
        localObject4 = ((Context)localObject4).getContentResolver();
        bool2 = false;
        localObject5 = null;
        ((ContentResolver)localObject4).notifyChange((Uri)localObject1, null);
      }
      return (ContentProviderResult[])localObject3;
    }
    finally
    {
      ((SQLiteDatabase)localObject2).endTransaction();
    }
  }
  
  public int bulkInsert(Uri paramUri, ContentValues[] paramArrayOfContentValues)
  {
    String str = paramUri.getLastPathSegment();
    SQLiteDatabase localSQLiteDatabase = a.getWritableDatabase();
    localSQLiteDatabase.beginTransaction();
    try
    {
      int i = paramArrayOfContentValues.length;
      int j = 0;
      int k = 0;
      while (j < i)
      {
        ContentValues localContentValues = paramArrayOfContentValues[j];
        long l1 = localSQLiteDatabase.insert(str, null, localContentValues);
        localSQLiteDatabase.yieldIfContendedSafely();
        long l2 = -1;
        boolean bool1 = l1 < l2;
        if (bool1) {
          k += 1;
        }
        j += 1;
      }
      localSQLiteDatabase.setTransactionSuccessful();
      localSQLiteDatabase.endTransaction();
      if (k != 0)
      {
        paramArrayOfContentValues = paramUri.getQueryParameter("QUERY_NOTIFY");
        if (paramArrayOfContentValues != null)
        {
          str = "true";
          boolean bool2 = str.equals(paramArrayOfContentValues);
          if (!bool2) {}
        }
        else
        {
          paramArrayOfContentValues = getContext().getContentResolver();
          paramArrayOfContentValues.notifyChange(paramUri, null);
        }
      }
      return k;
    }
    finally
    {
      localSQLiteDatabase.endTransaction();
    }
  }
  
  public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    paramString = a(paramUri, paramString);
    Object localObject = a.getWritableDatabase();
    String str = a;
    paramString = d;
    int i = ((SQLiteDatabase)localObject).delete(str, paramString, paramArrayOfString);
    if (i != 0)
    {
      paramArrayOfString = paramUri.getQueryParameter("QUERY_NOTIFY");
      if (paramArrayOfString != null)
      {
        localObject = "true";
        boolean bool = ((String)localObject).equals(paramArrayOfString);
        if (!bool) {}
      }
      else
      {
        paramArrayOfString = getContext().getContentResolver();
        localObject = null;
        paramArrayOfString.notifyChange(paramUri, null);
      }
    }
    return i;
  }
  
  public Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    String str1 = paramUri.getLastPathSegment();
    SQLiteDatabase localSQLiteDatabase = a.getWritableDatabase();
    long l1 = localSQLiteDatabase.insertOrThrow(str1, null, paramContentValues);
    long l2 = -1;
    boolean bool = l1 < l2;
    if (!bool) {
      return null;
    }
    paramContentValues = paramUri.getQueryParameter("QUERY_NOTIFY");
    if (paramContentValues != null)
    {
      String str2 = "true";
      bool = str2.equals(paramContentValues);
      if (!bool) {}
    }
    else
    {
      paramContentValues = getContext().getContentResolver();
      paramContentValues.notifyChange(paramUri, null);
    }
    paramUri = paramUri.buildUpon();
    paramContentValues = String.valueOf(l1);
    return paramUri.appendEncodedPath(paramContentValues).build();
  }
  
  public final boolean onCreate()
  {
    SQLiteOpenHelper localSQLiteOpenHelper = a();
    a = localSQLiteOpenHelper;
    return false;
  }
  
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    String str1 = paramUri.getQueryParameter("QUERY_GROUP_BY");
    String str2 = paramUri.getQueryParameter("QUERY_HAVING");
    String str3 = paramUri.getQueryParameter("QUERY_LIMIT");
    paramString1 = a(paramUri, paramString1);
    String str4 = a;
    Object localObject1 = c;
    int i;
    Object localObject2;
    if (paramArrayOfString1 == null)
    {
      paramArrayOfString1 = null;
      i = 0;
      localObject2 = null;
    }
    else
    {
      int j = paramArrayOfString1.length;
      localObject3 = new String[j];
      i = 0;
      localObject2 = null;
      for (;;)
      {
        int k = paramArrayOfString1.length;
        if (i >= k) {
          break;
        }
        localObject4 = paramArrayOfString1[i];
        boolean bool = ((String)localObject4).equals(localObject1);
        if (bool)
        {
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          ((StringBuilder)localObject4).append(str4);
          ((StringBuilder)localObject4).append(".");
          ((StringBuilder)localObject4).append((String)localObject1);
          localObject5 = " AS _id";
          ((StringBuilder)localObject4).append((String)localObject5);
          localObject4 = ((StringBuilder)localObject4).toString();
          localObject3[i] = localObject4;
        }
        else
        {
          localObject4 = paramArrayOfString1[i];
          localObject3[i] = localObject4;
        }
        i += 1;
      }
      localObject2 = localObject3;
    }
    paramArrayOfString1 = a;
    localObject1 = paramArrayOfString1.getReadableDatabase();
    Object localObject3 = b;
    Object localObject4 = d;
    if (paramString2 == null) {
      paramString2 = e;
    }
    Object localObject5 = paramArrayOfString2;
    paramArrayOfString1 = ((SQLiteDatabase)localObject1).query((String)localObject3, (String[])localObject2, (String)localObject4, paramArrayOfString2, str1, str2, paramString2, str3);
    paramString1 = getContext().getContentResolver();
    paramArrayOfString1.setNotificationUri(paramString1, paramUri);
    return paramArrayOfString1;
  }
  
  public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    paramString = a(paramUri, paramString);
    SQLiteDatabase localSQLiteDatabase = a.getWritableDatabase();
    String str = a;
    paramString = d;
    int i = localSQLiteDatabase.update(str, paramContentValues, paramString, paramArrayOfString);
    if (i != 0)
    {
      paramString = paramUri.getQueryParameter("QUERY_NOTIFY");
      if (paramString != null)
      {
        paramArrayOfString = "true";
        boolean bool = paramArrayOfString.equals(paramString);
        if (!bool) {}
      }
      else
      {
        paramString = getContext().getContentResolver();
        paramArrayOfString = null;
        paramString.notifyChange(paramUri, null);
      }
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */