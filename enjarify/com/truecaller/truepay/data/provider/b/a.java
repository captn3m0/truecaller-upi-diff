package com.truecaller.truepay.data.provider.b;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import com.truecaller.truepay.app.utils.p;
import java.security.GeneralSecurityException;

public abstract class a
{
  protected final ContentValues a;
  private Boolean b;
  
  public a()
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    a = localContentValues;
  }
  
  protected static String c(String paramString)
  {
    if (paramString != null)
    {
      boolean bool = paramString.isEmpty();
      if (bool) {}
    }
    try
    {
      String str1 = p.b();
      int i = 16;
      str1 = str1.substring(0, i);
      String str2 = p.c();
      String str3 = str2.substring(0, i);
      paramString = p.a(str1, paramString, str3);
    }
    catch (GeneralSecurityException localGeneralSecurityException)
    {
      for (;;) {}
    }
    return paramString;
  }
  
  protected abstract Uri a();
  
  public final Uri a(ContentResolver paramContentResolver)
  {
    Uri localUri = b();
    ContentValues localContentValues = a;
    return paramContentResolver.insert(localUri, localContentValues);
  }
  
  public final Uri b()
  {
    Uri localUri = a();
    Boolean localBoolean = b;
    if (localBoolean != null)
    {
      boolean bool = localBoolean.booleanValue();
      localUri = d.a(localUri, bool);
    }
    return localUri;
  }
  
  public final ContentValues c()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */