package com.truecaller.truepay.data.provider.b;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class c
{
  public final StringBuilder a;
  public String b;
  private final List c;
  private final StringBuilder d;
  private Boolean e;
  private String f;
  private Integer g;
  
  public c()
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    a = ((StringBuilder)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>(5);
    c = ((List)localObject);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    d = ((StringBuilder)localObject);
  }
  
  private static String a(Object paramObject)
  {
    boolean bool1 = paramObject instanceof Date;
    if (bool1) {
      return String.valueOf(((Date)paramObject).getTime());
    }
    bool1 = paramObject instanceof Boolean;
    if (bool1)
    {
      paramObject = (Boolean)paramObject;
      boolean bool2 = ((Boolean)paramObject).booleanValue();
      if (bool2) {
        return "1";
      }
      return "0";
    }
    bool1 = paramObject instanceof Enum;
    if (bool1) {
      return String.valueOf(((Enum)paramObject).ordinal());
    }
    return String.valueOf(paramObject);
  }
  
  public final int a(Context paramContext)
  {
    paramContext = paramContext.getContentResolver();
    Uri localUri = g();
    String str = a.toString();
    String[] arrayOfString = e();
    return paramContext.delete(localUri, str, arrayOfString);
  }
  
  protected abstract Uri a();
  
  public final c a(int paramInt)
  {
    Integer localInteger = Integer.valueOf(paramInt);
    g = localInteger;
    return this;
  }
  
  public final c a(String paramString)
  {
    StringBuilder localStringBuilder = d;
    int i = localStringBuilder.length();
    if (i > 0)
    {
      localStringBuilder = d;
      String str = ",";
      localStringBuilder.append(str);
    }
    d.append(paramString);
    d.append(" DESC");
    return this;
  }
  
  protected final void a(String paramString, Object[] paramArrayOfObject)
  {
    StringBuilder localStringBuilder = a;
    localStringBuilder.append(paramString);
    if (paramArrayOfObject == null)
    {
      a.append(" IS NULL");
      return;
    }
    int i = paramArrayOfObject.length;
    int j = 0;
    localStringBuilder = null;
    int k = 1;
    if (i > k)
    {
      paramString = a;
      String str = " IN (";
      paramString.append(str);
      for (;;)
      {
        i = paramArrayOfObject.length;
        if (j >= i) {
          break;
        }
        paramString = a;
        str = "?";
        paramString.append(str);
        i = paramArrayOfObject.length - k;
        if (j < i)
        {
          paramString = a;
          str = ",";
          paramString.append(str);
        }
        paramString = c;
        str = a(paramArrayOfObject[j]);
        paramString.add(str);
        j += 1;
      }
      a.append(")");
      return;
    }
    paramString = paramArrayOfObject[0];
    if (paramString == null)
    {
      a.append(" IS NULL");
      return;
    }
    a.append("=?");
    paramString = c;
    paramArrayOfObject = a(paramArrayOfObject[0]);
    paramString.add(paramArrayOfObject);
  }
  
  protected final void a(String paramString, String[] paramArrayOfString)
  {
    StringBuilder localStringBuilder = a;
    Object localObject = "(";
    localStringBuilder.append((String)localObject);
    int i = 0;
    localStringBuilder = null;
    for (;;)
    {
      int j = paramArrayOfString.length;
      if (i >= j) {
        break;
      }
      a.append(paramString);
      a.append(" LIKE '%' || ? || '%'");
      localObject = c;
      String str = paramArrayOfString[i];
      ((List)localObject).add(str);
      j = paramArrayOfString.length + -1;
      if (i < j)
      {
        localObject = a;
        str = " OR ";
        ((StringBuilder)localObject).append(str);
      }
      i += 1;
    }
    a.append(")");
  }
  
  public final int b(ContentResolver paramContentResolver)
  {
    Uri localUri = g();
    String str = a.toString();
    String[] arrayOfString = e();
    return paramContentResolver.delete(localUri, str, arrayOfString);
  }
  
  public final int c(ContentResolver paramContentResolver)
  {
    Uri localUri = g();
    String[] arrayOfString1 = { "COUNT(*)" };
    String str = a.toString();
    String[] arrayOfString2 = e();
    ContentResolver localContentResolver = paramContentResolver;
    paramContentResolver = paramContentResolver.query(localUri, arrayOfString1, str, arrayOfString2, null);
    int i = 0;
    localContentResolver = null;
    if (paramContentResolver == null) {
      return 0;
    }
    try
    {
      boolean bool = paramContentResolver.moveToFirst();
      if (bool) {
        i = paramContentResolver.getInt(0);
      }
      return i;
    }
    finally
    {
      paramContentResolver.close();
    }
  }
  
  public final c c()
  {
    a.append(" AND ");
    return this;
  }
  
  public final c d()
  {
    a.append(" OR ");
    return this;
  }
  
  public final String[] e()
  {
    Object localObject = c;
    int i = ((List)localObject).size();
    if (i == 0) {
      return null;
    }
    List localList = c;
    localObject = new String[i];
    return (String[])localList.toArray((Object[])localObject);
  }
  
  public final String f()
  {
    StringBuilder localStringBuilder = d;
    int i = localStringBuilder.length();
    if (i > 0) {
      return d.toString();
    }
    return null;
  }
  
  public final Uri g()
  {
    Uri localUri = a();
    Object localObject = e;
    if (localObject != null)
    {
      boolean bool = ((Boolean)localObject).booleanValue();
      localUri = d.a(localUri, bool);
    }
    localObject = b;
    if (localObject != null) {
      localUri = d.b(localUri, (String)localObject);
    }
    localObject = f;
    if (localObject != null) {
      localUri = d.c(localUri, (String)localObject);
    }
    localObject = g;
    if (localObject != null)
    {
      localObject = String.valueOf(localObject);
      localUri = d.d(localUri, (String)localObject);
    }
    return localUri;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */