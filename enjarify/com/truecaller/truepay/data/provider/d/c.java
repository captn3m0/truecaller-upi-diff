package com.truecaller.truepay.data.provider.d;

import android.database.Cursor;
import com.truecaller.truepay.data.provider.b.b;

public final class c
  extends b
{
  public c(Cursor paramCursor)
  {
    super(paramCursor);
  }
  
  public final String a()
  {
    Object localObject = b("msisdn");
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new java/lang/NullPointerException;
    ((NullPointerException)localObject).<init>("The value of 'msisdn' in the database was null, which is not allowed according to the model definition");
    throw ((Throwable)localObject);
  }
  
  public final String b()
  {
    Object localObject = b("normalized_number");
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new java/lang/NullPointerException;
    ((NullPointerException)localObject).<init>("The value of 'normalized_number' in the database was null, which is not allowed according to the model definition");
    throw ((Throwable)localObject);
  }
  
  public final String c()
  {
    Object localObject = b("lookup_key");
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new java/lang/NullPointerException;
    ((NullPointerException)localObject).<init>("The value of 'lookup_key' in the database was null, which is not allowed according to the model definition");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */