package com.truecaller.truepay.data.provider.d;

import android.content.ContentValues;
import android.net.Uri;

public final class b
  extends com.truecaller.truepay.data.provider.b.a
{
  public final Uri a()
  {
    return a.a;
  }
  
  public final b a(Boolean paramBoolean)
  {
    a.put("payments_enabled", paramBoolean);
    return this;
  }
  
  public final b a(String paramString)
  {
    a.put("full_name", paramString);
    return this;
  }
  
  public final b b(String paramString)
  {
    if (paramString != null)
    {
      a.put("msisdn", paramString);
      return this;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("msisdn must not be null");
    throw paramString;
  }
  
  public final b d(String paramString)
  {
    a.put("photo_thumbnail_uri", paramString);
    return this;
  }
  
  public final b e(String paramString)
  {
    if (paramString != null)
    {
      a.put("normalized_number", paramString);
      return this;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("normalizedNumber must not be null");
    throw paramString;
  }
  
  public final b f(String paramString)
  {
    if (paramString != null)
    {
      a.put("lookup_key", paramString);
      return this;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("lookupKey must not be null");
    throw paramString;
  }
  
  public final b g(String paramString)
  {
    a.put("user_id", paramString);
    return this;
  }
  
  public final b h(String paramString)
  {
    a.put("vpa", paramString);
    return this;
  }
  
  public final b i(String paramString)
  {
    a.put("bank_registered_name", paramString);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */