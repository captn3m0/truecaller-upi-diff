package com.truecaller.truepay.data.provider.d;

import android.net.Uri;
import android.provider.BaseColumns;

public final class a
  implements BaseColumns
{
  public static final Uri a = Uri.parse("content://com.truecaller.truepay.data.provider/contacts");
  public static final String[] b = tmp57_41;
  
  static
  {
    String[] tmp13_10 = new String[10];
    String[] tmp14_13 = tmp13_10;
    String[] tmp14_13 = tmp13_10;
    tmp14_13[0] = "_id";
    tmp14_13[1] = "full_name";
    String[] tmp23_14 = tmp14_13;
    String[] tmp23_14 = tmp14_13;
    tmp23_14[2] = "msisdn";
    tmp23_14[3] = "payments_enabled";
    String[] tmp32_23 = tmp23_14;
    String[] tmp32_23 = tmp23_14;
    tmp32_23[4] = "photo_thumbnail_uri";
    tmp32_23[5] = "normalized_number";
    String[] tmp41_32 = tmp32_23;
    String[] tmp41_32 = tmp32_23;
    tmp41_32[6] = "lookup_key";
    tmp41_32[7] = "user_id";
    tmp41_32[8] = "vpa";
    String[] tmp57_41 = tmp41_32;
    tmp57_41[9] = "bank_registered_name";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */