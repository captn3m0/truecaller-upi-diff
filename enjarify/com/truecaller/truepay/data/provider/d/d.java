package com.truecaller.truepay.data.provider.d;

import android.content.ContentResolver;
import android.net.Uri;

public final class d
  extends com.truecaller.truepay.data.provider.b.c
{
  public final Uri a()
  {
    return a.a;
  }
  
  public final c a(ContentResolver paramContentResolver, String[] paramArrayOfString)
  {
    Uri localUri = g();
    String str1 = a.toString();
    String[] arrayOfString = e();
    String str2 = f();
    paramContentResolver = paramContentResolver.query(localUri, paramArrayOfString, str1, arrayOfString, str2);
    if (paramContentResolver == null) {
      return null;
    }
    paramArrayOfString = new com/truecaller/truepay/data/provider/d/c;
    paramArrayOfString.<init>(paramContentResolver);
    return paramArrayOfString;
  }
  
  public final d a(String... paramVarArgs)
  {
    a("full_name", paramVarArgs);
    return this;
  }
  
  public final d b(String... paramVarArgs)
  {
    a("msisdn", paramVarArgs);
    return this;
  }
  
  public final d c(String... paramVarArgs)
  {
    a("msisdn", paramVarArgs);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.d.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */