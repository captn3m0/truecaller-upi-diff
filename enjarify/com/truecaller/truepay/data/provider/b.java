package com.truecaller.truepay.data.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.truecaller.truepay.data.provider.b.e;

final class b
  extends e
{
  public final void a(Context paramContext, SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    super.a(paramContext, paramSQLiteDatabase, paramInt1, paramInt2);
    paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS transactions");
    paramSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS transactions ( _id INTEGER PRIMARY KEY AUTOINCREMENT, t_id TEXT NOT NULL, seq_no TEXT NOT NULL, transaction_id TEXT NOT NULL, initiator_msisdn TEXT, initiator_user_id TEXT, initiator_vpa TEXT, receiver_user_id TEXT, transaction_type TEXT NOT NULL, initiator_bank_acc_num TEXT, amount REAL NOT NULL, status TEXT, transaction_timestamp INTEGER NOT NULL, initiator_bank_id TEXT, initiator_bank_name TEXT, initiator_bank_symbol TEXT, transaction_message TEXT, remarks TEXT, category TEXT, bank_rrn TEXT, upi_txn_id TEXT, dispute_id TEXT, request_expired INTEGER, payment_flow TEXT, receiver_vpa TEXT, receiver_msisdn TEXT, receiver_name TEXT, receiver_sub_type TEXT, receiver_bank_name TEXT, receiver_bank_symbol TEXT, receiver_bank_acc_num TEXT, dispute_status_check_at TEXT, updated_at INTEGER NOT NULL, sr_number TEXT, user_remarks TEXT, closure_remarks TEXT, action_data_1_title TEXT, action_data_1_type TEXT, action_data_2_title TEXT, action_data_2_type TEXT, utility_initiated_at TEXT, utility_recharge_number TEXT, utility_recharge_status TEXT, utility_recharge_message TEXT, utility_ref_id TEXT, utility_type TEXT, utility_vendor_trnx_id TEXT, utility_operator_name TEXT, utility_operator_type TEXT, utility_loc_name TEXT, utility_location_code TEXT, utility_refund_rrn TEXT, utility_refund_status TEXT, utility_refund_time TEXT, utility_refund_response_code TEXT, utility_refund_message TEXT, utility_refund_vpa TEXT, operator_symbol TEXT, bbps_transaction_id TEXT, operator_icon_url TEXT ,operator_logo_url TEXT ,vendor_logo_url TEXT ,bill_due_date TEXT ,utility_booking_info TEXT ,repeat_transaction_params TEXT ,utility_remarks TEXT ,utility_email TEXT , CONSTRAINT unique_msisdn UNIQUE (t_id) ON CONFLICT REPLACE );");
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS transactions");
    String str1 = "CREATE TABLE IF NOT EXISTS transactions ( _id INTEGER PRIMARY KEY AUTOINCREMENT, t_id TEXT NOT NULL, seq_no TEXT NOT NULL, transaction_id TEXT NOT NULL, initiator_msisdn TEXT, initiator_user_id TEXT, initiator_vpa TEXT, receiver_user_id TEXT, transaction_type TEXT NOT NULL, initiator_bank_acc_num TEXT, amount REAL NOT NULL, status TEXT, transaction_timestamp INTEGER NOT NULL, initiator_bank_id TEXT, initiator_bank_name TEXT, initiator_bank_symbol TEXT, transaction_message TEXT, remarks TEXT, category TEXT, bank_rrn TEXT, upi_txn_id TEXT, dispute_id TEXT, request_expired INTEGER, payment_flow TEXT, receiver_vpa TEXT, receiver_msisdn TEXT, receiver_name TEXT, receiver_sub_type TEXT, receiver_bank_name TEXT, receiver_bank_symbol TEXT, receiver_bank_acc_num TEXT, dispute_status_check_at TEXT, updated_at INTEGER NOT NULL, sr_number TEXT, user_remarks TEXT, closure_remarks TEXT, action_data_1_title TEXT, action_data_1_type TEXT, action_data_2_title TEXT, action_data_2_type TEXT, utility_initiated_at TEXT, utility_recharge_number TEXT, utility_recharge_status TEXT, utility_recharge_message TEXT, utility_ref_id TEXT, utility_type TEXT, utility_vendor_trnx_id TEXT, utility_operator_name TEXT, utility_operator_type TEXT, utility_loc_name TEXT, utility_location_code TEXT, utility_refund_rrn TEXT, utility_refund_status TEXT, utility_refund_time TEXT, utility_refund_response_code TEXT, utility_refund_message TEXT, utility_refund_vpa TEXT, operator_symbol TEXT, bbps_transaction_id TEXT, operator_icon_url TEXT ,operator_logo_url TEXT ,vendor_logo_url TEXT ,bill_due_date TEXT ,utility_booking_info TEXT ,repeat_transaction_params TEXT ,utility_remarks TEXT ,utility_email TEXT , CONSTRAINT unique_msisdn UNIQUE (t_id) ON CONFLICT REPLACE );";
    paramSQLiteDatabase.execSQL(str1);
    int i = 7;
    if (paramInt1 <= i)
    {
      i = 8;
      if (paramInt2 >= i)
      {
        String str2 = "CREATE TABLE IF NOT EXISTS bank_sms ( _id INTEGER PRIMARY KEY AUTOINCREMENT, sim_slot_index INTEGER NOT NULL, bank_name TEXT, bank_symbol TEXT, sms_count INTEGER  );";
        paramSQLiteDatabase.execSQL(str2);
      }
    }
    paramInt2 = 11;
    if (paramInt1 <= paramInt2)
    {
      String str3 = "ALTER TABLE contacts ADD COLUMN bank_registered_name TEXT ;";
      paramSQLiteDatabase.execSQL(str3);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.provider.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */