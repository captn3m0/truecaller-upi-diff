package com.truecaller.truepay.data.f;

import com.truecaller.truepay.SmsBankData;
import io.reactivex.d;
import io.reactivex.o;
import java.util.HashMap;
import java.util.List;

public abstract interface ah
{
  public abstract d a(String paramString);
  
  public abstract o a(SmsBankData paramSmsBankData);
  
  public abstract o a(SmsBankData paramSmsBankData, String paramString, int paramInt);
  
  public abstract HashMap a();
  
  public abstract HashMap b();
  
  public abstract List c();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */