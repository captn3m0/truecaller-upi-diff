package com.truecaller.truepay.data.f;

import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private b(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static b a(Provider paramProvider1, Provider paramProvider2)
  {
    b localb = new com/truecaller/truepay/data/f/b;
    localb.<init>(paramProvider1, paramProvider2);
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */