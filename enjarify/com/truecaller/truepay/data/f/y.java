package com.truecaller.truepay.data.f;

import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private y(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static y a(Provider paramProvider1, Provider paramProvider2)
  {
    y localy = new com/truecaller/truepay/data/f/y;
    localy.<init>(paramProvider1, paramProvider2);
    return localy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */