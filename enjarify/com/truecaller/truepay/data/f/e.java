package com.truecaller.truepay.data.f;

import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private e(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static e a(Provider paramProvider1, Provider paramProvider2)
  {
    e locale = new com/truecaller/truepay/data/f/e;
    locale.<init>(paramProvider1, paramProvider2);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */