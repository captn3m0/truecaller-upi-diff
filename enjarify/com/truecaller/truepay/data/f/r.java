package com.truecaller.truepay.data.f;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import com.truecaller.truepay.data.api.model.z;
import com.truecaller.truepay.data.provider.d.b;
import io.reactivex.a;
import io.reactivex.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class r
  implements q
{
  private final ContentResolver a;
  private final String b = "10";
  private final String c = "13";
  private final int d = 10;
  
  public r(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  public final int a(List paramList)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    paramList = paramList.iterator();
    Object localObject4;
    for (;;)
    {
      bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (com.truecaller.truepay.data.d.d)paramList.next();
      localObject3 = new com/truecaller/truepay/data/provider/d/b;
      ((b)localObject3).<init>();
      localObject4 = a;
      localObject4 = ((b)localObject3).a((String)localObject4);
      String str = b;
      localObject4 = ((b)localObject4).b(str);
      str = f;
      localObject4 = ((b)localObject4).f(str);
      str = e;
      localObject4 = ((b)localObject4).e(str);
      localObject2 = d;
      ((b)localObject4).d((String)localObject2);
      ((List)localObject1).add(localObject3);
    }
    int j = ((List)localObject1).size();
    paramList = new ContentValues[j];
    boolean bool1 = false;
    Object localObject2 = null;
    int k = 0;
    Object localObject3 = null;
    for (;;)
    {
      int m = ((List)localObject1).size();
      if (k >= m) {
        break;
      }
      localObject4 = ((b)((List)localObject1).get(k)).c();
      paramList[k] = localObject4;
      k += 1;
    }
    boolean bool2 = ((List)localObject1).isEmpty();
    int i;
    if (!bool2)
    {
      localObject3 = a;
      localObject1 = ((b)((List)localObject1).get(0)).b();
      i = ((ContentResolver)localObject3).bulkInsert((Uri)localObject1, paramList);
    }
    return i;
  }
  
  public final io.reactivex.d a(String paramString)
  {
    -..Lambda.r.tCHe_cgVqKorau6_hc20UNSkvP4 localtCHe_cgVqKorau6_hc20UNSkvP4 = new com/truecaller/truepay/data/f/-$$Lambda$r$tCHe_cgVqKorau6_hc20UNSkvP4;
    localtCHe_cgVqKorau6_hc20UNSkvP4.<init>(this, paramString);
    paramString = a.c;
    return io.reactivex.d.a(localtCHe_cgVqKorau6_hc20UNSkvP4, paramString);
  }
  
  public final List a()
  {
    Object localObject1 = new com/truecaller/truepay/data/provider/d/d;
    ((com.truecaller.truepay.data.provider.d.d)localObject1).<init>();
    Object localObject2 = a;
    Object localObject3 = { "msisdn" };
    localObject1 = ((com.truecaller.truepay.data.provider.d.d)localObject1).a((ContentResolver)localObject2, (String[])localObject3);
    localObject2 = new java/util/ArrayList;
    int i = ((com.truecaller.truepay.data.provider.d.c)localObject1).getCount();
    ((ArrayList)localObject2).<init>(i);
    ((com.truecaller.truepay.data.provider.d.c)localObject1).moveToFirst();
    ((com.truecaller.truepay.data.provider.d.c)localObject1).moveToFirst();
    for (;;)
    {
      boolean bool = ((com.truecaller.truepay.data.provider.d.c)localObject1).isAfterLast();
      if (bool) {
        break;
      }
      localObject3 = ((com.truecaller.truepay.data.provider.d.c)localObject1).a();
      ((List)localObject2).add(localObject3);
      ((com.truecaller.truepay.data.provider.d.c)localObject1).moveToNext();
    }
    ((com.truecaller.truepay.data.provider.d.c)localObject1).close();
    return (List)localObject2;
  }
  
  public final int b(String paramString)
  {
    if (paramString == null) {
      return 0;
    }
    com.truecaller.truepay.data.provider.d.d locald = new com/truecaller/truepay/data/provider/d/d;
    locald.<init>();
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    locald.b(arrayOfString);
    paramString = a;
    return locald.b(paramString);
  }
  
  public final o b()
  {
    -..Lambda.r.6FgcVpSyM6S6yRPqx1RCA5P6bPk local6FgcVpSyM6S6yRPqx1RCA5P6bPk = new com/truecaller/truepay/data/f/-$$Lambda$r$6FgcVpSyM6S6yRPqx1RCA5P6bPk;
    local6FgcVpSyM6S6yRPqx1RCA5P6bPk.<init>(this, "1");
    return o.a(local6FgcVpSyM6S6yRPqx1RCA5P6bPk);
  }
  
  public final void b(List paramList)
  {
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (z)paramList.next();
      Object localObject2 = new com/truecaller/truepay/data/provider/d/d;
      ((com.truecaller.truepay.data.provider.d.d)localObject2).<init>();
      int i = 1;
      Object localObject3 = new String[i];
      Object localObject4 = a;
      localObject3[0] = localObject4;
      localObject2 = ((com.truecaller.truepay.data.provider.d.d)localObject2).b((String[])localObject3);
      localObject3 = a;
      localObject3 = ((com.truecaller.truepay.data.provider.d.d)localObject2).a((ContentResolver)localObject3, null);
      ((com.truecaller.truepay.data.provider.d.c)localObject3).moveToFirst();
      Object localObject5 = new com/truecaller/truepay/data/provider/d/b;
      ((b)localObject5).<init>();
      localObject4 = c;
      localObject5 = ((b)localObject5).h((String)localObject4);
      localObject4 = b;
      localObject5 = ((b)localObject5).g((String)localObject4);
      localObject4 = Boolean.TRUE;
      localObject5 = ((b)localObject5).a((Boolean)localObject4);
      localObject1 = d;
      localObject1 = ((b)localObject5).i((String)localObject1);
      localObject5 = a;
      localObject4 = ((b)localObject1).b();
      localObject1 = ((b)localObject1).c();
      String str = a.toString();
      localObject2 = ((com.truecaller.truepay.data.provider.d.d)localObject2).e();
      ((ContentResolver)localObject5).update((Uri)localObject4, (ContentValues)localObject1, str, (String[])localObject2);
      ((com.truecaller.truepay.data.provider.d.c)localObject3).close();
    }
  }
  
  public final o c()
  {
    -..Lambda.r.NClCD20hJ3RC5mpMb44WeCxajIA localNClCD20hJ3RC5mpMb44WeCxajIA = new com/truecaller/truepay/data/f/-$$Lambda$r$NClCD20hJ3RC5mpMb44WeCxajIA;
    localNClCD20hJ3RC5mpMb44WeCxajIA.<init>(this);
    return o.a(localNClCD20hJ3RC5mpMb44WeCxajIA);
  }
  
  public final o c(String paramString)
  {
    -..Lambda.r.Z-8W0VlDZ6kebOGeWUp5sOv57Uk localZ-8W0VlDZ6kebOGeWUp5sOv57Uk = new com/truecaller/truepay/data/f/-$$Lambda$r$Z-8W0VlDZ6kebOGeWUp5sOv57Uk;
    localZ-8W0VlDZ6kebOGeWUp5sOv57Uk.<init>(this, paramString);
    return o.a(localZ-8W0VlDZ6kebOGeWUp5sOv57Uk);
  }
  
  public final o d()
  {
    -..Lambda.r.8HIJfXimRqjmJBCnMUWI37FbJKM local8HIJfXimRqjmJBCnMUWI37FbJKM = new com/truecaller/truepay/data/f/-$$Lambda$r$8HIJfXimRqjmJBCnMUWI37FbJKM;
    local8HIJfXimRqjmJBCnMUWI37FbJKM.<init>(this);
    return o.a(local8HIJfXimRqjmJBCnMUWI37FbJKM);
  }
  
  public final o d(String paramString)
  {
    -..Lambda.r.VSQQ8GLUuNhOD7xxCw0P-0oJZBg localVSQQ8GLUuNhOD7xxCw0P-0oJZBg = new com/truecaller/truepay/data/f/-$$Lambda$r$VSQQ8GLUuNhOD7xxCw0P-0oJZBg;
    localVSQQ8GLUuNhOD7xxCw0P-0oJZBg.<init>(this, paramString);
    return o.a(localVSQQ8GLUuNhOD7xxCw0P-0oJZBg);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */