package com.truecaller.truepay.data.f;

import dagger.a.d;
import javax.inject.Provider;

public final class ae
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private ae(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static ae a(Provider paramProvider1, Provider paramProvider2)
  {
    ae localae = new com/truecaller/truepay/data/f/ae;
    localae.<init>(paramProvider1, paramProvider2);
    return localae;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */