package com.truecaller.truepay.data.f;

import com.truecaller.truepay.SmsBankData;
import io.reactivex.d;
import io.reactivex.o;
import java.util.HashMap;
import java.util.List;

public final class ag
  implements ah
{
  private ah a;
  
  public ag(ah paramah)
  {
    a = paramah;
  }
  
  public final d a(String paramString)
  {
    return a.a(paramString);
  }
  
  public final o a(SmsBankData paramSmsBankData)
  {
    return a.a(paramSmsBankData);
  }
  
  public final o a(SmsBankData paramSmsBankData, String paramString, int paramInt)
  {
    return a.a(paramSmsBankData, paramString, paramInt);
  }
  
  public final HashMap a()
  {
    return a.a();
  }
  
  public final HashMap b()
  {
    return a.b();
  }
  
  public final List c()
  {
    return a.c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */