package com.truecaller.truepay.data.f;

import com.truecaller.truepay.app.ui.transaction.b.d;
import com.truecaller.truepay.app.ui.transaction.b.j;
import com.truecaller.truepay.data.api.model.ah;
import com.truecaller.truepay.data.api.model.u;
import io.reactivex.o;

public final class x
  implements z
{
  z a;
  z b;
  
  public x(z paramz1, z paramz2)
  {
    a = paramz1;
    b = paramz2;
  }
  
  public final o a()
  {
    return b.a();
  }
  
  public final o a(d paramd)
  {
    return b.a(paramd);
  }
  
  public final o a(j paramj)
  {
    o localo = a.a(paramj);
    if (localo != null) {
      return a.a(paramj);
    }
    return b.a(paramj);
  }
  
  public final o a(ah paramah)
  {
    return b.a(paramah);
  }
  
  public final o a(u paramu)
  {
    return b.a(paramu);
  }
  
  public final o b(ah paramah)
  {
    return b.b(paramah);
  }
  
  public final o c(ah paramah)
  {
    return b.c(paramah);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */