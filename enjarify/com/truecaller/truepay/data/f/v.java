package com.truecaller.truepay.data.f;

import com.truecaller.truepay.app.ui.history.models.i;
import com.truecaller.truepay.data.e.b;
import io.reactivex.n;
import io.reactivex.o;
import java.io.IOException;
import java.util.List;

public final class v
  implements w
{
  private final b a;
  private final com.truecaller.truepay.data.e.e b;
  private w c;
  private w d;
  
  public v(w paramw1, w paramw2, b paramb, com.truecaller.truepay.data.e.e parame)
  {
    c = paramw1;
    d = paramw2;
    a = paramb;
    b = parame;
  }
  
  private io.reactivex.d a(int paramInt, i parami)
  {
    Object localObject = Integer.valueOf(paramInt);
    b = ((Integer)localObject);
    localObject = d.a(parami, "all").b();
    -..Lambda.v.UXfgCTSkWifkgSlWsIykwNf3rKY localUXfgCTSkWifkgSlWsIykwNf3rKY = new com/truecaller/truepay/data/f/-$$Lambda$v$UXfgCTSkWifkgSlWsIykwNf3rKY;
    localUXfgCTSkWifkgSlWsIykwNf3rKY.<init>(this, parami);
    return ((io.reactivex.d)localObject).a(localUXfgCTSkWifkgSlWsIykwNf3rKY);
  }
  
  public final int a()
  {
    return c.a();
  }
  
  public final io.reactivex.d a(String paramString)
  {
    return c.a(null, paramString).b();
  }
  
  public final o a(i parami, String paramString)
  {
    return null;
  }
  
  public final void a(List paramList) {}
  
  public final io.reactivex.d b()
  {
    Object localObject1 = b;
    boolean bool = ((com.truecaller.truepay.data.e.e)localObject1).b();
    if (bool)
    {
      localObject1 = "";
      Object localObject2 = b.a();
      bool = ((String)localObject1).equals(localObject2);
      if (!bool)
      {
        localObject1 = new com/truecaller/truepay/app/ui/history/models/i;
        localObject2 = b.a();
        Object localObject3 = "all";
        ((i)localObject1).<init>((String)localObject2, (String)localObject3);
        long l1 = System.currentTimeMillis();
        long l2 = 1000L;
        l1 /= l2;
        int i = a();
        if (i == 0)
        {
          l2 = 0L;
        }
        else
        {
          localObject4 = a.a();
          l2 = ((Long)localObject4).longValue();
        }
        Object localObject4 = String.valueOf(l2);
        a = ((String)localObject4);
        localObject1 = a(1, (i)localObject1);
        localObject4 = -..Lambda.lQISZ-QQdp7HUwY06PHENVgPkII.INSTANCE;
        localObject1 = ((io.reactivex.d)localObject1).a((io.reactivex.c.e)localObject4);
        localObject4 = new com/truecaller/truepay/data/f/-$$Lambda$v$W90HFF0B4KpoZ7BtF2cD3ydYAkA;
        ((-..Lambda.v.W90HFF0B4KpoZ7BtF2cD3ydYAkA)localObject4).<init>(this, l1);
        localObject2 = io.reactivex.d.b.a.a();
        localObject3 = io.reactivex.d.b.a.c;
        localObject1 = ((io.reactivex.d)localObject1).a((io.reactivex.c.d)localObject4, (io.reactivex.c.d)localObject2, (io.reactivex.c.a)localObject3, (io.reactivex.c.a)localObject3);
        localObject2 = io.reactivex.g.a.b();
        return ((io.reactivex.d)localObject1).b((n)localObject2);
      }
    }
    localObject1 = new java/io/IOException;
    ((IOException)localObject1).<init>("Couldn't fetch your transactions.");
    return io.reactivex.d.b((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */