package com.truecaller.truepay.data.f;

import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private u(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static u a(Provider paramProvider1, Provider paramProvider2)
  {
    u localu = new com/truecaller/truepay/data/f/u;
    localu.<init>(paramProvider1, paramProvider2);
    return localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */