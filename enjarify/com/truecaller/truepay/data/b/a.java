package com.truecaller.truepay.data.b;

import android.text.TextUtils;
import com.truecaller.log.d;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.data.e.e;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import okhttp3.u;
import org.json.JSONException;
import org.json.JSONObject;

public class a
{
  private e a;
  private e b;
  private e c;
  private e d;
  private e e;
  private e f;
  private e g;
  private com.truecaller.truepay.data.e.b h;
  
  public a(e parame1, e parame2, e parame3, e parame4, e parame5, e parame6, e parame7, com.truecaller.truepay.data.e.b paramb)
  {
    a = parame1;
    b = parame2;
    c = parame3;
    d = parame4;
    e = parame5;
    f = parame6;
    g = parame7;
    h = paramb;
  }
  
  public static String a()
  {
    long l = System.currentTimeMillis() / 1000L;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(l);
    localStringBuilder.append("-");
    String str = UUID.randomUUID().toString().replaceAll("-", "");
    localStringBuilder.append(str);
    return localStringBuilder.toString();
  }
  
  private String a(boolean paramBoolean)
  {
    if (paramBoolean) {
      return "dual_sim";
    }
    return "single_sim";
  }
  
  private void a(JSONObject paramJSONObject)
  {
    paramJSONObject.put("reg_version", "1.0");
  }
  
  public void a(String paramString)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "BANNER_ID";
    try
    {
      localJSONObject.put(str, paramString);
      paramString = b.a();
      str = "PayHomeScreenBanner";
      paramString.a(str, localJSONObject);
      paramString = b.a();
      str = "app_payment_home_screen_banner";
      paramString.a(str, null, localJSONObject, null);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void a(String paramString, Object paramObject)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put(paramString, paramObject);
    b(localHashMap);
  }
  
  public void a(String paramString1, String paramString2)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    try
    {
      a(localJSONObject1);
      Object localObject1 = "psp";
      Object localObject2 = g;
      localObject2 = ((e)localObject2).a();
      localJSONObject1.put((String)localObject1, localObject2);
      localObject1 = "STATUS";
      localJSONObject1.put((String)localObject1, paramString1);
      paramString1 = "CONTEXT";
      localJSONObject1.put(paramString1, paramString2);
      paramString1 = b.a();
      paramString2 = "PaySelectBank";
      paramString1.a(paramString2, localJSONObject1);
      paramString1 = "REGISTER_ID";
      paramString2 = b;
      paramString2 = paramString2.a();
      localJSONObject1.put(paramString1, paramString2);
      paramString1 = "PHONE_NUMBER";
      paramString2 = a;
      paramString2 = paramString2.a();
      localJSONObject1.put(paramString1, paramString2);
      paramString1 = b.a();
      paramString2 = "app_payment_select_bank";
      localObject1 = c;
      localObject1 = ((e)localObject1).a();
      paramString1.a(paramString2, (String)localObject1, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, double paramDouble)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    Object localObject = "CONTEXT";
    try
    {
      localJSONObject1.put((String)localObject, paramString2);
      paramString2 = "CONTACTS_COUNT";
      localJSONObject1.put(paramString2, paramDouble);
      paramString2 = b.a();
      localObject = "PayContactInvite";
      paramString2.a((String)localObject, localJSONObject1);
      paramString2 = "REGISTER_ID";
      localObject = b;
      localObject = ((e)localObject).a();
      localJSONObject1.put(paramString2, localObject);
      paramString2 = "PHONE_NUMBER";
      localObject = a;
      localObject = ((e)localObject).a();
      localJSONObject1.put(paramString2, localObject);
      paramString2 = "CONTACTS_COUNT";
      boolean bool = localJSONObject1.has(paramString2);
      if (bool)
      {
        paramString2 = "CONTACTS_COUNT";
        localJSONObject1.remove(paramString2);
      }
      paramString2 = "CONTACTS_COUNT";
      localJSONObject2.put(paramString2, paramDouble);
      paramString2 = b.a();
      String str = a();
      paramString2.a(paramString1, str, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, com.truecaller.truepay.data.e.a parama, String paramString3)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("PayLatestTransactionStatus", paramString1);
    String str1 = "PayLatestTransactionDate";
    String str2 = b();
    localHashMap.put(str1, str2);
    if (paramString2 != null)
    {
      str1 = "PayLatestTransactionType";
      localHashMap.put(str1, paramString2);
    }
    if (paramString3 != null)
    {
      paramString2 = "PayTransactionResponseCode";
      localHashMap.put(paramString2, paramString3);
    }
    paramString2 = "success";
    boolean bool = paramString2.equalsIgnoreCase(paramString1);
    if (bool)
    {
      paramString1 = parama.a();
      bool = paramString1.booleanValue();
      if (!bool)
      {
        paramString2 = "complete";
        localHashMap.put("PayFirstTransactionStatus", paramString2);
        paramString1 = Boolean.TRUE;
        parama.a(paramString1);
      }
    }
    paramString1 = h;
    paramString2 = Long.valueOf(System.currentTimeMillis());
    paramString1.a(paramString2);
    b(localHashMap);
  }
  
  public void a(String paramString1, String paramString2, String paramString3)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    if (paramString3 == null) {
      paramString3 = "item";
    }
    try
    {
      localJSONObject1.put(paramString3, paramString1);
      break label49;
      paramString1 = "item";
      localJSONObject1.put(paramString1, paramString3);
      label49:
      paramString1 = "source";
      localJSONObject1.put(paramString1, paramString2);
      paramString1 = b.a();
      paramString2 = "PayTile";
      paramString1.a(paramString2, localJSONObject1);
      paramString1 = "REGISTER_ID";
      paramString2 = b;
      paramString2 = paramString2.a();
      localJSONObject1.put(paramString1, paramString2);
      paramString1 = "PHONE_NUMBER";
      paramString2 = a;
      paramString2 = paramString2.a();
      localJSONObject1.put(paramString1, paramString2);
      paramString1 = b.a();
      paramString2 = "app_payment_tile";
      paramString3 = a();
      paramString1.a(paramString2, paramString3, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    String str = "STATUS";
    try
    {
      localJSONObject1.put(str, paramString2);
      paramString2 = "CONTEXT";
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "ACTION";
      localJSONObject1.put(paramString2, paramString4);
      paramString2 = b.a();
      paramString3 = "PaySetPin";
      paramString2.a(paramString3, localJSONObject1);
      paramString2 = "REGISTER_ID";
      paramString3 = b;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "PHONE_NUMBER";
      paramString3 = a;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = b.a();
      paramString3 = e;
      paramString3 = paramString3.a();
      paramString2.a(paramString1, paramString3, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    String str = "CONTEXT";
    try
    {
      localJSONObject1.put(str, paramString2);
      paramString2 = "ACTION";
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "source";
      localJSONObject1.put(paramString2, paramString4);
      paramString2 = "REGISTERED";
      localJSONObject1.put(paramString2, paramBoolean);
      paramString2 = b.a();
      paramString3 = "PayTransactionIntent";
      paramString2.a(paramString3, localJSONObject1);
      paramString2 = "REGISTER_ID";
      paramString3 = b;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "PHONE_NUMBER";
      paramString3 = a;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = b.a();
      paramString3 = d;
      paramString3 = paramString3.a();
      paramString2.a(paramString1, paramString3, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, double paramDouble, String paramString6)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    try
    {
      a(localJSONObject1);
      String str = "psp";
      Object localObject = g;
      localObject = ((e)localObject).a();
      localJSONObject1.put(str, localObject);
      str = "STATUS";
      localJSONObject1.put(str, paramString2);
      paramString2 = "CONTEXT";
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "ACTION";
      localJSONObject1.put(paramString2, paramString4);
      paramString2 = "BANK";
      localJSONObject1.put(paramString2, paramString5);
      paramString2 = "ACCOUNTS_COUNT";
      localJSONObject2.put(paramString2, paramDouble);
      paramString2 = b.a();
      paramString3 = "PaySelectBank";
      paramString2.a(paramString3, localJSONObject1);
      paramString2 = "SELECTED_ACCOUNT";
      localJSONObject1.put(paramString2, paramString6);
      paramString2 = "REGISTER_ID";
      paramString3 = b;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "PHONE_NUMBER";
      paramString3 = a;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = b.a();
      paramString3 = c;
      paramString3 = paramString3.a();
      paramString2.a(paramString1, paramString3, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, p paramp, boolean paramBoolean1, boolean paramBoolean2)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    String str = "STATUS";
    try
    {
      localJSONObject1.put(str, paramString2);
      paramString2 = "CONTEXT";
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "ACTION";
      localJSONObject1.put(paramString2, paramString4);
      paramString2 = "AMOUNT";
      paramString3 = e;
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "TYPE";
      paramString3 = d;
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "BANK";
      paramString3 = k;
      paramString3 = j;
      paramString3 = b;
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "utility";
      paramString3 = d;
      boolean bool1 = paramString2.equals(paramString3);
      if (bool1)
      {
        paramString2 = "UTILITY-CATEGORY";
        paramString3 = v;
        localJSONObject1.put(paramString2, paramString3);
        paramString2 = "OPERATOR";
        paramString3 = t;
        localJSONObject1.put(paramString2, paramString3);
        paramString2 = "PLAN_SELECTED";
        boolean bool2 = x;
        localJSONObject1.put(paramString2, bool2);
        paramString2 = "RECENT_SELECTED";
        localJSONObject1.put(paramString2, paramBoolean2);
        paramString2 = "BBPS";
        localJSONObject1.put(paramString2, paramBoolean1);
        paramString2 = B;
        if (paramString2 != null)
        {
          paramString2 = "VENDOR";
          paramString3 = B;
          localJSONObject1.put(paramString2, paramString3);
        }
      }
      paramString2 = b.a();
      paramString3 = "PayTransactionInitiated";
      paramString2.a(paramString3, localJSONObject1);
      paramString2 = "AMOUNT";
      localJSONObject1.remove(paramString2);
      paramString2 = "REGISTER_ID";
      paramString3 = b;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "PHONE_NUMBER";
      paramString3 = a;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "RECEIVER";
      localJSONObject1.put(paramString2, paramString5);
      paramString2 = "TRANSACTION_ID";
      paramString3 = a;
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "SELECTED_ACCOUNT";
      paramString3 = k;
      paramString3 = c;
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = e;
      bool1 = TextUtils.isEmpty(paramString2);
      if (!bool1)
      {
        paramString2 = "AMOUNT";
        paramString3 = e;
        double d1 = Double.parseDouble(paramString3);
        localJSONObject2.put(paramString2, d1);
      }
      paramString2 = b.a();
      paramString3 = d;
      paramString3 = paramString3.a();
      paramString2.a(paramString1, paramString3, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, Boolean paramBoolean, String paramString13)
  {
    a locala = this;
    Object localObject1 = paramString5;
    Object localObject2 = paramString13;
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    Object localObject3 = "STATUS";
    try
    {
      localJSONObject1.put((String)localObject3, paramString2);
      localObject3 = "CONTEXT";
      localJSONObject1.put((String)localObject3, paramString3);
      localObject3 = "AMOUNT";
      localJSONObject1.put((String)localObject3, paramString4);
      localObject3 = "TYPE";
      localJSONObject1.put((String)localObject3, paramString5);
      localObject3 = "BANK";
      localJSONObject1.put((String)localObject3, paramString9);
      localObject3 = "RESPONSE_CODE";
      localJSONObject1.put((String)localObject3, paramString11);
      localObject3 = "utility";
      boolean bool = ((String)localObject3).equals(paramString5);
      if (bool)
      {
        localObject1 = "UTILITY-CATEGORY";
        localObject3 = paramString12;
        localJSONObject1.put((String)localObject1, paramString12);
        localObject1 = "BBPS";
        localObject3 = paramBoolean;
        localJSONObject1.put((String)localObject1, paramBoolean);
        if (paramString13 != null)
        {
          localObject1 = "VENDOR";
          localJSONObject1.put((String)localObject1, paramString13);
        }
      }
      localObject1 = b.a();
      localObject2 = "PayTransactionStatus";
      ((b)localObject1).a((String)localObject2, localJSONObject1);
      localObject1 = "AMOUNT";
      localJSONObject1.remove((String)localObject1);
      localObject1 = "REGISTER_ID";
      localObject2 = b;
      localObject2 = ((e)localObject2).a();
      localJSONObject1.put((String)localObject1, localObject2);
      localObject1 = "PHONE_NUMBER";
      localObject2 = a;
      localObject2 = ((e)localObject2).a();
      localJSONObject1.put((String)localObject1, localObject2);
      localObject1 = "RECEIVER";
      localObject2 = paramString6;
      localJSONObject1.put((String)localObject1, paramString6);
      localObject1 = "TRANSACTION_ID";
      localObject2 = paramString7;
      localJSONObject1.put((String)localObject1, paramString7);
      localObject1 = "BANK_RRN";
      localObject2 = paramString10;
      localJSONObject1.put((String)localObject1, paramString10);
      localObject1 = "SELECTED_ACCOUNT";
      localObject2 = paramString8;
      localJSONObject1.put((String)localObject1, paramString8);
      bool = TextUtils.isEmpty(paramString4);
      if (!bool)
      {
        localObject1 = "AMOUNT";
        double d1 = Double.parseDouble(paramString4);
        localJSONObject2.put((String)localObject1, d1);
      }
      localObject1 = b.a();
      localObject2 = d;
      localObject2 = ((e)localObject2).a();
      localObject3 = paramString1;
      ((b)localObject1).a(paramString1, (String)localObject2, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString10)
  {
    a locala = this;
    Object localObject1 = paramString3;
    Object localObject2 = paramString9;
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    Object localObject3 = "STATUS";
    try
    {
      localJSONObject1.put((String)localObject3, paramString1);
      localObject3 = "CONTEXT";
      localJSONObject1.put((String)localObject3, paramString10);
      localObject3 = "AMOUNT";
      localJSONObject1.put((String)localObject3, paramString2);
      localObject3 = "TYPE";
      localJSONObject1.put((String)localObject3, paramString3);
      localObject3 = "BANK";
      localJSONObject1.put((String)localObject3, paramString5);
      localObject3 = "RESPONSE_CODE";
      localJSONObject1.put((String)localObject3, paramString7);
      if (paramString9 != null)
      {
        localObject3 = "VENDOR";
        localJSONObject1.put((String)localObject3, paramString9);
      }
      localObject2 = "utility";
      boolean bool = ((String)localObject2).equals(paramString3);
      if (bool)
      {
        localObject1 = "UTILITY-CATEGORY";
        localObject2 = paramString8;
        localJSONObject1.put((String)localObject1, paramString8);
        localObject1 = "PLAN_SELECTED";
        localJSONObject1.put((String)localObject1, paramBoolean1);
        localObject1 = "RECENT_SELECTED";
        localJSONObject1.put((String)localObject1, paramBoolean3);
        localObject1 = "BBPS";
        localJSONObject1.put((String)localObject1, paramBoolean2);
      }
      localObject1 = b.a();
      localObject2 = "PayUtilityStatus";
      ((b)localObject1).a((String)localObject2, localJSONObject1);
      localObject1 = "AMOUNT";
      localJSONObject1.remove((String)localObject1);
      localObject1 = "REGISTER_ID";
      localObject2 = b;
      localObject2 = ((e)localObject2).a();
      localJSONObject1.put((String)localObject1, localObject2);
      localObject1 = "PHONE_NUMBER";
      localObject2 = a;
      localObject2 = ((e)localObject2).a();
      localJSONObject1.put((String)localObject1, localObject2);
      localObject1 = "TRANSACTION_ID";
      localObject2 = paramString4;
      localJSONObject1.put((String)localObject1, paramString4);
      localObject1 = "BANK_RRN";
      localObject2 = paramString6;
      localJSONObject1.put((String)localObject1, paramString6);
      bool = TextUtils.isEmpty(paramString2);
      if (!bool)
      {
        localObject1 = "AMOUNT";
        double d1 = Double.parseDouble(paramString2);
        localJSONObject2.put((String)localObject1, d1);
      }
      localObject1 = b.a();
      localObject2 = "app_payment_utility_satus";
      localObject3 = d;
      localObject3 = ((e)localObject3).a();
      ((b)localObject1).a((String)localObject2, (String)localObject3, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean1, boolean paramBoolean2)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    try
    {
      a(localJSONObject1);
      String str = "psp";
      Object localObject = g;
      localObject = ((e)localObject).a();
      localJSONObject1.put(str, localObject);
      str = "STATUS";
      localJSONObject1.put(str, paramString2);
      paramString2 = "CONTEXT";
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "ACTION";
      localJSONObject1.put(paramString2, paramString4);
      paramString2 = "SIM_TYPE";
      localJSONObject1.put(paramString2, paramString5);
      paramString2 = "SMS_SENT";
      localJSONObject1.put(paramString2, paramBoolean2);
      paramString2 = b.a();
      paramString3 = "PayDeviceRegistration";
      paramString2.a(paramString3, localJSONObject1);
      paramString2 = "REGISTER_ID";
      paramString3 = b;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "PHONE_NUMBER";
      paramString3 = a;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = b.a();
      paramString3 = c;
      paramString3 = paramString3.a();
      paramString2.a(paramString1, paramString3, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, String paramString5)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    String str = "STATUS";
    try
    {
      localJSONObject1.put(str, paramString1);
      paramString1 = "CONTEXT";
      localJSONObject1.put(paramString1, paramString5);
      paramString1 = "AMOUNT";
      localJSONObject1.put(paramString1, paramString3);
      paramString1 = "UTILITY-CATEGORY";
      localJSONObject1.put(paramString1, paramString2);
      if (paramString4 != null)
      {
        paramString1 = "OPERATOR";
        localJSONObject1.put(paramString1, paramString4);
      }
      paramString1 = "RECENT_SELECTED";
      localJSONObject1.put(paramString1, paramBoolean);
      paramString1 = b.a();
      paramString2 = "PayUtilityBillFetch";
      paramString1.a(paramString2, localJSONObject1);
      paramString1 = "AMOUNT";
      localJSONObject1.remove(paramString1);
      paramString1 = "REGISTER_ID";
      paramString2 = b;
      paramString2 = paramString2.a();
      localJSONObject1.put(paramString1, paramString2);
      paramString1 = "PHONE_NUMBER";
      paramString2 = a;
      paramString2 = paramString2.a();
      localJSONObject1.put(paramString1, paramString2);
      boolean bool = TextUtils.isEmpty(paramString3);
      if (!bool)
      {
        paramString1 = "AMOUNT";
        double d1 = Double.parseDouble(paramString3);
        localJSONObject2.put(paramString1, d1);
      }
      paramString1 = b.a();
      paramString2 = "app_payment_utility_bill_fetch";
      paramString3 = d;
      paramString3 = paramString3.a();
      paramString1.a(paramString2, paramString3, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, boolean paramBoolean)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    try
    {
      a(localJSONObject);
      String str = "psp";
      Object localObject1 = g;
      localObject1 = ((e)localObject1).a();
      localJSONObject.put(str, localObject1);
      str = "source";
      localJSONObject.put(str, paramString1);
      paramString1 = "CONTEXT";
      localJSONObject.put(paramString1, paramString2);
      paramString1 = "SIM_TYPE";
      paramString2 = a(paramBoolean);
      localJSONObject.put(paramString1, paramString2);
      paramString1 = b.a();
      paramString2 = "PayRegistrationShown";
      paramString1.a(paramString2, localJSONObject);
      paramString1 = b.a();
      paramString2 = "app_payment_registration_shown";
      Object localObject2 = c;
      localObject2 = ((e)localObject2).a();
      str = null;
      paramString1.a(paramString2, (String)localObject2, localJSONObject, null);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    try
    {
      a(localJSONObject1);
      String str = "psp";
      Object localObject1 = g;
      localObject1 = ((e)localObject1).a();
      localJSONObject1.put(str, localObject1);
      str = "CONTEXT";
      localJSONObject1.put(str, paramString2);
      paramString2 = "NEW_USER";
      localJSONObject1.put(paramString2, paramBoolean1);
      paramString2 = "PIN_VERIFICATION";
      localJSONObject1.put(paramString2, paramBoolean2);
      paramString2 = b.a();
      Object localObject2 = "PayRegistrationComplete";
      paramString2.a((String)localObject2, localJSONObject1);
      paramString2 = "REGISTER_ID";
      localObject2 = b;
      localObject2 = ((e)localObject2).a();
      localJSONObject1.put(paramString2, localObject2);
      paramString2 = "PHONE_NUMBER";
      localObject2 = a;
      localObject2 = ((e)localObject2).a();
      localJSONObject1.put(paramString2, localObject2);
      paramString2 = b.a();
      localObject2 = c;
      localObject2 = ((e)localObject2).a();
      paramString2.a(paramString1, (String)localObject2, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(String paramString, u paramu)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "error_code";
    try
    {
      localJSONObject.put(str, paramString);
      paramString = "api";
      paramu = paramu.toString();
      localJSONObject.put(paramString, paramu);
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
    b.a().c("PayServerError", localJSONObject);
    b.a().a("app_payment_server_error", null, localJSONObject, null);
  }
  
  public void a(String paramString, JSONObject paramJSONObject)
  {
    Object localObject1 = "REGISTER_ID";
    try
    {
      Object localObject2 = b;
      localObject2 = ((e)localObject2).a();
      paramJSONObject.put((String)localObject1, localObject2);
      localObject1 = "PHONE_NUMBER";
      localObject2 = a;
      localObject2 = ((e)localObject2).a();
      paramJSONObject.put((String)localObject1, localObject2);
      localObject1 = b.a();
      localObject2 = null;
      ((b)localObject1).a(paramString, null, paramJSONObject, null);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void a(String paramString, boolean paramBoolean)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str1 = "VENDOR";
    try
    {
      localJSONObject.put(str1, paramString);
      paramString = "LOGGED_IN";
      localJSONObject.put(paramString, paramBoolean);
      paramString = b.a();
      String str2 = "PayLoginPwa";
      paramString.a(str2, localJSONObject);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void a(String paramString1, boolean paramBoolean1, String paramString2, boolean paramBoolean2)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    try
    {
      a(localJSONObject);
      String str1 = "psp";
      Object localObject = g;
      localObject = ((e)localObject).a();
      localJSONObject.put(str1, localObject);
      str1 = "source";
      localJSONObject.put(str1, paramString1);
      paramString1 = "CONTEXT";
      localJSONObject.put(paramString1, paramString2);
      paramString1 = "sms_data";
      localJSONObject.put(paramString1, paramBoolean1);
      paramString1 = "SIM_TYPE";
      String str2 = a(paramBoolean2);
      localJSONObject.put(paramString1, str2);
      paramString1 = b.a();
      str2 = "PayRegistrationInitiated";
      paramString1.a(str2, localJSONObject);
      paramString1 = b.a();
      str2 = "app_payment_registration_initiated";
      paramString2 = c;
      paramString2 = paramString2.a();
      paramBoolean2 = false;
      paramString1.a(str2, paramString2, localJSONObject, null);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(boolean paramBoolean, String paramString1, String paramString2)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    try
    {
      a(localJSONObject);
      String str = "psp";
      Object localObject1 = g;
      localObject1 = ((e)localObject1).a();
      localJSONObject.put(str, localObject1);
      str = "sms_data";
      localJSONObject.put(str, paramBoolean);
      Object localObject2 = "CONTEXT";
      localJSONObject.put((String)localObject2, paramString1);
      localObject2 = "SIM_TYPE";
      localJSONObject.put((String)localObject2, paramString2);
      localObject2 = b.a();
      paramString1 = "PaySIMSelection";
      ((b)localObject2).a(paramString1, localJSONObject);
      localObject2 = b.a();
      paramString1 = "app_payment_sim_selection";
      paramString2 = c;
      paramString2 = paramString2.a();
      str = null;
      ((b)localObject2).a(paramString1, paramString2, localJSONObject, null);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    try
    {
      a(localJSONObject);
      String str1 = "psp";
      Object localObject1 = g;
      localObject1 = ((e)localObject1).a();
      localJSONObject.put(str1, localObject1);
      str1 = "sms_data";
      localJSONObject.put(str1, paramBoolean1);
      Object localObject2 = "sms_data_changed";
      localJSONObject.put((String)localObject2, paramBoolean2);
      localObject2 = "SIM_TYPE";
      String str2 = a(paramBoolean3);
      localJSONObject.put((String)localObject2, str2);
      localObject2 = b.a();
      str2 = "PayRegistrationStarted";
      ((b)localObject2).a(str2, localJSONObject);
      localObject2 = b.a();
      str2 = "app_payment_registration_started";
      Object localObject3 = c;
      localObject3 = ((e)localObject3).a();
      str1 = null;
      ((b)localObject2).a(str2, (String)localObject3, localJSONObject, null);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public boolean a(HashMap paramHashMap)
  {
    Object localObject1 = "sim_count";
    try
    {
      Object localObject2 = paramHashMap.keySet();
      int i = ((Set)localObject2).size();
      localObject2 = Integer.valueOf(i);
      paramHashMap.put(localObject1, localObject2);
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>(paramHashMap);
      localObject2 = b.a();
      String str = "app_payment_sms_data";
      boolean bool1 = false;
      Object localObject3 = null;
      ((b)localObject2).a(str, null, (JSONObject)localObject1, null);
      localObject1 = Locale.ENGLISH;
      localObject2 = "sim%d_banks";
      int k = 1;
      localObject3 = new Object[k];
      Object localObject4 = Integer.valueOf(0);
      localObject3[0] = localObject4;
      localObject1 = String.format((Locale)localObject1, (String)localObject2, (Object[])localObject3);
      localObject2 = Locale.ENGLISH;
      localObject3 = "sim%d_banks";
      localObject4 = new Object[k];
      Integer localInteger = Integer.valueOf(k);
      localObject4[0] = localInteger;
      localObject2 = String.format((Locale)localObject2, (String)localObject3, (Object[])localObject4);
      bool1 = paramHashMap.containsKey(localObject1);
      int j;
      if (bool1)
      {
        localObject3 = paramHashMap.get(localObject1);
        localObject3 = (HashSet)localObject3;
        j = ((HashSet)localObject3).size();
        localObject4 = Integer.valueOf(j);
        paramHashMap.put(localObject1, localObject4);
      }
      else
      {
        j = 0;
        localObject3 = null;
      }
      boolean bool2 = paramHashMap.containsKey(localObject2);
      int m;
      if (bool2)
      {
        localObject1 = paramHashMap.get(localObject2);
        localObject1 = (HashSet)localObject1;
        m = ((HashSet)localObject1).size();
        localObject4 = Integer.valueOf(m);
        paramHashMap.put(localObject2, localObject4);
      }
      else
      {
        m = 0;
        localObject1 = null;
      }
      j += m;
      localObject1 = "total_banks";
      localObject2 = Integer.valueOf(j);
      paramHashMap.put(localObject1, localObject2);
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>(paramHashMap);
      paramHashMap = b.a();
      localObject2 = "PaySmsData";
      paramHashMap.c((String)localObject2, (JSONObject)localObject1);
      paramHashMap = "PaySmsDataBankDetails";
      localObject1 = Integer.valueOf(j);
      a(paramHashMap, localObject1);
      return k;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
    return false;
  }
  
  public String b()
  {
    Object localObject = Calendar.getInstance();
    int i = 1;
    int j = ((Calendar)localObject).get(i);
    int k = 2;
    int m = ((Calendar)localObject).get(k);
    Object[] arrayOfObject = new Object[k];
    Integer localInteger = Integer.valueOf(j);
    arrayOfObject[0] = localInteger;
    localObject = Integer.valueOf(m + i);
    arrayOfObject[i] = localObject;
    return String.format("%d%02d", arrayOfObject);
  }
  
  public void b(String paramString)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "ACTION";
    try
    {
      localJSONObject.put(str, paramString);
      paramString = b.a();
      str = "BusBookingHistory";
      paramString.a(str, localJSONObject);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void b(String paramString1, String paramString2)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "CAMPAIGN_ID";
    try
    {
      localJSONObject.put(str, paramString1);
      paramString1 = "TYPE";
      localJSONObject.put(paramString1, paramString2);
      paramString1 = b.a();
      paramString2 = "PayPromoCard";
      paramString1.a(paramString2, localJSONObject);
      paramString1 = b.a();
      paramString2 = "app_payment_promo_card";
      str = null;
      paramString1.a(paramString2, null, localJSONObject, null);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void b(String paramString1, String paramString2, String paramString3)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str;
    if (paramString1 != null) {
      str = "STATUS";
    }
    try
    {
      localJSONObject.put(str, paramString1);
      paramString1 = "CONTEXT";
      localJSONObject.put(paramString1, paramString2);
      if (paramString3 != null)
      {
        paramString1 = "ACTION";
        localJSONObject.put(paramString1, paramString3);
      }
      paramString1 = b.a();
      paramString2 = "PayPopup";
      paramString1.a(paramString2, localJSONObject);
      paramString1 = b.a();
      paramString2 = "app_payment_popup";
      paramString3 = c;
      paramString3 = paramString3.a();
      str = null;
      paramString1.a(paramString2, paramString3, localJSONObject, null);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void b(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    JSONObject localJSONObject1 = new org/json/JSONObject;
    localJSONObject1.<init>();
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    String str = "CONTEXT";
    try
    {
      localJSONObject1.put(str, paramString2);
      paramString2 = "STATUS";
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "BANK";
      localJSONObject1.put(paramString2, paramString4);
      paramString2 = b.a();
      paramString3 = "PayBalanceCheck";
      paramString2.a(paramString3, localJSONObject1);
      paramString2 = "REGISTER_ID";
      paramString3 = b;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = "PHONE_NUMBER";
      paramString3 = a;
      paramString3 = paramString3.a();
      localJSONObject1.put(paramString2, paramString3);
      paramString2 = b.a();
      paramString3 = f;
      paramString3 = paramString3.a();
      paramString2.a(paramString1, paramString3, localJSONObject1, localJSONObject2);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void b(HashMap paramHashMap)
  {
    b.a().a(paramHashMap);
  }
  
  public void c(String paramString)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "VENDOR";
    try
    {
      localJSONObject.put(str, paramString);
      paramString = b.a();
      str = "PayPwaConsentScreen";
      paramString.a(str, localJSONObject);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void c(String paramString1, String paramString2)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "STATUS";
    try
    {
      localJSONObject.put(str, paramString1);
      paramString1 = "BANK";
      localJSONObject.put(paramString1, paramString2);
      paramString1 = b.a();
      paramString2 = "PaySmsDataShown";
      paramString1.a(paramString2, localJSONObject);
      paramString1 = b.a();
      paramString2 = "app_payment_sms_data_shown";
      str = null;
      paramString1.a(paramString2, null, localJSONObject, null);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void c(String paramString1, String paramString2, String paramString3)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "STATUS";
    try
    {
      localJSONObject.put(str, paramString1);
      paramString1 = "CONTEXT";
      localJSONObject.put(paramString1, paramString2);
      paramString1 = "TYPE";
      localJSONObject.put(paramString1, paramString3);
      paramString1 = b.a();
      paramString2 = "BusCancellation";
      paramString1.a(paramString2, localJSONObject);
      paramString1 = "REGISTER_ID";
      paramString2 = b;
      paramString2 = paramString2.a();
      localJSONObject.put(paramString1, paramString2);
      paramString1 = "PHONE_NUMBER";
      paramString2 = a;
      paramString2 = paramString2.a();
      localJSONObject.put(paramString1, paramString2);
      paramString1 = b.a();
      paramString2 = "app_payment_service_cancel";
      paramString3 = null;
      paramString1.a(paramString2, null, localJSONObject, null);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void d(String paramString)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "VENDOR";
    try
    {
      localJSONObject.put(str, paramString);
      paramString = b.a();
      str = "PayExitPwa";
      paramString.a(str, localJSONObject);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void d(String paramString1, String paramString2)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "ACTION";
    try
    {
      localJSONObject.put(str, paramString2);
      paramString2 = b.a();
      paramString2.a(paramString1, localJSONObject);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void e(String paramString)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "CONTEXT";
    try
    {
      localJSONObject.put(str, paramString);
      paramString = b.a();
      str = "pay_again_action";
      paramString.a(str, localJSONObject);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void e(String paramString1, String paramString2)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "VENDOR";
    try
    {
      localJSONObject.put(str, paramString1);
      paramString1 = b.a();
      str = "PayPwaPaymentRequest";
      paramString1.a(str, localJSONObject);
      paramString1 = "BOOKING_ID";
      localJSONObject.put(paramString1, paramString2);
      paramString1 = "REGISTER_ID";
      paramString2 = b;
      paramString2 = paramString2.a();
      localJSONObject.put(paramString1, paramString2);
      paramString1 = "PHONE_NUMBER";
      paramString2 = a;
      paramString2 = paramString2.a();
      localJSONObject.put(paramString1, paramString2);
      paramString1 = b.a();
      paramString2 = "app_payment_pwa_payment_request";
      str = null;
      paramString1.a(paramString2, null, localJSONObject, null);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void f(String paramString)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "CONTEXT";
    try
    {
      localJSONObject.put(str, paramString);
      paramString = b.a();
      str = "utility_repeat_action";
      paramString.a(str, localJSONObject);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  public void f(String paramString1, String paramString2)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "CONTEXT";
    try
    {
      localJSONObject.put(str, paramString2);
      paramString2 = "UTILITY-CATEGORY";
      localJSONObject.put(paramString2, paramString1);
      paramString1 = b.a();
      paramString2 = "app_payment_utility_search_clicked";
      paramString1.c(paramString2, localJSONObject);
      return;
    }
    catch (JSONException localJSONException) {}
  }
  
  public void g(String paramString)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "CONTEXT";
    try
    {
      localJSONObject.put(str, paramString);
      paramString = b.a();
      str = "PayShortcutIcon";
      paramString.a(str, localJSONObject);
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */