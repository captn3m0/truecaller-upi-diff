package com.truecaller.truepay.data.b;

import android.text.TextUtils;
import com.truecaller.truepay.TcPaySDKListener;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class b
{
  public static b a()
  {
    return b.a.a;
  }
  
  private Map a(JSONObject paramJSONObject)
  {
    if (paramJSONObject == null) {
      return null;
    }
    localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i = 1;
    try
    {
      Iterator localIterator = paramJSONObject.keys();
      for (;;)
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        Object localObject = localIterator.next();
        localObject = (String)localObject;
        String str = paramJSONObject.getString((String)localObject);
        boolean bool2 = TextUtils.isEmpty(str);
        if (!bool2) {
          localHashMap.put(localObject, str);
        }
      }
      String[] arrayOfString;
      return localHashMap;
    }
    catch (JSONException paramJSONObject)
    {
      arrayOfString = new String[i];
      paramJSONObject = paramJSONObject.getMessage();
      arrayOfString[0] = paramJSONObject;
    }
    catch (ConcurrentModificationException paramJSONObject)
    {
      arrayOfString = new String[i];
      paramJSONObject = paramJSONObject.getMessage();
      arrayOfString[0] = paramJSONObject;
    }
  }
  
  private TcPaySDKListener b()
  {
    return b.a.b;
  }
  
  private Map b(JSONObject paramJSONObject)
  {
    if (paramJSONObject == null) {
      return null;
    }
    localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i = 1;
    try
    {
      Iterator localIterator = paramJSONObject.keys();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        Object localObject1 = localIterator.next();
        localObject1 = (String)localObject1;
        Object localObject2 = paramJSONObject.get((String)localObject1);
        localHashMap.put(localObject1, localObject2);
      }
      String[] arrayOfString;
      return localHashMap;
    }
    catch (JSONException paramJSONObject)
    {
      arrayOfString = new String[i];
      paramJSONObject = paramJSONObject.getMessage();
      arrayOfString[0] = paramJSONObject;
    }
    catch (ConcurrentModificationException paramJSONObject)
    {
      arrayOfString = new String[i];
      paramJSONObject = paramJSONObject.getMessage();
      arrayOfString[0] = paramJSONObject;
    }
  }
  
  private Map c(JSONObject paramJSONObject)
  {
    if (paramJSONObject == null) {
      return null;
    }
    localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i = 1;
    try
    {
      Iterator localIterator = paramJSONObject.keys();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        Object localObject = localIterator.next();
        localObject = (String)localObject;
        double d = paramJSONObject.getDouble((String)localObject);
        Double localDouble = Double.valueOf(d);
        localHashMap.put(localObject, localDouble);
      }
      String[] arrayOfString;
      return localHashMap;
    }
    catch (JSONException paramJSONObject)
    {
      arrayOfString = new String[i];
      paramJSONObject = paramJSONObject.getMessage();
      arrayOfString[0] = paramJSONObject;
    }
    catch (ConcurrentModificationException paramJSONObject)
    {
      arrayOfString = new String[i];
      paramJSONObject = paramJSONObject.getMessage();
      arrayOfString[0] = paramJSONObject;
    }
  }
  
  public void a(String paramString1, String paramString2, JSONObject paramJSONObject1, JSONObject paramJSONObject2)
  {
    TcPaySDKListener localTcPaySDKListener = b();
    paramJSONObject1 = a(paramJSONObject1);
    paramJSONObject2 = c(paramJSONObject2);
    localTcPaySDKListener.logTcPayEvent(paramString1, paramString2, paramJSONObject1, paramJSONObject2);
  }
  
  public void a(String paramString, JSONObject paramJSONObject)
  {
    b(paramString, paramJSONObject);
    c(paramString, paramJSONObject);
  }
  
  public void a(HashMap paramHashMap)
  {
    b().updateCleverTapProfile(paramHashMap);
  }
  
  public void b(String paramString, JSONObject paramJSONObject)
  {
    TcPaySDKListener localTcPaySDKListener = b();
    paramJSONObject = b(paramJSONObject);
    localTcPaySDKListener.logTcPayCleverTapEvent(paramString, paramJSONObject);
  }
  
  public void c(String paramString, JSONObject paramJSONObject)
  {
    TcPaySDKListener localTcPaySDKListener = b();
    paramJSONObject = a(paramJSONObject);
    localTcPaySDKListener.logTcPayFacebookEvent(paramString, paramJSONObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */