package com.truecaller.truepay.data.d;

import java.io.Serializable;

public final class a
  implements Serializable
{
  String a;
  public String b;
  String c;
  public String d;
  public String e;
  public boolean f = true;
  
  public a() {}
  
  public a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramString4;
    e = paramString5;
    f = paramBoolean;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final void a(String paramString)
  {
    b = paramString;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final void b(String paramString)
  {
    d = paramString;
  }
  
  public final String c()
  {
    return c;
  }
  
  public final String d()
  {
    return d;
  }
  
  public final boolean e()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */