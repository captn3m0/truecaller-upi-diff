package com.truecaller.truepay.data.d;

import java.io.Serializable;

public final class c
  implements Serializable
{
  public final String a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  public final String f;
  public final String g;
  public final String h;
  public final String i;
  private final String j;
  private final String k;
  private final String l;
  private final String m;
  
  private c(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, String paramString13)
  {
    a = paramString1;
    b = paramString2;
    j = paramString3;
    c = paramString4;
    d = paramString5;
    e = paramString6;
    f = paramString7;
    k = paramString8;
    g = paramString9;
    l = paramString10;
    h = paramString11;
    m = paramString12;
    i = paramString13;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */