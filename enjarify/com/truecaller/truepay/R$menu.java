package com.truecaller.truepay;

public final class R$menu
{
  public static final int menu_dashboard = 2131623960;
  public static final int menu_frag_banks = 2131623961;
  public static final int menu_frag_search_banks = 2131623962;
  public static final int menu_history = 2131623963;
  public static final int menu_operator_selection = 2131623966;
  public static final int menu_payments = 2131623967;
  public static final int menu_pending_collect = 2131623968;
  public static final int menu_settings = 2131623969;
  public static final int options_menu_all = 2131623973;
  public static final int options_menu_primary = 2131623974;
  public static final int options_menu_set_expiry = 2131623975;
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.R.menu
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */