package com.truecaller.truepay;

import java.io.Serializable;

public class SmsBankData
  implements Serializable
{
  private final String bankName;
  private final String bankSymbol;
  private final int simSlotIndex;
  private final Integer smsCount;
  
  private SmsBankData(SmsBankData.a parama)
  {
    int i = a;
    simSlotIndex = i;
    String str = b;
    bankName = str;
    str = c;
    bankSymbol = str;
    parama = d;
    smsCount = parama;
  }
  
  public String getBankName()
  {
    return bankName;
  }
  
  public String getBankSymbol()
  {
    return bankSymbol;
  }
  
  public int getSimSlotIndex()
  {
    return simSlotIndex;
  }
  
  public Integer getSmsCount()
  {
    return smsCount;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.SmsBankData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */