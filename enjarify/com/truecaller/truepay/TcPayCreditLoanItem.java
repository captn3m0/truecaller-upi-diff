package com.truecaller.truepay;

import c.g.b.k;

public final class TcPayCreditLoanItem
{
  private final String amount;
  private final String categoryIcon;
  private final String categoryId;
  private final String categoryName;
  private final long disbursedOn;
  private final String emiAmount;
  private final Integer emiCount;
  private final String loanId;
  private final String name;
  private final Long nextEmiDueDate;
  private final Integer remainingEmiCount;
  private final String repaymentLink;
  private final String repaymentMessage;
  private final String status;
  private final String statusText;
  
  public TcPayCreditLoanItem(String paramString1, String paramString2, String paramString3, long paramLong, Integer paramInteger1, Integer paramInteger2, Long paramLong1, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11)
  {
    loanId = paramString1;
    name = paramString2;
    amount = paramString3;
    disbursedOn = paramLong;
    emiCount = paramInteger1;
    remainingEmiCount = paramInteger2;
    nextEmiDueDate = paramLong1;
    emiAmount = paramString4;
    categoryName = paramString5;
    categoryId = paramString6;
    categoryIcon = paramString7;
    status = paramString8;
    statusText = paramString9;
    repaymentLink = paramString10;
    repaymentMessage = paramString11;
  }
  
  public final String component1()
  {
    return loanId;
  }
  
  public final String component10()
  {
    return categoryId;
  }
  
  public final String component11()
  {
    return categoryIcon;
  }
  
  public final String component12()
  {
    return status;
  }
  
  public final String component13()
  {
    return statusText;
  }
  
  public final String component14()
  {
    return repaymentLink;
  }
  
  public final String component15()
  {
    return repaymentMessage;
  }
  
  public final String component2()
  {
    return name;
  }
  
  public final String component3()
  {
    return amount;
  }
  
  public final long component4()
  {
    return disbursedOn;
  }
  
  public final Integer component5()
  {
    return emiCount;
  }
  
  public final Integer component6()
  {
    return remainingEmiCount;
  }
  
  public final Long component7()
  {
    return nextEmiDueDate;
  }
  
  public final String component8()
  {
    return emiAmount;
  }
  
  public final String component9()
  {
    return categoryName;
  }
  
  public final TcPayCreditLoanItem copy(String paramString1, String paramString2, String paramString3, long paramLong, Integer paramInteger1, Integer paramInteger2, Long paramLong1, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11)
  {
    k.b(paramString1, "loanId");
    k.b(paramString2, "name");
    k.b(paramString3, "amount");
    k.b(paramString5, "categoryName");
    k.b(paramString6, "categoryId");
    k.b(paramString7, "categoryIcon");
    k.b(paramString8, "status");
    k.b(paramString9, "statusText");
    TcPayCreditLoanItem localTcPayCreditLoanItem = new com/truecaller/truepay/TcPayCreditLoanItem;
    localTcPayCreditLoanItem.<init>(paramString1, paramString2, paramString3, paramLong, paramInteger1, paramInteger2, paramLong1, paramString4, paramString5, paramString6, paramString7, paramString8, paramString9, paramString10, paramString11);
    return localTcPayCreditLoanItem;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof TcPayCreditLoanItem;
      if (bool2)
      {
        paramObject = (TcPayCreditLoanItem)paramObject;
        Object localObject1 = loanId;
        Object localObject2 = loanId;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = name;
          localObject2 = name;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = amount;
            localObject2 = amount;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              long l1 = disbursedOn;
              long l2 = disbursedOn;
              bool2 = l1 < l2;
              if (!bool2)
              {
                bool2 = true;
              }
              else
              {
                bool2 = false;
                localObject1 = null;
              }
              if (bool2)
              {
                localObject1 = emiCount;
                localObject2 = emiCount;
                bool2 = k.a(localObject1, localObject2);
                if (bool2)
                {
                  localObject1 = remainingEmiCount;
                  localObject2 = remainingEmiCount;
                  bool2 = k.a(localObject1, localObject2);
                  if (bool2)
                  {
                    localObject1 = nextEmiDueDate;
                    localObject2 = nextEmiDueDate;
                    bool2 = k.a(localObject1, localObject2);
                    if (bool2)
                    {
                      localObject1 = emiAmount;
                      localObject2 = emiAmount;
                      bool2 = k.a(localObject1, localObject2);
                      if (bool2)
                      {
                        localObject1 = categoryName;
                        localObject2 = categoryName;
                        bool2 = k.a(localObject1, localObject2);
                        if (bool2)
                        {
                          localObject1 = categoryId;
                          localObject2 = categoryId;
                          bool2 = k.a(localObject1, localObject2);
                          if (bool2)
                          {
                            localObject1 = categoryIcon;
                            localObject2 = categoryIcon;
                            bool2 = k.a(localObject1, localObject2);
                            if (bool2)
                            {
                              localObject1 = status;
                              localObject2 = status;
                              bool2 = k.a(localObject1, localObject2);
                              if (bool2)
                              {
                                localObject1 = statusText;
                                localObject2 = statusText;
                                bool2 = k.a(localObject1, localObject2);
                                if (bool2)
                                {
                                  localObject1 = repaymentLink;
                                  localObject2 = repaymentLink;
                                  bool2 = k.a(localObject1, localObject2);
                                  if (bool2)
                                  {
                                    localObject1 = repaymentMessage;
                                    paramObject = repaymentMessage;
                                    boolean bool3 = k.a(localObject1, paramObject);
                                    if (bool3) {
                                      return bool1;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getAmount()
  {
    return amount;
  }
  
  public final String getCategoryIcon()
  {
    return categoryIcon;
  }
  
  public final String getCategoryId()
  {
    return categoryId;
  }
  
  public final String getCategoryName()
  {
    return categoryName;
  }
  
  public final long getDisbursedOn()
  {
    return disbursedOn;
  }
  
  public final String getEmiAmount()
  {
    return emiAmount;
  }
  
  public final Integer getEmiCount()
  {
    return emiCount;
  }
  
  public final String getLoanId()
  {
    return loanId;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final Long getNextEmiDueDate()
  {
    return nextEmiDueDate;
  }
  
  public final Integer getRemainingEmiCount()
  {
    return remainingEmiCount;
  }
  
  public final String getRepaymentLink()
  {
    return repaymentLink;
  }
  
  public final String getRepaymentMessage()
  {
    return repaymentMessage;
  }
  
  public final String getStatus()
  {
    return status;
  }
  
  public final String getStatusText()
  {
    return statusText;
  }
  
  public final int hashCode()
  {
    String str = loanId;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = name;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = amount;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    long l1 = disbursedOn;
    int m = 32;
    long l2 = l1 >>> m;
    l1 ^= l2;
    int n = (int)l1;
    j = (j + n) * 31;
    localObject = emiCount;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = remainingEmiCount;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = nextEmiDueDate;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = emiAmount;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = categoryName;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = categoryId;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = categoryIcon;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = status;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = statusText;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = repaymentLink;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = repaymentMessage;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("TcPayCreditLoanItem(loanId=");
    Object localObject = loanId;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", name=");
    localObject = name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", amount=");
    localObject = amount;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", disbursedOn=");
    long l = disbursedOn;
    localStringBuilder.append(l);
    localStringBuilder.append(", emiCount=");
    localObject = emiCount;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", remainingEmiCount=");
    localObject = remainingEmiCount;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", nextEmiDueDate=");
    localObject = nextEmiDueDate;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", emiAmount=");
    localObject = emiAmount;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", categoryName=");
    localObject = categoryName;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", categoryId=");
    localObject = categoryId;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", categoryIcon=");
    localObject = categoryIcon;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", status=");
    localObject = status;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", statusText=");
    localObject = statusText;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", repaymentLink=");
    localObject = repaymentLink;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", repaymentMessage=");
    localObject = repaymentMessage;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.TcPayCreditLoanItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */