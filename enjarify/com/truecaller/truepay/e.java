package com.truecaller.truepay;

import com.truecaller.common.h.am;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.data.f.ag;
import io.reactivex.g;
import io.reactivex.n;
import io.reactivex.o;
import java.util.HashMap;

public final class e
  implements d
{
  HashMap a;
  ag b;
  private ax c;
  
  public e(ag paramag, ax paramax)
  {
    b = paramag;
    c = paramax;
    paramag = c.a();
    paramax = io.reactivex.g.a.b();
    paramag = paramag.b(paramax);
    paramax = new com/truecaller/truepay/-$$Lambda$e$iafG-XFz_g0xU2gOc8aM1kRvYK4;
    paramax.<init>(this);
    paramag = paramag.a(paramax);
    paramax = io.reactivex.android.b.a.a();
    paramag = paramag.a(paramax);
    paramax = new com/truecaller/truepay/e$1;
    paramax.<init>(this);
    paramag.a(paramax);
  }
  
  public final SenderInfo a(String paramString)
  {
    boolean bool = am.a(paramString);
    if (!bool) {
      return null;
    }
    Object localObject = a;
    if (localObject == null)
    {
      localObject = c;
      String str = ((ax)localObject).b();
      localObject = ((ax)localObject).c(str);
      a = ((HashMap)localObject);
    }
    return (SenderInfo)((HashMap)localObject).get(paramString);
  }
  
  public final SmsBankData a()
  {
    Object localObject1 = b.b();
    String str1 = (String)((HashMap)localObject1).get("bank_symbol");
    String str2 = (String)((HashMap)localObject1).get("bank_name");
    Object localObject2 = "sim_slot";
    localObject1 = (Integer)((HashMap)localObject1).get(localObject2);
    if ((str2 != null) && (str1 != null) && (localObject1 != null))
    {
      localObject2 = new com/truecaller/truepay/SmsBankData$a;
      ((SmsBankData.a)localObject2).<init>();
      b = str2;
      c = str1;
      int i = ((Integer)localObject1).intValue();
      a = i;
      return ((SmsBankData.a)localObject2).a();
    }
    return null;
  }
  
  public final void a(String paramString, int paramInt)
  {
    paramString = a(paramString);
    if (paramString != null)
    {
      Object localObject1 = "bank";
      String str = paramString.getCategory();
      boolean bool = ((String)localObject1).equalsIgnoreCase(str);
      if (bool)
      {
        localObject1 = new com/truecaller/truepay/SmsBankData$a;
        ((SmsBankData.a)localObject1).<init>();
        str = paramString.getName();
        b = str;
        a = paramInt;
        Object localObject2 = paramString.getSymbol();
        c = ((String)localObject2);
        paramInt = 1;
        localObject2 = Integer.valueOf(paramInt);
        d = ((Integer)localObject2);
        localObject2 = ((SmsBankData.a)localObject1).a();
        localObject1 = b;
        paramString = paramString.getName();
        paramString = ((ag)localObject1).a(paramString);
        localObject1 = io.reactivex.g.a.b();
        paramString = paramString.b((n)localObject1);
        localObject1 = new com/truecaller/truepay/e$2;
        ((e.2)localObject1).<init>(this, (SmsBankData)localObject2);
        paramString.a((g)localObject1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */