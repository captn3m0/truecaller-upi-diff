package com.truecaller.truepay;

public class SenderInfo
{
  private String category;
  private String icon;
  private String name;
  private String symbol;
  
  public String getCategory()
  {
    return category;
  }
  
  public String getIcon()
  {
    return icon;
  }
  
  public String getName()
  {
    return name;
  }
  
  public String getSymbol()
  {
    return symbol;
  }
  
  public String getUrl()
  {
    return icon;
  }
  
  public void setCategory(String paramString)
  {
    category = paramString;
  }
  
  public void setIcon(String paramString)
  {
    icon = paramString;
  }
  
  public void setName(String paramString)
  {
    name = paramString;
  }
  
  public void setSymbol(String paramString)
  {
    symbol = paramString;
  }
  
  public void setUrl(String paramString)
  {
    icon = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.SenderInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */