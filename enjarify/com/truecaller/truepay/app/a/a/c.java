package com.truecaller.truepay.app.a.a;

import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.a.a.f.q;
import com.truecaller.truepay.a.a.f.w;
import com.truecaller.truepay.app.a.b.cp;
import com.truecaller.truepay.app.a.b.cq;
import com.truecaller.truepay.app.a.b.cr;
import com.truecaller.truepay.app.a.b.cs;
import com.truecaller.truepay.app.ui.transaction.c.z;
import com.truecaller.truepay.app.ui.transaction.views.activities.SearchTransactionActivity;
import com.truecaller.truepay.data.f.h;
import dagger.a.g;
import javax.inject.Provider;

public final class c
  implements d
{
  private final a a;
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  
  private c(cp paramcp, a parama)
  {
    a = parama;
    Object localObject1 = new com/truecaller/truepay/app/a/a/c$b;
    ((c.b)localObject1).<init>(parama);
    b = ((Provider)localObject1);
    localObject1 = w.a(b);
    c = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/a/a/c$c;
    ((c.c)localObject1).<init>(parama);
    d = ((Provider)localObject1);
    localObject1 = d;
    localObject1 = dagger.a.c.a(cq.a(paramcp, (Provider)localObject1));
    e = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/a/a/c$g;
    ((c.g)localObject1).<init>(parama);
    f = ((Provider)localObject1);
    localObject1 = f;
    Object localObject2 = com.truecaller.truepay.data.c.d.a();
    localObject1 = dagger.a.c.a(cr.a(paramcp, (Provider)localObject1, (Provider)localObject2));
    g = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/a/a/c$d;
    ((c.d)localObject1).<init>(parama);
    h = ((Provider)localObject1);
    localObject1 = e;
    localObject2 = g;
    Provider localProvider = h;
    localObject1 = h.a((Provider)localObject1, (Provider)localObject2, localProvider);
    i = ((Provider)localObject1);
    localObject1 = com.truecaller.truepay.a.a.f.u.a(i);
    j = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/a/a/c$f;
    ((c.f)localObject1).<init>(parama);
    k = ((Provider)localObject1);
    localObject1 = q.a(k);
    l = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/a/a/c$e;
    ((c.e)localObject1).<init>(parama);
    m = ((Provider)localObject1);
    parama = c;
    localObject1 = j;
    localObject2 = l;
    localProvider = m;
    paramcp = dagger.a.c.a(cs.a(paramcp, parama, (Provider)localObject1, (Provider)localObject2, localProvider));
    n = paramcp;
  }
  
  public final void a(SearchTransactionActivity paramSearchTransactionActivity)
  {
    Object localObject = (e)g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    featuresRegistry = ((e)localObject);
    localObject = (com.truecaller.truepay.app.utils.u)g.a(a.x(), "Cannot return null from a non-@Nullable component method");
    instantRewardHandler = ((com.truecaller.truepay.app.utils.u)localObject);
    localObject = (z)n.get();
    d = ((z)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */