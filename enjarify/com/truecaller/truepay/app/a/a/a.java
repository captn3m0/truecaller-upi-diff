package com.truecaller.truepay.app.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.truecaller.truepay.CleverTapAppsTagTask;
import com.truecaller.truepay.SmsDataAnalyticsTask;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.services.TransactionsSyncTask;
import com.truecaller.truepay.app.utils.aj;
import com.truecaller.truepay.app.utils.al;
import com.truecaller.truepay.app.utils.ao;
import com.truecaller.truepay.app.utils.at;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.app.utils.u;
import com.truecaller.truepay.data.api.model.ResolveRequestUser;
import com.truecaller.truepay.data.f.ac;
import com.truecaller.utils.i;
import com.truecaller.utils.n;
import e.s;

public abstract interface a
{
  public abstract com.truecaller.truepay.data.e.b A();
  
  public abstract com.truecaller.truepay.data.e.e B();
  
  public abstract com.truecaller.truepay.data.e.a C();
  
  public abstract com.truecaller.truepay.data.e.a D();
  
  public abstract com.truecaller.truepay.data.e.a E();
  
  public abstract com.truecaller.truepay.data.e.e F();
  
  public abstract com.truecaller.truepay.data.e.e G();
  
  public abstract com.truecaller.truepay.data.e.e H();
  
  public abstract com.truecaller.truepay.data.e.e I();
  
  public abstract com.truecaller.truepay.data.e.e J();
  
  public abstract com.truecaller.truepay.data.e.e K();
  
  public abstract com.truecaller.truepay.data.e.e L();
  
  public abstract com.truecaller.truepay.data.e.e M();
  
  public abstract com.truecaller.truepay.data.e.e N();
  
  public abstract com.truecaller.truepay.data.e.e O();
  
  public abstract com.truecaller.truepay.data.e.e P();
  
  public abstract com.truecaller.truepay.data.e.e Q();
  
  public abstract com.truecaller.truepay.data.e.a R();
  
  public abstract com.truecaller.truepay.data.e.a S();
  
  public abstract com.truecaller.truepay.data.e.a T();
  
  public abstract com.truecaller.truepay.data.e.e U();
  
  public abstract com.truecaller.truepay.data.e.a V();
  
  public abstract com.truecaller.truepay.data.e.e W();
  
  public abstract com.truecaller.truepay.data.e.b X();
  
  public abstract c.d.f Y();
  
  public abstract c.d.f Z();
  
  public abstract Context a();
  
  public abstract void a(CleverTapAppsTagTask paramCleverTapAppsTagTask);
  
  public abstract void a(SmsDataAnalyticsTask paramSmsDataAnalyticsTask);
  
  public abstract void a(Truepay paramTruepay);
  
  public abstract void a(com.truecaller.truepay.app.ui.registration.e.b paramb);
  
  public abstract void a(TransactionsSyncTask paramTransactionsSyncTask);
  
  public abstract SharedPreferences aa();
  
  public abstract com.truecaller.truepay.data.f.q ab();
  
  public abstract com.truecaller.truepay.data.f.d ac();
  
  public abstract com.truecaller.truepay.data.b.a ad();
  
  public abstract at ae();
  
  public abstract n af();
  
  public abstract com.truecaller.truepay.a.a.c.e ag();
  
  public abstract com.truecaller.truepay.a.a.c.c ah();
  
  public abstract i ai();
  
  public abstract ResolveRequestUser aj();
  
  public abstract com.truecaller.common.g.a ak();
  
  public abstract ac al();
  
  public abstract com.google.gson.f am();
  
  public abstract s an();
  
  public abstract s ao();
  
  public abstract s ap();
  
  public abstract s aq();
  
  public abstract com.truecaller.utils.a ar();
  
  public abstract com.truecaller.truepay.app.ui.npci.e as();
  
  public abstract com.truecaller.truepay.app.ui.a.a at();
  
  public abstract com.truecaller.truepay.data.db.a au();
  
  public abstract com.truecaller.truepay.app.ui.registrationv2.data.a.a av();
  
  public abstract r b();
  
  public abstract com.truecaller.truepay.app.utils.g c();
  
  public abstract com.truecaller.truepay.app.utils.l d();
  
  public abstract ax e();
  
  public abstract com.truecaller.truepay.app.utils.q f();
  
  public abstract com.truecaller.truepay.f g();
  
  public abstract com.truecaller.truepay.app.utils.a h();
  
  public abstract aj i();
  
  public abstract ao j();
  
  public abstract au k();
  
  public abstract com.truecaller.truepay.data.api.f l();
  
  public abstract com.truecaller.truepay.data.api.h m();
  
  public abstract com.truecaller.truepay.data.api.a n();
  
  public abstract com.truecaller.truepay.data.api.d o();
  
  public abstract com.truecaller.truepay.data.api.g p();
  
  public abstract com.truecaller.truepay.data.api.e q();
  
  public abstract com.truecaller.truepay.app.utils.e r();
  
  public abstract com.truecaller.truepay.data.api.c s();
  
  public abstract com.truecaller.truepay.app.ui.growth.db.b t();
  
  public abstract com.truecaller.featuretoggles.e u();
  
  public abstract al v();
  
  public abstract com.truecaller.utils.l w();
  
  public abstract u x();
  
  public abstract com.truecaller.multisim.h y();
  
  public abstract com.truecaller.truepay.data.e.a z();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */