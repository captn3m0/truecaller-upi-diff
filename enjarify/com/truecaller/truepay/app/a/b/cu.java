package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cu
  implements d
{
  private final ct a;
  private final Provider b;
  
  private cu(ct paramct, Provider paramProvider)
  {
    a = paramct;
    b = paramProvider;
  }
  
  public static cu a(ct paramct, Provider paramProvider)
  {
    cu localcu = new com/truecaller/truepay/app/a/b/cu;
    localcu.<init>(paramct, paramProvider);
    return localcu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cu
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */