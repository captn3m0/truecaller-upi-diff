package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cn
  implements d
{
  private final br a;
  private final Provider b;
  
  private cn(br parambr, Provider paramProvider)
  {
    a = parambr;
    b = paramProvider;
  }
  
  public static cn a(br parambr, Provider paramProvider)
  {
    cn localcn = new com/truecaller/truepay/app/a/b/cn;
    localcn.<init>(parambr, paramProvider);
    return localcn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cn
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */