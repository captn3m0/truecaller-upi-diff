package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d
{
  private final c a;
  private final Provider b;
  
  private v(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static v a(c paramc, Provider paramProvider)
  {
    v localv = new com/truecaller/truepay/app/a/b/v;
    localv.<init>(paramc, paramProvider);
    return localv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */