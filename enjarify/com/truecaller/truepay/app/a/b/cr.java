package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cr
  implements d
{
  private final cp a;
  private final Provider b;
  private final Provider c;
  
  private cr(cp paramcp, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramcp;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static cr a(cp paramcp, Provider paramProvider1, Provider paramProvider2)
  {
    cr localcr = new com/truecaller/truepay/app/a/b/cr;
    localcr.<init>(paramcp, paramProvider1, paramProvider2);
    return localcr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cr
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */