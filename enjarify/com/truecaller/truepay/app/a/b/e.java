package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private e(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static e a(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    e locale = new com/truecaller/truepay/app/a/b/e;
    locale.<init>(paramc, paramProvider1, paramProvider2, paramProvider3);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */