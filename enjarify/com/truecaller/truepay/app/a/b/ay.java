package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ay
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ay(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ay a(ab paramab, Provider paramProvider)
  {
    ay localay = new com/truecaller/truepay/app/a/b/ay;
    localay.<init>(paramab, paramProvider);
    return localay;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ay
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */