package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class aj
  implements d
{
  private final ab a;
  private final Provider b;
  
  private aj(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static aj a(ab paramab, Provider paramProvider)
  {
    aj localaj = new com/truecaller/truepay/app/a/b/aj;
    localaj.<init>(paramab, paramProvider);
    return localaj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */