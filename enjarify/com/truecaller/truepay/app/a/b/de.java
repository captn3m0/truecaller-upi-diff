package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class de
  implements d
{
  private final dc a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private de(dc paramdc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramdc;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static de a(dc paramdc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    de localde = new com/truecaller/truepay/app/a/b/de;
    localde.<init>(paramdc, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localde;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.de
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */