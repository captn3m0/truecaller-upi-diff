package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ap
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ap(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ap a(ab paramab, Provider paramProvider)
  {
    ap localap = new com/truecaller/truepay/app/a/b/ap;
    localap.<init>(paramab, paramProvider);
    return localap;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */