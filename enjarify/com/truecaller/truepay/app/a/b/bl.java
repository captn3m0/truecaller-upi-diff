package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bl
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bl(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bl a(ab paramab, Provider paramProvider)
  {
    bl localbl = new com/truecaller/truepay/app/a/b/bl;
    localbl.<init>(paramab, paramProvider);
    return localbl;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */