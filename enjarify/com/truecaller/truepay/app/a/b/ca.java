package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ca
  implements d
{
  private final br a;
  private final Provider b;
  
  private ca(br parambr, Provider paramProvider)
  {
    a = parambr;
    b = paramProvider;
  }
  
  public static ca a(br parambr, Provider paramProvider)
  {
    ca localca = new com/truecaller/truepay/app/a/b/ca;
    localca.<init>(parambr, paramProvider);
    return localca;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ca
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */