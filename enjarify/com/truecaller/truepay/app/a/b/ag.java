package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ag
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ag(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ag a(ab paramab, Provider paramProvider)
  {
    ag localag = new com/truecaller/truepay/app/a/b/ag;
    localag.<init>(paramab, paramProvider);
    return localag;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */