package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bd
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bd(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bd a(ab paramab, Provider paramProvider)
  {
    bd localbd = new com/truecaller/truepay/app/a/b/bd;
    localbd.<init>(paramab, paramProvider);
    return localbd;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bd
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */