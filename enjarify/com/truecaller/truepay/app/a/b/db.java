package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class db
  implements d
{
  private final cx a;
  private final Provider b;
  private final Provider c;
  
  private db(cx paramcx, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramcx;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static db a(cx paramcx, Provider paramProvider1, Provider paramProvider2)
  {
    db localdb = new com/truecaller/truepay/app/a/b/db;
    localdb.<init>(paramcx, paramProvider1, paramProvider2);
    return localdb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.db
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */