package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ah(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ah a(ab paramab, Provider paramProvider)
  {
    ah localah = new com/truecaller/truepay/app/a/b/ah;
    localah.<init>(paramab, paramProvider);
    return localah;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */