package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ch
  implements d
{
  private final br a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  
  private ch(br parambr, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = parambr;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static ch a(br parambr, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    ch localch = new com/truecaller/truepay/app/a/b/ch;
    localch.<init>(parambr, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localch;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ch
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */