package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bk
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bk(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bk a(ab paramab, Provider paramProvider)
  {
    bk localbk = new com/truecaller/truepay/app/a/b/bk;
    localbk.<init>(paramab, paramProvider);
    return localbk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bk
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */