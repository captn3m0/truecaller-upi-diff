package com.truecaller.truepay.app.a.b;

import android.app.Application;
import android.arch.persistence.room.f.a;
import android.content.ContentResolver;
import android.content.Context;
import com.truecaller.truepay.app.ui.growth.db.TcPayGrowthDb;
import com.truecaller.truepay.app.utils.aj;
import com.truecaller.truepay.app.utils.ak;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.app.utils.n;
import com.truecaller.truepay.app.utils.o;
import com.truecaller.truepay.data.api.j;
import com.truecaller.truepay.data.api.model.ResolveRequestUser;
import com.truecaller.truepay.data.db.PayRoomDatabase;
import com.truecaller.truepay.data.db.PayRoomDatabase.a;
import com.truecaller.truepay.data.f.ac;
import com.truecaller.truepay.data.f.q;
import com.truecaller.truepay.data.f.r;
import com.truecaller.truepay.data.g.b.k;
import com.truecaller.truepay.g;

public class c
{
  Application a;
  
  public c(Application paramApplication)
  {
    a = paramApplication;
  }
  
  static ContentResolver a(Context paramContext)
  {
    return paramContext.getContentResolver();
  }
  
  static com.google.firebase.remoteconfig.a a()
  {
    return com.google.firebase.remoteconfig.a.a();
  }
  
  static com.truecaller.common.background.b a(Application paramApplication)
  {
    return c;
  }
  
  static com.truecaller.truepay.a a(Context paramContext, ax paramax, com.truecaller.truepay.data.b.a parama)
  {
    com.truecaller.truepay.b localb = new com/truecaller/truepay/b;
    localb.<init>(paramContext, paramax, parama);
    return localb;
  }
  
  static com.truecaller.truepay.app.ui.growth.db.b a(TcPayGrowthDb paramTcPayGrowthDb)
  {
    return paramTcPayGrowthDb.h();
  }
  
  static com.truecaller.truepay.app.ui.homescreen.core.a a(com.google.firebase.remoteconfig.a parama)
  {
    com.truecaller.truepay.app.ui.homescreen.core.b localb = new com/truecaller/truepay/app/ui/homescreen/core/b;
    localb.<init>(parama);
    return localb;
  }
  
  static aj a(com.truecaller.truepay.data.e.e parame)
  {
    ak localak = new com/truecaller/truepay/app/utils/ak;
    localak.<init>(parame);
    return localak;
  }
  
  static com.truecaller.truepay.app.utils.e a(com.truecaller.truepay.data.e.e parame, ax paramax)
  {
    com.truecaller.truepay.app.utils.f localf = new com/truecaller/truepay/app/utils/f;
    localf.<init>(parame, paramax);
    return localf;
  }
  
  static ResolveRequestUser a(com.truecaller.truepay.data.e.e parame1, com.truecaller.truepay.data.e.e parame2, com.truecaller.truepay.data.e.e parame3)
  {
    ResolveRequestUser localResolveRequestUser = new com/truecaller/truepay/data/api/model/ResolveRequestUser;
    parame3 = parame3.a();
    parame2 = parame2.a();
    parame1 = parame1.a();
    localResolveRequestUser.<init>(parame3, parame2, parame1);
    return localResolveRequestUser;
  }
  
  static PayRoomDatabase a(Context paramContext, PayRoomDatabase.a parama)
  {
    paramContext = android.arch.persistence.room.e.a(paramContext, PayRoomDatabase.class, "tc_pay_db");
    int i = 1;
    Object localObject = new android.arch.persistence.room.a.a[i];
    android.arch.persistence.room.a.a locala = com.truecaller.truepay.data.db.e.a();
    localObject[0] = locala;
    paramContext = paramContext.a((android.arch.persistence.room.a.a[])localObject);
    localObject = new android.arch.persistence.room.a.a[i];
    locala = com.truecaller.truepay.data.db.e.b();
    localObject[0] = locala;
    paramContext = paramContext.a((android.arch.persistence.room.a.a[])localObject);
    android.arch.persistence.room.a.a[] arrayOfa = new android.arch.persistence.room.a.a[i];
    localObject = com.truecaller.truepay.data.db.e.c();
    arrayOfa[0] = localObject;
    return (PayRoomDatabase)paramContext.a(arrayOfa).a(parama).b();
  }
  
  static com.truecaller.truepay.data.db.a a(PayRoomDatabase paramPayRoomDatabase)
  {
    return paramPayRoomDatabase.h();
  }
  
  static ac a(j paramj)
  {
    k localk = new com/truecaller/truepay/data/g/b/k;
    localk.<init>(paramj);
    return localk;
  }
  
  static q a(ContentResolver paramContentResolver)
  {
    r localr = new com/truecaller/truepay/data/f/r;
    localr.<init>(paramContentResolver);
    return localr;
  }
  
  static com.truecaller.truepay.app.ui.registrationv2.data.a.a b(PayRoomDatabase paramPayRoomDatabase)
  {
    return paramPayRoomDatabase.i();
  }
  
  static n b(Context paramContext)
  {
    o localo = new com/truecaller/truepay/app/utils/o;
    localo.<init>(paramContext);
    return localo;
  }
  
  static com.truecaller.truepay.f c(Context paramContext)
  {
    g localg = new com/truecaller/truepay/g;
    localg.<init>(paramContext);
    return localg;
  }
  
  static TcPayGrowthDb d(Context paramContext)
  {
    paramContext = android.arch.persistence.room.e.a(paramContext, TcPayGrowthDb.class, "tc_pay_growth.db");
    b = false;
    return (TcPayGrowthDb)paramContext.b();
  }
  
  static com.truecaller.featuretoggles.e e(Context paramContext)
  {
    return ((com.truecaller.common.b.a)paramContext.getApplicationContext()).f();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */