package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class dm
  implements d
{
  private final dj a;
  private final Provider b;
  
  private dm(dj paramdj, Provider paramProvider)
  {
    a = paramdj;
    b = paramProvider;
  }
  
  public static dm a(dj paramdj, Provider paramProvider)
  {
    dm localdm = new com/truecaller/truepay/app/a/b/dm;
    localdm.<init>(paramdj, paramProvider);
    return localdm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.dm
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */