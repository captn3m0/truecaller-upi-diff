package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final c a;
  private final Provider b;
  
  private q(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static q a(c paramc, Provider paramProvider)
  {
    q localq = new com/truecaller/truepay/app/a/b/q;
    localq.<init>(paramc, paramProvider);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */