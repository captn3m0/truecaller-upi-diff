package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final c a;
  private final Provider b;
  
  private w(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static w a(c paramc, Provider paramProvider)
  {
    w localw = new com/truecaller/truepay/app/a/b/w;
    localw.<init>(paramc, paramProvider);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */