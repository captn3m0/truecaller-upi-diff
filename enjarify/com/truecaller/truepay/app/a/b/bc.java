package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bc
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bc(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bc a(ab paramab, Provider paramProvider)
  {
    bc localbc = new com/truecaller/truepay/app/a/b/bc;
    localbc.<init>(paramab, paramProvider);
    return localbc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */