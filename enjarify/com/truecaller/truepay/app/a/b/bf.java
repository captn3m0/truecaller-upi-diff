package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bf
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bf(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bf a(ab paramab, Provider paramProvider)
  {
    bf localbf = new com/truecaller/truepay/app/a/b/bf;
    localbf.<init>(paramab, paramProvider);
    return localbf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bf
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */