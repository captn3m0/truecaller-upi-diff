package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cy
  implements d
{
  private final cx a;
  private final Provider b;
  
  private cy(cx paramcx, Provider paramProvider)
  {
    a = paramcx;
    b = paramProvider;
  }
  
  public static cy a(cx paramcx, Provider paramProvider)
  {
    cy localcy = new com/truecaller/truepay/app/a/b/cy;
    localcy.<init>(paramcx, paramProvider);
    return localcy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cy
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */