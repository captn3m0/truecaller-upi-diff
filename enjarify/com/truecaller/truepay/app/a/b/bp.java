package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bp
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bp(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bp a(ab paramab, Provider paramProvider)
  {
    bp localbp = new com/truecaller/truepay/app/a/b/bp;
    localbp.<init>(paramab, paramProvider);
    return localbp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bp
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */