package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ak
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ak(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ak a(ab paramab, Provider paramProvider)
  {
    ak localak = new com/truecaller/truepay/app/a/b/ak;
    localak.<init>(paramab, paramProvider);
    return localak;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */