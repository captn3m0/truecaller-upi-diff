package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final c a;
  private final Provider b;
  
  private o(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static o a(c paramc, Provider paramProvider)
  {
    o localo = new com/truecaller/truepay/app/a/b/o;
    localo.<init>(paramc, paramProvider);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */