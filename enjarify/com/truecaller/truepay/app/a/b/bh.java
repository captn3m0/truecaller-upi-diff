package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bh
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bh(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bh a(ab paramab, Provider paramProvider)
  {
    bh localbh = new com/truecaller/truepay/app/a/b/bh;
    localbh.<init>(paramab, paramProvider);
    return localbh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bh
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */