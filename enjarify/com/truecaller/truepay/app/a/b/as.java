package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class as
  implements d
{
  private final ab a;
  private final Provider b;
  
  private as(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static as a(ab paramab, Provider paramProvider)
  {
    as localas = new com/truecaller/truepay/app/a/b/as;
    localas.<init>(paramab, paramProvider);
    return localas;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.as
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */