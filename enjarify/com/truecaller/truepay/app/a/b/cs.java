package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cs
  implements d
{
  private final cp a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private cs(cp paramcp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramcp;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static cs a(cp paramcp, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    cs localcs = new com/truecaller/truepay/app/a/b/cs;
    localcs.<init>(paramcp, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localcs;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cs
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */