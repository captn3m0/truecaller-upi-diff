package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class az
  implements d
{
  private final ab a;
  private final Provider b;
  
  private az(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static az a(ab paramab, Provider paramProvider)
  {
    az localaz = new com/truecaller/truepay/app/a/b/az;
    localaz.<init>(paramab, paramProvider);
    return localaz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.az
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */