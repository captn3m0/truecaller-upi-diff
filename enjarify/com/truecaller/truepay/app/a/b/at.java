package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class at
  implements d
{
  private final ab a;
  private final Provider b;
  
  private at(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static at a(ab paramab, Provider paramProvider)
  {
    at localat = new com/truecaller/truepay/app/a/b/at;
    localat.<init>(paramab, paramProvider);
    return localat;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.at
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */