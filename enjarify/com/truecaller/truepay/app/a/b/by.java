package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class by
  implements d
{
  private final br a;
  private final Provider b;
  
  private by(br parambr, Provider paramProvider)
  {
    a = parambr;
    b = paramProvider;
  }
  
  public static by a(br parambr, Provider paramProvider)
  {
    by localby = new com/truecaller/truepay/app/a/b/by;
    localby.<init>(parambr, paramProvider);
    return localby;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.by
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */