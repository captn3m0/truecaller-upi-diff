package com.truecaller.truepay.app.a.b;

import android.os.Build.VERSION;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.data.api.a.a.c;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import javax.security.auth.x500.X500Principal;
import okhttp3.a.a;
import okhttp3.ag;
import okhttp3.k.a;
import okhttp3.y.a;
import org.apache.http.conn.ssl.AbstractVerifier;

public final class br
{
  private boolean a(String paramString, SSLSession paramSSLSession)
  {
    try
    {
      paramSSLSession = paramSSLSession.getPeerCertificates();
      paramSSLSession = paramSSLSession[0];
      paramSSLSession = (X509Certificate)paramSSLSession;
      paramString = paramString.trim();
      Object localObject1 = Locale.ENGLISH;
      paramString = paramString.toLowerCase((Locale)localObject1);
      localObject1 = paramSSLSession.getSubjectX500Principal();
      localObject1 = ((X500Principal)localObject1).toString();
      String str1 = ",";
      localObject1 = ((String)localObject1).split(str1);
      int i = localObject1.length;
      int j = 0;
      Object localObject2;
      String str2;
      while (j < i)
      {
        localObject2 = localObject1[j];
        str2 = "CN=";
        int m = ((String)localObject2).indexOf(str2);
        if (m >= 0)
        {
          m += 3;
          localObject1 = ((String)localObject2).substring(m);
          break label121;
        }
        j += 1;
      }
      boolean bool1 = false;
      localObject1 = null;
      label121:
      bool1 = Pattern.matches(paramString, (CharSequence)localObject1);
      i = 1;
      if (bool1) {
        return i;
      }
      paramSSLSession = AbstractVerifier.getDNSSubjectAlts(paramSSLSession);
      int n = paramSSLSession.length;
      int k = 0;
      while (k < n)
      {
        localObject2 = paramSSLSession[k];
        str2 = ".*%s$";
        Object[] arrayOfObject = new Object[i];
        int i1 = ((String)localObject2).length() + -8;
        localObject2 = ((String)localObject2).substring(i1);
        arrayOfObject[0] = localObject2;
        localObject2 = String.format(str2, arrayOfObject);
        localObject2 = Pattern.compile((String)localObject2);
        localObject2 = ((Pattern)localObject2).matcher(paramString);
        boolean bool2 = ((Matcher)localObject2).find();
        if (bool2) {
          return i;
        }
        k += 1;
      }
      return false;
    }
    catch (SSLException localSSLException) {}
    return false;
  }
  
  final y.a a(a parama, SSLSocketFactory paramSSLSocketFactory, X509TrustManager paramX509TrustManager, e parame, c paramc)
  {
    y.a locala = new okhttp3/y$a;
    locala.<init>();
    locala.b(parama);
    parama = new com/truecaller/truepay/app/a/b/-$$Lambda$br$9WK3PAyXZy6-GjKka_83ab9zNfs;
    parama.<init>(this);
    locala.a(parama);
    parama = TimeUnit.MINUTES;
    long l = 1L;
    locala.a(l, parama);
    parama = TimeUnit.MINUTES;
    locala.a(parama);
    parama = TimeUnit.MINUTES;
    locala.b(l, parama);
    int i = Build.VERSION.SDK_INT;
    int j = 16;
    if (i >= j)
    {
      i = Build.VERSION.SDK_INT;
      j = 22;
      if (i < j) {
        try
        {
          parama = new com/truecaller/utils/k;
          parama.<init>(paramSSLSocketFactory);
          locala.a(parama, paramX509TrustManager);
          parama = new okhttp3/k$a;
          paramSSLSocketFactory = okhttp3.k.b;
          parama.<init>(paramSSLSocketFactory);
          int k = 1;
          paramSSLSocketFactory = new ag[k];
          paramX509TrustManager = null;
          ag localag = ag.b;
          paramSSLSocketFactory[0] = localag;
          parama = parama.a(paramSSLSocketFactory);
          parama = parama.b();
          paramSSLSocketFactory = new java/util/ArrayList;
          paramSSLSocketFactory.<init>();
          paramSSLSocketFactory.add(parama);
          parama = okhttp3.k.c;
          paramSSLSocketFactory.add(parama);
          parama = okhttp3.k.d;
          paramSSLSocketFactory.add(parama);
          locala.b(paramSSLSocketFactory);
        }
        catch (Exception localException)
        {
          parama = "OkHttpTLSCompat";
          paramSSLSocketFactory = "Error while setting TLS 1.2";
          { parama }[1] = paramSSLSocketFactory;
        }
      }
    }
    locala.a(paramSSLSocketFactory, paramX509TrustManager);
    parama = parame.ae();
    boolean bool = parama.a();
    if (bool) {
      locala.a(paramc);
    }
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.br
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */