package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bo
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bo(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bo a(ab paramab, Provider paramProvider)
  {
    bo localbo = new com/truecaller/truepay/app/a/b/bo;
    localbo.<init>(paramab, paramProvider);
    return localbo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */