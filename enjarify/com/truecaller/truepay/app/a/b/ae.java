package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ae
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ae(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ae a(ab paramab, Provider paramProvider)
  {
    ae localae = new com/truecaller/truepay/app/a/b/ae;
    localae.<init>(paramab, paramProvider);
    return localae;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */