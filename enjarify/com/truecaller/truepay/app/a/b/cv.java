package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cv
  implements d
{
  private final ct a;
  private final Provider b;
  
  private cv(ct paramct, Provider paramProvider)
  {
    a = paramct;
    b = paramProvider;
  }
  
  public static cv a(ct paramct, Provider paramProvider)
  {
    cv localcv = new com/truecaller/truepay/app/a/b/cv;
    localcv.<init>(paramct, paramProvider);
    return localcv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cv
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */