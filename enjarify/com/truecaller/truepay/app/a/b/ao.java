package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ao
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ao(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ao a(ab paramab, Provider paramProvider)
  {
    ao localao = new com/truecaller/truepay/app/a/b/ao;
    localao.<init>(paramab, paramProvider);
    return localao;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */