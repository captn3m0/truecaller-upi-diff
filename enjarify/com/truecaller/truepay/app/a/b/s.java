package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private s(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static s a(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    s locals = new com/truecaller/truepay/app/a/b/s;
    locals.<init>(paramc, paramProvider1, paramProvider2, paramProvider3);
    return locals;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */