package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class au
  implements d
{
  private final ab a;
  private final Provider b;
  
  private au(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static au a(ab paramab, Provider paramProvider)
  {
    au localau = new com/truecaller/truepay/app/a/b/au;
    localau.<init>(paramab, paramProvider);
    return localau;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.au
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */