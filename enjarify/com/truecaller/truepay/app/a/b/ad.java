package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ad
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ad(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ad a(ab paramab, Provider paramProvider)
  {
    ad localad = new com/truecaller/truepay/app/a/b/ad;
    localad.<init>(paramab, paramProvider);
    return localad;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */