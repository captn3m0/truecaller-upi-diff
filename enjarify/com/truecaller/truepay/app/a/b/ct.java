package com.truecaller.truepay.app.a.b;

import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public final class ct
{
  /* Error */
  static KeyStore a(android.content.Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 14	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   4: astore_0
    //   5: getstatic 20	com/truecaller/truepay/R$raw:tc_pay_trust_store	I
    //   8: istore_1
    //   9: aload_0
    //   10: iload_1
    //   11: invokevirtual 26	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
    //   14: astore_0
    //   15: ldc 28
    //   17: astore_2
    //   18: aload_2
    //   19: invokestatic 34	java/security/KeyStore:getInstance	(Ljava/lang/String;)Ljava/security/KeyStore;
    //   22: astore_2
    //   23: new 36	java/lang/StringBuilder
    //   26: astore_3
    //   27: aload_3
    //   28: invokespecial 37	java/lang/StringBuilder:<init>	()V
    //   31: bipush 13
    //   33: istore 4
    //   35: iload 4
    //   37: newarray <illegal type>
    //   39: astore 5
    //   41: bipush 108
    //   43: istore 6
    //   45: iconst_0
    //   46: istore 7
    //   48: aload 5
    //   50: iconst_0
    //   51: iload 6
    //   53: iastore
    //   54: bipush 111
    //   56: istore 6
    //   58: iconst_1
    //   59: istore 8
    //   61: aload 5
    //   63: iload 8
    //   65: iload 6
    //   67: iastore
    //   68: iconst_2
    //   69: istore 8
    //   71: bipush 110
    //   73: istore 9
    //   75: aload 5
    //   77: iload 8
    //   79: iload 9
    //   81: iastore
    //   82: iconst_3
    //   83: istore 8
    //   85: bipush 103
    //   87: istore 10
    //   89: aload 5
    //   91: iload 8
    //   93: iload 10
    //   95: iastore
    //   96: iconst_4
    //   97: istore 8
    //   99: bipush 119
    //   101: istore 10
    //   103: aload 5
    //   105: iload 8
    //   107: iload 10
    //   109: iastore
    //   110: iconst_5
    //   111: istore 8
    //   113: bipush 105
    //   115: istore 10
    //   117: aload 5
    //   119: iload 8
    //   121: iload 10
    //   123: iastore
    //   124: bipush 6
    //   126: istore 8
    //   128: aload 5
    //   130: iload 8
    //   132: iload 9
    //   134: iastore
    //   135: bipush 7
    //   137: istore 8
    //   139: bipush 100
    //   141: istore 10
    //   143: aload 5
    //   145: iload 8
    //   147: iload 10
    //   149: iastore
    //   150: bipush 8
    //   152: istore 8
    //   154: aload 5
    //   156: iload 8
    //   158: iload 9
    //   160: iastore
    //   161: bipush 9
    //   163: istore 8
    //   165: aload 5
    //   167: iload 8
    //   169: iload 6
    //   171: iastore
    //   172: bipush 10
    //   174: istore 6
    //   176: bipush 114
    //   178: istore 8
    //   180: aload 5
    //   182: iload 6
    //   184: iload 8
    //   186: iastore
    //   187: bipush 11
    //   189: istore 6
    //   191: bipush 116
    //   193: istore 8
    //   195: aload 5
    //   197: iload 6
    //   199: iload 8
    //   201: iastore
    //   202: bipush 12
    //   204: istore 6
    //   206: bipush 104
    //   208: istore 8
    //   210: aload 5
    //   212: iload 6
    //   214: iload 8
    //   216: iastore
    //   217: iload 7
    //   219: iload 4
    //   221: if_icmpge +27 -> 248
    //   224: aload 5
    //   226: iload 7
    //   228: iaload
    //   229: i2c
    //   230: istore 6
    //   232: aload_3
    //   233: iload 6
    //   235: invokevirtual 64	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   238: pop
    //   239: iload 7
    //   241: iconst_1
    //   242: iadd
    //   243: istore 7
    //   245: goto -28 -> 217
    //   248: aload_3
    //   249: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   252: astore_3
    //   253: aload_3
    //   254: invokevirtual 74	java/lang/String:toCharArray	()[C
    //   257: astore_3
    //   258: aload_2
    //   259: aload_0
    //   260: aload_3
    //   261: invokevirtual 78	java/security/KeyStore:load	(Ljava/io/InputStream;[C)V
    //   264: goto +22 -> 286
    //   267: astore_2
    //   268: goto +24 -> 292
    //   271: pop
    //   272: iconst_0
    //   273: istore_1
    //   274: aconst_null
    //   275: astore_2
    //   276: ldc 80
    //   278: astore_3
    //   279: iconst_1
    //   280: anewarray 70	java/lang/String
    //   283: iconst_0
    //   284: aload_3
    //   285: aastore
    //   286: aload_0
    //   287: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   290: aload_2
    //   291: areturn
    //   292: aload_0
    //   293: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   296: aload_2
    //   297: athrow
    //   298: pop
    //   299: goto -23 -> 276
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	302	0	paramContext	android.content.Context
    //   8	266	1	i	int
    //   17	242	2	localObject1	Object
    //   267	1	2	localObject2	Object
    //   275	22	2	localKeyStore	KeyStore
    //   26	259	3	localObject3	Object
    //   33	189	4	j	int
    //   39	186	5	arrayOfInt	int[]
    //   43	191	6	c	char
    //   46	198	7	k	int
    //   59	156	8	m	int
    //   73	86	9	n	int
    //   87	61	10	i1	int
    //   271	1	13	localIOException1	java.io.IOException
    //   298	1	14	localIOException2	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   18	22	267	finally
    //   23	26	267	finally
    //   27	31	267	finally
    //   35	39	267	finally
    //   51	54	267	finally
    //   65	68	267	finally
    //   79	82	267	finally
    //   93	96	267	finally
    //   107	110	267	finally
    //   121	124	267	finally
    //   132	135	267	finally
    //   147	150	267	finally
    //   158	161	267	finally
    //   169	172	267	finally
    //   184	187	267	finally
    //   199	202	267	finally
    //   214	217	267	finally
    //   226	229	267	finally
    //   233	239	267	finally
    //   248	252	267	finally
    //   253	257	267	finally
    //   260	264	267	finally
    //   279	286	267	finally
    //   18	22	271	java/io/IOException
    //   18	22	271	java/security/cert/CertificateException
    //   18	22	271	java/security/NoSuchAlgorithmException
    //   18	22	271	java/security/KeyStoreException
    //   23	26	298	java/io/IOException
    //   23	26	298	java/security/cert/CertificateException
    //   23	26	298	java/security/NoSuchAlgorithmException
    //   23	26	298	java/security/KeyStoreException
    //   27	31	298	java/io/IOException
    //   27	31	298	java/security/cert/CertificateException
    //   27	31	298	java/security/NoSuchAlgorithmException
    //   27	31	298	java/security/KeyStoreException
    //   35	39	298	java/io/IOException
    //   35	39	298	java/security/cert/CertificateException
    //   35	39	298	java/security/NoSuchAlgorithmException
    //   35	39	298	java/security/KeyStoreException
    //   51	54	298	java/io/IOException
    //   51	54	298	java/security/cert/CertificateException
    //   51	54	298	java/security/NoSuchAlgorithmException
    //   51	54	298	java/security/KeyStoreException
    //   65	68	298	java/io/IOException
    //   65	68	298	java/security/cert/CertificateException
    //   65	68	298	java/security/NoSuchAlgorithmException
    //   65	68	298	java/security/KeyStoreException
    //   79	82	298	java/io/IOException
    //   79	82	298	java/security/cert/CertificateException
    //   79	82	298	java/security/NoSuchAlgorithmException
    //   79	82	298	java/security/KeyStoreException
    //   93	96	298	java/io/IOException
    //   93	96	298	java/security/cert/CertificateException
    //   93	96	298	java/security/NoSuchAlgorithmException
    //   93	96	298	java/security/KeyStoreException
    //   107	110	298	java/io/IOException
    //   107	110	298	java/security/cert/CertificateException
    //   107	110	298	java/security/NoSuchAlgorithmException
    //   107	110	298	java/security/KeyStoreException
    //   121	124	298	java/io/IOException
    //   121	124	298	java/security/cert/CertificateException
    //   121	124	298	java/security/NoSuchAlgorithmException
    //   121	124	298	java/security/KeyStoreException
    //   132	135	298	java/io/IOException
    //   132	135	298	java/security/cert/CertificateException
    //   132	135	298	java/security/NoSuchAlgorithmException
    //   132	135	298	java/security/KeyStoreException
    //   147	150	298	java/io/IOException
    //   147	150	298	java/security/cert/CertificateException
    //   147	150	298	java/security/NoSuchAlgorithmException
    //   147	150	298	java/security/KeyStoreException
    //   158	161	298	java/io/IOException
    //   158	161	298	java/security/cert/CertificateException
    //   158	161	298	java/security/NoSuchAlgorithmException
    //   158	161	298	java/security/KeyStoreException
    //   169	172	298	java/io/IOException
    //   169	172	298	java/security/cert/CertificateException
    //   169	172	298	java/security/NoSuchAlgorithmException
    //   169	172	298	java/security/KeyStoreException
    //   184	187	298	java/io/IOException
    //   184	187	298	java/security/cert/CertificateException
    //   184	187	298	java/security/NoSuchAlgorithmException
    //   184	187	298	java/security/KeyStoreException
    //   199	202	298	java/io/IOException
    //   199	202	298	java/security/cert/CertificateException
    //   199	202	298	java/security/NoSuchAlgorithmException
    //   199	202	298	java/security/KeyStoreException
    //   214	217	298	java/io/IOException
    //   214	217	298	java/security/cert/CertificateException
    //   214	217	298	java/security/NoSuchAlgorithmException
    //   214	217	298	java/security/KeyStoreException
    //   226	229	298	java/io/IOException
    //   226	229	298	java/security/cert/CertificateException
    //   226	229	298	java/security/NoSuchAlgorithmException
    //   226	229	298	java/security/KeyStoreException
    //   233	239	298	java/io/IOException
    //   233	239	298	java/security/cert/CertificateException
    //   233	239	298	java/security/NoSuchAlgorithmException
    //   233	239	298	java/security/KeyStoreException
    //   248	252	298	java/io/IOException
    //   248	252	298	java/security/cert/CertificateException
    //   248	252	298	java/security/NoSuchAlgorithmException
    //   248	252	298	java/security/KeyStoreException
    //   253	257	298	java/io/IOException
    //   253	257	298	java/security/cert/CertificateException
    //   253	257	298	java/security/NoSuchAlgorithmException
    //   253	257	298	java/security/KeyStoreException
    //   260	264	298	java/io/IOException
    //   260	264	298	java/security/cert/CertificateException
    //   260	264	298	java/security/NoSuchAlgorithmException
    //   260	264	298	java/security/KeyStoreException
  }
  
  static SSLSocketFactory a(X509TrustManager paramX509TrustManager)
  {
    Object localObject1 = "TLSv1.2";
    try
    {
      localObject1 = SSLContext.getInstance((String)localObject1);
      int i = 1;
      paramX509TrustManager = "SecurityModule";
    }
    catch (NoSuchAlgorithmException|KeyManagementException localNoSuchAlgorithmException1)
    {
      try
      {
        localObject2 = new TrustManager[i];
        localObject2[0] = paramX509TrustManager;
        paramX509TrustManager = new java/security/SecureRandom;
        paramX509TrustManager.<init>();
        ((SSLContext)localObject1).init(null, (TrustManager[])localObject2, paramX509TrustManager);
      }
      catch (NoSuchAlgorithmException|KeyManagementException localNoSuchAlgorithmException2)
      {
        Object localObject2;
        for (;;) {}
      }
      localNoSuchAlgorithmException1;
      localObject1 = null;
    }
    localObject2 = "Could not initiate socket factory";
    { paramX509TrustManager }[1] = localObject2;
    if (localObject1 != null) {
      return ((SSLContext)localObject1).getSocketFactory();
    }
    return null;
  }
  
  static X509TrustManager a(KeyStore paramKeyStore)
  {
    try
    {
      Object localObject = TrustManagerFactory.getDefaultAlgorithm();
      localObject = TrustManagerFactory.getInstance((String)localObject);
      ((TrustManagerFactory)localObject).init(paramKeyStore);
      paramKeyStore = ((TrustManagerFactory)localObject).getTrustManagers();
      int i = paramKeyStore.length;
      int j = 1;
      if (i == j)
      {
        i = 0;
        localObject = null;
        localStringBuilder = paramKeyStore[0];
        boolean bool = localStringBuilder instanceof X509TrustManager;
        if (bool)
        {
          paramKeyStore = paramKeyStore[0];
          return (X509TrustManager)paramKeyStore;
        }
      }
      localObject = new java/lang/IllegalStateException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      String str = "Unexpected default trust managers:";
      localStringBuilder.<init>(str);
      paramKeyStore = Arrays.toString(paramKeyStore);
      localStringBuilder.append(paramKeyStore);
      paramKeyStore = localStringBuilder.toString();
      ((IllegalStateException)localObject).<init>(paramKeyStore);
      throw ((Throwable)localObject);
    }
    catch (NoSuchAlgorithmException|KeyStoreException localNoSuchAlgorithmException)
    {
      for (;;) {}
    }
    { "SecurityModule" }[1] = "Could not initiate X509TrustManager";
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ct
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */