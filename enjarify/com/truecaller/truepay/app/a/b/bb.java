package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bb
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bb(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bb a(ab paramab, Provider paramProvider)
  {
    bb localbb = new com/truecaller/truepay/app/a/b/bb;
    localbb.<init>(paramab, paramProvider);
    return localbb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */