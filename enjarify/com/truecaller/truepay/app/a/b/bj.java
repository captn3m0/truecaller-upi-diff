package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bj
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bj(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bj a(ab paramab, Provider paramProvider)
  {
    bj localbj = new com/truecaller/truepay/app/a/b/bj;
    localbj.<init>(paramab, paramProvider);
    return localbj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */