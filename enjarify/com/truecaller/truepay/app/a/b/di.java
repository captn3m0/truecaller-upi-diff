package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class di
  implements d
{
  private final dc a;
  private final Provider b;
  
  private di(dc paramdc, Provider paramProvider)
  {
    a = paramdc;
    b = paramProvider;
  }
  
  public static di a(dc paramdc, Provider paramProvider)
  {
    di localdi = new com/truecaller/truepay/app/a/b/di;
    localdi.<init>(paramdc, paramProvider);
    return localdi;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.di
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */