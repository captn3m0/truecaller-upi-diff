package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bq
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bq(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bq a(ab paramab, Provider paramProvider)
  {
    bq localbq = new com/truecaller/truepay/app/a/b/bq;
    localbq.<init>(paramab, paramProvider);
    return localbq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */