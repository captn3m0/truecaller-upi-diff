package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cz
  implements d
{
  private final cx a;
  private final Provider b;
  
  private cz(cx paramcx, Provider paramProvider)
  {
    a = paramcx;
    b = paramProvider;
  }
  
  public static cz a(cx paramcx, Provider paramProvider)
  {
    cz localcz = new com/truecaller/truepay/app/a/b/cz;
    localcz.<init>(paramcx, paramProvider);
    return localcz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cz
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */