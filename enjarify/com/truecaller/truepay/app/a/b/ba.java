package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ba
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ba(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ba a(ab paramab, Provider paramProvider)
  {
    ba localba = new com/truecaller/truepay/app/a/b/ba;
    localba.<init>(paramab, paramProvider);
    return localba;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ba
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */