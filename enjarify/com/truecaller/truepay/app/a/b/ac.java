package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ac(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ac a(ab paramab, Provider paramProvider)
  {
    ac localac = new com/truecaller/truepay/app/a/b/ac;
    localac.<init>(paramab, paramProvider);
    return localac;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */