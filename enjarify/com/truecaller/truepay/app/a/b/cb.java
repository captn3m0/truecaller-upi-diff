package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cb
  implements d
{
  private final br a;
  private final Provider b;
  
  private cb(br parambr, Provider paramProvider)
  {
    a = parambr;
    b = paramProvider;
  }
  
  public static cb a(br parambr, Provider paramProvider)
  {
    cb localcb = new com/truecaller/truepay/app/a/b/cb;
    localcb.<init>(parambr, paramProvider);
    return localcb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */