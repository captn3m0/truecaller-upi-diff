package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cm
  implements d
{
  private final br a;
  private final Provider b;
  
  private cm(br parambr, Provider paramProvider)
  {
    a = parambr;
    b = paramProvider;
  }
  
  public static cm a(br parambr, Provider paramProvider)
  {
    cm localcm = new com/truecaller/truepay/app/a/b/cm;
    localcm.<init>(parambr, paramProvider);
    return localcm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cm
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */