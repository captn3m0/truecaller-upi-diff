package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final c a;
  private final Provider b;
  
  private i(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static i a(c paramc, Provider paramProvider)
  {
    i locali = new com/truecaller/truepay/app/a/b/i;
    locali.<init>(paramc, paramProvider);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */