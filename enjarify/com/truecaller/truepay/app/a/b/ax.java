package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ax
  implements d
{
  private final ab a;
  private final Provider b;
  
  private ax(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static ax a(ab paramab, Provider paramProvider)
  {
    ax localax = new com/truecaller/truepay/app/a/b/ax;
    localax.<init>(paramab, paramProvider);
    return localax;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ax
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */