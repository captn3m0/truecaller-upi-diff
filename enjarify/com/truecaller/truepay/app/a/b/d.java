package com.truecaller.truepay.app.a.b;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  
  private d(c paramc, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static d a(c paramc, Provider paramProvider1, Provider paramProvider2)
  {
    d locald = new com/truecaller/truepay/app/a/b/d;
    locald.<init>(paramc, paramProvider1, paramProvider2);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */