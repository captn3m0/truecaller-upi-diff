package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bv
  implements d
{
  private final br a;
  private final Provider b;
  
  private bv(br parambr, Provider paramProvider)
  {
    a = parambr;
    b = paramProvider;
  }
  
  public static bv a(br parambr, Provider paramProvider)
  {
    bv localbv = new com/truecaller/truepay/app/a/b/bv;
    localbv.<init>(parambr, paramProvider);
    return localbv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bv
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */