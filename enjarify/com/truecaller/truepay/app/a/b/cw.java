package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cw
  implements d
{
  private final ct a;
  private final Provider b;
  
  private cw(ct paramct, Provider paramProvider)
  {
    a = paramct;
    b = paramProvider;
  }
  
  public static cw a(ct paramct, Provider paramProvider)
  {
    cw localcw = new com/truecaller/truepay/app/a/b/cw;
    localcw.<init>(paramct, paramProvider);
    return localcw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */