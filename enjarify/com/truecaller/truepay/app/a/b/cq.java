package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class cq
  implements d
{
  private final cp a;
  private final Provider b;
  
  private cq(cp paramcp, Provider paramProvider)
  {
    a = paramcp;
    b = paramProvider;
  }
  
  public static cq a(cp paramcp, Provider paramProvider)
  {
    cq localcq = new com/truecaller/truepay/app/a/b/cq;
    localcq.<init>(paramcp, paramProvider);
    return localcq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */