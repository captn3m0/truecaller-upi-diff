package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final c a;
  private final Provider b;
  
  private h(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static h a(c paramc, Provider paramProvider)
  {
    h localh = new com/truecaller/truepay/app/a/b/h;
    localh.<init>(paramc, paramProvider);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */