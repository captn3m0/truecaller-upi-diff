package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class aw
  implements d
{
  private final ab a;
  private final Provider b;
  
  private aw(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static aw a(ab paramab, Provider paramProvider)
  {
    aw localaw = new com/truecaller/truepay/app/a/b/aw;
    localaw.<init>(paramab, paramProvider);
    return localaw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.aw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */