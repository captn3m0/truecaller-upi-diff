package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class da
  implements d
{
  private final cx a;
  private final Provider b;
  
  private da(cx paramcx, Provider paramProvider)
  {
    a = paramcx;
    b = paramProvider;
  }
  
  public static da a(cx paramcx, Provider paramProvider)
  {
    da localda = new com/truecaller/truepay/app/a/b/da;
    localda.<init>(paramcx, paramProvider);
    return localda;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.da
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */