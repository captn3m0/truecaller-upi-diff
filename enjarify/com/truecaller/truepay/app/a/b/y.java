package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d
{
  private final x a;
  private final Provider b;
  private final Provider c;
  
  private y(x paramx, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramx;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static y a(x paramx, Provider paramProvider1, Provider paramProvider2)
  {
    y localy = new com/truecaller/truepay/app/a/b/y;
    localy.<init>(paramx, paramProvider1, paramProvider2);
    return localy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */