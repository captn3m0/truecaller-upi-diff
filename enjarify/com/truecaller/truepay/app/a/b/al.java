package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class al
  implements d
{
  private final ab a;
  private final Provider b;
  
  private al(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static al a(ab paramab, Provider paramProvider)
  {
    al localal = new com/truecaller/truepay/app/a/b/al;
    localal.<init>(paramab, paramProvider);
    return localal;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */