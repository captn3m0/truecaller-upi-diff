package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class av
  implements d
{
  private final ab a;
  private final Provider b;
  
  private av(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static av a(ab paramab, Provider paramProvider)
  {
    av localav = new com/truecaller/truepay/app/a/b/av;
    localav.<init>(paramab, paramProvider);
    return localav;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.av
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */