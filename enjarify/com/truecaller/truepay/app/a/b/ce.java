package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class ce
  implements d
{
  private final br a;
  private final Provider b;
  
  private ce(br parambr, Provider paramProvider)
  {
    a = parambr;
    b = paramProvider;
  }
  
  public static ce a(br parambr, Provider paramProvider)
  {
    ce localce = new com/truecaller/truepay/app/a/b/ce;
    localce.<init>(parambr, paramProvider);
    return localce;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.ce
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */