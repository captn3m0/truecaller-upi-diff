package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class bg
  implements d
{
  private final ab a;
  private final Provider b;
  
  private bg(ab paramab, Provider paramProvider)
  {
    a = paramab;
    b = paramProvider;
  }
  
  public static bg a(ab paramab, Provider paramProvider)
  {
    bg localbg = new com/truecaller/truepay/app/a/b/bg;
    localbg.<init>(paramab, paramProvider);
    return localbg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.bg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */