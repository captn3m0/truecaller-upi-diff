package com.truecaller.truepay.app.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class dh
  implements d
{
  private final dc a;
  private final Provider b;
  private final Provider c;
  
  private dh(dc paramdc, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramdc;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static dh a(dc paramdc, Provider paramProvider1, Provider paramProvider2)
  {
    dh localdh = new com/truecaller/truepay/app/a/b/dh;
    localdh.<init>(paramdc, paramProvider1, paramProvider2);
    return localdh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.dh
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */