package com.truecaller.truepay.app.utils;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final Provider a;
  
  private w(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static w a(Provider paramProvider)
  {
    w localw = new com/truecaller/truepay/app/utils/w;
    localw.<init>(paramProvider);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */