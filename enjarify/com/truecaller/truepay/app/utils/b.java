package com.truecaller.truepay.app.utils;

import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d
{
  private final Provider a;
  
  private b(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static b a(Provider paramProvider)
  {
    b localb = new com/truecaller/truepay/app/utils/b;
    localb.<init>(paramProvider);
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */