package com.truecaller.truepay.app.utils;

import android.text.TextUtils;
import com.google.gson.f;
import com.google.gson.t;
import com.truecaller.truepay.data.e.e;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public final class a
{
  private final e a;
  
  public a(e parame)
  {
    a = parame;
  }
  
  public final com.truecaller.truepay.data.api.model.a a(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    Iterator localIterator = a().iterator();
    com.truecaller.truepay.data.api.model.a locala;
    boolean bool2;
    do
    {
      String str;
      do
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        locala = (com.truecaller.truepay.data.api.model.a)localIterator.next();
        str = a;
      } while (str == null);
      bool2 = str.equals(paramString);
    } while (!bool2);
    return locala;
    return null;
  }
  
  public final ArrayList a()
  {
    Object localObject1 = new com/google/gson/f;
    ((f)localObject1).<init>();
    Object localObject2 = a.a();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    boolean bool = TextUtils.isEmpty((CharSequence)localObject2);
    if (!bool) {
      try
      {
        localObject3 = new com/truecaller/truepay/app/utils/a$1;
        ((a.1)localObject3).<init>(this);
        localObject3 = b;
        localObject1 = ((f)localObject1).a((String)localObject2, (Type)localObject3);
        localObject1 = (Collection)localObject1;
        if (localObject1 != null) {
          localArrayList.addAll((Collection)localObject1);
        }
      }
      catch (t localt)
      {
        int i = 1;
        localObject2 = new String[i];
        bool = false;
        Object localObject3 = null;
        String str = localt.toString();
        localObject2[0] = str;
      }
    }
    return localArrayList;
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    if (parama != null)
    {
      int i = 1;
      try
      {
        ArrayList localArrayList = a();
        Object localObject1 = localArrayList.iterator();
        Object localObject2;
        boolean bool2;
        do
        {
          boolean bool1 = ((Iterator)localObject1).hasNext();
          if (!bool1) {
            break;
          }
          localObject2 = ((Iterator)localObject1).next();
          localObject2 = (com.truecaller.truepay.data.api.model.a)localObject2;
          String str1 = a;
          String str2 = a;
          bool2 = TextUtils.equals(str1, str2);
        } while (!bool2);
        localObject1 = i;
        i = ((String)localObject1);
        localObject1 = c;
        c = ((String)localObject1);
        int j = 0;
        localObject1 = null;
        break label112;
        j = 1;
        label112:
        if (j != 0) {
          localArrayList.add(parama);
        }
        a(localArrayList);
        return;
      }
      catch (Exception parama)
      {
        String[] arrayOfString = new String[i];
        parama = parama.toString();
        arrayOfString[0] = parama;
      }
    }
  }
  
  public final void a(ArrayList paramArrayList)
  {
    f localf = new com/google/gson/f;
    localf.<init>();
    paramArrayList = localf.b(paramArrayList);
    a.a(paramArrayList);
  }
  
  public final ArrayList b()
  {
    return a();
  }
  
  public final ArrayList b(com.truecaller.truepay.data.api.model.a parama)
  {
    Object localObject = a();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject = ((ArrayList)localObject).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      com.truecaller.truepay.data.api.model.a locala = (com.truecaller.truepay.data.api.model.a)((Iterator)localObject).next();
      String str1 = a;
      String str2 = a;
      boolean bool2 = str1.equalsIgnoreCase(str2);
      if (!bool2) {
        localArrayList.add(locala);
      }
    }
    return localArrayList;
  }
  
  public final com.truecaller.truepay.data.api.model.a c()
  {
    Iterator localIterator = a().iterator();
    com.truecaller.truepay.data.api.model.a locala;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      locala = (com.truecaller.truepay.data.api.model.a)localIterator.next();
      bool2 = g;
    } while (!bool2);
    return locala;
    return null;
  }
  
  public final void c(com.truecaller.truepay.data.api.model.a parama)
  {
    if (parama == null) {
      return;
    }
    ArrayList localArrayList = a();
    int i = 0;
    int j = 0;
    for (;;)
    {
      int k = localArrayList.size();
      if (i >= k) {
        break;
      }
      String str1 = a;
      String str2 = geta;
      boolean bool = str1.equals(str2);
      if (bool) {
        j = i;
      }
      i += 1;
    }
    localArrayList.set(j, parama);
    a(localArrayList);
  }
  
  public final String d()
  {
    Iterator localIterator = a().iterator();
    com.truecaller.truepay.data.api.model.a locala;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      locala = (com.truecaller.truepay.data.api.model.a)localIterator.next();
      bool2 = g;
    } while (!bool2);
    return b;
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */