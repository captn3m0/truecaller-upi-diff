package com.truecaller.truepay.app.utils;

import c.d.c;
import c.g.b.k;
import c.o;
import com.truecaller.truepay.app.ui.growth.db.a;
import com.truecaller.truepay.data.api.model.PromoBannerDO;
import com.truecaller.truepay.data.api.model.ad;
import com.truecaller.truepay.data.api.model.h;
import e.b;
import e.d;
import e.r;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.j;

public final class am$b
  implements d
{
  am$b(j paramj) {}
  
  public final void onFailure(b paramb, Throwable paramThrowable)
  {
    k.b(paramb, "call");
    k.b(paramThrowable, "t");
    paramb = (c)a;
    paramThrowable = o.a;
    paramThrowable = o.d(null);
    paramb.b(paramThrowable);
  }
  
  public final void onResponse(b paramb, r paramr)
  {
    Object localObject = "call";
    k.b(paramb, (String)localObject);
    k.b(paramr, "response");
    paramb = (h)paramr.e();
    if (paramb == null)
    {
      paramb = (c)a;
      paramr = o.a;
      paramr = o.d(null);
      paramb.b(paramr);
      return;
    }
    k.a(paramb, "response.body() ?: return coroutine.resume(null)");
    paramr = new java/util/ArrayList;
    paramr.<init>();
    paramr = (List)paramr;
    paramb = ca;
    if (paramb != null)
    {
      paramb = ((Iterable)paramb).iterator();
      for (;;)
      {
        boolean bool = paramb.hasNext();
        if (!bool) {
          break;
        }
        localObject = (PromoBannerDO)paramb.next();
        a locala = new com/truecaller/truepay/app/ui/growth/db/a;
        locala.<init>();
        String str = ((PromoBannerDO)localObject).getImageUrl();
        locala.a(str);
        str = ((PromoBannerDO)localObject).getExpiresAt();
        long l = Long.parseLong(str);
        d = l;
        localObject = ((PromoBannerDO)localObject).getDeepLink();
        locala.b((String)localObject);
        bool = false;
        localObject = null;
        e = 0;
        paramr.add(locala);
      }
    }
    paramb = (c)a;
    localObject = o.a;
    paramr = o.d(paramr);
    paramb.b(paramr);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.am.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */