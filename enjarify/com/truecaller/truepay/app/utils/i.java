package com.truecaller.truepay.app.utils;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final Provider a;
  
  private i(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static i a(Provider paramProvider)
  {
    i locali = new com/truecaller/truepay/app/utils/i;
    locali.<init>(paramProvider);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */