package com.truecaller.truepay.app.utils;

import android.app.Application;
import android.content.ContentResolver;
import android.provider.Settings.Global;

public final class d
  implements c
{
  private final Application a;
  
  public d(Application paramApplication)
  {
    a = paramApplication;
  }
  
  public final boolean a()
  {
    ContentResolver localContentResolver = a.getContentResolver();
    String str = "airplane_mode_on";
    int i = Settings.Global.getInt(localContentResolver, str, 0);
    return i != 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */