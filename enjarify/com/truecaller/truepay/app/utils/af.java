package com.truecaller.truepay.app.utils;

import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.TextView;
import c.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.utils.extensions.t;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class af
{
  public static final List a(View paramView, int... paramVarArgs)
  {
    k.b(paramView, "receiver$0");
    k.b(paramVarArgs, "ids");
    Object localObject = new java/util/ArrayList;
    int i = paramVarArgs.length;
    ((ArrayList)localObject).<init>(i);
    localObject = (Collection)localObject;
    i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      int k = paramVarArgs[j];
      View localView = paramView.findViewById(k);
      ((Collection)localObject).add(localView);
      j += 1;
    }
    return m.g((Iterable)localObject);
  }
  
  public static final void a(TextView paramTextView, Drawable paramDrawable)
  {
    String str = "receiver$0";
    k.b(paramTextView, str);
    int i = paramTextView.getLineHeight();
    if (paramDrawable != null) {
      paramDrawable.setBounds(0, 0, i, i);
    }
    i /= 2;
    paramTextView.setCompoundDrawablePadding(i);
    paramTextView.setCompoundDrawables(paramDrawable, null, null, null);
  }
  
  public static final void a(List paramList)
  {
    k.b(paramList, "receiver$0");
    paramList = (Iterable)paramList;
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramList = paramList.iterator();
    Object localObject2;
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = paramList.next();
      Object localObject3 = localObject2;
      localObject3 = (View)localObject2;
      boolean bool3 = localObject3 instanceof TextInputLayout;
      if (bool3) {
        ((Collection)localObject1).add(localObject2);
      }
    }
    localObject1 = (Iterable)localObject1;
    paramList = new java/util/ArrayList;
    int i = m.a((Iterable)localObject1, 10);
    paramList.<init>(i);
    paramList = (Collection)paramList;
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool2;
    for (;;)
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break label170;
      }
      localObject2 = (View)((Iterator)localObject1).next();
      if (localObject2 == null) {
        break;
      }
      localObject2 = (TextInputLayout)localObject2;
      paramList.add(localObject2);
    }
    paramList = new c/u;
    paramList.<init>("null cannot be cast to non-null type android.support.design.widget.TextInputLayout");
    throw paramList;
    label170:
    paramList = ((Iterable)paramList).iterator();
    for (;;)
    {
      boolean bool4 = paramList.hasNext();
      if (!bool4) {
        break;
      }
      localObject1 = (TextInputLayout)paramList.next();
      bool2 = false;
      ((TextInputLayout)localObject1).setErrorEnabled(false);
      localObject2 = (CharSequence)"";
      ((TextInputLayout)localObject1).setError((CharSequence)localObject2);
    }
  }
  
  public static final void a(List paramList, boolean paramBoolean)
  {
    Object localObject = "receiver$0";
    k.b(paramList, (String)localObject);
    paramList = ((Iterable)paramList).iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localObject = (View)paramList.next();
      t.a((View)localObject, paramBoolean);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */