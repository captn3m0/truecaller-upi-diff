package com.truecaller.truepay.app.utils;

import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.widget.EditText;

final class ax$1
  implements TextWatcher
{
  ax$1(String paramString, EditText paramEditText) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    paramEditable = paramEditable.toString();
    Object localObject = a;
    boolean bool = paramEditable.startsWith((String)localObject);
    if (!bool)
    {
      paramEditable = b;
      localObject = a;
      paramEditable.setText((CharSequence)localObject);
      paramEditable = b.getText();
      localObject = b.getText();
      int i = ((Editable)localObject).length();
      Selection.setSelection(paramEditable, i);
    }
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.ax.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */