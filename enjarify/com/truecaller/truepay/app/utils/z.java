package com.truecaller.truepay.app.utils;

import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.truepay.app.ui.payments.c.h;
import com.truecaller.truepay.app.ui.payments.models.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class z
{
  public static List a(List paramList, String paramString)
  {
    k.b(paramList, "list");
    k.b(paramString, "searchKey");
    paramList = (Iterable)paramList;
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramList = paramList.iterator();
    boolean bool1;
    Object localObject2;
    Object localObject3;
    boolean bool2;
    for (;;)
    {
      bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = paramList.next();
      localObject3 = localObject2;
      localObject3 = (h)localObject2;
      bool2 = localObject3 instanceof a;
      if (bool2) {
        ((Collection)localObject1).add(localObject2);
      }
    }
    localObject1 = (Iterable)localObject1;
    paramList = new java/util/ArrayList;
    paramList.<init>();
    paramList = (Collection)paramList;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break label230;
      }
      localObject2 = ((Iterator)localObject1).next();
      localObject3 = localObject2;
      localObject3 = (h)localObject2;
      if (localObject3 == null) {
        break;
      }
      localObject3 = ((a)localObject3).d();
      k.a(localObject3, "(it as BaseUtility).title");
      localObject3 = (CharSequence)localObject3;
      Object localObject4 = paramString;
      localObject4 = (CharSequence)paramString;
      boolean bool3 = true;
      bool2 = m.a((CharSequence)localObject3, (CharSequence)localObject4, bool3);
      if (bool2) {
        paramList.add(localObject2);
      }
    }
    paramList = new c/u;
    paramList.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.payments.models.BaseUtility");
    throw paramList;
    label230:
    return (List)paramList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */