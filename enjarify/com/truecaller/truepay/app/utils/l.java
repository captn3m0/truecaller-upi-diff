package com.truecaller.truepay.app.utils;

import android.os.Build.VERSION;
import android.text.TextUtils;
import com.truecaller.truepay.app.b.a;
import com.truecaller.truepay.app.b.f;
import com.truecaller.truepay.app.b.i;
import com.truecaller.truepay.data.e.e;

public final class l
{
  public final e a;
  public final e b;
  public final e c;
  private final e d;
  private final e e;
  private final e f;
  private final e g;
  private final e h;
  private final e i;
  
  public l(e parame1, e parame2, e parame3, e parame4, e parame5, e parame6, e parame7, e parame8, e parame9)
  {
    a = parame1;
    b = parame2;
    d = parame3;
    e = parame4;
    f = parame5;
    c = parame6;
    g = parame7;
    h = parame8;
    i = parame9;
  }
  
  public static String a()
  {
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = "10.41.6";
    Integer localInteger = Integer.valueOf(1041006);
    arrayOfObject[1] = localInteger;
    arrayOfObject[2] = "1114.0dbb949";
    return String.format("%s.%s.%s", arrayOfObject);
  }
  
  public static String a(String paramString1, String paramString2)
  {
    paramString1 = paramString2.concat(paramString1);
    paramString2 = String.valueOf(System.currentTimeMillis());
    return p.a(paramString1.concat(paramString2));
  }
  
  public static String b()
  {
    return String.valueOf(Build.VERSION.RELEASE);
  }
  
  public static String c()
  {
    return "android";
  }
  
  public final void a(String paramString)
  {
    a.a(paramString);
  }
  
  public final void b(String paramString)
  {
    i.a(paramString);
  }
  
  public final void c(String paramString)
  {
    g.a(paramString);
  }
  
  public final String d()
  {
    return b.a();
  }
  
  public final void d(String paramString)
  {
    c.a(paramString);
  }
  
  public final String e()
  {
    Object localObject = d.a();
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (!bool) {
      return d.a();
    }
    localObject = new com/truecaller/truepay/app/b/a;
    ((a)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final String f()
  {
    Object localObject = e;
    boolean bool = ((e)localObject).b();
    if (bool)
    {
      localObject = e.a();
      bool = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool) {
        return e.a();
      }
    }
    localObject = new com/truecaller/truepay/app/b/f;
    ((f)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final String g()
  {
    Object localObject = f;
    boolean bool = ((e)localObject).b();
    if (bool)
    {
      localObject = f.a();
      bool = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool) {
        return f.a();
      }
    }
    localObject = new com/truecaller/truepay/app/b/i;
    ((i)localObject).<init>();
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */