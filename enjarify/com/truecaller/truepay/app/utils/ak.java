package com.truecaller.truepay.app.utils;

import android.text.TextUtils;
import com.google.gson.c.a;
import com.google.gson.f;
import com.google.gson.t;
import com.truecaller.truepay.data.e.e;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

public final class ak
  implements aj
{
  private ArrayList a;
  private e b;
  
  public ak(e parame)
  {
    b = parame;
  }
  
  private ArrayList b()
  {
    Object localObject1 = new com/google/gson/f;
    ((f)localObject1).<init>();
    Object localObject2 = b.a();
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    a = ((ArrayList)localObject3);
    boolean bool = TextUtils.isEmpty((CharSequence)localObject2);
    if (!bool) {
      try
      {
        localObject3 = new com/truecaller/truepay/app/utils/ak$1;
        ((ak.1)localObject3).<init>(this);
        localObject3 = b;
        localObject1 = ((f)localObject1).a((String)localObject2, (Type)localObject3);
        localObject1 = (Collection)localObject1;
        if (localObject1 != null)
        {
          localObject2 = a;
          ((ArrayList)localObject2).addAll((Collection)localObject1);
        }
      }
      catch (t localt)
      {
        int i = 1;
        localObject2 = new String[i];
        bool = false;
        localObject3 = null;
        String str = localt.toString();
        localObject2[0] = str;
      }
    }
    return a;
  }
  
  public final ArrayList a()
  {
    ArrayList localArrayList = a;
    if (localArrayList != null) {
      return localArrayList;
    }
    return b();
  }
  
  public final void a(ArrayList paramArrayList)
  {
    f localf = new com/google/gson/f;
    localf.<init>();
    paramArrayList = localf.b(paramArrayList);
    b.a(paramArrayList);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */