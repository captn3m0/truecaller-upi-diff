package com.truecaller.truepay.app.utils;

import c.d.c;
import c.o.b;
import c.x;
import com.truecaller.truepay.app.ui.history.models.h;
import java.util.Collection;
import java.util.List;
import kotlinx.coroutines.ag;

final class ac$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag c;
  
  ac$a(ac paramac, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/truepay/app/utils/ac$a;
    ac localac = b;
    locala.<init>(localac, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    int j = 1;
    boolean bool1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label169;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label253;
      }
      paramObject = b.b;
      a = j;
      paramObject = ((com.truecaller.truepay.app.utils.a.a)paramObject).a();
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = b.b;
    String str = "last_transacted_item";
    int k = 2;
    a = k;
    paramObject = ((com.truecaller.truepay.app.utils.a.a)paramObject).a(str, this);
    if (paramObject == localObject1) {
      return localObject1;
    }
    label169:
    paramObject = (List)paramObject;
    long l = 0L;
    if (paramObject != null)
    {
      Object localObject2 = paramObject;
      localObject2 = (Collection)paramObject;
      boolean bool3 = ((Collection)localObject2).isEmpty() ^ j;
      if (bool3 == j)
      {
        paramObject = ((h)c.a.m.d((List)paramObject)).b();
        localObject1 = "yyyy-MM-dd HH:mm:ss Z";
        l = j.a((String)paramObject, (String)localObject1);
      }
    }
    paramObject = b.a;
    localObject1 = c.d.b.a.b.a(l);
    ((com.truecaller.truepay.data.e.b)paramObject).a((Long)localObject1);
    return x.a;
    label253:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.ac.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */