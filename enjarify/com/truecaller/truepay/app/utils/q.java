package com.truecaller.truepay.app.utils;

import android.content.Context;
import io.reactivex.o;
import java.io.File;
import java.io.IOException;

public final class q
{
  public Context a;
  
  public q(Context paramContext)
  {
    a = paramContext;
  }
  
  public final o a(String paramString)
  {
    -..Lambda.q.dLb43yAwVrnYK3EGYAXMZe-kBMs localdLb43yAwVrnYK3EGYAXMZe-kBMs = new com/truecaller/truepay/app/utils/-$$Lambda$q$dLb43yAwVrnYK3EGYAXMZe-kBMs;
    localdLb43yAwVrnYK3EGYAXMZe-kBMs.<init>(this, paramString);
    return o.a(localdLb43yAwVrnYK3EGYAXMZe-kBMs);
  }
  
  public final boolean b(String paramString)
  {
    File localFile = new java/io/File;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("/data/data/");
    String str = a.getPackageName();
    localStringBuilder.append(str);
    str = "/";
    localStringBuilder.append(str);
    localStringBuilder.append(paramString);
    paramString = localStringBuilder.toString();
    localFile.<init>(paramString);
    try
    {
      paramString = localFile.getCanonicalFile();
      return paramString.delete();
    }
    catch (IOException localIOException)
    {
      new String[1][0] = "Could not delete file.";
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */