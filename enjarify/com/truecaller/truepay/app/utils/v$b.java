package com.truecaller.truepay.app.utils;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.app.ui.rewards.views.activities.RewardsActivity;

public final class v$b
  extends BroadcastReceiver
{
  v$b(v paramv) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    k.b(paramIntent, "intent");
    String str = paramIntent.getStringExtra("instant_reward_content");
    int i = paramIntent.getIntExtra("instant_reward_notification_id", 0);
    Object localObject = paramContext.getSystemService("notification");
    if (localObject != null)
    {
      ((NotificationManager)localObject).cancel(i);
      paramIntent = new android/content/Intent;
      localObject = RewardsActivity.class;
      paramIntent.<init>(paramContext, (Class)localObject);
      boolean bool = true;
      paramIntent.putExtra("show_instant_reward", bool);
      paramIntent.putExtra("instant_reward_content", str);
      paramContext = a.a;
      if (paramContext != null)
      {
        paramContext.onRewardReceived(paramIntent);
        return;
      }
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.app.NotificationManager");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.v.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */