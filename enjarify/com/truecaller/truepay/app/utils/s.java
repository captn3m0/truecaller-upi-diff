package com.truecaller.truepay.app.utils;

import android.app.Notification;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.app.z.d;
import android.text.TextUtils;
import android.widget.ImageView;
import c.g.b.k;
import c.n.m;
import c.u;
import com.d.b.ab;
import com.d.b.ag;
import com.d.b.w;
import com.truecaller.truepay.R.drawable;

public final class s
  implements r
{
  final Context a;
  
  public s(Context paramContext)
  {
    a = paramContext;
  }
  
  public static Notification a(z.d paramd, r.a parama)
  {
    k.b(paramd, "builder");
    k.b(parama, "largeIconProvider");
    parama = parama.create();
    paramd = paramd.a(parama).h();
    k.a(paramd, "builder\n            .set…e())\n            .build()");
    return paramd;
  }
  
  private void a(String paramString, ImageView paramImageView, Drawable paramDrawable1, Drawable paramDrawable2, boolean paramBoolean)
  {
    k.b(paramImageView, "targetImageView");
    k.b(paramDrawable1, "placeholderDrawable");
    k.b(paramDrawable2, "errorDrawable");
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (bool) {
      return;
    }
    localObject = w.a(a);
    paramString = ((w)localObject).a(paramString).b(paramDrawable2).a(paramDrawable1);
    if (!paramBoolean)
    {
      paramString.a(paramImageView, null);
      return;
    }
    paramDrawable1 = new com/truecaller/truepay/app/utils/s$a;
    paramDrawable1.<init>(this, paramImageView);
    paramImageView.setTag(paramDrawable1);
    paramDrawable1 = (ag)paramDrawable1;
    paramString.a(paramDrawable1);
  }
  
  public final int a(String paramString)
  {
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    if (localObject != null)
    {
      i = ((CharSequence)localObject).length();
      if (i != 0)
      {
        i = 0;
        localObject = null;
        break label34;
      }
    }
    int i = 1;
    label34:
    if (i != 0) {
      return R.drawable.ic_no_sim;
    }
    if (paramString != null)
    {
      paramString = paramString.toLowerCase();
      k.a(paramString, "(this as java.lang.String).toLowerCase()");
      localObject = "airtel";
      boolean bool1 = m.b(paramString, (String)localObject, false);
      if (bool1) {
        return R.drawable.ic_airtel;
      }
      localObject = "aircel";
      bool1 = m.b(paramString, (String)localObject, false);
      if (bool1) {
        return R.drawable.ic_aircel;
      }
      localObject = "vodafone";
      bool1 = m.b(paramString, (String)localObject, false);
      if (bool1) {
        return R.drawable.ic_vodafone;
      }
      localObject = "mtnl";
      bool1 = m.b(paramString, (String)localObject, false);
      if (bool1) {
        return R.drawable.ic_mtnl;
      }
      localObject = "idea";
      bool1 = m.b(paramString, (String)localObject, false);
      if (bool1) {
        return R.drawable.ic_idea;
      }
      localObject = "jio";
      bool1 = m.b(paramString, (String)localObject, false);
      if (bool1) {
        return R.drawable.ic_jio;
      }
      localObject = "reliance";
      bool1 = m.b(paramString, (String)localObject, false);
      if (bool1) {
        return R.drawable.ic_reliance;
      }
      localObject = "bsnl";
      bool1 = m.b(paramString, (String)localObject, false);
      if (!bool1)
      {
        localObject = "cellone";
        bool1 = m.b(paramString, (String)localObject, false);
        if (!bool1)
        {
          localObject = "tata";
          bool1 = m.b(paramString, (String)localObject, false);
          if (bool1) {
            return R.drawable.ic_tataindicom;
          }
          localObject = "t24";
          boolean bool2 = m.b(paramString, (String)localObject, false);
          if (bool2) {
            return R.drawable.ic_t24;
          }
          return R.drawable.ic_no_sim;
        }
      }
      return R.drawable.ic_bsnl;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type java.lang.String");
    throw paramString;
  }
  
  public final void a(String paramString, ImageView paramImageView, int paramInt1, int paramInt2)
  {
    k.b(paramImageView, "targetImageView");
    a(paramString, paramImageView, paramInt1, paramInt2, false);
  }
  
  public final void a(String paramString, ImageView paramImageView, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    k.b(paramImageView, "targetImageView");
    Drawable localDrawable1 = a.getResources().getDrawable(paramInt1);
    k.a(localDrawable1, "context.resources.getDrawable(placeholderDrawable)");
    Drawable localDrawable2 = a.getResources().getDrawable(paramInt2);
    k.a(localDrawable2, "context.resources.getDrawable(errorDrawable)");
    a(paramString, paramImageView, localDrawable1, localDrawable2, paramBoolean);
  }
  
  public final void a(String paramString, ImageView paramImageView, Drawable paramDrawable1, Drawable paramDrawable2)
  {
    k.b(paramImageView, "targetImageView");
    k.b(paramDrawable1, "placeholderDrawable");
    k.b(paramDrawable2, "errorDrawable");
    a(paramString, paramImageView, paramDrawable1, paramDrawable2, false);
  }
  
  public final Drawable b(String paramString)
  {
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    if (bool) {
      paramString = "";
    }
    localObject1 = new java/lang/StringBuilder;
    String str = "ic_";
    ((StringBuilder)localObject1).<init>(str);
    int j;
    if (paramString != null)
    {
      if (paramString != null)
      {
        paramString = paramString.toLowerCase();
        str = "(this as java.lang.String).toLowerCase()";
        k.a(paramString, str);
      }
      else
      {
        paramString = new c/u;
        paramString.<init>("null cannot be cast to non-null type java.lang.String");
        throw paramString;
      }
    }
    else
    {
      j = 0;
      paramString = null;
    }
    ((StringBuilder)localObject1).append(paramString);
    ((StringBuilder)localObject1).append("_square");
    paramString = ((StringBuilder)localObject1).toString();
    try
    {
      localObject1 = a;
      localObject1 = ((Context)localObject1).getResources();
      str = "drawable";
      Object localObject2 = a;
      localObject2 = ((Context)localObject2).getPackageName();
      j = ((Resources)localObject1).getIdentifier(paramString, str, (String)localObject2);
      if (j != 0)
      {
        localObject1 = a;
        localObject1 = ((Context)localObject1).getResources();
        paramString = ((Resources)localObject1).getDrawable(j);
        localObject1 = "context.resources.getDrawable(identifier)";
        k.a(paramString, (String)localObject1);
      }
      else
      {
        paramString = a;
        paramString = paramString.getResources();
        i = R.drawable.ic_bank_icon;
        paramString = paramString.getDrawable(i);
        localObject1 = "context.resources.getDra…(R.drawable.ic_bank_icon)";
        k.a(paramString, (String)localObject1);
      }
    }
    catch (Exception localException)
    {
      paramString = a.getResources();
      int i = R.drawable.ic_bank_icon;
      paramString = paramString.getDrawable(i);
      localObject1 = "context.resources.getDra…(R.drawable.ic_bank_icon)";
      k.a(paramString, (String)localObject1);
    }
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */