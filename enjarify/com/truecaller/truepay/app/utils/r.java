package com.truecaller.truepay.app.utils;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public abstract interface r
{
  public abstract int a(String paramString);
  
  public abstract void a(String paramString, ImageView paramImageView, int paramInt1, int paramInt2);
  
  public abstract void a(String paramString, ImageView paramImageView, int paramInt1, int paramInt2, boolean paramBoolean);
  
  public abstract void a(String paramString, ImageView paramImageView, Drawable paramDrawable1, Drawable paramDrawable2);
  
  public abstract Drawable b(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */