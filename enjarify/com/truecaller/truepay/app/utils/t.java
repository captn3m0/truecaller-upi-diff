package com.truecaller.truepay.app.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.truecaller.truepay.R.drawable;

public final class t
{
  public static Drawable a(String paramString, Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      paramString = "";
    }
    Object localObject = new java/lang/StringBuilder;
    String str1 = "ic_";
    ((StringBuilder)localObject).<init>(str1);
    paramString = paramString.toLowerCase();
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("_square");
    paramString = ((StringBuilder)localObject).toString();
    try
    {
      localObject = paramContext.getResources();
      str1 = "drawable";
      String str2 = paramContext.getPackageName();
      int j = ((Resources)localObject).getIdentifier(paramString, str1, str2);
      if (j != 0)
      {
        localObject = paramContext.getResources();
        paramString = ((Resources)localObject).getDrawable(j);
      }
      else
      {
        paramString = paramContext.getResources();
        int i = R.drawable.ic_bank_icon;
        paramString = paramString.getDrawable(i);
      }
    }
    catch (Exception localException)
    {
      paramString = paramContext.getResources();
      int k = R.drawable.ic_bank_icon;
      paramString = paramString.getDrawable(k);
    }
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */