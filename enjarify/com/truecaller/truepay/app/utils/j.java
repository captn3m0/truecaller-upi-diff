package com.truecaller.truepay.app.utils;

import android.content.Context;
import android.content.res.Resources;
import com.truecaller.truepay.R.plurals;
import com.truecaller.truepay.R.string;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public final class j
{
  protected static java.text.DateFormat a = null;
  protected static java.text.DateFormat b = null;
  private static final long c;
  private static final long d;
  private static final long e;
  private static final long f;
  private static final StringBuilder g;
  private static final Formatter h;
  private static final SimpleDateFormat i;
  private static final SimpleDateFormat j;
  private static final SimpleDateFormat k;
  private static final SimpleDateFormat l;
  private static final SimpleDateFormat m;
  
  static
  {
    Object localObject = TimeUnit.DAYS;
    long l1 = 1L;
    c = ((TimeUnit)localObject).toMillis(l1);
    d = TimeUnit.DAYS.toHours(l1);
    e = TimeUnit.MINUTES.toSeconds(l1);
    f = TimeUnit.HOURS.toSeconds(l1);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>(32);
    g = (StringBuilder)localObject;
    localObject = new java/util/Formatter;
    StringBuilder localStringBuilder = g;
    Locale localLocale = Locale.ENGLISH;
    ((Formatter)localObject).<init>(localStringBuilder, localLocale);
    h = (Formatter)localObject;
    localObject = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject).<init>("yyyy-MM-dd");
    i = (SimpleDateFormat)localObject;
    localObject = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject).<init>("HH:mm");
    j = (SimpleDateFormat)localObject;
    localObject = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject).<init>("dd/MM");
    k = (SimpleDateFormat)localObject;
    localObject = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject).<init>("MM/dd");
    l = (SimpleDateFormat)localObject;
    localObject = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject).<init>("yyyy-MM-dd HH:mm:ss Z");
    m = (SimpleDateFormat)localObject;
  }
  
  public static long a(String paramString1, String paramString2)
  {
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    localSimpleDateFormat.<init>(paramString2);
    try
    {
      paramString1 = localSimpleDateFormat.parse(paramString1);
      return paramString1.getTime();
    }
    catch (ParseException localParseException) {}
    return 0L;
  }
  
  public static String a(Context paramContext, long paramLong)
  {
    Object localObject1 = Calendar.getInstance(Locale.ENGLISH);
    ((Calendar)localObject1).setTimeInMillis(paramLong);
    long l1 = System.currentTimeMillis();
    long l2 = 1000L;
    boolean bool1 = l1 < paramLong;
    long l3;
    if (bool1) {
      l3 = (l1 - paramLong) / l2;
    } else {
      l3 = (paramLong - l1) / l2;
    }
    int i1 = 2;
    long l4 = 1L;
    int i2 = 6;
    int i3 = 1;
    boolean bool2 = l1 < paramLong;
    Object localObject2;
    int i5;
    int i6;
    Object localObject3;
    Object localObject4;
    int i7;
    int n;
    int i8;
    if (!bool2)
    {
      localObject2 = TimeUnit.MINUTES;
      paramLong = ((TimeUnit)localObject2).toSeconds(l4);
      boolean bool3 = l3 < paramLong;
      if (bool3)
      {
        paramContext = paramContext.getResources();
        i5 = R.string.now;
        paramContext = paramContext.getString(i5);
      }
      else
      {
        localObject2 = TimeUnit.MINUTES;
        l1 = 10;
        paramLong = ((TimeUnit)localObject2).toSeconds(l1);
        bool3 = l3 < paramLong;
        if (bool3)
        {
          paramLong = TimeUnit.SECONDS.toMinutes(l3);
          paramContext = paramContext.getResources();
          i6 = R.string.n_minutes_ago;
          localObject3 = new Object[i3];
          localObject2 = Long.valueOf(paramLong);
          localObject3[0] = localObject2;
          paramContext = paramContext.getString(i6, (Object[])localObject3);
        }
        else
        {
          localObject2 = Calendar.getInstance(Locale.ENGLISH);
          localObject4 = Calendar.getInstance(Locale.ENGLISH);
          int i4 = -1;
          ((Calendar)localObject4).add(i2, i4);
          localObject3 = Calendar.getInstance(Locale.ENGLISH);
          ((Calendar)localObject3).add(i2, -7);
          i7 = ((Calendar)localObject1).get(i3);
          n = ((Calendar)localObject2).get(i3);
          if (i7 == n)
          {
            i7 = ((Calendar)localObject1).get(i2);
            n = ((Calendar)localObject2).get(i2);
            if (i7 == n)
            {
              localObject2 = a(paramContext);
              localObject4 = ((Calendar)localObject1).getTime();
              localObject2 = ((java.text.DateFormat)localObject2).format((Date)localObject4);
              paramContext = paramContext.getResources();
              i8 = R.string.today_at;
              localObject1 = new Object[i3];
              localObject1[0] = localObject2;
              return paramContext.getString(i8, (Object[])localObject1);
            }
          }
          i7 = ((Calendar)localObject1).get(i2);
          i8 = ((Calendar)localObject4).get(i2);
          if (i7 == i8)
          {
            paramContext = paramContext.getResources();
            i5 = R.string.pay_date_time_yesterday;
            paramContext = paramContext.getString(i5);
          }
          else
          {
            boolean bool5 = ((Calendar)localObject1).after(localObject3);
            if (bool5)
            {
              paramContext = new java/text/SimpleDateFormat;
              localObject4 = Locale.ENGLISH;
              paramContext.<init>("EEEE", (Locale)localObject4);
              localObject2 = ((Calendar)localObject1).getTime();
              paramContext = paramContext.format((Date)localObject2);
            }
            else
            {
              int i9 = ((Calendar)localObject1).get(i3);
              i5 = ((Calendar)localObject2).get(i3);
              if (i9 == i5)
              {
                paramContext = new java/text/SimpleDateFormat;
                localObject4 = Locale.ENGLISH;
                paramContext.<init>("dd MMM", (Locale)localObject4);
                paramContext.setCalendar((Calendar)localObject1);
                localObject2 = ((Calendar)localObject1).getTime();
                paramContext = paramContext.format((Date)localObject2);
              }
              else
              {
                paramContext = Locale.ENGLISH;
                paramContext = java.text.DateFormat.getDateInstance(i1, paramContext);
                paramContext.setCalendar((Calendar)localObject1);
                localObject2 = ((Calendar)localObject1).getTime();
                paramContext = paramContext.format((Date)localObject2);
              }
            }
          }
        }
      }
    }
    else
    {
      localObject2 = TimeUnit.MINUTES;
      paramLong = ((TimeUnit)localObject2).toSeconds(l4);
      boolean bool4 = l3 < paramLong;
      if (bool4)
      {
        paramContext = paramContext.getResources();
        i5 = R.string.now;
        paramContext = paramContext.getString(i5);
      }
      else
      {
        localObject2 = TimeUnit.MINUTES;
        l1 = 59;
        paramLong = ((TimeUnit)localObject2).toSeconds(l1);
        bool4 = l3 < paramLong;
        if (bool4)
        {
          paramLong = TimeUnit.SECONDS.toMinutes(l3);
          paramContext = paramContext.getResources();
          i6 = R.string.in_n_minutes;
          localObject3 = new Object[i3];
          localObject2 = Long.valueOf(paramLong);
          localObject3[0] = localObject2;
          paramContext = paramContext.getString(i6, (Object[])localObject3);
        }
        else
        {
          localObject2 = Calendar.getInstance(Locale.ENGLISH);
          localObject4 = Calendar.getInstance(Locale.ENGLISH);
          ((Calendar)localObject4).add(i2, i3);
          localObject3 = Calendar.getInstance(Locale.ENGLISH);
          ((Calendar)localObject3).add(i2, 7);
          i7 = ((Calendar)localObject1).get(i3);
          n = ((Calendar)localObject2).get(i3);
          if (i7 == n)
          {
            i7 = ((Calendar)localObject1).get(i2);
            n = ((Calendar)localObject2).get(i2);
            if (i7 == n)
            {
              localObject2 = a(paramContext);
              localObject4 = ((Calendar)localObject1).getTime();
              localObject2 = ((java.text.DateFormat)localObject2).format((Date)localObject4);
              paramContext = paramContext.getResources();
              i8 = R.string.today_at;
              localObject1 = new Object[i3];
              localObject1[0] = localObject2;
              return paramContext.getString(i8, (Object[])localObject1);
            }
          }
          i7 = ((Calendar)localObject1).get(i2);
          i8 = ((Calendar)localObject4).get(i2);
          if (i7 == i8)
          {
            paramContext = paramContext.getResources();
            i5 = R.string.pay_date_time_tomorrow;
            paramContext = paramContext.getString(i5);
          }
          else
          {
            boolean bool6 = ((Calendar)localObject1).before(localObject3);
            if (bool6)
            {
              paramContext = new java/text/SimpleDateFormat;
              localObject4 = Locale.ENGLISH;
              paramContext.<init>("EEEE", (Locale)localObject4);
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>("on ");
              localObject4 = ((Calendar)localObject1).getTime();
              paramContext = paramContext.format((Date)localObject4);
              ((StringBuilder)localObject2).append(paramContext);
              paramContext = ((StringBuilder)localObject2).toString();
            }
            else
            {
              int i10 = ((Calendar)localObject1).get(i3);
              i5 = ((Calendar)localObject2).get(i3);
              if (i10 == i5)
              {
                paramContext = new java/text/SimpleDateFormat;
                localObject4 = Locale.ENGLISH;
                paramContext.<init>("dd MMM", (Locale)localObject4);
                paramContext.setCalendar((Calendar)localObject1);
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>("on ");
                localObject4 = ((Calendar)localObject1).getTime();
                paramContext = paramContext.format((Date)localObject4);
                ((StringBuilder)localObject2).append(paramContext);
                paramContext = ((StringBuilder)localObject2).toString();
              }
              else
              {
                paramContext = Locale.ENGLISH;
                paramContext = java.text.DateFormat.getDateInstance(i1, paramContext);
                paramContext.setCalendar((Calendar)localObject1);
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>("on");
                localObject4 = ((Calendar)localObject1).getTime();
                paramContext = paramContext.format((Date)localObject4);
                ((StringBuilder)localObject2).append(paramContext);
                paramContext = ((StringBuilder)localObject2).toString();
              }
            }
          }
        }
      }
    }
    return paramContext;
  }
  
  public static String a(Date paramDate)
  {
    synchronized (j.class)
    {
      SimpleDateFormat localSimpleDateFormat = m;
      paramDate = localSimpleDateFormat.format(paramDate);
      return paramDate;
    }
  }
  
  private static java.text.DateFormat a(Context paramContext)
  {
    paramContext = android.text.format.DateFormat.getTimeFormat(paramContext);
    if (paramContext == null)
    {
      int n = 3;
      Locale localLocale = Locale.ENGLISH;
      paramContext = java.text.DateFormat.getTimeInstance(n, localLocale);
    }
    return paramContext;
  }
  
  public static Date a(String paramString)
  {
    try
    {
      synchronized (j.class)
      {
        SimpleDateFormat localSimpleDateFormat = m;
        paramString = localSimpleDateFormat.parse(paramString);
        return paramString;
      }
    }
    catch (ParseException paramString)
    {
      paramString.printStackTrace();
      return null;
    }
  }
  
  public static CharSequence b(Context paramContext, long paramLong)
  {
    synchronized (j.class)
    {
      long l1 = System.currentTimeMillis();
      Object localObject1 = TimeZone.getDefault();
      int n = ((TimeZone)localObject1).getOffset(l1);
      long l2 = n;
      l1 += l2;
      long l3 = c;
      l1 /= l3;
      l3 = 1000L;
      paramLong = paramLong * l3 + l2;
      l2 = c;
      paramLong /= l2;
      l2 = paramLong - l1;
      l3 = 0L;
      boolean bool = l2 < l3;
      int i1;
      if (bool)
      {
        paramContext = paramContext.getResources();
        i1 = R.string.rewards_time_expired;
        paramContext = paramContext.getString(i1);
        return paramContext;
      }
      int i2 = 1;
      bool = l1 < paramLong;
      Object localObject2;
      if (!bool)
      {
        localObject2 = paramContext.getResources();
        i3 = R.string.rewards_time_today;
        localObject2 = ((Resources)localObject2).getString(i3);
      }
      else
      {
        long l4 = 1L;
        l1 += l4;
        bool = paramLong < l1;
        if (!bool)
        {
          localObject2 = paramContext.getResources();
          i3 = R.string.rewards_time_tomorrow;
          localObject2 = ((Resources)localObject2).getString(i3);
        }
        else
        {
          i1 = (int)l2;
          Resources localResources = paramContext.getResources();
          int i4 = R.plurals.rewards_time_days;
          Object[] arrayOfObject1 = new Object[i2];
          localObject1 = Integer.valueOf(i1);
          arrayOfObject1[0] = localObject1;
          localObject2 = localResources.getQuantityString(i4, i1, arrayOfObject1);
        }
      }
      paramContext = paramContext.getResources();
      int i3 = R.string.rewards_time_expiry;
      Object[] arrayOfObject2 = new Object[i2];
      arrayOfObject2[0] = localObject2;
      paramContext = paramContext.getString(i3, arrayOfObject2);
      return paramContext;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */