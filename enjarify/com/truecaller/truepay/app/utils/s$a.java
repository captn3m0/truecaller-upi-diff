package com.truecaller.truepay.app.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.b;
import android.util.TypedValue;
import android.widget.ImageView;
import c.g.b.k;
import com.d.b.ag;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.dimen;
import com.truecaller.truepay.app.ui.payments.views.customviews.a;
import com.truecaller.truepay.app.ui.payments.views.customviews.a.a;

public final class s$a
  implements ag
{
  s$a(s params, ImageView paramImageView) {}
  
  public final void a(Bitmap paramBitmap)
  {
    if (paramBitmap == null) {
      return;
    }
    Object localObject1 = new com/truecaller/truepay/app/ui/payments/views/customviews/a$a;
    Object localObject2 = a.a;
    ((a.a)localObject1).<init>((Context)localObject2);
    localObject2 = "bitmap";
    k.b(paramBitmap, (String)localObject2);
    localObject1 = (a.a)localObject1;
    f = paramBitmap;
    paramBitmap = f;
    if (paramBitmap != null)
    {
      paramBitmap = e;
      int i;
      float f1;
      if (paramBitmap == null)
      {
        i = R.dimen.default_image_to_badge_ratio;
        localObject2 = localObject1;
        localObject2 = (a.a)localObject1;
        localObject3 = new android/util/TypedValue;
        ((TypedValue)localObject3).<init>();
        localObject4 = i.getResources();
        boolean bool = true;
        ((Resources)localObject4).getValue(i, (TypedValue)localObject3, bool);
        f1 = ((TypedValue)localObject3).getFloat();
        paramBitmap = Float.valueOf(f1);
        e = paramBitmap;
      }
      paramBitmap = a;
      if (paramBitmap == null)
      {
        i = R.color.default_badge_text_color;
        localObject2 = localObject1;
        localObject2 = (a.a)localObject1;
        localObject3 = i;
        i = b.c((Context)localObject3, i);
        paramBitmap = Integer.valueOf(i);
        a = paramBitmap;
      }
      paramBitmap = b;
      if (paramBitmap == null)
      {
        i = R.color.default_badge_color;
        localObject2 = localObject1;
        localObject2 = (a.a)localObject1;
        i = b.c(i, i);
        localObject3 = h;
        ((Paint)localObject3).setColor(i);
        paramBitmap = Integer.valueOf(i);
        b = paramBitmap;
      }
      paramBitmap = c;
      if (paramBitmap == null)
      {
        i = R.color.default_badge_border_color;
        localObject2 = localObject1;
        localObject2 = (a.a)localObject1;
        localObject3 = i;
        i = b.c((Context)localObject3, i);
        paramBitmap = Integer.valueOf(i);
        c = paramBitmap;
      }
      paramBitmap = d;
      if (paramBitmap == null)
      {
        i = R.dimen.default_badge_border_size;
        localObject2 = localObject1;
        localObject2 = (a.a)localObject1;
        localObject3 = i.getResources();
        i = ((Resources)localObject3).getDimensionPixelOffset(i);
        f1 = i;
        paramBitmap = Float.valueOf(f1);
        d = paramBitmap;
      }
      paramBitmap = g;
      if (paramBitmap == null)
      {
        paramBitmap = (Bitmap)localObject1;
        paramBitmap = (a.a)localObject1;
        localObject2 = Boolean.TRUE;
        g = ((Boolean)localObject2);
      }
      paramBitmap = new com/truecaller/truepay/app/ui/payments/views/customviews/a;
      Object localObject4 = i;
      Object localObject5 = f;
      Object localObject6 = a;
      Object localObject7 = e;
      Paint localPaint = h;
      Object localObject3 = paramBitmap;
      paramBitmap.<init>((Context)localObject4, (Integer)localObject6, (Float)localObject7, (Bitmap)localObject5, localPaint, (byte)0);
      localObject1 = a.getResources();
      localObject2 = c;
      if (localObject2 != null)
      {
        int j = ((Bitmap)localObject2).getWidth();
        int k = ((Bitmap)localObject2).getHeight();
        localObject6 = Bitmap.Config.ARGB_8888;
        localObject6 = Bitmap.createBitmap(j, k, (Bitmap.Config)localObject6);
        localObject7 = new android/graphics/Canvas;
        ((Canvas)localObject7).<init>((Bitmap)localObject6);
        localObject5 = new android/graphics/Rect;
        float f2 = 0.0F;
        ((Rect)localObject5).<init>(0, 0, j, k);
        localPaint = d;
        ((Canvas)localObject7).drawBitmap((Bitmap)localObject2, (Rect)localObject5, (Rect)localObject5, localPaint);
        localObject2 = b;
        if (localObject2 != null)
        {
          float f3 = j;
          float f4 = k;
          float f5 = b.floatValue();
          f5 = f3 / f5;
          localObject5 = new android/graphics/RectF;
          f2 = f3 - f5;
          f5 = f4 - f5;
          ((RectF)localObject5).<init>(f2, f5, f3, f4);
          paramBitmap = d;
          ((Canvas)localObject7).drawOval((RectF)localObject5, paramBitmap);
          paramBitmap = new android/graphics/drawable/BitmapDrawable;
          paramBitmap.<init>((Resources)localObject1, (Bitmap)localObject6);
          paramBitmap = (Drawable)paramBitmap;
          b.setImageDrawable(paramBitmap);
          return;
        }
        paramBitmap = new java/lang/IllegalArgumentException;
        paramBitmap.<init>("Badge size cannot be null");
        throw ((Throwable)paramBitmap);
      }
      paramBitmap = new java/lang/IllegalArgumentException;
      paramBitmap.<init>("Badge drawable/bitmap can not be null.");
      throw ((Throwable)paramBitmap);
    }
    paramBitmap = new java/lang/IllegalArgumentException;
    paramBitmap.<init>("Badge drawable/bitmap can not be null.");
    throw ((Throwable)paramBitmap);
  }
  
  public final void a(Drawable paramDrawable)
  {
    b.setImageDrawable(paramDrawable);
  }
  
  public final void b(Drawable paramDrawable)
  {
    b.setImageDrawable(paramDrawable);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.s.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */