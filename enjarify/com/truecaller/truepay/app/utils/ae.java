package com.truecaller.truepay.app.utils;

import c.g.b.k;
import c.n.m;

public final class ae
{
  public static final String a(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    boolean bool;
    if (parama != null)
    {
      parama = parama.j();
    }
    else
    {
      bool = false;
      parama = null;
    }
    if (parama != null)
    {
      int i = parama.hashCode();
      String str;
      switch (i)
      {
      default: 
        break;
      case 1789494714: 
        str = "datacard";
        bool = parama.equals(str);
        if (bool) {
          return "recent_datacard_unique_3";
        }
        break;
      case 958132849: 
        str = "electricity";
        bool = parama.equals(str);
        if (bool) {
          return "recent_electricity_unique_3";
        }
        break;
      case 757836652: 
        str = "postpaid";
        bool = parama.equals(str);
        if (bool) {
          return "recent_postpaid_unique_3";
        }
        break;
      case 112903447: 
        str = "water";
        bool = parama.equals(str);
        if (bool) {
          return "recent_water_unique";
        }
        break;
      case 102105: 
        str = "gas";
        bool = parama.equals(str);
        if (bool) {
          return "recent_gas_unique";
        }
        break;
      case 99800: 
        str = "dth";
        bool = parama.equals(str);
        if (bool) {
          return "recent_dth_unique";
        }
        break;
      case -318370833: 
        str = "prepaid";
        bool = parama.equals(str);
        if (bool) {
          return "recent_payments_unique_5";
        }
        break;
      case -1616620449: 
        str = "landline";
        bool = parama.equals(str);
        if (bool) {
          return "recent_landline_unique";
        }
        break;
      case -1618906185: 
        str = "broadband";
        bool = parama.equals(str);
        if (bool) {
          return "recent_broadband_unique";
        }
        break;
      }
    }
    return null;
  }
  
  public static final String a(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "receiver$0");
    String str1 = "XXXX";
    Object localObject = parama.d();
    String str2 = "accountNumber";
    k.a(localObject, str2);
    localObject = (CharSequence)localObject;
    int i = ((CharSequence)localObject).length();
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    char c = ' ';
    if (i != 0)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(str1);
      ((StringBuilder)localObject).append(c);
      ((StringBuilder)localObject).append(str1);
      str1 = ((StringBuilder)localObject).toString();
    }
    else
    {
      localObject = parama.d();
      i = ((String)localObject).length();
      int j = 4;
      if (i < j)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(str1);
        ((StringBuilder)localObject).append(c);
        str1 = parama.d();
        ((StringBuilder)localObject).append(str1);
        str1 = ((StringBuilder)localObject).toString();
      }
      else
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(str1);
        ((StringBuilder)localObject).append(c);
        str1 = parama.d();
        str2 = "accountNumber";
        k.a(str1, str2);
        str1 = m.b(str1, j);
        ((StringBuilder)localObject).append(str1);
        str1 = ((StringBuilder)localObject).toString();
      }
    }
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    parama = parama.l();
    k.a(parama, "bank");
    parama = parama.b();
    ((StringBuilder)localObject).append(parama);
    ((StringBuilder)localObject).append(" - ");
    ((StringBuilder)localObject).append(str1);
    return ((StringBuilder)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */