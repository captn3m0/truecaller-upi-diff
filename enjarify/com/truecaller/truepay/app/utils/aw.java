package com.truecaller.truepay.app.utils;

import c.g.b.k;
import com.truecaller.truepay.data.e.c;

public final class aw
  implements av
{
  private final c a;
  
  public aw(c paramc)
  {
    a = paramc;
  }
  
  public final void a()
  {
    c localc = a;
    Boolean localBoolean = Boolean.TRUE;
    localc.a("check_balance", localBoolean);
  }
  
  public final boolean b()
  {
    Boolean localBoolean = a.e("check_balance");
    k.a(localBoolean, "preference.getBool(CHECK_BALANCE)");
    return localBoolean.booleanValue();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.aw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */