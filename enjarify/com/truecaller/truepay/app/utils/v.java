package com.truecaller.truepay.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.d;
import c.g.b.k;

public final class v
  implements u
{
  v.a a;
  private final v.b b;
  private final Context c;
  
  public v(Context paramContext)
  {
    c = paramContext;
    paramContext = new com/truecaller/truepay/app/utils/v$b;
    paramContext.<init>(this);
    b = paramContext;
  }
  
  public final void a()
  {
    a = null;
    d locald = d.a(c);
    BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)b;
    locald.a(localBroadcastReceiver);
  }
  
  public final void a(v.a parama)
  {
    k.b(parama, "observer");
    a();
    a = parama;
    parama = d.a(c);
    BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)b;
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>("INSTANT_REWARD_RECEIVED");
    parama.a(localBroadcastReceiver, localIntentFilter);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */