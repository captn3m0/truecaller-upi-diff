package com.truecaller.truepay.app.utils;

import dagger.a.d;
import javax.inject.Provider;

public final class ad
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private ad(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static ad a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    ad localad = new com/truecaller/truepay/app/utils/ad;
    localad.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localad;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */