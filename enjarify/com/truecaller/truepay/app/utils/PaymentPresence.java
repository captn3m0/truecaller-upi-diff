package com.truecaller.truepay.app.utils;

public final class PaymentPresence
{
  private final boolean isEnabled;
  private final int lastTxnTs;
  private final int version;
  
  public PaymentPresence(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    isEnabled = paramBoolean;
    version = paramInt1;
    lastTxnTs = paramInt2;
  }
  
  public final boolean component1()
  {
    return isEnabled;
  }
  
  public final int component2()
  {
    return version;
  }
  
  public final int component3()
  {
    return lastTxnTs;
  }
  
  public final PaymentPresence copy(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    PaymentPresence localPaymentPresence = new com/truecaller/truepay/app/utils/PaymentPresence;
    localPaymentPresence.<init>(paramBoolean, paramInt1, paramInt2);
    return localPaymentPresence;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof PaymentPresence;
      if (bool2)
      {
        paramObject = (PaymentPresence)paramObject;
        bool2 = isEnabled;
        boolean bool3 = isEnabled;
        if (bool2 == bool3) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        if (bool2)
        {
          int i = version;
          int j = version;
          if (i == j) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0)
          {
            i = lastTxnTs;
            int k = lastTxnTs;
            if (i == k)
            {
              k = 1;
            }
            else
            {
              k = 0;
              paramObject = null;
            }
            if (k != 0) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int getLastTxnTs()
  {
    return lastTxnTs;
  }
  
  public final int getVersion()
  {
    return version;
  }
  
  public final int hashCode()
  {
    boolean bool = isEnabled;
    if (bool) {
      bool = true;
    }
    bool *= true;
    int j = version;
    int i = (i + j) * 31;
    j = lastTxnTs;
    return i + j;
  }
  
  public final boolean isEnabled()
  {
    return isEnabled;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PaymentPresence(isEnabled=");
    boolean bool = isEnabled;
    localStringBuilder.append(bool);
    localStringBuilder.append(", version=");
    int i = version;
    localStringBuilder.append(i);
    localStringBuilder.append(", lastTxnTs=");
    i = lastTxnTs;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.PaymentPresence
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */