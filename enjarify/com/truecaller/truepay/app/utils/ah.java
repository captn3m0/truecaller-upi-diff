package com.truecaller.truepay.app.utils;

import c.g.b.k;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.data.e.b;

public final class ah
  implements ag
{
  private final b a;
  
  public ah(b paramb)
  {
    a = paramb;
  }
  
  public final PaymentPresence a()
  {
    Object localObject = a;
    boolean bool1 = ((b)localObject).b();
    int i;
    if (bool1)
    {
      localObject = a.a();
      long l1 = ((Long)localObject).longValue();
      long l2 = 1000L;
      l1 /= l2;
      i = (int)l1;
    }
    else
    {
      i = 0;
    }
    localObject = new com/truecaller/truepay/app/utils/PaymentPresence;
    Truepay localTruepay = Truepay.getInstance();
    k.a(localTruepay, "Truepay.getInstance()");
    boolean bool2 = localTruepay.isRegistrationComplete();
    ((PaymentPresence)localObject).<init>(bool2, 1, i);
    return (PaymentPresence)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */