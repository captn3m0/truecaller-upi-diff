package com.truecaller.truepay.app.utils;

import android.content.Context;
import android.support.v4.content.b;

public final class h
  implements g
{
  private final Context a;
  
  public h(Context paramContext)
  {
    a = paramContext;
  }
  
  public final int a(int paramInt)
  {
    return b.c(a, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */