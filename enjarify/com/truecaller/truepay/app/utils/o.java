package com.truecaller.truepay.app.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo.Builder;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.Icon;
import android.os.Build.VERSION;
import com.truecaller.log.d;
import java.util.ArrayList;
import java.util.List;

public final class o
  implements n
{
  private ShortcutManager a;
  private Context b;
  private List c;
  
  public o(Context paramContext)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    c = localArrayList;
    b = paramContext;
  }
  
  public final n a(String paramString, int paramInt1, int paramInt2, Intent[] paramArrayOfIntent)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 25;
    if (i < j) {
      return this;
    }
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = b;
      localObject2 = ShortcutManager.class;
      localObject1 = (ShortcutManager)((Context)localObject1).getSystemService((Class)localObject2);
      a = ((ShortcutManager)localObject1);
    }
    localObject1 = paramString.replaceAll("\\s+", "");
    Object localObject2 = new android/content/pm/ShortcutInfo$Builder;
    Context localContext = b;
    ((ShortcutInfo.Builder)localObject2).<init>(localContext, (String)localObject1);
    paramString = ((ShortcutInfo.Builder)localObject2).setShortLabel(paramString);
    Icon localIcon = Icon.createWithResource(b, paramInt1);
    paramString = paramString.setIcon(localIcon).setRank(paramInt2).setIntents(paramArrayOfIntent).build();
    c.add(paramString);
    return this;
  }
  
  public final void a()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 25;
    if (i < j) {
      return;
    }
    Object localObject = a;
    if (localObject == null)
    {
      localObject = b;
      Class localClass = ShortcutManager.class;
      localObject = (ShortcutManager)((Context)localObject).getSystemService(localClass);
      a = ((ShortcutManager)localObject);
    }
    try
    {
      localObject = a;
      ((ShortcutManager)localObject).removeAllDynamicShortcuts();
      return;
    }
    catch (NullPointerException localNullPointerException) {}catch (IllegalStateException localIllegalStateException) {}
    d.a(localIllegalStateException);
  }
  
  public final void b()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 25;
    if (i < j) {
      return;
    }
    Object localObject1 = a;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = b;
      localObject2 = ShortcutManager.class;
      localObject1 = (ShortcutManager)((Context)localObject1).getSystemService((Class)localObject2);
      a = ((ShortcutManager)localObject1);
    }
    try
    {
      localObject1 = a;
      localObject2 = c;
      ((ShortcutManager)localObject1).addDynamicShortcuts((List)localObject2);
      return;
    }
    catch (NullPointerException localNullPointerException) {}catch (IllegalStateException localIllegalStateException) {}
    d.a(localIllegalStateException);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */