package com.truecaller.truepay.app.utils;

import c.d.a.a;
import c.o.b;
import com.truecaller.truepay.data.api.model.PromoContext;
import e.d;
import java.util.Collection;
import java.util.List;
import kotlinx.coroutines.j;

public final class am
  implements al
{
  private final com.truecaller.truepay.app.ui.growth.db.b a;
  private final com.truecaller.truepay.data.e.b b;
  private final com.truecaller.truepay.data.api.b c;
  private final com.truecaller.truepay.data.api.c d;
  private final com.truecaller.truepay.data.e.b e;
  
  public am(com.truecaller.truepay.app.ui.growth.db.b paramb, com.truecaller.truepay.data.e.b paramb1, com.truecaller.truepay.data.api.b paramb2, com.truecaller.truepay.data.api.c paramc, com.truecaller.truepay.data.e.b paramb3)
  {
    a = paramb;
    b = paramb1;
    c = paramb2;
    d = paramc;
    e = paramb3;
  }
  
  private static boolean a(com.truecaller.truepay.data.e.b paramb)
  {
    long l1 = System.currentTimeMillis();
    paramb = paramb.a();
    String str = "preference.get()";
    c.g.b.k.a(paramb, str);
    long l2 = paramb.longValue();
    l1 = (l1 - l2) / 3600000L;
    l2 = 6;
    boolean bool = l1 < l2;
    return !bool;
  }
  
  public final Object a(c.d.c paramc)
  {
    boolean bool1 = paramc instanceof am.a;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (am.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int m = b - j;
        b = m;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/truepay/app/utils/am$a;
    ((am.a)localObject1).<init>(this, (c.d.c)paramc);
    label70:
    paramc = a;
    Object localObject2 = a.a;
    int j = b;
    int n = 1;
    boolean bool4;
    Object localObject3;
    switch (j)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 1: 
      localObject1 = (am)d;
      bool2 = paramc instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool3 = paramc instanceof o.b;
      if (bool3) {
        break label438;
      }
      paramc = a;
      long l1 = System.currentTimeMillis();
      long l2 = 1000L;
      l1 /= l2;
      int k = (int)l1;
      paramc = paramc.a(k, 0);
      bool4 = paramc.isEmpty();
      if (bool4 != n)
      {
        localObject3 = b;
        bool4 = a((com.truecaller.truepay.data.e.b)localObject3);
        if (!bool4) {
          return paramc;
        }
      }
      d = this;
      e = paramc;
      b = n;
      paramc = new kotlinx/coroutines/k;
      localObject3 = c.d.a.b.a((c.d.c)localObject1);
      paramc.<init>((c.d.c)localObject3, n);
      localObject3 = paramc;
      localObject3 = (j)paramc;
      e.b localb = c.a();
      Object localObject4 = new com/truecaller/truepay/app/utils/am$b;
      ((am.b)localObject4).<init>((j)localObject3);
      localObject4 = (d)localObject4;
      localb.a((d)localObject4);
      paramc = paramc.h();
      localObject3 = a.a;
      if (paramc == localObject3)
      {
        localObject3 = "frame";
        c.g.b.k.b(localObject1, (String)localObject3);
      }
      if (paramc == localObject2) {
        return localObject2;
      }
      localObject1 = this;
    }
    paramc = (List)paramc;
    boolean bool2 = false;
    localObject2 = null;
    if (paramc != null)
    {
      localObject3 = paramc;
      localObject3 = (Collection)paramc;
      bool4 = ((Collection)localObject3).isEmpty() ^ n;
      if (bool4)
      {
        localObject2 = b;
        localObject3 = c.d.b.a.b.a(System.currentTimeMillis());
        ((com.truecaller.truepay.data.e.b)localObject2).a((Long)localObject3);
        a.a(0, paramc);
        return paramc;
      }
      return null;
    }
    return null;
    label438:
    throw a;
  }
  
  public final Object a(String paramString, c.d.c paramc)
  {
    boolean bool1 = paramc instanceof am.c;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (am.c)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int n = b - j;
        b = n;
        break label77;
      }
    }
    Object localObject1 = new com/truecaller/truepay/app/utils/am$c;
    ((am.c)localObject1).<init>(this, (c.d.c)paramc);
    label77:
    paramc = a;
    Object localObject2 = a.a;
    int j = b;
    int i1 = 1;
    switch (j)
    {
    default: 
      paramString = new java/lang/IllegalStateException;
      paramString.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramString;
    case 1: 
      paramString = (am)d;
      bool1 = paramc instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      boolean bool3 = paramc instanceof o.b;
      if (bool3) {
        break label472;
      }
      paramc = a;
      long l1 = System.currentTimeMillis();
      long l2 = 1000L;
      l1 /= l2;
      int k = (int)l1;
      paramc = paramc.a(k, i1);
      int m = paramc.isEmpty();
      if (m != i1)
      {
        localObject3 = e;
        m = a((com.truecaller.truepay.data.e.b)localObject3);
        if (m == 0) {
          return paramc;
        }
      }
      d = this;
      e = paramString;
      f = paramc;
      b = i1;
      paramc = new kotlinx/coroutines/k;
      Object localObject3 = c.d.a.b.a((c.d.c)localObject1);
      paramc.<init>((c.d.c)localObject3, i1);
      localObject3 = paramc;
      localObject3 = (j)paramc;
      Object localObject4 = d;
      PromoContext localPromoContext = new com/truecaller/truepay/data/api/model/PromoContext;
      localPromoContext.<init>(paramString);
      paramString = ((com.truecaller.truepay.data.api.c)localObject4).a(localPromoContext);
      localObject4 = new com/truecaller/truepay/app/utils/am$d;
      ((am.d)localObject4).<init>((j)localObject3);
      localObject4 = (d)localObject4;
      paramString.a((d)localObject4);
      paramc = paramc.h();
      paramString = a.a;
      if (paramc == paramString)
      {
        paramString = "frame";
        c.g.b.k.b(localObject1, paramString);
      }
      if (paramc == localObject2) {
        return localObject2;
      }
      paramString = this;
    }
    paramc = (List)paramc;
    bool1 = false;
    localObject1 = null;
    if (paramc != null)
    {
      localObject2 = paramc;
      localObject2 = (Collection)paramc;
      boolean bool2 = ((Collection)localObject2).isEmpty() ^ i1;
      if (bool2)
      {
        localObject1 = e;
        localObject2 = c.d.b.a.b.a(System.currentTimeMillis());
        ((com.truecaller.truepay.data.e.b)localObject1).a((Long)localObject2);
        a.a(i1, paramc);
        return paramc;
      }
      return null;
    }
    return null;
    label472:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */