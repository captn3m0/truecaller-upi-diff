package com.truecaller.truepay.app.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public final class at
{
  private Context a;
  private List b;
  private List c;
  private List d;
  
  public at(Context paramContext)
  {
    Object localObject = Arrays.asList(new String[] { "su", "magisk" });
    b = ((List)localObject);
    String[] tmp32_29 = new String[14];
    String[] tmp33_32 = tmp32_29;
    String[] tmp33_32 = tmp32_29;
    tmp33_32[0] = "/data/local/";
    tmp33_32[1] = "/data/local/bin/";
    String[] tmp42_33 = tmp33_32;
    String[] tmp42_33 = tmp33_32;
    tmp42_33[2] = "/data/local/xbin/";
    tmp42_33[3] = "/sbin/";
    String[] tmp51_42 = tmp42_33;
    String[] tmp51_42 = tmp42_33;
    tmp51_42[4] = "/su/bin/";
    tmp51_42[5] = "/system/bin/";
    String[] tmp60_51 = tmp51_42;
    String[] tmp60_51 = tmp51_42;
    tmp60_51[6] = "/system/bin/.ext/";
    tmp60_51[7] = "/system/bin/failsafe/";
    String[] tmp71_60 = tmp60_51;
    String[] tmp71_60 = tmp60_51;
    tmp71_60[8] = "/system/sd/xbin/";
    tmp71_60[9] = "/system/usr/we-need-root/";
    String[] tmp82_71 = tmp71_60;
    String[] tmp82_71 = tmp71_60;
    tmp82_71[10] = "/system/xbin/";
    tmp82_71[11] = "/cache";
    tmp82_71[12] = "/data";
    String[] tmp98_82 = tmp82_71;
    tmp98_82[13] = "/dev";
    localObject = Arrays.asList(tmp98_82);
    c = ((List)localObject);
    String[] tmp118_115 = new String[24];
    String[] tmp119_118 = tmp118_115;
    String[] tmp119_118 = tmp118_115;
    tmp119_118[0] = "com.devadvance.rootcloakplus";
    tmp119_118[1] = "com.chelpus.luckypatcher";
    String[] tmp128_119 = tmp119_118;
    String[] tmp128_119 = tmp119_118;
    tmp128_119[2] = "com.koushikdutta.rommanager";
    tmp128_119[3] = "com.noshufou.android.su.elite";
    String[] tmp137_128 = tmp128_119;
    String[] tmp137_128 = tmp128_119;
    tmp137_128[4] = "com.ramdroid.appquarantine";
    tmp137_128[5] = "eu.chainfire.supersu";
    String[] tmp146_137 = tmp137_128;
    String[] tmp146_137 = tmp137_128;
    tmp146_137[6] = "com.devadvance.rootcloak";
    tmp146_137[7] = "com.topjohnwu.magisk";
    String[] tmp157_146 = tmp146_137;
    String[] tmp157_146 = tmp146_137;
    tmp157_146[8] = "com.thirdparty.superuser";
    tmp157_146[9] = "com.formyhm.hiderootPremium";
    String[] tmp168_157 = tmp157_146;
    String[] tmp168_157 = tmp157_146;
    tmp168_157[10] = "de.robv.android.xposed.installer";
    tmp168_157[11] = "com.saurik.substrate";
    String[] tmp179_168 = tmp168_157;
    String[] tmp179_168 = tmp168_157;
    tmp179_168[12] = "com.chelpus.lackypatch";
    tmp179_168[13] = "com.zachspong.temprootremovejb";
    String[] tmp190_179 = tmp179_168;
    String[] tmp190_179 = tmp179_168;
    tmp190_179[14] = "com.dimonvideo.luckypatcher";
    tmp190_179[15] = "com.android.vending.billing.InAppBillingService.COIN";
    String[] tmp201_190 = tmp190_179;
    String[] tmp201_190 = tmp190_179;
    tmp201_190[16] = "com.noshufou.android.su";
    tmp201_190[17] = "com.koushikdutta.superuser";
    String[] tmp212_201 = tmp201_190;
    String[] tmp212_201 = tmp201_190;
    tmp212_201[18] = "com.yellowes.su";
    tmp212_201[19] = "com.koushikdutta.rommanager.license";
    String[] tmp223_212 = tmp212_201;
    String[] tmp223_212 = tmp212_201;
    tmp223_212[20] = "com.amphoras.hidemyroot";
    tmp223_212[21] = "com.formyhm.hideroot";
    tmp223_212[22] = "com.amphoras.hidemyrootadfree";
    String[] tmp239_223 = tmp223_212;
    tmp239_223[23] = "com.ramdroid.appquarantinepro";
    localObject = Arrays.asList(tmp239_223);
    d = ((List)localObject);
    localObject = paramContext;
    a = paramContext;
  }
  
  private boolean a(String paramString)
  {
    try
    {
      Object localObject = a;
      localObject = ((Context)localObject).getPackageManager();
      ((PackageManager)localObject).getPackageInfo(paramString, 0);
      return true;
    }
    catch (Exception localException) {}
    return false;
  }
  
  private boolean b()
  {
    Object localObject = d.iterator();
    int i = 0;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      String str = (String)((Iterator)localObject).next();
      bool1 = a(str);
      if (bool1) {
        i += 1;
      }
    }
    double d1 = i;
    localObject = d;
    double d2 = ((List)localObject).size();
    Double.isNaN(d1);
    Double.isNaN(d2);
    d1 /= d2;
    d2 = 0.3D;
    boolean bool2 = d1 < d2;
    return !bool2;
  }
  
  public final boolean a()
  {
    Iterator localIterator1 = b.iterator();
    int i = 0;
    boolean bool1 = localIterator1.hasNext();
    boolean bool2 = true;
    if (bool1)
    {
      String str1 = (String)localIterator1.next();
      Iterator localIterator2 = c.iterator();
      for (;;)
      {
        boolean bool3 = localIterator2.hasNext();
        if (!bool3) {
          break;
        }
        String str2 = (String)localIterator2.next();
        File localFile = new java/io/File;
        localFile.<init>(str2, str1);
        bool3 = localFile.exists();
        if (bool3) {
          i = 1;
        }
      }
    }
    if (i == 0)
    {
      boolean bool4 = b();
      if (!bool4) {
        return false;
      }
    }
    return bool2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.at
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */