package com.truecaller.truepay.app.utils;

import android.text.TextUtils;

public final class au
{
  public static String a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("XXXX ");
      paramString = c(paramString);
      localStringBuilder.append(paramString);
      return localStringBuilder.toString();
    }
    return paramString;
  }
  
  public static String a(String paramString1, String paramString2)
  {
    paramString2 = a(paramString2);
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString1);
    localStringBuilder.append(" - ");
    localStringBuilder.append(paramString2);
    return localStringBuilder.toString();
  }
  
  public static String a(String paramString, boolean paramBoolean)
  {
    if (paramString == null) {
      return "";
    }
    paramString = paramString.split("\\s+");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = paramString.length;
    int j = 0;
    while (j < i)
    {
      String str1 = paramString[j];
      String str2 = "[^\\w]";
      String str3 = "";
      str1 = str1.replaceAll(str2, str3);
      int k = str1.isEmpty();
      if (k == 0)
      {
        k = 1;
        str3 = str1.substring(0, k).toUpperCase();
        localStringBuilder.append(str3);
        str1 = str1.substring(k).toLowerCase();
        localStringBuilder.append(str1);
        str1 = " ";
        localStringBuilder.append(str1);
      }
      if (paramBoolean) {
        return localStringBuilder.toString();
      }
      j += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static String b(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    if (paramString != null)
    {
      int i = paramString.length();
      if (i > 0)
      {
        paramString = paramString.trim();
        boolean bool = paramString.isEmpty();
        if (!bool)
        {
          String str1 = " ";
          paramString = paramString.split(str1);
          int j = paramString.length;
          int k = 0;
          while (k < j)
          {
            String str2 = paramString[k];
            int m = str2.length();
            if (m > 0)
            {
              m = Character.toUpperCase(str2.charAt(0));
              localStringBuilder.append(m);
              int n = 1;
              str2 = str2.substring(n).toLowerCase();
              localStringBuilder.append(str2);
              str2 = " ";
              localStringBuilder.append(str2);
            }
            k += 1;
          }
        }
      }
    }
    return localStringBuilder.toString().trim();
  }
  
  private static String c(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      int i = paramString.length();
      int j = 4;
      if (i > j)
      {
        i = paramString.length() - j;
        j = paramString.length();
        paramString = paramString.substring(i, j);
      }
      return paramString;
    }
    return "XXXX";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.au
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */