package com.truecaller.truepay.app.utils;

import android.util.Base64;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.UUID;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class p
{
  public static String a()
  {
    long l = System.currentTimeMillis() / 1000L;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(l);
    localStringBuilder.append("-");
    String str = UUID.randomUUID().toString().replaceAll("-", "");
    localStringBuilder.append(str);
    return localStringBuilder.toString();
  }
  
  public static String a(String paramString)
  {
    MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-256");
    paramString = paramString.getBytes();
    int i = paramString.length;
    localMessageDigest.update(paramString, 0, i);
    return Base64.encodeToString(localMessageDigest.digest(), 2);
  }
  
  public static String a(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      paramString1 = b(paramString1);
      Object localObject = "AES/CBC/PKCS7Padding";
      localObject = Cipher.getInstance((String)localObject);
      IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
      paramString3 = paramString3.getBytes();
      localIvParameterSpec.<init>(paramString3);
      int i = 1;
      ((Cipher)localObject).init(i, paramString1, localIvParameterSpec);
      paramString1 = paramString2.getBytes();
      paramString1 = ((Cipher)localObject).doFinal(paramString1);
      int j = 2;
      return Base64.encodeToString(paramString1, j);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      paramString1 = new java/security/GeneralSecurityException;
      paramString1.<init>();
      throw paramString1;
    }
  }
  
  public static byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    localSecretKeySpec.<init>(paramArrayOfByte2, "AES");
    paramArrayOfByte2 = new byte[16];
    IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
    localIvParameterSpec.<init>(paramArrayOfByte2);
    paramArrayOfByte2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
    paramArrayOfByte2.init(1, localSecretKeySpec, localIvParameterSpec);
    return paramArrayOfByte2.doFinal(paramArrayOfByte1);
  }
  
  public static String b()
  {
    String str = new java/lang/String;
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = "eGxFVTRxdn14JztgW1VuUSNlLzUtQD9rZ";
    arrayOfObject[1] = "WJUbHJUPjYkUTIueClPKG10LmRz";
    int i = 2;
    arrayOfObject[i] = "Zip0cy1PKi81PVJ1WGpsZGkq";
    byte[] arrayOfByte = Base64.decode(String.format("%s%s%s", arrayOfObject), i);
    str.<init>(arrayOfByte);
    return str;
  }
  
  public static String b(String paramString1, String paramString2, String paramString3)
  {
    paramString1 = b(paramString1);
    int i = 2;
    paramString2 = Base64.decode(paramString2, i);
    Cipher localCipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
    IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
    paramString3 = paramString3.getBytes();
    localIvParameterSpec.<init>(paramString3);
    localCipher.init(i, paramString1, localIvParameterSpec);
    paramString1 = localCipher.doFinal(paramString2);
    paramString2 = new java/lang/String;
    paramString2.<init>(paramString1);
    return paramString2;
  }
  
  private static SecretKeySpec b(String paramString)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    paramString = paramString.getBytes("UTF-8");
    localSecretKeySpec.<init>(paramString, "AES");
    return localSecretKeySpec;
  }
  
  public static String c()
  {
    String str = new java/lang/String;
    int i = 2;
    Object[] arrayOfObject = new Object[i];
    arrayOfObject[0] = "LmRCW1orLUM0akd3W1ZRRmdtTjUpa3VLfX5NZXtaXyNCd";
    arrayOfObject[1] = "GU0OTlqSFtFKyNtSEM1aDJaZXFodT84Y2wkY2knWA==";
    byte[] arrayOfByte = Base64.decode(String.format("%s%s", arrayOfObject), i);
    str.<init>(arrayOfByte);
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */