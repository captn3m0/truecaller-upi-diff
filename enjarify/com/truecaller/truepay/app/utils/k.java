package com.truecaller.truepay.app.utils;

import android.text.InputFilter;
import android.text.Spanned;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class k
  implements InputFilter
{
  Pattern a;
  double b;
  double c;
  
  public k()
  {
    Pattern localPattern = Pattern.compile("[0-9]{0,5}+((\\.[0-9]{0,1})?)||(\\.)?");
    a = localPattern;
    b = 100000.0D;
    c = 1.0D;
  }
  
  public final CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
  {
    try
    {
      Object localObject = a;
      localObject = ((Pattern)localObject).matcher(paramSpanned);
      paramInt1 = ((Matcher)localObject).matches();
      if (paramInt1 == 0) {
        return "";
      }
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      String str = paramSpanned.toString();
      ((StringBuilder)localObject).append(str);
      paramCharSequence = paramCharSequence.toString();
      ((StringBuilder)localObject).append(paramCharSequence);
      paramCharSequence = ((StringBuilder)localObject).toString();
      float f = Float.parseFloat(paramCharSequence);
      double d1 = f;
      double d2 = c;
      double d3 = b;
      int i = 1;
      boolean bool = d3 < d2;
      if (bool)
      {
        bool = d1 < d2;
        if (!bool)
        {
          paramInt2 = d1 < d3;
          if (paramInt2 <= 0) {
            break label166;
          }
        }
      }
      else
      {
        bool = d1 < d3;
        if (!bool)
        {
          paramInt3 = d1 < d2;
          if (paramInt3 <= 0) {
            break label166;
          }
        }
      }
      i = 0;
      label166:
      if (i == 0) {
        return "";
      }
      return null;
    }
    catch (Exception localException) {}
    return "";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */