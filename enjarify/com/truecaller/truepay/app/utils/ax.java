package com.truecaller.truepay.app.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.AssetManager;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.widget.EditText;
import android.widget.Toast;
import com.google.c.a.g;
import com.google.c.a.k;
import com.google.c.a.k.c;
import com.google.c.a.m.a;
import com.google.gson.f;
import com.truecaller.truepay.SenderInfo;
import com.truecaller.truepay.data.c.b;
import io.reactivex.o;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ax
{
  private final Context a;
  
  public ax(Context paramContext)
  {
    a = paramContext;
  }
  
  public static TextWatcher a(EditText paramEditText, String paramString)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    ((StringBuilder)localObject).append(paramString);
    Editable localEditable = paramEditText.getText();
    ((StringBuilder)localObject).append(localEditable);
    localObject = ((StringBuilder)localObject).toString();
    paramEditText.setText((CharSequence)localObject);
    localObject = paramEditText.getText();
    int i = paramEditText.getText().length();
    Selection.setSelection((Spannable)localObject, i);
    localObject = new com/truecaller/truepay/app/utils/ax$1;
    ((ax.1)localObject).<init>(paramString, paramEditText);
    paramEditText.addTextChangedListener((TextWatcher)localObject);
    return (TextWatcher)localObject;
  }
  
  /* Error */
  private static String a(InputStream paramInputStream)
  {
    // Byte code:
    //   0: new 14	java/lang/StringBuilder
    //   3: astore_1
    //   4: aload_1
    //   5: invokespecial 15	java/lang/StringBuilder:<init>	()V
    //   8: aconst_null
    //   9: astore_2
    //   10: new 59	java/util/zip/GZIPInputStream
    //   13: astore_3
    //   14: aload_3
    //   15: aload_0
    //   16: invokespecial 62	java/util/zip/GZIPInputStream:<init>	(Ljava/io/InputStream;)V
    //   19: new 64	java/io/InputStreamReader
    //   22: astore_0
    //   23: aload_0
    //   24: aload_3
    //   25: invokespecial 65	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   28: new 67	java/io/BufferedReader
    //   31: astore 4
    //   33: aload 4
    //   35: aload_0
    //   36: invokespecial 70	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   39: aload 4
    //   41: invokevirtual 73	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   44: astore_2
    //   45: aload_2
    //   46: ifnull +21 -> 67
    //   49: aload_1
    //   50: aload_2
    //   51: invokevirtual 19	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: pop
    //   55: ldc 75
    //   57: astore_2
    //   58: aload_1
    //   59: aload_2
    //   60: invokevirtual 19	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   63: pop
    //   64: goto -25 -> 39
    //   67: aload_3
    //   68: invokestatic 80	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   71: aload_0
    //   72: invokestatic 80	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   75: aload 4
    //   77: invokestatic 80	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   80: goto +69 -> 149
    //   83: astore_1
    //   84: aload 4
    //   86: astore_2
    //   87: goto +31 -> 118
    //   90: pop
    //   91: aload 4
    //   93: astore_2
    //   94: goto +43 -> 137
    //   97: astore_1
    //   98: goto +20 -> 118
    //   101: astore_1
    //   102: aconst_null
    //   103: astore_0
    //   104: goto +14 -> 118
    //   107: pop
    //   108: aconst_null
    //   109: astore_0
    //   110: goto +27 -> 137
    //   113: astore_1
    //   114: aconst_null
    //   115: astore_0
    //   116: aconst_null
    //   117: astore_3
    //   118: aload_3
    //   119: invokestatic 80	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   122: aload_0
    //   123: invokestatic 80	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   126: aload_2
    //   127: invokestatic 80	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   130: aload_1
    //   131: athrow
    //   132: pop
    //   133: aconst_null
    //   134: astore_0
    //   135: aconst_null
    //   136: astore_3
    //   137: aload_3
    //   138: invokestatic 80	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   141: aload_0
    //   142: invokestatic 80	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   145: aload_2
    //   146: invokestatic 80	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   149: aload_1
    //   150: invokevirtual 32	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   153: areturn
    //   154: pop
    //   155: goto -18 -> 137
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	158	0	paramInputStream	InputStream
    //   3	56	1	localStringBuilder	StringBuilder
    //   83	1	1	localObject1	Object
    //   97	1	1	localObject2	Object
    //   101	1	1	localObject3	Object
    //   113	37	1	localObject4	Object
    //   9	137	2	localObject5	Object
    //   13	125	3	localGZIPInputStream	java.util.zip.GZIPInputStream
    //   31	61	4	localBufferedReader	BufferedReader
    //   90	1	9	localIOException1	IOException
    //   107	1	10	localIOException2	IOException
    //   132	1	11	localIOException3	IOException
    //   154	1	12	localIOException4	IOException
    // Exception table:
    //   from	to	target	type
    //   39	44	83	finally
    //   50	55	83	finally
    //   59	64	83	finally
    //   39	44	90	java/io/IOException
    //   50	55	90	java/io/IOException
    //   59	64	90	java/io/IOException
    //   28	31	97	finally
    //   35	39	97	finally
    //   19	22	101	finally
    //   24	28	101	finally
    //   19	22	107	java/io/IOException
    //   24	28	107	java/io/IOException
    //   10	13	113	finally
    //   15	19	113	finally
    //   10	13	132	java/io/IOException
    //   15	19	132	java/io/IOException
    //   28	31	154	java/io/IOException
    //   35	39	154	java/io/IOException
  }
  
  public static String a(Date paramDate)
  {
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    localSimpleDateFormat.<init>("yyyy-MM-dd HH:mm:ss");
    return localSimpleDateFormat.format(paramDate);
  }
  
  public static String b(String paramString)
  {
    k localk = k.a();
    Object localObject = "IN";
    try
    {
      localObject = localk.a(paramString, (String)localObject);
      k.c localc = k.c.a;
      paramString = localk.a((m.a)localObject, localc);
    }
    catch (g localg)
    {
      for (;;) {}
    }
    return paramString;
  }
  
  public static boolean b(String paramString1, String paramString2)
  {
    boolean bool = TextUtils.isEmpty(paramString1);
    if (bool) {
      return true;
    }
    try
    {
      paramString1 = Pattern.compile(paramString1);
      paramString1 = paramString1.matcher(paramString2);
      return paramString1.matches();
    }
    catch (PatternSyntaxException localPatternSyntaxException) {}
    return false;
  }
  
  public static String d(String paramString)
  {
    paramString = Base64.decode(paramString, 2);
    ByteArrayInputStream localByteArrayInputStream = new java/io/ByteArrayInputStream;
    localByteArrayInputStream.<init>(paramString);
    return a(localByteArrayInputStream);
  }
  
  public static HashMap g(String paramString)
  {
    localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      paramString = localJSONObject.keys();
      for (;;)
      {
        boolean bool = paramString.hasNext();
        if (!bool) {
          break;
        }
        Object localObject1 = paramString.next();
        localObject1 = (String)localObject1;
        Object localObject2 = localJSONObject.get((String)localObject1);
        localObject2 = localObject2.toString();
        localHashMap.put(localObject1, localObject2);
      }
      return localHashMap;
    }
    catch (JSONException localJSONException) {}
  }
  
  private String i(String paramString)
  {
    Object localObject1 = new java/io/File;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("/data/data/");
    Object localObject3 = a.getPackageName();
    ((StringBuilder)localObject2).append((String)localObject3);
    localObject3 = "/";
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append(paramString);
    paramString = ((StringBuilder)localObject2).toString();
    ((File)localObject1).<init>(paramString);
    paramString = new java/lang/StringBuilder;
    paramString.<init>();
    try
    {
      localObject2 = new java/io/BufferedReader;
      localObject3 = new java/io/FileReader;
      ((FileReader)localObject3).<init>((File)localObject1);
      ((BufferedReader)localObject2).<init>((Reader)localObject3);
      for (;;)
      {
        localObject1 = ((BufferedReader)localObject2).readLine();
        if (localObject1 == null) {
          break;
        }
        paramString.append((String)localObject1);
        char c = '\n';
        paramString.append(c);
      }
      ((BufferedReader)localObject2).close();
      return paramString.toString();
    }
    catch (IOException localIOException) {}
    return null;
  }
  
  public final o a()
  {
    -..Lambda.ax.xZe3FvVbUeUCfbgzRSWupwJmSuI localxZe3FvVbUeUCfbgzRSWupwJmSuI = new com/truecaller/truepay/app/utils/-$$Lambda$ax$xZe3FvVbUeUCfbgzRSWupwJmSuI;
    localxZe3FvVbUeUCfbgzRSWupwJmSuI.<init>(this);
    return o.a(localxZe3FvVbUeUCfbgzRSWupwJmSuI);
  }
  
  /* Error */
  public final String a(String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: iconst_1
    //   3: istore_3
    //   4: iconst_2
    //   5: istore 4
    //   7: new 67	java/io/BufferedReader
    //   10: astore 5
    //   12: new 64	java/io/InputStreamReader
    //   15: astore 6
    //   17: aload_0
    //   18: getfield 12	com/truecaller/truepay/app/utils/ax:a	Landroid/content/Context;
    //   21: astore 7
    //   23: aload 7
    //   25: invokevirtual 99	android/content/Context:getAssets	()Landroid/content/res/AssetManager;
    //   28: astore 7
    //   30: aload 7
    //   32: aload_1
    //   33: invokevirtual 107	android/content/res/AssetManager:open	(Ljava/lang/String;)Ljava/io/InputStream;
    //   36: astore_1
    //   37: ldc -5
    //   39: astore 7
    //   41: aload 6
    //   43: aload_1
    //   44: aload 7
    //   46: invokespecial 254	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   49: aload 5
    //   51: aload 6
    //   53: invokespecial 70	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   56: new 14	java/lang/StringBuilder
    //   59: astore_1
    //   60: aload_1
    //   61: invokespecial 15	java/lang/StringBuilder:<init>	()V
    //   64: aload 5
    //   66: invokevirtual 73	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   69: astore 6
    //   71: aload 6
    //   73: ifnull +24 -> 97
    //   76: aload_1
    //   77: aload 6
    //   79: invokevirtual 19	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   82: pop
    //   83: ldc 75
    //   85: astore 6
    //   87: aload_1
    //   88: aload 6
    //   90: invokevirtual 19	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   93: pop
    //   94: goto -30 -> 64
    //   97: aload_1
    //   98: invokevirtual 32	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   101: astore_1
    //   102: aload 5
    //   104: invokevirtual 230	java/io/BufferedReader:close	()V
    //   107: goto +32 -> 139
    //   110: astore_2
    //   111: iload 4
    //   113: anewarray 196	java/lang/String
    //   116: astore 8
    //   118: ldc_w 256
    //   121: astore 5
    //   123: aload 8
    //   125: iconst_0
    //   126: aload 5
    //   128: aastore
    //   129: aload_2
    //   130: invokevirtual 257	java/io/IOException:toString	()Ljava/lang/String;
    //   133: astore_2
    //   134: aload 8
    //   136: iload_3
    //   137: aload_2
    //   138: aastore
    //   139: aload_1
    //   140: areturn
    //   141: astore_1
    //   142: goto +14 -> 156
    //   145: astore_1
    //   146: aconst_null
    //   147: astore 5
    //   149: goto +80 -> 229
    //   152: astore_1
    //   153: aconst_null
    //   154: astore 5
    //   156: iload 4
    //   158: anewarray 196	java/lang/String
    //   161: astore 6
    //   163: ldc_w 256
    //   166: astore 7
    //   168: aload 6
    //   170: iconst_0
    //   171: aload 7
    //   173: aastore
    //   174: aload_1
    //   175: invokevirtual 257	java/io/IOException:toString	()Ljava/lang/String;
    //   178: astore_1
    //   179: aload 6
    //   181: iload_3
    //   182: aload_1
    //   183: aastore
    //   184: aload 5
    //   186: ifnull +40 -> 226
    //   189: aload 5
    //   191: invokevirtual 230	java/io/BufferedReader:close	()V
    //   194: goto +32 -> 226
    //   197: astore_1
    //   198: iload 4
    //   200: anewarray 196	java/lang/String
    //   203: astore 8
    //   205: ldc_w 256
    //   208: astore 5
    //   210: aload 8
    //   212: iconst_0
    //   213: aload 5
    //   215: aastore
    //   216: aload_1
    //   217: invokevirtual 257	java/io/IOException:toString	()Ljava/lang/String;
    //   220: astore_1
    //   221: aload 8
    //   223: iload_3
    //   224: aload_1
    //   225: aastore
    //   226: aconst_null
    //   227: areturn
    //   228: astore_1
    //   229: aload 5
    //   231: ifnull +40 -> 271
    //   234: aload 5
    //   236: invokevirtual 230	java/io/BufferedReader:close	()V
    //   239: goto +32 -> 271
    //   242: astore_2
    //   243: iload 4
    //   245: anewarray 196	java/lang/String
    //   248: astore 8
    //   250: ldc_w 256
    //   253: astore 5
    //   255: aload 8
    //   257: iconst_0
    //   258: aload 5
    //   260: aastore
    //   261: aload_2
    //   262: invokevirtual 257	java/io/IOException:toString	()Ljava/lang/String;
    //   265: astore_2
    //   266: aload 8
    //   268: iload_3
    //   269: aload_2
    //   270: aastore
    //   271: aload_1
    //   272: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	273	0	this	ax
    //   0	273	1	paramString	String
    //   1	1	2	localObject1	Object
    //   110	20	2	localIOException1	IOException
    //   133	5	2	str1	String
    //   242	20	2	localIOException2	IOException
    //   265	5	2	str2	String
    //   3	266	3	i	int
    //   5	239	4	j	int
    //   10	249	5	localObject2	Object
    //   15	165	6	localObject3	Object
    //   21	151	7	localObject4	Object
    //   116	151	8	arrayOfString	String[]
    // Exception table:
    //   from	to	target	type
    //   102	107	110	java/io/IOException
    //   56	59	141	java/io/IOException
    //   60	64	141	java/io/IOException
    //   64	69	141	java/io/IOException
    //   77	83	141	java/io/IOException
    //   88	94	141	java/io/IOException
    //   97	101	141	java/io/IOException
    //   7	10	145	finally
    //   12	15	145	finally
    //   17	21	145	finally
    //   23	28	145	finally
    //   32	36	145	finally
    //   44	49	145	finally
    //   51	56	145	finally
    //   7	10	152	java/io/IOException
    //   12	15	152	java/io/IOException
    //   17	21	152	java/io/IOException
    //   23	28	152	java/io/IOException
    //   32	36	152	java/io/IOException
    //   44	49	152	java/io/IOException
    //   51	56	152	java/io/IOException
    //   189	194	197	java/io/IOException
    //   56	59	228	finally
    //   60	64	228	finally
    //   64	69	228	finally
    //   77	83	228	finally
    //   88	94	228	finally
    //   97	101	228	finally
    //   156	161	228	finally
    //   171	174	228	finally
    //   174	178	228	finally
    //   182	184	228	finally
    //   234	239	242	java/io/IOException
  }
  
  public final void a(String paramString1, String paramString2)
  {
    ClipboardManager localClipboardManager = (ClipboardManager)a.getSystemService("clipboard");
    paramString1 = ClipData.newPlainText("text", paramString1);
    localClipboardManager.setPrimaryClip(paramString1);
    Toast.makeText(a, paramString2, 0).show();
  }
  
  public final String b()
  {
    try
    {
      Object localObject = a;
      localObject = ((Context)localObject).getAssets();
      String str = "sender_id.json";
      localObject = ((AssetManager)localObject).open(str);
      return a((InputStream)localObject);
    }
    catch (IOException localIOException) {}
    return "";
  }
  
  public final com.truecaller.truepay.app.ui.registration.c.d c()
  {
    try
    {
      Object localObject1 = a;
      localObject1 = ((Context)localObject1).getAssets();
      Object localObject2 = "bank_list.json";
      localObject1 = ((AssetManager)localObject1).open((String)localObject2);
      localObject1 = a((InputStream)localObject1);
      localObject2 = new com/truecaller/truepay/app/utils/ax$3;
      ((ax.3)localObject2).<init>(this);
      localObject2 = b;
      f localf = new com/google/gson/f;
      localf.<init>();
      localObject1 = localf.a((String)localObject1, (Type)localObject2);
      localObject1 = (com.truecaller.truepay.data.api.model.d)localObject1;
      localObject2 = new com/truecaller/truepay/data/c/b;
      ((b)localObject2).<init>();
      return ((b)localObject2).a((com.truecaller.truepay.data.api.model.d)localObject1);
    }
    catch (Exception localException)
    {
      com.truecaller.log.d.a(localException;
    }
    return null;
  }
  
  public final HashMap c(String paramString)
  {
    localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    try
    {
      Object localObject1 = new com/truecaller/truepay/app/utils/ax$2;
      ((ax.2)localObject1).<init>(this);
      localObject1 = b;
      f localf = new com/google/gson/f;
      localf.<init>();
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      paramString = localJSONObject.keys();
      int i = paramString.hasNext();
      if (i != 0)
      {
        Object localObject2 = paramString.next();
        localObject2 = (String)localObject2;
        Object localObject3 = localJSONObject.getJSONObject((String)localObject2);
        localObject3 = ((JSONObject)localObject3).toString();
        Object localObject4 = localJSONObject.getJSONObject((String)localObject2);
        String str = "sender_ids";
        localObject4 = ((JSONObject)localObject4).get(str);
        localObject4 = (JSONArray)localObject4;
        localObject3 = localf.a((String)localObject3, (Type)localObject1);
        localObject3 = (SenderInfo)localObject3;
        ((SenderInfo)localObject3).setSymbol((String)localObject2);
        i = 0;
        localObject2 = null;
        for (;;)
        {
          int k = ((JSONArray)localObject4).length();
          if (i >= k) {
            break;
          }
          str = ((JSONArray)localObject4).getString(i);
          localHashMap.put(str, localObject3);
          int j;
          i += 1;
        }
      }
      return localHashMap;
    }
    catch (JSONException paramString)
    {
      com.truecaller.log.d.a(paramString);
    }
  }
  
  public final List d()
  {
    Object localObject1 = "clevertap_app_tags_sheet.json";
    try
    {
      localObject1 = a((String)localObject1);
      Object localObject2 = new java/lang/String;
      int i = 2;
      localObject1 = Base64.decode((String)localObject1, i);
      ((String)localObject2).<init>((byte[])localObject1);
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>((String)localObject2);
      localObject2 = "app_ids_list";
      localObject2 = localJSONObject.getJSONArray((String)localObject2);
      i = 0;
      localJSONObject = null;
      for (;;)
      {
        int j = ((JSONArray)localObject2).length();
        if (i >= j) {
          break;
        }
        String str = ((JSONArray)localObject2).getString(i);
        ((List)localObject1).add(str);
        i += 1;
      }
      return (List)localObject1;
    }
    catch (Exception localException)
    {
      com.truecaller.log.d.a(localException;
    }
    return null;
  }
  
  public final o e(String paramString)
  {
    -..Lambda.ax.kml1FQnqpxSw4BaWoHejWY_7TD8 localkml1FQnqpxSw4BaWoHejWY_7TD8 = new com/truecaller/truepay/app/utils/-$$Lambda$ax$kml1FQnqpxSw4BaWoHejWY_7TD8;
    localkml1FQnqpxSw4BaWoHejWY_7TD8.<init>(this, paramString);
    return o.a(localkml1FQnqpxSw4BaWoHejWY_7TD8);
  }
  
  public final o f(String paramString)
  {
    -..Lambda.ax.NoebNKrFMtsF9YQEzKGZafaGMPk localNoebNKrFMtsF9YQEzKGZafaGMPk = new com/truecaller/truepay/app/utils/-$$Lambda$ax$NoebNKrFMtsF9YQEzKGZafaGMPk;
    localNoebNKrFMtsF9YQEzKGZafaGMPk.<init>(this, paramString);
    return o.a(localNoebNKrFMtsF9YQEzKGZafaGMPk);
  }
  
  public final com.truecaller.truepay.app.ui.payments.models.a h(String paramString)
  {
    paramString = i(paramString);
    if (paramString != null)
    {
      paramString = d(paramString);
      f localf = new com/google/gson/f;
      localf.<init>();
      return (com.truecaller.truepay.app.ui.payments.models.a)localf.a(paramString, com.truecaller.truepay.app.ui.payments.models.a.class);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.ax
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */