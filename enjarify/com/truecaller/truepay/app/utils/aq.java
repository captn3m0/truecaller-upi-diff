package com.truecaller.truepay.app.utils;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.b;
import c.g.b.k;

public final class aq
  implements as
{
  private final int b;
  private final int c;
  private final Context d;
  
  public aq(Context paramContext)
  {
    d = paramContext;
    int i = Color.parseColor("#ffffff");
    b = i;
    i = Color.parseColor("#000000");
    c = i;
  }
  
  public final int a(int paramInt)
  {
    return b.c(d, paramInt);
  }
  
  public final int a(String paramString)
  {
    String str = "colorHex";
    k.b(paramString, str);
    int i;
    try
    {
      i = Color.parseColor(paramString);
    }
    catch (Exception localException)
    {
      i = b;
    }
    return i;
  }
  
  public final int b(String paramString)
  {
    String str = "colorHex";
    k.b(paramString, str);
    int i;
    try
    {
      i = Color.parseColor(paramString);
    }
    catch (Exception localException)
    {
      i = c;
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */