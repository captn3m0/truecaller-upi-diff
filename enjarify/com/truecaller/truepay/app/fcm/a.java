package com.truecaller.truepay.app.fcm;

import android.content.Context;
import c.g.b.k;
import com.google.gson.l;
import com.google.gson.q;

public final class a
{
  public static final a.a a;
  
  static
  {
    a.a locala = new com/truecaller/truepay/app/fcm/a$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    l locall = q.a("{\"d\":{\"e\":{\"i\":1557835183,\"s\":2,\"t\":32,\"c\":1557835183},\"a\":{\"ac\":\"action.page.transaction_details,action.share_receipt\",\"s\":\"Mobile Number: 9895252002\",\"t\":\"Recharge Successful · ₹10.0\",\"u\":\"5cdaada7492041012bae931f\",\"i\":\"http://assets.api.tcpay.in/images/apps/utilities/operators/prepaid/icons/airtel_pre.png\"}},\"s\":1,\"m\":1,\"b\":null}");
    TruepayFcmManager localTruepayFcmManager = new com/truecaller/truepay/app/fcm/TruepayFcmManager;
    localTruepayFcmManager.<init>(paramContext);
    k.a(locall, "json");
    paramContext = locall.i();
    localTruepayFcmManager.handleNotification(32, paramContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.fcm.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */