package com.truecaller.truepay.app.fcm;

public enum NotificationType
{
  public final int value;
  
  static
  {
    Object localObject = new com/truecaller/truepay/app/fcm/NotificationType;
    ((NotificationType)localObject).<init>("UNSUPPORTED", 0, -1 << -1);
    UNSUPPORTED = (NotificationType)localObject;
    localObject = new com/truecaller/truepay/app/fcm/NotificationType;
    int i = 1;
    ((NotificationType)localObject).<init>("PAYMENT_REQUEST", i, 29);
    PAYMENT_REQUEST = (NotificationType)localObject;
    localObject = new com/truecaller/truepay/app/fcm/NotificationType;
    int j = 2;
    ((NotificationType)localObject).<init>("PAYMENT_INCOMING", j, 30);
    PAYMENT_INCOMING = (NotificationType)localObject;
    localObject = new com/truecaller/truepay/app/fcm/NotificationType;
    int k = 3;
    ((NotificationType)localObject).<init>("PAYMENT_CONFIRMATION", k, 31);
    PAYMENT_CONFIRMATION = (NotificationType)localObject;
    localObject = new com/truecaller/truepay/app/fcm/NotificationType;
    int m = 4;
    ((NotificationType)localObject).<init>("PAYMENT_CUSTOM", m, 32);
    PAYMENT_CUSTOM = (NotificationType)localObject;
    localObject = new NotificationType[5];
    NotificationType localNotificationType = UNSUPPORTED;
    localObject[0] = localNotificationType;
    localNotificationType = PAYMENT_REQUEST;
    localObject[i] = localNotificationType;
    localNotificationType = PAYMENT_INCOMING;
    localObject[j] = localNotificationType;
    localNotificationType = PAYMENT_CONFIRMATION;
    localObject[k] = localNotificationType;
    localNotificationType = PAYMENT_CUSTOM;
    localObject[m] = localNotificationType;
    $VALUES = (NotificationType[])localObject;
  }
  
  private NotificationType(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static NotificationType valueOf(int paramInt)
  {
    NotificationType[] arrayOfNotificationType = values();
    int i = arrayOfNotificationType.length;
    int j = 0;
    while (j < i)
    {
      NotificationType localNotificationType = arrayOfNotificationType[j];
      int k = value;
      if (k == paramInt) {
        return localNotificationType;
      }
      j += 1;
    }
    return UNSUPPORTED;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.fcm.NotificationType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */