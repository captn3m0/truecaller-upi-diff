package com.truecaller.truepay.app.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.z.a;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.support.v4.content.b;
import android.support.v4.content.d;
import android.text.TextUtils;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.notificationchannels.n.a;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.history.views.activities.TransactionHistoryActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import io.reactivex.q;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TruepayFcmManager
{
  private final String REGEX_GENERIC_NAME_RESOLVER = "\\d{10}";
  private final Context context;
  private final com.truecaller.notificationchannels.e coreNotificationChannelProvider;
  private int notificationId;
  
  public TruepayFcmManager(Context paramContext)
  {
    context = paramContext;
    paramContext = n.a.a(paramContext, null).c();
    coreNotificationChannelProvider = paramContext;
  }
  
  private Bitmap fetchAvatar(String paramString)
  {
    paramString = getBitmap(paramString);
    if (paramString == null)
    {
      paramString = context.getResources();
      int i = R.drawable.ic_stat_avatar;
      paramString = BitmapFactory.decodeResource(paramString, i);
    }
    return paramString;
  }
  
  private void fetchAvatarAndNotify(NotificationModel paramNotificationModel, z.d paramd)
  {
    Object localObject = d;
    boolean bool = isNotEmpty((String)localObject);
    if (bool)
    {
      paramNotificationModel = d;
    }
    else
    {
      localObject = c;
      bool = isNotEmpty((String)localObject);
      if (bool) {
        paramNotificationModel = c;
      } else {
        paramNotificationModel = "0000000000";
      }
    }
    localObject = new com/truecaller/truepay/data/f/r;
    ContentResolver localContentResolver = context.getContentResolver();
    ((com.truecaller.truepay.data.f.r)localObject).<init>(localContentResolver);
    paramNotificationModel = ((com.truecaller.truepay.data.f.r)localObject).c(paramNotificationModel);
    localObject = io.reactivex.g.a.b();
    paramNotificationModel = paramNotificationModel.b((io.reactivex.n)localObject);
    localObject = new com/truecaller/truepay/app/fcm/-$$Lambda$TruepayFcmManager$-TdriVIrHjAL7hOp-hh87s-yG3Y;
    ((-..Lambda.TruepayFcmManager.-TdriVIrHjAL7hOp-hh87s-yG3Y)localObject).<init>(this, paramd);
    paramNotificationModel = paramNotificationModel.a((io.reactivex.c.e)localObject);
    localObject = io.reactivex.android.b.a.a();
    paramNotificationModel = paramNotificationModel.a((io.reactivex.n)localObject);
    localObject = new com/truecaller/truepay/app/fcm/TruepayFcmManager$3;
    ((TruepayFcmManager.3)localObject).<init>(this, paramd);
    paramNotificationModel.a((q)localObject);
  }
  
  private void fetchContactAndNotify(z.d paramd, NotificationModel paramNotificationModel, String paramString)
  {
    Object localObject1 = new com/truecaller/truepay/data/f/r;
    Object localObject2 = context.getContentResolver();
    ((com.truecaller.truepay.data.f.r)localObject1).<init>((ContentResolver)localObject2);
    localObject1 = ((com.truecaller.truepay.data.f.r)localObject1).d(paramString);
    localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.o)localObject1).b((io.reactivex.n)localObject2);
    localObject2 = new com/truecaller/truepay/app/fcm/-$$Lambda$TruepayFcmManager$DeG7ufTJqz1D8CpM2plFGbaimVA;
    ((-..Lambda.TruepayFcmManager.DeG7ufTJqz1D8CpM2plFGbaimVA)localObject2).<init>(this, paramNotificationModel, paramString, paramd);
    paramNotificationModel = ((io.reactivex.o)localObject1).a((io.reactivex.c.e)localObject2);
    paramString = io.reactivex.android.b.a.a();
    paramNotificationModel = paramNotificationModel.a(paramString);
    paramString = new com/truecaller/truepay/app/fcm/TruepayFcmManager$2;
    paramString.<init>(this, paramd);
    paramNotificationModel.a(paramString);
  }
  
  private Bitmap fetchOperator(String paramString)
  {
    paramString = getBitmap(paramString);
    if (paramString == null)
    {
      paramString = context.getResources();
      int i = R.drawable.ic_place_holder_circle;
      paramString = BitmapFactory.decodeResource(paramString, i);
    }
    return paramString;
  }
  
  private void fetchOperatorAndNotify(NotificationModel paramNotificationModel, z.d paramd)
  {
    Object localObject = new com/truecaller/truepay/app/fcm/-$$Lambda$TruepayFcmManager$DLTfxaLA_jsQFp754xB44zxY2Lw;
    ((-..Lambda.TruepayFcmManager.DLTfxaLA_jsQFp754xB44zxY2Lw)localObject).<init>(this, paramNotificationModel);
    paramNotificationModel = io.reactivex.o.a((io.reactivex.r)localObject);
    localObject = io.reactivex.g.a.b();
    paramNotificationModel = paramNotificationModel.b((io.reactivex.n)localObject);
    localObject = new com/truecaller/truepay/app/fcm/-$$Lambda$TruepayFcmManager$3s1OpNtz84-dLWrsf2IULEOVqvc;
    ((-..Lambda.TruepayFcmManager.3s1OpNtz84-dLWrsf2IULEOVqvc)localObject).<init>(this, paramd);
    paramNotificationModel = paramNotificationModel.a((io.reactivex.c.e)localObject);
    localObject = io.reactivex.android.b.a.a();
    paramNotificationModel = paramNotificationModel.a((io.reactivex.n)localObject);
    localObject = new com/truecaller/truepay/app/fcm/TruepayFcmManager$1;
    ((TruepayFcmManager.1)localObject).<init>(this, paramd);
    paramNotificationModel.a((q)localObject);
  }
  
  private Intent getBankingTabIntent()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.setAction("android.intent.action.VIEW");
    Uri localUri = Uri.parse("truecaller://home/tabs/banking");
    localIntent.setData(localUri);
    return localIntent;
  }
  
  private Bitmap getBitmap(String paramString)
  {
    if (paramString != null) {
      try
      {
        paramString = Uri.parse(paramString);
        Object localObject = context;
        localObject = w.a((Context)localObject);
        paramString = ((w)localObject).a(paramString);
        localObject = aq.d.b();
        paramString = paramString.a((ai)localObject);
        paramString = paramString.d();
      }
      catch (IOException|SecurityException localIOException) {}
    } else {
      paramString = null;
    }
    return paramString;
  }
  
  private Intent getInstantRewardsIntent(NotificationModel paramNotificationModel)
  {
    Intent localIntent = getBankingTabIntent();
    paramNotificationModel = a;
    localIntent.putExtra("instant_reward_content", paramNotificationModel);
    localIntent.putExtra("show_instant_reward", true);
    return localIntent;
  }
  
  private Matcher getMatcher(String paramString)
  {
    return Pattern.compile("\\d{10}").matcher(paramString);
  }
  
  private boolean isNotEmpty(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      String str = "0000000000";
      boolean bool2 = TextUtils.equals(paramString, str);
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  private void notifyUser(Notification paramNotification)
  {
    Object localObject = context;
    String str = "notification";
    localObject = (NotificationManager)((Context)localObject).getSystemService(str);
    if (localObject != null)
    {
      int i = notificationId;
      ((NotificationManager)localObject).notify(i, paramNotification);
    }
  }
  
  private void notifyUser(NotificationModel paramNotificationModel, Intent paramIntent)
  {
    notifyUser(paramNotificationModel, paramIntent, null);
  }
  
  private void notifyUser(NotificationModel paramNotificationModel, Intent paramIntent, List paramList)
  {
    Object localObject1 = context;
    Object localObject2 = null;
    int i = 268435456;
    paramIntent = PendingIntent.getActivity((Context)localObject1, 0, paramIntent, i);
    localObject1 = coreNotificationChannelProvider.j();
    if (localObject1 == null)
    {
      localObject1 = new android/support/v4/app/z$d;
      localObject2 = context;
      ((z.d)localObject1).<init>((Context)localObject2);
    }
    else
    {
      localObject2 = new android/support/v4/app/z$d;
      localObject3 = context;
      ((z.d)localObject2).<init>((Context)localObject3, (String)localObject1);
      localObject1 = localObject2;
    }
    localObject2 = new android/support/v4/app/z$c;
    ((z.c)localObject2).<init>();
    Object localObject3 = a;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject3);
    if (!bool1)
    {
      localObject3 = a;
      ((z.d)localObject1).a((CharSequence)localObject3);
      localObject3 = a;
      ((z.c)localObject2).a((CharSequence)localObject3);
    }
    localObject3 = b;
    bool1 = TextUtils.isEmpty((CharSequence)localObject3);
    if (!bool1)
    {
      localObject3 = b;
      ((z.d)localObject1).b((CharSequence)localObject3);
      localObject3 = b;
      ((z.c)localObject2).b((CharSequence)localObject3);
    }
    int j = R.drawable.ic_stat_notification;
    ((z.d)localObject1).a(j);
    localObject3 = context;
    int k = R.color.accent_default;
    j = b.c((Context)localObject3, k);
    C = j;
    ((z.d)localObject1).a((z.g)localObject2);
    f = paramIntent;
    paramIntent = NotificationType.PAYMENT_INCOMING;
    localObject2 = h;
    int m = paramIntent.equals(localObject2);
    if (m != 0)
    {
      paramIntent = Truepay.getInstance().getListener().getTcPayNotificationTone();
      ((z.d)localObject1).a(paramIntent);
    }
    else
    {
      m = 1;
      ((z.d)localObject1).c(m);
    }
    l = 2;
    ((z.d)localObject1).d(16);
    paramIntent = NotificationType.PAYMENT_INCOMING;
    localObject2 = h;
    boolean bool2 = paramIntent.equals(localObject2);
    if (!bool2)
    {
      paramIntent = NotificationType.PAYMENT_REQUEST;
      localObject2 = h;
      bool2 = paramIntent.equals(localObject2);
      if (!bool2)
      {
        paramIntent = NotificationType.PAYMENT_CUSTOM;
        localObject2 = h;
        bool2 = paramIntent.equals(localObject2);
        if (bool2)
        {
          paramIntent = "action.instant_reward_show";
          localObject2 = g;
          bool2 = paramIntent.equals(localObject2);
          if (bool2)
          {
            paramIntent = ((z.d)localObject1).h();
            notifyUser(paramIntent);
            sendInstantRewardBroadcast(paramNotificationModel);
            return;
          }
          if (paramList != null)
          {
            paramIntent = paramList.iterator();
            for (;;)
            {
              bool3 = paramIntent.hasNext();
              if (!bool3) {
                break;
              }
              paramList = (z.a)paramIntent.next();
              if (paramList != null) {
                ((z.d)localObject1).a(paramList);
              }
            }
          }
          paramIntent = a;
          paramIntent = getMatcher(paramIntent);
          boolean bool3 = paramIntent.find();
          if (bool3)
          {
            paramIntent = paramIntent.group().replaceAll("[^0-9]", "");
            fetchContactAndNotify((z.d)localObject1, paramNotificationModel, paramIntent);
            return;
          }
          fetchOperatorAndNotify(paramNotificationModel, (z.d)localObject1);
          return;
        }
        paramNotificationModel = ((z.d)localObject1).h();
        notifyUser(paramNotificationModel);
        return;
      }
    }
    fetchAvatarAndNotify(paramNotificationModel, (z.d)localObject1);
  }
  
  private String resolveName(String paramString1, String paramString2)
  {
    return paramString1.replaceAll("\\d{10}", paramString2);
  }
  
  private void sendInstantRewardBroadcast(NotificationModel paramNotificationModel)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("INSTANT_REWARD_RECEIVED");
    paramNotificationModel = a;
    localIntent.putExtra("instant_reward_content", paramNotificationModel);
    int i = notificationId;
    localIntent.putExtra("instant_reward_notification_id", i);
    d.a(context).a(localIntent);
  }
  
  private void showCustomNotification(NotificationModel paramNotificationModel)
  {
    Object localObject1 = g;
    Object localObject2 = getBankingTabIntent();
    int i = TextUtils.isEmpty((CharSequence)localObject1);
    if (i == 0)
    {
      localObject1 = ((String)localObject1).split(",");
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      Intent localIntent = new android/content/Intent;
      Object localObject3 = context;
      localIntent.<init>((Context)localObject3, TransactionHistoryActivity.class);
      localObject3 = new android/os/Bundle;
      ((Bundle)localObject3).<init>();
      Object localObject4 = f;
      ((Bundle)localObject3).putString("id", (String)localObject4);
      int k = notificationId;
      ((Bundle)localObject3).putInt("EXTRA_NOTIFICATION_ID", k);
      localIntent.putExtras((Bundle)localObject3);
      int m = localObject1.length;
      localObject4 = localIntent;
      i = 0;
      localIntent = null;
      while (i < m)
      {
        Object localObject5 = localObject1[i];
        int n = 0;
        z.a locala = null;
        int i1 = -1;
        int i2 = ((String)localObject5).hashCode();
        String str1;
        boolean bool2;
        boolean bool4;
        switch (i2)
        {
        default: 
          break;
        case 1327427831: 
          str1 = "action.instant_reward_show";
          boolean bool1 = ((String)localObject5).equals(str1);
          if (bool1) {
            int i3 = 5;
          }
          break;
        case 970112032: 
          str1 = "action.share_receipt";
          bool2 = ((String)localObject5).equals(str1);
          if (bool2)
          {
            bool2 = false;
            localObject5 = null;
          }
          break;
        case 440937684: 
          str1 = "action.later";
          bool2 = ((String)localObject5).equals(str1);
          if (bool2) {
            int i4 = 4;
          }
          break;
        case -399989823: 
          str1 = "action.check_status";
          boolean bool3 = ((String)localObject5).equals(str1);
          if (bool3) {
            int i5 = 2;
          }
          break;
        case -1488693062: 
          str1 = "action.page.transaction_details";
          bool4 = ((String)localObject5).equals(str1);
          if (bool4) {
            bool4 = true;
          }
          break;
        case -1840980623: 
          str1 = "action.registration";
          bool4 = ((String)localObject5).equals(str1);
          if (bool4) {
            i6 = 3;
          }
          break;
        }
        int i6 = -1;
        i1 = 268435456;
        String str2;
        long l;
        switch (i6)
        {
        default: 
          break;
        case 5: 
          localObject4 = getInstantRewardsIntent(paramNotificationModel);
          break;
        case 4: 
          localObject4 = new android/content/Intent;
          localObject5 = context;
          ((Intent)localObject4).<init>((Context)localObject5, NotificationBroadcastReceiver.class);
          ((Intent)localObject4).setAction("com.truecaller.tcpay.notifications.LATER");
          localObject5 = context;
          n = notificationId;
          localObject5 = PendingIntent.getBroadcast((Context)localObject5, n, (Intent)localObject4, i1);
          locala = new android/support/v4/app/z$a;
          str2 = "LATER";
          locala.<init>(0, str2, (PendingIntent)localObject5);
          break;
        case 3: 
          localObject4 = getBankingTabIntent();
          localObject5 = context;
          l = System.currentTimeMillis();
          n = (int)l;
          localObject5 = PendingIntent.getActivity((Context)localObject5, n, (Intent)localObject4, i1);
          locala = new android/support/v4/app/z$a;
          str2 = "REGISTER";
          locala.<init>(0, str2, (PendingIntent)localObject5);
          break;
        case 2: 
          localObject5 = context;
          l = System.currentTimeMillis();
          n = (int)l;
          localObject5 = PendingIntent.getActivity((Context)localObject5, n, (Intent)localObject4, i1);
          locala = new android/support/v4/app/z$a;
          str2 = "CHECK STATUS";
          locala.<init>(0, str2, (PendingIntent)localObject5);
          break;
        case 1: 
          localObject5 = context;
          l = System.currentTimeMillis();
          n = (int)l;
          localObject5 = PendingIntent.getActivity((Context)localObject5, n, (Intent)localObject4, i1);
          locala = new android/support/v4/app/z$a;
          str2 = "VIEW DETAILS";
          locala.<init>(0, str2, (PendingIntent)localObject5);
          break;
        case 0: 
          localObject5 = context;
          l = System.currentTimeMillis();
          n = (int)l;
          localObject5 = PendingIntent.getActivity((Context)localObject5, n, (Intent)localObject4, i1);
          locala = new android/support/v4/app/z$a;
          str2 = "SHARE RECEIPT";
          locala.<init>(0, str2, (PendingIntent)localObject5);
        }
        if (locala != null) {
          ((List)localObject2).add(locala);
        }
        int j;
        i += 1;
      }
      notifyUser(paramNotificationModel, (Intent)localObject4, (List)localObject2);
      return;
    }
    notifyUser(paramNotificationModel, (Intent)localObject2);
  }
  
  private void showPaymentConfirmation(NotificationModel paramNotificationModel)
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = context;
    localIntent.<init>(localContext, TransactionHistoryActivity.class);
    notifyUser(paramNotificationModel, localIntent);
  }
  
  private void showPaymentIncoming(NotificationModel paramNotificationModel)
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = context;
    localIntent.<init>(localContext, TransactionHistoryActivity.class);
    notifyUser(paramNotificationModel, localIntent);
  }
  
  private void showPaymentRequest(NotificationModel paramNotificationModel)
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = context;
    localIntent.<init>(localContext, TransactionActivity.class);
    localIntent.putExtra("route", "route_pending_request");
    notifyUser(paramNotificationModel, localIntent);
  }
  
  public void handleNotification(int paramInt, com.google.gson.o paramo)
  {
    Object localObject1 = new java/util/Random;
    ((Random)localObject1).<init>();
    int i = ((Random)localObject1).nextInt();
    notificationId = i;
    Object localObject2 = NotificationType.valueOf(paramInt);
    paramo = NotificationModel.a(paramo);
    h = ((NotificationType)localObject2);
    localObject1 = TruepayFcmManager.4.a;
    paramInt = ((NotificationType)localObject2).ordinal();
    paramInt = localObject1[paramInt];
    switch (paramInt)
    {
    default: 
      break;
    case 4: 
      showPaymentConfirmation(paramo);
      break;
    case 3: 
      showPaymentIncoming(paramo);
      break;
    case 2: 
      showPaymentRequest(paramo);
      break;
    case 1: 
      showCustomNotification(paramo);
    }
    localObject2 = new android/content/Intent;
    ((Intent)localObject2).<init>("notification_received");
    d.a(context).a((Intent)localObject2);
    com.truecaller.truepay.app.ui.dashboard.a.b = true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.fcm.TruepayFcmManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */