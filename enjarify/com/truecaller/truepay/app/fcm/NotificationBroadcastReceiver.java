package com.truecaller.truepay.app.fcm;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import c.n.m;
import c.u;

public final class NotificationBroadcastReceiver
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    k.b(paramIntent, "intent");
    String str1 = paramIntent.getAction();
    boolean bool1 = false;
    int i = paramIntent.getIntExtra("EXTRA_NOTIFICATION_ID", 0);
    String str2 = "notification";
    paramContext = paramContext.getSystemService(str2);
    if (paramContext != null)
    {
      paramContext = (NotificationManager)paramContext;
      if (str1 != null)
      {
        str2 = "com.truecaller.tcpay.notifications.LATER";
        bool1 = true;
        boolean bool2 = m.a(str1, str2, bool1);
        if (bool2) {
          paramContext.cancel(i);
        }
      }
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.app.NotificationManager");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.fcm.NotificationBroadcastReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */