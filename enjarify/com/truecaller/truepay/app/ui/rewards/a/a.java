package com.truecaller.truepay.app.ui.rewards.a;

import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.app.ui.rewards.b.d.a;
import com.truecaller.truepay.app.ui.rewards.b.f;
import com.truecaller.truepay.app.ui.rewards.b.g.a;
import com.truecaller.truepay.app.ui.rewards.b.i;
import com.truecaller.truepay.app.ui.rewards.b.j.a;
import com.truecaller.truepay.app.ui.rewards.b.l;
import com.truecaller.truepay.app.ui.rewards.b.m.a;
import com.truecaller.truepay.app.ui.rewards.b.o;
import com.truecaller.truepay.app.ui.rewards.views.activities.RewardsActivity;
import com.truecaller.truepay.app.ui.rewards.views.b.d;
import com.truecaller.truepay.app.utils.ar;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.app.utils.u;
import dagger.a.g;
import javax.inject.Provider;

public final class a
  implements b
{
  private final com.truecaller.truepay.app.a.a.a a;
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  
  private a(com.truecaller.truepay.app.a.a.a parama)
  {
    a = parama;
    Object localObject = new com/truecaller/truepay/app/ui/rewards/a/a$b;
    ((a.b)localObject).<init>(parama);
    b = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/rewards/a/a$g;
    ((a.g)localObject).<init>(parama);
    c = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/rewards/a/a$f;
    ((a.f)localObject).<init>(parama);
    d = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/rewards/a/a$e;
    ((a.e)localObject).<init>(parama);
    e = ((Provider)localObject);
    localObject = b;
    Provider localProvider1 = c;
    Provider localProvider2 = d;
    Provider localProvider3 = e;
    localObject = l.a((Provider)localObject, localProvider1, localProvider2, localProvider3);
    f = ((Provider)localObject);
    localObject = dagger.a.c.a(f);
    g = ((Provider)localObject);
    localObject = com.truecaller.truepay.app.ui.rewards.b.c.a(d);
    h = ((Provider)localObject);
    localObject = dagger.a.c.a(h);
    i = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/rewards/a/a$d;
    ((a.d)localObject).<init>(parama);
    j = ((Provider)localObject);
    localObject = i.a(j);
    k = ((Provider)localObject);
    localObject = dagger.a.c.a(k);
    l = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/rewards/a/a$c;
    ((a.c)localObject).<init>(parama);
    m = ((Provider)localObject);
    localObject = ar.a(m);
    n = ((Provider)localObject);
    localObject = dagger.a.c.a(n);
    o = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/rewards/a/a$h;
    ((a.h)localObject).<init>(parama);
    p = ((Provider)localObject);
    localProvider1 = b;
    localProvider2 = c;
    localProvider3 = d;
    Provider localProvider4 = e;
    Provider localProvider5 = o;
    Provider localProvider6 = p;
    parama = f.a(localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6);
    q = parama;
    parama = dagger.a.c.a(q);
    r = parama;
    parama = dagger.a.c.a(o.a());
    s = parama;
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/truepay/app/ui/rewards/a/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final void a(RewardsActivity paramRewardsActivity)
  {
    Object localObject = (e)g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    featuresRegistry = ((e)localObject);
    localObject = (u)g.a(a.x(), "Cannot return null from a non-@Nullable component method");
    instantRewardHandler = ((u)localObject);
    localObject = (g.a)l.get();
    a = ((g.a)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.rewards.views.b.a parama)
  {
    com.truecaller.truepay.app.ui.rewards.b.a.a locala = (com.truecaller.truepay.app.ui.rewards.b.a.a)i.get();
    b = locala;
  }
  
  public final void a(com.truecaller.truepay.app.ui.rewards.views.b.b paramb)
  {
    Object localObject = (r)g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    b = ((r)localObject);
    localObject = (d.a)r.get();
    c = ((d.a)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.rewards.views.b.c paramc)
  {
    Object localObject = (r)g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    a = ((r)localObject);
    localObject = (j.a)g.get();
    b = ((j.a)localObject);
  }
  
  public final void a(d paramd)
  {
    m.a locala = (m.a)s.get();
    b = locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */