package com.truecaller.truepay.app.ui.rewards.b;

import com.truecaller.truepay.app.ui.rewards.models.Reward;
import com.truecaller.truepay.app.ui.rewards.models.RewardActionData;
import com.truecaller.truepay.app.ui.rewards.models.RewardItem;

public abstract interface d$b
{
  public static final d.b.a a = d.b.a.a;
  
  public abstract void a(int paramInt);
  
  public abstract void a(int paramInt1, String paramString1, String paramString2, int paramInt2);
  
  public abstract void a(int paramInt, String paramString, boolean paramBoolean);
  
  public abstract void a(long paramLong);
  
  public abstract void a(Reward paramReward);
  
  public abstract void a(RewardActionData paramRewardActionData, RewardItem paramRewardItem);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, int paramInt);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(String paramString1, String paramString2, long paramLong);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, int paramInt);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(RewardActionData paramRewardActionData, RewardItem paramRewardItem);
  
  public abstract void b(String paramString);
  
  public abstract void b(String paramString, int paramInt);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(String paramString, int paramInt);
  
  public abstract void d();
  
  public abstract void d(String paramString, int paramInt);
  
  public abstract void e(String paramString, int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.b.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */