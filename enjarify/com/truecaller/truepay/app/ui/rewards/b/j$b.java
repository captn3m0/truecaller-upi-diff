package com.truecaller.truepay.app.ui.rewards.b;

import com.truecaller.truepay.app.ui.rewards.models.RewardItem;
import java.util.List;

public abstract interface j$b
{
  public abstract void a(RewardItem paramRewardItem);
  
  public abstract void a(String paramString);
  
  public abstract void a(List paramList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.b.j.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */