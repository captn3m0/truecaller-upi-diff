package com.truecaller.truepay.app.ui.rewards.b;

import c.g.b.k;
import c.x;
import com.truecaller.bb;
import com.truecaller.truepay.R.string;
import com.truecaller.utils.n;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class b
  extends bb
  implements a.a
{
  private String a;
  private final n c;
  
  public b(n paramn)
  {
    c = paramn;
  }
  
  public final void a()
  {
    a.b localb = (a.b)b;
    if (localb != null)
    {
      localb.b();
      return;
    }
  }
  
  public final void a(String paramString)
  {
    if (paramString != null)
    {
      Object localObject1 = Pattern.compile("\\d+");
      paramString = (CharSequence)paramString;
      paramString = ((Pattern)localObject1).matcher(paramString);
      boolean bool = paramString.find();
      x localx = null;
      if (bool)
      {
        bool = false;
        paramString = paramString.group(0);
        k.a(paramString, "m.group(0)");
        k.b(paramString, "amount");
        a = paramString;
        paramString = c;
        int i = R.string.rs_amount;
        int j = 1;
        Object[] arrayOfObject1 = new Object[j];
        String str = a;
        arrayOfObject1[0] = str;
        paramString = paramString.a(i, arrayOfObject1);
        k.a(paramString, "resourceProvider.getStri…string.rs_amount, amount)");
        Object localObject2 = c;
        int k = R.string.instant_rewards_content;
        Object[] arrayOfObject2 = new Object[j];
        str = a;
        arrayOfObject2[0] = str;
        localObject1 = ((n)localObject2).a(k, arrayOfObject2);
        k.a(localObject1, "resourceProvider.getStri…_rewards_content, amount)");
        localObject2 = (a.b)b;
        if (localObject2 != null)
        {
          ((a.b)localObject2).a(paramString, (String)localObject1);
          localx = x.a;
        }
      }
      else
      {
        paramString = (a.b)b;
        if (paramString != null)
        {
          paramString.b();
          localx = x.a;
        }
      }
      if (localx != null) {}
    }
    else
    {
      paramString = this;
      paramString = (a.b)b;
      if (paramString != null)
      {
        paramString.b();
        paramString = x.a;
        return;
      }
    }
  }
  
  public final void b()
  {
    Object localObject = c;
    int i = R.string.instant_reward_share_text;
    int j = 1;
    Object[] arrayOfObject = new Object[j];
    String str = a;
    arrayOfObject[0] = str;
    localObject = ((n)localObject).a(i, arrayOfObject);
    k.a(localObject, "resourceProvider.getStri…eward_share_text, amount)");
    a.b localb = (a.b)b;
    if (localb != null)
    {
      localb.a((String)localObject);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */