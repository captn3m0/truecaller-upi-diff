package com.truecaller.truepay.app.ui.rewards.b;

import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private l(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static l a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    l locall = new com/truecaller/truepay/app/ui/rewards/b/l;
    locall.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return locall;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */