package com.truecaller.truepay.app.ui.rewards.b;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.rewards.models.RewardActionData;
import com.truecaller.truepay.app.ui.rewards.models.RewardBackground;
import com.truecaller.truepay.app.ui.rewards.models.RewardItem;
import com.truecaller.truepay.app.ui.rewards.models.RewardLogo;
import com.truecaller.truepay.app.ui.rewards.models.RewardMetaData;
import com.truecaller.truepay.app.ui.rewards.models.RewardTextAttributes;
import com.truecaller.truepay.app.utils.as;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.data.api.c;
import com.truecaller.utils.n;
import java.util.Collection;
import java.util.List;

public final class e
  extends ba
  implements d.a
{
  final f c;
  private final f d;
  private final n e;
  private final c f;
  private final as g;
  private final ax h;
  
  public e(f paramf1, f paramf2, n paramn, c paramc, as paramas, ax paramax)
  {
    super(paramf2);
    c = paramf1;
    d = paramf2;
    e = paramn;
    f = paramc;
    g = paramas;
    h = paramax;
  }
  
  private final void a(RewardMetaData paramRewardMetaData)
  {
    d.b localb = (d.b)b;
    if (localb != null)
    {
      as localas = g;
      String str = paramRewardMetaData.getBrandColor();
      int i = localas.a(str);
      str = paramRewardMetaData.getImageUrl();
      paramRewardMetaData = paramRewardMetaData.getContent();
      int j = R.drawable.ic_reward_empty_state;
      localb.a(i, str, paramRewardMetaData, j);
      return;
    }
  }
  
  private final void b(RewardItem paramRewardItem)
  {
    d.b localb = (d.b)b;
    if (localb == null) {
      return;
    }
    Object localObject1 = paramRewardItem.getRewardMetaData().getVendorName();
    Object localObject2 = g;
    int i = R.color.blue_color;
    int k = ((as)localObject2).a(i);
    localb.a((String)localObject1, k);
    int m = 0;
    localObject1 = null;
    localb.a(false);
    localObject2 = paramRewardItem.getActionData();
    if (localObject2 != null)
    {
      Object localObject3 = localObject2;
      localObject3 = (Collection)localObject2;
      boolean bool = ((Collection)localObject3).isEmpty();
      int n = 1;
      bool ^= n;
      if (!bool)
      {
        k = 0;
        localObject2 = null;
      }
      if (localObject2 != null)
      {
        localObject1 = (RewardActionData)((List)localObject2).get(0);
        localb.a((RewardActionData)localObject1, paramRewardItem);
        m = ((List)localObject2).size();
        int j = 2;
        if (m == j)
        {
          localObject1 = (RewardActionData)((List)localObject2).get(n);
          localb.b((RewardActionData)localObject1, paramRewardItem);
        }
      }
    }
    localObject1 = paramRewardItem.getHeader().getText();
    localObject2 = paramRewardItem.getContent().getText();
    long l = paramRewardItem.getExpiresAt();
    localb.a((String)localObject1, (String)localObject2, l);
  }
  
  public final void a()
  {
    d.b localb = (d.b)b;
    if (localb != null)
    {
      localb.b();
      return;
    }
  }
  
  public final void a(RewardItem paramRewardItem)
  {
    k.b(paramRewardItem, "rewardItem");
    Object localObject1 = (d.b)b;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = paramRewardItem.getRewardMetaData().getState();
    Object localObject3 = "locked";
    boolean bool1 = k.a(localObject2, localObject3);
    if (bool1)
    {
      localObject2 = paramRewardItem.getRewardMetaData().getVendorName();
      localObject3 = g;
      String str = paramRewardItem.getBackground().getDetailBackground();
      int j = ((as)localObject3).b(str);
      ((d.b)localObject1).a((String)localObject2, j);
      ((d.b)localObject1).a(true);
      localObject2 = paramRewardItem.getHeader().getText();
      localObject3 = g;
      str = paramRewardItem.getHeader().getColor();
      j = ((as)localObject3).a(str);
      ((d.b)localObject1).b((String)localObject2, j);
      localObject2 = paramRewardItem.getContent().getText();
      localObject3 = g;
      str = paramRewardItem.getContent().getColor();
      j = ((as)localObject3).a(str);
      ((d.b)localObject1).c((String)localObject2, j);
      localObject2 = paramRewardItem.getLogo().getUrl();
      j = R.drawable.ic_placeholder_rewards;
      ((d.b)localObject1).e((String)localObject2, j);
      int i = paramRewardItem.getTemplate();
      switch (i)
      {
      default: 
        break;
      case 2: 
        localObject2 = g;
        localObject3 = paramRewardItem.getBackground().getCardBackground();
        i = ((as)localObject2).b((String)localObject3);
        ((d.b)localObject1).a(i);
        break;
      case 1: 
        localObject2 = paramRewardItem.getBackground().getUrl();
        if (localObject2 != null)
        {
          localObject2 = paramRewardItem.getBackground().getUrl();
          j = R.drawable.ic_placeholder_rewards;
          ((d.b)localObject1).d((String)localObject2, j);
        }
        break;
      }
      long l = paramRewardItem.getExpiresAt();
      ((d.b)localObject1).a(l);
    }
    else
    {
      localObject1 = paramRewardItem.getRewardMetaData().getState();
      localObject2 = "unlocked";
      boolean bool2 = k.a(localObject1, localObject2);
      if (bool2)
      {
        b(paramRewardItem);
        localObject1 = paramRewardItem.getRewardMetaData();
        a((RewardMetaData)localObject1);
      }
    }
    k.b(paramRewardItem, "rewardItem");
    localObject1 = new com/truecaller/truepay/app/ui/rewards/b/e$a;
    ((e.a)localObject1).<init>(this, paramRewardItem, null);
    localObject1 = (m)localObject1;
    kotlinx.coroutines.e.b(this, null, (m)localObject1, 3);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "termsAndConditions");
    d.b localb = (d.b)b;
    if (localb != null)
    {
      localb.b(paramString);
      return;
    }
  }
  
  public final void a(String paramString, RewardItem paramRewardItem)
  {
    k.b(paramString, "type");
    Object localObject = "rewardItem";
    k.b(paramRewardItem, (String)localObject);
    int i = paramString.hashCode();
    int j = -1239339900;
    Object[] arrayOfObject1 = null;
    boolean bool;
    if (i != j)
    {
      j = -383667748;
      if (i == j)
      {
        localObject = "action.offer_copy_text";
        bool = paramString.equals(localObject);
        if (bool)
        {
          paramString = h;
          paramRewardItem = paramRewardItem.getRewardMetaData().getContent();
          localObject = e;
          j = R.string.rewards_coupon_code_copy;
          arrayOfObject1 = new Object[0];
          localObject = ((n)localObject).a(j, arrayOfObject1);
          paramString.a(paramRewardItem, (String)localObject);
        }
      }
    }
    else
    {
      localObject = "action.offer_share";
      bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = e;
        i = R.string.rewards_share_text;
        j = 1;
        Object[] arrayOfObject2 = new Object[j];
        paramRewardItem = paramRewardItem.getRewardMetaData().getVendorName();
        arrayOfObject2[0] = paramRewardItem;
        paramString = paramString.a(i, arrayOfObject2);
        k.a(paramString, "resourceProvider.getStri…ewardMetaData.vendorName)");
        paramRewardItem = e;
        i = R.string.rewards_share_header;
        arrayOfObject2 = new Object[0];
        paramRewardItem = paramRewardItem.a(i, arrayOfObject2);
        k.a(paramRewardItem, "resourceProvider.getStri…ing.rewards_share_header)");
        localObject = (d.b)b;
        if (localObject != null)
        {
          ((d.b)localObject).a(paramString, paramRewardItem);
          return;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */