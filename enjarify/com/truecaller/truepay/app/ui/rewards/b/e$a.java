package com.truecaller.truepay.app.ui.rewards.b;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.rewards.models.OfferDetailReq;
import com.truecaller.truepay.app.ui.rewards.models.Reward;
import com.truecaller.truepay.app.ui.rewards.models.RewardItem;
import com.truecaller.truepay.app.ui.rewards.models.RewardMetaData;
import com.truecaller.truepay.app.ui.rewards.models.RewardMileStone;
import com.truecaller.truepay.app.utils.as;
import com.truecaller.utils.n;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class e$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag e;
  
  e$a(e parame, RewardItem paramRewardItem, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/truepay/app/ui/rewards/b/e$a;
    e locale = c;
    RewardItem localRewardItem = d;
    locala.<init>(locale, localRewardItem, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    int j = 0;
    Object localObject1 = null;
    int k = 1;
    boolean bool1;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label793;
      }
      paramObject = e;
      localObject2 = e.a(c);
      if (localObject2 != null) {
        ((d.b)localObject2).b(k);
      }
      localObject2 = c;
      localObject3 = new com/truecaller/truepay/app/ui/rewards/models/OfferDetailReq;
      localObject4 = d.getOfferId();
      ((OfferDetailReq)localObject3).<init>((String)localObject4);
      a = paramObject;
      b = k;
      paramObject = c;
      localObject4 = new com/truecaller/truepay/app/ui/rewards/b/e$b;
      ((e.b)localObject4).<init>((e)localObject2, (OfferDetailReq)localObject3, null);
      localObject4 = (m)localObject4;
      paramObject = g.a((f)paramObject, (m)localObject4, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = (Reward)paramObject;
    boolean bool2 = false;
    locala = null;
    if (paramObject != null)
    {
      localObject2 = e.a(c);
      Object localObject5;
      Object localObject7;
      int i2;
      if (localObject2 != null)
      {
        localObject3 = ((Reward)paramObject).getTerms();
        localObject4 = e.b(c);
        int m = R.string.rewards_terms_text;
        localObject5 = new Object[0];
        localObject4 = ((n)localObject4).a(m, (Object[])localObject5);
        c.g.b.k.a(localObject4, "resourceProvider.getStri…tring.rewards_terms_text)");
        Object localObject6 = e.b(c);
        int i1 = R.string.rewards_terms_link_text;
        localObject7 = new Object[0];
        localObject6 = ((n)localObject6).a(i1, (Object[])localObject7);
        c.g.b.k.a(localObject6, "resourceProvider.getStri….rewards_terms_link_text)");
        localObject5 = e.c(c);
        i2 = R.color.blue_color;
        i1 = ((as)localObject5).a(i2);
        ((d.b)localObject2).a((String)localObject3, (String)localObject4, (String)localObject6, i1);
        ((d.b)localObject2).a((Reward)paramObject);
      }
      localObject2 = ((Reward)paramObject).getMilestones();
      if (localObject2 != null)
      {
        localObject3 = ((Reward)paramObject).getRewardMetaData().getState();
        localObject4 = "locked";
        boolean bool4 = c.g.b.k.a(localObject3, localObject4);
        if (!bool4)
        {
          bool1 = false;
          localObject2 = null;
        }
        if (localObject2 != null)
        {
          localObject1 = ((Reward)paramObject).getMilestones().iterator();
          for (;;)
          {
            bool4 = ((Iterator)localObject1).hasNext();
            if (!bool4) {
              break;
            }
            localObject3 = (RewardMileStone)((Iterator)localObject1).next();
            localObject4 = e.a(c);
            if (localObject4 != null)
            {
              boolean bool3 = ((RewardMileStone)localObject3).getCompleted();
              int n;
              if (bool3) {
                n = R.drawable.ic_tick;
              } else {
                n = R.drawable.milestone_unlocked_circle;
              }
              localObject5 = ((RewardMileStone)localObject3).getTitle();
              int i3 = ((Reward)paramObject).getMilestones().indexOf(localObject3);
              localObject7 = ((Reward)paramObject).getMilestones();
              i2 = ((List)localObject7).size() - k;
              if (i3 < i2)
              {
                i3 = 1;
              }
              else
              {
                i3 = 0;
                localObject3 = null;
              }
              ((d.b)localObject4).a(n, (String)localObject5, i3);
            }
          }
          localObject1 = e.a(c);
          if (localObject1 != null) {
            ((d.b)localObject1).c();
          }
          if (localObject2 != null) {
            break label606;
          }
        }
      }
      localObject2 = e.a(c);
      if (localObject2 != null)
      {
        ((d.b)localObject2).d();
        localObject2 = x.a;
      }
      label606:
      localObject2 = ((Reward)paramObject).getRewardMetaData().getState();
      localObject1 = d.getRewardMetaData().getState();
      bool1 = c.g.b.k.a(localObject2, localObject1) ^ k;
      if (bool1)
      {
        localObject2 = ((Reward)paramObject).getRewardMetaData().getState();
        localObject1 = "unlocked";
        bool1 = c.g.b.k.a(localObject2, localObject1);
        if (bool1)
        {
          localObject2 = c;
          localObject1 = d;
          e.a((e)localObject2, (RewardItem)localObject1);
          localObject2 = c;
          paramObject = ((Reward)paramObject).getRewardMetaData();
          e.a((e)localObject2, (RewardMetaData)paramObject);
        }
      }
    }
    else
    {
      paramObject = e.a(c);
      if (paramObject != null)
      {
        localObject2 = e.b(c);
        j = R.string.something_went_wrong;
        Object[] arrayOfObject = new Object[0];
        localObject2 = ((n)localObject2).a(j, arrayOfObject);
        localObject1 = "resourceProvider.getStri…ing.something_went_wrong)";
        c.g.b.k.a(localObject2, (String)localObject1);
        ((d.b)paramObject).a((String)localObject2);
      }
    }
    paramObject = e.a(c);
    if (paramObject != null) {
      ((d.b)paramObject).b(false);
    }
    return x.a;
    label793:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.b.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */