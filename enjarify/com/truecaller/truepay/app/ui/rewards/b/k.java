package com.truecaller.truepay.app.ui.rewards.b;

import c.d.f;
import c.g.a.m;
import com.truecaller.ba;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.rewards.models.RewardItem;
import com.truecaller.truepay.data.api.c;
import com.truecaller.utils.n;
import kotlinx.coroutines.e;

public final class k
  extends ba
  implements j.a
{
  final f c;
  private final f d;
  private final n e;
  private final c f;
  
  public k(f paramf1, f paramf2, n paramn, c paramc)
  {
    super(paramf2);
    c = paramf1;
    d = paramf2;
    e = paramn;
    f = paramc;
  }
  
  public final void a()
  {
    j.b localb = (j.b)b;
    if (localb != null)
    {
      Object localObject = e;
      int i = R.string.rewards_home_menu;
      Object[] arrayOfObject = new Object[0];
      localObject = ((n)localObject).a(i, arrayOfObject);
      c.g.b.k.a(localObject, "resourceProvider.getStri…string.rewards_home_menu)");
      localb.b((String)localObject);
      return;
    }
  }
  
  public final void a(RewardItem paramRewardItem)
  {
    c.g.b.k.b(paramRewardItem, "rewardItem");
    j.b localb = (j.b)b;
    if (localb != null)
    {
      localb.a(paramRewardItem);
      return;
    }
  }
  
  public final void b()
  {
    Object localObject = new com/truecaller/truepay/app/ui/rewards/b/k$b;
    ((k.b)localObject).<init>(this, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
  }
  
  public final void c()
  {
    j.b localb = (j.b)b;
    if (localb != null)
    {
      localb.b();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */