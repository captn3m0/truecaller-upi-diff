package com.truecaller.truepay.app.ui.rewards.b;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.R.string;
import com.truecaller.utils.n;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class k$b
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  k$b(k paramk, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/truepay/app/ui/rewards/b/k$b;
    k localk = c;
    localb.<init>(localk, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    int k = 1;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label305;
      }
      paramObject = d;
      localObject2 = k.a(c);
      if (localObject2 != null)
      {
        ((j.b)localObject2).a(false);
        ((j.b)localObject2).b(k);
      }
      localObject2 = c;
      a = paramObject;
      b = k;
      paramObject = c;
      Object localObject3 = new com/truecaller/truepay/app/ui/rewards/b/k$a;
      ((k.a)localObject3).<init>((k)localObject2, null);
      localObject3 = (m)localObject3;
      paramObject = g.a((f)paramObject, (m)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (List)paramObject;
    if (paramObject != null)
    {
      localObject1 = k.a(c);
      if (localObject1 != null) {
        ((j.b)localObject1).a((List)paramObject);
      }
      boolean bool3 = ((List)paramObject).isEmpty();
      if (bool3)
      {
        paramObject = k.a(c);
        if (paramObject != null) {
          ((j.b)paramObject).a(k);
        }
      }
    }
    else
    {
      paramObject = c;
      localObject1 = k.b((k)paramObject);
      int j = R.string.something_went_wrong;
      Object[] arrayOfObject = new Object[0];
      localObject1 = ((n)localObject1).a(j, arrayOfObject);
      localObject2 = "resourceProvider.getStri…ing.something_went_wrong)";
      c.g.b.k.a(localObject1, (String)localObject2);
      k.a((k)paramObject, (String)localObject1);
    }
    paramObject = k.a(c);
    if (paramObject != null) {
      ((j.b)paramObject).b(false);
    }
    return x.a;
    label305:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.b.k.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */