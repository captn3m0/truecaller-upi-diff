package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardMileStone$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    RewardMileStone localRewardMileStone = new com/truecaller/truepay/app/ui/rewards/models/RewardMileStone;
    String str = paramParcel.readString();
    int i = paramParcel.readInt();
    if (i != 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramParcel = null;
    }
    localRewardMileStone.<init>(str, i);
    return localRewardMileStone;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new RewardMileStone[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardMileStone.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */