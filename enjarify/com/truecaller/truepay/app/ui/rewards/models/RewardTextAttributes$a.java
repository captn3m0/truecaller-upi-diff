package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardTextAttributes$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    RewardTextAttributes localRewardTextAttributes = new com/truecaller/truepay/app/ui/rewards/models/RewardTextAttributes;
    String str = paramParcel.readString();
    paramParcel = paramParcel.readString();
    localRewardTextAttributes.<init>(str, paramParcel);
    return localRewardTextAttributes;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new RewardTextAttributes[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardTextAttributes.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */