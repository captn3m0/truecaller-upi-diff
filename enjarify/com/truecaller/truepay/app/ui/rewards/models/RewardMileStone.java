package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardMileStone
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final boolean completed;
  private final String title;
  
  static
  {
    RewardMileStone.a locala = new com/truecaller/truepay/app/ui/rewards/models/RewardMileStone$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public RewardMileStone(String paramString, boolean paramBoolean)
  {
    title = paramString;
    completed = paramBoolean;
  }
  
  public final String component1()
  {
    return title;
  }
  
  public final boolean component2()
  {
    return completed;
  }
  
  public final RewardMileStone copy(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "title");
    RewardMileStone localRewardMileStone = new com/truecaller/truepay/app/ui/rewards/models/RewardMileStone;
    localRewardMileStone.<init>(paramString, paramBoolean);
    return localRewardMileStone;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof RewardMileStone;
      if (bool2)
      {
        paramObject = (RewardMileStone)paramObject;
        String str1 = title;
        String str2 = title;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          bool2 = completed;
          boolean bool3 = completed;
          if (bool2 == bool3)
          {
            bool3 = true;
          }
          else
          {
            bool3 = false;
            paramObject = null;
          }
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final boolean getCompleted()
  {
    return completed;
  }
  
  public final String getTitle()
  {
    return title;
  }
  
  public final int hashCode()
  {
    String str = title;
    int i;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    i *= 31;
    int j = completed;
    if (j != 0) {
      j = 1;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RewardMileStone(title=");
    String str = title;
    localStringBuilder.append(str);
    localStringBuilder.append(", completed=");
    boolean bool = completed;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = title;
    paramParcel.writeString(str);
    paramInt = completed;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardMileStone
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */