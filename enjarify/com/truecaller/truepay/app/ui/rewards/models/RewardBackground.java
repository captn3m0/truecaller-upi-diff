package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardBackground
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String cardBackground;
  private final String detailBackground;
  private final String url;
  
  static
  {
    RewardBackground.a locala = new com/truecaller/truepay/app/ui/rewards/models/RewardBackground$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public RewardBackground(String paramString1, String paramString2, String paramString3)
  {
    url = paramString1;
    cardBackground = paramString2;
    detailBackground = paramString3;
  }
  
  public final String component1()
  {
    return url;
  }
  
  public final String component2()
  {
    return cardBackground;
  }
  
  public final String component3()
  {
    return detailBackground;
  }
  
  public final RewardBackground copy(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString2, "cardBackground");
    k.b(paramString3, "detailBackground");
    RewardBackground localRewardBackground = new com/truecaller/truepay/app/ui/rewards/models/RewardBackground;
    localRewardBackground.<init>(paramString1, paramString2, paramString3);
    return localRewardBackground;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RewardBackground;
      if (bool1)
      {
        paramObject = (RewardBackground)paramObject;
        String str1 = url;
        String str2 = url;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = cardBackground;
          str2 = cardBackground;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = detailBackground;
            paramObject = detailBackground;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getCardBackground()
  {
    return cardBackground;
  }
  
  public final String getDetailBackground()
  {
    return detailBackground;
  }
  
  public final String getUrl()
  {
    return url;
  }
  
  public final int hashCode()
  {
    String str1 = url;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = cardBackground;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = detailBackground;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RewardBackground(url=");
    String str = url;
    localStringBuilder.append(str);
    localStringBuilder.append(", cardBackground=");
    str = cardBackground;
    localStringBuilder.append(str);
    localStringBuilder.append(", detailBackground=");
    str = detailBackground;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = url;
    paramParcel.writeString(str);
    str = cardBackground;
    paramParcel.writeString(str);
    str = detailBackground;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardBackground
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */