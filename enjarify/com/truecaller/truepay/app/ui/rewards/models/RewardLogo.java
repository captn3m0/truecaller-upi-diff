package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardLogo
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String url;
  
  static
  {
    RewardLogo.a locala = new com/truecaller/truepay/app/ui/rewards/models/RewardLogo$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public RewardLogo(String paramString)
  {
    url = paramString;
  }
  
  public final String component1()
  {
    return url;
  }
  
  public final RewardLogo copy(String paramString)
  {
    k.b(paramString, "url");
    RewardLogo localRewardLogo = new com/truecaller/truepay/app/ui/rewards/models/RewardLogo;
    localRewardLogo.<init>(paramString);
    return localRewardLogo;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RewardLogo;
      if (bool1)
      {
        paramObject = (RewardLogo)paramObject;
        String str = url;
        paramObject = url;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getUrl()
  {
    return url;
  }
  
  public final int hashCode()
  {
    String str = url;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RewardLogo(url=");
    String str = url;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = url;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardLogo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */