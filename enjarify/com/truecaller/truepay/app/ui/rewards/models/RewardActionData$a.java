package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardActionData$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    RewardActionData localRewardActionData = new com/truecaller/truepay/app/ui/rewards/models/RewardActionData;
    String str = paramParcel.readString();
    paramParcel = paramParcel.readString();
    localRewardActionData.<init>(str, paramParcel);
    return localRewardActionData;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new RewardActionData[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardActionData.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */