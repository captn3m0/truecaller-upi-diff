package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.ArrayList;
import java.util.List;

public final class RewardItem$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    RewardItem localRewardItem = new com/truecaller/truepay/app/ui/rewards/models/RewardItem;
    int i = paramParcel.readInt();
    String str = paramParcel.readString();
    long l = paramParcel.readLong();
    Object localObject1 = RewardBackground.CREATOR.createFromParcel(paramParcel);
    Object localObject2 = localObject1;
    localObject2 = (RewardBackground)localObject1;
    localObject1 = RewardLogo.CREATOR.createFromParcel(paramParcel);
    Object localObject3 = localObject1;
    localObject3 = (RewardLogo)localObject1;
    localObject1 = RewardTextAttributes.CREATOR.createFromParcel(paramParcel);
    Object localObject4 = localObject1;
    localObject4 = (RewardTextAttributes)localObject1;
    localObject1 = RewardTextAttributes.CREATOR.createFromParcel(paramParcel);
    Object localObject5 = localObject1;
    localObject5 = (RewardTextAttributes)localObject1;
    int j = paramParcel.readInt();
    Object localObject6 = null;
    if (j != 0)
    {
      j = paramParcel.readInt();
      localObject7 = new java/util/ArrayList;
      ((ArrayList)localObject7).<init>(j);
      while (j != 0)
      {
        localObject8 = (RewardMileStone)RewardMileStone.CREATOR.createFromParcel(paramParcel);
        ((ArrayList)localObject7).add(localObject8);
        j += -1;
      }
    }
    Object localObject7 = null;
    localObject1 = RewardMetaData.CREATOR.createFromParcel(paramParcel);
    Object localObject8 = localObject1;
    localObject8 = (RewardMetaData)localObject1;
    j = paramParcel.readInt();
    if (j != 0)
    {
      j = paramParcel.readInt();
      localObject6 = new java/util/ArrayList;
      ((ArrayList)localObject6).<init>(j);
      while (j != 0)
      {
        RewardActionData localRewardActionData = (RewardActionData)RewardActionData.CREATOR.createFromParcel(paramParcel);
        ((ArrayList)localObject6).add(localRewardActionData);
        j += -1;
      }
    }
    paramParcel = (Parcel)localObject6;
    localObject1 = localRewardItem;
    localObject6 = localObject7;
    localObject7 = localObject8;
    localObject8 = paramParcel;
    localRewardItem.<init>(i, str, l, (RewardBackground)localObject2, (RewardLogo)localObject3, (RewardTextAttributes)localObject4, (RewardTextAttributes)localObject5, (List)localObject6, (RewardMetaData)localObject7, paramParcel);
    return localRewardItem;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new RewardItem[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardItem.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */