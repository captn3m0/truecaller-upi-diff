package com.truecaller.truepay.app.ui.rewards.models;

import c.g.b.k;

public final class OfferDetailReq
{
  private final String offerId;
  
  public OfferDetailReq(String paramString)
  {
    offerId = paramString;
  }
  
  public final String component1()
  {
    return offerId;
  }
  
  public final OfferDetailReq copy(String paramString)
  {
    k.b(paramString, "offerId");
    OfferDetailReq localOfferDetailReq = new com/truecaller/truepay/app/ui/rewards/models/OfferDetailReq;
    localOfferDetailReq.<init>(paramString);
    return localOfferDetailReq;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof OfferDetailReq;
      if (bool1)
      {
        paramObject = (OfferDetailReq)paramObject;
        String str = offerId;
        paramObject = offerId;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getOfferId()
  {
    return offerId;
  }
  
  public final int hashCode()
  {
    String str = offerId;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("OfferDetailReq(offerId=");
    String str = offerId;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.OfferDetailReq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */