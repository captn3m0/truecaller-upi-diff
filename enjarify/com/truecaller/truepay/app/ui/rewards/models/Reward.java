package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class Reward
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String content;
  private final String expiresAt;
  private final String expiryHeader;
  private final String header;
  private final List milestones;
  private final RewardMetaData rewardMetaData;
  private final String terms;
  
  static
  {
    Reward.a locala = new com/truecaller/truepay/app/ui/rewards/models/Reward$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public Reward(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, List paramList, RewardMetaData paramRewardMetaData)
  {
    header = paramString1;
    content = paramString2;
    expiryHeader = paramString3;
    expiresAt = paramString4;
    terms = paramString5;
    milestones = paramList;
    rewardMetaData = paramRewardMetaData;
  }
  
  public final String component1()
  {
    return header;
  }
  
  public final String component2()
  {
    return content;
  }
  
  public final String component3()
  {
    return expiryHeader;
  }
  
  public final String component4()
  {
    return expiresAt;
  }
  
  public final String component5()
  {
    return terms;
  }
  
  public final List component6()
  {
    return milestones;
  }
  
  public final RewardMetaData component7()
  {
    return rewardMetaData;
  }
  
  public final Reward copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, List paramList, RewardMetaData paramRewardMetaData)
  {
    k.b(paramString1, "header");
    k.b(paramString2, "content");
    k.b(paramString3, "expiryHeader");
    k.b(paramString4, "expiresAt");
    k.b(paramString5, "terms");
    k.b(paramRewardMetaData, "rewardMetaData");
    Reward localReward = new com/truecaller/truepay/app/ui/rewards/models/Reward;
    localReward.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramList, paramRewardMetaData);
    return localReward;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Reward;
      if (bool1)
      {
        paramObject = (Reward)paramObject;
        Object localObject1 = header;
        Object localObject2 = header;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = content;
          localObject2 = content;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = expiryHeader;
            localObject2 = expiryHeader;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = expiresAt;
              localObject2 = expiresAt;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = terms;
                localObject2 = terms;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = milestones;
                  localObject2 = milestones;
                  bool1 = k.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = rewardMetaData;
                    paramObject = rewardMetaData;
                    boolean bool2 = k.a(localObject1, paramObject);
                    if (bool2) {
                      break label178;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label178:
    return true;
  }
  
  public final String getContent()
  {
    return content;
  }
  
  public final String getExpiresAt()
  {
    return expiresAt;
  }
  
  public final String getExpiryHeader()
  {
    return expiryHeader;
  }
  
  public final String getHeader()
  {
    return header;
  }
  
  public final List getMilestones()
  {
    return milestones;
  }
  
  public final RewardMetaData getRewardMetaData()
  {
    return rewardMetaData;
  }
  
  public final String getTerms()
  {
    return terms;
  }
  
  public final int hashCode()
  {
    String str = header;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = content;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = expiryHeader;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = expiresAt;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = terms;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = milestones;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = rewardMetaData;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Reward(header=");
    Object localObject = header;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", content=");
    localObject = content;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", expiryHeader=");
    localObject = expiryHeader;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", expiresAt=");
    localObject = expiresAt;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", terms=");
    localObject = terms;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", milestones=");
    localObject = milestones;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", rewardMetaData=");
    localObject = rewardMetaData;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = header;
    paramParcel.writeString((String)localObject);
    localObject = content;
    paramParcel.writeString((String)localObject);
    localObject = expiryHeader;
    paramParcel.writeString((String)localObject);
    localObject = expiresAt;
    paramParcel.writeString((String)localObject);
    localObject = terms;
    paramParcel.writeString((String)localObject);
    localObject = milestones;
    if (localObject != null)
    {
      paramParcel.writeInt(1);
      int i = ((Collection)localObject).size();
      paramParcel.writeInt(i);
      localObject = ((Collection)localObject).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject).hasNext();
        if (!bool) {
          break;
        }
        RewardMileStone localRewardMileStone = (RewardMileStone)((Iterator)localObject).next();
        localRewardMileStone.writeToParcel(paramParcel, 0);
      }
    }
    paramParcel.writeInt(0);
    rewardMetaData.writeToParcel(paramParcel, 0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.Reward
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */