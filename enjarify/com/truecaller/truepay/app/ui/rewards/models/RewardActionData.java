package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardActionData
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String title;
  private final String type;
  
  static
  {
    RewardActionData.a locala = new com/truecaller/truepay/app/ui/rewards/models/RewardActionData$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public RewardActionData(String paramString1, String paramString2)
  {
    title = paramString1;
    type = paramString2;
  }
  
  public final String component1()
  {
    return title;
  }
  
  public final String component2()
  {
    return type;
  }
  
  public final RewardActionData copy(String paramString1, String paramString2)
  {
    k.b(paramString1, "title");
    k.b(paramString2, "type");
    RewardActionData localRewardActionData = new com/truecaller/truepay/app/ui/rewards/models/RewardActionData;
    localRewardActionData.<init>(paramString1, paramString2);
    return localRewardActionData;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RewardActionData;
      if (bool1)
      {
        paramObject = (RewardActionData)paramObject;
        String str1 = title;
        String str2 = title;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = type;
          paramObject = type;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getTitle()
  {
    return title;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    String str1 = title;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = type;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RewardActionData(title=");
    String str = title;
    localStringBuilder.append(str);
    localStringBuilder.append(", type=");
    str = type;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = title;
    paramParcel.writeString(str);
    str = type;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardActionData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */