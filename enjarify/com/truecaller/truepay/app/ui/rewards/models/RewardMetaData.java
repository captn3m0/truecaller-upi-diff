package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardMetaData
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String brandColor;
  private final String content;
  private final String imageUrl;
  private final String state;
  private final String vendorName;
  
  static
  {
    RewardMetaData.a locala = new com/truecaller/truepay/app/ui/rewards/models/RewardMetaData$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public RewardMetaData(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    vendorName = paramString1;
    state = paramString2;
    content = paramString3;
    imageUrl = paramString4;
    brandColor = paramString5;
  }
  
  public final String component1()
  {
    return vendorName;
  }
  
  public final String component2()
  {
    return state;
  }
  
  public final String component3()
  {
    return content;
  }
  
  public final String component4()
  {
    return imageUrl;
  }
  
  public final String component5()
  {
    return brandColor;
  }
  
  public final RewardMetaData copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    k.b(paramString1, "vendorName");
    k.b(paramString2, "state");
    k.b(paramString3, "content");
    k.b(paramString4, "imageUrl");
    k.b(paramString5, "brandColor");
    RewardMetaData localRewardMetaData = new com/truecaller/truepay/app/ui/rewards/models/RewardMetaData;
    localRewardMetaData.<init>(paramString1, paramString2, paramString3, paramString4, paramString5);
    return localRewardMetaData;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RewardMetaData;
      if (bool1)
      {
        paramObject = (RewardMetaData)paramObject;
        String str1 = vendorName;
        String str2 = vendorName;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = state;
          str2 = state;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = content;
            str2 = content;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = imageUrl;
              str2 = imageUrl;
              bool1 = k.a(str1, str2);
              if (bool1)
              {
                str1 = brandColor;
                paramObject = brandColor;
                boolean bool2 = k.a(str1, paramObject);
                if (bool2) {
                  break label134;
                }
              }
            }
          }
        }
      }
      return false;
    }
    label134:
    return true;
  }
  
  public final String getBrandColor()
  {
    return brandColor;
  }
  
  public final String getContent()
  {
    return content;
  }
  
  public final String getImageUrl()
  {
    return imageUrl;
  }
  
  public final String getState()
  {
    return state;
  }
  
  public final String getVendorName()
  {
    return vendorName;
  }
  
  public final int hashCode()
  {
    String str1 = vendorName;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = state;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = content;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = imageUrl;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = brandColor;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RewardMetaData(vendorName=");
    String str = vendorName;
    localStringBuilder.append(str);
    localStringBuilder.append(", state=");
    str = state;
    localStringBuilder.append(str);
    localStringBuilder.append(", content=");
    str = content;
    localStringBuilder.append(str);
    localStringBuilder.append(", imageUrl=");
    str = imageUrl;
    localStringBuilder.append(str);
    localStringBuilder.append(", brandColor=");
    str = brandColor;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = vendorName;
    paramParcel.writeString(str);
    str = state;
    paramParcel.writeString(str);
    str = content;
    paramParcel.writeString(str);
    str = imageUrl;
    paramParcel.writeString(str);
    str = brandColor;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardMetaData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */