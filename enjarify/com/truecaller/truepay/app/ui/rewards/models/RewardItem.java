package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class RewardItem
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final List actionData;
  private final RewardBackground background;
  private final RewardTextAttributes content;
  private final long expiresAt;
  private final RewardTextAttributes header;
  private final RewardLogo logo;
  private final List milestones;
  private final String offerId;
  private final RewardMetaData rewardMetaData;
  private final int template;
  
  static
  {
    RewardItem.a locala = new com/truecaller/truepay/app/ui/rewards/models/RewardItem$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public RewardItem(int paramInt, String paramString, long paramLong, RewardBackground paramRewardBackground, RewardLogo paramRewardLogo, RewardTextAttributes paramRewardTextAttributes1, RewardTextAttributes paramRewardTextAttributes2, List paramList1, RewardMetaData paramRewardMetaData, List paramList2)
  {
    template = paramInt;
    offerId = paramString;
    expiresAt = paramLong;
    background = paramRewardBackground;
    logo = paramRewardLogo;
    header = paramRewardTextAttributes1;
    content = paramRewardTextAttributes2;
    milestones = paramList1;
    rewardMetaData = paramRewardMetaData;
    actionData = paramList2;
  }
  
  public final int component1()
  {
    return template;
  }
  
  public final List component10()
  {
    return actionData;
  }
  
  public final String component2()
  {
    return offerId;
  }
  
  public final long component3()
  {
    return expiresAt;
  }
  
  public final RewardBackground component4()
  {
    return background;
  }
  
  public final RewardLogo component5()
  {
    return logo;
  }
  
  public final RewardTextAttributes component6()
  {
    return header;
  }
  
  public final RewardTextAttributes component7()
  {
    return content;
  }
  
  public final List component8()
  {
    return milestones;
  }
  
  public final RewardMetaData component9()
  {
    return rewardMetaData;
  }
  
  public final RewardItem copy(int paramInt, String paramString, long paramLong, RewardBackground paramRewardBackground, RewardLogo paramRewardLogo, RewardTextAttributes paramRewardTextAttributes1, RewardTextAttributes paramRewardTextAttributes2, List paramList1, RewardMetaData paramRewardMetaData, List paramList2)
  {
    k.b(paramString, "offerId");
    k.b(paramRewardBackground, "background");
    k.b(paramRewardLogo, "logo");
    k.b(paramRewardTextAttributes1, "header");
    k.b(paramRewardTextAttributes2, "content");
    k.b(paramRewardMetaData, "rewardMetaData");
    RewardItem localRewardItem = new com/truecaller/truepay/app/ui/rewards/models/RewardItem;
    localRewardItem.<init>(paramInt, paramString, paramLong, paramRewardBackground, paramRewardLogo, paramRewardTextAttributes1, paramRewardTextAttributes2, paramList1, paramRewardMetaData, paramList2);
    return localRewardItem;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof RewardItem;
      if (bool2)
      {
        paramObject = (RewardItem)paramObject;
        int i = template;
        int j = template;
        Object localObject1;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localObject1 = null;
        }
        if (i != 0)
        {
          localObject1 = offerId;
          Object localObject2 = offerId;
          boolean bool3 = k.a(localObject1, localObject2);
          if (bool3)
          {
            long l1 = expiresAt;
            long l2 = expiresAt;
            bool3 = l1 < l2;
            if (!bool3)
            {
              bool3 = true;
            }
            else
            {
              bool3 = false;
              localObject1 = null;
            }
            if (bool3)
            {
              localObject1 = background;
              localObject2 = background;
              bool3 = k.a(localObject1, localObject2);
              if (bool3)
              {
                localObject1 = logo;
                localObject2 = logo;
                bool3 = k.a(localObject1, localObject2);
                if (bool3)
                {
                  localObject1 = header;
                  localObject2 = header;
                  bool3 = k.a(localObject1, localObject2);
                  if (bool3)
                  {
                    localObject1 = content;
                    localObject2 = content;
                    bool3 = k.a(localObject1, localObject2);
                    if (bool3)
                    {
                      localObject1 = milestones;
                      localObject2 = milestones;
                      bool3 = k.a(localObject1, localObject2);
                      if (bool3)
                      {
                        localObject1 = rewardMetaData;
                        localObject2 = rewardMetaData;
                        bool3 = k.a(localObject1, localObject2);
                        if (bool3)
                        {
                          localObject1 = actionData;
                          paramObject = actionData;
                          boolean bool4 = k.a(localObject1, paramObject);
                          if (bool4) {
                            return bool1;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final List getActionData()
  {
    return actionData;
  }
  
  public final RewardBackground getBackground()
  {
    return background;
  }
  
  public final RewardTextAttributes getContent()
  {
    return content;
  }
  
  public final long getExpiresAt()
  {
    return expiresAt;
  }
  
  public final RewardTextAttributes getHeader()
  {
    return header;
  }
  
  public final RewardLogo getLogo()
  {
    return logo;
  }
  
  public final List getMilestones()
  {
    return milestones;
  }
  
  public final String getOfferId()
  {
    return offerId;
  }
  
  public final RewardMetaData getRewardMetaData()
  {
    return rewardMetaData;
  }
  
  public final int getTemplate()
  {
    return template;
  }
  
  public final int hashCode()
  {
    int i = template * 31;
    Object localObject = offerId;
    int j = 0;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    long l1 = expiresAt;
    long l2 = l1 >>> 32;
    l1 ^= l2;
    int k = (int)l1;
    i = (i + k) * 31;
    localObject = background;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = logo;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = header;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = content;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = milestones;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = rewardMetaData;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = actionData;
    if (localObject != null) {
      j = localObject.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RewardItem(template=");
    int i = template;
    localStringBuilder.append(i);
    localStringBuilder.append(", offerId=");
    Object localObject = offerId;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", expiresAt=");
    long l = expiresAt;
    localStringBuilder.append(l);
    localStringBuilder.append(", background=");
    localObject = background;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", logo=");
    localObject = logo;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", header=");
    localObject = header;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", content=");
    localObject = content;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", milestones=");
    localObject = milestones;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", rewardMetaData=");
    localObject = rewardMetaData;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", actionData=");
    localObject = actionData;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    paramInt = template;
    paramParcel.writeInt(paramInt);
    Object localObject = offerId;
    paramParcel.writeString((String)localObject);
    long l = expiresAt;
    paramParcel.writeLong(l);
    background.writeToParcel(paramParcel, 0);
    logo.writeToParcel(paramParcel, 0);
    header.writeToParcel(paramParcel, 0);
    content.writeToParcel(paramParcel, 0);
    localObject = milestones;
    int i = 1;
    if (localObject != null)
    {
      paramParcel.writeInt(i);
      int j = ((Collection)localObject).size();
      paramParcel.writeInt(j);
      localObject = ((Collection)localObject).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject).hasNext();
        if (!bool2) {
          break;
        }
        RewardMileStone localRewardMileStone = (RewardMileStone)((Iterator)localObject).next();
        localRewardMileStone.writeToParcel(paramParcel, 0);
      }
    }
    paramParcel.writeInt(0);
    rewardMetaData.writeToParcel(paramParcel, 0);
    localObject = actionData;
    if (localObject != null)
    {
      paramParcel.writeInt(i);
      i = ((Collection)localObject).size();
      paramParcel.writeInt(i);
      localObject = ((Collection)localObject).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject).hasNext();
        if (!bool1) {
          break;
        }
        RewardActionData localRewardActionData = (RewardActionData)((Iterator)localObject).next();
        localRewardActionData.writeToParcel(paramParcel, 0);
      }
      return;
    }
    paramParcel.writeInt(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */