package com.truecaller.truepay.app.ui.rewards.models;

import c.g.b.k;
import java.util.List;

public final class RewardBaseResponse
{
  private final List rewards;
  
  public RewardBaseResponse()
  {
    this(null, 1, null);
  }
  
  public RewardBaseResponse(List paramList)
  {
    rewards = paramList;
  }
  
  public final List component1()
  {
    return rewards;
  }
  
  public final RewardBaseResponse copy(List paramList)
  {
    RewardBaseResponse localRewardBaseResponse = new com/truecaller/truepay/app/ui/rewards/models/RewardBaseResponse;
    localRewardBaseResponse.<init>(paramList);
    return localRewardBaseResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RewardBaseResponse;
      if (bool1)
      {
        paramObject = (RewardBaseResponse)paramObject;
        List localList = rewards;
        paramObject = rewards;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final List getRewards()
  {
    return rewards;
  }
  
  public final int hashCode()
  {
    List localList = rewards;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RewardBaseResponse(rewards=");
    List localList = rewards;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardBaseResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */