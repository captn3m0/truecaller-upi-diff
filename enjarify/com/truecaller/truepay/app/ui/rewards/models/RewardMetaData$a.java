package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardMetaData$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    RewardMetaData localRewardMetaData = new com/truecaller/truepay/app/ui/rewards/models/RewardMetaData;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    String str5 = paramParcel.readString();
    localRewardMetaData.<init>(str1, str2, str3, str4, str5);
    return localRewardMetaData;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new RewardMetaData[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardMetaData.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */