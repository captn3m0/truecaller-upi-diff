package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardBackground$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    RewardBackground localRewardBackground = new com/truecaller/truepay/app/ui/rewards/models/RewardBackground;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    paramParcel = paramParcel.readString();
    localRewardBackground.<init>(str1, str2, paramParcel);
    return localRewardBackground;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new RewardBackground[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardBackground.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */