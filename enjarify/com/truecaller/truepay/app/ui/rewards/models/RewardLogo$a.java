package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardLogo$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    RewardLogo localRewardLogo = new com/truecaller/truepay/app/ui/rewards/models/RewardLogo;
    paramParcel = paramParcel.readString();
    localRewardLogo.<init>(paramParcel);
    return localRewardLogo;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new RewardLogo[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardLogo.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */