package com.truecaller.truepay.app.ui.rewards.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RewardTextAttributes
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String color;
  private final String text;
  
  static
  {
    RewardTextAttributes.a locala = new com/truecaller/truepay/app/ui/rewards/models/RewardTextAttributes$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public RewardTextAttributes(String paramString1, String paramString2)
  {
    text = paramString1;
    color = paramString2;
  }
  
  public final String component1()
  {
    return text;
  }
  
  public final String component2()
  {
    return color;
  }
  
  public final RewardTextAttributes copy(String paramString1, String paramString2)
  {
    k.b(paramString1, "text");
    k.b(paramString2, "color");
    RewardTextAttributes localRewardTextAttributes = new com/truecaller/truepay/app/ui/rewards/models/RewardTextAttributes;
    localRewardTextAttributes.<init>(paramString1, paramString2);
    return localRewardTextAttributes;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RewardTextAttributes;
      if (bool1)
      {
        paramObject = (RewardTextAttributes)paramObject;
        String str1 = text;
        String str2 = text;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = color;
          paramObject = color;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getColor()
  {
    return color;
  }
  
  public final String getText()
  {
    return text;
  }
  
  public final int hashCode()
  {
    String str1 = text;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = color;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RewardTextAttributes(text=");
    String str = text;
    localStringBuilder.append(str);
    localStringBuilder.append(", color=");
    str = color;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = text;
    paramParcel.writeString(str);
    str = color;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.models.RewardTextAttributes
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */