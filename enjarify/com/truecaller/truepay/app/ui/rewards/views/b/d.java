package com.truecaller.truepay.app.ui.rewards.views.b;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.e;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.rewards.a.a.a;
import com.truecaller.truepay.app.ui.rewards.a.b;
import com.truecaller.truepay.app.ui.rewards.b.m.a;
import com.truecaller.truepay.app.ui.rewards.b.m.b;
import java.util.HashMap;

public final class d
  extends e
  implements m.b
{
  public static final d.a c;
  public m.a b;
  private HashMap d;
  
  static
  {
    d.a locala = new com/truecaller/truepay/app/ui/rewards/views/b/d$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public final void a(String paramString)
  {
    Object localObject1 = "termsText";
    k.b(paramString, (String)localObject1);
    int i = R.id.tvRewardsTerms;
    Object localObject2 = d;
    if (localObject2 == null)
    {
      localObject2 = new java/util/HashMap;
      ((HashMap)localObject2).<init>();
      d = ((HashMap)localObject2);
    }
    localObject2 = d;
    Object localObject3 = Integer.valueOf(i);
    localObject2 = (View)((HashMap)localObject2).get(localObject3);
    if (localObject2 == null)
    {
      localObject2 = getView();
      if (localObject2 == null)
      {
        i = 0;
        localObject1 = null;
      }
      else
      {
        localObject2 = ((View)localObject2).findViewById(i);
        localObject3 = d;
        localObject1 = Integer.valueOf(i);
        ((HashMap)localObject3).put(localObject1, localObject2);
      }
    }
    else
    {
      localObject1 = localObject2;
    }
    localObject1 = (TextView)localObject1;
    k.a(localObject1, "tvRewardsTerms");
    paramString = (CharSequence)paramString;
    ((TextView)localObject1).setText(paramString);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.rewards.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/app/Dialog;
    Object localObject = (Context)getActivity();
    int i = R.style.Base_ThemeOverlay_AppCompat_Dialog;
    paramBundle.<init>((Context)localObject, i);
    int j = 1;
    paramBundle.requestWindowFeature(j);
    i = R.layout.fragment_rewards_terms;
    paramBundle.setContentView(i);
    paramBundle.setCanceledOnTouchOutside(j);
    localObject = paramBundle.getWindow();
    k.a(localObject, "dialog.window");
    localObject = ((Window)localObject).getAttributes();
    gravity = 17;
    width = -1;
    height = -2;
    Window localWindow = paramBundle.getWindow();
    k.a(localWindow, "dialog.window");
    localWindow.setAttributes((WindowManager.LayoutParams)localObject);
    return paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_rewards_terms;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = getArguments();
    if (paramView != null)
    {
      str = "";
      paramView = paramView.getString("extra_reward_terms", str);
      paramBundle = b;
      if (paramBundle == null)
      {
        str = "presenter";
        k.a(str);
      }
      k.a(paramView, "termsText");
      paramBundle.a(paramView);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.views.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */