package com.truecaller.truepay.app.ui.rewards.views.a;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.rewards.models.RewardItem;
import java.util.List;

public final class a$c
  extends RecyclerView.ViewHolder
  implements View.OnClickListener
{
  final CardView a;
  final TextView b;
  final TextView c;
  final TextView d;
  final ImageView e;
  final ImageView f;
  private final a.b h;
  
  public a$c(a parama, View paramView, a.b paramb)
  {
    super(paramView);
    h = paramb;
    int i = R.id.containerRewards;
    parama = paramView.findViewById(i);
    k.a(parama, "itemLayoutView.findViewById(R.id.containerRewards)");
    parama = (CardView)parama;
    a = parama;
    i = R.id.tvHeaderReward;
    parama = paramView.findViewById(i);
    k.a(parama, "itemLayoutView.findViewById(R.id.tvHeaderReward)");
    parama = (TextView)parama;
    b = parama;
    i = R.id.tvContentReward;
    parama = paramView.findViewById(i);
    k.a(parama, "itemLayoutView.findViewById(R.id.tvContentReward)");
    parama = (TextView)parama;
    c = parama;
    i = R.id.tvExpiryReward;
    parama = paramView.findViewById(i);
    k.a(parama, "itemLayoutView.findViewById(R.id.tvExpiryReward)");
    parama = (TextView)parama;
    d = parama;
    i = R.id.ivLogoReward;
    parama = paramView.findViewById(i);
    k.a(parama, "itemLayoutView.findViewById(R.id.ivLogoReward)");
    parama = (ImageView)parama;
    e = parama;
    i = R.id.ivBackgroundReward;
    parama = paramView.findViewById(i);
    k.a(parama, "itemLayoutView.findViewB…(R.id.ivBackgroundReward)");
    parama = (ImageView)parama;
    f = parama;
    parama = this;
    parama = (View.OnClickListener)this;
    paramView.setOnClickListener(parama);
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "v");
    paramView = h;
    Object localObject = a.a(g);
    int i = getAdapterPosition();
    localObject = (RewardItem)((List)localObject).get(i);
    paramView.a((RewardItem)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.views.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */