package com.truecaller.truepay.app.ui.rewards.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.b;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.rewards.a.a.a;
import com.truecaller.truepay.app.ui.rewards.b.j.a;
import com.truecaller.truepay.app.ui.rewards.b.j.b;
import com.truecaller.truepay.app.ui.rewards.models.RewardItem;
import com.truecaller.truepay.app.ui.rewards.views.a.a.b;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class c
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements SwipeRefreshLayout.b, j.b
{
  public static final c.a c;
  public r a;
  public j.a b;
  private com.truecaller.truepay.app.ui.rewards.views.a.a d;
  private c.b e;
  private HashMap f;
  
  static
  {
    c.a locala = new com/truecaller/truepay/app/ui/rewards/views/b/c$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final int a()
  {
    return R.layout.fragment_rewards_list;
  }
  
  public final void a(RewardItem paramRewardItem)
  {
    k.b(paramRewardItem, "rewardItem");
    c.b localb = e;
    if (localb != null)
    {
      localb.a(paramRewardItem);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = (Context)getActivity();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 0).show();
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "rewards");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      com.truecaller.truepay.app.ui.rewards.views.a.a locala = new com/truecaller/truepay/app/ui/rewards/views/a/a;
      k.a(localObject1, "it");
      Object localObject2 = b;
      if (localObject2 == null)
      {
        localObject3 = "presenter";
        k.a((String)localObject3);
      }
      localObject2 = (a.b)localObject2;
      Object localObject3 = a;
      if (localObject3 == null)
      {
        String str = "imageLoader";
        k.a(str);
      }
      locala.<init>((Context)localObject1, paramList, (a.b)localObject2, (r)localObject3);
      d = locala;
      int i = R.id.rvRewards;
      paramList = (RecyclerView)a(i);
      if (paramList != null)
      {
        paramList.setHasFixedSize(true);
        localObject1 = (RecyclerView.Adapter)d;
        paramList.setAdapter((RecyclerView.Adapter)localObject1);
        return;
      }
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.layoutRewardsEmpty;
    Object localObject = (LinearLayout)a(i);
    k.a(localObject, "layoutRewardsEmpty");
    t.a((View)localObject, paramBoolean);
    i = R.id.rvRewards;
    localObject = (RecyclerView)a(i);
    k.a(localObject, "rvRewards");
    localObject = (View)localObject;
    paramBoolean ^= true;
    t.a((View)localObject, paramBoolean);
  }
  
  public final void ad_()
  {
    j.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.b();
  }
  
  public final void b()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.onBackPressed();
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "title");
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = (AppCompatActivity)localObject;
      int i = R.id.toolbar;
      Toolbar localToolbar = (Toolbar)a(i);
      ((AppCompatActivity)localObject).setSupportActionBar(localToolbar);
      localObject = getActivity();
      if (localObject != null)
      {
        localObject = ((AppCompatActivity)localObject).getSupportActionBar();
        if (localObject != null)
        {
          paramString = (CharSequence)paramString;
          ((ActionBar)localObject).setTitle(paramString);
          boolean bool = true;
          ((ActionBar)localObject).setDisplayHomeAsUpEnabled(bool);
          ((ActionBar)localObject).setDisplayShowTitleEnabled(bool);
        }
        int j = R.id.toolbar;
        paramString = (Toolbar)a(j);
        if (paramString != null)
        {
          localObject = new com/truecaller/truepay/app/ui/rewards/views/b/c$c;
          ((c.c)localObject).<init>(this);
          localObject = (View.OnClickListener)localObject;
          paramString.setNavigationOnClickListener((View.OnClickListener)localObject);
          return;
        }
        return;
      }
      paramString = new c/u;
      paramString.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
      throw paramString;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw paramString;
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.swipeRefreshLayoutFragRewards;
    SwipeRefreshLayout localSwipeRefreshLayout = (SwipeRefreshLayout)a(i);
    k.a(localSwipeRefreshLayout, "swipeRefreshLayoutFragRewards");
    localSwipeRefreshLayout.setRefreshing(paramBoolean);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof c.b;
    if (bool)
    {
      paramContext = getActivity();
      if (paramContext != null)
      {
        paramContext = (c.b)paramContext;
        e = paramContext;
        return;
      }
      paramContext = new c/u;
      paramContext.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.rewards.views.fragments.RewardsListFragment.OnFragmentInteractionListener");
      throw paramContext;
    }
    RuntimeException localRuntimeException = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" should implement the OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    localRuntimeException.<init>(paramContext);
    throw ((Throwable)localRuntimeException);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.rewards.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    String str = "inflater";
    k.b(paramLayoutInflater, str);
    paramLayoutInflater = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramViewGroup = b;
    if (paramViewGroup == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramViewGroup.a(this);
    return paramLayoutInflater;
  }
  
  public final void onDetach()
  {
    super.onDetach();
    e = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.swipeRefreshLayoutFragRewards;
    paramView = (SwipeRefreshLayout)a(i);
    paramBundle = this;
    paramBundle = (SwipeRefreshLayout.b)this;
    paramView.setOnRefreshListener(paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a();
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */