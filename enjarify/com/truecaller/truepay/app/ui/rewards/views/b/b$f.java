package com.truecaller.truepay.app.ui.rewards.views.b;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import c.g.b.k;
import com.truecaller.truepay.app.ui.rewards.b.d.a;

public final class b$f
  extends ClickableSpan
{
  b$f(b paramb, String paramString, int paramInt) {}
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "widget");
    paramView = a.e();
    String str = b;
    paramView.a(str);
  }
  
  public final void updateDrawState(TextPaint paramTextPaint)
  {
    k.b(paramTextPaint, "textPaint");
    int i = c;
    paramTextPaint.setColor(i);
    paramTextPaint.setUnderlineText(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.views.b.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */