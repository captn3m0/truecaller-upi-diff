package com.truecaller.truepay.app.ui.rewards.views.b;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import java.util.HashMap;

public final class a
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements com.truecaller.truepay.app.ui.rewards.b.a.b
{
  public static final a.a c;
  public com.truecaller.truepay.app.ui.rewards.b.a.a b;
  private final int[] d;
  private HashMap e;
  
  static
  {
    a.a locala = new com/truecaller/truepay/app/ui/rewards/views/b/a$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public a()
  {
    int[] arrayOfInt = new int[6];
    int i = Color.parseColor("#0087ff");
    arrayOfInt[0] = i;
    i = Color.parseColor("#0DC896");
    arrayOfInt[1] = i;
    i = Color.parseColor("#FCDC2A");
    arrayOfInt[2] = i;
    i = Color.parseColor("#F48429");
    arrayOfInt[3] = i;
    i = Color.parseColor("#E7265A");
    arrayOfInt[4] = i;
    i = Color.parseColor("#812B71");
    arrayOfInt[5] = i;
    d = arrayOfInt;
  }
  
  public final int a()
  {
    return R.layout.fragment_instant_reward_display;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "shareText");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.SEND");
    localIntent.setType("text/plain");
    localIntent.putExtra("android.intent.extra.TEXT", paramString);
    paramString = (CharSequence)"";
    paramString = Intent.createChooser(localIntent, paramString);
    startActivity(paramString);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "displayAmount");
    k.b(paramString2, "content");
    int i = R.id.tvAmountInstantReward;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvAmountInstantReward");
    paramString1 = (CharSequence)paramString1;
    localTextView.setText(paramString1);
    int j = R.id.tvContentInstantReward;
    paramString1 = (TextView)a(j);
    k.a(paramString1, "tvContentInstantReward");
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
  }
  
  public final void b()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final com.truecaller.truepay.app.ui.rewards.b.a.a c()
  {
    com.truecaller.truepay.app.ui.rewards.b.a.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.rewards.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    String str = "inflater";
    k.b(paramLayoutInflater, str);
    paramLayoutInflater = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramViewGroup = b;
    if (paramViewGroup == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramViewGroup.a(this);
    return paramLayoutInflater;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = "instant_reward_content";
      paramView = paramView.getString(paramBundle);
    }
    else
    {
      i = 0;
      paramView = null;
    }
    paramBundle = b;
    if (paramBundle == null)
    {
      str = "presenter";
      k.a(str);
    }
    paramBundle.a(paramView);
    int i = R.id.ivCloseInstantReward;
    paramView = (ImageView)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/rewards/views/b/a$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.btnShareInstantReward;
    paramView = (Button)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/rewards/views/b/a$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = m;
    k.a(paramView, "rootView");
    paramBundle = new com/truecaller/truepay/app/ui/rewards/views/b/a$b;
    paramBundle.<init>(paramView, this);
    paramView = paramView.getViewTreeObserver();
    paramBundle = (ViewTreeObserver.OnGlobalLayoutListener)paramBundle;
    paramView.addOnGlobalLayoutListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */