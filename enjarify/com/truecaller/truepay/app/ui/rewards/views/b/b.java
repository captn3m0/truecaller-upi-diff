package com.truecaller.truepay.app.ui.rewards.views.b;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.rewards.a.a.a;
import com.truecaller.truepay.app.ui.rewards.b.d.a;
import com.truecaller.truepay.app.ui.rewards.b.d.b;
import com.truecaller.truepay.app.ui.rewards.models.Reward;
import com.truecaller.truepay.app.ui.rewards.models.RewardActionData;
import com.truecaller.truepay.app.ui.rewards.models.RewardItem;
import com.truecaller.truepay.app.utils.j;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class b
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements d.b
{
  public static final b.a d;
  public r b;
  public d.a c;
  private b.b e;
  private HashMap f;
  
  static
  {
    b.a locala = new com/truecaller/truepay/app/ui/rewards/views/b/b$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  private View b(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final int a()
  {
    return R.layout.fragment_reward_details;
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.containerRewards;
    ((CardView)b(i)).setCardBackgroundColor(paramInt);
  }
  
  public final void a(int paramInt1, String paramString1, String paramString2, int paramInt2)
  {
    k.b(paramString1, "couponImageUrl");
    k.b(paramString2, "code");
    int i = R.id.tvRewardCouponCode;
    TextView localTextView = (TextView)b(i);
    paramString2 = (CharSequence)paramString2;
    localTextView.setText(paramString2);
    localTextView.setTextColor(paramInt1);
    r localr = b;
    if (localr == null)
    {
      paramString2 = "imageLoader";
      k.a(paramString2);
    }
    int j = R.id.ivCouponRewardUnlocked;
    paramString2 = (ImageView)b(j);
    k.a(paramString2, "ivCouponRewardUnlocked");
    localr.a(paramString1, paramString2, paramInt2, paramInt2);
  }
  
  public final void a(int paramInt, String paramString, boolean paramBoolean)
  {
    k.b(paramString, "header");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      String str = "layout_inflater";
      localObject1 = ((Context)localObject1).getSystemService(str);
      if (localObject1 != null)
      {
        localObject1 = (LayoutInflater)localObject1;
        int i = R.layout.item_reward_milestone;
        localObject1 = ((LayoutInflater)localObject1).inflate(i, null);
        i = R.id.ivMileStone;
        ((ImageView)((View)localObject1).findViewById(i)).setImageResource(paramInt);
        paramInt = R.id.tvMilestoneHeader;
        localObject2 = (TextView)((View)localObject1).findViewById(paramInt);
        k.a(localObject2, "tvMilestoneHeader");
        paramString = (CharSequence)paramString;
        ((TextView)localObject2).setText(paramString);
        paramInt = R.id.dividerMilestone;
        localObject2 = (ImageView)((View)localObject1).findViewById(paramInt);
        k.a(localObject2, "dividerMilestone");
        t.a((View)localObject2, paramBoolean);
        paramInt = R.id.containerRewardMilestones;
        ((LinearLayout)b(paramInt)).addView((View)localObject1);
        return;
      }
      Object localObject2 = new c/u;
      ((u)localObject2).<init>("null cannot be cast to non-null type android.view.LayoutInflater");
      throw ((Throwable)localObject2);
    }
  }
  
  public final void a(long paramLong)
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      int i = R.id.tvExpiryReward;
      TextView localTextView = (TextView)b(i);
      k.a(localTextView, "tvExpiryReward");
      CharSequence localCharSequence = j.b(localContext, paramLong);
      localTextView.setText(localCharSequence);
      return;
    }
  }
  
  public final void a(Reward paramReward)
  {
    k.b(paramReward, "rewardDetails");
    int i = R.id.tvRewardDetailCouponHeader;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "tvRewardDetailCouponHeader");
    CharSequence localCharSequence = (CharSequence)paramReward.getHeader();
    localTextView.setText(localCharSequence);
    i = R.id.tvRewardDetailCouponContent;
    localTextView = (TextView)b(i);
    k.a(localTextView, "tvRewardDetailCouponContent");
    localCharSequence = (CharSequence)paramReward.getContent();
    localTextView.setText(localCharSequence);
    i = R.id.tvRewardDetailExpiryHeader;
    localTextView = (TextView)b(i);
    k.a(localTextView, "tvRewardDetailExpiryHeader");
    localCharSequence = (CharSequence)paramReward.getExpiryHeader();
    localTextView.setText(localCharSequence);
    i = R.id.tvRewardDetailExpiryContent;
    localTextView = (TextView)b(i);
    k.a(localTextView, "tvRewardDetailExpiryContent");
    paramReward = (CharSequence)paramReward.getExpiresAt();
    localTextView.setText(paramReward);
  }
  
  public final void a(RewardActionData paramRewardActionData, RewardItem paramRewardItem)
  {
    k.b(paramRewardActionData, "actionData");
    k.b(paramRewardItem, "rewardItem");
    int i = R.id.layoutRewardsActionButtons;
    Object localObject1 = (LinearLayout)b(i);
    k.a(localObject1, "layoutRewardsActionButtons");
    localObject1 = (View)localObject1;
    boolean bool = true;
    t.a((View)localObject1, bool);
    i = R.id.btnRewardsPrimary;
    localObject1 = (Button)b(i);
    t.a((View)localObject1, bool);
    Object localObject2 = (CharSequence)paramRewardActionData.getTitle();
    ((Button)localObject1).setText((CharSequence)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/rewards/views/b/b$c;
    ((b.c)localObject2).<init>(this, paramRewardActionData, paramRewardItem);
    localObject2 = (View.OnClickListener)localObject2;
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = getContext();
    if (localContext != null)
    {
      localContext = getContext();
      paramString = (CharSequence)paramString;
      paramString = Toast.makeText(localContext, paramString, 0);
      paramString.show();
    }
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "title");
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      localObject1 = (AppCompatActivity)localObject1;
      int i = R.id.rewardDetailToolbar;
      Object localObject2 = (Toolbar)b(i);
      ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
      localObject1 = ((AppCompatActivity)localObject1).getSupportActionBar();
      if (localObject1 != null)
      {
        localObject2 = "this";
        k.a(localObject1, (String)localObject2);
        paramString = (CharSequence)paramString;
        ((ActionBar)localObject1).setTitle(paramString);
        boolean bool = true;
        ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(bool);
        ((ActionBar)localObject1).setDisplayShowTitleEnabled(bool);
      }
      int j = R.id.appBarLayout;
      ((AppBarLayout)b(j)).setBackgroundColor(paramInt);
      j = R.id.rewardDetailToolbar;
      paramString = (Toolbar)b(j);
      if (paramString != null)
      {
        Object localObject3 = new com/truecaller/truepay/app/ui/rewards/views/b/b$e;
        ((b.e)localObject3).<init>(this);
        localObject3 = (View.OnClickListener)localObject3;
        paramString.setNavigationOnClickListener((View.OnClickListener)localObject3);
        return;
      }
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw paramString;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "shareText");
    k.b(paramString2, "shareHeader");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.SEND");
    localIntent.setType("text/plain");
    localIntent.putExtra("android.intent.extra.TEXT", paramString1);
    paramString2 = (CharSequence)paramString2;
    paramString1 = Intent.createChooser(localIntent, paramString2);
    startActivity(paramString1);
  }
  
  public final void a(String paramString1, String paramString2, long paramLong)
  {
    k.b(paramString1, "header");
    k.b(paramString2, "content");
    int i = R.id.tvHeaderRewardUnlocked;
    Object localObject = (TextView)b(i);
    String str = "tvHeaderRewardUnlocked";
    k.a(localObject, str);
    paramString1 = (CharSequence)paramString1;
    ((TextView)localObject).setText(paramString1);
    int j = R.id.tvContentRewardUnlocked;
    paramString1 = (TextView)b(j);
    localObject = "tvContentRewardUnlocked";
    k.a(paramString1, (String)localObject);
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
    paramString1 = getContext();
    if (paramString1 != null)
    {
      int k = R.id.tvExpiryRewardUnlocked;
      paramString2 = (TextView)b(k);
      k.a(paramString2, "tvExpiryRewardUnlocked");
      paramString1 = j.b(paramString1, paramLong);
      paramString2.setText(paramString1);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    k.b(paramString1, "termsAndConditions");
    k.b(paramString2, "termsDescText");
    k.b(paramString3, "termsLinkText");
    paramString2 = (CharSequence)paramString2;
    int i = m.a(paramString2, paramString3, 0, false, 6);
    Object localObject = new android/text/SpannableStringBuilder;
    ((SpannableStringBuilder)localObject).<init>(paramString2);
    paramString2 = new com/truecaller/truepay/app/ui/rewards/views/b/b$f;
    paramString2.<init>(this, paramString1, paramInt);
    int j = paramString3.length() + i;
    ((SpannableStringBuilder)localObject).setSpan(paramString2, i, j, 0);
    j = R.id.tvRewardDetailsTermsAndConditions;
    paramString1 = (TextView)b(j);
    paramString2 = LinkMovementMethod.getInstance();
    paramString1.setMovementMethod(paramString2);
    localObject = (CharSequence)localObject;
    paramString2 = TextView.BufferType.SPANNABLE;
    paramString1.setText((CharSequence)localObject, paramString2);
    t.a((View)paramString1, true);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.layoutLockedCard;
    View localView = b(i);
    k.a(localView, "layoutLockedCard");
    t.a(localView, paramBoolean);
    i = R.id.layoutUnLockedCard;
    localView = b(i);
    k.a(localView, "layoutUnLockedCard");
    paramBoolean ^= true;
    t.a(localView, paramBoolean);
  }
  
  public final void b()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.onBackPressed();
      return;
    }
  }
  
  public final void b(RewardActionData paramRewardActionData, RewardItem paramRewardItem)
  {
    k.b(paramRewardActionData, "actionData");
    k.b(paramRewardItem, "rewardItem");
    int i = R.id.btnRewardsSecondary;
    Button localButton = (Button)b(i);
    Object localObject = localButton;
    t.a((View)localButton, true);
    localObject = (CharSequence)paramRewardActionData.getTitle();
    localButton.setText((CharSequence)localObject);
    localObject = new com/truecaller/truepay/app/ui/rewards/views/b/b$d;
    ((b.d)localObject).<init>(this, paramRewardActionData, paramRewardItem);
    localObject = (View.OnClickListener)localObject;
    localButton.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "termsAndConditions");
    b.b localb = e;
    if (localb != null)
    {
      localb.b(paramString);
      return;
    }
  }
  
  public final void b(String paramString, int paramInt)
  {
    k.b(paramString, "header");
    int i = R.id.tvHeaderReward;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "tvHeaderReward");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    int j = R.id.tvHeaderReward;
    ((TextView)b(j)).setTextColor(paramInt);
    j = R.id.tvExpiryReward;
    ((TextView)b(j)).setTextColor(paramInt);
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.rewardDetailsProgress;
    Object localObject = (ProgressBar)b(i);
    if (localObject != null)
    {
      localObject = (View)localObject;
      t.a((View)localObject, paramBoolean);
    }
    i = R.id.dividerRewardDetails;
    localObject = b(i);
    if (localObject != null)
    {
      paramBoolean ^= true;
      t.a((View)localObject, paramBoolean);
      return;
    }
  }
  
  public final void c()
  {
    int i = R.id.containerRewardMilestones;
    LinearLayout localLinearLayout = (LinearLayout)b(i);
    if (localLinearLayout != null)
    {
      localLinearLayout.setVisibility(0);
      return;
    }
  }
  
  public final void c(String paramString, int paramInt)
  {
    k.b(paramString, "content");
    int i = R.id.tvContentReward;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "tvContentReward");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    int j = R.id.tvContentReward;
    ((TextView)b(j)).setTextColor(paramInt);
  }
  
  public final void d()
  {
    int i = R.id.containerRewardMilestones;
    LinearLayout localLinearLayout = (LinearLayout)b(i);
    if (localLinearLayout != null)
    {
      localLinearLayout.setVisibility(8);
      return;
    }
  }
  
  public final void d(String paramString, int paramInt)
  {
    k.b(paramString, "backgroundImageUrl");
    r localr = b;
    if (localr == null)
    {
      localObject = "imageLoader";
      k.a((String)localObject);
    }
    int i = R.id.ivBackgroundReward;
    Object localObject = (ImageView)b(i);
    k.a(localObject, "ivBackgroundReward");
    localr.a(paramString, (ImageView)localObject, paramInt, paramInt);
  }
  
  public final d.a e()
  {
    d.a locala = c;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void e(String paramString, int paramInt)
  {
    k.b(paramString, "logoUrl");
    r localr = b;
    if (localr == null)
    {
      localObject = "imageLoader";
      k.a((String)localObject);
    }
    int i = R.id.ivLogoReward;
    Object localObject = (ImageView)b(i);
    k.a(localObject, "ivLogoReward");
    localr.a(paramString, (ImageView)localObject, paramInt, paramInt);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof b.b;
    if (bool)
    {
      paramContext = getActivity();
      if (paramContext != null)
      {
        paramContext = (b.b)paramContext;
        e = paramContext;
        return;
      }
      paramContext = new c/u;
      paramContext.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.rewards.views.fragments.RewardDetailsFragment.OnFragmentInteractionListener");
      throw paramContext;
    }
    RuntimeException localRuntimeException = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" should implement the OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    localRuntimeException.<init>(paramContext);
    throw ((Throwable)localRuntimeException);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.rewards.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    String str = "inflater";
    k.b(paramLayoutInflater, str);
    paramLayoutInflater = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramViewGroup = c;
    if (paramViewGroup == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramViewGroup.a(this);
    return paramLayoutInflater;
  }
  
  public final void onDetach()
  {
    super.onDetach();
    e = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = getArguments();
    if (paramView == null) {
      return;
    }
    k.a(paramView, "arguments ?: return");
    paramView = (RewardItem)paramView.getParcelable("extra_reward_item");
    paramBundle = c;
    if (paramBundle == null)
    {
      str = "presenter";
      k.a(str);
    }
    k.a(paramView, "rewardItem");
    paramBundle.a(paramView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.views.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */