package com.truecaller.truepay.app.ui.rewards.views.a;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView.Adapter;
import com.truecaller.truepay.app.ui.rewards.models.RewardItem;
import com.truecaller.truepay.app.utils.r;
import java.util.List;

public final class a
  extends RecyclerView.Adapter
{
  public static final a.a a;
  private final int b;
  private final int c;
  private final Context d;
  private final List e;
  private final a.b f;
  private final r g;
  
  static
  {
    a.a locala = new com/truecaller/truepay/app/ui/rewards/views/a/a$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public a(Context paramContext, List paramList, a.b paramb, r paramr)
  {
    d = paramContext;
    e = paramList;
    f = paramb;
    g = paramr;
    int i = Color.parseColor("#ffffff");
    b = i;
    i = Color.parseColor("#000000");
    c = i;
  }
  
  private final int a(String paramString)
  {
    int i;
    try
    {
      i = Color.parseColor(paramString);
    }
    catch (Exception localException)
    {
      i = b;
    }
    return i;
  }
  
  private final int b(String paramString)
  {
    int i;
    try
    {
      i = Color.parseColor(paramString);
    }
    catch (Exception localException)
    {
      i = c;
    }
    return i;
  }
  
  public final int getItemCount()
  {
    return e.size();
  }
  
  public final int getItemViewType(int paramInt)
  {
    List localList = e;
    RewardItem localRewardItem = (RewardItem)localList.get(paramInt);
    paramInt = localRewardItem.getTemplate();
    switch (paramInt)
    {
    default: 
      return -1;
    case 2: 
      return 1;
    }
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */