package com.truecaller.truepay.app.ui.rewards.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.rewards.a.a.a;
import com.truecaller.truepay.app.ui.rewards.b.g.a;
import com.truecaller.truepay.app.ui.rewards.b.g.b;
import com.truecaller.truepay.app.ui.rewards.models.RewardItem;
import com.truecaller.truepay.app.ui.rewards.views.b.b.b;
import com.truecaller.truepay.app.ui.rewards.views.b.c;
import com.truecaller.truepay.app.ui.rewards.views.b.c.b;
import com.truecaller.truepay.app.ui.rewards.views.b.d;

public final class RewardsActivity
  extends com.truecaller.truepay.app.ui.base.views.a.b
  implements g.b, b.b, c.b
{
  public static final RewardsActivity.a b;
  public g.a a;
  
  static
  {
    RewardsActivity.a locala = new com/truecaller/truepay/app/ui/rewards/views/activities/RewardsActivity$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public static final Intent a(Context paramContext)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, RewardsActivity.class);
    return localIntent;
  }
  
  private void a(Fragment paramFragment, boolean paramBoolean)
  {
    k.b(paramFragment, "fragment");
    o localo = getSupportFragmentManager().a();
    String str1 = "supportFragmentManager.beginTransaction()";
    k.a(localo, str1);
    if (paramBoolean)
    {
      String str2 = paramFragment.getClass().getSimpleName();
      localo.a(str2);
    }
    paramBoolean = R.id.rewards_container;
    str1 = paramFragment.getClass().getSimpleName();
    localo.a(paramBoolean, paramFragment, str1).d();
  }
  
  public final void a()
  {
    finish();
  }
  
  public final void a(RewardItem paramRewardItem)
  {
    k.b(paramRewardItem, "rewardItem");
    Object localObject = com.truecaller.truepay.app.ui.rewards.views.b.b.d;
    k.b(paramRewardItem, "rewardItem");
    localObject = new android/os/Bundle;
    ((Bundle)localObject).<init>();
    paramRewardItem = (Parcelable)paramRewardItem;
    ((Bundle)localObject).putParcelable("extra_reward_item", paramRewardItem);
    paramRewardItem = new com/truecaller/truepay/app/ui/rewards/views/b/b;
    paramRewardItem.<init>();
    paramRewardItem.setArguments((Bundle)localObject);
    paramRewardItem = (Fragment)paramRewardItem;
    a(paramRewardItem, true);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    int i = getLayoutId();
    setContentView(i);
    Object localObject = com.truecaller.truepay.app.ui.rewards.views.b.a.c;
    k.b(paramString, "title");
    localObject = new android/os/Bundle;
    ((Bundle)localObject).<init>();
    ((Bundle)localObject).putString("instant_reward_content", paramString);
    paramString = new com/truecaller/truepay/app/ui/rewards/views/b/a;
    paramString.<init>();
    paramString.setArguments((Bundle)localObject);
    paramString = (Fragment)paramString;
    a(paramString, false);
  }
  
  public final void b()
  {
    int i = R.style.AppTheme_NoTitleBar;
    setTheme(i);
    i = getLayoutId();
    setContentView(i);
    Object localObject1 = c.c;
    localObject1 = new android/os/Bundle;
    ((Bundle)localObject1).<init>();
    Object localObject2 = new com/truecaller/truepay/app/ui/rewards/views/b/c;
    ((c)localObject2).<init>();
    ((c)localObject2).setArguments((Bundle)localObject1);
    localObject2 = (Fragment)localObject2;
    a((Fragment)localObject2, false);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "termsAndConditions");
    Object localObject = d.c;
    k.b(paramString, "terms");
    localObject = new android/os/Bundle;
    ((Bundle)localObject).<init>();
    ((Bundle)localObject).putString("extra_reward_terms", paramString);
    paramString = new com/truecaller/truepay/app/ui/rewards/views/b/d;
    paramString.<init>();
    paramString.setArguments((Bundle)localObject);
    localObject = getSupportFragmentManager().a();
    paramString = (Fragment)paramString;
    String str = d.class.getSimpleName();
    ((o)localObject).a(paramString, str).d();
  }
  
  public final int getLayoutId()
  {
    return R.layout.activity_rewards;
  }
  
  public final void initDagger(com.truecaller.truepay.app.a.a.a parama)
  {
    parama = com.truecaller.truepay.app.ui.rewards.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    parama.a(locala).a().a(this);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = a;
    if (paramBundle == null)
    {
      str1 = "presenter";
      k.a(str1);
    }
    paramBundle.a(this);
    paramBundle = getIntent();
    k.a(paramBundle, "intent");
    paramBundle = paramBundle.getExtras();
    String str1 = null;
    String str2;
    boolean bool;
    Boolean localBoolean;
    if (paramBundle != null)
    {
      str2 = null;
      bool = paramBundle.getBoolean("show_instant_reward", false);
      localBoolean = Boolean.valueOf(bool);
    }
    else
    {
      bool = false;
      localBoolean = null;
    }
    if (paramBundle != null) {
      str1 = paramBundle.getString("instant_reward_content");
    }
    paramBundle = a;
    if (paramBundle == null)
    {
      str2 = "presenter";
      k.a(str2);
    }
    paramBundle.a(localBoolean, str1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.views.activities.RewardsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */