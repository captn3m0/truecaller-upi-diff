package com.truecaller.truepay.app.ui.rewards.views.b;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.id;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import nl.dionsegijn.konfetti.KonfettiView;

public final class a$b
  implements ViewTreeObserver.OnGlobalLayoutListener
{
  public a$b(View paramView, a parama) {}
  
  public final void onGlobalLayout()
  {
    Object localObject1 = a.getViewTreeObserver();
    Object localObject2 = this;
    localObject2 = (ViewTreeObserver.OnGlobalLayoutListener)this;
    ((ViewTreeObserver)localObject1).removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject2);
    localObject1 = b;
    int i = R.id.viewKonfetti;
    localObject1 = (KonfettiView)((a)localObject1).a(i);
    localObject2 = new nl/dionsegijn/konfetti/c;
    ((nl.dionsegijn.konfetti.c)localObject2).<init>((KonfettiView)localObject1);
    localObject1 = a.a(b);
    int j = localObject1.length;
    localObject1 = Arrays.copyOf((int[])localObject1, j);
    k.b(localObject1, "colors");
    c = ((int[])localObject1);
    localObject1 = b;
    double d = Math.toRadians(0.0D);
    a = d;
    localObject1 = b;
    long l1 = 4645023210981556224L;
    d = Math.toRadians(359.0D);
    Object localObject3 = Double.valueOf(d);
    b = ((Double)localObject3);
    b.c = 1.0F;
    localObject1 = b;
    j = 1084227584;
    float f1 = 5.0F;
    localObject3 = Float.valueOf(f1);
    if (localObject3 == null) {
      k.a();
    }
    float f2 = ((Float)localObject3).floatValue();
    Object localObject4 = null;
    boolean bool1 = f2 < 0.0F;
    if (bool1) {
      localObject3 = Float.valueOf(0.0F);
    }
    d = ((Float)localObject3);
    localObject1 = f;
    j = 1;
    f1 = Float.MIN_VALUE;
    a = j;
    localObject1 = f;
    long l2 = 2000L;
    b = l2;
    int m = 2;
    Object localObject5 = new nl.dionsegijn.konfetti.c.b[m];
    localObject4 = nl.dionsegijn.konfetti.c.b.a;
    nl.dionsegijn.konfetti.d.b localb = null;
    localObject5[0] = localObject4;
    localObject4 = nl.dionsegijn.konfetti.c.b.b;
    localObject5[j] = localObject4;
    k.b(localObject5, "shapes");
    localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject4 = (Collection)localObject4;
    int n = 0;
    nl.dionsegijn.konfetti.c.c[] arrayOfc = null;
    Object localObject6;
    while (n < m)
    {
      localObject6 = localObject5[n];
      boolean bool2 = localObject6 instanceof nl.dionsegijn.konfetti.c.b;
      if (bool2) {
        ((Collection)localObject4).add(localObject6);
      }
      n += 1;
    }
    localObject4 = (Collection)localObject4;
    localObject1 = new nl.dionsegijn.konfetti.c.b[0];
    localObject1 = ((Collection)localObject4).toArray((Object[])localObject1);
    if (localObject1 != null)
    {
      localObject1 = (nl.dionsegijn.konfetti.c.b[])localObject1;
      e = ((nl.dionsegijn.konfetti.c.b[])localObject1);
      localObject1 = new nl.dionsegijn.konfetti.c.c[j];
      localObject3 = new nl/dionsegijn/konfetti/c/c;
      int k = 6;
      f2 = 8.4E-45F;
      ((nl.dionsegijn.konfetti.c.c)localObject3).<init>(k);
      localObject1[0] = localObject3;
      localObject1 = ((nl.dionsegijn.konfetti.c)localObject2).a((nl.dionsegijn.konfetti.c.c[])localObject1);
      localObject2 = b;
      j = R.id.viewKonfetti;
      localObject2 = (KonfettiView)((a)localObject2).a(j);
      k.a(localObject2, "viewKonfetti");
      i = ((KonfettiView)localObject2).getWidth();
      float f3 = i + 50.0F;
      localObject2 = Float.valueOf(f3);
      f1 = -50.0F;
      localObject5 = Float.valueOf(f1);
      localObject4 = a;
      a = f1;
      b = ((Float)localObject2);
      localObject2 = a;
      c = f1;
      d = ((Float)localObject5);
      localObject2 = new nl/dionsegijn/konfetti/a/c;
      ((nl.dionsegijn.konfetti.a.c)localObject2).<init>();
      b = -1;
      l1 = 5000L;
      d = 2.4703E-320D;
      c = l1;
      j = 995783694;
      f1 = 0.0033333334F;
      d = f1;
      Object localObject7 = localObject2;
      localObject7 = (nl.dionsegijn.konfetti.a.a)localObject2;
      localObject2 = new nl/dionsegijn/konfetti/a/b;
      localObject4 = a;
      localb = b;
      arrayOfc = d;
      localObject6 = e;
      int[] arrayOfInt = c;
      nl.dionsegijn.konfetti.c.a locala = f;
      localObject5 = localObject2;
      ((nl.dionsegijn.konfetti.a.b)localObject2).<init>((nl.dionsegijn.konfetti.d.a)localObject4, localb, arrayOfc, (nl.dionsegijn.konfetti.c.b[])localObject6, arrayOfInt, locala, (nl.dionsegijn.konfetti.a.a)localObject7);
      g = ((nl.dionsegijn.konfetti.a.b)localObject2);
      localObject2 = h;
      k.b(localObject1, "particleSystem");
      localObject3 = a;
      ((List)localObject3).add(localObject1);
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = a;
        ((List)localObject1).size();
      }
      ((KonfettiView)localObject2).invalidate();
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.rewards.views.b.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */