package com.truecaller.truepay.app.ui.history.b;

import android.support.v4.f.j;
import com.truecaller.truepay.a.a.c.c;
import com.truecaller.truepay.a.a.c.e;
import io.reactivex.a.b;
import io.reactivex.n;

public final class d
  extends com.truecaller.truepay.app.ui.base.a.a
{
  private final c a;
  private final e b;
  
  public d(c paramc, e parame)
  {
    a = paramc;
    b = parame;
  }
  
  public final void a()
  {
    Object localObject1 = b.a();
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.d)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.d)localObject1).a((n)localObject2);
    localObject2 = -..Lambda.d.yr8QuySypIlrTNE2laqpzyULVzk.INSTANCE;
    -..Lambda.d.0HGudqRNN8u_3i4wJy42eUS1Ho4 local0HGudqRNN8u_3i4wJy42eUS1Ho4 = new com/truecaller/truepay/app/ui/history/b/-$$Lambda$d$0HGudqRNN8u_3i4wJy42eUS1Ho4;
    local0HGudqRNN8u_3i4wJy42eUS1Ho4.<init>(this);
    -..Lambda.d.T09e7TIUnxvp1FxFNI1W8G5J1eM localT09e7TIUnxvp1FxFNI1W8G5J1eM = -..Lambda.d.T09e7TIUnxvp1FxFNI1W8G5J1eM.INSTANCE;
    -..Lambda.d.E7Md1VeM-NE8kyP5H6_P3t9z-w8 localE7Md1VeM-NE8kyP5H6_P3t9z-w8 = -..Lambda.d.E7Md1VeM-NE8kyP5H6_P3t9z-w8.INSTANCE;
    localObject1 = ((io.reactivex.d)localObject1).a((io.reactivex.c.d)localObject2, local0HGudqRNN8u_3i4wJy42eUS1Ho4, localT09e7TIUnxvp1FxFNI1W8G5J1eM, localE7Md1VeM-NE8kyP5H6_P3t9z-w8);
    e.a((b)localObject1);
  }
  
  public final void a(String paramString)
  {
    Object localObject1 = a;
    Object localObject2 = new android/support/v4/f/j;
    ((j)localObject2).<init>(null, paramString);
    paramString = ((c)localObject1).a((j)localObject2);
    localObject1 = io.reactivex.g.a.b();
    paramString = paramString.b((n)localObject1);
    localObject1 = io.reactivex.android.b.a.a();
    paramString = paramString.a((n)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/history/b/-$$Lambda$d$SkgG8N1whoCykPhZEHfFlcc4Ssw;
    ((-..Lambda.d.SkgG8N1whoCykPhZEHfFlcc4Ssw)localObject1).<init>(this);
    localObject2 = new com/truecaller/truepay/app/ui/history/b/-$$Lambda$d$n5ms_4P8dW2vHDO2d8KRhtPI-io;
    ((-..Lambda.d.n5ms_4P8dW2vHDO2d8KRhtPI-io)localObject2).<init>(this);
    -..Lambda.d.k0tXX6yYbGnUwnKlro_8VuBiJtI localk0tXX6yYbGnUwnKlro_8VuBiJtI = -..Lambda.d.k0tXX6yYbGnUwnKlro_8VuBiJtI.INSTANCE;
    -..Lambda.d.WpioRLj0gs9S74lqUjMugCGbM1A localWpioRLj0gs9S74lqUjMugCGbM1A = new com/truecaller/truepay/app/ui/history/b/-$$Lambda$d$WpioRLj0gs9S74lqUjMugCGbM1A;
    localWpioRLj0gs9S74lqUjMugCGbM1A.<init>(this);
    paramString = paramString.a((io.reactivex.c.d)localObject1, (io.reactivex.c.d)localObject2, localk0tXX6yYbGnUwnKlro_8VuBiJtI, localWpioRLj0gs9S74lqUjMugCGbM1A);
    e.a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */