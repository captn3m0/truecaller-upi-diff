package com.truecaller.truepay.app.ui.history.b;

import c.d.c;
import c.o.b;
import c.x;
import com.google.gson.f;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import com.truecaller.truepay.app.utils.ax;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class e$a$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag c;
  
  e$a$a(e.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/truepay/app/ui/history/b/e$a$a;
    e.a locala1 = b;
    locala.<init>(locala1, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = e.b(b.c).h("upreftils_v1");
        bool1 = false;
        localObject1 = null;
        if (paramObject != null)
        {
          paramObject = ((com.truecaller.truepay.app.ui.payments.models.a)paramObject).c();
          if (paramObject != null)
          {
            paramObject = ((Iterable)paramObject).iterator();
            boolean bool3;
            Object localObject3;
            String str1;
            String str2;
            boolean bool4;
            do
            {
              bool2 = ((Iterator)paramObject).hasNext();
              bool3 = true;
              if (!bool2) {
                break;
              }
              localObject2 = ((Iterator)paramObject).next();
              localObject3 = localObject2;
              localObject3 = (com.truecaller.truepay.app.ui.payments.models.a)localObject2;
              str1 = "apps";
              str2 = "it";
              c.g.b.k.a(localObject3, str2);
              localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).a();
              bool4 = c.n.m.a(str1, (String)localObject3, bool3);
            } while (!bool4);
            break label147;
            boolean bool2 = false;
            Object localObject2 = null;
            label147:
            localObject2 = (com.truecaller.truepay.app.ui.payments.models.a)localObject2;
            if (localObject2 != null)
            {
              paramObject = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).c();
              if (paramObject != null)
              {
                paramObject = ((Iterable)paramObject).iterator();
                do
                {
                  bool2 = ((Iterator)paramObject).hasNext();
                  if (!bool2) {
                    break;
                  }
                  localObject2 = ((Iterator)paramObject).next();
                  localObject3 = localObject2;
                  localObject3 = (com.truecaller.truepay.app.ui.payments.models.a)localObject2;
                  str1 = "ixigo";
                  str2 = "it";
                  c.g.b.k.a(localObject3, str2);
                  localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).a();
                  bool4 = c.n.m.a(str1, (String)localObject3, bool3);
                } while (!bool4);
                break label258;
                bool2 = false;
                localObject2 = null;
                label258:
                paramObject = localObject2;
                paramObject = (com.truecaller.truepay.app.ui.payments.models.a)localObject2;
                if (paramObject != null)
                {
                  localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)paramObject).c();
                  if (localObject2 != null)
                  {
                    localObject2 = (Collection)localObject2;
                    bool2 = ((Collection)localObject2).isEmpty() ^ bool3;
                    if (bool2 == bool3) {}
                  }
                  else
                  {
                    bool3 = false;
                  }
                  if (!bool3) {
                    paramObject = null;
                  }
                  if (paramObject != null)
                  {
                    localObject1 = e.c(b.c);
                    localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)paramObject).c();
                    c.g.b.k.a(localObject2, "it.utilityFields");
                    localObject2 = c.a.m.d((List)localObject2);
                    c.g.b.k.a(localObject2, "it.utilityFields.first()");
                    localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).s();
                    localObject1 = (WebAppConfig)((f)localObject1).a((String)localObject2, WebAppConfig.class);
                    ((com.truecaller.truepay.app.ui.payments.models.a)paramObject).a((WebAppConfig)localObject1);
                    return paramObject;
                  }
                }
              }
            }
          }
        }
        return null;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.b.e.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */