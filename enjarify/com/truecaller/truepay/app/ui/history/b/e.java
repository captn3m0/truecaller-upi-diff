package com.truecaller.truepay.app.ui.history.b;

import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.truepay.app.ui.history.views.e.b.a;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.data.b.a;
import com.truecaller.utils.n;

public final class e
  extends ba
  implements b.a
{
  private final c.d.f c;
  private final c.d.f d;
  private final ax e;
  private final com.google.gson.f f;
  private final n g;
  private final a h;
  
  public e(c.d.f paramf1, c.d.f paramf2, ax paramax, com.google.gson.f paramf, n paramn, a parama)
  {
    super(paramf2);
    c = paramf1;
    d = paramf2;
    e = paramax;
    f = paramf;
    g = paramn;
    h = parama;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "detailsUrl");
    k.b(paramString2, "action");
    Object localObject = new com/truecaller/truepay/app/ui/history/b/e$a;
    ((e.a)localObject).<init>(this, paramString2, paramString1, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */