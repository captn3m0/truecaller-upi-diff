package com.truecaller.truepay.app.ui.history.b;

import android.text.TextUtils;
import com.google.gson.f;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.history.models.RedbusBookingDetail;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.history.models.o;
import com.truecaller.truepay.app.ui.webapps.ixigo.IxigoBookingDetail;
import com.truecaller.truepay.app.utils.g;
import java.lang.reflect.Type;
import java.util.List;

public final class b
  extends com.truecaller.truepay.app.ui.base.a.a
  implements a
{
  public com.truecaller.truepay.a.a.d.d a;
  public com.truecaller.truepay.a.a.c.e b;
  public com.truecaller.truepay.data.e.e c;
  public com.truecaller.utils.n f;
  public g g;
  public f h;
  
  public final void a()
  {
    Object localObject1 = b.a();
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.d)localObject1).b((io.reactivex.n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.d)localObject1).a((io.reactivex.n)localObject2);
    localObject2 = -..Lambda.b.nqJH2eYcc3QMhUAbIvfBldNZ_VI.INSTANCE;
    -..Lambda.b.QrQBrNAfUuWLDbQX16MoXZ6NrIM localQrQBrNAfUuWLDbQX16MoXZ6NrIM = -..Lambda.b.QrQBrNAfUuWLDbQX16MoXZ6NrIM.INSTANCE;
    -..Lambda.b.GSG03var36f2wl1MwhmTFksnGcc localGSG03var36f2wl1MwhmTFksnGcc = -..Lambda.b.GSG03var36f2wl1MwhmTFksnGcc.INSTANCE;
    -..Lambda.b.HlE5Pn8vm5hYCj1OSzdPRtwXD6o localHlE5Pn8vm5hYCj1OSzdPRtwXD6o = -..Lambda.b.HlE5Pn8vm5hYCj1OSzdPRtwXD6o.INSTANCE;
    localObject1 = ((io.reactivex.d)localObject1).a((io.reactivex.c.d)localObject2, localQrQBrNAfUuWLDbQX16MoXZ6NrIM, localGSG03var36f2wl1MwhmTFksnGcc, localHlE5Pn8vm5hYCj1OSzdPRtwXD6o);
    e.a((io.reactivex.a.b)localObject1);
  }
  
  public final void a(h paramh, String paramString, Boolean paramBoolean)
  {
    Object localObject1 = Truepay.getInstance().getAnalyticLoggerHelper();
    ((com.truecaller.truepay.data.b.a)localObject1).b(paramString);
    paramString = d;
    if (paramString != null)
    {
      paramString = w;
      if (paramString != null)
      {
        paramString = w.x;
        if (paramString != null)
        {
          paramString = new com/truecaller/truepay/app/ui/history/b/b$3;
          paramString.<init>(this);
          paramString = b;
          localObject1 = h;
          Object localObject2 = w.x;
          paramString = (com.truecaller.truepay.app.ui.history.models.c)((RedbusBookingDetail)((f)localObject1).a((String)localObject2, paramString)).getDetails().get(0);
          localObject1 = d;
          localObject2 = localObject1;
          localObject2 = (com.truecaller.truepay.app.ui.history.views.e.c)localObject1;
          String str1 = w.f;
          localObject1 = w;
          String str2 = r;
          paramh = w;
          String str3 = e;
          String str4 = a;
          ((com.truecaller.truepay.app.ui.history.views.e.c)localObject2).a(str1, str2, str3, str4, paramBoolean);
        }
      }
    }
  }
  
  public final void a(o paramo)
  {
    Object localObject1 = d;
    if (localObject1 != null)
    {
      if (paramo != null)
      {
        localObject1 = "success";
        Object localObject2 = c;
        boolean bool1 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (bool1)
        {
          localObject1 = x;
          if (localObject1 != null)
          {
            localObject1 = new com/truecaller/truepay/app/ui/history/b/b$2;
            ((b.2)localObject1).<init>(this);
            localObject1 = b;
            localObject2 = h;
            paramo = x;
            paramo = (RedbusBookingDetail)((f)localObject2).a(paramo, (Type)localObject1);
            localObject1 = paramo.getDetails();
            int i = 0;
            localObject2 = null;
            localObject1 = (com.truecaller.truepay.app.ui.history.models.c)((List)localObject1).get(0);
            Object localObject3 = d;
            Object localObject4 = localObject3;
            localObject4 = (com.truecaller.truepay.app.ui.history.views.e.c)localObject3;
            localObject3 = f;
            int j = R.string.ticket_number;
            Object localObject5 = new Object[0];
            String str1 = ((com.truecaller.utils.n)localObject3).a(j, (Object[])localObject5);
            localObject5 = a;
            String str2 = f;
            int k = R.drawable.ic_bus_history;
            String str3 = b;
            String str4 = c;
            String str5 = d;
            String str6 = e;
            ((com.truecaller.truepay.app.ui.history.views.e.c)localObject4).a(str1, (String)localObject5, str2, k, str3, str4, str5, str6);
            ((com.truecaller.truepay.app.ui.history.views.e.c)d).b();
            ((com.truecaller.truepay.app.ui.history.views.e.c)d).c();
            localObject1 = "success";
            paramo = paramo.getCancel_status();
            boolean bool2 = ((String)localObject1).equalsIgnoreCase(paramo);
            if (bool2)
            {
              paramo = (com.truecaller.truepay.app.ui.history.views.e.c)d;
              localObject1 = f;
              int m = R.string.booking_cancelled_status;
              localObject2 = new Object[0];
              localObject1 = ((com.truecaller.utils.n)localObject1).a(m, (Object[])localObject2);
              localObject2 = g;
              m = R.color.status_failure_color;
              i = ((g)localObject2).a(m);
              paramo.a((String)localObject1, i);
            }
            return;
          }
        }
      }
      paramo = (com.truecaller.truepay.app.ui.history.views.e.c)d;
      paramo.e();
    }
  }
  
  public final void a(String paramString)
  {
    Object localObject = d;
    if (localObject != null)
    {
      boolean bool = TextUtils.isEmpty(paramString);
      if (!bool)
      {
        localObject = (com.truecaller.truepay.app.ui.history.views.e.c)d;
        ((com.truecaller.truepay.app.ui.history.views.e.c)localObject).b(paramString);
      }
    }
  }
  
  public final void b(o paramo)
  {
    Object localObject1 = d;
    if (localObject1 != null)
    {
      if (paramo != null)
      {
        localObject1 = "success";
        Object localObject2 = c;
        boolean bool1 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (bool1)
        {
          localObject1 = x;
          if (localObject1 != null)
          {
            localObject1 = h;
            paramo = x;
            localObject2 = IxigoBookingDetail.class;
            paramo = (IxigoBookingDetail)((f)localObject1).a(paramo, (Class)localObject2);
            ((com.truecaller.truepay.app.ui.history.views.e.c)d).a(paramo);
            ((com.truecaller.truepay.app.ui.history.views.e.c)d).d();
            ((com.truecaller.truepay.app.ui.history.views.e.c)d).e();
            localObject1 = "success";
            paramo = paramo.getCancel_status();
            boolean bool2 = ((String)localObject1).equalsIgnoreCase(paramo);
            if (bool2)
            {
              paramo = (com.truecaller.truepay.app.ui.history.views.e.c)d;
              localObject1 = f;
              int i = R.string.booking_cancelled_status;
              Object[] arrayOfObject = new Object[0];
              localObject1 = ((com.truecaller.utils.n)localObject1).a(i, arrayOfObject);
              localObject2 = g;
              int j = R.color.status_failure_color;
              i = ((g)localObject2).a(j);
              paramo.a((String)localObject1, i);
            }
            return;
          }
        }
      }
      paramo = (com.truecaller.truepay.app.ui.history.views.e.c)d;
      paramo.c();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */