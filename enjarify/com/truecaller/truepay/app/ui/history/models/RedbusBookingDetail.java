package com.truecaller.truepay.app.ui.history.models;

import c.g.b.k;
import java.util.List;

public final class RedbusBookingDetail
{
  private final String cancel_status;
  private final List details;
  
  public RedbusBookingDetail(String paramString, List paramList)
  {
    cancel_status = paramString;
    details = paramList;
  }
  
  public final String component1()
  {
    return cancel_status;
  }
  
  public final List component2()
  {
    return details;
  }
  
  public final RedbusBookingDetail copy(String paramString, List paramList)
  {
    k.b(paramString, "cancel_status");
    k.b(paramList, "details");
    RedbusBookingDetail localRedbusBookingDetail = new com/truecaller/truepay/app/ui/history/models/RedbusBookingDetail;
    localRedbusBookingDetail.<init>(paramString, paramList);
    return localRedbusBookingDetail;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RedbusBookingDetail;
      if (bool1)
      {
        paramObject = (RedbusBookingDetail)paramObject;
        Object localObject = cancel_status;
        String str = cancel_status;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = details;
          paramObject = details;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getCancel_status()
  {
    return cancel_status;
  }
  
  public final List getDetails()
  {
    return details;
  }
  
  public final int hashCode()
  {
    String str = cancel_status;
    int i = 0;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    List localList = details;
    if (localList != null) {
      i = localList.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RedbusBookingDetail(cancel_status=");
    Object localObject = cancel_status;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", details=");
    localObject = details;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.models.RedbusBookingDetail
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */