package com.truecaller.truepay.app.ui.history.models;

import java.io.Serializable;
import java.util.List;

public final class h
  implements com.truecaller.truepay.app.ui.payments.c.h, Serializable
{
  public List a;
  public String b;
  public String c;
  public String d;
  public String e;
  public String f;
  public String g;
  public String h;
  public String i;
  public String j;
  public String k;
  public String l;
  public String m;
  public String n;
  public n o;
  public k p;
  public String q;
  public String r;
  public String s;
  public String t;
  public String u;
  public long v;
  public o w;
  private String x;
  private String y;
  
  public h(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, n paramn, String paramString13, List paramList, String paramString14, String paramString15, k paramk, String paramString16, String paramString17, String paramString18, long paramLong, o paramo, String paramString19)
  {
    b = paramString1;
    e = paramString2;
    f = paramString3;
    g = paramString4;
    h = paramString5;
    i = paramString6;
    j = paramString7;
    x = paramString8;
    k = paramString9;
    l = paramString10;
    m = paramString11;
    n = paramString12;
    o = paramn;
    d = paramString13;
    a = paramList;
    r = paramString14;
    c = paramString15;
    p = paramk;
    s = paramString16;
    t = paramString17;
    y = paramString18;
    v = paramLong;
    w = paramo;
    u = paramString19;
  }
  
  public final o a()
  {
    return w;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final String c()
  {
    return l;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.models.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */