package com.truecaller.truepay.app.ui.history.views;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.webapps.ixigo.IxigoBookingDetail;

public final class a
{
  public final LayoutInflater a;
  public final LinearLayout b;
  public final TextView c;
  public IxigoBookingDetail d;
  public final View e;
  private final TextView f;
  private final TextView g;
  private final a.a h;
  
  public a(View paramView, a.a parama)
  {
    e = paramView;
    h = parama;
    paramView = LayoutInflater.from(e.getContext());
    k.a(paramView, "LayoutInflater.from(view.context)");
    a = paramView;
    paramView = e;
    int i = R.id.container_view;
    paramView = (LinearLayout)paramView.findViewById(i);
    b = paramView;
    paramView = e;
    i = R.id.cancel_action;
    paramView = (TextView)paramView.findViewById(i);
    f = paramView;
    paramView = e;
    i = R.id.details_action;
    paramView = (TextView)paramView.findViewById(i);
    g = paramView;
    paramView = e;
    i = R.id.passenger_names;
    paramView = (TextView)paramView.findViewById(i);
    c = paramView;
    paramView = f;
    parama = new com/truecaller/truepay/app/ui/history/views/a$1;
    parama.<init>(this);
    parama = (View.OnClickListener)parama;
    paramView.setOnClickListener(parama);
    paramView = g;
    parama = new com/truecaller/truepay/app/ui/history/views/a$2;
    parama.<init>(this);
    parama = (View.OnClickListener)parama;
    paramView.setOnClickListener(parama);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */