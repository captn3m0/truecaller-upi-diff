package com.truecaller.truepay.app.ui.history.views.e;

import com.truecaller.truepay.data.e.b;

public abstract interface f
  extends com.truecaller.truepay.app.ui.base.views.a
{
  public abstract void a(b paramb, com.truecaller.truepay.data.e.a parama);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(Throwable paramThrowable);
  
  public abstract void b(String paramString);
  
  public abstract void b(String paramString1, String paramString2);
  
  public abstract void b(Throwable paramThrowable);
  
  public abstract void c(String paramString);
  
  public abstract void c(Throwable paramThrowable);
  
  public abstract void d(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.e.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */