package com.truecaller.truepay.app.ui.history.views.e;

import com.truecaller.truepay.app.ui.base.views.a;
import com.truecaller.truepay.app.ui.webapps.ixigo.IxigoBookingDetail;

public abstract interface c
  extends a
{
  public abstract void a(IxigoBookingDetail paramIxigoBookingDetail);
  
  public abstract void a(String paramString, int paramInt);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, int paramInt, String paramString4, String paramString5, String paramString6, String paramString7);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(String paramString);
  
  public abstract void c();
  
  public abstract void c(String paramString);
  
  public abstract void d();
  
  public abstract void e();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */