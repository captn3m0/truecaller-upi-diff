package com.truecaller.truepay.app.ui.history.views.activities;

import android.app.NotificationManager;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.R.array;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.ui.history.a.a.a;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.history.views.a.d;
import com.truecaller.truepay.app.ui.history.views.e.g;
import com.truecaller.truepay.app.ui.payments.views.activities.PaymentsActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;

public class TransactionHistoryActivity
  extends com.truecaller.truepay.app.ui.base.views.a.b
  implements g
{
  ViewPager a;
  TabLayout b;
  Toolbar c;
  public e d;
  private CharSequence[] e;
  private boolean f = false;
  private d g;
  
  private void a()
  {
    Object localObject1 = d.o();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject1).a();
    Object localObject2 = new com/truecaller/truepay/app/ui/history/views/a/d;
    j localj = getSupportFragmentManager();
    CharSequence[] arrayOfCharSequence = e;
    ((d)localObject2).<init>(localj, arrayOfCharSequence, bool);
    g = ((d)localObject2);
    if (!bool)
    {
      localObject1 = b;
      int i = 8;
      ((TabLayout)localObject1).setVisibility(i);
    }
    else
    {
      localObject1 = b;
      localObject2 = a;
      ((TabLayout)localObject1).setupWithViewPager((ViewPager)localObject2);
    }
    localObject1 = a;
    localObject2 = g;
    ((ViewPager)localObject1).setAdapter((android.support.v4.view.o)localObject2);
    a.setOffscreenPageLimit(2);
  }
  
  public final void a(Fragment paramFragment)
  {
    try
    {
      Object localObject1 = getSupportFragmentManager();
      localObject1 = ((j)localObject1).a();
      Object localObject2 = paramFragment.getClass();
      localObject2 = ((Class)localObject2).getName();
      ((android.support.v4.app.o)localObject1).a((String)localObject2);
      int i = R.id.history_container;
      Object localObject3 = paramFragment.getClass();
      localObject3 = ((Class)localObject3).getSimpleName();
      paramFragment = ((android.support.v4.app.o)localObject1).a(i, paramFragment, (String)localObject3);
      paramFragment.d();
      return;
    }
    catch (Exception localException) {}
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    ManageAccountsActivity.a(this, "action.page.reset_upi_pin", parama, null);
  }
  
  public final void a(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(this, TruePayWebViewActivity.class);
    localIntent.putExtra("url", paramString);
    startActivity(localIntent);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    paramString1 = PaymentsActivity.a(this, paramString2, paramString3, paramString4, paramString1, "history_repeat");
    startActivity(paramString1);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean)
  {
    paramString1 = PaymentsActivity.a(this, paramString1, paramString2, paramString3, paramString4, paramBoolean, "history");
    startActivityForResult(paramString1, 105);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    TransactionActivity.startForSend(this, paramString2, paramString1, paramString5, paramString3, paramString4);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = d.o();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    int i = 0;
    int j = 8;
    if (bool)
    {
      localObject = b;
      int k;
      if (paramBoolean) {
        k = 0;
      } else {
        k = 8;
      }
      ((TabLayout)localObject).setVisibility(k);
    }
    else
    {
      localObject = b;
      ((TabLayout)localObject).setVisibility(j);
    }
    localObject = c;
    if (!paramBoolean) {
      i = 8;
    }
    ((Toolbar)localObject).setVisibility(i);
  }
  
  public final void b(com.truecaller.truepay.data.api.model.a parama)
  {
    ManageAccountsActivity.a(this, "action.page.forgot_upi_pin", parama, null);
  }
  
  public int getLayoutId()
  {
    return R.layout.activity_transaction_history;
  }
  
  public void initDagger(com.truecaller.truepay.app.a.a.a parama)
  {
    parama = com.truecaller.truepay.app.ui.history.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    parama.a(locala).a().a(this);
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i = -1;
    if (paramInt2 == i)
    {
      paramInt2 = 105;
      if (paramInt1 == paramInt2) {
        finish();
      }
    }
  }
  
  public void onBackPressed()
  {
    boolean bool = f;
    if (bool)
    {
      finish();
      return;
    }
    j localj = getSupportFragmentManager();
    int i = localj.e();
    if (i != 0)
    {
      super.onBackPressed();
      localj = getSupportFragmentManager();
      i = localj.e();
      if (i == 0)
      {
        a(true);
        return;
      }
      a(false);
      return;
    }
    finish();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.id.pager_history;
    paramBundle = (ViewPager)findViewById(i);
    a = paramBundle;
    i = R.id.tab_layout_history;
    paramBundle = (TabLayout)findViewById(i);
    b = paramBundle;
    i = R.id.toolbar_history;
    paramBundle = (Toolbar)findViewById(i);
    c = paramBundle;
    paramBundle = c;
    setSupportActionBar(paramBundle);
    paramBundle = getSupportActionBar();
    int m = R.string.transaction_history;
    Object localObject = getString(m);
    paramBundle.setTitle((CharSequence)localObject);
    paramBundle = getSupportActionBar();
    m = 1;
    paramBundle.setDisplayShowTitleEnabled(m);
    getSupportActionBar().setDisplayHomeAsUpEnabled(m);
    paramBundle = c;
    -..Lambda.TransactionHistoryActivity.znfButpEkzxkKDMTQfyrHoi2YR4 localznfButpEkzxkKDMTQfyrHoi2YR4 = new com/truecaller/truepay/app/ui/history/views/activities/-$$Lambda$TransactionHistoryActivity$znfButpEkzxkKDMTQfyrHoi2YR4;
    localznfButpEkzxkKDMTQfyrHoi2YR4.<init>(this);
    paramBundle.setNavigationOnClickListener(localznfButpEkzxkKDMTQfyrHoi2YR4);
    paramBundle = getResources();
    int i2 = R.array.history_tabs;
    paramBundle = paramBundle.getStringArray(i2);
    e = paramBundle;
    paramBundle = "android.permission.READ_CONTACTS";
    i = android.support.v4.app.a.a(this, paramBundle);
    i2 = 0;
    localznfButpEkzxkKDMTQfyrHoi2YR4 = null;
    int n;
    if (i != 0)
    {
      paramBundle = "android.permission.READ_CONTACTS";
      boolean bool1 = android.support.v4.app.a.a(this, paramBundle);
      if (bool1)
      {
        paramBundle = getResources();
        n = R.string.read_phone_permission_text;
        paramBundle = paramBundle.getString(n);
        paramBundle = Toast.makeText(this, paramBundle, 0);
        paramBundle.show();
      }
      paramBundle = new String[] { "android.permission.READ_CONTACTS" };
      android.support.v4.app.a.a(this, paramBundle, 1001);
      return;
    }
    paramBundle = getIntent().getExtras();
    if (paramBundle != null)
    {
      String str = "history_detail";
      boolean bool4 = paramBundle.getBoolean(str);
      if (bool4) {}
    }
    else
    {
      n = 0;
      localObject = null;
    }
    f = n;
    boolean bool3 = f;
    if (bool3)
    {
      localObject = paramBundle.getSerializable("history_item");
      if (localObject != null)
      {
        localObject = com.truecaller.truepay.app.ui.history.views.b.a.a((h)paramBundle.getSerializable("history_item"));
        a((Fragment)localObject);
        break label329;
      }
    }
    a();
    label329:
    if (paramBundle != null)
    {
      localObject = "selected_tab";
      int i3 = -1;
      int i1 = paramBundle.getInt((String)localObject, i3);
      if (i1 != i3)
      {
        int j = paramBundle.getInt("selected_tab");
        localObject = a;
        ((ViewPager)localObject).setCurrentItem(j);
      }
    }
    paramBundle = getIntent();
    localObject = "EXTRA_NOTIFICATION_ID";
    boolean bool2 = paramBundle.hasExtra((String)localObject);
    if (bool2)
    {
      paramBundle = getIntent();
      int k = paramBundle.getIntExtra("EXTRA_NOTIFICATION_ID", 0);
      localObject = (NotificationManager)getSystemService("notification");
      ((NotificationManager)localObject).cancel(k);
    }
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int i = 1001;
    if (paramInt == i)
    {
      paramInt = paramArrayOfInt.length;
      i = 1;
      if (paramInt == i)
      {
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0)
        {
          a();
          return;
        }
      }
      Object localObject = getResources();
      i = R.string.phone_read_permission_denied;
      localObject = ((Resources)localObject).getString(i);
      localObject = Toast.makeText(this, (CharSequence)localObject, 0);
      ((Toast)localObject).show();
      onBackPressed();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.activities.TransactionHistoryActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */