package com.truecaller.truepay.app.ui.history.views.e;

import android.support.v4.app.Fragment;

public abstract interface g
  extends com.truecaller.truepay.app.ui.base.views.a
{
  public abstract void a(Fragment paramFragment);
  
  public abstract void a(com.truecaller.truepay.data.api.model.a parama);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b(com.truecaller.truepay.data.api.model.a parama);
  
  public abstract void onBackPressed();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.e.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */