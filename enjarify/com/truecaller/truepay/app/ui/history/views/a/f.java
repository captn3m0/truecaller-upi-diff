package com.truecaller.truepay.app.ui.history.views.a;

import android.support.v7.util.DiffUtil.Callback;
import com.truecaller.truepay.app.ui.history.models.h;
import java.util.List;

public final class f
  extends DiffUtil.Callback
{
  List a;
  List b;
  
  public f(List paramList1, List paramList2)
  {
    a = paramList1;
    b = paramList2;
  }
  
  public final boolean areContentsTheSame(int paramInt1, int paramInt2)
  {
    String str1 = a.get(paramInt1)).e;
    String str2 = b.get(paramInt2)).e;
    return str1.equalsIgnoreCase(str2);
  }
  
  public final boolean areItemsTheSame(int paramInt1, int paramInt2)
  {
    String str1 = a.get(paramInt1)).j;
    String str2 = b.get(paramInt2)).j;
    return str1.equalsIgnoreCase(str2);
  }
  
  public final int getNewListSize()
  {
    return b.size();
  }
  
  public final int getOldListSize()
  {
    return a.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */