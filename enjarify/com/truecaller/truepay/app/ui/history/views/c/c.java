package com.truecaller.truepay.app.ui.history.views.c;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.widget.ImageView;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.history.models.k;
import com.truecaller.truepay.app.ui.history.models.n;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class c
{
  private final Context a;
  
  public c(Context paramContext)
  {
    a = paramContext;
  }
  
  private static String a(String paramString1, String paramString2, b paramb)
  {
    String str = "0000000000";
    boolean bool = TextUtils.equals(paramString1, str);
    if (bool) {
      return paramString2;
    }
    return paramb.a(paramString1, paramString2);
  }
  
  public static void a(ImageView paramImageView, h paramh, a parama, b paramb)
  {
    String str1 = "";
    String str2 = "";
    Object localObject = i;
    int i = ((String)localObject).hashCode();
    boolean bool3;
    switch (i)
    {
    default: 
      break;
    case 2039438176: 
      str3 = "pay_direct";
      boolean bool1 = ((String)localObject).equals(str3);
      if (bool1) {
        int j = 3;
      }
      break;
    case 24489626: 
      str3 = "cashback";
      boolean bool2 = ((String)localObject).equals(str3);
      if (bool2) {
        int k = 6;
      }
      break;
    case 110760: 
      str3 = "pay";
      bool3 = ((String)localObject).equals(str3);
      if (bool3)
      {
        bool3 = false;
        localObject = null;
      }
      break;
    case -192199837: 
      str3 = "collect_request_pay";
      bool3 = ((String)localObject).equals(str3);
      if (bool3) {
        bool3 = true;
      }
      break;
    case -477924167: 
      str3 = "pay_other";
      bool3 = ((String)localObject).equals(str3);
      if (bool3) {
        int m = 2;
      }
      break;
    case -934813832: 
      str3 = "refund";
      boolean bool4 = ((String)localObject).equals(str3);
      if (bool4) {
        int n = 5;
      }
      break;
    case -1654673097: 
      str3 = "change_mpin";
      boolean bool5 = ((String)localObject).equals(str3);
      if (bool5) {
        i1 = 4;
      }
      break;
    }
    int i1 = -1;
    int i2;
    switch (i1)
    {
    default: 
      i2 = R.drawable.ic_avatar_common;
      paramImageView.setImageResource(i2);
      return;
    case 5: 
    case 6: 
      i2 = R.drawable.log_ic_other_account;
      paramImageView.setImageResource(i2);
      return;
    case 4: 
      i2 = R.drawable.ic_mpin_log;
      paramImageView.setImageResource(i2);
      return;
    case 3: 
      i2 = R.drawable.log_ic_other_account;
      paramImageView.setImageResource(i2);
      return;
    }
    localObject = "outgoing";
    String str3 = d;
    boolean bool6 = ((String)localObject).equalsIgnoreCase(str3);
    boolean bool8;
    if (bool6)
    {
      localObject = o;
      if (localObject != null)
      {
        str1 = o.c;
        bool8 = c(str1);
        if (bool8) {
          str1 = o.c;
        } else {
          str1 = o.a;
        }
        str2 = a(o.b, str1, paramb);
        paramh = o;
        str1 = b;
      }
    }
    else
    {
      localObject = p;
      if (localObject != null)
      {
        str1 = p.b;
        bool8 = c(str1);
        if (bool8) {
          str1 = p.b;
        } else {
          str1 = p.c;
        }
        str2 = a(p.d, str1, paramb);
        paramh = p;
        str1 = d;
      }
    }
    boolean bool7 = c(str2);
    if (bool7)
    {
      bool7 = c(str1);
      if (bool7)
      {
        parama.a(str1, paramImageView);
        return;
      }
    }
    int i3 = R.drawable.ic_avatar_common;
    paramImageView.setImageResource(i3);
  }
  
  private static boolean c(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      String str = "null";
      boolean bool2 = TextUtils.equals(paramString, str);
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int a(h paramh)
  {
    paramh = e;
    int i = paramh.hashCode();
    int j = -1867169789;
    String str;
    if (i != j)
    {
      j = -1086574198;
      boolean bool1;
      if (i != j)
      {
        j = -682587753;
        if (i != j)
        {
          j = -248987413;
          if (i == j)
          {
            str = "initiated";
            bool1 = paramh.equals(str);
            if (bool1)
            {
              bool1 = true;
              break label142;
            }
          }
        }
        else
        {
          str = "pending";
          bool1 = paramh.equals(str);
          if (bool1)
          {
            bool1 = false;
            paramh = null;
            break label142;
          }
        }
      }
      else
      {
        str = "failure";
        bool1 = paramh.equals(str);
        if (bool1)
        {
          int k = 3;
          break label142;
        }
      }
    }
    else
    {
      str = "success";
      boolean bool2 = paramh.equals(str);
      if (bool2)
      {
        m = 2;
        break label142;
      }
    }
    int m = -1;
    switch (m)
    {
    default: 
      paramh = a.getResources();
      i = R.color.status_failure_color;
      m = paramh.getColor(i);
      break;
    case 2: 
      paramh = a.getResources();
      i = R.color.status_success_color;
      m = paramh.getColor(i);
      break;
    case 0: 
    case 1: 
      label142:
      paramh = a.getResources();
      i = R.color.status_pending_color;
      m = paramh.getColor(i);
    }
    return m;
  }
  
  public final String a(h paramh, b paramb)
  {
    String str1 = "";
    Object localObject = i;
    int i = ((String)localObject).hashCode();
    int j = 1;
    String str2;
    boolean bool3;
    switch (i)
    {
    default: 
      break;
    case 2039438176: 
      str2 = "pay_direct";
      boolean bool1 = ((String)localObject).equals(str2);
      if (bool1) {
        int k = 4;
      }
      break;
    case 949444906: 
      str2 = "collect";
      boolean bool2 = ((String)localObject).equals(str2);
      if (bool2) {
        int m = 3;
      }
      break;
    case 110760: 
      str2 = "pay";
      bool3 = ((String)localObject).equals(str2);
      if (bool3)
      {
        bool3 = false;
        localObject = null;
      }
      break;
    case -192199837: 
      str2 = "collect_request_pay";
      bool3 = ((String)localObject).equals(str2);
      if (bool3) {
        bool3 = true;
      }
      break;
    case -477924167: 
      str2 = "pay_other";
      bool3 = ((String)localObject).equals(str2);
      if (bool3) {
        int n = 2;
      }
      break;
    case -787088529: 
      str2 = "pay_own";
      boolean bool4 = ((String)localObject).equals(str2);
      if (bool4) {
        int i1 = 6;
      }
      break;
    case -951650934: 
      str2 = "qr_pay";
      boolean bool5 = ((String)localObject).equals(str2);
      if (bool5) {
        i2 = 5;
      }
      break;
    }
    int i2 = -1;
    boolean bool7;
    int i3;
    boolean bool6;
    boolean bool8;
    Object[] arrayOfObject;
    switch (i2)
    {
    default: 
      break;
    case 6: 
      paramb = "outgoing";
      localObject = d;
      bool7 = paramb.equalsIgnoreCase((String)localObject);
      if (bool7)
      {
        paramb = o;
        if (paramb != null)
        {
          paramb = o.g;
          bool7 = c(paramb);
          if (bool7) {
            paramh = o.g;
          } else {
            paramh = o.a;
          }
          paramb = a;
          i3 = R.string.outgoing_history_pay_title;
          localObject = new Object[j];
          localObject[0] = paramh;
          str1 = paramb.getString(i3, (Object[])localObject);
        }
      }
      else
      {
        paramb = p;
        if (paramb != null)
        {
          paramb = p.h;
          bool7 = c(paramb);
          if (bool7) {
            paramh = p.h;
          } else {
            paramh = p.c;
          }
          paramb = a;
          i3 = R.string.incoming_history_pay_title;
          localObject = new Object[j];
          localObject[0] = paramh;
          str1 = paramb.getString(i3, (Object[])localObject);
        }
      }
      break;
    case 4: 
    case 5: 
      paramb = "outgoing";
      localObject = d;
      bool7 = paramb.equalsIgnoreCase((String)localObject);
      if (bool7)
      {
        paramb = o;
        if (paramb != null)
        {
          paramb = o.c;
          bool7 = c(paramb);
          if (bool7) {
            paramh = o.c;
          } else {
            paramh = o.a;
          }
          paramb = a;
          i3 = R.string.outgoing_history_pay_title;
          localObject = new Object[j];
          localObject[0] = paramh;
          str1 = paramb.getString(i3, (Object[])localObject);
        }
      }
      else
      {
        paramb = p;
        if (paramb != null)
        {
          paramb = p.b;
          bool7 = c(paramb);
          if (bool7) {
            paramh = p.b;
          } else {
            paramh = p.c;
          }
          paramb = a;
          i3 = R.string.incoming_history_pay_title;
          localObject = new Object[j];
          localObject[0] = paramh;
          str1 = paramb.getString(i3, (Object[])localObject);
        }
      }
      break;
    case 3: 
      localObject = "outgoing";
      str2 = d;
      bool6 = ((String)localObject).equalsIgnoreCase(str2);
      if (bool6)
      {
        localObject = o;
        if (localObject != null)
        {
          str1 = o.c;
          bool8 = c(str1);
          if (bool8) {
            str1 = o.c;
          } else {
            str1 = o.a;
          }
          localObject = a;
          i = R.string.outgoing_history_collect_title;
          arrayOfObject = new Object[j];
          paramh = a(o.b, str1, paramb);
          arrayOfObject[0] = paramh;
          str1 = ((Context)localObject).getString(i, arrayOfObject);
        }
      }
      else
      {
        localObject = p;
        if (localObject != null)
        {
          str1 = p.b;
          bool8 = c(str1);
          if (bool8) {
            str1 = p.b;
          } else {
            str1 = p.c;
          }
          localObject = a;
          i = R.string.incoming_history_collect_title;
          arrayOfObject = new Object[j];
          paramh = a(p.d, str1, paramb);
          arrayOfObject[0] = paramh;
          str1 = ((Context)localObject).getString(i, arrayOfObject);
        }
      }
      break;
    case 0: 
    case 1: 
    case 2: 
      localObject = "outgoing";
      str2 = d;
      bool6 = ((String)localObject).equalsIgnoreCase(str2);
      if (bool6)
      {
        localObject = o;
        if (localObject != null)
        {
          str1 = o.c;
          bool8 = c(str1);
          if (bool8) {
            str1 = o.c;
          } else {
            str1 = o.a;
          }
          localObject = a;
          i = R.string.outgoing_history_pay_title;
          arrayOfObject = new Object[j];
          paramh = a(o.b, str1, paramb);
          arrayOfObject[0] = paramh;
          str1 = ((Context)localObject).getString(i, arrayOfObject);
        }
      }
      else
      {
        localObject = p;
        if (localObject != null)
        {
          str1 = p.b;
          bool8 = c(str1);
          if (bool8) {
            str1 = p.b;
          } else {
            str1 = p.c;
          }
          localObject = a;
          i = R.string.incoming_history_pay_title;
          arrayOfObject = new Object[j];
          paramh = a(p.d, str1, paramb);
          arrayOfObject[0] = paramh;
          str1 = ((Context)localObject).getString(i, arrayOfObject);
        }
      }
      break;
    }
    return str1;
  }
  
  public final String a(String paramString)
  {
    int i = paramString.hashCode();
    int j = -1867169789;
    String str;
    if (i != j)
    {
      j = -1086574198;
      boolean bool1;
      if (i != j)
      {
        j = -682587753;
        if (i != j)
        {
          j = -248987413;
          if (i == j)
          {
            str = "initiated";
            bool1 = paramString.equals(str);
            if (bool1)
            {
              bool1 = true;
              break label137;
            }
          }
        }
        else
        {
          str = "pending";
          bool1 = paramString.equals(str);
          if (bool1)
          {
            bool1 = false;
            paramString = null;
            break label137;
          }
        }
      }
      else
      {
        str = "failure";
        bool1 = paramString.equals(str);
        if (bool1)
        {
          int k = 3;
          break label137;
        }
      }
    }
    else
    {
      str = "success";
      boolean bool2 = paramString.equals(str);
      if (bool2)
      {
        m = 2;
        break label137;
      }
    }
    int m = -1;
    switch (m)
    {
    default: 
      paramString = a.getResources();
      i = R.string.failure_history;
      paramString = paramString.getString(i);
      break;
    case 2: 
      paramString = a.getResources();
      i = R.string.success_history;
      paramString = paramString.getString(i);
      break;
    case 0: 
    case 1: 
      label137:
      paramString = a.getResources();
      i = R.string.pending_history;
      paramString = paramString.getString(i);
    }
    return paramString;
  }
  
  public final String b(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (bool1) {
      return "-";
    }
    Object localObject = new java/text/SimpleDateFormat;
    String str = "yyyy-MM-dd HH:mm:ss";
    Locale localLocale = Locale.US;
    ((SimpleDateFormat)localObject).<init>(str, localLocale);
    try
    {
      paramString = ((SimpleDateFormat)localObject).parse(paramString);
      localObject = new java/util/Date;
      ((Date)localObject).<init>();
      long l1 = ((Date)localObject).getTime();
      long l2 = paramString.getTime();
      l1 -= l2;
      l2 = 1000L;
      l1 /= l2;
      l2 = 60;
      long l3 = l1 / l2;
      int j = (int)l3;
      int k = j / 60;
      int m = k / 24;
      long l4 = 10;
      boolean bool2 = l1 < l4;
      int i;
      if (bool2)
      {
        paramString = a;
        i = R.string.just_now;
        paramString = paramString.getString(i);
      }
      else
      {
        boolean bool3 = l1 < l2;
        if (bool3)
        {
          paramString = a;
          i = R.string.seconds_ago;
          paramString = paramString.getString(i);
        }
        else
        {
          i = 1;
          if (j == i)
          {
            paramString = a;
            i = R.string.one_min_ago;
            paramString = paramString.getString(i);
          }
          else
          {
            str = null;
            int n = 60;
            Integer localInteger;
            if (j < n)
            {
              paramString = a;
              n = R.string.min_ago;
              localObject = new Object[i];
              localInteger = Integer.valueOf(j);
              localObject[0] = localInteger;
              paramString = paramString.getString(n, (Object[])localObject);
            }
            else if (k == i)
            {
              paramString = a;
              i = R.string.one_hr_ago;
              paramString = paramString.getString(i);
            }
            else
            {
              n = 24;
              if (k < n)
              {
                paramString = a;
                n = R.string.hr_ago;
                localObject = new Object[i];
                localInteger = Integer.valueOf(k);
                localObject[0] = localInteger;
                paramString = paramString.getString(n, (Object[])localObject);
              }
              else if (m == i)
              {
                paramString = a;
                i = R.string.one_d_ago;
                paramString = paramString.getString(i);
              }
              else
              {
                localObject = new java/text/SimpleDateFormat;
                str = "dd MMM, yyyy";
                localLocale = Locale.US;
                ((SimpleDateFormat)localObject).<init>(str, localLocale);
                paramString = ((SimpleDateFormat)localObject).format(paramString);
              }
            }
          }
        }
      }
      return paramString;
    }
    catch (ParseException localParseException) {}
    return "-";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */