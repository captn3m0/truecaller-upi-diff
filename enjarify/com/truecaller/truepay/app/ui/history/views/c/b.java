package com.truecaller.truepay.app.ui.history.views.c;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;
import android.support.v4.f.g;
import android.text.TextUtils;

public final class b
{
  private final g a;
  private final Context b;
  
  public b(Context paramContext)
  {
    b = paramContext;
    paramContext = new com/truecaller/truepay/app/ui/history/views/c/b$1;
    paramContext.<init>(this);
    a = paramContext;
  }
  
  public final String a(String paramString1, String paramString2)
  {
    if (paramString1 != null)
    {
      boolean bool1 = TextUtils.isEmpty(paramString1);
      if (!bool1)
      {
        if ((paramString1 == null) && (paramString2 == null)) {
          return null;
        }
        synchronized (a)
        {
          Object localObject2 = a;
          localObject2 = ((g)localObject2).get(paramString1);
          if (localObject2 == null)
          {
            localObject2 = "display_name";
            String[] arrayOfString = { localObject2 };
            localObject2 = ContactsContract.PhoneLookup.CONTENT_FILTER_URI;
            Object localObject3 = Uri.encode(paramString1);
            Uri localUri = Uri.withAppendedPath((Uri)localObject2, (String)localObject3);
            localObject2 = b;
            localObject3 = ((Context)localObject2).getContentResolver();
            localObject2 = ((ContentResolver)localObject3).query(localUri, arrayOfString, null, null, null);
            if (localObject2 != null)
            {
              boolean bool3 = ((Cursor)localObject2).moveToFirst();
              if (bool3)
              {
                paramString2 = null;
                paramString2 = ((Cursor)localObject2).getString(0);
              }
              else
              {
                bool3 = TextUtils.isEmpty(paramString2);
                if (bool3) {
                  paramString2 = paramString1;
                }
              }
              ((Cursor)localObject2).close();
            }
            else
            {
              paramString2 = paramString1;
            }
            localObject2 = a;
            ((g)localObject2).put(paramString1, paramString2);
          }
          else
          {
            paramString2 = a;
            paramString2 = paramString2.get(paramString1);
            paramString2 = (String)paramString2;
          }
          int i = paramString2.length();
          if (i > 0)
          {
            ??? = "null";
            boolean bool2 = ((String)???).equals(paramString2);
            if (!bool2) {
              paramString1 = paramString2;
            }
          }
          return paramString1;
        }
      }
    }
    return paramString2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */