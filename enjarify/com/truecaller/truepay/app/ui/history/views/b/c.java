package com.truecaller.truepay.app.ui.history.views.b;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.e;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.app.ui.history.views.e.a;

public class c
  extends e
{
  TextView a;
  a b;
  private String c;
  
  public static c a(String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("ref_num", paramString);
    paramString = new com/truecaller/truepay/app/ui/history/views/b/c;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getActivity().getLayoutInflater();
    int i = R.layout.fragment_raise_dispute;
    paramBundle = paramBundle.inflate(i, null);
    i = R.id.tv_issue_ref_number;
    Object localObject1 = (TextView)paramBundle.findViewById(i);
    a = ((TextView)localObject1);
    i = R.id.btn_raise_dispute_done;
    localObject1 = paramBundle.findViewById(i);
    Object localObject2 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$c$kLhRSEjlWinZrFiODNRPLXw68Js;
    ((-..Lambda.c.kLhRSEjlWinZrFiODNRPLXw68Js)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    i = R.id.img_copy_raise_disp;
    localObject1 = paramBundle.findViewById(i);
    localObject2 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$c$0KQENUh3FnH8E4DPn6gRnCUbHY0;
    ((-..Lambda.c.0KQENUh3FnH8E4DPn6gRnCUbHY0)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = new android/support/v7/app/AlertDialog$Builder;
    localObject2 = new android/view/ContextThemeWrapper;
    Object localObject3 = getActivity();
    int j = R.style.popup_theme;
    ((ContextThemeWrapper)localObject2).<init>((Context)localObject3, j);
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).create();
    localObject2 = ((AlertDialog)localObject1).getWindow();
    if (localObject2 != null)
    {
      localObject2 = ((AlertDialog)localObject1).getWindow();
      int k = 80;
      ((Window)localObject2).setGravity(k);
    }
    ((AlertDialog)localObject1).setCanceledOnTouchOutside(false);
    ((AlertDialog)localObject1).setView(paramBundle);
    paramBundle = getArguments().getString("ref_num");
    c = paramBundle;
    paramBundle = a;
    localObject3 = getContext();
    j = R.string.raise_dispute_issue_referance_number;
    Object[] arrayOfObject = new Object[1];
    String str = c;
    arrayOfObject[0] = str;
    localObject2 = ((Context)localObject3).getString(j, arrayOfObject);
    paramBundle.setText((CharSequence)localObject2);
    return (Dialog)localObject1;
  }
  
  public void onStart()
  {
    super.onStart();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      localWindow.setBackgroundDrawableResource(17170445);
      int i = -1;
      int j = -2;
      localWindow.setLayout(i, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */