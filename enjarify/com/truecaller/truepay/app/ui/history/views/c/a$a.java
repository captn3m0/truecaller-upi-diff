package com.truecaller.truepay.app.ui.history.views.c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.lang.ref.WeakReference;

final class a$a
  extends BitmapDrawable
{
  final WeakReference a;
  
  public a$a(Resources paramResources, Bitmap paramBitmap, a.b paramb)
  {
    super(paramResources, paramBitmap);
    paramResources = new java/lang/ref/WeakReference;
    paramResources.<init>(paramb);
    a = paramResources;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.c.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */