package com.truecaller.truepay.app.ui.history.views.d;

import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.history.a.a.a;
import com.truecaller.truepay.app.ui.history.views.a.e;
import com.truecaller.truepay.app.ui.history.views.c.c;
import java.text.DecimalFormat;

public abstract class a
  extends com.truecaller.truepay.app.ui.base.views.c.a
  implements View.OnClickListener
{
  public c b;
  public com.truecaller.truepay.app.ui.history.views.c.b c;
  public com.truecaller.truepay.app.ui.history.views.c.a d;
  protected DecimalFormat e;
  
  public a(View paramView, f paramf)
  {
    super(paramView, paramf);
    paramf = new java/text/DecimalFormat;
    paramf.<init>("#,###.##");
    e = paramf;
    paramView.setOnClickListener(this);
    paramView = com.truecaller.truepay.app.ui.history.a.a.a();
    paramf = Truepay.getApplicationComponent();
    paramView.a(paramf).a().a(this);
  }
  
  public void onClick(View paramView)
  {
    int i = getAdapterPosition();
    if (i >= 0)
    {
      paramView = (e)a;
      int j = getAdapterPosition();
      paramView.a(j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */