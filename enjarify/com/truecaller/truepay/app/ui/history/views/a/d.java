package com.truecaller.truepay.app.ui.history.views.a;

import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.n;
import android.util.SparseArray;
import android.view.ViewGroup;
import com.truecaller.truepay.app.ui.history.views.b.b;

public final class d
  extends n
{
  private final CharSequence[] a;
  private final SparseArray b;
  private final boolean c;
  
  public d(j paramj, CharSequence[] paramArrayOfCharSequence, boolean paramBoolean)
  {
    super(paramj);
    a = paramArrayOfCharSequence;
    paramj = new android/util/SparseArray;
    paramj.<init>();
    b = paramj;
    c = paramBoolean;
  }
  
  public final Fragment a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return b.a("all");
    case 2: 
      return b.a("utility");
    case 1: 
      return b.a("banking");
    }
    return b.a("all");
  }
  
  public final void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    super.destroyItem(paramViewGroup, paramInt, paramObject);
    b.remove(paramInt);
  }
  
  public final int getCount()
  {
    boolean bool = c;
    if (!bool) {
      return 1;
    }
    return a.length;
  }
  
  public final CharSequence getPageTitle(int paramInt)
  {
    return a[paramInt];
  }
  
  public final Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
  {
    paramViewGroup = (Fragment)super.instantiateItem(paramViewGroup, paramInt);
    b.put(paramInt, paramViewGroup);
    return paramViewGroup;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */