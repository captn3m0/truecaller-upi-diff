package com.truecaller.truepay.app.ui.history.views.b;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.constraint.ConstraintLayout;
import android.support.v4.f.j;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.b;
import android.support.v7.util.DiffUtil;
import android.support.v7.util.DiffUtil.Callback;
import android.support.v7.util.DiffUtil.DiffResult;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.history.a.a.a;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.history.views.a.c;
import com.truecaller.truepay.app.ui.history.views.e.g;
import com.truecaller.truepay.data.e.e;
import java.util.ArrayList;
import java.util.List;

public final class b
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements SwipeRefreshLayout.b, c, com.truecaller.truepay.app.ui.history.views.e.d
{
  RecyclerView a;
  SwipeRefreshLayout b;
  ConstraintLayout c;
  public com.truecaller.truepay.app.ui.history.b.d d;
  public e e;
  public com.google.gson.f f;
  private com.truecaller.truepay.app.ui.history.views.a.a g;
  private List h;
  private boolean i;
  private String j;
  private g k;
  private LinearLayoutManager l;
  private final ContentObserver n;
  
  public b()
  {
    b.1 local1 = new com/truecaller/truepay/app/ui/history/views/b/b$1;
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    local1.<init>(this, localHandler);
    n = local1;
  }
  
  public static b a(String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("type", paramString);
    paramString = new com/truecaller/truepay/app/ui/history/views/b/b;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  public final int a()
  {
    return R.layout.fragment_history;
  }
  
  public final void a(j paramj)
  {
    com.truecaller.truepay.app.ui.history.views.a.a locala = g;
    paramj = (List)a;
    Object localObject = new com/truecaller/truepay/app/ui/history/views/a/f;
    List localList = (List)locala.a();
    ((com.truecaller.truepay.app.ui.history.views.a.f)localObject).<init>(localList, paramj);
    localObject = DiffUtil.calculateDiff((DiffUtil.Callback)localObject);
    locala.a(paramj);
    ((DiffUtil.DiffResult)localObject).dispatchUpdatesTo(locala);
    paramj = l;
    int m = paramj.findFirstCompletelyVisibleItemPosition();
    locala = null;
    if (m == 0)
    {
      paramj = l;
      paramj.scrollToPosition(0);
    }
    paramj = (List)g.a();
    m = paramj.size();
    if (m == 0)
    {
      paramj = c;
      paramj.setVisibility(0);
    }
    else
    {
      paramj = c;
      int i1 = 8;
      paramj.setVisibility(i1);
    }
    paramj = b;
    boolean bool = b;
    if (bool)
    {
      paramj = b;
      paramj.setRefreshing(false);
    }
    i = false;
  }
  
  public final void a(h paramh)
  {
    paramh = a.a(paramh);
    k.a(paramh);
  }
  
  public final void a(Throwable paramThrowable)
  {
    i = false;
    b.setRefreshing(false);
    int m = R.string.history_fetch_failure;
    String str = getString(m);
    a(str, paramThrowable);
    paramThrowable = (List)g.a();
    int i1 = paramThrowable.size();
    if (i1 == 0)
    {
      c.setVisibility(0);
      return;
    }
    c.setVisibility(8);
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      c.setVisibility(8);
      b.setRefreshing(true);
      return;
    }
    b.setRefreshing(false);
  }
  
  public final void ad_()
  {
    boolean bool = i;
    if (!bool)
    {
      bool = true;
      i = bool;
      com.truecaller.truepay.app.ui.history.b.d locald = d;
      String str = j;
      locald.a(str);
      locald = d;
      locald.a();
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof g;
    if (bool)
    {
      paramContext = (g)getActivity();
      k = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement TransactionHistoryView");
    throw paramContext;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.history.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    d.b();
    ContentResolver localContentResolver = getContext().getContentResolver();
    ContentObserver localContentObserver = n;
    localContentResolver.unregisterContentObserver(localContentObserver);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    k = null;
  }
  
  public final void onResume()
  {
    super.onResume();
    ContentResolver localContentResolver = getContext().getContentResolver();
    Object localObject = n;
    localContentResolver.unregisterContentObserver((ContentObserver)localObject);
    localContentResolver = getContext().getContentResolver();
    localObject = com.truecaller.truepay.data.provider.e.d.a;
    ContentObserver localContentObserver = n;
    localContentResolver.registerContentObserver((Uri)localObject, false, localContentObserver);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.rv_history;
    paramBundle = (RecyclerView)paramView.findViewById(m);
    a = paramBundle;
    m = R.id.srl_frag_history;
    paramBundle = (SwipeRefreshLayout)paramView.findViewById(m);
    b = paramBundle;
    m = R.id.rl_empty_layout;
    paramView = (ConstraintLayout)paramView.findViewById(m);
    c = paramView;
    paramView = new com/truecaller/truepay/app/ui/history/views/a/a;
    paramBundle = f;
    paramView.<init>(this, paramBundle);
    g = paramView;
    paramView = new android/support/v7/widget/LinearLayoutManager;
    paramBundle = getActivity();
    int i1 = 1;
    paramView.<init>(paramBundle, i1, false);
    l = paramView;
    paramView = a;
    paramBundle = l;
    paramView.setLayoutManager(paramBundle);
    paramView = a;
    paramBundle = g;
    paramView.setAdapter(paramBundle);
    paramView = new android/support/v7/widget/DividerItemDecoration;
    paramBundle = getContext();
    paramView.<init>(paramBundle, i1);
    paramBundle = getContext();
    int i2 = R.drawable.divider_history;
    paramBundle = android.support.v4.content.b.a(paramBundle, i2);
    paramView.setDrawable(paramBundle);
    paramBundle = a;
    paramBundle.addItemDecoration(paramView);
    b.setOnRefreshListener(this);
    k.a(i1);
    paramView = getArguments();
    if (paramView != null)
    {
      paramView = getArguments();
      paramBundle = "type";
      paramView = paramView.getString(paramBundle);
      j = paramView;
    }
    d.a(this);
    paramView = h;
    if (paramView == null)
    {
      paramView = new java/util/ArrayList;
      paramView.<init>();
      h = paramView;
    }
    paramView = h;
    int i3 = paramView.size();
    if (i3 == 0)
    {
      i = i1;
      paramView = d;
      paramBundle = j;
      paramView.a(paramBundle);
    }
    d.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */