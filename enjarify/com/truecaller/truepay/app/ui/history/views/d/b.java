package com.truecaller.truepay.app.ui.history.views.d;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.history.views.c.c;
import java.text.DecimalFormat;

public final class b
  extends a
{
  private final ImageView f;
  private final TextView g;
  private final TextView h;
  private final TextView i;
  private final TextView j;
  
  public b(View paramView, f paramf)
  {
    super(paramView, paramf);
    int k = R.id.im_profile_pic;
    paramf = (ImageView)paramView.findViewById(k);
    f = paramf;
    k = R.id.tv_person_name;
    paramf = (TextView)paramView.findViewById(k);
    g = paramf;
    k = R.id.tv_payment_status;
    paramf = (TextView)paramView.findViewById(k);
    h = paramf;
    k = R.id.tv_payment_amount;
    paramf = (TextView)paramView.findViewById(k);
    i = paramf;
    k = R.id.tv_payment_time;
    paramView = (TextView)paramView.findViewById(k);
    j = paramView;
  }
  
  public final void a(h paramh)
  {
    Object localObject1 = i;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("₹");
    Object localObject3 = e;
    double d = Double.parseDouble(l);
    localObject3 = ((DecimalFormat)localObject3).format(d);
    ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject1 = h;
    localObject2 = b;
    localObject3 = e;
    localObject2 = ((c)localObject2).a((String)localObject3);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject1 = h;
    int k = b.a(paramh);
    ((TextView)localObject1).setTextColor(k);
    localObject1 = j;
    localObject2 = b;
    localObject3 = b;
    localObject2 = ((c)localObject2).b((String)localObject3);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject1 = g;
    localObject2 = b;
    localObject3 = c;
    localObject2 = ((c)localObject2).a(paramh, (com.truecaller.truepay.app.ui.history.views.c.b)localObject3);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject1 = f;
    localObject2 = this.d;
    localObject3 = c;
    c.a((ImageView)localObject1, paramh, (com.truecaller.truepay.app.ui.history.views.c.a)localObject2, (com.truecaller.truepay.app.ui.history.views.c.b)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */