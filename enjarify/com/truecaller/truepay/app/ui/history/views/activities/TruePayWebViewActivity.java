package com.truecaller.truepay.app.ui.history.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.a.a;

public class TruePayWebViewActivity
  extends a
{
  WebView a;
  ProgressBar b;
  Toolbar c;
  AppBarLayout d;
  private String e = "";
  private boolean f;
  private String g;
  
  public int getLayoutId()
  {
    return R.layout.activity_truepay_web;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.id.webview_help;
    paramBundle = (WebView)findViewById(i);
    a = paramBundle;
    i = R.id.progress_bar_help;
    paramBundle = (ProgressBar)findViewById(i);
    b = paramBundle;
    i = R.id.toolbar;
    paramBundle = (Toolbar)findViewById(i);
    c = paramBundle;
    i = R.id.app_bar_layout;
    paramBundle = (AppBarLayout)findViewById(i);
    d = paramBundle;
    paramBundle = getIntent();
    if (paramBundle != null)
    {
      paramBundle = getIntent().getExtras();
      if (paramBundle != null)
      {
        paramBundle = getIntent().getStringExtra("url");
        e = paramBundle;
        paramBundle = getIntent();
        String str = "show_toolbar";
        boolean bool1 = paramBundle.getBooleanExtra(str, false);
        f = bool1;
        paramBundle = c;
        setSupportActionBar(paramBundle);
        bool1 = f;
        boolean bool2 = true;
        int j;
        if (bool1)
        {
          paramBundle = getIntent();
          localObject = "title";
          paramBundle = paramBundle.getStringExtra((String)localObject);
          g = paramBundle;
          paramBundle = getSupportActionBar();
          if (paramBundle != null)
          {
            localObject = g;
            paramBundle.setTitle((CharSequence)localObject);
            paramBundle.setDisplayShowTitleEnabled(bool2);
            paramBundle.setDisplayHomeAsUpEnabled(bool2);
            j = R.drawable.ic_close_black_48_px;
            paramBundle.setHomeAsUpIndicator(j);
          }
          paramBundle = c;
          localObject = new com/truecaller/truepay/app/ui/history/views/activities/-$$Lambda$TruePayWebViewActivity$szlHXpDxjJHbR33F2zkUFHNwYHY;
          ((-..Lambda.TruePayWebViewActivity.szlHXpDxjJHbR33F2zkUFHNwYHY)localObject).<init>(this);
          paramBundle.setNavigationOnClickListener((View.OnClickListener)localObject);
        }
        else
        {
          paramBundle = d;
          j = 8;
          paramBundle.setVisibility(j);
        }
        paramBundle = a;
        Object localObject = new com/truecaller/truepay/app/ui/history/views/activities/TruePayWebViewActivity$1;
        ((TruePayWebViewActivity.1)localObject).<init>(this);
        paramBundle.setWebViewClient((WebViewClient)localObject);
        a.getSettings().setJavaScriptEnabled(bool2);
        a.getSettings().setAllowContentAccess(false);
        a.getSettings().setDomStorageEnabled(bool2);
        paramBundle = a;
        str = e;
        paramBundle.loadUrl(str);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.activities.TruePayWebViewActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */