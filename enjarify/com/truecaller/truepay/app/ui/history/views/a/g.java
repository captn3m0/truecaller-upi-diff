package com.truecaller.truepay.app.ui.history.views.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;

public final class g
  extends com.truecaller.truepay.app.ui.base.views.b.c
{
  private final com.google.gson.f b;
  
  public g(com.truecaller.truepay.app.ui.base.views.b.f paramf, com.google.gson.f paramf1)
  {
    super(paramf);
    b = paramf1;
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    com.truecaller.truepay.app.ui.history.views.d.c localc = new com/truecaller/truepay/app/ui/history/views/d/c;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.list_item_history;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    com.google.gson.f localf = b;
    localc.<init>(paramViewGroup, (com.truecaller.truepay.app.ui.base.views.b.f)localObject, localf);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */