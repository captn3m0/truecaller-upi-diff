package com.truecaller.truepay.app.ui.history.views.d;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.b.ab;
import com.d.b.w;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.history.models.RedbusBookingDetail;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.history.models.o;
import com.truecaller.truepay.app.ui.webapps.ixigo.IxigoBookingDetail;
import com.truecaller.truepay.app.utils.au;
import java.text.DecimalFormat;

public final class c
  extends a
{
  private final ImageView f;
  private final TextView g;
  private final TextView h;
  private final TextView i;
  private final TextView j;
  private final com.google.gson.f k;
  
  public c(View paramView, com.truecaller.truepay.app.ui.base.views.b.f paramf, com.google.gson.f paramf1)
  {
    super(paramView, paramf);
    int m = R.id.im_profile_pic;
    paramf = (ImageView)paramView.findViewById(m);
    f = paramf;
    m = R.id.tv_person_name;
    paramf = (TextView)paramView.findViewById(m);
    g = paramf;
    m = R.id.tv_payment_status;
    paramf = (TextView)paramView.findViewById(m);
    h = paramf;
    m = R.id.tv_payment_amount;
    paramf = (TextView)paramView.findViewById(m);
    i = paramf;
    m = R.id.tv_payment_time;
    paramView = (TextView)paramView.findViewById(m);
    j = paramView;
    k = paramf1;
  }
  
  public final void a(h paramh)
  {
    Object localObject1 = w;
    if (localObject1 != null)
    {
      Object localObject2 = g;
      Object localObject3 = itemView.getContext();
      int m = R.string.utility_history_title;
      int n = 2;
      Object localObject4 = new Object[n];
      String str1 = h;
      localObject4[0] = str1;
      str1 = b;
      int i1 = 1;
      localObject4[i1] = str1;
      localObject3 = ((Context)localObject3).getString(m, (Object[])localObject4);
      ((TextView)localObject2).setText((CharSequence)localObject3);
      localObject2 = h;
      localObject3 = null;
      ((TextView)localObject2).setBackground(null);
      localObject2 = i;
      Object localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>("₹");
      localObject4 = e;
      str1 = l;
      double d = Double.parseDouble(str1);
      localObject4 = ((DecimalFormat)localObject4).format(d);
      ((StringBuilder)localObject5).append((String)localObject4);
      localObject5 = ((StringBuilder)localObject5).toString();
      ((TextView)localObject2).setText((CharSequence)localObject5);
      localObject2 = j;
      localObject5 = b;
      paramh = b;
      paramh = ((com.truecaller.truepay.app.ui.history.views.c.c)localObject5).b(paramh);
      ((TextView)localObject2).setText(paramh);
      paramh = c;
      if (paramh != null) {
        paramh = c;
      } else {
        paramh = "Unknown";
      }
      localObject2 = h;
      localObject5 = au.b(paramh);
      ((TextView)localObject2).setText((CharSequence)localObject5);
      int i2 = paramh.hashCode();
      m = -1867169789;
      if (i2 != m)
      {
        m = -1086574198;
        boolean bool3;
        if (i2 != m)
        {
          m = -682587753;
          if (i2 != m)
          {
            m = -248987413;
            if (i2 == m)
            {
              localObject2 = "initiated";
              boolean bool2 = paramh.equals(localObject2);
              if (bool2)
              {
                int i4 = 2;
                break label369;
              }
            }
          }
          else
          {
            localObject2 = "pending";
            bool3 = paramh.equals(localObject2);
            if (bool3)
            {
              bool3 = false;
              paramh = null;
              break label369;
            }
          }
        }
        else
        {
          localObject2 = "failure";
          bool3 = paramh.equals(localObject2);
          if (bool3)
          {
            int i5 = 3;
            break label369;
          }
        }
      }
      else
      {
        localObject2 = "success";
        boolean bool4 = paramh.equals(localObject2);
        if (bool4)
        {
          bool4 = true;
          break label369;
        }
      }
      int i6 = -1;
      switch (i6)
      {
      default: 
        paramh = h;
        localObject2 = itemView.getContext().getResources();
        m = R.color.status_failure_color;
        i2 = ((Resources)localObject2).getColor(m);
        paramh.setTextColor(i2);
        break;
      case 1: 
      case 2: 
        paramh = h;
        localObject2 = itemView.getContext().getResources();
        m = R.color.status_success_color;
        i2 = ((Resources)localObject2).getColor(m);
        paramh.setTextColor(i2);
        break;
      case 0: 
        label369:
        paramh = h;
        localObject2 = itemView.getContext().getResources();
        m = R.color.status_pending_color;
        i2 = ((Resources)localObject2).getColor(m);
        paramh.setTextColor(i2);
      }
      paramh = "google_play";
      localObject2 = f;
      boolean bool5 = paramh.equalsIgnoreCase((String)localObject2);
      label827:
      int i3;
      if (bool5)
      {
        paramh = g;
        localObject2 = h;
        paramh.setText((CharSequence)localObject2);
      }
      else
      {
        paramh = "booking";
        localObject2 = f;
        bool5 = paramh.equalsIgnoreCase((String)localObject2);
        if (bool5)
        {
          paramh = x;
          if (paramh != null)
          {
            paramh = g;
            localObject2 = itemView.getContext();
            m = R.string.utility_booking_history_title;
            Object[] arrayOfObject = new Object[n];
            String str2 = h;
            arrayOfObject[0] = str2;
            str2 = f;
            arrayOfObject[i1] = str2;
            localObject2 = ((Context)localObject2).getString(m, arrayOfObject);
            paramh.setText((CharSequence)localObject2);
            paramh = "redbus";
            localObject2 = r;
            bool5 = paramh.equalsIgnoreCase((String)localObject2);
            boolean bool1;
            if (bool5)
            {
              paramh = new com/truecaller/truepay/app/ui/history/views/d/c$1;
              paramh.<init>(this);
              paramh = b;
              localObject2 = k;
              localObject5 = x;
              paramh = (RedbusBookingDetail)((com.google.gson.f)localObject2).a((String)localObject5, paramh);
              if (paramh != null)
              {
                localObject2 = "success";
                localObject5 = c;
                bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject5);
                if (bool1)
                {
                  paramh = paramh.getCancel_status();
                  break label827;
                }
              }
              bool5 = false;
              paramh = null;
            }
            else
            {
              paramh = "ixigo";
              localObject2 = r;
              bool5 = paramh.equalsIgnoreCase((String)localObject2);
              if (bool5)
              {
                paramh = new com/truecaller/truepay/app/ui/history/views/d/c$2;
                paramh.<init>(this);
                paramh = b;
                localObject2 = k;
                localObject5 = x;
                paramh = (IxigoBookingDetail)((com.google.gson.f)localObject2).a((String)localObject5, paramh);
                localObject2 = "success";
                localObject5 = c;
                bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject5);
                if (bool1)
                {
                  paramh = paramh.getCancel_status();
                  break label827;
                }
              }
              bool5 = false;
              paramh = null;
            }
            localObject2 = "success";
            bool5 = ((String)localObject2).equalsIgnoreCase(paramh);
            if (bool5)
            {
              paramh = h;
              localObject2 = itemView.getContext();
              m = R.string.booking_cancelled_status;
              localObject2 = ((Context)localObject2).getString(m);
              paramh.setText((CharSequence)localObject2);
              paramh = h;
              localObject2 = itemView.getContext().getResources();
              m = R.color.status_failure_color;
              i3 = ((Resources)localObject2).getColor(m);
              paramh.setTextColor(i3);
            }
          }
        }
      }
      paramh = m;
      if (paramh != null)
      {
        paramh = m;
        i3 = paramh.hashCode();
        switch (i3)
        {
        default: 
          break;
        case 531647627: 
          localObject2 = "not_applicable";
          bool5 = paramh.equals(localObject2);
          if (bool5) {
            n = 4;
          }
          break;
        case -248987413: 
          localObject2 = "initiated";
          bool5 = paramh.equals(localObject2);
          if (bool5) {
            n = 1;
          }
          break;
        case -682587753: 
          localObject2 = "pending";
          bool5 = paramh.equals(localObject2);
          if (!bool5) {
            break;
          }
          break;
        case -1086574198: 
          localObject2 = "failure";
          bool5 = paramh.equals(localObject2);
          if (bool5) {
            n = 3;
          }
          break;
        case -1867169789: 
          localObject2 = "success";
          bool5 = paramh.equals(localObject2);
          if (bool5) {
            n = 0;
          }
          break;
        }
        n = -1;
        if (n == 0)
        {
          paramh = h;
          localObject2 = itemView.getContext().getResources();
          m = R.color.white;
          i3 = ((Resources)localObject2).getColor(m);
          paramh.setTextColor(i3);
          h.setText("Refunded");
          paramh = h;
          localObject2 = itemView.getResources();
          m = R.drawable.bg_green_solid_rounded;
          localObject2 = ((Resources)localObject2).getDrawable(m);
          paramh.setBackground((Drawable)localObject2);
        }
      }
      paramh = w.a(itemView.getContext());
      localObject1 = t;
      paramh = paramh.a((String)localObject1);
      int i7 = R.drawable.ic_place_holder_circle;
      paramh = paramh.a(i7);
      localObject1 = f;
      paramh.a((ImageView)localObject1, null);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */