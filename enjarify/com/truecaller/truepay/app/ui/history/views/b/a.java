package com.truecaller.truepay.app.ui.history.views.b;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import c.a.m;
import com.d.b.ab;
import com.d.b.w;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.menu;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.base.widgets.CircleImageView;
import com.truecaller.truepay.app.ui.history.b.b.1;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.history.views.e.b.a;
import com.truecaller.truepay.app.ui.history.views.e.b.b;
import com.truecaller.truepay.app.ui.history.views.e.g;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.ui.webapps.WebAppActivity;
import com.truecaller.truepay.app.ui.webapps.ixigo.IxigoBookingData;
import com.truecaller.truepay.app.ui.webapps.ixigo.IxigoBookingDetail;
import com.truecaller.truepay.app.utils.af;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.data.api.model.ak;
import com.truecaller.utils.extensions.t;
import io.reactivex.q;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class a
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements b.b, com.truecaller.truepay.app.ui.history.views.e.c, com.truecaller.truepay.app.ui.history.views.e.e
{
  TextView A;
  ConstraintLayout B;
  TextView C;
  TextView D;
  View E;
  TextView F;
  TextView G;
  View H;
  TextView I;
  TextView J;
  ImageView K;
  TextView L;
  TextView M;
  ConstraintLayout N;
  ConstraintLayout O;
  TextView P;
  ImageView Q;
  ConstraintLayout R;
  TextView S;
  TextView T;
  TextView U;
  RelativeLayout V;
  Group W;
  Group X;
  TextView Y;
  TextView Z;
  public com.truecaller.utils.n a;
  TextView aa;
  TextView ab;
  TextView ac;
  TextView ad;
  TextView ae;
  TextView af;
  TextView ag;
  View ah;
  View ai;
  ImageView aj;
  TextView ak;
  TextView al;
  TextView am;
  TextView an;
  com.truecaller.truepay.app.ui.history.views.a ao;
  private DecimalFormat ap;
  private ProgressDialog aq;
  private g ar;
  private h as;
  private String at;
  public com.truecaller.truepay.app.ui.history.b.b b;
  public b.a c;
  public com.truecaller.truepay.app.ui.history.views.c.c d;
  public com.truecaller.truepay.app.ui.history.views.c.b e;
  public com.truecaller.truepay.app.ui.history.views.c.a f;
  public au g;
  public com.truecaller.truepay.data.e.e h;
  public com.truecaller.truepay.app.utils.a i;
  public r j;
  public ax k;
  public com.truecaller.truepay.data.e.e l;
  public com.truecaller.truepay.data.e.e n;
  List o;
  List p;
  List q;
  List r;
  List s;
  Toolbar t;
  CircleImageView u;
  TextView v;
  TextView w;
  TextView x;
  View y;
  TextView z;
  
  public a()
  {
    DecimalFormat localDecimalFormat = new java/text/DecimalFormat;
    localDecimalFormat.<init>("#,###.##");
    ap = localDecimalFormat;
  }
  
  public static a a(h paramh)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("item", paramh);
    paramh = new com/truecaller/truepay/app/ui/history/views/b/a;
    paramh.<init>();
    paramh.setArguments(localBundle);
    return paramh;
  }
  
  private static String a(com.truecaller.truepay.app.ui.history.models.o paramo)
  {
    String str1 = "booking";
    String str2 = f;
    boolean bool1 = str1.equalsIgnoreCase(str2);
    if (bool1)
    {
      str1 = "redbus";
      str2 = r;
      bool1 = str1.equalsIgnoreCase(str2);
      if (bool1)
      {
        str1 = h;
        paramo = f;
        break label122;
      }
    }
    str1 = "wallet";
    str2 = f;
    bool1 = str1.equalsIgnoreCase(str2);
    if (!bool1)
    {
      str1 = "google_play";
      str2 = f;
      bool1 = str1.equalsIgnoreCase(str2);
      if (!bool1)
      {
        str1 = i;
        paramo = f;
        str2 = "recharge";
        boolean bool2 = paramo.equalsIgnoreCase(str2);
        if (bool2) {
          paramo = "recharge";
        } else {
          paramo = "bill payment";
        }
        label122:
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = str1;
        arrayOfObject[1] = paramo;
        return au.b(String.format("%s %s", arrayOfObject));
      }
    }
    return h;
  }
  
  private void a(int paramInt, h paramh)
  {
    Object localObject1 = a;
    Object localObject2 = getb;
    int m = ((String)localObject2).hashCode();
    switch (m)
    {
    default: 
      break;
    case 2140067083: 
      localObject1 = "action.page.repeat_pay";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0) {
        paramInt = 10;
      }
      break;
    case 1661903715: 
      localObject1 = "action.page.need_help";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0) {
        paramInt = 8;
      }
      break;
    case 970112032: 
      localObject1 = "action.share_receipt";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0) {
        paramInt = 7;
      }
      break;
    case 872695663: 
      localObject1 = "action.page.forgot_upi_pin";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0) {
        paramInt = 4;
      }
      break;
    case 744347170: 
      localObject1 = "action.page.repeat";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0) {
        paramInt = 9;
      }
      break;
    case 440013445: 
      localObject1 = "action.dispute_status";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0) {
        paramInt = 1;
      }
      break;
    case 319442005: 
      localObject1 = "action.say_thanks";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0) {
        paramInt = 3;
      }
      break;
    case -399989823: 
      localObject1 = "action.check_status";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0) {
        paramInt = 2;
      }
      break;
    case -721197651: 
      localObject1 = "action.page.reset_upi_pin";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0) {
        paramInt = 6;
      }
      break;
    case -737803847: 
      localObject1 = "action.raise_dispute";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0)
      {
        paramInt = 0;
        localObject2 = null;
      }
      break;
    case -1912660920: 
      localObject1 = "action.page.create_upi_pin";
      paramInt = ((String)localObject2).equals(localObject1);
      if (paramInt != 0) {
        paramInt = 5;
      }
      break;
    }
    paramInt = -1;
    Object localObject3;
    String str1;
    switch (paramInt)
    {
    default: 
      break;
    case 10: 
      localObject2 = Truepay.getInstance().getAnalyticLoggerHelper();
      localObject1 = "history_repeat";
      ((com.truecaller.truepay.data.b.a)localObject2).e((String)localObject1);
      localObject3 = ar;
      str1 = o.a;
      String str2 = l;
      String str3 = g;
      localObject2 = o;
      String str4 = b;
      String str5 = q;
      ((g)localObject3).a(str1, str2, str3, str4, str5);
      break;
    case 9: 
      Truepay.getInstance().getAnalyticLoggerHelper().f("history_repeat");
      localObject2 = ar;
      localObject1 = u;
      localObject3 = w.i;
      str1 = w.r;
      paramh = w.k;
      ((g)localObject2).a((String)localObject1, (String)localObject3, str1, paramh);
      return;
    case 8: 
      localObject2 = ar;
      paramh = h.a();
      ((g)localObject2).a(paramh);
      return;
    case 7: 
      g(paramh);
      return;
    case 6: 
      localObject2 = i;
      paramh = p.e;
      localObject2 = ((com.truecaller.truepay.app.utils.a)localObject2).a(paramh);
      if (localObject2 != null)
      {
        ar.a((com.truecaller.truepay.data.api.model.a)localObject2);
        return;
      }
      break;
    case 4: 
    case 5: 
      localObject2 = i;
      paramh = p.e;
      localObject2 = ((com.truecaller.truepay.app.utils.a)localObject2).a(paramh);
      if (localObject2 != null)
      {
        ar.b((com.truecaller.truepay.data.api.model.a)localObject2);
        return;
      }
      break;
    case 3: 
      
    case 2: 
      localObject2 = j;
      paramInt = TextUtils.isEmpty((CharSequence)localObject2);
      if (paramInt == 0)
      {
        localObject2 = "";
        m = R.string.check_with_bank;
        localObject1 = getString(m);
        boolean bool = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool)
        {
          localObject3 = aq;
          ((ProgressDialog)localObject3).setTitle((CharSequence)localObject2);
        }
        aq.setMessage((CharSequence)localObject1);
        aq.show();
        localObject2 = b;
        localObject1 = new com/truecaller/truepay/data/api/model/ak;
        localObject3 = j;
        str1 = c.a();
        ((ak)localObject1).<init>((String)localObject3, str1);
        localObject1 = a.a((ak)localObject1);
        localObject3 = io.reactivex.g.a.b();
        localObject1 = ((io.reactivex.o)localObject1).b((io.reactivex.n)localObject3);
        localObject3 = io.reactivex.android.b.a.a();
        localObject1 = ((io.reactivex.o)localObject1).a((io.reactivex.n)localObject3);
        localObject3 = new com/truecaller/truepay/app/ui/history/b/b$1;
        ((b.1)localObject3).<init>((com.truecaller.truepay.app.ui.history.b.b)localObject2, paramh);
        ((io.reactivex.o)localObject1).a((q)localObject3);
        return;
      }
      break;
    case 0: 
    case 1: 
      localObject2 = d.a(paramh);
      t = this;
      ar.a((Fragment)localObject2);
      return;
    }
  }
  
  private void a(com.truecaller.truepay.app.ui.history.models.a parama, h paramh, TextView paramTextView)
  {
    Object localObject = "action.raise_dispute";
    parama = b;
    boolean bool1 = ((String)localObject).equalsIgnoreCase(parama);
    if (bool1)
    {
      bool1 = g();
      if (bool1) {
        return;
      }
      parama = new java/text/SimpleDateFormat;
      localObject = "yyyy-MM-dd HH:mm:ss";
      Locale localLocale = Locale.US;
      parama.<init>((String)localObject, localLocale);
      try
      {
        localObject = b;
        parama = parama.parse((String)localObject);
        localObject = Calendar.getInstance();
        ((Calendar)localObject).setTime(parama);
        parama = "pending";
        paramh = e;
        bool1 = parama.equalsIgnoreCase(paramh);
        int i2 = 5;
        int m;
        if (bool1)
        {
          m = 3;
          ((Calendar)localObject).add(i2, m);
        }
        else
        {
          m = 1;
          ((Calendar)localObject).add(i2, m);
        }
        parama = ((Calendar)localObject).getTime();
        paramh = new java/util/Date;
        paramh.<init>();
        boolean bool2 = parama.after(paramh);
        if (bool2)
        {
          int i1 = 8;
          paramTextView.setVisibility(i1);
        }
        return;
      }
      catch (ParseException localParseException) {}
    }
  }
  
  private void a(h paramh, String paramString)
  {
    paramh = e;
    int m = paramh.hashCode();
    int i1 = -1086574198;
    String str;
    boolean bool;
    if (m != i1)
    {
      i1 = -682587753;
      if (m != i1)
      {
        i1 = -248987413;
        if (m == i1)
        {
          str = "initiated";
          bool = paramh.equals(str);
          if (bool)
          {
            bool = true;
            break label123;
          }
        }
      }
      else
      {
        str = "pending";
        bool = paramh.equals(str);
        if (bool)
        {
          bool = false;
          paramh = null;
          break label123;
        }
      }
    }
    else
    {
      str = "failure";
      bool = paramh.equals(str);
      if (bool)
      {
        i2 = 2;
        break label123;
      }
    }
    int i2 = -1;
    switch (i2)
    {
    default: 
      label123:
      F.setText(paramString);
      return;
    }
    paramh = F;
    int i3 = R.string.bank_account;
    paramh.setText(i3);
  }
  
  private void a(String paramString1, int paramInt, String paramString2)
  {
    w.setText(paramString1);
    w.setTextColor(paramInt);
    z.setText(paramString2);
  }
  
  private String b(h paramh)
  {
    Object localObject = i;
    boolean bool = ((String)localObject).equals("pay");
    if (!bool)
    {
      localObject = i;
      str = "collect_request_pay";
      bool = ((String)localObject).equals(str);
      if (!bool)
      {
        localObject = i;
        str = "collect";
        bool = ((String)localObject).equals(str);
        if (bool)
        {
          localObject = "outgoing";
          str = d;
          bool = ((String)localObject).equalsIgnoreCase(str);
          if (bool)
          {
            localObject = o;
            if (localObject != null) {
              return o.a;
            }
            return "";
          }
        }
        af.a(o, false);
        return "";
      }
    }
    localObject = "incoming";
    String str = d;
    bool = ((String)localObject).equalsIgnoreCase(str);
    if (bool)
    {
      af.a(o, false);
      localObject = p;
      if (localObject != null) {
        return p.c;
      }
      return "";
    }
    localObject = o;
    if (localObject != null) {
      return o.a;
    }
    return "";
  }
  
  private void b(com.truecaller.truepay.app.ui.history.models.o paramo)
  {
    W.setVisibility(0);
    TextView localTextView = ab;
    String str = h;
    localTextView.setText(str);
    localTextView = Z;
    paramo = b;
    localTextView.setText(paramo);
  }
  
  private void b(String paramString1, String paramString2)
  {
    a.a locala = new com/truecaller/truepay/app/ui/history/views/b/a$a;
    locala.<init>(this, this, paramString1, paramString2);
    paramString1 = new String[] { "bank_customer_numbers.txt" };
    locala.execute(paramString1);
  }
  
  private void c(h paramh)
  {
    Object localObject = f;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject);
    String str1 = null;
    if (bool1)
    {
      localObject = q;
      af.a((List)localObject, false);
    }
    localObject = J;
    String str2 = f;
    ((TextView)localObject).setText(str2);
    localObject = i;
    int i3 = ((String)localObject).hashCode();
    String str3;
    boolean bool2;
    switch (i3)
    {
    default: 
      break;
    case 949444906: 
      str3 = "collect";
      bool1 = ((String)localObject).equals(str3);
      if (bool1)
      {
        bool1 = false;
        localObject = null;
      }
      break;
    case 24489626: 
      str3 = "cashback";
      bool1 = ((String)localObject).equals(str3);
      if (bool1) {
        int m = 4;
      }
      break;
    case 110760: 
      str3 = "pay";
      bool2 = ((String)localObject).equals(str3);
      if (bool2) {
        bool2 = true;
      }
      break;
    case -192199837: 
      str3 = "collect_request_pay";
      bool2 = ((String)localObject).equals(str3);
      if (bool2) {
        int i1 = 2;
      }
      break;
    case -934813832: 
      str3 = "refund";
      boolean bool3 = ((String)localObject).equals(str3);
      if (bool3) {
        i2 = 3;
      }
      break;
    }
    int i2 = -1;
    switch (i2)
    {
    default: 
      d(paramh);
      return;
    case 3: 
    case 4: 
      e(paramh);
      return;
    case 0: 
      localObject = "incoming";
      str2 = d;
      bool4 = ((String)localObject).equalsIgnoreCase(str2);
      if (bool4)
      {
        localObject = p;
        af.a((List)localObject, false);
      }
      else
      {
        d(paramh);
      }
      break;
    }
    localObject = "incoming";
    str1 = d;
    boolean bool4 = ((String)localObject).equalsIgnoreCase(str1);
    if (bool4)
    {
      e(paramh);
      return;
    }
    d(paramh);
  }
  
  private void c(com.truecaller.truepay.app.ui.history.models.o paramo)
  {
    X.setVisibility(0);
    Object localObject = ad;
    String str = b;
    ((TextView)localObject).setText(str);
    localObject = ae;
    paramo = h;
    ((TextView)localObject).setText(paramo);
    paramo = af;
    int m = R.string.all_operator;
    localObject = getString(m);
    paramo.setText((CharSequence)localObject);
  }
  
  private void d(h paramh)
  {
    Object localObject = p;
    if (localObject != null)
    {
      localObject = p.f;
      boolean bool = TextUtils.isEmpty((CharSequence)localObject);
      if (bool)
      {
        localObject = p;
        str1 = "";
        f = str1;
      }
      int m = R.string.debited_from;
      localObject = getString(m);
      a(paramh, (String)localObject);
      localObject = G;
      int i1 = R.string.bank_name_account_number;
      String str1 = getString(i1);
      int i2 = 2;
      Object[] arrayOfObject = new Object[i2];
      String str2 = p.h;
      arrayOfObject[0] = str2;
      int i3 = 1;
      paramh = p.f;
      arrayOfObject[i3] = paramh;
      paramh = String.format(str1, arrayOfObject);
      ((TextView)localObject).setText(paramh);
    }
  }
  
  private void d(String paramString)
  {
    boolean bool = isAdded();
    if (bool)
    {
      aq.dismiss();
      bool = false;
      a(paramString, null);
    }
  }
  
  private void e(h paramh)
  {
    Object localObject = o;
    if (localObject != null)
    {
      int m = R.string.credited_to;
      localObject = getString(m);
      a(paramh, (String)localObject);
      localObject = G;
      paramh = o.a;
      ((TextView)localObject).setText(paramh);
    }
  }
  
  private void f()
  {
    Object localObject1 = as;
    if (localObject1 != null)
    {
      localObject1 = x;
      int m = R.string.rs_amount;
      Object localObject2 = getString(m);
      boolean bool6 = true;
      Object localObject3 = new Object[bool6];
      Object localObject4 = ap;
      double d1 = Double.parseDouble(as.l);
      localObject4 = ((DecimalFormat)localObject4).format(d1);
      localObject3[0] = localObject4;
      localObject2 = String.format((String)localObject2, (Object[])localObject3);
      ((TextView)localObject1).setText((CharSequence)localObject2);
      localObject1 = v;
      localObject2 = d;
      localObject3 = as;
      localObject4 = e;
      localObject2 = ((com.truecaller.truepay.app.ui.history.views.c.c)localObject2).a((h)localObject3, (com.truecaller.truepay.app.ui.history.views.c.b)localObject4);
      ((TextView)localObject1).setText((CharSequence)localObject2);
      localObject1 = u;
      localObject2 = as;
      localObject3 = f;
      localObject4 = e;
      com.truecaller.truepay.app.ui.history.views.c.c.a((ImageView)localObject1, (h)localObject2, (com.truecaller.truepay.app.ui.history.views.c.a)localObject3, (com.truecaller.truepay.app.ui.history.views.c.b)localObject4);
      localObject1 = d;
      localObject2 = as.e;
      localObject1 = ((com.truecaller.truepay.app.ui.history.views.c.c)localObject1).a((String)localObject2);
      localObject2 = d;
      localObject3 = as;
      m = ((com.truecaller.truepay.app.ui.history.views.c.c)localObject2).a((h)localObject3);
      localObject3 = as.h;
      a((String)localObject1, m, (String)localObject3);
      localObject1 = D;
      localObject2 = as;
      localObject2 = b((h)localObject2);
      ((TextView)localObject1).setText((CharSequence)localObject2);
      localObject1 = as;
      c((h)localObject1);
      localObject1 = as;
      f((h)localObject1);
      localObject1 = t;
      m = R.string.transction_history_header;
      ((Toolbar)localObject1).setTitle(m);
      localObject1 = t;
      localObject2 = d;
      localObject3 = as.b;
      localObject2 = ((com.truecaller.truepay.app.ui.history.views.c.c)localObject2).b((String)localObject3);
      ((Toolbar)localObject1).setSubtitle((CharSequence)localObject2);
      localObject1 = t;
      localObject2 = getActivity();
      int i5 = R.style.ToolbarSubtitleAppearance;
      ((Toolbar)localObject1).setSubtitleTextAppearance((Context)localObject2, i5);
      localObject1 = t;
      localObject2 = getActivity();
      i5 = R.style.ToolbarTitleAppearance;
      ((Toolbar)localObject1).setTitleTextAppearance((Context)localObject2, i5);
      localObject1 = as.e;
      localObject2 = "success";
      boolean bool14 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
      if (bool14)
      {
        localObject1 = "outgoing";
        localObject2 = as.d;
        bool14 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (bool14)
        {
          localObject1 = as.i;
          localObject2 = "collect";
          bool14 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
          if (!bool14)
          {
            localObject1 = Q;
            ((ImageView)localObject1).setVisibility(0);
          }
        }
      }
      localObject1 = as.r;
      bool14 = TextUtils.isEmpty((CharSequence)localObject1);
      m = 8;
      if (!bool14)
      {
        localObject1 = "incoming";
        localObject3 = as.d;
        bool14 = ((String)localObject1).equalsIgnoreCase((String)localObject3);
        if (!bool14)
        {
          M.setVisibility(m);
          O.setVisibility(0);
          localObject1 = P;
          i5 = R.string.reference_number;
          localObject3 = getString(i5);
          localObject4 = new Object[bool6];
          String str1 = as.s;
          localObject4[0] = str1;
          localObject3 = String.format((String)localObject3, (Object[])localObject4);
          ((TextView)localObject1).setText((CharSequence)localObject3);
          break label547;
        }
      }
      localObject1 = O;
      ((ConstraintLayout)localObject1).setVisibility(m);
      label547:
      localObject1 = as.g;
      bool14 = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool14)
      {
        localObject1 = T;
        localObject2 = as.g;
        ((TextView)localObject1).setText((CharSequence)localObject2);
      }
      else
      {
        localObject1 = V;
        ((RelativeLayout)localObject1).setVisibility(m);
      }
      localObject1 = as.p;
      if (localObject1 != null)
      {
        localObject1 = "outgoing";
        localObject2 = as.d;
        bool14 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (bool14)
        {
          localObject1 = "collect";
          localObject2 = as.i;
          bool14 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
          if (!bool14)
          {
            localObject1 = as.p.g;
            localObject2 = as.p.h;
            b((String)localObject1, (String)localObject2);
          }
        }
      }
      localObject1 = as.w;
      if (localObject1 != null)
      {
        localObject2 = e;
        if (localObject2 != null)
        {
          localObject2 = f;
          if (localObject2 != null)
          {
            i5 = ((String)localObject2).hashCode();
            int i13 = -806191449;
            boolean bool9;
            if (i5 != i13)
            {
              i13 = -795192327;
              if (i5 != i13)
              {
                i13 = -334831238;
                if (i5 != i13)
                {
                  i13 = 889956464;
                  if (i5 == i13)
                  {
                    localObject3 = "bill_pay";
                    boolean bool7 = ((String)localObject2).equals(localObject3);
                    if (bool7)
                    {
                      int i6 = 3;
                      break label871;
                    }
                  }
                }
                else
                {
                  localObject3 = "google_play";
                  boolean bool8 = ((String)localObject2).equals(localObject3);
                  if (bool8)
                  {
                    int i7 = 2;
                    break label871;
                  }
                }
              }
              else
              {
                localObject3 = "wallet";
                bool9 = ((String)localObject2).equals(localObject3);
                if (bool9)
                {
                  bool9 = true;
                  break label871;
                }
              }
            }
            else
            {
              localObject3 = "recharge";
              bool9 = ((String)localObject2).equals(localObject3);
              if (bool9)
              {
                bool9 = false;
                localObject3 = null;
                break label871;
              }
            }
            int i8 = -1;
            switch (i8)
            {
            default: 
              localObject3 = "Unkown utility type, skipping UI";
              new String[1][0] = localObject3;
              break;
            case 3: 
              b((com.truecaller.truepay.app.ui.history.models.o)localObject1);
              break;
            case 0: 
            case 1: 
            case 2: 
              label871:
              c((com.truecaller.truepay.app.ui.history.models.o)localObject1);
            }
            localObject3 = v;
            localObject4 = a((com.truecaller.truepay.app.ui.history.models.o)localObject1);
            ((TextView)localObject3).setText((CharSequence)localObject4);
            localObject3 = c;
            if (localObject3 != null) {
              localObject3 = c;
            } else {
              localObject3 = as.e;
            }
            localObject4 = w;
            String str2 = au.b((String)localObject3);
            ((TextView)localObject4).setText(str2);
            i13 = ((String)localObject3).hashCode();
            int i14 = -1867169789;
            if (i13 != i14)
            {
              i14 = -1086574198;
              boolean bool11;
              if (i13 != i14)
              {
                i14 = -682587753;
                if (i13 != i14)
                {
                  i14 = -248987413;
                  if (i13 == i14)
                  {
                    localObject4 = "initiated";
                    boolean bool10 = ((String)localObject3).equals(localObject4);
                    if (bool10)
                    {
                      int i9 = 2;
                      break label1165;
                    }
                  }
                }
                else
                {
                  localObject4 = "pending";
                  bool11 = ((String)localObject3).equals(localObject4);
                  if (bool11)
                  {
                    bool11 = false;
                    localObject3 = null;
                    break label1165;
                  }
                }
              }
              else
              {
                localObject4 = "failure";
                bool11 = ((String)localObject3).equals(localObject4);
                if (bool11)
                {
                  int i10 = 3;
                  break label1165;
                }
              }
            }
            else
            {
              localObject4 = "success";
              boolean bool12 = ((String)localObject3).equals(localObject4);
              if (bool12)
              {
                bool12 = true;
                break label1165;
              }
            }
            int i11 = -1;
            switch (i11)
            {
            default: 
              localObject3 = w;
              localObject4 = getResources();
              i14 = R.color.status_failure_color;
              i13 = ((Resources)localObject4).getColor(i14);
              ((TextView)localObject3).setTextColor(i13);
              break;
            case 1: 
            case 2: 
              localObject3 = w;
              localObject4 = getResources();
              i14 = R.color.status_success_color;
              i13 = ((Resources)localObject4).getColor(i14);
              ((TextView)localObject3).setTextColor(i13);
              break;
            case 0: 
              label1165:
              localObject3 = w;
              localObject4 = getResources();
              i14 = R.color.status_pending_color;
              i13 = ((Resources)localObject4).getColor(i14);
              ((TextView)localObject3).setTextColor(i13);
            }
            localObject3 = "booking";
            boolean bool13 = ((String)localObject3).equalsIgnoreCase((String)localObject2);
            boolean bool1;
            if (bool13)
            {
              localObject2 = "redbus";
              localObject3 = r;
              bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
              if (bool1)
              {
                localObject2 = b;
                ((com.truecaller.truepay.app.ui.history.b.b)localObject2).a((com.truecaller.truepay.app.ui.history.models.o)localObject1);
              }
              else
              {
                localObject2 = "ixigo";
                localObject3 = r;
                bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
                if (bool1)
                {
                  localObject2 = b;
                  ((com.truecaller.truepay.app.ui.history.b.b)localObject2).b((com.truecaller.truepay.app.ui.history.models.o)localObject1);
                }
              }
            }
            else
            {
              localObject3 = "google_play";
              bool1 = ((String)localObject3).equalsIgnoreCase((String)localObject2);
              if (bool1)
              {
                localObject2 = b;
                localObject3 = y;
                ((com.truecaller.truepay.app.ui.history.b.b)localObject2).a((String)localObject3);
              }
            }
            localObject2 = m;
            if (localObject2 != null)
            {
              localObject2 = m;
              i12 = ((String)localObject2).hashCode();
              boolean bool2;
              switch (i12)
              {
              default: 
                break;
              case 531647627: 
                localObject3 = "not_applicable";
                bool1 = ((String)localObject2).equals(localObject3);
                if (bool1) {
                  int i1 = 4;
                }
                break;
              case -248987413: 
                localObject3 = "initiated";
                bool2 = ((String)localObject2).equals(localObject3);
                if (bool2) {
                  bool2 = true;
                }
                break;
              case -682587753: 
                localObject3 = "pending";
                bool2 = ((String)localObject2).equals(localObject3);
                if (bool2) {
                  int i2 = 2;
                }
                break;
              case -1086574198: 
                localObject3 = "failure";
                boolean bool3 = ((String)localObject2).equals(localObject3);
                if (bool3) {
                  int i3 = 3;
                }
                break;
              case -1867169789: 
                localObject3 = "success";
                boolean bool4 = ((String)localObject2).equals(localObject3);
                if (bool4)
                {
                  bool4 = false;
                  localObject2 = null;
                }
                break;
              }
              int i4 = -1;
              if (i4 == 0)
              {
                localObject2 = w;
                localObject3 = getResources();
                i13 = R.color.white;
                i12 = ((Resources)localObject3).getColor(i13);
                ((TextView)localObject2).setTextColor(i12);
                w.setText("Refunded");
                localObject2 = w;
                localObject3 = getResources();
                i13 = R.drawable.bg_green_solid_rounded;
                localObject3 = ((Resources)localObject3).getDrawable(i13);
                ((TextView)localObject2).setBackground((Drawable)localObject3);
              }
            }
            localObject2 = w.a(getActivity());
            localObject3 = t;
            localObject2 = ((w)localObject2).a((String)localObject3);
            int i12 = R.drawable.ic_place_holder_circle;
            localObject2 = ((ab)localObject2).a(i12);
            localObject3 = u;
            i13 = 0;
            localObject4 = null;
            ((ab)localObject2).a((ImageView)localObject3, null);
          }
          localObject2 = s;
          boolean bool5 = TextUtils.isEmpty((CharSequence)localObject2);
          if (bool5)
          {
            af.a(r, false);
            return;
          }
          af.a(r, bool6);
          localObject2 = L;
          localObject1 = s;
          ((TextView)localObject2).setText((CharSequence)localObject1);
        }
      }
    }
  }
  
  private void f(h paramh)
  {
    Object localObject1 = A;
    int m = 8;
    ((TextView)localObject1).setVisibility(m);
    M.setVisibility(m);
    localObject1 = a;
    if (localObject1 != null)
    {
      localObject1 = a;
      int i1 = ((List)localObject1).size();
      m = 0;
      TextView localTextView = null;
      Object localObject2;
      if (i1 > 0)
      {
        localObject1 = (com.truecaller.truepay.app.ui.history.models.a)a.get(0);
        if (localObject1 != null)
        {
          localObject2 = a;
          boolean bool1 = TextUtils.isEmpty((CharSequence)localObject2);
          if (!bool1)
          {
            A.setVisibility(0);
            localObject2 = A;
            String str = a.toUpperCase();
            ((TextView)localObject2).setText(str);
            localObject2 = A;
            a((com.truecaller.truepay.app.ui.history.models.a)localObject1, paramh, (TextView)localObject2);
          }
        }
      }
      localObject1 = a;
      i1 = ((List)localObject1).size();
      int i2 = 2;
      if (i1 == i2)
      {
        localObject1 = a;
        i2 = 1;
        localObject1 = (com.truecaller.truepay.app.ui.history.models.a)((List)localObject1).get(i2);
        if (localObject1 != null)
        {
          localObject2 = a;
          boolean bool2 = TextUtils.isEmpty((CharSequence)localObject2);
          if (!bool2)
          {
            M.setVisibility(0);
            localTextView = M;
            localObject2 = a.toUpperCase();
            localTextView.setText((CharSequence)localObject2);
            localTextView = M;
            a((com.truecaller.truepay.app.ui.history.models.a)localObject1, paramh, localTextView);
          }
        }
      }
    }
  }
  
  private void g(h paramh)
  {
    Object localObject1 = getContext();
    Object localObject2 = "android.permission.WRITE_EXTERNAL_STORAGE";
    int m = android.support.v4.app.a.a((Context)localObject1, (String)localObject2);
    if (m != 0)
    {
      paramh = getActivity();
      localObject1 = "android.permission.WRITE_EXTERNAL_STORAGE";
      boolean bool2 = android.support.v4.app.a.a(paramh, (String)localObject1);
      if (bool2)
      {
        paramh = a;
        m = R.string.read_phone_permission;
        localObject2 = new Object[0];
        paramh = paramh.a(m, (Object[])localObject2);
        m = 0;
        localObject1 = null;
        a(paramh, null);
      }
      paramh = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
      requestPermissions(paramh, 1005);
      return;
    }
    String str1 = i.d();
    localObject1 = getActivity();
    if ((localObject1 != null) && (paramh != null))
    {
      boolean bool1 = TextUtils.isEmpty(str1);
      if (!bool1)
      {
        localObject1 = g;
        if (localObject1 != null)
        {
          localObject1 = w;
          if (localObject1 != null)
          {
            localObject1 = w.s;
            if (localObject1 != null)
            {
              localObject1 = p.h;
              localObject2 = p.f;
              String str2 = au.a((String)localObject1, (String)localObject2);
              p localp = new com/truecaller/truepay/app/ui/transaction/b/p;
              localp.<init>();
              localObject1 = w.v;
              y = ((String)localObject1);
              localObject1 = w.w;
              z = ((String)localObject1);
              localObject1 = w.h;
              t = ((String)localObject1);
              localObject1 = w.b;
              u = ((String)localObject1);
              localObject1 = w.t;
              w = ((String)localObject1);
              localObject1 = new com/truecaller/truepay/app/ui/transaction/b/l;
              ((l)localObject1).<init>();
              localObject2 = f;
              a = ((String)localObject2);
              localObject2 = w.s;
              c = ((String)localObject2);
              j = ((l)localObject1);
              localObject1 = new java/text/SimpleDateFormat;
              ((SimpleDateFormat)localObject1).<init>("yyyy-MM-dd HH:mm:ss");
              localObject2 = new java/util/Date;
              ((Date)localObject2).<init>();
              localObject1 = ((SimpleDateFormat)localObject1).format((Date)localObject2);
              l = ((String)localObject1);
              paramh = l;
              e = paramh;
              paramh = new com/truecaller/truepay/app/ui/payments/views/b/d;
              localObject3 = getActivity();
              r localr = j;
              String str3 = l.a();
              localObject2 = paramh;
              paramh.<init>((Context)localObject3, localp, str1, localr, str3, str2);
              paramh.a();
              return;
            }
          }
          localObject1 = new com/truecaller/truepay/app/ui/history/views/c/d;
          localObject2 = getActivity();
          Object localObject3 = g;
          ((com.truecaller.truepay.app.ui.history.views.c.d)localObject1).<init>((Context)localObject2, paramh, str1, (au)localObject3);
          ((com.truecaller.truepay.app.ui.history.views.c.d)localObject1).a();
        }
      }
    }
  }
  
  private boolean g()
  {
    return n.a().equals("baroda");
  }
  
  public final int a()
  {
    return R.layout.fragment_history_details;
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama, String paramString)
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      localContext = getContext();
      parama = WebAppActivity.a(localContext, parama, paramString);
      startActivity(parama);
    }
  }
  
  public final void a(IxigoBookingDetail paramIxigoBookingDetail)
  {
    com.truecaller.truepay.app.ui.history.views.a locala = ao;
    Object localObject1 = "bookingDetail";
    c.g.b.k.b(paramIxigoBookingDetail, (String)localObject1);
    d = paramIxigoBookingDetail;
    paramIxigoBookingDetail = ((Iterable)paramIxigoBookingDetail.getDetails()).iterator();
    for (;;)
    {
      boolean bool = paramIxigoBookingDetail.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (IxigoBookingData)paramIxigoBookingDetail.next();
      Object localObject2 = a;
      int m = R.layout.layout_history_details_ixigo;
      localObject2 = ((LayoutInflater)localObject2).inflate(m, null, false);
      Object localObject3 = new com/truecaller/truepay/app/ui/history/views/b;
      c.g.b.k.a(localObject2, "bookingView");
      ((com.truecaller.truepay.app.ui.history.views.b)localObject3).<init>((View)localObject2);
      c.g.b.k.b(localObject1, "booking");
      Object localObject4 = a;
      c.g.b.k.a(localObject4, "bookingDate");
      Object localObject5 = (CharSequence)((IxigoBookingData)localObject1).getBooking_date();
      ((TextView)localObject4).setText((CharSequence)localObject5);
      localObject4 = b;
      c.g.b.k.a(localObject4, "ticketPnr");
      localObject5 = k.getContext();
      int i1 = R.string.ixigo_pnr;
      Object[] arrayOfObject = new Object[1];
      String str = ((IxigoBookingData)localObject1).getPnr();
      arrayOfObject[0] = str;
      CharSequence localCharSequence = (CharSequence)((Context)localObject5).getString(i1, arrayOfObject);
      ((TextView)localObject4).setText(localCharSequence);
      localObject4 = c;
      c.g.b.k.a(localObject4, "fromPlace");
      localCharSequence = (CharSequence)((IxigoBookingData)localObject1).getSource_name();
      ((TextView)localObject4).setText(localCharSequence);
      localObject4 = d;
      c.g.b.k.a(localObject4, "toPlace");
      localCharSequence = (CharSequence)((IxigoBookingData)localObject1).getDestination_name();
      ((TextView)localObject4).setText(localCharSequence);
      localObject4 = f;
      c.g.b.k.a(localObject4, "fromPlaceCode");
      localCharSequence = (CharSequence)((IxigoBookingData)localObject1).getSource_code();
      ((TextView)localObject4).setText(localCharSequence);
      localObject4 = e;
      c.g.b.k.a(localObject4, "toPlaceCode");
      localCharSequence = (CharSequence)((IxigoBookingData)localObject1).getDestination_code();
      ((TextView)localObject4).setText(localCharSequence);
      localObject4 = g;
      c.g.b.k.a(localObject4, "flightNameHeader");
      localCharSequence = (CharSequence)((IxigoBookingData)localObject1).getService_provider_name();
      ((TextView)localObject4).setText(localCharSequence);
      localObject4 = h;
      c.g.b.k.a(localObject4, "departTime");
      localCharSequence = (CharSequence)((IxigoBookingData)localObject1).getDepart_time();
      ((TextView)localObject4).setText(localCharSequence);
      localObject4 = i;
      c.g.b.k.a(localObject4, "flightNumber");
      localCharSequence = (CharSequence)((IxigoBookingData)localObject1).getFlight_number();
      ((TextView)localObject4).setText(localCharSequence);
      localObject3 = j;
      c.g.b.k.a(localObject3, "arrivalTime");
      localObject4 = (CharSequence)((IxigoBookingData)localObject1).getArrival_time();
      ((TextView)localObject3).setText((CharSequence)localObject4);
      b.addView((View)localObject2);
      localObject2 = c;
      c.g.b.k.a(localObject2, "passengerNames");
      localObject1 = ((IxigoBookingData)localObject1).getPassenger_names();
      localObject3 = localObject1;
      localObject3 = (Iterable)localObject1;
      localObject4 = (CharSequence)" • ";
      localCharSequence = null;
      localObject5 = null;
      i1 = 0;
      arrayOfObject = null;
      str = null;
      int i2 = 62;
      localObject1 = (CharSequence)m.a((Iterable)localObject3, (CharSequence)localObject4, null, null, 0, null, null, i2);
      ((TextView)localObject2).setText((CharSequence)localObject1);
    }
  }
  
  public final void a(String paramString)
  {
    Toast.makeText(getActivity(), paramString, 0).show();
  }
  
  public final void a(String paramString, int paramInt)
  {
    af.a(s, false);
    w.setText(paramString);
    w.setTextColor(paramInt);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    int m = R.string.check_status_result_confirmation;
    Object localObject1 = getString(m);
    Object localObject2 = new Object[1];
    localObject2[0] = paramString1;
    localObject1 = String.format((String)localObject1, (Object[])localObject2);
    d((String)localObject1);
    paramString1 = d.a(paramString1);
    localObject1 = d;
    localObject2 = as;
    m = ((com.truecaller.truepay.app.ui.history.views.c.c)localObject1).a((h)localObject2);
    a(paramString1, m, paramString2);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    h localh = as;
    if (localh != null)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      a = localArrayList;
      localh = as;
      s = paramString2;
      r = paramString1;
      t = paramString3;
      paramString1 = new java/text/SimpleDateFormat;
      paramString3 = Locale.ENGLISH;
      paramString1.<init>("yyyy-MM-dd HH:mm:ss", paramString3);
      paramString2 = Calendar.getInstance();
      int m = 5;
      int i1 = 2;
      paramString2.add(m, i1);
      paramString3 = as;
      paramString2 = paramString2.getTime();
      paramString1 = paramString1.format(paramString2);
      c = paramString1;
      f();
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, int paramInt, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    U.setText(paramString1);
    T.setText(paramString2);
    paramString1 = j;
    paramString2 = aj;
    paramString1.a(paramString3, paramString2, paramInt, paramInt);
    ak.setText(paramString4);
    al.setText(paramString5);
    an.setText(paramString6);
    am.setText(paramString7);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean)
  {
    g localg = ar;
    if (localg != null) {
      localg.a(paramString1, paramString2, paramString3, paramString4, paramBoolean);
    }
  }
  
  public final void b()
  {
    ah.setVisibility(0);
    V.setVisibility(0);
  }
  
  public final void b(String paramString)
  {
    ai.setVisibility(0);
    ag.setText(paramString);
  }
  
  public final void c()
  {
    t.b(ao.e);
  }
  
  public final void c(String paramString)
  {
    d(paramString);
  }
  
  public final void d()
  {
    t.a(ao.e);
  }
  
  public final void e()
  {
    ah.setVisibility(8);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof g;
    if (bool)
    {
      paramContext = (g)getActivity();
      ar = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement TransactionView");
    throw paramContext;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.history.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
    setHasOptionsMenu(true);
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    int m = R.menu.menu_history;
    paramMenuInflater.inflate(m, paramMenu);
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    b.b();
    c.y_();
  }
  
  public final void onDetach()
  {
    super.onDetach();
    ar = null;
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int m = paramMenuItem.getItemId();
    boolean bool = true;
    int i1 = 16908332;
    if (m == i1)
    {
      getActivity().onBackPressed();
      return bool;
    }
    int i2 = paramMenuItem.getItemId();
    m = R.id.menu_item_history_details;
    if (i2 == m)
    {
      paramMenuItem = ar;
      String str = h.a();
      paramMenuItem.a(str);
      return bool;
    }
    return false;
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int m = 1005;
    if (paramInt == m)
    {
      paramInt = paramArrayOfInt.length;
      m = 1;
      if (paramInt == m)
      {
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0)
        {
          localObject = as;
          if (localObject == null) {
            return;
          }
          g((h)localObject);
          return;
        }
      }
      Object localObject = a;
      m = R.string.external_directory_permission_denied;
      paramArrayOfInt = new Object[0];
      localObject = ((com.truecaller.utils.n)localObject).a(m, paramArrayOfInt);
      m = 0;
      paramArrayOfString = null;
      a((String)localObject, null);
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    ar.a(false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = 3;
    Object localObject1 = new int[m];
    int i1 = R.id.tv_beneficiary_vpa_header;
    localObject1[0] = i1;
    i1 = R.id.tv_beneficiary_vpa_value;
    boolean bool1 = true;
    localObject1[bool1] = i1;
    i1 = R.id.view1;
    int i2 = 2;
    localObject1[i2] = i1;
    localObject1 = af.a(paramView, (int[])localObject1);
    o = ((List)localObject1);
    localObject1 = new int[m];
    i1 = R.id.tv_bank_name_header;
    localObject1[0] = i1;
    i1 = R.id.tv_bank_name_value;
    localObject1[bool1] = i1;
    i1 = R.id.view2;
    localObject1[i2] = i1;
    localObject1 = af.a(paramView, (int[])localObject1);
    p = ((List)localObject1);
    int i3 = 4;
    Object localObject2 = new int[i3];
    int i4 = R.id.tv_upi_ref_header;
    localObject2[0] = i4;
    i4 = R.id.tv_upi_ref_value;
    localObject2[bool1] = i4;
    i4 = R.id.im_upi_id_cop;
    localObject2[i2] = i4;
    i4 = R.id.view2;
    localObject2[m] = i4;
    localObject2 = af.a(paramView, (int[])localObject2);
    q = ((List)localObject2);
    localObject1 = new int[i3];
    i1 = R.id.tv_bbps_ref_header;
    localObject1[0] = i1;
    i1 = R.id.tv_bbps_ref_value;
    localObject1[bool1] = i1;
    i1 = R.id.im_bbps_id_cop;
    localObject1[i2] = i1;
    i1 = R.id.view7;
    localObject1[m] = i1;
    paramBundle = af.a(paramView, (int[])localObject1);
    r = paramBundle;
    paramBundle = new int[i2];
    i3 = R.id.tvBookingViewDetails;
    paramBundle[0] = i3;
    i3 = R.id.tvBookingCancel;
    paramBundle[bool1] = i3;
    paramBundle = af.a(paramView, paramBundle);
    s = paramBundle;
    m = R.id.toolbar_history_details;
    paramBundle = (Toolbar)paramView.findViewById(m);
    t = paramBundle;
    m = R.id.profile_pic;
    paramBundle = (CircleImageView)paramView.findViewById(m);
    u = paramBundle;
    m = R.id.tv_payment_header;
    paramBundle = (TextView)paramView.findViewById(m);
    v = paramBundle;
    m = R.id.tv_payment_status;
    paramBundle = (TextView)paramView.findViewById(m);
    w = paramBundle;
    m = R.id.tv_payment_amount;
    paramBundle = (TextView)paramView.findViewById(m);
    x = paramBundle;
    m = R.id.view;
    paramBundle = paramView.findViewById(m);
    y = paramBundle;
    m = R.id.tv_message_header;
    paramBundle = (TextView)paramView.findViewById(m);
    U = paramBundle;
    m = R.id.rl_message_layout;
    paramBundle = (RelativeLayout)paramView.findViewById(m);
    V = paramBundle;
    m = R.id.tv_payment_description;
    paramBundle = (TextView)paramView.findViewById(m);
    z = paramBundle;
    m = R.id.tv_payment_main_action;
    paramBundle = (TextView)paramView.findViewById(m);
    A = paramBundle;
    m = R.id.cl_history_header;
    paramBundle = (ConstraintLayout)paramView.findViewById(m);
    B = paramBundle;
    m = R.id.tv_beneficiary_vpa_header;
    paramBundle = (TextView)paramView.findViewById(m);
    C = paramBundle;
    m = R.id.tv_beneficiary_vpa_value;
    paramBundle = (TextView)paramView.findViewById(m);
    D = paramBundle;
    m = R.id.view1;
    paramBundle = paramView.findViewById(m);
    E = paramBundle;
    m = R.id.tv_bank_name_header;
    paramBundle = (TextView)paramView.findViewById(m);
    F = paramBundle;
    m = R.id.tv_bank_name_value;
    paramBundle = (TextView)paramView.findViewById(m);
    G = paramBundle;
    m = R.id.view2;
    paramBundle = paramView.findViewById(m);
    H = paramBundle;
    m = R.id.tv_upi_ref_header;
    paramBundle = (TextView)paramView.findViewById(m);
    I = paramBundle;
    m = R.id.tv_upi_ref_value;
    paramBundle = (TextView)paramView.findViewById(m);
    J = paramBundle;
    m = R.id.im_upi_id_cop;
    paramBundle = (ImageView)paramView.findViewById(m);
    K = paramBundle;
    m = R.id.tv_bbps_ref_value;
    paramBundle = (TextView)paramView.findViewById(m);
    L = paramBundle;
    m = R.id.tv_upi_report_issue;
    paramBundle = (TextView)paramView.findViewById(m);
    M = paramBundle;
    m = R.id.cl_history_content;
    paramBundle = (ConstraintLayout)paramView.findViewById(m);
    N = paramBundle;
    m = R.id.cl_history_disputed;
    paramBundle = (ConstraintLayout)paramView.findViewById(m);
    O = paramBundle;
    m = R.id.tv_dispute_status;
    paramBundle = (TextView)paramView.findViewById(m);
    P = paramBundle;
    m = R.id.im_share_receipt;
    paramBundle = (ImageView)paramView.findViewById(m);
    Q = paramBundle;
    m = R.id.cl_history_customer_care_layout;
    paramBundle = (ConstraintLayout)paramView.findViewById(m);
    R = paramBundle;
    m = R.id.tv_customer_care_description;
    paramBundle = (TextView)paramView.findViewById(m);
    S = paramBundle;
    m = R.id.tv_message_value;
    paramBundle = (TextView)paramView.findViewById(m);
    T = paramBundle;
    m = R.id.rl_message_layout;
    paramBundle = (RelativeLayout)paramView.findViewById(m);
    V = paramBundle;
    m = R.id.group_utility_bill_frag_history_details;
    paramBundle = (Group)paramView.findViewById(m);
    W = paramBundle;
    m = R.id.group_utility_recharge_frag_history_details;
    paramBundle = (Group)paramView.findViewById(m);
    X = paramBundle;
    m = R.id.tv_utility_subscriber_id_header_frag_history_details;
    paramBundle = (TextView)paramView.findViewById(m);
    Y = paramBundle;
    m = R.id.tv_utility_subscriber_id_frag_history_details;
    paramBundle = (TextView)paramView.findViewById(m);
    Z = paramBundle;
    m = R.id.tv_utility_service_provider_header_frag_history_details;
    paramBundle = (TextView)paramView.findViewById(m);
    aa = paramBundle;
    m = R.id.tv_utility_service_provider_frag_history_details;
    paramBundle = (TextView)paramView.findViewById(m);
    ab = paramBundle;
    m = R.id.tv_utility_mobile_num_header_frag_history_details;
    paramBundle = (TextView)paramView.findViewById(m);
    ac = paramBundle;
    m = R.id.tv_utility_mobile_num_frag_history_details;
    paramBundle = (TextView)paramView.findViewById(m);
    ad = paramBundle;
    m = R.id.tv_utility_operator_frag_history_details;
    paramBundle = (TextView)paramView.findViewById(m);
    ae = paramBundle;
    m = R.id.tv_utility_operator_header_frag_history_details;
    paramBundle = (TextView)paramView.findViewById(m);
    af = paramBundle;
    m = R.id.cl_booking_details;
    paramBundle = paramView.findViewById(m);
    ah = paramBundle;
    m = R.id.ivHistoryBooking;
    paramBundle = (ImageView)paramView.findViewById(m);
    aj = paramBundle;
    m = R.id.tvBookingRouteDetails;
    paramBundle = (TextView)paramView.findViewById(m);
    ak = paramBundle;
    m = R.id.tvBookingOperatorName;
    paramBundle = (TextView)paramView.findViewById(m);
    al = paramBundle;
    m = R.id.tvBookingBookingDayName;
    paramBundle = (TextView)paramView.findViewById(m);
    am = paramBundle;
    m = R.id.tvBookingBookingDate;
    paramBundle = (TextView)paramView.findViewById(m);
    an = paramBundle;
    m = R.id.cl_history_remarks;
    paramBundle = paramView.findViewById(m);
    ai = paramBundle;
    m = R.id.tvHistoryRemarks;
    paramBundle = (TextView)paramView.findViewById(m);
    ag = paramBundle;
    paramBundle = new com/truecaller/truepay/app/ui/history/views/a;
    i3 = R.id.cl_ixigo_booking_details;
    localObject1 = paramView.findViewById(i3);
    localObject2 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$Lc1cMeeeue62YiF2thLuFNYT6Hg;
    ((-..Lambda.a.Lc1cMeeeue62YiF2thLuFNYT6Hg)localObject2).<init>(this);
    paramBundle.<init>((View)localObject1, (com.truecaller.truepay.app.ui.history.views.a.a)localObject2);
    ao = paramBundle;
    paramBundle = M;
    localObject1 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$kOUKbO50KjXAPbozwhrvSNaAnYE;
    ((-..Lambda.a.kOUKbO50KjXAPbozwhrvSNaAnYE)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    paramBundle = A;
    localObject1 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$CeVMf_j-NR_1uphnd4UOVc0Ld9g;
    ((-..Lambda.a.CeVMf_j-NR_1uphnd4UOVc0Ld9g)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    paramBundle = Q;
    localObject1 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$miAxFbO-i2tTdR2MAdDKlFakSSA;
    ((-..Lambda.a.miAxFbO-i2tTdR2MAdDKlFakSSA)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    paramBundle = K;
    localObject1 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$Xk7uToX6S-6vV5i5-Ll_onnex0U;
    ((-..Lambda.a.Xk7uToX6S-6vV5i5-Ll_onnex0U)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    m = R.id.im_bbps_id_cop;
    paramBundle = paramView.findViewById(m);
    localObject1 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$LlvQ8a8y1TCqO2X2_ueeKo2GtyU;
    ((-..Lambda.a.LlvQ8a8y1TCqO2X2_ueeKo2GtyU)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    m = R.id.tv_dispute_details;
    paramBundle = paramView.findViewById(m);
    localObject1 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$VXU7RQeFMTLnk8U0reFZ_tNb_5Q;
    ((-..Lambda.a.VXU7RQeFMTLnk8U0reFZ_tNb_5Q)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    m = R.id.img_call;
    paramBundle = paramView.findViewById(m);
    localObject1 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$XsavkPrxQx6P4hUdhEhPbT2kbnc;
    ((-..Lambda.a.XsavkPrxQx6P4hUdhEhPbT2kbnc)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    m = R.id.tvBookingCancel;
    paramBundle = paramView.findViewById(m);
    localObject1 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$tMiXlJMyMEiltABNuEizfKxI8Qg;
    ((-..Lambda.a.tMiXlJMyMEiltABNuEizfKxI8Qg)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    m = R.id.tvBookingViewDetails;
    paramView = paramView.findViewById(m);
    paramBundle = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$SeeoNoed4BPmX4rb1FqxhO4WPY0;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = (AppCompatActivity)getActivity();
    paramBundle = t;
    paramView.setSupportActionBar(paramBundle);
    paramView.getSupportActionBar().setDisplayShowTitleEnabled(bool1);
    paramView.getSupportActionBar().setDisplayHomeAsUpEnabled(bool1);
    b.a(this);
    c.a(this);
    paramView = new android/app/ProgressDialog;
    paramBundle = getActivity();
    paramView.<init>(paramBundle);
    aq = paramView;
    paramView = t;
    paramBundle = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$a$NzxTtA2hmzdGHWisGlKBIDkelaU;
    paramBundle.<init>(this);
    paramView.setNavigationOnClickListener(paramBundle);
    paramView = getArguments();
    if (paramView != null)
    {
      paramView = getArguments();
      paramBundle = "item";
      boolean bool2 = paramView.containsKey(paramBundle);
      if (bool2)
      {
        paramView = getArguments();
        paramBundle = "item";
        paramView = (h)paramView.getSerializable(paramBundle);
        as = paramView;
        f();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */