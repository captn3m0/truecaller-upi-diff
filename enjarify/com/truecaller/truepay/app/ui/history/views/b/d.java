package com.truecaller.truepay.app.ui.history.views.b;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.history.a.a.a;
import com.truecaller.truepay.app.ui.history.views.e.f;
import com.truecaller.truepay.app.ui.history.views.e.g;
import com.truecaller.utils.n;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class d
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements f
{
  public com.truecaller.truepay.app.ui.history.views.c.c a;
  public com.truecaller.truepay.app.ui.history.views.c.b b;
  public com.truecaller.truepay.app.ui.history.b.h c;
  public com.truecaller.truepay.data.e.e d;
  public n e;
  Toolbar f;
  EditText g;
  TextView h;
  TextView i;
  TextView j;
  TextView k;
  Button l;
  ConstraintLayout n;
  ConstraintLayout o;
  TextView p;
  TextView q;
  TextView r;
  TextView s;
  com.truecaller.truepay.app.ui.history.views.e.e t;
  private g u;
  private com.truecaller.truepay.app.ui.history.models.h v;
  private ProgressDialog w;
  
  public static d a(com.truecaller.truepay.app.ui.history.models.h paramh)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("item", paramh);
    paramh = new com/truecaller/truepay/app/ui/history/views/b/d;
    paramh.<init>();
    paramh.setArguments(localBundle);
    return paramh;
  }
  
  private void b()
  {
    ProgressDialog localProgressDialog = w;
    if (localProgressDialog != null)
    {
      boolean bool = isAdded();
      if (bool)
      {
        localProgressDialog = w;
        localProgressDialog.dismiss();
      }
    }
  }
  
  private void e(String paramString)
  {
    ProgressDialog localProgressDialog = w;
    if (localProgressDialog != null)
    {
      boolean bool = isAdded();
      if (bool)
      {
        localProgressDialog = w;
        localProgressDialog.setMessage(paramString);
        paramString = w;
        paramString.show();
      }
    }
  }
  
  public final int a()
  {
    return R.layout.fragment_report_issue;
  }
  
  public final void a(com.truecaller.truepay.data.e.b paramb, com.truecaller.truepay.data.e.a parama)
  {
    parama = parama.a();
    boolean bool = parama.booleanValue();
    if (bool)
    {
      parama = s;
      int i1 = 0;
      Resources localResources = null;
      parama.setVisibility(0);
      long l1 = System.currentTimeMillis();
      parama = paramb.a();
      long l2 = parama.longValue();
      l1 -= l2;
      l2 = 172800000L;
      bool = l1 < l2;
      if (bool)
      {
        parama = s;
        i1 = R.string.call_me_back;
        parama.setText(i1);
        parama = s;
        localResources = getResources();
        i2 = R.color.azure;
        i1 = localResources.getColor(i2);
        parama.setTextColor(i1);
        parama = Long.valueOf(0L);
        paramb.a(parama);
        return;
      }
      paramb = s;
      int m = R.string.call_back_requested;
      paramb.setText(m);
      paramb = s;
      parama = getResources();
      int i2 = R.color.blue_grey;
      m = parama.getColor(i2);
      paramb.setTextColor(m);
      s.setEnabled(false);
      return;
    }
    s.setVisibility(8);
  }
  
  public final void a(String paramString)
  {
    a(paramString, null);
    b();
  }
  
  public final void a(String paramString1, String paramString2)
  {
    b();
    String str = "closed";
    boolean bool = paramString1.equalsIgnoreCase(str);
    if (bool)
    {
      r.setText(paramString2);
      q.setVisibility(8);
      return;
    }
    a(paramString2, null);
  }
  
  public final void a(Throwable paramThrowable)
  {
    b();
    Object localObject = e;
    int m = R.string.server_error_message;
    Object[] arrayOfObject = new Object[0];
    localObject = ((n)localObject).a(m, arrayOfObject);
    a((String)localObject, paramThrowable);
  }
  
  public final void b(String paramString)
  {
    b();
    a(paramString, null);
  }
  
  public final void b(String paramString1, String paramString2)
  {
    b();
    c localc = c.a(paramString2);
    Object localObject = getFragmentManager();
    String str = c.class.getSimpleName();
    localc.show((j)localObject, str);
    localObject = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$d$9HTn20oJLogrfpUV9r-ELggkV3g;
    ((-..Lambda.d.9HTn20oJLogrfpUV9r-ELggkV3g)localObject).<init>(this, paramString1, paramString2);
    b = ((com.truecaller.truepay.app.ui.history.views.e.a)localObject);
  }
  
  public final void b(Throwable paramThrowable)
  {
    int m = R.string.server_error_message;
    String str = getString(m);
    a(str, paramThrowable);
    b();
  }
  
  public final void c(String paramString)
  {
    TextView localTextView = s;
    int m = R.string.call_back_requested;
    localTextView.setText(m);
    localTextView = s;
    Resources localResources = getResources();
    int i1 = R.color.blue_grey;
    m = localResources.getColor(i1);
    localTextView.setTextColor(m);
    s.setEnabled(false);
    a(paramString, null);
    b();
  }
  
  public final void c(Throwable paramThrowable)
  {
    Object localObject = e;
    int m = R.string.server_error_message;
    Object[] arrayOfObject = new Object[0];
    localObject = ((n)localObject).a(m, arrayOfObject);
    a((String)localObject, paramThrowable);
    b();
  }
  
  public final void d(String paramString)
  {
    a(paramString, null);
    b();
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof g;
    if (bool)
    {
      paramContext = (g)getActivity();
      u = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement TransactionHistoryView");
    throw paramContext;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.history.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
    c.a(this);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    u = null;
  }
  
  public final void onResume()
  {
    super.onResume();
    u.a(false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.toolbar_report_issue;
    Object localObject1 = (Toolbar)paramView.findViewById(m);
    f = ((Toolbar)localObject1);
    m = R.id.et_report_issue;
    localObject1 = (EditText)paramView.findViewById(m);
    g = ((EditText)localObject1);
    m = R.id.tv_benefice_name;
    localObject1 = (TextView)paramView.findViewById(m);
    h = ((TextView)localObject1);
    m = R.id.tv_payment_status;
    localObject1 = (TextView)paramView.findViewById(m);
    i = ((TextView)localObject1);
    m = R.id.tv_report_issue_ref_num;
    localObject1 = (TextView)paramView.findViewById(m);
    j = ((TextView)localObject1);
    m = R.id.tv_payment_amount;
    localObject1 = (TextView)paramView.findViewById(m);
    k = ((TextView)localObject1);
    m = R.id.btn_report_issue;
    localObject1 = (Button)paramView.findViewById(m);
    l = ((Button)localObject1);
    m = R.id.cl_report_issue;
    localObject1 = (ConstraintLayout)paramView.findViewById(m);
    n = ((ConstraintLayout)localObject1);
    m = R.id.cl_report_issue_details;
    localObject1 = (ConstraintLayout)paramView.findViewById(m);
    o = ((ConstraintLayout)localObject1);
    m = R.id.tv_report_issue_message;
    localObject1 = (TextView)paramView.findViewById(m);
    p = ((TextView)localObject1);
    m = R.id.tv_check_status;
    localObject1 = (TextView)paramView.findViewById(m);
    q = ((TextView)localObject1);
    m = R.id.tv_report_issue_info_three;
    localObject1 = (TextView)paramView.findViewById(m);
    r = ((TextView)localObject1);
    m = R.id.tv_payment_call_me_back;
    localObject1 = (TextView)paramView.findViewById(m);
    s = ((TextView)localObject1);
    localObject1 = l;
    Object localObject2 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$d$F8ViChxtIJY_bcadnvb08vFegq8;
    ((-..Lambda.d.F8ViChxtIJY_bcadnvb08vFegq8)localObject2).<init>(this);
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = q;
    localObject2 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$d$k9RdYRCT7tj2eqS3DgvlsV_FaqU;
    ((-..Lambda.d.k9RdYRCT7tj2eqS3DgvlsV_FaqU)localObject2).<init>(this);
    ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = s;
    localObject2 = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$d$aVMs_IDM3qbNf61N4t_u1NsSWIc;
    ((-..Lambda.d.aVMs_IDM3qbNf61N4t_u1NsSWIc)localObject2).<init>(this);
    ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = (AppCompatActivity)getActivity();
    localObject2 = f;
    ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
    localObject2 = ((AppCompatActivity)localObject1).getSupportActionBar();
    boolean bool1 = true;
    ((ActionBar)localObject2).setDisplayShowTitleEnabled(bool1);
    localObject1 = ((AppCompatActivity)localObject1).getSupportActionBar();
    ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(bool1);
    super.onViewCreated(paramView, paramBundle);
    c.a(this);
    paramView = new android/app/ProgressDialog;
    paramBundle = getActivity();
    paramView.<init>(paramBundle);
    w = paramView;
    paramView = getArguments();
    if (paramView != null)
    {
      paramView = getArguments();
      paramBundle = "item";
      boolean bool2 = paramView.containsKey(paramBundle);
      if (bool2)
      {
        paramView = (com.truecaller.truepay.app.ui.history.models.h)getArguments().getSerializable("item");
        v = paramView;
        paramView = f;
        paramBundle = new com/truecaller/truepay/app/ui/history/views/b/-$$Lambda$d$Fg5WumQkcD2RTiZfxQUN3sqYM9w;
        paramBundle.<init>(this);
        paramView.setNavigationOnClickListener(paramBundle);
        paramView = v;
        if (paramView != null)
        {
          paramView = k;
          int i1 = R.string.rs_amount;
          paramBundle = getString(i1);
          localObject1 = new Object[bool1];
          localObject2 = v.l;
          localObject1[0] = localObject2;
          paramBundle = String.format(paramBundle, (Object[])localObject1);
          paramView.setText(paramBundle);
          paramView = i;
          paramBundle = a;
          localObject1 = v.e;
          paramBundle = paramBundle.a((String)localObject1);
          paramView.setText(paramBundle);
          paramView = h;
          paramBundle = a;
          localObject1 = v;
          localObject2 = b;
          paramBundle = paramBundle.a((com.truecaller.truepay.app.ui.history.models.h)localObject1, (com.truecaller.truepay.app.ui.history.views.c.b)localObject2);
          paramView.setText(paramBundle);
          paramView = j;
          i1 = R.string.upi_ref_number;
          paramBundle = getString(i1);
          localObject1 = new Object[bool1];
          localObject2 = v.f;
          localObject1[0] = localObject2;
          paramBundle = String.format(paramBundle, (Object[])localObject1);
          paramView.setText(paramBundle);
          paramView = p;
          paramBundle = v.t;
          paramView.setText(paramBundle);
          paramView = v.r;
          bool2 = TextUtils.isEmpty(paramView);
          i1 = 8;
          if (bool2)
          {
            o.setVisibility(i1);
            n.setVisibility(0);
            paramView = f;
            i1 = R.string.report_an_issue;
            paramView.setTitle(i1);
            return;
          }
          o.setVisibility(0);
          n.setVisibility(i1);
          paramView = f;
          m = R.string.issue_reported;
          paramView.setTitle(m);
          paramView = f;
          localObject1 = getActivity();
          int i2 = R.style.ToolbarSubtitleAppearance;
          paramView.setSubtitleTextAppearance((Context)localObject1, i2);
          paramView = f;
          localObject1 = getActivity();
          i2 = R.style.ToolbarTitleAppearance;
          paramView.setTitleTextAppearance((Context)localObject1, i2);
          paramView = f;
          m = R.string.reference_number;
          localObject1 = getString(m);
          localObject2 = new Object[bool1];
          Object localObject3 = v.s;
          localObject2[0] = localObject3;
          localObject1 = String.format((String)localObject1, (Object[])localObject2);
          paramView.setSubtitle((CharSequence)localObject1);
          paramView = v;
          localObject1 = c;
          if (localObject1 != null)
          {
            localObject1 = new java/text/SimpleDateFormat;
            localObject2 = "yyyy-MM-dd HH:mm:ss";
            localObject3 = Locale.US;
            ((SimpleDateFormat)localObject1).<init>((String)localObject2, (Locale)localObject3);
            try
            {
              paramView = c;
              paramView = ((SimpleDateFormat)localObject1).parse(paramView);
              localObject1 = new java/util/Date;
              ((Date)localObject1).<init>();
              bool2 = ((Date)localObject1).after(paramView);
              if (!bool2)
              {
                paramView = q;
                paramView.setVisibility(i1);
              }
              else
              {
                paramView = q;
                paramView.setVisibility(0);
              }
            }
            catch (ParseException localParseException) {}
          }
          paramView = c;
          paramBundle = d;
          if (paramBundle != null)
          {
            paramBundle = (f)d;
            localObject1 = f;
            paramView = g;
            paramBundle.a((com.truecaller.truepay.data.e.b)localObject1, paramView);
          }
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */