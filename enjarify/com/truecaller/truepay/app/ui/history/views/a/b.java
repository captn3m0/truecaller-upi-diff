package com.truecaller.truepay.app.ui.history.views.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;

public final class b
  extends c
{
  public b(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    com.truecaller.truepay.app.ui.history.views.d.b localb = new com/truecaller/truepay/app/ui/history/views/d/b;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.list_item_history;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localb.<init>(paramViewGroup, (f)localObject);
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */