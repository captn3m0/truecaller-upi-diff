package com.truecaller.truepay.app.ui.history.views.c;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.v4.f.g;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

public final class a
{
  private static final String[] b = { "photo_id" };
  private static final String[] c = { "data15" };
  final g a;
  private final Context d;
  private Bitmap e;
  
  public a(Context paramContext)
  {
    d = paramContext;
    int i = (int)(Runtime.getRuntime().maxMemory() / 1024L) / 8;
    a.1 local1 = new com/truecaller/truepay/app/ui/history/views/c/a$1;
    local1.<init>(this, i);
    a = local1;
  }
  
  private static a.b b(ImageView paramImageView)
  {
    if (paramImageView != null)
    {
      paramImageView = paramImageView.getDrawable();
      boolean bool = paramImageView instanceof a.a;
      if (bool) {
        return (a.b)a.get();
      }
    }
    return null;
  }
  
  private Integer b(String paramString)
  {
    ContentResolver localContentResolver = d.getContentResolver();
    Object localObject = ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI;
    paramString = Uri.encode(paramString);
    Uri localUri = Uri.withAppendedPath((Uri)localObject, paramString);
    String[] arrayOfString = b;
    String str = "display_name ASC";
    paramString = localContentResolver.query(localUri, arrayOfString, null, null, str);
    int i = 0;
    localObject = null;
    if (paramString != null) {
      try
      {
        boolean bool = paramString.moveToFirst();
        if (bool)
        {
          localObject = "photo_id";
          i = paramString.getColumnIndex((String)localObject);
          i = paramString.getInt(i);
          localObject = Integer.valueOf(i);
        }
      }
      finally
      {
        if (paramString != null) {
          paramString.close();
        }
      }
    }
    if (paramString != null) {
      paramString.close();
    }
    return localInteger;
  }
  
  /* Error */
  final Bitmap a(int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 29	com/truecaller/truepay/app/ui/history/views/c/a:d	Landroid/content/Context;
    //   4: invokevirtual 81	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   7: astore_2
    //   8: getstatic 133	android/provider/ContactsContract$Data:CONTENT_URI	Landroid/net/Uri;
    //   11: astore_3
    //   12: iload_1
    //   13: i2l
    //   14: lstore 4
    //   16: aload_3
    //   17: lload 4
    //   19: invokestatic 139	android/content/ContentUris:withAppendedId	(Landroid/net/Uri;J)Landroid/net/Uri;
    //   22: astore 6
    //   24: iconst_0
    //   25: istore_1
    //   26: aconst_null
    //   27: astore 7
    //   29: getstatic 23	com/truecaller/truepay/app/ui/history/views/c/a:c	[Ljava/lang/String;
    //   32: astore 8
    //   34: aload_2
    //   35: aload 6
    //   37: aload 8
    //   39: aconst_null
    //   40: aconst_null
    //   41: aconst_null
    //   42: invokevirtual 105	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   45: astore_3
    //   46: aload_3
    //   47: invokeinterface 111 1 0
    //   52: istore 9
    //   54: iload 9
    //   56: ifeq +37 -> 93
    //   59: iconst_0
    //   60: istore 9
    //   62: aconst_null
    //   63: astore_2
    //   64: aload_3
    //   65: iconst_0
    //   66: invokeinterface 143 2 0
    //   71: astore 6
    //   73: aload 6
    //   75: ifnull +18 -> 93
    //   78: aload 6
    //   80: arraylength
    //   81: istore 10
    //   83: aload 6
    //   85: iconst_0
    //   86: iload 10
    //   88: invokestatic 149	android/graphics/BitmapFactory:decodeByteArray	([BII)Landroid/graphics/Bitmap;
    //   91: astore 7
    //   93: aload_3
    //   94: ifnull +9 -> 103
    //   97: aload_3
    //   98: invokeinterface 128 1 0
    //   103: aload 7
    //   105: areturn
    //   106: astore 7
    //   108: goto +11 -> 119
    //   111: astore 11
    //   113: aconst_null
    //   114: astore_3
    //   115: aload 11
    //   117: astore 7
    //   119: aload_3
    //   120: ifnull +9 -> 129
    //   123: aload_3
    //   124: invokeinterface 128 1 0
    //   129: aload 7
    //   131: athrow
    //   132: pop
    //   133: aconst_null
    //   134: astore_3
    //   135: aload_3
    //   136: ifnull +9 -> 145
    //   139: aload_3
    //   140: invokeinterface 128 1 0
    //   145: aconst_null
    //   146: areturn
    //   147: pop
    //   148: goto -13 -> 135
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	151	0	this	a
    //   0	151	1	paramInt	int
    //   7	57	2	localContentResolver	ContentResolver
    //   11	129	3	localObject1	Object
    //   14	4	4	l	long
    //   22	62	6	localObject2	Object
    //   27	77	7	localBitmap	Bitmap
    //   106	1	7	localObject3	Object
    //   117	13	7	localObject4	Object
    //   32	6	8	arrayOfString	String[]
    //   52	9	9	bool	boolean
    //   81	6	10	i	int
    //   111	5	11	localObject5	Object
    //   132	1	13	localIllegalArgumentException1	IllegalArgumentException
    //   147	1	14	localIllegalArgumentException2	IllegalArgumentException
    // Exception table:
    //   from	to	target	type
    //   46	52	106	finally
    //   65	71	106	finally
    //   78	81	106	finally
    //   86	91	106	finally
    //   29	32	111	finally
    //   41	45	111	finally
    //   29	32	132	java/lang/IllegalArgumentException
    //   41	45	132	java/lang/IllegalArgumentException
    //   46	52	147	java/lang/IllegalArgumentException
    //   65	71	147	java/lang/IllegalArgumentException
    //   78	81	147	java/lang/IllegalArgumentException
    //   86	91	147	java/lang/IllegalArgumentException
  }
  
  public final Bitmap a(String paramString)
  {
    return (Bitmap)a.get(paramString);
  }
  
  public final void a(String paramString, ImageView paramImageView)
  {
    Object localObject1 = b(paramImageView);
    boolean bool1 = true;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = a.b.a((a.b)localObject1);
      if (localObject2 != null)
      {
        int i = ((String)localObject2).length();
        if (i != 0)
        {
          boolean bool2 = ((String)localObject2).equals(paramString);
          if (bool2) {}
        }
        else
        {
          ((a.b)localObject1).cancel(bool1);
          break label66;
        }
      }
      j = 0;
      localObject1 = null;
      break label69;
    }
    label66:
    int j = 1;
    label69:
    if (j != 0)
    {
      if (paramString != null)
      {
        localObject1 = a(paramString);
        if (localObject1 != null)
        {
          paramImageView.setImageBitmap((Bitmap)localObject1);
          return;
        }
        localObject1 = new com/truecaller/truepay/app/ui/history/views/c/a$b;
        ((a.b)localObject1).<init>(this, paramImageView);
        localObject2 = new com/truecaller/truepay/app/ui/history/views/c/a$a;
        localResources = d.getResources();
        localBitmap = e;
        ((a.a)localObject2).<init>(localResources, localBitmap, (a.b)localObject1);
        paramImageView.setImageDrawable((Drawable)localObject2);
        paramImageView = new String[bool1];
        paramImageView[0] = paramString;
        ((a.b)localObject1).execute(paramImageView);
        return;
      }
      localObject1 = new com/truecaller/truepay/app/ui/history/views/c/a$b;
      ((a.b)localObject1).<init>(this, paramImageView);
      localObject2 = new com/truecaller/truepay/app/ui/history/views/c/a$a;
      Resources localResources = d.getResources();
      Bitmap localBitmap = e;
      ((a.a)localObject2).<init>(localResources, localBitmap, (a.b)localObject1);
      paramImageView.setImageDrawable((Drawable)localObject2);
      paramImageView = new String[bool1];
      paramImageView[0] = paramString;
      ((a.b)localObject1).execute(paramImageView);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */