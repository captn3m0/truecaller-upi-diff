package com.truecaller.truepay.app.ui.history.views.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

final class TruePayWebViewActivity$1
  extends WebViewClient
{
  TruePayWebViewActivity$1(TruePayWebViewActivity paramTruePayWebViewActivity) {}
  
  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    super.onPageFinished(paramWebView, paramString);
    a.b.setVisibility(8);
  }
  
  public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    super.onPageStarted(paramWebView, paramString, paramBitmap);
    a.b.setVisibility(0);
  }
  
  public final void onReceivedError(WebView paramWebView, WebResourceRequest paramWebResourceRequest, WebResourceError paramWebResourceError)
  {
    super.onReceivedError(paramWebView, paramWebResourceRequest, paramWebResourceError);
    a.b.setVisibility(8);
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    paramWebView = "/close-webview";
    boolean bool = paramString.contains(paramWebView);
    if (bool)
    {
      paramWebView = a;
      paramWebView.finish();
    }
    else
    {
      paramWebView = "mailto:";
      bool = paramString.startsWith(paramWebView);
      if (bool)
      {
        paramWebView = new android/content/Intent;
        paramString = Uri.parse(paramString);
        paramWebView.<init>("android.intent.action.SENDTO", paramString);
        a.startActivity(paramWebView);
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.activities.TruePayWebViewActivity.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */