package com.truecaller.truepay.app.ui.history.views.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.history.models.o;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.au;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class d
{
  private final Context a;
  private h b;
  private p c;
  private String d;
  private au e;
  
  public d(Context paramContext, h paramh, String paramString, au paramau)
  {
    a = paramContext;
    b = paramh;
    d = paramString;
    e = paramau;
  }
  
  public d(Context paramContext, p paramp, String paramString, au paramau)
  {
    a = paramContext;
    c = paramp;
    d = paramString;
    e = paramau;
  }
  
  private Bitmap a(int paramInt)
  {
    RelativeLayout localRelativeLayout = new android/widget/RelativeLayout;
    Object localObject1 = a;
    localRelativeLayout.<init>((Context)localObject1);
    localObject1 = (LayoutInflater)a.getSystemService("layout_inflater");
    int i = R.layout.fragment_share_receipt;
    boolean bool3 = true;
    localObject1 = ((LayoutInflater)localObject1).inflate(i, localRelativeLayout, bool3);
    i = R.id.tv_amount_receipt;
    Object localObject2 = (TextView)((View)localObject1).findViewById(i);
    int k = R.id.tv_to_name_receipt;
    Object localObject3 = (TextView)((View)localObject1).findViewById(k);
    int m = R.id.tv_from_name_sender;
    TextView localTextView1 = (TextView)((View)localObject1).findViewById(m);
    int n = R.id.tv_acc_number_receipt;
    Object localObject4 = (TextView)((View)localObject1).findViewById(n);
    int i1 = R.id.tv_transaction_id_receipt;
    TextView localTextView2 = (TextView)((View)localObject1).findViewById(i1);
    int i2 = R.id.tv_time_stamp_receipt;
    localObject1 = (TextView)((View)localObject1).findViewById(i2);
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    Object localObject5 = Locale.US;
    localSimpleDateFormat.<init>("yyyy-MM-dd HH:mm:ss", (Locale)localObject5);
    Object localObject6 = "";
    localObject5 = b;
    int i3;
    Object localObject7;
    String str;
    if (localObject5 != null)
    {
      localObject6 = a;
      i3 = R.string.rs_amount;
      localObject7 = new Object[bool3];
      str = b.l;
      localObject7[0] = str;
      localObject7 = ((Context)localObject6).getString(i3, (Object[])localObject7);
      ((TextView)localObject2).setText((CharSequence)localObject7);
      localObject2 = "utility";
      localObject7 = b.i;
      boolean bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject7);
      if (bool1)
      {
        localObject2 = b.w;
        if (localObject2 != null)
        {
          localObject2 = au.b(b.w.h);
          ((TextView)localObject3).setText((CharSequence)localObject2);
          localObject2 = a(b.w.b);
          ((TextView)localObject4).setText((CharSequence)localObject2);
          break label395;
        }
      }
      localObject2 = au.b(b.o.c);
      ((TextView)localObject3).setText((CharSequence)localObject2);
      localObject2 = b.o.b;
      bool1 = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool1)
      {
        localObject2 = a(b.o.b);
        ((TextView)localObject4).setText((CharSequence)localObject2);
      }
      else
      {
        int j = 8;
        ((TextView)localObject4).setVisibility(j);
      }
      label395:
      localObject2 = b.f;
      localTextView2.setText((CharSequence)localObject2);
      localObject2 = b;
      localObject6 = b;
    }
    else
    {
      localObject5 = c;
      if (localObject5 != null)
      {
        localObject6 = a;
        i3 = R.string.rs_amount;
        localObject7 = new Object[bool3];
        str = c.e;
        localObject7[0] = str;
        localObject7 = ((Context)localObject6).getString(i3, (Object[])localObject7);
        ((TextView)localObject2).setText((CharSequence)localObject7);
        localObject2 = "utility";
        localObject7 = c.d;
        boolean bool2 = ((String)localObject2).equalsIgnoreCase((String)localObject7);
        if (bool2)
        {
          localObject2 = au.b(c.t);
          ((TextView)localObject3).setText((CharSequence)localObject2);
          localObject2 = c.u;
          ((TextView)localObject4).setText((CharSequence)localObject2);
        }
        else
        {
          localObject2 = au.b(c.h.h);
          ((TextView)localObject3).setText((CharSequence)localObject2);
          localObject2 = a(c.h.f);
          ((TextView)localObject4).setText((CharSequence)localObject2);
        }
        localObject2 = c.j.a;
        localTextView2.setText((CharSequence)localObject2);
        localObject2 = c;
        localObject6 = l;
      }
    }
    try
    {
      localObject2 = localSimpleDateFormat.parse((String)localObject6);
      localObject7 = new java/text/DateFormatSymbols;
      localObject3 = Locale.getDefault();
      ((DateFormatSymbols)localObject7).<init>((Locale)localObject3);
      localObject3 = "AM";
      localObject4 = "PM";
      localObject3 = new String[] { localObject3, localObject4 };
      ((DateFormatSymbols)localObject7).setAmPmStrings((String[])localObject3);
      localSimpleDateFormat.setDateFormatSymbols((DateFormatSymbols)localObject7);
      localObject7 = "d MMM yyyy | h:mm a";
      localObject2 = DateFormat.format((CharSequence)localObject7, (Date)localObject2);
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    catch (ParseException localParseException)
    {
      ((TextView)localObject1).setText((CharSequence)localObject6);
    }
    localObject1 = au.b(d);
    localTextView1.setText((CharSequence)localObject1);
    localObject1 = new android/widget/RelativeLayout$LayoutParams;
    ((RelativeLayout.LayoutParams)localObject1).<init>(-1, -2);
    localRelativeLayout.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    paramInt = View.MeasureSpec.makeMeasureSpec(paramInt, -1 << -1);
    int i4 = View.MeasureSpec.makeMeasureSpec(0, 0);
    localRelativeLayout.measure(paramInt, i4);
    paramInt = localRelativeLayout.getMeasuredWidth();
    i4 = localRelativeLayout.getMeasuredHeight();
    localRelativeLayout.layout(0, 0, paramInt, i4);
    paramInt = localRelativeLayout.getMeasuredWidth();
    i4 = localRelativeLayout.getMeasuredHeight();
    localObject2 = Bitmap.Config.ARGB_8888;
    Bitmap localBitmap = Bitmap.createBitmap(paramInt, i4, (Bitmap.Config)localObject2);
    localObject1 = new android/graphics/Canvas;
    ((Canvas)localObject1).<init>(localBitmap);
    localRelativeLayout.draw((Canvas)localObject1);
    return localBitmap;
  }
  
  private static String a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      Object localObject = "null";
      bool = ((String)localObject).equals(paramString);
      if (!bool)
      {
        localObject = "^[A-Za-z0-9-\\+]+(\\.[A-Za-z0-9-]+)*@[A-Za-z0-9-\\+]+(\\.[A-Za-z0-9]+)*$";
        bool = paramString.matches((String)localObject);
        if (bool) {
          return paramString;
        }
        int i = paramString.length();
        int j = 0;
        String str = null;
        int k = 10;
        int m = 2;
        if (i == k)
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>("+91 ");
          str = paramString.substring(0, m);
          ((StringBuilder)localObject).append(str);
          ((StringBuilder)localObject).append("xxxxxx");
          j = paramString.length() - m;
          k = paramString.length();
          paramString = paramString.substring(j, k);
          ((StringBuilder)localObject).append(paramString);
          return ((StringBuilder)localObject).toString();
        }
        i = paramString.length();
        if (i > k)
        {
          i = paramString.length() - k;
          paramString = paramString.substring(i);
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>("+91 ");
          str = paramString.substring(0, m);
          ((StringBuilder)localObject).append(str);
          ((StringBuilder)localObject).append("xxxxxx");
          j = paramString.length() - m;
          k = paramString.length();
          paramString = paramString.substring(j, k);
          ((StringBuilder)localObject).append(paramString);
          return ((StringBuilder)localObject).toString();
        }
        i = paramString.length();
        if (i > m)
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>("xxxxxx");
          j = paramString.length() - m;
          k = paramString.length();
          paramString = paramString.substring(j, k);
          ((StringBuilder)localObject).append(paramString);
          return ((StringBuilder)localObject).toString();
        }
        paramString = String.valueOf(paramString);
        return "+91 ".concat(paramString);
      }
    }
    return "";
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: invokestatic 333	android/content/res/Resources:getSystem	()Landroid/content/res/Resources;
    //   3: invokevirtual 337	android/content/res/Resources:getDisplayMetrics	()Landroid/util/DisplayMetrics;
    //   6: getfield 342	android/util/DisplayMetrics:widthPixels	I
    //   9: istore_1
    //   10: aload_0
    //   11: iload_1
    //   12: invokespecial 345	com/truecaller/truepay/app/ui/history/views/c/d:a	(I)Landroid/graphics/Bitmap;
    //   15: astore_2
    //   16: new 347	android/content/Intent
    //   19: astore_3
    //   20: aload_3
    //   21: ldc_w 349
    //   24: invokespecial 350	android/content/Intent:<init>	(Ljava/lang/String;)V
    //   27: aload_3
    //   28: ldc_w 352
    //   31: invokevirtual 356	android/content/Intent:setType	(Ljava/lang/String;)Landroid/content/Intent;
    //   34: pop
    //   35: new 358	android/content/ContentValues
    //   38: astore 4
    //   40: aload 4
    //   42: invokespecial 359	android/content/ContentValues:<init>	()V
    //   45: aload 4
    //   47: ldc_w 361
    //   50: ldc_w 361
    //   53: invokevirtual 365	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   56: aload 4
    //   58: ldc_w 367
    //   61: ldc_w 352
    //   64: invokevirtual 365	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   67: aload_0
    //   68: getfield 20	com/truecaller/truepay/app/ui/history/views/c/d:a	Landroid/content/Context;
    //   71: invokevirtual 371	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   74: astore 5
    //   76: getstatic 377	android/provider/MediaStore$Images$Media:EXTERNAL_CONTENT_URI	Landroid/net/Uri;
    //   79: astore 6
    //   81: aload 5
    //   83: aload 6
    //   85: aload 4
    //   87: invokevirtual 383	android/content/ContentResolver:insert	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   90: astore 4
    //   92: aconst_null
    //   93: astore 5
    //   95: aload_0
    //   96: getfield 20	com/truecaller/truepay/app/ui/history/views/c/d:a	Landroid/content/Context;
    //   99: astore 6
    //   101: aload 6
    //   103: invokevirtual 371	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   106: astore 6
    //   108: aload 6
    //   110: aload 4
    //   112: invokevirtual 387	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
    //   115: astore 5
    //   117: getstatic 393	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   120: astore 6
    //   122: bipush 100
    //   124: istore 7
    //   126: aload_2
    //   127: aload 6
    //   129: iload 7
    //   131: aload 5
    //   133: invokevirtual 398	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   136: pop
    //   137: ldc_w 400
    //   140: astore_2
    //   141: aload_3
    //   142: aload_2
    //   143: aload 4
    //   145: invokevirtual 404	android/content/Intent:putExtra	(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    //   148: pop
    //   149: iconst_1
    //   150: istore_1
    //   151: aload_3
    //   152: iload_1
    //   153: invokevirtual 408	android/content/Intent:addFlags	(I)Landroid/content/Intent;
    //   156: pop
    //   157: aload_0
    //   158: getfield 20	com/truecaller/truepay/app/ui/history/views/c/d:a	Landroid/content/Context;
    //   161: astore_2
    //   162: aload_0
    //   163: getfield 20	com/truecaller/truepay/app/ui/history/views/c/d:a	Landroid/content/Context;
    //   166: astore 4
    //   168: getstatic 411	com/truecaller/truepay/R$string:share_image	I
    //   171: istore 8
    //   173: aload 4
    //   175: iload 8
    //   177: invokevirtual 413	android/content/Context:getString	(I)Ljava/lang/String;
    //   180: astore 4
    //   182: aload_3
    //   183: aload 4
    //   185: invokestatic 417	android/content/Intent:createChooser	(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;
    //   188: astore_3
    //   189: aload_2
    //   190: aload_3
    //   191: invokevirtual 421	android/content/Context:startActivity	(Landroid/content/Intent;)V
    //   194: aload 5
    //   196: invokestatic 426	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   199: return
    //   200: astore_2
    //   201: aload 5
    //   203: invokestatic 426	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   206: aload_2
    //   207: athrow
    //   208: pop
    //   209: aload 5
    //   211: invokestatic 426	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   214: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	215	0	this	d
    //   9	144	1	i	int
    //   15	175	2	localObject1	Object
    //   200	7	2	localObject2	Object
    //   19	172	3	localIntent	android.content.Intent
    //   38	146	4	localObject3	Object
    //   74	136	5	localObject4	Object
    //   79	49	6	localObject5	Object
    //   124	6	7	j	int
    //   171	5	8	k	int
    //   208	1	10	localFileNotFoundException	java.io.FileNotFoundException
    // Exception table:
    //   from	to	target	type
    //   95	99	200	finally
    //   101	106	200	finally
    //   110	115	200	finally
    //   117	120	200	finally
    //   131	137	200	finally
    //   143	149	200	finally
    //   152	157	200	finally
    //   157	161	200	finally
    //   162	166	200	finally
    //   168	171	200	finally
    //   175	180	200	finally
    //   183	188	200	finally
    //   190	194	200	finally
    //   95	99	208	java/io/FileNotFoundException
    //   101	106	208	java/io/FileNotFoundException
    //   110	115	208	java/io/FileNotFoundException
    //   117	120	208	java/io/FileNotFoundException
    //   131	137	208	java/io/FileNotFoundException
    //   143	149	208	java/io/FileNotFoundException
    //   152	157	208	java/io/FileNotFoundException
    //   157	161	208	java/io/FileNotFoundException
    //   162	166	208	java/io/FileNotFoundException
    //   168	171	208	java/io/FileNotFoundException
    //   175	180	208	java/io/FileNotFoundException
    //   183	188	208	java/io/FileNotFoundException
    //   190	194	208	java/io/FileNotFoundException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */