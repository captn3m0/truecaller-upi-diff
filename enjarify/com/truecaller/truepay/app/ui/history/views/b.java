package com.truecaller.truepay.app.ui.history.views;

import android.view.View;
import android.widget.TextView;
import com.truecaller.truepay.R.id;

public final class b
{
  public final TextView a;
  public final TextView b;
  public final TextView c;
  public final TextView d;
  public final TextView e;
  public final TextView f;
  public final TextView g;
  public final TextView h;
  public final TextView i;
  public final TextView j;
  public final View k;
  
  public b(View paramView)
  {
    k = paramView;
    paramView = k;
    int m = R.id.booking_date;
    paramView = (TextView)paramView.findViewById(m);
    a = paramView;
    paramView = k;
    m = R.id.ticket_pnr;
    paramView = (TextView)paramView.findViewById(m);
    b = paramView;
    paramView = k;
    m = R.id.from_place;
    paramView = (TextView)paramView.findViewById(m);
    c = paramView;
    paramView = k;
    m = R.id.to_place;
    paramView = (TextView)paramView.findViewById(m);
    d = paramView;
    paramView = k;
    m = R.id.to_place_code;
    paramView = (TextView)paramView.findViewById(m);
    e = paramView;
    paramView = k;
    m = R.id.from_place_code;
    paramView = (TextView)paramView.findViewById(m);
    f = paramView;
    paramView = k;
    m = R.id.flight_name_header;
    paramView = (TextView)paramView.findViewById(m);
    g = paramView;
    paramView = k;
    m = R.id.depart_time;
    paramView = (TextView)paramView.findViewById(m);
    h = paramView;
    paramView = k;
    m = R.id.flight_number;
    paramView = (TextView)paramView.findViewById(m);
    i = paramView;
    paramView = k;
    m = R.id.arrival_time;
    paramView = (TextView)paramView.findViewById(m);
    j = paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.views.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */