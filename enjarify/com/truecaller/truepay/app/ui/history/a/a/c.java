package com.truecaller.truepay.app.ui.history.a.a;

import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d
{
  private final a a;
  private final Provider b;
  
  private c(a parama, Provider paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static c a(a parama, Provider paramProvider)
  {
    c localc = new com/truecaller/truepay/app/ui/history/a/a/c;
    localc.<init>(parama, paramProvider);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */