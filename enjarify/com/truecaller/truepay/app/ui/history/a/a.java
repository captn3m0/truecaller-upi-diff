package com.truecaller.truepay.app.ui.history.a;

import com.truecaller.truepay.app.ui.history.views.activities.TransactionHistoryActivity;
import com.truecaller.truepay.app.ui.history.views.e.b.a;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.app.utils.u;
import com.truecaller.truepay.data.f.aa;
import com.truecaller.truepay.data.f.ab;
import com.truecaller.truepay.data.f.o;
import com.truecaller.truepay.data.f.p;
import com.truecaller.utils.n;
import javax.inject.Provider;

public final class a
  implements b
{
  private final com.truecaller.truepay.app.a.a.a a;
  private final com.truecaller.truepay.data.a.g b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  
  private a(com.truecaller.truepay.app.ui.history.a.a.a parama, com.truecaller.truepay.data.a.g paramg, com.truecaller.truepay.app.a.a.a parama1)
  {
    a = parama1;
    b = paramg;
    paramg = new com/truecaller/truepay/app/ui/history/a/a$d;
    paramg.<init>(parama1);
    c = paramg;
    paramg = c;
    paramg = dagger.a.c.a(com.truecaller.truepay.app.ui.history.a.a.d.a(parama, paramg));
    d = paramg;
    paramg = c;
    paramg = dagger.a.c.a(com.truecaller.truepay.app.ui.history.a.a.c.a(parama, paramg));
    e = paramg;
    paramg = c;
    parama = dagger.a.c.a(com.truecaller.truepay.app.ui.history.a.a.b.a(parama, paramg));
    f = parama;
    parama = new com/truecaller/truepay/app/ui/history/a/a$c;
    parama.<init>(parama1);
    g = parama;
    parama = new com/truecaller/truepay/app/ui/history/a/a$g;
    parama.<init>(parama1);
    h = parama;
    parama = new com/truecaller/truepay/app/ui/history/a/a$h;
    parama.<init>(parama1);
    i = parama;
    parama = new com/truecaller/truepay/app/ui/history/a/a$e;
    parama.<init>(parama1);
    j = parama;
    parama = new com/truecaller/truepay/app/ui/history/a/a$f;
    parama.<init>(parama1);
    k = parama;
    parama = new com/truecaller/truepay/app/ui/history/a/a$b;
    parama.<init>(parama1);
    l = parama;
    Provider localProvider1 = g;
    Provider localProvider2 = h;
    Provider localProvider3 = i;
    Provider localProvider4 = j;
    Provider localProvider5 = k;
    Provider localProvider6 = l;
    parama = com.truecaller.truepay.app.ui.history.b.f.a(localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6);
    m = parama;
    parama = dagger.a.c.a(m);
    n = parama;
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/truepay/app/ui/history/a/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  private aa b()
  {
    aa localaa = new com/truecaller/truepay/data/f/aa;
    ab localab = com.truecaller.truepay.data.a.i.a((com.truecaller.truepay.data.api.g)dagger.a.g.a(a.p(), "Cannot return null from a non-@Nullable component method"));
    localaa.<init>(localab);
    return localaa;
  }
  
  public final void a(TransactionHistoryActivity paramTransactionHistoryActivity)
  {
    Object localObject = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    featuresRegistry = ((com.truecaller.featuretoggles.e)localObject);
    localObject = (u)dagger.a.g.a(a.x(), "Cannot return null from a non-@Nullable component method");
    instantRewardHandler = ((u)localObject);
    localObject = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    d = ((com.truecaller.featuretoggles.e)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.history.views.b.a parama)
  {
    Object localObject1 = (n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    a = ((n)localObject1);
    localObject1 = com.truecaller.truepay.app.ui.history.b.c.a();
    Object localObject2 = new com/truecaller/truepay/a/a/d/d;
    o localo = new com/truecaller/truepay/data/f/o;
    p localp = com.truecaller.truepay.data.a.h.a((com.truecaller.truepay.data.api.d)dagger.a.g.a(a.o(), "Cannot return null from a non-@Nullable component method"));
    localo.<init>(localp);
    ((com.truecaller.truepay.a.a.d.d)localObject2).<init>(localo);
    a = ((com.truecaller.truepay.a.a.d.d)localObject2);
    localObject2 = (com.truecaller.truepay.a.a.c.e)dagger.a.g.a(a.ag(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.truepay.a.a.c.e)localObject2);
    localObject2 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.K(), "Cannot return null from a non-@Nullable component method");
    c = ((com.truecaller.truepay.data.e.e)localObject2);
    localObject2 = (n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    f = ((n)localObject2);
    localObject2 = (com.truecaller.truepay.app.utils.g)dagger.a.g.a(a.c(), "Cannot return null from a non-@Nullable component method");
    g = ((com.truecaller.truepay.app.utils.g)localObject2);
    localObject2 = (com.google.gson.f)dagger.a.g.a(a.am(), "Cannot return null from a non-@Nullable component method");
    h = ((com.google.gson.f)localObject2);
    b = ((com.truecaller.truepay.app.ui.history.b.b)localObject1);
    localObject1 = (b.a)n.get();
    c = ((b.a)localObject1);
    localObject1 = (com.truecaller.truepay.app.ui.history.views.c.c)d.get();
    d = ((com.truecaller.truepay.app.ui.history.views.c.c)localObject1);
    localObject1 = (com.truecaller.truepay.app.ui.history.views.c.b)e.get();
    e = ((com.truecaller.truepay.app.ui.history.views.c.b)localObject1);
    localObject1 = (com.truecaller.truepay.app.ui.history.views.c.a)f.get();
    f = ((com.truecaller.truepay.app.ui.history.views.c.a)localObject1);
    localObject1 = (au)dagger.a.g.a(a.k(), "Cannot return null from a non-@Nullable component method");
    g = ((au)localObject1);
    localObject1 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.Q(), "Cannot return null from a non-@Nullable component method");
    h = ((com.truecaller.truepay.data.e.e)localObject1);
    localObject1 = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    i = ((com.truecaller.truepay.app.utils.a)localObject1);
    localObject1 = (r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    j = ((r)localObject1);
    localObject1 = (ax)dagger.a.g.a(a.e(), "Cannot return null from a non-@Nullable component method");
    k = ((ax)localObject1);
    localObject1 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.J(), "Cannot return null from a non-@Nullable component method");
    l = ((com.truecaller.truepay.data.e.e)localObject1);
    localObject1 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.U(), "Cannot return null from a non-@Nullable component method");
    n = ((com.truecaller.truepay.data.e.e)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.history.views.b.b paramb)
  {
    Object localObject = new com/truecaller/truepay/app/ui/history/b/d;
    com.truecaller.truepay.a.a.c.c localc = (com.truecaller.truepay.a.a.c.c)dagger.a.g.a(a.ah(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.a.a.c.e locale = (com.truecaller.truepay.a.a.c.e)dagger.a.g.a(a.ag(), "Cannot return null from a non-@Nullable component method");
    ((com.truecaller.truepay.app.ui.history.b.d)localObject).<init>(localc, locale);
    d = ((com.truecaller.truepay.app.ui.history.b.d)localObject);
    localObject = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.K(), "Cannot return null from a non-@Nullable component method");
    e = ((com.truecaller.truepay.data.e.e)localObject);
    localObject = (com.google.gson.f)dagger.a.g.a(a.am(), "Cannot return null from a non-@Nullable component method");
    f = ((com.google.gson.f)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.history.views.b.d paramd)
  {
    Object localObject1 = (com.truecaller.truepay.app.ui.history.views.c.c)d.get();
    a = ((com.truecaller.truepay.app.ui.history.views.c.c)localObject1);
    localObject1 = (com.truecaller.truepay.app.ui.history.views.c.b)e.get();
    b = ((com.truecaller.truepay.app.ui.history.views.c.b)localObject1);
    localObject1 = (com.truecaller.truepay.data.e.b)dagger.a.g.a(a.A(), "Cannot return null from a non-@Nullable component method");
    Object localObject2 = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.z(), "Cannot return null from a non-@Nullable component method");
    localObject1 = com.truecaller.truepay.app.ui.history.b.i.a((com.truecaller.truepay.data.e.b)localObject1, (com.truecaller.truepay.data.e.a)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/c/d;
    Object localObject3 = b();
    ((com.truecaller.truepay.a.a.c.d)localObject2).<init>((aa)localObject3);
    a = ((com.truecaller.truepay.a.a.c.d)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/c/b;
    localObject3 = b();
    ((com.truecaller.truepay.a.a.c.b)localObject2).<init>((aa)localObject3);
    b = ((com.truecaller.truepay.a.a.c.b)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/d/c;
    localObject3 = (com.truecaller.truepay.data.api.g)dagger.a.g.a(a.p(), "Cannot return null from a non-@Nullable component method");
    ((com.truecaller.truepay.a.a.d.c)localObject2).<init>((com.truecaller.truepay.data.api.g)localObject3);
    c = ((com.truecaller.truepay.a.a.d.c)localObject2);
    c = ((com.truecaller.truepay.app.ui.history.b.h)localObject1);
    localObject1 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.J(), "Cannot return null from a non-@Nullable component method");
    d = ((com.truecaller.truepay.data.e.e)localObject1);
    localObject1 = (n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    e = ((n)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.history.views.d.a parama)
  {
    Object localObject = (com.truecaller.truepay.app.ui.history.views.c.c)d.get();
    b = ((com.truecaller.truepay.app.ui.history.views.c.c)localObject);
    localObject = (com.truecaller.truepay.app.ui.history.views.c.b)e.get();
    c = ((com.truecaller.truepay.app.ui.history.views.c.b)localObject);
    localObject = (com.truecaller.truepay.app.ui.history.views.c.a)f.get();
    d = ((com.truecaller.truepay.app.ui.history.views.c.a)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.history.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */