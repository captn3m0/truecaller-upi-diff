package com.truecaller.truepay.app.ui.homescreen.presenters;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.truepay.app.ui.growth.db.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.e.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.e.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.f.b.a;
import java.util.List;

public final class i
  extends c
  implements e.b
{
  private final e.a c;
  private final f.b.a d;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(i.class);
    ((u)localObject).<init>(localb, "banners", "getBanners()Ljava/util/List;");
    localObject = (g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public i(f.b.a parama, e.a parama1)
  {
    d = parama;
    c = parama1;
  }
  
  private final List a()
  {
    e.a locala = c;
    Object localObject = this;
    localObject = (e.b)this;
    g localg = b[0];
    return locala.a((e.b)localObject, localg);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject = a;
    int i = ((String)localObject).hashCode();
    int j = -1743572928;
    if (i == j)
    {
      String str = "ItemEvent.CLICKED";
      boolean bool = ((String)localObject).equals(str);
      if (bool)
      {
        int k = b;
        paramh = (a)a().get(k);
        localObject = d;
        str = c;
        paramh = f;
        ((f.b.a)localObject).a(str, paramh);
        return true;
      }
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    return Long.valueOf(ageta).hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.presenters.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */