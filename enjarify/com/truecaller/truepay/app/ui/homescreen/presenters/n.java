package com.truecaller.truepay.app.ui.homescreen.presenters;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.truepay.app.ui.homescreen.views.quickactions.models.QuickAction;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.g.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.g.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.h.b.a;
import java.util.List;

public final class n
  extends c
  implements g.b
{
  private final g.a c;
  private final h.b.a d;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(n.class);
    ((u)localObject).<init>(localb, "actions", "getActions()Ljava/util/List;");
    localObject = (g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public n(g.a parama, h.b.a parama1)
  {
    d = parama1;
    c = parama;
  }
  
  private final List a()
  {
    g.a locala = c;
    Object localObject = this;
    localObject = (g.b)this;
    g localg = b[0];
    return locala.a((g.b)localObject, localg);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject = a;
    int i = ((String)localObject).hashCode();
    int j = -1743572928;
    if (i == j)
    {
      String str = "ItemEvent.CLICKED";
      boolean bool = ((String)localObject).equals(str);
      if (bool)
      {
        int k = b;
        localObject = d;
        paramh = (QuickAction)a().get(k);
        ((h.b.a)localObject).a(paramh);
        return true;
      }
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    return ((QuickAction)a().get(paramInt)).getTag().hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.presenters.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */