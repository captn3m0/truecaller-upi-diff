package com.truecaller.truepay.app.ui.homescreen.presenters;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private o(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static o a(Provider paramProvider1, Provider paramProvider2)
  {
    o localo = new com/truecaller/truepay/app/ui/homescreen/presenters/o;
    localo.<init>(paramProvider1, paramProvider2);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.presenters.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */