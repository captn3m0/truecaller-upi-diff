package com.truecaller.truepay.app.ui.homescreen.presenters;

import c.a.y;
import c.d.f;
import c.g.b.k;
import com.truecaller.bd;
import com.truecaller.featuretoggles.b;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.homescreen.a.d;
import com.truecaller.truepay.app.ui.homescreen.core.base.BaseRoutedCoroutineLifecycleAwarePresenter;
import com.truecaller.truepay.app.ui.homescreen.views.quickactions.models.QuickAction;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.Presenter;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.a.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.d.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.e.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.g.b;
import com.truecaller.truepay.c;
import java.util.List;

public final class HomeScreenPresenter
  extends BaseRoutedCoroutineLifecycleAwarePresenter
  implements HomeScreenMvp.Presenter
{
  private List e;
  private List f;
  private List g;
  private com.truecaller.common.payments.a.a h;
  private final f i;
  private final f j;
  private final d k;
  private final com.truecaller.truepay.app.ui.homescreen.a.g l;
  private final com.truecaller.truepay.data.e.e m;
  private final com.truecaller.featuretoggles.e n;
  
  public HomeScreenPresenter(f paramf1, f paramf2, d paramd, com.truecaller.truepay.app.ui.homescreen.a.g paramg, com.truecaller.truepay.data.e.e parame, com.truecaller.featuretoggles.e parame1)
  {
    super(paramf1);
    i = paramf1;
    j = paramf2;
    k = paramd;
    l = paramg;
    m = parame;
    n = parame1;
    paramf1 = (List)y.a;
    e = paramf1;
    paramf1 = (List)y.a;
    f = paramf1;
    paramf1 = (List)y.a;
    g = paramf1;
  }
  
  private static void a(String paramString)
  {
    Truepay localTruepay = Truepay.getInstance();
    k.a(localTruepay, "Truepay.getInstance()");
    com.truecaller.truepay.data.b.a locala = localTruepay.getAnalyticLoggerHelper();
    localTruepay = Truepay.getInstance();
    k.a(localTruepay, "Truepay.getInstance()");
    Boolean localBoolean = Boolean.valueOf(localTruepay.isRegistrationComplete());
    locala.a("app_payment_transaction_intent", "home_screen_v2", paramString, null, localBoolean);
  }
  
  private final void a(boolean paramBoolean, com.truecaller.common.payments.a.a parama)
  {
    Object localObject = n.r();
    boolean bool = ((b)localObject).a();
    if (bool)
    {
      localObject = getInstancecreditHelper;
      String str = g;
      parama = b;
      ((c)localObject).a(paramBoolean, str, parama);
    }
  }
  
  public final com.truecaller.common.payments.a.a a(d.b paramb, c.l.g paramg)
  {
    k.b(paramb, "thisRef");
    k.b(paramg, "property");
    return h;
  }
  
  public final List a(a.b paramb, c.l.g paramg)
  {
    k.b(paramb, "thisRef");
    k.b(paramg, "property");
    return g;
  }
  
  public final List a(e.b paramb, c.l.g paramg)
  {
    k.b(paramb, "thisRef");
    k.b(paramg, "property");
    return f;
  }
  
  public final List a(g.b paramb, c.l.g paramg)
  {
    k.b(paramb, "thisRef");
    k.b(paramg, "property");
    return e;
  }
  
  public final void a(com.truecaller.common.payments.a.a parama)
  {
    k.b(parama, "creditBanner");
    String str = f;
    if (str != null)
    {
      boolean bool = true;
      a(bool, parama);
      parama = (HomeScreenMvp.a)c;
      if (parama != null)
      {
        parama.a(str);
        return;
      }
      return;
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.dashboard.b.a parama)
  {
    k.b(parama, "accountDo");
    String str1 = parama.a();
    k.a(str1, "accountDo.bankName");
    Object localObject = Truepay.getInstance();
    k.a(localObject, "Truepay.getInstance()");
    localObject = ((Truepay)localObject).getAnalyticLoggerHelper();
    String str2 = "app_payment_balance_check";
    String str3 = "home_screen_v2";
    ((com.truecaller.truepay.data.b.a)localObject).b(str2, str3, "initiated", str1);
    HomeScreenMvp.a locala = (HomeScreenMvp.a)c;
    if (locala != null)
    {
      locala.a(parama);
      return;
    }
  }
  
  public final void a(QuickAction paramQuickAction)
  {
    String str = "actionData";
    k.b(paramQuickAction, str);
    paramQuickAction = paramQuickAction.getTag();
    int i1 = paramQuickAction.hashCode();
    boolean bool;
    switch (i1)
    {
    default: 
      break;
    case 1910947619: 
      str = "scan_qr";
      bool = paramQuickAction.equals(str);
      if (bool)
      {
        a("scan_qr");
        paramQuickAction = (HomeScreenMvp.a)c;
        if (paramQuickAction != null)
        {
          str = "home_screen";
          paramQuickAction.d(str);
        }
        return;
      }
      break;
    case 1095692943: 
      str = "request";
      bool = paramQuickAction.equals(str);
      if (bool)
      {
        a("request");
        paramQuickAction = (HomeScreenMvp.a)c;
        if (paramQuickAction != null)
        {
          str = "home_screen";
          paramQuickAction.e(str);
        }
        return;
      }
      break;
    case 926934164: 
      str = "history";
      bool = paramQuickAction.equals(str);
      if (bool)
      {
        a("history");
        paramQuickAction = (HomeScreenMvp.a)c;
        if (paramQuickAction != null) {
          paramQuickAction.b();
        }
        return;
      }
      break;
    case 624330366: 
      str = "mobile_no";
      bool = paramQuickAction.equals(str);
      if (bool)
      {
        a("mobile_no");
        paramQuickAction = (HomeScreenMvp.a)c;
        if (paramQuickAction != null) {
          paramQuickAction.i();
        }
        return;
      }
      break;
    case 421001844: 
      str = "own_account";
      bool = paramQuickAction.equals(str);
      if (bool)
      {
        a("own_account");
        paramQuickAction = (HomeScreenMvp.a)c;
        if (paramQuickAction != null)
        {
          str = "truecaller://beneficiaries";
          paramQuickAction.a(str);
        }
        return;
      }
      break;
    case -318370833: 
      str = "prepaid";
      bool = paramQuickAction.equals(str);
      if (bool)
      {
        a("home_prepaid");
        paramQuickAction = (HomeScreenMvp.a)c;
        if (paramQuickAction != null)
        {
          paramQuickAction.a("truecaller://utility/prepaid");
          return;
        }
      }
      break;
    case -567451565: 
      str = "contacts";
      bool = paramQuickAction.equals(str);
      if (bool)
      {
        a("contacts");
        paramQuickAction = (HomeScreenMvp.a)c;
        if (paramQuickAction != null)
        {
          str = "home_screen";
          paramQuickAction.c(str);
        }
        return;
      }
      break;
    case -838699572: 
      str = "upi_id";
      bool = paramQuickAction.equals(str);
      if (bool)
      {
        a("UPI_id");
        paramQuickAction = (HomeScreenMvp.a)c;
        if (paramQuickAction != null)
        {
          str = "truecaller://beneficiaries";
          paramQuickAction.a(str);
        }
        return;
      }
      break;
    case -2137146394: 
      str = "accounts";
      bool = paramQuickAction.equals(str);
      if (bool)
      {
        a("accounts");
        paramQuickAction = (HomeScreenMvp.a)c;
        if (paramQuickAction != null)
        {
          str = "truecaller://beneficiaries";
          paramQuickAction.a(str);
        }
        return;
      }
      break;
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    Object localObject = "bannerId";
    k.b(paramString2, (String)localObject);
    if (paramString1 != null)
    {
      localObject = paramString1;
      localObject = (CharSequence)paramString1;
      int i1 = ((CharSequence)localObject).length();
      int i2 = 1;
      if (i1 > 0)
      {
        i1 = 1;
      }
      else
      {
        i1 = 0;
        localObject = null;
      }
      if (i1 == i2)
      {
        localObject = Truepay.getInstance();
        String str = "Truepay.getInstance()";
        k.a(localObject, str);
        localObject = ((Truepay)localObject).getAnalyticLoggerHelper();
        ((com.truecaller.truepay.data.b.a)localObject).a(paramString2);
        paramString2 = (HomeScreenMvp.a)c;
        if (paramString2 != null)
        {
          paramString2.a(paramString1);
          return;
        }
      }
      return;
    }
  }
  
  public final boolean a(int paramInt)
  {
    int i1 = R.id.menu_item_pending_request;
    HomeScreenMvp.a locala;
    if (paramInt == i1)
    {
      locala = (HomeScreenMvp.a)c;
      if (locala != null) {
        locala.g();
      }
    }
    else
    {
      i1 = R.id.menu_item_transaction_history;
      if (paramInt == i1)
      {
        locala = (HomeScreenMvp.a)c;
        if (locala != null) {
          locala.b();
        }
      }
      else
      {
        i1 = R.id.menu_item_manage_accounts;
        if (paramInt == i1)
        {
          locala = (HomeScreenMvp.a)c;
          if (locala != null) {
            locala.c();
          }
        }
        else
        {
          i1 = R.id.menu_item_settings;
          if (paramInt == i1)
          {
            locala = (HomeScreenMvp.a)c;
            if (locala != null) {
              locala.d();
            }
          }
          else
          {
            i1 = R.id.menu_item_support;
            if (paramInt == i1)
            {
              locala = (HomeScreenMvp.a)c;
              if (locala != null)
              {
                String str = m.a();
                if (str == null) {
                  str = "";
                }
                locala.b(str);
              }
            }
            else
            {
              i1 = R.id.menu_item_add_to_home;
              if (paramInt == i1)
              {
                locala = (HomeScreenMvp.a)c;
                if (locala != null) {
                  locala.e();
                }
              }
            }
          }
        }
      }
    }
    return false;
  }
  
  public final void b()
  {
    HomeScreenMvp.b localb = (HomeScreenMvp.b)b;
    if (localb != null)
    {
      localb.b();
      return;
    }
  }
  
  public final void e()
  {
    HomeScreenMvp.a locala = (HomeScreenMvp.a)c;
    if (locala != null)
    {
      locala.h();
      return;
    }
  }
  
  public final void f()
  {
    HomeScreenMvp.a locala = (HomeScreenMvp.a)c;
    if (locala != null)
    {
      locala.j();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.presenters.HomeScreenPresenter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */