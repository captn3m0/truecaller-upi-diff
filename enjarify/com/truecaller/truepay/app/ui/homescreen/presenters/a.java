package com.truecaller.truepay.app.ui.homescreen.presenters;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.a.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.a.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.b.b.a;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.n;
import java.util.List;

public final class a
  extends c
  implements a.b
{
  private final a.a c;
  private final b.b.a d;
  private final n e;
  private final r f;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(a.class);
    ((u)localObject).<init>(localb, "accounts", "getAccounts()Ljava/util/List;");
    localObject = (g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public a(a.a parama, b.b.a parama1, n paramn, r paramr)
  {
    d = parama1;
    e = paramn;
    f = paramr;
    c = parama;
  }
  
  private final List a()
  {
    a.a locala = c;
    Object localObject = this;
    localObject = (a.b)this;
    g localg = b[0];
    return locala.a((a.b)localObject, localg);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject = a;
    int i = ((String)localObject).hashCode();
    int j = -1743572928;
    if (i == j)
    {
      String str = "ItemEvent.CLICKED";
      boolean bool = ((String)localObject).equals(str);
      if (bool)
      {
        int k = b;
        localObject = a();
        paramh = (com.truecaller.truepay.app.ui.dashboard.b.a)((List)localObject).get(k);
        bool = paramh.f();
        if (bool)
        {
          paramh = d;
          paramh.f();
        }
        else
        {
          localObject = d;
          ((b.b.a)localObject).a(paramh);
        }
        return true;
      }
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    return ((com.truecaller.truepay.app.ui.dashboard.b.a)a().get(paramInt)).c().hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.presenters.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */