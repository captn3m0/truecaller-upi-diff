package com.truecaller.truepay.app.ui.homescreen.presenters;

import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import com.truecaller.adapter_delegates.c;
import com.truecaller.common.payments.a.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.d.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.d.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.d.c.a;
import com.truecaller.utils.n;

public final class g
  extends c
  implements d.b
{
  private final d.a c;
  private final n d;
  private final d.c.a e;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(g.class);
    ((u)localObject).<init>(localb, "creditBanner", "getCreditBanner()Lcom/truecaller/common/payments/credit/CreditBanner;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    b = arrayOfg;
  }
  
  public g(d.a parama, n paramn, d.c.a parama1)
  {
    d = paramn;
    e = parama1;
    c = parama;
  }
  
  private final a b()
  {
    d.a locala = c;
    Object localObject = this;
    localObject = (d.b)this;
    c.l.g localg = b[0];
    return locala.a((d.b)localObject, localg);
  }
  
  public final void a()
  {
    a locala = b();
    if (locala != null)
    {
      e.a(locala);
      return;
    }
  }
  
  public final int getItemCount()
  {
    return 1;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.presenters.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */