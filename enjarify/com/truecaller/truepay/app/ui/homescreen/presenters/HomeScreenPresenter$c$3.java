package com.truecaller.truepay.app.ui.homescreen.presenters;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.app.ui.homescreen.a.g;
import java.util.List;
import kotlinx.coroutines.ag;

final class HomeScreenPresenter$c$3
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  HomeScreenPresenter$c$3(HomeScreenPresenter.c paramc, c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    3 local3 = new com/truecaller/truepay/app/ui/homescreen/presenters/HomeScreenPresenter$c$3;
    HomeScreenPresenter.c localc = b;
    local3.<init>(localc, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return local3;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label133;
      }
      paramObject = HomeScreenPresenter.c(b.g);
      j = 1;
      a = j;
      paramObject = ((g)paramObject).b();
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = (List)paramObject;
    if (paramObject != null)
    {
      HomeScreenPresenter.c(b.g, (List)paramObject);
      return x.a;
    }
    return null;
    label133:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (3)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((3)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.presenters.HomeScreenPresenter.c.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */