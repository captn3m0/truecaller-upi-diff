package com.truecaller.truepay.app.ui.homescreen.presenters;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.e;

final class HomeScreenPresenter$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  int f;
  private ag h;
  
  HomeScreenPresenter$c(HomeScreenPresenter paramHomeScreenPresenter, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/truepay/app/ui/homescreen/presenters/HomeScreenPresenter$c;
    HomeScreenPresenter localHomeScreenPresenter = g;
    localc.<init>(localHomeScreenPresenter, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = f;
    Object localObject1;
    Object localObject2;
    Object localObject3;
    int j;
    Object localObject4;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject1 = (Iterator)c;
      localObject2 = (Iterable)b;
      localObject3 = (List)a;
      j = paramObject instanceof o.b;
      if (j == 0) {
        paramObject = this;
      } else {
        throw a;
      }
      break;
    case 0: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        break label426;
      }
      paramObject = h;
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject1 = (List)localObject1;
      localObject2 = HomeScreenPresenter.b(g);
      localObject3 = new com/truecaller/truepay/app/ui/homescreen/presenters/HomeScreenPresenter$c$1;
      j = 0;
      localObject4 = null;
      ((HomeScreenPresenter.c.1)localObject3).<init>(this, null);
      localObject3 = (m)localObject3;
      int k = 2;
      localObject2 = e.a((ag)paramObject, (f)localObject2, (m)localObject3, k);
      ((List)localObject1).add(localObject2);
      localObject2 = HomeScreenPresenter.b(g);
      localObject3 = new com/truecaller/truepay/app/ui/homescreen/presenters/HomeScreenPresenter$c$2;
      ((HomeScreenPresenter.c.2)localObject3).<init>(this, null);
      localObject3 = (m)localObject3;
      localObject2 = e.a((ag)paramObject, (f)localObject2, (m)localObject3, k);
      ((List)localObject1).add(localObject2);
      localObject2 = HomeScreenPresenter.b(g);
      localObject3 = new com/truecaller/truepay/app/ui/homescreen/presenters/HomeScreenPresenter$c$3;
      ((HomeScreenPresenter.c.3)localObject3).<init>(this, null);
      localObject3 = (m)localObject3;
      paramObject = e.a((ag)paramObject, (f)localObject2, (m)localObject3, k);
      ((List)localObject1).add(paramObject);
      paramObject = localObject1;
      paramObject = (Iterable)localObject1;
      localObject2 = ((Iterable)paramObject).iterator();
      localObject3 = localObject1;
      localObject1 = localObject2;
      localObject2 = paramObject;
      paramObject = this;
    }
    do
    {
      j = ((Iterator)localObject1).hasNext();
      if (j == 0) {
        break;
      }
      localObject4 = ((Iterator)localObject1).next();
      Object localObject5 = localObject4;
      localObject5 = (ao)localObject4;
      a = localObject3;
      b = localObject2;
      c = localObject1;
      d = localObject4;
      e = localObject5;
      j = 1;
      f = j;
      localObject4 = ((ao)localObject5).a((c)paramObject);
    } while (localObject4 != locala);
    return locala;
    paramObject = HomeScreenPresenter.d(g);
    if (paramObject != null) {
      ((HomeScreenMvp.b)paramObject).c();
    }
    return x.a;
    label426:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.presenters.HomeScreenPresenter.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */