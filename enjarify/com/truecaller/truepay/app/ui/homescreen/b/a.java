package com.truecaller.truepay.app.ui.homescreen.b;

import com.truecaller.truepay.app.ui.homescreen.a.i;
import com.truecaller.truepay.app.ui.homescreen.a.k;
import com.truecaller.truepay.app.ui.homescreen.presenters.h;
import com.truecaller.truepay.app.ui.homescreen.presenters.j;
import com.truecaller.truepay.app.ui.homescreen.presenters.l;
import com.truecaller.truepay.app.ui.homescreen.presenters.m;
import com.truecaller.truepay.app.ui.homescreen.presenters.o;
import com.truecaller.truepay.app.ui.homescreen.presenters.q;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.Presenter;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.b.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.c.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.d.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.e.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.f.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.g.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.h.a;
import com.truecaller.truepay.app.utils.r;
import dagger.a.g;
import javax.inject.Provider;

public final class a
  implements b
{
  private Provider A;
  private Provider B;
  private Provider C;
  private Provider D;
  private Provider E;
  private Provider F;
  private Provider G;
  private Provider H;
  private Provider I;
  private Provider J;
  private Provider K;
  private Provider L;
  private Provider M;
  private final com.truecaller.truepay.app.a.a.a a;
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  private Provider w;
  private Provider x;
  private Provider y;
  private Provider z;
  
  private a(com.truecaller.truepay.app.a.a.a parama)
  {
    a = parama;
    Object localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$o;
    ((a.o)localObject).<init>(parama);
    b = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$c;
    ((a.c)localObject).<init>(parama);
    c = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$f;
    ((a.f)localObject).<init>(parama);
    d = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$n;
    ((a.n)localObject).<init>(parama);
    e = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$g;
    ((a.g)localObject).<init>(parama);
    f = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$j;
    ((a.j)localObject).<init>(parama);
    g = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$d;
    ((a.d)localObject).<init>(parama);
    h = ((Provider)localObject);
    localObject = d;
    Provider localProvider1 = e;
    Provider localProvider2 = f;
    Provider localProvider3 = g;
    Provider localProvider4 = h;
    localObject = com.truecaller.truepay.app.ui.homescreen.a.f.a((Provider)localObject, localProvider1, localProvider2, localProvider3, localProvider4);
    i = ((Provider)localObject);
    localObject = dagger.a.c.a(i);
    j = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$i;
    ((a.i)localObject).<init>(parama);
    k = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$m;
    ((a.m)localObject).<init>(parama);
    l = ((Provider)localObject);
    localObject = com.truecaller.truepay.data.e.d.a(l);
    m = ((Provider)localObject);
    localObject = k;
    localProvider1 = m;
    localObject = k.a((Provider)localObject, localProvider1);
    n = ((Provider)localObject);
    localObject = dagger.a.c.a(n);
    o = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$k;
    ((a.k)localObject).<init>(parama);
    p = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$b;
    ((a.b)localObject).<init>(parama);
    q = ((Provider)localObject);
    localObject = p;
    localProvider1 = q;
    localObject = com.truecaller.truepay.app.ui.homescreen.a.c.a((Provider)localObject, localProvider1);
    r = ((Provider)localObject);
    localObject = dagger.a.c.a(r);
    s = ((Provider)localObject);
    localObject = o;
    localProvider1 = j;
    localProvider2 = s;
    localObject = i.a((Provider)localObject, localProvider1, localProvider2);
    t = ((Provider)localObject);
    localObject = dagger.a.c.a(t);
    u = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$l;
    ((a.l)localObject).<init>(parama);
    v = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$e;
    ((a.e)localObject).<init>(parama);
    w = ((Provider)localObject);
    localProvider1 = b;
    localProvider2 = c;
    localProvider3 = j;
    localProvider4 = u;
    Provider localProvider5 = v;
    Provider localProvider6 = w;
    localObject = m.a(localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6);
    x = ((Provider)localObject);
    localObject = dagger.a.c.a(x);
    y = ((Provider)localObject);
    localObject = y;
    localObject = j.a((Provider)localObject, (Provider)localObject);
    z = ((Provider)localObject);
    localObject = dagger.a.c.a(z);
    A = ((Provider)localObject);
    localObject = dagger.a.c.a(l.a());
    B = ((Provider)localObject);
    localObject = dagger.a.c.a(q.a());
    C = ((Provider)localObject);
    localObject = y;
    localObject = o.a((Provider)localObject, (Provider)localObject);
    D = ((Provider)localObject);
    localObject = dagger.a.c.a(D);
    E = ((Provider)localObject);
    localObject = dagger.a.c.a(com.truecaller.truepay.app.ui.homescreen.presenters.d.a());
    F = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/homescreen/b/a$h;
    ((a.h)localObject).<init>(parama);
    G = ((Provider)localObject);
    parama = y;
    localObject = p;
    localProvider1 = G;
    parama = com.truecaller.truepay.app.ui.homescreen.presenters.b.a(parama, parama, (Provider)localObject, localProvider1);
    H = parama;
    parama = dagger.a.c.a(H);
    I = parama;
    parama = m;
    localObject = w;
    localProvider1 = p;
    parama = com.truecaller.truepay.app.ui.homescreen.presenters.f.a(parama, (Provider)localObject, localProvider1);
    J = parama;
    parama = dagger.a.c.a(J);
    K = parama;
    parama = y;
    localObject = p;
    parama = h.a(parama, (Provider)localObject, parama);
    L = parama;
    parama = dagger.a.c.a(L);
    M = parama;
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/truepay/app/ui/homescreen/b/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final void a(com.truecaller.truepay.app.ui.homescreen.views.d.a parama)
  {
    Object localObject = (HomeScreenMvp.Presenter)y.get();
    a = ((HomeScreenMvp.Presenter)localObject);
    localObject = (e.b)A.get();
    b = ((e.b)localObject);
    localObject = (f.a)B.get();
    c = ((f.a)localObject);
    localObject = (h.a)C.get();
    d = ((h.a)localObject);
    localObject = (g.b)E.get();
    e = ((g.b)localObject);
    localObject = (b.a)F.get();
    f = ((b.a)localObject);
    localObject = (com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.a.b)I.get();
    g = ((com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.a.b)localObject);
    localObject = (c.a)K.get();
    h = ((c.a)localObject);
    localObject = (d.b)M.get();
    i = ((d.b)localObject);
    localObject = (r)g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    j = ((r)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */