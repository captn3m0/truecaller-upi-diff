package com.truecaller.truepay.app.ui.homescreen.a;

import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private k(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static k a(Provider paramProvider1, Provider paramProvider2)
  {
    k localk = new com/truecaller/truepay/app/ui/homescreen/a/k;
    localk.<init>(paramProvider1, paramProvider2);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */