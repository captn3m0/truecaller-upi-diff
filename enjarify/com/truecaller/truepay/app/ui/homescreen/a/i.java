package com.truecaller.truepay.app.ui.homescreen.a;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private i(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static i a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    i locali = new com/truecaller/truepay/app/ui/homescreen/a/i;
    locali.<init>(paramProvider1, paramProvider2, paramProvider3);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */