package com.truecaller.truepay.app.ui.homescreen.a;

import c.a.m;
import c.g.b.k;
import com.truecaller.truepay.R.string;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class b
  implements a
{
  private final n a;
  private final com.truecaller.truepay.app.utils.a b;
  
  public b(n paramn, com.truecaller.truepay.app.utils.a parama)
  {
    a = paramn;
    b = parama;
  }
  
  public final Object a()
  {
    Object localObject1 = b.b();
    k.a(localObject1, "accounts");
    localObject1 = (Iterable)localObject1;
    Object localObject2 = new java/util/ArrayList;
    int i = m.a((Iterable)localObject1, 10);
    ((ArrayList)localObject2).<init>(i);
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = (com.truecaller.truepay.data.api.model.a)((Iterator)localObject1).next();
      com.truecaller.truepay.app.ui.dashboard.b.a locala = new com/truecaller/truepay/app/ui/dashboard/b/a;
      k.a(localObject3, "account");
      localObject4 = ((com.truecaller.truepay.data.api.model.a)localObject3).l();
      k.a(localObject4, "account.bank");
      localObject5 = ((com.truecaller.truepay.data.d.a)localObject4).b();
      localObject4 = ((com.truecaller.truepay.data.api.model.a)localObject3).l();
      k.a(localObject4, "account.bank");
      String str1 = ((com.truecaller.truepay.data.d.a)localObject4).d();
      localObject6 = ((com.truecaller.truepay.data.api.model.a)localObject3).j();
      boolean bool2 = ((com.truecaller.truepay.data.api.model.a)localObject3).a();
      localObject7 = ((com.truecaller.truepay.data.api.model.a)localObject3).d();
      localObject4 = locala;
      locala.<init>((String)localObject5, str1, (String)localObject6, bool2, (String)localObject7, false);
      ((Collection)localObject2).add(locala);
    }
    localObject1 = m.d((Collection)localObject2);
    Object localObject7 = new com/truecaller/truepay/app/ui/dashboard/b/a;
    localObject2 = a;
    int j = R.string.multiple_accounts_dashboard;
    Object localObject5 = new Object[0];
    Object localObject3 = ((n)localObject2).a(j, (Object[])localObject5);
    localObject2 = a;
    int k = R.string.manage_all_in_one_place_accounts;
    Object localObject4 = new Object[0];
    String str2 = ((n)localObject2).a(k, (Object[])localObject4);
    localObject2 = localObject7;
    localObject4 = "MANAGE_ACCOUNTS";
    localObject5 = " ";
    Object localObject6 = str2;
    k = 1;
    ((com.truecaller.truepay.app.ui.dashboard.b.a)localObject7).<init>((String)localObject3, (String)localObject4, (String)localObject5, false, str2, k);
    ((List)localObject1).add(localObject7);
    return m.g((Iterable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */