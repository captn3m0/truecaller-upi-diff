package com.truecaller.truepay.app.ui.homescreen.a;

import android.arch.lifecycle.LiveData;
import c.a.m;
import c.g.b.k;
import c.x;
import com.truecaller.common.h.q;
import com.truecaller.truepay.app.utils.al;
import com.truecaller.truepay.data.api.c;
import com.truecaller.truepay.data.api.model.PromoBannerDO;
import com.truecaller.truepay.data.api.model.PromoContext;
import com.truecaller.truepay.data.api.model.ad;
import com.truecaller.truepay.data.api.model.h;
import e.r;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class e
  implements d
{
  private final c a;
  private final com.truecaller.truepay.app.ui.growth.db.b b;
  private final com.truecaller.truepay.data.e.b c;
  private final al d;
  private final com.truecaller.utils.a e;
  
  public e(c paramc, com.truecaller.truepay.app.ui.growth.db.b paramb, com.truecaller.truepay.data.e.b paramb1, al paramal, com.truecaller.utils.a parama)
  {
    a = paramc;
    b = paramb;
    c = paramb1;
    d = paramal;
    e = parama;
  }
  
  public final Object a()
  {
    Object localObject1 = a;
    Object localObject2 = new com/truecaller/truepay/data/api/model/PromoContext;
    Object localObject3 = "home";
    ((PromoContext)localObject2).<init>((String)localObject3);
    localObject1 = q.a(((c)localObject1).a((PromoContext)localObject2));
    if (localObject1 != null)
    {
      boolean bool1 = ((r)localObject1).d();
      if (!bool1) {
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        localObject1 = (h)((r)localObject1).e();
        if (localObject1 != null)
        {
          localObject2 = "response?.takeIf { it.is…return logRequestFailed()";
          k.a(localObject1, (String)localObject2);
          localObject1 = (ad)((h)localObject1).c();
          if (localObject1 != null)
          {
            localObject1 = a;
            if (localObject1 != null)
            {
              localObject1 = (Iterable)localObject1;
              localObject2 = new java/util/ArrayList;
              int i = m.a((Iterable)localObject1, 10);
              ((ArrayList)localObject2).<init>(i);
              localObject2 = (Collection)localObject2;
              localObject1 = ((Iterable)localObject1).iterator();
              int j;
              for (;;)
              {
                boolean bool2 = ((Iterator)localObject1).hasNext();
                j = 1;
                if (!bool2) {
                  break;
                }
                localObject3 = (PromoBannerDO)((Iterator)localObject1).next();
                com.truecaller.truepay.app.ui.growth.db.a locala = new com/truecaller/truepay/app/ui/growth/db/a;
                locala.<init>();
                String str = ((PromoBannerDO)localObject3).getImageUrl();
                locala.a(str);
                long l1 = Long.parseLong(((PromoBannerDO)localObject3).getExpiresAt());
                d = l1;
                str = ((PromoBannerDO)localObject3).getDeepLink();
                locala.b(str);
                e = j;
                localObject3 = ((PromoBannerDO)localObject3).getBannerId();
                locala.c((String)localObject3);
                ((Collection)localObject2).add(locala);
              }
              localObject2 = (List)localObject2;
              localObject1 = c;
              long l2 = e.a();
              localObject3 = c.d.b.a.b.a(l2);
              ((com.truecaller.truepay.data.e.b)localObject1).a((Long)localObject3);
              localObject1 = b;
              ((com.truecaller.truepay.app.ui.growth.db.b)localObject1).a(j, (List)localObject2);
            }
          }
          return x.a;
        }
      }
    }
    return x.a;
  }
  
  public final LiveData b()
  {
    com.truecaller.truepay.app.ui.growth.db.b localb = b;
    int i = (int)(e.a() / 1000L);
    return localb.b(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */