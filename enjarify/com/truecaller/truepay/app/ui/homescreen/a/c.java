package com.truecaller.truepay.app.ui.homescreen.a;

import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private c(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static c a(Provider paramProvider1, Provider paramProvider2)
  {
    c localc = new com/truecaller/truepay/app/ui/homescreen/a/c;
    localc.<init>(paramProvider1, paramProvider2);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */