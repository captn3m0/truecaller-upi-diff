package com.truecaller.truepay.app.ui.homescreen.a;

import android.arch.lifecycle.LiveData;
import com.truecaller.truepay.Truepay;

public final class h
  implements g
{
  private final l a;
  private final d b;
  private final a c;
  
  public h(l paraml, d paramd, a parama)
  {
    a = paraml;
    b = paramd;
    c = parama;
  }
  
  public final Object a()
  {
    return b.a();
  }
  
  public final Object a(c.d.c paramc)
  {
    return a.a(paramc);
  }
  
  public final Object b()
  {
    return c.a();
  }
  
  public final LiveData c()
  {
    return getInstancecreditHelper.d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */