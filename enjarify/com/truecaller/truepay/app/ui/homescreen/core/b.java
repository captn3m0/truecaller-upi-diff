package com.truecaller.truepay.app.ui.homescreen.core;

import c.g.b.k;
import com.truecaller.common.h.am;

public final class b
  implements a
{
  private final com.google.firebase.remoteconfig.a a;
  
  public b(com.google.firebase.remoteconfig.a parama)
  {
    a = parama;
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "key");
    paramString = am.n(a.a(paramString));
    k.a(paramString, "StringUtils.defaultStrin…oteConfig.getString(key))");
    return paramString;
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "key");
    paramString = a.a(paramString);
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    if (localObject != null)
    {
      i = ((CharSequence)localObject).length();
      if (i != 0)
      {
        i = 0;
        localObject = null;
        break label49;
      }
    }
    int i = 1;
    label49:
    if (i == 0) {
      return Boolean.parseBoolean(paramString);
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */