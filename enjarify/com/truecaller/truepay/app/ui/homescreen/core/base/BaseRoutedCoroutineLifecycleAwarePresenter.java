package com.truecaller.truepay.app.ui.homescreen.core.base;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.e;
import android.arch.lifecycle.g;
import android.arch.lifecycle.h;
import android.arch.lifecycle.p;
import c.d.f;
import c.g.a.b;
import c.g.b.k;
import com.truecaller.bc;

public abstract class BaseRoutedCoroutineLifecycleAwarePresenter
  extends bc
  implements RoutedLifecycleAwarePresenter
{
  protected e d;
  
  public BaseRoutedCoroutineLifecycleAwarePresenter(f paramf)
  {
    super(paramf);
  }
  
  protected static void a(e parame, LiveData paramLiveData, b paramb)
  {
    k.b(parame, "receiver$0");
    k.b(paramLiveData, "liveData");
    k.b(paramb, "observer");
    Object localObject = new com/truecaller/truepay/app/ui/homescreen/core/base/BaseRoutedCoroutineLifecycleAwarePresenter$a;
    ((BaseRoutedCoroutineLifecycleAwarePresenter.a)localObject).<init>(parame);
    localObject = (h)localObject;
    parame = new com/truecaller/truepay/app/ui/homescreen/core/base/a;
    parame.<init>(paramb);
    parame = (p)parame;
    paramLiveData.a((h)localObject, parame);
  }
  
  public final void a(Object paramObject)
  {
    super.a(paramObject);
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("Use #onAttachView(presenterView: PV, lifecycle: Lifecycle) when using BaseRoutedCoroutineLifecycleAwarePresenter");
    throw ((Throwable)paramObject);
  }
  
  public void a(Object paramObject, e parame)
  {
    k.b(parame, "lifecycle");
    super.a(paramObject);
    d = parame;
    paramObject = this;
    paramObject = (g)this;
    parame.a((g)paramObject);
  }
  
  public void onViewDestroyed()
  {
    e locale = d;
    if (locale != null)
    {
      Object localObject = this;
      localObject = (g)this;
      locale.b((g)localObject);
    }
    d = null;
  }
  
  public final void y_()
  {
    super.y_();
    e locale = d;
    if (locale != null)
    {
      Object localObject = this;
      localObject = (g)this;
      locale.b((g)localObject);
    }
    d = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.base.BaseRoutedCoroutineLifecycleAwarePresenter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */