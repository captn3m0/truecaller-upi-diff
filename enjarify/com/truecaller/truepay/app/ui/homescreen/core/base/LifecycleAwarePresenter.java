package com.truecaller.truepay.app.ui.homescreen.core.base;

import android.arch.lifecycle.e;
import android.arch.lifecycle.g;
import com.truecaller.bm;

public abstract interface LifecycleAwarePresenter
  extends g, bm
{
  public abstract void a(Object paramObject, e parame);
  
  public abstract void onViewDestroyed();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.base.LifecycleAwarePresenter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */