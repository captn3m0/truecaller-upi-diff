package com.truecaller.truepay.app.ui.homescreen.core.base;

import android.arch.lifecycle.e;
import android.arch.lifecycle.g;
import com.truecaller.ba;

public abstract class BaseCoroutineLifecycleAwarePresenter
  extends ba
  implements LifecycleAwarePresenter
{
  protected e c;
  
  public void onViewDestroyed()
  {
    e locale = c;
    if (locale != null)
    {
      Object localObject = this;
      localObject = (g)this;
      locale.b((g)localObject);
    }
    c = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.base.BaseCoroutineLifecycleAwarePresenter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */