package com.truecaller.truepay.app.ui.homescreen.core.base;

import android.arch.lifecycle.e;
import android.arch.lifecycle.g;
import com.truecaller.bb;

public abstract class BaseLifecycleAwarePresenter
  extends bb
  implements LifecycleAwarePresenter
{
  protected e a;
  
  public void onViewDestroyed()
  {
    e locale = a;
    if (locale != null)
    {
      Object localObject = this;
      localObject = (g)this;
      locale.b((g)localObject);
    }
    a = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.base.BaseLifecycleAwarePresenter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */