package com.truecaller.truepay.app.ui.homescreen.core.base;

import android.arch.lifecycle.g;
import com.truecaller.bn;

public abstract interface RoutedLifecycleAwarePresenter
  extends g, bn, LifecycleAwarePresenter
{}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.base.RoutedLifecycleAwarePresenter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */