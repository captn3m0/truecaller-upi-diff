package com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces;

public abstract interface HomeScreenMvp$b
{
  public static final HomeScreenMvp.b.a b = HomeScreenMvp.b.a.a;
  
  public abstract void a();
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */