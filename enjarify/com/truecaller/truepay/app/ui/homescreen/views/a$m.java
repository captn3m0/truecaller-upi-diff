package com.truecaller.truepay.app.ui.homescreen.views;

import android.view.MenuItem;
import android.widget.PopupMenu.OnMenuItemClickListener;
import c.g.b.k;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.Presenter;

final class a$m
  implements PopupMenu.OnMenuItemClickListener
{
  a$m(a parama, boolean paramBoolean) {}
  
  public final boolean onMenuItemClick(MenuItem paramMenuItem)
  {
    HomeScreenMvp.Presenter localPresenter = a.a(a);
    k.a(paramMenuItem, "it");
    int i = paramMenuItem.getItemId();
    return localPresenter.a(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.a.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */