package com.truecaller.truepay.app.ui.homescreen.views.quickactions.models;

import c.g.b.k;

public final class QuickAction
{
  private final String icon;
  private final String name;
  private final String psp;
  private final String tag;
  
  public QuickAction(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    name = paramString1;
    tag = paramString2;
    icon = paramString3;
    psp = paramString4;
  }
  
  public final String component1()
  {
    return name;
  }
  
  public final String component2()
  {
    return tag;
  }
  
  public final String component3()
  {
    return icon;
  }
  
  public final String component4()
  {
    return psp;
  }
  
  public final QuickAction copy(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "name");
    k.b(paramString2, "tag");
    k.b(paramString3, "icon");
    k.b(paramString4, "psp");
    QuickAction localQuickAction = new com/truecaller/truepay/app/ui/homescreen/views/quickactions/models/QuickAction;
    localQuickAction.<init>(paramString1, paramString2, paramString3, paramString4);
    return localQuickAction;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof QuickAction;
      if (bool1)
      {
        paramObject = (QuickAction)paramObject;
        String str1 = name;
        String str2 = name;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = tag;
          str2 = tag;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = icon;
            str2 = icon;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = psp;
              paramObject = psp;
              boolean bool2 = k.a(str1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final String getIcon()
  {
    return icon;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final String getPsp()
  {
    return psp;
  }
  
  public final String getTag()
  {
    return tag;
  }
  
  public final int hashCode()
  {
    String str1 = name;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = tag;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = icon;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = psp;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("QuickAction(name=");
    String str = name;
    localStringBuilder.append(str);
    localStringBuilder.append(", tag=");
    str = tag;
    localStringBuilder.append(str);
    localStringBuilder.append(", icon=");
    str = icon;
    localStringBuilder.append(str);
    localStringBuilder.append(", psp=");
    str = psp;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.quickactions.models.QuickAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */