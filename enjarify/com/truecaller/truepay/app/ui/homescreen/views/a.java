package com.truecaller.truepay.app.ui.homescreen.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.adapter_delegates.p;
import com.truecaller.adapter_delegates.s;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.menu;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.Presenter;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.b.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.c.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.d.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.e.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.f.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.g.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.h.a;
import com.truecaller.truepay.app.utils.r;

public final class a
  implements HomeScreenMvp.b
{
  private final c.f c;
  private final c.f d;
  private final c.f e;
  private PopupMenu f;
  private final com.truecaller.adapter_delegates.g g;
  private final p h;
  private final p i;
  private final p j;
  private final p k;
  private final p l;
  private final com.truecaller.adapter_delegates.f m;
  private final View n;
  private final HomeScreenMvp.Presenter o;
  private final r p;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[3];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(a.class);
    ((u)localObject).<init>(localb, "listHome", "getListHome()Landroid/support/v7/widget/RecyclerView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "optionMenu", "getOptionMenu()Landroid/widget/ImageView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "navButton", "getNavButton()Landroid/widget/ImageView;");
    localObject = (c.l.g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    a = arrayOfg;
  }
  
  public a(View paramView, HomeScreenMvp.Presenter paramPresenter, c.a parama, e.b paramb, f.a parama1, h.a parama2, g.b paramb1, b.a parama3, com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.a.b paramb2, d.b paramb3, r paramr)
  {
    n = paramView;
    o = paramPresenter;
    p = paramr;
    paramView = n;
    int i1 = R.id.payHomeList;
    paramView = com.truecaller.utils.extensions.t.a(paramView, i1);
    c = paramView;
    paramView = n;
    i1 = R.id.imgOption;
    paramView = com.truecaller.utils.extensions.t.a(paramView, i1);
    d = paramView;
    paramView = n;
    i1 = R.id.imgNav;
    paramView = com.truecaller.utils.extensions.t.a(paramView, i1);
    e = paramView;
    paramView = new com/truecaller/adapter_delegates/g;
    paramView.<init>((byte)0);
    g = paramView;
    paramView = new com/truecaller/adapter_delegates/p;
    parama1 = (com.truecaller.adapter_delegates.b)parama1;
    i1 = R.layout.layout_home_screen_promo;
    paramr = new com/truecaller/truepay/app/ui/homescreen/views/a$g;
    paramr.<init>(paramb);
    paramr = (c.g.a.b)paramr;
    paramb = (c.g.a.b)a.h.a;
    paramView.<init>(parama1, i1, paramr, paramb);
    h = paramView;
    paramView = new com/truecaller/adapter_delegates/p;
    parama2 = (com.truecaller.adapter_delegates.b)parama2;
    i1 = R.layout.layout_home_screen_quick_actions;
    paramb = new com/truecaller/truepay/app/ui/homescreen/views/a$i;
    paramb.<init>(paramb1);
    paramb = (c.g.a.b)paramb;
    parama1 = (c.g.a.b)a.j.a;
    paramView.<init>(parama2, i1, paramb, parama1);
    i = paramView;
    paramView = new com/truecaller/adapter_delegates/p;
    parama3 = (com.truecaller.adapter_delegates.b)parama3;
    i1 = R.layout.layout_home_screen_balance_check;
    paramb = new com/truecaller/truepay/app/ui/homescreen/views/a$a;
    paramb.<init>(paramb2);
    paramb = (c.g.a.b)paramb;
    parama1 = (c.g.a.b)a.b.a;
    paramView.<init>(parama3, i1, paramb, parama1);
    j = paramView;
    paramView = new com/truecaller/adapter_delegates/p;
    parama = (com.truecaller.adapter_delegates.b)parama;
    i1 = R.layout.layout_home_bank_footer;
    paramb = (c.g.a.b)a.c.a;
    parama1 = (c.g.a.b)a.d.a;
    paramView.<init>(parama, i1, paramb, parama1);
    k = paramView;
    paramView = new com/truecaller/adapter_delegates/p;
    paramPresenter = paramb3;
    paramPresenter = (com.truecaller.adapter_delegates.b)paramb3;
    int i2 = R.layout.layout_home_credit_banner;
    paramb = new com/truecaller/truepay/app/ui/homescreen/views/a$e;
    paramb.<init>(this, paramb3);
    paramb = (c.g.a.b)paramb;
    parama1 = (c.g.a.b)a.f.a;
    paramView.<init>(paramPresenter, i2, paramb, parama1);
    i1 = 1;
    a = i1;
    l = paramView;
    paramView = new com/truecaller/adapter_delegates/f;
    parama = k;
    paramb = (com.truecaller.adapter_delegates.a)l;
    parama1 = (s)g;
    parama = parama.a(paramb, parama1);
    paramb = (com.truecaller.adapter_delegates.a)j;
    parama1 = (s)g;
    parama = parama.a(paramb, parama1);
    paramb = (com.truecaller.adapter_delegates.a)i;
    parama1 = (s)g;
    parama = parama.a(paramb, parama1);
    paramb = (com.truecaller.adapter_delegates.a)h;
    parama1 = (s)g;
    parama = (com.truecaller.adapter_delegates.a)parama.a(paramb, parama1);
    paramView.<init>(parama);
    paramView.setHasStableIds(i1);
    m = paramView;
  }
  
  private final ImageView d()
  {
    return (ImageView)d.b();
  }
  
  public final void a()
  {
    ImageView localImageView = d();
    Object localObject = new com/truecaller/truepay/app/ui/homescreen/views/a$k;
    ((a.k)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    localImageView.setOnClickListener((View.OnClickListener)localObject);
    localImageView = (ImageView)e.b();
    localObject = new com/truecaller/truepay/app/ui/homescreen/views/a$l;
    ((a.l)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    localImageView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject1 = new android/widget/PopupMenu;
    Object localObject2 = n.getContext();
    View localView = (View)d();
    ((PopupMenu)localObject1).<init>((Context)localObject2, localView);
    f = ((PopupMenu)localObject1);
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject2 = new com/truecaller/truepay/app/ui/homescreen/views/a$m;
      ((a.m)localObject2).<init>(this, paramBoolean);
      localObject2 = (PopupMenu.OnMenuItemClickListener)localObject2;
      ((PopupMenu)localObject1).setOnMenuItemClickListener((PopupMenu.OnMenuItemClickListener)localObject2);
      localObject2 = ((PopupMenu)localObject1).getMenuInflater();
      int i1 = R.menu.menu_dashboard;
      Menu localMenu = ((PopupMenu)localObject1).getMenu();
      ((MenuInflater)localObject2).inflate(i1, localMenu);
      localObject1 = ((PopupMenu)localObject1).getMenu();
      int i2 = R.id.menu_item_add_to_home;
      localObject1 = ((Menu)localObject1).findItem(i2);
      k.a(localObject1, "shortcutItem");
      ((MenuItem)localObject1).setVisible(paramBoolean);
      return;
    }
  }
  
  public final void b()
  {
    PopupMenu localPopupMenu = f;
    if (localPopupMenu != null)
    {
      localPopupMenu.show();
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    p localp = l;
    paramBoolean ^= true;
    a = paramBoolean;
    m.notifyDataSetChanged();
  }
  
  public final void c()
  {
    RecyclerView localRecyclerView = (RecyclerView)c.b();
    Object localObject = (RecyclerView.Adapter)m;
    localRecyclerView.setAdapter((RecyclerView.Adapter)localObject);
    localObject = new android/support/v7/widget/LinearLayoutManager;
    Context localContext = n.getContext();
    ((LinearLayoutManager)localObject).<init>(localContext);
    localObject = (RecyclerView.LayoutManager)localObject;
    localRecyclerView.setLayoutManager((RecyclerView.LayoutManager)localObject);
    localRecyclerView.setItemAnimator(null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */