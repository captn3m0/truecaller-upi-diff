package com.truecaller.truepay.app.ui.homescreen.views.d;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.view.View;
import c.g.b.k;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.ui.base.views.fragments.TcPayOnFragmentInteractionListener;
import com.truecaller.truepay.app.ui.dashboard.views.activities.SettingsActivity;
import com.truecaller.truepay.app.ui.history.views.activities.TransactionHistoryActivity;
import com.truecaller.truepay.app.ui.history.views.activities.TruePayWebViewActivity;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.Presenter;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.a.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.b.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.c.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.d.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.e.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.f.a;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.g.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.h.a;
import com.truecaller.truepay.app.ui.scan.views.activities.ScanAndPayActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.SearchTransactionActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.truepay.app.utils.r;
import java.io.Serializable;
import java.util.HashMap;

public final class a
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements com.truecaller.common.ui.b, HomeScreenMvp.a
{
  public static final a.a k;
  public HomeScreenMvp.Presenter a;
  public e.b b;
  public f.a c;
  public h.a d;
  public g.b e;
  public b.a f;
  public a.b g;
  public c.a h;
  public d.b i;
  public r j;
  private TcPayOnFragmentInteractionListener l;
  private final int n = 8;
  private HashMap o;
  
  static
  {
    a.a locala = new com/truecaller/truepay/app/ui/homescreen/views/d/a$a;
    locala.<init>((byte)0);
    k = locala;
  }
  
  public static final a k()
  {
    a locala = new com/truecaller/truepay/app/ui/homescreen/views/d/a;
    locala.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    locala.setArguments(localBundle);
    return locala;
  }
  
  public final int a()
  {
    return R.layout.fragment_banking_home_screen;
  }
  
  public final void a(com.truecaller.truepay.app.ui.dashboard.b.a parama)
  {
    k.b(parama, "accountDo");
    Object localObject1 = com.truecaller.truepay.app.ui.balancecheck.c.a.c;
    k.b(parama, "accountDo");
    localObject1 = new com/truecaller/truepay/app/ui/balancecheck/c/a;
    ((com.truecaller.truepay.app.ui.balancecheck.c.a)localObject1).<init>();
    Object localObject2 = new android/os/Bundle;
    ((Bundle)localObject2).<init>();
    parama = (Serializable)parama;
    ((Bundle)localObject2).putSerializable("account", parama);
    ((com.truecaller.truepay.app.ui.balancecheck.c.a)localObject1).setArguments((Bundle)localObject2);
    localObject1 = (android.support.design.widget.b)localObject1;
    parama = this;
    parama = (Fragment)this;
    ((android.support.design.widget.b)localObject1).setTargetFragment(parama, 1007);
    parama = getFragmentManager();
    localObject2 = ((android.support.design.widget.b)localObject1).getTag();
    ((android.support.design.widget.b)localObject1).show(parama, (String)localObject2);
  }
  
  public final void a(String paramString)
  {
    Object localObject = "deepLink";
    k.b(paramString, (String)localObject);
    try
    {
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>();
      String str = "android.intent.action.VIEW";
      ((Intent)localObject).setAction(str);
      paramString = Uri.parse(paramString);
      ((Intent)localObject).setData(paramString);
      startActivity((Intent)localObject);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException) {}
  }
  
  public final void b()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      Intent localIntent = new android/content/Intent;
      localObject = (Context)localObject;
      localIntent.<init>((Context)localObject, TransactionHistoryActivity.class);
      startActivity(localIntent);
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "url");
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      localObject1 = new android/content/Intent;
      Object localObject2 = (Context)getActivity();
      Class localClass = TruePayWebViewActivity.class;
      ((Intent)localObject1).<init>((Context)localObject2, localClass);
      localObject2 = "url";
      ((Intent)localObject1).putExtra((String)localObject2, paramString);
      startActivity((Intent)localObject1);
    }
  }
  
  public final void c()
  {
    f localf = getActivity();
    if (localf != null)
    {
      ManageAccountsActivity.a((Context)localf, null, null, "");
      return;
    }
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "analyticsContext");
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      localObject1 = new android/content/Intent;
      Object localObject2 = (Context)getActivity();
      Class localClass = TransactionActivity.class;
      ((Intent)localObject1).<init>((Context)localObject2, localClass);
      int m = 1004;
      ((Intent)localObject1).putExtra("tranx_type", m);
      localObject2 = "from";
      ((Intent)localObject1).putExtra((String)localObject2, paramString);
      startActivity((Intent)localObject1);
    }
  }
  
  public final void d()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      Intent localIntent = new android/content/Intent;
      localObject = (Context)localObject;
      localIntent.<init>((Context)localObject, SettingsActivity.class);
      startActivityForResult(localIntent, 100);
      return;
    }
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "analyticsContext");
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      localObject1 = new android/content/Intent;
      Object localObject2 = (Context)getActivity();
      Class localClass = ScanAndPayActivity.class;
      ((Intent)localObject1).<init>((Context)localObject2, localClass);
      localObject2 = "from";
      ((Intent)localObject1).putExtra((String)localObject2, paramString);
      startActivity((Intent)localObject1);
    }
  }
  
  public final void e()
  {
    Truepay localTruepay = Truepay.getInstance();
    k.a(localTruepay, "Truepay.getInstance()");
    localTruepay.getAnalyticLoggerHelper().g("dashboard_tab_menu");
    localTruepay = Truepay.getInstance();
    k.a(localTruepay, "Truepay.getInstance()");
    localTruepay.getListener().createShortcut(3);
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "analyticsContext");
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      localObject1 = new android/content/Intent;
      Object localObject2 = (Context)getActivity();
      Class localClass = TransactionActivity.class;
      ((Intent)localObject1).<init>((Context)localObject2, localClass);
      int m = 1003;
      ((Intent)localObject1).putExtra("tranx_type", m);
      localObject2 = "from";
      ((Intent)localObject1).putExtra((String)localObject2, paramString);
      startActivity((Intent)localObject1);
    }
  }
  
  public final int f()
  {
    return n;
  }
  
  public final void g()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      Intent localIntent = new android/content/Intent;
      localObject = (Context)localObject;
      localIntent.<init>((Context)localObject, TransactionActivity.class);
      localIntent.putExtra("route", "route_pending_request");
      startActivity(localIntent);
      return;
    }
  }
  
  public final void h()
  {
    TcPayOnFragmentInteractionListener localTcPayOnFragmentInteractionListener = l;
    if (localTcPayOnFragmentInteractionListener != null)
    {
      localTcPayOnFragmentInteractionListener.onHamburgerClicked();
      return;
    }
  }
  
  public final void i()
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      localObject1 = new android/content/Intent;
      Object localObject2 = (Context)getActivity();
      ((Intent)localObject1).<init>((Context)localObject2, SearchTransactionActivity.class);
      localObject2 = "search_type";
      String str = "search_type_send";
      ((Intent)localObject1).putExtra((String)localObject2, str);
      int m = 1005;
      startActivityForResult((Intent)localObject1, m);
    }
  }
  
  public final void j()
  {
    f localf = getActivity();
    if (localf != null)
    {
      ManageAccountsActivity.a((Context)localf, null, null, "");
      return;
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int m = 100;
    if (paramInt1 == m)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        TcPayOnFragmentInteractionListener localTcPayOnFragmentInteractionListener = l;
        if (localTcPayOnFragmentInteractionListener != null)
        {
          Object localObject = Truepay.getInstance();
          k.a(localObject, "Truepay.getInstance()");
          localObject = (Fragment)((Truepay)localObject).getBankingFragment();
          localTcPayOnFragmentInteractionListener.replaceFragment((Fragment)localObject);
          return;
        }
      }
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject = getActivity();
    boolean bool = localObject instanceof TcPayOnFragmentInteractionListener;
    if (bool)
    {
      paramContext = (TcPayOnFragmentInteractionListener)getActivity();
      l = paramContext;
      return;
    }
    localObject = new java/lang/IllegalStateException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = String.valueOf(paramContext);
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implemenet ");
    paramContext = TcPayOnFragmentInteractionListener.class.getSimpleName();
    localStringBuilder.append(paramContext);
    paramContext = localStringBuilder.toString();
    ((IllegalStateException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.homescreen.b.a.a();
    Object localObject = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a().a(this);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    paramBundle.b(this);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject1 = "view";
    k.b(paramView, (String)localObject1);
    super.onViewCreated(paramView, paramBundle);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject1 = "presenter";
      k.a((String)localObject1);
    }
    localObject1 = new com/truecaller/truepay/app/ui/homescreen/views/a;
    HomeScreenMvp.Presenter localPresenter = a;
    if (localPresenter == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    c.a locala = h;
    if (locala == null)
    {
      localObject2 = "bankFooterPresenter";
      k.a((String)localObject2);
    }
    e.b localb = b;
    if (localb == null)
    {
      localObject2 = "homePromoItemPresenter";
      k.a((String)localObject2);
    }
    f.a locala1 = c;
    if (locala1 == null)
    {
      localObject2 = "promoPresenter";
      k.a((String)localObject2);
    }
    h.a locala2 = d;
    if (locala2 == null)
    {
      localObject2 = "quickActionPresenter";
      k.a((String)localObject2);
    }
    g.b localb1 = e;
    if (localb1 == null)
    {
      localObject2 = "quickActionItemPresenter";
      k.a((String)localObject2);
    }
    b.a locala3 = f;
    if (locala3 == null)
    {
      localObject2 = "balanceCheckPresenter";
      k.a((String)localObject2);
    }
    a.b localb2 = g;
    if (localb2 == null)
    {
      localObject2 = "balanceCheckItemPresenter";
      k.a((String)localObject2);
    }
    d.b localb3 = i;
    if (localb3 == null)
    {
      localObject2 = "creditPresenter";
      k.a((String)localObject2);
    }
    r localr = j;
    if (localr == null)
    {
      localObject2 = "imageLoader";
      k.a((String)localObject2);
    }
    Object localObject2 = localObject1;
    ((com.truecaller.truepay.app.ui.homescreen.views.a)localObject1).<init>(paramView, localPresenter, locala, localb, locala1, locala2, localb1, locala3, localb2, localb3, localr);
    paramView = getLifecycle();
    k.a(paramView, "lifecycle");
    paramBundle.a(localObject1, paramView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */