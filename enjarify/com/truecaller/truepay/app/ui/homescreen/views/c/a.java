package com.truecaller.truepay.app.ui.homescreen.views.c;

import android.support.constraint.Group;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.d.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.d.c;
import com.truecaller.truepay.app.utils.r;

public final class a
  extends RecyclerView.ViewHolder
  implements d.c
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final f f;
  private final f g;
  private final f h;
  private final f i;
  private final f j;
  private final f k;
  private final r l;
  private final d.b m;
  
  static
  {
    g[] arrayOfg = new g[10];
    Object localObject = new c/g/b/u;
    b localb = w.a(a.class);
    ((u)localObject).<init>(localb, "titleText", "getTitleText()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "leftHeaderContainer", "getLeftHeaderContainer()Landroid/widget/LinearLayout;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "leftHeaderText", "getLeftHeaderText()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "rightHeaderText", "getRightHeaderText()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[3] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "progressText", "getProgressText()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[4] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "progressStatus", "getProgressStatus()Landroid/widget/ProgressBar;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[5] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "bannerImage", "getBannerImage()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[6] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "bannerSubTitle", "getBannerSubTitle()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[7] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "bannerButton", "getBannerButton()Landroid/widget/Button;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[8] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(a.class);
    ((u)localObject).<init>(localb, "progressGroup", "getProgressGroup()Landroid/support/constraint/Group;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[9] = localObject;
    a = arrayOfg;
  }
  
  public a(View paramView, r paramr, d.b paramb)
  {
    super(paramView);
    l = paramr;
    m = paramb;
    int n = R.id.tvTitle;
    paramr = com.truecaller.utils.extensions.t.a(paramView, n);
    b = paramr;
    n = R.id.llHeader;
    paramr = com.truecaller.utils.extensions.t.a(paramView, n);
    c = paramr;
    n = R.id.tvHeaderLeft;
    paramr = com.truecaller.utils.extensions.t.a(paramView, n);
    d = paramr;
    n = R.id.tvHeaderRight;
    paramr = com.truecaller.utils.extensions.t.a(paramView, n);
    e = paramr;
    n = R.id.tvPercentage;
    paramr = com.truecaller.utils.extensions.t.a(paramView, n);
    f = paramr;
    n = R.id.pbPercentage;
    paramr = com.truecaller.utils.extensions.t.a(paramView, n);
    g = paramr;
    n = R.id.ivRight;
    paramr = com.truecaller.utils.extensions.t.a(paramView, n);
    h = paramr;
    n = R.id.tvSubtitle;
    paramr = com.truecaller.utils.extensions.t.a(paramView, n);
    i = paramr;
    n = R.id.btnCta;
    paramr = com.truecaller.utils.extensions.t.a(paramView, n);
    j = paramr;
    n = R.id.groupProgress;
    paramView = com.truecaller.utils.extensions.t.a(paramView, n);
    k = paramView;
    paramView = b();
    paramr = new com/truecaller/truepay/app/ui/homescreen/views/c/a$1;
    paramr.<init>(this);
    paramr = (View.OnClickListener)paramr;
    paramView.setOnClickListener(paramr);
  }
  
  private final ImageView a()
  {
    return (ImageView)h.b();
  }
  
  private final Button b()
  {
    return (Button)j.b();
  }
  
  public final void a(int paramInt)
  {
    ProgressBar localProgressBar = (ProgressBar)g.b();
    k.a(localProgressBar, "progressStatus");
    localProgressBar.setProgress(paramInt);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "buttonText");
    Button localButton = b();
    k.a(localButton, "bannerButton");
    paramString = (CharSequence)paramString;
    localButton.setText(paramString);
  }
  
  public final void a(String paramString, int paramInt1, int paramInt2)
  {
    k.b(paramString, "url");
    r localr = l;
    ImageView localImageView = a();
    k.a(localImageView, "bannerImage");
    localr.a(paramString, localImageView, paramInt1, paramInt2);
  }
  
  public final void a(boolean paramBoolean)
  {
    LinearLayout localLinearLayout = (LinearLayout)c.b();
    k.a(localLinearLayout, "leftHeaderContainer");
    com.truecaller.utils.extensions.t.a((View)localLinearLayout, paramBoolean);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "subTitle");
    TextView localTextView = (TextView)i.b();
    k.a(localTextView, "bannerSubTitle");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    ImageView localImageView = a();
    k.a(localImageView, "bannerImage");
    com.truecaller.utils.extensions.t.a((View)localImageView, paramBoolean);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "progressVal");
    TextView localTextView = (TextView)f.b();
    k.a(localTextView, "progressText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c(boolean paramBoolean)
  {
    Group localGroup = (Group)k.b();
    k.a(localGroup, "progressGroup");
    com.truecaller.utils.extensions.t.a((View)localGroup, paramBoolean);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "leftHeader");
    TextView localTextView = (TextView)d.b();
    k.a(localTextView, "leftHeaderText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "rightHeader");
    TextView localTextView = (TextView)e.b();
    k.a(localTextView, "rightHeaderText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void f(String paramString)
  {
    k.b(paramString, "title");
    TextView localTextView = (TextView)b.b();
    k.a(localTextView, "titleText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */