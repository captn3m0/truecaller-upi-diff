package com.truecaller.truepay.app.ui.homescreen.views.e;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import com.truecaller.adapter_delegates.p;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.e.b;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.f.b;

public final class a
  extends RecyclerView.ViewHolder
  implements f.b
{
  private final c.f b;
  private final p c;
  private final com.truecaller.adapter_delegates.f d;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(a.class);
    ((u)localObject).<init>(localb, "recycleView", "getRecycleView()Landroid/support/v7/widget/RecyclerView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public a(View paramView, e.b paramb)
  {
    super(paramView);
    int i = R.id.payHomePromoList;
    Object localObject1 = com.truecaller.utils.extensions.t.a(paramView, i);
    b = ((c.f)localObject1);
    localObject1 = new com/truecaller/adapter_delegates/p;
    paramb = (com.truecaller.adapter_delegates.b)paramb;
    int j = R.layout.item_promo_banner;
    Object localObject2 = new com/truecaller/truepay/app/ui/homescreen/views/e/a$a;
    ((a.a)localObject2).<init>(this);
    localObject2 = (c.g.a.b)localObject2;
    c.g.a.b localb = (c.g.a.b)a.b.a;
    ((p)localObject1).<init>(paramb, j, (c.g.a.b)localObject2, localb);
    c = ((p)localObject1);
    paramb = new com/truecaller/adapter_delegates/f;
    localObject1 = (com.truecaller.adapter_delegates.a)c;
    paramb.<init>((com.truecaller.adapter_delegates.a)localObject1);
    paramb.setHasStableIds(true);
    d = paramb;
    paramb = a();
    k.a(paramb, "recycleView");
    localObject1 = new android/support/v7/widget/LinearLayoutManager;
    paramView = paramView.getContext();
    ((LinearLayoutManager)localObject1).<init>(paramView, 0, false);
    localObject1 = (RecyclerView.LayoutManager)localObject1;
    paramb.setLayoutManager((RecyclerView.LayoutManager)localObject1);
    paramView = a();
    k.a(paramView, "recycleView");
    paramb = (RecyclerView.Adapter)d;
    paramView.setAdapter(paramb);
  }
  
  private final RecyclerView a()
  {
    return (RecyclerView)b.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */