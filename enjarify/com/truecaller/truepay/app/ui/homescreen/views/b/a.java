package com.truecaller.truepay.app.ui.homescreen.views.b;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import c.f;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.c.b;

public final class a
  extends RecyclerView.ViewHolder
  implements c.b
{
  private final f b;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(a.class);
    ((u)localObject).<init>(localb, "pspImage", "getPspImage()Landroid/widget/ImageView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public a(View paramView)
  {
    super(paramView);
    int i = R.id.pspLogo;
    paramView = com.truecaller.utils.extensions.t.a(paramView, i);
    b = paramView;
  }
  
  public final void a(Drawable paramDrawable)
  {
    k.b(paramDrawable, "drawable");
    ((ImageView)b.b()).setImageDrawable(paramDrawable);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */