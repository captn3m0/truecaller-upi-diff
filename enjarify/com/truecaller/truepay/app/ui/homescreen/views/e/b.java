package com.truecaller.truepay.app.ui.homescreen.views.e;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import com.d.b.ab;
import com.d.b.w;
import com.truecaller.adapter_delegates.i;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.e.c;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class b
  extends RecyclerView.ViewHolder
  implements e.c, a
{
  private final Drawable a;
  private final View b;
  private HashMap c;
  
  public b(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    b = paramView;
    paramView = b.getContext();
    int i = R.drawable.ic_placeholder_home_promo;
    paramView = android.support.v4.content.b.a(paramView, i);
    a = paramView;
    paramView = b;
    Object localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, 12);
  }
  
  public final View a()
  {
    return b;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    Object localObject = "bannerId";
    c.g.b.k.b(paramString2, (String)localObject);
    if (paramString1 != null)
    {
      paramString1 = w.a(b.getContext()).a(paramString1);
      paramString2 = a;
      paramString1 = paramString1.b(paramString2);
      paramString2 = a;
      paramString1 = paramString1.a(paramString2);
      int i = R.id.imageItemHomeBanner;
      localObject = c;
      if (localObject == null)
      {
        localObject = new java/util/HashMap;
        ((HashMap)localObject).<init>();
        c = ((HashMap)localObject);
      }
      localObject = c;
      Integer localInteger = Integer.valueOf(i);
      localObject = (View)((HashMap)localObject).get(localInteger);
      localInteger = null;
      if (localObject == null)
      {
        localObject = a();
        if (localObject == null)
        {
          localObject = null;
        }
        else
        {
          localObject = ((View)localObject).findViewById(i);
          HashMap localHashMap = c;
          paramString2 = Integer.valueOf(i);
          localHashMap.put(paramString2, localObject);
        }
      }
      localObject = (ImageView)localObject;
      paramString1.a((ImageView)localObject, null);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */