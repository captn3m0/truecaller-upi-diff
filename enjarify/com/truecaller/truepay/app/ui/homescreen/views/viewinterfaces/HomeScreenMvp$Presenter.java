package com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces;

import com.truecaller.truepay.app.ui.homescreen.core.base.RoutedLifecycleAwarePresenter;

public abstract interface HomeScreenMvp$Presenter
  extends RoutedLifecycleAwarePresenter, a.a, b.b.a, d.a, d.c.a, e.a, f.b.a, g.a, h.b.a
{
  public abstract boolean a(int paramInt);
  
  public abstract void b();
  
  public abstract void e();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.HomeScreenMvp.Presenter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */