package com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces;

import android.graphics.drawable.Drawable;

public abstract interface a$c
{
  public abstract void a(Drawable paramDrawable);
  
  public abstract void a(String paramString);
  
  public abstract void b(Drawable paramDrawable);
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */