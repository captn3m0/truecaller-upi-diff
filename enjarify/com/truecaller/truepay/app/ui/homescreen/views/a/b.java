package com.truecaller.truepay.app.ui.homescreen.views.a;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.adapter_delegates.i;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.a.c;
import com.truecaller.truepay.app.utils.af;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class b
  extends RecyclerView.ViewHolder
  implements a.c, a
{
  private final View a;
  private HashMap b;
  
  public b(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    a = paramView;
    paramView = a;
    Object localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, 12);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final View a()
  {
    return a;
  }
  
  public final void a(Drawable paramDrawable)
  {
    c.g.b.k.b(paramDrawable, "image");
    int i = R.id.imageBankIcon;
    ((ImageView)a(i)).setImageDrawable(paramDrawable);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "accountNumber");
    int i = R.id.textAccountNumber;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "textAccountNumber");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(Drawable paramDrawable)
  {
    c.g.b.k.b(paramDrawable, "image");
    int i = R.id.textAccountNumber;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "textAccountNumber");
    af.a(localTextView, paramDrawable);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "name");
    int i = R.id.textAccountName;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "textAccountName");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "buttonText");
    int i = R.id.buttonBalanceCheck;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "buttonBalanceCheck");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */