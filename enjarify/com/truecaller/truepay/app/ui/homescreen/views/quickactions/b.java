package com.truecaller.truepay.app.ui.homescreen.views.quickactions;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.b.ab;
import com.d.b.w;
import com.truecaller.adapter_delegates.i;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.homescreen.views.viewinterfaces.g.c;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class b
  extends RecyclerView.ViewHolder
  implements g.c, a
{
  private final View a;
  private final Drawable b;
  private HashMap c;
  
  public b(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    a = paramView;
    paramView = a.getContext();
    int i = R.drawable.ic_pay_place_holder_round;
    paramView = android.support.v4.content.b.a(paramView, i);
    b = paramView;
    paramView = a;
    Object localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, 12);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final View a()
  {
    return a;
  }
  
  public final void a(String paramString)
  {
    if (paramString != null)
    {
      paramString = w.a(a.getContext()).a(paramString);
      Object localObject = b;
      paramString = paramString.b((Drawable)localObject);
      localObject = b;
      paramString = paramString.a((Drawable)localObject);
      int i = R.id.imageItemHomeBanner;
      localObject = (AppCompatImageView)a(i);
      paramString.a((ImageView)localObject, null);
      return;
    }
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "name");
    int i = R.id.quickActionItemText;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "quickActionItemText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.views.quickactions.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */