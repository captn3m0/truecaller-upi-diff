package com.truecaller.truepay.app.ui.registrationv2.a;

import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceRequest;
import com.truecaller.truepay.app.ui.registrationv2.data.VerifyBindingRequest;
import e.b;

public abstract interface d
{
  public abstract b a(CheckDeviceRequest paramCheckDeviceRequest);
  
  public abstract b a(VerifyBindingRequest paramVerifyBindingRequest);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */