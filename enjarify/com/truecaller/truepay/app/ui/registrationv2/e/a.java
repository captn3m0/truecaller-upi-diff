package com.truecaller.truepay.app.ui.registrationv2.e;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.truepay.app.ui.registrationv2.d.a.b;
import com.truecaller.truepay.app.ui.registrationv2.data.BankResponse;
import java.util.List;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class a
  extends ba
  implements com.truecaller.truepay.app.ui.registrationv2.d.a.a
{
  private BankResponse c;
  private bn d;
  private final f e;
  private final f f;
  
  public a(f paramf1, f paramf2)
  {
    super(paramf2);
    e = paramf1;
    f = paramf2;
  }
  
  public final void a()
  {
    a.b localb = (a.b)b;
    if (localb != null)
    {
      localb.g();
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "query");
    Object localObject = d;
    if (localObject != null) {
      ((bn)localObject).n();
    }
    int i = paramString.length();
    if (i == 0)
    {
      paramString = (a.b)b;
      if (paramString != null)
      {
        paramString.e();
        paramString.f();
        localObject = c;
        if (localObject != null)
        {
          localObject = ((BankResponse)localObject).getBanks();
          if (localObject != null)
          {
            paramString.c((List)localObject);
            return;
          }
        }
        return;
      }
      return;
    }
    localObject = (a.b)b;
    if (localObject != null)
    {
      ((a.b)localObject).c();
      ((a.b)localObject).d();
    }
    localObject = new com/truecaller/truepay/app/ui/registrationv2/e/a$a;
    ((a.a)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    paramString = e.b(this, null, (m)localObject, 3);
    d = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */