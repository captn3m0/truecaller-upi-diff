package com.truecaller.truepay.app.ui.registrationv2.e;

import c.d.a.a;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.app.ui.registrationv2.d.b.c;
import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class c$d
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private ag f;
  
  c$d(c paramc, b.c paramc1, c.d.c paramc2)
  {
    super(2, paramc2);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/truepay/app/ui/registrationv2/e/c$d;
    c localc = d;
    b.c localc1 = e;
    locald.<init>(localc, localc1, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = c;
    Object localObject1;
    int j;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label407;
      }
      throw a;
    case 2: 
      localObject1 = (ag)a;
      j = paramObject instanceof o.b;
      if (j == 0) {
        break label346;
      }
      throw a;
    case 1: 
      localObject1 = (ag)a;
      j = paramObject instanceof o.b;
      if (j != 0) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label411;
      }
      paramObject = f;
      localObject1 = c.a(d);
      if (localObject1 == null)
      {
        localObject1 = d;
        a = paramObject;
        j = 1;
        c = j;
        localObject2 = c;
        Object localObject3 = new com/truecaller/truepay/app/ui/registrationv2/e/c$a;
        ((c.a)localObject3).<init>((c)localObject1, null);
        localObject3 = (m)localObject3;
        localObject1 = g.a((f)localObject2, (m)localObject3, this);
        if (localObject1 == locala) {
          return locala;
        }
      }
      localObject1 = paramObject;
    }
    paramObject = c.a(d);
    int k;
    if (paramObject != null)
    {
      j = ((CheckDeviceResponse)paramObject).getDevice_id_match();
      if (j != 0)
      {
        j = ((CheckDeviceResponse)paramObject).getSim_id_match();
        if (j != 0)
        {
          localObject2 = e;
          ((b.c)localObject2).a((CheckDeviceResponse)paramObject);
          break label346;
        }
      }
      localObject2 = c.b(d);
      if (localObject2 == null)
      {
        localObject2 = d;
        a = localObject1;
        b = paramObject;
        k = 2;
        c = k;
        paramObject = ((c)localObject2).a(this);
        if (paramObject == locala) {
          return locala;
        }
      }
    }
    else
    {
      paramObject = d;
      c.c((c)paramObject);
    }
    label346:
    paramObject = c.b(d);
    if (paramObject != null)
    {
      localObject2 = d;
      a = localObject1;
      b = paramObject;
      k = 3;
      c = k;
      paramObject = ((c)localObject2).b(this);
      if (paramObject == locala) {
        return locala;
      }
    }
    else
    {
      paramObject = d;
      c.c((c)paramObject);
    }
    label407:
    return x.a;
    label411:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */