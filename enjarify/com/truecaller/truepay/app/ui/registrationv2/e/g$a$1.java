package com.truecaller.truepay.app.ui.registrationv2.e;

import c.d.a.a;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.registrationv2.d.e.b;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;

final class g$a$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  g$a$1(g.a parama, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/truepay/app/ui/registrationv2/e/g$a$1;
    g.a locala = b;
    local1.<init>(locala, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label352;
      }
      paramObject = g.k(b.b);
      Object localObject1 = "j<@$f)qntd=?5e!y";
      int k = 105;
      Object localObject2 = Integer.valueOf(k);
      ((com.truecaller.truepay.data.e.c)paramObject).a((String)localObject1, (Integer)localObject2);
      paramObject = g.c(b.b);
      if (paramObject != null)
      {
        ((e.b)paramObject).c("lottie_failed_reg.json");
        localObject1 = g.h(b.b);
        k = R.string.pay_device_verification_failed_text;
        Object[] arrayOfObject1 = new Object[0];
        localObject1 = ((n)localObject1).a(k, arrayOfObject1);
        c.g.b.k.a(localObject1, "resourceProvider.getStri…verification_failed_text)");
        ((e.b)paramObject).a((String)localObject1);
        localObject1 = g.h(b.b);
        k = R.color.pay_failure;
        int j = ((n)localObject1).d(k);
        ((e.b)paramObject).a(j);
        localObject1 = g.h(b.b);
        k = R.string.pay_sms_normal_charges_apply_sub_text;
        Object[] arrayOfObject2 = new Object[0];
        localObject1 = ((n)localObject1).a(k, arrayOfObject2);
        localObject2 = "resourceProvider.getStri…l_charges_apply_sub_text)";
        c.g.b.k.a(localObject1, (String)localObject2);
        ((e.b)paramObject).b((String)localObject1);
      }
      paramObject = g.a(b.b);
      localObject1 = new com/truecaller/truepay/app/ui/registrationv2/e/g$a$1$1;
      ((g.a.1.1)localObject1).<init>(null);
      localObject1 = (m)localObject1;
      k = 1;
      a = k;
      paramObject = kotlinx.coroutines.g.a((f)paramObject, (m)localObject1, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = g.c(b.b);
    if (paramObject != null)
    {
      ((e.b)paramObject).b();
      return x.a;
    }
    return null;
    label352:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.g.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */