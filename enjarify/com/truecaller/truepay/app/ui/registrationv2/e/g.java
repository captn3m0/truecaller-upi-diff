package com.truecaller.truepay.app.ui.registrationv2.e;

import android.app.PendingIntent;
import android.os.CountDownTimer;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.bc;
import com.truecaller.featuretoggles.b;
import com.truecaller.log.d;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.registrationv2.b.h;
import com.truecaller.truepay.app.ui.registrationv2.d.e.a;
import com.truecaller.truepay.app.ui.registrationv2.d.e.b;
import com.truecaller.truepay.app.ui.registrationv2.data.SmsManagerResponse;
import com.truecaller.truepay.app.ui.registrationv2.f.a;
import com.truecaller.truepay.data.e.c;
import com.truecaller.utils.n;
import java.util.concurrent.TimeUnit;

public final class g
  extends bc
  implements e.a
{
  private CountDownTimer d;
  private int e;
  private Boolean f;
  private final c.d.f g;
  private final c.d.f h;
  private final h i;
  private final a j;
  private final n k;
  private final c l;
  private final com.truecaller.truepay.f m;
  private final com.truecaller.featuretoggles.e n;
  
  public g(c.d.f paramf1, c.d.f paramf2, h paramh, a parama, n paramn, c paramc, com.truecaller.truepay.f paramf, com.truecaller.featuretoggles.e parame)
  {
    super(paramf1);
    g = paramf1;
    h = paramf2;
    i = paramh;
    j = parama;
    k = paramn;
    l = paramc;
    m = paramf;
    n = parame;
    e = 45;
  }
  
  private final void g()
  {
    CountDownTimer localCountDownTimer = d;
    if (localCountDownTimer != null) {
      localCountDownTimer.cancel();
    }
    d = null;
  }
  
  public final void a()
  {
    Object localObject = new com/truecaller/truepay/app/ui/registrationv2/e/g$c;
    ((g.c)localObject).<init>(this, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
  
  public final void a(int paramInt)
  {
    Object localObject = d;
    if (localObject != null) {
      return;
    }
    localObject = new com/truecaller/truepay/app/ui/registrationv2/e/g$g;
    ((g.g)localObject).<init>(this, paramInt, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
  
  public final void a(SmsManagerResponse paramSmsManagerResponse)
  {
    k.b(paramSmsManagerResponse, "smsManagerResponse");
    Object localObject1 = Boolean.FALSE;
    f = ((Boolean)localObject1);
    localObject1 = TimeUnit.SECONDS;
    long l1 = paramSmsManagerResponse.getCountDownTimer();
    long l2 = ((TimeUnit)localObject1).toMillis(l1);
    int i1 = (int)l2;
    e = i1;
    int i2 = e;
    a(i2);
    localObject1 = (e.b)b;
    if (localObject1 != null)
    {
      Object localObject2 = k;
      int i3 = R.string.pay_sending_sms_text;
      Object localObject3 = new Object[0];
      localObject2 = ((n)localObject2).a(i3, (Object[])localObject3);
      String str1 = "resourceProvider.getStri…ing.pay_sending_sms_text)";
      k.a(localObject2, str1);
      ((e.b)localObject1).a((String)localObject2);
      localObject3 = j;
      String str2 = paramSmsManagerResponse.getAddress();
      String str3 = paramSmsManagerResponse.getMessage();
      PendingIntent localPendingIntent = ((e.b)localObject1).c();
      localObject2 = n.E();
      boolean bool = ((b)localObject2).a();
      if (bool)
      {
        localObject1 = ((e.b)localObject1).d();
      }
      else
      {
        i2 = 0;
        localObject1 = null;
      }
      String str4 = paramSmsManagerResponse.getSimToken();
      ((a)localObject3).a(str2, null, str3, localPendingIntent, (PendingIntent)localObject1, str4);
      return;
    }
  }
  
  public final void b()
  {
    Object localObject = new com/truecaller/truepay/app/ui/registrationv2/e/g$a;
    ((g.a)localObject).<init>(this, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
  
  public final void c()
  {
    Object localObject = new com/truecaller/truepay/app/ui/registrationv2/e/g$h;
    ((g.h)localObject).<init>(this, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
  
  public final void e()
  {
    Object localObject = n.E();
    boolean bool = ((b)localObject).a();
    if (bool) {
      return;
    }
    localObject = Boolean.TRUE;
    f = ((Boolean)localObject);
    g();
    try
    {
      localObject = new com/truecaller/truepay/app/ui/registrationv2/e/g$e;
      ((g.e)localObject).<init>(this, null);
      localObject = (m)localObject;
      int i1 = 3;
      kotlinx.coroutines.e.b(this, null, (m)localObject, i1);
      return;
    }
    catch (Exception localException)
    {
      localObject = new java/lang/AssertionError;
      ((AssertionError)localObject).<init>("Error onSmsSendingSuccess");
      d.a((Throwable)localObject);
    }
  }
  
  public final void f()
  {
    Object localObject = Boolean.TRUE;
    f = ((Boolean)localObject);
    g();
    try
    {
      localObject = new com/truecaller/truepay/app/ui/registrationv2/e/g$d;
      ((g.d)localObject).<init>(this, null);
      localObject = (m)localObject;
      int i1 = 3;
      kotlinx.coroutines.e.b(this, null, (m)localObject, i1);
      return;
    }
    catch (Exception localException)
    {
      localObject = new java/lang/AssertionError;
      ((AssertionError)localObject).<init>("Error onSmsSendingSuccess");
      d.a((Throwable)localObject);
    }
  }
  
  public final void y_()
  {
    super.y_();
    CountDownTimer localCountDownTimer = d;
    if (localCountDownTimer != null)
    {
      localCountDownTimer.cancel();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */