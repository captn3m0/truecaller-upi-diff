package com.truecaller.truepay.app.ui.registrationv2.e;

import c.g.b.k;
import com.truecaller.bb;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.registrationv2.d.c.a;
import com.truecaller.truepay.app.ui.registrationv2.d.c.b;
import com.truecaller.utils.n;

public final class e
  extends bb
  implements c.a
{
  private final n a;
  
  public e(n paramn)
  {
    a = paramn;
  }
  
  public final void a()
  {
    c.b localb = (c.b)b;
    if (localb != null)
    {
      Object localObject1 = a;
      int i = R.string.pay_registration_exit_popup_header;
      Object localObject2 = new Object[0];
      localObject1 = ((n)localObject1).a(i, (Object[])localObject2);
      k.a(localObject1, "resourceProvider.getStri…ration_exit_popup_header)");
      Object localObject3 = a;
      int j = R.string.pay_registration_exit_popup_text;
      Object localObject4 = new Object[0];
      localObject3 = ((n)localObject3).a(j, (Object[])localObject4);
      k.a(localObject3, "resourceProvider.getStri…stration_exit_popup_text)");
      localObject2 = a;
      int k = R.string.pay_registration_exit_popup_positive_button;
      Object[] arrayOfObject = new Object[0];
      localObject2 = ((n)localObject2).a(k, arrayOfObject);
      k.a(localObject2, "resourceProvider.getStri…it_popup_positive_button)");
      localObject4 = a;
      int m = R.string.pay_registration_exit_popup_negative_button;
      Object localObject5 = new Object[0];
      localObject5 = ((n)localObject4).a(m, (Object[])localObject5);
      k.a(localObject5, "resourceProvider.getStri…it_popup_negative_button)");
      localb.a((String)localObject1, (String)localObject3, (String)localObject2, (String)localObject5);
      return;
    }
  }
  
  public final void b()
  {
    c.b localb = (c.b)b;
    if (localb != null)
    {
      localb.a();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */