package com.truecaller.truepay.app.ui.registrationv2.e;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.registrationv2.d.e.b;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;

final class g$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  g$b(g paramg, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/truepay/app/ui/registrationv2/e/g$b;
    g localg = b;
    localb.<init>(localg, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label322;
      }
      paramObject = g.g(b);
      if (paramObject != null)
      {
        paramObject = g.g(b);
        localObject2 = Boolean.TRUE;
        boolean bool3 = c.g.b.k.a(paramObject, localObject2);
        if (!bool3)
        {
          paramObject = g.c(b);
          if (paramObject == null) {
            break;
          }
          ((e.b)paramObject).c("lottie_failed_reg.json");
          localObject1 = g.h(b);
          int j = R.string.pay_sms_failed_text;
          m = 0;
          Object[] arrayOfObject1 = new Object[0];
          localObject1 = ((n)localObject1).a(j, arrayOfObject1);
          c.g.b.k.a(localObject1, "resourceProvider.getStri…ring.pay_sms_failed_text)");
          ((e.b)paramObject).a((String)localObject1);
          localObject1 = g.h(b);
          j = R.color.pay_failure;
          int k = ((n)localObject1).d(j);
          ((e.b)paramObject).a(k);
          localObject1 = g.h(b);
          j = R.string.pay_sms_failed_sub_text;
          arrayOfObject2 = new Object[0];
          localObject1 = ((n)localObject1).a(j, arrayOfObject2);
          localObject2 = "resourceProvider.getStri….pay_sms_failed_sub_text)";
          c.g.b.k.a(localObject1, (String)localObject2);
          ((e.b)paramObject).b((String)localObject1);
          break;
        }
      }
      g.d(b);
      paramObject = g.a(b);
      Object localObject2 = new com/truecaller/truepay/app/ui/registrationv2/e/g$b$1;
      Object[] arrayOfObject2 = null;
      ((g.b.1)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      int m = 1;
      a = m;
      paramObject = kotlinx.coroutines.g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    return x.a;
    label322:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.g.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */