package com.truecaller.truepay.app.ui.registrationv2.e;

import c.d.a.a;
import c.o.b;
import c.x;
import com.truecaller.truepay.SmsBankData;
import com.truecaller.truepay.app.ui.registrationv2.data.BankData;
import com.truecaller.truepay.app.ui.registrationv2.data.BankResponse;
import java.util.Iterator;
import java.util.LinkedHashMap;

final class c$f
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private kotlinx.coroutines.ag c;
  
  c$f(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/truepay/app/ui/registrationv2/e/c$f;
    c localc = b;
    localf.<init>(localc, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    c = ((kotlinx.coroutines.ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = new java/util/LinkedHashMap;
        ((LinkedHashMap)paramObject).<init>();
        localObject1 = c.i(b).c();
        if (localObject1 != null)
        {
          localObject1 = ((Iterable)localObject1).iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject1).hasNext();
            if (!bool2) {
              break;
            }
            Object localObject2 = (SmsBankData)((Iterator)localObject1).next();
            Object localObject3 = c.b(b);
            if (localObject3 != null)
            {
              localObject3 = ((BankResponse)localObject3).getBanks();
              if (localObject3 != null)
              {
                localObject3 = ((Iterable)localObject3).iterator();
                boolean bool4;
                do
                {
                  bool3 = ((Iterator)localObject3).hasNext();
                  if (!bool3) {
                    break;
                  }
                  localObject4 = ((Iterator)localObject3).next();
                  Object localObject5 = localObject4;
                  localObject5 = ((BankData)localObject4).getBank_symbol();
                  c.g.b.k.a(localObject2, "info");
                  String str = ((SmsBankData)localObject2).getBankSymbol();
                  bool4 = c.g.b.k.a(localObject5, str);
                } while (!bool4);
                break label191;
                boolean bool3 = false;
                Object localObject4 = null;
                label191:
                localObject4 = (BankData)localObject4;
                if (localObject4 != null)
                {
                  localObject3 = "info";
                  c.g.b.k.a(localObject2, (String)localObject3);
                  k = ((SmsBankData)localObject2).getSimSlotIndex();
                  ((BankData)localObject4).setSim_index(k);
                  if (localObject4 != null)
                  {
                    localObject2 = ((BankData)localObject4).getBank_symbol();
                    ((LinkedHashMap)paramObject).put(localObject2, localObject4);
                  }
                }
              }
            }
          }
        }
        int j = ((LinkedHashMap)paramObject).size();
        int k = 2;
        if (j > 0)
        {
          paramObject = ((LinkedHashMap)paramObject).values();
          c.g.b.k.a(paramObject, "bankLists.values");
          return c.a.m.d((Iterable)paramObject, k);
        }
        paramObject = c.b(b);
        if (paramObject != null)
        {
          paramObject = ((BankResponse)paramObject).getPopular_banks();
          if (paramObject != null) {
            return c.a.m.d((Iterable)paramObject, k);
          }
        }
        return null;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.c.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */