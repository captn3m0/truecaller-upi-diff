package com.truecaller.truepay.app.ui.registrationv2.e;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.truepay.app.ui.registrationv2.a.g;
import com.truecaller.truepay.app.ui.registrationv2.b.l;
import com.truecaller.truepay.app.ui.registrationv2.b.l.a;
import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse;
import com.truecaller.truepay.app.ui.registrationv2.data.j;
import kotlinx.coroutines.ag;

final class c$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  c$a(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/truepay/app/ui/registrationv2/e/c$a;
    c localc = c;
    locala.<init>(localc, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    boolean bool;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      bool = paramObject instanceof o.b;
      if (!bool) {
        break label185;
      }
      throw a;
    case 1: 
      j = paramObject instanceof o.b;
      if (j != 0) {
        throw a;
      }
      break;
    case 0: 
      j = paramObject instanceof o.b;
      if (j != 0) {
        break label274;
      }
      paramObject = c.d(c);
      j = 1;
      b = j;
      paramObject = l.a.a((l)paramObject, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (j)paramObject;
    Object localObject2 = a;
    int j = localObject2 instanceof g;
    if (j != 0)
    {
      localObject2 = c.e(c);
      a = paramObject;
      int k = 2;
      b = k;
      paramObject = l.a.a((l)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label185:
      paramObject = (j)paramObject;
      localObject1 = a;
      bool = localObject1 instanceof g;
      if (bool)
      {
        localObject1 = c;
        paramObject = a;
        if (paramObject != null)
        {
          paramObject = a;
          if (paramObject != null)
          {
            paramObject = (CheckDeviceResponse)paramObject;
            c.a((c)localObject1, (CheckDeviceResponse)paramObject);
            return x.a;
          }
          paramObject = new c/u;
          ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse");
          throw ((Throwable)paramObject);
        }
        paramObject = new c/u;
        ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registrationv2.api.Success<kotlin.Any>");
        throw ((Throwable)paramObject);
      }
    }
    return x.a;
    label274:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */