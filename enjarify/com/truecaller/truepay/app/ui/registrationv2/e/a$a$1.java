package com.truecaller.truepay.app.ui.registrationv2.e;

import c.d.c;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.truepay.app.ui.registrationv2.data.BankData;
import com.truecaller.truepay.app.ui.registrationv2.data.BankResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class a$a$1
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag c;
  
  a$a$1(a.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/truepay/app/ui/registrationv2/e/a$a$1;
    a.a locala = b;
    local1.<init>(locala, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = a.b(b.b);
        if (paramObject != null)
        {
          paramObject = ((BankResponse)paramObject).getBanks();
          if (paramObject != null)
          {
            paramObject = (Iterable)paramObject;
            localObject1 = new java/util/ArrayList;
            ((ArrayList)localObject1).<init>();
            localObject1 = (Collection)localObject1;
            paramObject = ((Iterable)paramObject).iterator();
            for (;;)
            {
              boolean bool2 = ((Iterator)paramObject).hasNext();
              if (!bool2) {
                break label221;
              }
              Object localObject2 = ((Iterator)paramObject).next();
              Object localObject3 = localObject2;
              localObject3 = ((BankData)localObject2).getName();
              if (localObject3 == null) {
                break label209;
              }
              localObject3 = ((String)localObject3).toLowerCase();
              c.g.b.k.a(localObject3, "(this as java.lang.String).toLowerCase()");
              localObject3 = (CharSequence)localObject3;
              Object localObject4 = b.c;
              if (localObject4 == null) {
                break;
              }
              localObject4 = ((String)localObject4).toLowerCase();
              String str = "(this as java.lang.String).toLowerCase()";
              c.g.b.k.a(localObject4, str);
              localObject4 = (CharSequence)localObject4;
              boolean bool3 = c.n.m.b((CharSequence)localObject3, (CharSequence)localObject4);
              if (bool3) {
                ((Collection)localObject1).add(localObject2);
              }
            }
            paramObject = new c/u;
            ((u)paramObject).<init>("null cannot be cast to non-null type java.lang.String");
            throw ((Throwable)paramObject);
            label209:
            paramObject = new c/u;
            ((u)paramObject).<init>("null cannot be cast to non-null type java.lang.String");
            throw ((Throwable)paramObject);
            label221:
            return (List)localObject1;
          }
        }
        return null;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.a.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */