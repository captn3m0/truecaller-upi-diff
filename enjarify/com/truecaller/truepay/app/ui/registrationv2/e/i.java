package com.truecaller.truepay.app.ui.registrationv2.e;

import android.graphics.drawable.Drawable;
import c.d.f;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.multisim.h;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.app.ui.registrationv2.d.f.a;
import com.truecaller.truepay.app.ui.registrationv2.d.f.b;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.n;

public final class i
  extends ba
  implements f.a
{
  private int c;
  private final f d;
  private final n e;
  private final h f;
  private final r g;
  
  public i(f paramf, n paramn, h paramh, r paramr)
  {
    super(paramf);
    d = paramf;
    e = paramn;
    f = paramh;
    g = paramr;
  }
  
  public final void a()
  {
    c = 0;
    f.b localb = (f.b)b;
    if (localb != null)
    {
      Object localObject = e;
      int i = R.drawable.ic_pay_sim_selector_active;
      localObject = ((n)localObject).c(i);
      k.a(localObject, "resourceProvider.getDraw…_pay_sim_selector_active)");
      localb.c((Drawable)localObject);
      localObject = e;
      i = R.drawable.ic_pay_sim_selector_normal;
      localObject = ((n)localObject).c(i);
      k.a(localObject, "resourceProvider.getDraw…_pay_sim_selector_normal)");
      localb.d((Drawable)localObject);
      return;
    }
  }
  
  public final void b()
  {
    int i = 1;
    c = i;
    f.b localb = (f.b)b;
    if (localb != null)
    {
      Object localObject = e;
      int j = R.drawable.ic_pay_sim_selector_normal;
      localObject = ((n)localObject).c(j);
      k.a(localObject, "resourceProvider.getDraw…_pay_sim_selector_normal)");
      localb.c((Drawable)localObject);
      localObject = e;
      j = R.drawable.ic_pay_sim_selector_active;
      localObject = ((n)localObject).c(j);
      k.a(localObject, "resourceProvider.getDraw…_pay_sim_selector_active)");
      localb.d((Drawable)localObject);
      return;
    }
  }
  
  public final void c()
  {
    f.b localb = (f.b)b;
    if (localb != null)
    {
      int i = c;
      localb.a(i);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */