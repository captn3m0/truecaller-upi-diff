package com.truecaller.truepay.app.ui.registrationv2.e;

import c.g.a.m;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.truepay.app.ui.registrationv2.a.g;
import com.truecaller.truepay.app.ui.registrationv2.b.l;
import com.truecaller.truepay.app.ui.registrationv2.b.l.a;
import com.truecaller.truepay.app.ui.registrationv2.data.BankResponse;
import com.truecaller.truepay.app.ui.registrationv2.data.j;
import java.util.List;
import kotlinx.coroutines.ag;

final class c$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  c$c(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/truepay/app/ui/registrationv2/e/c$c;
    c localc1 = c;
    localc.<init>(localc1, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = b;
    boolean bool2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      j = paramObject instanceof o.b;
      if (j != 0) {
        break label366;
      }
      paramObject = c.f(c);
      localList1 = ((com.truecaller.truepay.app.ui.registrationv2.data.a.a)paramObject).a();
      bool2 = localList1.isEmpty();
      if (!bool2) {
        break label320;
      }
      paramObject = c.g(c);
      a = localList1;
      j = 1;
      b = j;
      paramObject = l.a.a((l)paramObject, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (j)paramObject;
    localObject1 = c;
    Object localObject2 = a;
    int j = localObject2 instanceof g;
    List localList1 = null;
    if (j != 0)
    {
      paramObject = a;
      if (paramObject != null)
      {
        paramObject = a;
        if (paramObject != null)
        {
          paramObject = (BankResponse)paramObject;
        }
        else
        {
          paramObject = new c/u;
          ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registrationv2.data.BankResponse");
          throw ((Throwable)paramObject);
        }
      }
      else
      {
        paramObject = new c/u;
        ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registrationv2.api.Success<kotlin.Any>");
        throw ((Throwable)paramObject);
      }
    }
    else
    {
      bool2 = false;
      paramObject = null;
    }
    c.a((c)localObject1, (BankResponse)paramObject);
    paramObject = c.b(c);
    if (paramObject != null)
    {
      paramObject = ((BankResponse)paramObject).getBanks();
      if (paramObject != null)
      {
        c.f(c).a((List)paramObject);
        paramObject = c.h(c);
        localObject1 = "dfr$)qtd=?de!w";
        localObject2 = c.b(c);
        if (localObject2 != null)
        {
          localObject2 = ((BankResponse)localObject2).getLogo_base_url();
          if (localObject2 != null) {}
        }
        else
        {
          localObject2 = "";
        }
        ((com.truecaller.truepay.data.e.c)paramObject).b((String)localObject1, (String)localObject2);
        return x.a;
      }
    }
    return null;
    label320:
    List localList2 = c.f(c).b();
    paramObject = c;
    localObject1 = new com/truecaller/truepay/app/ui/registrationv2/data/BankResponse;
    localObject2 = localObject1;
    ((BankResponse)localObject1).<init>(localList1, localList2, null, 4, null);
    c.a((c)paramObject, (BankResponse)localObject1);
    return x.a;
    label366:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */