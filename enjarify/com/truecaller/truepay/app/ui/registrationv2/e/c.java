package com.truecaller.truepay.app.ui.registrationv2.e;

import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.multisim.SimInfo;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.registrationv2.b.g;
import com.truecaller.truepay.app.ui.registrationv2.b.m;
import com.truecaller.truepay.app.ui.registrationv2.d.b.b;
import com.truecaller.truepay.app.ui.registrationv2.d.b.c;
import com.truecaller.truepay.app.ui.registrationv2.data.BankData;
import com.truecaller.truepay.app.ui.registrationv2.data.BankResponse;
import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse;
import com.truecaller.truepay.data.f.ag;
import com.truecaller.utils.n;

public final class c
  extends ba
  implements b.b
{
  final c.d.f c;
  private BankResponse d;
  private BankData e;
  private int f;
  private CheckDeviceResponse g;
  private final c.d.f h;
  private final n i;
  private final com.truecaller.truepay.app.ui.registrationv2.f.a j;
  private final com.truecaller.truepay.data.e.c k;
  private final com.truecaller.truepay.app.ui.registrationv2.b.f l;
  private final ag m;
  private final m n;
  private final g o;
  private final com.truecaller.truepay.app.ui.registrationv2.data.a.a p;
  
  public c(c.d.f paramf1, c.d.f paramf2, n paramn, com.truecaller.truepay.app.ui.registrationv2.f.a parama, com.truecaller.truepay.data.e.c paramc, com.truecaller.truepay.app.ui.registrationv2.b.f paramf, ag paramag, m paramm, g paramg, com.truecaller.truepay.app.ui.registrationv2.data.a.a parama1)
  {
    super(paramf2);
    c = paramf1;
    h = paramf2;
    i = paramn;
    j = parama;
    k = paramc;
    l = paramf;
    m = paramag;
    n = paramm;
    o = paramg;
    p = parama1;
    f = -1;
  }
  
  private final void e()
  {
    int i1 = f;
    Object localObject1 = null;
    int i2 = -1;
    if (i1 == i2)
    {
      localObject2 = (b.c)b;
      if (localObject2 != null)
      {
        localObject3 = i;
        i3 = R.string.pay_sim_selection_hint_without_sim_selected;
        localObject1 = new Object[0];
        localObject1 = ((n)localObject3).a(i3, (Object[])localObject1);
        localObject3 = "resourceProvider.getStri…int_without_sim_selected)";
        k.a(localObject1, (String)localObject3);
        ((b.c)localObject2).c((String)localObject1);
      }
      return;
    }
    Object localObject2 = j.a(i1);
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    int i3 = f;
    int i4 = 1;
    i3 += i4;
    ((StringBuilder)localObject3).append(i3);
    i3 = 32;
    ((StringBuilder)localObject3).append(i3);
    if (localObject2 != null)
    {
      localObject2 = d;
    }
    else
    {
      i1 = 0;
      localObject2 = null;
    }
    ((StringBuilder)localObject3).append((String)localObject2);
    localObject2 = ((StringBuilder)localObject3).toString();
    localObject3 = (b.c)b;
    if (localObject3 != null)
    {
      n localn = i;
      int i5 = R.string.pay_sim_selection_hint;
      Object[] arrayOfObject = new Object[i4];
      arrayOfObject[0] = localObject2;
      localObject2 = localn.a(i5, arrayOfObject);
      k.a(localObject2, "resourceProvider.getStri…ection_hint, selectedSim)");
      ((b.c)localObject3).c((String)localObject2);
      return;
    }
  }
  
  public final void a()
  {
    Object localObject1 = e;
    if (localObject1 != null)
    {
      Object localObject2 = j;
      boolean bool = ((com.truecaller.truepay.app.ui.registrationv2.f.a)localObject2).j();
      Object localObject3;
      Object localObject4;
      if (!bool)
      {
        localObject2 = k;
        localObject3 = "#1~2c403c$1";
        localObject4 = null;
        Integer localInteger = Integer.valueOf(0);
        ((com.truecaller.truepay.data.e.c)localObject2).a((String)localObject3, localInteger);
        localObject2 = (b.c)b;
        if (localObject2 != null)
        {
          localObject1 = ((BankData)localObject1).getId();
          localObject3 = g;
          ((b.c)localObject2).a((String)localObject1, 0, (CheckDeviceResponse)localObject3);
        }
        return;
      }
      int i1 = f;
      int i2 = -1;
      if (i1 != i2)
      {
        localObject3 = k;
        localObject4 = "#1~2c403c$1";
        localObject2 = Integer.valueOf(i1);
        ((com.truecaller.truepay.data.e.c)localObject3).a((String)localObject4, (Integer)localObject2);
        localObject2 = (b.c)b;
        if (localObject2 != null)
        {
          localObject1 = ((BankData)localObject1).getId();
          i2 = f;
          localObject4 = g;
          ((b.c)localObject2).a((String)localObject1, i2, (CheckDeviceResponse)localObject4);
        }
        return;
      }
      localObject2 = (b.c)b;
      if (localObject2 != null)
      {
        localObject1 = ((BankData)localObject1).getName();
        ((b.c)localObject2).d((String)localObject1);
        return;
      }
      return;
    }
  }
  
  public final void a(int paramInt)
  {
    com.truecaller.truepay.app.ui.registrationv2.f.a locala = j;
    boolean bool = locala.j();
    if (!bool) {
      paramInt = 0;
    }
    f = paramInt;
    e();
  }
  
  public final void a(BankData paramBankData)
  {
    k.b(paramBankData, "bankData");
    e = paramBankData;
    int i1 = paramBankData.getSim_index();
    a(i1);
  }
  
  public final void b()
  {
    BankResponse localBankResponse = d;
    if (localBankResponse != null)
    {
      b.c localc = (b.c)b;
      if (localc != null)
      {
        localc.a(localBankResponse);
        return;
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */