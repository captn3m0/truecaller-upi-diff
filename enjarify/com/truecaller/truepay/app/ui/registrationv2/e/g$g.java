package com.truecaller.truepay.app.ui.registrationv2.e;

import android.os.CountDownTimer;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class g$g
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  g$g(g paramg, int paramInt, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    g localg = new com/truecaller/truepay/app/ui/registrationv2/e/g$g;
    g localg1 = b;
    int i = c;
    localg.<init>(localg1, i, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localg;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        g.d(b);
        paramObject = b;
        localObject = new com/truecaller/truepay/app/ui/registrationv2/e/g$g$1;
        int j = c;
        long l = j;
        ((g.g.1)localObject).<init>(this, l);
        localObject = (CountDownTimer)localObject;
        g.a((g)paramObject, (CountDownTimer)localObject);
        paramObject = g.e(b);
        if (paramObject != null) {
          ((CountDownTimer)paramObject).start();
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (g)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((g)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.e.g.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */