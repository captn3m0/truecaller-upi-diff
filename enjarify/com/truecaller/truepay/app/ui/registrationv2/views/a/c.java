package com.truecaller.truepay.app.ui.registrationv2.views.a;

import android.graphics.drawable.Drawable;
import c.g.b.k;
import com.truecaller.truepay.app.ui.registrationv2.data.BankData;
import com.truecaller.truepay.app.utils.r;

public final class c
  implements b
{
  private int b;
  private final r c;
  
  public c(r paramr)
  {
    c = paramr;
    b = -1;
  }
  
  public final void a(int paramInt)
  {
    b = paramInt;
  }
  
  public final void a(e parame)
  {
    k.b(parame, "bankSelectorItemViewHolder");
    parame.a("Others");
    Drawable localDrawable = c.b("other");
    parame.a(localDrawable);
  }
  
  public final void a(e parame, BankData paramBankData)
  {
    k.b(parame, "bankSelectorItemViewHolder");
    k.b(paramBankData, "bankData");
    Object localObject = paramBankData.getName();
    parame.a((String)localObject);
    localObject = c;
    paramBankData = paramBankData.getBank_symbol();
    paramBankData = ((r)localObject).b(paramBankData);
    parame.a(paramBankData);
    int i = b;
    int j = -1;
    if (i != j)
    {
      parame.a(i);
      return;
    }
    parame.a(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */