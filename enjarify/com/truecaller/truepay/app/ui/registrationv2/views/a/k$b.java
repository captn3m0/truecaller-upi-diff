package com.truecaller.truepay.app.ui.registrationv2.views.a;

import android.view.View;
import android.view.View.OnClickListener;
import c.u;
import com.truecaller.truepay.app.ui.registrationv2.data.BankData;

final class k$b
  implements View.OnClickListener
{
  k$b(k paramk, int paramInt) {}
  
  public final void onClick(View paramView)
  {
    Object localObject = k.a(a);
    int i = b;
    ((l)localObject).a(i);
    localObject = k.b(a);
    if (localObject != null)
    {
      String str = "it";
      c.g.b.k.a(paramView, str);
      paramView = paramView.getTag();
      if (paramView != null)
      {
        paramView = (BankData)paramView;
        ((k.a)localObject).a(paramView);
        return;
      }
      paramView = new c/u;
      paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registrationv2.data.BankData");
      throw paramView;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.a.k.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */