package com.truecaller.truepay.app.ui.registrationv2.views.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import c.g.b.k;
import java.util.List;

public final class a
  extends RecyclerView.Adapter
{
  private final LayoutInflater a;
  private final b b;
  private final List c;
  private final a.a d;
  
  public a(Context paramContext, b paramb, List paramList, a.a parama)
  {
    b = paramb;
    c = paramList;
    d = parama;
    paramContext = LayoutInflater.from(paramContext);
    k.a(paramContext, "LayoutInflater.from(context)");
    a = paramContext;
  }
  
  public final int getItemCount()
  {
    return c.size() + 1;
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */