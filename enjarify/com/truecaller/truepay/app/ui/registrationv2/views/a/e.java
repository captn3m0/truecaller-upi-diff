package com.truecaller.truepay.app.ui.registrationv2.views.a;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class e
  extends RecyclerView.ViewHolder
  implements a
{
  private final View a;
  private HashMap b;
  
  public e(View paramView)
  {
    super(paramView);
    a = paramView;
  }
  
  private View b(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final View a()
  {
    return a;
  }
  
  public final void a(int paramInt)
  {
    int i = getAdapterPosition();
    if (i == paramInt)
    {
      paramInt = R.id.bankIcon;
      localImageView = (ImageView)b(paramInt);
      i = R.drawable.ic_pay_sim_selector_active;
      localImageView.setBackgroundResource(i);
      return;
    }
    paramInt = R.id.bankIcon;
    ImageView localImageView = (ImageView)b(paramInt);
    i = R.drawable.ic_pay_sim_selector_normal;
    localImageView.setBackgroundResource(i);
  }
  
  public final void a(Drawable paramDrawable)
  {
    k.b(paramDrawable, "drawable");
    int i = R.id.bankIcon;
    ((ImageView)b(i)).setImageDrawable(paramDrawable);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.textBankName;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "textBankName");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */