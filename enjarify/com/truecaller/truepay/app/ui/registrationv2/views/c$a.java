package com.truecaller.truepay.app.ui.registrationv2.views;

import android.os.Bundle;
import android.os.Parcelable;
import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse;

public final class c$a
{
  public static c a(CheckDeviceResponse paramCheckDeviceResponse, String paramString, int paramInt)
  {
    c localc = new com/truecaller/truepay/app/ui/registrationv2/views/c;
    localc.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("bank_id", paramString);
    localBundle.putInt("sim_index", paramInt);
    paramCheckDeviceResponse = (Parcelable)paramCheckDeviceResponse;
    localBundle.putParcelable("check_device_resp", paramCheckDeviceResponse);
    localc.setArguments(localBundle);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */