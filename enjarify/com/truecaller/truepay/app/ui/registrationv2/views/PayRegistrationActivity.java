package com.truecaller.truepay.app.ui.registrationv2.views;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources.Theme;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.Window;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registrationv2.c.a.a;
import com.truecaller.truepay.app.ui.registrationv2.d.c.a;
import com.truecaller.truepay.app.ui.registrationv2.d.c.b;
import com.truecaller.truepay.app.ui.registrationv2.d.d;

public final class PayRegistrationActivity
  extends AppCompatActivity
  implements c.b, d
{
  public static final PayRegistrationActivity.a b;
  public c.a a;
  private final ColorDrawable c;
  
  static
  {
    PayRegistrationActivity.a locala = new com/truecaller/truepay/app/ui/registrationv2/views/PayRegistrationActivity$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public PayRegistrationActivity()
  {
    ColorDrawable localColorDrawable = new android/graphics/drawable/ColorDrawable;
    int i = Color.argb(178, 0, 0, 0);
    localColorDrawable.<init>(i);
    c = localColorDrawable;
  }
  
  private final void b(Fragment paramFragment, boolean paramBoolean)
  {
    o localo = getSupportFragmentManager().a();
    if (paramBoolean)
    {
      String str = paramFragment.getClass().getSimpleName();
      localo.a(str);
    }
    paramBoolean = R.id.frame;
    localo.b(paramBoolean, paramFragment);
    localo.d();
    getSupportFragmentManager().b();
  }
  
  public final void a()
  {
    finish();
  }
  
  public final void a(Fragment paramFragment, boolean paramBoolean)
  {
    if (paramFragment != null) {
      b(paramFragment, paramBoolean);
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "headerText");
    k.b(paramString2, "text");
    k.b(paramString3, "positiveButton");
    k.b(paramString4, "negativeButton");
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject1 = new android/support/v7/view/ContextThemeWrapper;
    Object localObject2 = this;
    localObject2 = (Context)this;
    int i = R.style.AppTheme_BlueAccent;
    ((ContextThemeWrapper)localObject1).<init>((Context)localObject2, i);
    localObject1 = (Context)localObject1;
    localBuilder.<init>((Context)localObject1);
    paramString1 = (CharSequence)paramString1;
    paramString1 = localBuilder.setTitle(paramString1);
    paramString2 = (CharSequence)paramString2;
    paramString1 = paramString1.setMessage(paramString2);
    paramString3 = (CharSequence)paramString3;
    paramString2 = new com/truecaller/truepay/app/ui/registrationv2/views/PayRegistrationActivity$b;
    paramString2.<init>(this);
    paramString2 = (DialogInterface.OnClickListener)paramString2;
    paramString1 = paramString1.setPositiveButton(paramString3, paramString2);
    paramString4 = (CharSequence)paramString4;
    paramString2 = (DialogInterface.OnClickListener)PayRegistrationActivity.c.a;
    paramString1 = paramString1.setNegativeButton(paramString4, paramString2);
    paramString2 = null;
    paramString1 = paramString1.setCancelable(false);
    if (paramString1 != null)
    {
      paramString1.show();
      return;
    }
  }
  
  public final void b()
  {
    setResult(-1);
    finish();
  }
  
  public final void c()
  {
    setResult(0);
    finish();
  }
  
  public final void onBackPressed()
  {
    Object localObject = getSupportFragmentManager();
    int i = R.id.frame;
    localObject = ((j)localObject).a(i);
    boolean bool = localObject instanceof c;
    if (bool)
    {
      localObject = a;
      if (localObject == null)
      {
        String str = "presenter";
        k.a(str);
      }
      ((c.a)localObject).a();
      return;
    }
    super.onBackPressed();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getTheme();
    int i = R.style.TransparentTheme1;
    paramBundle.applyStyle(i, false);
    int j = R.layout.activity_pay_registration;
    setContentView(j);
    paramBundle = getWindow();
    Object localObject = (Drawable)c;
    paramBundle.setBackgroundDrawable((Drawable)localObject);
    paramBundle = com.truecaller.truepay.app.ui.registrationv2.c.a.a();
    localObject = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a().a(this);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    paramBundle.a(this);
    paramBundle = b.c;
    paramBundle = new com/truecaller/truepay/app/ui/registrationv2/views/b;
    paramBundle.<init>();
    paramBundle = (Fragment)paramBundle;
    b(paramBundle, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    c.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.PayRegistrationActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */