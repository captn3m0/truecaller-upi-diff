package com.truecaller.truepay.app.ui.registrationv2.views.a;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final Provider a;
  
  private d(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static d a(Provider paramProvider)
  {
    d locald = new com/truecaller/truepay/app/ui/registrationv2/views/a/d;
    locald.<init>(paramProvider);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */