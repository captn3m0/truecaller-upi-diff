package com.truecaller.truepay.app.ui.registrationv2.views;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registrationv2.c.a.a;
import com.truecaller.truepay.app.ui.registrationv2.c.b;
import com.truecaller.truepay.app.ui.registrationv2.d.f.a;
import com.truecaller.truepay.app.ui.registrationv2.d.f.b;
import java.util.HashMap;

public final class d
  extends Fragment
  implements View.OnClickListener, f.b
{
  public static final d.a d;
  public f.a b;
  d.b c;
  private HashMap e;
  
  static
  {
    d.a locala = new com/truecaller/truepay/app/ui/registrationv2/views/d$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  private View b(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final String a()
  {
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "bank_name";
      localObject = ((Bundle)localObject).getString(str);
      if (localObject != null) {}
    }
    else
    {
      localObject = "";
    }
    return (String)localObject;
  }
  
  public final void a(int paramInt)
  {
    d.b localb = c;
    if (localb != null)
    {
      localb.b(paramInt);
      return;
    }
  }
  
  public final void a(Drawable paramDrawable)
  {
    k.b(paramDrawable, "drawable");
    int i = R.id.simOneIcon;
    ((ImageView)b(i)).setImageDrawable(paramDrawable);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.titleText;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "titleText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(Drawable paramDrawable)
  {
    k.b(paramDrawable, "drawable");
    int i = R.id.simTwoIcon;
    ((ImageView)b(i)).setImageDrawable(paramDrawable);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "name");
    int i = R.id.textSimOneName;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "textSimOneName");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c(Drawable paramDrawable)
  {
    k.b(paramDrawable, "drawable");
    int i = R.id.simOneIcon;
    ImageView localImageView = (ImageView)b(i);
    k.a(localImageView, "simOneIcon");
    localImageView.setBackground(paramDrawable);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "name");
    int i = R.id.textSimTwoName;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "textSimTwoName");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void d(Drawable paramDrawable)
  {
    k.b(paramDrawable, "drawable");
    int i = R.id.simTwoIcon;
    ImageView localImageView = (ImageView)b(i);
    k.a(localImageView, "simTwoIcon");
    localImageView.setBackground(paramDrawable);
  }
  
  public final void onClick(View paramView)
  {
    int i = R.id.simOne;
    Object localObject = (ConstraintLayout)b(i);
    boolean bool1 = k.a(paramView, localObject);
    if (bool1)
    {
      paramView = b;
      if (paramView == null)
      {
        localObject = "presenter";
        k.a((String)localObject);
      }
      paramView.a();
      return;
    }
    int j = R.id.simTwo;
    localObject = (ConstraintLayout)b(j);
    boolean bool2 = k.a(paramView, localObject);
    if (bool2)
    {
      paramView = b;
      if (paramView == null)
      {
        localObject = "presenter";
        k.a((String)localObject);
      }
      paramView.b();
      return;
    }
    int k = R.id.doneButton;
    localObject = (Button)b(k);
    boolean bool3 = k.a(paramView, localObject);
    if (bool3)
    {
      paramView = b;
      if (paramView == null)
      {
        localObject = "presenter";
        k.a((String)localObject);
      }
      paramView.c();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.registrationv2.c.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_pay_sim_selection;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    f.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.y_();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    int i = R.id.simOne;
    paramView = (ConstraintLayout)b(i);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    paramView.setOnClickListener(paramBundle);
    i = R.id.simTwo;
    ((ConstraintLayout)b(i)).setOnClickListener(paramBundle);
    i = R.id.doneButton;
    ((Button)b(i)).setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */