package com.truecaller.truepay.app.ui.registrationv2.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.r;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registrationv2.data.BankData;
import com.truecaller.truepay.app.ui.registrationv2.data.BankResponse;
import com.truecaller.truepay.app.ui.registrationv2.views.a.f.a;
import com.truecaller.truepay.app.ui.registrationv2.views.a.g;
import com.truecaller.truepay.app.ui.registrationv2.views.a.k.a;
import com.truecaller.truepay.app.ui.registrationv2.views.a.l;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class a
  extends Fragment
  implements View.OnClickListener, com.truecaller.truepay.app.ui.registrationv2.d.a.b, f.a, k.a
{
  public static final a.a f;
  public com.truecaller.truepay.app.ui.registrationv2.d.a.a b;
  public l c;
  public g d;
  a.b e;
  private com.truecaller.truepay.app.ui.registrationv2.views.a.k g;
  private com.truecaller.truepay.app.ui.registrationv2.views.a.f h;
  private final c.g.a.b i;
  private HashMap j;
  
  static
  {
    a.a locala = new com/truecaller/truepay/app/ui/registrationv2/views/a$a;
    locala.<init>((byte)0);
    f = locala;
  }
  
  public a()
  {
    Object localObject = new com/truecaller/truepay/app/ui/registrationv2/views/a$c;
    ((a.c)localObject).<init>(this);
    localObject = (c.g.a.b)localObject;
    i = ((c.g.a.b)localObject);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = j;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      j = ((HashMap)localObject1);
    }
    localObject1 = j;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = j;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    int k = R.id.collapseButton;
    Object localObject1 = (ImageView)a(k);
    Object localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    k = R.id.searchBank;
    localObject1 = (AppCompatEditText)a(k);
    c.g.b.k.a(localObject1, "searchBank");
    localObject1 = (TextView)localObject1;
    localObject2 = i;
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    k = R.id.recyclerPopularBanks;
    localObject1 = (RecyclerView)a(k);
    localObject2 = localObject1;
    r.A((View)localObject1);
    localObject2 = new android/support/v7/widget/GridLayoutManager;
    Object localObject3 = ((RecyclerView)localObject1).getContext();
    ((GridLayoutManager)localObject2).<init>((Context)localObject3, 3);
    localObject2 = (RecyclerView.LayoutManager)localObject2;
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject2);
    boolean bool = true;
    ((RecyclerView)localObject1).setHasFixedSize(bool);
    k = R.id.recyclerBanks;
    localObject1 = (RecyclerView)a(k);
    localObject3 = localObject1;
    r.A((View)localObject1);
    localObject3 = new android/support/v7/widget/LinearLayoutManager;
    Context localContext = ((RecyclerView)localObject1).getContext();
    ((LinearLayoutManager)localObject3).<init>(localContext);
    localObject3 = (RecyclerView.LayoutManager)localObject3;
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject3);
    ((RecyclerView)localObject1).setHasFixedSize(bool);
  }
  
  public final void a(BankData paramBankData)
  {
    c.g.b.k.b(paramBankData, "bankData");
    b(paramBankData);
  }
  
  public final void a(List paramList)
  {
    c.g.b.k.b(paramList, "popularBanks");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      com.truecaller.truepay.app.ui.registrationv2.views.a.k localk = new com/truecaller/truepay/app/ui/registrationv2/views/a/k;
      c.g.b.k.a(localObject1, "it");
      l locall = c;
      if (locall == null)
      {
        localObject2 = "popularBanksItemPresenter";
        c.g.b.k.a((String)localObject2);
      }
      Object localObject2 = this;
      localObject2 = (k.a)this;
      localk.<init>((Context)localObject1, locall, paramList, (k.a)localObject2);
      g = localk;
      int k = R.id.recyclerPopularBanks;
      paramList = (RecyclerView)a(k);
      c.g.b.k.a(paramList, "recyclerPopularBanks");
      localObject1 = (RecyclerView.Adapter)g;
      paramList.setAdapter((RecyclerView.Adapter)localObject1);
      return;
    }
  }
  
  public final BankResponse b()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return (BankResponse)localBundle.getParcelable("bank_response");
    }
    return null;
  }
  
  public final void b(BankData paramBankData)
  {
    c.g.b.k.b(paramBankData, "bankData");
    a.b localb = e;
    if (localb != null)
    {
      localb.a(paramBankData);
      return;
    }
  }
  
  public final void b(List paramList)
  {
    c.g.b.k.b(paramList, "banks");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      com.truecaller.truepay.app.ui.registrationv2.views.a.f localf = new com/truecaller/truepay/app/ui/registrationv2/views/a/f;
      c.g.b.k.a(localObject1, "it");
      g localg = d;
      if (localg == null)
      {
        localObject2 = "banksItemPresenter";
        c.g.b.k.a((String)localObject2);
      }
      Object localObject2 = this;
      localObject2 = (f.a)this;
      localf.<init>((Context)localObject1, localg, paramList, (f.a)localObject2);
      h = localf;
      int k = R.id.recyclerBanks;
      paramList = (RecyclerView)a(k);
      c.g.b.k.a(paramList, "recyclerBanks");
      localObject1 = (RecyclerView.Adapter)h;
      paramList.setAdapter((RecyclerView.Adapter)localObject1);
      return;
    }
  }
  
  public final void c()
  {
    int k = R.id.popularBanksTitle;
    TextView localTextView = (TextView)a(k);
    c.g.b.k.a(localTextView, "popularBanksTitle");
    t.b((View)localTextView);
  }
  
  public final void c(List paramList)
  {
    c.g.b.k.b(paramList, "banks");
    com.truecaller.truepay.app.ui.registrationv2.views.a.f localf = h;
    if (localf != null)
    {
      c.g.b.k.b(paramList, "banks");
      a = paramList;
      localf.notifyDataSetChanged();
      return;
    }
  }
  
  public final void d()
  {
    int k = R.id.recyclerPopularBanks;
    RecyclerView localRecyclerView = (RecyclerView)a(k);
    c.g.b.k.a(localRecyclerView, "recyclerPopularBanks");
    t.b((View)localRecyclerView);
  }
  
  public final void e()
  {
    int k = R.id.popularBanksTitle;
    TextView localTextView = (TextView)a(k);
    c.g.b.k.a(localTextView, "popularBanksTitle");
    t.a((View)localTextView);
  }
  
  public final void f()
  {
    int k = R.id.recyclerPopularBanks;
    RecyclerView localRecyclerView = (RecyclerView)a(k);
    c.g.b.k.a(localRecyclerView, "recyclerPopularBanks");
    t.a((View)localRecyclerView);
  }
  
  public final void g()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      localf.onBackPressed();
      return;
    }
  }
  
  public final void onClick(View paramView)
  {
    int k = R.id.collapseButton;
    Object localObject = (ImageView)a(k);
    boolean bool = c.g.b.k.a(paramView, localObject);
    if (bool)
    {
      paramView = b;
      if (paramView == null)
      {
        localObject = "presenter";
        c.g.b.k.a((String)localObject);
      }
      paramView.a();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.registrationv2.c.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    int k = R.layout.fragment_pay_bank_selection;
    return paramLayoutInflater.inflate(k, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    com.truecaller.truepay.app.ui.registrationv2.d.a.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.y_();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    c.g.b.k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      c.g.b.k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */