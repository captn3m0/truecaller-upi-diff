package com.truecaller.truepay.app.ui.registrationv2.views.a;

import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.registrationv2.data.BankData;
import com.truecaller.truepay.app.utils.r;

public final class h
  implements g
{
  private int a;
  private final r b;
  
  public h(r paramr)
  {
    b = paramr;
    a = -1;
  }
  
  public final void a(int paramInt)
  {
    a = paramInt;
  }
  
  public final void a(j paramj, BankData paramBankData)
  {
    k.b(paramj, "payBanksItemViewHolder");
    k.b(paramBankData, "bankData");
    Object localObject = paramBankData.getName();
    k.b(localObject, "title");
    int i = R.id.textBankName;
    TextView localTextView = (TextView)paramj.a(i);
    k.a(localTextView, "textBankName");
    localObject = (CharSequence)localObject;
    localTextView.setText((CharSequence)localObject);
    localObject = b;
    paramBankData = paramBankData.getBank_symbol();
    paramBankData = ((r)localObject).b(paramBankData);
    k.b(paramBankData, "drawable");
    int j = R.id.bankIcon;
    ((ImageView)paramj.a(j)).setImageDrawable(paramBankData);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */