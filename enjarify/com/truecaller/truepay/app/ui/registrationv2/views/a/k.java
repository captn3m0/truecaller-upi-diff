package com.truecaller.truepay.app.ui.registrationv2.views.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import java.util.List;

public final class k
  extends RecyclerView.Adapter
{
  private final LayoutInflater a;
  private final l b;
  private List c;
  private final k.a d;
  
  public k(Context paramContext, l paraml, List paramList, k.a parama)
  {
    b = paraml;
    c = paramList;
    d = parama;
    paramContext = LayoutInflater.from(paramContext);
    c.g.b.k.a(paramContext, "LayoutInflater.from(context)");
    a = paramContext;
  }
  
  public final int getItemCount()
  {
    return c.size();
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */