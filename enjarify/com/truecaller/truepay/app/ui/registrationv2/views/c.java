package com.truecaller.truepay.app.ui.registrationv2.views;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.common.ui.PausingLottieAnimationView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registrationv2.c.a.a;
import com.truecaller.truepay.app.ui.registrationv2.c.b;
import com.truecaller.truepay.app.ui.registrationv2.d.d;
import com.truecaller.truepay.app.ui.registrationv2.d.e.a;
import com.truecaller.truepay.app.ui.registrationv2.d.e.b;
import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse;
import java.util.HashMap;

public final class c
  extends Fragment
  implements e.b
{
  public static final c.a c;
  public e.a b;
  private d d;
  private final c.c e;
  private final c.b f;
  private HashMap g;
  
  static
  {
    c.a locala = new com/truecaller/truepay/app/ui/registrationv2/views/c$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public c()
  {
    Object localObject = new com/truecaller/truepay/app/ui/registrationv2/views/c$c;
    ((c.c)localObject).<init>(this);
    e = ((c.c)localObject);
    localObject = new com/truecaller/truepay/app/ui/registrationv2/views/c$b;
    ((c.b)localObject).<init>(this);
    f = ((c.b)localObject);
  }
  
  private View b(int paramInt)
  {
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      g = ((HashMap)localObject1);
    }
    localObject1 = g;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = g;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    d locald = d;
    if (locald != null)
    {
      locald.b();
      return;
    }
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.timerTitleText;
    ((TextView)b(i)).setTextColor(paramInt);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.timerTitleText;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "timerTitleText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b()
  {
    d locald = d;
    if (locald != null)
    {
      locald.c();
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.timerBottomText;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "timerBottomText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final PendingIntent c()
  {
    Object localObject1 = getContext();
    Object localObject2 = new android/content/Intent;
    ((Intent)localObject2).<init>("SMS_SENT_INTENT");
    BroadcastReceiver localBroadcastReceiver = null;
    localObject1 = PendingIntent.getBroadcast((Context)localObject1, 0, (Intent)localObject2, 0);
    localObject2 = getContext();
    if (localObject2 != null)
    {
      localBroadcastReceiver = (BroadcastReceiver)e;
      IntentFilter localIntentFilter = new android/content/IntentFilter;
      String str = "SMS_SENT_INTENT";
      localIntentFilter.<init>(str);
      ((Context)localObject2).registerReceiver(localBroadcastReceiver, localIntentFilter);
    }
    k.a(localObject1, "smsIntent");
    return (PendingIntent)localObject1;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "fileName");
    int i = R.id.lottieView;
    PausingLottieAnimationView localPausingLottieAnimationView = (PausingLottieAnimationView)b(i);
    localPausingLottieAnimationView.setAnimation(paramString);
    localPausingLottieAnimationView.a(true);
    localPausingLottieAnimationView.a();
  }
  
  public final PendingIntent d()
  {
    Object localObject1 = getContext();
    Object localObject2 = new android/content/Intent;
    ((Intent)localObject2).<init>("SMS_DELIVERED_INTENT");
    BroadcastReceiver localBroadcastReceiver = null;
    localObject1 = PendingIntent.getBroadcast((Context)localObject1, 0, (Intent)localObject2, 0);
    localObject2 = getContext();
    if (localObject2 != null)
    {
      localBroadcastReceiver = (BroadcastReceiver)f;
      IntentFilter localIntentFilter = new android/content/IntentFilter;
      String str = "SMS_DELIVERED_INTENT";
      localIntentFilter.<init>(str);
      ((Context)localObject2).registerReceiver(localBroadcastReceiver, localIntentFilter);
    }
    return (PendingIntent)localObject1;
  }
  
  public final String e()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return localBundle.getString("bank_id");
    }
    return null;
  }
  
  public final Integer f()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return Integer.valueOf(localBundle.getInt("sim_index"));
    }
    return null;
  }
  
  public final CheckDeviceResponse g()
  {
    Bundle localBundle = getArguments();
    if (localBundle != null) {
      return (CheckDeviceResponse)localBundle.getParcelable("check_device_resp");
    }
    return null;
  }
  
  public final e.a h()
  {
    e.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof d;
    if (bool)
    {
      paramContext = (d)paramContext;
      d = paramContext;
      return;
    }
    RuntimeException localRuntimeException = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement PayRegistrationListener");
    paramContext = localStringBuilder.toString();
    localRuntimeException.<init>(paramContext);
    throw ((Throwable)localRuntimeException);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.registrationv2.c.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_pay_registration;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    Object localObject2;
    try
    {
      localObject1 = getContext();
      if (localObject1 != null)
      {
        localObject2 = e;
        localObject2 = (BroadcastReceiver)localObject2;
        ((Context)localObject1).unregisterReceiver((BroadcastReceiver)localObject2);
      }
      localObject1 = getContext();
      if (localObject1 != null)
      {
        localObject2 = f;
        localObject2 = (BroadcastReceiver)localObject2;
        ((Context)localObject1).unregisterReceiver((BroadcastReceiver)localObject2);
      }
    }
    catch (Exception localException) {}
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((e.a)localObject1).y_();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */