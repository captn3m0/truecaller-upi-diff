package com.truecaller.truepay.app.ui.registrationv2.views;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.common.ui.PausingLottieAnimationView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registrationv2.d.b.c;
import com.truecaller.truepay.app.ui.registrationv2.d.d.a;
import com.truecaller.truepay.app.ui.registrationv2.data.BankData;
import com.truecaller.truepay.app.ui.registrationv2.data.BankResponse;
import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class b
  extends Fragment
  implements b.c, a.b, com.truecaller.truepay.app.ui.registrationv2.views.a.a.a, d.b
{
  public static final b.a c;
  public com.truecaller.truepay.app.ui.registrationv2.d.b.b a;
  public com.truecaller.truepay.app.ui.registrationv2.views.a.b b;
  private com.truecaller.truepay.app.ui.registrationv2.d.d d;
  private com.truecaller.truepay.app.ui.registrationv2.views.a.a e;
  private HashMap f;
  
  static
  {
    b.a locala = new com/truecaller/truepay/app/ui/registrationv2/views/b$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private View c(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    int i = R.id.progressBar;
    ConstraintLayout localConstraintLayout = (ConstraintLayout)c(i);
    k.a(localConstraintLayout, "progressBar");
    t.b((View)localConstraintLayout);
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.progressText;
    ((TextView)c(i)).setTextColor(paramInt);
  }
  
  public final void a(BankData paramBankData)
  {
    k.b(paramBankData, "bankData");
    Object localObject = getActivity();
    if (localObject != null) {
      ((f)localObject).onBackPressed();
    }
    localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((com.truecaller.truepay.app.ui.registrationv2.d.b.b)localObject).a(paramBankData);
    ((com.truecaller.truepay.app.ui.registrationv2.d.b.b)localObject).a();
  }
  
  public final void a(BankResponse paramBankResponse)
  {
    k.b(paramBankResponse, "bankResponse");
    Object localObject = a.f;
    k.b(paramBankResponse, "bankResponse");
    localObject = new com/truecaller/truepay/app/ui/registrationv2/views/a;
    ((a)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str = "bank_response";
    paramBankResponse = (Parcelable)paramBankResponse;
    localBundle.putParcelable(str, paramBankResponse);
    ((a)localObject).setArguments(localBundle);
    paramBankResponse = this;
    paramBankResponse = (a.b)this;
    e = paramBankResponse;
    paramBankResponse = d;
    if (paramBankResponse != null)
    {
      localObject = (Fragment)localObject;
      paramBankResponse.a((Fragment)localObject, true);
      return;
    }
  }
  
  public final void a(CheckDeviceResponse paramCheckDeviceResponse)
  {
    com.truecaller.truepay.app.ui.registrationv2.d.d locald = d;
    if (locald != null)
    {
      paramCheckDeviceResponse = (Fragment)c.a.a(paramCheckDeviceResponse, null, 0);
      d.a.a(locald, paramCheckDeviceResponse);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.progressText;
    TextView localTextView = (TextView)c(i);
    k.a(localTextView, "progressText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(String paramString, int paramInt, CheckDeviceResponse paramCheckDeviceResponse)
  {
    k.b(paramString, "selectedBankId");
    com.truecaller.truepay.app.ui.registrationv2.d.d locald = d;
    if (locald != null)
    {
      paramString = (Fragment)c.a.a(paramCheckDeviceResponse, paramString, paramInt);
      d.a.a(locald, paramString);
      return;
    }
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "banks");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      int i = paramList.size();
      if (i > 0)
      {
        localObject2 = a;
        if (localObject2 == null)
        {
          localObject3 = "presenter";
          k.a((String)localObject3);
        }
        localObject3 = (BankData)paramList.get(0);
        ((com.truecaller.truepay.app.ui.registrationv2.d.b.b)localObject2).a((BankData)localObject3);
      }
      Object localObject2 = new com/truecaller/truepay/app/ui/registrationv2/views/a/a;
      k.a(localObject1, "it");
      Object localObject3 = b;
      if (localObject3 == null)
      {
        localObject4 = "bankSelectorItemPresenter";
        k.a((String)localObject4);
      }
      Object localObject4 = this;
      localObject4 = (com.truecaller.truepay.app.ui.registrationv2.views.a.a.a)this;
      ((com.truecaller.truepay.app.ui.registrationv2.views.a.a)localObject2).<init>((Context)localObject1, (com.truecaller.truepay.app.ui.registrationv2.views.a.b)localObject3, paramList, (com.truecaller.truepay.app.ui.registrationv2.views.a.a.a)localObject4);
      e = ((com.truecaller.truepay.app.ui.registrationv2.views.a.a)localObject2);
      int j = R.id.listBanks;
      paramList = (RecyclerView)c(j);
      localObject2 = new android/support/v7/widget/LinearLayoutManager;
      ((LinearLayoutManager)localObject2).<init>((Context)localObject1, 0, false);
      localObject2 = (RecyclerView.LayoutManager)localObject2;
      paramList.setLayoutManager((RecyclerView.LayoutManager)localObject2);
      paramList.setHasFixedSize(true);
      localObject1 = (RecyclerView.Adapter)e;
      paramList.setAdapter((RecyclerView.Adapter)localObject1);
      return;
    }
  }
  
  public final void b()
  {
    com.truecaller.truepay.app.ui.registrationv2.d.b.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localb.b();
  }
  
  public final void b(int paramInt)
  {
    com.truecaller.truepay.app.ui.registrationv2.d.b.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localb.a(paramInt);
  }
  
  public final void b(BankData paramBankData)
  {
    k.b(paramBankData, "bankData");
    com.truecaller.truepay.app.ui.registrationv2.d.b.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localb.a(paramBankData);
    paramBankData = e;
    if (paramBankData != null)
    {
      paramBankData.notifyDataSetChanged();
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "fileName");
    int i = R.id.lottieView;
    PausingLottieAnimationView localPausingLottieAnimationView = (PausingLottieAnimationView)c(i);
    localPausingLottieAnimationView.setAnimation(paramString);
    localPausingLottieAnimationView.a(true);
    localPausingLottieAnimationView.a();
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "string");
    int i = R.id.simSelectedText;
    TextView localTextView = (TextView)c(i);
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    t.a((View)localTextView);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "selectedBankName");
    Object localObject = d.d;
    k.b(paramString, "selectedBankName");
    localObject = new com/truecaller/truepay/app/ui/registrationv2/views/d;
    ((d)localObject).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str = "bank_name";
    localBundle.putString(str, paramString);
    ((d)localObject).setArguments(localBundle);
    paramString = this;
    paramString = (d.b)this;
    c = paramString;
    paramString = d;
    if (paramString != null)
    {
      localObject = (Fragment)localObject;
      paramString.a((Fragment)localObject, true);
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.registrationv2.d.d;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.registrationv2.d.d)paramContext;
      d = paramContext;
      return;
    }
    RuntimeException localRuntimeException = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement PayRegistrationListener");
    paramContext = localStringBuilder.toString();
    localRuntimeException.<init>(paramContext);
    throw ((Throwable)localRuntimeException);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.registrationv2.c.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_pay_intro;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    com.truecaller.truepay.app.ui.registrationv2.d.b.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localb.y_();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    int i = R.id.nextButton;
    paramView = (Button)c(i);
    paramBundle = new com/truecaller/truepay/app/ui/registrationv2/views/b$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */