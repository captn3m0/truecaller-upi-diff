package com.truecaller.truepay.app.ui.registrationv2.views.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import c.g.b.k;
import java.util.List;

public final class f
  extends RecyclerView.Adapter
{
  public List a;
  private final LayoutInflater b;
  private final g c;
  private final f.a d;
  
  public f(Context paramContext, g paramg, List paramList, f.a parama)
  {
    c = paramg;
    a = paramList;
    d = parama;
    paramContext = LayoutInflater.from(paramContext);
    k.a(paramContext, "LayoutInflater.from(context)");
    b = paramContext;
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.views.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */