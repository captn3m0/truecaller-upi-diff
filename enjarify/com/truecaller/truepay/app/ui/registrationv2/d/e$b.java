package com.truecaller.truepay.app.ui.registrationv2.d;

import android.app.PendingIntent;
import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse;

public abstract interface e$b
{
  public static final e.b.a a = e.b.a.a;
  
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(String paramString);
  
  public abstract void b();
  
  public abstract void b(String paramString);
  
  public abstract PendingIntent c();
  
  public abstract void c(String paramString);
  
  public abstract PendingIntent d();
  
  public abstract String e();
  
  public abstract Integer f();
  
  public abstract CheckDeviceResponse g();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.d.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */