package com.truecaller.truepay.app.ui.registrationv2.d;

import android.graphics.drawable.Drawable;

public abstract interface f$b
{
  public static final f.b.a a = f.b.a.a;
  
  public abstract String a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(Drawable paramDrawable);
  
  public abstract void a(String paramString);
  
  public abstract void b(Drawable paramDrawable);
  
  public abstract void b(String paramString);
  
  public abstract void c(Drawable paramDrawable);
  
  public abstract void c(String paramString);
  
  public abstract void d(Drawable paramDrawable);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.d.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */