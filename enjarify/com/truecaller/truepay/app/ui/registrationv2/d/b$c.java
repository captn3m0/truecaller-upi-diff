package com.truecaller.truepay.app.ui.registrationv2.d;

import com.truecaller.truepay.app.ui.registrationv2.data.BankResponse;
import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse;
import java.util.List;

public abstract interface b$c
{
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(BankResponse paramBankResponse);
  
  public abstract void a(CheckDeviceResponse paramCheckDeviceResponse);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, int paramInt, CheckDeviceResponse paramCheckDeviceResponse);
  
  public abstract void a(List paramList);
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
  
  public abstract void d(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.d.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */