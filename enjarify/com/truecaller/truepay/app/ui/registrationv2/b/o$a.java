package com.truecaller.truepay.app.ui.registrationv2.b;

import c.g.b.k;
import com.truecaller.truepay.PayTempTokenCallBack;
import com.truecaller.truepay.app.ui.registrationv2.a.a;
import com.truecaller.truepay.app.ui.registrationv2.a.e;
import com.truecaller.truepay.app.ui.registrationv2.a.g;
import com.truecaller.truepay.app.ui.registrationv2.data.RegisterRequest;
import com.truecaller.truepay.app.ui.registrationv2.data.RegisterResponse;
import e.b;
import e.r;

public final class o$a
  implements PayTempTokenCallBack
{
  o$a(kotlinx.coroutines.j paramj, o paramo) {}
  
  public final void onFailure()
  {
    Object localObject1 = a;
    boolean bool = ((kotlinx.coroutines.j)localObject1).b();
    if (bool)
    {
      localObject1 = (c.d.c)a;
      Object localObject2 = new com/truecaller/truepay/app/ui/registrationv2/data/j;
      int i = 2;
      ((com.truecaller.truepay.app.ui.registrationv2.data.j)localObject2).<init>("", null, i);
      localObject2 = c.o.d(localObject2);
      ((c.d.c)localObject1).b(localObject2);
    }
  }
  
  public final void onSuccess(String paramString)
  {
    k.b(paramString, "tempToken");
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    int i = ((CharSequence)localObject1).length();
    int j = 1;
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    int k = 2;
    boolean bool2;
    Object localObject4;
    if (i != 0)
    {
      localObject1 = new com/truecaller/truepay/app/ui/registrationv2/data/RegisterRequest;
      ((RegisterRequest)localObject1).<init>(paramString);
      paramString = a;
      bool2 = paramString.b();
      if (bool2)
      {
        paramString = (c.d.c)a;
        Object localObject2 = b;
        k.b(localObject1, "registerRequest");
        localObject1 = a.a((RegisterRequest)localObject1);
        k.b(localObject1, "receiver$0");
        localObject1 = ((b)localObject1).c();
        Object localObject3 = "result";
        k.a(localObject1, (String)localObject3);
        boolean bool3 = ((r)localObject1).d();
        if (bool3)
        {
          localObject3 = new com/truecaller/truepay/app/ui/registrationv2/a/g;
          localObject1 = ((r)localObject1).e();
          ((g)localObject3).<init>(localObject1);
          localObject3 = (e)localObject3;
        }
        else
        {
          localObject1 = new com/truecaller/truepay/app/ui/registrationv2/a/a;
          ((a)localObject1).<init>("");
          localObject3 = localObject1;
          localObject3 = (e)localObject1;
        }
        boolean bool1 = localObject3 instanceof g;
        if (bool1)
        {
          localObject1 = localObject3;
          localObject1 = (RegisterResponse)a;
          if (localObject1 != null)
          {
            com.truecaller.truepay.data.e.c localc = b;
            String str1 = ((RegisterResponse)localObject1).getTc_user_id();
            localc.b("#6&&&#dd%bbnb^1v$3", str1);
            localc = b;
            String str2 = "gb^9ej$k~n*62ms}";
            str1 = ((RegisterResponse)localObject1).getUuid();
            localc.b(str2, str1);
            localc = b;
            localObject2 = "dfb@6g%^hB#4*(@4";
            localObject1 = ((RegisterResponse)localObject1).getSecret_token();
            localc.b((String)localObject2, (String)localObject1);
          }
          localObject1 = new com/truecaller/truepay/app/ui/registrationv2/data/j;
          ((com.truecaller.truepay.app.ui.registrationv2.data.j)localObject1).<init>(null, (e)localObject3, j);
        }
        else
        {
          localObject1 = new com/truecaller/truepay/app/ui/registrationv2/data/j;
          localObject4 = "";
          ((com.truecaller.truepay.app.ui.registrationv2.data.j)localObject1).<init>((String)localObject4, null, k);
        }
        localObject4 = c.o.a;
        localObject1 = c.o.d(localObject1);
        paramString.b(localObject1);
      }
    }
    else
    {
      paramString = a;
      bool2 = paramString.b();
      if (bool2)
      {
        paramString = (c.d.c)a;
        localObject1 = new com/truecaller/truepay/app/ui/registrationv2/data/j;
        ((com.truecaller.truepay.app.ui.registrationv2.data.j)localObject1).<init>("", null, k);
        localObject4 = c.o.a;
        localObject1 = c.o.d(localObject1);
        paramString.b(localObject1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.b.o.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */