package com.truecaller.truepay.app.ui.registrationv2.b;

import android.util.Base64;
import c.n.d;
import c.u;
import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse;
import com.truecaller.truepay.app.ui.registrationv2.data.IciciSmsResponse;
import com.truecaller.truepay.app.ui.registrationv2.data.SmsCheckDeviceResponse;
import com.truecaller.truepay.data.e.c;

public final class q
  implements k
{
  private final com.truecaller.truepay.app.ui.registrationv2.f.a a;
  private final e b;
  private final com.truecaller.utils.a c;
  private final c d;
  
  public q(com.truecaller.truepay.app.ui.registrationv2.f.a parama, e parame, com.truecaller.utils.a parama1, c paramc)
  {
    a = parama;
    b = parame;
    c = parama1;
    d = paramc;
  }
  
  private final String a(CheckDeviceResponse paramCheckDeviceResponse, String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = paramCheckDeviceResponse.getSms().getIcici().getContent();
    localStringBuilder.append(str);
    char c1 = '-';
    localStringBuilder.append(c1);
    localStringBuilder.append(paramString2);
    localStringBuilder.append(c1);
    localStringBuilder.append(paramString1);
    localStringBuilder.append(c1);
    long l = c.a();
    localStringBuilder.append(l);
    paramString1 = localStringBuilder.toString();
    paramString2 = new java/lang/StringBuilder;
    paramString2.<init>();
    paramCheckDeviceResponse = paramCheckDeviceResponse.getSms().getIcici().getSms_prefix();
    paramString2.append(paramCheckDeviceResponse);
    char c2 = ' ';
    paramString2.append(c2);
    paramCheckDeviceResponse = d.a;
    if (paramString1 != null)
    {
      paramCheckDeviceResponse = paramString1.getBytes(paramCheckDeviceResponse);
      c.g.b.k.a(paramCheckDeviceResponse, "(this as java.lang.String).getBytes(charset)");
      paramCheckDeviceResponse = Base64.encodeToString(paramCheckDeviceResponse, 2);
      paramString2.append(paramCheckDeviceResponse);
      return paramString2.toString();
    }
    paramCheckDeviceResponse = new c/u;
    paramCheckDeviceResponse.<init>("null cannot be cast to non-null type java.lang.String");
    throw paramCheckDeviceResponse;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.b.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */