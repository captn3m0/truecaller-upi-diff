package com.truecaller.truepay.app.ui.registrationv2.b;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private j(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static j a(Provider paramProvider1, Provider paramProvider2)
  {
    j localj = new com/truecaller/truepay/app/ui/registrationv2/b/j;
    localj.<init>(paramProvider1, paramProvider2);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */