package com.truecaller.truepay.app.ui.registrationv2.b;

import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private t(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
  }
  
  public static t a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    t localt = new com/truecaller/truepay/app/ui/registrationv2/b/t;
    localt.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.b.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */