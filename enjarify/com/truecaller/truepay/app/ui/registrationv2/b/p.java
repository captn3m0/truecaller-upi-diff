package com.truecaller.truepay.app.ui.registrationv2.b;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private p(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static p a(Provider paramProvider1, Provider paramProvider2)
  {
    p localp = new com/truecaller/truepay/app/ui/registrationv2/b/p;
    localp.<init>(paramProvider1, paramProvider2);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */