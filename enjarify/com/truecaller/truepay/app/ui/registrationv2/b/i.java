package com.truecaller.truepay.app.ui.registrationv2.b;

import c.d.a.a;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.truepay.app.ui.registrationv2.a.g;
import com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse;
import com.truecaller.truepay.app.ui.registrationv2.data.SmsManagerResponse;
import com.truecaller.truepay.app.ui.registrationv2.data.VerifyBindingResponse;
import com.truecaller.truepay.app.ui.registrationv2.data.b;
import com.truecaller.truepay.app.ui.registrationv2.data.d;
import com.truecaller.truepay.app.ui.registrationv2.data.j;

public final class i
  implements h
{
  private h.a a;
  private final n b;
  private final k c;
  
  public i(n paramn, k paramk)
  {
    b = paramn;
    c = paramk;
  }
  
  public final Object a(c.d.c paramc)
  {
    boolean bool1 = paramc instanceof i.c;
    int m;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (i.c)paramc;
      int j = b;
      k = -1 << -1;
      j &= k;
      if (j != 0)
      {
        m = b - k;
        b = m;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/truepay/app/ui/registrationv2/b/i$c;
    ((i.c)localObject1).<init>(this, (c.d.c)paramc);
    label70:
    paramc = a;
    Object localObject2 = a.a;
    int k = b;
    int i1 = 1;
    boolean bool3;
    Object localObject3;
    switch (k)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 2: 
      localObject2 = (VerifyBindingResponse)f;
      localObject1 = (i)d;
      bool3 = paramc instanceof o.b;
      if (!bool3) {
        break label537;
      }
      throw a;
    case 1: 
      localObject3 = (i)d;
      bool6 = paramc instanceof o.b;
      if (bool6) {
        throw a;
      }
      break;
    case 0: 
      bool3 = paramc instanceof o.b;
      if (bool3) {
        break label812;
      }
      paramc = a;
      if (paramc == null)
      {
        localObject3 = "registrationListener";
        c.g.b.k.a((String)localObject3);
      }
      paramc.c();
      paramc = b;
      d = this;
      b = i1;
      paramc = l.a.a(paramc, (c.d.c)localObject1);
      if (paramc == localObject2) {
        return localObject2;
      }
      localObject3 = this;
    }
    paramc = (j)paramc;
    Object localObject4 = a;
    boolean bool6 = localObject4 instanceof g;
    if (bool6)
    {
      localObject4 = a;
      if (localObject4 != null)
      {
        localObject4 = a;
        if (localObject4 != null)
        {
          localObject4 = (VerifyBindingResponse)localObject4;
          d = localObject3;
          e = paramc;
          f = localObject4;
          m = 2;
          b = m;
          paramc = ((VerifyBindingResponse)localObject4).getRegistration_status();
          int i = paramc.hashCode();
          int i2 = -2039330720;
          boolean bool4;
          if (i != i2)
          {
            i2 = 17754868;
            if (i != i2)
            {
              i2 = 236489792;
              if (i != i2)
              {
                i2 = 1808082440;
                if (i != i2) {
                  break label718;
                }
                localObject1 = "device_binding_initiated";
                bool4 = paramc.equals(localObject1);
                if (!bool4) {
                  break label718;
                }
                paramc = new com/truecaller/truepay/app/ui/registrationv2/data/c;
                paramc.<init>();
                paramc = (com.truecaller.truepay.app.ui.registrationv2.data.i)paramc;
              }
              else
              {
                localObject1 = "device_binding_failed";
                bool4 = paramc.equals(localObject1);
                if (!bool4) {
                  break label718;
                }
                paramc = new com/truecaller/truepay/app/ui/registrationv2/data/b;
                paramc.<init>();
                paramc = (com.truecaller.truepay.app.ui.registrationv2.data.i)paramc;
              }
            }
            else
            {
              localObject1 = "sending_sms";
              bool4 = paramc.equals(localObject1);
              if (!bool4) {
                break label718;
              }
              paramc = new com/truecaller/truepay/app/ui/registrationv2/data/k;
              paramc.<init>();
              paramc = (com.truecaller.truepay.app.ui.registrationv2.data.i)paramc;
            }
          }
          else
          {
            localObject1 = "device_binding_success";
            bool4 = paramc.equals(localObject1);
            if (!bool4) {
              break label718;
            }
            paramc = new com/truecaller/truepay/app/ui/registrationv2/data/d;
            paramc.<init>();
            paramc = (com.truecaller.truepay.app.ui.registrationv2.data.i)paramc;
          }
          if (paramc == localObject2) {
            return localObject2;
          }
          localObject1 = localObject3;
          localObject2 = localObject4;
          label537:
          paramc = (com.truecaller.truepay.app.ui.registrationv2.data.i)paramc;
          bool3 = paramc instanceof com.truecaller.truepay.app.ui.registrationv2.data.c;
          if (bool3)
          {
            int n = ((VerifyBindingResponse)localObject2).getCallCount();
            if (n == i1)
            {
              paramc = a;
              if (paramc == null)
              {
                localObject1 = "registrationListener";
                c.g.b.k.a((String)localObject1);
              }
              i = 45;
              paramc.a(i);
            }
            else
            {
              paramc = a;
              if (paramc == null)
              {
                localObject1 = "registrationListener";
                c.g.b.k.a((String)localObject1);
              }
              paramc.b();
            }
          }
          else
          {
            boolean bool2 = paramc instanceof d;
            if (bool2)
            {
              paramc = a;
              if (paramc == null)
              {
                localObject1 = "registrationListener";
                c.g.b.k.a((String)localObject1);
              }
              paramc.a();
            }
            else
            {
              boolean bool5 = paramc instanceof b;
              if (bool5)
              {
                paramc = a;
                if (paramc == null)
                {
                  localObject1 = "registrationListener";
                  c.g.b.k.a((String)localObject1);
                }
                paramc.b();
              }
              else
              {
                paramc = a;
                if (paramc == null)
                {
                  localObject1 = "registrationListener";
                  c.g.b.k.a((String)localObject1);
                }
                paramc.b();
                break label808;
                label718:
                paramc = new java/lang/IllegalArgumentException;
                localObject1 = new java/lang/StringBuilder;
                ((StringBuilder)localObject1).<init>("Unknown State ");
                localObject2 = ((VerifyBindingResponse)localObject4).getRegistration_status();
                ((StringBuilder)localObject1).append((String)localObject2);
                localObject1 = ((StringBuilder)localObject1).toString();
                paramc.<init>((String)localObject1);
                throw ((Throwable)paramc);
              }
            }
          }
        }
        else
        {
          paramc = new c/u;
          paramc.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registrationv2.data.VerifyBindingResponse");
          throw paramc;
        }
      }
      else
      {
        paramc = new c/u;
        paramc.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registrationv2.api.Success<kotlin.Any>");
        throw paramc;
      }
    }
    else
    {
      paramc = a;
      if (paramc == null)
      {
        localObject1 = "registrationListener";
        c.g.b.k.a((String)localObject1);
      }
      paramc.b();
    }
    label808:
    return x.a;
    label812:
    throw a;
  }
  
  public final Object a(CheckDeviceResponse paramCheckDeviceResponse, c.d.c paramc)
  {
    boolean bool1 = paramc instanceof i.b;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (i.b)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int m = b - j;
        b = m;
        break label77;
      }
    }
    Object localObject1 = new com/truecaller/truepay/app/ui/registrationv2/b/i$b;
    ((i.b)localObject1).<init>(this, (c.d.c)paramc);
    label77:
    paramc = a;
    a locala = a.a;
    int j = b;
    i locali;
    switch (j)
    {
    default: 
      paramCheckDeviceResponse = new java/lang/IllegalStateException;
      paramCheckDeviceResponse.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramCheckDeviceResponse;
    case 2: 
      boolean bool2 = paramc instanceof o.b;
      if (!bool2) {
        break label382;
      }
      throw a;
    case 1: 
      paramCheckDeviceResponse = (CheckDeviceResponse)e;
      locali = (i)d;
      bool3 = paramc instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      int k = paramc instanceof o.b;
      if (k != 0) {
        break label440;
      }
      paramc = c;
      d = this;
      e = paramCheckDeviceResponse;
      k = 1;
      b = k;
      paramc = paramc.a(paramCheckDeviceResponse, (c.d.c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      locali = this;
    }
    paramc = (j)paramc;
    Object localObject2 = a;
    boolean bool3 = localObject2 instanceof g;
    if (bool3)
    {
      localObject2 = a;
      if (localObject2 != null)
      {
        localObject2 = a;
        if (localObject2 != null)
        {
          localObject2 = (SmsManagerResponse)localObject2;
          int n = ((SmsManagerResponse)localObject2).getRegistrationFlow();
          int i1 = 2;
          if (n == i1)
          {
            d = locali;
            e = paramCheckDeviceResponse;
            f = paramc;
            g = localObject2;
            b = i1;
            paramc = locali.a((c.d.c)localObject1);
            if (paramc == locala) {
              return locala;
            }
            label382:
            return paramc;
          }
          paramCheckDeviceResponse = a;
          if (paramCheckDeviceResponse == null)
          {
            paramc = "registrationListener";
            c.g.b.k.a(paramc);
          }
          paramCheckDeviceResponse.a((SmsManagerResponse)localObject2);
        }
        else
        {
          paramCheckDeviceResponse = new c/u;
          paramCheckDeviceResponse.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registrationv2.data.SmsManagerResponse");
          throw paramCheckDeviceResponse;
        }
      }
      else
      {
        paramCheckDeviceResponse = new c/u;
        paramCheckDeviceResponse.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registrationv2.api.Success<kotlin.Any>");
        throw paramCheckDeviceResponse;
      }
    }
    return x.a;
    label440:
    throw a;
  }
  
  public final void a(h.a parama)
  {
    c.g.b.k.b(parama, "registrationListener");
    a = parama;
  }
  
  public final Object b(c.d.c paramc)
  {
    boolean bool1 = paramc instanceof i.a;
    if (bool1)
    {
      localObject = paramc;
      localObject = (i.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        k = b - j;
        b = k;
        break label70;
      }
    }
    Object localObject = new com/truecaller/truepay/app/ui/registrationv2/b/i$a;
    ((i.a)localObject).<init>(this, (c.d.c)paramc);
    label70:
    paramc = a;
    a locala = a.a;
    int j = b;
    i locali;
    switch (j)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 2: 
      bool1 = paramc instanceof o.b;
      if (!bool1) {
        break label251;
      }
      throw a;
    case 1: 
      locali = (i)d;
      boolean bool3 = paramc instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label253;
      }
      d = this;
      k = 1;
      b = k;
      paramc = x.a;
      if (paramc == locala) {
        return locala;
      }
      locali = this;
    }
    d = locali;
    int k = 2;
    b = k;
    paramc = locali.a((c.d.c)localObject);
    if (paramc == locala) {
      return locala;
    }
    label251:
    return paramc;
    label253:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */