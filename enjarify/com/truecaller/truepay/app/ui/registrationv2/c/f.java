package com.truecaller.truepay.app.ui.registrationv2.c;

import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d
{
  private final c a;
  private final Provider b;
  
  private f(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static f a(c paramc, Provider paramProvider)
  {
    f localf = new com/truecaller/truepay/app/ui/registrationv2/c/f;
    localf.<init>(paramc, paramProvider);
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.c.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */