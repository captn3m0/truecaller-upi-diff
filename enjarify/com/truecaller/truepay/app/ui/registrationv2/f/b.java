package com.truecaller.truepay.app.ui.registrationv2.f;

import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.telephony.SmsManager;
import c.g.b.k;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.d;
import com.truecaller.multisim.h;
import java.util.ArrayList;
import java.util.List;

public final class b
  implements a
{
  private final h a;
  
  public b(h paramh)
  {
    a = paramh;
  }
  
  public final SimInfo a(int paramInt)
  {
    return a.a(paramInt);
  }
  
  public final d a(Cursor paramCursor)
  {
    k.b(paramCursor, "cursor");
    paramCursor = a.a(paramCursor);
    k.a(paramCursor, "multiSimManager.wrapCallLogCursor(cursor)");
    return paramCursor;
  }
  
  public final String a()
  {
    String str = a.a();
    k.a(str, "multiSimManager.getAnalyticsName()");
    return str;
  }
  
  public final String a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    paramIntent = a.a(paramIntent);
    k.a(paramIntent, "multiSimManager.getSimTo…FromDeliverIntent(intent)");
    return paramIntent;
  }
  
  public final void a(Intent paramIntent, String paramString)
  {
    k.b(paramIntent, "intent");
    k.b(paramString, "simToken");
    a.a(paramIntent, paramString);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "simToken");
    a.a(paramString);
  }
  
  public final boolean a(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString4)
  {
    k.b(paramString1, "destinationAddress");
    k.b(paramString3, "text");
    k.b(paramPendingIntent1, "sentIntent");
    k.b(paramString4, "simToken");
    return a.a(paramString1, paramString2, paramString3, paramPendingIntent1, paramPendingIntent2, paramString4);
  }
  
  public final boolean a(String paramString1, String paramString2, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3, String paramString3)
  {
    k.b(paramString1, "destinationAddress");
    k.b(paramArrayList1, "parts");
    k.b(paramArrayList2, "sentIntents");
    k.b(paramString3, "simToken");
    return a.a(paramString1, paramString2, paramArrayList1, paramArrayList2, paramArrayList3, paramString3);
  }
  
  public final SimInfo b(String paramString)
  {
    k.b(paramString, "simToken");
    return a.b(paramString);
  }
  
  public final String b()
  {
    return a.b();
  }
  
  public final String b(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    paramIntent = a.b(paramIntent);
    k.a(paramIntent, "multiSimManager.getSimTo…dViaMessageIntent(intent)");
    return paramIntent;
  }
  
  public final com.truecaller.multisim.a c(String paramString)
  {
    k.b(paramString, "simToken");
    paramString = a.c(paramString);
    k.a(paramString, "multiSimManager.getCarrierConfiguration(simToken)");
    return paramString;
  }
  
  public final String c()
  {
    return a.c();
  }
  
  public final String d()
  {
    return a.d();
  }
  
  public final String d(String paramString)
  {
    k.b(paramString, "simToken");
    return a.d(paramString);
  }
  
  public final String e(String paramString)
  {
    k.b(paramString, "simToken");
    return a.d(paramString);
  }
  
  public final boolean e()
  {
    return a.e();
  }
  
  public final SmsManager f(String paramString)
  {
    k.b(paramString, "simToken");
    paramString = a.f(paramString);
    k.a(paramString, "multiSimManager.getSmsManager(simToken)");
    return paramString;
  }
  
  public final String f()
  {
    String str = a.f();
    k.a(str, "multiSimManager.defaultSimToken");
    return str;
  }
  
  public final int g(String paramString)
  {
    return a.g(paramString);
  }
  
  public final String g()
  {
    String str = a.g();
    k.a(str, "multiSimManager.selectedCallSimToken");
    return str;
  }
  
  public final List h()
  {
    List localList = a.h();
    k.a(localList, "multiSimManager.allSimInfos");
    return localList;
  }
  
  public final List i()
  {
    List localList = a.i();
    k.a(localList, "multiSimManager.simSerials");
    return localList;
  }
  
  public final boolean j()
  {
    return a.j();
  }
  
  public final boolean k()
  {
    return a.k();
  }
  
  public final boolean l()
  {
    return a.l();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */