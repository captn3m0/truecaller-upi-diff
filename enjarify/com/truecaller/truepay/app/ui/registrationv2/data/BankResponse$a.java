package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.ArrayList;
import java.util.List;

public final class BankResponse$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    BankResponse localBankResponse = new com/truecaller/truepay/app/ui/registrationv2/data/BankResponse;
    int i = paramParcel.readInt();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(i);
    while (i != 0)
    {
      localObject = (BankData)BankData.CREATOR.createFromParcel(paramParcel);
      localArrayList.add(localObject);
      i += -1;
    }
    i = paramParcel.readInt();
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>(i);
    while (i != 0)
    {
      BankData localBankData = (BankData)BankData.CREATOR.createFromParcel(paramParcel);
      ((ArrayList)localObject).add(localBankData);
      i += -1;
    }
    paramParcel = paramParcel.readString();
    localBankResponse.<init>(localArrayList, (List)localObject, paramParcel);
    return localBankResponse;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new BankResponse[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.BankResponse.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */