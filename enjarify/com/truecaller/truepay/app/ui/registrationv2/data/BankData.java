package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class BankData
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String account_provider_id;
  private final String bank_symbol;
  private final String id;
  private final String iin;
  private final boolean is_popular;
  private final String mandatory_psp;
  private final String name;
  private boolean popular;
  private final String popularity_index;
  private int sim_index;
  private int sms_count;
  private final boolean upi_pin_required;
  
  static
  {
    BankData.a locala = new com/truecaller/truepay/app/ui/registrationv2/data/BankData$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public BankData(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean1, String paramString6, boolean paramBoolean2, String paramString7, int paramInt1, int paramInt2, boolean paramBoolean3)
  {
    bank_symbol = paramString1;
    account_provider_id = paramString2;
    name = paramString3;
    id = paramString4;
    iin = paramString5;
    is_popular = paramBoolean1;
    popularity_index = paramString6;
    upi_pin_required = paramBoolean2;
    mandatory_psp = paramString7;
    sim_index = paramInt1;
    sms_count = paramInt2;
    popular = paramBoolean3;
  }
  
  public final String component1()
  {
    return bank_symbol;
  }
  
  public final int component10()
  {
    return sim_index;
  }
  
  public final int component11()
  {
    return sms_count;
  }
  
  public final boolean component12()
  {
    return popular;
  }
  
  public final String component2()
  {
    return account_provider_id;
  }
  
  public final String component3()
  {
    return name;
  }
  
  public final String component4()
  {
    return id;
  }
  
  public final String component5()
  {
    return iin;
  }
  
  public final boolean component6()
  {
    return is_popular;
  }
  
  public final String component7()
  {
    return popularity_index;
  }
  
  public final boolean component8()
  {
    return upi_pin_required;
  }
  
  public final String component9()
  {
    return mandatory_psp;
  }
  
  public final BankData copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean1, String paramString6, boolean paramBoolean2, String paramString7, int paramInt1, int paramInt2, boolean paramBoolean3)
  {
    k.b(paramString1, "bank_symbol");
    k.b(paramString3, "name");
    k.b(paramString4, "id");
    k.b(paramString6, "popularity_index");
    k.b(paramString7, "mandatory_psp");
    BankData localBankData = new com/truecaller/truepay/app/ui/registrationv2/data/BankData;
    localBankData.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramBoolean1, paramString6, paramBoolean2, paramString7, paramInt1, paramInt2, paramBoolean3);
    return localBankData;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof BankData;
      if (bool2)
      {
        paramObject = (BankData)paramObject;
        String str1 = bank_symbol;
        String str2 = bank_symbol;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          str1 = account_provider_id;
          str2 = account_provider_id;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            str1 = name;
            str2 = name;
            bool2 = k.a(str1, str2);
            if (bool2)
            {
              str1 = id;
              str2 = id;
              bool2 = k.a(str1, str2);
              if (bool2)
              {
                str1 = iin;
                str2 = iin;
                bool2 = k.a(str1, str2);
                if (bool2)
                {
                  bool2 = is_popular;
                  boolean bool4 = is_popular;
                  if (bool2 == bool4)
                  {
                    bool2 = true;
                  }
                  else
                  {
                    bool2 = false;
                    str1 = null;
                  }
                  if (bool2)
                  {
                    str1 = popularity_index;
                    str2 = popularity_index;
                    bool2 = k.a(str1, str2);
                    if (bool2)
                    {
                      bool2 = upi_pin_required;
                      bool4 = upi_pin_required;
                      if (bool2 == bool4)
                      {
                        bool2 = true;
                      }
                      else
                      {
                        bool2 = false;
                        str1 = null;
                      }
                      if (bool2)
                      {
                        str1 = mandatory_psp;
                        str2 = mandatory_psp;
                        bool2 = k.a(str1, str2);
                        if (bool2)
                        {
                          int i = sim_index;
                          int j = sim_index;
                          if (i == j)
                          {
                            i = 1;
                          }
                          else
                          {
                            i = 0;
                            str1 = null;
                          }
                          if (i != 0)
                          {
                            i = sms_count;
                            j = sms_count;
                            if (i == j)
                            {
                              i = 1;
                            }
                            else
                            {
                              i = 0;
                              str1 = null;
                            }
                            if (i != 0)
                            {
                              boolean bool3 = popular;
                              boolean bool5 = popular;
                              if (bool3 == bool5)
                              {
                                bool5 = true;
                              }
                              else
                              {
                                bool5 = false;
                                paramObject = null;
                              }
                              if (bool5) {
                                return bool1;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getAccount_provider_id()
  {
    return account_provider_id;
  }
  
  public final String getBank_symbol()
  {
    return bank_symbol;
  }
  
  public final String getId()
  {
    return id;
  }
  
  public final String getIin()
  {
    return iin;
  }
  
  public final String getMandatory_psp()
  {
    return mandatory_psp;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final boolean getPopular()
  {
    return popular;
  }
  
  public final String getPopularity_index()
  {
    return popularity_index;
  }
  
  public final int getSim_index()
  {
    return sim_index;
  }
  
  public final int getSms_count()
  {
    return sms_count;
  }
  
  public final boolean getUpi_pin_required()
  {
    return upi_pin_required;
  }
  
  public final int hashCode()
  {
    String str1 = bank_symbol;
    int i = 0;
    if (str1 != null)
    {
      k = str1.hashCode();
    }
    else
    {
      k = 0;
      str1 = null;
    }
    k *= 31;
    String str2 = account_provider_id;
    int m;
    if (str2 != null)
    {
      m = str2.hashCode();
    }
    else
    {
      m = 0;
      str2 = null;
    }
    int k = (k + m) * 31;
    str2 = name;
    int n;
    if (str2 != null)
    {
      n = str2.hashCode();
    }
    else
    {
      n = 0;
      str2 = null;
    }
    k = (k + n) * 31;
    str2 = id;
    int i1;
    if (str2 != null)
    {
      i1 = str2.hashCode();
    }
    else
    {
      i1 = 0;
      str2 = null;
    }
    k = (k + i1) * 31;
    str2 = iin;
    int i2;
    if (str2 != null)
    {
      i2 = str2.hashCode();
    }
    else
    {
      i2 = 0;
      str2 = null;
    }
    k = (k + i2) * 31;
    int i3 = is_popular;
    if (i3 != 0) {
      i3 = 1;
    }
    k = (k + i3) * 31;
    str2 = popularity_index;
    int i4;
    if (str2 != null)
    {
      i4 = str2.hashCode();
    }
    else
    {
      i4 = 0;
      str2 = null;
    }
    k = (k + i4) * 31;
    int i5 = upi_pin_required;
    if (i5 != 0) {
      i5 = 1;
    }
    k = (k + i5) * 31;
    str2 = mandatory_psp;
    if (str2 != null) {
      i = str2.hashCode();
    }
    k = (k + i) * 31;
    i = sim_index;
    k = (k + i) * 31;
    i = sms_count;
    k = (k + i) * 31;
    int j = popular;
    if (j != 0) {
      j = 1;
    }
    return k + j;
  }
  
  public final boolean is_popular()
  {
    return is_popular;
  }
  
  public final void setPopular(boolean paramBoolean)
  {
    popular = paramBoolean;
  }
  
  public final void setSim_index(int paramInt)
  {
    sim_index = paramInt;
  }
  
  public final void setSms_count(int paramInt)
  {
    sms_count = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BankData(bank_symbol=");
    String str = bank_symbol;
    localStringBuilder.append(str);
    localStringBuilder.append(", account_provider_id=");
    str = account_provider_id;
    localStringBuilder.append(str);
    localStringBuilder.append(", name=");
    str = name;
    localStringBuilder.append(str);
    localStringBuilder.append(", id=");
    str = id;
    localStringBuilder.append(str);
    localStringBuilder.append(", iin=");
    str = iin;
    localStringBuilder.append(str);
    localStringBuilder.append(", is_popular=");
    boolean bool1 = is_popular;
    localStringBuilder.append(bool1);
    localStringBuilder.append(", popularity_index=");
    str = popularity_index;
    localStringBuilder.append(str);
    localStringBuilder.append(", upi_pin_required=");
    bool1 = upi_pin_required;
    localStringBuilder.append(bool1);
    localStringBuilder.append(", mandatory_psp=");
    str = mandatory_psp;
    localStringBuilder.append(str);
    localStringBuilder.append(", sim_index=");
    int i = sim_index;
    localStringBuilder.append(i);
    localStringBuilder.append(", sms_count=");
    i = sms_count;
    localStringBuilder.append(i);
    localStringBuilder.append(", popular=");
    boolean bool2 = popular;
    localStringBuilder.append(bool2);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = bank_symbol;
    paramParcel.writeString(str);
    str = account_provider_id;
    paramParcel.writeString(str);
    str = name;
    paramParcel.writeString(str);
    str = id;
    paramParcel.writeString(str);
    str = iin;
    paramParcel.writeString(str);
    paramInt = is_popular;
    paramParcel.writeInt(paramInt);
    str = popularity_index;
    paramParcel.writeString(str);
    paramInt = upi_pin_required;
    paramParcel.writeInt(paramInt);
    str = mandatory_psp;
    paramParcel.writeString(str);
    paramInt = sim_index;
    paramParcel.writeInt(paramInt);
    paramInt = sms_count;
    paramParcel.writeInt(paramInt);
    paramInt = popular;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.BankData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */