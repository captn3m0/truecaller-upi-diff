package com.truecaller.truepay.app.ui.registrationv2.data.a;

import android.arch.persistence.room.c;
import android.arch.persistence.room.f;
import android.arch.persistence.room.i;
import android.arch.persistence.room.j;
import android.database.Cursor;
import com.truecaller.truepay.app.ui.registrationv2.data.BankData;
import java.util.ArrayList;
import java.util.List;

public final class b
  implements a
{
  private final f a;
  private final c b;
  private final j c;
  private final j d;
  
  public b(f paramf)
  {
    a = paramf;
    Object localObject = new com/truecaller/truepay/app/ui/registrationv2/data/a/b$1;
    ((b.1)localObject).<init>(this, paramf);
    b = ((c)localObject);
    localObject = new com/truecaller/truepay/app/ui/registrationv2/data/a/b$2;
    ((b.2)localObject).<init>(this, paramf);
    c = ((j)localObject);
    localObject = new com/truecaller/truepay/app/ui/registrationv2/data/a/b$3;
    ((b.3)localObject).<init>(this, paramf);
    d = ((j)localObject);
  }
  
  public final List a()
  {
    ArrayList localArrayList = null;
    i locali = i.a("SELECT * FROM bank_list ORDER BY name ASC", 0);
    Object localObject1 = this;
    Cursor localCursor = a.a(locali);
    String str1 = "bank_symbol";
    try
    {
      int i = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "account_provider_id";
      int j = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "name";
      int k = localCursor.getColumnIndexOrThrow(str3);
      String str4 = "id";
      int m = localCursor.getColumnIndexOrThrow(str4);
      String str5 = "iin";
      int n = localCursor.getColumnIndexOrThrow(str5);
      String str6 = "is_popular";
      int i1 = localCursor.getColumnIndexOrThrow(str6);
      String str7 = "popularity_index";
      int i2 = localCursor.getColumnIndexOrThrow(str7);
      String str8 = "upi_pin_required";
      int i3 = localCursor.getColumnIndexOrThrow(str8);
      String str9 = "mandatory_psp";
      int i4 = localCursor.getColumnIndexOrThrow(str9);
      String str10 = "sim_index";
      int i5 = localCursor.getColumnIndexOrThrow(str10);
      String str11 = "sms_count";
      int i6 = localCursor.getColumnIndexOrThrow(str11);
      String str12 = "popular";
      int i7 = localCursor.getColumnIndexOrThrow(str12);
      localArrayList = new java/util/ArrayList;
      int i8 = localCursor.getCount();
      localArrayList.<init>(i8);
      for (;;)
      {
        boolean bool1 = localCursor.moveToNext();
        if (!bool1) {
          break;
        }
        String str13 = localCursor.getString(i);
        String str14 = localCursor.getString(j);
        String str15 = localCursor.getString(k);
        String str16 = localCursor.getString(m);
        String str17 = localCursor.getString(n);
        int i9 = localCursor.getInt(i1);
        boolean bool2;
        if (i9 != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        String str18 = localCursor.getString(i2);
        i9 = localCursor.getInt(i3);
        boolean bool3;
        if (i9 != 0) {
          bool3 = true;
        } else {
          bool3 = false;
        }
        String str19 = localCursor.getString(i4);
        int i10 = localCursor.getInt(i5);
        int i11 = localCursor.getInt(i6);
        i9 = localCursor.getInt(i7);
        boolean bool4;
        if (i9 != 0) {
          bool4 = true;
        } else {
          bool4 = false;
        }
        localObject1 = new com/truecaller/truepay/app/ui/registrationv2/data/BankData;
        ((BankData)localObject1).<init>(str13, str14, str15, str16, str17, bool2, str18, bool3, str19, i10, i11, bool4);
        localArrayList.add(localObject1);
      }
      return localArrayList;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final List a(List paramList)
  {
    Object localObject = a;
    ((f)localObject).d();
    try
    {
      localObject = b;
      paramList = ((c)localObject).b(paramList);
      localObject = a;
      ((f)localObject).f();
      return paramList;
    }
    finally
    {
      a.e();
    }
  }
  
  public final List b()
  {
    int i = 1;
    i locali = i.a("SELECT * FROM bank_list WHERE is_popular = ? ORDER BY popularity_index ASC", i);
    long l = 1L;
    locali.a(i, l);
    Object localObject1 = this;
    Cursor localCursor = a.a(locali);
    String str1 = "bank_symbol";
    try
    {
      int j = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "account_provider_id";
      int k = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "name";
      int m = localCursor.getColumnIndexOrThrow(str3);
      String str4 = "id";
      int n = localCursor.getColumnIndexOrThrow(str4);
      String str5 = "iin";
      int i1 = localCursor.getColumnIndexOrThrow(str5);
      String str6 = "is_popular";
      int i2 = localCursor.getColumnIndexOrThrow(str6);
      String str7 = "popularity_index";
      int i3 = localCursor.getColumnIndexOrThrow(str7);
      String str8 = "upi_pin_required";
      int i4 = localCursor.getColumnIndexOrThrow(str8);
      String str9 = "mandatory_psp";
      int i5 = localCursor.getColumnIndexOrThrow(str9);
      String str10 = "sim_index";
      int i6 = localCursor.getColumnIndexOrThrow(str10);
      String str11 = "sms_count";
      int i7 = localCursor.getColumnIndexOrThrow(str11);
      String str12 = "popular";
      int i8 = localCursor.getColumnIndexOrThrow(str12);
      ArrayList localArrayList = new java/util/ArrayList;
      int i9 = localCursor.getCount();
      localArrayList.<init>(i9);
      for (;;)
      {
        boolean bool1 = localCursor.moveToNext();
        if (!bool1) {
          break;
        }
        String str13 = localCursor.getString(j);
        String str14 = localCursor.getString(k);
        String str15 = localCursor.getString(m);
        String str16 = localCursor.getString(n);
        String str17 = localCursor.getString(i1);
        int i10 = localCursor.getInt(i2);
        boolean bool2;
        if (i10 != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        String str18 = localCursor.getString(i3);
        i10 = localCursor.getInt(i4);
        boolean bool3;
        if (i10 != 0) {
          bool3 = true;
        } else {
          bool3 = false;
        }
        String str19 = localCursor.getString(i5);
        int i11 = localCursor.getInt(i6);
        int i12 = localCursor.getInt(i7);
        i10 = localCursor.getInt(i8);
        boolean bool4;
        if (i10 != 0) {
          bool4 = true;
        } else {
          bool4 = false;
        }
        localObject1 = new com/truecaller/truepay/app/ui/registrationv2/data/BankData;
        ((BankData)localObject1).<init>(str13, str14, str15, str16, str17, bool2, str18, bool3, str19, i11, i12, bool4);
        localArrayList.add(localObject1);
      }
      return localArrayList;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */