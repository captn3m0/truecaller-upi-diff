package com.truecaller.truepay.app.ui.registrationv2.data;

import c.g.b.k;

public final class PayAccount
{
  private int _id;
  private final String accountId;
  private final String accountNumber;
  private final String accountProvideId;
  private final String bankId;
  private final String bankRegName;
  private final String bankSymbol;
  private final String ifsc;
  private final String iin;
  private final boolean isPopular;
  private final String name;
  private final String ownAccountVpa;
  private final int popularityIndex;
  private final boolean primary;
  private final String vpa;
  
  public PayAccount(int paramInt1, boolean paramBoolean1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, boolean paramBoolean2, int paramInt2, String paramString11)
  {
    _id = paramInt1;
    primary = paramBoolean1;
    accountNumber = paramString1;
    ifsc = paramString2;
    vpa = paramString3;
    ownAccountVpa = paramString4;
    accountId = paramString5;
    accountProvideId = paramString6;
    bankSymbol = paramString7;
    name = paramString8;
    bankId = paramString9;
    iin = paramString10;
    isPopular = paramBoolean2;
    popularityIndex = paramInt2;
    bankRegName = paramString11;
  }
  
  public final int component1()
  {
    return _id;
  }
  
  public final String component10()
  {
    return name;
  }
  
  public final String component11()
  {
    return bankId;
  }
  
  public final String component12()
  {
    return iin;
  }
  
  public final boolean component13()
  {
    return isPopular;
  }
  
  public final int component14()
  {
    return popularityIndex;
  }
  
  public final String component15()
  {
    return bankRegName;
  }
  
  public final boolean component2()
  {
    return primary;
  }
  
  public final String component3()
  {
    return accountNumber;
  }
  
  public final String component4()
  {
    return ifsc;
  }
  
  public final String component5()
  {
    return vpa;
  }
  
  public final String component6()
  {
    return ownAccountVpa;
  }
  
  public final String component7()
  {
    return accountId;
  }
  
  public final String component8()
  {
    return accountProvideId;
  }
  
  public final String component9()
  {
    return bankSymbol;
  }
  
  public final PayAccount copy(int paramInt1, boolean paramBoolean1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, boolean paramBoolean2, int paramInt2, String paramString11)
  {
    k.b(paramString1, "accountNumber");
    k.b(paramString2, "ifsc");
    k.b(paramString3, "vpa");
    k.b(paramString4, "ownAccountVpa");
    k.b(paramString5, "accountId");
    k.b(paramString6, "accountProvideId");
    k.b(paramString7, "bankSymbol");
    k.b(paramString8, "name");
    k.b(paramString9, "bankId");
    k.b(paramString10, "iin");
    k.b(paramString11, "bankRegName");
    PayAccount localPayAccount = new com/truecaller/truepay/app/ui/registrationv2/data/PayAccount;
    localPayAccount.<init>(paramInt1, paramBoolean1, paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, paramString9, paramString10, paramBoolean2, paramInt2, paramString11);
    return localPayAccount;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof PayAccount;
      if (bool2)
      {
        paramObject = (PayAccount)paramObject;
        int i = _id;
        int k = _id;
        String str1;
        if (i == k)
        {
          i = 1;
        }
        else
        {
          i = 0;
          str1 = null;
        }
        if (i != 0)
        {
          boolean bool3 = primary;
          boolean bool4 = primary;
          if (bool3 == bool4)
          {
            bool3 = true;
          }
          else
          {
            bool3 = false;
            str1 = null;
          }
          if (bool3)
          {
            str1 = accountNumber;
            String str2 = accountNumber;
            bool3 = k.a(str1, str2);
            if (bool3)
            {
              str1 = ifsc;
              str2 = ifsc;
              bool3 = k.a(str1, str2);
              if (bool3)
              {
                str1 = vpa;
                str2 = vpa;
                bool3 = k.a(str1, str2);
                if (bool3)
                {
                  str1 = ownAccountVpa;
                  str2 = ownAccountVpa;
                  bool3 = k.a(str1, str2);
                  if (bool3)
                  {
                    str1 = accountId;
                    str2 = accountId;
                    bool3 = k.a(str1, str2);
                    if (bool3)
                    {
                      str1 = accountProvideId;
                      str2 = accountProvideId;
                      bool3 = k.a(str1, str2);
                      if (bool3)
                      {
                        str1 = bankSymbol;
                        str2 = bankSymbol;
                        bool3 = k.a(str1, str2);
                        if (bool3)
                        {
                          str1 = name;
                          str2 = name;
                          bool3 = k.a(str1, str2);
                          if (bool3)
                          {
                            str1 = bankId;
                            str2 = bankId;
                            bool3 = k.a(str1, str2);
                            if (bool3)
                            {
                              str1 = iin;
                              str2 = iin;
                              bool3 = k.a(str1, str2);
                              if (bool3)
                              {
                                bool3 = isPopular;
                                bool4 = isPopular;
                                if (bool3 == bool4)
                                {
                                  bool3 = true;
                                }
                                else
                                {
                                  bool3 = false;
                                  str1 = null;
                                }
                                if (bool3)
                                {
                                  int j = popularityIndex;
                                  int m = popularityIndex;
                                  if (j == m)
                                  {
                                    j = 1;
                                  }
                                  else
                                  {
                                    j = 0;
                                    str1 = null;
                                  }
                                  if (j != 0)
                                  {
                                    str1 = bankRegName;
                                    paramObject = bankRegName;
                                    boolean bool5 = k.a(str1, paramObject);
                                    if (bool5) {
                                      return bool1;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getAccountId()
  {
    return accountId;
  }
  
  public final String getAccountNumber()
  {
    return accountNumber;
  }
  
  public final String getAccountProvideId()
  {
    return accountProvideId;
  }
  
  public final String getBankId()
  {
    return bankId;
  }
  
  public final String getBankRegName()
  {
    return bankRegName;
  }
  
  public final String getBankSymbol()
  {
    return bankSymbol;
  }
  
  public final String getIfsc()
  {
    return ifsc;
  }
  
  public final String getIin()
  {
    return iin;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final String getOwnAccountVpa()
  {
    return ownAccountVpa;
  }
  
  public final int getPopularityIndex()
  {
    return popularityIndex;
  }
  
  public final boolean getPrimary()
  {
    return primary;
  }
  
  public final String getVpa()
  {
    return vpa;
  }
  
  public final int get_id()
  {
    return _id;
  }
  
  public final int hashCode()
  {
    int i = _id * 31;
    int j = primary;
    if (j != 0) {
      j = 1;
    }
    i = (i + j) * 31;
    String str = accountNumber;
    int i10 = 0;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = ifsc;
    int m;
    if (str != null)
    {
      m = str.hashCode();
    }
    else
    {
      m = 0;
      str = null;
    }
    i = (i + m) * 31;
    str = vpa;
    int n;
    if (str != null)
    {
      n = str.hashCode();
    }
    else
    {
      n = 0;
      str = null;
    }
    i = (i + n) * 31;
    str = ownAccountVpa;
    int i1;
    if (str != null)
    {
      i1 = str.hashCode();
    }
    else
    {
      i1 = 0;
      str = null;
    }
    i = (i + i1) * 31;
    str = accountId;
    int i2;
    if (str != null)
    {
      i2 = str.hashCode();
    }
    else
    {
      i2 = 0;
      str = null;
    }
    i = (i + i2) * 31;
    str = accountProvideId;
    int i3;
    if (str != null)
    {
      i3 = str.hashCode();
    }
    else
    {
      i3 = 0;
      str = null;
    }
    i = (i + i3) * 31;
    str = bankSymbol;
    int i4;
    if (str != null)
    {
      i4 = str.hashCode();
    }
    else
    {
      i4 = 0;
      str = null;
    }
    i = (i + i4) * 31;
    str = name;
    int i5;
    if (str != null)
    {
      i5 = str.hashCode();
    }
    else
    {
      i5 = 0;
      str = null;
    }
    i = (i + i5) * 31;
    str = bankId;
    int i6;
    if (str != null)
    {
      i6 = str.hashCode();
    }
    else
    {
      i6 = 0;
      str = null;
    }
    i = (i + i6) * 31;
    str = iin;
    int i7;
    if (str != null)
    {
      i7 = str.hashCode();
    }
    else
    {
      i7 = 0;
      str = null;
    }
    i = (i + i7) * 31;
    int i8 = isPopular;
    if (i8 != 0) {
      i8 = 1;
    }
    i = (i + i8) * 31;
    int i9 = popularityIndex;
    i = (i + i9) * 31;
    str = bankRegName;
    if (str != null) {
      i10 = str.hashCode();
    }
    return i + i10;
  }
  
  public final boolean isPopular()
  {
    return isPopular;
  }
  
  public final void set_id(int paramInt)
  {
    _id = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PayAccount(_id=");
    int i = _id;
    localStringBuilder.append(i);
    localStringBuilder.append(", primary=");
    boolean bool = primary;
    localStringBuilder.append(bool);
    localStringBuilder.append(", accountNumber=");
    String str = accountNumber;
    localStringBuilder.append(str);
    localStringBuilder.append(", ifsc=");
    str = ifsc;
    localStringBuilder.append(str);
    localStringBuilder.append(", vpa=");
    str = vpa;
    localStringBuilder.append(str);
    localStringBuilder.append(", ownAccountVpa=");
    str = ownAccountVpa;
    localStringBuilder.append(str);
    localStringBuilder.append(", accountId=");
    str = accountId;
    localStringBuilder.append(str);
    localStringBuilder.append(", accountProvideId=");
    str = accountProvideId;
    localStringBuilder.append(str);
    localStringBuilder.append(", bankSymbol=");
    str = bankSymbol;
    localStringBuilder.append(str);
    localStringBuilder.append(", name=");
    str = name;
    localStringBuilder.append(str);
    localStringBuilder.append(", bankId=");
    str = bankId;
    localStringBuilder.append(str);
    localStringBuilder.append(", iin=");
    str = iin;
    localStringBuilder.append(str);
    localStringBuilder.append(", isPopular=");
    bool = isPopular;
    localStringBuilder.append(bool);
    localStringBuilder.append(", popularityIndex=");
    int j = popularityIndex;
    localStringBuilder.append(j);
    localStringBuilder.append(", bankRegName=");
    str = bankRegName;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.PayAccount
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */