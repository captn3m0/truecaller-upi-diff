package com.truecaller.truepay.app.ui.registrationv2.data;

import c.g.b.k;
import java.util.Arrays;

public final class CheckDeviceRequest
{
  private final String[] device_ids;
  private final String psp;
  private final String[] sim_ids;
  
  public CheckDeviceRequest(String[] paramArrayOfString1, String[] paramArrayOfString2, String paramString)
  {
    device_ids = paramArrayOfString1;
    sim_ids = paramArrayOfString2;
    psp = paramString;
  }
  
  public final String[] component1()
  {
    return device_ids;
  }
  
  public final String[] component2()
  {
    return sim_ids;
  }
  
  public final String component3()
  {
    return psp;
  }
  
  public final CheckDeviceRequest copy(String[] paramArrayOfString1, String[] paramArrayOfString2, String paramString)
  {
    k.b(paramArrayOfString1, "device_ids");
    k.b(paramArrayOfString2, "sim_ids");
    CheckDeviceRequest localCheckDeviceRequest = new com/truecaller/truepay/app/ui/registrationv2/data/CheckDeviceRequest;
    localCheckDeviceRequest.<init>(paramArrayOfString1, paramArrayOfString2, paramString);
    return localCheckDeviceRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof CheckDeviceRequest;
      if (bool1)
      {
        paramObject = (CheckDeviceRequest)paramObject;
        Object localObject = device_ids;
        String[] arrayOfString = device_ids;
        bool1 = k.a(localObject, arrayOfString);
        if (bool1)
        {
          localObject = sim_ids;
          arrayOfString = sim_ids;
          bool1 = k.a(localObject, arrayOfString);
          if (bool1)
          {
            localObject = psp;
            paramObject = psp;
            boolean bool2 = k.a(localObject, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String[] getDevice_ids()
  {
    return device_ids;
  }
  
  public final String getPsp()
  {
    return psp;
  }
  
  public final String[] getSim_ids()
  {
    return sim_ids;
  }
  
  public final int hashCode()
  {
    String[] arrayOfString = device_ids;
    int i = 0;
    if (arrayOfString != null)
    {
      j = Arrays.hashCode(arrayOfString);
    }
    else
    {
      j = 0;
      arrayOfString = null;
    }
    j *= 31;
    Object localObject = sim_ids;
    int k;
    if (localObject != null)
    {
      k = Arrays.hashCode((Object[])localObject);
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = psp;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CheckDeviceRequest(device_ids=");
    String str = Arrays.toString(device_ids);
    localStringBuilder.append(str);
    localStringBuilder.append(", sim_ids=");
    str = Arrays.toString(sim_ids);
    localStringBuilder.append(str);
    localStringBuilder.append(", psp=");
    str = psp;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */