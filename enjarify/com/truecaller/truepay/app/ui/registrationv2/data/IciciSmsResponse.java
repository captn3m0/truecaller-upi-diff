package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class IciciSmsResponse
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String content;
  private final String expiry;
  private final String short_code;
  private final String sms_prefix;
  
  static
  {
    IciciSmsResponse.a locala = new com/truecaller/truepay/app/ui/registrationv2/data/IciciSmsResponse$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public IciciSmsResponse(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    sms_prefix = paramString1;
    content = paramString2;
    short_code = paramString3;
    expiry = paramString4;
  }
  
  public final String component1()
  {
    return sms_prefix;
  }
  
  public final String component2()
  {
    return content;
  }
  
  public final String component3()
  {
    return short_code;
  }
  
  public final String component4()
  {
    return expiry;
  }
  
  public final IciciSmsResponse copy(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "sms_prefix");
    k.b(paramString2, "content");
    k.b(paramString3, "short_code");
    k.b(paramString4, "expiry");
    IciciSmsResponse localIciciSmsResponse = new com/truecaller/truepay/app/ui/registrationv2/data/IciciSmsResponse;
    localIciciSmsResponse.<init>(paramString1, paramString2, paramString3, paramString4);
    return localIciciSmsResponse;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IciciSmsResponse;
      if (bool1)
      {
        paramObject = (IciciSmsResponse)paramObject;
        String str1 = sms_prefix;
        String str2 = sms_prefix;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = content;
          str2 = content;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = short_code;
            str2 = short_code;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = expiry;
              paramObject = expiry;
              boolean bool2 = k.a(str1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final String getContent()
  {
    return content;
  }
  
  public final String getExpiry()
  {
    return expiry;
  }
  
  public final String getShort_code()
  {
    return short_code;
  }
  
  public final String getSms_prefix()
  {
    return sms_prefix;
  }
  
  public final int hashCode()
  {
    String str1 = sms_prefix;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = content;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = short_code;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = expiry;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IciciSmsResponse(sms_prefix=");
    String str = sms_prefix;
    localStringBuilder.append(str);
    localStringBuilder.append(", content=");
    str = content;
    localStringBuilder.append(str);
    localStringBuilder.append(", short_code=");
    str = short_code;
    localStringBuilder.append(str);
    localStringBuilder.append(", expiry=");
    str = expiry;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = sms_prefix;
    paramParcel.writeString(str);
    str = content;
    paramParcel.writeString(str);
    str = short_code;
    paramParcel.writeString(str);
    str = expiry;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.IciciSmsResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */