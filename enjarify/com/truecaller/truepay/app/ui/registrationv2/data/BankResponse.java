package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class BankResponse
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final List banks;
  private final String logo_base_url;
  private final List popular_banks;
  
  static
  {
    BankResponse.a locala = new com/truecaller/truepay/app/ui/registrationv2/data/BankResponse$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public BankResponse(List paramList1, List paramList2, String paramString)
  {
    banks = paramList1;
    popular_banks = paramList2;
    logo_base_url = paramString;
  }
  
  public final List component1()
  {
    return banks;
  }
  
  public final List component2()
  {
    return popular_banks;
  }
  
  public final String component3()
  {
    return logo_base_url;
  }
  
  public final BankResponse copy(List paramList1, List paramList2, String paramString)
  {
    k.b(paramList1, "banks");
    k.b(paramList2, "popular_banks");
    BankResponse localBankResponse = new com/truecaller/truepay/app/ui/registrationv2/data/BankResponse;
    localBankResponse.<init>(paramList1, paramList2, paramString);
    return localBankResponse;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BankResponse;
      if (bool1)
      {
        paramObject = (BankResponse)paramObject;
        Object localObject = banks;
        List localList = banks;
        bool1 = k.a(localObject, localList);
        if (bool1)
        {
          localObject = popular_banks;
          localList = popular_banks;
          bool1 = k.a(localObject, localList);
          if (bool1)
          {
            localObject = logo_base_url;
            paramObject = logo_base_url;
            boolean bool2 = k.a(localObject, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final List getBanks()
  {
    return banks;
  }
  
  public final String getLogo_base_url()
  {
    return logo_base_url;
  }
  
  public final List getPopular_banks()
  {
    return popular_banks;
  }
  
  public final int hashCode()
  {
    List localList = banks;
    int i = 0;
    if (localList != null)
    {
      j = localList.hashCode();
    }
    else
    {
      j = 0;
      localList = null;
    }
    j *= 31;
    Object localObject = popular_banks;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = logo_base_url;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BankResponse(banks=");
    Object localObject = banks;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", popular_banks=");
    localObject = popular_banks;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", logo_base_url=");
    localObject = logo_base_url;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = banks;
    int i = ((Collection)localObject).size();
    paramParcel.writeInt(i);
    localObject = ((Collection)localObject).iterator();
    BankData localBankData;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      localBankData = (BankData)((Iterator)localObject).next();
      localBankData.writeToParcel(paramParcel, 0);
    }
    localObject = popular_banks;
    int j = ((Collection)localObject).size();
    paramParcel.writeInt(j);
    localObject = ((Collection)localObject).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject).hasNext();
      if (!bool2) {
        break;
      }
      localBankData = (BankData)((Iterator)localObject).next();
      localBankData.writeToParcel(paramParcel, 0);
    }
    localObject = logo_base_url;
    paramParcel.writeString((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.BankResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */