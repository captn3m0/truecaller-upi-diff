package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class BankData$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    BankData localBankData = new com/truecaller/truepay/app/ui/registrationv2/data/BankData;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    String str5 = paramParcel.readString();
    int i = paramParcel.readInt();
    boolean bool = false;
    if (i != 0) {
      i = 1;
    } else {
      i = 0;
    }
    Object localObject1 = paramParcel.readString();
    int j = paramParcel.readInt();
    if (j != 0) {
      j = 1;
    } else {
      j = 0;
    }
    String str6 = paramParcel.readString();
    int k = paramParcel.readInt();
    int m = paramParcel.readInt();
    int n = paramParcel.readInt();
    int i1;
    if (n != 0) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    Object localObject2 = localObject1;
    bool = j;
    localObject1 = str6;
    j = k;
    k = i1;
    localBankData.<init>(str1, str2, str3, str4, str5, i, (String)localObject2, bool, str6, j, m, i1);
    return localBankData;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new BankData[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.BankData.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */