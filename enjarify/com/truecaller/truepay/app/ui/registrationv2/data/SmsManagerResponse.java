package com.truecaller.truepay.app.ui.registrationv2.data;

import c.g.b.k;

public final class SmsManagerResponse
{
  private String address;
  private int countDownTimer;
  private String message;
  private String psp;
  private int registrationFlow;
  private String simToken;
  
  public SmsManagerResponse()
  {
    this(0, null, null, null, null, 0, 63, null);
  }
  
  public SmsManagerResponse(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt2)
  {
    countDownTimer = paramInt1;
    message = paramString1;
    address = paramString2;
    psp = paramString3;
    simToken = paramString4;
    registrationFlow = paramInt2;
  }
  
  public final int component1()
  {
    return countDownTimer;
  }
  
  public final String component2()
  {
    return message;
  }
  
  public final String component3()
  {
    return address;
  }
  
  public final String component4()
  {
    return psp;
  }
  
  public final String component5()
  {
    return simToken;
  }
  
  public final int component6()
  {
    return registrationFlow;
  }
  
  public final SmsManagerResponse copy(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt2)
  {
    k.b(paramString1, "message");
    k.b(paramString2, "address");
    k.b(paramString3, "psp");
    k.b(paramString4, "simToken");
    SmsManagerResponse localSmsManagerResponse = new com/truecaller/truepay/app/ui/registrationv2/data/SmsManagerResponse;
    localSmsManagerResponse.<init>(paramInt1, paramString1, paramString2, paramString3, paramString4, paramInt2);
    return localSmsManagerResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof SmsManagerResponse;
      if (bool2)
      {
        paramObject = (SmsManagerResponse)paramObject;
        int i = countDownTimer;
        int k = countDownTimer;
        String str1;
        if (i == k)
        {
          i = 1;
        }
        else
        {
          i = 0;
          str1 = null;
        }
        if (i != 0)
        {
          str1 = message;
          String str2 = message;
          boolean bool3 = k.a(str1, str2);
          if (bool3)
          {
            str1 = address;
            str2 = address;
            bool3 = k.a(str1, str2);
            if (bool3)
            {
              str1 = psp;
              str2 = psp;
              bool3 = k.a(str1, str2);
              if (bool3)
              {
                str1 = simToken;
                str2 = simToken;
                bool3 = k.a(str1, str2);
                if (bool3)
                {
                  int j = registrationFlow;
                  int m = registrationFlow;
                  if (j == m)
                  {
                    m = 1;
                  }
                  else
                  {
                    m = 0;
                    paramObject = null;
                  }
                  if (m != 0) {
                    return bool1;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getAddress()
  {
    return address;
  }
  
  public final int getCountDownTimer()
  {
    return countDownTimer;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final String getPsp()
  {
    return psp;
  }
  
  public final int getRegistrationFlow()
  {
    return registrationFlow;
  }
  
  public final String getSimToken()
  {
    return simToken;
  }
  
  public final int hashCode()
  {
    int i = countDownTimer * 31;
    String str = message;
    int j = 0;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = address;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = psp;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = simToken;
    if (str != null) {
      j = str.hashCode();
    }
    i = (i + j) * 31;
    int k = registrationFlow;
    return i + k;
  }
  
  public final void setAddress(String paramString)
  {
    k.b(paramString, "<set-?>");
    address = paramString;
  }
  
  public final void setCountDownTimer(int paramInt)
  {
    countDownTimer = paramInt;
  }
  
  public final void setMessage(String paramString)
  {
    k.b(paramString, "<set-?>");
    message = paramString;
  }
  
  public final void setPsp(String paramString)
  {
    k.b(paramString, "<set-?>");
    psp = paramString;
  }
  
  public final void setRegistrationFlow(int paramInt)
  {
    registrationFlow = paramInt;
  }
  
  public final void setSimToken(String paramString)
  {
    k.b(paramString, "<set-?>");
    simToken = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SmsManagerResponse(countDownTimer=");
    int i = countDownTimer;
    localStringBuilder.append(i);
    localStringBuilder.append(", message=");
    String str = message;
    localStringBuilder.append(str);
    localStringBuilder.append(", address=");
    str = address;
    localStringBuilder.append(str);
    localStringBuilder.append(", psp=");
    str = psp;
    localStringBuilder.append(str);
    localStringBuilder.append(", simToken=");
    str = simToken;
    localStringBuilder.append(str);
    localStringBuilder.append(", registrationFlow=");
    i = registrationFlow;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.SmsManagerResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */