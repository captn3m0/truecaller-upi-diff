package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SmsCheckDeviceResponse$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    SmsCheckDeviceResponse localSmsCheckDeviceResponse = new com/truecaller/truepay/app/ui/registrationv2/data/SmsCheckDeviceResponse;
    IciciSmsResponse localIciciSmsResponse = (IciciSmsResponse)IciciSmsResponse.CREATOR.createFromParcel(paramParcel);
    paramParcel = (BarodaSmsResponse)BarodaSmsResponse.CREATOR.createFromParcel(paramParcel);
    localSmsCheckDeviceResponse.<init>(localIciciSmsResponse, paramParcel);
    return localSmsCheckDeviceResponse;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new SmsCheckDeviceResponse[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.SmsCheckDeviceResponse.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */