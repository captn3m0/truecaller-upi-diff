package com.truecaller.truepay.app.ui.registrationv2.data;

import c.g.b.k;

public final class RegisterRequest
{
  private final String tmp_token;
  
  public RegisterRequest(String paramString)
  {
    tmp_token = paramString;
  }
  
  public final String component1()
  {
    return tmp_token;
  }
  
  public final RegisterRequest copy(String paramString)
  {
    k.b(paramString, "tmp_token");
    RegisterRequest localRegisterRequest = new com/truecaller/truepay/app/ui/registrationv2/data/RegisterRequest;
    localRegisterRequest.<init>(paramString);
    return localRegisterRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RegisterRequest;
      if (bool1)
      {
        paramObject = (RegisterRequest)paramObject;
        String str = tmp_token;
        paramObject = tmp_token;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getTmp_token()
  {
    return tmp_token;
  }
  
  public final int hashCode()
  {
    String str = tmp_token;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RegisterRequest(tmp_token=");
    String str = tmp_token;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.RegisterRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */