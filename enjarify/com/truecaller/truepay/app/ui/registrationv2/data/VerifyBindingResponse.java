package com.truecaller.truepay.app.ui.registrationv2.data;

import c.g.b.k;

public final class VerifyBindingResponse
{
  private int callCount;
  private final String msisdn;
  private final String name;
  private final String old_psp;
  private final String registration_status;
  private final String user_id;
  
  public VerifyBindingResponse(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt)
  {
    msisdn = paramString1;
    user_id = paramString2;
    registration_status = paramString3;
    name = paramString4;
    old_psp = paramString5;
    callCount = paramInt;
  }
  
  public final String component1()
  {
    return msisdn;
  }
  
  public final String component2()
  {
    return user_id;
  }
  
  public final String component3()
  {
    return registration_status;
  }
  
  public final String component4()
  {
    return name;
  }
  
  public final String component5()
  {
    return old_psp;
  }
  
  public final int component6()
  {
    return callCount;
  }
  
  public final VerifyBindingResponse copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt)
  {
    k.b(paramString1, "msisdn");
    k.b(paramString2, "user_id");
    k.b(paramString3, "registration_status");
    k.b(paramString4, "name");
    k.b(paramString5, "old_psp");
    VerifyBindingResponse localVerifyBindingResponse = new com/truecaller/truepay/app/ui/registrationv2/data/VerifyBindingResponse;
    localVerifyBindingResponse.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramInt);
    return localVerifyBindingResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof VerifyBindingResponse;
      if (bool2)
      {
        paramObject = (VerifyBindingResponse)paramObject;
        String str1 = msisdn;
        String str2 = msisdn;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          str1 = user_id;
          str2 = user_id;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            str1 = registration_status;
            str2 = registration_status;
            bool2 = k.a(str1, str2);
            if (bool2)
            {
              str1 = name;
              str2 = name;
              bool2 = k.a(str1, str2);
              if (bool2)
              {
                str1 = old_psp;
                str2 = old_psp;
                bool2 = k.a(str1, str2);
                if (bool2)
                {
                  int i = callCount;
                  int j = callCount;
                  if (i == j)
                  {
                    j = 1;
                  }
                  else
                  {
                    j = 0;
                    paramObject = null;
                  }
                  if (j != 0) {
                    return bool1;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int getCallCount()
  {
    return callCount;
  }
  
  public final String getMsisdn()
  {
    return msisdn;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final String getOld_psp()
  {
    return old_psp;
  }
  
  public final String getRegistration_status()
  {
    return registration_status;
  }
  
  public final String getUser_id()
  {
    return user_id;
  }
  
  public final int hashCode()
  {
    String str1 = msisdn;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = user_id;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = registration_status;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = name;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = old_psp;
    if (str2 != null) {
      i = str2.hashCode();
    }
    j = (j + i) * 31;
    i = callCount;
    return j + i;
  }
  
  public final void setCallCount(int paramInt)
  {
    callCount = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VerifyBindingResponse(msisdn=");
    String str = msisdn;
    localStringBuilder.append(str);
    localStringBuilder.append(", user_id=");
    str = user_id;
    localStringBuilder.append(str);
    localStringBuilder.append(", registration_status=");
    str = registration_status;
    localStringBuilder.append(str);
    localStringBuilder.append(", name=");
    str = name;
    localStringBuilder.append(str);
    localStringBuilder.append(", old_psp=");
    str = old_psp;
    localStringBuilder.append(str);
    localStringBuilder.append(", callCount=");
    int i = callCount;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.VerifyBindingResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */