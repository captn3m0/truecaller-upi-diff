package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class BarodaSmsResponse$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    BarodaSmsResponse localBarodaSmsResponse = new com/truecaller/truepay/app/ui/registrationv2/data/BarodaSmsResponse;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    paramParcel = paramParcel.readString();
    localBarodaSmsResponse.<init>(str1, str2, paramParcel);
    return localBarodaSmsResponse;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new BarodaSmsResponse[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.BarodaSmsResponse.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */