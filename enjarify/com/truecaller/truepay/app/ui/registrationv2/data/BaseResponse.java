package com.truecaller.truepay.app.ui.registrationv2.data;

import c.g.b.k;

public final class BaseResponse
{
  private final Object data;
  private final String message;
  private final String status;
  
  public BaseResponse(String paramString1, String paramString2, Object paramObject)
  {
    status = paramString1;
    message = paramString2;
    data = paramObject;
  }
  
  public final String component1()
  {
    return status;
  }
  
  public final String component2()
  {
    return message;
  }
  
  public final Object component3()
  {
    return data;
  }
  
  public final BaseResponse copy(String paramString1, String paramString2, Object paramObject)
  {
    k.b(paramString1, "status");
    k.b(paramString2, "message");
    BaseResponse localBaseResponse = new com/truecaller/truepay/app/ui/registrationv2/data/BaseResponse;
    localBaseResponse.<init>(paramString1, paramString2, paramObject);
    return localBaseResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BaseResponse;
      if (bool1)
      {
        paramObject = (BaseResponse)paramObject;
        Object localObject = status;
        String str = status;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = message;
          str = message;
          bool1 = k.a(localObject, str);
          if (bool1)
          {
            localObject = data;
            paramObject = data;
            boolean bool2 = k.a(localObject, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final Object getData()
  {
    return data;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final String getStatus()
  {
    return status;
  }
  
  public final int hashCode()
  {
    String str = status;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = message;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = data;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BaseResponse(status=");
    Object localObject = status;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", message=");
    localObject = message;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", data=");
    localObject = data;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.BaseResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */