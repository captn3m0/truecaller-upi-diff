package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class IciciSmsResponse$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    IciciSmsResponse localIciciSmsResponse = new com/truecaller/truepay/app/ui/registrationv2/data/IciciSmsResponse;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    paramParcel = paramParcel.readString();
    localIciciSmsResponse.<init>(str1, str2, str3, paramParcel);
    return localIciciSmsResponse;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new IciciSmsResponse[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.IciciSmsResponse.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */