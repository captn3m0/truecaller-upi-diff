package com.truecaller.truepay.app.ui.registrationv2.data;

import c.g.b.k;
import com.truecaller.truepay.app.ui.registrationv2.a.e;

public final class j
{
  public final e a;
  private final String b;
  
  public j()
  {
    this(null, null, 3);
  }
  
  private j(String paramString, e parame)
  {
    b = paramString;
    a = parame;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof j;
      if (bool1)
      {
        paramObject = (j)paramObject;
        Object localObject = b;
        String str = b;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = a;
          paramObject = a;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    String str = b;
    int i = 0;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    e locale = a;
    if (locale != null) {
      i = locale.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ResponseData(message=");
    Object localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", result=");
    localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */