package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class CheckDeviceResponse$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    CheckDeviceResponse localCheckDeviceResponse = new com/truecaller/truepay/app/ui/registrationv2/data/CheckDeviceResponse;
    int i = paramParcel.readInt();
    boolean bool1 = true;
    if (i != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    String str1 = paramParcel.readString();
    i = paramParcel.readInt();
    boolean bool3;
    float f;
    if (i != 0)
    {
      bool3 = true;
      f = Float.MIN_VALUE;
    }
    else
    {
      bool3 = false;
      f = 0.0F;
      localObject1 = null;
    }
    i = paramParcel.readInt();
    boolean bool4;
    if (i != 0)
    {
      bool4 = true;
    }
    else
    {
      bool4 = false;
      localObject2 = null;
    }
    Object localObject3 = paramParcel.readString();
    Object localObject4 = paramParcel.readString();
    String str2 = paramParcel.readString();
    Object localObject5 = paramParcel.readString();
    int j = paramParcel.readInt();
    paramParcel = (SmsCheckDeviceResponse)SmsCheckDeviceResponse.CREATOR.createFromParcel(paramParcel);
    bool1 = bool2;
    boolean bool2 = bool3;
    Object localObject1 = localObject3;
    Object localObject2 = localObject4;
    localObject3 = str2;
    localObject4 = localObject5;
    localObject5 = paramParcel;
    localCheckDeviceResponse.<init>(bool1, str1, bool3, bool4, (String)localObject1, (String)localObject2, str2, (String)localObject4, j, paramParcel);
    return localCheckDeviceResponse;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new CheckDeviceResponse[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */