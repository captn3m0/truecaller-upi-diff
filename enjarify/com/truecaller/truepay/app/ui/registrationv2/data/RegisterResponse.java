package com.truecaller.truepay.app.ui.registrationv2.data;

import c.g.b.k;

public final class RegisterResponse
{
  private final String secret_token;
  private final String status;
  private String tc_user_id;
  private final String uuid;
  
  public RegisterResponse(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    status = paramString1;
    uuid = paramString2;
    tc_user_id = paramString3;
    secret_token = paramString4;
  }
  
  public final String component1()
  {
    return status;
  }
  
  public final String component2()
  {
    return uuid;
  }
  
  public final String component3()
  {
    return tc_user_id;
  }
  
  public final String component4()
  {
    return secret_token;
  }
  
  public final RegisterResponse copy(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "status");
    k.b(paramString2, "uuid");
    k.b(paramString3, "tc_user_id");
    k.b(paramString4, "secret_token");
    RegisterResponse localRegisterResponse = new com/truecaller/truepay/app/ui/registrationv2/data/RegisterResponse;
    localRegisterResponse.<init>(paramString1, paramString2, paramString3, paramString4);
    return localRegisterResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RegisterResponse;
      if (bool1)
      {
        paramObject = (RegisterResponse)paramObject;
        String str1 = status;
        String str2 = status;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = uuid;
          str2 = uuid;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = tc_user_id;
            str2 = tc_user_id;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = secret_token;
              paramObject = secret_token;
              boolean bool2 = k.a(str1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final String getSecret_token()
  {
    return secret_token;
  }
  
  public final String getStatus()
  {
    return status;
  }
  
  public final String getTc_user_id()
  {
    return tc_user_id;
  }
  
  public final String getUuid()
  {
    return uuid;
  }
  
  public final int hashCode()
  {
    String str1 = status;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = uuid;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = tc_user_id;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = secret_token;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final void setTc_user_id(String paramString)
  {
    k.b(paramString, "<set-?>");
    tc_user_id = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RegisterResponse(status=");
    String str = status;
    localStringBuilder.append(str);
    localStringBuilder.append(", uuid=");
    str = uuid;
    localStringBuilder.append(str);
    localStringBuilder.append(", tc_user_id=");
    str = tc_user_id;
    localStringBuilder.append(str);
    localStringBuilder.append(", secret_token=");
    str = secret_token;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.RegisterResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */