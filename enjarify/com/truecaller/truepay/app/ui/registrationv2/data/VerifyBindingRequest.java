package com.truecaller.truepay.app.ui.registrationv2.data;

import c.g.b.k;

public final class VerifyBindingRequest
{
  private final String auth_hash;
  private final String bank_id;
  private final String imsi;
  private final String psp;
  private final String selected_device_id;
  private final String selected_sim_id;
  private final boolean sms_sent;
  
  public VerifyBindingRequest(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean, String paramString6)
  {
    auth_hash = paramString1;
    selected_device_id = paramString2;
    selected_sim_id = paramString3;
    psp = paramString4;
    imsi = paramString5;
    sms_sent = paramBoolean;
    bank_id = paramString6;
  }
  
  public final String component1()
  {
    return auth_hash;
  }
  
  public final String component2()
  {
    return selected_device_id;
  }
  
  public final String component3()
  {
    return selected_sim_id;
  }
  
  public final String component4()
  {
    return psp;
  }
  
  public final String component5()
  {
    return imsi;
  }
  
  public final boolean component6()
  {
    return sms_sent;
  }
  
  public final String component7()
  {
    return bank_id;
  }
  
  public final VerifyBindingRequest copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean, String paramString6)
  {
    k.b(paramString1, "auth_hash");
    k.b(paramString2, "selected_device_id");
    k.b(paramString3, "selected_sim_id");
    k.b(paramString4, "psp");
    k.b(paramString5, "imsi");
    VerifyBindingRequest localVerifyBindingRequest = new com/truecaller/truepay/app/ui/registrationv2/data/VerifyBindingRequest;
    localVerifyBindingRequest.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramBoolean, paramString6);
    return localVerifyBindingRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof VerifyBindingRequest;
      if (bool2)
      {
        paramObject = (VerifyBindingRequest)paramObject;
        String str1 = auth_hash;
        String str2 = auth_hash;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          str1 = selected_device_id;
          str2 = selected_device_id;
          bool2 = k.a(str1, str2);
          if (bool2)
          {
            str1 = selected_sim_id;
            str2 = selected_sim_id;
            bool2 = k.a(str1, str2);
            if (bool2)
            {
              str1 = psp;
              str2 = psp;
              bool2 = k.a(str1, str2);
              if (bool2)
              {
                str1 = imsi;
                str2 = imsi;
                bool2 = k.a(str1, str2);
                if (bool2)
                {
                  bool2 = sms_sent;
                  boolean bool3 = sms_sent;
                  if (bool2 == bool3)
                  {
                    bool2 = true;
                  }
                  else
                  {
                    bool2 = false;
                    str1 = null;
                  }
                  if (bool2)
                  {
                    str1 = bank_id;
                    paramObject = bank_id;
                    boolean bool4 = k.a(str1, paramObject);
                    if (bool4) {
                      return bool1;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getAuth_hash()
  {
    return auth_hash;
  }
  
  public final String getBank_id()
  {
    return bank_id;
  }
  
  public final String getImsi()
  {
    return imsi;
  }
  
  public final String getPsp()
  {
    return psp;
  }
  
  public final String getSelected_device_id()
  {
    return selected_device_id;
  }
  
  public final String getSelected_sim_id()
  {
    return selected_sim_id;
  }
  
  public final boolean getSms_sent()
  {
    return sms_sent;
  }
  
  public final int hashCode()
  {
    String str1 = auth_hash;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = selected_device_id;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = selected_sim_id;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = psp;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = imsi;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    int m = sms_sent;
    if (m != 0) {
      m = 1;
    }
    j = (j + m) * 31;
    str2 = bank_id;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VerifyBindingRequest(auth_hash=");
    String str = auth_hash;
    localStringBuilder.append(str);
    localStringBuilder.append(", selected_device_id=");
    str = selected_device_id;
    localStringBuilder.append(str);
    localStringBuilder.append(", selected_sim_id=");
    str = selected_sim_id;
    localStringBuilder.append(str);
    localStringBuilder.append(", psp=");
    str = psp;
    localStringBuilder.append(str);
    localStringBuilder.append(", imsi=");
    str = imsi;
    localStringBuilder.append(str);
    localStringBuilder.append(", sms_sent=");
    boolean bool = sms_sent;
    localStringBuilder.append(bool);
    localStringBuilder.append(", bank_id=");
    str = bank_id;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.VerifyBindingRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */