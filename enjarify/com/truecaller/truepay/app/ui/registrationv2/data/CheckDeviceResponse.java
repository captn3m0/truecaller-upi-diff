package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class CheckDeviceResponse
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final boolean device_id_match;
  private final boolean existing_user;
  private final int notification_timeout;
  private final String preferred_psp;
  private final String registered_device_id;
  private final String registered_sim_id;
  private final String registered_user_name;
  private final boolean sim_id_match;
  private final SmsCheckDeviceResponse sms;
  private final String vpa;
  
  static
  {
    CheckDeviceResponse.a locala = new com/truecaller/truepay/app/ui/registrationv2/data/CheckDeviceResponse$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public CheckDeviceResponse(boolean paramBoolean1, String paramString1, boolean paramBoolean2, boolean paramBoolean3, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt, SmsCheckDeviceResponse paramSmsCheckDeviceResponse)
  {
    existing_user = paramBoolean1;
    vpa = paramString1;
    device_id_match = paramBoolean2;
    sim_id_match = paramBoolean3;
    registered_user_name = paramString2;
    registered_sim_id = paramString3;
    registered_device_id = paramString4;
    preferred_psp = paramString5;
    notification_timeout = paramInt;
    sms = paramSmsCheckDeviceResponse;
  }
  
  public final boolean component1()
  {
    return existing_user;
  }
  
  public final SmsCheckDeviceResponse component10()
  {
    return sms;
  }
  
  public final String component2()
  {
    return vpa;
  }
  
  public final boolean component3()
  {
    return device_id_match;
  }
  
  public final boolean component4()
  {
    return sim_id_match;
  }
  
  public final String component5()
  {
    return registered_user_name;
  }
  
  public final String component6()
  {
    return registered_sim_id;
  }
  
  public final String component7()
  {
    return registered_device_id;
  }
  
  public final String component8()
  {
    return preferred_psp;
  }
  
  public final int component9()
  {
    return notification_timeout;
  }
  
  public final CheckDeviceResponse copy(boolean paramBoolean1, String paramString1, boolean paramBoolean2, boolean paramBoolean3, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt, SmsCheckDeviceResponse paramSmsCheckDeviceResponse)
  {
    k.b(paramString1, "vpa");
    k.b(paramString2, "registered_user_name");
    k.b(paramString3, "registered_sim_id");
    k.b(paramString4, "registered_device_id");
    k.b(paramString5, "preferred_psp");
    k.b(paramSmsCheckDeviceResponse, "sms");
    CheckDeviceResponse localCheckDeviceResponse = new com/truecaller/truepay/app/ui/registrationv2/data/CheckDeviceResponse;
    localCheckDeviceResponse.<init>(paramBoolean1, paramString1, paramBoolean2, paramBoolean3, paramString2, paramString3, paramString4, paramString5, paramInt, paramSmsCheckDeviceResponse);
    return localCheckDeviceResponse;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof CheckDeviceResponse;
      if (bool2)
      {
        paramObject = (CheckDeviceResponse)paramObject;
        bool2 = existing_user;
        boolean bool3 = existing_user;
        Object localObject;
        if (bool2 == bool3)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localObject = null;
        }
        if (bool2)
        {
          localObject = vpa;
          String str = vpa;
          bool2 = k.a(localObject, str);
          if (bool2)
          {
            bool2 = device_id_match;
            bool3 = device_id_match;
            if (bool2 == bool3)
            {
              bool2 = true;
            }
            else
            {
              bool2 = false;
              localObject = null;
            }
            if (bool2)
            {
              bool2 = sim_id_match;
              bool3 = sim_id_match;
              if (bool2 == bool3)
              {
                bool2 = true;
              }
              else
              {
                bool2 = false;
                localObject = null;
              }
              if (bool2)
              {
                localObject = registered_user_name;
                str = registered_user_name;
                bool2 = k.a(localObject, str);
                if (bool2)
                {
                  localObject = registered_sim_id;
                  str = registered_sim_id;
                  bool2 = k.a(localObject, str);
                  if (bool2)
                  {
                    localObject = registered_device_id;
                    str = registered_device_id;
                    bool2 = k.a(localObject, str);
                    if (bool2)
                    {
                      localObject = preferred_psp;
                      str = preferred_psp;
                      bool2 = k.a(localObject, str);
                      if (bool2)
                      {
                        int i = notification_timeout;
                        int j = notification_timeout;
                        if (i == j)
                        {
                          i = 1;
                        }
                        else
                        {
                          i = 0;
                          localObject = null;
                        }
                        if (i != 0)
                        {
                          localObject = sms;
                          paramObject = sms;
                          boolean bool4 = k.a(localObject, paramObject);
                          if (bool4) {
                            return bool1;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final boolean getDevice_id_match()
  {
    return device_id_match;
  }
  
  public final boolean getExisting_user()
  {
    return existing_user;
  }
  
  public final int getNotification_timeout()
  {
    return notification_timeout;
  }
  
  public final String getPreferred_psp()
  {
    return preferred_psp;
  }
  
  public final String getRegistered_device_id()
  {
    return registered_device_id;
  }
  
  public final String getRegistered_sim_id()
  {
    return registered_sim_id;
  }
  
  public final String getRegistered_user_name()
  {
    return registered_user_name;
  }
  
  public final boolean getSim_id_match()
  {
    return sim_id_match;
  }
  
  public final SmsCheckDeviceResponse getSms()
  {
    return sms;
  }
  
  public final String getVpa()
  {
    return vpa;
  }
  
  public final int hashCode()
  {
    boolean bool = existing_user;
    int j = 1;
    if (bool) {
      bool = true;
    }
    bool *= true;
    String str = vpa;
    int k = 0;
    int m;
    if (str != null)
    {
      m = str.hashCode();
    }
    else
    {
      m = 0;
      str = null;
    }
    int i = (i + m) * 31;
    int n = device_id_match;
    if (n != 0) {
      n = 1;
    }
    i = (i + n) * 31;
    int i1 = sim_id_match;
    if (i1 == 0) {
      j = i1;
    }
    i = (i + j) * 31;
    Object localObject = registered_user_name;
    if (localObject != null)
    {
      j = localObject.hashCode();
    }
    else
    {
      j = 0;
      localObject = null;
    }
    i = (i + j) * 31;
    localObject = registered_sim_id;
    if (localObject != null)
    {
      j = localObject.hashCode();
    }
    else
    {
      j = 0;
      localObject = null;
    }
    i = (i + j) * 31;
    localObject = registered_device_id;
    if (localObject != null)
    {
      j = localObject.hashCode();
    }
    else
    {
      j = 0;
      localObject = null;
    }
    i = (i + j) * 31;
    localObject = preferred_psp;
    if (localObject != null)
    {
      j = localObject.hashCode();
    }
    else
    {
      j = 0;
      localObject = null;
    }
    i = (i + j) * 31;
    j = notification_timeout;
    i = (i + j) * 31;
    localObject = sms;
    if (localObject != null) {
      k = localObject.hashCode();
    }
    return i + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CheckDeviceResponse(existing_user=");
    boolean bool = existing_user;
    localStringBuilder.append(bool);
    localStringBuilder.append(", vpa=");
    Object localObject = vpa;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", device_id_match=");
    bool = device_id_match;
    localStringBuilder.append(bool);
    localStringBuilder.append(", sim_id_match=");
    bool = sim_id_match;
    localStringBuilder.append(bool);
    localStringBuilder.append(", registered_user_name=");
    localObject = registered_user_name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", registered_sim_id=");
    localObject = registered_sim_id;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", registered_device_id=");
    localObject = registered_device_id;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", preferred_psp=");
    localObject = preferred_psp;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", notification_timeout=");
    int i = notification_timeout;
    localStringBuilder.append(i);
    localStringBuilder.append(", sms=");
    localObject = sms;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    paramInt = existing_user;
    paramParcel.writeInt(paramInt);
    String str = vpa;
    paramParcel.writeString(str);
    paramInt = device_id_match;
    paramParcel.writeInt(paramInt);
    paramInt = sim_id_match;
    paramParcel.writeInt(paramInt);
    str = registered_user_name;
    paramParcel.writeString(str);
    str = registered_sim_id;
    paramParcel.writeString(str);
    str = registered_device_id;
    paramParcel.writeString(str);
    str = preferred_psp;
    paramParcel.writeString(str);
    paramInt = notification_timeout;
    paramParcel.writeInt(paramInt);
    sms.writeToParcel(paramParcel, 0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.CheckDeviceResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */