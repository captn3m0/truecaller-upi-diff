package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SmsCheckDeviceResponse
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final BarodaSmsResponse baroda;
  private final IciciSmsResponse icici;
  
  static
  {
    SmsCheckDeviceResponse.a locala = new com/truecaller/truepay/app/ui/registrationv2/data/SmsCheckDeviceResponse$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public SmsCheckDeviceResponse(IciciSmsResponse paramIciciSmsResponse, BarodaSmsResponse paramBarodaSmsResponse)
  {
    icici = paramIciciSmsResponse;
    baroda = paramBarodaSmsResponse;
  }
  
  public final IciciSmsResponse component1()
  {
    return icici;
  }
  
  public final BarodaSmsResponse component2()
  {
    return baroda;
  }
  
  public final SmsCheckDeviceResponse copy(IciciSmsResponse paramIciciSmsResponse, BarodaSmsResponse paramBarodaSmsResponse)
  {
    k.b(paramIciciSmsResponse, "icici");
    k.b(paramBarodaSmsResponse, "baroda");
    SmsCheckDeviceResponse localSmsCheckDeviceResponse = new com/truecaller/truepay/app/ui/registrationv2/data/SmsCheckDeviceResponse;
    localSmsCheckDeviceResponse.<init>(paramIciciSmsResponse, paramBarodaSmsResponse);
    return localSmsCheckDeviceResponse;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof SmsCheckDeviceResponse;
      if (bool1)
      {
        paramObject = (SmsCheckDeviceResponse)paramObject;
        Object localObject = icici;
        IciciSmsResponse localIciciSmsResponse = icici;
        bool1 = k.a(localObject, localIciciSmsResponse);
        if (bool1)
        {
          localObject = baroda;
          paramObject = baroda;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final BarodaSmsResponse getBaroda()
  {
    return baroda;
  }
  
  public final IciciSmsResponse getIcici()
  {
    return icici;
  }
  
  public final int hashCode()
  {
    IciciSmsResponse localIciciSmsResponse = icici;
    int i = 0;
    int j;
    if (localIciciSmsResponse != null)
    {
      j = localIciciSmsResponse.hashCode();
    }
    else
    {
      j = 0;
      localIciciSmsResponse = null;
    }
    j *= 31;
    BarodaSmsResponse localBarodaSmsResponse = baroda;
    if (localBarodaSmsResponse != null) {
      i = localBarodaSmsResponse.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SmsCheckDeviceResponse(icici=");
    Object localObject = icici;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", baroda=");
    localObject = baroda;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    icici.writeToParcel(paramParcel, 0);
    baroda.writeToParcel(paramParcel, 0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.SmsCheckDeviceResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */