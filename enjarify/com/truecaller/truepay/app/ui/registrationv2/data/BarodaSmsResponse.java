package com.truecaller.truepay.app.ui.registrationv2.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class BarodaSmsResponse
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String encryption_key;
  private final String short_code;
  private final String sms_prefix;
  
  static
  {
    BarodaSmsResponse.a locala = new com/truecaller/truepay/app/ui/registrationv2/data/BarodaSmsResponse$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public BarodaSmsResponse(String paramString1, String paramString2, String paramString3)
  {
    sms_prefix = paramString1;
    short_code = paramString2;
    encryption_key = paramString3;
  }
  
  public final String component1()
  {
    return sms_prefix;
  }
  
  public final String component2()
  {
    return short_code;
  }
  
  public final String component3()
  {
    return encryption_key;
  }
  
  public final BarodaSmsResponse copy(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "sms_prefix");
    k.b(paramString2, "short_code");
    k.b(paramString3, "encryption_key");
    BarodaSmsResponse localBarodaSmsResponse = new com/truecaller/truepay/app/ui/registrationv2/data/BarodaSmsResponse;
    localBarodaSmsResponse.<init>(paramString1, paramString2, paramString3);
    return localBarodaSmsResponse;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof BarodaSmsResponse;
      if (bool1)
      {
        paramObject = (BarodaSmsResponse)paramObject;
        String str1 = sms_prefix;
        String str2 = sms_prefix;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = short_code;
          str2 = short_code;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = encryption_key;
            paramObject = encryption_key;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getEncryption_key()
  {
    return encryption_key;
  }
  
  public final String getShort_code()
  {
    return short_code;
  }
  
  public final String getSms_prefix()
  {
    return sms_prefix;
  }
  
  public final int hashCode()
  {
    String str1 = sms_prefix;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = short_code;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = encryption_key;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BarodaSmsResponse(sms_prefix=");
    String str = sms_prefix;
    localStringBuilder.append(str);
    localStringBuilder.append(", short_code=");
    str = short_code;
    localStringBuilder.append(str);
    localStringBuilder.append(", encryption_key=");
    str = encryption_key;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = sms_prefix;
    paramParcel.writeString(str);
    str = short_code;
    paramParcel.writeString(str);
    str = encryption_key;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.BarodaSmsResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */