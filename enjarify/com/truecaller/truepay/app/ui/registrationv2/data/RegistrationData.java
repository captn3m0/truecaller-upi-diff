package com.truecaller.truepay.app.ui.registrationv2.data;

import c.g.b.k;
import c.g.b.n;
import c.g.b.o;
import c.g.b.w;
import c.l.b;
import c.l.g;
import com.truecaller.truepay.data.e.c;

public final class RegistrationData
{
  private final a authTime$delegate;
  private BankData bankData;
  private CheckDeviceResponse checkDeviceResponse;
  private final e logoBaseUrl$delegate;
  private int notificationTimeout;
  private final f psp$delegate;
  private final c securePreferences;
  private int simIndex;
  
  static
  {
    g[] arrayOfg = new g[3];
    Object localObject = new c/g/b/o;
    b localb = w.a(RegistrationData.class);
    ((o)localObject).<init>(localb, "authTime", "getAuthTime()J");
    localObject = (g)w.a((n)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/o;
    localb = w.a(RegistrationData.class);
    ((o)localObject).<init>(localb, "psp", "getPsp()Ljava/lang/String;");
    localObject = (g)w.a((n)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/o;
    localb = w.a(RegistrationData.class);
    ((o)localObject).<init>(localb, "logoBaseUrl", "getLogoBaseUrl()Ljava/lang/String;");
    localObject = (g)w.a((n)localObject);
    arrayOfg[2] = localObject;
    $$delegatedProperties = arrayOfg;
  }
  
  public RegistrationData(c paramc, BankData paramBankData, int paramInt1, CheckDeviceResponse paramCheckDeviceResponse, int paramInt2)
  {
    securePreferences = paramc;
    bankData = paramBankData;
    simIndex = paramInt1;
    checkDeviceResponse = paramCheckDeviceResponse;
    notificationTimeout = paramInt2;
    paramc = new com/truecaller/truepay/app/ui/registrationv2/data/a;
    paramBankData = securePreferences;
    paramc.<init>(paramBankData);
    authTime$delegate = paramc;
    paramc = new com/truecaller/truepay/app/ui/registrationv2/data/f;
    paramBankData = securePreferences;
    paramc.<init>(paramBankData);
    psp$delegate = paramc;
    paramc = new com/truecaller/truepay/app/ui/registrationv2/data/e;
    paramBankData = securePreferences;
    paramc.<init>(paramBankData);
    logoBaseUrl$delegate = paramc;
  }
  
  public final long getAuthTime()
  {
    a locala = authTime$delegate;
    g localg = $$delegatedProperties[0];
    return ((Number)locala.a(localg)).longValue();
  }
  
  public final BankData getBankData()
  {
    return bankData;
  }
  
  public final CheckDeviceResponse getCheckDeviceResponse()
  {
    return checkDeviceResponse;
  }
  
  public final String getLogoBaseUrl()
  {
    e locale = logoBaseUrl$delegate;
    g localg = $$delegatedProperties[2];
    return (String)locale.a(localg);
  }
  
  public final int getNotificationTimeout()
  {
    return notificationTimeout;
  }
  
  public final String getPsp()
  {
    f localf = psp$delegate;
    g localg = $$delegatedProperties[1];
    return (String)localf.a(localg);
  }
  
  public final c getSecurePreferences()
  {
    return securePreferences;
  }
  
  public final int getSimIndex()
  {
    return simIndex;
  }
  
  public final void setAuthTime(long paramLong)
  {
    a locala = authTime$delegate;
    g localg = $$delegatedProperties[0];
    Long localLong = Long.valueOf(paramLong);
    locala.a(localg, localLong);
  }
  
  public final void setBankData(BankData paramBankData)
  {
    bankData = paramBankData;
  }
  
  public final void setCheckDeviceResponse(CheckDeviceResponse paramCheckDeviceResponse)
  {
    checkDeviceResponse = paramCheckDeviceResponse;
  }
  
  public final void setLogoBaseUrl(String paramString)
  {
    k.b(paramString, "<set-?>");
    e locale = logoBaseUrl$delegate;
    g localg = $$delegatedProperties[2];
    locale.a(localg, paramString);
  }
  
  public final void setNotificationTimeout(int paramInt)
  {
    notificationTimeout = paramInt;
  }
  
  public final void setPsp(String paramString)
  {
    k.b(paramString, "<set-?>");
    f localf = psp$delegate;
    g localg = $$delegatedProperties[1];
    localf.a(localg, paramString);
  }
  
  public final void setSimIndex(int paramInt)
  {
    simIndex = paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.RegistrationData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */