package com.truecaller.truepay.app.ui.registrationv2.data;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final Provider a;
  
  private h(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static h a(Provider paramProvider)
  {
    h localh = new com/truecaller/truepay/app/ui/registrationv2/data/h;
    localh.<init>(paramProvider);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registrationv2.data.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */