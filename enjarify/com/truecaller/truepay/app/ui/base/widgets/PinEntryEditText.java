package com.truecaller.truepay.app.ui.base.widgets;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.content.b;
import android.support.v4.e.f;
import android.support.v4.view.r;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ActionMode.Callback;
import android.view.View.OnClickListener;
import android.view.animation.OvershootInterpolator;
import com.truecaller.truepay.R.attr;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.styleable;
import java.util.Locale;

public class PinEntryEditText
  extends AppCompatEditText
{
  protected int[] A;
  protected ColorStateList B;
  protected String a;
  protected StringBuilder b;
  protected String c;
  protected int d;
  protected float e = 24.0F;
  protected float f;
  protected float g = 4.0F;
  protected float h = 8.0F;
  protected int i;
  protected RectF[] j;
  protected float[] k;
  protected Paint l;
  protected Paint m;
  protected Paint n;
  protected Drawable o;
  protected Rect p;
  protected boolean q;
  protected View.OnClickListener r;
  protected PinEntryEditText.a s;
  protected float t;
  protected float u;
  protected Paint v;
  protected boolean w;
  protected boolean x;
  protected ColorStateList y;
  protected int[][] z;
  
  public PinEntryEditText(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    int i1 = 4;
    i = i1;
    Rect localRect1 = new android/graphics/Rect;
    localRect1.<init>();
    p = localRect1;
    s = null;
    t = 1.0F;
    u = 2.0F;
    localRect1 = null;
    w = false;
    x = false;
    Object localObject1 = new int[i1][];
    int i2 = 1;
    Object localObject2 = new int[i2];
    localObject2[0] = 16842913;
    localObject1[0] = localObject2;
    localObject2 = new int[i2];
    localObject2[0] = 16842914;
    localObject1[i2] = localObject2;
    localObject2 = new int[i2];
    localObject2[0] = 16842908;
    int i3 = 2;
    localObject1[i3] = localObject2;
    localObject2 = new int[i2];
    localObject2[0] = -16842908;
    int i4 = 3;
    float f1 = 4.2E-45F;
    localObject1[i4] = localObject2;
    z = ((int[][])localObject1);
    localObject1 = new int[i1];
    Object tmp184_182 = localObject1;
    Object tmp185_184 = tmp184_182;
    Object tmp185_184 = tmp184_182;
    tmp185_184[0] = -16711936;
    tmp185_184[1] = -65536;
    tmp185_184[2] = -16777216;
    tmp185_184[3] = -7829368;
    A = ((int[])localObject1);
    localObject1 = new android/content/res/ColorStateList;
    localObject2 = z;
    int[] arrayOfInt = A;
    ((ColorStateList)localObject1).<init>((int[][])localObject2, arrayOfInt);
    B = ((ColorStateList)localObject1);
    float f2 = getResourcesgetDisplayMetricsdensity;
    float f3 = t * f2;
    t = f3;
    f3 = u * f2;
    u = f3;
    f3 = e * f2;
    e = f3;
    f3 = h;
    f2 *= f3;
    h = f2;
    localObject1 = R.styleable.PinEntryEditText;
    localObject1 = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject1, 0, 0);
    try
    {
      localObject2 = new android/util/TypedValue;
      ((TypedValue)localObject2).<init>();
      i4 = R.styleable.PinEntryEditText_pinCharacterAnimationType;
      ((TypedArray)localObject1).getValue(i4, (TypedValue)localObject2);
      int i6 = data;
      d = i6;
      i6 = R.styleable.PinEntryEditText_pinCharacterMask;
      localObject2 = ((TypedArray)localObject1).getString(i6);
      a = ((String)localObject2);
      i6 = R.styleable.PinEntryEditText_pinRepeatedHint;
      localObject2 = ((TypedArray)localObject1).getString(i6);
      c = ((String)localObject2);
      i6 = R.styleable.PinEntryEditText_pinLineStroke;
      f1 = t;
      f3 = ((TypedArray)localObject1).getDimension(i6, f1);
      t = f3;
      i6 = R.styleable.PinEntryEditText_pinLineStrokeSelected;
      f1 = u;
      f3 = ((TypedArray)localObject1).getDimension(i6, f1);
      u = f3;
      i6 = R.styleable.PinEntryEditText_pinCharacterSpacing;
      f1 = e;
      f3 = ((TypedArray)localObject1).getDimension(i6, f1);
      e = f3;
      i6 = R.styleable.PinEntryEditText_pinTextBottomPadding;
      f1 = h;
      f3 = ((TypedArray)localObject1).getDimension(i6, f1);
      h = f3;
      i6 = R.styleable.PinEntryEditText_pinBackgroundIsSquare;
      boolean bool2 = q;
      boolean bool3 = ((TypedArray)localObject1).getBoolean(i6, bool2);
      q = bool3;
      int i7 = R.styleable.PinEntryEditText_pinBackgroundDrawable;
      localObject2 = ((TypedArray)localObject1).getDrawable(i7);
      o = ((Drawable)localObject2);
      i7 = R.styleable.PinEntryEditText_pinLineColors;
      localObject2 = ((TypedArray)localObject1).getColorStateList(i7);
      if (localObject2 != null) {
        B = ((ColorStateList)localObject2);
      }
      ((TypedArray)localObject1).recycle();
      localObject1 = new android/graphics/Paint;
      localObject2 = getPaint();
      ((Paint)localObject1).<init>((Paint)localObject2);
      l = ((Paint)localObject1);
      localObject1 = new android/graphics/Paint;
      localObject2 = getPaint();
      ((Paint)localObject1).<init>((Paint)localObject2);
      m = ((Paint)localObject1);
      localObject1 = new android/graphics/Paint;
      localObject2 = getPaint();
      ((Paint)localObject1).<init>((Paint)localObject2);
      n = ((Paint)localObject1);
      localObject1 = new android/graphics/Paint;
      localObject2 = getPaint();
      ((Paint)localObject1).<init>((Paint)localObject2);
      v = ((Paint)localObject1);
      localObject1 = v;
      f3 = t;
      ((Paint)localObject1).setStrokeWidth(f3);
      localObject1 = new android/util/TypedValue;
      ((TypedValue)localObject1).<init>();
      localObject2 = paramContext.getTheme();
      int i5 = R.attr.colorControlActivated;
      ((Resources.Theme)localObject2).resolveAttribute(i5, (TypedValue)localObject1, i2);
      int i8 = data;
      localObject2 = A;
      localObject2[0] = i8;
      boolean bool4 = isInEditMode();
      i7 = -7829368;
      f3 = 0.0F / 0.0F;
      int i9;
      if (bool4)
      {
        i9 = -7829368;
        f2 = 0.0F / 0.0F;
      }
      else
      {
        i9 = R.color.pin_normal;
        i9 = b.c(paramContext, i9);
      }
      arrayOfInt = A;
      arrayOfInt[i2] = i9;
      boolean bool5 = isInEditMode();
      if (!bool5)
      {
        int i10 = R.color.pin_normal;
        i7 = b.c(paramContext, i10);
      }
      A[i3] = i7;
      setBackgroundResource(0);
      localObject1 = "maxLength";
      int i11 = paramAttributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", (String)localObject1, i1);
      i = i11;
      float f4 = i;
      g = f4;
      paramContext = new com/truecaller/truepay/app/ui/base/widgets/PinEntryEditText$1;
      paramContext.<init>(this);
      super.setCustomSelectionActionModeCallback(paramContext);
      paramContext = new com/truecaller/truepay/app/ui/base/widgets/PinEntryEditText$2;
      paramContext.<init>(this);
      super.setOnClickListener(paramContext);
      paramContext = new com/truecaller/truepay/app/ui/base/widgets/PinEntryEditText$3;
      paramContext.<init>(this);
      super.setOnLongClickListener(paramContext);
      i11 = getInputType();
      int i14 = 128;
      i11 &= i14;
      if (i11 == i14)
      {
        paramContext = a;
        boolean bool6 = TextUtils.isEmpty(paramContext);
        if (bool6)
        {
          paramContext = "●";
          a = paramContext;
          break label1048;
        }
      }
      int i12 = getInputType();
      i14 = 16;
      i12 &= i14;
      if (i12 == i14)
      {
        paramContext = a;
        bool7 = TextUtils.isEmpty(paramContext);
        if (bool7)
        {
          paramContext = "●";
          a = paramContext;
        }
      }
      label1048:
      paramContext = a;
      boolean bool7 = TextUtils.isEmpty(paramContext);
      if (!bool7)
      {
        paramContext = getMaskChars();
        b = paramContext;
      }
      paramContext = getPaint();
      paramAttributeSet = "|";
      Rect localRect2 = p;
      paramContext.getTextBounds(paramAttributeSet, 0, i2, localRect2);
      int i13 = d;
      boolean bool1;
      if (i13 < 0) {
        bool1 = false;
      }
      w = bool1;
      setCursorVisible(false);
      return;
    }
    finally
    {
      ((TypedArray)localObject1).recycle();
    }
  }
  
  private int a(int... paramVarArgs)
  {
    return B.getColorForState(paramVarArgs, -7829368);
  }
  
  private CharSequence getFullText()
  {
    String str = a;
    if (str == null) {
      return getText();
    }
    return getMaskChars();
  }
  
  private StringBuilder getMaskChars()
  {
    Object localObject = b;
    if (localObject == null)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      b = ((StringBuilder)localObject);
    }
    localObject = getText();
    int i1 = ((Editable)localObject).length();
    for (;;)
    {
      StringBuilder localStringBuilder = b;
      int i2 = localStringBuilder.length();
      if (i2 == i1) {
        break;
      }
      localStringBuilder = b;
      i2 = localStringBuilder.length();
      if (i2 < i1)
      {
        localStringBuilder = b;
        String str = a;
        localStringBuilder.append(str);
      }
      else
      {
        localStringBuilder = b;
        int i3 = localStringBuilder.length() + -1;
        localStringBuilder.deleteCharAt(i3);
      }
    }
    return b;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    PinEntryEditText localPinEntryEditText = this;
    Canvas localCanvas = paramCanvas;
    CharSequence localCharSequence = getFullText();
    int i1 = localCharSequence.length();
    float[] arrayOfFloat = new float[i1];
    Object localObject1 = getPaint();
    int i2 = 0;
    ((TextPaint)localObject1).getTextWidths(localCharSequence, 0, i1, arrayOfFloat);
    localObject1 = c;
    int i3 = 0;
    float f1 = 0.0F;
    Object localObject2 = null;
    Object localObject3;
    Object localObject4;
    int i10;
    float f2;
    float f3;
    float f4;
    if (localObject1 != null)
    {
      int i7 = ((String)localObject1).length();
      localObject1 = new float[i7];
      localObject3 = getPaint();
      localObject4 = c;
      ((TextPaint)localObject3).getTextWidths((String)localObject4, (float[])localObject1);
      int i8 = localObject1.length;
      i3 = 0;
      f1 = 0.0F;
      localObject2 = null;
      i10 = 0;
      f2 = 0.0F;
      localObject4 = null;
      while (i3 < i8)
      {
        f3 = localObject1[i3];
        f2 += f3;
        i3 += 1;
      }
      f4 = f2;
    }
    else
    {
      f4 = 0.0F;
    }
    int i11 = 0;
    for (;;)
    {
      float f5 = i11;
      f1 = g;
      boolean bool4 = f5 < f1;
      if (!bool4) {
        break;
      }
      localObject1 = o;
      int i12 = 16842914;
      int i13 = -16842908;
      int i14 = 1;
      if (localObject1 != null)
      {
        if (i11 < i1)
        {
          bool4 = true;
          f5 = Float.MIN_VALUE;
        }
        else
        {
          bool4 = false;
          f5 = 0.0F;
          localObject1 = null;
        }
        if (i11 == i1)
        {
          i3 = 1;
          f1 = Float.MIN_VALUE;
        }
        else
        {
          i3 = 0;
          f1 = 0.0F;
          localObject2 = null;
        }
        boolean bool5 = x;
        if (bool5)
        {
          localObject1 = o;
          localObject2 = new int[i14];
          localObject2[0] = i12;
          ((Drawable)localObject1).setState((int[])localObject2);
        }
        else
        {
          bool5 = isFocused();
          if (bool5)
          {
            localObject3 = o;
            localObject4 = new int[i14];
            i15 = 16842908;
            f3 = 2.3693995E-38F;
            localObject4[0] = i15;
            ((Drawable)localObject3).setState((int[])localObject4);
            i9 = 2;
            f6 = 2.8E-45F;
            if (i3 != 0)
            {
              localObject1 = o;
              localObject2 = new int[i9];
              Object tmp376_374 = localObject2;
              tmp376_374[0] = 16842908;
              tmp376_374[1] = 16842913;
              ((Drawable)localObject1).setState((int[])localObject2);
            }
            else if (bool4)
            {
              localObject1 = o;
              localObject2 = new int[i9];
              Object tmp415_413 = localObject2;
              tmp415_413[0] = 16842908;
              tmp415_413[1] = 16842912;
              ((Drawable)localObject1).setState((int[])localObject2);
            }
          }
          else
          {
            localObject1 = o;
            localObject2 = new int[i14];
            localObject2[0] = i13;
            ((Drawable)localObject1).setState((int[])localObject2);
          }
        }
        localObject1 = o;
        localObject2 = j[i11];
        f1 = left;
        i3 = (int)f1;
        localObject3 = j[i11];
        f6 = top;
        i9 = (int)f6;
        localObject4 = j[i11];
        f2 = right;
        i10 = (int)f2;
        RectF localRectF = j[i11];
        f3 = bottom;
        int i15 = (int)f3;
        ((Drawable)localObject1).setBounds(i3, i9, i10, i15);
        localObject1 = o;
        ((Drawable)localObject1).draw(localCanvas);
      }
      localObject1 = j[i11];
      f5 = left;
      f1 = f;
      int i9 = 1073741824;
      float f6 = 2.0F;
      f1 /= f6;
      f5 += f1;
      Object localObject5;
      if (i1 > i11)
      {
        boolean bool1 = w;
        if (bool1)
        {
          int i4 = i1 + -1;
          if (i11 == i4)
          {
            i10 = i11 + 1;
            f1 = arrayOfFloat[i11] / f6;
            f3 = f5 - f1;
            f7 = k[i11];
            localObject3 = m;
            localObject1 = paramCanvas;
            localObject2 = localCharSequence;
            i9 = i11;
            i2 = 1;
            localObject5 = localObject3;
            paramCanvas.drawText(localCharSequence, i11, i10, f3, f7, (Paint)localObject3);
            break label860;
          }
        }
        i2 = 1;
        i10 = i11 + 1;
        f1 = arrayOfFloat[i11] / f6;
        f3 = f5 - f1;
        float f7 = k[i11];
        localObject5 = l;
        localObject1 = paramCanvas;
        localObject2 = localCharSequence;
        i9 = i11;
        paramCanvas.drawText(localCharSequence, i11, i10, f3, f7, (Paint)localObject5);
      }
      else
      {
        i2 = 1;
        localObject2 = c;
        if (localObject2 != null)
        {
          f6 = f4 / f6;
          f5 -= f6;
          localObject3 = k;
          f6 = localObject3[i11];
          localObject4 = n;
          localCanvas.drawText((String)localObject2, f5, f6, (Paint)localObject4);
        }
      }
      label860:
      localObject1 = o;
      if (localObject1 == null)
      {
        if (i11 <= i1)
        {
          bool4 = true;
          f5 = Float.MIN_VALUE;
        }
        else
        {
          bool4 = false;
          f5 = 0.0F;
          localObject1 = null;
        }
        boolean bool2 = x;
        if (bool2)
        {
          localObject1 = v;
          localObject2 = new int[i2];
          i9 = 0;
          f6 = 0.0F;
          localObject3 = null;
          localObject2[0] = i12;
          int i5 = localPinEntryEditText.a((int[])localObject2);
          ((Paint)localObject1).setColor(i5);
          i14 = 0;
          localObject5 = null;
        }
        else
        {
          i9 = 0;
          f6 = 0.0F;
          localObject3 = null;
          boolean bool3 = isFocused();
          int i6;
          if (bool3)
          {
            localObject2 = v;
            f2 = u;
            ((Paint)localObject2).setStrokeWidth(f2);
            localObject2 = v;
            localObject4 = new int[i2];
            localObject4[0] = i13;
            i9 = localPinEntryEditText.a((int[])localObject4);
            ((Paint)localObject2).setColor(i9);
            if (bool4)
            {
              localObject1 = v;
              localObject2 = "#00bb6e";
              i6 = Color.parseColor((String)localObject2);
              ((Paint)localObject1).setColor(i6);
              i14 = 0;
              localObject5 = null;
            }
            else
            {
              i14 = 0;
              localObject5 = null;
            }
          }
          else
          {
            localObject1 = v;
            f1 = t;
            ((Paint)localObject1).setStrokeWidth(f1);
            localObject1 = v;
            localObject2 = new int[i2];
            i14 = 0;
            localObject5 = null;
            localObject2[0] = i13;
            i6 = localPinEntryEditText.a((int[])localObject2);
            ((Paint)localObject1).setColor(i6);
          }
        }
        f1 = j[i11].left;
        f6 = j[i11].top;
        f2 = j[i11].right;
        f3 = j[i11].bottom;
        Paint localPaint = v;
        localObject1 = paramCanvas;
        paramCanvas.drawLine(f1, f6, f2, f3, localPaint);
      }
      else
      {
        i14 = 0;
        localObject5 = null;
      }
      i11 += 1;
      i2 = 0;
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    Object localObject1 = getTextColors();
    y = ((ColorStateList)localObject1);
    localObject1 = y;
    if (localObject1 != null)
    {
      localObject2 = m;
      paramInt1 = ((ColorStateList)localObject1).getDefaultColor();
      ((Paint)localObject2).setColor(paramInt1);
      localObject1 = l;
      localObject2 = y;
      paramInt2 = ((ColorStateList)localObject2).getDefaultColor();
      ((Paint)localObject1).setColor(paramInt2);
      localObject1 = n;
      paramInt2 = getCurrentHintTextColor();
      ((Paint)localObject1).setColor(paramInt2);
    }
    paramInt1 = getWidth();
    paramInt2 = r.k(this);
    paramInt1 -= paramInt2;
    paramInt2 = r.j(this);
    paramInt1 -= paramInt2;
    float f1 = e;
    paramInt3 = 1065353216;
    float f2 = 1.0F;
    paramInt4 = 0;
    float f3 = 2.0F;
    int i1 = f1 < 0.0F;
    if (i1 < 0)
    {
      f4 = paramInt1;
      f1 = g * f3 - f2;
      f4 /= f1;
      f = f4;
    }
    else
    {
      f4 = paramInt1;
      f5 = g;
      f2 = f5 - f2;
      f1 *= f2;
      f4 = (f4 - f1) / f5;
      f = f4;
    }
    float f4 = g;
    Object localObject2 = new RectF[(int)f4];
    j = ((RectF[])localObject2);
    localObject1 = new float[(int)f4];
    k = ((float[])localObject1);
    paramInt1 = getHeight();
    paramInt2 = getPaddingBottom();
    paramInt1 -= paramInt2;
    localObject2 = Locale.getDefault();
    paramInt2 = f.a((Locale)localObject2);
    paramInt3 = 0;
    f2 = 0.0F;
    i1 = 1;
    float f5 = Float.MIN_VALUE;
    if (paramInt2 == i1)
    {
      paramInt2 = 1;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      paramInt2 = 0;
      f1 = 0.0F;
      localObject2 = null;
    }
    float f6;
    if (paramInt2 != 0)
    {
      i1 = -1;
      f5 = 0.0F / 0.0F;
      paramInt2 = getWidth();
      int i2 = r.j(this);
      f1 = paramInt2 - i2;
      f6 = f;
      f1 -= f6;
      paramInt2 = (int)f1;
    }
    else
    {
      paramInt2 = r.j(this);
    }
    for (;;)
    {
      f6 = paramInt3;
      float f7 = g;
      boolean bool1 = f6 < f7;
      if (!bool1) {
        break;
      }
      Object localObject3 = j;
      RectF localRectF = new android/graphics/RectF;
      f1 = paramInt2;
      float f8 = paramInt1;
      float f9 = f + f1;
      localRectF.<init>(f1, f8, f9, f8);
      localObject3[paramInt3] = localRectF;
      localObject3 = o;
      if (localObject3 != null)
      {
        bool1 = q;
        if (bool1)
        {
          localObject3 = j[paramInt3];
          int i3 = getPaddingTop();
          f7 = i3;
          top = f7;
          localObject3 = j;
          localRectF = localObject3[paramInt3];
          localObject3 = localObject3[paramInt3];
          f6 = ((RectF)localObject3).height() + f1;
          right = f6;
        }
        else
        {
          localObject3 = j[paramInt3];
          f7 = top;
          Rect localRect = p;
          int i4 = localRect.height();
          f8 = i4;
          f9 = h * f3;
          f8 += f9;
          f7 -= f8;
          top = f7;
        }
      }
      f6 = e;
      boolean bool2 = f6 < 0.0F;
      if (bool2)
      {
        f6 = i1;
        f7 = f;
        f6 = f6 * f7 * f3;
        f1 += f6;
        paramInt2 = (int)f1;
      }
      else
      {
        f7 = i1;
        f8 = f + f6;
        f7 *= f8;
        f1 += f7;
        paramInt2 = (int)f1;
      }
      localObject3 = k;
      localRectF = j[paramInt3];
      f7 = bottom;
      f8 = h;
      f7 -= f8;
      localObject3[paramInt3] = f7;
      paramInt3 += 1;
    }
  }
  
  protected void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    setError(false);
    Object localObject1 = j;
    if (localObject1 != null)
    {
      boolean bool = w;
      if (bool)
      {
        int i1 = d;
        int i2 = -1;
        float f1 = 0.0F / 0.0F;
        if (i1 == i2)
        {
          invalidate();
          return;
        }
        if (paramInt3 > paramInt2)
        {
          paramInt2 = 1;
          paramInt3 = 2;
          if (i1 == 0)
          {
            paramCharSequence = new float[paramInt3];
            paramCharSequence[0] = 1.0F;
            float f2 = getPaint().getTextSize();
            paramCharSequence[paramInt2] = f2;
            paramCharSequence = ValueAnimator.ofFloat(paramCharSequence);
            long l1 = 200L;
            paramCharSequence.setDuration(l1);
            localObject2 = new android/view/animation/OvershootInterpolator;
            ((OvershootInterpolator)localObject2).<init>();
            paramCharSequence.setInterpolator((TimeInterpolator)localObject2);
            localObject2 = new com/truecaller/truepay/app/ui/base/widgets/PinEntryEditText$4;
            ((PinEntryEditText.4)localObject2).<init>(this);
            paramCharSequence.addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
            localObject2 = getText();
            paramInt1 = ((Editable)localObject2).length();
            paramInt2 = i;
            if (paramInt1 == paramInt2)
            {
              localObject2 = s;
              if (localObject2 != null)
              {
                localObject2 = new com/truecaller/truepay/app/ui/base/widgets/PinEntryEditText$5;
                ((PinEntryEditText.5)localObject2).<init>(this);
                paramCharSequence.addListener((Animator.AnimatorListener)localObject2);
              }
            }
            paramCharSequence.start();
            return;
          }
          Object localObject3 = k;
          float f3 = bottom;
          f1 = h;
          f3 -= f1;
          localObject3[paramInt1] = f3;
          localObject1 = new float[paramInt3];
          float f4 = k[paramInt1];
          TextPaint localTextPaint = getPaint();
          f1 = localTextPaint.getTextSize();
          f4 += f1;
          localObject1[0] = f4;
          f4 = k[paramInt1];
          localObject1[paramInt2] = f4;
          localObject1 = ValueAnimator.ofFloat((float[])localObject1);
          long l2 = 300L;
          ((ValueAnimator)localObject1).setDuration(l2);
          Object localObject4 = new android/view/animation/OvershootInterpolator;
          ((OvershootInterpolator)localObject4).<init>();
          ((ValueAnimator)localObject1).setInterpolator((TimeInterpolator)localObject4);
          localObject4 = new com/truecaller/truepay/app/ui/base/widgets/PinEntryEditText$6;
          ((PinEntryEditText.6)localObject4).<init>(this, paramInt1);
          ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject4);
          localObject2 = m;
          int i3 = 255;
          ((Paint)localObject2).setAlpha(i3);
          localObject2 = new int[paramInt3];
          Object tmp373_371 = localObject2;
          tmp373_371[0] = 0;
          tmp373_371[1] = 'ÿ';
          localObject2 = ValueAnimator.ofInt((int[])localObject2);
          ((ValueAnimator)localObject2).setDuration(l2);
          localObject3 = new com/truecaller/truepay/app/ui/base/widgets/PinEntryEditText$7;
          ((PinEntryEditText.7)localObject3).<init>(this);
          ((ValueAnimator)localObject2).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject3);
          localObject3 = new android/animation/AnimatorSet;
          ((AnimatorSet)localObject3).<init>();
          int i4 = paramCharSequence.length();
          i2 = i;
          if (i4 == i2)
          {
            paramCharSequence = s;
            if (paramCharSequence != null)
            {
              paramCharSequence = new com/truecaller/truepay/app/ui/base/widgets/PinEntryEditText$8;
              paramCharSequence.<init>(this);
              ((AnimatorSet)localObject3).addListener(paramCharSequence);
            }
          }
          paramCharSequence = new Animator[paramInt3];
          paramCharSequence[0] = localObject1;
          paramCharSequence[paramInt2] = localObject2;
          ((AnimatorSet)localObject3).playTogether(paramCharSequence);
          ((AnimatorSet)localObject3).start();
        }
        return;
      }
    }
    Object localObject2 = s;
    if (localObject2 != null) {
      paramCharSequence.length();
    }
  }
  
  public void setAnimateText(boolean paramBoolean)
  {
    w = paramBoolean;
  }
  
  public void setCustomSelectionActionModeCallback(ActionMode.Callback paramCallback)
  {
    paramCallback = new java/lang/RuntimeException;
    paramCallback.<init>("setCustomSelectionActionModeCallback() not supported.");
    throw paramCallback;
  }
  
  public void setError(boolean paramBoolean)
  {
    x = paramBoolean;
  }
  
  public void setMaxLength(int paramInt)
  {
    i = paramInt;
    float f1 = paramInt;
    g = f1;
    InputFilter[] arrayOfInputFilter = new InputFilter[1];
    InputFilter.LengthFilter localLengthFilter = new android/text/InputFilter$LengthFilter;
    localLengthFilter.<init>(paramInt);
    arrayOfInputFilter[0] = localLengthFilter;
    setFilters(arrayOfInputFilter);
    setText(null);
    invalidate();
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener)
  {
    r = paramOnClickListener;
  }
  
  public void setOnPinEnteredListener(PinEntryEditText.a parama)
  {
    s = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.PinEntryEditText
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */