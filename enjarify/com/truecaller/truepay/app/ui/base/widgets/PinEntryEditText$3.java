package com.truecaller.truepay.app.ui.base.widgets;

import android.text.Editable;
import android.view.View;
import android.view.View.OnLongClickListener;

final class PinEntryEditText$3
  implements View.OnLongClickListener
{
  PinEntryEditText$3(PinEntryEditText paramPinEntryEditText) {}
  
  public final boolean onLongClick(View paramView)
  {
    paramView = a;
    int i = paramView.getText().length();
    paramView.setSelection(i);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.PinEntryEditText.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */