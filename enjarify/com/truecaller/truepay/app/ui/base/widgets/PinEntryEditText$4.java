package com.truecaller.truepay.app.ui.base.widgets;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Paint;

final class PinEntryEditText$4
  implements ValueAnimator.AnimatorUpdateListener
{
  PinEntryEditText$4(PinEntryEditText paramPinEntryEditText) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    Paint localPaint = a.m;
    float f = ((Float)paramValueAnimator.getAnimatedValue()).floatValue();
    localPaint.setTextSize(f);
    a.invalidate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.PinEntryEditText.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */