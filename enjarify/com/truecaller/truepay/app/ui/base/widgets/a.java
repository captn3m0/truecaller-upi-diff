package com.truecaller.truepay.app.ui.base.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

public final class a
  extends RecyclerView.ItemDecoration
{
  private static final int[] b;
  public Drawable a;
  private int c;
  private final Rect d;
  
  static
  {
    int[] arrayOfInt = new int[1];
    arrayOfInt[0] = 16843284;
    b = arrayOfInt;
  }
  
  public a(Context paramContext)
  {
    Object localObject = new android/graphics/Rect;
    ((Rect)localObject).<init>();
    d = ((Rect)localObject);
    localObject = b;
    paramContext = paramContext.obtainStyledAttributes((int[])localObject);
    localObject = paramContext.getDrawable(0);
    a = ((Drawable)localObject);
    paramContext.recycle();
    c = 1;
  }
  
  public final void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    int i = paramRecyclerView.getChildAdapterPosition(paramView);
    paramRecyclerView = paramRecyclerView.getAdapter();
    i = paramRecyclerView.getItemViewType(i);
    int j = 1;
    if (i == j)
    {
      paramView = a;
      if (paramView == null)
      {
        paramRect.set(0, 0, 0, 0);
        return;
      }
      int k = c;
      if (k == j)
      {
        i = paramView.getIntrinsicHeight();
        paramRect.set(0, 0, 0, i);
        return;
      }
      i = paramView.getIntrinsicWidth();
      paramRect.set(0, 0, i, 0);
      return;
    }
    paramRect.setEmpty();
  }
  
  public final void onDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    paramState = paramRecyclerView.getLayoutManager();
    if (paramState != null)
    {
      paramState = a;
      if (paramState != null)
      {
        int i = c;
        int m = 0;
        int n = 1;
        int i2;
        int i3;
        Object localObject1;
        Object localObject2;
        if (i == n)
        {
          paramCanvas.save();
          boolean bool1 = paramRecyclerView.getClipToPadding();
          int j;
          int i4;
          if (bool1)
          {
            j = paramRecyclerView.getPaddingLeft();
            i1 = paramRecyclerView.getWidth();
            i2 = paramRecyclerView.getPaddingRight();
            i1 -= i2;
            i2 = paramRecyclerView.getPaddingTop();
            i3 = paramRecyclerView.getHeight();
            i4 = paramRecyclerView.getPaddingBottom();
            i3 -= i4;
            paramCanvas.clipRect(j, i2, i1, i3);
          }
          else
          {
            i1 = paramRecyclerView.getWidth();
            j = 0;
            paramState = null;
          }
          i2 = paramRecyclerView.getChildCount();
          while (m < i2)
          {
            localObject1 = paramRecyclerView.getChildAt(m);
            localObject2 = d;
            paramRecyclerView.getDecoratedBoundsWithMargins((View)localObject1, (Rect)localObject2);
            localObject2 = paramRecyclerView.getChildAt(m);
            i4 = paramRecyclerView.getChildAdapterPosition((View)localObject2);
            Object localObject3 = paramRecyclerView.getAdapter();
            i4 = ((RecyclerView.Adapter)localObject3).getItemViewType(i4);
            if (i4 == n)
            {
              localObject2 = d;
              i4 = bottom;
              float f1 = ((View)localObject1).getTranslationY();
              i3 = Math.round(f1);
              i4 += i3;
              i3 = a.getIntrinsicHeight();
              i3 = i4 - i3;
              localObject3 = a;
              ((Drawable)localObject3).setBounds(j, i3, i1, i4);
              localObject1 = a;
              ((Drawable)localObject1).draw(paramCanvas);
            }
            m += 1;
          }
          paramCanvas.restore();
          return;
        }
        paramCanvas.save();
        boolean bool2 = paramRecyclerView.getClipToPadding();
        int k;
        if (bool2)
        {
          k = paramRecyclerView.getPaddingTop();
          n = paramRecyclerView.getHeight();
          i1 = paramRecyclerView.getPaddingBottom();
          n -= i1;
          i1 = paramRecyclerView.getPaddingLeft();
          i2 = paramRecyclerView.getWidth();
          i3 = paramRecyclerView.getPaddingRight();
          i2 -= i3;
          paramCanvas.clipRect(i1, k, i2, n);
        }
        else
        {
          n = paramRecyclerView.getHeight();
          k = 0;
          paramState = null;
        }
        int i1 = paramRecyclerView.getChildCount();
        while (m < i1)
        {
          Object localObject4 = paramRecyclerView.getChildAt(m);
          localObject1 = paramRecyclerView.getLayoutManager();
          localObject2 = d;
          ((RecyclerView.LayoutManager)localObject1).getDecoratedBoundsWithMargins((View)localObject4, (Rect)localObject2);
          localObject1 = d;
          i3 = right;
          float f2 = ((View)localObject4).getTranslationX();
          i2 = Math.round(f2);
          i3 += i2;
          i2 = a.getIntrinsicWidth();
          i2 = i3 - i2;
          localObject2 = a;
          ((Drawable)localObject2).setBounds(i2, k, i3, n);
          localObject4 = a;
          ((Drawable)localObject4).draw(paramCanvas);
          m += 1;
        }
        paramCanvas.restore();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */