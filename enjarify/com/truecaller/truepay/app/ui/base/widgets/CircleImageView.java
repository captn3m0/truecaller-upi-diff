package com.truecaller.truepay.app.ui.base.widgets;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.widget.ImageView.ScaleType;
import com.truecaller.truepay.R.styleable;

public class CircleImageView
  extends AppCompatImageView
{
  private static final ImageView.ScaleType a = ImageView.ScaleType.CENTER_CROP;
  private static final Bitmap.Config b = Bitmap.Config.ARGB_8888;
  private final RectF c;
  private final RectF d;
  private final Matrix e;
  private final Paint f;
  private final Paint g;
  private final Paint h;
  private int i;
  private int j;
  private int k;
  private Bitmap l;
  private BitmapShader m;
  private int n;
  private int o;
  private float p;
  private float q;
  private ColorFilter r;
  private boolean s;
  private boolean t;
  private boolean u;
  private boolean v;
  
  public CircleImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private CircleImageView(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    Object localObject = new android/graphics/RectF;
    ((RectF)localObject).<init>();
    c = ((RectF)localObject);
    localObject = new android/graphics/RectF;
    ((RectF)localObject).<init>();
    d = ((RectF)localObject);
    localObject = new android/graphics/Matrix;
    ((Matrix)localObject).<init>();
    e = ((Matrix)localObject);
    localObject = new android/graphics/Paint;
    ((Paint)localObject).<init>();
    f = ((Paint)localObject);
    localObject = new android/graphics/Paint;
    ((Paint)localObject).<init>();
    g = ((Paint)localObject);
    localObject = new android/graphics/Paint;
    ((Paint)localObject).<init>();
    h = ((Paint)localObject);
    int i1 = -16777216;
    i = i1;
    j = 0;
    k = 0;
    int[] arrayOfInt = R.styleable.CircleImageView;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
    int i2 = R.styleable.CircleImageView_civ_border_width;
    i2 = paramContext.getDimensionPixelSize(i2, 0);
    j = i2;
    i2 = R.styleable.CircleImageView_civ_border_color;
    i2 = paramContext.getColor(i2, i1);
    i = i2;
    i2 = R.styleable.CircleImageView_civ_border_overlay;
    boolean bool1 = paramContext.getBoolean(i2, false);
    u = bool1;
    int i3 = R.styleable.CircleImageView_civ_circle_background_color;
    boolean bool2 = paramContext.hasValue(i3);
    int i4;
    if (bool2)
    {
      i4 = R.styleable.CircleImageView_civ_circle_background_color;
      i4 = paramContext.getColor(i4, 0);
      k = i4;
    }
    else
    {
      i4 = R.styleable.CircleImageView_civ_fill_color;
      boolean bool3 = paramContext.hasValue(i4);
      if (bool3)
      {
        i5 = R.styleable.CircleImageView_civ_fill_color;
        i5 = paramContext.getColor(i5, 0);
        k = i5;
      }
    }
    paramContext.recycle();
    paramContext = a;
    super.setScaleType(paramContext);
    s = true;
    int i6 = Build.VERSION.SDK_INT;
    int i5 = 21;
    if (i6 >= i5)
    {
      paramContext = new com/truecaller/truepay/app/ui/base/widgets/CircleImageView$a;
      paramContext.<init>(this, (byte)0);
      setOutlineProvider(paramContext);
    }
    boolean bool4 = t;
    if (bool4)
    {
      c();
      t = false;
    }
  }
  
  private static Bitmap a(Drawable paramDrawable)
  {
    if (paramDrawable == null) {
      return null;
    }
    boolean bool = paramDrawable instanceof BitmapDrawable;
    if (bool) {
      return ((BitmapDrawable)paramDrawable).getBitmap();
    }
    bool = paramDrawable instanceof ColorDrawable;
    if (bool) {}
    try
    {
      Object localObject = b;
      int i2 = 2;
      localObject = Bitmap.createBitmap(i2, i2, (Bitmap.Config)localObject);
      break label71;
      int i1 = paramDrawable.getIntrinsicWidth();
      i2 = paramDrawable.getIntrinsicHeight();
      Bitmap.Config localConfig = b;
      localObject = Bitmap.createBitmap(i1, i2, localConfig);
      label71:
      Canvas localCanvas = new android/graphics/Canvas;
      localCanvas.<init>((Bitmap)localObject);
      int i3 = localCanvas.getWidth();
      int i4 = localCanvas.getHeight();
      ((Drawable)paramDrawable).setBounds(0, 0, i3, i4);
      ((Drawable)paramDrawable).draw(localCanvas);
      return (Bitmap)localObject;
    }
    catch (Exception localException)
    {
      localException;
    }
    return null;
  }
  
  private void a()
  {
    Paint localPaint = f;
    if (localPaint != null)
    {
      ColorFilter localColorFilter = r;
      localPaint.setColorFilter(localColorFilter);
    }
  }
  
  private void b()
  {
    boolean bool = v;
    Bitmap localBitmap;
    if (bool)
    {
      bool = false;
      localBitmap = null;
      l = null;
    }
    else
    {
      localBitmap = a(getDrawable());
      l = localBitmap;
    }
    c();
  }
  
  private void c()
  {
    boolean bool1 = s;
    boolean bool3 = true;
    float f1 = Float.MIN_VALUE;
    if (!bool1)
    {
      t = bool3;
      return;
    }
    int i1 = getWidth();
    if (i1 == 0)
    {
      i1 = getHeight();
      if (i1 == 0) {
        return;
      }
    }
    Object localObject1 = l;
    if (localObject1 == null)
    {
      invalidate();
      return;
    }
    Object localObject2 = new android/graphics/BitmapShader;
    Shader.TileMode localTileMode = Shader.TileMode.CLAMP;
    ((BitmapShader)localObject2).<init>((Bitmap)localObject1, localTileMode, localTileMode);
    m = ((BitmapShader)localObject2);
    f.setAntiAlias(bool3);
    localObject1 = f;
    localObject2 = m;
    ((Paint)localObject1).setShader((Shader)localObject2);
    localObject1 = g;
    localObject2 = Paint.Style.STROKE;
    ((Paint)localObject1).setStyle((Paint.Style)localObject2);
    g.setAntiAlias(bool3);
    localObject1 = g;
    int i4 = i;
    ((Paint)localObject1).setColor(i4);
    localObject1 = g;
    i4 = j;
    float f2 = i4;
    ((Paint)localObject1).setStrokeWidth(f2);
    localObject1 = h;
    localObject2 = Paint.Style.FILL;
    ((Paint)localObject1).setStyle((Paint.Style)localObject2);
    h.setAntiAlias(bool3);
    localObject1 = h;
    int i3 = k;
    ((Paint)localObject1).setColor(i3);
    i1 = l.getHeight();
    o = i1;
    i1 = l.getWidth();
    n = i1;
    localObject1 = d;
    RectF localRectF = d();
    ((RectF)localObject1).set(localRectF);
    float f3 = d.height();
    f1 = j;
    f3 -= f1;
    i3 = 1073741824;
    f1 = 2.0F;
    f3 /= f1;
    f2 = d.width();
    int i5 = j;
    float f4 = i5;
    f2 = (f2 - f4) / f1;
    f3 = Math.min(f3, f2);
    q = f3;
    localObject1 = c;
    localObject2 = d;
    ((RectF)localObject1).set((RectF)localObject2);
    boolean bool2 = u;
    if (!bool2)
    {
      int i2 = j;
      if (i2 > 0)
      {
        localObject2 = c;
        f4 = i2;
        float f5 = 1.0F;
        f4 -= f5;
        f3 = i2 - f5;
        ((RectF)localObject2).inset(f4, f3);
      }
    }
    f3 = c.height() / f1;
    f2 = c.width() / f1;
    f3 = Math.min(f3, f2);
    p = f3;
    a();
    e();
    invalidate();
  }
  
  private RectF d()
  {
    int i1 = getWidth();
    int i2 = getPaddingLeft();
    i1 -= i2;
    i2 = getPaddingRight();
    i1 -= i2;
    i2 = getHeight();
    int i3 = getPaddingTop();
    i2 -= i3;
    i3 = getPaddingBottom();
    i2 -= i3;
    i3 = Math.min(i1, i2);
    float f1 = getPaddingLeft();
    float f2 = i1 - i3;
    float f3 = 2.0F;
    f2 /= f3;
    f1 += f2;
    f2 = getPaddingTop();
    float f4 = (i2 - i3) / f3;
    f2 += f4;
    RectF localRectF = new android/graphics/RectF;
    float f5 = i3;
    f3 = f1 + f5;
    f5 += f2;
    localRectF.<init>(f1, f2, f3, f5);
    return localRectF;
  }
  
  private void e()
  {
    Object localObject1 = e;
    int i1 = 0;
    ((Matrix)localObject1).set(null);
    float f1 = n;
    float f2 = c.height();
    f1 *= f2;
    Object localObject2 = c;
    f2 = ((RectF)localObject2).width();
    float f3 = o;
    f2 *= f3;
    f3 = 0.0F;
    float f4 = 0.5F;
    boolean bool = f1 < f2;
    int i2;
    if (bool)
    {
      localObject1 = c;
      f1 = ((RectF)localObject1).height();
      i1 = o;
      f2 = i1;
      f1 /= f2;
      localObject2 = c;
      f2 = ((RectF)localObject2).width();
      i2 = n;
      f5 = i2 * f1;
      f2 = (f2 - f5) * f4;
    }
    else
    {
      localObject1 = c;
      f1 = ((RectF)localObject1).width();
      f2 = n;
      f1 /= f2;
      f2 = c.height();
      i2 = o;
      f5 = i2 * f1;
      f3 = (f2 - f5) * f4;
      i1 = 0;
      f2 = 0.0F;
      localObject2 = null;
    }
    e.setScale(f1, f1);
    localObject1 = e;
    f2 = (int)(f2 + f4);
    float f5 = c.left;
    f2 += f5;
    f3 = (int)(f3 + f4);
    f4 = c.top;
    f3 += f4;
    ((Matrix)localObject1).postTranslate(f2, f3);
    localObject1 = m;
    localObject2 = e;
    ((BitmapShader)localObject1).setLocalMatrix((Matrix)localObject2);
  }
  
  public int getBorderColor()
  {
    return i;
  }
  
  public int getBorderWidth()
  {
    return j;
  }
  
  public int getCircleBackgroundColor()
  {
    return k;
  }
  
  public ColorFilter getColorFilter()
  {
    return r;
  }
  
  public int getFillColor()
  {
    return getCircleBackgroundColor();
  }
  
  public ImageView.ScaleType getScaleType()
  {
    return a;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    boolean bool = v;
    if (bool)
    {
      super.onDraw(paramCanvas);
      return;
    }
    Object localObject = l;
    if (localObject == null) {
      return;
    }
    int i1 = k;
    if (i1 != 0)
    {
      localObject = c;
      f1 = ((RectF)localObject).centerX();
      localRectF = c;
      f2 = localRectF.centerY();
      f3 = p;
      localPaint = h;
      paramCanvas.drawCircle(f1, f2, f3, localPaint);
    }
    localObject = c;
    float f1 = ((RectF)localObject).centerX();
    RectF localRectF = c;
    float f2 = localRectF.centerY();
    float f3 = p;
    Paint localPaint = f;
    paramCanvas.drawCircle(f1, f2, f3, localPaint);
    i1 = j;
    if (i1 > 0)
    {
      localObject = d;
      f1 = ((RectF)localObject).centerX();
      localRectF = d;
      f2 = localRectF.centerY();
      f3 = q;
      localPaint = g;
      paramCanvas.drawCircle(f1, f2, f3, localPaint);
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    c();
  }
  
  public void setAdjustViewBounds(boolean paramBoolean)
  {
    if (!paramBoolean) {
      return;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    localIllegalArgumentException.<init>("adjustViewBounds not supported.");
    throw localIllegalArgumentException;
  }
  
  public void setBorderColor(int paramInt)
  {
    int i1 = i;
    if (paramInt == i1) {
      return;
    }
    i = paramInt;
    Paint localPaint = g;
    i1 = i;
    localPaint.setColor(i1);
    invalidate();
  }
  
  public void setBorderColorResource(int paramInt)
  {
    paramInt = getContext().getResources().getColor(paramInt);
    setBorderColor(paramInt);
  }
  
  public void setBorderOverlay(boolean paramBoolean)
  {
    boolean bool = u;
    if (paramBoolean == bool) {
      return;
    }
    u = paramBoolean;
    c();
  }
  
  public void setBorderWidth(int paramInt)
  {
    int i1 = j;
    if (paramInt == i1) {
      return;
    }
    j = paramInt;
    c();
  }
  
  public void setCircleBackgroundColor(int paramInt)
  {
    int i1 = k;
    if (paramInt == i1) {
      return;
    }
    k = paramInt;
    h.setColor(paramInt);
    invalidate();
  }
  
  public void setCircleBackgroundColorResource(int paramInt)
  {
    paramInt = getContext().getResources().getColor(paramInt);
    setCircleBackgroundColor(paramInt);
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    ColorFilter localColorFilter = r;
    if (paramColorFilter == localColorFilter) {
      return;
    }
    r = paramColorFilter;
    a();
    invalidate();
  }
  
  public void setDisableCircularTransformation(boolean paramBoolean)
  {
    boolean bool = v;
    if (bool == paramBoolean) {
      return;
    }
    v = paramBoolean;
    b();
  }
  
  public void setFillColor(int paramInt)
  {
    setCircleBackgroundColor(paramInt);
  }
  
  public void setFillColorResource(int paramInt)
  {
    setCircleBackgroundColorResource(paramInt);
  }
  
  public void setImageBitmap(Bitmap paramBitmap)
  {
    super.setImageBitmap(paramBitmap);
    b();
  }
  
  public void setImageDrawable(Drawable paramDrawable)
  {
    super.setImageDrawable(paramDrawable);
    b();
  }
  
  public void setImageResource(int paramInt)
  {
    super.setImageResource(paramInt);
    b();
  }
  
  public void setImageURI(Uri paramUri)
  {
    super.setImageURI(paramUri);
    b();
  }
  
  public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.setPadding(paramInt1, paramInt2, paramInt3, paramInt4);
    c();
  }
  
  public void setPaddingRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.setPaddingRelative(paramInt1, paramInt2, paramInt3, paramInt4);
    c();
  }
  
  public void setScaleType(ImageView.ScaleType paramScaleType)
  {
    Object localObject = a;
    if (paramScaleType == localObject) {
      return;
    }
    localObject = new java/lang/IllegalArgumentException;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramScaleType;
    paramScaleType = String.format("ScaleType %s not supported.", arrayOfObject);
    ((IllegalArgumentException)localObject).<init>(paramScaleType);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.CircleImageView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */