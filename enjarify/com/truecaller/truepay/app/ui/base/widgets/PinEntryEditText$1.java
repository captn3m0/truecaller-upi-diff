package com.truecaller.truepay.app.ui.base.widgets;

import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuItem;

final class PinEntryEditText$1
  implements ActionMode.Callback
{
  PinEntryEditText$1(PinEntryEditText paramPinEntryEditText) {}
  
  public final boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
  {
    return false;
  }
  
  public final boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    return false;
  }
  
  public final void onDestroyActionMode(ActionMode paramActionMode) {}
  
  public final boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.PinEntryEditText.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */