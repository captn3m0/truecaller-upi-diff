package com.truecaller.truepay.app.ui.base.widgets;

import android.graphics.Outline;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewOutlineProvider;

final class CircleImageView$a
  extends ViewOutlineProvider
{
  private CircleImageView$a(CircleImageView paramCircleImageView) {}
  
  public final void getOutline(View paramView, Outline paramOutline)
  {
    paramView = new android/graphics/Rect;
    paramView.<init>();
    CircleImageView.a(a).roundOut(paramView);
    float f = paramView.width() / 2.0F;
    paramOutline.setRoundRect(paramView, f);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.CircleImageView.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */