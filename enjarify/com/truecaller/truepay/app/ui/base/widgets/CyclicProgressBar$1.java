package com.truecaller.truepay.app.ui.base.widgets;

final class CyclicProgressBar$1
  implements Runnable
{
  CyclicProgressBar$1(CyclicProgressBar paramCyclicProgressBar) {}
  
  public final void run()
  {
    CyclicProgressBar.a(a);
    CyclicProgressBar localCyclicProgressBar = a;
    boolean bool = CyclicProgressBar.b(localCyclicProgressBar);
    if (!bool)
    {
      localCyclicProgressBar = a;
      long l = System.currentTimeMillis();
      CyclicProgressBar.a(localCyclicProgressBar, l);
      localCyclicProgressBar = a;
      localCyclicProgressBar.setVisibility(0);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.CyclicProgressBar.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */