package com.truecaller.truepay.app.ui.base.widgets;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public final class RecommendedRechargesView$b
  implements CompoundButton.OnCheckedChangeListener
{
  RecommendedRechargesView$b(RecommendedRechargesView paramRecommendedRechargesView) {}
  
  public final void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    RecommendedRechargesView localRecommendedRechargesView = a;
    int i;
    if (paramCompoundButton != null)
    {
      i = paramCompoundButton.getId();
      paramCompoundButton = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramCompoundButton = null;
    }
    RecommendedRechargesView.a(localRecommendedRechargesView, paramCompoundButton, paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.RecommendedRechargesView.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */