package com.truecaller.truepay.app.ui.base.widgets;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.Behavior;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

public class SnappyAppBarLayoutBehavior
  extends AppBarLayout.Behavior
{
  private boolean b;
  private boolean c;
  
  public SnappyAppBarLayoutBehavior() {}
  
  public SnappyAppBarLayoutBehavior(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public final void a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView, int paramInt)
  {
    paramInt = b;
    if (paramInt != 0)
    {
      b = false;
      return;
    }
    paramInt = c;
    if (paramInt != 0) {
      return;
    }
    c = true;
    super.a(paramCoordinatorLayout, paramAppBarLayout, paramView, 0);
  }
  
  public final boolean a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView1, View paramView2, int paramInt1, int paramInt2)
  {
    boolean bool = c;
    if (!bool)
    {
      a(paramCoordinatorLayout, paramAppBarLayout, paramView2, paramInt2);
      bool = true;
      b = bool;
    }
    c = false;
    return super.a(paramCoordinatorLayout, paramAppBarLayout, paramView1, paramView2, paramInt1, paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.SnappyAppBarLayoutBehavior
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */