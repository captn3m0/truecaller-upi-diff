package com.truecaller.truepay.app.ui.base.widgets;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Paint;

final class PinEntryEditText$7
  implements ValueAnimator.AnimatorUpdateListener
{
  PinEntryEditText$7(PinEntryEditText paramPinEntryEditText) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    paramValueAnimator = (Integer)paramValueAnimator.getAnimatedValue();
    Paint localPaint = a.m;
    int i = paramValueAnimator.intValue();
    localPaint.setAlpha(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.PinEntryEditText.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */