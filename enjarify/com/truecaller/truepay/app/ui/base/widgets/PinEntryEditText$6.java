package com.truecaller.truepay.app.ui.base.widgets;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;

final class PinEntryEditText$6
  implements ValueAnimator.AnimatorUpdateListener
{
  PinEntryEditText$6(PinEntryEditText paramPinEntryEditText, int paramInt) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    paramValueAnimator = (Float)paramValueAnimator.getAnimatedValue();
    float[] arrayOfFloat = b.k;
    int i = a;
    float f = paramValueAnimator.floatValue();
    arrayOfFloat[i] = f;
    b.invalidate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.PinEntryEditText.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */