package com.truecaller.truepay.app.ui.base.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.HorizontalScrollView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import c.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.layout;
import java.util.List;

public final class RecommendedRechargesView
  extends HorizontalScrollView
{
  public List a;
  public RadioGroup b;
  private final String c;
  private RecommendedRechargesView.a d;
  
  public RecommendedRechargesView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private RecommendedRechargesView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = new Integer[3];
    paramAttributeSet = Integer.valueOf(100);
    paramContext[0] = paramAttributeSet;
    paramAttributeSet = Integer.valueOf(200);
    paramContext[1] = paramAttributeSet;
    paramAttributeSet = Integer.valueOf(300);
    paramContext[2] = paramAttributeSet;
    paramContext = m.b(paramContext);
    a = paramContext;
    c = "RadioGroupTag";
    paramContext = getRecommendedAmountParentGroup();
    addView(paramContext);
    a();
  }
  
  private final View getRecommendedAmountParentGroup()
  {
    RadioGroup localRadioGroup = new android/widget/RadioGroup;
    Object localObject = getContext();
    localRadioGroup.<init>((Context)localObject);
    b = localRadioGroup;
    localRadioGroup = b;
    if (localRadioGroup == null)
    {
      localObject = "radioGroup";
      k.a((String)localObject);
    }
    localObject = c;
    localRadioGroup.setTag(localObject);
    localRadioGroup = b;
    if (localRadioGroup == null)
    {
      localObject = "radioGroup";
      k.a((String)localObject);
    }
    localObject = null;
    localRadioGroup.setOrientation(0);
    localRadioGroup = b;
    if (localRadioGroup == null)
    {
      localObject = "radioGroup";
      k.a((String)localObject);
    }
    return (View)localRadioGroup;
  }
  
  public final void a()
  {
    Object localObject1 = a;
    int i = ((List)localObject1).size();
    int j = 0;
    while (j < i)
    {
      RadioGroup localRadioGroup = b;
      if (localRadioGroup == null)
      {
        localObject2 = "radioGroup";
        k.a((String)localObject2);
      }
      Object localObject2 = LayoutInflater.from(getContext());
      int k = R.layout.recharge_amount_item;
      Object localObject3 = b;
      if (localObject3 == null)
      {
        String str = "radioGroup";
        k.a(str);
      }
      localObject3 = (ViewGroup)localObject3;
      localObject2 = ((LayoutInflater)localObject2).inflate(k, (ViewGroup)localObject3, false);
      if (localObject2 != null)
      {
        localObject2 = (RadioButton)localObject2;
        Object localObject4 = b;
        if (localObject4 == null)
        {
          localObject3 = "radioGroup";
          k.a((String)localObject3);
        }
        k = ((RadioGroup)localObject4).getChildCount();
        ((RadioButton)localObject2).setId(k);
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>("₹ ");
        localObject3 = (Number)a.get(j);
        int m = ((Number)localObject3).intValue();
        ((StringBuilder)localObject4).append(m);
        localObject4 = (CharSequence)((StringBuilder)localObject4).toString();
        ((RadioButton)localObject2).setText((CharSequence)localObject4);
        localObject4 = new com/truecaller/truepay/app/ui/base/widgets/RecommendedRechargesView$b;
        ((RecommendedRechargesView.b)localObject4).<init>(this);
        localObject4 = (CompoundButton.OnCheckedChangeListener)localObject4;
        ((RadioButton)localObject2).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject4);
        localObject2 = (View)localObject2;
        localRadioGroup.addView((View)localObject2);
        j += 1;
      }
      else
      {
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type android.widget.RadioButton");
        throw ((Throwable)localObject1);
      }
    }
  }
  
  public final RadioGroup getRadioGroup()
  {
    RadioGroup localRadioGroup = b;
    if (localRadioGroup == null)
    {
      String str = "radioGroup";
      k.a(str);
    }
    return localRadioGroup;
  }
  
  public final void setRadioGroup(RadioGroup paramRadioGroup)
  {
    k.b(paramRadioGroup, "<set-?>");
    b = paramRadioGroup;
  }
  
  public final void setRechargeAmountListener(RecommendedRechargesView.a parama)
  {
    k.b(parama, "rechargeAmountChangedListener");
    d = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.RecommendedRechargesView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */