package com.truecaller.truepay.app.ui.base.widgets;

import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;

final class PinEntryEditText$2
  implements View.OnClickListener
{
  PinEntryEditText$2(PinEntryEditText paramPinEntryEditText) {}
  
  public final void onClick(View paramView)
  {
    Object localObject = a;
    Editable localEditable = ((PinEntryEditText)localObject).getText();
    int i = localEditable.length();
    ((PinEntryEditText)localObject).setSelection(i);
    localObject = a.r;
    if (localObject != null)
    {
      localObject = a.r;
      ((View.OnClickListener)localObject).onClick(paramView);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.PinEntryEditText.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */