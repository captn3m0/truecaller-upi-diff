package com.truecaller.truepay.app.ui.base.widgets;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.os.SystemClock;
import android.support.v4.view.r;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import com.truecaller.truepay.R.styleable;
import java.util.Stack;

public class CyclicProgressBar
  extends View
{
  private static final Interpolator h;
  private static boolean i = true;
  public long a;
  public boolean b;
  public boolean c;
  public boolean d;
  public final Runnable e;
  public final Runnable f;
  public Stack g;
  private float j;
  private float k;
  private float l;
  private float m;
  private RectF n;
  private Paint o;
  private float p;
  
  static
  {
    AccelerateDecelerateInterpolator localAccelerateDecelerateInterpolator = new android/view/animation/AccelerateDecelerateInterpolator;
    localAccelerateDecelerateInterpolator.<init>();
    h = localAccelerateDecelerateInterpolator;
  }
  
  public CyclicProgressBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Object localObject1 = new android/graphics/RectF;
    ((RectF)localObject1).<init>();
    n = ((RectF)localObject1);
    long l1 = -1;
    a = l1;
    int i1 = 0;
    float f1 = 0.0F;
    localObject1 = null;
    b = false;
    c = false;
    d = false;
    Object localObject2 = new com/truecaller/truepay/app/ui/base/widgets/CyclicProgressBar$1;
    ((CyclicProgressBar.1)localObject2).<init>(this);
    e = ((Runnable)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/base/widgets/CyclicProgressBar$2;
    ((CyclicProgressBar.2)localObject2).<init>(this);
    f = ((Runnable)localObject2);
    localObject2 = new java/util/Stack;
    ((Stack)localObject2).<init>();
    g = ((Stack)localObject2);
    boolean bool = isInEditMode();
    int i2 = 1;
    float f2 = 4.0F;
    int i3;
    float f3;
    if (bool)
    {
      i3 = -7829368;
      f3 = 0.0F / 0.0F;
      p = f2;
    }
    else
    {
      localObject2 = paramContext.getTheme();
      int[] arrayOfInt = R.styleable.CyclicProgressBar;
      paramAttributeSet = ((Resources.Theme)localObject2).obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
    }
    try
    {
      i1 = R.styleable.CyclicProgressBar_cpb_strokeWidth;
      paramContext = paramContext.getResources();
      paramContext = paramContext.getDisplayMetrics();
      i3 = (int)TypedValue.applyDimension(i2, f2, paramContext);
      f3 = i3;
      f3 = paramAttributeSet.getDimension(i1, f3);
      p = f3;
      i3 = R.styleable.CyclicProgressBar_cpb_strokeColor;
      i1 = -1;
      f1 = 0.0F / 0.0F;
      i3 = paramAttributeSet.getColor(i3, i1);
      paramAttributeSet.recycle();
      paramAttributeSet = new android/graphics/Paint;
      paramAttributeSet.<init>();
      o = paramAttributeSet;
      paramAttributeSet = o;
      localObject1 = Paint.Style.STROKE;
      paramAttributeSet.setStyle((Paint.Style)localObject1);
      paramAttributeSet = o;
      f1 = p;
      paramAttributeSet.setStrokeWidth(f1);
      paramAttributeSet = o;
      localObject1 = Paint.Cap.ROUND;
      paramAttributeSet.setStrokeCap((Paint.Cap)localObject1);
      o.setColor(i3);
      o.setAntiAlias(i2);
      return;
    }
    finally
    {
      paramAttributeSet.recycle();
    }
  }
  
  public static void setAnimationEnabled(boolean paramBoolean)
  {
    i = paramBoolean;
  }
  
  public final void a()
  {
    g.size();
    Stack localStack = g;
    localStack.clear();
    boolean bool1 = true;
    d = bool1;
    c = false;
    Runnable localRunnable1 = e;
    removeCallbacks(localRunnable1);
    long l1 = System.currentTimeMillis();
    long l2 = a;
    l1 -= l2;
    long l3 = 600L;
    boolean bool2 = l1 < l3;
    if (bool2)
    {
      long l4 = -1;
      boolean bool3 = l2 < l4;
      if (bool3)
      {
        boolean bool4 = b;
        if (!bool4)
        {
          Runnable localRunnable2 = f;
          l3 -= l1;
          postDelayed(localRunnable2, l3);
          b = bool1;
        }
        return;
      }
    }
    localRunnable1 = f;
    removeCallbacks(localRunnable1);
    localRunnable1 = f;
    postDelayed(localRunnable1, 100);
    b = bool1;
  }
  
  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    Runnable localRunnable = f;
    removeCallbacks(localRunnable);
    localRunnable = e;
    removeCallbacks(localRunnable);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    long l1 = SystemClock.elapsedRealtime();
    long l2 = 2000L;
    long l3 = l1 % l2;
    float f1 = (float)l3;
    float f2 = f1 / 2000.0F;
    int i1 = 1135869952;
    float f3 = 360.0F;
    float f4 = f2 * f3;
    j = f4;
    l1 /= l2;
    float f5 = (float)l1;
    float f6 = 225.0F;
    f5 *= f6;
    float f7 = (int)(f5 / f3) * 360;
    f5 -= f7;
    m = f5;
    int i2 = 1133903872;
    f5 = 300.0F;
    f7 = 270.0F;
    float f8 = 0.85F;
    boolean bool3 = f2 < f8;
    Interpolator localInterpolator;
    if (!bool3)
    {
      int i3 = 1154777088;
      f8 = 1700.0F;
      f1 = (f1 - f8) / f5;
      localInterpolator = h;
      f5 = localInterpolator.getInterpolation(f1) * f6;
      f5 = f7 - f5;
      k = f5;
      f5 = k;
      f7 -= f5;
      l = f7;
    }
    else
    {
      f8 = 0.5F;
      boolean bool4 = f2 < f8;
      if (!bool4)
      {
        k = f7;
      }
      else
      {
        f7 = 45.0F;
        int i4 = 1051931443;
        f8 = 0.35F;
        boolean bool1 = f2 < f8;
        if (!bool1)
        {
          i4 = 1143930880;
          f8 = 700.0F;
          f1 = (f1 - f8) / f5;
          localInterpolator = h;
          f5 = localInterpolator.getInterpolation(f1) * f6 + f7;
          k = f5;
        }
        else
        {
          boolean bool2 = f2 < f8;
          if (bool2)
          {
            k = f7;
            bool2 = false;
            f5 = 0.0F;
            localInterpolator = null;
            l = 0.0F;
          }
        }
      }
    }
    RectF localRectF = n;
    f5 = j;
    f6 = m;
    f5 += f6;
    f6 = l;
    f8 = f5 + f6;
    f1 = k;
    f2 = 0.0F;
    Paint localPaint = o;
    paramCanvas.drawArc(localRectF, f8, f1, false, localPaint);
    boolean bool5 = i;
    if (bool5) {
      r.e(this);
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    RectF localRectF = n;
    float f1 = p;
    float f2 = 0.5F;
    float f3 = f1 * f2;
    float f4 = f1 * f2;
    float f5 = paramInt1;
    float f6 = f1 * f2;
    f5 -= f6;
    float f7 = paramInt2;
    f1 *= f2;
    f7 -= f1;
    localRectF.set(f3, f4, f5, f7);
    n.inset(f2, f2);
  }
  
  public void setStrokeColor(int paramInt)
  {
    Paint localPaint = o;
    if (localPaint != null) {
      localPaint.setColor(paramInt);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.widgets.CyclicProgressBar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */