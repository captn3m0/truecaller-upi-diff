package com.truecaller.truepay.app.ui.base.views.a;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.views.activities.PaymentsActivity;
import com.truecaller.truepay.app.ui.scan.views.activities.MerchantActivity;
import com.truecaller.truepay.app.utils.u;
import com.truecaller.truepay.app.utils.v.a;

public abstract class b
  extends a
  implements v.a
{
  public e featuresRegistry;
  public u instantRewardHandler;
  
  private void setStatusBar()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      Window localWindow = getWindow();
      Resources localResources = getResources();
      int k = R.color.colorPrimaryDark;
      j = localResources.getColor(k);
      localWindow.setStatusBarColor(j);
    }
  }
  
  protected abstract void initDagger(com.truecaller.truepay.app.a.a.a parama);
  
  protected boolean isUserOnboarded()
  {
    return Truepay.getInstance().isRegistrationComplete();
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int i = 999;
    if (paramInt1 == i)
    {
      paramInt1 = -1;
      if (paramInt2 != paramInt1)
      {
        paramInt1 = R.string.pre_registration_failure;
        paramInt2 = 0;
        Toast localToast = Toast.makeText(this, paramInt1, 0);
        localToast.show();
        finish();
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = Truepay.getApplicationComponent();
    initDagger(paramBundle);
    setStatusBar();
    paramBundle = Truepay.getApplicationComponent();
    if (paramBundle != null)
    {
      boolean bool = isUserOnboarded();
      if (!bool)
      {
        bool = this instanceof PaymentsActivity;
        if (!bool)
        {
          bool = this instanceof MerchantActivity;
          if (!bool)
          {
            paramBundle = Truepay.getInstance();
            paramBundle.openBankingTab(this);
            finish();
          }
        }
      }
    }
  }
  
  public void onPause()
  {
    super.onPause();
    Object localObject = instantRewardHandler;
    if (localObject != null)
    {
      boolean bool = isUserOnboarded();
      if (bool)
      {
        localObject = featuresRegistry;
        if (localObject != null)
        {
          localObject = ((e)localObject).H();
          bool = ((com.truecaller.featuretoggles.b)localObject).a();
          if (bool)
          {
            localObject = instantRewardHandler;
            ((u)localObject).a();
          }
        }
      }
    }
  }
  
  public void onResume()
  {
    super.onResume();
    Object localObject = instantRewardHandler;
    if (localObject != null)
    {
      boolean bool = isUserOnboarded();
      if (bool)
      {
        localObject = featuresRegistry;
        if (localObject != null)
        {
          localObject = ((e)localObject).H();
          bool = ((com.truecaller.featuretoggles.b)localObject).a();
          if (bool)
          {
            localObject = instantRewardHandler;
            ((u)localObject).a(this);
          }
        }
      }
    }
  }
  
  public void onRewardReceived(Intent paramIntent)
  {
    startActivity(paramIntent);
  }
  
  protected void setVisibility(int paramInt, View... paramVarArgs)
  {
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      View localView = paramVarArgs[j];
      int k = localView.getVisibility();
      if (k != paramInt) {
        localView.setVisibility(paramInt);
      }
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */