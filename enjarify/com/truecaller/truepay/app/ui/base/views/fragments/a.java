package com.truecaller.truepay.app.ui.base.views.fragments;

import android.os.Bundle;
import android.support.design.widget.b;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class a
  extends b
{
  private View a;
  
  public abstract int a();
  
  protected abstract void b();
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    b();
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = a();
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    a = paramLayoutInflater;
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.fragments.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */