package com.truecaller.truepay.app.ui.base.views.c;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import com.truecaller.truepay.app.ui.base.views.b.f;

public abstract class a
  extends RecyclerView.ViewHolder
{
  protected final f a;
  
  public a(View paramView, f paramf)
  {
    super(paramView);
    a = paramf;
  }
  
  protected final f a()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */