package com.truecaller.truepay.app.ui.base.views.b;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import java.util.List;

public abstract class a
  extends RecyclerView.Adapter
{
  protected e a;
  protected Object b;
  
  public a()
  {
    this(locale);
  }
  
  private a(e parame)
  {
    a = parame;
  }
  
  public final Object a()
  {
    return b;
  }
  
  public final void a(Object paramObject)
  {
    b = paramObject;
  }
  
  public int getItemViewType(int paramInt)
  {
    e locale = a;
    Object localObject = b;
    return locale.a(localObject, paramInt);
  }
  
  public void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    e locale = a;
    Object localObject = b;
    locale.a(localObject, paramInt, paramViewHolder, null);
  }
  
  public void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt, List paramList)
  {
    e locale = a;
    Object localObject = b;
    locale.a(localObject, paramInt, paramViewHolder, paramList);
  }
  
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    return a.a(paramViewGroup, paramInt);
  }
  
  public boolean onFailedToRecycleView(RecyclerView.ViewHolder paramViewHolder)
  {
    Object localObject = a;
    int i = paramViewHolder.getItemViewType();
    localObject = ((e)localObject).a(i);
    if (localObject != null) {
      return false;
    }
    localObject = new java/lang/NullPointerException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("No delegate found for ");
    localStringBuilder.append(paramViewHolder);
    localStringBuilder.append(" for item at position = ");
    int j = paramViewHolder.getAdapterPosition();
    localStringBuilder.append(j);
    localStringBuilder.append(" for viewType = ");
    int k = paramViewHolder.getItemViewType();
    localStringBuilder.append(k);
    paramViewHolder = localStringBuilder.toString();
    ((NullPointerException)localObject).<init>(paramViewHolder);
    throw ((Throwable)localObject);
  }
  
  public void onViewAttachedToWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    Object localObject = a;
    int i = paramViewHolder.getItemViewType();
    localObject = ((e)localObject).a(i);
    if (localObject != null) {
      return;
    }
    localObject = new java/lang/NullPointerException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("No delegate found for ");
    localStringBuilder.append(paramViewHolder);
    localStringBuilder.append(" for item at position = ");
    int j = paramViewHolder.getAdapterPosition();
    localStringBuilder.append(j);
    localStringBuilder.append(" for viewType = ");
    int k = paramViewHolder.getItemViewType();
    localStringBuilder.append(k);
    paramViewHolder = localStringBuilder.toString();
    ((NullPointerException)localObject).<init>(paramViewHolder);
    throw ((Throwable)localObject);
  }
  
  public void onViewDetachedFromWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    Object localObject = a;
    int i = paramViewHolder.getItemViewType();
    localObject = ((e)localObject).a(i);
    if (localObject != null) {
      return;
    }
    localObject = new java/lang/NullPointerException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("No delegate found for ");
    localStringBuilder.append(paramViewHolder);
    localStringBuilder.append(" for item at position = ");
    int j = paramViewHolder.getAdapterPosition();
    localStringBuilder.append(j);
    localStringBuilder.append(" for viewType = ");
    int k = paramViewHolder.getItemViewType();
    localStringBuilder.append(k);
    paramViewHolder = localStringBuilder.toString();
    ((NullPointerException)localObject).<init>(paramViewHolder);
    throw ((Throwable)localObject);
  }
  
  public void onViewRecycled(RecyclerView.ViewHolder paramViewHolder)
  {
    Object localObject = a;
    int i = paramViewHolder.getItemViewType();
    localObject = ((e)localObject).a(i);
    if (localObject != null) {
      return;
    }
    localObject = new java/lang/NullPointerException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("No delegate found for ");
    localStringBuilder.append(paramViewHolder);
    localStringBuilder.append(" for item at position = ");
    int j = paramViewHolder.getAdapterPosition();
    localStringBuilder.append(j);
    localStringBuilder.append(" for viewType = ");
    int k = paramViewHolder.getItemViewType();
    localStringBuilder.append(k);
    paramViewHolder = localStringBuilder.toString();
    ((NullPointerException)localObject).<init>(paramViewHolder);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */