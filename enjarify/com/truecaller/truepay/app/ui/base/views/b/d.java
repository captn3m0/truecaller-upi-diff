package com.truecaller.truepay.app.ui.base.views.b;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import java.util.List;

public abstract class d
{
  protected abstract RecyclerView.ViewHolder a(ViewGroup paramViewGroup);
  
  protected abstract void a(Object paramObject, int paramInt, RecyclerView.ViewHolder paramViewHolder, List paramList);
  
  protected abstract boolean a(Object paramObject, int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */