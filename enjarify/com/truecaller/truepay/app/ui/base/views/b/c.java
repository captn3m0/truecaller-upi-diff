package com.truecaller.truepay.app.ui.base.views.b;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import java.util.List;

public abstract class c
  extends d
{
  protected final f a;
  
  public c(f paramf)
  {
    a = paramf;
  }
  
  protected abstract RecyclerView.ViewHolder a(ViewGroup paramViewGroup);
  
  protected abstract void a(Object paramObject, RecyclerView.ViewHolder paramViewHolder, List paramList);
  
  protected abstract boolean a(Object paramObject, List paramList);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */