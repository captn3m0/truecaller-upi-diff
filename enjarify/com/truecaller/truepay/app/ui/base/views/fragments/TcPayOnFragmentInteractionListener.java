package com.truecaller.truepay.app.ui.base.views.fragments;

import android.support.v4.app.Fragment;

public abstract interface TcPayOnFragmentInteractionListener
{
  public abstract void onHamburgerClicked();
  
  public abstract void replaceFragment(Fragment paramFragment);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.fragments.TcPayOnFragmentInteractionListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */