package com.truecaller.truepay.app.ui.base.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.truecaller.log.d;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.b.e;

public abstract class b
  extends Fragment
{
  public View m;
  
  protected abstract int a();
  
  public final void a(String paramString, Throwable paramThrowable)
  {
    f localf = getActivity();
    if (localf != null)
    {
      boolean bool;
      if (paramThrowable != null)
      {
        bool = paramThrowable instanceof e;
        if (bool)
        {
          int i = R.string.error_no_network;
          paramString = getString(i);
        }
      }
      else
      {
        bool = TextUtils.isEmpty(paramString);
        if (bool)
        {
          paramString = new java/lang/AssertionError;
          paramString.<init>("Empty toast message hit");
          d.a(paramString);
          return;
        }
      }
      paramThrowable = getActivity();
      localf = null;
      paramString = Toast.makeText(paramThrowable, paramString, 0);
      paramString.show();
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    int i = a();
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    m = paramLayoutInflater;
    return m;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.fragments.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */