package com.truecaller.truepay.app.ui.base.views.b;

import android.support.v4.f.o;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import java.util.Collections;
import java.util.List;

public final class e
{
  private static final List c = ;
  protected o a;
  protected d b;
  
  public e()
  {
    o localo = new android/support/v4/f/o;
    localo.<init>();
    a = localo;
  }
  
  public final int a(Object paramObject, int paramInt)
  {
    if (paramObject != null)
    {
      Object localObject = a;
      int i = ((o)localObject).c();
      int j = 0;
      while (j < i)
      {
        d locald = (d)a.d(j);
        boolean bool = locald.a(paramObject, paramInt);
        if (bool) {
          return a.c(j);
        }
        j += 1;
      }
      paramObject = b;
      if (paramObject != null) {
        return 2147483646;
      }
      paramObject = new java/lang/NullPointerException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("No AdapterDelegate added that matches position=");
      ((StringBuilder)localObject).append(paramInt);
      ((StringBuilder)localObject).append(" in data source");
      String str = ((StringBuilder)localObject).toString();
      ((NullPointerException)paramObject).<init>(str);
      throw ((Throwable)paramObject);
    }
    paramObject = new java/lang/NullPointerException;
    ((NullPointerException)paramObject).<init>("Items datasource is null!");
    throw ((Throwable)paramObject);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup, int paramInt)
  {
    d locald = a(paramInt);
    if (locald != null) {
      return locald.a(paramViewGroup);
    }
    paramViewGroup = new java/lang/NullPointerException;
    String str = String.valueOf(paramInt);
    str = "No AdapterDelegate added for ViewType ".concat(str);
    paramViewGroup.<init>(str);
    throw paramViewGroup;
  }
  
  public final d a(int paramInt)
  {
    o localo = a;
    d locald = (d)localo.a(paramInt, null);
    if (locald == null)
    {
      locald = b;
      if (locald == null) {
        return null;
      }
      return locald;
    }
    return locald;
  }
  
  public final e a(d paramd)
  {
    Object localObject1 = a;
    int i = ((o)localObject1).c();
    Object localObject2;
    int j;
    do
    {
      localObject2 = a.a(i, null);
      j = 2147483646;
      if (localObject2 == null) {
        break;
      }
      i += 1;
    } while (i != j);
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("Oops, we are very close to Integer.MAX_VALUE. It seems that there are no more free and unused view type integers left to add another AdapterDelegate.");
    throw paramd;
    if (i != j)
    {
      localObject2 = a.a(i, null);
      if (localObject2 == null)
      {
        a.b(i, paramd);
        return this;
      }
      paramd = new java/lang/IllegalArgumentException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("An AdapterDelegate is already registered for the viewType = ");
      ((StringBuilder)localObject2).append(i);
      ((StringBuilder)localObject2).append(". Already registered AdapterDelegate is ");
      localObject1 = a.a(i, null);
      ((StringBuilder)localObject2).append(localObject1);
      localObject1 = ((StringBuilder)localObject2).toString();
      paramd.<init>((String)localObject1);
      throw paramd;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("The view type = 2147483646 is reserved for fallback adapter delegate (see setFallbackDelegate() ). Please use another view type.");
    throw paramd;
  }
  
  public final void a(Object paramObject, int paramInt, RecyclerView.ViewHolder paramViewHolder, List paramList)
  {
    int i = paramViewHolder.getItemViewType();
    d locald = a(i);
    if (locald != null)
    {
      locald.a(paramObject, paramInt, paramViewHolder, paramList);
      return;
    }
    paramObject = new java/lang/NullPointerException;
    paramList = new java/lang/StringBuilder;
    paramList.<init>("No delegate found for item at position = ");
    paramList.append(paramInt);
    paramList.append(" for viewType = ");
    paramInt = paramViewHolder.getItemViewType();
    paramList.append(paramInt);
    String str = paramList.toString();
    ((NullPointerException)paramObject).<init>(str);
    throw ((Throwable)paramObject);
  }
  
  public final e b(d paramd)
  {
    b = paramd;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */