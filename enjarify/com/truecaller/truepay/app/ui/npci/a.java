package com.truecaller.truepay.app.ui.npci;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Base64;
import com.google.gson.l;
import com.truecaller.truepay.a.a.e.m;
import io.reactivex.n;
import io.reactivex.q;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import org.npci.upi.security.services.CLRemoteResultReceiver;
import org.npci.upi.security.services.CLServices;

public final class a
{
  final SharedPreferences.Editor a;
  boolean b;
  b c;
  Handler d;
  private final String e;
  private final Context f;
  private final SharedPreferences g;
  private final c h;
  private String i;
  private String j;
  private String k;
  private String l;
  private String m;
  private String n;
  private String o;
  private String p;
  private String q;
  private String r;
  private String s;
  private int t = 0;
  private m u;
  private Runnable v;
  
  public a(SharedPreferences paramSharedPreferences, SharedPreferences.Editor paramEditor, c paramc, Context paramContext, String paramString, m paramm)
  {
    -..Lambda.a.-Gi_mg2wmKmtavlv1_D7wcN03Uw local-Gi_mg2wmKmtavlv1_D7wcN03Uw = new com/truecaller/truepay/app/ui/npci/-$$Lambda$a$-Gi_mg2wmKmtavlv1_D7wcN03Uw;
    local-Gi_mg2wmKmtavlv1_D7wcN03Uw.<init>(this);
    v = local-Gi_mg2wmKmtavlv1_D7wcN03Uw;
    g = paramSharedPreferences;
    a = paramEditor;
    h = paramc;
    f = paramContext;
    e = paramString;
    u = paramm;
  }
  
  private String a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    String str1 = "";
    Object localObject = "production";
    int i1 = -1;
    try
    {
      int i2 = ((String)localObject).hashCode();
      int i3 = -1897523141;
      int i4 = 1;
      int i5 = 2;
      String str2;
      boolean bool;
      if (i2 != i3)
      {
        i3 = -316279908;
        if (i2 != i3)
        {
          i3 = 1753018553;
          if (i2 == i3)
          {
            str2 = "production";
            bool = ((String)localObject).equals(str2);
            if (bool) {
              i1 = 1;
            }
          }
        }
        else
        {
          str2 = "preproduction";
          bool = ((String)localObject).equals(str2);
          if (bool) {
            i1 = 0;
          }
        }
      }
      else
      {
        str2 = "staging";
        bool = ((String)localObject).equals(str2);
        if (bool) {
          i1 = 2;
        }
      }
      switch (i1)
      {
      default: 
        localObject = new java/lang/StringBuilder;
        break;
      case 2: 
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(paramString1);
        paramString1 = "|";
        ((StringBuilder)localObject).append(paramString1);
        ((StringBuilder)localObject).append(paramString2);
        paramString1 = "|";
        ((StringBuilder)localObject).append(paramString1);
        ((StringBuilder)localObject).append(paramString4);
        paramString1 = ((StringBuilder)localObject).toString();
        break;
      case 0: 
      case 1: 
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(paramString4);
        paramString4 = "|";
        ((StringBuilder)localObject).append(paramString4);
        ((StringBuilder)localObject).append(paramString1);
        paramString1 = "|";
        ((StringBuilder)localObject).append(paramString1);
        ((StringBuilder)localObject).append(paramString2);
        paramString1 = ((StringBuilder)localObject).toString();
        break;
      }
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(paramString4);
      paramString4 = "|";
      ((StringBuilder)localObject).append(paramString4);
      ((StringBuilder)localObject).append(paramString1);
      paramString1 = "|";
      ((StringBuilder)localObject).append(paramString1);
      ((StringBuilder)localObject).append(paramString2);
      paramString1 = ((StringBuilder)localObject).toString();
      paramString2 = new String[i5];
      paramString4 = "HMAC";
      paramString2[0] = paramString4;
      paramString2[i4] = paramString1;
      paramString2 = c.b(paramString3);
      paramString1 = c.a(paramString1);
      paramString1 = c.a(paramString1, paramString2);
      str1 = Base64.encodeToString(paramString1, i5);
    }
    catch (Exception localException)
    {
      paramString1 = localException.getMessage();
      paramString2 = "0";
      a(paramString1, paramString2);
    }
    return str1;
  }
  
  private static String b(String paramString1, String paramString2)
  {
    paramString1 = d.a.getChallenge(paramString2, paramString1);
    paramString2 = new String[2];
    paramString2[0] = "challenge";
    paramString2[1] = paramString1;
    return paramString1;
  }
  
  private void b()
  {
    int i1 = 2;
    Object localObject1 = new String[i1];
    localObject1[0] = "@credString";
    Object localObject2 = m;
    int i2 = 1;
    localObject1[i2] = localObject2;
    try
    {
      localObject1 = i;
      localObject1 = c.b((String)localObject1);
      localObject2 = q;
      localObject2 = c.a((String)localObject2);
      localObject1 = c.a((byte[])localObject2, (byte[])localObject1);
      localObject1 = Base64.encodeToString((byte[])localObject1, i1);
      q = ((String)localObject1);
      Object localObject3 = new String[i1];
      localObject1 = "Trust Msg :";
      localObject3[0] = localObject1;
      localObject1 = q;
      localObject3[i2] = localObject1;
      CLRemoteResultReceiver localCLRemoteResultReceiver = new org/npci/upi/security/services/CLRemoteResultReceiver;
      localObject3 = new com/truecaller/truepay/app/ui/npci/CLAuthParser$3;
      localObject1 = new android/os/Handler;
      ((Handler)localObject1).<init>();
      ((CLAuthParser.3)localObject3).<init>(this, (Handler)localObject1);
      localCLRemoteResultReceiver.<init>((ResultReceiver)localObject3);
      CLServices localCLServices = d.a;
      String str1 = n;
      String str2 = e;
      String str3 = m;
      String str4 = o;
      String str5 = s;
      String str6 = p;
      String str7 = q;
      String str8 = r;
      localCLServices.getCredential(str1, str2, str3, str4, str5, str6, str7, str8, localCLRemoteResultReceiver);
      try
      {
        t = 0;
        a();
        return;
      }
      catch (Exception localException1)
      {
        localObject3 = new java/lang/AssertionError;
        localObject1 = "General exception handled for request focus";
        ((AssertionError)localObject3).<init>(localObject1);
        com.truecaller.log.d.a((Throwable)localObject3);
        return;
      }
      return;
    }
    catch (Exception localException2)
    {
      localObject3 = localException2.getMessage();
      a((String)localObject3, "0");
    }
  }
  
  private static Activity c()
  {
    Object localObject1 = "android.app.ActivityThread";
    try
    {
      localObject1 = Class.forName((String)localObject1);
      localObject2 = "currentActivityThread";
      boolean bool1 = false;
      Object localObject3 = null;
      Object localObject4 = new Class[0];
      localObject2 = ((Class)localObject1).getMethod((String)localObject2, (Class[])localObject4);
      localObject3 = new Object[0];
      localObject2 = ((Method)localObject2).invoke(null, (Object[])localObject3);
      localObject3 = "mActivities";
      localObject1 = ((Class)localObject1).getDeclaredField((String)localObject3);
      bool1 = true;
      ((Field)localObject1).setAccessible(bool1);
      localObject1 = ((Field)localObject1).get(localObject2);
      localObject1 = (Map)localObject1;
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((Map)localObject1).values();
      localObject1 = ((Collection)localObject1).iterator();
      boolean bool3;
      do
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = ((Iterator)localObject1).next();
        localObject4 = localObject2.getClass();
        Object localObject5 = "paused";
        localObject5 = ((Class)localObject4).getDeclaredField((String)localObject5);
        ((Field)localObject5).setAccessible(bool1);
        bool3 = ((Field)localObject5).getBoolean(localObject2);
      } while (bool3);
      localObject1 = "activity";
      localObject1 = ((Class)localObject4).getDeclaredField((String)localObject1);
      ((Field)localObject1).setAccessible(bool1);
      localObject1 = ((Field)localObject1).get(localObject2);
      return (Activity)localObject1;
    }
    catch (ClassNotFoundException|IllegalAccessException|InvocationTargetException|NoSuchMethodException|NoSuchFieldException localClassNotFoundException)
    {
      localObject1 = new java/lang/AssertionError;
      Object localObject2 = "Exception in reflection for request focus";
      ((AssertionError)localObject1).<init>(localObject2);
      com.truecaller.log.d.a((Throwable)localObject1);
    }
    return null;
  }
  
  final void a()
  {
    int i1 = t;
    int i2 = 6;
    if (i1 < i2)
    {
      i1 += 1;
      t = i1;
      Handler localHandler = d;
      if (localHandler == null)
      {
        localHandler = new android/os/Handler;
        localHandler.<init>();
        d = localHandler;
      }
      localHandler = d;
      Runnable localRunnable = v;
      long l1 = 300L;
      localHandler.postDelayed(localRunnable, l1);
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.npci.b.a parama, b paramb)
  {
    Object localObject = g;
    String str = "appId";
    localObject = ((com.google.gson.o)localObject).b(str).toString();
    j = ((String)localObject);
    localObject = l;
    n = ((String)localObject);
    localObject = i.toString();
    m = ((String)localObject);
    localObject = j.toString();
    o = ((String)localObject);
    localObject = g.toString();
    s = ((String)localObject);
    localObject = h.toString();
    p = ((String)localObject);
    localObject = f;
    q = ((String)localObject);
    localObject = k;
    r = ((String)localObject);
    c = paramb;
    paramb = d;
    l = paramb;
    parama = e;
    k = parama;
    parama = d.a;
    if (parama != null)
    {
      parama = "initial";
      try
      {
        a(parama);
        return;
      }
      catch (Exception localException)
      {
        parama = localException.getMessage();
        a(parama, "0");
        return;
      }
    }
    parama = f;
    paramb = new com/truecaller/truepay/app/ui/npci/a$1;
    paramb.<init>(this);
    CLServices.initService(parama, paramb);
  }
  
  final void a(String paramString)
  {
    Object localObject1 = g;
    Object localObject2 = "tokenapi";
    localObject1 = ((SharedPreferences)localObject1).getString((String)localObject2, "");
    i = ((String)localObject1);
    localObject1 = i;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    int i1 = 1;
    boolean bool2 = false;
    String str1 = null;
    int i2 = 2;
    if (!bool1)
    {
      paramString = new String[i2];
      paramString[0] = "@TokenAPI";
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      str2 = j;
      ((StringBuilder)localObject1).append(str2);
      ((StringBuilder)localObject1).append(":");
      str2 = l;
      ((StringBuilder)localObject1).append(str2);
      ((StringBuilder)localObject1).append(":");
      str2 = i;
      ((StringBuilder)localObject1).append(str2);
      ((StringBuilder)localObject1).append(":");
      str2 = k;
      ((StringBuilder)localObject1).append(str2);
      localObject1 = ((StringBuilder)localObject1).toString();
      paramString[i1] = localObject1;
      paramString = j;
      localObject1 = l;
      str2 = i;
      String str3 = k;
      paramString = a(paramString, (String)localObject1, str2, str3);
      localObject1 = new String[i2];
      localObject1[0] = "hmac";
      localObject1[i1] = paramString;
      localObject1 = d.a;
      str2 = j;
      str3 = l;
      String str4 = k;
      boolean bool3 = ((CLServices)localObject1).registerApp(str2, str3, str4, paramString);
      b = bool3;
      paramString = new String[i2];
      paramString[0] = "clInitialized";
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      bool2 = b;
      ((StringBuilder)localObject1).append(bool2);
      localObject1 = ((StringBuilder)localObject1).toString();
      paramString[i1] = localObject1;
      b();
      return;
    }
    localObject1 = b(k, paramString);
    if (localObject1 == null)
    {
      paramString = new String[i2];
      localObject1 = getClass().getName();
      paramString[0] = localObject1;
      paramString[i1] = "Error while getting challenge.";
      return;
    }
    String[] arrayOfString = new String[i2];
    String str2 = getClass().getName();
    arrayOfString[0] = str2;
    arrayOfString[i1] = "With ListKeys";
    localObject2 = new com/truecaller/truepay/app/ui/registration/c/i;
    str1 = k;
    ((com.truecaller.truepay.app.ui.registration.c.i)localObject2).<init>(str1, paramString, (String)localObject1);
    localObject1 = u.a((com.truecaller.truepay.app.ui.registration.c.i)localObject2);
    localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.o)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.o)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/npci/a$2;
    ((a.2)localObject2).<init>(this, paramString);
    ((io.reactivex.o)localObject1).a((q)localObject2);
  }
  
  final void a(String paramString1, String paramString2)
  {
    b localb = c;
    if (localb != null)
    {
      int i1 = Integer.parseInt(paramString2);
      localb.a(paramString1, i1);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.npci.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */