package com.truecaller.truepay.app.ui.npci.a;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final a a;
  private final Provider b;
  
  private d(a parama, Provider paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static d a(a parama, Provider paramProvider)
  {
    d locald = new com/truecaller/truepay/app/ui/npci/a/d;
    locald.<init>(parama, paramProvider);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.npci.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */