package com.truecaller.truepay.app.ui.npci;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final Provider a;
  
  private g(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static g a(Provider paramProvider)
  {
    g localg = new com/truecaller/truepay/app/ui/npci/g;
    localg.<init>(paramProvider);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.npci.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */