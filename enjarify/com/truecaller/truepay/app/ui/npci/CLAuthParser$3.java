package com.truecaller.truepay.app.ui.npci;

import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import com.google.gson.l;
import com.google.gson.q;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.services.CLServices;

class CLAuthParser$3
  extends ResultReceiver
{
  CLAuthParser$3(a parama, Handler paramHandler)
  {
    super(paramHandler);
  }
  
  protected void onReceiveResult(int paramInt, Bundle paramBundle)
  {
    super.onReceiveResult(paramInt, paramBundle);
    a locala = a;
    int i;
    Object localObject3;
    if (paramBundle != null)
    {
      localObject1 = paramBundle.getString("error");
      i = 2;
      if (localObject1 != null)
      {
        boolean bool2 = ((String)localObject1).isEmpty();
        if (!bool2)
        {
          localObject2 = new String[i];
          paramBundle = null;
          localObject3 = "Error:";
          localObject2[0] = localObject3;
          int j = 1;
          localObject2[j] = localObject1;
          localObject2 = "USER_ABORTED";
          paramInt = ((String)localObject2).equalsIgnoreCase((String)localObject1);
          if (paramInt != 0)
          {
            paramInt = 0;
            localObject2 = null;
            c = null;
            d = null;
            break label356;
          }
        }
      }
    }
    try
    {
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>((String)localObject1);
      paramBundle = "errorCode";
      paramBundle = ((JSONObject)localObject2).getString(paramBundle);
      localObject1 = "errorText";
      localObject2 = ((JSONObject)localObject2).getString((String)localObject1);
      locala.a((String)localObject2, paramBundle);
    }
    catch (JSONException localJSONException2)
    {
      for (;;) {}
    }
    Object localObject2 = a;
    paramBundle = "tokenapi";
    Object localObject1 = "";
    ((SharedPreferences.Editor)localObject2).putString(paramBundle, (String)localObject1);
    localObject2 = a;
    ((SharedPreferences.Editor)localObject2).commit();
    break label356;
    if (paramInt == i)
    {
      localObject2 = c;
      if (localObject2 != null)
      {
        localObject2 = c;
        ((b)localObject2).c();
      }
    }
    else
    {
      localObject2 = (HashMap)paramBundle.getSerializable("credBlocks");
      paramBundle = new org/json/JSONArray;
      paramBundle.<init>();
      if (localObject2 != null)
      {
        localObject1 = ((HashMap)localObject2).keySet().iterator();
        for (;;)
        {
          boolean bool1 = ((Iterator)localObject1).hasNext();
          if (!bool1) {
            break;
          }
          localObject3 = (String)((Iterator)localObject1).next();
          try
          {
            JSONObject localJSONObject = new org/json/JSONObject;
            localObject3 = ((HashMap)localObject2).get(localObject3);
            localObject3 = (String)localObject3;
            localJSONObject.<init>((String)localObject3);
            paramBundle.put(localJSONObject);
          }
          catch (JSONException localJSONException1) {}
        }
        localObject2 = q.a(paramBundle.toString()).j();
        paramBundle = c;
        if (paramBundle != null)
        {
          paramBundle = c;
          paramBundle.a(localObject2);
        }
      }
    }
    label356:
    localObject2 = d.a;
    if (localObject2 != null) {
      try
      {
        localObject2 = d.a;
        ((CLServices)localObject2).unbindService();
        return;
      }
      catch (Exception localException) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.npci.CLAuthParser.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */