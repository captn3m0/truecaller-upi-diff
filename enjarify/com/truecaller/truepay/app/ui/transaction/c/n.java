package com.truecaller.truepay.app.ui.transaction.c;

import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.a.a.f.f;
import com.truecaller.truepay.data.e.c;
import io.reactivex.d;
import io.reactivex.d.b.b;
import io.reactivex.h;
import io.reactivex.o;
import io.reactivex.q;
import io.reactivex.s;

public final class n
  extends com.truecaller.truepay.app.ui.base.a.a
  implements m
{
  e a;
  c b;
  private com.truecaller.truepay.a.a.f.g c;
  private com.truecaller.truepay.a.a.f.j f;
  private f g;
  
  public n(com.truecaller.truepay.a.a.f.g paramg, f paramf, com.truecaller.truepay.a.a.f.j paramj, e parame, c paramc)
  {
    c = paramg;
    g = paramf;
    f = paramj;
    a = parame;
    b = paramc;
  }
  
  public final void a()
  {
    Object localObject1 = c;
    Object localObject2 = b.a();
    boolean bool = ((Boolean)localObject2).booleanValue();
    if (bool)
    {
      localObject1 = a;
      localObject2 = a.a();
      localObject1 = ((com.truecaller.truepay.data.f.g)localObject1).b();
      localObject1 = o.a((s)localObject2, (s)localObject1);
      localObject2 = new io/reactivex/d/e/a/j;
      ((io.reactivex.d.e.a.j)localObject2).<init>((d)localObject1);
      localObject1 = io.reactivex.e.a.a((h)localObject2);
    }
    else
    {
      localObject1 = a.a().c();
    }
    localObject2 = io.reactivex.g.a.b();
    b.a(localObject2, "scheduler is null");
    io.reactivex.d.e.b.g localg = new io/reactivex/d/e/b/g;
    localg.<init>((io.reactivex.j)localObject1, (io.reactivex.n)localObject2);
    localObject1 = io.reactivex.e.a.a(localg);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((h)localObject1).a((io.reactivex.n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/transaction/c/n$1;
    ((n.1)localObject2).<init>(this);
    ((h)localObject1).a((io.reactivex.i)localObject2);
  }
  
  public final void a(String paramString)
  {
    paramString = g.a.a(paramString);
    Object localObject = io.reactivex.g.a.b();
    paramString = paramString.b((io.reactivex.n)localObject);
    localObject = io.reactivex.android.b.a.a();
    paramString = paramString.a((io.reactivex.n)localObject);
    localObject = new com/truecaller/truepay/app/ui/transaction/c/n$2;
    ((n.2)localObject).<init>(this);
    paramString.a((q)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */