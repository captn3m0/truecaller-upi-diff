package com.truecaller.truepay.app.ui.transaction.c;

import c.d.c;
import c.g.a.m;
import c.x;
import com.truecaller.truepay.data.d.b;
import kotlinx.coroutines.ag;

final class d$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  d$b(d paramd, c.b paramb, b paramb1, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/truepay/app/ui/transaction/c/d$b;
    d locald = b;
    c.b localb1 = c;
    b localb2 = d;
    localb.<init>(locald, localb1, localb2, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localb;
  }
  
  /* Error */
  public final Object a(Object paramObject)
  {
    // Byte code:
    //   0: getstatic 46	c/d/a/a:a	Lc/d/a/a;
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 48	com/truecaller/truepay/app/ui/transaction/c/d$b:a	I
    //   8: istore_3
    //   9: iconst_1
    //   10: istore 4
    //   12: iload_3
    //   13: tableswitch	default:+23->36, 0:+65->78, 1:+35->48
    //   36: new 51	java/lang/IllegalStateException
    //   39: astore_1
    //   40: aload_1
    //   41: ldc 53
    //   43: invokespecial 56	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   46: aload_1
    //   47: athrow
    //   48: aload_1
    //   49: instanceof 58
    //   52: istore 5
    //   54: iload 5
    //   56: ifne +6 -> 62
    //   59: goto +101 -> 160
    //   62: aload_1
    //   63: checkcast 58	c/o$b
    //   66: astore_1
    //   67: aload_1
    //   68: getfield 61	c/o$b:a	Ljava/lang/Throwable;
    //   71: astore_1
    //   72: aload_1
    //   73: athrow
    //   74: astore_1
    //   75: goto +307 -> 382
    //   78: aload_1
    //   79: instanceof 58
    //   82: istore_3
    //   83: iload_3
    //   84: ifne +310 -> 394
    //   87: aload_0
    //   88: getfield 20	com/truecaller/truepay/app/ui/transaction/c/d$b:c	Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   91: astore_1
    //   92: aload_1
    //   93: iload 4
    //   95: invokeinterface 66 2 0
    //   100: aload_0
    //   101: getfield 18	com/truecaller/truepay/app/ui/transaction/c/d$b:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   104: astore_1
    //   105: aload_0
    //   106: getfield 22	com/truecaller/truepay/app/ui/transaction/c/d$b:d	Lcom/truecaller/truepay/data/d/b;
    //   109: astore 6
    //   111: aload_0
    //   112: iload 4
    //   114: putfield 48	com/truecaller/truepay/app/ui/transaction/c/d$b:a	I
    //   117: aload_1
    //   118: getfield 71	com/truecaller/truepay/app/ui/transaction/c/d:c	Lc/d/f;
    //   121: astore 7
    //   123: new 73	com/truecaller/truepay/app/ui/transaction/c/d$d
    //   126: astore 8
    //   128: aload 8
    //   130: aload_1
    //   131: aload 6
    //   133: aconst_null
    //   134: invokespecial 76	com/truecaller/truepay/app/ui/transaction/c/d$d:<init>	(Lcom/truecaller/truepay/app/ui/transaction/c/d;Lcom/truecaller/truepay/data/d/b;Lc/d/c;)V
    //   137: aload 8
    //   139: checkcast 6	c/g/a/m
    //   142: astore 8
    //   144: aload 7
    //   146: aload 8
    //   148: aload_0
    //   149: invokestatic 81	kotlinx/coroutines/g:a	(Lc/d/f;Lc/g/a/m;Lc/d/c;)Ljava/lang/Object;
    //   152: astore_1
    //   153: aload_1
    //   154: aload_2
    //   155: if_acmpne +5 -> 160
    //   158: aload_2
    //   159: areturn
    //   160: aload_1
    //   161: checkcast 83	java/lang/Boolean
    //   164: astore_1
    //   165: aload_1
    //   166: invokevirtual 87	java/lang/Boolean:booleanValue	()Z
    //   169: istore 9
    //   171: iload 9
    //   173: ifeq +87 -> 260
    //   176: aload_0
    //   177: getfield 20	com/truecaller/truepay/app/ui/transaction/c/d$b:c	Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   180: astore_1
    //   181: aload_0
    //   182: getfield 18	com/truecaller/truepay/app/ui/transaction/c/d$b:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   185: astore_2
    //   186: aload_2
    //   187: invokestatic 90	com/truecaller/truepay/app/ui/transaction/c/d:a	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/utils/n;
    //   190: astore_2
    //   191: getstatic 95	com/truecaller/truepay/R$string:unblock_successful_msg	I
    //   194: istore_3
    //   195: iload 4
    //   197: anewarray 97	java/lang/Object
    //   200: astore 10
    //   202: aload_0
    //   203: getfield 22	com/truecaller/truepay/app/ui/transaction/c/d$b:d	Lcom/truecaller/truepay/data/d/b;
    //   206: astore 7
    //   208: aload 7
    //   210: invokevirtual 102	com/truecaller/truepay/data/d/b:a	()Ljava/lang/String;
    //   213: astore 7
    //   215: aload 10
    //   217: iconst_0
    //   218: aload 7
    //   220: aastore
    //   221: aload_2
    //   222: iload_3
    //   223: aload 10
    //   225: invokeinterface 107 3 0
    //   230: astore_2
    //   231: ldc 109
    //   233: astore 6
    //   235: aload_2
    //   236: aload 6
    //   238: invokestatic 111	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   241: aload_1
    //   242: aload_2
    //   243: invokeinterface 113 2 0
    //   248: aload_0
    //   249: getfield 18	com/truecaller/truepay/app/ui/transaction/c/d$b:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   252: astore_1
    //   253: aload_1
    //   254: invokestatic 116	com/truecaller/truepay/app/ui/transaction/c/d:b	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)V
    //   257: goto +111 -> 368
    //   260: aload_0
    //   261: getfield 20	com/truecaller/truepay/app/ui/transaction/c/d$b:c	Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   264: astore_1
    //   265: aload_0
    //   266: getfield 18	com/truecaller/truepay/app/ui/transaction/c/d$b:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   269: astore_2
    //   270: aload_2
    //   271: invokestatic 90	com/truecaller/truepay/app/ui/transaction/c/d:a	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/utils/n;
    //   274: astore_2
    //   275: getstatic 119	com/truecaller/truepay/R$string:unblock_vpa_error	I
    //   278: istore_3
    //   279: iconst_0
    //   280: anewarray 97	java/lang/Object
    //   283: astore 10
    //   285: aload_2
    //   286: iload_3
    //   287: aload 10
    //   289: invokeinterface 107 3 0
    //   294: astore_2
    //   295: ldc 121
    //   297: astore 6
    //   299: aload_2
    //   300: aload 6
    //   302: invokestatic 111	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   305: aload_1
    //   306: aload_2
    //   307: invokeinterface 113 2 0
    //   312: goto +56 -> 368
    //   315: pop
    //   316: aload_0
    //   317: getfield 20	com/truecaller/truepay/app/ui/transaction/c/d$b:c	Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   320: astore_1
    //   321: aload_0
    //   322: getfield 18	com/truecaller/truepay/app/ui/transaction/c/d$b:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   325: astore_2
    //   326: aload_2
    //   327: invokestatic 90	com/truecaller/truepay/app/ui/transaction/c/d:a	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/utils/n;
    //   330: astore_2
    //   331: getstatic 119	com/truecaller/truepay/R$string:unblock_vpa_error	I
    //   334: istore_3
    //   335: iconst_0
    //   336: anewarray 97	java/lang/Object
    //   339: astore 10
    //   341: aload_2
    //   342: iload_3
    //   343: aload 10
    //   345: invokeinterface 107 3 0
    //   350: astore_2
    //   351: ldc 121
    //   353: astore 6
    //   355: aload_2
    //   356: aload 6
    //   358: invokestatic 111	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   361: aload_1
    //   362: aload_2
    //   363: invokeinterface 113 2 0
    //   368: aload_0
    //   369: getfield 20	com/truecaller/truepay/app/ui/transaction/c/d$b:c	Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   372: iconst_0
    //   373: invokeinterface 66 2 0
    //   378: getstatic 126	c/x:a	Lc/x;
    //   381: areturn
    //   382: aload_0
    //   383: getfield 20	com/truecaller/truepay/app/ui/transaction/c/d$b:c	Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   386: iconst_0
    //   387: invokeinterface 66 2 0
    //   392: aload_1
    //   393: athrow
    //   394: aload_1
    //   395: checkcast 58	c/o$b
    //   398: getfield 61	c/o$b:a	Ljava/lang/Throwable;
    //   401: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	402	0	this	b
    //   0	402	1	paramObject	Object
    //   3	360	2	localObject1	Object
    //   8	5	3	i	int
    //   82	2	3	bool1	boolean
    //   194	149	3	j	int
    //   10	186	4	k	int
    //   52	3	5	bool2	boolean
    //   109	248	6	localObject2	Object
    //   121	98	7	localObject3	Object
    //   126	21	8	localObject4	Object
    //   169	3	9	bool3	boolean
    //   200	144	10	arrayOfObject	Object[]
    //   315	1	13	localCancellationException	java.util.concurrent.CancellationException
    // Exception table:
    //   from	to	target	type
    //   62	66	74	finally
    //   67	71	74	finally
    //   72	74	74	finally
    //   87	91	74	finally
    //   93	100	74	finally
    //   100	104	74	finally
    //   105	109	74	finally
    //   112	117	74	finally
    //   117	121	74	finally
    //   123	126	74	finally
    //   133	137	74	finally
    //   137	142	74	finally
    //   148	152	74	finally
    //   160	164	74	finally
    //   165	169	74	finally
    //   176	180	74	finally
    //   181	185	74	finally
    //   186	190	74	finally
    //   191	194	74	finally
    //   195	200	74	finally
    //   202	206	74	finally
    //   208	213	74	finally
    //   218	221	74	finally
    //   223	230	74	finally
    //   236	241	74	finally
    //   242	248	74	finally
    //   248	252	74	finally
    //   253	257	74	finally
    //   260	264	74	finally
    //   265	269	74	finally
    //   270	274	74	finally
    //   275	278	74	finally
    //   279	283	74	finally
    //   287	294	74	finally
    //   300	305	74	finally
    //   306	312	74	finally
    //   316	320	74	finally
    //   321	325	74	finally
    //   326	330	74	finally
    //   331	334	74	finally
    //   335	339	74	finally
    //   343	350	74	finally
    //   356	361	74	finally
    //   362	368	74	finally
    //   62	66	315	java/util/concurrent/CancellationException
    //   67	71	315	java/util/concurrent/CancellationException
    //   72	74	315	java/util/concurrent/CancellationException
    //   87	91	315	java/util/concurrent/CancellationException
    //   93	100	315	java/util/concurrent/CancellationException
    //   100	104	315	java/util/concurrent/CancellationException
    //   105	109	315	java/util/concurrent/CancellationException
    //   112	117	315	java/util/concurrent/CancellationException
    //   117	121	315	java/util/concurrent/CancellationException
    //   123	126	315	java/util/concurrent/CancellationException
    //   133	137	315	java/util/concurrent/CancellationException
    //   137	142	315	java/util/concurrent/CancellationException
    //   148	152	315	java/util/concurrent/CancellationException
    //   160	164	315	java/util/concurrent/CancellationException
    //   165	169	315	java/util/concurrent/CancellationException
    //   176	180	315	java/util/concurrent/CancellationException
    //   181	185	315	java/util/concurrent/CancellationException
    //   186	190	315	java/util/concurrent/CancellationException
    //   191	194	315	java/util/concurrent/CancellationException
    //   195	200	315	java/util/concurrent/CancellationException
    //   202	206	315	java/util/concurrent/CancellationException
    //   208	213	315	java/util/concurrent/CancellationException
    //   218	221	315	java/util/concurrent/CancellationException
    //   223	230	315	java/util/concurrent/CancellationException
    //   236	241	315	java/util/concurrent/CancellationException
    //   242	248	315	java/util/concurrent/CancellationException
    //   248	252	315	java/util/concurrent/CancellationException
    //   253	257	315	java/util/concurrent/CancellationException
    //   260	264	315	java/util/concurrent/CancellationException
    //   265	269	315	java/util/concurrent/CancellationException
    //   270	274	315	java/util/concurrent/CancellationException
    //   275	278	315	java/util/concurrent/CancellationException
    //   279	283	315	java/util/concurrent/CancellationException
    //   287	294	315	java/util/concurrent/CancellationException
    //   300	305	315	java/util/concurrent/CancellationException
    //   306	312	315	java/util/concurrent/CancellationException
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */