package com.truecaller.truepay.app.ui.transaction.c;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.truepay.data.d.b;
import com.truecaller.utils.n;
import kotlinx.coroutines.e;

public final class d
  extends ba
  implements c.a
{
  final f c;
  private final f d;
  private final com.truecaller.truepay.app.utils.a.d e;
  private final n f;
  
  public d(f paramf1, f paramf2, com.truecaller.truepay.app.utils.a.d paramd, n paramn)
  {
    super(paramf2);
    c = paramf1;
    d = paramf2;
    e = paramd;
    f = paramn;
  }
  
  private final void e()
  {
    Object localObject = new com/truecaller/truepay/app/ui/transaction/c/d$c;
    ((d.c)localObject).<init>(this, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
  }
  
  public final void a()
  {
    c.b localb = (c.b)b;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void a(b paramb)
  {
    c.b localb = (c.b)b;
    if (localb == null) {
      return;
    }
    if (paramb == null) {
      return;
    }
    Object localObject = new com/truecaller/truepay/app/ui/transaction/c/d$b;
    ((d.b)localObject).<init>(this, localb, paramb, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
  }
  
  public final void b()
  {
    e();
  }
  
  public final void b(b paramb)
  {
    k.b(paramb, "blockedVpa");
    c.b localb = (c.b)b;
    if (localb != null)
    {
      localb.a(paramb);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */