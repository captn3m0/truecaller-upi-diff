package com.truecaller.truepay.app.ui.transaction.c;

import c.d.c;
import c.g.a.m;
import c.x;
import kotlinx.coroutines.ag;

final class d$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  d$c(d paramd, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/truepay/app/ui/transaction/c/d$c;
    d locald = b;
    localc.<init>(locald, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localc;
  }
  
  /* Error */
  public final Object a(Object paramObject)
  {
    // Byte code:
    //   0: getstatic 38	c/d/a/a:a	Lc/d/a/a;
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 40	com/truecaller/truepay/app/ui/transaction/c/d$c:a	I
    //   8: istore_3
    //   9: iconst_1
    //   10: istore 4
    //   12: iload_3
    //   13: tableswitch	default:+23->36, 0:+65->78, 1:+35->48
    //   36: new 43	java/lang/IllegalStateException
    //   39: astore_1
    //   40: aload_1
    //   41: ldc 45
    //   43: invokespecial 48	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   46: aload_1
    //   47: athrow
    //   48: aload_1
    //   49: instanceof 50
    //   52: istore 5
    //   54: iload 5
    //   56: ifne +6 -> 62
    //   59: goto +102 -> 161
    //   62: aload_1
    //   63: checkcast 50	c/o$b
    //   66: astore_1
    //   67: aload_1
    //   68: getfield 53	c/o$b:a	Ljava/lang/Throwable;
    //   71: astore_1
    //   72: aload_1
    //   73: athrow
    //   74: astore_1
    //   75: goto +287 -> 362
    //   78: aload_1
    //   79: instanceof 50
    //   82: istore_3
    //   83: iload_3
    //   84: ifne +299 -> 383
    //   87: aload_0
    //   88: getfield 14	com/truecaller/truepay/app/ui/transaction/c/d$c:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   91: astore_1
    //   92: aload_1
    //   93: invokestatic 58	com/truecaller/truepay/app/ui/transaction/c/d:c	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   96: astore_1
    //   97: aload_1
    //   98: ifnull +11 -> 109
    //   101: aload_1
    //   102: iload 4
    //   104: invokeinterface 63 2 0
    //   109: aload_0
    //   110: getfield 14	com/truecaller/truepay/app/ui/transaction/c/d$c:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   113: astore_1
    //   114: aload_0
    //   115: iload 4
    //   117: putfield 40	com/truecaller/truepay/app/ui/transaction/c/d$c:a	I
    //   120: aload_1
    //   121: getfield 66	com/truecaller/truepay/app/ui/transaction/c/d:c	Lc/d/f;
    //   124: astore 6
    //   126: new 68	com/truecaller/truepay/app/ui/transaction/c/d$a
    //   129: astore 7
    //   131: aload 7
    //   133: aload_1
    //   134: aconst_null
    //   135: invokespecial 69	com/truecaller/truepay/app/ui/transaction/c/d$a:<init>	(Lcom/truecaller/truepay/app/ui/transaction/c/d;Lc/d/c;)V
    //   138: aload 7
    //   140: checkcast 6	c/g/a/m
    //   143: astore 7
    //   145: aload 6
    //   147: aload 7
    //   149: aload_0
    //   150: invokestatic 74	kotlinx/coroutines/g:a	(Lc/d/f;Lc/g/a/m;Lc/d/c;)Ljava/lang/Object;
    //   153: astore_1
    //   154: aload_1
    //   155: aload_2
    //   156: if_acmpne +5 -> 161
    //   159: aload_2
    //   160: areturn
    //   161: aload_1
    //   162: checkcast 76	java/util/List
    //   165: astore_1
    //   166: aload_1
    //   167: astore_2
    //   168: aload_1
    //   169: checkcast 78	java/util/Collection
    //   172: astore_2
    //   173: aload_2
    //   174: invokeinterface 82 1 0
    //   179: iload 4
    //   181: ixor
    //   182: istore 5
    //   184: iload 5
    //   186: ifeq +34 -> 220
    //   189: aload_0
    //   190: getfield 14	com/truecaller/truepay/app/ui/transaction/c/d$c:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   193: astore_2
    //   194: aload_2
    //   195: invokestatic 58	com/truecaller/truepay/app/ui/transaction/c/d:c	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   198: astore_2
    //   199: aload_2
    //   200: ifnull +48 -> 248
    //   203: aload_2
    //   204: iconst_0
    //   205: invokeinterface 84 2 0
    //   210: aload_2
    //   211: aload_1
    //   212: invokeinterface 87 2 0
    //   217: goto +31 -> 248
    //   220: aload_0
    //   221: getfield 14	com/truecaller/truepay/app/ui/transaction/c/d$c:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   224: astore_1
    //   225: aload_1
    //   226: invokestatic 58	com/truecaller/truepay/app/ui/transaction/c/d:c	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   229: astore_1
    //   230: aload_1
    //   231: ifnull +17 -> 248
    //   234: aload_1
    //   235: iload 4
    //   237: invokeinterface 84 2 0
    //   242: aload_1
    //   243: invokeinterface 91 1 0
    //   248: aload_0
    //   249: getfield 14	com/truecaller/truepay/app/ui/transaction/c/d$c:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   252: invokestatic 58	com/truecaller/truepay/app/ui/transaction/c/d:c	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   255: astore_1
    //   256: aload_1
    //   257: ifnull +101 -> 358
    //   260: goto +91 -> 351
    //   263: pop
    //   264: aload_0
    //   265: getfield 14	com/truecaller/truepay/app/ui/transaction/c/d$c:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   268: astore_1
    //   269: aload_1
    //   270: invokestatic 58	com/truecaller/truepay/app/ui/transaction/c/d:c	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   273: astore_1
    //   274: aload_1
    //   275: ifnull +64 -> 339
    //   278: aload_1
    //   279: iload 4
    //   281: invokeinterface 84 2 0
    //   286: aload_1
    //   287: invokeinterface 91 1 0
    //   292: aload_0
    //   293: getfield 14	com/truecaller/truepay/app/ui/transaction/c/d$c:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   296: astore_2
    //   297: aload_2
    //   298: invokestatic 94	com/truecaller/truepay/app/ui/transaction/c/d:a	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/utils/n;
    //   301: astore_2
    //   302: getstatic 99	com/truecaller/truepay/R$string:error_fetching_pending_requests	I
    //   305: istore_3
    //   306: iconst_0
    //   307: anewarray 101	java/lang/Object
    //   310: astore 8
    //   312: aload_2
    //   313: iload_3
    //   314: aload 8
    //   316: invokeinterface 106 3 0
    //   321: astore_2
    //   322: ldc 108
    //   324: astore 6
    //   326: aload_2
    //   327: aload 6
    //   329: invokestatic 110	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   332: aload_1
    //   333: aload_2
    //   334: invokeinterface 112 2 0
    //   339: aload_0
    //   340: getfield 14	com/truecaller/truepay/app/ui/transaction/c/d$c:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   343: invokestatic 58	com/truecaller/truepay/app/ui/transaction/c/d:c	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   346: astore_1
    //   347: aload_1
    //   348: ifnull +10 -> 358
    //   351: aload_1
    //   352: iconst_0
    //   353: invokeinterface 63 2 0
    //   358: getstatic 117	c/x:a	Lc/x;
    //   361: areturn
    //   362: aload_0
    //   363: getfield 14	com/truecaller/truepay/app/ui/transaction/c/d$c:b	Lcom/truecaller/truepay/app/ui/transaction/c/d;
    //   366: invokestatic 58	com/truecaller/truepay/app/ui/transaction/c/d:c	(Lcom/truecaller/truepay/app/ui/transaction/c/d;)Lcom/truecaller/truepay/app/ui/transaction/c/c$b;
    //   369: astore_2
    //   370: aload_2
    //   371: ifnull +10 -> 381
    //   374: aload_2
    //   375: iconst_0
    //   376: invokeinterface 63 2 0
    //   381: aload_1
    //   382: athrow
    //   383: aload_1
    //   384: checkcast 50	c/o$b
    //   387: getfield 53	c/o$b:a	Ljava/lang/Throwable;
    //   390: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	391	0	this	c
    //   0	391	1	paramObject	Object
    //   3	372	2	localObject1	Object
    //   8	5	3	i	int
    //   82	2	3	bool1	boolean
    //   305	9	3	j	int
    //   10	270	4	k	int
    //   52	133	5	bool2	boolean
    //   124	204	6	localObject2	Object
    //   129	19	7	localObject3	Object
    //   310	5	8	arrayOfObject	Object[]
    //   263	1	11	localCancellationException	java.util.concurrent.CancellationException
    // Exception table:
    //   from	to	target	type
    //   62	66	74	finally
    //   67	71	74	finally
    //   72	74	74	finally
    //   87	91	74	finally
    //   92	96	74	finally
    //   102	109	74	finally
    //   109	113	74	finally
    //   115	120	74	finally
    //   120	124	74	finally
    //   126	129	74	finally
    //   134	138	74	finally
    //   138	143	74	finally
    //   149	153	74	finally
    //   161	165	74	finally
    //   168	172	74	finally
    //   173	179	74	finally
    //   189	193	74	finally
    //   194	198	74	finally
    //   204	210	74	finally
    //   211	217	74	finally
    //   220	224	74	finally
    //   225	229	74	finally
    //   235	242	74	finally
    //   242	248	74	finally
    //   264	268	74	finally
    //   269	273	74	finally
    //   279	286	74	finally
    //   286	292	74	finally
    //   292	296	74	finally
    //   297	301	74	finally
    //   302	305	74	finally
    //   306	310	74	finally
    //   314	321	74	finally
    //   327	332	74	finally
    //   333	339	74	finally
    //   62	66	263	java/util/concurrent/CancellationException
    //   67	71	263	java/util/concurrent/CancellationException
    //   72	74	263	java/util/concurrent/CancellationException
    //   87	91	263	java/util/concurrent/CancellationException
    //   92	96	263	java/util/concurrent/CancellationException
    //   102	109	263	java/util/concurrent/CancellationException
    //   109	113	263	java/util/concurrent/CancellationException
    //   115	120	263	java/util/concurrent/CancellationException
    //   120	124	263	java/util/concurrent/CancellationException
    //   126	129	263	java/util/concurrent/CancellationException
    //   134	138	263	java/util/concurrent/CancellationException
    //   138	143	263	java/util/concurrent/CancellationException
    //   149	153	263	java/util/concurrent/CancellationException
    //   161	165	263	java/util/concurrent/CancellationException
    //   168	172	263	java/util/concurrent/CancellationException
    //   173	179	263	java/util/concurrent/CancellationException
    //   189	193	263	java/util/concurrent/CancellationException
    //   194	198	263	java/util/concurrent/CancellationException
    //   204	210	263	java/util/concurrent/CancellationException
    //   211	217	263	java/util/concurrent/CancellationException
    //   220	224	263	java/util/concurrent/CancellationException
    //   225	229	263	java/util/concurrent/CancellationException
    //   235	242	263	java/util/concurrent/CancellationException
    //   242	248	263	java/util/concurrent/CancellationException
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */