package com.truecaller.truepay.app.ui.transaction.c;

import com.truecaller.truepay.data.d.b;
import java.util.List;

public abstract interface c$b
{
  public abstract void a(int paramInt1, int paramInt2);
  
  public abstract void a(b paramb);
  
  public abstract void a(String paramString);
  
  public abstract void a(List paramList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void d();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */