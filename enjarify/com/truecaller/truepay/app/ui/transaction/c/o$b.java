package com.truecaller.truepay.app.ui.transaction.c;

import c.d.f;
import c.g.a.m;
import c.x;
import com.truecaller.truepay.data.api.model.h;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

public final class o$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  public o$b(o paramo, com.truecaller.truepay.app.ui.accounts.b.c paramc, l.b paramb, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/truepay/app/ui/transaction/c/o$b;
    o localo = b;
    com.truecaller.truepay.app.ui.accounts.b.c localc = c;
    l.b localb1 = d;
    localb.<init>(localo, localc, localb1, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    int j = 1;
    boolean bool2;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof c.o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof c.o.b;
      if (bool1) {
        break label393;
      }
      paramObject = b;
      localObject2 = c;
      a = j;
      f localf = c;
      Object localObject3 = new com/truecaller/truepay/app/ui/transaction/c/o$a;
      ((o.a)localObject3).<init>((o)paramObject, (com.truecaller.truepay.app.ui.accounts.b.c)localObject2, null);
      localObject3 = (m)localObject3;
      paramObject = g.a(localf, (m)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (h)paramObject;
    if (paramObject != null)
    {
      localObject1 = ((h)paramObject).a();
      localObject2 = "success";
      bool2 = c.g.b.k.a(localObject1, localObject2);
      if (bool2)
      {
        localObject1 = ((h)paramObject).c();
        if (localObject1 != null)
        {
          localObject1 = ((h)paramObject).c();
          c.g.b.k.a(localObject1, "responseDO.data");
          localObject1 = ((com.truecaller.truepay.app.ui.registration.c.a)localObject1).a();
          localObject2 = "responseDO.data.account";
          c.g.b.k.a(localObject1, (String)localObject2);
          localObject1 = (CharSequence)((com.truecaller.truepay.data.api.model.a)localObject1).o();
          String str;
          if (localObject1 != null)
          {
            int k = ((CharSequence)localObject1).length();
            if (k != 0)
            {
              j = 0;
              str = null;
            }
          }
          if (j == 0)
          {
            localObject1 = o.a(b);
            localObject2 = ((h)paramObject).c();
            str = "responseDO.data";
            c.g.b.k.a(localObject2, str);
            localObject2 = ((com.truecaller.truepay.app.ui.registration.c.a)localObject2).a();
            ((com.truecaller.truepay.app.utils.a)localObject1).c((com.truecaller.truepay.data.api.model.a)localObject2);
            localObject1 = d;
            paramObject = ((h)paramObject).c();
            c.g.b.k.a(paramObject, "responseDO.data");
            paramObject = ((com.truecaller.truepay.app.ui.registration.c.a)paramObject).a();
            localObject2 = "responseDO.data.account";
            c.g.b.k.a(paramObject, (String)localObject2);
            ((l.b)localObject1).a((com.truecaller.truepay.data.api.model.a)paramObject);
            break label389;
          }
        }
        localObject1 = d;
        paramObject = ((h)paramObject).b();
        localObject2 = "responseDO.message";
        c.g.b.k.a(paramObject, (String)localObject2);
        ((l.b)localObject1).b((String)paramObject);
      }
      else
      {
        localObject1 = d;
        paramObject = ((h)paramObject).b();
        localObject2 = "responseDO.message";
        c.g.b.k.a(paramObject, (String)localObject2);
        ((l.b)localObject1).b((String)paramObject);
      }
    }
    label389:
    return x.a;
    label393:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.o.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */