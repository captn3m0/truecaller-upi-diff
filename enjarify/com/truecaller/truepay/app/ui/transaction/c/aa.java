package com.truecaller.truepay.app.ui.transaction.c;

import com.truecaller.truepay.a.a.f.p;
import com.truecaller.truepay.a.a.f.t;
import com.truecaller.truepay.a.a.f.v;
import com.truecaller.truepay.app.ui.transaction.views.c.k;
import com.truecaller.truepay.data.api.model.ResolveRequestVpa;
import com.truecaller.truepay.data.api.model.ResolveVpaRequestDO;
import io.reactivex.d;
import io.reactivex.d.e.b.c;
import io.reactivex.h;
import io.reactivex.i;
import io.reactivex.j;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.q;
import io.reactivex.s;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class aa
  extends com.truecaller.truepay.app.ui.base.a.a
  implements z
{
  private v a;
  private t b;
  private p c;
  private final List f;
  
  public aa(v paramv, t paramt, p paramp)
  {
    String[] tmp9_6 = new String[38];
    String[] tmp10_9 = tmp9_6;
    String[] tmp10_9 = tmp9_6;
    tmp10_9[0] = "andb";
    tmp10_9[1] = "axisbank";
    String[] tmp19_10 = tmp10_9;
    String[] tmp19_10 = tmp10_9;
    tmp19_10[2] = "barodampay";
    tmp19_10[3] = "centralbank";
    String[] tmp28_19 = tmp19_10;
    String[] tmp28_19 = tmp19_10;
    tmp28_19[4] = "cnrb";
    tmp28_19[5] = "csbpay";
    String[] tmp37_28 = tmp28_19;
    String[] tmp37_28 = tmp28_19;
    tmp37_28[6] = "dcb";
    tmp37_28[7] = "denabank";
    String[] tmp48_37 = tmp37_28;
    String[] tmp48_37 = tmp37_28;
    tmp48_37[8] = "federal";
    tmp48_37[9] = "hdfcbank";
    String[] tmp59_48 = tmp48_37;
    String[] tmp59_48 = tmp48_37;
    tmp59_48[10] = "hsbc";
    tmp59_48[11] = "icici";
    String[] tmp70_59 = tmp59_48;
    String[] tmp70_59 = tmp59_48;
    tmp70_59[12] = "idfcbank";
    tmp70_59[13] = "idbi";
    String[] tmp81_70 = tmp70_59;
    String[] tmp81_70 = tmp70_59;
    tmp81_70[14] = "indianbank";
    tmp81_70[15] = "indus";
    String[] tmp92_81 = tmp81_70;
    String[] tmp92_81 = tmp81_70;
    tmp92_81[16] = "kaypay";
    tmp92_81[17] = "kbl";
    String[] tmp103_92 = tmp92_81;
    String[] tmp103_92 = tmp92_81;
    tmp103_92[18] = "kvb";
    tmp103_92[19] = "mahab";
    String[] tmp114_103 = tmp103_92;
    String[] tmp114_103 = tmp103_92;
    tmp114_103[20] = "obc";
    tmp114_103[21] = "pnb";
    String[] tmp125_114 = tmp114_103;
    String[] tmp125_114 = tmp114_103;
    tmp125_114[22] = "pockets";
    tmp125_114[23] = "rbl";
    String[] tmp136_125 = tmp125_114;
    String[] tmp136_125 = tmp125_114;
    tmp136_125[24] = "sbi";
    tmp136_125[25] = "sib";
    String[] tmp147_136 = tmp136_125;
    String[] tmp147_136 = tmp136_125;
    tmp147_136[26] = "syndicate";
    tmp147_136[27] = "tjsb";
    String[] tmp158_147 = tmp147_136;
    String[] tmp158_147 = tmp147_136;
    tmp158_147[28] = "ubi";
    tmp158_147[29] = "uboi";
    String[] tmp169_158 = tmp158_147;
    String[] tmp169_158 = tmp158_147;
    tmp169_158[30] = "uco";
    tmp169_158[31] = "utbi";
    String[] tmp180_169 = tmp169_158;
    String[] tmp180_169 = tmp169_158;
    tmp180_169[32] = "united";
    tmp180_169[33] = "unionbank";
    String[] tmp191_180 = tmp180_169;
    String[] tmp191_180 = tmp180_169;
    tmp191_180[34] = "upi";
    tmp191_180[35] = "vijb";
    tmp191_180[36] = "ybl";
    String[] tmp207_191 = tmp191_180;
    tmp207_191[37] = "yesbank";
    Object localObject = Arrays.asList(tmp207_191);
    f = ((List)localObject);
    localObject = paramv;
    a = paramv;
    localObject = paramt;
    b = paramt;
    localObject = paramp;
    c = paramp;
  }
  
  private static boolean a(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    String str1 = "vpa";
    String str2 = g;
    boolean bool1 = str1.equalsIgnoreCase(str2);
    if (bool1)
    {
      str1 = h;
      if (str1 != null)
      {
        paramb = h;
        str1 = "*";
        boolean bool2 = paramb.equals(str1);
        if (!bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  private static ResolveVpaRequestDO c(String paramString)
  {
    Object localObject = "";
    String str1 = "";
    String str2 = "@";
    boolean bool = paramString.contains(str2);
    if (!bool)
    {
      str1 = paramString;
      paramString = (String)localObject;
    }
    localObject = new com/truecaller/truepay/data/api/model/ResolveRequestVpa;
    ((ResolveRequestVpa)localObject).<init>(paramString, str1);
    paramString = new com/truecaller/truepay/data/api/model/ResolveVpaRequestDO;
    paramString.<init>((ResolveRequestVpa)localObject, null);
    return paramString;
  }
  
  public final void a(String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = o.a(paramString);
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$aa$t3SeZ5jfA5_55HQUtvP5yHNKVT4;
    ((-..Lambda.aa.t3SeZ5jfA5_55HQUtvP5yHNKVT4)localObject2).<init>(this);
    io.reactivex.d.b.b.a(localObject2, "predicate is null");
    Object localObject3 = new io/reactivex/d/e/b/b;
    ((io.reactivex.d.e.b.b)localObject3).<init>((s)localObject1, (io.reactivex.c.g)localObject2);
    localObject1 = io.reactivex.e.a.a((h)localObject3);
    localObject2 = -..Lambda.aa.NyDJ4tKQxuVxYtBnt5HFQhg9tNM.INSTANCE;
    io.reactivex.d.b.b.a(localObject2, "mapper is null");
    localObject3 = new io/reactivex/d/e/b/e;
    ((io.reactivex.d.e.b.e)localObject3).<init>((j)localObject1, (io.reactivex.c.e)localObject2);
    localObject1 = io.reactivex.e.a.a((h)localObject3);
    localObject2 = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$aa$Aw1sltNQHMabeqDLlH4p0greFvs;
    ((-..Lambda.aa.Aw1sltNQHMabeqDLlH4p0greFvs)localObject2).<init>(this);
    io.reactivex.d.b.b.a(localObject2, "mapper is null");
    localObject3 = new io/reactivex/d/e/b/c;
    ((c)localObject3).<init>((j)localObject1, (io.reactivex.c.e)localObject2);
    localObject1 = io.reactivex.e.a.a((h)localObject3);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((h)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/transaction/c/aa$1;
    ((aa.1)localObject2).<init>(this, paramString, localArrayList);
    ((h)localObject1).a((i)localObject2);
    localObject1 = b.a.b(paramString).c();
    paramString = a.a(paramString);
    localObject2 = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$aa$4OpnnM-BBOP-7O5-H00YEEbfiE8;
    ((-..Lambda.aa.4OpnnM-BBOP-7O5-H00YEEbfiE8)localObject2).<init>(localArrayList);
    paramString = o.a((s)localObject1, paramString, (io.reactivex.c.b)localObject2);
    localObject1 = io.reactivex.g.a.b();
    paramString = paramString.b((n)localObject1);
    localObject1 = io.reactivex.android.b.a.a();
    paramString = paramString.a((n)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/c/aa$2;
    ((aa.2)localObject1).<init>(this, localArrayList);
    paramString.a((q)localObject1);
  }
  
  public final boolean a(com.truecaller.truepay.app.ui.transaction.b.b paramb, String paramString)
  {
    boolean bool1 = false;
    if (paramb == null) {
      return false;
    }
    String str1 = "search_type_request";
    boolean bool2 = str1.equalsIgnoreCase(paramString);
    boolean bool3;
    if (bool2)
    {
      bool3 = a(paramb);
      if (bool3) {
        return true;
      }
    }
    else
    {
      str1 = "search_type_send";
      boolean bool4 = str1.equalsIgnoreCase(paramString);
      if (bool4)
      {
        paramString = g;
        int i = -1;
        int m = paramString.hashCode();
        int n = -1233881810;
        String str2;
        boolean bool5;
        if (m != n)
        {
          n = -1177318867;
          if (m != n)
          {
            n = 116967;
            if (m == n)
            {
              str2 = "vpa";
              bool4 = paramString.equals(str2);
              if (bool4)
              {
                int j = 2;
                break label192;
              }
            }
          }
          else
          {
            str2 = "account";
            bool5 = paramString.equals(str2);
            if (bool5)
            {
              bool5 = true;
              break label192;
            }
          }
        }
        else
        {
          str2 = "aadhaar";
          bool5 = paramString.equals(str2);
          if (bool5)
          {
            bool5 = false;
            paramString = null;
            break label192;
          }
        }
        int k = -1;
        switch (k)
        {
        default: 
          break;
        case 2: 
          bool3 = a(paramb);
          if (!bool3)
          {
            paramb = (k)d;
            paramb.b();
          }
          break;
        case 1: 
          paramb = b;
          if (paramb != null) {
            break;
          }
          break;
        case 0: 
          label192:
          paramb = a;
          if (paramb == null) {
            return bool1;
          }
          break;
        }
        return true;
      }
    }
    paramb = (k)d;
    paramb.b();
    return bool1;
  }
  
  final boolean b(String paramString)
  {
    Object localObject = "@";
    paramString = paramString.split((String)localObject);
    int i = paramString.length;
    int j = 2;
    if (i == j)
    {
      localObject = f;
      j = 1;
      paramString = paramString[j];
      boolean bool = ((List)localObject).contains(paramString);
      if (bool) {
        return j;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */