package com.truecaller.truepay.app.ui.transaction.c;

import c.g.b.k;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.data.api.model.v;
import com.truecaller.utils.i;
import io.reactivex.n;
import io.reactivex.o;

public final class s
  extends com.truecaller.truepay.app.ui.base.a.a
  implements r
{
  com.truecaller.truepay.data.e.b a;
  boolean b;
  private com.truecaller.truepay.data.e.e c;
  private com.truecaller.truepay.data.api.a f;
  private com.truecaller.truepay.data.f.q g;
  private com.truecaller.truepay.a.a.b h;
  private com.truecaller.truepay.a.a.d i;
  private i j;
  
  public s(com.truecaller.truepay.data.e.b paramb, com.truecaller.truepay.data.e.e parame, com.truecaller.truepay.data.api.a parama, com.truecaller.truepay.data.f.q paramq, com.truecaller.truepay.a.a.b paramb1, com.truecaller.truepay.a.a.d paramd, i parami)
  {
    a = paramb;
    f = parama;
    g = paramq;
    h = paramb1;
    i = paramd;
    j = parami;
    c = parame;
  }
  
  public final void a()
  {
    o localo = g.b();
    Object localObject = io.reactivex.g.a.b();
    localo = localo.b((n)localObject);
    localObject = io.reactivex.android.b.a.a();
    localo = localo.a((n)localObject);
    localObject = new com/truecaller/truepay/app/ui/transaction/c/s$1;
    ((s.1)localObject).<init>(this);
    localo.a((io.reactivex.q)localObject);
  }
  
  public final void a(int paramInt)
  {
    com.truecaller.truepay.data.b.a locala = Truepay.getInstance().getAnalyticLoggerHelper();
    Integer localInteger = Integer.valueOf(paramInt);
    locala.a("TCPayContacts", localInteger);
  }
  
  public final void a(String paramString)
  {
    Object localObject = j;
    boolean bool = ((i)localObject).a();
    if (bool)
    {
      localObject = new com/truecaller/truepay/data/api/model/v;
      ((v)localObject).<init>(paramString);
      paramString = i;
      String str = "requestDO";
      k.b(localObject, str);
      paramString = a.a((v)localObject);
      k.a(paramString, "contactsSyncApiService.i…teContactToPay(requestDO)");
      localObject = io.reactivex.g.a.b();
      paramString = paramString.b((n)localObject);
      localObject = io.reactivex.android.b.a.a();
      paramString = paramString.a((n)localObject);
      paramString.am_();
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject1 = null;
    boolean bool = true;
    if (!paramBoolean)
    {
      long l1 = System.currentTimeMillis();
      localObject2 = a.a();
      long l2 = ((Long)localObject2).longValue();
      l1 -= l2;
      l2 = 86400000L;
      paramBoolean = l1 < l2;
      if (paramBoolean)
      {
        paramBoolean = true;
      }
      else
      {
        paramBoolean = false;
        localObject2 = null;
      }
      if (paramBoolean)
      {
        new String[1][0] = "Last contact sync is recent, skipping sync";
        return;
      }
    }
    Object localObject2 = j;
    paramBoolean = ((i)localObject2).a();
    if (!paramBoolean)
    {
      localObject2 = (com.truecaller.truepay.app.ui.transaction.views.c.e)d;
      localObject3 = new com/truecaller/truepay/app/b/e;
      ((com.truecaller.truepay.app.b.e)localObject3).<init>();
      ((com.truecaller.truepay.app.ui.transaction.views.c.e)localObject2).a((Throwable)localObject3);
      ((com.truecaller.truepay.app.ui.transaction.views.c.e)d).a(false);
      return;
    }
    paramBoolean = b;
    if (paramBoolean) {
      return;
    }
    b = bool;
    ((com.truecaller.truepay.app.ui.transaction.views.c.e)d).a(bool);
    localObject2 = g.d();
    localObject1 = io.reactivex.g.a.b();
    localObject2 = ((o)localObject2).b((n)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$s$pT5-UE9wtmRF21gvVTx4LXpZHfs;
    ((-..Lambda.s.pT5-UE9wtmRF21gvVTx4LXpZHfs)localObject1).<init>(this);
    localObject2 = ((o)localObject2).a((io.reactivex.c.e)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$s$DDTN0rGl1AfLGhRGlHjyoo3LybA;
    ((-..Lambda.s.DDTN0rGl1AfLGhRGlHjyoo3LybA)localObject1).<init>(this);
    localObject2 = ((o)localObject2).a((io.reactivex.c.e)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$s$HEVHwXazs0pQot00GQ-jMFvDV24;
    ((-..Lambda.s.HEVHwXazs0pQot00GQ-jMFvDV24)localObject1).<init>(this);
    localObject2 = ((o)localObject2).a((io.reactivex.c.e)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$s$sXtnknavbbMjNsdQjRSZ2r9Oqz0;
    ((-..Lambda.s.sXtnknavbbMjNsdQjRSZ2r9Oqz0)localObject1).<init>(this);
    io.reactivex.d.b.b.a(localObject1, "mapper is null");
    Object localObject3 = new io/reactivex/d/e/d/d;
    ((io.reactivex.d.e.d.d)localObject3).<init>((io.reactivex.s)localObject2, (io.reactivex.c.e)localObject1);
    localObject2 = io.reactivex.e.a.a((o)localObject3);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$s$nHIWpcuDvfLQVkV_7aTcQiN3wqE;
    ((-..Lambda.s.nHIWpcuDvfLQVkV_7aTcQiN3wqE)localObject1).<init>(this);
    localObject2 = ((o)localObject2).a((io.reactivex.c.e)localObject1);
    localObject1 = io.reactivex.android.b.a.a();
    localObject2 = ((o)localObject2).a((n)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/c/s$3;
    ((s.3)localObject1).<init>(this);
    ((o)localObject2).a((io.reactivex.q)localObject1);
  }
  
  public final void c()
  {
    o localo = g.c();
    Object localObject = io.reactivex.g.a.b();
    localo = localo.b((n)localObject);
    localObject = io.reactivex.android.b.a.a();
    localo = localo.a((n)localObject);
    localObject = new com/truecaller/truepay/app/ui/transaction/c/s$2;
    ((s.2)localObject).<init>(this);
    localo.a((io.reactivex.q)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */