package com.truecaller.truepay.app.ui.transaction.c;

import android.text.TextUtils;
import com.c.b.a.b.c;
import com.truecaller.truepay.a.a.f.d;
import com.truecaller.truepay.a.a.f.l;
import com.truecaller.truepay.a.a.f.m;
import com.truecaller.truepay.app.ui.npci.e;
import com.truecaller.truepay.data.api.model.ResolveRequestUser;
import com.truecaller.truepay.data.api.model.ResolveRequestVpa;
import com.truecaller.truepay.data.api.model.ResolveVpaRequestDO;
import io.reactivex.o;
import io.reactivex.q;
import okhttp3.ae;

public final class t
  extends com.truecaller.truepay.app.ui.base.a.a
{
  public com.truecaller.truepay.a.a.f.n a;
  public d b;
  public m c;
  public com.truecaller.truepay.a.a.f.r f;
  public l g;
  public com.truecaller.truepay.a.a.f.p h;
  public ResolveRequestUser i;
  public e j;
  public com.truecaller.truepay.app.utils.a k;
  
  static String a(com.truecaller.truepay.app.ui.transaction.b.p paramp)
  {
    Object localObject = "pay_direct";
    String str = d;
    boolean bool = ((String)localObject).equalsIgnoreCase(str);
    if (bool)
    {
      localObject = h.c;
      bool = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool)
      {
        paramp = h.c;
      }
      else
      {
        localObject = h.e;
        bool = TextUtils.isEmpty((CharSequence)localObject);
        if (!bool)
        {
          paramp = h.e;
        }
        else
        {
          localObject = h.a;
          bool = TextUtils.isEmpty((CharSequence)localObject);
          if (!bool)
          {
            localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>();
            str = h.a;
            ((StringBuilder)localObject).append(str);
            str = "@";
            ((StringBuilder)localObject).append(str);
            paramp = h.b;
            ((StringBuilder)localObject).append(paramp);
            paramp = ((StringBuilder)localObject).toString();
          }
          else
          {
            paramp = h.h;
          }
        }
      }
    }
    else
    {
      paramp = h.f;
    }
    return paramp;
  }
  
  static String a(Throwable paramThrowable)
  {
    String str = paramThrowable.getMessage();
    try
    {
      paramThrowable = (c)paramThrowable;
      paramThrowable = a;
      paramThrowable = c;
      str = paramThrowable.g();
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return str;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    Object localObject1 = new com/truecaller/truepay/data/api/model/ResolveRequestVpa;
    ((ResolveRequestVpa)localObject1).<init>(paramString2, paramString1);
    Object localObject2 = new com/truecaller/truepay/data/api/model/ResolveVpaRequestDO;
    ResolveRequestUser localResolveRequestUser = i;
    ((ResolveVpaRequestDO)localObject2).<init>((ResolveRequestVpa)localObject1, localResolveRequestUser);
    localObject1 = h.a((ResolveVpaRequestDO)localObject2);
    localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((io.reactivex.n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((o)localObject1).a((io.reactivex.n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/transaction/c/t$7;
    ((t.7)localObject2).<init>(this, paramString1, paramString2);
    ((o)localObject1).a((q)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */