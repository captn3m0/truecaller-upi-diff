package com.truecaller.truepay.app.ui.transaction.c;

import com.truecaller.featuretoggles.b;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.views.c.h;
import io.reactivex.n;
import io.reactivex.o;

public final class p
  extends com.truecaller.truepay.app.ui.base.a.a
{
  public com.truecaller.truepay.a.a.c.a a;
  public com.truecaller.featuretoggles.e b;
  private com.truecaller.truepay.data.e.a c;
  
  public p(com.truecaller.truepay.data.e.a parama)
  {
    c = parama;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.p paramp)
  {
    Object localObject = j;
    if (localObject != null)
    {
      localObject = Truepay.getInstance().getAnalyticLoggerHelper();
      String str1 = j.b;
      String str2 = d;
      com.truecaller.truepay.data.e.a locala = c;
      paramp = j.e;
      ((com.truecaller.truepay.data.b.a)localObject).a(str1, str2, locala, paramp);
    }
  }
  
  public final void a(h paramh)
  {
    super.a(paramh);
    paramh.b();
    b localb = b.ab();
    boolean bool = localb.a();
    if (bool)
    {
      paramh.d();
      return;
    }
    paramh.c();
  }
  
  public final void a(String paramString)
  {
    Object localObject = new com/truecaller/truepay/app/ui/history/models/d;
    ((com.truecaller.truepay.app.ui.history.models.d)localObject).<init>(paramString);
    paramString = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$p$m5o2PZFovALrcspkjVlWNiGGfNM;
    paramString.<init>(this, (com.truecaller.truepay.app.ui.history.models.d)localObject);
    paramString = o.a(paramString);
    localObject = new com/truecaller/truepay/app/ui/transaction/c/p$a;
    ((p.a)localObject).<init>();
    paramString = paramString.c((io.reactivex.c.e)localObject);
    localObject = io.reactivex.g.a.b();
    paramString = paramString.b((n)localObject);
    localObject = io.reactivex.android.b.a.a();
    paramString = paramString.a((n)localObject);
    localObject = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$p$GZWgFOZiEbi6gc5SMxD86-erhi0;
    ((-..Lambda.p.GZWgFOZiEbi6gc5SMxD86-erhi0)localObject).<init>(this);
    -..Lambda.p.lwSfukTmyC0TslKwxydSCkNZxQw locallwSfukTmyC0TslKwxydSCkNZxQw = new com/truecaller/truepay/app/ui/transaction/c/-$$Lambda$p$lwSfukTmyC0TslKwxydSCkNZxQw;
    locallwSfukTmyC0TslKwxydSCkNZxQw.<init>(this);
    -..Lambda.p.ZeTErJFSI2baZbxsfE5pKKU3etc localZeTErJFSI2baZbxsfE5pKKU3etc = -..Lambda.p.ZeTErJFSI2baZbxsfE5pKKU3etc.INSTANCE;
    paramString = paramString.a((io.reactivex.c.d)localObject, locallwSfukTmyC0TslKwxydSCkNZxQw, localZeTErJFSI2baZbxsfE5pKKU3etc);
    e.a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */