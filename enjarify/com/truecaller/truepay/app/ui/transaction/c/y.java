package com.truecaller.truepay.app.ui.transaction.c;

import com.truecaller.truepay.a.a.f.b;
import com.truecaller.truepay.a.a.f.h;
import com.truecaller.truepay.a.a.f.r;
import com.truecaller.truepay.data.api.model.ah;
import com.truecaller.truepay.data.d.c;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.q;

public final class y
  extends com.truecaller.truepay.app.ui.base.a.a
  implements x
{
  private final h a;
  private final r b;
  private final b c;
  
  public y(h paramh, r paramr, b paramb)
  {
    a = paramh;
    b = paramr;
    c = paramb;
  }
  
  private static ah a(c paramc, String paramString)
  {
    ah localah = new com/truecaller/truepay/data/api/model/ah;
    localah.<init>();
    String str = e;
    f = str;
    str = b;
    c = str;
    str = a;
    d = str;
    str = h;
    e = str;
    str = c;
    j = str;
    str = "reject_request";
    boolean bool = str.equalsIgnoreCase(paramString);
    if (bool)
    {
      paramc = "R";
      i = paramc;
      a = paramString;
    }
    else
    {
      paramc = i;
      k = paramc;
    }
    return localah;
  }
  
  public final void a()
  {
    o localo = a.a.a();
    Object localObject = io.reactivex.g.a.b();
    localo = localo.b((n)localObject);
    localObject = io.reactivex.android.b.a.a();
    localo = localo.a((n)localObject);
    localObject = new com/truecaller/truepay/app/ui/transaction/c/y$1;
    ((y.1)localObject).<init>(this);
    localo.a((q)localObject);
  }
  
  public final void a(c paramc)
  {
    paramc = a(paramc, "reject_request");
    paramc = b.a(paramc);
    Object localObject = io.reactivex.g.a.b();
    paramc = paramc.b((n)localObject);
    localObject = io.reactivex.android.b.a.a();
    paramc = paramc.a((n)localObject);
    localObject = new com/truecaller/truepay/app/ui/transaction/c/y$2;
    ((y.2)localObject).<init>(this);
    paramc.a((q)localObject);
  }
  
  public final void b(c paramc)
  {
    Object localObject1 = a(paramc, "block_vpa");
    localObject1 = c.a.c((ah)localObject1);
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((o)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/transaction/c/y$3;
    ((y.3)localObject2).<init>(this, paramc);
    ((o)localObject1).a((q)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.c.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */