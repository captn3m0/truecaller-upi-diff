package com.truecaller.truepay.app.ui.transaction.b;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.h;

public final class i
  extends c
{
  public i(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    h localh = new com/truecaller/truepay/app/ui/transaction/views/b/h;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_inactive_contacts_list;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localh.<init>(paramViewGroup, (f)localObject);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */