package com.truecaller.truepay.app.ui.transaction.b;

import com.truecaller.truepay.data.api.model.a;
import java.io.Serializable;

public final class p
  implements Serializable
{
  public String A;
  public String B;
  public boolean C;
  public String D;
  public String E;
  public String F;
  private String G;
  private String H;
  private String I;
  private String J;
  private String K;
  private boolean L;
  private String M;
  public String a;
  public String b;
  public String c;
  public String d;
  public String e;
  public String f;
  public String g;
  public n h;
  public String i;
  public l j;
  public a k;
  public String l;
  public boolean m;
  public String n;
  public String o;
  public String p;
  public String q;
  public String r;
  public String s;
  public String t;
  public String u;
  public String v;
  public String w;
  public boolean x;
  public String y;
  public String z;
  
  public final boolean A()
  {
    return C;
  }
  
  public final String B()
  {
    return B;
  }
  
  public final String C()
  {
    return M;
  }
  
  public final String D()
  {
    return D;
  }
  
  public final String E()
  {
    return E;
  }
  
  public final String F()
  {
    return F;
  }
  
  public final l a()
  {
    return j;
  }
  
  public final void a(l paraml)
  {
    j = paraml;
  }
  
  public final void a(n paramn)
  {
    h = paramn;
  }
  
  public final void a(a parama)
  {
    k = parama;
  }
  
  public final void a(String paramString)
  {
    i = paramString;
  }
  
  public final void a(boolean paramBoolean)
  {
    C = paramBoolean;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final void b(String paramString)
  {
    b = paramString;
  }
  
  public final String c()
  {
    return d;
  }
  
  public final void c(String paramString)
  {
    c = paramString;
  }
  
  public final String d()
  {
    return e;
  }
  
  public final void d(String paramString)
  {
    d = paramString;
  }
  
  public final n e()
  {
    return h;
  }
  
  public final void e(String paramString)
  {
    e = paramString;
  }
  
  public final String f()
  {
    return a;
  }
  
  public final void f(String paramString)
  {
    a = paramString;
  }
  
  public final a g()
  {
    return k;
  }
  
  public final void g(String paramString)
  {
    l = paramString;
  }
  
  public final String h()
  {
    return l;
  }
  
  public final void h(String paramString)
  {
    n = paramString;
  }
  
  public final void i()
  {
    m = true;
  }
  
  public final void i(String paramString)
  {
    G = paramString;
  }
  
  public final String j()
  {
    return n;
  }
  
  public final void j(String paramString)
  {
    t = paramString;
  }
  
  public final String k()
  {
    return G;
  }
  
  public final void k(String paramString)
  {
    u = paramString;
  }
  
  public final String l()
  {
    return t;
  }
  
  public final void l(String paramString)
  {
    v = paramString;
  }
  
  public final String m()
  {
    return u;
  }
  
  public final void m(String paramString)
  {
    w = paramString;
  }
  
  public final String n()
  {
    return v;
  }
  
  public final void n(String paramString)
  {
    I = paramString;
  }
  
  public final String o()
  {
    return w;
  }
  
  public final void o(String paramString)
  {
    J = paramString;
  }
  
  public final void p(String paramString)
  {
    K = paramString;
  }
  
  public final boolean p()
  {
    return L;
  }
  
  public final void q()
  {
    L = true;
  }
  
  public final void q(String paramString)
  {
    H = paramString;
  }
  
  public final String r()
  {
    return I;
  }
  
  public final void r(String paramString)
  {
    y = paramString;
  }
  
  public final String s()
  {
    return J;
  }
  
  public final void s(String paramString)
  {
    z = paramString;
  }
  
  public final String t()
  {
    return K;
  }
  
  public final void t(String paramString)
  {
    M = paramString;
  }
  
  public final boolean u()
  {
    return x;
  }
  
  public final void v()
  {
    x = true;
  }
  
  public final String w()
  {
    return H;
  }
  
  public final String x()
  {
    return y;
  }
  
  public final String y()
  {
    return z;
  }
  
  public final String z()
  {
    return A;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */