package com.truecaller.truepay.app.ui.transaction.services;

import android.content.Context;
import android.os.Bundle;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e.a;
import com.truecaller.truepay.Truepay;
import io.reactivex.d;
import java.util.concurrent.TimeUnit;

public class TransactionsSyncTask
  extends PersistentBackgroundTask
{
  public com.truecaller.truepay.a.a.c.e a;
  public com.truecaller.truepay.data.e.e b;
  private io.reactivex.a.a c;
  
  public TransactionsSyncTask()
  {
    io.reactivex.a.a locala = new io/reactivex/a/a;
    locala.<init>();
    c = locala;
  }
  
  public final int a()
  {
    return 20001;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    Truepay.getApplicationComponent().a(this);
    paramContext = a.a();
    paramBundle = io.reactivex.g.a.b();
    paramContext = paramContext.b(paramBundle);
    paramBundle = -..Lambda.TransactionsSyncTask.AMamMfZTF44R7hEKPf7proOCcTo.INSTANCE;
    -..Lambda.TransactionsSyncTask.SMHagevQ29km8NUW6skErO9es30 localSMHagevQ29km8NUW6skErO9es30 = -..Lambda.TransactionsSyncTask.SMHagevQ29km8NUW6skErO9es30.INSTANCE;
    -..Lambda.TransactionsSyncTask.2zUdxojBnur_8Zi6eIi3cNvOVl0 local2zUdxojBnur_8Zi6eIi3cNvOVl0 = -..Lambda.TransactionsSyncTask.2zUdxojBnur_8Zi6eIi3cNvOVl0.INSTANCE;
    -..Lambda.TransactionsSyncTask.LtdaslV7eWUF3hLcMoIdJWrND-g localLtdaslV7eWUF3hLcMoIdJWrND-g = -..Lambda.TransactionsSyncTask.LtdaslV7eWUF3hLcMoIdJWrND-g.INSTANCE;
    paramContext = paramContext.a(paramBundle, localSMHagevQ29km8NUW6skErO9es30, local2zUdxojBnur_8Zi6eIi3cNvOVl0, localLtdaslV7eWUF3hLcMoIdJWrND-g);
    c.a(paramContext);
    return PersistentBackgroundTask.RunResult.Success;
  }
  
  public final boolean a(Context paramContext)
  {
    paramContext = Truepay.getApplicationComponent();
    if (paramContext != null)
    {
      Truepay.getApplicationComponent().a(this);
      paramContext = b;
      boolean bool = paramContext.b();
      if (bool)
      {
        paramContext = "";
        String str = b.a();
        bool = paramContext.equals(str);
        if (!bool)
        {
          paramContext = Truepay.getInstance();
          bool = paramContext.isRegistrationComplete();
          if (bool) {
            return true;
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final com.truecaller.common.background.e b()
  {
    e.a locala = new com/truecaller/common/background/e$a;
    int i = 1;
    locala.<init>(i);
    b = i;
    TimeUnit localTimeUnit = TimeUnit.DAYS;
    locala = locala.a(1L, localTimeUnit);
    localTimeUnit = TimeUnit.MINUTES;
    locala = locala.b(20, localTimeUnit);
    localTimeUnit = TimeUnit.MINUTES;
    return locala.c(10, localTimeUnit).b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.services.TransactionsSyncTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */