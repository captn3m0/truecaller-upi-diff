package com.truecaller.truepay.app.ui.transaction.a.a;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final a a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  
  private h(a parama, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
  }
  
  public static h a(a parama, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7)
  {
    h localh = new com/truecaller/truepay/app/ui/transaction/a/a/h;
    localh.<init>(parama, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.a.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */