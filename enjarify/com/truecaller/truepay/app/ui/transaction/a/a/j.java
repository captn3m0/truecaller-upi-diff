package com.truecaller.truepay.app.ui.transaction.a.a;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final a a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private j(a parama, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static j a(a parama, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    j localj = new com/truecaller/truepay/app/ui/transaction/a/a/j;
    localj.<init>(parama, paramProvider1, paramProvider2, paramProvider3);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.a.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */