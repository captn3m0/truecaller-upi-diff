package com.truecaller.truepay.app.ui.transaction.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.truecaller.truepay.a.a.f.s;
import com.truecaller.truepay.app.ui.transaction.c.c.a;
import com.truecaller.truepay.app.ui.transaction.c.g.a;
import com.truecaller.truepay.app.ui.transaction.c.q;
import com.truecaller.truepay.app.ui.transaction.c.t;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.truepay.app.utils.ao;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.data.a.ae;
import com.truecaller.truepay.data.a.af;
import com.truecaller.truepay.data.a.ag;
import com.truecaller.truepay.data.a.ah;
import com.truecaller.truepay.data.a.ai;
import com.truecaller.truepay.data.a.aj;
import com.truecaller.truepay.data.api.model.ResolveRequestUser;
import com.truecaller.truepay.data.f.ac;
import com.truecaller.truepay.data.f.y;
import com.truecaller.truepay.data.f.z;
import javax.inject.Provider;

public final class a
  implements b
{
  private Provider A;
  private Provider B;
  private Provider C;
  private Provider D;
  private Provider E;
  private Provider F;
  private Provider G;
  private Provider H;
  private Provider I;
  private Provider J;
  private Provider K;
  private Provider L;
  private Provider M;
  private Provider N;
  private Provider O;
  private final com.truecaller.truepay.app.a.a.a a;
  private final ae b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  private Provider w;
  private Provider x;
  private Provider y;
  private Provider z;
  
  private a(ae paramae, com.truecaller.truepay.app.ui.transaction.a.a.a parama, com.truecaller.truepay.app.a.a.a parama1)
  {
    a = parama1;
    b = paramae;
    Object localObject1 = new com/truecaller/truepay/app/ui/transaction/a/a$d;
    ((a.d)localObject1).<init>(parama1);
    c = ((Provider)localObject1);
    localObject1 = c;
    localObject1 = dagger.a.c.a(ah.a(paramae, (Provider)localObject1));
    d = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/a/a$l;
    ((a.l)localObject1).<init>(parama1);
    e = ((Provider)localObject1);
    localObject1 = e;
    localObject1 = dagger.a.c.a(ai.a(paramae, (Provider)localObject1));
    f = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/a/a$k;
    ((a.k)localObject1).<init>(parama1);
    g = ((Provider)localObject1);
    localObject1 = com.truecaller.truepay.data.e.d.a(g);
    h = ((Provider)localObject1);
    localObject1 = h;
    localObject1 = dagger.a.c.a(com.truecaller.truepay.app.ui.transaction.a.a.f.a(parama, (Provider)localObject1));
    i = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/a/a$n;
    ((a.n)localObject1).<init>(parama1);
    j = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/a/a$e;
    ((a.e)localObject1).<init>(parama1);
    k = ((Provider)localObject1);
    localObject1 = com.truecaller.truepay.a.a.c.a(k);
    l = ((Provider)localObject1);
    localObject1 = com.truecaller.truepay.a.a.e.a(k);
    m = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/a/a$i;
    ((a.i)localObject1).<init>(parama1);
    n = ((Provider)localObject1);
    Provider localProvider1 = i;
    Provider localProvider2 = j;
    Provider localProvider3 = k;
    Provider localProvider4 = c;
    Provider localProvider5 = l;
    Provider localProvider6 = m;
    Provider localProvider7 = n;
    Object localObject2 = parama;
    localObject1 = dagger.a.c.a(com.truecaller.truepay.app.ui.transaction.a.a.h.a(parama, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7));
    o = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/a/a$f;
    ((a.f)localObject1).<init>(parama1);
    p = ((Provider)localObject1);
    localObject1 = p;
    localObject1 = dagger.a.c.a(ag.a(paramae, (Provider)localObject1));
    q = ((Provider)localObject1);
    localObject1 = e;
    localObject2 = com.truecaller.truepay.data.c.d.a();
    paramae = dagger.a.c.a(aj.a(paramae, (Provider)localObject1, (Provider)localObject2));
    r = paramae;
    paramae = new com/truecaller/truepay/app/ui/transaction/a/a$h;
    paramae.<init>(parama1);
    s = paramae;
    paramae = q;
    localObject1 = r;
    localObject2 = s;
    paramae = com.truecaller.truepay.data.f.h.a(paramae, (Provider)localObject1, (Provider)localObject2);
    t = paramae;
    paramae = t;
    localObject1 = s;
    paramae = dagger.a.c.a(com.truecaller.truepay.app.ui.transaction.a.a.d.a(parama, paramae, (Provider)localObject1));
    u = paramae;
    paramae = t;
    paramae = dagger.a.c.a(com.truecaller.truepay.app.ui.transaction.a.a.b.a(parama, paramae));
    v = paramae;
    paramae = t;
    paramae = dagger.a.c.a(com.truecaller.truepay.app.ui.transaction.a.a.c.a(parama, paramae));
    w = paramae;
    paramae = new com/truecaller/truepay/app/ui/transaction/a/a$c;
    paramae.<init>(parama1);
    x = paramae;
    paramae = x;
    paramae = dagger.a.c.a(com.truecaller.truepay.app.ui.transaction.a.a.e.a(parama, paramae));
    y = paramae;
    paramae = new com/truecaller/truepay/app/ui/transaction/a/a$g;
    paramae.<init>(parama1);
    z = paramae;
    localObject2 = u;
    localProvider1 = v;
    localProvider2 = w;
    localProvider3 = y;
    localProvider4 = z;
    localProvider5 = h;
    localObject1 = parama;
    paramae = dagger.a.c.a(com.truecaller.truepay.app.ui.transaction.a.a.g.a(parama, (Provider)localObject2, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5));
    A = paramae;
    paramae = dagger.a.c.a(com.truecaller.truepay.app.ui.transaction.a.a.i.a(parama));
    B = paramae;
    paramae = d;
    localObject1 = f;
    paramae = y.a(paramae, (Provider)localObject1);
    C = paramae;
    paramae = com.truecaller.truepay.a.a.f.i.a(C);
    D = paramae;
    paramae = s.a(C);
    E = paramae;
    paramae = com.truecaller.truepay.a.a.f.c.a(C);
    F = paramae;
    paramae = D;
    localObject1 = E;
    localObject2 = F;
    paramae = dagger.a.c.a(com.truecaller.truepay.app.ui.transaction.a.a.j.a(parama, paramae, (Provider)localObject1, (Provider)localObject2));
    G = paramae;
    paramae = new com/truecaller/truepay/app/ui/transaction/a/a$b;
    paramae.<init>(parama1);
    H = paramae;
    paramae = new com/truecaller/truepay/app/ui/transaction/a/a$m;
    paramae.<init>(parama1);
    I = paramae;
    paramae = com.truecaller.truepay.app.utils.a.f.a(e);
    J = paramae;
    paramae = dagger.a.c.a(J);
    K = paramae;
    paramae = new com/truecaller/truepay/app/ui/transaction/a/a$j;
    paramae.<init>(parama1);
    L = paramae;
    paramae = H;
    parama = I;
    parama1 = K;
    localObject1 = L;
    paramae = com.truecaller.truepay.app.ui.transaction.c.e.a(paramae, parama, parama1, (Provider)localObject1);
    M = paramae;
    paramae = dagger.a.c.a(M);
    N = paramae;
    paramae = dagger.a.c.a(com.truecaller.truepay.app.ui.transaction.c.i.a());
    O = paramae;
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/truepay/app/ui/transaction/a/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  private com.truecaller.truepay.data.f.x b()
  {
    com.truecaller.truepay.data.f.x localx = new com/truecaller/truepay/data/f/x;
    z localz1 = (z)d.get();
    z localz2 = (z)f.get();
    localx.<init>(localz1, localz2);
    return localx;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.a parama)
  {
    com.truecaller.truepay.app.ui.transaction.c.f localf = new com/truecaller/truepay/app/ui/transaction/c/f;
    localf.<init>();
    b = localf;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.b paramb)
  {
    Object localObject1 = new com/truecaller/truepay/app/ui/transaction/c/b;
    com.truecaller.truepay.a.a.f.a locala = (com.truecaller.truepay.a.a.f.a)v.get();
    com.truecaller.truepay.a.a.f.x localx = new com/truecaller/truepay/a/a/f/x;
    Object localObject2 = (ac)dagger.a.g.a(a.al(), "Cannot return null from a non-@Nullable component method");
    localx.<init>((ac)localObject2);
    localObject2 = (ResolveRequestUser)dagger.a.g.a(a.aj(), "Cannot return null from a non-@Nullable component method");
    ((com.truecaller.truepay.app.ui.transaction.c.b)localObject1).<init>(locala, localx, (ResolveRequestUser)localObject2);
    q = ((com.truecaller.truepay.app.ui.transaction.c.b)localObject1);
    localObject1 = (Context)dagger.a.g.a(a.a(), "Cannot return null from a non-@Nullable component method");
    r = ((Context)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.d paramd)
  {
    c.a locala = (c.a)N.get();
    b = locala;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.e parame)
  {
    Object localObject = (com.truecaller.truepay.app.ui.transaction.c.v)B.get();
    e = ((com.truecaller.truepay.app.ui.transaction.c.v)localObject);
    localObject = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.N(), "Cannot return null from a non-@Nullable component method");
    f = ((com.truecaller.truepay.data.e.e)localObject);
    localObject = (com.truecaller.utils.n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    g = ((com.truecaller.utils.n)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.f paramf)
  {
    com.truecaller.truepay.app.ui.transaction.c.m localm = (com.truecaller.truepay.app.ui.transaction.c.m)A.get();
    c = localm;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.g paramg)
  {
    g.a locala = (g.a)O.get();
    a = locala;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.h paramh)
  {
    com.truecaller.truepay.app.ui.transaction.c.k localk = new com/truecaller/truepay/app/ui/transaction/c/k;
    localk.<init>();
    a = localk;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.i parami)
  {
    Object localObject = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    a = ((com.truecaller.truepay.app.utils.a)localObject);
    localObject = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.S(), "Cannot return null from a non-@Nullable component method");
    h = ((com.truecaller.truepay.data.e.a)localObject);
    localObject = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.T(), "Cannot return null from a non-@Nullable component method");
    i = ((com.truecaller.truepay.data.e.a)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.j paramj)
  {
    Object localObject1 = (com.truecaller.truepay.app.ui.transaction.c.m)A.get();
    k = ((com.truecaller.truepay.app.ui.transaction.c.m)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/c/o;
    Object localObject2 = dagger.a.g.a(a.Y(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject2;
    localObject3 = (c.d.f)localObject2;
    localObject2 = dagger.a.g.a(a.Z(), "Cannot return null from a non-@Nullable component method");
    Object localObject4 = localObject2;
    localObject4 = (c.d.f)localObject2;
    localObject2 = dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    Object localObject5 = localObject2;
    localObject5 = (com.truecaller.utils.n)localObject2;
    com.truecaller.truepay.data.e.c localc = new com/truecaller/truepay/data/e/c;
    localObject2 = (SharedPreferences)dagger.a.g.a(a.aa(), "Cannot return null from a non-@Nullable component method");
    localc.<init>((SharedPreferences)localObject2);
    localObject2 = dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject2;
    localObject6 = (com.truecaller.truepay.app.utils.a)localObject2;
    localObject2 = dagger.a.g.a(a.ac(), "Cannot return null from a non-@Nullable component method");
    Object localObject7 = localObject2;
    localObject7 = (com.truecaller.truepay.data.f.d)localObject2;
    localObject2 = dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    Object localObject8 = localObject2;
    localObject8 = (com.truecaller.featuretoggles.e)localObject2;
    localObject2 = localObject1;
    ((com.truecaller.truepay.app.ui.transaction.c.o)localObject1).<init>((c.d.f)localObject3, (c.d.f)localObject4, (com.truecaller.utils.n)localObject5, localc, (com.truecaller.truepay.app.utils.a)localObject6, (com.truecaller.truepay.data.f.d)localObject7, (com.truecaller.featuretoggles.e)localObject8);
    l = ((com.truecaller.truepay.app.ui.transaction.c.o)localObject1);
    localObject1 = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    n = ((com.truecaller.truepay.app.utils.a)localObject1);
    localObject1 = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    o = ((com.truecaller.truepay.app.utils.r)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.k paramk)
  {
    Object localObject1 = (au)dagger.a.g.a(a.k(), "Cannot return null from a non-@Nullable component method");
    v = ((au)localObject1);
    localObject1 = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    w = ((com.truecaller.truepay.app.utils.r)localObject1);
    localObject1 = (ax)dagger.a.g.a(a.e(), "Cannot return null from a non-@Nullable component method");
    x = ((ax)localObject1);
    localObject1 = q.a((com.truecaller.truepay.data.e.a)dagger.a.g.a(a.D(), "Cannot return null from a non-@Nullable component method"));
    Object localObject2 = new com/truecaller/truepay/a/a/c/a;
    com.truecaller.truepay.data.f.m localm = new com/truecaller/truepay/data/f/m;
    com.truecaller.truepay.data.f.n localn = af.a((com.truecaller.truepay.data.api.g)dagger.a.g.a(a.p(), "Cannot return null from a non-@Nullable component method"));
    localm.<init>(localn);
    ((com.truecaller.truepay.a.a.c.a)localObject2).<init>(localm);
    a = ((com.truecaller.truepay.a.a.c.a)localObject2);
    localObject2 = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.featuretoggles.e)localObject2);
    y = ((com.truecaller.truepay.app.ui.transaction.c.p)localObject1);
    localObject1 = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    z = ((com.truecaller.truepay.app.utils.a)localObject1);
    localObject1 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.Q(), "Cannot return null from a non-@Nullable component method");
    A = ((com.truecaller.truepay.data.e.e)localObject1);
    localObject1 = (ao)dagger.a.g.a(a.j(), "Cannot return null from a non-@Nullable component method");
    B = ((ao)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.l paraml)
  {
    com.truecaller.truepay.app.ui.transaction.c.r localr = (com.truecaller.truepay.app.ui.transaction.c.r)o.get();
    c = localr;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.m paramm)
  {
    Object localObject1 = com.truecaller.truepay.app.ui.transaction.c.u.a();
    Object localObject2 = new com/truecaller/truepay/a/a/f/n;
    Object localObject3 = b();
    ((com.truecaller.truepay.a.a.f.n)localObject2).<init>((com.truecaller.truepay.data.f.x)localObject3);
    a = ((com.truecaller.truepay.a.a.f.n)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/f/d;
    localObject3 = b();
    ((com.truecaller.truepay.a.a.f.d)localObject2).<init>((com.truecaller.truepay.data.f.x)localObject3);
    b = ((com.truecaller.truepay.a.a.f.d)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/f/m;
    localObject3 = b();
    ((com.truecaller.truepay.a.a.f.m)localObject2).<init>((com.truecaller.truepay.data.f.x)localObject3);
    c = ((com.truecaller.truepay.a.a.f.m)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/f/r;
    localObject3 = b();
    ((com.truecaller.truepay.a.a.f.r)localObject2).<init>((com.truecaller.truepay.data.f.x)localObject3);
    f = ((com.truecaller.truepay.a.a.f.r)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/f/l;
    localObject3 = b();
    ((com.truecaller.truepay.a.a.f.l)localObject2).<init>((com.truecaller.truepay.data.f.x)localObject3);
    g = ((com.truecaller.truepay.a.a.f.l)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/f/p;
    localObject3 = (ac)dagger.a.g.a(a.al(), "Cannot return null from a non-@Nullable component method");
    ((com.truecaller.truepay.a.a.f.p)localObject2).<init>((ac)localObject3);
    h = ((com.truecaller.truepay.a.a.f.p)localObject2);
    localObject2 = (ResolveRequestUser)dagger.a.g.a(a.aj(), "Cannot return null from a non-@Nullable component method");
    i = ((ResolveRequestUser)localObject2);
    localObject2 = (com.truecaller.truepay.app.ui.npci.e)dagger.a.g.a(a.as(), "Cannot return null from a non-@Nullable component method");
    j = ((com.truecaller.truepay.app.ui.npci.e)localObject2);
    localObject2 = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    k = ((com.truecaller.truepay.app.utils.a)localObject2);
    n = ((t)localObject1);
    localObject1 = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    o = ((com.truecaller.truepay.app.utils.a)localObject1);
    localObject1 = (au)dagger.a.g.a(a.k(), "Cannot return null from a non-@Nullable component method");
    p = ((au)localObject1);
    localObject1 = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    q = ((com.truecaller.truepay.app.utils.r)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.n paramn)
  {
    Object localObject = (com.truecaller.truepay.app.ui.transaction.c.v)B.get();
    e = ((com.truecaller.truepay.app.ui.transaction.c.v)localObject);
    localObject = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.N(), "Cannot return null from a non-@Nullable component method");
    f = ((com.truecaller.truepay.data.e.e)localObject);
    localObject = (com.truecaller.utils.n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    g = ((com.truecaller.utils.n)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.o paramo)
  {
    com.truecaller.truepay.app.ui.transaction.c.x localx = (com.truecaller.truepay.app.ui.transaction.c.x)G.get();
    f = localx;
  }
  
  public final void a(TransactionActivity paramTransactionActivity)
  {
    Object localObject = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    featuresRegistry = ((com.truecaller.featuretoggles.e)localObject);
    localObject = (com.truecaller.truepay.app.utils.u)dagger.a.g.a(a.x(), "Cannot return null from a non-@Nullable component method");
    instantRewardHandler = ((com.truecaller.truepay.app.utils.u)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.adapters.v paramv)
  {
    Object localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    a = ((com.truecaller.truepay.app.utils.r)localObject);
    localObject = (au)dagger.a.g.a(a.k(), "Cannot return null from a non-@Nullable component method");
    b = ((au)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */