package com.truecaller.truepay.app.ui.transaction.a.a;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final a a;
  private final Provider b;
  private final Provider c;
  
  private d(a parama, Provider paramProvider1, Provider paramProvider2)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static d a(a parama, Provider paramProvider1, Provider paramProvider2)
  {
    d locald = new com/truecaller/truepay/app/ui/transaction/a/a/d;
    locald.<init>(parama, paramProvider1, paramProvider2);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.a.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */