package com.truecaller.truepay.app.ui.transaction.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.ui.history.views.activities.TruePayWebViewActivity;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.b.f;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.ui.transaction.views.a.b.a;
import com.truecaller.truepay.app.ui.transaction.views.a.e;
import com.truecaller.truepay.app.ui.transaction.views.a.f.a;
import com.truecaller.truepay.app.ui.transaction.views.a.h;
import com.truecaller.truepay.app.ui.transaction.views.a.h.b;
import com.truecaller.truepay.app.ui.transaction.views.a.i;
import com.truecaller.truepay.app.ui.transaction.views.a.k;
import com.truecaller.truepay.app.ui.transaction.views.a.m;
import com.truecaller.truepay.app.ui.transaction.views.a.o.a;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.data.d.c;
import com.truecaller.utils.extensions.t;

public class TransactionActivity
  extends com.truecaller.truepay.app.ui.base.views.a.b
  implements b.a, f.a, h.b, com.truecaller.truepay.app.ui.transaction.views.a.j.a, o.a, com.truecaller.truepay.app.ui.transaction.views.c.l
{
  public static boolean isMerchantTxn;
  public static boolean isShowingProgress;
  FrameLayout progressFrameLayout;
  
  private String getDeeplinkHost()
  {
    Object localObject = getIntent();
    if (localObject != null)
    {
      localObject = getIntent().getData();
      if (localObject != null)
      {
        localObject = getIntent().getData().getHost();
        if (localObject != null)
        {
          localObject = getIntent().getData().getHost();
          int i = ((String)localObject).length();
          if (i != 0) {
            return getIntent().getData().getHost();
          }
        }
      }
    }
    return "";
  }
  
  private String getInvitationMessage()
  {
    int i = R.string.invitation_message;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "truecaller.com/download";
    return getString(i, arrayOfObject);
  }
  
  private String getRoute()
  {
    Object localObject = getDeeplinkHost();
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (!bool) {
      return getDeeplinkHost();
    }
    localObject = getIntent().getExtras();
    if (localObject != null)
    {
      localObject = getIntent().getExtras();
      String str = "tranx_type";
      bool = ((Bundle)localObject).containsKey(str);
      if (bool) {
        return getIntent().getExtras().getString("tranx_type", "");
      }
    }
    return "";
  }
  
  private void handleDeeplinkToAddBenfy()
  {
    Bundle localBundle = getIntent().getExtras();
    String str1 = "account";
    if (localBundle != null)
    {
      String str2 = localBundle.getString("benfy_type");
      if (str2 != null) {
        str1 = localBundle.getString("benfy_type");
      }
    }
    getIntent().putExtra("tranx_is_beneficiary", true);
    getIntent().putExtra("tranx_type", 1004);
    showContactSelection();
    onAddBeneficiaryClicked(str1);
  }
  
  private void handleDeeplinkToRequest()
  {
    Object localObject1 = getIntent();
    if (localObject1 != null)
    {
      localObject1 = getIntent().getExtras();
      if (localObject1 != null)
      {
        localObject1 = getIntent().getExtras().getString("payer_vpa");
        String str1 = getIntent().getExtras().getString("amount");
        String str2 = getIntent().getExtras().getString("payer_mobile");
        Object localObject2 = getIntent().getExtras();
        Object localObject3 = "trnx_comment";
        localObject2 = ((Bundle)localObject2).getString((String)localObject3);
        boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
        if (bool)
        {
          bool = TextUtils.isEmpty(str1);
          if (bool)
          {
            bool = TextUtils.isEmpty(str2);
            if (bool)
            {
              showCollectOptionContacts("deeplink");
              return;
            }
          }
        }
        localObject3 = new com/truecaller/truepay/app/ui/transaction/b/n;
        ((com.truecaller.truepay.app.ui.transaction.b.n)localObject3).<init>();
        p localp = new com/truecaller/truepay/app/ui/transaction/b/p;
        localp.<init>();
        e = ((String)localObject1);
        f = str2;
        str2 = null;
        if (localObject1 != null)
        {
          String str3 = "@";
          localObject1 = au.b(localObject1.split(str3)[0]);
        }
        else
        {
          localObject1 = "";
        }
        h = ((String)localObject1);
        h = ((com.truecaller.truepay.app.ui.transaction.b.n)localObject3);
        e = str1;
        e = str1;
        i = ((String)localObject2);
        m = false;
        localObject1 = new android/os/Bundle;
        ((Bundle)localObject1).<init>();
        ((Bundle)localObject1).putSerializable("payable_object", localp);
        ((Bundle)localObject1).putInt("tranx_type", 1003);
        localObject1 = m.a((Bundle)localObject1);
        showFragment((Fragment)localObject1, true);
        return;
      }
    }
    showCollectOptionContacts("deeplink");
  }
  
  private void handleDeeplinkToSend()
  {
    Object localObject1 = getIntent();
    if (localObject1 != null)
    {
      localObject1 = getIntent().getExtras();
      if (localObject1 != null)
      {
        localObject1 = getIntent().getExtras().getString("receiver_vpa");
        String str1 = getIntent().getExtras().getString("amount");
        String str2 = getIntent().getExtras().getString("trnx_comment");
        String str3 = getIntent().getExtras().getString("receiver_mobile");
        String str4 = getIntent().getExtras().getString("receiver_acc_num");
        String str5 = getIntent().getExtras().getString("receiver_ifsc");
        String str6 = getIntent().getExtras().getString("receiver_aadhaar_num");
        String str7 = getIntent().getExtras().getString("receiver_iin");
        Object localObject2 = getIntent().getExtras();
        Object localObject3 = "receiver_name";
        localObject2 = ((Bundle)localObject2).getString((String)localObject3);
        boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
        if (bool)
        {
          bool = TextUtils.isEmpty(str3);
          if (bool)
          {
            bool = TextUtils.isEmpty(str4);
            if (bool)
            {
              bool = TextUtils.isEmpty(str5);
              if (bool)
              {
                bool = TextUtils.isEmpty(str6);
                if (bool)
                {
                  bool = TextUtils.isEmpty(str7);
                  if (bool)
                  {
                    bool = TextUtils.isEmpty((CharSequence)localObject2);
                    if (bool)
                    {
                      showContactSelection();
                      return;
                    }
                  }
                }
              }
            }
          }
        }
        localObject3 = new com/truecaller/truepay/app/ui/transaction/b/n;
        ((com.truecaller.truepay.app.ui.transaction.b.n)localObject3).<init>();
        p localp = new com/truecaller/truepay/app/ui/transaction/b/p;
        localp.<init>();
        e = ((String)localObject1);
        if (localObject1 != null)
        {
          String str8 = "@";
          localObject1 = au.b(localObject1.split(str8)[0]);
        }
        else
        {
          localObject1 = "";
        }
        h = ((String)localObject1);
        a = str4;
        b = str5;
        c = str6;
        d = str7;
        f = str3;
        h = ((String)localObject2);
        h = ((com.truecaller.truepay.app.ui.transaction.b.n)localObject3);
        c = "Deeplink";
        d = "pay_other";
        e = str1;
        i = str2;
        m = false;
        localObject1 = new android/os/Bundle;
        ((Bundle)localObject1).<init>();
        ((Bundle)localObject1).putSerializable("payable_object", localp);
        ((Bundle)localObject1).putInt("tranx_type", 1004);
        localObject1 = m.a((Bundle)localObject1);
        showFragment((Fragment)localObject1, true);
        return;
      }
    }
    showContactSelection();
  }
  
  private void handleDeeplinkToShowBeneficiaries()
  {
    getIntent().putExtra("tranx_is_beneficiary", true);
    getIntent().putExtra("tranx_type", 1004);
    showContactSelection();
  }
  
  private void initUI()
  {
    Object localObject1 = getRoute();
    int i = ((String)localObject1).hashCode();
    int j = 1;
    int k = -1;
    boolean bool3;
    switch (i)
    {
    default: 
      break;
    case 1855357622: 
      localObject2 = "trnx_type_send";
      boolean bool1 = ((String)localObject1).equals(localObject2);
      if (bool1) {
        int m = 3;
      }
      break;
    case 1095692943: 
      localObject2 = "request";
      boolean bool2 = ((String)localObject1).equals(localObject2);
      if (bool2) {
        int n = 5;
      }
      break;
    case 724706375: 
      localObject2 = "add_beneficiary";
      bool3 = ((String)localObject1).equals(localObject2);
      if (bool3)
      {
        bool3 = false;
        localObject1 = null;
      }
      break;
    case 140658849: 
      localObject2 = "trnx_type_request";
      bool3 = ((String)localObject1).equals(localObject2);
      if (bool3) {
        int i1 = 4;
      }
      break;
    case 3526536: 
      localObject2 = "send";
      boolean bool4 = ((String)localObject1).equals(localObject2);
      if (bool4) {
        int i2 = 2;
      }
      break;
    case -1897995709: 
      localObject2 = "beneficiaries";
      boolean bool5 = ((String)localObject1).equals(localObject2);
      if (bool5) {
        bool5 = true;
      }
      break;
    }
    int i3 = -1;
    switch (i3)
    {
    default: 
      localObject1 = new String[j];
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Host not handled");
      String str = getDeeplinkHost();
      ((StringBuilder)localObject2).append(str);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject1[0] = localObject2;
      localObject1 = getIntent();
      localObject2 = "route";
      localObject1 = ((Intent)localObject1).getStringExtra((String)localObject2);
      if (localObject1 == null) {
        break label398;
      }
      i = ((String)localObject1).hashCode();
      j = 224129521;
      if (i == j) {
        break;
      }
      break;
    case 4: 
    case 5: 
      handleDeeplinkToRequest();
      return;
    case 2: 
    case 3: 
      handleDeeplinkToSend();
      return;
    case 1: 
      handleDeeplinkToShowBeneficiaries();
      return;
    case 0: 
      handleDeeplinkToAddBenfy();
      return;
    }
    Object localObject2 = "route_pending_request";
    boolean bool6 = ((String)localObject1).equals(localObject2);
    if (bool6) {
      k = 0;
    }
    if (k == 0)
    {
      showPendingRequests();
      return;
    }
    label398:
    localObject1 = getIntent();
    localObject2 = "type";
    localObject1 = ((Intent)localObject1).getStringExtra((String)localObject2);
    if (localObject1 == null)
    {
      showContactSelection();
      return;
    }
    isMerchantTxn = getIntent().getBooleanExtra("is_merchant_txn", false);
    proceedToMerchantPayment();
  }
  
  private void initViews()
  {
    int i = R.id.overlay_progress_frame;
    FrameLayout localFrameLayout = (FrameLayout)findViewById(i);
    progressFrameLayout = localFrameLayout;
  }
  
  private void showAddBeneficiary(String paramString, com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    int i = getIntent().getIntExtra("tranx_type", 0);
    android.support.v4.app.o localo = getSupportFragmentManager().a();
    int j = R.id.payment_container;
    paramString = com.truecaller.truepay.app.ui.transaction.views.a.b.a(paramString, i, parama);
    parama = com.truecaller.truepay.app.ui.transaction.views.a.b.class.getSimpleName();
    localo.b(j, paramString, parama).a(null).c();
  }
  
  private void showCollectOptionContacts(String paramString)
  {
    android.support.v4.app.o localo = getSupportFragmentManager().a();
    int i = R.id.payment_container;
    paramString = e.a(paramString);
    localo.b(i, paramString).a(null).c();
  }
  
  private void showPendingRequests()
  {
    com.truecaller.truepay.app.ui.transaction.views.a.o localo = com.truecaller.truepay.app.ui.transaction.views.a.o.b();
    showFragment(localo, true);
  }
  
  private void showSendOptionContacts(String paramString, boolean paramBoolean)
  {
    android.support.v4.app.o localo = getSupportFragmentManager().a();
    int i = R.id.payment_container;
    paramString = com.truecaller.truepay.app.ui.transaction.views.a.n.a(paramBoolean, paramString);
    String str = com.truecaller.truepay.app.ui.transaction.views.a.n.class.getSimpleName();
    localo.b(i, paramString, str).a(null).c();
  }
  
  public static void startForRequest(Context paramContext, String paramString1, String paramString2)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, TransactionActivity.class);
    localIntent.setFlags(536870912);
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("tranx_type", "trnx_type_request");
    localBundle.putString("payer_mobile", paramString1);
    localBundle.putString("payer_name", paramString2);
    localIntent.putExtras(localBundle);
    paramContext.startActivity(localIntent);
  }
  
  public static void startForRequest(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, TransactionActivity.class);
    localIntent.setFlags(536870912);
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("tranx_type", "trnx_type_request");
    localBundle.putString("amount", paramString1);
    localBundle.putString("trnx_comment", paramString2);
    localBundle.putString("payer_vpa", paramString3);
    localBundle.putString("payer_mobile", paramString4);
    localBundle.putString("payer_name", paramString5);
    localIntent.putExtras(localBundle);
    paramContext.startActivity(localIntent);
  }
  
  public static void startForSend(Context paramContext, String paramString1, String paramString2)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, TransactionActivity.class);
    localIntent.setFlags(536870912);
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("tranx_type", "trnx_type_send");
    localBundle.putString("receiver_mobile", paramString1);
    localBundle.putString("receiver_name", paramString2);
    localIntent.putExtras(localBundle);
    paramContext.startActivity(localIntent);
  }
  
  public static void startForSend(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, TransactionActivity.class);
    localIntent.setFlags(536870912);
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("tranx_type", "trnx_type_send");
    localBundle.putString("receiver_vpa", paramString2);
    localBundle.putString("amount", paramString1);
    localBundle.putString("trnx_comment", paramString4);
    localBundle.putString("receiver_mobile", paramString5);
    localBundle.putString("receiver_name", paramString3);
    localIntent.putExtras(localBundle);
    paramContext.startActivity(localIntent);
  }
  
  protected String getCurrentFragmentName()
  {
    int i = getFragmentCount();
    if (i > 0)
    {
      j localj = getSupportFragmentManager();
      int j = getFragmentCount() + -1;
      return localj.c(j).h();
    }
    return "";
  }
  
  protected int getFragmentCount()
  {
    return getSupportFragmentManager().e();
  }
  
  public int getLayoutId()
  {
    return R.layout.activity_transaction;
  }
  
  public void hideProgress()
  {
    boolean bool = isFinishing();
    if (!bool)
    {
      bool = false;
      isShowingProgress = false;
      FrameLayout localFrameLayout = progressFrameLayout;
      int i = 8;
      localFrameLayout.setVisibility(i);
    }
  }
  
  public void initDagger(com.truecaller.truepay.app.a.a.a parama)
  {
    parama = com.truecaller.truepay.app.ui.transaction.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    parama.a(locala).a().a(this);
  }
  
  public void inviteContact(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    onContactInvite(parama);
  }
  
  public void onAcceptClicked(c paramc)
  {
    paramc = m.a(null, null, paramc, null, 1004);
    showFragment(paramc, true);
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int i = 999;
    if (paramInt1 != i)
    {
      new String[1][0] = "Error routing";
      return;
    }
    paramInt1 = -1;
    if (paramInt2 == paramInt1)
    {
      initUI();
      return;
    }
    new String[1][0] = "Error getting onboarded";
  }
  
  public void onAddBeneficiaryClicked(String paramString)
  {
    showAddBeneficiary(paramString, null);
  }
  
  public void onBackPressed()
  {
    Object localObject = progressFrameLayout;
    int i = 2;
    t.a((View)localObject, false, i);
    boolean bool1 = isShowingProgress;
    if (bool1) {
      return;
    }
    localObject = progressFrameLayout;
    i = 8;
    ((FrameLayout)localObject).setVisibility(i);
    bool1 = isMerchantTxn;
    String str;
    if (bool1)
    {
      localObject = m.class.getName();
      str = getCurrentFragmentName();
      bool1 = ((String)localObject).equalsIgnoreCase(str);
      if (!bool1)
      {
        localObject = k.class.getName();
        str = getCurrentFragmentName();
        bool1 = ((String)localObject).equalsIgnoreCase(str);
        if (!bool1) {}
      }
      else
      {
        localObject = new android/content/Intent;
        ((Intent)localObject).<init>("MERCHANT_INTENT");
        android.support.v4.content.d.a(this).a((Intent)localObject);
        finish();
        return;
      }
    }
    int j = getFragmentCount();
    i = 1;
    if (j != i)
    {
      localObject = k.class.getName();
      str = getCurrentFragmentName();
      boolean bool2 = ((String)localObject).equalsIgnoreCase(str);
      if (bool2)
      {
        bool2 = com.truecaller.truepay.app.ui.transaction.a.a;
        if (bool2)
        {
          localObject = getResources();
          i = R.color.colorPrimaryDark;
          int k = ((Resources)localObject).getColor(i);
          setStatusBarColor(k);
          com.truecaller.truepay.app.ui.transaction.a.a = false;
          super.onBackPressed();
          return;
        }
        finish();
        return;
      }
      super.onBackPressed();
      return;
    }
    finish();
  }
  
  public void onBeneficiaryClicked(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    paramb = m.a(null, paramb, null, null, 1003);
    showFragment(paramb, true);
  }
  
  public void onBeneficiarySelected(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    paramb = m.a(null, paramb, null, null, 1004);
    showFragment(paramb, true);
  }
  
  public void onBlockedVpaListClicked()
  {
    com.truecaller.truepay.app.ui.transaction.views.a.d locald = com.truecaller.truepay.app.ui.transaction.views.a.d.f();
    showFragment(locald, true);
  }
  
  public void onContactInvite(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    Intent localIntent = new android/content/Intent;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("https://api.whatsapp.com/send?phone=91");
    parama = e;
    localStringBuilder.append(parama);
    localStringBuilder.append("&text=");
    parama = getInvitationMessage();
    localStringBuilder.append(parama);
    parama = Uri.parse(localStringBuilder.toString());
    localIntent.<init>("android.intent.action.VIEW", parama);
    startActivity(localIntent);
  }
  
  public void onContactSelected(com.truecaller.truepay.app.ui.transaction.b.a parama, int paramInt)
  {
    parama = m.a(parama, null, null, null, paramInt);
    showFragment(parama, true);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    initViews();
    boolean bool = isUserOnboarded();
    if (bool)
    {
      initUI();
      return;
    }
    new String[1][0] = "TransActivity: Trying to register user";
  }
  
  public void onFetchingOwnAccountVPA(com.truecaller.truepay.data.api.model.a parama)
  {
    parama = m.a(null, null, null, parama, 1004);
    showFragment(parama, true);
  }
  
  public void onPaySavedBeneficiary(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    paramb = m.a(null, paramb, null, null, 1004);
    showFragment(paramb, true);
  }
  
  public void onRequestSavedBeneficiary(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    onBeneficiaryClicked(paramb);
  }
  
  public void onResume()
  {
    super.onResume();
  }
  
  public void onSaveBeneficiarySucessful()
  {
    getSupportFragmentManager().c();
  }
  
  public void proceedToMerchantPayment()
  {
    m localm = m.a(getIntent().getExtras());
    showFragment(localm, true);
  }
  
  public void returnToCallingIntent(p paramp)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    Object localObject = j;
    String str = o;
    localBundle.putString("txnId", str);
    str = e;
    localBundle.putString("responseCode", str);
    str = b;
    localBundle.putString("status", str);
    paramp = n;
    localBundle.putString("txnRef", paramp);
    localObject = d;
    localBundle.putString("message", (String)localObject);
    paramp = new android/content/Intent;
    paramp.<init>("MERCHANT_INTENT");
    paramp.putExtras(localBundle);
    android.support.v4.content.d.a(this).a(paramp);
    finish();
  }
  
  public void setStatusBarColor(int paramInt)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      Window localWindow = getWindow();
      localWindow.setStatusBarColor(paramInt);
    }
  }
  
  public void showAccountChooser(i parami)
  {
    showFragment(parami, true);
  }
  
  public void showAddBeneficiaryUsingAccNumberIfsc(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    showAddBeneficiary("account", parama);
  }
  
  public void showContactSelection()
  {
    Intent localIntent = getIntent();
    int i = 1004;
    int j = localIntent.getIntExtra("tranx_type", i);
    Object localObject = getIntent();
    String str = "from";
    localObject = ((Intent)localObject).getStringExtra(str);
    int k = 1003;
    if (j == k)
    {
      showCollectOptionContacts((String)localObject);
      return;
    }
    if (j == i)
    {
      boolean bool = getIntent().getBooleanExtra("tranx_is_beneficiary", false);
      showSendOptionContacts((String)localObject, bool);
      return;
    }
    new String[1][0] = "Invalid trnx type selected";
  }
  
  public void showFragment(Fragment paramFragment, boolean paramBoolean)
  {
    try
    {
      Object localObject1 = getSupportFragmentManager();
      localObject1 = ((j)localObject1).a();
      if (paramBoolean)
      {
        Object localObject2 = paramFragment.getClass();
        localObject2 = ((Class)localObject2).getName();
        ((android.support.v4.app.o)localObject1).a((String)localObject2);
      }
      paramBoolean = R.id.payment_container;
      Object localObject3 = paramFragment.getClass();
      localObject3 = ((Class)localObject3).getSimpleName();
      paramFragment = ((android.support.v4.app.o)localObject1).a(paramBoolean, paramFragment, (String)localObject3);
      paramFragment.d();
      return;
    }
    catch (Exception localException) {}
  }
  
  public void showInviteOptionPopup(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    parama = h.c(parama);
    android.support.v4.app.o localo = getSupportFragmentManager().a();
    String str = h.class.getSimpleName();
    localo.a(parama, str).d();
  }
  
  public void showNeedHelp(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(this, TruePayWebViewActivity.class);
    localIntent.putExtra("url", paramString);
    startActivity(localIntent);
    finish();
  }
  
  public void showPayConfirmation(p paramp)
  {
    paramp = k.a(paramp);
    boolean bool = true;
    showFragment(paramp, bool);
    com.truecaller.truepay.app.ui.dashboard.a.b = bool;
  }
  
  public void showProgress()
  {
    boolean bool = isFinishing();
    if (!bool)
    {
      bool = true;
      isShowingProgress = bool;
      progressFrameLayout.setVisibility(0);
      progressFrameLayout.setClickable(bool);
      Object localObject = getSupportFragmentManager();
      com.truecaller.truepay.app.ui.registration.views.a locala = new com/truecaller/truepay/app/ui/registration/views/a;
      locala.<init>();
      String str = "";
      a = str;
      localObject = ((j)localObject).a();
      int i = R.id.overlay_progress_frame;
      localObject = ((android.support.v4.app.o)localObject).b(i, locala);
      ((android.support.v4.app.o)localObject).d();
    }
  }
  
  public void showResetPin(com.truecaller.truepay.data.api.model.a parama, String paramString)
  {
    ManageAccountsActivity.a(this, "action.page.reset_upi_pin", parama, paramString);
    finish();
  }
  
  public void showSetPin(com.truecaller.truepay.data.api.model.a parama, String paramString)
  {
    ManageAccountsActivity.a(this, "action.page.forgot_upi_pin", parama, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */