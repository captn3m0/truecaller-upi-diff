package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.Recycler;
import android.support.v7.widget.RecyclerView.State;
import android.util.AttributeSet;

public class WrapContentLinearLayoutManager
  extends LinearLayoutManager
{
  public WrapContentLinearLayoutManager(Context paramContext)
  {
    super(paramContext);
  }
  
  public WrapContentLinearLayoutManager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
  }
  
  public void onLayoutChildren(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    try
    {
      super.onLayoutChildren(paramRecycler, paramState);
      return;
    }
    catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
    {
      new String[1][0] = "IOOBE in RecyclerViey, notifyDataSetChanged condition";
    }
  }
  
  public boolean supportsPredictiveItemAnimations()
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.WrapContentLinearLayoutManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */