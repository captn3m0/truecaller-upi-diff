package com.truecaller.truepay.app.ui.transaction.views.a;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.constraint.Group;
import android.text.InputFilter;
import android.text.TextUtils;
import android.transition.Slide;
import android.transition.Transition;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.ui.transaction.c.t.1;
import com.truecaller.truepay.app.ui.transaction.c.t.4;
import com.truecaller.truepay.app.ui.transaction.c.t.5;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.k;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.data.api.model.ag;
import com.truecaller.truepay.data.api.model.ah;
import com.truecaller.truepay.data.api.model.u;
import com.truecaller.truepay.data.d.c;
import com.truecaller.truepay.data.f.x;
import io.reactivex.q;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class m
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements q.a, s.a, com.truecaller.truepay.app.ui.transaction.views.c.f
{
  private static String A;
  private static long B;
  ImageView a;
  TextView b;
  TextView c;
  EditText d;
  EditText e;
  ImageView f;
  ImageView g;
  TextView h;
  Group i;
  Button j;
  TextView k;
  ImageView l;
  public com.truecaller.truepay.app.ui.transaction.c.t n;
  public com.truecaller.truepay.app.utils.a o;
  public au p;
  public r q;
  private com.truecaller.truepay.app.ui.transaction.views.c.l r;
  private com.truecaller.truepay.data.api.model.a s;
  private com.truecaller.truepay.app.ui.transaction.b.a t;
  private com.truecaller.truepay.app.ui.transaction.b.b u;
  private com.truecaller.truepay.app.ui.transaction.b.n v;
  private p w;
  private c x;
  private boolean y;
  private com.truecaller.truepay.data.api.model.a z;
  
  public static m a(Bundle paramBundle)
  {
    m localm = new com/truecaller/truepay/app/ui/transaction/views/a/m;
    localm.<init>();
    localm.setArguments(paramBundle);
    return localm;
  }
  
  public static m a(com.truecaller.truepay.app.ui.transaction.b.a parama, com.truecaller.truepay.app.ui.transaction.b.b paramb, c paramc, com.truecaller.truepay.data.api.model.a parama1, int paramInt)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    m localm = new com/truecaller/truepay/app/ui/transaction/views/a/m;
    localm.<init>();
    localBundle.putSerializable("receiver_contact", parama);
    localBundle.putSerializable("receiver_beneficiary", paramb);
    localBundle.putSerializable("pending_collect_request", paramc);
    localBundle.putSerializable("receiver_own_account", parama1);
    localBundle.putInt("tranx_type", paramInt);
    localm.setArguments(localBundle);
    return localm;
  }
  
  private void b(com.truecaller.truepay.data.api.model.a parama)
  {
    m localm = this;
    Object localObject1 = parama;
    Object localObject2 = d.getText().toString();
    boolean bool1 = h((String)localObject2);
    String str1 = null;
    if (bool1)
    {
      Object localObject3 = v.i;
      p localp = w;
      if (localp == null)
      {
        localp = new com/truecaller/truepay/app/ui/transaction/b/p;
        localp.<init>();
        g = ((String)localObject3);
        localObject3 = v;
        h = ((com.truecaller.truepay.app.ui.transaction.b.n)localObject3);
      }
      localObject3 = p;
      bool1 = h((String)localObject3);
      boolean bool2 = false;
      Object localObject4 = null;
      boolean bool3 = true;
      if (bool1)
      {
        double d1 = Double.parseDouble((String)localObject2);
        localObject3 = p;
        double d2 = Double.parseDouble((String)localObject3);
        bool1 = d1 < d2;
        if (bool1)
        {
          m = R.string.minimum_amount_text;
          localObject2 = new Object[bool3];
          localObject3 = p;
          localObject2[0] = localObject3;
          localObject1 = localm.getString(m, (Object[])localObject2);
          localm.a((String)localObject1, null);
          return;
        }
        localObject3 = f;
        bool1 = h((String)localObject3);
        if (bool1)
        {
          d1 = Double.parseDouble((String)localObject2);
          localObject3 = f;
          d2 = Double.parseDouble((String)localObject3);
          bool1 = d1 < d2;
          if (bool1)
          {
            m = R.string.maximum_amount_text;
            localObject2 = new Object[bool3];
            localObject3 = f;
            localObject2[0] = localObject3;
            localObject1 = localm.getString(m, (Object[])localObject2);
            localm.a((String)localObject1, null);
            return;
          }
        }
      }
      localObject3 = a;
      b = ((String)localObject3);
      k = ((com.truecaller.truepay.data.api.model.a)localObject1);
      c = "";
      localObject1 = t;
      if (localObject1 != null)
      {
        localObject1 = a;
        if (localObject1 != null) {
          localObject1 = "pay";
        } else {
          localObject1 = "pay_other";
        }
      }
      else
      {
        localObject1 = u;
        if (localObject1 != null)
        {
          localObject1 = i;
          if (localObject1 != null)
          {
            localObject1 = "pay_direct";
            break label409;
          }
        }
        else
        {
          localObject1 = w;
          if (localObject1 != null)
          {
            localObject1 = d;
            break label409;
          }
          localObject1 = x;
          if (localObject1 != null)
          {
            localObject1 = "collect_request_pay";
            break label409;
          }
          localObject1 = z;
          if (localObject1 != null)
          {
            localObject1 = "pay_own";
            break label409;
          }
        }
        localObject1 = "pay_other";
      }
      label409:
      d = ((String)localObject1);
      e = ((String)localObject2);
      localObject1 = e.getText().toString();
      i = ((String)localObject1);
      localObject1 = x;
      Object localObject5;
      String str2;
      if (localObject1 != null)
      {
        localObject1 = h;
        localObject2 = x.i;
        h = ((String)localObject2);
        localObject1 = x.c;
        o = ((String)localObject1);
        localObject1 = n;
        localObject2 = x;
        localObject3 = m();
        if (localObject3 != null)
        {
          localObject4 = a;
          if (localObject4 != null) {}
        }
        else
        {
          localObject4 = (com.truecaller.truepay.app.ui.transaction.views.c.f)d;
          ((com.truecaller.truepay.app.ui.transaction.views.c.f)localObject4).e();
        }
        localObject4 = "accept_request";
        localObject3 = a;
        localObject5 = new com/truecaller/truepay/data/api/model/ah;
        ((ah)localObject5).<init>();
        a = ((String)localObject4);
        str2 = "accept_request";
        bool2 = ((String)localObject4).equals(str2);
        if (bool2) {
          localObject4 = "collect_request_pay";
        } else {
          localObject4 = "";
        }
        i = ((String)localObject4);
        localObject4 = e;
        f = ((String)localObject4);
        localObject4 = b;
        c = ((String)localObject4);
        localObject4 = a;
        d = ((String)localObject4);
        localObject4 = h;
        e = ((String)localObject4);
        g = null;
        b = ((String)localObject3);
        localObject2 = c;
        j = ((String)localObject2);
        ((com.truecaller.truepay.app.ui.transaction.views.c.f)d).a(bool3);
        localObject2 = g.a.b((ah)localObject5);
        localObject3 = io.reactivex.g.a.b();
        localObject2 = ((io.reactivex.o)localObject2).b((io.reactivex.n)localObject3);
        localObject3 = io.reactivex.android.b.a.a();
        localObject2 = ((io.reactivex.o)localObject2).a((io.reactivex.n)localObject3);
        localObject3 = new com/truecaller/truepay/app/ui/transaction/c/t$5;
        ((t.5)localObject3).<init>((com.truecaller.truepay.app.ui.transaction.c.t)localObject1, localp);
        ((io.reactivex.o)localObject2).a((q)localObject3);
        return;
      }
      localObject1 = getArguments();
      localObject2 = "tranx_type";
      m = ((Bundle)localObject1).getInt((String)localObject2);
      int i1 = 1003;
      if (m == i1)
      {
        d = "create_collect";
        long l1 = B;
        localObject1 = String.valueOf(l1);
        s = ((String)localObject1);
        localObject1 = n;
        localObject2 = new com/truecaller/truepay/data/api/model/u;
        ((u)localObject2).<init>();
        localObject3 = b;
        b = ((String)localObject3);
        localObject3 = e;
        c = ((String)localObject3);
        localObject3 = i;
        d = ((String)localObject3);
        localObject3 = h.e;
        a = ((String)localObject3);
        localObject3 = h.i;
        e = ((String)localObject3);
        localObject3 = o;
        f = ((String)localObject3);
        localObject3 = h.h;
        g = ((String)localObject3);
        localObject3 = s;
        h = ((String)localObject3);
        ((com.truecaller.truepay.app.ui.transaction.views.c.f)d).a(bool3);
        localObject2 = c.a.a((u)localObject2);
        localObject3 = io.reactivex.g.a.b();
        localObject2 = ((io.reactivex.o)localObject2).b((io.reactivex.n)localObject3);
        localObject3 = io.reactivex.android.b.a.a();
        localObject2 = ((io.reactivex.o)localObject2).a((io.reactivex.n)localObject3);
        localObject3 = new com/truecaller/truepay/app/ui/transaction/c/t$4;
        ((t.4)localObject3).<init>((com.truecaller.truepay.app.ui.transaction.c.t)localObject1, localp);
        ((io.reactivex.o)localObject2).a((q)localObject3);
      }
      else
      {
        localObject1 = n;
        localObject2 = d;
        if (localObject2 != null)
        {
          ((com.truecaller.truepay.app.ui.transaction.views.c.f)d).a(bool3);
          localObject2 = new com/truecaller/truepay/app/ui/transaction/b/j;
          str2 = b;
          String str3 = c;
          String str4 = d;
          String str5 = e;
          String str6 = g;
          com.truecaller.truepay.app.ui.transaction.b.n localn = h;
          String str7 = i;
          localObject3 = o;
          str1 = n;
          String str8 = q;
          String str9 = r;
          localObject5 = localObject2;
          ((com.truecaller.truepay.app.ui.transaction.b.j)localObject2).<init>(str2, str3, str4, str5, str6, localn, str7, (String)localObject3, str1, str8, str9);
          localObject2 = a.a((com.truecaller.truepay.app.ui.transaction.b.j)localObject2);
          localObject3 = io.reactivex.g.a.b();
          localObject2 = ((io.reactivex.o)localObject2).b((io.reactivex.n)localObject3);
          localObject3 = io.reactivex.android.b.a.a();
          localObject2 = ((io.reactivex.o)localObject2).a((io.reactivex.n)localObject3);
          localObject3 = new com/truecaller/truepay/app/ui/transaction/c/t$1;
          ((t.1)localObject3).<init>((com.truecaller.truepay.app.ui.transaction.c.t)localObject1, localp);
          ((io.reactivex.o)localObject2).a((q)localObject3);
        }
      }
      localObject1 = d;
      i1 = 2;
      com.truecaller.utils.extensions.t.a((View)localObject1, false, i1);
      com.truecaller.utils.extensions.t.a(e, false, i1);
      return;
    }
    int m = R.string.invalid_amount;
    localObject1 = getString(m);
    a((String)localObject1, null);
  }
  
  private static String g(String paramString)
  {
    String str1 = "";
    String str2 = "utf-8";
    try
    {
      str1 = URLEncoder.encode(paramString, str2);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      for (;;) {}
    }
    return str1;
  }
  
  private static boolean h(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      String str = "null";
      bool1 = str.equalsIgnoreCase(paramString);
      if (!bool1)
      {
        double d1 = Double.parseDouble(paramString);
        double d2 = 1.0D;
        boolean bool2 = d1 < d2;
        if (!bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  private void l()
  {
    Object localObject1 = w;
    boolean bool1 = true;
    if (localObject1 != null)
    {
      bool2 = m;
      if (bool2)
      {
        bool2 = true;
        break label29;
      }
    }
    boolean bool2 = false;
    localObject1 = null;
    label29:
    Object localObject2 = z;
    if (localObject2 == null)
    {
      bool1 = false;
      localObject3 = null;
    }
    if (bool1)
    {
      localObject1 = z.a;
      localObject3 = new android/os/Bundle;
      ((Bundle)localObject3).<init>();
      localObject2 = "selectedAccountId";
      ((Bundle)localObject3).putString((String)localObject2, (String)localObject1);
      localObject1 = new com/truecaller/truepay/app/ui/transaction/views/a/i;
      ((i)localObject1).<init>();
      ((i)localObject1).setArguments((Bundle)localObject3);
    }
    else
    {
      bool1 = y;
      localObject1 = i.a(bool1, bool2);
    }
    d = this;
    int m = Build.VERSION.SDK_INT;
    int i1 = 21;
    if (m >= i1)
    {
      localObject3 = new android/transition/Slide;
      ((Slide)localObject3).<init>();
      long l1 = 50;
      ((Transition)localObject3).setDuration(l1);
      ((i)localObject1).setEnterTransition(localObject3);
    }
    Object localObject3 = d;
    i1 = 2;
    com.truecaller.utils.extensions.t.a((View)localObject3, false, i1);
    com.truecaller.utils.extensions.t.a(e, false, i1);
    r.showAccountChooser((i)localObject1);
  }
  
  private com.truecaller.truepay.data.api.model.a m()
  {
    Object localObject = s;
    if (localObject == null)
    {
      int m = R.string.no_account_available;
      localObject = getString(m);
      a((String)localObject, null);
    }
    return s;
  }
  
  private void n()
  {
    Object localObject1 = s;
    if (localObject1 != null)
    {
      localObject1 = j;
      if (localObject1 != null)
      {
        localObject1 = s.a;
        Object localObject2 = "pay_via_other";
        boolean bool = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (bool)
        {
          localObject1 = h;
          localObject2 = getResources();
          int m = R.string.pay_via_other;
          localObject2 = ((Resources)localObject2).getString(m);
          ((TextView)localObject1).setText((CharSequence)localObject2);
        }
        else
        {
          localObject1 = h;
          localObject2 = s.j.b;
          str = s.c;
          localObject2 = au.a((String)localObject2, str);
          ((TextView)localObject1).setText((CharSequence)localObject2);
        }
        localObject1 = f;
        localObject2 = q;
        String str = s.j.d;
        localObject2 = ((r)localObject2).b(str);
        ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
      }
    }
  }
  
  public final int a()
  {
    return R.layout.fragment_pay_entry;
  }
  
  public final void a(p paramp)
  {
    r.showPayConfirmation(paramp);
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    s = parama;
    n();
  }
  
  public final void a(ag paramag)
  {
    Object localObject = v.h;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (!bool)
    {
      localObject = b;
      int m = 2;
      Object[] arrayOfObject = new Object[m];
      String str1 = b;
      arrayOfObject[0] = str1;
      int i1 = 1;
      str1 = v.h;
      arrayOfObject[i1] = str1;
      str2 = String.format("%s (%s)", arrayOfObject);
      ((TextView)localObject).setText(str2);
    }
    else
    {
      localObject = b;
      str2 = b;
      ((TextView)localObject).setText(str2);
    }
    localObject = c;
    String str2 = a;
    ((TextView)localObject).setText(str2);
    localObject = v;
    str2 = a;
    e = str2;
    localObject = v;
    paramag = b;
    h = paramag;
    paramag = w;
    localObject = v;
    h = ((com.truecaller.truepay.app.ui.transaction.b.n)localObject);
  }
  
  public final void a(String paramString)
  {
    int m = R.string.collect_request_failed;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    paramString = getString(m, arrayOfObject);
    a(paramString, null);
  }
  
  public final void a(Throwable paramThrowable)
  {
    int m = R.string.server_error_message;
    String str = getString(m);
    a(str, paramThrowable);
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      r.showProgress();
      return;
    }
    r.hideProgress();
  }
  
  public final void b()
  {
    d.requestFocus();
    Object localObject1 = d;
    boolean bool1 = true;
    Object localObject2 = new InputFilter[bool1];
    Object localObject3 = new com/truecaller/truepay/app/utils/k;
    ((k)localObject3).<init>();
    localObject2[0] = localObject3;
    ((EditText)localObject1).setFilters((InputFilter[])localObject2);
    localObject1 = d;
    int m = 2;
    com.truecaller.utils.extensions.t.a((View)localObject1, bool1, m);
    localObject1 = o.c();
    s = ((com.truecaller.truepay.data.api.model.a)localObject1);
    localObject1 = (com.truecaller.truepay.app.ui.transaction.b.a)getArguments().getSerializable("receiver_contact");
    t = ((com.truecaller.truepay.app.ui.transaction.b.a)localObject1);
    localObject1 = (com.truecaller.truepay.app.ui.transaction.b.b)getArguments().getSerializable("receiver_beneficiary");
    u = ((com.truecaller.truepay.app.ui.transaction.b.b)localObject1);
    localObject1 = (p)getArguments().getSerializable("payable_object");
    w = ((p)localObject1);
    localObject1 = (c)getArguments().getSerializable("pending_collect_request");
    x = ((c)localObject1);
    localObject1 = getArguments();
    localObject3 = "receiver_own_account";
    localObject1 = (com.truecaller.truepay.data.api.model.a)((Bundle)localObject1).getSerializable((String)localObject3);
    z = ((com.truecaller.truepay.data.api.model.a)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/b/n;
    ((com.truecaller.truepay.app.ui.transaction.b.n)localObject1).<init>();
    v = ((com.truecaller.truepay.app.ui.transaction.b.n)localObject1);
    localObject1 = t;
    boolean bool2;
    if (localObject1 != null)
    {
      localObject1 = c;
      bool2 = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool2)
      {
        localObject1 = b;
        localObject2 = au.b(t.c);
        ((TextView)localObject1).setText((CharSequence)localObject2);
      }
      else
      {
        localObject1 = b;
        localObject2 = t.d;
        ((TextView)localObject1).setText((CharSequence)localObject2);
      }
      localObject1 = c;
      m = 8;
      ((TextView)localObject1).setVisibility(m);
      localObject1 = q;
      localObject2 = t.f;
      localObject3 = a;
      Object localObject4 = getContext().getResources();
      int i2 = R.drawable.ic_avatar_common;
      localObject4 = ((Resources)localObject4).getDrawable(i2);
      Object localObject5 = getContext().getResources();
      int i3 = R.drawable.ic_avatar_common;
      localObject5 = ((Resources)localObject5).getDrawable(i3);
      ((r)localObject1).a((String)localObject2, (ImageView)localObject3, (Drawable)localObject4, (Drawable)localObject5);
      localObject1 = v;
      localObject2 = t.d;
      h = ((String)localObject2);
      localObject1 = v;
      localObject2 = t.e;
      f = ((String)localObject2);
      localObject1 = v;
      localObject2 = t.a;
      i = ((String)localObject2);
      localObject1 = v;
      localObject2 = t.f;
      j = ((String)localObject2);
      localObject1 = v;
      localObject2 = t.b;
      e = ((String)localObject2);
      localObject1 = a;
      ((ImageView)localObject1).setVisibility(0);
    }
    else
    {
      localObject1 = u;
      if (localObject1 != null)
      {
        localObject2 = b;
        localObject1 = f;
        ((TextView)localObject2).setText((CharSequence)localObject1);
        localObject1 = v;
        localObject2 = u.h;
        e = ((String)localObject2);
        localObject1 = v;
        localObject2 = u.d;
        b = ((String)localObject2);
        localObject1 = v;
        localObject2 = u.b;
        a = ((String)localObject2);
        localObject1 = v;
        localObject2 = u.a;
        c = ((String)localObject2);
        localObject1 = v;
        localObject2 = u.k;
        d = ((String)localObject2);
        localObject1 = v;
        localObject2 = u.f;
        h = ((String)localObject2);
        localObject1 = v;
        localObject2 = u.i;
        i = ((String)localObject2);
        localObject1 = v;
        localObject2 = "";
        j = ((String)localObject2);
      }
      else
      {
        localObject1 = w;
        if (localObject1 != null)
        {
          localObject1 = h.h;
          bool2 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool2)
          {
            localObject1 = b;
            localObject3 = w.h.h;
            ((TextView)localObject1).setText((CharSequence)localObject3);
            localObject1 = v;
            localObject3 = w.h.h;
            h = ((String)localObject3);
          }
          localObject1 = w.h.e;
          bool2 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool2)
          {
            localObject1 = v;
            localObject3 = w.h.e;
            e = ((String)localObject3);
          }
          localObject1 = w;
          bool2 = m;
          if (!bool2)
          {
            localObject1 = w.e;
            bool2 = h((String)localObject1);
            if (!bool2) {}
          }
          else
          {
            localObject1 = d;
            localObject3 = w.e;
            ((EditText)localObject1).setText((CharSequence)localObject3);
            d.setEnabled(false);
            com.truecaller.utils.extensions.t.a(d, false, m);
            localObject1 = e;
            com.truecaller.utils.extensions.t.a((View)localObject1, false, m);
          }
          localObject1 = w.p;
          bool2 = h((String)localObject1);
          if (bool2)
          {
            d.setEnabled(bool1);
            localObject1 = d;
            ((EditText)localObject1).requestFocus();
          }
          localObject1 = w.i;
          bool2 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool2)
          {
            localObject1 = e;
            localObject2 = w.i;
            ((EditText)localObject1).setText((CharSequence)localObject2);
            localObject1 = e;
            ((EditText)localObject1).setEnabled(false);
          }
        }
        else
        {
          localObject1 = x;
          if (localObject1 != null)
          {
            localObject2 = d;
            localObject1 = a;
            ((EditText)localObject2).setText((CharSequence)localObject1);
            d.setEnabled(false);
            y = bool1;
            localObject1 = b;
            localObject2 = x.i;
            ((TextView)localObject1).setText((CharSequence)localObject2);
            localObject1 = v;
            localObject2 = x.b;
            e = ((String)localObject2);
            localObject1 = v;
            localObject2 = x.i;
            h = ((String)localObject2);
          }
          else
          {
            localObject1 = z;
            if (localObject1 != null)
            {
              localObject1 = n;
              if (localObject1 != null)
              {
                localObject1 = c;
                localObject2 = z.h;
                ((TextView)localObject1).setText((CharSequence)localObject2);
                localObject1 = v;
                localObject2 = z.n;
                e = ((String)localObject2);
              }
              localObject1 = z.b;
              if (localObject1 != null)
              {
                localObject1 = b;
                localObject2 = z.b;
                ((TextView)localObject1).setText((CharSequence)localObject2);
                localObject1 = v;
                localObject2 = z.b;
                h = ((String)localObject2);
              }
              localObject1 = z.a;
              if (localObject1 != null)
              {
                localObject1 = v;
                localObject2 = z.a;
                i = ((String)localObject2);
              }
              localObject1 = n;
              localObject2 = z;
              localObject1 = (com.truecaller.truepay.data.api.model.a)k.b((com.truecaller.truepay.data.api.model.a)localObject2).get(0);
              s = ((com.truecaller.truepay.data.api.model.a)localObject1);
            }
          }
        }
      }
    }
    n();
    localObject1 = getArguments();
    localObject2 = "tranx_type";
    int i1 = ((Bundle)localObject1).getInt((String)localObject2);
    m = 1003;
    if (i1 == m)
    {
      localObject1 = j;
      localObject2 = getResources();
      int i4 = R.string.pay_entry_request;
      localObject2 = ((Resources)localObject2).getString(i4);
      ((Button)localObject1).setText((CharSequence)localObject2);
      localObject1 = k;
      m = R.string.pay_entry_bank_selection_title;
      ((TextView)localObject1).setText(m);
      y = bool1;
      long l1 = 30;
      B = l1;
      localObject1 = l;
      ((ImageView)localObject1).setVisibility(0);
    }
    localObject1 = t;
    boolean bool3;
    String str;
    if (localObject1 == null)
    {
      localObject1 = v.e;
      bool3 = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool3)
      {
        localObject1 = c;
        str = v.e;
        ((TextView)localObject1).setText(str);
      }
      else
      {
        localObject1 = v.a;
        bool3 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool3)
        {
          localObject1 = c;
          str = v.a;
          ((TextView)localObject1).setText(str);
        }
        else
        {
          localObject1 = v.c;
          bool3 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool3)
          {
            localObject1 = c;
            str = v.c;
            ((TextView)localObject1).setText(str);
          }
        }
      }
    }
    localObject1 = w;
    if (localObject1 != null)
    {
      localObject1 = h.f;
      bool3 = TextUtils.isEmpty((CharSequence)localObject1);
      bool1 = false;
      str = null;
      if (!bool3)
      {
        localObject1 = n;
        localObject2 = w.h.f;
        ((com.truecaller.truepay.app.ui.transaction.c.t)localObject1).a((String)localObject2, null);
        return;
      }
      localObject1 = w.h.e;
      bool3 = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool3)
      {
        localObject1 = n;
        localObject2 = w.h.e;
        ((com.truecaller.truepay.app.ui.transaction.c.t)localObject1).a(null, (String)localObject2);
      }
    }
  }
  
  public final void b(p paramp)
  {
    a(paramp);
  }
  
  public final void b(String paramString)
  {
    a(paramString, null);
  }
  
  public final void c()
  {
    EditText localEditText = d;
    int m = 2;
    com.truecaller.utils.extensions.t.a(localEditText, false, m);
    com.truecaller.utils.extensions.t.a(e, false, m);
    r.onBackPressed();
  }
  
  public final void c(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      h();
      return;
    }
    paramString = s.a(paramString);
    paramString.setTargetFragment(this, 1010);
    android.support.v4.app.o localo = getFragmentManager().a();
    String str = s.class.getSimpleName();
    localo.a(paramString, str).d();
  }
  
  public final void d()
  {
    int m = R.string.failed_to_accept_request;
    String str = getString(m);
    a(str, null);
  }
  
  public final void d(String paramString)
  {
    a(paramString, null);
  }
  
  public final void e()
  {
    int m = R.string.account_not_available_message;
    String str = getString(m);
    a(str, null);
  }
  
  public final void e(String paramString)
  {
    com.truecaller.truepay.app.ui.transaction.b.a locala = new com/truecaller/truepay/app/ui/transaction/b/a;
    locala.<init>(paramString, paramString, "", false, "", "", "");
    r.onContactInvite(locala);
    getActivity().finish();
  }
  
  public final void f()
  {
    com.truecaller.truepay.app.ui.transaction.views.c.l locall = r;
    com.truecaller.truepay.data.api.model.a locala = m();
    locall.showSetPin(locala, "set_pin_reminder");
  }
  
  public final void g()
  {
    com.truecaller.truepay.data.api.model.a locala = s;
    if (locala != null)
    {
      boolean bool = true;
      l = bool;
      com.truecaller.truepay.app.utils.a locala1 = o;
      locala1.c(locala);
      locala = s;
      b(locala);
    }
  }
  
  public final void h()
  {
    int m = R.string.failed_to_resolve_vpa;
    Object[] arrayOfObject = new Object[1];
    String str = "";
    arrayOfObject[0] = str;
    Object localObject = getString(m, arrayOfObject);
    int i1 = 0;
    arrayOfObject = null;
    a((String)localObject, null);
    localObject = w;
    if (localObject != null)
    {
      boolean bool = m;
      if (!bool)
      {
        j.setEnabled(false);
        localObject = j;
        i1 = R.drawable.disable_cell;
        ((Button)localObject).setBackgroundResource(i1);
      }
    }
  }
  
  public final void i()
  {
    getActivity().finish();
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int m = 1011;
    if (paramInt1 == m)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        android.support.v4.app.f localf1 = getActivity();
        localf1.setResult(paramInt1);
        android.support.v4.app.f localf2 = getActivity();
        localf2.finish();
      }
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.transaction.views.c.l;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.transaction.views.c.l)getActivity();
      r = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement TransactionView");
    throw paramContext;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    r = null;
    n.b();
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.img_profile;
    paramBundle = (ImageView)paramView.findViewById(m);
    a = paramBundle;
    m = R.id.tv_name;
    paramBundle = (TextView)paramView.findViewById(m);
    b = paramBundle;
    m = R.id.tv_sub_name;
    paramBundle = (TextView)paramView.findViewById(m);
    c = paramBundle;
    m = R.id.tv_amount;
    paramBundle = (EditText)paramView.findViewById(m);
    d = paramBundle;
    m = R.id.tv_message;
    paramBundle = (EditText)paramView.findViewById(m);
    e = paramBundle;
    m = R.id.img_bank;
    paramBundle = (ImageView)paramView.findViewById(m);
    f = paramBundle;
    m = R.id.btn_down;
    paramBundle = (ImageView)paramView.findViewById(m);
    g = paramBundle;
    m = R.id.tv_bank_name;
    paramBundle = (TextView)paramView.findViewById(m);
    h = paramBundle;
    m = R.id.group_bank_acc_selector_frag_pay_entry;
    paramBundle = (Group)paramView.findViewById(m);
    i = paramBundle;
    m = R.id.btn_pay;
    paramBundle = (Button)paramView.findViewById(m);
    j = paramBundle;
    m = R.id.tv_bank_selection_title_frag_pay_entry;
    paramBundle = (TextView)paramView.findViewById(m);
    k = paramBundle;
    m = R.id.btn_options;
    paramBundle = (ImageView)paramView.findViewById(m);
    l = paramBundle;
    paramBundle = j;
    Object localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$m$ebalHP5xFtuszl5pdKXDbxXHO1M;
    ((-..Lambda.m.ebalHP5xFtuszl5pdKXDbxXHO1M)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    m = R.id.btn_close;
    paramBundle = paramView.findViewById(m);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$m$d4vR2heTw--58SE-s9t-eCBOXvI;
    ((-..Lambda.m.d4vR2heTw--58SE-s9t-eCBOXvI)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    paramBundle = g;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$m$cnfMUGdrqxDy5zO7lJQeo29T7AA;
    ((-..Lambda.m.cnfMUGdrqxDy5zO7lJQeo29T7AA)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    paramBundle = h;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$m$UCFHpXhEi-Cl8_b73yNW0tV7LA4;
    ((-..Lambda.m.UCFHpXhEi-Cl8_b73yNW0tV7LA4)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    m = R.id.view;
    paramView = paramView.findViewById(m);
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$m$kQz5zfRFCL8sKWz42V5OEHUnBHw;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = l;
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$m$XWL2hradgt-XeAScK5xHDMn-hmo;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    n.a(this);
    ((com.truecaller.truepay.app.ui.transaction.views.c.f)n.d).b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */