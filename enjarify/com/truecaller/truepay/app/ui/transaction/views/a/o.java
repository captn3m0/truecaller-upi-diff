package com.truecaller.truepay.app.ui.transaction.views.a;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.b;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.menu;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.views.adapters.y;
import com.truecaller.truepay.app.ui.transaction.views.c.i;
import com.truecaller.truepay.data.d.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class o
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements SwipeRefreshLayout.b, p.a, y, i
{
  RecyclerView a;
  Toolbar b;
  View c;
  ProgressBar d;
  SwipeRefreshLayout e;
  public com.truecaller.truepay.app.ui.transaction.c.x f;
  com.truecaller.truepay.app.ui.transaction.views.adapters.x g;
  o.a h;
  
  public static o b()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    o localo = new com/truecaller/truepay/app/ui/transaction/views/a/o;
    localo.<init>();
    localo.setArguments(localBundle);
    return localo;
  }
  
  public final int a()
  {
    return R.layout.fragment_pending_collect_request;
  }
  
  public final void a(c paramc)
  {
    h.onAcceptClicked(paramc);
  }
  
  public final void a(String paramString)
  {
    a(paramString, null);
  }
  
  public final void a(Throwable paramThrowable)
  {
    int i = R.string.error_reject_request;
    String str = getString(i);
    a(str, paramThrowable);
  }
  
  public final void a(List paramList)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      c localc = (c)paramList.next();
      str1 = d;
      String str2 = "received";
      bool2 = str1.equalsIgnoreCase(str2);
      if (bool2) {
        localArrayList.add(localc);
      }
    }
    boolean bool3 = localArrayList.isEmpty();
    int i = 8;
    boolean bool2 = false;
    String str1 = null;
    if (!bool3)
    {
      c.setVisibility(i);
      a.setVisibility(0);
      g.a(localArrayList);
      g.notifyDataSetChanged();
      return;
    }
    c.setVisibility(0);
    a.setVisibility(i);
  }
  
  public final void a(boolean paramBoolean)
  {
    e.setRefreshing(paramBoolean);
  }
  
  public final void ad_()
  {
    f.a();
  }
  
  public final void b(c paramc)
  {
    paramc = p.a(paramc);
    paramc.setTargetFragment(this, 1008);
    j localj = getFragmentManager();
    String str = p.class.getSimpleName();
    paramc.show(localj, str);
  }
  
  public final void b(String paramString)
  {
    int i = R.string.block_vpa_success;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    paramString = getString(i, arrayOfObject);
    a(paramString, null);
    f.a();
  }
  
  public final void c()
  {
    Object localObject = getResources();
    int i = R.string.error_fetching_pending_requests;
    localObject = ((Resources)localObject).getString(i);
    a((String)localObject, null);
  }
  
  public final void c(c paramc)
  {
    f.a(paramc);
  }
  
  public final void c(String paramString)
  {
    Resources localResources = getResources();
    int i = R.string.block_vpa_failure;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    paramString = localResources.getString(i, arrayOfObject);
    a(paramString, null);
  }
  
  public final void d()
  {
    Object localObject = getResources();
    int i = R.string.reject_successful;
    localObject = ((Resources)localObject).getString(i);
    a((String)localObject, null);
    f.a();
  }
  
  public final void d(c paramc)
  {
    f.b(paramc);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof o.a;
    if (bool)
    {
      paramContext = (o.a)paramContext;
      h = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Parent must implement ");
    String str = o.a.class.getSimpleName();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    paramContext.<init>((String)localObject);
    throw paramContext;
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    int i = R.menu.menu_pending_collect;
    paramMenuInflater.inflate(i, paramMenu);
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    h = null;
    f.b();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    boolean bool = true;
    int j = 16908332;
    if (i == j)
    {
      getActivity().onBackPressed();
      return bool;
    }
    int k = paramMenuItem.getItemId();
    i = R.id.menu_item_blocked_vpas;
    if (k == i)
    {
      h.onBlockedVpaListClicked();
      return bool;
    }
    return false;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    Object localObject = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a().a(this);
    int i = R.id.rv_frag_pending_collect_req;
    paramBundle = (RecyclerView)paramView.findViewById(i);
    a = paramBundle;
    i = R.id.toolbar_frag_pending_collect_req;
    paramBundle = (Toolbar)paramView.findViewById(i);
    b = paramBundle;
    i = R.id.empty_state_fragment_pending_request;
    paramBundle = paramView.findViewById(i);
    c = paramBundle;
    i = R.id.pb_frag_pending_request;
    paramBundle = (ProgressBar)paramView.findViewById(i);
    d = paramBundle;
    i = R.id.srl_frag_pending_collect_req;
    paramView = (SwipeRefreshLayout)paramView.findViewById(i);
    e = paramView;
    e.setOnRefreshListener(this);
    paramView = a;
    paramBundle = new android/support/v7/widget/LinearLayoutManager;
    localObject = getContext();
    int j = 1;
    paramBundle.<init>((Context)localObject, j, false);
    paramView.setLayoutManager(paramBundle);
    paramView = new com/truecaller/truepay/app/ui/transaction/views/adapters/x;
    paramView.<init>(this);
    g = paramView;
    paramView = a;
    paramBundle = g;
    paramView.setAdapter(paramBundle);
    paramView = (com.truecaller.truepay.app.ui.base.views.a.b)getActivity();
    paramBundle = b;
    paramView.setSupportActionBar(paramBundle);
    paramView = ((com.truecaller.truepay.app.ui.base.views.a.b)getActivity()).getSupportActionBar();
    i = R.string.frag_pending_collect_req_title;
    paramView.setTitle(i);
    ((com.truecaller.truepay.app.ui.base.views.a.b)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(j);
    paramView = ((com.truecaller.truepay.app.ui.base.views.a.b)getActivity()).getSupportActionBar();
    i = R.drawable.ic_arrow_back_white_24dp;
    paramView.setHomeAsUpIndicator(i);
    paramView = b;
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/o$1;
    paramBundle.<init>(this);
    paramView.setNavigationOnClickListener(paramBundle);
    f.a(this);
    f.a();
    setHasOptionsMenu(j);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */