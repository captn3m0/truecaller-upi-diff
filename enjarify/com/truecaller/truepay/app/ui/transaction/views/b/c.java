package com.truecaller.truepay.app.ui.transaction.views.b;

import android.view.View;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.base.views.c.a;

public final class c
  extends a
{
  public final TextView b;
  public final TextView c;
  
  public c(View paramView, f paramf)
  {
    super(paramView, paramf);
    int i = R.id.tv_blocked_vpa_name;
    paramf = (TextView)paramView.findViewById(i);
    b = paramf;
    i = R.id.tv_blocked_vpa;
    paramf = (TextView)paramView.findViewById(i);
    c = paramf;
    i = R.id.btn_unblock_vpa;
    paramView = paramView.findViewById(i);
    paramf = new com/truecaller/truepay/app/ui/transaction/views/b/-$$Lambda$c$7AUTtD6RpydCiEwpAF5SrsbHsc8;
    paramf.<init>(this);
    paramView.setOnClickListener(paramf);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */