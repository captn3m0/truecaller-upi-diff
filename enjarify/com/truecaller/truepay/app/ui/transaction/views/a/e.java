package com.truecaller.truepay.app.ui.transaction.views.a;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.c.v;
import com.truecaller.truepay.app.ui.transaction.views.adapters.p;
import com.truecaller.truepay.app.ui.transaction.views.c.g;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.n;

public final class e
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements g
{
  ViewPager a;
  TabLayout b;
  FrameLayout c;
  p d;
  public v e;
  public com.truecaller.truepay.data.e.e f;
  public n g;
  l h;
  f i;
  
  public static e a(String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("flow_from", paramString);
    paramString = new com/truecaller/truepay/app/ui/transaction/views/a/e;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  private l b()
  {
    l locall = h;
    if (locall == null)
    {
      locall = (l)d.a(0);
      h = locall;
    }
    return h;
  }
  
  public final int a()
  {
    return R.layout.fragment_collect_selection;
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int j = 1005;
    if (paramInt1 == j)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        Object localObject1 = paramIntent.getSerializableExtra("beneficiary_account");
        paramInt2 = 0;
        Object localObject2 = null;
        if (localObject1 != null)
        {
          localObject1 = (com.truecaller.truepay.app.ui.transaction.b.b)paramIntent.getSerializableExtra("beneficiary_account");
          paramIntent = "vpa";
          String str = g;
          int k = paramIntent.equalsIgnoreCase(str);
          if (k == 0)
          {
            paramInt1 = R.string.collect_req_invalid_vpa;
            localObject1 = getString(paramInt1);
            a((String)localObject1, null);
            return;
          }
          localObject2 = i;
          if (localObject2 == null)
          {
            localObject2 = d;
            k = 1;
            localObject2 = (f)((p)localObject2).a(k);
            i = ((f)localObject2);
          }
          localObject2 = i;
          if (localObject2 != null)
          {
            ((f)localObject2).a((com.truecaller.truepay.app.ui.transaction.b.b)localObject1);
            return;
          }
          new String[1][0] = "PayBenfy frag is null";
          return;
        }
        localObject1 = paramIntent.getSerializableExtra("receiver_contact");
        if (localObject1 != null)
        {
          localObject1 = (com.truecaller.truepay.app.ui.transaction.b.a)paramIntent.getSerializableExtra("receiver_contact");
          localObject2 = b();
          if (localObject2 != null)
          {
            b().a((com.truecaller.truepay.app.ui.transaction.b.e)localObject1);
            return;
          }
          new String[1][0] = "PayContacts frag is null";
          return;
        }
        localObject1 = paramIntent.getSerializableExtra("invited_contact");
        if (localObject1 != null)
        {
          localObject1 = (com.truecaller.truepay.app.ui.transaction.b.f)paramIntent.getSerializableExtra("invited_contact");
          b().b((com.truecaller.truepay.app.ui.transaction.b.e)localObject1);
          return;
        }
        localObject1 = getResources();
        int m = R.string.error_selecting_contacts;
        localObject1 = ((Resources)localObject1).getString(m);
        a((String)localObject1, null);
      }
    }
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    e.b();
  }
  
  public final void onResume()
  {
    super.onResume();
    View localView = getView();
    if (localView != null)
    {
      int j = 2;
      t.a(localView, false, j);
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    Object localObject = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a().a(this);
    int j = R.id.vp_frag_collect_selection;
    paramBundle = (ViewPager)paramView.findViewById(j);
    a = paramBundle;
    j = R.id.tabs_frag_collect_selection;
    paramBundle = (TabLayout)paramView.findViewById(j);
    b = paramBundle;
    j = R.id.fl_loading_trnx_contact_selection;
    paramBundle = (FrameLayout)paramView.findViewById(j);
    c = paramBundle;
    j = R.id.layout_search_bar_contact_selection_fragment;
    paramBundle = paramView.findViewById(j);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$e$K-teCAPTyEy_zwKjI0dBtxH-H0Y;
    ((-..Lambda.e.K-teCAPTyEy_zwKjI0dBtxH-H0Y)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    j = R.id.iv_back_search_bar_contact_selection;
    paramView = paramView.findViewById(j);
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$e$n9D07LWYuzdH0UDLoE3xBsp9row;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    e.a(this);
    paramView = new com/truecaller/truepay/app/ui/transaction/views/adapters/p;
    paramBundle = getChildFragmentManager();
    localObject = g;
    paramView.<init>(paramBundle, (n)localObject);
    d = paramView;
    paramView = b;
    paramBundle = a;
    paramView.setupWithViewPager(paramBundle);
    paramView = a;
    paramBundle = d;
    paramView.setAdapter(paramBundle);
    a.setOffscreenPageLimit(2);
    String str = getArguments().getString("flow_from");
    paramView = f;
    paramBundle = com.truecaller.truepay.data.b.a.a();
    paramView.a(paramBundle);
    localObject = Truepay.getInstance().getAnalyticLoggerHelper();
    Boolean localBoolean = Boolean.valueOf(Truepay.getInstance().isRegistrationComplete());
    ((com.truecaller.truepay.data.b.a)localObject).a("app_payment_transaction_intent", str, "request_money", null, localBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */