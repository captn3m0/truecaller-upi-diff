package com.truecaller.truepay.app.ui.transaction.views.a;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.fragments.a;

public class c
  extends a
{
  c.a a;
  
  public static c c()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    c localc = new com/truecaller/truepay/app/ui/transaction/views/a/c;
    localc.<init>();
    localc.setArguments(localBundle);
    return localc;
  }
  
  private void d()
  {
    a.c("account");
    dismiss();
  }
  
  private void e()
  {
    a.c("vpa");
    dismiss();
  }
  
  public final int a()
  {
    return R.layout.fragment_beneficiary_type_selection;
  }
  
  public final void b()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = getTargetFragment();
      boolean bool = localObject1 instanceof c.a;
      if (bool)
      {
        localObject1 = (c.a)getTargetFragment();
        a = ((c.a)localObject1);
        return;
      }
    }
    localObject1 = new java/lang/RuntimeException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("parent fragment should implement ");
    String str = c.a.class.getSimpleName();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((RuntimeException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public void dismiss()
  {
    super.dismiss();
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    a = null;
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.tv_bank_acc_frag_benfy_type_selection;
    paramBundle = paramView.findViewById(i);
    Object localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$c$B9anovbdTSo2Tn_J83WLs_CafP4;
    ((-..Lambda.c.B9anovbdTSo2Tn_J83WLs_CafP4)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.iv_bank_acc_frag_benfy_type_selection;
    paramBundle = paramView.findViewById(i);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$c$FDIQFgjzAWxpKEKLDvTmcpPD5s8;
    ((-..Lambda.c.FDIQFgjzAWxpKEKLDvTmcpPD5s8)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.tv_upi_frag_benfy_type_selection;
    paramBundle = paramView.findViewById(i);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$c$QGUWwqppgGuem3LlRtCtP4ir5q8;
    ((-..Lambda.c.QGUWwqppgGuem3LlRtCtP4ir5q8)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.iv_upi_frag_benfy_type_selection;
    paramView = paramView.findViewById(i);
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$c$qrsCWnBXV3A7-UEbIN831h_NJF8;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */