package com.truecaller.truepay.app.ui.transaction.views.a;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.fragments.a;
import com.truecaller.truepay.data.d.c;

public class p
  extends a
{
  p.a a;
  
  public static p a(c paramc)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("pending_collect_request", paramc);
    paramc = new com/truecaller/truepay/app/ui/transaction/views/a/p;
    paramc.<init>();
    paramc.setArguments(localBundle);
    return paramc;
  }
  
  public final int a()
  {
    return R.layout.fragment_reject_confirmation_dialog;
  }
  
  public final void b()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = getTargetFragment();
      boolean bool = localObject1 instanceof p.a;
      if (bool)
      {
        localObject1 = (p.a)getTargetFragment();
        a = ((p.a)localObject1);
        return;
      }
    }
    localObject1 = new java/lang/RuntimeException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("parent fragment should implement ");
    String str = p.a.class.getSimpleName();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((RuntimeException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btn_reject_frag_reject_confirmation_dialog;
    paramBundle = paramView.findViewById(i);
    Object localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$p$KM7fCoZQ4FEEeFw-U1qBkKWkhBQ;
    ((-..Lambda.p.KM7fCoZQ4FEEeFw-U1qBkKWkhBQ)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.btn_mark_spam_frag_reject_confirmation_dialog;
    paramBundle = paramView.findViewById(i);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$p$yHiVjBEKv5_WvpdvQe3iNQ_SxkQ;
    ((-..Lambda.p.yHiVjBEKv5_WvpdvQe3iNQ_SxkQ)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.iv_close_frag_reject_confirmation_dialog;
    paramView = paramView.findViewById(i);
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$p$Nzwv54NZ6C0UohOJzRSnEOh5EFQ;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */