package com.truecaller.truepay.app.ui.transaction.views.a;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.utils.extensions.t;

final class b$1
  implements TextWatcher
{
  b$1(b paramb) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    t.a(a.l, false);
    t.a(a.n, false);
    a.h.setVisibility(0);
    a.p.setVisibility(8);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */