package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.k;

final class z
  extends c
{
  public z(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    k localk = new com/truecaller/truepay/app/ui/transaction/views/b/k;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_pending_collect_request;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localk.<init>(paramViewGroup, (f)localObject);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */