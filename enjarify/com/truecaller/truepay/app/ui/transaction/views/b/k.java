package com.truecaller.truepay.app.ui.transaction.views.b;

import android.view.View;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.base.views.c.a;

public final class k
  extends a
{
  public final TextView b;
  public final TextView c;
  public final TextView d;
  public final TextView e;
  
  public k(View paramView, f paramf)
  {
    super(paramView, paramf);
    int i = R.id.tv_amt_item_pending_collect_request;
    paramf = (TextView)paramView.findViewById(i);
    b = paramf;
    i = R.id.tv_name_direction_item_pending_collect_request;
    paramf = (TextView)paramView.findViewById(i);
    c = paramf;
    i = R.id.tv_expiry_item_pending_collect_request;
    paramf = (TextView)paramView.findViewById(i);
    d = paramf;
    i = R.id.tv_created_at_date_item_pending_collect_request;
    paramf = (TextView)paramView.findViewById(i);
    e = paramf;
    i = R.id.btn_reject_item_pending_collect_request;
    paramf = paramView.findViewById(i);
    -..Lambda.k._Owh7S_TESAHAGMQywEEsZJ1Hc4 local_Owh7S_TESAHAGMQywEEsZJ1Hc4 = new com/truecaller/truepay/app/ui/transaction/views/b/-$$Lambda$k$_Owh7S_TESAHAGMQywEEsZJ1Hc4;
    local_Owh7S_TESAHAGMQywEEsZJ1Hc4.<init>(this);
    paramf.setOnClickListener(local_Owh7S_TESAHAGMQywEEsZJ1Hc4);
    i = R.id.btn_accept_item_pending_collect_request;
    paramView = paramView.findViewById(i);
    paramf = new com/truecaller/truepay/app/ui/transaction/views/b/-$$Lambda$k$FtvNakOIVEM0xVtCMe4AydvZFg4;
    paramf.<init>(this);
    paramView.setOnClickListener(paramf);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */