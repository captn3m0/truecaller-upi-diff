package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.p;
import com.truecaller.truepay.app.ui.transaction.views.b.q;
import java.util.List;

final class am
  extends c
{
  public am(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    q localq = new com/truecaller/truepay/app/ui/transaction/views/b/q;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_search_result_resolved_vpa_header;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localq.<init>(paramViewGroup, (f)localObject);
    return localq;
  }
  
  public final boolean a(Object paramObject, List paramList)
  {
    return paramObject instanceof p;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */