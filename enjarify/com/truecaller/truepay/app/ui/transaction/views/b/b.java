package com.truecaller.truepay.app.ui.transaction.views.b;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.base.views.c.a;
import com.truecaller.truepay.app.ui.transaction.views.adapters.h;

public final class b
  extends a
  implements View.OnClickListener
{
  public TextView b;
  public TextView c;
  public ImageView d;
  
  public b(View paramView, f paramf)
  {
    super(paramView, paramf);
    int i = R.id.tv_name_item_benfy_bank_acc;
    paramf = (TextView)paramView.findViewById(i);
    b = paramf;
    i = R.id.tv_acc_number_item_benfy_bank_acc;
    paramf = (TextView)paramView.findViewById(i);
    c = paramf;
    i = R.id.iv_bank_logo_item_benfy_bank_acc;
    paramf = (ImageView)paramView.findViewById(i);
    d = paramf;
    i = R.id.btn_delete_item_benfy_bank_acc;
    paramf = paramView.findViewById(i);
    if (paramf != null)
    {
      -..Lambda.b.E58SrVwqn_6Rkm7PWMNlwkDeL5Y localE58SrVwqn_6Rkm7PWMNlwkDeL5Y = new com/truecaller/truepay/app/ui/transaction/views/b/-$$Lambda$b$E58SrVwqn_6Rkm7PWMNlwkDeL5Y;
      localE58SrVwqn_6Rkm7PWMNlwkDeL5Y.<init>(this);
      paramf.setOnClickListener(localE58SrVwqn_6Rkm7PWMNlwkDeL5Y);
    }
    paramView.setOnClickListener(this);
  }
  
  public final void onClick(View paramView)
  {
    paramView = (h)a;
    int i = getAdapterPosition();
    paramView.d(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */