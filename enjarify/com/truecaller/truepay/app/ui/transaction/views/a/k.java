package com.truecaller.truepay.app.ui.transaction.views.a;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.views.customviews.AddShortcutView;
import com.truecaller.truepay.app.ui.payments.views.customviews.AddShortcutView.a;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.c.p.1;
import com.truecaller.truepay.app.ui.transaction.views.c.h;
import com.truecaller.truepay.app.utils.ao;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.data.e.e;
import com.truecaller.utils.extensions.t;
import io.reactivex.o;
import io.reactivex.q;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class k
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements h
{
  public e A;
  public ao B;
  private ProgressDialog C;
  private boolean D;
  private Interpolator E;
  private com.truecaller.truepay.app.ui.transaction.views.c.l F;
  private com.truecaller.truepay.app.ui.transaction.b.p G;
  private com.truecaller.truepay.app.ui.transaction.b.l H;
  private DecimalFormat I;
  ImageView a;
  LinearLayout b;
  RelativeLayout c;
  ImageView d;
  TextView e;
  TextView f;
  TextView g;
  ImageView h;
  TextView i;
  TextView j;
  TextView k;
  RelativeLayout l;
  TextView n;
  TextView o;
  TextView p;
  TextView q;
  Button r;
  Button s;
  AddShortcutView t;
  CardView u;
  public au v;
  public r w;
  public ax x;
  public com.truecaller.truepay.app.ui.transaction.c.p y;
  public com.truecaller.truepay.app.utils.a z;
  
  public k()
  {
    DecimalFormat localDecimalFormat = new java/text/DecimalFormat;
    localDecimalFormat.<init>("#,###.##");
    I = localDecimalFormat;
  }
  
  private static int a(View paramView)
  {
    float f1 = paramView.getMeasuredHeight();
    float f2 = getContextgetResourcesgetDisplayMetricsdensity;
    return (int)(f1 / f2);
  }
  
  public static k a(com.truecaller.truepay.app.ui.transaction.b.p paramp)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("response", paramp);
    paramp = new com/truecaller/truepay/app/ui/transaction/views/a/k;
    paramp.<init>();
    paramp.setArguments(localBundle);
    return paramp;
  }
  
  private void a(int paramInt)
  {
    int m = Build.VERSION.SDK_INT;
    int i1 = 21;
    if (m >= i1)
    {
      Window localWindow = getActivity().getWindow();
      localWindow.setStatusBarColor(paramInt);
    }
  }
  
  private void c(String paramString)
  {
    int m = paramString.hashCode();
    int i1 = 1;
    boolean bool5;
    boolean bool7;
    switch (m)
    {
    default: 
      break;
    case 1661903715: 
      localObject1 = "action.page.need_help";
      boolean bool2 = paramString.equals(localObject1);
      if (bool2) {
        int i2 = 6;
      }
      break;
    case 970112032: 
      localObject1 = "action.share_receipt";
      boolean bool3 = paramString.equals(localObject1);
      if (bool3) {
        int i3 = 5;
      }
      break;
    case 872695663: 
      localObject1 = "action.page.forgot_upi_pin";
      boolean bool4 = paramString.equals(localObject1);
      if (bool4) {
        int i4 = 2;
      }
      break;
    case -399989823: 
      localObject1 = "action.check_status";
      bool5 = paramString.equals(localObject1);
      if (bool5)
      {
        bool5 = false;
        paramString = null;
      }
      break;
    case -721197651: 
      localObject1 = "action.page.reset_upi_pin";
      bool5 = paramString.equals(localObject1);
      if (bool5) {
        int i5 = 4;
      }
      break;
    case -875490522: 
      localObject1 = "action.page.home";
      boolean bool6 = paramString.equals(localObject1);
      if (bool6) {
        int i6 = 7;
      }
      break;
    case -1361457855: 
      localObject1 = "action.page.retry";
      bool7 = paramString.equals(localObject1);
      if (bool7) {
        bool7 = true;
      }
      break;
    case -1912660920: 
      localObject1 = "action.page.create_upi_pin";
      bool7 = paramString.equals(localObject1);
      if (bool7) {
        i7 = 3;
      }
      break;
    }
    int i7 = -1;
    switch (i7)
    {
    default: 
      getActivity().finish();
      return;
    case 6: 
      paramString = F;
      localObject1 = A.a();
      paramString.showNeedHelp((String)localObject1);
      return;
    case 5: 
      j();
      return;
    case 4: 
      paramString = F;
      localObject1 = G.k;
      paramString.showResetPin((com.truecaller.truepay.data.api.model.a)localObject1, "other");
      return;
    case 2: 
    case 3: 
      paramString = F;
      localObject1 = G.k;
      paramString.showSetPin((com.truecaller.truepay.data.api.model.a)localObject1, "other");
      return;
    case 1: 
      com.truecaller.truepay.app.ui.transaction.a.a = i1;
      F.onBackPressed();
      return;
    }
    paramString = "";
    Object localObject1 = getResources();
    i1 = R.string.check_status_text;
    localObject1 = ((Resources)localObject1).getString(i1);
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      localObject2 = C;
      ((ProgressDialog)localObject2).setTitle(paramString);
    }
    C.setMessage((CharSequence)localObject1);
    C.show();
    paramString = y;
    localObject1 = G.a;
    Object localObject2 = new com/truecaller/truepay/app/ui/history/models/d;
    ((com.truecaller.truepay.app.ui.history.models.d)localObject2).<init>((String)localObject1);
    localObject1 = a.a((com.truecaller.truepay.app.ui.history.models.d)localObject2);
    localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((io.reactivex.n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((o)localObject1).a((io.reactivex.n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/transaction/c/p$1;
    ((p.1)localObject2).<init>(paramString);
    ((o)localObject1).a((q)localObject2);
  }
  
  private void i()
  {
    Truepay.getInstance().getAnalyticLoggerHelper().g("payment");
    Truepay.getInstance().getListener().createShortcut(3);
  }
  
  private void j()
  {
    Object localObject1 = getContext();
    Object localObject2 = "android.permission.WRITE_EXTERNAL_STORAGE";
    int m = android.support.v4.app.a.a((Context)localObject1, (String)localObject2);
    boolean bool2;
    if (m != 0)
    {
      localObject1 = getActivity();
      localObject2 = "android.permission.WRITE_EXTERNAL_STORAGE";
      boolean bool1 = android.support.v4.app.a.a((Activity)localObject1, (String)localObject2);
      if (bool1)
      {
        int i1 = R.string.read_phone_permission;
        localObject1 = getString(i1);
        bool2 = false;
        localObject2 = null;
        a((String)localObject1, null);
      }
      localObject1 = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
      requestPermissions((String[])localObject1, 1005);
      return;
    }
    localObject1 = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject1).<init>("yyyy-MM-dd HH:mm:ss");
    localObject2 = G;
    Object localObject3 = new java/util/Date;
    ((Date)localObject3).<init>();
    localObject1 = ((SimpleDateFormat)localObject1).format((Date)localObject3);
    l = ((String)localObject1);
    localObject1 = z.d();
    localObject2 = getActivity();
    if (localObject2 != null)
    {
      localObject2 = G;
      if (localObject2 != null)
      {
        bool2 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool2)
        {
          localObject2 = v;
          if (localObject2 != null)
          {
            localObject2 = new com/truecaller/truepay/app/ui/history/views/c/d;
            localObject3 = getContext();
            com.truecaller.truepay.app.ui.transaction.b.p localp = G;
            au localau = v;
            ((com.truecaller.truepay.app.ui.history.views.c.d)localObject2).<init>((Context)localObject3, localp, (String)localObject1, localau);
            ((com.truecaller.truepay.app.ui.history.views.c.d)localObject2).a();
          }
        }
      }
    }
  }
  
  private void k()
  {
    Object localObject = y;
    if (localObject != null)
    {
      localObject = e;
      boolean bool = b;
      if (!bool)
      {
        localObject = y.e;
        ((io.reactivex.a.a)localObject).a();
      }
    }
  }
  
  public final int a()
  {
    Object localObject = getArguments();
    String str1 = "response";
    localObject = (com.truecaller.truepay.app.ui.transaction.b.p)((Bundle)localObject).getSerializable(str1);
    G = ((com.truecaller.truepay.app.ui.transaction.b.p)localObject);
    localObject = G;
    boolean bool;
    if (localObject != null)
    {
      localObject = j;
    }
    else
    {
      bool = false;
      localObject = null;
    }
    H = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject);
    localObject = H;
    if (localObject != null) {
      localObject = b;
    } else {
      localObject = "";
    }
    int m = -1;
    int i1 = ((String)localObject).hashCode();
    int i2 = -1867169789;
    String str2;
    if (i1 != i2)
    {
      i2 = -1086574198;
      if (i1 != i2)
      {
        i2 = -682587753;
        if (i1 == i2)
        {
          str2 = "pending";
          bool = ((String)localObject).equals(str2);
          if (bool) {
            m = 2;
          }
        }
      }
      else
      {
        str2 = "failure";
        bool = ((String)localObject).equals(str2);
        if (bool) {
          m = 1;
        }
      }
    }
    else
    {
      str2 = "success";
      bool = ((String)localObject).equals(str2);
      if (bool)
      {
        m = 0;
        str1 = null;
      }
    }
    switch (m)
    {
    default: 
      return R.layout.fragment_confirmation_pending;
    case 2: 
      return R.layout.fragment_confirmation_pending;
    case 1: 
      return R.layout.fragment_confirmation_failure;
    }
    return R.layout.fragment_confirmation_success;
  }
  
  public final void a(String paramString)
  {
    boolean bool = isAdded();
    if (bool)
    {
      C.dismiss();
      int m = R.string.check_status_result_confirmation;
      String str = getString(m);
      int i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      arrayOfObject[0] = paramString;
      paramString = String.format(str, arrayOfObject);
      m = 0;
      str = null;
      a(paramString, null);
    }
  }
  
  public final void b()
  {
    Object localObject1 = getView();
    if (localObject1 == null) {
      return;
    }
    int m = R.id.expand_icon;
    Object localObject2 = (ImageView)((View)localObject1).findViewById(m);
    a = ((ImageView)localObject2);
    m = R.id.bank_expanded_section;
    localObject2 = (LinearLayout)((View)localObject1).findViewById(m);
    b = ((LinearLayout)localObject2);
    m = R.id.message_section;
    localObject2 = (RelativeLayout)((View)localObject1).findViewById(m);
    c = ((RelativeLayout)localObject2);
    m = R.id.beneficiary_icon;
    localObject2 = (ImageView)((View)localObject1).findViewById(m);
    d = ((ImageView)localObject2);
    m = R.id.text_amount;
    localObject2 = (TextView)((View)localObject1).findViewById(m);
    e = ((TextView)localObject2);
    m = R.id.beneficiary_name;
    localObject2 = (TextView)((View)localObject1).findViewById(m);
    f = ((TextView)localObject2);
    m = R.id.beneficiary_subname;
    localObject2 = (TextView)((View)localObject1).findViewById(m);
    g = ((TextView)localObject2);
    m = R.id.bank_icon;
    localObject2 = (ImageView)((View)localObject1).findViewById(m);
    h = ((ImageView)localObject2);
    m = R.id.acc_details;
    localObject2 = (TextView)((View)localObject1).findViewById(m);
    i = ((TextView)localObject2);
    m = R.id.bank_message;
    localObject2 = (TextView)((View)localObject1).findViewById(m);
    j = ((TextView)localObject2);
    m = R.id.transaction_id_text;
    localObject2 = (TextView)((View)localObject1).findViewById(m);
    k = ((TextView)localObject2);
    m = R.id.transaction_id_section;
    localObject2 = (RelativeLayout)((View)localObject1).findViewById(m);
    l = ((RelativeLayout)localObject2);
    m = R.id.remarks_text;
    localObject2 = (TextView)((View)localObject1).findViewById(m);
    n = ((TextView)localObject2);
    m = R.id.payment_response;
    localObject2 = (TextView)((View)localObject1).findViewById(m);
    o = ((TextView)localObject2);
    m = R.id.payment_status;
    localObject2 = (TextView)((View)localObject1).findViewById(m);
    p = ((TextView)localObject2);
    m = R.id.bank_text;
    localObject2 = (TextView)((View)localObject1).findViewById(m);
    q = ((TextView)localObject2);
    m = R.id.action_left_button_text;
    localObject2 = (Button)((View)localObject1).findViewById(m);
    r = ((Button)localObject2);
    m = R.id.action_right_button_text;
    localObject2 = (Button)((View)localObject1).findViewById(m);
    s = ((Button)localObject2);
    m = R.id.addShortcutView;
    localObject2 = (AddShortcutView)((View)localObject1).findViewById(m);
    t = ((AddShortcutView)localObject2);
    m = R.id.cardViewAddShortcut;
    localObject2 = (CardView)((View)localObject1).findViewById(m);
    u = ((CardView)localObject2);
    localObject2 = a;
    Object localObject3 = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$k$DFzTRMo5_xOolIVNPmkvGx02GP4;
    ((-..Lambda.k.DFzTRMo5_xOolIVNPmkvGx02GP4)localObject3).<init>(this);
    ((ImageView)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    localObject2 = s;
    localObject3 = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$k$zmyfYcaukDwS-xS6lKtgu3euDYI;
    ((-..Lambda.k.zmyfYcaukDwS-xS6lKtgu3euDYI)localObject3).<init>(this);
    ((Button)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    localObject2 = r;
    localObject3 = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$k$HSa81efQhP6I242zhEHIIMFhnNs;
    ((-..Lambda.k.HSa81efQhP6I242zhEHIIMFhnNs)localObject3).<init>(this);
    ((Button)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    m = R.id.transaction_id_copy_icon;
    localObject1 = ((View)localObject1).findViewById(m);
    localObject2 = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$k$v9EH0iV0HQ3OqCRtucSXainPxss;
    ((-..Lambda.k.v9EH0iV0HQ3OqCRtucSXainPxss)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = t;
    localObject2 = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$k$UUwpTanidVjl767c01K2WNUZhiI;
    ((-..Lambda.k.UUwpTanidVjl767c01K2WNUZhiI)localObject2).<init>(this);
    ((AddShortcutView)localObject1).setAddClickListener((AddShortcutView.a)localObject2);
    localObject1 = G;
    if (localObject1 != null)
    {
      localObject1 = e;
      m = R.string.amount_string_transaction;
      int i1 = 1;
      Object localObject4 = new Object[i1];
      Object localObject5 = I;
      double d1 = Double.parseDouble(G.e);
      localObject5 = ((DecimalFormat)localObject5).format(d1);
      localObject4[0] = localObject5;
      localObject2 = getString(m, (Object[])localObject4);
      ((TextView)localObject1).setText((CharSequence)localObject2);
      localObject1 = G.j;
      if (localObject1 != null)
      {
        localObject1 = k;
        localObject2 = G.j.a;
        ((TextView)localObject1).setText((CharSequence)localObject2);
      }
      else
      {
        localObject1 = k;
        localObject2 = G.a;
        ((TextView)localObject1).setText((CharSequence)localObject2);
      }
      localObject1 = k.getText();
      boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
      m = 8;
      if (bool1)
      {
        localObject1 = l;
        ((RelativeLayout)localObject1).setVisibility(m);
      }
      localObject1 = f;
      if (localObject1 != null)
      {
        localObject4 = G.h.h;
        ((TextView)localObject1).setText((CharSequence)localObject4);
      }
      localObject1 = i;
      if (localObject1 != null)
      {
        localObject4 = G.k.j.b;
        localObject5 = G.k.c;
        localObject4 = au.a((String)localObject4, (String)localObject5);
        ((TextView)localObject1).setText((CharSequence)localObject4);
      }
      localObject1 = g;
      if (localObject1 != null)
      {
        localObject1 = G.h;
        if (localObject1 != null)
        {
          localObject1 = G.h.f;
          bool1 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool1)
          {
            localObject1 = g;
            localObject4 = G.h.f;
            ((TextView)localObject1).setText((CharSequence)localObject4);
          }
          else
          {
            localObject1 = G.h.e;
            bool1 = TextUtils.isEmpty((CharSequence)localObject1);
            if (!bool1)
            {
              localObject1 = g;
              localObject4 = G.h.e;
              ((TextView)localObject1).setText((CharSequence)localObject4);
            }
          }
        }
      }
      localObject1 = h;
      if (localObject1 != null)
      {
        localObject4 = w;
        localObject5 = G.k.j.d;
        localObject4 = ((r)localObject4).b((String)localObject5);
        ((ImageView)localObject1).setImageDrawable((Drawable)localObject4);
      }
      localObject1 = G.h;
      Object localObject6;
      int i4;
      if (localObject1 != null)
      {
        localObject1 = G.h.h;
        bool1 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool1)
        {
          localObject1 = w;
          localObject4 = G.h.j;
          localObject5 = d;
          localObject6 = getContext().getResources();
          i4 = R.drawable.ic_avatar_common;
          localObject6 = ((Resources)localObject6).getDrawable(i4);
          Object localObject7 = getContext().getResources();
          int i5 = R.drawable.ic_avatar_common;
          localObject7 = ((Resources)localObject7).getDrawable(i5);
          ((r)localObject1).a((String)localObject4, (ImageView)localObject5, (Drawable)localObject6, (Drawable)localObject7);
        }
      }
      localObject1 = c;
      if (localObject1 != null)
      {
        localObject1 = G.i;
        bool1 = TextUtils.isEmpty((CharSequence)localObject1);
        if (bool1)
        {
          localObject1 = c;
          ((RelativeLayout)localObject1).setVisibility(m);
        }
        else
        {
          localObject1 = n;
          localObject4 = G.i;
          ((TextView)localObject1).setText((CharSequence)localObject4);
        }
      }
      localObject1 = o;
      if (localObject1 != null)
      {
        localObject1 = H;
        if (localObject1 != null)
        {
          localObject1 = f;
          i6 = 2;
          if (localObject1 != null)
          {
            localObject1 = H.f;
            int i2 = ((List)localObject1).size();
            if (i2 == i6)
            {
              r.setVisibility(0);
              s.setVisibility(0);
              localObject1 = r;
              localObject5 = H.f.get(i1)).a;
              ((Button)localObject1).setText((CharSequence)localObject5);
              localObject1 = s;
              localObject5 = H.f.get(0)).a;
              ((Button)localObject1).setText((CharSequence)localObject5);
            }
            else
            {
              localObject1 = H.f;
              i2 = ((List)localObject1).size();
              if (i2 == i1)
              {
                r.setVisibility(m);
                s.setVisibility(0);
                localObject1 = s;
                localObject5 = H.f.get(0)).a;
                ((Button)localObject1).setText((CharSequence)localObject5);
              }
              else
              {
                localObject1 = r;
                i7 = 4;
                ((Button)localObject1).setVisibility(i7);
              }
            }
          }
          else
          {
            localObject1 = s;
            ((Button)localObject1).setVisibility(0);
          }
          localObject1 = H.d;
          boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool2)
          {
            localObject1 = o;
            localObject5 = H.d;
            ((TextView)localObject1).setText((CharSequence)localObject5);
          }
          localObject1 = H.b;
          int i7 = -1;
          int i8 = ((String)localObject1).hashCode();
          i4 = -1867169789;
          if (i8 != i4)
          {
            i4 = -1086574198;
            if (i8 != i4)
            {
              i4 = -682587753;
              if (i8 == i4)
              {
                localObject6 = "pending";
                bool2 = ((String)localObject1).equals(localObject6);
                if (bool2) {
                  break label1461;
                }
              }
            }
            else
            {
              localObject4 = "failure";
              bool2 = ((String)localObject1).equals(localObject4);
              if (bool2)
              {
                i6 = 1;
                break label1461;
              }
            }
          }
          else
          {
            localObject4 = "success";
            bool2 = ((String)localObject1).equals(localObject4);
            if (bool2)
            {
              i6 = 0;
              localObject4 = null;
              break label1461;
            }
          }
          i6 = -1;
          switch (i6)
          {
          default: 
            localObject1 = getResources();
            i6 = R.color.red_color;
            i3 = ((Resources)localObject1).getColor(i6);
            a(i3);
            break;
          case 2: 
            localObject1 = getResources();
            i6 = R.color.orange_color;
            i3 = ((Resources)localObject1).getColor(i6);
            a(i3);
            break;
          case 1: 
            localObject1 = getResources();
            i6 = R.color.red_color;
            i3 = ((Resources)localObject1).getColor(i6);
            a(i3);
            break;
          case 0: 
            label1461:
            localObject1 = getResources();
            i6 = R.color.light_green_color;
            i3 = ((Resources)localObject1).getColor(i6);
            a(i3);
            break;
          }
        }
      }
      localObject1 = getResources();
      int i6 = R.color.red_color;
      int i3 = ((Resources)localObject1).getColor(i6);
      a(i3);
      localObject1 = G.d;
      localObject4 = "create_collect";
      boolean bool3 = ((String)localObject1).equalsIgnoreCase((String)localObject4);
      if (bool3)
      {
        localObject1 = H;
        if (localObject1 != null)
        {
          l.setVisibility(m);
          localObject1 = "success";
          localObject4 = H.b;
          bool3 = ((String)localObject1).equalsIgnoreCase((String)localObject4);
          if (bool3)
          {
            localObject1 = o;
            i6 = R.string.confirmation_request_messge;
            localObject4 = getString(i6);
            localObject3 = new Object[i1];
            localObject5 = G.h.e;
            localObject3[0] = localObject5;
            localObject3 = String.format((String)localObject4, (Object[])localObject3);
            ((TextView)localObject1).setText((CharSequence)localObject3);
            localObject1 = p;
            i1 = R.string.frag_pay_confirmation_request_successful_title;
            ((TextView)localObject1).setText(i1);
            localObject1 = q;
            if (localObject1 != null)
            {
              localObject3 = getResources();
              i6 = R.string.credit_to;
              localObject3 = ((Resources)localObject3).getString(i6);
              ((TextView)localObject1).setText((CharSequence)localObject3);
            }
          }
          else
          {
            localObject1 = o;
            localObject3 = H.d;
            ((TextView)localObject1).setText((CharSequence)localObject3);
            localObject1 = p;
            i1 = R.string.frag_pay_confirmation_request_failure_title;
            ((TextView)localObject1).setText(i1);
          }
          localObject1 = G.h;
          if (localObject1 != null)
          {
            localObject1 = G.h.h;
            bool3 = TextUtils.isEmpty((CharSequence)localObject1);
            if (!bool3)
            {
              localObject1 = f;
              localObject3 = G.h.h;
              ((TextView)localObject1).setText((CharSequence)localObject3);
            }
          }
          localObject1 = g;
          if (localObject1 != null)
          {
            localObject3 = G.h.e;
            ((TextView)localObject1).setText((CharSequence)localObject3);
          }
          localObject1 = a;
          if (localObject1 == null) {
            break label2288;
          }
          ((ImageView)localObject1).setVisibility(m);
          break label2288;
        }
      }
      localObject1 = G.d;
      localObject3 = "collect_request_pay";
      bool3 = ((String)localObject1).equalsIgnoreCase((String)localObject3);
      if (bool3)
      {
        localObject1 = G.h;
        if (localObject1 != null)
        {
          localObject1 = G.h.h;
          bool3 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool3)
          {
            localObject1 = f;
            localObject2 = G.h.h;
            ((TextView)localObject1).setText((CharSequence)localObject2);
            localObject1 = g;
            if (localObject1 != null)
            {
              localObject2 = G.h.e;
              ((TextView)localObject1).setText((CharSequence)localObject2);
            }
          }
          else
          {
            localObject1 = f;
            localObject3 = G.h.e;
            ((TextView)localObject1).setText((CharSequence)localObject3);
            localObject1 = g;
            if (localObject1 != null) {
              ((TextView)localObject1).setVisibility(m);
            }
          }
        }
      }
      else
      {
        localObject1 = G.d;
        localObject2 = "pay_direct";
        bool3 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (!bool3)
        {
          localObject1 = G.d;
          localObject2 = "qr_pay";
          bool3 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
          if (!bool3) {}
        }
        else
        {
          localObject1 = G.h;
          if (localObject1 != null)
          {
            localObject1 = G.h.e;
            bool3 = TextUtils.isEmpty((CharSequence)localObject1);
            if (!bool3)
            {
              localObject1 = g;
              localObject2 = G.h.e;
              ((TextView)localObject1).setText((CharSequence)localObject2);
            }
            else
            {
              localObject1 = G.h.a;
              bool3 = TextUtils.isEmpty((CharSequence)localObject1);
              if (!bool3)
              {
                localObject1 = g;
                localObject2 = au.a(G.h.a);
                ((TextView)localObject1).setText((CharSequence)localObject2);
              }
              else
              {
                localObject1 = G.h.c;
                bool3 = TextUtils.isEmpty((CharSequence)localObject1);
                if (!bool3)
                {
                  localObject1 = g;
                  localObject2 = au.a(G.h.c);
                  ((TextView)localObject1).setText((CharSequence)localObject2);
                }
              }
            }
          }
        }
      }
      label2288:
      localObject1 = H;
      if (localObject1 != null)
      {
        localObject2 = "pending";
        localObject1 = b;
        bool3 = ((String)localObject2).equalsIgnoreCase((String)localObject1);
        if (bool3)
        {
          localObject1 = y;
          localObject2 = G.a;
          ((com.truecaller.truepay.app.ui.transaction.c.p)localObject1).a((String)localObject2);
        }
        else
        {
          localObject1 = y;
          localObject2 = G;
          ((com.truecaller.truepay.app.ui.transaction.c.p)localObject1).a((com.truecaller.truepay.app.ui.transaction.b.p)localObject2);
        }
        localObject1 = G;
        bool3 = m;
        if (bool3)
        {
          localObject1 = G;
          localObject2 = new android/os/Handler;
          ((Handler)localObject2).<init>();
          localObject3 = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$k$nZ8cn990h569zoaLqDeezpn0ntE;
          ((-..Lambda.k.nZ8cn990h569zoaLqDeezpn0ntE)localObject3).<init>(this, (com.truecaller.truepay.app.ui.transaction.b.p)localObject1);
          long l1 = 2000L;
          ((Handler)localObject2).postDelayed((Runnable)localObject3, l1);
        }
      }
    }
  }
  
  public final void b(String paramString)
  {
    boolean bool = isAdded();
    if (bool)
    {
      C.dismiss();
      bool = false;
      a(paramString, null);
    }
  }
  
  public final void c()
  {
    t.b(u);
  }
  
  public final void d()
  {
    t.a(u);
  }
  
  public final void e()
  {
    Object localObject = G;
    if (localObject != null) {
      localObject = j;
    } else {
      localObject = null;
    }
    H = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject);
    localObject = H;
    if (localObject != null)
    {
      b = "success";
      com.truecaller.truepay.app.ui.transaction.b.p localp = G;
      j = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject);
      localObject = y;
      ((com.truecaller.truepay.app.ui.transaction.c.p)localObject).a(localp);
    }
  }
  
  public final void f()
  {
    k();
  }
  
  public final void g()
  {
    k();
    Object localObject1 = G;
    if (localObject1 != null) {
      localObject1 = j;
    } else {
      localObject1 = null;
    }
    H = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject1);
    localObject1 = H;
    if (localObject1 != null)
    {
      b = "success";
      int m = R.string.default_success_message;
      Object localObject2 = getString(m);
      d = ((String)localObject2);
      localObject1 = G;
      localObject2 = H;
      j = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject2);
      F.showPayConfirmation((com.truecaller.truepay.app.ui.transaction.b.p)localObject1);
      localObject1 = y;
      localObject2 = G;
      ((com.truecaller.truepay.app.ui.transaction.c.p)localObject1).a((com.truecaller.truepay.app.ui.transaction.b.p)localObject2);
    }
  }
  
  public final void h()
  {
    k();
    Object localObject1 = G;
    if (localObject1 != null) {
      localObject1 = j;
    } else {
      localObject1 = null;
    }
    H = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject1);
    localObject1 = H;
    if (localObject1 != null)
    {
      b = "failure";
      int m = R.string.default_failure_message;
      Object localObject2 = getString(m);
      d = ((String)localObject2);
      localObject1 = G;
      localObject2 = H;
      j = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject2);
      F.showPayConfirmation((com.truecaller.truepay.app.ui.transaction.b.p)localObject1);
      localObject1 = y;
      localObject2 = G;
      ((com.truecaller.truepay.app.ui.transaction.c.p)localObject1).a((com.truecaller.truepay.app.ui.transaction.b.p)localObject2);
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.transaction.views.c.l;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.transaction.views.c.l)getActivity();
      F = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement TransactionView");
    throw paramContext;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    a.a locala = com.truecaller.truepay.app.ui.transaction.a.a.a();
    com.truecaller.truepay.app.a.a.a locala1 = Truepay.getApplicationComponent();
    locala.a(locala1).a().a(this);
    super.onCreate(paramBundle);
  }
  
  public void onDetach()
  {
    super.onDetach();
    F = null;
    y.b();
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int m = 1005;
    if (paramInt == m)
    {
      paramInt = paramArrayOfInt.length;
      m = 1;
      if (paramInt == m)
      {
        str = null;
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0)
        {
          j();
          return;
        }
      }
      paramInt = R.string.external_directory_permission_denied;
      String str = getString(paramInt);
      m = 0;
      paramArrayOfString = null;
      a(str, null);
    }
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = android.support.v4.view.b.f.a(0.77F, 0.0F, 0.175F, 1.0F);
    E = ((Interpolator)localObject);
    y.a(this);
    localObject = new android/app/ProgressDialog;
    android.support.v4.app.f localf = getActivity();
    ((ProgressDialog)localObject).<init>(localf);
    C = ((ProgressDialog)localObject);
    com.truecaller.truepay.app.ui.dashboard.a.b = true;
    t.a(paramView, false, 2);
    super.onViewCreated(paramView, paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */