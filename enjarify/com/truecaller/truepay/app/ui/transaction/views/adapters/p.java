package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.m;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.views.a.f;
import com.truecaller.truepay.app.ui.transaction.views.a.l;
import com.truecaller.utils.n;

public final class p
  extends m
{
  private l a;
  private f b;
  private n c;
  
  public p(j paramj, n paramn)
  {
    super(paramj);
    c = paramn;
  }
  
  public final Fragment a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      localObject1 = new String[1];
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("invalid position selected for ");
      String str = getClass().getSimpleName();
      ((StringBuilder)localObject2).append(str);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject1[0] = localObject2;
      return l.a(0);
    case 1: 
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject1 = f.d();
        b = ((f)localObject1);
      }
      return b;
    }
    Object localObject1 = a;
    if (localObject1 == null)
    {
      paramInt = 1003;
      localObject1 = l.a(paramInt);
      a = ((l)localObject1);
    }
    return a;
  }
  
  public final int getCount()
  {
    return 2;
  }
  
  public final CharSequence getPageTitle(int paramInt)
  {
    Object[] arrayOfObject = null;
    switch (paramInt)
    {
    default: 
      localObject1 = new String[1];
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("invalid position selected for ");
      String str = getClass().getSimpleName();
      ((StringBuilder)localObject2).append(str);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject1[0] = localObject2;
      return "";
    case 1: 
      localObject1 = c;
      i = R.string.upi_id_title;
      arrayOfObject = new Object[0];
      return ((n)localObject1).a(i, arrayOfObject);
    }
    Object localObject1 = c;
    int i = R.string.contacts_caps;
    arrayOfObject = new Object[0];
    return ((n)localObject1).a(i, arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */