package com.truecaller.truepay.app.ui.transaction.views.a;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

final class k$2
  extends Animation
{
  k$2(k paramk, View paramView, int paramInt) {}
  
  protected final void applyTransformation(float paramFloat, Transformation paramTransformation)
  {
    float f = 1.0F;
    boolean bool = paramFloat < f;
    if (!bool)
    {
      a.setVisibility(8);
      return;
    }
    paramTransformation = a.getLayoutParams();
    int i = b;
    int j = (int)(i * paramFloat);
    i -= j;
    height = i;
    a.requestLayout();
  }
  
  public final boolean willChangeBounds()
  {
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.k.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */