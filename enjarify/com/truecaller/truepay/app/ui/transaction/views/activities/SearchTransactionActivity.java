package com.truecaller.truepay.app.ui.transaction.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.util.DiffUtil;
import android.support.v7.util.DiffUtil.Callback;
import android.support.v7.util.DiffUtil.DiffResult;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.a.a.c;
import com.truecaller.truepay.app.a.a.c.a;
import com.truecaller.truepay.app.a.a.d;
import com.truecaller.truepay.app.a.b.cp;
import com.truecaller.truepay.app.ui.transaction.c.z;
import com.truecaller.truepay.app.ui.transaction.views.adapters.ag;
import com.truecaller.truepay.app.ui.transaction.views.adapters.ah;
import com.truecaller.truepay.app.ui.transaction.views.adapters.ai;
import com.truecaller.truepay.app.ui.transaction.views.c.k;
import dagger.a.g;
import java.util.List;

public class SearchTransactionActivity
  extends com.truecaller.truepay.app.ui.base.views.a.b
  implements ah, k
{
  EditText a;
  View b;
  RecyclerView c;
  public z d;
  ag e;
  
  public final void a()
  {
    int i = R.string.search_failure_desc;
    String str = getString(i);
    Toast.makeText(this, str, 0).show();
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.putExtra("invited_contact", parama);
    setResult(-1, localIntent);
    finish();
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    Object localObject1 = d;
    Object localObject2 = getIntent();
    String str = "search_type";
    localObject2 = ((Intent)localObject2).getStringExtra(str);
    boolean bool = ((z)localObject1).a(paramb, (String)localObject2);
    if (bool)
    {
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>();
      localObject2 = "beneficiary_account";
      ((Intent)localObject1).putExtra((String)localObject2, paramb);
      int i = -1;
      setResult(i, (Intent)localObject1);
      finish();
    }
  }
  
  public final void a(List paramList)
  {
    boolean bool = paramList.isEmpty();
    int j = 1;
    List localList = null;
    if (bool)
    {
      localObject1 = new View[j];
      localObject2 = b;
      localObject1[0] = localObject2;
      setVisibility(0, (View[])localObject1);
    }
    else
    {
      int i = 8;
      localObject2 = new View[j];
      View localView = b;
      localObject2[0] = localView;
      setVisibility(i, (View[])localObject2);
    }
    Object localObject1 = e;
    Object localObject2 = new com/truecaller/truepay/app/ui/transaction/views/adapters/ai;
    localList = (List)((ag)localObject1).a();
    ((ai)localObject2).<init>(localList, paramList);
    localObject2 = DiffUtil.calculateDiff((DiffUtil.Callback)localObject2);
    ((ag)localObject1).a(paramList);
    ((DiffUtil.DiffResult)localObject2).dispatchUpdatesTo((RecyclerView.Adapter)localObject1);
  }
  
  public final void b()
  {
    int i = R.string.collect_req_invalid_vpa;
    Toast.makeText(this, i, 0).show();
  }
  
  public final void b(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.putExtra("receiver_contact", parama);
    setResult(-1, localIntent);
    finish();
  }
  
  public int getLayoutId()
  {
    return R.layout.activity_search_transaction;
  }
  
  public void initDagger(com.truecaller.truepay.app.a.a.a parama)
  {
    parama = new com/truecaller/truepay/app/a/a/c$a;
    parama.<init>((byte)0);
    Object localObject = (com.truecaller.truepay.app.a.a.a)g.a(Truepay.getApplicationComponent());
    b = ((com.truecaller.truepay.app.a.a.a)localObject);
    localObject = a;
    if (localObject == null)
    {
      localObject = new com/truecaller/truepay/app/a/b/cp;
      ((cp)localObject).<init>();
      a = ((cp)localObject);
    }
    g.a(b, com.truecaller.truepay.app.a.a.a.class);
    localObject = new com/truecaller/truepay/app/a/a/c;
    cp localcp = a;
    parama = b;
    ((c)localObject).<init>(localcp, parama, (byte)0);
    ((d)localObject).a(this);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.id.et_search_bar_activity_search_transaction;
    paramBundle = (EditText)findViewById(i);
    a = paramBundle;
    i = R.id.layout_search_result_empty_state;
    paramBundle = findViewById(i);
    b = paramBundle;
    i = R.id.rv_search_result_activity_search_transaction;
    paramBundle = (RecyclerView)findViewById(i);
    c = paramBundle;
    i = R.id.iv_back_search_bar_activity_search_transaction;
    paramBundle = findViewById(i);
    Object localObject = new com/truecaller/truepay/app/ui/transaction/views/activities/-$$Lambda$SearchTransactionActivity$ky_OdV7BARUCt4sxg3IWK1V-XLo;
    ((-..Lambda.SearchTransactionActivity.ky_OdV7BARUCt4sxg3IWK1V-XLo)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/adapters/ag;
    paramBundle.<init>(this);
    e = paramBundle;
    paramBundle = c;
    localObject = new android/support/v7/widget/LinearLayoutManager;
    int j = 1;
    ((LinearLayoutManager)localObject).<init>(this, j, false);
    paramBundle.setLayoutManager((RecyclerView.LayoutManager)localObject);
    paramBundle = c;
    localObject = e;
    paramBundle.setAdapter((RecyclerView.Adapter)localObject);
    d.a(this);
    paramBundle = new View[j];
    localObject = b;
    paramBundle[0] = localObject;
    setVisibility(0, paramBundle);
    paramBundle = a;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/activities/SearchTransactionActivity$1;
    ((SearchTransactionActivity.1)localObject).<init>(this);
    paramBundle.addTextChangedListener((TextWatcher)localObject);
    a.requestFocus();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.activities.SearchTransactionActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */