package com.truecaller.truepay.app.ui.transaction.views.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.b;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.RecycledViewPool;
import android.view.View;
import android.widget.Toast;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.b.g;
import com.truecaller.truepay.app.ui.transaction.b.h;
import com.truecaller.truepay.app.ui.transaction.b.k;
import com.truecaller.truepay.app.ui.transaction.c.r;
import com.truecaller.truepay.app.ui.transaction.views.adapters.WrapContentLinearLayoutManager;
import com.truecaller.truepay.app.ui.transaction.views.adapters.c;
import com.truecaller.truepay.data.d.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class l
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements SwipeRefreshLayout.b, c, com.truecaller.truepay.app.ui.transaction.views.c.e
{
  RecyclerView a;
  SwipeRefreshLayout b;
  public r c;
  private com.truecaller.truepay.app.ui.transaction.views.c.l d;
  private double e;
  private com.truecaller.truepay.app.ui.transaction.views.adapters.a f;
  
  public static l a(int paramInt)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putInt("tranx_type", paramInt);
    l locall = new com/truecaller/truepay/app/ui/transaction/views/a/l;
    locall.<init>();
    locall.setArguments(localBundle);
    return locall;
  }
  
  private void h()
  {
    Object localObject = getActivity();
    boolean bool = localObject instanceof com.truecaller.truepay.app.ui.transaction.views.c.l;
    if (bool)
    {
      localObject = (com.truecaller.truepay.app.ui.transaction.views.c.l)getActivity();
      d = ((com.truecaller.truepay.app.ui.transaction.views.c.l)localObject);
      return;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Activity should implement TransactionView");
    throw ((Throwable)localObject);
  }
  
  public final int a()
  {
    return R.layout.fragment_pay_contacts;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.e parame)
  {
    boolean bool = parame instanceof com.truecaller.truepay.app.ui.transaction.b.a;
    if (bool)
    {
      com.truecaller.truepay.app.ui.transaction.views.c.l locall = d;
      if (locall != null)
      {
        parame = (com.truecaller.truepay.app.ui.transaction.b.a)parame;
        Bundle localBundle = getArguments();
        String str = "tranx_type";
        int i = localBundle.getInt(str, 0);
        locall.onContactSelected(parame, i);
      }
    }
  }
  
  public final void a(Throwable paramThrowable)
  {
    int i = R.string.server_error_message;
    String str = getString(i);
    a(str, paramThrowable);
  }
  
  public final void a(List paramList)
  {
    int i = paramList.size();
    double d1 = i;
    e = d1;
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = new com/truecaller/truepay/app/ui/transaction/b/h;
    int j = R.string.friends_on_truecaller_pay;
    int k = 1;
    Object localObject3 = new Object[k];
    int m = paramList.size();
    Object localObject4 = Integer.valueOf(m);
    String str1 = null;
    localObject3[0] = localObject4;
    Object localObject5 = getString(j, (Object[])localObject3);
    ((h)localObject2).<init>((String)localObject5);
    ((List)localObject1).add(localObject2);
    localObject2 = paramList.iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject5 = (d)((Iterator)localObject2).next();
      com.truecaller.truepay.app.ui.transaction.b.a locala = new com/truecaller/truepay/app/ui/transaction/b/a;
      localObject4 = a;
      str1 = b;
      String str2 = d;
      boolean bool2 = true;
      String str3 = g;
      String str4 = h;
      String str5 = i;
      localObject3 = locala;
      locala.<init>((String)localObject4, str1, str2, bool2, str3, str4, str5);
      ((List)localObject1).add(locala);
    }
    localObject2 = new com/truecaller/truepay/app/ui/transaction/b/k;
    ((k)localObject2).<init>();
    ((List)localObject1).add(localObject2);
    ((List)f.a()).clear();
    ((List)f.a()).addAll((Collection)localObject1);
    a.getRecycledViewPool().clear();
    a.getAdapter().notifyDataSetChanged();
    localObject1 = c;
    int n = paramList.size();
    ((r)localObject1).a(n);
  }
  
  public final void a(boolean paramBoolean)
  {
    b.setRefreshing(paramBoolean);
  }
  
  public final void ad_()
  {
    c.a(true);
  }
  
  public final void b()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.setAction("android.intent.action.SEND");
    int i = R.string.invitation_message;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "truecaller.com/download";
    String str = getString(i, arrayOfObject);
    localIntent.putExtra("android.intent.extra.TEXT", str);
    localIntent.setType("text/plain");
    startActivity(localIntent);
  }
  
  public final void b(com.truecaller.truepay.app.ui.transaction.b.e parame)
  {
    boolean bool = parame instanceof com.truecaller.truepay.app.ui.transaction.b.a;
    if (bool)
    {
      parame = (com.truecaller.truepay.app.ui.transaction.b.a)parame;
      Object localObject1 = e;
      Object localObject2 = c;
      if ((localObject2 != null) && (localObject1 != null)) {
        ((r)localObject2).a((String)localObject1);
      }
      localObject1 = d;
      if (localObject1 != null) {
        ((com.truecaller.truepay.app.ui.transaction.views.c.l)localObject1).showInviteOptionPopup(parame);
      }
      parame = Truepay.getInstance().getAnalyticLoggerHelper();
      localObject1 = "app_payment_contact_invite";
      localObject2 = "contacts";
      double d1 = e;
      parame.a((String)localObject1, (String)localObject2, d1);
    }
  }
  
  public final void b(List paramList)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = new com/truecaller/truepay/app/ui/transaction/b/h;
    int i = R.string.all_contacts;
    Object localObject2 = getString(i);
    ((h)localObject1).<init>((String)localObject2);
    localArrayList.add(localObject1);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (d)paramList.next();
      com.truecaller.truepay.app.ui.transaction.b.a locala = new com/truecaller/truepay/app/ui/transaction/b/a;
      String str1 = a;
      String str2 = b;
      String str3 = d;
      boolean bool2 = c;
      String str4 = g;
      String str5 = h;
      String str6 = i;
      localObject2 = locala;
      locala.<init>(str1, str2, str3, bool2, str4, str5, str6);
      localArrayList.add(locala);
    }
    ((List)f.a()).addAll(localArrayList);
    a.getRecycledViewPool().clear();
    a.getAdapter().notifyDataSetChanged();
  }
  
  public final void c()
  {
    c.a();
  }
  
  public final void d()
  {
    c.c();
  }
  
  public final void e()
  {
    Object localObject = getResources();
    int i = R.string.error_fetching_contacts;
    localObject = ((Resources)localObject).getString(i);
    a((String)localObject, null);
  }
  
  public final void f()
  {
    android.support.v4.app.f localf = getActivity();
    Object localObject = getResources();
    int i = R.string.error_fetching_active_contacts;
    localObject = ((Resources)localObject).getString(i);
    Toast.makeText(localf, (CharSequence)localObject, 0).show();
  }
  
  public final void g()
  {
    ((List)f.a()).clear();
    List localList = (List)f.a();
    g localg = new com/truecaller/truepay/app/ui/transaction/b/g;
    localg.<init>();
    localList.add(localg);
    a.getRecycledViewPool().clear();
    a.getAdapter().notifyDataSetChanged();
  }
  
  public final void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    h();
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    h();
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    d = null;
    c.b();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int i = 1001;
    if (paramInt == i)
    {
      paramInt = paramArrayOfInt.length;
      i = 1;
      if (paramInt == i)
      {
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0)
        {
          c.a(false);
          b.setOnRefreshListener(this);
          return;
        }
      }
      Object localObject = getContext();
      paramArrayOfString = getResources();
      int j = R.string.phone_read_permission_denied;
      paramArrayOfString = paramArrayOfString.getString(j);
      Toast.makeText((Context)localObject, paramArrayOfString, 0).show();
      localObject = getActivity();
      ((android.support.v4.app.f)localObject).onBackPressed();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    Object localObject = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a().a(this);
    int i = R.id.rv_active_contacts_frag_pay_contacts;
    paramBundle = (RecyclerView)paramView.findViewById(i);
    a = paramBundle;
    i = R.id.srl_contacts_frag_pay_contacts;
    paramView = (SwipeRefreshLayout)paramView.findViewById(i);
    b = paramView;
    c.a(this);
    h();
    paramView = new com/truecaller/truepay/app/ui/transaction/views/adapters/a;
    paramView.<init>(this);
    f = paramView;
    paramView = a;
    paramBundle = f;
    paramView.setAdapter(paramBundle);
    paramView = a;
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/adapters/WrapContentLinearLayoutManager;
    localObject = getActivity();
    paramBundle.<init>((Context)localObject);
    paramView.setLayoutManager(paramBundle);
    paramView = getContext();
    int j = android.support.v4.app.a.a(paramView, "android.permission.READ_CONTACTS");
    i = 0;
    paramBundle = null;
    if (j != 0)
    {
      paramView = getActivity();
      localObject = "android.permission.READ_CONTACTS";
      boolean bool = android.support.v4.app.a.a(paramView, (String)localObject);
      if (bool)
      {
        paramView = getContext();
        localObject = getResources();
        int k = R.string.read_phone_permission;
        localObject = ((Resources)localObject).getString(k);
        paramView = Toast.makeText(paramView, (CharSequence)localObject, 0);
        paramView.show();
      }
      paramView = new String[] { "android.permission.READ_CONTACTS" };
      requestPermissions(paramView, 1001);
      return;
    }
    c.a(false);
    b.setOnRefreshListener(this);
    c.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */