package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.base.views.c.a;

final class d
  extends a
  implements View.OnClickListener
{
  final TextView b;
  final TextView c;
  final ImageView d;
  
  d(View paramView, f paramf)
  {
    super(paramView, paramf);
    int i = R.id.tv_msisdn_item_active_contact;
    paramf = (TextView)paramView.findViewById(i);
    b = paramf;
    i = R.id.tv_name_item_active_contacts;
    paramf = (TextView)paramView.findViewById(i);
    c = paramf;
    i = R.id.iv_profile_pic_item_active_contact;
    paramf = (ImageView)paramView.findViewById(i);
    d = paramf;
    paramView.setOnClickListener(this);
  }
  
  public final void onClick(View paramView)
  {
    paramView = (e)a;
    int i = getAdapterPosition();
    paramView.a(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */