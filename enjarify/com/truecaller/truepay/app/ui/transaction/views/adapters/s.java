package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.g;

final class s
  extends c
{
  public s(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    g localg = new com/truecaller/truepay/app/ui/transaction/views/b/g;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_header_contact_list;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localg.<init>(paramViewGroup, (f)localObject);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */