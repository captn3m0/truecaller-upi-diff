package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.b.b;
import com.truecaller.truepay.app.ui.transaction.views.b.t;
import java.util.List;

class ao
  extends c
{
  public ao(f paramf)
  {
    super(paramf);
  }
  
  public RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    t localt = new com/truecaller/truepay/app/ui/transaction/views/b/t;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_beneficiary_vpa;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localt.<init>(paramViewGroup, (f)localObject);
    return localt;
  }
  
  public final boolean a(Object paramObject, List paramList)
  {
    boolean bool1 = paramObject instanceof b;
    if (bool1)
    {
      paramObject = g;
      paramList = "vpa";
      boolean bool2 = ((String)paramObject).equalsIgnoreCase(paramList);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */