package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;

final class b
  extends c
{
  public b(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    d locald = new com/truecaller/truepay/app/ui/transaction/views/adapters/d;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_active_contacts_list;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    locald.<init>(paramViewGroup, (f)localObject);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */