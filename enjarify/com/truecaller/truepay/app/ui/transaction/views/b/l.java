package com.truecaller.truepay.app.ui.transaction.views.b;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.base.views.c.a;
import com.truecaller.truepay.app.ui.transaction.views.adapters.ae;
import com.truecaller.truepay.app.utils.r;

public final class l
  extends a
  implements View.OnClickListener
{
  public final r b;
  public ImageView c;
  public TextView d;
  public TextView e;
  
  public l(View paramView, f paramf, r paramr)
  {
    super(paramView, paramf);
    int i = R.id.iv_bank_logo_item_personal_accounts;
    paramf = (ImageView)paramView.findViewById(i);
    c = paramf;
    i = R.id.tv_account_number_item_personal_accounts;
    paramf = (TextView)paramView.findViewById(i);
    d = paramf;
    i = R.id.tv_bank_name_item_personal_accounts;
    paramf = (TextView)paramView.findViewById(i);
    e = paramf;
    b = paramr;
    paramView.setOnClickListener(this);
  }
  
  public final void onClick(View paramView)
  {
    paramView = (ae)a;
    int i = getAdapterPosition();
    paramView.a(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */