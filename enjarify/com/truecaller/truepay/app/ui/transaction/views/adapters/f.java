package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import java.util.List;

final class f
  extends c
{
  public f(com.truecaller.truepay.app.ui.base.views.b.f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    com.truecaller.truepay.app.ui.transaction.views.b.a locala = new com/truecaller/truepay/app/ui/transaction/views/b/a;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_inactive_contacts_list;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    locala.<init>(paramViewGroup, (com.truecaller.truepay.app.ui.base.views.b.f)localObject);
    return locala;
  }
  
  public final boolean a(Object paramObject, List paramList)
  {
    return paramObject instanceof com.truecaller.truepay.app.ui.transaction.b.a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */