package com.truecaller.truepay.app.ui.transaction.views.a;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.base.views.fragments.a;
import com.truecaller.truepay.data.d.b;

public class r
  extends a
{
  r.a a;
  Button b;
  Button c;
  TextView d;
  private b e;
  
  public static r a(b paramb)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("blocked_vpa", paramb);
    paramb = new com/truecaller/truepay/app/ui/transaction/views/a/r;
    paramb.<init>();
    paramb.setArguments(localBundle);
    return paramb;
  }
  
  public final int a()
  {
    return R.layout.fragment_unblock_vpa_confirmation_dialog;
  }
  
  public final void b()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = getTargetFragment();
      boolean bool = localObject1 instanceof r.a;
      if (bool)
      {
        localObject1 = (r.a)getTargetFragment();
        a = ((r.a)localObject1);
        return;
      }
    }
    localObject1 = new java/lang/RuntimeException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("parent fragment should implement ");
    String str = r.a.class.getSimpleName();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((RuntimeException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btn_cancel_frag_unblock_confirmation_dialog;
    paramBundle = (Button)paramView.findViewById(i);
    b = paramBundle;
    i = R.id.btn_okay_frag_unblock_confirmation_dialog;
    paramBundle = (Button)paramView.findViewById(i);
    c = paramBundle;
    i = R.id.tv_frag_unblock_confirm_description;
    paramView = (TextView)paramView.findViewById(i);
    d = paramView;
    paramView = c;
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$r$5_AmswIYPqo2NxkD_KrvZgnnmH4;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = b;
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$r$jghSH5pfm23mTc7yArg5cf0xrfk;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = (b)getArguments().getSerializable("blocked_vpa");
    e = paramView;
    paramView = d;
    i = R.string.unblock_confirmation_desc;
    Object[] arrayOfObject = new Object[1];
    String str = e.a;
    arrayOfObject[0] = str;
    paramBundle = getString(i, arrayOfObject);
    paramView.setText(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */