package com.truecaller.truepay.app.ui.transaction.views.adapters;

import com.truecaller.truepay.app.ui.base.views.b.d;
import com.truecaller.truepay.app.ui.base.views.b.e;
import com.truecaller.truepay.app.ui.transaction.b.b;
import java.util.List;

public final class j
  extends com.truecaller.truepay.app.ui.base.views.b.g
  implements ap, h
{
  private final k c;
  
  public j(k paramk)
  {
    c = paramk;
    paramk = a;
    Object localObject = new com/truecaller/truepay/app/ui/transaction/views/adapters/g;
    ((g)localObject).<init>(this);
    paramk.a((d)localObject);
    paramk = a;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/adapters/ao;
    ((ao)localObject).<init>(this);
    paramk.a((d)localObject);
  }
  
  public final void a(int paramInt)
  {
    k localk = c;
    b localb = (b)((List)a()).get(paramInt);
    localk.a(localb);
  }
  
  public final void d(int paramInt)
  {
    k localk = c;
    b localb = (b)((List)a()).get(paramInt);
    localk.b(localb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */