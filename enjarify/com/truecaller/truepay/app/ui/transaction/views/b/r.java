package com.truecaller.truepay.app.ui.transaction.views.b;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.base.views.c.a;
import com.truecaller.truepay.app.ui.base.widgets.CyclicProgressBar;
import com.truecaller.truepay.app.ui.transaction.views.c.j;

public final class r
  extends a
  implements View.OnClickListener
{
  public TextView b;
  public TextView c;
  public TextView d;
  public CyclicProgressBar e;
  
  public r(View paramView, f paramf)
  {
    super(paramView, paramf);
    int i = R.id.tv_resolved_name_item_search_result_resolved_vpa;
    paramf = (TextView)paramView.findViewById(i);
    b = paramf;
    i = R.id.tv_resolved_vpa_name_item_search_result_resolved_vpa;
    paramf = (TextView)paramView.findViewById(i);
    c = paramf;
    i = R.id.tv_searching_vpa_name_item_search_result_resolved_vpa;
    paramf = (TextView)paramView.findViewById(i);
    d = paramf;
    i = R.id.pb_item_search_result_resolved_vpa;
    paramf = (CyclicProgressBar)paramView.findViewById(i);
    e = paramf;
    paramView.setOnClickListener(this);
  }
  
  public final void b()
  {
    TextView localTextView = b;
    int i = 8;
    localTextView.setVisibility(i);
    c.setVisibility(i);
  }
  
  public final void onClick(View paramView)
  {
    int i = getAdapterPosition();
    int j = -1;
    if (i != j)
    {
      paramView = (j)a;
      j = getAdapterPosition();
      paramView.e(j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.b.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */