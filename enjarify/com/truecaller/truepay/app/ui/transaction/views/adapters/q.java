package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.b;
import com.truecaller.truepay.app.ui.transaction.views.b.d;
import java.util.List;

final class q
  extends b
{
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    d locald = new com/truecaller/truepay/app/ui/transaction/views/b/d;
    LayoutInflater localLayoutInflater = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_empty_fallback;
    paramViewGroup = localLayoutInflater.inflate(i, paramViewGroup, false);
    locald.<init>(paramViewGroup);
    return locald;
  }
  
  public final void a(Object paramObject, int paramInt, RecyclerView.ViewHolder paramViewHolder, List paramList) {}
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */