package com.truecaller.truepay.app.ui.transaction.views.a;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.b;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;
import c.g.b.k;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.c.c.a;
import com.truecaller.truepay.app.ui.transaction.c.c.b;
import com.truecaller.truepay.app.ui.transaction.views.adapters.l;
import java.util.HashMap;
import java.util.List;

public final class d
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements SwipeRefreshLayout.b, c.b, r.a
{
  public static final d.a c;
  public c.a b;
  private final c.f d;
  private HashMap e;
  
  static
  {
    Object localObject1 = new c.l.g[1];
    Object localObject2 = new c/g/b/u;
    c.l.b localb = w.a(d.class);
    ((u)localObject2).<init>(localb, "adapter", "getAdapter()Lcom/truecaller/truepay/app/ui/transaction/views/adapters/BlockedVpaListAdapter;");
    localObject2 = (c.l.g)w.a((c.g.b.t)localObject2);
    localObject1[0] = localObject2;
    a = (c.l.g[])localObject1;
    localObject1 = new com/truecaller/truepay/app/ui/transaction/views/a/d$a;
    ((d.a)localObject1).<init>((byte)0);
    c = (d.a)localObject1;
  }
  
  public d()
  {
    Object localObject = new com/truecaller/truepay/app/ui/transaction/views/a/d$b;
    ((d.b)localObject).<init>(this);
    localObject = c.g.a((c.g.a.a)localObject);
    d = ((c.f)localObject);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final d f()
  {
    d locald = new com/truecaller/truepay/app/ui/transaction/views/a/d;
    locald.<init>();
    return locald;
  }
  
  private final l g()
  {
    return (l)d.b();
  }
  
  public final int a()
  {
    return R.layout.fragment_blocked_vpa_list;
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject1 = getActivity();
    boolean bool = localObject1 instanceof com.truecaller.truepay.app.ui.base.views.a.b;
    if (!bool) {
      localObject1 = null;
    }
    localObject1 = (com.truecaller.truepay.app.ui.base.views.a.b)localObject1;
    if (localObject1 != null)
    {
      Object localObject2 = localObject1;
      localObject2 = (android.support.v4.app.f)localObject1;
      int i = R.id.toolbar_frag_blocked_vpa;
      localObject2 = (Toolbar)((android.support.v4.app.f)localObject2).findViewById(i);
      ((com.truecaller.truepay.app.ui.base.views.a.b)localObject1).setSupportActionBar((Toolbar)localObject2);
    }
    localObject1 = getActivity();
    bool = localObject1 instanceof com.truecaller.truepay.app.ui.base.views.a.b;
    if (!bool) {
      localObject1 = null;
    }
    localObject1 = (com.truecaller.truepay.app.ui.base.views.a.b)localObject1;
    if (localObject1 != null)
    {
      localObject1 = ((com.truecaller.truepay.app.ui.base.views.a.b)localObject1).getSupportActionBar();
      if (localObject1 != null)
      {
        ((ActionBar)localObject1).setTitle(paramInt1);
        paramInt1 = 1;
        ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(paramInt1);
        ((ActionBar)localObject1).setHomeAsUpIndicator(paramInt2);
      }
    }
    paramInt1 = R.id.toolbar_frag_blocked_vpa;
    Toolbar localToolbar = (Toolbar)a(paramInt1);
    Object localObject3 = new com/truecaller/truepay/app/ui/transaction/views/a/d$c;
    ((d.c)localObject3).<init>(this);
    localObject3 = (View.OnClickListener)localObject3;
    localToolbar.setNavigationOnClickListener((View.OnClickListener)localObject3);
  }
  
  public final void a(com.truecaller.truepay.data.d.b paramb)
  {
    k.b(paramb, "blockedVpa");
    j localj = getFragmentManager();
    if (localj != null)
    {
      paramb = r.a(paramb);
      Object localObject = this;
      localObject = (Fragment)this;
      paramb.setTargetFragment((Fragment)localObject, 1008);
      localObject = r.class.getSimpleName();
      paramb.show(localj, (String)localObject);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "message");
    Context localContext = getContext();
    if (localContext != null)
    {
      paramString = (CharSequence)paramString;
      Toast.makeText(localContext, paramString, 0).show();
      return;
    }
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "list");
    int i = R.id.rv_frag_blocked_vpa;
    RecyclerView localRecyclerView = (RecyclerView)a(i);
    k.a(localRecyclerView, "rv_frag_blocked_vpa");
    com.truecaller.utils.extensions.t.a((View)localRecyclerView, true);
    g().a(paramList);
    g().notifyDataSetChanged();
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.srl_frag_blocked_vpa;
    SwipeRefreshLayout localSwipeRefreshLayout = (SwipeRefreshLayout)a(i);
    k.a(localSwipeRefreshLayout, "srl_frag_blocked_vpa");
    localSwipeRefreshLayout.setRefreshing(paramBoolean);
  }
  
  public final void ad_()
  {
    c.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.b();
  }
  
  public final void b()
  {
    int i = R.id.srl_frag_blocked_vpa;
    Object localObject1 = (SwipeRefreshLayout)a(i);
    Object localObject2 = this;
    localObject2 = (SwipeRefreshLayout.b)this;
    ((SwipeRefreshLayout)localObject1).setOnRefreshListener((SwipeRefreshLayout.b)localObject2);
    i = R.id.rv_frag_blocked_vpa;
    localObject1 = (RecyclerView)a(i);
    k.a(localObject1, "rv_frag_blocked_vpa");
    localObject2 = new android/support/v7/widget/LinearLayoutManager;
    Context localContext = (Context)getActivity();
    ((LinearLayoutManager)localObject2).<init>(localContext);
    localObject2 = (RecyclerView.LayoutManager)localObject2;
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject2);
    i = R.id.rv_frag_blocked_vpa;
    localObject1 = (RecyclerView)a(i);
    k.a(localObject1, "rv_frag_blocked_vpa");
    localObject2 = (RecyclerView.Adapter)g();
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
  }
  
  public final void b(com.truecaller.truepay.data.d.b paramb)
  {
    c.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a(paramb);
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.empty_state_frag_blocked_vpa_list;
    View localView = a(i);
    k.a(localView, "empty_state_frag_blocked_vpa_list");
    com.truecaller.utils.extensions.t.a(localView, paramBoolean);
  }
  
  public final void c()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      localf.onBackPressed();
      return;
    }
  }
  
  public final void d()
  {
    int i = R.id.rv_frag_blocked_vpa;
    RecyclerView localRecyclerView = (RecyclerView)a(i);
    k.a(localRecyclerView, "rv_frag_blocked_vpa");
    com.truecaller.utils.extensions.t.a((View)localRecyclerView, false);
  }
  
  public final c.a e()
  {
    c.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null) {
      paramMenu.clear();
    }
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    setHasOptionsMenu(true);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    c.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.y_();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */