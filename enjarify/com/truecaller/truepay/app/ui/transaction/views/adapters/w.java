package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.m;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.views.a.l;
import com.truecaller.utils.n;

public final class w
  extends m
{
  private l a;
  private com.truecaller.truepay.app.ui.transaction.views.a.j b;
  private n c;
  
  public w(android.support.v4.app.j paramj, n paramn)
  {
    super(paramj);
    c = paramn;
  }
  
  public final Fragment a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      localObject1 = new String[1];
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("invalid position selected for ");
      String str = getClass().getSimpleName();
      ((StringBuilder)localObject2).append(str);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject1[0] = localObject2;
      return l.a(0);
    case 1: 
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject1 = com.truecaller.truepay.app.ui.transaction.views.a.j.j();
        b = ((com.truecaller.truepay.app.ui.transaction.views.a.j)localObject1);
      }
      return b;
    }
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = l.a(0);
      a = ((l)localObject1);
    }
    return a;
  }
  
  public final int getCount()
  {
    return 2;
  }
  
  public final CharSequence getPageTitle(int paramInt)
  {
    Object[] arrayOfObject = null;
    switch (paramInt)
    {
    default: 
      localObject1 = new String[1];
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("invalid position selected for ");
      String str = getClass().getSimpleName();
      ((StringBuilder)localObject2).append(str);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject1[0] = localObject2;
      return "";
    case 1: 
      localObject1 = c;
      i = R.string.beneficiaries_caps;
      arrayOfObject = new Object[0];
      return ((n)localObject1).a(i, arrayOfObject);
    }
    Object localObject1 = c;
    int i = R.string.contacts_caps;
    arrayOfObject = new Object[0];
    return ((n)localObject1).a(i, arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */