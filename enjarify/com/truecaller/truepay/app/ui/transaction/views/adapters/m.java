package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.f;

final class m
  extends com.truecaller.truepay.app.ui.base.views.b.c
{
  public m(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    com.truecaller.truepay.app.ui.transaction.views.b.c localc = new com/truecaller/truepay/app/ui/transaction/views/b/c;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_blocked_vpa;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localc.<init>(paramViewGroup, (f)localObject);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */