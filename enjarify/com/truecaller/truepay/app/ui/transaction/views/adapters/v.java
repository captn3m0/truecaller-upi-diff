package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.Adapter;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.a.b;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import java.util.List;

public final class v
  extends RecyclerView.Adapter
{
  public r a;
  public au b;
  public v.a c;
  private final List d;
  private int e;
  
  public v(List paramList)
  {
    d = paramList;
    paramList = com.truecaller.truepay.app.ui.transaction.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramList.a(locala).a().a(this);
  }
  
  public final int getItemCount()
  {
    return d.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */