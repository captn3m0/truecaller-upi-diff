package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.e;

final class r
  extends c
{
  public r(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    e locale = new com/truecaller/truepay/app/ui/transaction/views/b/e;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_empty_state_contacts_list;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    locale.<init>(paramViewGroup, (f)localObject);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */