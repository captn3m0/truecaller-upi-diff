package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.util.DiffUtil.Callback;
import com.truecaller.truepay.app.ui.transaction.b.a;
import com.truecaller.truepay.app.ui.transaction.b.b;
import com.truecaller.truepay.app.ui.transaction.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.m;
import com.truecaller.truepay.app.ui.transaction.views.b.n;
import com.truecaller.truepay.app.ui.transaction.views.b.p;
import java.util.List;

public final class ai
  extends DiffUtil.Callback
{
  List a;
  List b;
  
  public ai(List paramList1, List paramList2)
  {
    a = paramList1;
    b = paramList2;
  }
  
  public final boolean areContentsTheSame(int paramInt1, int paramInt2)
  {
    Object localObject1 = a.get(paramInt1);
    boolean bool1 = localObject1 instanceof a;
    boolean bool2 = true;
    Object localObject2;
    String str;
    if (bool1)
    {
      localObject2 = (a)a.get(paramInt1);
      localObject3 = (a)b.get(paramInt2);
      localObject1 = d;
      str = d;
      bool1 = ((String)localObject1).equals(str);
      if (bool1)
      {
        paramInt1 = g;
        paramInt2 = g;
        if (paramInt1 == paramInt2) {
          return bool2;
        }
      }
      return false;
    }
    localObject1 = a.get(paramInt1);
    bool1 = localObject1 instanceof b;
    if (bool1)
    {
      localObject2 = (b)a.get(paramInt1);
      localObject3 = (b)b.get(paramInt2);
      localObject1 = f;
      str = f;
      bool1 = ((String)localObject1).equals(str);
      if (bool1)
      {
        localObject2 = g;
        localObject3 = g;
        paramInt1 = ((String)localObject2).equals(localObject3);
        if (paramInt1 != 0) {
          return bool2;
        }
      }
      return false;
    }
    Object localObject3 = a.get(paramInt1);
    paramInt2 = localObject3 instanceof p;
    if (paramInt2 == 0)
    {
      localObject3 = a.get(paramInt1);
      paramInt2 = localObject3 instanceof m;
      if (paramInt2 == 0)
      {
        localObject3 = a;
        localObject2 = ((List)localObject3).get(paramInt1);
        paramInt1 = localObject2 instanceof n;
        if (paramInt1 == 0) {
          return false;
        }
      }
    }
    return bool2;
  }
  
  public final boolean areItemsTheSame(int paramInt1, int paramInt2)
  {
    Object localObject1 = a.get(paramInt1).getClass();
    Class localClass = b.get(paramInt2).getClass();
    if (localObject1 != localClass) {
      return false;
    }
    localObject1 = a.get(paramInt1);
    boolean bool = localObject1 instanceof a;
    Object localObject2;
    if (bool)
    {
      localObject2 = a.get(paramInt1)).e;
      localObject3 = b.get(paramInt2)).e;
      return ((String)localObject2).equals(localObject3);
    }
    localObject1 = a.get(paramInt1);
    bool = localObject1 instanceof b;
    if (bool)
    {
      localObject2 = a.get(paramInt1)).i;
      localObject3 = b.get(paramInt2)).i;
      return ((String)localObject2).equals(localObject3);
    }
    Object localObject3 = a.get(paramInt1);
    paramInt2 = localObject3 instanceof p;
    if (paramInt2 == 0)
    {
      localObject3 = a.get(paramInt1);
      paramInt2 = localObject3 instanceof m;
      if (paramInt2 == 0)
      {
        localObject3 = a;
        localObject2 = ((List)localObject3).get(paramInt1);
        paramInt1 = localObject2 instanceof n;
        if (paramInt1 == 0) {
          return false;
        }
      }
    }
    return true;
  }
  
  public final Object getChangePayload(int paramInt1, int paramInt2)
  {
    return super.getChangePayload(paramInt1, paramInt2);
  }
  
  public final int getNewListSize()
  {
    return b.size();
  }
  
  public final int getOldListSize()
  {
    return a.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */