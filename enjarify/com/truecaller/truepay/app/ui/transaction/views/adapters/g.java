package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import java.util.List;

class g
  extends c
{
  public g(f paramf)
  {
    super(paramf);
  }
  
  public RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    com.truecaller.truepay.app.ui.transaction.views.b.b localb = new com/truecaller/truepay/app/ui/transaction/views/b/b;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_beneficiary_bank_account;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localb.<init>(paramViewGroup, (f)localObject);
    return localb;
  }
  
  public final boolean a(Object paramObject, List paramList)
  {
    boolean bool1 = paramObject instanceof com.truecaller.truepay.app.ui.transaction.b.b;
    if (bool1)
    {
      paramObject = g;
      paramList = "account";
      boolean bool2 = ((String)paramObject).equalsIgnoreCase(paramList);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */