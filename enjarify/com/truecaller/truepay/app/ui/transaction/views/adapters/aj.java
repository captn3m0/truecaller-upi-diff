package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.m;
import com.truecaller.truepay.app.ui.transaction.views.b.s;
import java.util.List;

final class aj
  extends c
{
  public aj(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    s locals = new com/truecaller/truepay/app/ui/transaction/views/b/s;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_search_result_beneficiary_header;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    locals.<init>(paramViewGroup, (f)localObject);
    return locals;
  }
  
  public final boolean a(Object paramObject, List paramList)
  {
    return paramObject instanceof m;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */