package com.truecaller.truepay.app.ui.transaction.views.a;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

final class k$1
  extends Animation
{
  k$1(k paramk, View paramView, int paramInt) {}
  
  protected final void applyTransformation(float paramFloat, Transformation paramTransformation)
  {
    paramTransformation = a.getLayoutParams();
    float f = 1.0F;
    boolean bool = paramFloat < f;
    int j;
    if (!bool)
    {
      j = -2;
      paramFloat = 0.0F / 0.0F;
    }
    else
    {
      int i = b;
      f = i * paramFloat;
      j = (int)f;
    }
    height = j;
    a.requestLayout();
  }
  
  public final boolean willChangeBounds()
  {
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.k.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */