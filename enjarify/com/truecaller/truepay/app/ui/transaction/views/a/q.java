package com.truecaller.truepay.app.ui.transaction.views.a;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.a;
import android.support.design.widget.b;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import java.util.HashMap;

public final class q
  extends b
{
  q.a a;
  private TextView b;
  private Button c;
  private HashMap d;
  
  public final int getTheme()
  {
    return R.style.BottomSheetDialogTheme;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = a;
    if (paramBundle == null)
    {
      paramBundle = getTargetFragment();
      boolean bool = paramBundle instanceof q.a;
      if (bool)
      {
        paramBundle = (q.a)getTargetFragment();
        a = paramBundle;
        return;
      }
    }
    paramBundle = new java/lang/RuntimeException;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("parent fragment should implement ");
    String str = q.a.class.getSimpleName();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    paramBundle.<init>((String)localObject);
    throw ((Throwable)paramBundle);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/design/widget/a;
    Context localContext = requireContext();
    int i = getTheme();
    paramBundle.<init>(localContext, i);
    return (Dialog)paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_upi_pin_dialog;
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    int j = R.id.btn_proceed_anyway;
    paramViewGroup = paramLayoutInflater.findViewById(j);
    k.a(paramViewGroup, "rootView.findViewById(R.id.btn_proceed_anyway)");
    paramViewGroup = (TextView)paramViewGroup;
    b = paramViewGroup;
    j = R.id.btn_set_upi_pin;
    paramViewGroup = paramLayoutInflater.findViewById(j);
    k.a(paramViewGroup, "rootView.findViewById(R.id.btn_set_upi_pin)");
    paramViewGroup = (Button)paramViewGroup;
    c = paramViewGroup;
    return paramLayoutInflater;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "tvProceed";
      k.a(paramBundle);
    }
    if (paramView != null)
    {
      paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/q$b;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
    }
    paramView = c;
    if (paramView == null)
    {
      paramBundle = "btnSetUpiPin";
      k.a(paramBundle);
    }
    if (paramView != null)
    {
      paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/q$c;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */