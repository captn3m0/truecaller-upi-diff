package com.truecaller.truepay.app.ui.transaction.views.a;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.c.f;
import java.io.Serializable;
import java.util.HashMap;

public final class a
  extends com.truecaller.truepay.app.ui.base.views.fragments.a
  implements com.truecaller.truepay.app.ui.transaction.views.c.a
{
  public static final a.a c;
  a.b a;
  public f b;
  private com.truecaller.truepay.app.ui.transaction.b.b d;
  private boolean e;
  private HashMap f;
  
  static
  {
    a.a locala = new com/truecaller/truepay/app/ui/transaction/views/a/a$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final a a(com.truecaller.truepay.app.ui.transaction.b.b paramb, boolean paramBoolean)
  {
    k.b(paramb, "beneficiaryAccount");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramb = (Serializable)paramb;
    localBundle.putSerializable("beneficiary_account", paramb);
    Serializable localSerializable = (Serializable)Boolean.valueOf(paramBoolean);
    localBundle.putSerializable("beneficiary_pay_after_save", localSerializable);
    paramb = new com/truecaller/truepay/app/ui/transaction/views/a/a;
    paramb.<init>();
    paramb.setArguments(localBundle);
    return paramb;
  }
  
  public final int a()
  {
    return R.layout.fragment_dialog_add_beneficiary_confirm;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "benfyName");
    int i = R.id.tvNameFragDialogConfirmBenfy;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvNameFragDialogConfirmBenfy");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "accNumber");
    k.b(paramString2, "ifsc");
    int i = R.id.tvIfscFragDialogConfirmBenfy;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvIfscFragDialogConfirmBenfy");
    paramString2 = (CharSequence)paramString2;
    localTextView.setText(paramString2);
    int j = R.id.tvAccountNumberFragDialogConfirmBenfy;
    paramString2 = (TextView)a(j);
    k.a(paramString2, "tvAccountNumberFragDialogConfirmBenfy");
    paramString1 = (CharSequence)paramString1;
    paramString2.setText(paramString1);
  }
  
  public final void b()
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = getTargetFragment();
      boolean bool = localObject instanceof a.b;
      if (bool)
      {
        localObject = (a.b)getTargetFragment();
        a = ((a.b)localObject);
        return;
      }
    }
    new String[1][0] = "Parent fragment does not implemenet listener";
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "benfyMsisdn");
    int i = R.id.tvMobileNumFragDialogConfirmBenfy;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvMobileNumFragDialogConfirmBenfy");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    int j = R.id.tvMobileNumFragDialogConfirmBenfy;
    paramString = (TextView)a(j);
    k.a(paramString, "tvMobileNumFragDialogConfirmBenfy");
    paramString.setVisibility(0);
    j = R.id.tvLabelMobileNumFragDialogConfirmBenfy;
    paramString = (TextView)a(j);
    k.a(paramString, "tvLabelMobileNumFragDialogConfirmBenfy");
    paramString.setVisibility(0);
  }
  
  public final void c()
  {
    int i = R.id.tvMobileNumFragDialogConfirmBenfy;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvMobileNumFragDialogConfirmBenfy");
    int j = 8;
    localTextView.setVisibility(j);
    i = R.id.tvLabelMobileNumFragDialogConfirmBenfy;
    localTextView = (TextView)a(i);
    k.a(localTextView, "tvLabelMobileNumFragDialogConfirmBenfy");
    localTextView.setVisibility(j);
  }
  
  public final int getTheme()
  {
    return R.style.BottomSheetDialogTheme;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/design/widget/a;
    Context localContext = requireContext();
    int i = getTheme();
    paramBundle.<init>(localContext, i);
    return (Dialog)paramBundle;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    a = null;
    f localf = b;
    if (localf == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localf.b();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramView = getArguments();
    if (paramView == null) {
      return;
    }
    k.a(paramView, "arguments ?: return");
    paramBundle = b;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    localObject = this;
    localObject = (com.truecaller.truepay.app.ui.base.views.a)this;
    paramBundle.a((com.truecaller.truepay.app.ui.base.views.a)localObject);
    b();
    int i = R.id.btnConfirmFragDialogAddBenfy;
    paramBundle = (Button)a(i);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/a$c;
    ((a.c)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.btnCancelFragDialogConfirmBenfy;
    paramBundle = (Button)a(i);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/a$d;
    ((a.d)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    paramBundle = paramView.getSerializable("beneficiary_account");
    if (paramBundle != null)
    {
      boolean bool1 = paramView.getBoolean("beneficiary_pay_after_save");
      e = bool1;
      paramBundle = "beneficiary_account";
      paramView = paramView.getSerializable(paramBundle);
      if (paramView != null)
      {
        paramView = (com.truecaller.truepay.app.ui.transaction.b.b)paramView;
        d = paramView;
        paramView = b;
        if (paramView == null)
        {
          paramBundle = "presenter";
          k.a(paramBundle);
        }
        paramBundle = d;
        if (paramBundle == null) {
          k.a();
        }
        k.b(paramBundle, "beneficiaryAccount");
        localObject = (com.truecaller.truepay.app.ui.transaction.views.c.a)paramView.af_();
        String str1;
        if (localObject != null)
        {
          str1 = paramBundle.a();
          if (str1 == null) {
            k.a();
          }
          String str2 = paramBundle.b();
          if (str2 == null) {
            k.a();
          }
          ((com.truecaller.truepay.app.ui.transaction.views.c.a)localObject).a(str1, str2);
        }
        localObject = (com.truecaller.truepay.app.ui.transaction.views.c.a)paramView.af_();
        if (localObject != null)
        {
          str1 = paramBundle.d();
          if (str1 == null) {
            k.a();
          }
          ((com.truecaller.truepay.app.ui.transaction.views.c.a)localObject).a(str1);
        }
        localObject = (CharSequence)paramBundle.c();
        if (localObject != null)
        {
          bool2 = m.a((CharSequence)localObject);
          if (!bool2)
          {
            bool2 = false;
            localObject = null;
            break label333;
          }
        }
        boolean bool2 = true;
        label333:
        if (!bool2)
        {
          paramView = (com.truecaller.truepay.app.ui.transaction.views.c.a)paramView.af_();
          if (paramView != null)
          {
            paramBundle = paramBundle.c();
            localObject = "beneficiaryAccount.benfyMsisdn";
            k.a(paramBundle, (String)localObject);
            paramView.b(paramBundle);
          }
          return;
        }
        paramView = (com.truecaller.truepay.app.ui.transaction.views.c.a)paramView.af_();
        if (paramView != null) {
          paramView.c();
        }
      }
      else
      {
        paramView = new c/u;
        paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.transaction.models.BeneficiaryAccount");
        throw paramView;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */