package com.truecaller.truepay.app.ui.transaction.views.b;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;

public final class a
  extends com.truecaller.truepay.app.ui.base.views.c.a
  implements View.OnClickListener
{
  public TextView b;
  public TextView c;
  public ImageView d;
  public Boolean e;
  
  public a(View paramView, f paramf)
  {
    super(paramView, paramf);
    paramf = Boolean.FALSE;
    e = paramf;
    int i = R.id.tv_name_item_inactive_contacts;
    paramf = (TextView)paramView.findViewById(i);
    b = paramf;
    i = R.id.tv_msisdn_item_inactive_contact;
    paramf = (TextView)paramView.findViewById(i);
    c = paramf;
    i = R.id.iv_profile_pic_item_inactive_contact;
    paramView = (ImageView)paramView.findViewById(i);
    d = paramView;
  }
  
  public final void onClick(View paramView)
  {
    paramView = e;
    boolean bool = paramView.booleanValue();
    if (bool)
    {
      paramView = (i)a;
      i = getAdapterPosition();
      paramView.b(i);
      return;
    }
    paramView = (i)a;
    int i = getAdapterPosition();
    paramView.c(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */