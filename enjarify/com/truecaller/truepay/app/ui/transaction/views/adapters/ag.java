package com.truecaller.truepay.app.ui.transaction.views.adapters;

import com.truecaller.truepay.app.ui.base.views.b.d;
import com.truecaller.truepay.app.ui.base.views.b.e;
import com.truecaller.truepay.app.ui.base.views.b.g;
import com.truecaller.truepay.app.ui.transaction.b.a;
import com.truecaller.truepay.app.ui.transaction.b.b;
import com.truecaller.truepay.app.ui.transaction.b.b.a;
import com.truecaller.truepay.app.ui.transaction.views.b.i;
import com.truecaller.truepay.app.ui.transaction.views.c.j;
import java.util.ArrayList;
import java.util.List;

public final class ag
  extends g
  implements ap, h, i, j
{
  private final ah c;
  
  public ag(ah paramah)
  {
    c = paramah;
    paramah = a;
    Object localObject = new com/truecaller/truepay/app/ui/transaction/views/adapters/f;
    ((f)localObject).<init>(this);
    paramah.a((d)localObject);
    paramah = a;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/adapters/an;
    ((an)localObject).<init>(this);
    paramah.a((d)localObject);
    paramah = a;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/adapters/af;
    ((af)localObject).<init>(this);
    paramah.a((d)localObject);
    paramah = a;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/adapters/aj;
    ((aj)localObject).<init>(this);
    paramah.a((d)localObject);
    paramah = a;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/adapters/ak;
    ((ak)localObject).<init>(this);
    paramah.a((d)localObject);
    paramah = a;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/adapters/am;
    ((am)localObject).<init>(this);
    paramah.a((d)localObject);
    paramah = a;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/adapters/al;
    ((al)localObject).<init>(this);
    paramah.a((d)localObject);
    paramah = a;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/adapters/q;
    ((q)localObject).<init>();
    paramah.b((d)localObject);
    paramah = new java/util/ArrayList;
    paramah.<init>();
    b = paramah;
  }
  
  public final void a(int paramInt) {}
  
  public final void b(int paramInt)
  {
    ah localah = c;
    a locala = (a)((List)b).get(paramInt);
    localah.b(locala);
  }
  
  public final void c(int paramInt)
  {
    ah localah = c;
    a locala = (a)((List)b).get(paramInt);
    localah.a(locala);
  }
  
  public final void d(int paramInt)
  {
    ah localah = c;
    b localb = (b)((List)a()).get(paramInt);
    localah.a(localb);
  }
  
  public final void e(int paramInt)
  {
    Object localObject1 = ((List)b).get(paramInt);
    boolean bool1 = localObject1 instanceof com.truecaller.truepay.data.api.model.ag;
    if (bool1)
    {
      localObject1 = (List)b;
      int i = ((List)localObject1).size();
      if (paramInt < i)
      {
        Object localObject2 = (com.truecaller.truepay.data.api.model.ag)((List)b).get(paramInt);
        localObject1 = "*";
        String str = a;
        boolean bool2 = ((String)localObject1).equals(str);
        if (!bool2)
        {
          localObject1 = "failure";
          str = a;
          bool2 = ((String)localObject1).equalsIgnoreCase(str);
          if (!bool2)
          {
            localObject1 = b;
            if (localObject1 != null)
            {
              localObject1 = new com/truecaller/truepay/app/ui/transaction/b/b$a;
              ((b.a)localObject1).<init>();
              str = a;
              h = str;
              localObject2 = b;
              f = ((String)localObject2);
              g = "vpa";
              localObject2 = ((b.a)localObject1).a();
              localObject1 = c;
              ((ah)localObject1).a((b)localObject2);
              return;
            }
          }
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */