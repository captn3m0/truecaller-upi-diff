package com.truecaller.truepay.app.ui.transaction.views.a;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.b.f;
import com.truecaller.truepay.app.ui.transaction.c.v;
import com.truecaller.truepay.app.ui.transaction.views.adapters.w;
import com.truecaller.truepay.app.ui.transaction.views.c.g;
import com.truecaller.utils.extensions.t;

public class n
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements g
{
  ViewPager a;
  TabLayout b;
  FrameLayout c;
  w d;
  public v e;
  public com.truecaller.truepay.data.e.e f;
  public com.truecaller.utils.n g;
  l h;
  j i;
  
  public static n a(boolean paramBoolean, String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    n localn = new com/truecaller/truepay/app/ui/transaction/views/a/n;
    localn.<init>();
    localBundle.putBoolean("is_beneficiary_tab", paramBoolean);
    localBundle.putString("flow_from", paramString);
    localn.setArguments(localBundle);
    return localn;
  }
  
  private j b()
  {
    Object localObject = i;
    if (localObject == null)
    {
      localObject = d;
      int j = 1;
      localObject = (j)((w)localObject).a(j);
      i = ((j)localObject);
    }
    return i;
  }
  
  private l c()
  {
    l locall = h;
    if (locall == null)
    {
      locall = (l)d.a(0);
      h = locall;
    }
    return h;
  }
  
  public final int a()
  {
    return R.layout.fragment_pay_selection;
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int j = 1005;
    if (paramInt1 == j)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        Object localObject1 = paramIntent.getSerializableExtra("beneficiary_account");
        if (localObject1 != null)
        {
          localObject1 = (com.truecaller.truepay.app.ui.transaction.b.b)paramIntent.getSerializableExtra("beneficiary_account");
          localObject2 = b();
          if (localObject2 != null)
          {
            b().b((com.truecaller.truepay.app.ui.transaction.b.b)localObject1);
            return;
          }
          new String[1][0] = "PayBenfy frag is null";
          return;
        }
        localObject1 = paramIntent.getSerializableExtra("receiver_contact");
        if (localObject1 != null)
        {
          localObject1 = (com.truecaller.truepay.app.ui.transaction.b.a)paramIntent.getSerializableExtra("receiver_contact");
          localObject2 = c();
          if (localObject2 != null)
          {
            c().a((com.truecaller.truepay.app.ui.transaction.b.e)localObject1);
            return;
          }
          new String[1][0] = "PayContacts frag is null";
          return;
        }
        localObject1 = paramIntent.getSerializableExtra("invited_contact");
        if (localObject1 != null)
        {
          localObject1 = (f)paramIntent.getSerializableExtra("invited_contact");
          c().b((com.truecaller.truepay.app.ui.transaction.b.e)localObject1);
          return;
        }
        localObject1 = getResources();
        paramInt2 = R.string.error_selecting_contacts;
        localObject1 = ((Resources)localObject1).getString(paramInt2);
        paramInt2 = 0;
        Object localObject2 = null;
        a((String)localObject1, null);
      }
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    e.b();
  }
  
  public void onResume()
  {
    super.onResume();
    View localView = getView();
    if (localView != null)
    {
      int j = 2;
      t.a(localView, false, j);
    }
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    Object localObject = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a().a(this);
    int j = R.id.vp_frag_pay_select;
    paramBundle = (ViewPager)paramView.findViewById(j);
    a = paramBundle;
    j = R.id.tabs_frag_pay_select;
    paramBundle = (TabLayout)paramView.findViewById(j);
    b = paramBundle;
    j = R.id.fl_loading_trnx_contact_selection;
    paramBundle = (FrameLayout)paramView.findViewById(j);
    c = paramBundle;
    j = R.id.layout_search_bar_contact_selection_fragment;
    paramBundle = paramView.findViewById(j);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$n$Q_zZm9NRyDjCO9D_MkXLc1WyqBA;
    ((-..Lambda.n.Q_zZm9NRyDjCO9D_MkXLc1WyqBA)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    j = R.id.iv_back_search_bar_contact_selection;
    paramView = paramView.findViewById(j);
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$n$GYmP1Cal9lF6oSMM5pQVH5i94TA;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    e.a(this);
    paramView = d;
    if (paramView == null)
    {
      paramView = new com/truecaller/truepay/app/ui/transaction/views/adapters/w;
      paramBundle = getChildFragmentManager();
      localObject = g;
      paramView.<init>(paramBundle, (com.truecaller.utils.n)localObject);
      d = paramView;
    }
    paramView = b;
    paramBundle = a;
    paramView.setupWithViewPager(paramBundle);
    paramView = a;
    paramBundle = d;
    paramView.setAdapter(paramBundle);
    paramView = a;
    j = 2;
    paramView.setOffscreenPageLimit(j);
    paramView = getArguments();
    paramBundle = "is_beneficiary_tab";
    boolean bool = paramView.getBoolean(paramBundle);
    if (bool)
    {
      paramView = a;
      j = 1;
      paramView.setCurrentItem(j);
    }
    String str = getArguments().getString("flow_from");
    paramView = f;
    paramBundle = com.truecaller.truepay.data.b.a.a();
    paramView.a(paramBundle);
    localObject = Truepay.getInstance().getAnalyticLoggerHelper();
    Boolean localBoolean = Boolean.valueOf(Truepay.getInstance().isRegistrationComplete());
    ((com.truecaller.truepay.data.b.a)localObject).a("app_payment_transaction_intent", str, "send_money", null, localBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */