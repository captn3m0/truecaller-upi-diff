package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;

final class v$b
  extends RecyclerView.ViewHolder
{
  TextView a;
  ImageView b;
  TextView c;
  
  v$b(v paramv, View paramView)
  {
    super(paramView);
    int i = R.id.account_name;
    paramv = (TextView)paramView.findViewById(i);
    a = paramv;
    i = R.id.account_pic;
    paramv = (ImageView)paramView.findViewById(i);
    b = paramv;
    i = R.id.account_number;
    paramv = (TextView)paramView.findViewById(i);
    c = paramv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.v.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */