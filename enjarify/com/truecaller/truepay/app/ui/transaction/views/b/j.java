package com.truecaller.truepay.app.ui.transaction.views.b;

import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.base.views.c.a;
import com.truecaller.truepay.app.ui.transaction.views.adapters.u;

public final class j
  extends a
  implements View.OnClickListener
{
  public j(View paramView, f paramf)
  {
    super(paramView, paramf);
    paramView.setOnClickListener(this);
  }
  
  public final void onClick(View paramView)
  {
    ((u)a).b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */