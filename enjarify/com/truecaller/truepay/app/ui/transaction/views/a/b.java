package com.truecaller.truepay.app.ui.transaction.views.a;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.b.c;
import com.truecaller.truepay.app.ui.transaction.b.c.a;
import com.truecaller.truepay.app.utils.af;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.data.api.model.ag;
import com.truecaller.utils.extensions.t;
import java.util.List;

public class b
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements a.b, com.truecaller.truepay.app.ui.transaction.views.c.b
{
  List a;
  List b;
  List c;
  List d;
  List e;
  List f;
  TextInputLayout g;
  TextView h;
  TextView i;
  ImageView j;
  Toolbar k;
  Button l;
  Button n;
  ProgressBar o;
  ImageView p;
  public com.truecaller.truepay.app.ui.transaction.c.b q;
  public Context r;
  b.a s;
  private int t;
  private com.truecaller.truepay.data.d.a u;
  private com.truecaller.truepay.app.ui.transaction.b.a v;
  private final int w = 10;
  
  public static b a(String paramString, int paramInt, com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str = "type";
    localBundle.putString(str, paramString);
    paramString = "tranx_type";
    localBundle.putInt(paramString, paramInt);
    if (parama != null)
    {
      paramString = "add_benefy_contact";
      localBundle.putSerializable(paramString, parama);
    }
    paramString = new com/truecaller/truepay/app/ui/transaction/views/a/b;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  private void b(boolean paramBoolean)
  {
    b localb = this;
    boolean bool1 = paramBoolean;
    af.a(a);
    af.a(e);
    af.a(c);
    Object localObject1 = new com/truecaller/truepay/app/ui/transaction/b/c$a;
    ((c.a)localObject1).<init>();
    Object localObject2 = b;
    boolean bool2 = false;
    Object localObject3 = null;
    localObject2 = ((TextInputEditText)((List)localObject2).get(0)).getText().toString().trim();
    c = ((String)localObject2);
    localObject2 = ((TextInputEditText)d.get(0)).getText().toString().trim();
    g = ((String)localObject2);
    localObject2 = d;
    int i1 = 1;
    localObject2 = ((TextInputEditText)((List)localObject2).get(i1)).getText().toString().trim();
    f = ((String)localObject2);
    localObject2 = ((TextInputEditText)f.get(0)).getText().toString().trim();
    i = ((String)localObject2);
    localObject2 = ((TextInputEditText)f.get(i1)).getText().toString().trim();
    k = ((String)localObject2);
    localObject2 = ((TextInputEditText)b.get(i1)).getText().toString().trim();
    e = ((String)localObject2);
    localObject2 = getArguments();
    if (localObject2 != null)
    {
      localObject2 = getArguments();
      localObject4 = "type";
      str1 = "";
      localObject2 = ((Bundle)localObject2).getString((String)localObject4, str1);
    }
    else
    {
      localObject2 = "";
    }
    h = ((String)localObject2);
    localObject2 = u;
    if (localObject2 != null) {
      localObject2 = e;
    } else {
      localObject2 = null;
    }
    l = ((String)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/transaction/b/c;
    String str1 = b;
    String str2 = c;
    Boolean localBoolean = d;
    String str3 = e;
    String str4 = f;
    String str5 = g;
    String str6 = h;
    String str7 = i;
    String str8 = j;
    Object localObject4 = k;
    localObject3 = l;
    localObject1 = a;
    Object localObject5 = localObject4;
    localObject4 = localObject2;
    ((c)localObject2).<init>(str1, str2, localBoolean, str3, str4, str5, str6, str7, str8, (String)localObject5, (String)localObject3, (String)localObject1);
    localObject1 = q;
    localObject3 = g;
    label724:
    String str9;
    label810:
    int i6;
    if (localObject3 != null)
    {
      localObject3 = g;
      int i3 = -1;
      int i4 = ((String)localObject3).hashCode();
      int i5 = -1177318867;
      if (i4 != i5)
      {
        i5 = 116967;
        if (i4 == i5)
        {
          str1 = "vpa";
          bool2 = ((String)localObject3).equals(str1);
          if (bool2) {
            i3 = 1;
          }
        }
      }
      else
      {
        str1 = "account";
        bool2 = ((String)localObject3).equals(str1);
        if (bool2)
        {
          i3 = 0;
          localObject4 = null;
        }
      }
      int m;
      if (i3 != 0)
      {
        bool2 = true;
      }
      else
      {
        localObject3 = b;
        if (localObject3 != null)
        {
          localObject3 = b;
          m = ((String)localObject3).length();
          if (m != 0)
          {
            localObject3 = b;
            m = ((String)localObject3).length();
            i3 = 8;
            if (m >= i3)
            {
              localObject3 = b;
              m = ((String)localObject3).length();
              i3 = 25;
              if (m <= i3)
              {
                m = 1;
                break label724;
              }
            }
            localObject3 = (com.truecaller.truepay.app.ui.transaction.views.c.b)d;
            i1 = R.string.add_benfy_error_msg_bank_acc_length;
            ((com.truecaller.truepay.app.ui.transaction.views.c.b)localObject3).b(i1);
            m = 0;
            localObject3 = null;
            break label724;
          }
        }
        localObject3 = (com.truecaller.truepay.app.ui.transaction.views.c.b)d;
        i1 = R.string.add_benfy_error_msg_bank_acc_empty;
        ((com.truecaller.truepay.app.ui.transaction.views.c.b)localObject3).b(i1);
        m = 0;
        localObject3 = null;
        str9 = f;
        if (str9 != null)
        {
          str9 = f;
          i1 = str9.length();
          if (i1 != 0)
          {
            str9 = f;
            i1 = str9.length();
            i3 = 60;
            if (i1 <= i3) {
              break label810;
            }
          }
        }
        localObject3 = (com.truecaller.truepay.app.ui.transaction.views.c.b)d;
        i1 = R.string.add_benfy_error_msg_benfy_name_invalid;
        ((com.truecaller.truepay.app.ui.transaction.views.c.b)localObject3).a(i1);
        m = 0;
        localObject3 = null;
        str9 = d;
        boolean bool4 = TextUtils.isEmpty(str9);
        if (!bool4)
        {
          str9 = d;
          int i2 = str9.length();
          i3 = 11;
          if (i2 == i3)
          {
            str9 = d;
            localObject4 = "^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$";
            boolean bool5 = str9.matches((String)localObject4);
            if (!bool5)
            {
              localObject1 = (com.truecaller.truepay.app.ui.transaction.views.c.b)d;
              m = R.string.add_benfy_error_msg_ifsc_invalid;
              ((com.truecaller.truepay.app.ui.transaction.views.c.b)localObject1).c(m);
              i6 = 0;
              break label997;
            }
          }
          else
          {
            localObject1 = (com.truecaller.truepay.app.ui.transaction.views.c.b)d;
            m = R.string.add_benfy_error_msg_ifsc_length;
            ((com.truecaller.truepay.app.ui.transaction.views.c.b)localObject1).c(m);
            i6 = 0;
            break label997;
          }
        }
        else
        {
          localObject1 = (com.truecaller.truepay.app.ui.transaction.views.c.b)d;
          m = R.string.add_benfy_error_msg_ifsc_empty;
          ((com.truecaller.truepay.app.ui.transaction.views.c.b)localObject1).c(m);
          m = 0;
          localObject3 = null;
        }
      }
      i6 = m;
    }
    else
    {
      localObject1 = (com.truecaller.truepay.app.ui.transaction.views.c.b)d;
      ((com.truecaller.truepay.app.ui.transaction.views.c.b)localObject1).b();
      i6 = 0;
    }
    label997:
    if (i6 != 0)
    {
      localObject1 = q;
      localObject3 = d;
      if (localObject3 != null)
      {
        localObject3 = "account";
        str9 = g;
        boolean bool3 = ((String)localObject3).equalsIgnoreCase(str9);
        if (bool3)
        {
          ((com.truecaller.truepay.app.ui.transaction.views.c.b)d).a((c)localObject2, bool1);
          return;
        }
        localObject3 = (com.truecaller.truepay.app.ui.transaction.views.c.b)d;
        ((com.truecaller.truepay.app.ui.transaction.views.c.b)localObject3).e();
        ((com.truecaller.truepay.app.ui.transaction.c.b)localObject1).a((com.truecaller.truepay.app.ui.transaction.b.b)localObject2, bool1);
      }
    }
  }
  
  private void f()
  {
    b.a locala = s;
    if (locala != null) {
      locala.hideProgress();
    }
  }
  
  public final int a()
  {
    return R.layout.fragment_add_beneficiary;
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = (TextInputLayout)c.get(0);
    boolean bool = true;
    ((TextInputLayout)localObject1).setErrorEnabled(bool);
    if (paramInt != 0)
    {
      localObject1 = (TextInputLayout)c.get(0);
      localObject2 = getString(paramInt);
      ((TextInputLayout)localObject1).setError((CharSequence)localObject2);
      return;
    }
    Object localObject2 = (TextInputLayout)c.get(0);
    int m = R.string.add_benfy_error_msg_benfy_name_empty;
    localObject1 = getString(m);
    ((TextInputLayout)localObject2).setError((CharSequence)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    TextInputEditText localTextInputEditText = (TextInputEditText)d.get(0);
    String str = d;
    localTextInputEditText.setText(str);
    localTextInputEditText = (TextInputEditText)d.get(1);
    parama = e;
    localTextInputEditText.setText(parama);
    ((TextInputEditText)b.get(0)).requestFocus();
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.b paramb, boolean paramBoolean)
  {
    e();
    q.a(paramb, paramBoolean);
  }
  
  public final void a(c paramc, boolean paramBoolean)
  {
    paramc = a.a(paramc, paramBoolean);
    paramc.setTargetFragment(this, 1014);
    o localo = getFragmentManager().a();
    String str = a.class.getSimpleName();
    localo.a(paramc, str).d();
  }
  
  public final void a(ag paramag)
  {
    paramag = au.a(b, false);
    TextInputLayout localTextInputLayout = (TextInputLayout)e.get(0);
    int m = 1;
    localTextInputLayout.setErrorEnabled(m);
    localTextInputLayout = (TextInputLayout)e.get(0);
    int i1 = R.style.TextAppearence_TextInputLayout_BlueGrey;
    localTextInputLayout.setErrorTextAppearance(i1);
    localTextInputLayout = (TextInputLayout)e.get(0);
    i1 = R.string.name_add_beneficiary;
    Object[] arrayOfObject = new Object[m];
    arrayOfObject[0] = paramag;
    String str = getString(i1, arrayOfObject);
    localTextInputLayout.setError(str);
    t.a((View)e.get(m), m);
    ((TextInputEditText)d.get(0)).setText(paramag);
    ((TextInputEditText)f.get(m)).setText(paramag);
    t.a(l, m);
    t.a(n, m);
    h.setVisibility(8);
    p.setVisibility(0);
  }
  
  public final void a(String paramString)
  {
    TextInputLayout localTextInputLayout = (TextInputLayout)e.get(0);
    boolean bool = true;
    localTextInputLayout.setErrorEnabled(bool);
    localTextInputLayout = (TextInputLayout)e.get(0);
    int m = R.style.TextAppearence_TextInputLayout_Error;
    localTextInputLayout.setErrorTextAppearance(m);
    localTextInputLayout = (TextInputLayout)e.get(0);
    m = R.string.invalid_vpa;
    Object[] arrayOfObject = new Object[bool];
    arrayOfObject[0] = paramString;
    paramString = getString(m, arrayOfObject);
    localTextInputLayout.setError(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    int m = 8;
    if (paramBoolean)
    {
      o.setVisibility(0);
      h.setVisibility(m);
      return;
    }
    o.setVisibility(m);
    h.setVisibility(0);
  }
  
  public final void b()
  {
    new String[1][0] = "invalid beneficiary type";
  }
  
  public final void b(int paramInt)
  {
    TextInputLayout localTextInputLayout = (TextInputLayout)a.get(0);
    boolean bool = true;
    localTextInputLayout.setErrorEnabled(bool);
    String str;
    if (paramInt != 0)
    {
      str = getString(paramInt);
    }
    else
    {
      paramInt = R.string.add_benfy_error_msg_bank_acc_empty;
      str = getString(paramInt);
    }
    ((TextInputLayout)a.get(0)).setError(str);
  }
  
  public final void b(com.truecaller.truepay.app.ui.transaction.b.b paramb, boolean paramBoolean)
  {
    int m = R.string.add_benfy_success_msg;
    Object[] arrayOfObject = new Object[1];
    String str1 = f;
    arrayOfObject[0] = str1;
    String str2 = getString(m, arrayOfObject);
    arrayOfObject = null;
    a(str2, null);
    if (paramBoolean)
    {
      paramBoolean = t;
      b.a locala;
      switch (paramBoolean)
      {
      default: 
        paramb = "Invalid req type";
        new String[1][0] = paramb;
        break;
      case 1004: 
        locala = s;
        locala.onPaySavedBeneficiary(paramb);
        break;
      case 1003: 
        locala = s;
        locala.onRequestSavedBeneficiary(paramb);
        break;
      }
    }
    else
    {
      paramb = s;
      paramb.onSaveBeneficiarySucessful();
    }
    f();
  }
  
  public final void c()
  {
    int m = R.string.add_beneficiary_save_failure;
    String str = getString(m);
    a(str, null);
    f();
  }
  
  public final void c(int paramInt)
  {
    Object localObject = a;
    int m = 1;
    localObject = (TextInputLayout)((List)localObject).get(m);
    ((TextInputLayout)localObject).setErrorEnabled(m);
    String str;
    if (paramInt != 0)
    {
      str = getString(paramInt);
    }
    else
    {
      paramInt = R.string.add_benfy_error_msg_ifsc_invalid;
      str = getString(paramInt);
    }
    ((TextInputLayout)a.get(m)).setError(str);
  }
  
  public final void d()
  {
    int m = R.string.failed_validating_upi_id;
    String str = getString(m);
    a(str, null);
  }
  
  public final void d(int paramInt)
  {
    TextInputLayout localTextInputLayout = (TextInputLayout)e.get(0);
    boolean bool = true;
    localTextInputLayout.setErrorEnabled(bool);
    if (paramInt == 0) {
      paramInt = R.string.add_benfy_error_msg_upi_id_invalid;
    }
    String str = getString(paramInt);
    ((TextInputLayout)e.get(0)).setError(str);
  }
  
  public final void e()
  {
    b.a locala = s;
    if (locala != null)
    {
      locala.showProgress();
      locala = null;
      com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity.isShowingProgress = false;
    }
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int m = 1006;
    if (paramInt1 == m)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        Uri localUri = paramIntent.getData();
        Object localObject1 = "display_name";
        String[] arrayOfString = { "data4", localObject1 };
        paramInt1 = 0;
        Cursor localCursor = null;
        try
        {
          localObject1 = getActivity();
          localObject1 = ((android.support.v4.app.f)localObject1).getApplicationContext();
          Object localObject3 = ((Context)localObject1).getContentResolver();
          localCursor = ((ContentResolver)localObject3).query(localUri, arrayOfString, null, null, null);
          if (localCursor != null)
          {
            paramInt2 = localCursor.moveToFirst();
            if (paramInt2 != 0)
            {
              localObject1 = "data4";
              paramInt2 = localCursor.getColumnIndex((String)localObject1);
              paramIntent = "display_name";
              int i1 = localCursor.getColumnIndex(paramIntent);
              localObject1 = localCursor.getString(paramInt2);
              paramIntent = localCursor.getString(i1);
              localObject3 = d;
              int i3 = 0;
              localUri = null;
              localObject3 = ((List)localObject3).get(0);
              localObject3 = (TextInputEditText)localObject3;
              ((TextInputEditText)localObject3).setText(paramIntent);
              boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
              m = 1;
              if (!bool)
              {
                int i2 = ((String)localObject1).length();
                i3 = 10;
                if (i2 >= i3)
                {
                  i2 = ((String)localObject1).length() - i3;
                  i3 = ((String)localObject1).length();
                  localObject1 = ((String)localObject1).substring(i2, i3);
                  paramIntent = d;
                  paramIntent = paramIntent.get(m);
                  paramIntent = (TextInputEditText)paramIntent;
                  paramIntent.setText((CharSequence)localObject1);
                  break label308;
                }
              }
              localObject1 = d;
              localObject1 = ((List)localObject1).get(m);
              localObject1 = (TextInputEditText)localObject1;
              paramIntent = "";
              ((TextInputEditText)localObject1).setText(paramIntent);
            }
          }
          label308:
          return;
        }
        finally
        {
          if (localCursor != null) {
            localCursor.close();
          }
        }
      }
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = s;
    if (paramContext == null)
    {
      paramContext = getActivity();
      boolean bool = paramContext instanceof b.a;
      if (bool)
      {
        paramContext = (b.a)getActivity();
        s = paramContext;
        return;
      }
      paramContext = new java/lang/IllegalStateException;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("Parent should implement ");
      String str = b.a.class.getSimpleName();
      ((StringBuilder)localObject).append(str);
      localObject = ((StringBuilder)localObject).toString();
      paramContext.<init>((String)localObject);
      throw paramContext;
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    setHasOptionsMenu(true);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    q.b();
    s = null;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int m = paramMenuItem.getItemId();
    int i1 = 16908332;
    if (m == i1)
    {
      getActivity().onBackPressed();
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    Object localObject = Truepay.getApplicationComponent();
    paramBundle = paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a();
    paramBundle.a(this);
    int m = 2;
    localObject = new int[m];
    int i1 = R.id.til_bank_acc_number_frag_add_benfy;
    localObject[0] = i1;
    i1 = R.id.til_ifsc_number_frag_add_benfy;
    boolean bool1 = true;
    localObject[bool1] = i1;
    localObject = af.a(paramView, (int[])localObject);
    a = ((List)localObject);
    localObject = new int[m];
    i1 = R.id.tiet_bank_acc_number_frag_add_benfy;
    localObject[0] = i1;
    i1 = R.id.tiet_ifsc_number_frag_add_benfy;
    localObject[bool1] = i1;
    localObject = af.a(paramView, (int[])localObject);
    b = ((List)localObject);
    localObject = new int[m];
    i1 = R.id.til_beneficiary_name_frag_add_benfy;
    localObject[0] = i1;
    i1 = R.id.til_mobile_number_frag_add_benfy;
    localObject[bool1] = i1;
    localObject = af.a(paramView, (int[])localObject);
    c = ((List)localObject);
    localObject = new int[m];
    i1 = R.id.tiet_beneficiary_name_frag_add_benfy;
    localObject[0] = i1;
    i1 = R.id.tiet_mobile__number_frag_add_benfy;
    localObject[bool1] = i1;
    localObject = af.a(paramView, (int[])localObject);
    d = ((List)localObject);
    localObject = new int[m];
    i1 = R.id.til_upi_id_frag_add_benfy;
    localObject[0] = i1;
    i1 = R.id.til_upi_nickname_frag_add_benfy;
    localObject[bool1] = i1;
    localObject = af.a(paramView, (int[])localObject);
    e = ((List)localObject);
    localObject = new int[m];
    i1 = R.id.tiet_upi_id_frag_add_benfy;
    localObject[0] = i1;
    i1 = R.id.tiet_upi_nickname_frag_add_benfy;
    localObject[bool1] = i1;
    localObject = af.a(paramView, (int[])localObject);
    f = ((List)localObject);
    int i2 = R.id.til_upi_nickname_frag_add_benfy;
    localObject = (TextInputLayout)paramView.findViewById(i2);
    g = ((TextInputLayout)localObject);
    i2 = R.id.tv_verify_upi_id_frag_add_benfy;
    localObject = (TextView)paramView.findViewById(i2);
    h = ((TextView)localObject);
    i2 = R.id.tv_find_ifsc_frag_add_benfy;
    localObject = (TextView)paramView.findViewById(i2);
    i = ((TextView)localObject);
    i2 = R.id.iv_select_contact_frag_add_benfy;
    localObject = (ImageView)paramView.findViewById(i2);
    j = ((ImageView)localObject);
    i2 = R.id.toolbar_frag_add_benfy;
    localObject = (Toolbar)paramView.findViewById(i2);
    k = ((Toolbar)localObject);
    i2 = R.id.btn_next_frag_add_benfy;
    localObject = (Button)paramView.findViewById(i2);
    l = ((Button)localObject);
    i2 = R.id.btn_save_frag_add_benfy;
    localObject = (Button)paramView.findViewById(i2);
    n = ((Button)localObject);
    i2 = R.id.pb_verify_upi_id_frag_add_benfy;
    localObject = (ProgressBar)paramView.findViewById(i2);
    o = ((ProgressBar)localObject);
    i2 = R.id.iv_verified_upi_id_frag_add_benfy;
    paramView = (ImageView)paramView.findViewById(i2);
    p = paramView;
    paramView = n;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$b$fJH9z-kBa5j5dxM-eOHPWKUi9EQ;
    ((-..Lambda.b.fJH9z-kBa5j5dxM-eOHPWKUi9EQ)localObject).<init>(this);
    paramView.setOnClickListener((View.OnClickListener)localObject);
    paramView = l;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$b$bup7RElgXb6uO9FvHx1zGbfhp8A;
    ((-..Lambda.b.bup7RElgXb6uO9FvHx1zGbfhp8A)localObject).<init>(this);
    paramView.setOnClickListener((View.OnClickListener)localObject);
    paramView = h;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$b$rjcDQH-dvITZMhiXETSNswaYECw;
    ((-..Lambda.b.rjcDQH-dvITZMhiXETSNswaYECw)localObject).<init>(this);
    paramView.setOnClickListener((View.OnClickListener)localObject);
    paramView = j;
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$b$mEofTs1MPFZrZs-imQRxzyvez8U;
    ((-..Lambda.b.mEofTs1MPFZrZs-imQRxzyvez8U)localObject).<init>(this);
    paramView.setOnClickListener((View.OnClickListener)localObject);
    q.a(this);
    paramView = (com.truecaller.truepay.app.ui.base.views.a.b)getActivity();
    localObject = k;
    paramView.setSupportActionBar((Toolbar)localObject);
    ((com.truecaller.truepay.app.ui.base.views.a.b)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(bool1);
    paramView = ((com.truecaller.truepay.app.ui.base.views.a.b)getActivity()).getSupportActionBar();
    i2 = R.drawable.ic_arrow_back_white_24dp;
    paramView.setHomeAsUpIndicator(i2);
    int i3 = getArguments().getInt("tranx_type");
    t = i3;
    paramView = getArguments();
    localObject = "add_benefy_contact";
    boolean bool2 = paramView.containsKey((String)localObject);
    if (bool2)
    {
      paramView = getArguments();
      localObject = "add_benefy_contact";
      paramView = (com.truecaller.truepay.app.ui.transaction.b.a)paramView.getSerializable((String)localObject);
      v = paramView;
    }
    paramView = getArguments();
    if (paramView != null)
    {
      paramView = getArguments();
      localObject = "type";
      paramView = paramView.getString((String)localObject);
      if (paramView != null)
      {
        af.a(a, false);
        af.a(b, false);
        af.a(e, false);
        af.a(f, false);
        af.a(d, false);
        af.a(c, false);
        t.a(i, false);
        t.a(h, false);
        t.a(j, false);
        paramView = getArguments();
        localObject = "type";
        paramView = paramView.getString((String)localObject);
        i2 = -1;
        i1 = paramView.hashCode();
        int i5 = -1177318867;
        String str;
        if (i1 != i5)
        {
          i5 = 116967;
          if (i1 == i5)
          {
            str = "vpa";
            bool2 = paramView.equals(str);
            if (bool2) {
              i2 = 1;
            }
          }
        }
        else
        {
          str = "account";
          bool2 = paramView.equals(str);
          if (bool2)
          {
            i2 = 0;
            localObject = null;
          }
        }
        switch (i2)
        {
        default: 
          paramView = new String[bool1];
          paramBundle = new java/lang/StringBuilder;
          paramBundle.<init>("Error choosing beneficiary type for addition- type rcvd ");
          localObject = getArguments();
          str = "type";
          localObject = ((Bundle)localObject).getString(str);
          paramBundle.append((String)localObject);
          paramBundle = paramBundle.toString();
          paramView[0] = paramBundle;
          break;
        case 1: 
          af.a(e, bool1);
          af.a(f, bool1);
          t.a(g, false);
          t.a(h, bool1);
          t.a(l, false);
          t.a(n, false);
          paramView = k;
          i2 = R.string.frag_add_benfy_title_upi_id;
          paramView.setTitle(i2);
          t.a((View)f.get(0), bool1, m);
          paramView = (TextInputEditText)f.get(0);
          paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/b$1;
          paramBundle.<init>(this);
          paramView.addTextChangedListener(paramBundle);
          break;
        case 0: 
          af.a(a, bool1);
          af.a(b, bool1);
          af.a(d, bool1);
          af.a(c, bool1);
          t.a(i, false);
          t.a(j, bool1);
          paramView = k;
          i2 = R.string.frag_add_benfy_title_bank_acc;
          paramView.setTitle(i2);
          paramView = (View)b.get(0);
          t.a(paramView, bool1, m);
        }
      }
    }
    int i4 = t;
    m = 1003;
    if (i4 == m)
    {
      paramView = l;
      m = R.string.frag_add_benfy_request_now_title;
      paramView.setText(m);
    }
    else
    {
      m = 1004;
      if (i4 == m)
      {
        paramView = l;
        m = R.string.frag_add_benfy_pay_now_title;
        paramView.setText(m);
      }
      else
      {
        paramView = "Invalid trnx type";
        new String[1][0] = paramView;
      }
    }
    paramView = q;
    paramBundle = v;
    if (paramBundle != null)
    {
      localObject = d;
      if (localObject != null)
      {
        paramView = (com.truecaller.truepay.app.ui.transaction.views.c.b)d;
        paramView.a(paramBundle);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */