package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.l;
import com.truecaller.truepay.app.utils.r;

final class ac
  extends c
{
  r b;
  
  public ac(f paramf, r paramr)
  {
    super(paramf);
    b = paramr;
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    l locall = new com/truecaller/truepay/app/ui/transaction/views/b/l;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext().getApplicationContext());
    int i = R.layout.item_personal_accounts;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    r localr = b;
    locall.<init>(paramViewGroup, (f)localObject, localr);
    return locall;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */