package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.j;

final class t
  extends c
{
  public t(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    j localj = new com/truecaller/truepay/app/ui/transaction/views/b/j;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_invite_contacts_list;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localj.<init>(paramViewGroup, (f)localObject);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */