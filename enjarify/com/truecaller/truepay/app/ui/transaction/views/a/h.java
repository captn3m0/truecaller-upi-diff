package com.truecaller.truepay.app.ui.transaction.views.a;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.e;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;
import c.n.m;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.a.b;
import com.truecaller.truepay.app.ui.transaction.views.c.c;
import java.io.Serializable;
import java.util.HashMap;

public final class h
  extends e
  implements c
{
  public static final h.a b;
  public com.truecaller.truepay.app.ui.transaction.c.k a;
  private h.b c;
  private HashMap d;
  
  static
  {
    h.a locala = new com/truecaller/truepay/app/ui/transaction/views/a/h$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final h c(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    c.g.b.k.b(parama, "contact");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    parama = (Serializable)parama;
    localBundle.putSerializable("selected_contact", parama);
    parama = new com/truecaller/truepay/app/ui/transaction/views/a/h;
    parama.<init>();
    parama.setArguments(localBundle);
    return parama;
  }
  
  public final com.truecaller.truepay.app.ui.transaction.c.k a()
  {
    com.truecaller.truepay.app.ui.transaction.c.k localk = a;
    if (localk == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return localk;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    c.g.b.k.b(parama, "contact");
    h.b localb = c;
    if (localb != null) {
      localb.inviteContact(parama);
    }
    dismiss();
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "name");
    int i = R.id.tvInviteViaWhatsAppSubTitle;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "tvInviteViaWhatsAppSubTitle");
    int j = R.string.invite_popup_send_invitation_text;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    paramString = (CharSequence)getString(j, arrayOfObject);
    localTextView.setText(paramString);
  }
  
  public final void b(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    c.g.b.k.b(parama, "contact");
    h.b localb = c;
    if (localb != null) {
      localb.showAddBeneficiaryUsingAccNumberIfsc(parama);
    }
    dismiss();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/app/Dialog;
    Object localObject = (Context)getActivity();
    int i = R.style.Base_ThemeOverlay_AppCompat_Dialog;
    paramBundle.<init>((Context)localObject, i);
    int j = 1;
    paramBundle.requestWindowFeature(j);
    i = R.layout.fragment_dialog_contact_invite;
    paramBundle.setContentView(i);
    paramBundle.setCanceledOnTouchOutside(j);
    localObject = paramBundle.getWindow();
    c.g.b.k.a(localObject, "dialog.window");
    localObject = ((Window)localObject).getAttributes();
    gravity = 17;
    width = -1;
    height = -2;
    Window localWindow = paramBundle.getWindow();
    c.g.b.k.a(localWindow, "dialog.window");
    localWindow.setAttributes((WindowManager.LayoutParams)localObject);
    return paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_dialog_contact_invite;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    c = null;
    com.truecaller.truepay.app.ui.transaction.c.k localk = a;
    if (localk == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localk.b();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    c.g.b.k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      c.g.b.k.a(paramBundle);
    }
    paramBundle = this;
    paramBundle = (com.truecaller.truepay.app.ui.base.views.a)this;
    paramView.a(paramBundle);
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = "selected_contact";
      paramView = paramView.getSerializable(paramBundle);
      if (paramView != null)
      {
        paramView = (com.truecaller.truepay.app.ui.transaction.b.a)paramView;
        paramBundle = a;
        if (paramBundle == null)
        {
          localObject = "presenter";
          c.g.b.k.a((String)localObject);
        }
        localObject = "contact";
        c.g.b.k.b(paramView, (String)localObject);
        a = paramView;
        paramView = a;
        if (paramView != null)
        {
          localObject = (CharSequence)paramView.a();
          if (localObject != null)
          {
            bool1 = m.a((CharSequence)localObject);
            if (!bool1)
            {
              bool1 = false;
              localObject = null;
              break label144;
            }
          }
          boolean bool1 = true;
          label144:
          if (!bool1)
          {
            paramBundle = (c)paramBundle.af_();
            if (paramBundle != null)
            {
              paramView = paramView.a();
              localObject = "it.name";
              c.g.b.k.a(paramView, (String)localObject);
              paramBundle.a(paramView);
            }
          }
        }
      }
      else
      {
        paramView = new c/u;
        paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.transaction.models.ActiveContactsListItem");
        throw paramView;
      }
    }
    int i = R.id.containerSendMoney;
    paramView = (ConstraintLayout)a(i);
    if (paramView != null)
    {
      paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/h$c;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
    }
    i = R.id.containerInviteViaWhatsapp;
    paramView = (ConstraintLayout)a(i);
    if (paramView != null)
    {
      paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/h$d;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
    }
    paramView = c;
    if (paramView == null)
    {
      paramView = getActivity();
      boolean bool2 = paramView instanceof h.b;
      if (bool2)
      {
        paramView = getActivity();
        if (paramView != null)
        {
          paramView = (h.b)paramView;
          c = paramView;
          return;
        }
        paramView = new c/u;
        paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.transaction.views.fragments.InviteOptionDialogFragment.InviteContactInteractionListener");
        throw paramView;
      }
      paramView = new java/lang/RuntimeException;
      paramBundle = new java/lang/StringBuilder;
      paramBundle.<init>("Parent should implement ");
      localObject = h.b.class.getSimpleName();
      paramBundle.append((String)localObject);
      paramBundle = paramBundle.toString();
      paramView.<init>(paramBundle);
      throw ((Throwable)paramView);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */