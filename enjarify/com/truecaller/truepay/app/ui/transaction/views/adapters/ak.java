package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.n;
import com.truecaller.truepay.app.ui.transaction.views.b.o;
import java.util.List;

final class ak
  extends c
{
  public ak(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    o localo = new com/truecaller/truepay/app/ui/transaction/views/b/o;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_search_result_contacts_aadhaar;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localo.<init>(paramViewGroup, (f)localObject);
    return localo;
  }
  
  public final boolean a(Object paramObject, List paramList)
  {
    return paramObject instanceof n;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */