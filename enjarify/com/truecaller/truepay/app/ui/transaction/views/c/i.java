package com.truecaller.truepay.app.ui.transaction.views.c;

import com.truecaller.truepay.app.ui.base.views.a;
import java.util.List;

public abstract interface i
  extends a
{
  public abstract void a(String paramString);
  
  public abstract void a(Throwable paramThrowable);
  
  public abstract void a(List paramList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b(String paramString);
  
  public abstract void c();
  
  public abstract void c(String paramString);
  
  public abstract void d();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.c.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */