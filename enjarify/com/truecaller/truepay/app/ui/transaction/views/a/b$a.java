package com.truecaller.truepay.app.ui.transaction.views.a;

import com.truecaller.truepay.app.ui.transaction.b.b;

public abstract interface b$a
{
  public abstract void hideProgress();
  
  public abstract void onPaySavedBeneficiary(b paramb);
  
  public abstract void onRequestSavedBeneficiary(b paramb);
  
  public abstract void onSaveBeneficiarySucessful();
  
  public abstract void showProgress();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */