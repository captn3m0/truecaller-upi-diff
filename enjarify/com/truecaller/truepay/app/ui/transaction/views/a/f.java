package com.truecaller.truepay.app.ui.transaction.views.a;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.j;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.c.m;
import com.truecaller.truepay.app.ui.transaction.views.adapters.aq;
import com.truecaller.truepay.app.ui.transaction.views.adapters.ar;
import com.truecaller.truepay.app.ui.transaction.views.c.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class f
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements c.a, g.b, ar, d
{
  RecyclerView a;
  View b;
  public m c;
  f.a d;
  private aq e;
  
  public static f d()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    f localf = new com/truecaller/truepay/app/ui/transaction/views/a/f;
    localf.<init>();
    localf.setArguments(localBundle);
    return localf;
  }
  
  public final int a()
  {
    return R.layout.fragment_collect_vpa;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    d.onBeneficiaryClicked(paramb);
  }
  
  public final void a(Throwable paramThrowable)
  {
    int i = R.string.fetch_beneficiaries_failure;
    String str = getString(i);
    a(str, paramThrowable);
  }
  
  public final void a(List paramList)
  {
    b.setVisibility(8);
    Object localObject = a;
    boolean bool1 = false;
    com.truecaller.truepay.app.ui.transaction.b.b localb = null;
    ((RecyclerView)localObject).setVisibility(0);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localb = (com.truecaller.truepay.app.ui.transaction.b.b)paramList.next();
      String str1 = g;
      String str2 = "vpa";
      boolean bool2 = str1.equalsIgnoreCase(str2);
      if (bool2) {
        ((List)localObject).add(localb);
      }
    }
    paramList = new java/util/ArrayList;
    paramList.<init>();
    paramList.addAll((Collection)localObject);
    e.a(paramList);
    e.notifyDataSetChanged();
  }
  
  public final void b() {}
  
  public final void b(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    paramb = g.b(paramb);
    paramb.setTargetFragment(this, 1001);
    j localj = getFragmentManager();
    String str = g.class.getSimpleName();
    paramb.show(localj, str);
  }
  
  public final void b(Throwable paramThrowable)
  {
    int i = R.string.delete_beneficiaries_failure;
    String str = getString(i);
    a(str, paramThrowable);
  }
  
  public final void c() {}
  
  public final void c(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    int i = R.string.deleted_beneficiary;
    Object[] arrayOfObject = new Object[1];
    paramb = f;
    arrayOfObject[0] = paramb;
    paramb = getString(i, arrayOfObject);
    a(paramb, null);
    c.a();
  }
  
  public final void c(String paramString)
  {
    d.onAddBeneficiaryClicked(paramString);
  }
  
  public final void d(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    m localm = c;
    paramb = i;
    localm.a(paramb);
  }
  
  public final void e()
  {
    a.setVisibility(8);
    b.setVisibility(0);
    ((List)e.a()).clear();
    e.notifyDataSetChanged();
  }
  
  public final void f() {}
  
  public final void g() {}
  
  public final void h() {}
  
  public final void i() {}
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = d;
    if (paramContext == null)
    {
      paramContext = getActivity();
      boolean bool = paramContext instanceof f.a;
      if (bool)
      {
        paramContext = (f.a)getActivity();
        d = paramContext;
        return;
      }
    }
    paramContext = new java/lang/IllegalStateException;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Parent must implement ");
    String str = f.a.class.getSimpleName();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    paramContext.<init>((String)localObject);
    throw paramContext;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    d = null;
    c.b();
  }
  
  public final void onResume()
  {
    super.onResume();
    c.a();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    Object localObject = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a().a(this);
    int i = R.id.rv_vpa_list_frag_collect_vpa;
    paramBundle = (RecyclerView)paramView.findViewById(i);
    a = paramBundle;
    i = R.id.empty_state_fragment_collect_vpa;
    paramBundle = paramView.findViewById(i);
    b = paramBundle;
    i = R.id.btn_add_accounts_frag_collect_vpa;
    paramView = paramView.findViewById(i);
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$f$qirvbM0zXEpMnzxfj72Q8Sqd_G0;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = a;
    paramBundle = new android/support/v7/widget/LinearLayoutManager;
    localObject = getActivity();
    paramBundle.<init>((Context)localObject, 1, false);
    paramView.setLayoutManager(paramBundle);
    paramView = new com/truecaller/truepay/app/ui/transaction/views/adapters/aq;
    paramView.<init>(this);
    e = paramView;
    paramView = a;
    paramBundle = e;
    paramView.setAdapter(paramBundle);
    c.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */