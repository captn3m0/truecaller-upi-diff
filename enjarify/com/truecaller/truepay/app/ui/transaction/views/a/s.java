package com.truecaller.truepay.app.ui.transaction.views.a;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.base.views.fragments.a;

public class s
  extends a
{
  s.a a;
  TextView b;
  
  public static s a(String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("selected_msisdn", paramString);
    paramString = new com/truecaller/truepay/app/ui/transaction/views/a/s;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  public final int a()
  {
    return R.layout.fragment_vpa_unresolvable_dialog;
  }
  
  public final void b()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = getTargetFragment();
      boolean bool = localObject1 instanceof s.a;
      if (bool)
      {
        localObject1 = (s.a)getTargetFragment();
        a = ((s.a)localObject1);
        return;
      }
    }
    localObject1 = new java/lang/RuntimeException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("parent fragment should implement ");
    String str = s.a.class.getSimpleName();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((RuntimeException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public void onCancel(DialogInterface paramDialogInterface)
  {
    super.onCancel(paramDialogInterface);
    a.i();
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.tv_title_frag_vpa_unresolvable;
    paramBundle = (TextView)paramView.findViewById(i);
    b = paramBundle;
    i = R.id.btn_invite_frag_vpa_unresolvable;
    paramBundle = paramView.findViewById(i);
    Object localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$s$nBSJzITj-9dTe9UbjSxnQR0KD8g;
    ((-..Lambda.s.nBSJzITj-9dTe9UbjSxnQR0KD8g)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.btn_cancel_frag_vpa_unresolvable;
    paramView = paramView.findViewById(i);
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$s$VvKJBkbPxRqJz09pB9FNqcpBBTc;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = b;
    i = R.string.vpa_unresolvable_title;
    localObject = new Object[1];
    String str = getArguments().getString("selected_msisdn", "This number");
    localObject[0] = str;
    paramBundle = getString(i, (Object[])localObject);
    paramView.setText(paramBundle);
    getDialog().setCancelable(false);
    getDialog().setCanceledOnTouchOutside(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */