package com.truecaller.truepay.app.ui.transaction.views.b;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.base.views.c.a;

public final class h
  extends a
  implements View.OnClickListener
{
  public TextView b;
  public TextView c;
  public ImageView d;
  
  public h(View paramView, f paramf)
  {
    super(paramView, paramf);
    int i = R.id.tv_name_item_inactive_contacts;
    paramf = (TextView)paramView.findViewById(i);
    b = paramf;
    i = R.id.tv_msisdn_item_inactive_contact;
    paramf = (TextView)paramView.findViewById(i);
    c = paramf;
    i = R.id.iv_profile_pic_item_inactive_contact;
    paramView = (ImageView)paramView.findViewById(i);
    d = paramView;
  }
  
  public final void onClick(View paramView)
  {
    paramView = (i)a;
    int i = getAdapterPosition();
    paramView.c(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */