package com.truecaller.truepay.app.ui.transaction.views.a;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.R.id;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.e;
import android.support.v4.app.f;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class m$a
  extends e
  implements DatePickerDialog.OnDateSetListener
{
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = Calendar.getInstance();
    long l1 = System.currentTimeMillis();
    paramBundle.setTimeInMillis(l1);
    int i = paramBundle.get(1);
    int j = paramBundle.get(2);
    int k = paramBundle.get(5);
    paramBundle = new android/app/DatePickerDialog;
    f localf = getActivity();
    int m = R.style.DatePickerStyle;
    paramBundle.<init>(localf, m, this, i, j, k);
    DatePicker localDatePicker = paramBundle.getDatePicker();
    long l2 = System.currentTimeMillis() + 2073600000L;
    localDatePicker.setMaxDate(l2);
    l2 = System.currentTimeMillis();
    localDatePicker.setMinDate(l2);
    return paramBundle;
  }
  
  public void onDateSet(DatePicker paramDatePicker, int paramInt1, int paramInt2, int paramInt3)
  {
    paramDatePicker = Calendar.getInstance();
    paramDatePicker.set(paramInt1, paramInt2, paramInt3);
    Object localObject1 = new java/text/SimpleDateFormat;
    Object localObject2 = Locale.ENGLISH;
    ((SimpleDateFormat)localObject1).<init>("dd-MMMM", (Locale)localObject2);
    Object localObject3 = paramDatePicker.getTime();
    m.f(((SimpleDateFormat)localObject1).format((Date)localObject3));
    int i = R.string.collect_expiry_message;
    localObject1 = getString(i);
    localObject3 = new Object[1];
    localObject2 = m.j();
    localObject3[0] = localObject2;
    localObject1 = String.format((String)localObject1, (Object[])localObject3);
    localObject3 = getActivity().findViewById(16908290);
    int j = -1;
    localObject1 = Snackbar.a((View)localObject3, (CharSequence)localObject1, j);
    localObject3 = ((Snackbar)localObject1).b();
    int k = R.id.snackbar_text;
    localObject3 = (TextView)((View)localObject3).findViewById(k);
    ((TextView)localObject3).setTextColor(j);
    int m = 2000;
    d = m;
    localObject1 = (Snackbar)localObject1;
    ((Snackbar)localObject1).c();
    long l1 = paramDatePicker.getTimeInMillis();
    long l2 = System.currentTimeMillis();
    l1 = Math.abs(l1 - l2);
    long l3 = 60000L;
    m.a(l1 / l3);
    l1 = m.k();
    l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool)
    {
      int n = 23;
      int i1 = 59;
      int i2 = 59;
      localObject1 = paramDatePicker;
      m = paramInt1;
      j = paramInt2;
      k = paramInt3;
      paramDatePicker.set(paramInt1, paramInt2, paramInt3, n, i1, i2);
      long l4 = paramDatePicker.getTimeInMillis();
      long l5 = System.currentTimeMillis();
      l4 = Math.abs(l4 - l5) / l3;
      m.a(l4);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.m.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */