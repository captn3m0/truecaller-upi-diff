package com.truecaller.truepay.app.ui.transaction.views.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.transaction.views.b.r;
import com.truecaller.truepay.data.api.model.ag;
import java.util.List;

final class al
  extends c
{
  public al(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    r localr = new com/truecaller/truepay/app/ui/transaction/views/b/r;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_search_result_resolved_vpa;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    localr.<init>(paramViewGroup, (f)localObject);
    return localr;
  }
  
  public final boolean a(Object paramObject, List paramList)
  {
    return paramObject instanceof ag;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.adapters.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */