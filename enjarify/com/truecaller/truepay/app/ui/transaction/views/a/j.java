package com.truecaller.truepay.app.ui.transaction.views.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.v4.app.f;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.b.c;
import com.truecaller.truepay.app.ui.registration.views.activities.AccountConnectionActivity;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.c.l.b;
import com.truecaller.truepay.app.ui.transaction.c.o;
import com.truecaller.truepay.app.ui.transaction.c.o.b;
import com.truecaller.truepay.app.ui.transaction.views.adapters.ab;
import com.truecaller.truepay.app.ui.transaction.views.adapters.ad;
import com.truecaller.truepay.app.ui.transaction.views.c.d;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.List;
import kotlinx.coroutines.e;

public final class j
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements l.b, c.a, g.b, ad, com.truecaller.truepay.app.ui.transaction.views.adapters.k, d
{
  RecyclerView a;
  RecyclerView b;
  View c;
  Group d;
  Group e;
  View f;
  TextView g;
  ConstraintLayout h;
  ab i;
  com.truecaller.truepay.app.ui.transaction.views.adapters.j j;
  public com.truecaller.truepay.app.ui.transaction.c.m k;
  public o l;
  public com.truecaller.truepay.app.utils.a n;
  public r o;
  j.a p;
  
  public static j j()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    j localj = new com/truecaller/truepay/app/ui/transaction/views/a/j;
    localj.<init>();
    localj.setArguments(localBundle);
    return localj;
  }
  
  private void k()
  {
    Object localObject = getActivity();
    boolean bool = localObject instanceof j.a;
    if (bool)
    {
      localObject = (j.a)getActivity();
      p = ((j.a)localObject);
      return;
    }
    localObject = new java/lang/RuntimeException;
    ((RuntimeException)localObject).<init>("implement frag interaction listener in parent activity");
    throw ((Throwable)localObject);
  }
  
  private void l()
  {
    Intent localIntent = new android/content/Intent;
    Object localObject = getActivity();
    localIntent.<init>((Context)localObject, AccountConnectionActivity.class);
    localObject = new android/os/Bundle;
    ((Bundle)localObject).<init>();
    ((Bundle)localObject).putSerializable("action", "add_account");
    localIntent.putExtras((Bundle)localObject);
    startActivityForResult(localIntent, 2002);
  }
  
  public final int a()
  {
    return R.layout.fragment_pay_beneficiaries;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    paramb = g.b(paramb);
    paramb.setTargetFragment(this, 1001);
    android.support.v4.app.j localj = getFragmentManager();
    String str = g.class.getSimpleName();
    paramb.show(localj, str);
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    p.onFetchingOwnAccountVPA(parama);
  }
  
  public final void a(String paramString)
  {
    g.setText(paramString);
  }
  
  public final void a(Throwable paramThrowable)
  {
    int m = R.string.fetch_beneficiaries_failure;
    String str = getString(m);
    a(str, paramThrowable);
  }
  
  public final void a(List paramList)
  {
    b.setVisibility(0);
    c.setVisibility(8);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localArrayList.addAll(paramList);
    j.a(localArrayList);
    j.notifyDataSetChanged();
  }
  
  public final void a_(List paramList)
  {
    i.a(paramList);
    i.notifyDataSetChanged();
  }
  
  public final void b()
  {
    Group localGroup = d;
    int m = 8;
    localGroup.setVisibility(m);
    a.setVisibility(m);
  }
  
  public final void b(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    p.onBeneficiarySelected(paramb);
  }
  
  public final void b(com.truecaller.truepay.data.api.model.a parama)
  {
    o localo = l;
    c.g.b.k.b(parama, "account");
    l.b localb = (l.b)b;
    if (localb == null) {
      return;
    }
    Object localObject = e.a();
    int m = ((ArrayList)localObject).size();
    int i1 = 0;
    int i2 = 1;
    if (m > i2)
    {
      localObject = n;
      if (localObject != null)
      {
        localObject = n;
        String str = "account.ownAccountVpa";
        c.g.b.k.a(localObject, str);
        localObject = (CharSequence)localObject;
        m = ((CharSequence)localObject).length();
        if (m == 0) {
          i1 = 1;
        }
        if (i1 == 0)
        {
          localb.a(parama);
          return;
        }
      }
      localObject = new com/truecaller/truepay/app/ui/accounts/b/c;
      ((c)localObject).<init>();
      parama = a;
      a = parama;
      parama = new com/truecaller/truepay/app/ui/transaction/c/o$b;
      parama.<init>(localo, (c)localObject, localb, null);
      parama = (c.g.a.m)parama;
      e.b(localo, null, parama, 3);
      return;
    }
    parama = d;
    int i3 = R.string.text_error_minimum_accounts;
    localObject = new Object[0];
    parama = parama.a(i3, (Object[])localObject);
    c.g.b.k.a(parama, "resourceProvider.getStri…t_error_minimum_accounts)");
    localb.b(parama);
  }
  
  public final void b(String paramString)
  {
    Toast.makeText(getActivity(), paramString, 0).show();
  }
  
  public final void b(Throwable paramThrowable)
  {
    int m = R.string.delete_beneficiaries_failure;
    String str = getString(m);
    a(str, paramThrowable);
  }
  
  public final void c()
  {
    d.setVisibility(0);
    a.setVisibility(0);
  }
  
  public final void c(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    Context localContext = getActivity().getApplicationContext();
    Resources localResources = getResources();
    int m = R.string.deleted_beneficiary;
    Object[] arrayOfObject = new Object[1];
    paramb = f;
    arrayOfObject[0] = paramb;
    paramb = localResources.getString(m, arrayOfObject);
    Toast.makeText(localContext, paramb, 0).show();
    k.a();
  }
  
  public final void c(String paramString)
  {
    p.onAddBeneficiaryClicked(paramString);
  }
  
  public final void d()
  {
    c.setVisibility(8);
  }
  
  public final void d(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    com.truecaller.truepay.app.ui.transaction.c.m localm = k;
    paramb = i;
    localm.a(paramb);
  }
  
  public final void e() {}
  
  public final void f()
  {
    Group localGroup = e;
    int m = 8;
    localGroup.setVisibility(m);
    d.setVisibility(m);
    a.setVisibility(m);
    b.setVisibility(m);
    c.setVisibility(0);
  }
  
  public final void g()
  {
    e.setVisibility(8);
  }
  
  public final void h()
  {
    e.setVisibility(0);
  }
  
  public final void i()
  {
    b.setVisibility(8);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int m = 2002;
    if (paramInt1 == m)
    {
      paramInt1 = -1;
      if (paramInt2 != paramInt1)
      {
        String str = "Error adding acc";
        new String[1][0] = str;
      }
    }
  }
  
  public final void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    k();
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    k();
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    k.b();
  }
  
  public final void onDetach()
  {
    super.onDetach();
    p = null;
  }
  
  public final void onResume()
  {
    super.onResume();
    k.a();
    l.a();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    Object localObject = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a().a(this);
    int m = R.id.rv_personal_accounts_frag_pay_beneficiaries;
    paramBundle = (RecyclerView)paramView.findViewById(m);
    a = paramBundle;
    m = R.id.rv_beneficiaries_accounts_frag_pay_beneficiaries;
    paramBundle = (RecyclerView)paramView.findViewById(m);
    b = paramBundle;
    m = R.id.empty_state_benfys_frag_pay_beneficiaries;
    paramBundle = paramView.findViewById(m);
    c = paramBundle;
    m = R.id.group_personal_acc_pay_beneficiaries;
    paramBundle = (Group)paramView.findViewById(m);
    d = paramBundle;
    m = R.id.empty_state_personal_acc_frag_pay_beneficiaries;
    paramBundle = paramView.findViewById(m);
    f = paramBundle;
    m = R.id.viewAddAccount;
    paramBundle = (ConstraintLayout)paramView.findViewById(m);
    h = paramBundle;
    m = R.id.tvMobileNumber;
    paramBundle = (TextView)paramView.findViewById(m);
    g = paramBundle;
    m = R.id.groupBeneficiaries;
    paramBundle = (Group)paramView.findViewById(m);
    e = paramBundle;
    m = R.id.btn_add_accounts_frag_pay_beneficiaries;
    paramBundle = paramView.findViewById(m);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$j$H7XRkDEERwT9P0A8f4jNulFvBMU;
    ((-..Lambda.j.H7XRkDEERwT9P0A8f4jNulFvBMU)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    m = R.id.tv_dismiss_empty_acc_frag_pay_benfy;
    paramBundle = paramView.findViewById(m);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$j$o1QOrNgg2RlFTt9iFk_HJVK_imk;
    ((-..Lambda.j.o1QOrNgg2RlFTt9iFk_HJVK_imk)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    m = R.id.tv_add_acc_empty_acc_frag_pay_benfy;
    paramView = paramView.findViewById(m);
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$j$8jnKKep6bqK83Oc_Wm_vQgsx10c;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = h;
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$j$W89dYe-VRNPaq2GXdJFwr9QHhjY;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    k.a(this);
    l.a(this);
    paramView = a;
    paramBundle = new android/support/v7/widget/LinearLayoutManager;
    localObject = getActivity().getApplicationContext();
    int i1 = 1;
    paramBundle.<init>((Context)localObject, i1, false);
    paramView.setLayoutManager(paramBundle);
    a.setNestedScrollingEnabled(false);
    paramView = new com/truecaller/truepay/app/ui/transaction/views/adapters/ab;
    paramBundle = o;
    paramView.<init>(this, paramBundle);
    i = paramView;
    paramView = a;
    paramBundle = i;
    paramView.setAdapter(paramBundle);
    l.a();
    paramView = b;
    paramBundle = new android/support/v7/widget/LinearLayoutManager;
    localObject = getActivity();
    paramBundle.<init>((Context)localObject, i1, false);
    paramView.setLayoutManager(paramBundle);
    paramView = new com/truecaller/truepay/app/ui/transaction/views/adapters/j;
    paramView.<init>(this);
    j = paramView;
    b.setNestedScrollingEnabled(false);
    b.setHasFixedSize(i1);
    j.setHasStableIds(i1);
    paramView = b;
    paramBundle = j;
    paramView.setAdapter(paramBundle);
    paramView = new java/util/ArrayList;
    paramView.<init>();
    j.a(paramView);
    j.notifyDataSetChanged();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */