package com.truecaller.truepay.app.ui.transaction.views.a;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.c.l.b;
import com.truecaller.truepay.app.ui.payments.views.c.d;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.views.adapters.v;
import com.truecaller.truepay.app.ui.transaction.views.adapters.v.a;
import com.truecaller.truepay.app.ui.transaction.views.c.f;
import java.util.ArrayList;
import java.util.List;

public final class i
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements v.a
{
  public com.truecaller.truepay.app.utils.a a;
  RecyclerView b;
  Toolbar c;
  f d;
  public d e;
  l.b f;
  v g;
  public com.truecaller.truepay.data.e.a h;
  public com.truecaller.truepay.data.e.a i;
  
  public static i a(boolean paramBoolean1, boolean paramBoolean2)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putBoolean("isFromCollect", paramBoolean1);
    localBundle.putBoolean("is_merchant_txn", paramBoolean2);
    i locali = new com/truecaller/truepay/app/ui/transaction/views/a/i;
    locali.<init>();
    locali.setArguments(localBundle);
    return locali;
  }
  
  public final int a()
  {
    return R.layout.fragment_account_selection;
  }
  
  public final void a(l.b paramb)
  {
    f = paramb;
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    Object localObject = d;
    if (localObject != null)
    {
      ((f)localObject).a(parama);
      localObject = d;
      ((f)localObject).c();
    }
    localObject = e;
    if (localObject != null)
    {
      ((d)localObject).a(parama);
      localObject = e;
      ((d)localObject).h();
    }
    localObject = f;
    if (localObject != null)
    {
      ((l.b)localObject).a(parama);
      parama = f;
      parama.b();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int j = R.id.rv_account_list;
    paramBundle = (RecyclerView)paramView.findViewById(j);
    b = paramBundle;
    j = R.id.toolbar;
    paramView = (Toolbar)paramView.findViewById(j);
    c = paramView;
    paramView = (AppCompatActivity)getActivity();
    paramBundle = c;
    paramView.setSupportActionBar(paramBundle);
    paramView = ((AppCompatActivity)getActivity()).getSupportActionBar();
    paramBundle = getResources();
    int k = R.string.select_account;
    paramBundle = paramBundle.getString(k);
    paramView.setTitle(paramBundle);
    paramView = ((AppCompatActivity)getActivity()).getSupportActionBar();
    j = 1;
    paramView.setDisplayShowTitleEnabled(j);
    ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(j);
    b.setHasFixedSize(j);
    paramView = new android/support/v7/widget/LinearLayoutManager;
    Object localObject1 = getContext();
    paramView.<init>((Context)localObject1);
    b.setLayoutManager(paramView);
    paramView = new com/truecaller/truepay/app/ui/transaction/views/adapters/v;
    localObject1 = a.a();
    com.truecaller.truepay.data.api.model.a locala = new com/truecaller/truepay/data/api/model/a;
    locala.<init>();
    Object localObject2 = getArguments();
    boolean bool1 = ((Bundle)localObject2).getBoolean("isFromCollect");
    Object localObject3 = getArguments();
    boolean bool2 = ((Bundle)localObject3).getBoolean("is_merchant_txn");
    Object localObject4 = getArguments();
    String str1 = "selectedAccountId";
    localObject4 = ((Bundle)localObject4).getString(str1);
    if (localObject4 != null)
    {
      boolean bool3 = ((String)localObject4).isEmpty();
      if (!bool3)
      {
        localObject1 = a.a((String)localObject4);
        localObject4 = a;
        localObject1 = ((com.truecaller.truepay.app.utils.a)localObject4).b((com.truecaller.truepay.data.api.model.a)localObject1);
      }
    }
    localObject4 = h.a();
    boolean bool4 = ((Boolean)localObject4).booleanValue();
    if ((!bool4) && (!bool1) && (!bool2))
    {
      a = "pay_via_other";
      l = j;
      localObject2 = new com/truecaller/truepay/data/d/a;
      localObject4 = "";
      localObject3 = getResources();
      int m = R.string.pay_via_other;
      str1 = ((Resources)localObject3).getString(m);
      String str2 = "";
      String str3 = "upi";
      String str4 = "";
      localObject3 = localObject2;
      ((com.truecaller.truepay.data.d.a)localObject2).<init>((String)localObject4, str1, str2, str3, str4, false);
      j = ((com.truecaller.truepay.data.d.a)localObject2);
      f = j;
      e = j;
      ((ArrayList)localObject1).add(locala);
    }
    paramView.<init>((List)localObject1);
    g = paramView;
    paramView = g;
    c = this;
    b.setAdapter(paramView);
    paramView = c;
    paramBundle = new com/truecaller/truepay/app/ui/transaction/views/a/-$$Lambda$i$kH6uojeTtwZToiUkTV4XCKyKDO4;
    paramBundle.<init>(this);
    paramView.setNavigationOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */