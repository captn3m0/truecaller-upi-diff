package com.truecaller.truepay.app.ui.transaction.views.b;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.base.views.c.a;
import com.truecaller.truepay.app.ui.transaction.views.adapters.ap;

public final class t
  extends a
  implements View.OnClickListener
{
  public TextView b;
  public TextView c;
  ImageView d;
  
  public t(View paramView, f paramf)
  {
    super(paramView, paramf);
    int i = R.id.tv_name_item_benfy_vpa;
    paramf = (TextView)paramView.findViewById(i);
    b = paramf;
    i = R.id.tv_vpa_item_benfy_vpa;
    paramf = (TextView)paramView.findViewById(i);
    c = paramf;
    i = R.id.iv_profile_pic_item_benfy_vpa;
    paramf = (ImageView)paramView.findViewById(i);
    d = paramf;
    i = R.id.btn_delete_item_benfy_vpa;
    paramf = paramView.findViewById(i);
    if (paramf != null)
    {
      -..Lambda.t.CcbIdN2HJXGLf064JVuoQLrImBM localCcbIdN2HJXGLf064JVuoQLrImBM = new com/truecaller/truepay/app/ui/transaction/views/b/-$$Lambda$t$CcbIdN2HJXGLf064JVuoQLrImBM;
      localCcbIdN2HJXGLf064JVuoQLrImBM.<init>(this);
      paramf.setOnClickListener(localCcbIdN2HJXGLf064JVuoQLrImBM);
    }
    paramView.setOnClickListener(this);
  }
  
  public final void onClick(View paramView)
  {
    paramView = (ap)a;
    int i = getAdapterPosition();
    paramView.d(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.b.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */