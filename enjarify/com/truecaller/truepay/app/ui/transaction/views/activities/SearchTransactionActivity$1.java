package com.truecaller.truepay.app.ui.transaction.views.activities;

import android.text.Editable;
import android.text.TextWatcher;
import com.truecaller.truepay.app.ui.transaction.c.z;

final class SearchTransactionActivity$1
  implements TextWatcher
{
  SearchTransactionActivity$1(SearchTransactionActivity paramSearchTransactionActivity) {}
  
  public final void afterTextChanged(Editable paramEditable) {}
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    z localz = a.d;
    paramCharSequence = paramCharSequence.toString().trim();
    localz.a(paramCharSequence);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.activities.SearchTransactionActivity.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */