package com.truecaller.truepay.app.ui.transaction.views.a;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.utils.t;
import java.io.Serializable;
import java.util.HashMap;

public final class g
  extends com.truecaller.truepay.app.ui.base.views.fragments.a
  implements com.truecaller.truepay.app.ui.transaction.c.g.b
{
  public static final g.a b;
  public com.truecaller.truepay.app.ui.transaction.c.g.a a;
  private g.b c;
  private HashMap d;
  
  static
  {
    g.a locala = new com/truecaller/truepay/app/ui/transaction/views/a/g$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final g b(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    k.b(paramb, "beneficiaryAccount");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramb = (Serializable)paramb;
    localBundle.putSerializable("beneficiary_account", paramb);
    paramb = new com/truecaller/truepay/app/ui/transaction/views/a/g;
    paramb.<init>();
    paramb.setArguments(localBundle);
    return paramb;
  }
  
  public final int a()
  {
    return R.layout.fragment_dialog_delete_beneficiary;
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    k.b(paramb, "beneficiaryAccount");
    g.b localb = c;
    if (localb == null) {
      k.a();
    }
    localb.d(paramb);
    dismiss();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "name");
    int i = R.id.tvName;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvName");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b()
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = getTargetFragment();
      boolean bool = localObject instanceof g.b;
      if (bool)
      {
        localObject = (g.b)getTargetFragment();
        c = ((g.b)localObject);
        return;
      }
    }
    new String[1][0] = "Parent fragment does not implemenet listener";
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.tvSubText;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvSubText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c()
  {
    int i = R.id.btnCancel;
    Button localButton = (Button)a(i);
    Object localObject = new com/truecaller/truepay/app/ui/transaction/views/a/g$c;
    ((g.c)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    localButton.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.btnDelete;
    localButton = (Button)a(i);
    localObject = new com/truecaller/truepay/app/ui/transaction/views/a/g$d;
    ((g.d)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    localButton.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.tvTextTop;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvTextTop");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final com.truecaller.truepay.app.ui.transaction.b.b d()
  {
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "beneficiary_account";
      localObject = ((Bundle)localObject).getSerializable(str);
    }
    else
    {
      localObject = null;
    }
    if (localObject != null) {
      return (com.truecaller.truepay.app.ui.transaction.b.b)localObject;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.transaction.models.BeneficiaryAccount");
    throw ((Throwable)localObject);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.tvTextBottom;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvTextBottom");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void e()
  {
    int i = R.id.tvTextBottom;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvTextBottom");
    localTextView.setVisibility(8);
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "symbol");
    Context localContext = getContext();
    paramString = t.a(paramString, localContext);
    int i = R.id.imgBank;
    ((ImageView)a(i)).setImageDrawable(paramString);
  }
  
  public final void f()
  {
    int i = R.id.tvHeaderBottom;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvHeaderBottom");
    localTextView.setVisibility(8);
  }
  
  public final void g()
  {
    int i = R.id.tvTextTop;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvTextTop");
    localTextView.setVisibility(8);
  }
  
  public final void h()
  {
    int i = R.id.tvHeaderTop;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvHeaderTop");
    localTextView.setVisibility(8);
  }
  
  public final void i()
  {
    int i = R.id.tvName;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvName");
    localTextView.setVisibility(8);
  }
  
  public final void j()
  {
    int i = R.id.imgBank;
    ImageView localImageView = (ImageView)a(i);
    k.a(localImageView, "imgBank");
    localImageView.setVisibility(8);
  }
  
  public final void k()
  {
    g.b localb = c;
    if (localb == null) {
      k.a();
    }
    dismiss();
  }
  
  public final com.truecaller.truepay.app.ui.transaction.c.g.a l()
  {
    com.truecaller.truepay.app.ui.transaction.c.g.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.transaction.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    c = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */