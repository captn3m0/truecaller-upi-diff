package com.truecaller.truepay.app.ui.transaction.views.c;

import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.ui.transaction.views.a.i;

public abstract interface l
{
  public abstract void hideProgress();
  
  public abstract void onBackPressed();
  
  public abstract void onContactInvite(com.truecaller.truepay.app.ui.transaction.b.a parama);
  
  public abstract void onContactSelected(com.truecaller.truepay.app.ui.transaction.b.a parama, int paramInt);
  
  public abstract void returnToCallingIntent(p paramp);
  
  public abstract void showAccountChooser(i parami);
  
  public abstract void showInviteOptionPopup(com.truecaller.truepay.app.ui.transaction.b.a parama);
  
  public abstract void showNeedHelp(String paramString);
  
  public abstract void showPayConfirmation(p paramp);
  
  public abstract void showProgress();
  
  public abstract void showResetPin(com.truecaller.truepay.data.api.model.a parama, String paramString);
  
  public abstract void showSetPin(com.truecaller.truepay.data.api.model.a parama, String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.c.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */