package com.truecaller.truepay.app.ui.growth.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.i;
import android.arch.persistence.room.j;
import android.database.Cursor;
import c.g.b.k;
import java.util.ArrayList;
import java.util.List;

public final class c
  implements b
{
  final android.arch.persistence.room.f a;
  private final android.arch.persistence.room.c b;
  private final j c;
  
  public c(android.arch.persistence.room.f paramf)
  {
    a = paramf;
    Object localObject = new com/truecaller/truepay/app/ui/growth/db/c$1;
    ((c.1)localObject).<init>(this, paramf);
    b = ((android.arch.persistence.room.c)localObject);
    localObject = new com/truecaller/truepay/app/ui/growth/db/c$2;
    ((c.2)localObject).<init>(this, paramf);
    c = ((j)localObject);
  }
  
  public final int a(int paramInt)
  {
    android.arch.persistence.db.f localf = c.b();
    android.arch.persistence.room.f localf1 = a;
    localf1.d();
    int i = 1;
    long l = paramInt;
    try
    {
      localf.a(i, l);
      paramInt = localf.a();
      localf1 = a;
      localf1.f();
      return paramInt;
    }
    finally
    {
      a.e();
      c.a(localf);
    }
  }
  
  public final List a(int paramInt1, int paramInt2)
  {
    int i = 2;
    i locali = i.a("Select * from banners where expires_at > ? and type = ?", i);
    long l1 = paramInt1;
    paramInt1 = 1;
    locali.a(paramInt1, l1);
    long l2 = paramInt2;
    locali.a(i, l2);
    Cursor localCursor = a.a(locali);
    String str1 = "_id";
    try
    {
      paramInt2 = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "url";
      i = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "deep_link";
      int j = localCursor.getColumnIndexOrThrow(str3);
      String str4 = "expires_at";
      int k = localCursor.getColumnIndexOrThrow(str4);
      String str5 = "type";
      int m = localCursor.getColumnIndexOrThrow(str5);
      String str6 = "banner_id";
      int n = localCursor.getColumnIndexOrThrow(str6);
      ArrayList localArrayList = new java/util/ArrayList;
      int i1 = localCursor.getCount();
      localArrayList.<init>(i1);
      for (;;)
      {
        boolean bool = localCursor.moveToNext();
        if (!bool) {
          break;
        }
        a locala = new com/truecaller/truepay/app/ui/growth/db/a;
        locala.<init>();
        long l3 = localCursor.getLong(paramInt2);
        a = l3;
        String str7 = localCursor.getString(i);
        locala.a(str7);
        str7 = localCursor.getString(j);
        locala.b(str7);
        l3 = localCursor.getLong(k);
        d = l3;
        int i2 = localCursor.getInt(m);
        e = i2;
        str7 = localCursor.getString(n);
        locala.c(str7);
        localArrayList.add(locala);
      }
      return localArrayList;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final void a(int paramInt, List paramList)
  {
    a.d();
    String str = "tcPayGrowthBanner";
    try
    {
      k.b(paramList, str);
      a(paramInt);
      a(paramList);
      android.arch.persistence.room.f localf = a;
      localf.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
  
  public final long[] a(List paramList)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = b;
      paramList = ((android.arch.persistence.room.c)localObject).a(paramList);
      localObject = a;
      ((android.arch.persistence.room.f)localObject).f();
      return paramList;
    }
    finally
    {
      a.e();
    }
  }
  
  public final LiveData b(int paramInt)
  {
    int i = 2;
    i locali = i.a("Select * from banners where expires_at > ? and type = ?", i);
    long l = paramInt;
    locali.a(1, l);
    locali.a(i, 1L);
    c.3 local3 = new com/truecaller/truepay/app/ui/growth/db/c$3;
    local3.<init>(this, locali);
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.growth.db.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */