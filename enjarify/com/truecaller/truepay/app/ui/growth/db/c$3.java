package com.truecaller.truepay.app.ui.growth.db;

import android.arch.lifecycle.b;
import android.arch.persistence.db.e;
import android.arch.persistence.room.d;
import android.arch.persistence.room.d.b;
import android.arch.persistence.room.f;
import android.arch.persistence.room.i;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

final class c$3
  extends b
{
  private d.b i;
  
  c$3(c paramc, i parami) {}
  
  private List c()
  {
    Object localObject1 = i;
    int j;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/truepay/app/ui/growth/db/c$3$1;
      j = 0;
      localObject2 = new String[0];
      ((c.3.1)localObject1).<init>(this, "banners", (String[])localObject2);
      i = ((d.b)localObject1);
      localObject1 = h.a.c;
      localObject3 = i;
      ((d)localObject1).a((d.b)localObject3);
    }
    localObject1 = h.a;
    Object localObject3 = g;
    localObject1 = ((f)localObject1).a((e)localObject3);
    localObject3 = "_id";
    try
    {
      int k = ((Cursor)localObject1).getColumnIndexOrThrow((String)localObject3);
      localObject2 = "url";
      j = ((Cursor)localObject1).getColumnIndexOrThrow((String)localObject2);
      String str1 = "deep_link";
      int m = ((Cursor)localObject1).getColumnIndexOrThrow(str1);
      String str2 = "expires_at";
      int n = ((Cursor)localObject1).getColumnIndexOrThrow(str2);
      String str3 = "type";
      int i1 = ((Cursor)localObject1).getColumnIndexOrThrow(str3);
      String str4 = "banner_id";
      int i2 = ((Cursor)localObject1).getColumnIndexOrThrow(str4);
      ArrayList localArrayList = new java/util/ArrayList;
      int i3 = ((Cursor)localObject1).getCount();
      localArrayList.<init>(i3);
      for (;;)
      {
        boolean bool = ((Cursor)localObject1).moveToNext();
        if (!bool) {
          break;
        }
        a locala = new com/truecaller/truepay/app/ui/growth/db/a;
        locala.<init>();
        long l = ((Cursor)localObject1).getLong(k);
        a = l;
        String str5 = ((Cursor)localObject1).getString(j);
        locala.a(str5);
        str5 = ((Cursor)localObject1).getString(m);
        locala.b(str5);
        l = ((Cursor)localObject1).getLong(n);
        d = l;
        int i4 = ((Cursor)localObject1).getInt(i1);
        e = i4;
        str5 = ((Cursor)localObject1).getString(i2);
        locala.c(str5);
        localArrayList.add(locala);
      }
      return localArrayList;
    }
    finally
    {
      ((Cursor)localObject1).close();
    }
  }
  
  protected final void finalize()
  {
    g.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.growth.db.c.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */