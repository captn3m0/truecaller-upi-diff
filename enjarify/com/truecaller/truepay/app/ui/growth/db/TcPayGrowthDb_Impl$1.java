package com.truecaller.truepay.app.ui.growth.db;

import android.arch.persistence.room.b.b.a;
import android.arch.persistence.room.b.b.d;
import android.arch.persistence.room.f.b;
import android.arch.persistence.room.h.a;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class TcPayGrowthDb_Impl$1
  extends h.a
{
  TcPayGrowthDb_Impl$1(TcPayGrowthDb_Impl paramTcPayGrowthDb_Impl)
  {
    super(3);
  }
  
  public final void a(android.arch.persistence.db.b paramb)
  {
    paramb.c("DROP TABLE IF EXISTS `banners`");
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    paramb.c("CREATE TABLE IF NOT EXISTS `banners` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `url` TEXT NOT NULL, `deep_link` TEXT NOT NULL, `expires_at` INTEGER NOT NULL, `type` INTEGER NOT NULL, `banner_id` TEXT NOT NULL)");
    paramb.c("CREATE UNIQUE INDEX `index_banners_url` ON `banners` (`url`)");
    paramb.c("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    paramb.c("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"96512d87634c96fae128c56bc2eae3c4\")");
  }
  
  public final void c(android.arch.persistence.db.b paramb)
  {
    TcPayGrowthDb_Impl.a(b, paramb);
    TcPayGrowthDb_Impl.b(b, paramb);
    List localList1 = TcPayGrowthDb_Impl.d(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = TcPayGrowthDb_Impl.e(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)TcPayGrowthDb_Impl.f(b).get(i);
        localb.b(paramb);
        i += 1;
      }
    }
  }
  
  public final void d(android.arch.persistence.db.b paramb)
  {
    List localList1 = TcPayGrowthDb_Impl.a(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = TcPayGrowthDb_Impl.b(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)TcPayGrowthDb_Impl.c(b).get(i);
        localb.a(paramb);
        i += 1;
      }
    }
  }
  
  public final void e(android.arch.persistence.db.b paramb)
  {
    Object localObject1 = new java/util/HashMap;
    int i = 6;
    ((HashMap)localObject1).<init>(i);
    Object localObject2 = new android/arch/persistence/room/b/b$a;
    int j = 1;
    ((b.a)localObject2).<init>("_id", "INTEGER", j, j);
    ((HashMap)localObject1).put("_id", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("url", "TEXT", j, 0);
    ((HashMap)localObject1).put("url", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("deep_link", "TEXT", j, 0);
    ((HashMap)localObject1).put("deep_link", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("expires_at", "INTEGER", j, 0);
    ((HashMap)localObject1).put("expires_at", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("type", "INTEGER", j, 0);
    ((HashMap)localObject1).put("type", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("banner_id", "TEXT", j, 0);
    ((HashMap)localObject1).put("banner_id", localObject2);
    Object localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>(0);
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>(j);
    Object localObject4 = new android/arch/persistence/room/b/b$d;
    List localList = Arrays.asList(new String[] { "url" });
    ((b.d)localObject4).<init>("index_banners_url", j, localList);
    ((HashSet)localObject2).add(localObject4);
    localObject4 = new android/arch/persistence/room/b/b;
    String str = "banners";
    ((android.arch.persistence.room.b.b)localObject4).<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
    localObject1 = "banners";
    paramb = android.arch.persistence.room.b.b.a(paramb, (String)localObject1);
    boolean bool = ((android.arch.persistence.room.b.b)localObject4).equals(paramb);
    if (bool) {
      return;
    }
    localObject1 = new java/lang/IllegalStateException;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Migration didn't properly handle banners(com.truecaller.truepay.app.ui.growth.db.TcPayGrowthBanner).\n Expected:\n");
    ((StringBuilder)localObject3).append(localObject4);
    ((StringBuilder)localObject3).append("\n Found:\n");
    ((StringBuilder)localObject3).append(paramb);
    paramb = ((StringBuilder)localObject3).toString();
    ((IllegalStateException)localObject1).<init>(paramb);
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.growth.db.TcPayGrowthDb_Impl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */