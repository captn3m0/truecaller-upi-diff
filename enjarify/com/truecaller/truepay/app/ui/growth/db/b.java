package com.truecaller.truepay.app.ui.growth.db;

import android.arch.lifecycle.LiveData;
import java.util.List;

public abstract interface b
{
  public abstract int a(int paramInt);
  
  public abstract List a(int paramInt1, int paramInt2);
  
  public abstract void a(int paramInt, List paramList);
  
  public abstract long[] a(List paramList);
  
  public abstract LiveData b(int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.growth.db.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */