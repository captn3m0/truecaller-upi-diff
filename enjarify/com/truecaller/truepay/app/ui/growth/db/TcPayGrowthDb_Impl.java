package com.truecaller.truepay.app.ui.growth.db;

import android.arch.persistence.db.c.a;
import android.arch.persistence.db.c.b;
import android.arch.persistence.db.c.b.a;
import android.arch.persistence.db.c.c;
import android.arch.persistence.room.a;
import android.arch.persistence.room.d;
import android.arch.persistence.room.h;
import android.arch.persistence.room.h.a;

public class TcPayGrowthDb_Impl
  extends TcPayGrowthDb
{
  private volatile b g;
  
  public final d a()
  {
    d locald = new android/arch/persistence/room/d;
    String[] arrayOfString = { "banners" };
    locald.<init>(this, arrayOfString);
    return locald;
  }
  
  public final android.arch.persistence.db.c b(a parama)
  {
    Object localObject1 = new android/arch/persistence/room/h;
    Object localObject2 = new com/truecaller/truepay/app/ui/growth/db/TcPayGrowthDb_Impl$1;
    ((TcPayGrowthDb_Impl.1)localObject2).<init>(this);
    ((h)localObject1).<init>(parama, (h.a)localObject2, "96512d87634c96fae128c56bc2eae3c4", "8f127ce07e3d481e2c306f10588a3cb0");
    localObject2 = c.b.a(b);
    String str = c;
    b = str;
    c = ((c.a)localObject1);
    localObject1 = ((c.b.a)localObject2).a();
    return a.a((c.b)localObject1);
  }
  
  public final b h()
  {
    Object localObject1 = g;
    if (localObject1 != null) {
      return g;
    }
    try
    {
      localObject1 = g;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/truepay/app/ui/growth/db/c;
        ((c)localObject1).<init>(this);
        g = ((b)localObject1);
      }
      localObject1 = g;
      return (b)localObject1;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.growth.db.TcPayGrowthDb_Impl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */