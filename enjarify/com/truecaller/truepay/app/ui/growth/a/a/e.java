package com.truecaller.truepay.app.ui.growth.a.a;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import c.g.b.k;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.utils.r;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class e
  extends RecyclerView.ViewHolder
  implements d, a
{
  final View a;
  private final Drawable b;
  private final r c;
  private HashMap d;
  
  public e(View paramView, r paramr)
  {
    super(paramView);
    a = paramView;
    c = paramr;
    paramView = a.getContext();
    k.a(paramView, "containerView.context");
    paramView = paramView.getResources();
    int i = R.drawable.ic_placeholder_home_promo;
    paramView = paramView.getDrawable(i);
    b = paramView;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final View a()
  {
    return a;
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    Object localObject = "bannerId";
    k.b(paramString3, (String)localObject);
    if (paramString1 != null)
    {
      localObject = c;
      int i = R.id.imageItemHomeBanner;
      ImageView localImageView = (ImageView)a(i);
      k.a(localImageView, "imageItemHomeBanner");
      Drawable localDrawable1 = b;
      k.a(localDrawable1, "placeHolder");
      Drawable localDrawable2 = b;
      String str = "placeHolder";
      k.a(localDrawable2, str);
      ((r)localObject).a(paramString1, localImageView, localDrawable1, localDrawable2);
    }
    int j = R.id.imageItemHomeBanner;
    paramString1 = (ImageView)a(j);
    localObject = new com/truecaller/truepay/app/ui/growth/a/a/e$a;
    ((e.a)localObject).<init>(this, paramString2, paramString3);
    localObject = (View.OnClickListener)localObject;
    paramString1.setOnClickListener((View.OnClickListener)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.growth.a.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */