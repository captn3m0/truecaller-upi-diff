package com.truecaller.truepay.app.ui.growth.a.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import c.g.b.k;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.utils.r;
import java.util.Collection;
import java.util.List;

public final class a
  extends RecyclerView.Adapter
{
  private final LayoutInflater a;
  private final r b;
  private final List c;
  private final b d;
  
  public a(Context paramContext, r paramr, List paramList, b paramb)
  {
    b = paramr;
    c = paramList;
    d = paramb;
    paramContext = LayoutInflater.from(paramContext);
    k.a(paramContext, "LayoutInflater.from(context)");
    a = paramContext;
  }
  
  public final int getItemCount()
  {
    List localList = c;
    if (localList != null) {
      return localList.size();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    k.b(paramViewHolder, "holder");
    Object localObject = c;
    if (localObject != null)
    {
      localObject = (Collection)localObject;
      boolean bool1 = ((Collection)localObject).isEmpty();
      boolean bool2 = true;
      bool1 ^= bool2;
      if (bool1 == bool2)
      {
        localObject = d;
        paramViewHolder = (d)paramViewHolder;
        List localList = c;
        com.truecaller.truepay.app.ui.growth.db.a locala = (com.truecaller.truepay.app.ui.growth.db.a)localList.get(paramInt);
        ((b)localObject).a(paramViewHolder, locala);
      }
      return;
    }
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "parent");
    e locale = new com/truecaller/truepay/app/ui/growth/a/a/e;
    Object localObject = a;
    int i = R.layout.item_promo_banner;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    k.a(paramViewGroup, "inflater.inflate(R.layou…mo_banner, parent, false)");
    localObject = b;
    locale.<init>(paramViewGroup, (r)localObject);
    return (RecyclerView.ViewHolder)locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.growth.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */