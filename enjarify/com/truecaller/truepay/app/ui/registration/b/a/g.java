package com.truecaller.truepay.app.ui.registration.b.a;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final a a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private g(a parama, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static g a(a parama, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    g localg = new com/truecaller/truepay/app/ui/registration/b/a/g;
    localg.<init>(parama, paramProvider1, paramProvider2, paramProvider3);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.b.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */