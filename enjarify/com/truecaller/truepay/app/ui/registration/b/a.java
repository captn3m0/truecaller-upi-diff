package com.truecaller.truepay.app.ui.registration.b;

import android.content.Context;
import android.content.SharedPreferences;
import com.truecaller.truepay.a.a.e.q;
import com.truecaller.truepay.app.ui.registration.d.i;
import com.truecaller.truepay.app.ui.registration.views.activities.PreRegistrationActivity;
import com.truecaller.truepay.app.ui.registration.views.b.b.a;
import com.truecaller.truepay.app.utils.al;
import com.truecaller.truepay.app.utils.at;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.data.a.aa;
import com.truecaller.truepay.data.a.ab;
import com.truecaller.truepay.data.a.ac;
import com.truecaller.truepay.data.a.v;
import com.truecaller.truepay.data.a.w;
import com.truecaller.truepay.data.a.x;
import com.truecaller.truepay.data.a.y;
import com.truecaller.truepay.data.a.z;
import com.truecaller.truepay.data.f.af;
import com.truecaller.truepay.data.f.ag;
import com.truecaller.truepay.data.f.ak;
import javax.inject.Provider;

public final class a
  implements b
{
  private Provider A;
  private Provider B;
  private Provider C;
  private Provider D;
  private Provider E;
  private Provider F;
  private Provider G;
  private Provider H;
  private Provider I;
  private Provider J;
  private Provider K;
  private Provider L;
  private Provider M;
  private Provider N;
  private Provider O;
  private Provider P;
  private Provider Q;
  private final com.truecaller.truepay.app.a.a.a a;
  private final v b;
  private final ab c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  private Provider w;
  private Provider x;
  private Provider y;
  private Provider z;
  
  private a(v paramv, ab paramab, com.truecaller.truepay.app.ui.registration.b.a.a parama, com.truecaller.truepay.app.a.a.a parama1)
  {
    a = parama1;
    b = paramv;
    Object localObject5 = paramab;
    c = paramab;
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$i;
    ((a.i)localObject5).<init>(parama1);
    d = ((Provider)localObject5);
    localObject5 = i.a(d);
    e = ((Provider)localObject5);
    localObject5 = dagger.a.c.a(e);
    f = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$e;
    ((a.e)localObject5).<init>(parama1);
    g = ((Provider)localObject5);
    localObject5 = g;
    localObject5 = dagger.a.c.a(com.truecaller.truepay.app.ui.registration.b.a.d.a(parama, (Provider)localObject5));
    h = ((Provider)localObject5);
    localObject5 = h;
    localObject5 = dagger.a.c.a(com.truecaller.truepay.app.ui.registration.b.a.c.a(parama, (Provider)localObject5));
    i = ((Provider)localObject5);
    localObject5 = g;
    localObject5 = dagger.a.c.a(com.truecaller.truepay.app.ui.registration.b.a.b.a(parama, (Provider)localObject5));
    j = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$o;
    ((a.o)localObject5).<init>(parama1);
    k = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$m;
    ((a.m)localObject5).<init>(parama1);
    l = ((Provider)localObject5);
    localObject5 = k;
    Object localObject6 = l;
    localObject5 = dagger.a.c.a(w.a(paramv, (Provider)localObject5, (Provider)localObject6));
    m = ((Provider)localObject5);
    localObject5 = dagger.a.c.a(ak.a(m));
    n = ((Provider)localObject5);
    localObject5 = com.truecaller.truepay.a.a.e.p.a(n);
    o = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$c;
    ((a.c)localObject5).<init>(parama1);
    p = ((Provider)localObject5);
    localObject5 = com.truecaller.truepay.a.a.e.j.a(p);
    q = ((Provider)localObject5);
    localObject5 = com.truecaller.truepay.a.a.e.t.a(p);
    r = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$k;
    ((a.k)localObject5).<init>(parama1);
    s = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$j;
    ((a.j)localObject5).<init>(parama1);
    t = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$l;
    ((a.l)localObject5).<init>(parama1);
    u = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$f;
    ((a.f)localObject5).<init>(parama1);
    v = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$b;
    ((a.b)localObject5).<init>(parama1);
    w = ((Provider)localObject5);
    localObject5 = com.truecaller.truepay.a.a.e.d.a(n);
    x = ((Provider)localObject5);
    localObject5 = com.truecaller.truepay.a.a.f.k.a(p);
    y = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$n;
    ((a.n)localObject5).<init>(parama1);
    z = ((Provider)localObject5);
    localObject5 = com.truecaller.truepay.data.e.d.a(z);
    A = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$h;
    ((a.h)localObject5).<init>(parama1);
    B = ((Provider)localObject5);
    localObject5 = com.truecaller.truepay.a.a.e.h.a(k);
    C = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$p;
    ((a.p)localObject5).<init>(parama1);
    D = ((Provider)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/registration/b/a$g;
    ((a.g)localObject5).<init>(parama1);
    E = ((Provider)localObject5);
    localObject5 = o;
    localObject6 = q;
    Provider localProvider1 = r;
    Provider localProvider2 = s;
    Provider localProvider3 = t;
    Provider localProvider4 = u;
    Provider localProvider5 = v;
    Provider localProvider6 = w;
    Provider localProvider7 = x;
    Provider localProvider8 = y;
    Provider localProvider9 = A;
    localObject3 = d;
    localObject2 = B;
    Provider localProvider10 = C;
    Provider localProvider11 = i;
    Provider localProvider12 = j;
    Provider localProvider13 = D;
    localObject1 = E;
    localObject1 = dagger.a.c.a(com.truecaller.truepay.app.ui.registration.b.a.e.a(parama, (Provider)localObject5, (Provider)localObject6, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7, localProvider8, localProvider9, (Provider)localObject3, (Provider)localObject2, localProvider10, localProvider11, localProvider12, localProvider13, (Provider)localObject1));
    F = ((Provider)localObject1);
    localObject1 = com.truecaller.truepay.app.ui.registration.d.t.a(E);
    G = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(G);
    H = ((Provider)localObject1);
    localObject1 = x.a(paramv);
    I = ((Provider)localObject1);
    localObject1 = k;
    localObject4 = paramv;
    localObject1 = y.a(paramv, (Provider)localObject1);
    J = ((Provider)localObject1);
    localObject1 = I;
    localObject4 = J;
    localObject1 = com.truecaller.truepay.data.f.b.a((Provider)localObject1, (Provider)localObject4);
    K = ((Provider)localObject1);
    localObject1 = com.truecaller.truepay.a.a.b.d.a(K);
    L = ((Provider)localObject1);
    localObject1 = com.truecaller.truepay.a.a.b.b.a(K);
    M = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/registration/b/a$d;
    localObject4 = parama1;
    ((a.d)localObject1).<init>(parama1);
    N = ((Provider)localObject1);
    localObject1 = L;
    localObject4 = M;
    localObject5 = N;
    localObject6 = parama;
    localObject1 = dagger.a.c.a(com.truecaller.truepay.app.ui.registration.b.a.g.a(parama, (Provider)localObject1, (Provider)localObject4, (Provider)localObject5));
    O = ((Provider)localObject1);
    localObject1 = com.truecaller.truepay.a.a.e.l.a(p);
    P = ((Provider)localObject1);
    localObject1 = P;
    localObject1 = dagger.a.c.a(com.truecaller.truepay.app.ui.registration.b.a.f.a(parama, (Provider)localObject1));
    Q = ((Provider)localObject1);
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/truepay/app/ui/registration/b/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  private com.truecaller.truepay.data.f.ad b()
  {
    com.truecaller.truepay.data.f.ad localad = new com/truecaller/truepay/data/f/ad;
    af localaf1 = z.a();
    af localaf2 = aa.a((com.truecaller.truepay.data.api.g)dagger.a.g.a(a.p(), "Cannot return null from a non-@Nullable component method"));
    localad.<init>(localaf1, localaf2);
    return localad;
  }
  
  private com.truecaller.truepay.data.e.c c()
  {
    com.truecaller.truepay.data.e.c localc = new com/truecaller/truepay/data/e/c;
    SharedPreferences localSharedPreferences = (SharedPreferences)dagger.a.g.a(a.aa(), "Cannot return null from a non-@Nullable component method");
    localc.<init>(localSharedPreferences);
    return localc;
  }
  
  private ag d()
  {
    return com.truecaller.truepay.data.a.ad.a(ac.a((Context)dagger.a.g.a(a.a(), "Cannot return null from a non-@Nullable component method")));
  }
  
  private com.truecaller.truepay.a.a.e.a e()
  {
    com.truecaller.truepay.a.a.e.a locala = new com/truecaller/truepay/a/a/e/a;
    com.truecaller.truepay.data.api.e locale = (com.truecaller.truepay.data.api.e)dagger.a.g.a(a.q(), "Cannot return null from a non-@Nullable component method");
    locala.<init>(locale);
    return locala;
  }
  
  public final void a(com.truecaller.truepay.app.ui.bankList.a parama)
  {
    Object localObject = (com.truecaller.truepay.app.ui.registration.d.b)Q.get();
    a = ((com.truecaller.truepay.app.ui.registration.d.b)localObject);
    localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.truepay.app.utils.r)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.views.a.a parama)
  {
    com.truecaller.truepay.app.utils.r localr = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    d = localr;
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.views.a.b paramb)
  {
    Object localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    a = ((com.truecaller.truepay.app.utils.r)localObject);
    localObject = (au)dagger.a.g.a(a.k(), "Cannot return null from a non-@Nullable component method");
    b = ((au)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.views.a.c paramc)
  {
    com.truecaller.truepay.app.utils.r localr = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    c = localr;
  }
  
  public final void a(PreRegistrationActivity paramPreRegistrationActivity)
  {
    Object localObject1 = paramPreRegistrationActivity;
    Object localObject2 = new com/truecaller/truepay/app/ui/registration/d/k;
    Object localObject3 = localObject2;
    c.d.f localf1 = (c.d.f)dagger.a.g.a(a.Y(), "Cannot return null from a non-@Nullable component method");
    c.d.f localf2 = (c.d.f)dagger.a.g.a(a.Z(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.utils.n localn = (com.truecaller.utils.n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    ag localag = d();
    com.truecaller.multisim.h localh = (com.truecaller.multisim.h)dagger.a.g.a(a.y(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.api.f localf = (com.truecaller.truepay.data.api.f)dagger.a.g.a(a.l(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.c localc = c();
    com.truecaller.truepay.a.a.e.a locala = e();
    com.truecaller.truepay.app.ui.registration.views.c.e locale = (com.truecaller.truepay.app.ui.registration.views.c.e)f.get();
    at localat = (at)dagger.a.g.a(a.ae(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.app.utils.e locale1 = (com.truecaller.truepay.app.utils.e)dagger.a.g.a(a.r(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.utils.l locall = a.w();
    Object localObject4 = localObject2;
    locall = (com.truecaller.utils.l)dagger.a.g.a(locall, "Cannot return null from a non-@Nullable component method");
    localObject1 = dagger.a.g.a(a.M(), "Cannot return null from a non-@Nullable component method");
    localObject2 = localObject1;
    localObject2 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = localObject3;
    localObject1 = dagger.a.g.a(a.L(), "Cannot return null from a non-@Nullable component method");
    localObject4 = localObject1;
    localObject4 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.G(), "Cannot return null from a non-@Nullable component method");
    Object localObject5 = localObject1;
    localObject5 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.F(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject1;
    localObject6 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.U(), "Cannot return null from a non-@Nullable component method");
    Object localObject7 = localObject1;
    localObject7 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.d(), "Cannot return null from a non-@Nullable component method");
    Object localObject8 = localObject1;
    localObject8 = (com.truecaller.truepay.app.utils.l)localObject1;
    localObject1 = i.get();
    Object localObject9 = localObject1;
    localObject9 = (com.truecaller.multisim.b.b)localObject1;
    localObject1 = j.get();
    Object localObject10 = localObject1;
    localObject10 = (com.truecaller.truepay.app.utils.c)localObject1;
    localObject1 = dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    Object localObject11 = localObject1;
    localObject11 = (com.truecaller.featuretoggles.e)localObject1;
    ((com.truecaller.truepay.app.ui.registration.d.k)localObject3).<init>(localf1, localf2, localn, localag, localh, localf, localc, locala, locale, localat, locale1, locall, (com.truecaller.truepay.data.e.e)localObject2, (com.truecaller.truepay.data.e.e)localObject4, (com.truecaller.truepay.data.e.e)localObject5, (com.truecaller.truepay.data.e.e)localObject6, (com.truecaller.truepay.data.e.e)localObject7, (com.truecaller.truepay.app.utils.l)localObject8, (com.truecaller.multisim.b.b)localObject9, (com.truecaller.truepay.app.utils.c)localObject10, (com.truecaller.featuretoggles.e)localObject11);
    localObject1 = paramPreRegistrationActivity;
    a = ((com.truecaller.truepay.app.ui.registration.d.k)localObject3);
    localObject3 = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.featuretoggles.e)localObject3);
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.views.b.a parama)
  {
    Object localObject = (b.a)F.get();
    a = ((b.a)localObject);
    localObject = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.featuretoggles.e)localObject);
    localObject = (com.truecaller.multisim.h)dagger.a.g.a(a.y(), "Cannot return null from a non-@Nullable component method");
    c = ((com.truecaller.multisim.h)localObject);
    localObject = (com.truecaller.truepay.app.ui.registration.views.c.k)H.get();
    d = ((com.truecaller.truepay.app.ui.registration.views.c.k)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.views.b.f paramf)
  {
    com.truecaller.truepay.app.ui.registration.d.d locald = (com.truecaller.truepay.app.ui.registration.d.d)O.get();
    h = locald;
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.views.b.h paramh)
  {
    Object localObject1 = paramh;
    Object localObject2 = new com/truecaller/truepay/app/ui/registration/d/f;
    Object localObject3 = localObject2;
    com.truecaller.utils.n localn = (com.truecaller.utils.n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    ag localag = d();
    com.truecaller.multisim.h localh = (com.truecaller.multisim.h)dagger.a.g.a(a.y(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.api.f localf = (com.truecaller.truepay.data.api.f)dagger.a.g.a(a.l(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.c localc = c();
    com.truecaller.truepay.a.a.e.a locala = e();
    com.truecaller.truepay.app.ui.registration.views.c.e locale = (com.truecaller.truepay.app.ui.registration.views.c.e)f.get();
    at localat = (at)dagger.a.g.a(a.ae(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.app.utils.e locale1 = (com.truecaller.truepay.app.utils.e)dagger.a.g.a(a.r(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.utils.l locall = (com.truecaller.utils.l)dagger.a.g.a(a.w(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.multisim.b.b localb = (com.truecaller.multisim.b.b)i.get();
    com.truecaller.truepay.app.utils.c localc1 = (com.truecaller.truepay.app.utils.c)j.get();
    Object localObject4 = localObject2;
    localObject1 = dagger.a.g.a(a.M(), "Cannot return null from a non-@Nullable component method");
    localObject2 = localObject1;
    localObject2 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = localObject3;
    localObject1 = dagger.a.g.a(a.L(), "Cannot return null from a non-@Nullable component method");
    localObject4 = localObject1;
    localObject4 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.G(), "Cannot return null from a non-@Nullable component method");
    Object localObject5 = localObject1;
    localObject5 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.F(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject1;
    localObject6 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.U(), "Cannot return null from a non-@Nullable component method");
    Object localObject7 = localObject1;
    localObject7 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.d(), "Cannot return null from a non-@Nullable component method");
    Object localObject8 = localObject1;
    localObject8 = (com.truecaller.truepay.app.utils.l)localObject1;
    localObject1 = dagger.a.g.a(a.v(), "Cannot return null from a non-@Nullable component method");
    Object localObject9 = localObject1;
    localObject9 = (al)localObject1;
    localObject1 = dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    Object localObject10 = localObject1;
    localObject10 = (com.truecaller.featuretoggles.e)localObject1;
    localObject1 = dagger.a.g.a(a.Y(), "Cannot return null from a non-@Nullable component method");
    Object localObject11 = localObject1;
    localObject11 = (c.d.f)localObject1;
    localObject1 = dagger.a.g.a(a.Z(), "Cannot return null from a non-@Nullable component method");
    Object localObject12 = localObject1;
    localObject12 = (c.d.f)localObject1;
    ((com.truecaller.truepay.app.ui.registration.d.f)localObject3).<init>(localn, localag, localh, localf, localc, locala, locale, localat, locale1, locall, localb, localc1, (com.truecaller.truepay.data.e.e)localObject2, (com.truecaller.truepay.data.e.e)localObject4, (com.truecaller.truepay.data.e.e)localObject5, (com.truecaller.truepay.data.e.e)localObject6, (com.truecaller.truepay.data.e.e)localObject7, (com.truecaller.truepay.app.utils.l)localObject8, (al)localObject9, (com.truecaller.featuretoggles.e)localObject10, (c.d.f)localObject11, (c.d.f)localObject12);
    localObject1 = paramh;
    a = ((com.truecaller.truepay.app.ui.registration.d.f)localObject3);
    localObject3 = (com.truecaller.utils.n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.utils.n)localObject3);
    localObject3 = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    c = ((com.truecaller.truepay.app.utils.r)localObject3);
    localObject3 = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    d = ((com.truecaller.featuretoggles.e)localObject3);
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.views.b.j paramj)
  {
    Object localObject1 = com.truecaller.truepay.app.ui.registration.d.n.a();
    Object localObject2 = new com/truecaller/truepay/a/a/e/q;
    com.truecaller.truepay.data.f.ad localad = b();
    ((q)localObject2).<init>(localad);
    a = ((q)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/e/r;
    localad = b();
    ((com.truecaller.truepay.a.a.e.r)localObject2).<init>(localad);
    b = ((com.truecaller.truepay.a.a.e.r)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/e/b;
    localad = b();
    ((com.truecaller.truepay.a.a.e.b)localObject2).<init>(localad);
    c = ((com.truecaller.truepay.a.a.e.b)localObject2);
    localObject2 = (com.truecaller.truepay.app.ui.npci.e)dagger.a.g.a(a.as(), "Cannot return null from a non-@Nullable component method");
    f = ((com.truecaller.truepay.app.ui.npci.e)localObject2);
    localObject2 = c();
    g = ((com.truecaller.truepay.data.e.c)localObject2);
    j = ((com.truecaller.truepay.app.ui.registration.d.m)localObject1);
    localObject1 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.O(), "Cannot return null from a non-@Nullable component method");
    k = ((com.truecaller.truepay.data.e.e)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.views.b.k paramk)
  {
    Object localObject = new com/truecaller/truepay/app/ui/registration/d/l;
    ((com.truecaller.truepay.app.ui.registration.d.l)localObject).<init>();
    e = ((com.truecaller.truepay.app.ui.registration.d.l)localObject);
    localObject = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.O(), "Cannot return null from a non-@Nullable component method");
    f = ((com.truecaller.truepay.data.e.e)localObject);
    localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    g = ((com.truecaller.truepay.app.utils.r)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.views.b.m paramm)
  {
    com.truecaller.truepay.app.ui.registration.d.p localp = new com/truecaller/truepay/app/ui/registration/d/p;
    localp.<init>();
    a = localp;
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.views.b.n paramn)
  {
    Object localObject = new com/truecaller/truepay/app/ui/registration/d/r;
    com.truecaller.multisim.h localh = (com.truecaller.multisim.h)dagger.a.g.a(a.y(), "Cannot return null from a non-@Nullable component method");
    ((com.truecaller.truepay.app.ui.registration.d.r)localObject).<init>(localh);
    a = ((com.truecaller.truepay.app.ui.registration.d.r)localObject);
    localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.truepay.app.utils.r)localObject);
    localObject = (com.truecaller.utils.n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    c = ((com.truecaller.utils.n)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */