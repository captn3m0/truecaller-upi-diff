package com.truecaller.truepay.app.ui.registration.a;

import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;

public final class a$a
  extends d
{
  final TextView a;
  private final ImageView d;
  private ConstraintLayout e;
  
  public a$a(a parama, View paramView)
  {
    super(paramView);
    int i = R.id.cl_bank_list_layout;
    parama = (ConstraintLayout)paramView.findViewById(i);
    e = parama;
    i = R.id.bank_name_textView;
    parama = (TextView)paramView.findViewById(i);
    a = parama;
    i = R.id.bank_imageView;
    parama = (ImageView)paramView.findViewById(i);
    d = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */