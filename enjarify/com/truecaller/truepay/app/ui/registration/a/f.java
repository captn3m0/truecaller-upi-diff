package com.truecaller.truepay.app.ui.registration.a;

import android.support.v4.f.a;
import android.support.v7.widget.RecyclerView.Adapter;
import java.util.Iterator;
import java.util.Set;

public abstract class f
  extends RecyclerView.Adapter
  implements c
{
  private final e a;
  
  public f()
  {
    e locale = new com/truecaller/truepay/app/ui/registration/a/e;
    locale.<init>();
    a = locale;
  }
  
  private boolean c(int paramInt)
  {
    a locala = a.a;
    Object localObject = Integer.valueOf(paramInt);
    localObject = locala.get(localObject);
    return localObject != null;
  }
  
  private b d(int paramInt)
  {
    e locale = a;
    Object localObject1 = a;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (Integer)((a)localObject1).get(localObject2);
    int i = -1;
    if (localObject1 != null)
    {
      b localb = new com/truecaller/truepay/app/ui/registration/a/b;
      j = ((Integer)localObject1).intValue();
      localb.<init>(j, i);
      return localb;
    }
    localObject1 = Integer.valueOf(i);
    localObject2 = a.keySet().iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      Integer localInteger = (Integer)((Iterator)localObject2).next();
      int k = localInteger.intValue();
      if (paramInt <= k) {
        break;
      }
      localObject1 = localInteger;
    }
    localObject2 = new com/truecaller/truepay/app/ui/registration/a/b;
    int j = ((Integer)a.get(localObject1)).intValue();
    int m = ((Integer)localObject1).intValue();
    paramInt = paramInt - m + -1;
    ((b)localObject2).<init>(j, paramInt);
    return (b)localObject2;
  }
  
  public abstract void a(d paramd, int paramInt);
  
  public abstract void a(d paramd, int paramInt1, int paramInt2, int paramInt3);
  
  public int b(int paramInt)
  {
    return -1;
  }
  
  public final int getItemCount()
  {
    e locale = a;
    c = true;
    b = this;
    a.clear();
    int i = 0;
    int j = 0;
    for (;;)
    {
      int k = a();
      if (i >= k) {
        break;
      }
      k = a(i);
      if (k > 0)
      {
        a locala = a;
        Integer localInteger1 = Integer.valueOf(j);
        Integer localInteger2 = Integer.valueOf(i);
        locala.put(localInteger1, localInteger2);
        k += 1;
        j += k;
      }
      i += 1;
    }
    return j;
  }
  
  public long getItemId(int paramInt)
  {
    boolean bool = c(paramInt);
    if (bool)
    {
      paramInt = a.a(paramInt);
      return super.getItemId(paramInt);
    }
    paramInt = db;
    return super.getItemId(paramInt);
  }
  
  public final int getItemViewType(int paramInt)
  {
    boolean bool = c(paramInt);
    if (bool)
    {
      a.a(paramInt);
      return -2;
    }
    paramInt = da;
    return b(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */