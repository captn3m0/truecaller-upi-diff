package com.truecaller.truepay.app.ui.registration.a;

import java.util.Objects;

public final class b
{
  final int a;
  final int b;
  
  b(int paramInt1, int paramInt2)
  {
    a = paramInt1;
    b = paramInt2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool = paramObject instanceof b;
    if (bool)
    {
      paramObject = (b)paramObject;
      int i = a;
      int j = a;
      if (i == j)
      {
        int k = b;
        i = b;
        if (k == i) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    Object[] arrayOfObject = new Object[2];
    Integer localInteger = Integer.valueOf(a);
    arrayOfObject[0] = localInteger;
    localInteger = Integer.valueOf(b);
    arrayOfObject[1] = localInteger;
    return Objects.hash(arrayOfObject);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(":");
    i = b;
    localStringBuilder.append(i);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */