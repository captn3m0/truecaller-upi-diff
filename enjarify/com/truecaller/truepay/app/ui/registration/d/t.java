package com.truecaller.truepay.app.ui.registration.d;

import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d
{
  private final Provider a;
  
  private t(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static t a(Provider paramProvider)
  {
    t localt = new com/truecaller/truepay/app/ui/registration/d/t;
    localt.<init>(paramProvider);
    return localt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */