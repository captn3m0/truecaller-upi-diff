package com.truecaller.truepay.app.ui.registration.d;

import c.g.b.k;
import com.truecaller.truepay.PayTempTokenCallBack;
import com.truecaller.truepay.R.string;
import com.truecaller.utils.n;

public final class f$e
  implements PayTempTokenCallBack
{
  f$e(f paramf) {}
  
  public final void onFailure()
  {
    Object localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.d)a.af_();
    Object localObject2 = null;
    if (localObject1 != null) {
      ((com.truecaller.truepay.app.ui.registration.views.c.d)localObject1).a(false);
    }
    localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.d)a.af_();
    if (localObject1 != null)
    {
      Object localObject3 = a.f;
      int i = R.string.server_error_message;
      localObject2 = new Object[0];
      localObject2 = ((n)localObject3).a(i, (Object[])localObject2);
      localObject3 = "resourceProvider.getStri…ing.server_error_message)";
      k.a(localObject2, (String)localObject3);
      ((com.truecaller.truepay.app.ui.registration.views.c.d)localObject1).c((String)localObject2);
    }
    localObject1 = new java/lang/AssertionError;
    ((AssertionError)localObject1).<init>("fetchTempToken#onFailure");
    com.truecaller.log.d.a((Throwable)localObject1);
  }
  
  public final void onSuccess(String paramString)
  {
    k.b(paramString, "tempToken");
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    int i = ((CharSequence)localObject).length();
    Object[] arrayOfObject = null;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i == 0)
    {
      localObject = (com.truecaller.truepay.app.ui.registration.views.c.d)a.af_();
      if (localObject != null) {
        ((com.truecaller.truepay.app.ui.registration.views.c.d)localObject).b(paramString);
      }
      return;
    }
    paramString = (com.truecaller.truepay.app.ui.registration.views.c.d)a.af_();
    if (paramString != null)
    {
      localObject = a.f;
      int j = R.string.server_error_message;
      arrayOfObject = new Object[0];
      localObject = ((n)localObject).a(j, arrayOfObject);
      k.a(localObject, "resourceProvider.getStri…ing.server_error_message)");
      paramString.c((String)localObject);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.f.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */