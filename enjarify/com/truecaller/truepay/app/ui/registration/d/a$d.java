package com.truecaller.truepay.app.ui.registration.d;

import c.g.b.k;
import c.u;
import com.truecaller.truepay.app.ui.registration.views.b.b.b;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.data.api.model.h;
import io.reactivex.o;
import io.reactivex.q;
import java.util.ArrayList;

public final class a$d
  implements q
{
  private final boolean b;
  
  public a$d(a parama, boolean paramBoolean)
  {
    b = paramBoolean;
  }
  
  public final void a(io.reactivex.a.b paramb)
  {
    k.b(paramb, "d");
    a.e.a(paramb);
  }
  
  public final void a(Throwable paramThrowable)
  {
    String str1 = "e";
    k.b(paramThrowable, str1);
    paramThrowable = (b.b)a.af_();
    if (paramThrowable != null)
    {
      String str2 = a.h();
      k.a(str2, "getUserMsisdn()");
      paramThrowable.b("error message", str2);
      return;
    }
  }
  
  public final void c_(Object paramObject)
  {
    Object localObject1 = "t";
    k.b(paramObject, (String)localObject1);
    boolean bool1 = paramObject instanceof h;
    Object localObject2;
    Object localObject3;
    if (bool1)
    {
      localObject1 = paramObject;
      localObject1 = (h)paramObject;
      localObject2 = ((h)localObject1).c();
      boolean bool2 = localObject2 instanceof com.truecaller.truepay.app.ui.registration.c.b;
      if (bool2)
      {
        paramObject = (b.b)a.af_();
        if (paramObject != null)
        {
          localObject1 = ((h)localObject1).c();
          if (localObject1 != null)
          {
            localObject1 = ((com.truecaller.truepay.app.ui.registration.c.b)localObject1).a();
            k.a(localObject1, "(t.data as AccountListHolderDO).accounts");
            bool2 = b;
            localObject3 = a.i;
            ((b.b)paramObject).a((ArrayList)localObject1, bool2, (r)localObject3);
            return;
          }
          paramObject = new c/u;
          ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registration.models.AccountListHolderDO");
          throw ((Throwable)paramObject);
        }
        return;
      }
    }
    boolean bool3 = paramObject instanceof o;
    if (bool3)
    {
      paramObject = (b.b)a.af_();
      if (paramObject != null)
      {
        localObject1 = "";
        localObject2 = a.h();
        localObject3 = "getUserMsisdn()";
        k.a(localObject2, (String)localObject3);
        ((b.b)paramObject).b((String)localObject1, (String)localObject2);
      }
      return;
    }
    paramObject = (b.b)a.af_();
    if (paramObject != null)
    {
      localObject2 = a.h();
      k.a(localObject2, "getUserMsisdn()");
      ((b.b)paramObject).b("", (String)localObject2);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */