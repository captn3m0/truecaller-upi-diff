package com.truecaller.truepay.app.ui.registration.d;

import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d
{
  private final u a;
  private final Provider b;
  
  private v(u paramu, Provider paramProvider)
  {
    a = paramu;
    b = paramProvider;
  }
  
  public static v a(u paramu, Provider paramProvider)
  {
    v localv = new com/truecaller/truepay/app/ui/registration/d/v;
    localv.<init>(paramu, paramProvider);
    return localv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */