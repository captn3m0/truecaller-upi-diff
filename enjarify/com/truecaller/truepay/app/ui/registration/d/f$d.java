package com.truecaller.truepay.app.ui.registration.d;

import android.graphics.drawable.Drawable;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.log.UnmutedException.j;
import com.truecaller.truepay.R.drawable;
import com.truecaller.utils.n;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class f$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  f$d(f paramf, com.truecaller.truepay.app.ui.registration.views.c.d paramd, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/truepay/app/ui/registration/d/f$d;
    f localf = b;
    com.truecaller.truepay.app.ui.registration.views.c.d locald1 = c;
    locald.<init>(localf, locald1, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    int k = 0;
    Object localObject2 = null;
    int j;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      j = paramObject instanceof o.b;
      if (j != 0) {
        break label347;
      }
      paramObject = b;
      j = 1;
      a = j;
      localObject3 = z;
      Object localObject4 = A;
      localObject3 = ((c.d.f)localObject3).plus((c.d.f)localObject4);
      localObject4 = new com/truecaller/truepay/app/ui/registration/d/f$c;
      ((f.c)localObject4).<init>((f)paramObject, null);
      localObject4 = (m)localObject4;
      paramObject = g.a((c.d.f)localObject3, (m)localObject4, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (List)paramObject;
    if (paramObject != null)
    {
      int m = ((List)paramObject).size();
      if (m > 0)
      {
        localObject1 = b;
        j = 0;
        localObject3 = null;
        paramObject = (com.truecaller.truepay.app.ui.growth.db.a)((List)paramObject).get(0);
        c = ((com.truecaller.truepay.app.ui.growth.db.a)paramObject);
        paramObject = b.c;
        if (paramObject == null) {
          return x.a;
        }
        localObject1 = c;
        if (localObject1 != null)
        {
          try
          {
            paramObject = b;
            localObject3 = b;
            localObject3 = f;
            k = R.drawable.blue_background;
            localObject3 = ((n)localObject3).c(k);
            localObject2 = "resourceProvider.getDraw…drawable.blue_background)";
            c.g.b.k.a(localObject3, (String)localObject2);
            ((com.truecaller.truepay.app.ui.registration.views.c.d)localObject1).a((String)paramObject, (Drawable)localObject3);
          }
          catch (IllegalStateException localIllegalStateException)
          {
            paramObject = new com/truecaller/log/UnmutedException$j;
            localObject3 = "Get started banner crash";
            ((UnmutedException.j)paramObject).<init>((String)localObject3);
            paramObject = (Throwable)paramObject;
            com.truecaller.log.d.a((Throwable)paramObject);
          }
          localObject2 = localObject1;
        }
      }
      else
      {
        paramObject = b;
        ((f)paramObject).d();
        localObject2 = x.a;
      }
      if (localObject2 != null) {}
    }
    else
    {
      b.d();
      paramObject = x.a;
    }
    return x.a;
    label347:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.f.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */