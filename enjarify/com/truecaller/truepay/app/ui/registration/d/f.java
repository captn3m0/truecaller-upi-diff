package com.truecaller.truepay.app.ui.registration.d;

import c.g.a.m;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.truepay.PayTempTokenCallBack;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.views.c.d;
import com.truecaller.truepay.app.utils.al;
import com.truecaller.truepay.app.utils.at;
import com.truecaller.utils.n;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;

public final class f
  extends com.truecaller.truepay.app.ui.base.a.a
  implements g
{
  final c.d.f A;
  private bn B;
  private bn C;
  public final String[] a;
  final c.d.f b;
  public com.truecaller.truepay.app.ui.growth.db.a c;
  final n f;
  final com.truecaller.truepay.data.f.ag g;
  public final h h;
  public final com.truecaller.truepay.data.api.f i;
  public final com.truecaller.truepay.data.e.c j;
  public final com.truecaller.truepay.a.a.e.a k;
  public final com.truecaller.truepay.app.ui.registration.views.c.e l;
  public final at m;
  final com.truecaller.truepay.app.utils.e n;
  public final com.truecaller.utils.l o;
  public final com.truecaller.multisim.b.b p;
  public final com.truecaller.truepay.app.utils.c q;
  final com.truecaller.truepay.data.e.e r;
  public final com.truecaller.truepay.data.e.e s;
  public final com.truecaller.truepay.data.e.e t;
  public final com.truecaller.truepay.data.e.e u;
  final com.truecaller.truepay.data.e.e v;
  final com.truecaller.truepay.app.utils.l w;
  final al x;
  public final com.truecaller.featuretoggles.e y;
  final c.d.f z;
  
  public f(n paramn, com.truecaller.truepay.data.f.ag paramag, h paramh, com.truecaller.truepay.data.api.f paramf, com.truecaller.truepay.data.e.c paramc, com.truecaller.truepay.a.a.e.a parama, com.truecaller.truepay.app.ui.registration.views.c.e parame, at paramat, com.truecaller.truepay.app.utils.e parame1, com.truecaller.utils.l paraml, com.truecaller.multisim.b.b paramb, com.truecaller.truepay.app.utils.c paramc1, com.truecaller.truepay.data.e.e parame2, com.truecaller.truepay.data.e.e parame3, com.truecaller.truepay.data.e.e parame4, com.truecaller.truepay.data.e.e parame5, com.truecaller.truepay.data.e.e parame6, com.truecaller.truepay.app.utils.l paraml1, al paramal, com.truecaller.featuretoggles.e parame7, c.d.f paramf1, c.d.f paramf2)
  {
    f = paramn;
    g = paramag;
    h = paramh;
    i = paramf;
    j = paramc;
    k = parama;
    l = parame;
    m = paramat;
    n = parame1;
    o = paraml;
    p = paramb;
    q = paramc1;
    r = parame2;
    s = parame3;
    localObject = parame4;
    t = parame4;
    u = parame5;
    localObject = parame6;
    v = parame6;
    w = paraml1;
    localObject = paramal;
    x = paramal;
    y = parame7;
    localObject = paramf1;
    z = paramf1;
    A = paramf2;
    String[] tmp307_304 = new String[3];
    String[] tmp308_307 = tmp307_304;
    String[] tmp308_307 = tmp307_304;
    tmp308_307[0] = "android.permission.SEND_SMS";
    tmp308_307[1] = "android.permission.RECEIVE_SMS";
    tmp308_307[2] = "android.permission.READ_SMS";
    localObject = tmp308_307;
    a = ((String[])localObject);
    localObject = z;
    b = ((c.d.f)localObject);
  }
  
  public final int a(SimInfo paramSimInfo)
  {
    d locald = (d)af_();
    if (locald != null)
    {
      String str = String.valueOf(d);
      int i1 = a;
      locald.a(str, i1, false);
    }
    return 1;
  }
  
  public final void a()
  {
    Object localObject1 = (kotlinx.coroutines.ag)bg.a;
    c.d.f localf = b;
    Object localObject2 = new com/truecaller/truepay/app/ui/registration/d/f$f;
    ((f.f)localObject2).<init>(this, null);
    localObject2 = (m)localObject2;
    localObject1 = kotlinx.coroutines.e.b((kotlinx.coroutines.ag)localObject1, localf, (m)localObject2, 2);
    B = ((bn)localObject1);
  }
  
  public final void a(d paramd)
  {
    Object localObject1 = paramd;
    localObject1 = (com.truecaller.truepay.app.ui.base.views.a)paramd;
    super.a((com.truecaller.truepay.app.ui.base.views.a)localObject1);
    localObject1 = y;
    Object localObject2 = q;
    Object localObject3 = com.truecaller.featuretoggles.e.a;
    int i1 = 49;
    localObject3 = localObject3[i1];
    localObject1 = ((e.a)localObject2).a((com.truecaller.featuretoggles.e)localObject1, (c.l.g)localObject3);
    boolean bool = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool)
    {
      localObject1 = (kotlinx.coroutines.ag)bg.a;
      localObject2 = z;
      localObject3 = new com/truecaller/truepay/app/ui/registration/d/f$d;
      ((f.d)localObject3).<init>(this, paramd, null);
      localObject3 = (m)localObject3;
      paramd = kotlinx.coroutines.e.b((kotlinx.coroutines.ag)localObject1, (c.d.f)localObject2, (m)localObject3, 2);
      C = paramd;
      return;
    }
    d();
  }
  
  public final void b()
  {
    bn localbn = B;
    if (localbn != null) {
      localbn.n();
    }
    localbn = C;
    if (localbn != null) {
      localbn.n();
    }
    super.b();
  }
  
  final void d()
  {
    d locald = (d)af_();
    if (locald != null)
    {
      locald.h();
      return;
    }
  }
  
  public final void e()
  {
    Object localObject1 = (d)af_();
    if (localObject1 != null)
    {
      boolean bool = true;
      ((d)localObject1).a(bool);
    }
    localObject1 = Truepay.getInstance();
    if (localObject1 != null)
    {
      localObject1 = ((Truepay)localObject1).getListener();
      if (localObject1 != null)
      {
        Object localObject2 = new com/truecaller/truepay/app/ui/registration/d/f$e;
        ((f.e)localObject2).<init>(this);
        localObject2 = (PayTempTokenCallBack)localObject2;
        ((TcPaySDKListener)localObject1).fetchTempToken((PayTempTokenCallBack)localObject2);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */