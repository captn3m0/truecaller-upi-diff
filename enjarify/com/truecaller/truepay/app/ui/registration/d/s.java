package com.truecaller.truepay.app.ui.registration.d;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.app.ui.registration.c.o;
import com.truecaller.truepay.app.ui.registration.views.b.b.a;

public final class s
  implements com.truecaller.truepay.app.ui.registration.views.c.k
{
  private final e a;
  
  public s(e parame)
  {
    a = parame;
  }
  
  public final void a(Context paramContext, BroadcastReceiver paramBroadcastReceiver, b.a parama, o paramo, Integer paramInteger)
  {
    c.g.b.k.b(paramBroadcastReceiver, "broadcastReceiver");
    c.g.b.k.b(parama, "presenter");
    Object localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>("SMS_SENT_INTENT");
    IntentFilter localIntentFilter = null;
    localObject1 = PendingIntent.getBroadcast(paramContext, 0, (Intent)localObject1, 0);
    if (paramContext != null)
    {
      localObject2 = new android/content/IntentFilter;
      localObject3 = "SMS_SENT_INTENT";
      ((IntentFilter)localObject2).<init>((String)localObject3);
      paramContext.registerReceiver(paramBroadcastReceiver, (IntentFilter)localObject2);
    }
    Object localObject2 = null;
    Object localObject3 = a.E();
    boolean bool = ((b)localObject3).a();
    if (bool)
    {
      localObject2 = new android/content/Intent;
      localObject3 = "SMS_DELIVERED_INTENT";
      ((Intent)localObject2).<init>((String)localObject3);
      localObject2 = PendingIntent.getBroadcast(paramContext, 0, (Intent)localObject2, 0);
      if (paramContext != null)
      {
        localIntentFilter = new android/content/IntentFilter;
        localObject3 = "SMS_DELIVERED_INTENT";
        localIntentFilter.<init>((String)localObject3);
        paramContext.registerReceiver(paramBroadcastReceiver, localIntentFilter);
      }
    }
    if (paramo != null)
    {
      paramContext = "sentPI";
      c.g.b.k.a(localObject1, paramContext);
      int i;
      if (paramInteger != null) {
        i = paramInteger.intValue();
      } else {
        i = -1;
      }
      parama.a((PendingIntent)localObject1, (PendingIntent)localObject2, i, paramo);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */