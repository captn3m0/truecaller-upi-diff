package com.truecaller.truepay.app.ui.registration.d;

import c.d.c;
import c.g.a.m;
import c.g.b.v.c;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.views.c.d;
import java.util.HashMap;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ax;
import kotlinx.coroutines.g;

final class f$f
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  Object f;
  boolean g;
  int h;
  private ag j;
  
  f$f(f paramf, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/truepay/app/ui/registration/d/f$f;
    f localf1 = i;
    localf.<init>(localf1, paramc);
    paramObject = (ag)paramObject;
    j = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int k = h;
    int m = 1;
    Object localObject2 = null;
    boolean bool1;
    Object localObject3;
    boolean bool3;
    Object localObject6;
    switch (k)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      localObject1 = (String)f;
      bool1 = g;
      localObject3 = (d)e;
      localObject4 = (v.c)d;
      localObject5 = (v.c)c;
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        bool3 = bool1;
        localObject6 = localObject5;
        break label652;
      }
      throw a;
    case 1: 
      localObject6 = (ag)a;
      boolean bool4 = paramObject instanceof o.b;
      if (bool4) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label940;
      }
      localObject6 = j;
      paramObject = i.b;
      localObject4 = (c.d.f)ax.b();
      paramObject = ((c.d.f)paramObject).plus((c.d.f)localObject4);
      localObject4 = new com/truecaller/truepay/app/ui/registration/d/f$f$a;
      ((f.f.a)localObject4).<init>(this, null);
      localObject4 = (m)localObject4;
      a = localObject6;
      h = m;
      paramObject = g.a((c.d.f)paramObject, (m)localObject4, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (HashMap)paramObject;
    Object localObject5 = new c/g/b/v$c;
    ((v.c)localObject5).<init>();
    Object localObject4 = ((HashMap)paramObject).get("bank_symbol");
    a = localObject4;
    localObject4 = new c/g/b/v$c;
    ((v.c)localObject4).<init>();
    Object localObject7 = ((HashMap)paramObject).get("sim_slot");
    a = localObject7;
    localObject7 = (d)i.af_();
    if (localObject7 == null) {
      return x.a;
    }
    Object localObject8 = i.h.h();
    int n = ((List)localObject8).size();
    if (n <= m)
    {
      m = 0;
      localObject3 = null;
    }
    localObject8 = a;
    if (localObject8 != null)
    {
      localObject8 = i.y.D();
      boolean bool5 = ((com.truecaller.featuretoggles.b)localObject8).a();
      int i1;
      if (bool5)
      {
        localObject8 = i.p;
        localObject9 = a;
        if (localObject9 != null)
        {
          localObject9 = (Integer)localObject9;
          i1 = ((Integer)localObject9).intValue();
          bool5 = ((com.truecaller.multisim.b.b)localObject8).a(i1);
          if (!bool5)
          {
            a = null;
            a = null;
            break label912;
          }
        }
        else
        {
          paramObject = new c/u;
          ((u)paramObject).<init>("null cannot be cast to non-null type kotlin.Int");
          throw ((Throwable)paramObject);
        }
      }
      localObject8 = i.h;
      Object localObject9 = a;
      if (localObject9 != null)
      {
        localObject9 = (Integer)localObject9;
        i1 = ((Integer)localObject9).intValue();
        localObject8 = ((h)localObject8).a(i1);
        if (localObject8 != null)
        {
          localObject8 = d;
        }
        else
        {
          bool5 = false;
          localObject8 = null;
        }
        localObject9 = a;
        if (localObject9 != null)
        {
          localObject9 = i.n;
          String str = (String)a;
          a = localObject6;
          b = paramObject;
          c = localObject5;
          d = localObject4;
          e = localObject7;
          g = m;
          f = localObject8;
          int i2 = 2;
          h = i2;
          paramObject = ((com.truecaller.truepay.app.utils.e)localObject9).a(str, this);
          if (paramObject == localObject1) {
            return localObject1;
          }
          bool3 = m;
          localObject6 = localObject5;
          localObject3 = localObject7;
          localObject1 = localObject8;
          label652:
          paramObject = (com.truecaller.truepay.data.d.a)paramObject;
          if (paramObject != null)
          {
            ((d)localObject3).a((com.truecaller.truepay.data.d.a)paramObject);
            localObject5 = ((com.truecaller.truepay.data.d.a)paramObject).b();
            c.g.b.k.a(localObject5, "bank.bankName");
            localObject7 = ((com.truecaller.truepay.data.d.a)paramObject).d();
            c.g.b.k.a(localObject7, "bank.bankSymbol");
            localObject8 = String.valueOf(localObject1);
            i1 = ((Number)a).intValue();
            localObject4 = localObject3;
            ((d)localObject3).a((String)localObject5, (String)localObject7, (String)localObject8, i1, bool3);
            paramObject = Truepay.getInstance();
            if (paramObject != null)
            {
              paramObject = ((Truepay)paramObject).getAnalyticLoggerHelper();
              if (paramObject != null)
              {
                localObject1 = "success";
                localObject2 = (String)a;
                ((com.truecaller.truepay.data.b.a)paramObject).c((String)localObject1, (String)localObject2);
                localObject2 = x.a;
              }
            }
            if (localObject2 != null) {
              break label919;
            }
          }
          paramObject = Truepay.getInstance();
          if (paramObject != null)
          {
            paramObject = ((Truepay)paramObject).getAnalyticLoggerHelper();
            if (paramObject != null)
            {
              localObject1 = "failure";
              localObject6 = (String)a;
              ((com.truecaller.truepay.data.b.a)paramObject).c((String)localObject1, (String)localObject6);
            }
          }
          ((d)localObject3).i();
          paramObject = x.a;
          break label919;
        }
        paramObject = i.y.D();
        boolean bool6 = ((com.truecaller.featuretoggles.b)paramObject).a();
        if (bool6)
        {
          paramObject = String.valueOf(localObject8);
          localObject1 = (Number)a;
          int i3 = ((Number)localObject1).intValue();
          ((d)localObject7).a((String)paramObject, i3, m);
        }
        ((d)localObject7).i();
        break label919;
      }
      paramObject = new c/u;
      ((u)paramObject).<init>("null cannot be cast to non-null type kotlin.Int");
      throw ((Throwable)paramObject);
    }
    label912:
    ((d)localObject7).i();
    label919:
    paramObject = i.r;
    localObject1 = com.truecaller.truepay.data.b.a.a();
    ((com.truecaller.truepay.data.e.e)paramObject).a((String)localObject1);
    return x.a;
    label940:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.f.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */