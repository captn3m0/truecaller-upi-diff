package com.truecaller.truepay.app.ui.registration.d;

import com.truecaller.truepay.PayTempTokenCallBack;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.registration.views.c.f;
import com.truecaller.utils.n;

public final class k$c
  implements PayTempTokenCallBack
{
  public k$c(k paramk) {}
  
  public final void onFailure()
  {
    f localf = (f)a.af_();
    if (localf != null)
    {
      Object localObject = a.o;
      int i = R.string.server_error_message;
      Object[] arrayOfObject = new Object[0];
      localObject = ((n)localObject).a(i, arrayOfObject);
      c.g.b.k.a(localObject, "resourceProvider.getStri…ing.server_error_message)");
      localf.a((String)localObject);
      return;
    }
  }
  
  public final void onSuccess(String paramString)
  {
    c.g.b.k.b(paramString, "tempToken");
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    int i = ((CharSequence)localObject).length();
    Object[] arrayOfObject = null;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i == 0)
    {
      localObject = (f)a.af_();
      if (localObject != null) {
        ((f)localObject).b(paramString);
      }
      return;
    }
    paramString = (f)a.af_();
    if (paramString != null)
    {
      localObject = a.o;
      int j = R.string.server_error_message;
      arrayOfObject = new Object[0];
      localObject = ((n)localObject).a(j, arrayOfObject);
      c.g.b.k.a(localObject, "resourceProvider.getStri…ing.server_error_message)");
      paramString.a((String)localObject);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.k.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */