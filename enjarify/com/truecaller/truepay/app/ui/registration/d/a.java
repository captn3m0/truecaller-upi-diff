package com.truecaller.truepay.app.ui.registration.d;

import android.app.PendingIntent;
import android.util.Base64;
import c.g.b.k;
import c.g.b.v.a;
import c.u;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.a.a.e.i;
import com.truecaller.truepay.a.a.e.s;
import com.truecaller.truepay.a.a.f.j;
import com.truecaller.truepay.app.ui.registration.views.b.b.a;
import com.truecaller.truepay.app.ui.registration.views.b.b.b;
import com.truecaller.truepay.app.utils.l;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.f;
import io.reactivex.d.e.a.p.a;
import io.reactivex.n;
import io.reactivex.q;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class a
  extends com.truecaller.truepay.app.ui.base.a.a
  implements b.a
{
  final com.truecaller.truepay.data.e.e a;
  final com.truecaller.truepay.data.e.e b;
  final com.truecaller.truepay.data.e.e c;
  final l f;
  final com.truecaller.truepay.a.a.e.c g;
  final j h;
  final r i;
  final com.truecaller.featuretoggles.e j;
  private final com.truecaller.truepay.a.a.e.o k;
  private final i l;
  private final s m;
  private final com.truecaller.truepay.app.utils.a n;
  private final com.truecaller.truepay.data.e.c o;
  private final h p;
  private final com.truecaller.truepay.a.a.e.g q;
  private final com.truecaller.multisim.b.b r;
  private final com.truecaller.truepay.app.utils.c s;
  private final f t;
  
  public a(com.truecaller.truepay.a.a.e.o paramo, i parami, s params, com.truecaller.truepay.data.e.e parame1, com.truecaller.truepay.data.e.e parame2, com.truecaller.truepay.data.e.e parame3, l paraml, com.truecaller.truepay.app.utils.a parama, com.truecaller.truepay.a.a.e.c paramc, j paramj, com.truecaller.truepay.data.e.c paramc1, h paramh, r paramr, com.truecaller.truepay.a.a.e.g paramg, com.truecaller.multisim.b.b paramb, com.truecaller.truepay.app.utils.c paramc2, f paramf, com.truecaller.featuretoggles.e parame)
  {
    k = paramo;
    l = parami;
    m = params;
    a = parame1;
    b = parame2;
    c = parame3;
    f = paraml;
    n = parama;
    g = paramc;
    h = paramj;
    o = paramc1;
    p = paramh;
    i = paramr;
    q = paramg;
    r = paramb;
    s = paramc2;
    t = paramf;
    j = parame;
  }
  
  public final void a()
  {
    io.reactivex.o localo = q.a();
    Object localObject = io.reactivex.g.a.b();
    localo = localo.b((n)localObject);
    localObject = io.reactivex.android.b.a.a();
    localo = localo.a((n)localObject);
    localObject = new com/truecaller/truepay/app/ui/registration/d/a$b;
    ((a.b)localObject).<init>(this);
    localObject = (q)localObject;
    localo.a((q)localObject);
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = new com/truecaller/truepay/app/ui/registration/c/p;
    ((com.truecaller.truepay.app.ui.registration.c.p)localObject1).<init>();
    a("");
    Object localObject2 = p.a(paramInt);
    boolean bool1 = false;
    Object localObject3 = null;
    if (localObject2 != null)
    {
      localObject2 = h;
    }
    else
    {
      bool2 = false;
      localObject2 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.p)localObject1).a((String)localObject2);
    localObject2 = p.a(paramInt);
    if (localObject2 != null)
    {
      localObject2 = g;
    }
    else
    {
      bool2 = false;
      localObject2 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.p)localObject1).b((String)localObject2);
    localObject2 = p;
    Object localObject4 = ((h)localObject2).a(paramInt);
    if (localObject4 != null)
    {
      localObject4 = i;
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.p)localObject1).e((String)localObject4);
    localObject4 = j.q();
    paramInt = ((com.truecaller.featuretoggles.b)localObject4).a();
    if (paramInt != 0) {
      localObject3 = localObject1;
    }
    if (localObject3 != null)
    {
      localObject4 = c.a();
      ((com.truecaller.truepay.app.ui.registration.c.p)localObject3).d((String)localObject4);
    }
    localObject4 = (CharSequence)((com.truecaller.truepay.app.ui.registration.c.p)localObject1).a();
    boolean bool2 = false;
    localObject2 = null;
    bool1 = true;
    if (localObject4 != null)
    {
      paramInt = ((CharSequence)localObject4).length();
      if (paramInt != 0)
      {
        paramInt = 0;
        localObject4 = null;
        break label218;
      }
    }
    paramInt = 1;
    label218:
    if (paramInt == 0)
    {
      localObject4 = (CharSequence)((com.truecaller.truepay.app.ui.registration.c.p)localObject1).b();
      if (localObject4 != null)
      {
        paramInt = ((CharSequence)localObject4).length();
        if (paramInt != 0) {}
      }
      else
      {
        bool2 = true;
      }
      if (!bool2)
      {
        ((com.truecaller.truepay.app.ui.registration.c.p)localObject1).a(bool1);
        localObject4 = ((com.truecaller.truepay.app.ui.registration.c.p)localObject1).a();
        localObject2 = ((com.truecaller.truepay.app.ui.registration.c.p)localObject1).b();
        localObject4 = l.a((String)localObject4, (String)localObject2);
        ((com.truecaller.truepay.app.ui.registration.c.p)localObject1).c((String)localObject4);
        paramInt = 9;
        localObject2 = j.D();
        bool2 = ((com.truecaller.featuretoggles.b)localObject2).a();
        if (!bool2) {
          paramInt = 10;
        }
        localObject2 = k.a((com.truecaller.truepay.app.ui.registration.c.p)localObject1);
        localObject3 = new com/truecaller/truepay/app/ui/registration/d/a$e;
        ((a.e)localObject3).<init>(this, paramInt);
        localObject3 = (io.reactivex.c.e)localObject3;
        localObject4 = ((io.reactivex.o)localObject2).c((io.reactivex.c.e)localObject3);
        localObject2 = io.reactivex.g.a.b();
        localObject4 = ((io.reactivex.d)localObject4).b((n)localObject2);
        localObject2 = io.reactivex.android.b.a.a();
        localObject4 = ((io.reactivex.d)localObject4).a((n)localObject2);
        localObject2 = new com/truecaller/truepay/app/ui/registration/d/a$f;
        ((a.f)localObject2).<init>(this, (com.truecaller.truepay.app.ui.registration.c.p)localObject1);
        localObject2 = (io.reactivex.c.d)localObject2;
        localObject1 = new com/truecaller/truepay/app/ui/registration/d/a$g;
        ((a.g)localObject1).<init>(this);
        localObject1 = (io.reactivex.c.d)localObject1;
        localObject3 = (io.reactivex.c.a)a.h.a;
        localObject4 = ((io.reactivex.d)localObject4).a((io.reactivex.c.d)localObject2, (io.reactivex.c.d)localObject1, (io.reactivex.c.a)localObject3);
        e.a((io.reactivex.a.b)localObject4);
        return;
      }
    }
    localObject4 = (b.b)af_();
    if (localObject4 != null)
    {
      ((b.b)localObject4).q();
      return;
    }
  }
  
  public final void a(PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, int paramInt, com.truecaller.truepay.app.ui.registration.c.o paramo)
  {
    k.b(paramPendingIntent1, "smsSendReportPI");
    Object localObject1 = "userDeviceStatusResponseDO";
    k.b(paramo, (String)localObject1);
    int i1 = -1;
    if (paramInt != i1)
    {
      localObject1 = p;
      localObject2 = ((h)localObject1).a(paramInt);
      if (localObject2 != null)
      {
        localObject2 = b;
        if (localObject2 != null)
        {
          localObject3 = localObject2;
          break label86;
        }
      }
    }
    Object localObject2 = p.f();
    Object localObject3 = localObject2;
    label86:
    k.a(localObject3, "if (selectedSimSlot != I…imManager.defaultSimToken");
    localObject2 = p.b((String)localObject3);
    i1 = 0;
    localObject1 = null;
    if (localObject2 != null)
    {
      localObject2 = g;
    }
    else
    {
      paramInt = 0;
      localObject2 = null;
    }
    Object localObject4 = p.b((String)localObject3);
    if (localObject4 != null) {
      localObject4 = h;
    } else {
      localObject4 = null;
    }
    Object localObject5 = c.a();
    int i2 = 2;
    char c1 = ' ';
    char c2 = '-';
    int i4;
    Object localObject7;
    if (localObject5 != null)
    {
      localObject6 = j.q();
      boolean bool2 = ((com.truecaller.featuretoggles.b)localObject6).a();
      if (bool2) {
        localObject1 = localObject5;
      }
      if (localObject1 != null)
      {
        i4 = ((String)localObject1).hashCode();
        int i3 = -1396222919;
        boolean bool1;
        if (i4 != i3)
        {
          i3 = 100023093;
          if (i4 == i3)
          {
            localObject5 = "icici";
            bool1 = ((String)localObject1).equals(localObject5);
            if (bool1)
            {
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>();
              localObject5 = paramo.d();
              k.a(localObject5, "userDeviceStatusResponseDO.smsDO");
              localObject5 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject5).d();
              localObject6 = "userDeviceStatusResponseDO.smsDO.icici";
              k.a(localObject5, (String)localObject6);
              localObject5 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject5).a();
              ((StringBuilder)localObject1).append((String)localObject5);
              ((StringBuilder)localObject1).append(c2);
              ((StringBuilder)localObject1).append((String)localObject4);
              ((StringBuilder)localObject1).append(c2);
              ((StringBuilder)localObject1).append((String)localObject2);
              ((StringBuilder)localObject1).append(c2);
              l1 = System.currentTimeMillis();
              ((StringBuilder)localObject1).append(l1);
              localObject2 = ((StringBuilder)localObject1).toString();
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>();
              localObject4 = paramo.d();
              k.a(localObject4, "userDeviceStatusResponseDO.smsDO");
              localObject4 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject4).d();
              localObject5 = "userDeviceStatusResponseDO.smsDO.icici";
              k.a(localObject4, (String)localObject5);
              localObject4 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject4).c();
              ((StringBuilder)localObject1).append((String)localObject4);
              ((StringBuilder)localObject1).append(c1);
              localObject4 = c.n.d.a;
              if (localObject2 != null)
              {
                localObject2 = ((String)localObject2).getBytes((Charset)localObject4);
                k.a(localObject2, "(this as java.lang.String).getBytes(charset)");
                localObject2 = Base64.encodeToString((byte[])localObject2, i2);
                ((StringBuilder)localObject1).append((String)localObject2);
                localObject2 = ((StringBuilder)localObject1).toString();
                paramo = paramo.d();
                k.a(paramo, "userDeviceStatusResponseDO.smsDO");
                paramo = paramo.d();
                k.a(paramo, "userDeviceStatusResponseDO.smsDO.icici");
                paramo = paramo.b();
                localObject1 = "userDeviceStatusResponseDO.smsDO.icici.shortCode";
                k.a(paramo, (String)localObject1);
                localObject7 = localObject2;
                localObject4 = paramo;
                break label1271;
              }
              paramPendingIntent1 = new c/u;
              paramPendingIntent1.<init>("null cannot be cast to non-null type java.lang.String");
              throw paramPendingIntent1;
            }
          }
        }
        else
        {
          localObject5 = "baroda";
          bool1 = ((String)localObject1).equals(localObject5);
          if (bool1)
          {
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>();
            ((StringBuilder)localObject1).append((String)localObject2);
            paramInt = 124;
            ((StringBuilder)localObject1).append(paramInt);
            ((StringBuilder)localObject1).append((String)localObject4);
            ((StringBuilder)localObject1).append("|35379708185175|00:B3:62:DF:E0:E0|00:B3:62:DF:E0:E1");
            localObject2 = ((StringBuilder)localObject1).toString();
            localObject1 = c.n.d.a;
            if (localObject2 != null)
            {
              localObject2 = ((String)localObject2).getBytes((Charset)localObject1);
              k.a(localObject2, "(this as java.lang.String).getBytes(charset)");
              localObject1 = paramo.d();
              k.a(localObject1, "userDeviceStatusResponseDO.smsDO");
              localObject1 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject1).e();
              k.a(localObject1, "userDeviceStatusResponseDO.smsDO.baroda");
              localObject1 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject1).f();
              k.a(localObject1, "userDeviceStatusResponse…msDO.baroda.encryptionKey");
              localObject4 = c.n.d.a;
              if (localObject1 != null)
              {
                localObject1 = ((String)localObject1).getBytes((Charset)localObject4);
                k.a(localObject1, "(this as java.lang.String).getBytes(charset)");
                localObject2 = com.truecaller.truepay.app.utils.p.a((byte[])localObject2, (byte[])localObject1);
                localObject1 = new java/lang/StringBuilder;
                ((StringBuilder)localObject1).<init>();
                localObject4 = paramo.d();
                k.a(localObject4, "userDeviceStatusResponseDO.smsDO");
                localObject4 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject4).e();
                localObject5 = "userDeviceStatusResponseDO.smsDO.baroda";
                k.a(localObject4, (String)localObject5);
                localObject4 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject4).c();
                ((StringBuilder)localObject1).append((String)localObject4);
                ((StringBuilder)localObject1).append(c1);
                localObject2 = Base64.encodeToString((byte[])localObject2, i2);
                localObject4 = c.n.d.a.name();
                localObject2 = URLEncoder.encode((String)localObject2, (String)localObject4);
                ((StringBuilder)localObject1).append((String)localObject2);
                localObject2 = ((StringBuilder)localObject1).toString();
                paramo = paramo.d();
                k.a(paramo, "userDeviceStatusResponseDO.smsDO");
                paramo = paramo.e();
                k.a(paramo, "userDeviceStatusResponseDO.smsDO.baroda");
                paramo = paramo.b();
                localObject1 = "userDeviceStatusResponseDO.smsDO.baroda.shortCode";
                k.a(paramo, (String)localObject1);
                localObject7 = localObject2;
                localObject4 = paramo;
                break label1271;
              }
              paramPendingIntent1 = new c/u;
              paramPendingIntent1.<init>("null cannot be cast to non-null type java.lang.String");
              throw paramPendingIntent1;
            }
            paramPendingIntent1 = new c/u;
            paramPendingIntent1.<init>("null cannot be cast to non-null type java.lang.String");
            throw paramPendingIntent1;
          }
        }
        paramPendingIntent1 = (b.b)af_();
        if (paramPendingIntent1 != null)
        {
          paramPendingIntent1.r();
          return;
        }
        return;
      }
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject5 = paramo.d();
    k.a(localObject5, "userDeviceStatusResponseDO.smsDO");
    localObject5 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject5).d();
    Object localObject6 = "userDeviceStatusResponseDO.smsDO.icici";
    k.a(localObject5, (String)localObject6);
    localObject5 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject5).a();
    ((StringBuilder)localObject1).append((String)localObject5);
    ((StringBuilder)localObject1).append(c2);
    ((StringBuilder)localObject1).append((String)localObject4);
    ((StringBuilder)localObject1).append(c2);
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(c2);
    long l1 = System.currentTimeMillis();
    ((StringBuilder)localObject1).append(l1);
    localObject2 = ((StringBuilder)localObject1).toString();
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject4 = paramo.d();
    k.a(localObject4, "userDeviceStatusResponseDO.smsDO");
    localObject4 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject4).d();
    localObject5 = "userDeviceStatusResponseDO.smsDO.icici";
    k.a(localObject4, (String)localObject5);
    localObject4 = ((com.truecaller.truepay.app.ui.registration.c.m)localObject4).c();
    ((StringBuilder)localObject1).append((String)localObject4);
    ((StringBuilder)localObject1).append(c1);
    localObject4 = c.n.d.a;
    if (localObject2 != null)
    {
      localObject2 = ((String)localObject2).getBytes((Charset)localObject4);
      k.a(localObject2, "(this as java.lang.String).getBytes(charset)");
      localObject2 = Base64.encodeToString((byte[])localObject2, i2);
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject2 = ((StringBuilder)localObject1).toString();
      paramo = paramo.d();
      k.a(paramo, "userDeviceStatusResponseDO.smsDO");
      paramo = paramo.d();
      k.a(paramo, "userDeviceStatusResponseDO.smsDO.icici");
      paramo = paramo.b();
      localObject1 = "userDeviceStatusResponseDO.smsDO.icici.shortCode";
      k.a(paramo, (String)localObject1);
      localObject7 = localObject2;
      localObject4 = paramo;
      label1271:
      localObject1 = p;
      i4 = 0;
      localObject5 = null;
      boolean bool3 = ((h)localObject1).a((String)localObject4, null, (String)localObject7, paramPendingIntent1, paramPendingIntent2, (String)localObject3);
      if (bool3)
      {
        paramPendingIntent1 = (b.b)af_();
        if (paramPendingIntent1 != null) {
          paramPendingIntent1.s();
        }
        return;
      }
      paramPendingIntent1 = (b.b)af_();
      if (paramPendingIntent1 != null)
      {
        paramPendingIntent1.r();
        return;
      }
      return;
    }
    paramPendingIntent1 = new c/u;
    paramPendingIntent1.<init>("null cannot be cast to non-null type java.lang.String");
    throw paramPendingIntent1;
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.c.b paramb, int paramInt)
  {
    String str1 = null;
    if (paramb != null)
    {
      localObject1 = paramb.a();
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = (com.truecaller.truepay.data.api.model.a)((ArrayList)localObject1).get(paramInt);
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    Object localObject2 = n.b();
    Object localObject3 = "accountManager.accountsList";
    k.a(localObject2, (String)localObject3);
    localObject2 = (Iterable)localObject2;
    boolean bool1 = localObject2 instanceof Collection;
    boolean bool2 = false;
    if (bool1)
    {
      localObject3 = localObject2;
      localObject3 = (Collection)localObject2;
      bool1 = ((Collection)localObject3).isEmpty();
      if (bool1) {}
    }
    else
    {
      localObject2 = ((Iterable)localObject2).iterator();
      do
      {
        bool1 = ((Iterator)localObject2).hasNext();
        if (!bool1) {
          break;
        }
        localObject3 = (com.truecaller.truepay.data.api.model.a)((Iterator)localObject2).next();
        bool1 = k.a(localObject3, localObject1);
      } while (!bool1);
      i1 = 1;
      break label175;
    }
    int i1 = 0;
    Object localObject1 = null;
    label175:
    if (i1 != 0)
    {
      paramb = (b.b)af_();
      if (paramb != null)
      {
        paramb.f();
        return;
      }
      return;
    }
    localObject1 = new com/truecaller/truepay/app/ui/registration/c/c;
    if (paramb != null) {
      localObject2 = paramb.b();
    } else {
      localObject2 = null;
    }
    if (paramb != null)
    {
      localObject3 = paramb.c();
    }
    else
    {
      bool1 = false;
      localObject3 = null;
    }
    String str2;
    if (paramb != null) {
      str2 = paramb.d();
    } else {
      str2 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).<init>((String)localObject2, (String)localObject3, str2);
    if (paramb != null) {
      paramb = paramb.a();
    } else {
      paramb = null;
    }
    if (paramb != null) {
      paramb = (com.truecaller.truepay.data.api.model.a)paramb.get(paramInt);
    } else {
      paramb = null;
    }
    if (paramb != null)
    {
      localObject4 = paramb.l();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).a((com.truecaller.truepay.data.d.a)localObject4);
    if (paramb != null)
    {
      localObject4 = paramb.d();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).c((String)localObject4);
    if (paramb != null)
    {
      localObject4 = paramb.c();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).b((String)localObject4);
    if (paramb != null)
    {
      localObject4 = paramb.h();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).a((ArrayList)localObject4);
    if (paramb != null)
    {
      localObject4 = paramb.j();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).f((String)localObject4);
    if (paramb != null)
    {
      localObject4 = paramb.e();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).d((String)localObject4);
    if (paramb != null)
    {
      paramInt = paramb.i();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).c(paramInt);
    if (paramb != null)
    {
      paramInt = paramb.g();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).b(paramInt);
    if (paramb != null)
    {
      paramInt = paramb.n();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).e(paramInt);
    if (paramb != null)
    {
      paramInt = paramb.a();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).a(paramInt);
    if (paramb != null) {
      bool2 = paramb.k();
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).d(bool2);
    if (paramb != null)
    {
      localObject4 = paramb.m();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).g((String)localObject4);
    if (paramb != null)
    {
      localObject4 = paramb.b();
    }
    else
    {
      paramInt = 0;
      localObject4 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).a((String)localObject4);
    if (paramb != null) {
      str1 = paramb.f();
    }
    ((com.truecaller.truepay.app.ui.registration.c.c)localObject1).e(str1);
    paramb = m.a((com.truecaller.truepay.app.ui.registration.c.c)localObject1);
    Object localObject4 = io.reactivex.g.a.b();
    paramb = paramb.b((n)localObject4);
    localObject4 = io.reactivex.android.b.a.a();
    paramb = paramb.a((n)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/registration/d/a$a;
    ((a.a)localObject4).<init>(this);
    localObject4 = (q)localObject4;
    paramb.a((q)localObject4);
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "data");
    n.a(parama);
    parama = Truepay.getInstance();
    k.a(parama, "Truepay.getInstance()");
    parama = parama.getAnalyticLoggerHelper();
    Integer localInteger = Integer.valueOf(1);
    parama.a("PayBankAccountsLinked", localInteger);
  }
  
  public final void a(com.truecaller.truepay.data.d.a parama, com.truecaller.truepay.app.ui.registration.c.o paramo)
  {
    boolean bool1 = true;
    if (paramo != null)
    {
      boolean bool2 = paramo.a();
      if (bool2 == bool1) {
        return;
      }
    }
    boolean bool3;
    if (paramo != null)
    {
      bool3 = paramo.b();
      if (bool3 == bool1) {
        return;
      }
    }
    if (parama != null)
    {
      paramo = j.q();
      bool3 = paramo.a();
      boolean bool4;
      if (!bool3)
      {
        bool4 = false;
        parama = null;
      }
      if (parama != null)
      {
        parama = parama.d();
        if (parama == null) {
          return;
        }
        int i1 = parama.hashCode();
        switch (i1)
        {
        default: 
          break;
        case 2634539: 
          paramo = "VIJB";
          bool4 = parama.equals(paramo);
          if (!bool4) {
            break label261;
          }
          break;
        case 2241460: 
          paramo = "ICIC";
          bool4 = parama.equals(paramo);
          if (!bool4) {
            break label261;
          }
          c.a("icici");
          return;
        case 2096286: 
          paramo = "DGGB";
          bool4 = parama.equals(paramo);
          if (!bool4) {
            break label261;
          }
          break;
        case 2040467: 
          paramo = "BKDN";
          bool4 = parama.equals(paramo);
          if (!bool4) {
            break label261;
          }
          break;
        case 2031279: 
          paramo = "BARB";
          bool4 = parama.equals(paramo);
          if (!bool4) {
            break label261;
          }
        }
        parama = c;
        paramo = "baroda";
        parama.a(paramo);
        label261:
        return;
      }
    }
  }
  
  final void a(String paramString)
  {
    o.b("s#&bf2)^hn@1lp*n", paramString);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "accountProviderId");
    k.b(paramString2, "bankId");
    com.truecaller.truepay.app.ui.registration.c.g localg = new com/truecaller/truepay/app/ui/registration/c/g;
    localg.<init>(paramString1, paramString2);
    paramString1 = l.a(localg);
    paramString2 = io.reactivex.g.a.b();
    paramString1 = paramString1.b(paramString2);
    paramString2 = io.reactivex.android.b.a.a();
    paramString1 = paramString1.a(paramString2);
    paramString2 = new com/truecaller/truepay/app/ui/registration/d/a$c;
    paramString2.<init>(this);
    paramString2 = (q)paramString2;
    paramString1.a(paramString2);
  }
  
  public final void a(ArrayList paramArrayList)
  {
    k.b(paramArrayList, "accounts");
    n.a(paramArrayList);
    Object localObject = Truepay.getInstance();
    k.a(localObject, "Truepay.getInstance()");
    localObject = ((Truepay)localObject).getAnalyticLoggerHelper();
    paramArrayList = Integer.valueOf(paramArrayList.size());
    ((com.truecaller.truepay.data.b.a)localObject).a("PayBankAccountsLinked", paramArrayList);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = af_();
    if (localObject == null) {
      return;
    }
    localObject = (b.b)af_();
    if (localObject != null)
    {
      ((b.b)localObject).a(paramBoolean);
      return;
    }
  }
  
  public final void a(boolean paramBoolean, int paramInt)
  {
    Object localObject1 = new c/g/b/v$a;
    ((v.a)localObject1).<init>();
    a = paramBoolean;
    Object localObject2 = new com/truecaller/truepay/app/ui/registration/c/p;
    ((com.truecaller.truepay.app.ui.registration.c.p)localObject2).<init>();
    Object localObject3 = a.a();
    Object localObject4 = localObject3;
    localObject4 = (CharSequence)localObject3;
    boolean bool1 = true;
    if (localObject4 != null)
    {
      bool2 = c.n.m.a((CharSequence)localObject4);
      if (!bool2)
      {
        bool2 = false;
        localObject4 = null;
        break label78;
      }
    }
    boolean bool2 = true;
    label78:
    bool2 ^= bool1;
    Object localObject5 = null;
    if (!bool2) {
      localObject3 = null;
    }
    if (localObject3 == null)
    {
      localObject3 = p.a(paramInt);
      if (localObject3 != null) {
        localObject3 = h;
      } else {
        localObject3 = null;
      }
    }
    ((com.truecaller.truepay.app.ui.registration.c.p)localObject2).a((String)localObject3);
    localObject3 = b.a();
    localObject4 = localObject3;
    localObject4 = (CharSequence)localObject3;
    if (localObject4 != null)
    {
      bool2 = c.n.m.a((CharSequence)localObject4);
      if (!bool2)
      {
        bool2 = false;
        localObject4 = null;
        break label193;
      }
    }
    bool2 = true;
    label193:
    bool2 ^= bool1;
    if (!bool2) {
      localObject3 = null;
    }
    if (localObject3 == null)
    {
      localObject3 = p.a(paramInt);
      if (localObject3 != null) {
        localObject3 = g;
      } else {
        localObject3 = null;
      }
    }
    ((com.truecaller.truepay.app.ui.registration.c.p)localObject2).b((String)localObject3);
    localObject3 = p;
    Object localObject6 = ((h)localObject3).a(paramInt);
    if (localObject6 != null)
    {
      localObject6 = i;
    }
    else
    {
      paramInt = 0;
      localObject6 = null;
    }
    ((com.truecaller.truepay.app.ui.registration.c.p)localObject2).e((String)localObject6);
    localObject6 = ((com.truecaller.truepay.app.ui.registration.c.p)localObject2).a();
    localObject3 = ((com.truecaller.truepay.app.ui.registration.c.p)localObject2).b();
    localObject6 = l.a((String)localObject6, (String)localObject3);
    ((com.truecaller.truepay.app.ui.registration.c.p)localObject2).c((String)localObject6);
    ((com.truecaller.truepay.app.ui.registration.c.p)localObject2).a(false);
    localObject6 = j.q();
    paramInt = ((com.truecaller.featuretoggles.b)localObject6).a();
    if (paramInt != 0) {
      localObject5 = localObject2;
    }
    if (localObject5 != null)
    {
      localObject6 = c.a();
      ((com.truecaller.truepay.app.ui.registration.c.p)localObject2).d((String)localObject6);
    }
    localObject6 = k.a((com.truecaller.truepay.app.ui.registration.c.p)localObject2);
    localObject3 = new com/truecaller/truepay/app/ui/registration/d/a$e;
    ((a.e)localObject3).<init>(this, 5);
    localObject3 = (io.reactivex.c.e)localObject3;
    localObject6 = ((io.reactivex.o)localObject6).c((io.reactivex.c.e)localObject3);
    localObject3 = io.reactivex.g.a.b();
    localObject6 = ((io.reactivex.d)localObject6).b((n)localObject3);
    localObject3 = io.reactivex.android.b.a.a();
    localObject6 = ((io.reactivex.d)localObject6).a((n)localObject3);
    localObject3 = new com/truecaller/truepay/app/ui/registration/d/a$j;
    ((a.j)localObject3).<init>(this, (com.truecaller.truepay.app.ui.registration.c.p)localObject2, (v.a)localObject1);
    localObject3 = (io.reactivex.c.d)localObject3;
    localObject2 = new com/truecaller/truepay/app/ui/registration/d/a$k;
    ((a.k)localObject2).<init>(this);
    localObject2 = (io.reactivex.c.d)localObject2;
    localObject1 = io.reactivex.d.b.a.c;
    localObject4 = p.a.a;
    localObject2 = ((io.reactivex.d)localObject6).a((io.reactivex.c.d)localObject3, (io.reactivex.c.d)localObject2, (io.reactivex.c.a)localObject1, (io.reactivex.c.d)localObject4);
    e.a((io.reactivex.a.b)localObject2);
  }
  
  public final void b(int paramInt)
  {
    b.b localb = (b.b)af_();
    if (localb != null)
    {
      localb.a(paramInt);
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localc = o;
      localInteger = Integer.valueOf(104);
      localc.a("j<@$f)qntd=?5e!y", localInteger);
      t.b();
      return;
    }
    com.truecaller.truepay.data.e.c localc = o;
    Integer localInteger = Integer.valueOf(105);
    localc.a("j<@$f)qntd=?5e!y", localInteger);
  }
  
  public final void d()
  {
    e.b();
    b.b localb = (b.b)af_();
    if (localb != null)
    {
      localb.t();
      return;
    }
  }
  
  public final void e()
  {
    b.b localb = (b.b)af_();
    if (localb != null)
    {
      String str = h();
      k.a(str, "getUserMsisdn()");
      r localr = i;
      localb.b(str, localr);
      return;
    }
  }
  
  public final int f()
  {
    com.truecaller.truepay.app.utils.c localc = s;
    boolean bool = localc.a();
    if (bool) {
      return 4;
    }
    return 0;
  }
  
  public final int g()
  {
    Object localObject = p.h();
    boolean bool = com.truecaller.multisim.b.b.c((List)localObject);
    if (bool) {
      return 5;
    }
    localObject = r;
    List localList = p.h();
    bool = ((com.truecaller.multisim.b.b)localObject).b(localList);
    if (bool) {
      return 6;
    }
    return 0;
  }
  
  final String h()
  {
    return o.a("s#&bf2)^hn@1lp*n", "");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */