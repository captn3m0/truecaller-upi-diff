package com.truecaller.truepay.app.ui.registration.d;

import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.truepay.app.utils.at;
import com.truecaller.truepay.data.f.ag;
import com.truecaller.utils.n;
import java.util.List;
import kotlinx.coroutines.bn;

public final class k
  extends com.truecaller.truepay.app.ui.base.a.a
  implements j
{
  public final com.truecaller.truepay.data.e.e A;
  final com.truecaller.truepay.data.e.e B;
  final com.truecaller.truepay.app.utils.l C;
  public final com.truecaller.multisim.b.b D;
  public final com.truecaller.truepay.app.utils.c E;
  final com.truecaller.featuretoggles.e F;
  private final com.truecaller.truepay.data.e.e G;
  public final String[] a;
  public bn b;
  public com.truecaller.truepay.data.d.a c;
  public String f;
  public String g;
  public Integer h;
  boolean i;
  public final int j;
  public boolean k;
  public boolean l;
  public final c.d.f m;
  public final c.d.f n;
  public final n o;
  final ag p;
  public final h q;
  public final com.truecaller.truepay.data.api.f r;
  public final com.truecaller.truepay.data.e.c s;
  public final com.truecaller.truepay.a.a.e.a t;
  public final com.truecaller.truepay.app.ui.registration.views.c.e u;
  public final at v;
  final com.truecaller.truepay.app.utils.e w;
  public final com.truecaller.utils.l x;
  public final com.truecaller.truepay.data.e.e y;
  public final com.truecaller.truepay.data.e.e z;
  
  public k(c.d.f paramf1, c.d.f paramf2, n paramn, ag paramag, h paramh, com.truecaller.truepay.data.api.f paramf, com.truecaller.truepay.data.e.c paramc, com.truecaller.truepay.a.a.e.a parama, com.truecaller.truepay.app.ui.registration.views.c.e parame, at paramat, com.truecaller.truepay.app.utils.e parame1, com.truecaller.utils.l paraml, com.truecaller.truepay.data.e.e parame2, com.truecaller.truepay.data.e.e parame3, com.truecaller.truepay.data.e.e parame4, com.truecaller.truepay.data.e.e parame5, com.truecaller.truepay.data.e.e parame6, com.truecaller.truepay.app.utils.l paraml1, com.truecaller.multisim.b.b paramb, com.truecaller.truepay.app.utils.c paramc1, com.truecaller.featuretoggles.e parame7)
  {
    m = paramf1;
    n = paramf2;
    o = paramn;
    p = paramag;
    q = paramh;
    r = paramf;
    s = paramc;
    t = parama;
    u = parame;
    v = paramat;
    w = parame1;
    x = paraml;
    G = parame2;
    y = parame3;
    localObject = parame4;
    z = parame4;
    A = parame5;
    localObject = parame6;
    B = parame6;
    C = paraml1;
    localObject = paramb;
    D = paramb;
    E = paramc1;
    localObject = parame7;
    F = parame7;
    String[] tmp294_291 = new String[3];
    String[] tmp295_294 = tmp294_291;
    String[] tmp295_294 = tmp294_291;
    tmp295_294[0] = "android.permission.SEND_SMS";
    tmp295_294[1] = "android.permission.RECEIVE_SMS";
    tmp295_294[2] = "android.permission.READ_SMS";
    localObject = tmp295_294;
    a = ((String[])localObject);
    i = true;
    j = 100;
  }
  
  private final void a(SimInfo paramSimInfo)
  {
    String str = String.valueOf(d);
    g = str;
    paramSimInfo = Integer.valueOf(a);
    h = paramSimInfo;
    i = false;
    paramSimInfo = (com.truecaller.truepay.app.ui.registration.views.c.f)af_();
    if (paramSimInfo != null)
    {
      paramSimInfo.b();
      return;
    }
  }
  
  public final void a()
  {
    Object localObject1 = F.D();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    Object localObject2 = E;
    boolean bool2 = ((com.truecaller.truepay.app.utils.c)localObject2).a();
    if ((bool2) && (bool1))
    {
      localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)af_();
      if (localObject1 != null) {
        ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).j();
      }
      return;
    }
    localObject2 = q.h();
    bool2 = com.truecaller.multisim.b.b.c((List)localObject2);
    if ((bool2) && (bool1))
    {
      localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)af_();
      if (localObject1 != null) {
        ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).l();
      }
      return;
    }
    localObject2 = D;
    List localList = q.h();
    bool2 = ((com.truecaller.multisim.b.b)localObject2).b(localList);
    if ((bool2) && (bool1))
    {
      localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)af_();
      if (localObject1 != null) {
        ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).k();
      }
      return;
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = h;
      if (localObject1 != null)
      {
        localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)af_();
        if (localObject1 != null) {
          ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).b();
        }
        return;
      }
    }
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)af_();
      if (localObject1 != null) {
        ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).h();
      }
      return;
    }
    localObject1 = q.h();
    int i1 = ((List)localObject1).size();
    bool2 = false;
    localObject2 = null;
    int i2 = 1;
    if (i1 == i2)
    {
      localObject1 = q.h().get(0);
      c.g.b.k.a(localObject1, "multiSimManager.allSimInfos[0]");
      localObject1 = (SimInfo)localObject1;
      a((SimInfo)localObject1);
      return;
    }
    localObject1 = h;
    if (localObject1 == null)
    {
      localObject1 = q.h();
      i1 = ((List)localObject1).size();
      if (i1 > i2)
      {
        localObject1 = (CharSequence)q.h().get(0)).d;
        if (localObject1 != null)
        {
          i1 = ((CharSequence)localObject1).length();
          if (i1 != 0)
          {
            i1 = 0;
            localObject1 = null;
            break label365;
          }
        }
        i1 = 1;
        label365:
        if (i1 == 0)
        {
          localObject1 = (CharSequence)q.h().get(i2)).d;
          if (localObject1 != null)
          {
            i1 = ((CharSequence)localObject1).length();
            if (i1 != 0)
            {
              i1 = 0;
              localObject1 = null;
              break label422;
            }
          }
          i1 = 1;
          label422:
          if (i1 == 0)
          {
            localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)af_();
            if (localObject1 != null) {
              ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).g();
            }
            return;
          }
        }
      }
    }
    localObject1 = h;
    if (localObject1 == null)
    {
      localObject1 = q.h();
      i1 = ((List)localObject1).size();
      if (i1 > i2)
      {
        localObject1 = (CharSequence)q.h().get(0)).d;
        if (localObject1 != null)
        {
          i1 = ((CharSequence)localObject1).length();
          if (i1 != 0)
          {
            i1 = 0;
            localObject1 = null;
            break label529;
          }
        }
        i1 = 1;
        label529:
        if (i1 != 0)
        {
          localObject1 = (CharSequence)q.h().get(i2)).d;
          if (localObject1 != null)
          {
            i1 = ((CharSequence)localObject1).length();
            if (i1 != 0)
            {
              i1 = 0;
              localObject1 = null;
              break label586;
            }
          }
          i1 = 1;
          label586:
          if (i1 == 0)
          {
            localObject1 = q.h().get(i2);
            c.g.b.k.a(localObject1, "multiSimManager.allSimInfos[1]");
            localObject1 = (SimInfo)localObject1;
            a((SimInfo)localObject1);
            return;
          }
        }
      }
    }
    localObject1 = h;
    if (localObject1 == null)
    {
      localObject1 = q.h();
      i1 = ((List)localObject1).size();
      if (i1 > i2)
      {
        localObject1 = (CharSequence)q.h().get(0)).d;
        if (localObject1 != null)
        {
          i1 = ((CharSequence)localObject1).length();
          if (i1 != 0)
          {
            i1 = 0;
            localObject1 = null;
            break label709;
          }
        }
        i1 = 1;
        label709:
        if (i1 == 0)
        {
          localObject1 = (CharSequence)q.h().get(i2)).d;
          if (localObject1 != null)
          {
            i1 = ((CharSequence)localObject1).length();
            if (i1 != 0)
            {
              i2 = 0;
              localList = null;
            }
          }
          if (i2 != 0)
          {
            localObject1 = q.h().get(0);
            c.g.b.k.a(localObject1, "multiSimManager.allSimInfos[0]");
            localObject1 = (SimInfo)localObject1;
            a((SimInfo)localObject1);
            return;
          }
        }
      }
    }
    localObject1 = h;
    if (localObject1 == null)
    {
      localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)af_();
      if (localObject1 != null) {
        ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).g();
      }
      return;
    }
    localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)af_();
    if (localObject1 != null)
    {
      ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).c();
      return;
    }
  }
  
  public final void b()
  {
    bn localbn = b;
    if (localbn != null) {
      localbn.n();
    }
    super.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */