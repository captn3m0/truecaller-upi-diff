package com.truecaller.truepay.app.ui.registration.d;

import c.d.c;
import c.g.a.m;
import c.g.b.v.c;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import java.util.HashMap;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

public final class k$d
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  Object f;
  int g;
  private ag i;
  
  public k$d(k paramk, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/truepay/app/ui/registration/d/k$d;
    k localk = h;
    locald.<init>(localk, paramc);
    paramObject = (ag)paramObject;
    i = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int j = g;
    int k = 1;
    Object localObject2 = null;
    Object localObject3;
    boolean bool2;
    switch (j)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      localObject1 = (String)f;
      localObject3 = (com.truecaller.truepay.app.ui.registration.views.c.f)e;
      localObject2 = (v.c)d;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject4 = localObject3;
        break label901;
      }
      throw a;
    case 2: 
      localObject1 = (String)f;
      localObject3 = (com.truecaller.truepay.app.ui.registration.views.c.f)e;
      localObject2 = (v.c)d;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject4 = localObject3;
        break label589;
      }
      throw a;
    case 1: 
      localObject3 = (ag)a;
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label1006;
      }
      localObject3 = i;
      paramObject = h.m;
      localObject5 = h.n;
      paramObject = ((c.d.f)paramObject).plus((c.d.f)localObject5);
      localObject5 = new com/truecaller/truepay/app/ui/registration/d/k$d$a;
      ((k.d.a)localObject5).<init>(this, null);
      localObject5 = (m)localObject5;
      a = localObject3;
      g = k;
      paramObject = g.a((c.d.f)paramObject, (m)localObject5, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (HashMap)paramObject;
    Object localObject5 = ((HashMap)paramObject).get("bank_symbol");
    v.c localc = new c/g/b/v$c;
    localc.<init>();
    Object localObject4 = ((HashMap)paramObject).get("sim_slot");
    a = localObject4;
    localObject4 = (com.truecaller.truepay.app.ui.registration.views.c.f)h.af_();
    if (localObject4 == null) {
      return x.a;
    }
    Object localObject6 = h.F.D();
    boolean bool3 = ((com.truecaller.featuretoggles.b)localObject6).a();
    Object localObject7;
    int m;
    int n;
    if (bool3)
    {
      localObject6 = a;
      if (localObject6 != null)
      {
        localObject6 = h.D;
        localObject7 = a;
        if (localObject7 != null)
        {
          localObject7 = (Integer)localObject7;
          m = ((Integer)localObject7).intValue();
          bool3 = ((com.truecaller.multisim.b.b)localObject6).a(m);
          if (!bool3)
          {
            a = null;
          }
          else
          {
            localObject6 = h.q;
            localObject7 = a;
            if (localObject7 != null)
            {
              localObject7 = (Integer)localObject7;
              m = ((Integer)localObject7).intValue();
              localObject6 = ((h)localObject6).a(m);
              if (localObject6 != null) {
                localObject2 = d;
              }
              if (localObject5 != null)
              {
                localObject6 = h.w;
                localObject7 = localObject5;
                localObject7 = (String)localObject5;
                a = localObject3;
                b = paramObject;
                c = localObject5;
                d = localc;
                e = localObject4;
                f = localObject2;
                n = 2;
                g = n;
                paramObject = ((com.truecaller.truepay.app.utils.e)localObject6).a((String)localObject7, this);
                if (paramObject == localObject1) {
                  return localObject1;
                }
                localObject1 = localObject2;
                localObject2 = localc;
                label589:
                paramObject = (com.truecaller.truepay.data.d.a)paramObject;
                if (paramObject != null)
                {
                  localObject3 = h;
                  c = ((com.truecaller.truepay.data.d.a)paramObject);
                  paramObject = ((com.truecaller.truepay.data.d.a)paramObject).b();
                  f = ((String)paramObject);
                  paramObject = h;
                  localObject3 = (Integer)a;
                  h = ((Integer)localObject3);
                  paramObject = h;
                  localObject1 = String.valueOf(localObject1);
                  g = ((String)localObject1);
                  paramObject = h;
                  k = k;
                }
              }
              else
              {
                ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject4).i();
                break label1002;
              }
            }
            else
            {
              paramObject = new c/u;
              ((u)paramObject).<init>("null cannot be cast to non-null type kotlin.Int");
              throw ((Throwable)paramObject);
            }
          }
        }
        else
        {
          paramObject = new c/u;
          ((u)paramObject).<init>("null cannot be cast to non-null type kotlin.Int");
          throw ((Throwable)paramObject);
        }
      }
      else
      {
        ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject4).i();
        break label1002;
      }
    }
    else if (localObject5 != null)
    {
      localObject6 = a;
      if (localObject6 != null)
      {
        localObject6 = h.q;
        localObject7 = a;
        if (localObject7 != null)
        {
          localObject7 = (Integer)localObject7;
          m = ((Integer)localObject7).intValue();
          localObject6 = ((h)localObject6).a(m);
          if (localObject6 != null) {
            localObject2 = d;
          }
          localObject6 = h;
          boolean bool4 = q.j();
          i = bool4;
          localObject6 = h.w;
          localObject7 = localObject5;
          localObject7 = (String)localObject5;
          a = localObject3;
          b = paramObject;
          c = localObject5;
          d = localc;
          e = localObject4;
          f = localObject2;
          n = 3;
          g = n;
          paramObject = ((com.truecaller.truepay.app.utils.e)localObject6).a((String)localObject7, this);
          if (paramObject == localObject1) {
            return localObject1;
          }
          localObject1 = localObject2;
          localObject2 = localc;
          label901:
          paramObject = (com.truecaller.truepay.data.d.a)paramObject;
          if (paramObject != null)
          {
            localObject3 = h;
            c = ((com.truecaller.truepay.data.d.a)paramObject);
            paramObject = ((com.truecaller.truepay.data.d.a)paramObject).b();
            f = ((String)paramObject);
            paramObject = h;
            localObject3 = (Integer)a;
            h = ((Integer)localObject3);
            paramObject = h;
            localObject1 = String.valueOf(localObject1);
            g = ((String)localObject1);
            paramObject = h;
            k = k;
          }
        }
        else
        {
          paramObject = new c/u;
          ((u)paramObject).<init>("null cannot be cast to non-null type kotlin.Int");
          throw ((Throwable)paramObject);
        }
      }
    }
    ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject4).i();
    label1002:
    return x.a;
    label1006:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.d.k.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */