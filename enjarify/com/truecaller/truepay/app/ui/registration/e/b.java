package com.truecaller.truepay.app.ui.registration.e;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import c.a.f;
import c.g.b.k;
import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.Truepay;

public final class b
  extends BroadcastReceiver
{
  public static final b.a c;
  private static b i;
  public e a;
  public boolean b;
  private final String[] d;
  private final String e;
  private final String f;
  private final String g;
  private a h;
  
  static
  {
    b.a locala = new com/truecaller/truepay/app/ui/registration/e/b$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public b(a parama)
  {
    String[] arrayOfString = { "ABSENT", "NOT_READY" };
    d = arrayOfString;
    e = "ss";
    f = "android.intent.action.SIM_STATE_CHANGED";
    g = "android.intent.action.AIRPLANE_MODE";
    a(parama);
    Truepay.getApplicationComponent().a(this);
  }
  
  public final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    b = true;
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>();
    Object localObject = f;
    localIntentFilter.addAction((String)localObject);
    localObject = g;
    localIntentFilter.addAction((String)localObject);
    localObject = this;
    localObject = (BroadcastReceiver)this;
    paramContext.registerReceiver((BroadcastReceiver)localObject, localIntentFilter);
  }
  
  public final void a(a parama)
  {
    k.b(parama, "simAeroplaneModeStateChange");
    h = parama;
  }
  
  public final void b(Context paramContext)
  {
    k.b(paramContext, "context");
    Object localObject = this;
    try
    {
      localObject = (BroadcastReceiver)this;
      paramContext.unregisterReceiver((BroadcastReceiver)localObject);
      paramContext = null;
      b = false;
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException) {}
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = "context";
    k.b(paramContext, str);
    k.b(paramIntent, "intent");
    paramContext = a;
    if (paramContext == null)
    {
      str = "featuresRegistry";
      k.a(str);
    }
    paramContext = paramContext.D();
    boolean bool1 = paramContext.a();
    if (bool1)
    {
      paramContext = paramIntent.getAction();
      str = f;
      boolean bool2 = k.a(paramContext, str);
      if (bool2)
      {
        paramContext = paramIntent.getExtras();
        if (paramContext == null) {
          k.a();
        }
        paramIntent = e;
        paramContext = paramContext.getString(paramIntent);
        paramIntent = d;
        bool1 = f.b(paramIntent, paramContext);
        if (bool1)
        {
          paramContext = h;
          if (paramContext == null)
          {
            paramIntent = "simAeroplaneModeStateChange";
            k.a(paramIntent);
          }
          paramContext.d();
        }
      }
      else
      {
        paramIntent = g;
        bool1 = k.a(paramContext, paramIntent);
        if (bool1)
        {
          paramContext = h;
          if (paramContext == null)
          {
            paramIntent = "simAeroplaneModeStateChange";
            k.a(paramIntent);
          }
          paramContext.e();
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */