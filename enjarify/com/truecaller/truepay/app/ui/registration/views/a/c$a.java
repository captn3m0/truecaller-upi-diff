package com.truecaller.truepay.app.ui.registration.views.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;

final class c$a
  extends RecyclerView.ViewHolder
{
  TextView a;
  ImageView b;
  CheckBox c;
  
  c$a(c paramc, View paramView)
  {
    super(paramView);
    int i = R.id.bank_name_textView;
    paramc = (TextView)paramView.findViewById(i);
    a = paramc;
    i = R.id.bank_imageView;
    paramc = (ImageView)paramView.findViewById(i);
    b = paramc;
    i = R.id.cb_selector;
    paramc = (CheckBox)paramView.findViewById(i);
    c = paramc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.a.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */