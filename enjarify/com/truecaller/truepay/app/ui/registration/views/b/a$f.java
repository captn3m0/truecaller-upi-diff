package com.truecaller.truepay.app.ui.registration.views.b;

import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.truecaller.multisim.h;

final class a$f
  implements View.OnClickListener
{
  a$f(a parama) {}
  
  public final void onClick(View paramView)
  {
    paramView = a.c();
    boolean bool1 = paramView.j();
    Object localObject;
    String str1;
    String str2;
    String str3;
    if (bool1)
    {
      paramView = a;
      bool1 = a.e(paramView);
      if (!bool1)
      {
        paramView = a;
        int i = a.c(paramView);
        localObject = a;
        switch (i)
        {
        default: 
          i = -1;
          break;
        case 1: 
          i = 0;
          paramView = null;
          break;
        case 0: 
          i = 1;
        }
        a.a((a)localObject, i);
        paramView = a;
        int j = a.c(paramView);
        localObject = Integer.valueOf(j);
        paramView.a((Integer)localObject);
        a.g(a);
        paramView = a;
        localObject = "retry";
        boolean bool2 = a.h(paramView);
        a.a(paramView, (String)localObject, bool2);
        paramView = a.f(a);
        if (paramView != null)
        {
          localObject = a;
          str1 = "failure";
          str2 = "change_sim";
          paramView = paramView.b();
          str3 = "it.bankName";
          k.a(paramView, str3);
          a.a((a)localObject, str1, str2, paramView);
        }
        return;
      }
    }
    paramView = a.f(a);
    if (paramView != null)
    {
      localObject = a;
      str1 = "failure";
      str2 = "select_another_bank";
      paramView = paramView.b();
      str3 = "it.bankName";
      k.a(paramView, str3);
      a.a((a)localObject, str1, str2, paramView);
    }
    a.a(a, "retry");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */