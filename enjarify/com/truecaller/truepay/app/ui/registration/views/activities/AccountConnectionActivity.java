package com.truecaller.truepay.app.ui.registration.views.activities;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.FrameLayout;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.views.b.a.b;
import com.truecaller.truepay.app.ui.registration.views.b.j.a;
import com.truecaller.truepay.app.ui.registration.views.b.k.a;
import java.io.Serializable;
import java.util.HashMap;

public final class AccountConnectionActivity
  extends AppCompatActivity
  implements a.b, j.a, k.a
{
  private HashMap a;
  
  private View a(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      a = ((HashMap)localObject1);
    }
    localObject1 = a;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = a;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    setResult(-1);
    finish();
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    c.g.b.k.b(parama, "account");
    parama = com.truecaller.truepay.app.ui.registration.views.b.j.a(parama);
    android.support.v4.app.o localo = getSupportFragmentManager().a();
    int i = R.id.container;
    parama = (Fragment)parama;
    String str = com.truecaller.truepay.app.ui.registration.views.b.j.class.getSimpleName();
    localo.a(i, parama, str).c();
  }
  
  public final void b()
  {
    a();
  }
  
  public final void b(com.truecaller.truepay.data.api.model.a parama)
  {
    c.g.b.k.b(parama, "account");
    parama = com.truecaller.truepay.app.ui.registration.views.b.k.a(parama);
    android.support.v4.app.o localo = getSupportFragmentManager().a();
    int i = R.id.container;
    parama = (Fragment)parama;
    String str = com.truecaller.truepay.app.ui.registration.views.b.k.class.getSimpleName();
    localo.a(i, parama, str).d();
  }
  
  public final void c()
  {
    int i = R.id.progressFrame;
    Object localObject = (FrameLayout)a(i);
    c.g.b.k.a(localObject, "progressFrame");
    ((FrameLayout)localObject).setVisibility(0);
    i = R.id.progressFrame;
    localObject = (FrameLayout)a(i);
    c.g.b.k.a(localObject, "progressFrame");
    ((FrameLayout)localObject).setClickable(true);
    localObject = new com/truecaller/truepay/app/ui/registration/views/a;
    ((com.truecaller.truepay.app.ui.registration.views.a)localObject).<init>();
    ((com.truecaller.truepay.app.ui.registration.views.a)localObject).a("");
    android.support.v4.app.o localo = getSupportFragmentManager().a();
    int j = R.id.progressFrame;
    localObject = (Fragment)localObject;
    String str = com.truecaller.truepay.app.ui.registration.views.a.class.getSimpleName();
    localo.b(j, (Fragment)localObject, str).d();
  }
  
  public final void d()
  {
    int i = R.id.progressFrame;
    FrameLayout localFrameLayout = (FrameLayout)a(i);
    c.g.b.k.a(localFrameLayout, "progressFrame");
    localFrameLayout.setVisibility(8);
  }
  
  public final void onBackPressed()
  {
    Object localObject1 = Truepay.getInstance();
    Object localObject2 = "Truepay.getInstance()";
    c.g.b.k.a(localObject1, (String)localObject2);
    boolean bool = ((Truepay)localObject1).isRegistrationComplete();
    if (bool)
    {
      a();
      return;
    }
    localObject1 = Truepay.getInstance();
    c.g.b.k.a(localObject1, "Truepay.getInstance()");
    ((Truepay)localObject1).getAnalyticLoggerHelper().b("shown", "exit_registration", null);
    localObject1 = new android/app/AlertDialog$Builder;
    localObject2 = new android/view/ContextThemeWrapper;
    Object localObject3 = this;
    localObject3 = (Context)this;
    int i = R.style.AppTheme_BlueAccent;
    ((ContextThemeWrapper)localObject2).<init>((Context)localObject3, i);
    localObject2 = (Context)localObject2;
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    int j = R.string.reg_cancel_alert_body;
    localObject2 = (CharSequence)getString(j);
    localObject1 = ((AlertDialog.Builder)localObject1).setMessage((CharSequence)localObject2);
    j = R.string.reg_cancel_alert_positive_btn;
    localObject2 = (CharSequence)getString(j);
    localObject3 = (DialogInterface.OnClickListener)AccountConnectionActivity.a.a;
    localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton((CharSequence)localObject2, (DialogInterface.OnClickListener)localObject3);
    j = R.string.reg_cancel_alert_negative_btn;
    localObject2 = (CharSequence)getString(j);
    localObject3 = new com/truecaller/truepay/app/ui/registration/views/activities/AccountConnectionActivity$b;
    ((AccountConnectionActivity.b)localObject3).<init>(this);
    localObject3 = (DialogInterface.OnClickListener)localObject3;
    localObject1 = ((AlertDialog.Builder)localObject1).setNegativeButton((CharSequence)localObject2, (DialogInterface.OnClickListener)localObject3);
    j = 0;
    localObject2 = null;
    localObject1 = ((AlertDialog.Builder)localObject1).setCancelable(false);
    if (localObject1 != null)
    {
      ((AlertDialog.Builder)localObject1).show();
      return;
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.layout.activity_account_connection;
    setContentView(i);
    paramBundle = com.truecaller.truepay.app.ui.registration.views.b.a.e;
    paramBundle = (com.truecaller.truepay.data.d.a)getIntent().getSerializableExtra("selected_bank");
    Object localObject1 = getIntent();
    int j = ((Intent)localObject1).getIntExtra("selected_sim", -1);
    Object localObject2 = (com.truecaller.truepay.app.ui.registration.c.o)getIntent().getSerializableExtra("cd_response");
    Object localObject3 = getIntent();
    boolean bool = ((Intent)localObject3).getBooleanExtra("is_using_sms_data", false);
    Object localObject4 = getIntent().getStringExtra("reg_source");
    Object localObject5 = getIntent();
    Object localObject6 = "action";
    localObject5 = ((Intent)localObject5).getStringExtra((String)localObject6);
    if (localObject5 == null)
    {
      localObject5 = getIntent();
      localObject6 = "intent";
      c.g.b.k.a(localObject5, (String)localObject6);
      localObject5 = ((Intent)localObject5).getData();
      if (localObject5 != null) {
        localObject5 = ((Uri)localObject5).getHost();
      } else {
        localObject5 = null;
      }
    }
    localObject6 = new android/os/Bundle;
    ((Bundle)localObject6).<init>();
    paramBundle = (Serializable)paramBundle;
    ((Bundle)localObject6).putSerializable("selected_bank", paramBundle);
    localObject1 = (Serializable)Integer.valueOf(j);
    ((Bundle)localObject6).putSerializable("selected_sim", (Serializable)localObject1);
    localObject2 = (Serializable)localObject2;
    ((Bundle)localObject6).putSerializable("cd_response", (Serializable)localObject2);
    localObject1 = (Serializable)Boolean.valueOf(bool);
    ((Bundle)localObject6).putSerializable("is_using_sms_data", (Serializable)localObject1);
    localObject4 = (Serializable)localObject4;
    ((Bundle)localObject6).putSerializable("reg_source", (Serializable)localObject4);
    localObject5 = (Serializable)localObject5;
    ((Bundle)localObject6).putSerializable("deeplink_host", (Serializable)localObject5);
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/a;
    paramBundle.<init>();
    paramBundle.setArguments((Bundle)localObject6);
    localObject1 = getSupportFragmentManager().a();
    int k = R.id.container;
    paramBundle = (Fragment)paramBundle;
    localObject3 = com.truecaller.truepay.app.ui.registration.views.b.a.class.getSimpleName();
    ((android.support.v4.app.o)localObject1).a(k, paramBundle, (String)localObject3).c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.activities.AccountConnectionActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */