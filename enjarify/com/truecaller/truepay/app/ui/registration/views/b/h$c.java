package com.truecaller.truepay.app.ui.registration.views.b;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.history.views.activities.TruePayWebViewActivity;

final class h$c
  implements View.OnClickListener
{
  h$c(h paramh) {}
  
  public final void onClick(View paramView)
  {
    paramView = new android/content/Intent;
    Context localContext = (Context)a.getActivity();
    paramView.<init>(localContext, TruePayWebViewActivity.class);
    paramView.putExtra("url", "https://support.truecaller.com/hc/en-us/articles/360001677057-Terms-and-Conditions-");
    paramView.putExtra("show_toolbar", true);
    Object localObject = a;
    int i = R.string.term_conditions_header;
    localObject = ((h)localObject).getString(i);
    paramView.putExtra("title", (String)localObject);
    a.startActivity(paramView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.h.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */