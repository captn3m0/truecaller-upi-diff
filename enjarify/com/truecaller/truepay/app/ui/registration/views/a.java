package com.truecaller.truepay.app.ui.registration.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;

public class a
  extends Fragment
{
  public String a;
  private RelativeLayout b;
  private TextView c;
  private int d;
  
  public final void a(String paramString)
  {
    a = paramString;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = R.layout.fragment_progress;
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    int j = R.id.progress_text;
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(j);
    c = paramViewGroup;
    j = R.id.progress_wheel_layout;
    paramViewGroup = (RelativeLayout)paramLayoutInflater.findViewById(j);
    b = paramViewGroup;
    paramViewGroup = a;
    paramBundle = c;
    if (paramBundle != null)
    {
      boolean bool = TextUtils.isEmpty(paramViewGroup);
      if (!bool)
      {
        paramBundle = c;
        paramBundle.setText(paramViewGroup);
      }
    }
    j = d;
    paramBundle = b;
    if ((paramBundle != null) && (j != 0)) {
      paramBundle.setBackgroundColor(j);
    }
    return paramLayoutInflater;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */