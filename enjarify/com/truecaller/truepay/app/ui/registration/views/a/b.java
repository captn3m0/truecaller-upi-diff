package com.truecaller.truepay.app.ui.registration.views.a;

import android.support.v7.widget.RecyclerView.Adapter;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.b.a.a;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import java.util.List;

public final class b
  extends RecyclerView.Adapter
{
  public r a;
  public au b;
  private final List c;
  private int d;
  
  public b(List paramList)
  {
    c = paramList;
    paramList = com.truecaller.truepay.app.ui.registration.b.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramList.a(locala).a().a(this);
  }
  
  public final com.truecaller.truepay.data.api.model.a a()
  {
    List localList = c;
    int i = d;
    return (com.truecaller.truepay.data.api.model.a)localList.get(i);
  }
  
  public final int getItemCount()
  {
    return c.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */