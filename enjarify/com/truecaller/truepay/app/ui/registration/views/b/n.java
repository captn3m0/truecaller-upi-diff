package com.truecaller.truepay.app.ui.registration.views.b;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.b.a.a;
import com.truecaller.truepay.app.ui.registration.views.c.j;
import java.util.HashMap;

public final class n
  extends android.support.design.widget.b
  implements j
{
  public com.truecaller.truepay.app.ui.registration.d.r a;
  public com.truecaller.truepay.app.utils.r b;
  public com.truecaller.utils.n c;
  private CheckBox d;
  private CheckBox e;
  private RelativeLayout f;
  private RelativeLayout g;
  private TextView h;
  private TextView i;
  private TextView j;
  private ImageView k;
  private ImageView l;
  private Button m;
  private n.a n;
  private HashMap o;
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i1 = 8;
    Object localObject2;
    if ((paramBoolean1) && (!paramBoolean2))
    {
      localObject1 = g;
      if (localObject1 == null)
      {
        localObject2 = "simLayout2";
        k.a((String)localObject2);
      }
      ((RelativeLayout)localObject1).setVisibility(i1);
    }
    else if ((!paramBoolean1) && (paramBoolean2))
    {
      localObject1 = f;
      if (localObject1 == null)
      {
        localObject2 = "simLayout1";
        k.a((String)localObject2);
      }
      ((RelativeLayout)localObject1).setVisibility(i1);
    }
    paramBoolean1 = false;
    Object localObject1 = null;
    Object localObject3;
    if (paramString1 != null)
    {
      localObject2 = f;
      if (localObject2 == null)
      {
        localObject3 = "simLayout1";
        k.a((String)localObject3);
      }
      ((RelativeLayout)localObject2).setVisibility(0);
      localObject2 = h;
      if (localObject2 == null)
      {
        localObject3 = "operatorSIM1";
        k.a((String)localObject3);
      }
      localObject3 = paramString1;
      localObject3 = (CharSequence)paramString1;
      ((TextView)localObject2).setText((CharSequence)localObject3);
      localObject2 = k;
      if (localObject2 == null)
      {
        localObject3 = "simOneImage";
        k.a((String)localObject3);
      }
      localObject3 = c;
      if (localObject3 == null)
      {
        localObject4 = "resourceProvider";
        k.a((String)localObject4);
      }
      Object localObject4 = b;
      if (localObject4 == null)
      {
        String str = "imageLoader";
        k.a(str);
      }
      int i2 = ((com.truecaller.truepay.app.utils.r)localObject4).a(paramString1);
      paramString1 = ((com.truecaller.utils.n)localObject3).c(i2);
      ((ImageView)localObject2).setImageDrawable(paramString1);
    }
    if (paramString2 != null)
    {
      paramString1 = g;
      if (paramString1 == null)
      {
        localObject2 = "simLayout2";
        k.a((String)localObject2);
      }
      paramString1.setVisibility(0);
      paramString1 = i;
      if (paramString1 == null)
      {
        localObject1 = "operatorSIM2";
        k.a((String)localObject1);
      }
      localObject1 = paramString2;
      localObject1 = (CharSequence)paramString2;
      paramString1.setText((CharSequence)localObject1);
      paramString1 = l;
      if (paramString1 == null)
      {
        localObject1 = "simTwoImage";
        k.a((String)localObject1);
      }
      localObject1 = c;
      if (localObject1 == null)
      {
        localObject2 = "resourceProvider";
        k.a((String)localObject2);
      }
      localObject2 = b;
      if (localObject2 == null)
      {
        localObject3 = "imageLoader";
        k.a((String)localObject3);
      }
      int i3 = ((com.truecaller.truepay.app.utils.r)localObject2).a(paramString2);
      paramString2 = ((com.truecaller.utils.n)localObject1).c(i3);
      paramString1.setImageDrawable(paramString2);
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.registration.b.a.a();
    Object localObject = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a().a(this);
    paramBundle = n;
    boolean bool;
    if (paramBundle == null)
    {
      paramBundle = getActivity();
      bool = paramBundle instanceof n.a;
      if (bool)
      {
        paramBundle = getActivity();
        if (paramBundle != null)
        {
          paramBundle = (n.a)paramBundle;
          n = paramBundle;
          return;
        }
        paramBundle = new c/u;
        paramBundle.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registration.views.fragments.SimSelectionBottomSheetFragment.OnFragmentInteractionListener");
        throw paramBundle;
      }
    }
    paramBundle = n;
    if (paramBundle == null)
    {
      paramBundle = getTargetFragment();
      bool = paramBundle instanceof n.a;
      if (bool)
      {
        paramBundle = getTargetFragment();
        if (paramBundle != null)
        {
          paramBundle = (n.a)paramBundle;
          n = paramBundle;
          return;
        }
        paramBundle = new c/u;
        paramBundle.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.registration.views.fragments.SimSelectionBottomSheetFragment.OnFragmentInteractionListener");
        throw paramBundle;
      }
    }
    paramBundle = new java/lang/RuntimeException;
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("parent should implement ");
    String str = n.a.class.getSimpleName();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    paramBundle.<init>((String)localObject);
    throw ((Throwable)paramBundle);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i1 = R.layout.layout_sim_selection;
    paramLayoutInflater = paramLayoutInflater.inflate(i1, paramViewGroup, false);
    int i2 = R.id.cb_sim_selected_one;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k.a(paramViewGroup, "rootView.findViewById(R.id.cb_sim_selected_one)");
    paramViewGroup = (CheckBox)paramViewGroup;
    d = paramViewGroup;
    i2 = R.id.cb_sim_selected_two;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k.a(paramViewGroup, "rootView.findViewById(R.id.cb_sim_selected_two)");
    paramViewGroup = (CheckBox)paramViewGroup;
    e = paramViewGroup;
    i2 = R.id.sim_one_layout;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k.a(paramViewGroup, "rootView.findViewById(R.id.sim_one_layout)");
    paramViewGroup = (RelativeLayout)paramViewGroup;
    f = paramViewGroup;
    i2 = R.id.sim_two_layout;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k.a(paramViewGroup, "rootView.findViewById(R.id.sim_two_layout)");
    paramViewGroup = (RelativeLayout)paramViewGroup;
    g = paramViewGroup;
    i2 = R.id.tv_sim_operator_one;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k.a(paramViewGroup, "rootView.findViewById(R.id.tv_sim_operator_one)");
    paramViewGroup = (TextView)paramViewGroup;
    h = paramViewGroup;
    i2 = R.id.tv_sim_operator_two;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k.a(paramViewGroup, "rootView.findViewById(R.id.tv_sim_operator_two)");
    paramViewGroup = (TextView)paramViewGroup;
    i = paramViewGroup;
    i2 = R.id.tv_bank_name;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k.a(paramViewGroup, "rootView.findViewById(R.id.tv_bank_name)");
    paramViewGroup = (TextView)paramViewGroup;
    j = paramViewGroup;
    i2 = R.id.ico_sim_one;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k.a(paramViewGroup, "rootView.findViewById(R.id.ico_sim_one)");
    paramViewGroup = (ImageView)paramViewGroup;
    k = paramViewGroup;
    i2 = R.id.ico_sim_two;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k.a(paramViewGroup, "rootView.findViewById(R.id.ico_sim_two)");
    paramViewGroup = (ImageView)paramViewGroup;
    l = paramViewGroup;
    i2 = R.id.btn_continue_sim_selection;
    paramViewGroup = paramLayoutInflater.findViewById(i2);
    k.a(paramViewGroup, "rootView.findViewById(R.…n_continue_sim_selection)");
    paramViewGroup = (Button)paramViewGroup;
    m = paramViewGroup;
    return paramLayoutInflater;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    com.truecaller.truepay.app.ui.registration.d.r localr = a;
    if (localr == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localr.b();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = this;
    paramBundle = (com.truecaller.truepay.app.ui.base.views.a)this;
    paramView.a(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = (j)paramView.af_();
    if (paramBundle != null)
    {
      localObject = a;
      boolean bool1 = false;
      localObject = ((h)localObject).a(0);
      paramView = a;
      int i1 = 1;
      paramView = paramView.a(i1);
      String str1 = null;
      String str2;
      if (localObject != null) {
        str2 = d;
      } else {
        str2 = null;
      }
      if (paramView != null) {
        str1 = d;
      }
      boolean bool2;
      if (localObject != null)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        localObject = null;
      }
      if (paramView != null) {
        bool1 = true;
      }
      paramBundle.a(str2, str1, bool2, bool1);
    }
    paramView = f;
    if (paramView == null)
    {
      paramBundle = "simLayout1";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/n$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = g;
    if (paramView == null)
    {
      paramBundle = "simLayout2";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/n$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = m;
    if (paramView == null)
    {
      paramBundle = "continueButton";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/n$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */