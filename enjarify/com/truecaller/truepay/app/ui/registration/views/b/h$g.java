package com.truecaller.truepay.app.ui.registration.views.b;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import c.g.b.k;

public final class h$g
  extends ClickableSpan
{
  h$g(h paramh) {}
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "widget");
    h.b(a, "pre-registration");
  }
  
  public final void updateDrawState(TextPaint paramTextPaint)
  {
    k.b(paramTextPaint, "textPaint");
    int i = linkColor;
    paramTextPaint.setColor(i);
    paramTextPaint.setUnderlineText(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.h.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */