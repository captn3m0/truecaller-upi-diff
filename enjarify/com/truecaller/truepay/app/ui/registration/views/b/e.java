package com.truecaller.truepay.app.ui.registration.views.b;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.fragments.a;
import java.util.HashMap;

public final class e
  extends a
{
  private e.a a;
  private HashMap b;
  
  private View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final int a()
  {
    return R.layout.fragment_register_as_new_user_confirmation;
  }
  
  public final void b()
  {
    e.a locala = (e.a)getTargetFragment();
    a = locala;
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    a = null;
    HashMap localHashMap = b;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btnConfirmNewRegistration;
    paramView = (AppCompatButton)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/e$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.btnCancelNewRegistration;
    paramView = (TextView)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/e$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */