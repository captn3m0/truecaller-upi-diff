package com.truecaller.truepay.app.ui.registration.views.b;

import android.app.PendingIntent;
import com.truecaller.truepay.app.ui.registration.c.o;
import java.util.ArrayList;

public abstract interface b$a
  extends com.truecaller.truepay.app.ui.base.a.b
{
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, int paramInt, o paramo);
  
  public abstract void a(com.truecaller.truepay.app.ui.registration.c.b paramb, int paramInt);
  
  public abstract void a(com.truecaller.truepay.data.api.model.a parama);
  
  public abstract void a(com.truecaller.truepay.data.d.a parama, o paramo);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(ArrayList paramArrayList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean, int paramInt);
  
  public abstract void b(int paramInt);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract int f();
  
  public abstract int g();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */