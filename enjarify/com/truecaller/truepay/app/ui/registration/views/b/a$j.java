package com.truecaller.truepay.app.ui.registration.views.b;

import android.content.Context;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.truepay.R.string;
import java.util.ArrayList;

public final class a$j
  implements f.a
{
  a$j(a parama, ArrayList paramArrayList) {}
  
  public final void a()
  {
    Object localObject = a.a();
    ArrayList localArrayList = b;
    ((b.a)localObject).a(localArrayList);
    new String[1][0] = "Registration successful";
    localObject = a.a();
    boolean bool = true;
    ((b.a)localObject).b(bool);
    a.k(a);
    localObject = a.j(a);
    if (localObject != null)
    {
      ((a.b)localObject).a();
      return;
    }
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "account");
    Object localObject = a.a();
    ArrayList localArrayList = b;
    ((b.a)localObject).a(localArrayList);
    localObject = a.j(a);
    if (localObject != null)
    {
      ((a.b)localObject).a(parama);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    Context localContext = a.requireContext();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = a;
    int i = R.string.failed_to_verify_pin;
    localObject = ((a)localObject).getString(i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(paramString);
    paramString = (CharSequence)localStringBuilder.toString();
    Toast.makeText(localContext, paramString, 1).show();
  }
  
  public final void b()
  {
    a.a().a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */