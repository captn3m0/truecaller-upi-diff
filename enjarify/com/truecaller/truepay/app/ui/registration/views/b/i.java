package com.truecaller.truepay.app.ui.registration.views.b;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.e;
import android.support.v4.app.f;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;

public final class i
  extends e
{
  TextView a;
  TextView b;
  Button c;
  TextView d;
  j.a e;
  com.truecaller.truepay.data.api.model.a f;
  
  public static i a(com.truecaller.truepay.data.api.model.a parama, String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("connected_account", parama);
    localBundle.putString("errorText", paramString);
    parama = new com/truecaller/truepay/app/ui/registration/views/b/i;
    parama.<init>();
    parama.setArguments(localBundle);
    return parama;
  }
  
  private static void a(String paramString)
  {
    String str = "account_add_success";
    boolean bool = com.truecaller.truepay.app.ui.registration.a.d;
    if (bool) {
      str = "manage_account";
    }
    bool = com.truecaller.truepay.app.ui.registration.a.c;
    if (bool) {
      str = "retry_set_pin";
    }
    com.truecaller.truepay.app.ui.registration.a.c = true;
    Truepay.getInstance().getAnalyticLoggerHelper().a("app_payment_set_pin", "failure", str, paramString);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject = getActivity();
    boolean bool = localObject instanceof j.a;
    if (bool)
    {
      paramContext = (j.a)getActivity();
      e = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.getClass();
    localStringBuilder.append(paramContext);
    localStringBuilder.append("should implement the OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getActivity().getLayoutInflater();
    int i = R.layout.fragment_upi_pin_failure;
    paramBundle = paramBundle.inflate(i, null);
    i = R.id.tv_error_header;
    Object localObject1 = (TextView)paramBundle.findViewById(i);
    a = ((TextView)localObject1);
    i = R.id.tv_error_body;
    localObject1 = (TextView)paramBundle.findViewById(i);
    b = ((TextView)localObject1);
    i = R.id.btn_retry;
    localObject1 = (Button)paramBundle.findViewById(i);
    c = ((Button)localObject1);
    i = R.id.btn_do_it_later;
    localObject1 = (TextView)paramBundle.findViewById(i);
    d = ((TextView)localObject1);
    localObject1 = c;
    Object localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$i$DG9GD3alS2thJfxLQFQxfwQwx1c;
    ((-..Lambda.i.DG9GD3alS2thJfxLQFQxfwQwx1c)localObject2).<init>(this);
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = d;
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$i$KqQHTohftXi4sjI76CsO4_0mDMA;
    ((-..Lambda.i.KqQHTohftXi4sjI76CsO4_0mDMA)localObject2).<init>(this);
    ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = new android/app/AlertDialog$Builder;
    localObject2 = new android/view/ContextThemeWrapper;
    Object localObject3 = getActivity();
    int j = R.style.popup_theme;
    ((ContextThemeWrapper)localObject2).<init>((Context)localObject3, j);
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).create();
    localObject2 = ((AlertDialog)localObject1).getWindow();
    if (localObject2 != null)
    {
      localObject2 = ((AlertDialog)localObject1).getWindow();
      int k = 80;
      ((Window)localObject2).setGravity(k);
    }
    ((AlertDialog)localObject1).setCanceledOnTouchOutside(true);
    ((AlertDialog)localObject1).setView(paramBundle);
    paramBundle = getArguments();
    localObject2 = b;
    localObject3 = paramBundle.getString("errorText");
    ((TextView)localObject2).setText((CharSequence)localObject3);
    paramBundle = (com.truecaller.truepay.data.api.model.a)paramBundle.getSerializable("connected_account");
    f = paramBundle;
    return (Dialog)localObject1;
  }
  
  public final void onDetach()
  {
    super.onDetach();
    e = null;
  }
  
  public final void onStart()
  {
    super.onStart();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      int i = R.color.transparent;
      localWindow.setBackgroundDrawableResource(i);
      i = -1;
      int j = -2;
      localWindow.setLayout(i, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */