package com.truecaller.truepay.app.ui.registration.views.activities;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import c.g.b.k;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.data.b.a;

final class AccountConnectionActivity$a
  implements DialogInterface.OnClickListener
{
  public static final a a;
  
  static
  {
    a locala = new com/truecaller/truepay/app/ui/registration/views/activities/AccountConnectionActivity$a;
    locala.<init>();
    a = locala;
  }
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    paramDialogInterface = Truepay.getInstance();
    k.a(paramDialogInterface, "Truepay.getInstance()");
    paramDialogInterface.getAnalyticLoggerHelper().b(null, "exit_registration", "continue");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.activities.AccountConnectionActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */