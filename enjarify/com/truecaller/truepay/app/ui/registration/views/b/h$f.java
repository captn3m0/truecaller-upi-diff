package com.truecaller.truepay.app.ui.registration.views.b;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import c.g.b.k;

public final class h$f
  extends ClickableSpan
{
  h$f(h paramh) {}
  
  public final void onClick(View paramView)
  {
    String str = "widget";
    k.b(paramView, str);
    paramView = a;
    boolean bool = h.i(paramView);
    if (bool)
    {
      paramView = a;
      str = "pre-registration";
      h.a(paramView, str);
    }
  }
  
  public final void updateDrawState(TextPaint paramTextPaint)
  {
    k.b(paramTextPaint, "textPaint");
    h.a(a, paramTextPaint);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.h.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */