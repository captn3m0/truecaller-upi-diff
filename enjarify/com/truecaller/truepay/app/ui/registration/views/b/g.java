package com.truecaller.truepay.app.ui.registration.views.b;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.e;
import android.support.v4.app.f;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.app.ui.registration.views.a.c;
import com.truecaller.truepay.data.d.a;
import java.util.ArrayList;
import java.util.List;

public class g
  extends e
{
  RecyclerView a;
  Button b;
  Context c;
  g.a d;
  c e;
  ArrayList f;
  
  public static g a(ArrayList paramArrayList, a parama)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("accounts", paramArrayList);
    localBundle.putSerializable("selected_bank", parama);
    paramArrayList = new com/truecaller/truepay/app/ui/registration/views/b/g;
    paramArrayList.<init>();
    paramArrayList.setArguments(localBundle);
    return paramArrayList;
  }
  
  public final void a(g.a parama)
  {
    d = parama;
  }
  
  public void dismiss()
  {
    super.dismiss();
  }
  
  public void onAttach(Context paramContext)
  {
    c = paramContext;
    super.onAttach(paramContext);
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getActivity().getLayoutInflater();
    int i = R.layout.fragment_multi_account_chooser;
    paramBundle = paramBundle.inflate(i, null);
    i = R.id.rv_acc_list;
    Object localObject1 = (RecyclerView)paramBundle.findViewById(i);
    a = ((RecyclerView)localObject1);
    i = R.id.btn_retry;
    localObject1 = (Button)paramBundle.findViewById(i);
    b = ((Button)localObject1);
    localObject1 = b;
    Object localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$g$em4U_tPC7ERzMbkg1eA-Khp7V1A;
    ((-..Lambda.g.em4U_tPC7ERzMbkg1eA-Khp7V1A)localObject2).<init>(this);
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = new android/app/AlertDialog$Builder;
    localObject2 = new android/view/ContextThemeWrapper;
    f localf = getActivity();
    int j = R.style.popup_theme;
    ((ContextThemeWrapper)localObject2).<init>(localf, j);
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).create();
    localObject2 = ((AlertDialog)localObject1).getWindow();
    if (localObject2 != null)
    {
      localObject2 = ((AlertDialog)localObject1).getWindow();
      int k = 80;
      ((Window)localObject2).setGravity(k);
    }
    ((AlertDialog)localObject1).setCancelable(false);
    ((AlertDialog)localObject1).setCanceledOnTouchOutside(false);
    ((AlertDialog)localObject1).setView(paramBundle);
    return (Dialog)localObject1;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    a.setHasFixedSize(true);
    Object localObject1 = a;
    Object localObject2 = new android/support/v7/widget/LinearLayoutManager;
    Object localObject3 = getContext();
    ((LinearLayoutManager)localObject2).<init>((Context)localObject3);
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject2);
    localObject1 = (ArrayList)getArguments().getSerializable("accounts");
    f = ((ArrayList)localObject1);
    localObject1 = (a)getArguments().getSerializable("selected_bank");
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/a/c;
    localObject3 = f;
    ((c)localObject2).<init>((List)localObject3, (a)localObject1);
    e = ((c)localObject2);
    localObject1 = a;
    localObject2 = e;
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public void onDetach()
  {
    c = null;
    super.onDetach();
    d = null;
  }
  
  public void onStart()
  {
    super.onStart();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      localWindow.setBackgroundDrawableResource(17170445);
      int i = -1;
      int j = -2;
      localWindow.setLayout(i, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */