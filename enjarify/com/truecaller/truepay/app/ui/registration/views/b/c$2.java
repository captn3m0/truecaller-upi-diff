package com.truecaller.truepay.app.ui.registration.views.b;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

final class c$2
  implements TextWatcher
{
  c$2(c paramc) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    paramEditable = a.c.getText().toString();
    com.truecaller.truepay.app.ui.registration.views.a.a locala = a.g;
    paramEditable = paramEditable.toLowerCase();
    Object localObject1 = b;
    ((List)localObject1).clear();
    int i = paramEditable.length();
    if (i != 0)
    {
      localObject1 = a.iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        com.truecaller.truepay.data.d.a locala1 = (com.truecaller.truepay.data.d.a)((Iterator)localObject1).next();
        int j = paramEditable.length();
        if (j != 0)
        {
          Object localObject2 = b;
          Locale localLocale = Locale.getDefault();
          localObject2 = ((String)localObject2).toLowerCase(localLocale);
          boolean bool2 = ((String)localObject2).contains(paramEditable);
          if (bool2)
          {
            localObject2 = b;
            ((List)localObject2).add(locala1);
          }
        }
      }
    }
    locala.notifyDataSetChanged();
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.c.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */