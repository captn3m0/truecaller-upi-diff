package com.truecaller.truepay.app.ui.registration.views.b;

import com.truecaller.truepay.app.ui.registration.c.b;
import com.truecaller.truepay.app.utils.r;
import java.util.ArrayList;

public abstract interface b$b
  extends com.truecaller.truepay.app.ui.base.views.a
{
  public abstract void a(int paramInt);
  
  public abstract void a(com.truecaller.truepay.app.ui.registration.c.a parama, r paramr);
  
  public abstract void a(b paramb);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, r paramr);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(ArrayList paramArrayList, boolean paramBoolean, r paramr);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b(String paramString, r paramr);
  
  public abstract void b(String paramString1, String paramString2);
  
  public abstract void c(String paramString1, String paramString2);
  
  public abstract void f();
  
  public abstract void g();
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract void m();
  
  public abstract void n();
  
  public abstract void o();
  
  public abstract void p();
  
  public abstract void q();
  
  public abstract void r();
  
  public abstract void s();
  
  public abstract void t();
  
  public abstract void u();
  
  public abstract void v();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */