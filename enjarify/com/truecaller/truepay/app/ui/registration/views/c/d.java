package com.truecaller.truepay.app.ui.registration.views.c;

import android.graphics.drawable.Drawable;
import com.truecaller.truepay.app.ui.registration.c.o;

public abstract interface d
  extends com.truecaller.truepay.app.ui.base.views.a
{
  public abstract void a(o paramo);
  
  public abstract void a(com.truecaller.truepay.data.d.a parama);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, int paramInt, boolean paramBoolean);
  
  public abstract void a(String paramString, Drawable paramDrawable);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, int paramInt, boolean paramBoolean);
  
  public abstract void a(Throwable paramThrowable);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(String[] paramArrayOfString);
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */