package com.truecaller.truepay.app.ui.registration.views.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.truecaller.truepay.R.id;

final class b$a
  extends RecyclerView.ViewHolder
{
  TextView a;
  ImageView b;
  RadioButton c;
  
  b$a(b paramb, View paramView)
  {
    super(paramView);
    int i = R.id.bank_name_textView;
    paramb = (TextView)paramView.findViewById(i);
    a = paramb;
    i = R.id.bank_imageView;
    paramb = (ImageView)paramView.findViewById(i);
    b = paramb;
    i = R.id.cb_selector;
    paramb = (RadioButton)paramView.findViewById(i);
    c = paramb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */