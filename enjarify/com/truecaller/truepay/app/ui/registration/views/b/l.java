package com.truecaller.truepay.app.ui.registration.views.b;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.e;
import android.support.v4.app.f;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Window;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;

public final class l
  extends e
{
  j.a a;
  
  public static l a()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    l locall = new com/truecaller/truepay/app/ui/registration/views/b/l;
    locall.<init>();
    locall.setArguments(localBundle);
    return locall;
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject = getActivity();
    boolean bool = localObject instanceof j.a;
    if (bool)
    {
      paramContext = (j.a)getActivity();
      a = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.getClass();
    localStringBuilder.append(paramContext);
    localStringBuilder.append("should implement the OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getActivity().getLayoutInflater();
    int i = R.layout.fragment_set_upi_pin_success;
    paramBundle = paramBundle.inflate(i, null);
    Object localObject1 = new android/app/AlertDialog$Builder;
    Object localObject2 = new android/view/ContextThemeWrapper;
    f localf = getActivity();
    int j = R.style.popup_theme;
    ((ContextThemeWrapper)localObject2).<init>(localf, j);
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).create();
    localObject2 = ((AlertDialog)localObject1).getWindow();
    if (localObject2 != null)
    {
      localObject2 = ((AlertDialog)localObject1).getWindow();
      int k = 80;
      ((Window)localObject2).setGravity(k);
    }
    ((AlertDialog)localObject1).setView(paramBundle);
    paramBundle = new android/os/Handler;
    paramBundle.<init>();
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/l$1;
    ((l.1)localObject2).<init>(this);
    paramBundle.postDelayed((Runnable)localObject2, 2000L);
    return (Dialog)localObject1;
  }
  
  public final void onDetach()
  {
    super.onDetach();
    a = null;
  }
  
  public final void onStart()
  {
    super.onStart();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      localWindow.setBackgroundDrawableResource(17170445);
      int i = -1;
      int j = -2;
      localWindow.setLayout(i, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */