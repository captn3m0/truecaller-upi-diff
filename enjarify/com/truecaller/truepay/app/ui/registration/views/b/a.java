package com.truecaller.truepay.app.ui.registration.views.b;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.n.m;
import c.u;
import com.truecaller.common.ui.PausingLottieAnimationView;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.d;
import com.truecaller.multisim.h;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.bankList.BankListActivity;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.extensions.t;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class a
  extends Fragment
  implements com.truecaller.truepay.app.ui.registration.e.a, b.b
{
  public static final a.a e;
  public b.a a;
  public e b;
  public h c;
  public com.truecaller.truepay.app.ui.registration.views.c.k d;
  private a.b f;
  private CountDownTimer g;
  private int h = -1;
  private com.truecaller.truepay.data.d.a i;
  private boolean j;
  private boolean k;
  private boolean l;
  private int m;
  private boolean n;
  private boolean o;
  private final Handler p;
  private com.truecaller.truepay.app.ui.registration.e.b q;
  private boolean r;
  private com.truecaller.truepay.app.ui.registration.c.o s;
  private final a.l t;
  private HashMap u;
  
  static
  {
    a.a locala = new com/truecaller/truepay/app/ui/registration/views/b/a$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public a()
  {
    Object localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    p = ((Handler)localObject);
    localObject = new com/truecaller/truepay/app/ui/registration/views/b/a$l;
    ((a.l)localObject).<init>(this);
    t = ((a.l)localObject);
  }
  
  private final String A()
  {
    boolean bool = o;
    if (bool) {
      return "manage_account";
    }
    bool = k;
    if (bool) {
      return "retry_bank_selection";
    }
    return "registration";
  }
  
  private final String B()
  {
    boolean bool = l;
    if (bool) {
      return "change_sim";
    }
    bool = j;
    if (bool) {
      return "retry_device_registration";
    }
    bool = o;
    if (bool) {
      return "manage_account";
    }
    return "sim_selection";
  }
  
  private final void a(String paramString1, String paramString2, int paramInt, String paramString3)
  {
    String str = A();
    Object localObject = Truepay.getInstance();
    c.g.b.k.a(localObject, "Truepay.getInstance()");
    localObject = ((Truepay)localObject).getAnalyticLoggerHelper();
    double d1 = paramInt;
    ((com.truecaller.truepay.data.b.a)localObject).a("app_payment_select_bank", paramString2, str, "continue", paramString3, d1, paramString1);
  }
  
  private final void a(String paramString1, String paramString2, String paramString3)
  {
    String str = A();
    Truepay localTruepay = Truepay.getInstance();
    c.g.b.k.a(localTruepay, "Truepay.getInstance()");
    localTruepay.getAnalyticLoggerHelper().a("app_payment_select_bank", paramString1, str, paramString2, paramString3, 0.0D, "");
  }
  
  private final void a(String paramString1, String paramString2, boolean paramBoolean)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "multiSimManager";
      c.g.b.k.a((String)localObject2);
    }
    boolean bool = ((h)localObject1).j();
    if (bool) {
      localObject1 = "dual_sim";
    } else {
      localObject1 = "single_sim";
    }
    Object localObject3 = localObject1;
    String str1 = B();
    localObject1 = Truepay.getInstance();
    c.g.b.k.a(localObject1, "Truepay.getInstance()");
    Object localObject2 = ((Truepay)localObject1).getAnalyticLoggerHelper();
    String str2 = "app_payment_device_registration";
    ((com.truecaller.truepay.data.b.a)localObject2).a(str2, paramString1, str1, paramString2, (String)localObject3, false, paramBoolean);
    paramString2 = new java/util/HashMap;
    paramString2.<init>();
    Object localObject4 = paramString2;
    localObject4 = (Map)paramString2;
    ((Map)localObject4).put("PayDeviceRegistrationStatus", paramString1);
    paramString1 = "PayDeviceRegistrationDate";
    localObject1 = Truepay.getInstance();
    if (localObject1 != null)
    {
      localObject1 = ((Truepay)localObject1).getAnalyticLoggerHelper();
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.data.b.a)localObject1).b();
        if (localObject1 != null) {
          break label162;
        }
      }
    }
    localObject1 = "unknown";
    label162:
    ((Map)localObject4).put(paramString1, localObject1);
    paramString1 = Truepay.getInstance();
    if (paramString1 != null)
    {
      paramString1 = paramString1.getAnalyticLoggerHelper();
      if (paramString1 != null)
      {
        paramString1.b(paramString2);
        return;
      }
    }
  }
  
  private final void a(String paramString, boolean paramBoolean)
  {
    int i1 = R.id.animationViewNewRegFlow;
    ((PausingLottieAnimationView)b(i1)).d();
    i1 = R.id.animationViewNewRegFlow;
    ((PausingLottieAnimationView)b(i1)).clearAnimation();
    i1 = R.id.animationViewNewRegFlow;
    ((PausingLottieAnimationView)b(i1)).setAnimation(paramString);
    int i2 = R.id.animationViewNewRegFlow;
    ((PausingLottieAnimationView)b(i2)).a(paramBoolean);
    i2 = R.id.animationViewNewRegFlow;
    ((PausingLottieAnimationView)b(i2)).a();
  }
  
  private final void a(boolean paramBoolean, String paramString1, String paramString2)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((b.a)localObject1).b(6);
    localObject1 = null;
    String str = null;
    int i1 = 2;
    boolean bool = true;
    if (paramBoolean)
    {
      paramBoolean = o;
      if (!paramBoolean)
      {
        paramBoolean = R.id.btnPrimaryAccountFetchFailed;
        localObject2 = (AppCompatButton)b(paramBoolean);
        c.g.b.k.a(localObject2, "btnPrimaryAccountFetchFailed");
        i2 = R.string.choose_another_bank;
        localObject3 = (CharSequence)getString(i2);
        ((AppCompatButton)localObject2).setText((CharSequence)localObject3);
        localObject2 = getArguments();
        if (localObject2 != null)
        {
          localObject3 = "selected_sim";
          paramBoolean = ((Bundle)localObject2).getInt((String)localObject3);
          localObject2 = Integer.valueOf(paramBoolean);
        }
        else
        {
          paramBoolean = false;
          localObject2 = null;
        }
        if (localObject2 != null)
        {
          i2 = ((Integer)localObject2).intValue();
          if (i2 == 0)
          {
            paramBoolean = true;
            break label187;
          }
        }
        if (localObject2 != null)
        {
          paramBoolean = ((Integer)localObject2).intValue();
          if (paramBoolean == bool)
          {
            paramBoolean = true;
            break label187;
          }
        }
        paramBoolean = true;
        label187:
        i2 = R.id.btnSecondaryAccountFetchFailed;
        localObject3 = (TextView)b(i2);
        localObject4 = "btnSecondaryAccountFetchFailed";
        c.g.b.k.a(localObject3, (String)localObject4);
        int i3 = R.string.use_different_sim;
        Object[] arrayOfObject1 = new Object[bool];
        localObject2 = Integer.valueOf(paramBoolean);
        arrayOfObject1[0] = localObject2;
        localObject2 = (CharSequence)getString(i3, arrayOfObject1);
        ((TextView)localObject3).setText((CharSequence)localObject2);
        break label351;
      }
    }
    paramBoolean = R.id.btnPrimaryAccountFetchFailed;
    Object localObject2 = (AppCompatButton)b(paramBoolean);
    c.g.b.k.a(localObject2, "btnPrimaryAccountFetchFailed");
    int i2 = R.string.retry;
    Object localObject3 = (CharSequence)getString(i2);
    ((AppCompatButton)localObject2).setText((CharSequence)localObject3);
    paramBoolean = R.id.btnSecondaryAccountFetchFailed;
    localObject2 = (TextView)b(paramBoolean);
    c.g.b.k.a(localObject2, "btnSecondaryAccountFetchFailed");
    i2 = R.string.choose_another_bank;
    localObject3 = (CharSequence)getString(i2);
    ((TextView)localObject2).setText((CharSequence)localObject3);
    label351:
    paramBoolean = R.id.tvStatusAccountFetchFailed;
    localObject2 = (TextView)b(paramBoolean);
    localObject3 = "tvStatusAccountFetchFailed";
    c.g.b.k.a(localObject2, (String)localObject3);
    i2 = R.string.account_link_failed_status;
    Object[] arrayOfObject2 = new Object[i1];
    Object localObject4 = i;
    if (localObject4 != null) {
      localObject1 = ((com.truecaller.truepay.data.d.a)localObject4).b();
    }
    arrayOfObject2[0] = localObject1;
    arrayOfObject2[bool] = paramString2;
    paramString2 = (CharSequence)getString(i2, arrayOfObject2);
    ((TextView)localObject2).setText(paramString2);
    paramBoolean = R.id.tvAccountLinkFailureMessage;
    localObject2 = (TextView)b(paramBoolean);
    c.g.b.k.a(localObject2, "tvAccountLinkFailureMessage");
    paramString1 = (CharSequence)paramString1;
    ((TextView)localObject2).setText(paramString1);
  }
  
  private final void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject1 = Truepay.getInstance();
    c.g.b.k.a(localObject1, "Truepay.getInstance()");
    localObject1 = ((Truepay)localObject1).getAnalyticLoggerHelper();
    Object localObject2 = "app_payment_registration_complete";
    Object localObject3 = "registration";
    ((com.truecaller.truepay.data.b.a)localObject1).a((String)localObject2, (String)localObject3, paramBoolean1, paramBoolean2);
    Object localObject4 = getArguments();
    paramBoolean2 = false;
    String str = null;
    if (localObject4 != null)
    {
      localObject1 = "reg_source";
      localObject2 = "";
      localObject4 = ((Bundle)localObject4).getString((String)localObject1, (String)localObject2);
    }
    else
    {
      paramBoolean1 = false;
      localObject4 = null;
    }
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    if (localObject4 != null)
    {
      localObject2 = localObject1;
      localObject2 = (Map)localObject1;
      localObject3 = "PayRegistrationCompleteSource";
      ((Map)localObject2).put(localObject3, localObject4);
    }
    localObject4 = localObject1;
    localObject4 = (Map)localObject1;
    localObject2 = "PayRegistrationCompleteDate";
    localObject3 = Truepay.getInstance();
    if (localObject3 != null)
    {
      localObject3 = ((Truepay)localObject3).getAnalyticLoggerHelper();
      if (localObject3 != null) {
        str = ((com.truecaller.truepay.data.b.a)localObject3).b();
      }
    }
    if (str == null) {
      c.g.b.k.a();
    }
    ((Map)localObject4).put(localObject2, str);
    localObject4 = Truepay.getInstance();
    if (localObject4 != null)
    {
      localObject4 = ((Truepay)localObject4).getAnalyticLoggerHelper();
      if (localObject4 != null)
      {
        ((com.truecaller.truepay.data.b.a)localObject4).b((HashMap)localObject1);
        return;
      }
    }
  }
  
  private final void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8, boolean paramBoolean9, boolean paramBoolean10)
  {
    int i1 = R.id.viewRegStatus;
    Object localObject = b(i1);
    c.g.b.k.a(localObject, "viewRegStatus");
    int i2 = 0;
    if (paramBoolean1)
    {
      paramBoolean1 = false;
      localView = null;
    }
    else
    {
      paramBoolean1 = true;
    }
    ((View)localObject).setVisibility(paramBoolean1);
    paramBoolean1 = R.id.viewRegTimer;
    View localView = b(paramBoolean1);
    localObject = "viewRegTimer";
    c.g.b.k.a(localView, (String)localObject);
    if (paramBoolean2)
    {
      paramBoolean2 = false;
      str = null;
    }
    else
    {
      paramBoolean2 = true;
    }
    localView.setVisibility(paramBoolean2);
    paramBoolean1 = R.id.viewRegDividerReg;
    localView = b(paramBoolean1);
    String str = "viewRegDividerReg";
    c.g.b.k.a(localView, str);
    if (paramBoolean3)
    {
      paramBoolean2 = false;
      str = null;
    }
    else
    {
      paramBoolean2 = true;
    }
    localView.setVisibility(paramBoolean2);
    paramBoolean1 = R.id.viewRegSecondaryStatus;
    localView = b(paramBoolean1);
    str = "viewRegSecondaryStatus";
    c.g.b.k.a(localView, str);
    if (paramBoolean4)
    {
      paramBoolean2 = false;
      str = null;
    }
    else
    {
      paramBoolean2 = true;
    }
    localView.setVisibility(paramBoolean2);
    paramBoolean1 = R.id.viewRegAccountConnection;
    localView = b(paramBoolean1);
    str = "viewRegAccountConnection";
    c.g.b.k.a(localView, str);
    if (paramBoolean5)
    {
      paramBoolean2 = false;
      str = null;
    }
    else
    {
      paramBoolean2 = true;
    }
    localView.setVisibility(paramBoolean2);
    paramBoolean1 = R.id.viewRegAccountConnectionSuccess;
    localView = b(paramBoolean1);
    str = "viewRegAccountConnectionSuccess";
    c.g.b.k.a(localView, str);
    if (paramBoolean6)
    {
      paramBoolean2 = false;
      str = null;
    }
    else
    {
      paramBoolean2 = true;
    }
    localView.setVisibility(paramBoolean2);
    paramBoolean1 = R.id.viewRegSmsSendFailure;
    localView = b(paramBoolean1);
    str = "viewRegSmsSendFailure";
    c.g.b.k.a(localView, str);
    if (paramBoolean7)
    {
      paramBoolean2 = false;
      str = null;
    }
    else
    {
      paramBoolean2 = true;
    }
    localView.setVisibility(paramBoolean2);
    paramBoolean1 = R.id.viewRegSmsVerificationFailure;
    localView = b(paramBoolean1);
    str = "viewRegSmsVerificationFailure";
    c.g.b.k.a(localView, str);
    if (paramBoolean8)
    {
      paramBoolean2 = false;
      str = null;
    }
    else
    {
      paramBoolean2 = true;
    }
    localView.setVisibility(paramBoolean2);
    paramBoolean1 = R.id.viewConnectAccountFailure;
    localView = b(paramBoolean1);
    str = "viewConnectAccountFailure";
    c.g.b.k.a(localView, str);
    if (paramBoolean9)
    {
      paramBoolean2 = false;
      str = null;
    }
    else
    {
      paramBoolean2 = true;
    }
    localView.setVisibility(paramBoolean2);
    paramBoolean1 = R.id.viewWelcomeBackUser;
    localView = b(paramBoolean1);
    str = "viewWelcomeBackUser";
    c.g.b.k.a(localView, str);
    if (!paramBoolean10) {
      i2 = 8;
    }
    localView.setVisibility(i2);
  }
  
  private final void b(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = (Context)getActivity();
    localIntent.<init>(localContext, BankListActivity.class);
    localIntent.putExtra("bank_selection_context", paramString);
    startActivityForResult(localIntent, 5001);
  }
  
  private final void w()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null) {
      localf.setResult(0);
    }
    localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  private final void x()
  {
    t();
    int i1 = R.id.ivStatusIconReg;
    Object localObject = (ImageView)b(i1);
    int i2;
    if (localObject != null)
    {
      i2 = R.drawable.ic_cross;
      ((ImageView)localObject).setImageResource(i2);
    }
    i1 = R.id.tvStatusHeader;
    localObject = (TextView)b(i1);
    CharSequence localCharSequence;
    if (localObject != null)
    {
      i2 = R.string.unable_to_verify_your_mobile_number;
      localCharSequence = (CharSequence)getString(i2);
      ((TextView)localObject).setText(localCharSequence);
    }
    i1 = R.id.tvTimerVerification;
    localObject = (TextView)b(i1);
    if (localObject != null)
    {
      i2 = R.string.mobile_verification_timer_failure_value;
      localCharSequence = (CharSequence)getString(i2);
      ((TextView)localObject).setText(localCharSequence);
    }
    i1 = R.id.viewRegSecondaryStatus;
    localObject = b(i1);
    c.g.b.k.a(localObject, "viewRegSecondaryStatus");
    ((View)localObject).setVisibility(8);
  }
  
  private final void y()
  {
    int i1 = R.id.ivStatusIconReg;
    Object localObject1 = (ImageView)b(i1);
    if (localObject1 != null)
    {
      i2 = R.drawable.ic_tick;
      ((ImageView)localObject1).setImageResource(i2);
    }
    i1 = R.id.tvStatusHeader;
    localObject1 = (TextView)b(i1);
    if (localObject1 != null)
    {
      localObject2 = s;
      if (localObject2 != null)
      {
        localObject2 = ((com.truecaller.truepay.app.ui.registration.c.o)localObject2).c();
        if (localObject2 != null)
        {
          String str = "@";
          localObject2 = m.a((String)localObject2, str);
          if (localObject2 != null)
          {
            str = "-";
            localObject2 = m.a((String)localObject2, str);
            break label107;
          }
        }
      }
      i2 = 0;
      localObject2 = null;
      label107:
      localObject2 = (CharSequence)localObject2;
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    i1 = R.id.tvStatusSubHeader;
    localObject1 = (TextView)b(i1);
    if (localObject1 != null)
    {
      i2 = R.string.mobile_number_verified;
      localObject2 = (CharSequence)getString(i2);
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    i1 = R.id.ivStatusIconSecondary;
    localObject1 = (ImageView)b(i1);
    if (localObject1 != null)
    {
      i2 = R.drawable.ic_second_step_activated;
      ((ImageView)localObject1).setImageResource(i2);
    }
    i1 = R.id.tvStatusHeaderSecondary;
    localObject1 = (TextView)b(i1);
    if (localObject1 != null)
    {
      i2 = R.string.awaiting_bank_response;
      localObject2 = (CharSequence)getString(i2);
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    i1 = R.id.tvStatusHeaderSecondary;
    localObject1 = (TextView)b(i1);
    if (localObject1 != null)
    {
      localObject2 = getResources();
      int i3 = R.color.black;
      i2 = ((Resources)localObject2).getColor(i3);
      ((TextView)localObject1).setTextColor(i2);
    }
    i1 = R.id.ivStatusSecondaryAccountConn;
    localObject1 = (ImageView)b(i1);
    int i2 = 0;
    Object localObject2 = null;
    if (localObject1 != null) {
      ((ImageView)localObject1).setVisibility(0);
    }
    i1 = R.id.pbAccountConnectionProgress;
    localObject1 = (ProgressBar)b(i1);
    if (localObject1 != null) {
      ((ProgressBar)localObject1).setVisibility(0);
    }
    i1 = R.id.ivBankIconAccountConn;
    localObject1 = (ImageView)b(i1);
    if (localObject1 != null)
    {
      ((ImageView)localObject1).setVisibility(0);
      return;
    }
  }
  
  private final void z()
  {
    try
    {
      localObject1 = getContext();
      if (localObject1 != null)
      {
        Object localObject2 = t;
        localObject2 = (BroadcastReceiver)localObject2;
        ((Context)localObject1).unregisterReceiver((BroadcastReceiver)localObject2);
      }
      return;
    }
    catch (Exception localException)
    {
      Object localObject1 = new java/lang/AssertionError;
      ((AssertionError)localObject1).<init>("Error unregistering sms receiver");
      d.a((Throwable)localObject1);
    }
  }
  
  public final b.a a()
  {
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locala;
  }
  
  public final void a(int paramInt)
  {
    boolean bool1 = true;
    int i1 = 8;
    switch (paramInt)
    {
    default: 
      localObject1 = new java/lang/AssertionError;
      ((AssertionError)localObject1).<init>("UNKNOWN_STATE");
      d.a((Throwable)localObject1);
      return;
    case 7: 
      localObject1 = this;
      a(true, false, true, false, false, false, false, false, false, true);
      return;
    case 6: 
      localObject1 = this;
      a(false, false, false, false, false, false, false, false, true, false);
      a("animations/lottie_bank_response failed.json", false);
      return;
    case 5: 
      localObject1 = this;
      a(false, false, false, false, false, true, false, false, false, false);
      a("animations/lottie_bank_response_success.json", false);
      return;
    case 4: 
      localObject1 = this;
      a(true, false, true, true, true, false, false, false, false, false);
      a("animations/awaiting_bank_response.json", bool1);
      i2 = R.id.tvStatusSubHeader;
      localObject1 = (TextView)b(i2);
      c.g.b.k.a(localObject1, "tvStatusSubHeader");
      ((TextView)localObject1).setVisibility(0);
      return;
    case 3: 
      localObject1 = this;
      a(true, true, true, false, false, false, true, false, false, false);
      a("animations/lottie_sms_verification_failed.json", bool1);
      i2 = R.id.tvStatusSubHeader;
      localObject1 = (TextView)b(i2);
      c.g.b.k.a(localObject1, "tvStatusSubHeader");
      ((TextView)localObject1).setVisibility(i1);
      x();
      return;
    case 2: 
      localObject1 = this;
      a(true, true, true, false, false, false, false, true, false, false);
      a("animations/lottie_sms_verification_failed.json", bool1);
      i2 = R.id.tvStatusSubHeader;
      localObject1 = (TextView)b(i2);
      c.g.b.k.a(localObject1, "tvStatusSubHeader");
      ((TextView)localObject1).setVisibility(i1);
      x();
      return;
    }
    boolean bool3 = true;
    boolean bool4 = true;
    boolean bool5 = true;
    boolean bool6 = true;
    Object localObject1 = this;
    a(bool3, bool4, bool5, bool6, false, false, false, false, false, false);
    a("animations/lottie_new_sms_verification.json", false);
    int i2 = R.id.tvStatusSubHeader;
    localObject1 = (TextView)b(i2);
    Object localObject2 = "tvStatusSubHeader";
    c.g.b.k.a(localObject1, (String)localObject2);
    ((TextView)localObject1).setVisibility(i1);
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "featuresRegistry";
      c.g.b.k.a((String)localObject2);
    }
    localObject1 = ((e)localObject1).D();
    boolean bool2 = ((com.truecaller.featuretoggles.b)localObject1).a();
    int i4;
    if (bool2)
    {
      i3 = R.id.tvTimerVerification;
      localObject1 = (TextView)b(i3);
      if (localObject1 != null)
      {
        i4 = R.string.mobile_verification_timer_45_seconds;
        localObject2 = (CharSequence)getString(i4);
        ((TextView)localObject1).setText((CharSequence)localObject2);
      }
    }
    else
    {
      i3 = R.id.tvTimerVerification;
      localObject1 = (TextView)b(i3);
      if (localObject1 != null)
      {
        i4 = R.string.mobile_verification_timer_one_minute;
        localObject2 = (CharSequence)getString(i4);
        ((TextView)localObject1).setText((CharSequence)localObject2);
      }
    }
    int i3 = R.id.ivStatusIconReg;
    localObject1 = (ImageView)b(i3);
    if (localObject1 != null)
    {
      i4 = R.drawable.ic_first_step;
      ((ImageView)localObject1).setImageResource(i4);
    }
    i3 = R.id.tvStatusHeader;
    localObject1 = (TextView)b(i3);
    if (localObject1 != null)
    {
      i4 = R.string.sending_sms;
      localObject2 = (CharSequence)getString(i4);
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    i3 = R.id.ivStatusIconSecondary;
    localObject1 = (ImageView)b(i3);
    if (localObject1 != null)
    {
      i4 = R.drawable.ic_second_step_deactivated;
      ((ImageView)localObject1).setImageResource(i4);
    }
    i3 = R.id.tvStatusHeaderSecondary;
    localObject1 = (TextView)b(i3);
    if (localObject1 != null)
    {
      i4 = R.string.connecting_bank_account;
      localObject2 = (CharSequence)getString(i4);
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.c.a parama, r paramr)
  {
    c.g.b.k.b(paramr, "imageLoader");
    new String[1][0] = "New registration successful";
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    ((b.a)localObject1).b(5);
    int i1 = R.id.ivBankLogoBankAccountConnected;
    localObject1 = (ImageView)b(i1);
    Object localObject2 = null;
    if (localObject1 != null)
    {
      if (parama != null)
      {
        localObject3 = parama.a();
        if (localObject3 != null)
        {
          localObject3 = ((com.truecaller.truepay.data.api.model.a)localObject3).l();
          break label94;
        }
      }
      bool1 = false;
      Object localObject3 = null;
      label94:
      if (localObject3 == null) {
        c.g.b.k.a();
      }
      localObject3 = ((com.truecaller.truepay.data.d.a)localObject3).d();
      paramr = paramr.b((String)localObject3);
      ((ImageView)localObject1).setImageDrawable(paramr);
    }
    int i2 = R.id.tvAccountNumberBankAccountConnected;
    paramr = (TextView)b(i2);
    i1 = 0;
    localObject1 = null;
    boolean bool1 = true;
    int i3;
    int i4;
    Object[] arrayOfObject;
    Object localObject4;
    label193:
    label242:
    Object localObject5;
    if (paramr != null)
    {
      i3 = R.string.bank_name_acc_number;
      i4 = 2;
      arrayOfObject = new Object[i4];
      if (parama != null)
      {
        localObject4 = parama.a();
        if (localObject4 != null)
        {
          localObject4 = ((com.truecaller.truepay.data.api.model.a)localObject4).l();
          break label193;
        }
      }
      localObject4 = null;
      if (localObject4 == null) {
        c.g.b.k.a();
      }
      localObject4 = ((com.truecaller.truepay.data.d.a)localObject4).b();
      arrayOfObject[0] = localObject4;
      if (parama != null)
      {
        localObject4 = parama.a();
        if (localObject4 != null)
        {
          localObject4 = ((com.truecaller.truepay.data.api.model.a)localObject4).d();
          break label242;
        }
      }
      localObject4 = null;
      arrayOfObject[bool1] = localObject4;
      localObject5 = (CharSequence)getString(i3, arrayOfObject);
      paramr.setText((CharSequence)localObject5);
    }
    i2 = R.id.tvBankIfscBankAccountConnected;
    paramr = (TextView)b(i2);
    if (paramr != null)
    {
      i3 = R.string.ifsc_code;
      arrayOfObject = new Object[bool1];
      if (parama != null)
      {
        localObject4 = parama.a();
        if (localObject4 != null)
        {
          localObject4 = ((com.truecaller.truepay.data.api.model.a)localObject4).e();
          break label327;
        }
      }
      localObject4 = null;
      label327:
      arrayOfObject[0] = localObject4;
      localObject5 = (CharSequence)getString(i3, arrayOfObject);
      paramr.setText((CharSequence)localObject5);
    }
    i2 = R.id.tvBankNameBankAccountConnected;
    paramr = (TextView)b(i2);
    if (paramr != null)
    {
      if (parama != null)
      {
        localObject5 = parama.a();
        if (localObject5 != null) {
          localObject2 = ((com.truecaller.truepay.data.api.model.a)localObject5).c();
        }
      }
      localObject2 = (CharSequence)localObject2;
      paramr.setText((CharSequence)localObject2);
    }
    if (parama != null)
    {
      parama = parama.a();
      if (parama != null)
      {
        paramr = i;
        if (paramr != null)
        {
          localObject2 = parama.d();
          c.g.b.k.a(localObject2, "it.accountNumber");
          localObject5 = "success";
          i4 = m;
          paramr = paramr.b();
          localObject4 = "bank.bankName";
          c.g.b.k.a(paramr, (String)localObject4);
          a((String)localObject2, (String)localObject5, i4, paramr);
        }
        paramr = a;
        if (paramr == null)
        {
          localObject2 = "presenter";
          c.g.b.k.a((String)localObject2);
        }
        paramr.a(parama);
        boolean bool2 = o ^ bool1;
        a(bool2, false);
        paramr = p;
        localObject1 = new com/truecaller/truepay/app/ui/registration/views/b/a$g;
        ((a.g)localObject1).<init>(parama, this);
        localObject1 = (Runnable)localObject1;
        long l1 = 2000L;
        paramr.postDelayed((Runnable)localObject1, l1);
        break label572;
      }
    }
    parama = f;
    if (parama != null) {
      parama.a();
    }
    label572:
    parama = a;
    if (parama == null)
    {
      paramr = "presenter";
      c.g.b.k.a(paramr);
    }
    parama.b(bool1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.c.b paramb)
  {
    if (paramb != null)
    {
      localObject1 = paramb.a();
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
    }
    int i1 = ((ArrayList)localObject1).size();
    m = i1;
    i1 = m;
    int i2 = 0;
    Object localObject2 = null;
    String str1;
    switch (i1)
    {
    default: 
      if (paramb != null) {
        localObject1 = paramb.a();
      }
      break;
    case 1: 
      localObject3 = a;
      if (localObject3 == null)
      {
        str1 = "presenter";
        c.g.b.k.a(str1);
      }
      ((b.a)localObject3).a(paramb, 0);
      paramb = i;
      if (paramb != null)
      {
        localObject1 = ((ArrayList)localObject1).get(0);
        c.g.b.k.a(localObject1, "accounts[0]");
        localObject1 = ((com.truecaller.truepay.data.api.model.a)localObject1).d();
        c.g.b.k.a(localObject1, "accounts[0].accountNumber");
        i2 = m;
        str1 = paramb.b();
        String str2 = "bank.bankName";
        c.g.b.k.a(str1, str2);
        a((String)localObject1, "pending", i2, str1);
        localObject1 = Truepay.getInstance();
        c.g.b.k.a(localObject1, "Truepay.getInstance()");
        localObject1 = ((Truepay)localObject1).getAnalyticLoggerHelper();
        localObject3 = "PayRegistrationBankSelectedName";
        paramb = paramb.b();
        ((com.truecaller.truepay.data.b.a)localObject1).a((String)localObject3, paramb);
      }
      return;
    case 0: 
      paramb = requireContext();
      i3 = R.string.no_accounts_available;
      localObject1 = (CharSequence)getString(i3);
      Toast.makeText(paramb, (CharSequence)localObject1, 0).show();
      return;
    }
    int i3 = 0;
    Object localObject1 = null;
    Object localObject3 = i;
    localObject1 = g.a((ArrayList)localObject1, (com.truecaller.truepay.data.d.a)localObject3);
    localObject3 = getFragmentManager();
    if (localObject3 != null)
    {
      localObject3 = ((j)localObject3).a();
      localObject2 = localObject1;
      localObject2 = (Fragment)localObject1;
      str1 = g.class.getSimpleName();
      localObject3 = ((android.support.v4.app.o)localObject3).a((Fragment)localObject2, str1);
      ((android.support.v4.app.o)localObject3).d();
    }
    localObject3 = new com/truecaller/truepay/app/ui/registration/views/b/a$m;
    ((a.m)localObject3).<init>(this, paramb);
    localObject3 = (g.a)localObject3;
    ((g)localObject1).a((g.a)localObject3);
  }
  
  public final void a(Integer paramInteger)
  {
    String str = "sim_verification_started";
    com.truecaller.truepay.app.ui.registration.views.c.k localk = null;
    a("initiated", str, false);
    Object localObject1 = a;
    if (localObject1 == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    int i1 = 1;
    ((b.a)localObject1).b(i1);
    localk = d;
    if (localk == null)
    {
      localObject1 = "smsVerification";
      c.g.b.k.a((String)localObject1);
    }
    Context localContext = getContext();
    localObject1 = t;
    Object localObject2 = localObject1;
    localObject2 = (BroadcastReceiver)localObject1;
    b.a locala = a;
    if (locala == null)
    {
      localObject1 = "presenter";
      c.g.b.k.a((String)localObject1);
    }
    com.truecaller.truepay.app.ui.registration.c.o localo = s;
    localk.a(localContext, (BroadcastReceiver)localObject2, locala, localo, paramInteger);
  }
  
  public final void a(String paramString)
  {
    int i1 = 1;
    Object localObject = new String[i1];
    paramString = String.valueOf(paramString);
    paramString = "onCheckVerificationFailure".concat(paramString);
    localObject[0] = paramString;
    r = false;
    paramString = a;
    if (paramString == null)
    {
      localObject = "presenter";
      c.g.b.k.a((String)localObject);
    }
    paramString.d();
    paramString = a;
    if (paramString == null)
    {
      localObject = "presenter";
      c.g.b.k.a((String)localObject);
    }
    paramString.b(2);
    paramString = new java/lang/AssertionError;
    paramString.<init>("CheckVerificationFailure");
    d.a((Throwable)paramString);
  }
  
  public final void a(String paramString, r paramr)
  {
    c.g.b.k.b(paramString, "msisdn");
    c.g.b.k.b(paramr, "imageLoader");
    new String[1][0] = "onVerifiationAuthSaved";
    Object localObject1 = "continue";
    boolean bool = true;
    a("success", (String)localObject1, bool);
    r = false;
    Object localObject2 = a;
    if (localObject2 == null)
    {
      localObject1 = "presenter";
      c.g.b.k.a((String)localObject1);
    }
    ((b.a)localObject2).d();
    localObject2 = i;
    if (localObject2 != null)
    {
      localObject1 = a;
      if (localObject1 == null)
      {
        str1 = "presenter";
        c.g.b.k.a(str1);
      }
      String str1 = ((com.truecaller.truepay.data.d.a)localObject2).c();
      c.g.b.k.a(str1, "it.accountProviderId");
      String str2 = ((com.truecaller.truepay.data.d.a)localObject2).a();
      String str3 = "it.id";
      c.g.b.k.a(str2, str3);
      ((b.a)localObject1).a(str1, str2);
      localObject1 = "initiated";
      str1 = "select_bank";
      localObject2 = ((com.truecaller.truepay.data.d.a)localObject2).b();
      str2 = "it.bankName";
      c.g.b.k.a(localObject2, str2);
      a((String)localObject1, str1, (String)localObject2);
    }
    localObject2 = a;
    if (localObject2 == null)
    {
      localObject1 = "presenter";
      c.g.b.k.a((String)localObject1);
    }
    ((b.a)localObject2).b(4);
    b(paramString, paramr);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString2, "msisdn");
    h localh = c;
    if (localh == null)
    {
      String str = "multiSimManager";
      c.g.b.k.a(str);
    }
    boolean bool = localh.j();
    a(bool, paramString1, paramString2);
    paramString1 = new java/lang/AssertionError;
    paramString1.<init>("AddAccountFailure");
    d.a((Throwable)paramString1);
  }
  
  public final void a(ArrayList paramArrayList, boolean paramBoolean, r paramr)
  {
    c.g.b.k.b(paramArrayList, "accounts");
    c.g.b.k.b(paramr, "imageLoader");
    new String[1][0] = "onFetchConnectedBanksSuccessful";
    Object localObject2;
    if (paramBoolean)
    {
      localObject1 = paramArrayList;
      localObject1 = (Collection)paramArrayList;
      paramBoolean = ((Collection)localObject1).isEmpty();
      boolean bool = true;
      paramBoolean ^= bool;
      if (paramBoolean)
      {
        localObject1 = a;
        if (localObject1 == null)
        {
          localObject2 = "presenter";
          c.g.b.k.a((String)localObject2);
        }
        int i1 = 5;
        ((b.a)localObject1).b(i1);
        paramBoolean = R.id.ivBankLogoBankAccountConnected;
        localObject1 = (ImageView)b(paramBoolean);
        Object localObject3;
        if (localObject1 != null)
        {
          localObject2 = paramArrayList.get(0);
          c.g.b.k.a(localObject2, "accounts[0]");
          localObject2 = ((com.truecaller.truepay.data.api.model.a)localObject2).l();
          localObject3 = "accounts[0].bank";
          c.g.b.k.a(localObject2, (String)localObject3);
          localObject2 = ((com.truecaller.truepay.data.d.a)localObject2).d();
          paramr = paramr.b((String)localObject2);
          ((ImageView)localObject1).setImageDrawable(paramr);
        }
        paramBoolean = R.id.tvAccountNumberBankAccountConnected;
        localObject1 = (TextView)b(paramBoolean);
        int i2;
        String str;
        if (localObject1 != null)
        {
          i2 = R.string.bank_name_acc_number;
          i1 = 2;
          localObject2 = new Object[i1];
          localObject3 = paramArrayList.get(0);
          c.g.b.k.a(localObject3, "accounts[0]");
          localObject3 = ((com.truecaller.truepay.data.api.model.a)localObject3).l();
          c.g.b.k.a(localObject3, "accounts[0].bank");
          localObject3 = ((com.truecaller.truepay.data.d.a)localObject3).b();
          localObject2[0] = localObject3;
          localObject3 = paramArrayList.get(0);
          str = "accounts[0]";
          c.g.b.k.a(localObject3, str);
          localObject3 = ((com.truecaller.truepay.data.api.model.a)localObject3).d();
          localObject2[bool] = localObject3;
          paramr = (CharSequence)getString(i2, (Object[])localObject2);
          ((TextView)localObject1).setText(paramr);
        }
        paramBoolean = R.id.tvBankIfscBankAccountConnected;
        localObject1 = (TextView)b(paramBoolean);
        if (localObject1 != null)
        {
          i2 = R.string.ifsc_code;
          localObject2 = new Object[bool];
          localObject3 = paramArrayList.get(0);
          str = "accounts[0]";
          c.g.b.k.a(localObject3, str);
          localObject3 = ((com.truecaller.truepay.data.api.model.a)localObject3).e();
          localObject2[0] = localObject3;
          paramr = (CharSequence)getString(i2, (Object[])localObject2);
          ((TextView)localObject1).setText(paramr);
        }
        paramBoolean = R.id.tvBankNameBankAccountConnected;
        localObject1 = (TextView)b(paramBoolean);
        if (localObject1 != null)
        {
          paramr = paramArrayList.get(0);
          localObject2 = "accounts[0]";
          c.g.b.k.a(paramr, (String)localObject2);
          paramr = (CharSequence)((com.truecaller.truepay.data.api.model.a)paramr).c();
          ((TextView)localObject1).setText(paramr);
        }
      }
      localObject1 = a;
      if (localObject1 == null)
      {
        paramr = "presenter";
        c.g.b.k.a(paramr);
      }
      ((b.a)localObject1).a(paramArrayList);
      paramArrayList = a;
      if (paramArrayList == null)
      {
        localObject1 = "presenter";
        c.g.b.k.a((String)localObject1);
      }
      paramArrayList.b(bool);
      a(false, false);
      paramArrayList = p;
      localObject1 = new com/truecaller/truepay/app/ui/registration/views/b/a$i;
      ((a.i)localObject1).<init>(this);
      localObject1 = (Runnable)localObject1;
      paramArrayList.postDelayed((Runnable)localObject1, 2000L);
      return;
    }
    Object localObject1 = f.a(paramArrayList);
    paramr = getFragmentManager();
    if (paramr != null)
    {
      paramr = paramr.a();
      Object localObject4 = localObject1;
      localObject4 = (Fragment)localObject1;
      localObject2 = f.class.getSimpleName();
      paramr = paramr.a((Fragment)localObject4, (String)localObject2);
      paramr.d();
    }
    c.g.b.k.a(localObject1, "dialog");
    ((f)localObject1).setCancelable(false);
    paramr = new com/truecaller/truepay/app/ui/registration/views/b/a$j;
    paramr.<init>(this, paramArrayList);
    paramr = (f.a)paramr;
    ((f)localObject1).a(paramr);
  }
  
  public final void a(boolean paramBoolean)
  {
    long l1;
    double d1;
    long l2;
    if (paramBoolean)
    {
      l1 = 45000L;
      d1 = 2.2233E-319D;
      l2 = l1;
    }
    else
    {
      l1 = 60000L;
      d1 = 2.9644E-319D;
      l2 = l1;
    }
    paramBoolean = R.id.tvStatusHeader;
    Object localObject = (TextView)b(paramBoolean);
    if (localObject != null)
    {
      int i1 = R.string.verifying_your_mobile_num;
      CharSequence localCharSequence = (CharSequence)getString(i1);
      ((TextView)localObject).setText(localCharSequence);
    }
    localObject = new com/truecaller/truepay/app/ui/registration/views/b/a$k;
    ((a.k)localObject).<init>(this, l2, l2);
    localObject = ((a.k)localObject).start();
    g = ((CountDownTimer)localObject);
    r = true;
  }
  
  public final View b(int paramInt)
  {
    Object localObject1 = u;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      u = ((HashMap)localObject1);
    }
    localObject1 = u;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = u;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final e b()
  {
    e locale = b;
    if (locale == null)
    {
      String str = "featuresRegistry";
      c.g.b.k.a(str);
    }
    return locale;
  }
  
  public final void b(String paramString, r paramr)
  {
    c.g.b.k.b(paramString, "msisdn");
    c.g.b.k.b(paramr, "imageLoader");
    int i1 = R.id.ivStatusIconReg;
    Object localObject1 = (ImageView)b(i1);
    int i2;
    if (localObject1 != null)
    {
      i2 = R.drawable.ic_tick;
      ((ImageView)localObject1).setImageResource(i2);
    }
    i1 = R.id.tvStatusHeader;
    localObject1 = (TextView)b(i1);
    if (localObject1 != null)
    {
      paramString = (CharSequence)paramString;
      ((TextView)localObject1).setText(paramString);
    }
    int i3 = R.id.tvStatusSubHeader;
    paramString = (TextView)b(i3);
    if (paramString != null)
    {
      i1 = R.string.mobile_number_verified;
      localObject1 = (CharSequence)getString(i1);
      paramString.setText((CharSequence)localObject1);
    }
    i3 = R.id.ivStatusIconSecondary;
    paramString = (ImageView)b(i3);
    if (paramString != null)
    {
      i1 = R.drawable.ic_second_step_activated;
      paramString.setImageResource(i1);
    }
    i3 = R.id.tvStatusHeaderSecondary;
    paramString = (TextView)b(i3);
    if (paramString != null)
    {
      i1 = R.string.awaiting_bank_response;
      localObject1 = (CharSequence)getString(i1);
      paramString.setText((CharSequence)localObject1);
    }
    i3 = R.id.tvStatusHeaderSecondary;
    paramString = (TextView)b(i3);
    if (paramString != null)
    {
      localObject1 = getResources();
      i2 = R.color.black;
      i1 = ((Resources)localObject1).getColor(i2);
      paramString.setTextColor(i1);
    }
    i3 = R.id.ivStatusSecondaryAccountConn;
    paramString = (ImageView)b(i3);
    i1 = 0;
    localObject1 = null;
    if (paramString != null) {
      paramString.setVisibility(0);
    }
    i3 = R.id.ivBankIconAccountConn;
    paramString = (ImageView)b(i3);
    if (paramString != null)
    {
      Object localObject2 = i;
      if (localObject2 == null) {
        c.g.b.k.a();
      }
      localObject2 = ((com.truecaller.truepay.data.d.a)localObject2).d();
      paramr = paramr.b((String)localObject2);
      paramString.setImageDrawable(paramr);
    }
    i3 = R.id.pbAccountConnectionProgress;
    paramString = (ProgressBar)b(i3);
    if (paramString != null)
    {
      paramString.setVisibility(0);
      return;
    }
  }
  
  public final void b(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "message");
    c.g.b.k.b(paramString2, "msisdn");
    Object localObject1 = a;
    if (localObject1 == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((b.a)localObject1).b(6);
    int i1 = R.id.tvSeparatorOr;
    localObject1 = (TextView)b(i1);
    c.g.b.k.a(localObject1, "tvSeparatorOr");
    localObject1 = (View)localObject1;
    String str = null;
    t.a((View)localObject1, false);
    i1 = R.id.btnSecondaryAccountFetchFailed;
    localObject1 = (TextView)b(i1);
    c.g.b.k.a(localObject1, "btnSecondaryAccountFetchFailed");
    t.a((View)localObject1, false);
    i1 = R.id.btnPrimaryAccountFetchFailed;
    localObject1 = (AppCompatButton)b(i1);
    c.g.b.k.a(localObject1, "btnPrimaryAccountFetchFailed");
    int i2 = R.string.retry;
    Object localObject2 = (CharSequence)getString(i2);
    ((AppCompatButton)localObject1).setText((CharSequence)localObject2);
    i1 = R.id.tvStatusAccountFetchFailed;
    localObject1 = (TextView)b(i1);
    localObject2 = "tvStatusAccountFetchFailed";
    c.g.b.k.a(localObject1, (String)localObject2);
    i2 = R.string.account_link_failed_status;
    int i3 = 2;
    Object[] arrayOfObject = new Object[i3];
    Object localObject3 = i;
    if (localObject3 != null) {
      localObject3 = ((com.truecaller.truepay.data.d.a)localObject3).b();
    } else {
      localObject3 = null;
    }
    arrayOfObject[0] = localObject3;
    arrayOfObject[1] = paramString2;
    paramString2 = (CharSequence)getString(i2, arrayOfObject);
    ((TextView)localObject1).setText(paramString2);
    int i4 = R.id.tvAccountLinkFailureMessage;
    paramString2 = (TextView)b(i4);
    c.g.b.k.a(paramString2, "tvAccountLinkFailureMessage");
    paramString1 = (CharSequence)paramString1;
    paramString2.setText(paramString1);
    int i5 = R.id.btnPrimaryAccountFetchFailed;
    paramString1 = (AppCompatButton)b(i5);
    paramString2 = new com/truecaller/truepay/app/ui/registration/views/b/a$h;
    paramString2.<init>(this);
    paramString2 = (View.OnClickListener)paramString2;
    paramString1.setOnClickListener(paramString2);
  }
  
  public final h c()
  {
    h localh = c;
    if (localh == null)
    {
      String str = "multiSimManager";
      c.g.b.k.a(str);
    }
    return localh;
  }
  
  public final void c(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString2, "msisdn");
    boolean bool = true;
    Object localObject = new String[bool];
    String str1 = String.valueOf(paramString1);
    String str2 = "onFetchAccountFailure ".concat(str1);
    str1 = null;
    localObject[0] = str2;
    localObject = c;
    if (localObject == null)
    {
      str2 = "multiSimManager";
      c.g.b.k.a(str2);
    }
    bool = ((h)localObject).j();
    a(bool, paramString1, paramString2);
    paramString1 = i;
    if (paramString1 != null)
    {
      paramString2 = "failure";
      localObject = "select_bank";
      paramString1 = paramString1.b();
      str2 = "it.bankName";
      c.g.b.k.a(paramString1, str2);
      a(paramString2, (String)localObject, paramString1);
    }
    paramString1 = new java/lang/AssertionError;
    paramString1.<init>("FetchAccountFailure");
    d.a((Throwable)paramString1);
  }
  
  public final void d()
  {
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    int i1 = locala.g();
    switch (i1)
    {
    default: 
      break;
    case 6: 
      w();
      break;
    case 5: 
      w();
      return;
    }
  }
  
  public final void e()
  {
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    int i1 = locala.f();
    int i2 = 4;
    if (i1 == i2) {
      w();
    }
  }
  
  public final void f()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    ((b.a)localObject1).b(6);
    int i1 = R.id.btnPrimaryAccountFetchFailed;
    localObject1 = (AppCompatButton)b(i1);
    c.g.b.k.a(localObject1, "btnPrimaryAccountFetchFailed");
    int i2 = R.string.retry;
    Object localObject2 = (CharSequence)getString(i2);
    ((AppCompatButton)localObject1).setText((CharSequence)localObject2);
    i1 = R.id.btnSecondaryAccountFetchFailed;
    localObject1 = (TextView)b(i1);
    c.g.b.k.a(localObject1, "btnSecondaryAccountFetchFailed");
    i2 = R.string.choose_another_bank;
    localObject2 = (CharSequence)getString(i2);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    i1 = R.id.tvStatusAccountFetchFailed;
    localObject1 = (TextView)b(i1);
    c.g.b.k.a(localObject1, "tvStatusAccountFetchFailed");
    i2 = R.string.already_connected_account;
    localObject2 = (CharSequence)getString(i2);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    i1 = R.id.tvAccountLinkFailureMessage;
    localObject1 = (TextView)b(i1);
    localObject2 = "tvAccountLinkFailureMessage";
    c.g.b.k.a(localObject1, (String)localObject2);
    i2 = R.string.duplicate_account_msg;
    int i3 = 1;
    Object[] arrayOfObject = new Object[i3];
    Object localObject3 = i;
    if (localObject3 != null) {
      localObject3 = ((com.truecaller.truepay.data.d.a)localObject3).b();
    } else {
      localObject3 = null;
    }
    arrayOfObject[0] = localObject3;
    localObject2 = (CharSequence)getString(i2, arrayOfObject);
    ((TextView)localObject1).setText((CharSequence)localObject2);
  }
  
  public final void g()
  {
    new String[1][0] = "onVerificationDeviceBindingFailed";
    r = false;
    b.a locala = a;
    String str;
    if (locala == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    locala.d();
    locala = a;
    if (locala == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    locala.b(2);
  }
  
  public final void h()
  {
    new String[1][0] = "onVerificationUserExisting";
    b.a locala = null;
    r = false;
    Object localObject = a;
    String str1;
    if (localObject == null)
    {
      str1 = "presenter";
      c.g.b.k.a(str1);
    }
    ((b.a)localObject).d();
    localObject = a;
    if (localObject == null)
    {
      str1 = "presenter";
      c.g.b.k.a(str1);
    }
    int i1 = 4;
    ((b.a)localObject).b(i1);
    y();
    localObject = a;
    if (localObject == null)
    {
      String str2 = "presenter";
      c.g.b.k.a(str2);
    }
    int i2 = h;
    ((b.a)localObject).a(false, i2);
    locala = a;
    if (locala == null)
    {
      localObject = "presenter";
      c.g.b.k.a((String)localObject);
    }
    locala.b(i1);
  }
  
  public final void i()
  {
    new String[1][0] = "onVerifiationUserReturning";
    r = false;
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.d();
    t();
  }
  
  public final void j()
  {
    new String[1][0] = "onVerificationUnknownStatus";
    r = false;
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.d();
    t();
  }
  
  public final void k()
  {
    new String[1][0] = "onVerificationInitiated";
  }
  
  public final void l()
  {
    new String[1][0] = "onVerificationCallbackReceived";
  }
  
  public final void m()
  {
    new String[1][0] = "onVerificationMsisdnMismatch";
    r = false;
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.d();
  }
  
  public final void n()
  {
    new String[1][0] = "onVerificationDeviceBound";
  }
  
  public final void o()
  {
    new String[1][0] = "onVerificationDeviceBindingPending";
  }
  
  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.registration.b.a.a();
    Object localObject1 = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject1).a().a(this);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject1 = "presenter";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = this;
    localObject1 = (com.truecaller.truepay.app.ui.base.views.a)this;
    paramBundle.a((com.truecaller.truepay.app.ui.base.views.a)localObject1);
    paramBundle = getArguments();
    int i1 = 0;
    localObject1 = null;
    if (paramBundle != null)
    {
      localObject2 = "cd_response";
      paramBundle = paramBundle.getSerializable((String)localObject2);
    }
    else
    {
      i3 = 0;
      paramBundle = null;
    }
    paramBundle = (com.truecaller.truepay.app.ui.registration.c.o)paramBundle;
    s = paramBundle;
    int i3 = R.id.btnRetrySmsVerification;
    paramBundle = (AppCompatButton)b(i3);
    Object localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/a$c;
    ((a.c)localObject2).<init>(this);
    localObject2 = (View.OnClickListener)localObject2;
    paramBundle.setOnClickListener((View.OnClickListener)localObject2);
    i3 = R.id.btnRetrySendSms;
    paramBundle = (AppCompatButton)b(i3);
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/a$d;
    ((a.d)localObject2).<init>(this);
    localObject2 = (View.OnClickListener)localObject2;
    paramBundle.setOnClickListener((View.OnClickListener)localObject2);
    i3 = R.id.btnPrimaryAccountFetchFailed;
    paramBundle = (AppCompatButton)b(i3);
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/a$e;
    ((a.e)localObject2).<init>(this);
    localObject2 = (View.OnClickListener)localObject2;
    paramBundle.setOnClickListener((View.OnClickListener)localObject2);
    i3 = R.id.btnSecondaryAccountFetchFailed;
    paramBundle = (TextView)b(i3);
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/a$f;
    ((a.f)localObject2).<init>(this);
    localObject2 = (View.OnClickListener)localObject2;
    paramBundle.setOnClickListener((View.OnClickListener)localObject2);
    paramBundle = getArguments();
    if (paramBundle != null)
    {
      localObject2 = "selected_sim";
      i3 = paramBundle.getInt((String)localObject2);
    }
    else
    {
      i3 = -1;
    }
    h = i3;
    paramBundle = getArguments();
    if (paramBundle != null)
    {
      localObject2 = "selected_bank";
      paramBundle = paramBundle.get((String)localObject2);
    }
    else
    {
      i3 = 0;
      paramBundle = null;
    }
    paramBundle = (com.truecaller.truepay.data.d.a)paramBundle;
    i = paramBundle;
    paramBundle = getArguments();
    if (paramBundle != null)
    {
      localObject2 = "is_using_sms_data";
      paramBundle = paramBundle.get((String)localObject2);
    }
    else
    {
      i3 = 0;
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      boolean bool2 = ((Boolean)paramBundle).booleanValue();
      n = bool2;
      paramBundle = getArguments();
      if (paramBundle != null)
      {
        localObject2 = "deeplink_host";
        paramBundle = paramBundle.getString((String)localObject2);
      }
      else
      {
        bool2 = false;
        paramBundle = null;
      }
      localObject2 = getArguments();
      if (localObject2 != null)
      {
        String str = "cd_response";
        localObject2 = ((Bundle)localObject2).getSerializable(str);
      }
      else
      {
        bool3 = false;
        localObject2 = null;
      }
      localObject2 = (com.truecaller.truepay.app.ui.registration.c.o)localObject2;
      s = ((com.truecaller.truepay.app.ui.registration.c.o)localObject2);
      localObject2 = s;
      if (localObject2 != null) {
        localObject1 = ((com.truecaller.truepay.app.ui.registration.c.o)localObject2).c();
      }
      localObject1 = (CharSequence)localObject1;
      boolean bool3 = true;
      if (localObject1 != null)
      {
        i1 = ((CharSequence)localObject1).length();
        if (i1 != 0)
        {
          i1 = 0;
          localObject1 = null;
          break label504;
        }
      }
      i1 = 1;
      label504:
      if (i1 == 0)
      {
        localObject1 = s;
        if (localObject1 != null)
        {
          boolean bool1 = ((com.truecaller.truepay.app.ui.registration.c.o)localObject1).a();
          if (bool1 == bool3)
          {
            localObject1 = s;
            if (localObject1 != null)
            {
              bool1 = ((com.truecaller.truepay.app.ui.registration.c.o)localObject1).b();
              if (bool1 == bool3)
              {
                y();
                paramBundle = a;
                if (paramBundle == null)
                {
                  localObject1 = "presenter";
                  c.g.b.k.a((String)localObject1);
                }
                int i2 = 4;
                paramBundle.b(i2);
                paramBundle = a;
                if (paramBundle == null)
                {
                  localObject1 = "presenter";
                  c.g.b.k.a((String)localObject1);
                }
                i2 = h;
                paramBundle.a(bool3, i2);
                break label700;
              }
            }
          }
        }
      }
      if (paramBundle != null)
      {
        localObject1 = "add_account";
        bool2 = c.g.b.k.a(paramBundle, localObject1);
        if (bool2)
        {
          o = bool3;
          paramBundle = "manage_account";
          b(paramBundle);
          break label700;
        }
      }
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        c.g.b.k.a((String)localObject1);
      }
      localObject1 = i;
      localObject2 = s;
      paramBundle.a((com.truecaller.truepay.data.d.a)localObject1, (com.truecaller.truepay.app.ui.registration.c.o)localObject2);
      int i4 = h;
      paramBundle = Integer.valueOf(i4);
      a(paramBundle);
      label700:
      paramBundle = new com/truecaller/truepay/app/ui/registration/e/b;
      localObject1 = this;
      localObject1 = (com.truecaller.truepay.app.ui.registration.e.a)this;
      paramBundle.<init>((com.truecaller.truepay.app.ui.registration.e.a)localObject1);
      q = paramBundle;
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type kotlin.Boolean");
    throw paramBundle;
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int i1 = 5001;
    if (paramInt1 == i1)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        if (paramIntent != null)
        {
          localObject1 = paramIntent.getSerializableExtra("selected_bank");
        }
        else
        {
          paramInt1 = 0;
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          localObject1 = (com.truecaller.truepay.data.d.a)localObject1;
          i = ((com.truecaller.truepay.data.d.a)localObject1);
          localObject1 = i;
          Object localObject2;
          if (localObject1 != null)
          {
            paramInt2 = o;
            if (paramInt2 == 0)
            {
              paramInt2 = 1;
              k = paramInt2;
            }
            localObject2 = a;
            if (localObject2 == null)
            {
              paramIntent = "presenter";
              c.g.b.k.a(paramIntent);
            }
            paramIntent = ((com.truecaller.truepay.data.d.a)localObject1).c();
            c.g.b.k.a(paramIntent, "it.accountProviderId");
            String str1 = ((com.truecaller.truepay.data.d.a)localObject1).a();
            String str2 = "it.id";
            c.g.b.k.a(str1, str2);
            ((b.a)localObject2).a(paramIntent, str1);
            localObject2 = "initiated";
            paramIntent = "select_bank";
            localObject1 = ((com.truecaller.truepay.data.d.a)localObject1).b();
            str1 = "it.bankName";
            c.g.b.k.a(localObject1, str1);
            a((String)localObject2, paramIntent, (String)localObject1);
          }
          localObject1 = a;
          if (localObject1 == null)
          {
            localObject2 = "presenter";
            c.g.b.k.a((String)localObject2);
          }
          paramInt2 = 4;
          ((b.a)localObject1).b(paramInt2);
          localObject1 = a;
          if (localObject1 == null)
          {
            localObject2 = "presenter";
            c.g.b.k.a((String)localObject2);
          }
          ((b.a)localObject1).e();
          return;
        }
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type com.truecaller.truepay.data.model.Bank");
        throw ((Throwable)localObject1);
      }
      Object localObject1 = getActivity();
      if (localObject1 != null)
      {
        ((android.support.v4.app.f)localObject1).finish();
        return;
      }
    }
  }
  
  public final void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    if (paramActivity != null) {
      localObject1 = paramActivity;
    }
    try
    {
      localObject1 = (a.b)paramActivity;
      f = ((a.b)localObject1);
      return;
    }
    catch (ClassCastException localClassCastException)
    {
      Object localObject2;
      for (;;) {}
    }
    Object localObject1 = new c/u;
    localObject2 = "null cannot be cast to non-null type com.truecaller.truepay.app.ui.registration.views.fragments.AccountConnectionFragment.OnFragmentInteractionListener";
    ((u)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
    localObject1 = new java/lang/ClassCastException;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    paramActivity = String.valueOf(paramActivity);
    ((StringBuilder)localObject2).append(paramActivity);
    ((StringBuilder)localObject2).append(" must implement OnFragmentInteractionListener");
    paramActivity = ((StringBuilder)localObject2).toString();
    ((ClassCastException)localObject1).<init>(paramActivity);
    throw ((Throwable)localObject1);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    int i1 = R.layout.fragment_account_connection;
    return paramLayoutInflater.inflate(i1, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    f = null;
    Object localObject1 = a;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    ((b.a)localObject1).b();
    localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = "smsVerification";
      c.g.b.k.a((String)localObject1);
    }
    t();
    z();
    localObject1 = q;
    if (localObject1 != null)
    {
      localObject2 = getContext();
      if (localObject2 == null) {
        c.g.b.k.a();
      }
      String str = "context!!";
      c.g.b.k.a(localObject2, str);
      ((com.truecaller.truepay.app.ui.registration.e.b)localObject1).b((Context)localObject2);
    }
    localObject1 = u;
    if (localObject1 != null) {
      ((HashMap)localObject1).clear();
    }
  }
  
  public final void onPause()
  {
    super.onPause();
    Object localObject = b;
    if (localObject == null)
    {
      String str = "featuresRegistry";
      c.g.b.k.a(str);
    }
    localObject = ((e)localObject).D();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      bool = r;
      if (bool)
      {
        localObject = getActivity();
        if (localObject != null)
        {
          ((android.support.v4.app.f)localObject).finish();
          return;
        }
      }
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    com.truecaller.truepay.app.ui.registration.e.b localb = q;
    if (localb != null)
    {
      Context localContext = getContext();
      if (localContext == null) {
        c.g.b.k.a();
      }
      c.g.b.k.a(localContext, "context!!");
      localb.a(localContext);
      return;
    }
  }
  
  public final void p()
  {
    new String[1][0] = "onVerificationUserCreated";
  }
  
  public final void q()
  {
    new String[1][0] = "onVerificationFailed";
    r = false;
    Object localObject = a;
    String str;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((b.a)localObject).d();
    localObject = a;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((b.a)localObject).b(2);
    localObject = new java/lang/AssertionError;
    ((AssertionError)localObject).<init>("VerificationFailure");
    d.a((Throwable)localObject);
  }
  
  public final void r()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    ((b.a)localObject).b(3);
    localObject = new java/lang/AssertionError;
    ((AssertionError)localObject).<init>("SmsSendingFailure");
    d.a((Throwable)localObject);
  }
  
  public final void s()
  {
    new String[1][0] = "onSmsSendingInitiated";
  }
  
  public final void t()
  {
    CountDownTimer localCountDownTimer = g;
    if (localCountDownTimer != null) {
      localCountDownTimer.cancel();
    }
    g = null;
  }
  
  public final void u()
  {
    Object localObject = a;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    String str = null;
    ((b.a)localObject).b(false);
    localObject = f;
    if (localObject != null)
    {
      ((a.b)localObject).a();
      return;
    }
  }
  
  public final void v()
  {
    Object localObject = requireContext();
    int i1 = R.string.server_error_message;
    CharSequence localCharSequence = (CharSequence)getString(i1);
    Toast.makeText((Context)localObject, localCharSequence, 0).show();
    localObject = new java/lang/AssertionError;
    ((AssertionError)localObject).<init>("Failed to delete existing user");
    d.a((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */