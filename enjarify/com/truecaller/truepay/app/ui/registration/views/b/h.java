package com.truecaller.truepay.app.ui.registration.views.b;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v7.widget.AppCompatButton;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.multisim.SimInfo;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.bankList.BankListActivity;
import com.truecaller.truepay.app.ui.base.views.fragments.TcPayOnFragmentInteractionListener;
import com.truecaller.truepay.app.ui.registration.b.a.a;
import com.truecaller.truepay.app.ui.registration.d.f.a;
import com.truecaller.truepay.app.ui.registration.d.f.b;
import com.truecaller.truepay.app.ui.registration.e.b.a;
import com.truecaller.truepay.app.ui.registration.views.activities.AccountConnectionActivity;
import com.truecaller.truepay.app.utils.at;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.data.api.model.ae;
import com.truecaller.utils.l;
import io.reactivex.q;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public final class h
  extends d
  implements com.truecaller.common.ui.b, com.truecaller.truepay.app.ui.registration.e.a, n.a, com.truecaller.truepay.app.ui.registration.views.c.d
{
  public static final h.a e;
  public com.truecaller.truepay.app.ui.registration.d.f a;
  public com.truecaller.utils.n b;
  public r c;
  public com.truecaller.featuretoggles.e d;
  private TcPayOnFragmentInteractionListener f;
  private com.truecaller.truepay.data.d.a g;
  private String h;
  private String i;
  private Integer j;
  private boolean k = true;
  private final int l = 100;
  private String n;
  private boolean o;
  private boolean p;
  private com.truecaller.truepay.app.ui.registration.e.b q;
  private m r;
  private HashMap s;
  
  static
  {
    h.a locala = new com/truecaller/truepay/app/ui/registration/views/b/h$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  private final void a(int paramInt, String paramString1, String paramString2)
  {
    int m = 1;
    paramInt += m;
    Object localObject1 = String.valueOf(paramInt);
    localObject1 = "SIM ".concat((String)localObject1);
    Object localObject2 = b;
    if (localObject2 == null)
    {
      localObject3 = "resourceProvider";
      k.a((String)localObject3);
    }
    int i1 = R.string.registration_msg_sms_data_available;
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = localObject1;
    arrayOfObject[m] = paramString1;
    arrayOfObject[2] = paramString2;
    Object localObject4 = ((com.truecaller.utils.n)localObject2).a(i1, arrayOfObject);
    k.a(localObject4, "resourceProvider.getStri…, operatorName, bankName)");
    localObject2 = new android/text/SpannableStringBuilder;
    localObject4 = (CharSequence)localObject4;
    ((SpannableStringBuilder)localObject2).<init>((CharSequence)localObject4);
    i1 = 6;
    int i2 = c.n.m.a((CharSequence)localObject4, (String)localObject1, 0, false, i1);
    int i3 = c.n.m.a((CharSequence)localObject4, paramString1, 0, false, i1);
    m = c.n.m.a((CharSequence)localObject4, paramString2, 0, false, i1);
    Object localObject3 = new com/truecaller/truepay/app/ui/registration/views/b/h$e;
    ((h.e)localObject3).<init>(this);
    paramInt = ((String)localObject1).length() + i2;
    ((SpannableStringBuilder)localObject2).setSpan(localObject3, i2, paramInt, 0);
    localObject1 = new com/truecaller/truepay/app/ui/registration/views/b/h$f;
    ((h.f)localObject1).<init>(this);
    int i4 = paramString1.length() + i3;
    ((SpannableStringBuilder)localObject2).setSpan(localObject1, i3, i4, 0);
    localObject1 = new com/truecaller/truepay/app/ui/registration/views/b/h$g;
    ((h.g)localObject1).<init>(this);
    i4 = paramString2.length() + m;
    ((SpannableStringBuilder)localObject2).setSpan(localObject1, m, i4, 0);
    paramInt = R.id.tvBankInfo;
    localObject1 = (TextView)a(paramInt);
    k.a(localObject1, "tvBankInfo");
    paramString1 = LinkMovementMethod.getInstance();
    ((TextView)localObject1).setMovementMethod(paramString1);
    paramInt = R.id.tvBankInfo;
    localObject1 = (TextView)a(paramInt);
    localObject2 = (CharSequence)localObject2;
    paramString1 = TextView.BufferType.SPANNABLE;
    ((TextView)localObject1).setText((CharSequence)localObject2, paramString1);
  }
  
  private final void b(int paramInt)
  {
    Object localObject1 = d;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "featuresRegistry";
      k.a((String)localObject2);
    }
    localObject1 = ((com.truecaller.featuretoggles.e)localObject1).D();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool1)
    {
      localObject1 = a;
      if (localObject1 == null)
      {
        localObject2 = "presenter";
        k.a((String)localObject2);
      }
      e.a();
      localObject1 = m.class.getName();
      localObject2 = r;
      if (localObject2 != null)
      {
        if (localObject2 == null) {
          k.a();
        }
        boolean bool2 = ((m)localObject2).isVisible();
        if (bool2)
        {
          localObject2 = r;
          if (localObject2 == null) {
            k.a();
          }
          bool2 = ((m)localObject2).isAdded();
          if (bool2) {
            return;
          }
        }
      }
      localObject2 = m.b;
      localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/m;
      ((m)localObject2).<init>();
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      String str = "notification_type";
      localBundle.putInt(str, paramInt);
      ((m)localObject2).setArguments(localBundle);
      r = ((m)localObject2);
      Object localObject3 = getFragmentManager();
      if (localObject3 == null) {
        k.a();
      }
      localObject3 = ((j)localObject3).a();
      localObject2 = r;
      if (localObject2 == null) {
        k.a();
      }
      localObject2 = (Fragment)localObject2;
      localObject3 = ((android.support.v4.app.o)localObject3).a((Fragment)localObject2, (String)localObject1);
      ((android.support.v4.app.o)localObject3).d();
    }
  }
  
  public static final h d(String paramString)
  {
    k.b(paramString, "type");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("registration_tab", paramString);
    paramString = new com/truecaller/truepay/app/ui/registration/views/b/h;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  private final void e(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = (Context)getActivity();
    localIntent.<init>(localContext, BankListActivity.class);
    localIntent.putExtra("bank_selection_context", paramString);
    startActivityForResult(localIntent, 5001);
  }
  
  private final void l()
  {
    Object localObject = g;
    if (localObject == null)
    {
      localObject = new java/lang/AssertionError;
      ((AssertionError)localObject).<init>("selected bank should not be null");
      com.truecaller.log.d.a((Throwable)localObject);
      e("registration");
      return;
    }
    localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((com.truecaller.truepay.app.ui.registration.d.f)localObject).e();
  }
  
  private final String m()
  {
    String str1 = n;
    String str2;
    if (str1 == null)
    {
      str2 = "registrationTab";
      k.a(str2);
    }
    int m = str1.hashCode();
    int i1 = -337045466;
    boolean bool;
    if (m != i1)
    {
      i1 = 1382682413;
      if (m == i1)
      {
        str2 = "payments";
        bool = str1.equals(str2);
        if (bool) {
          return "payment";
        }
      }
    }
    else
    {
      str2 = "banking";
      bool = str1.equals(str2);
    }
    return "banking";
  }
  
  private final void n()
  {
    Object localObject = d;
    String str;
    if (localObject == null)
    {
      str = "featuresRegistry";
      k.a(str);
    }
    localObject = ((com.truecaller.featuretoggles.e)localObject).p();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      localObject = a;
      if (localObject == null)
      {
        str = "presenter";
        k.a(str);
      }
      ((com.truecaller.truepay.app.ui.registration.d.f)localObject).a();
      return;
    }
    i();
  }
  
  public final int a()
  {
    return R.layout.fragment_get_started_banking;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = s;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      s = ((HashMap)localObject1);
    }
    localObject1 = s;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = s;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(SimInfo paramSimInfo, int paramInt)
  {
    String str = "simInfo";
    k.b(paramSimInfo, str);
    Object localObject = Integer.valueOf(paramInt);
    j = ((Integer)localObject);
    paramSimInfo = d;
    i = paramSimInfo;
    paramSimInfo = j;
    if (paramSimInfo != null)
    {
      paramSimInfo = (Number)paramSimInfo;
      int m = paramSimInfo.intValue();
      localObject = i;
      if (localObject != null)
      {
        str = h;
        if (str != null) {
          a(m, (String)localObject, str);
        }
      }
    }
    boolean bool = o;
    if (bool)
    {
      bool = true;
      p = bool;
    }
    int i1 = R.id.btnGetStarted;
    ((AppCompatButton)a(i1)).callOnClick();
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.c.o paramo)
  {
    k.b(paramo, "responseDO");
    Intent localIntent = new android/content/Intent;
    Object localObject = (Context)getActivity();
    localIntent.<init>((Context)localObject, AccountConnectionActivity.class);
    Serializable localSerializable = (Serializable)g;
    localIntent.putExtra("selected_bank", localSerializable);
    localSerializable = (Serializable)j;
    localIntent.putExtra("selected_sim", localSerializable);
    paramo = (Serializable)paramo;
    localIntent.putExtra("cd_response", paramo);
    boolean bool = o;
    localIntent.putExtra("is_using_sms_data", bool);
    localObject = m();
    localIntent.putExtra("reg_source", (String)localObject);
    startActivityForResult(localIntent, 2005);
  }
  
  public final void a(com.truecaller.truepay.data.d.a parama)
  {
    k.b(parama, "selectedBank");
    g = parama;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "deepLink");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.setAction("android.intent.action.VIEW");
    paramString = Uri.parse(paramString);
    localIntent.setData(paramString);
    startActivity(localIntent);
  }
  
  public final void a(String paramString, int paramInt, boolean paramBoolean)
  {
    k.b(paramString, "operatorName");
    i = paramString;
    paramString = Integer.valueOf(paramInt);
    j = paramString;
    k = paramBoolean;
  }
  
  public final void a(String paramString, Drawable paramDrawable)
  {
    k.b(paramString, "imageUrl");
    k.b(paramDrawable, "placeHolder");
    int m = R.id.bannerGetStarted;
    Object localObject1 = (ImageView)a(m);
    k.a(localObject1, "bannerGetStarted");
    ((ImageView)localObject1).setVisibility(0);
    m = R.id.imTcBrand;
    localObject1 = (ImageView)a(m);
    Object localObject2 = "imTcBrand";
    k.a(localObject1, (String)localObject2);
    int i1 = 8;
    ((ImageView)localObject1).setVisibility(i1);
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "imageLoader";
      k.a((String)localObject2);
    }
    i1 = R.id.bannerGetStarted;
    localObject2 = (ImageView)a(i1);
    k.a(localObject2, "bannerGetStarted");
    ((r)localObject1).a(paramString, (ImageView)localObject2, paramDrawable, paramDrawable);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, int paramInt, boolean paramBoolean)
  {
    k.b(paramString1, "bankName");
    k.b(paramString2, "bankSymbol");
    k.b(paramString3, "operatorName");
    h = paramString1;
    i = paramString3;
    paramString2 = Integer.valueOf(paramInt);
    j = paramString2;
    k = paramBoolean;
    o = true;
    a(paramInt, paramString3, paramString1);
  }
  
  public final void a(Throwable paramThrowable)
  {
    k.b(paramThrowable, "e");
    a(false);
    int m = R.string.server_error_message;
    String str = getString(m);
    a(str, paramThrowable);
    com.truecaller.log.d.a(paramThrowable);
  }
  
  public final void a(boolean paramBoolean)
  {
    int m = 4;
    if (paramBoolean)
    {
      paramBoolean = R.id.btnGetStarted;
      localObject = (AppCompatButton)a(paramBoolean);
      k.a(localObject, "btnGetStarted");
      ((AppCompatButton)localObject).setVisibility(m);
      paramBoolean = R.id.btnProgress;
      localObject = (ProgressBar)a(paramBoolean);
      k.a(localObject, "btnProgress");
      ((ProgressBar)localObject).setVisibility(0);
      return;
    }
    paramBoolean = R.id.btnGetStarted;
    Object localObject = (AppCompatButton)a(paramBoolean);
    k.a(localObject, "btnGetStarted");
    ((AppCompatButton)localObject).setVisibility(0);
    paramBoolean = R.id.btnProgress;
    localObject = (ProgressBar)a(paramBoolean);
    k.a(localObject, "btnProgress");
    ((ProgressBar)localObject).setVisibility(m);
  }
  
  public final void a(String[] paramArrayOfString)
  {
    k.b(paramArrayOfString, "requiredPermissions");
    int m = l;
    requestPermissions(paramArrayOfString, m);
  }
  
  protected final void b()
  {
    a.a locala = com.truecaller.truepay.app.ui.registration.b.a.a();
    com.truecaller.truepay.app.a.a.a locala1 = Truepay.getApplicationComponent();
    locala.a(locala1).a().a(this);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "tempToken");
    Object localObject1 = a;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    boolean bool1 = o;
    boolean bool2 = p;
    k.b(paramString, "tempToken");
    Object localObject3 = m;
    boolean bool3 = ((at)localObject3).a();
    if (bool3)
    {
      paramString = (com.truecaller.truepay.app.ui.registration.views.c.d)((com.truecaller.truepay.app.ui.registration.d.f)localObject1).af_();
      if (paramString != null)
      {
        paramString.j();
        return;
      }
      return;
    }
    t.a("");
    s.a("");
    localObject3 = u;
    String str = "";
    ((com.truecaller.truepay.data.e.e)localObject3).a(str);
    j.a();
    localObject3 = new com/truecaller/truepay/data/api/model/ae;
    ((ae)localObject3).<init>();
    ((ae)localObject3).a(paramString);
    paramString = i.a((ae)localObject3);
    localObject3 = io.reactivex.g.a.b();
    paramString = paramString.b((io.reactivex.n)localObject3);
    localObject3 = io.reactivex.android.b.a.a();
    paramString = paramString.a((io.reactivex.n)localObject3);
    localObject3 = new com/truecaller/truepay/app/ui/registration/d/f$b;
    ((f.b)localObject3).<init>((com.truecaller.truepay.app.ui.registration.d.f)localObject1);
    localObject3 = (q)localObject3;
    paramString.a((q)localObject3);
    paramString = Truepay.getInstance();
    if (paramString != null)
    {
      paramString = paramString.getAnalyticLoggerHelper();
      if (paramString != null)
      {
        localObject1 = h;
        boolean bool4 = ((com.truecaller.multisim.h)localObject1).j();
        paramString.a(bool1, bool2, bool4);
      }
    }
    paramString = Truepay.getInstance();
    if (paramString != null)
    {
      paramString = paramString.getAnalyticLoggerHelper();
      if (paramString != null)
      {
        localObject1 = "PayRegistrationStartedDate";
        localObject2 = Truepay.getInstance();
        if (localObject2 != null)
        {
          localObject2 = ((Truepay)localObject2).getAnalyticLoggerHelper();
          if (localObject2 != null)
          {
            localObject2 = ((com.truecaller.truepay.data.b.a)localObject2).b();
            break label292;
          }
        }
        bool1 = false;
        localObject2 = null;
        label292:
        if (localObject2 == null) {
          k.a();
        }
        paramString.a((String)localObject1, localObject2);
        return;
      }
    }
  }
  
  public final void c()
  {
    HashMap localHashMap = s;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "message");
    a(false);
    a(paramString, null);
    AssertionError localAssertionError = new java/lang/AssertionError;
    localAssertionError.<init>(paramString);
    com.truecaller.log.d.a((Throwable)localAssertionError);
  }
  
  public final void d()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    Object localObject2 = h.h();
    boolean bool1 = com.truecaller.multisim.b.b.c((List)localObject2);
    int m = 5;
    int i1 = 6;
    int i3;
    if (bool1)
    {
      int i2 = 5;
    }
    else
    {
      localObject2 = p;
      localObject1 = h.h();
      boolean bool2 = ((com.truecaller.multisim.b.b)localObject2).b((List)localObject1);
      if (bool2)
      {
        i3 = 6;
      }
      else
      {
        i3 = 0;
        localObject1 = null;
      }
    }
    if (i3 != 0)
    {
      if (i3 != i1)
      {
        b(m);
        return;
      }
      b(i1);
      return;
    }
    n();
  }
  
  public final void e()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localObject = q;
    boolean bool = ((com.truecaller.truepay.app.utils.c)localObject).a();
    int i1 = 4;
    int m;
    if (bool)
    {
      m = 4;
    }
    else
    {
      m = 0;
      localObject = null;
    }
    if (m != 0)
    {
      b(i1);
      return;
    }
    n();
  }
  
  public final int f()
  {
    return 8;
  }
  
  public final com.truecaller.truepay.app.ui.registration.d.f g()
  {
    com.truecaller.truepay.app.ui.registration.d.f localf = a;
    if (localf == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localf;
  }
  
  public final void h()
  {
    int m = R.id.bannerGetStarted;
    ImageView localImageView = (ImageView)a(m);
    k.a(localImageView, "bannerGetStarted");
    localImageView.setVisibility(8);
    m = R.id.imTcBrand;
    localImageView = (ImageView)a(m);
    k.a(localImageView, "imTcBrand");
    localImageView.setVisibility(0);
  }
  
  public final void i()
  {
    int m = R.id.tvBankInfo;
    TextView localTextView = (TextView)a(m);
    k.a(localTextView, "tvBankInfo");
    Object localObject = b;
    if (localObject == null)
    {
      String str = "resourceProvider";
      k.a(str);
    }
    int i1 = R.string.registration_msg_sms_data_unavailable;
    Object[] arrayOfObject = new Object[0];
    localObject = (CharSequence)((com.truecaller.utils.n)localObject).a(i1, arrayOfObject);
    localTextView.setText((CharSequence)localObject);
  }
  
  public final void j()
  {
    a(false);
    int m = R.string.intro_root_detected;
    String str = getString(m);
    a(str, null);
  }
  
  public final void k()
  {
    com.truecaller.truepay.app.ui.registration.d.f localf = a;
    if (localf == null)
    {
      localObject1 = "presenter";
      k.a((String)localObject1);
    }
    Object localObject1 = l.a();
    localObject1 = k.a((com.truecaller.truepay.app.ui.registration.c.n)localObject1);
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.o)localObject1).b((io.reactivex.n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.o)localObject1).a((io.reactivex.n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/registration/d/f$a;
    ((f.a)localObject2).<init>(localf);
    localObject2 = (q)localObject2;
    ((io.reactivex.o)localObject1).a((q)localObject2);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int m = 1010;
    int i1 = -1;
    Object localObject1;
    Object localObject2;
    if (paramInt1 != m)
    {
      m = 2005;
      String str = null;
      if (paramInt1 != m)
      {
        m = 5001;
        if (paramInt1 == m)
        {
          if (paramInt2 == i1)
          {
            if (paramIntent != null)
            {
              localObject1 = paramIntent.getSerializableExtra("selected_bank");
            }
            else
            {
              paramInt1 = 0;
              localObject1 = null;
            }
            localObject1 = (com.truecaller.truepay.data.d.a)localObject1;
            g = ((com.truecaller.truepay.data.d.a)localObject1);
            localObject1 = g;
            if (localObject1 != null) {
              str = ((com.truecaller.truepay.data.d.a)localObject1).b();
            }
            h = str;
            localObject1 = j;
            if (localObject1 != null)
            {
              localObject1 = (Number)localObject1;
              paramInt1 = ((Number)localObject1).intValue();
              localObject2 = i;
              if (localObject2 != null)
              {
                paramIntent = h;
                if (paramIntent != null) {
                  a(paramInt1, (String)localObject2, paramIntent);
                }
              }
            }
            paramInt1 = o;
            if (paramInt1 != 0)
            {
              paramInt1 = 1;
              p = paramInt1;
            }
            paramInt1 = R.id.btnGetStarted;
            ((AppCompatButton)a(paramInt1)).callOnClick();
            return;
          }
          paramInt1 = o;
          if (paramInt1 == 0) {
            g = null;
          }
        }
      }
      else
      {
        if (paramInt2 != i1)
        {
          paramInt1 = o;
          if (paramInt1 == 0)
          {
            g = null;
            j = null;
          }
          return;
        }
        localObject1 = n;
        if (localObject1 == null)
        {
          localObject2 = "registrationTab";
          k.a((String)localObject2);
        }
        paramInt2 = ((String)localObject1).hashCode();
        int i2 = -337045466;
        if (paramInt2 != i2)
        {
          i2 = 1382682413;
          if (paramInt2 == i2)
          {
            localObject2 = "payments";
            paramInt1 = ((String)localObject1).equals(localObject2);
            if (paramInt1 != 0)
            {
              localObject1 = f;
              if (localObject1 != null)
              {
                localObject2 = Truepay.getInstance();
                paramIntent = "Truepay.getInstance()";
                k.a(localObject2, paramIntent);
                localObject2 = (Fragment)((Truepay)localObject2).getPaymentsFragment();
                ((TcPayOnFragmentInteractionListener)localObject1).replaceFragment((Fragment)localObject2);
              }
            }
          }
        }
        else
        {
          localObject2 = "banking";
          paramInt1 = ((String)localObject1).equals(localObject2);
          if (paramInt1 != 0)
          {
            localObject1 = f;
            if (localObject1 != null)
            {
              localObject2 = Truepay.getInstance();
              paramIntent = "Truepay.getInstance()";
              k.a(localObject2, paramIntent);
              localObject2 = (Fragment)((Truepay)localObject2).getBankingFragment();
              ((TcPayOnFragmentInteractionListener)localObject1).replaceFragment((Fragment)localObject2);
            }
            return;
          }
        }
        localObject1 = requireContext();
        paramInt2 = R.string.failed_to_complete_registration;
        localObject2 = (CharSequence)getString(paramInt2);
        Toast.makeText((Context)localObject1, (CharSequence)localObject2, 0).show();
      }
    }
    else
    {
      localObject1 = d;
      if (localObject1 == null)
      {
        paramIntent = "featuresRegistry";
        k.a(paramIntent);
      }
      localObject1 = ((com.truecaller.featuretoggles.e)localObject1).ae();
      paramInt1 = ((com.truecaller.featuretoggles.b)localObject1).a();
      if ((paramInt1 != 0) && (paramInt2 == i1))
      {
        localObject1 = f;
        if (localObject1 != null)
        {
          localObject2 = Truepay.getInstance();
          k.a(localObject2, "Truepay.getInstance()");
          localObject2 = (Fragment)((Truepay)localObject2).getBankingFragment();
          ((TcPayOnFragmentInteractionListener)localObject1).replaceFragment((Fragment)localObject2);
          return;
        }
      }
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof TcPayOnFragmentInteractionListener;
    if (bool)
    {
      paramContext = (TcPayOnFragmentInteractionListener)getActivity();
      f = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = getActivity();
    if (localObject2 == null) {
      k.a();
    }
    localObject2 = localObject2.getClass().getSimpleName();
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(" must implement ");
    ((StringBuilder)localObject1).append(TcPayOnFragmentInteractionListener.class);
    localObject1 = ((StringBuilder)localObject1).toString();
    paramContext.<init>((String)localObject1);
    throw ((Throwable)paramContext);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    com.truecaller.truepay.app.ui.registration.d.f localf = a;
    if (localf == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localf.b();
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    com.truecaller.truepay.app.ui.registration.e.b localb = q;
    if (localb == null) {
      k.a();
    }
    boolean bool = b;
    if (bool)
    {
      localb = q;
      if (localb != null)
      {
        Context localContext = getContext();
        if (localContext == null) {
          k.a();
        }
        String str = "context!!";
        k.a(localContext, str);
        localb.b(localContext);
      }
    }
    c();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    Object localObject1 = "grantResults";
    k.b(paramArrayOfInt, (String)localObject1);
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int m = l;
    if (paramInt == m)
    {
      Object localObject2 = a;
      if (localObject2 == null)
      {
        localObject1 = "presenter";
        k.a((String)localObject1);
      }
      k.b(paramArrayOfString, "permissions");
      k.b(paramArrayOfInt, "grantResults");
      localObject1 = o;
      localObject2 = a;
      int i1 = localObject2.length;
      localObject2 = (String[])Arrays.copyOf((Object[])localObject2, i1);
      paramInt = ((l)localObject1).a(paramArrayOfString, paramArrayOfInt, (String[])localObject2);
      if (paramInt != 0)
      {
        l();
        return;
      }
      paramInt = R.string.sms_permission_denied;
      localObject2 = getString(paramInt);
      paramArrayOfString = "getString(R.string.sms_permission_denied)";
      k.a(localObject2, paramArrayOfString);
      c((String)localObject2);
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = this;
    paramBundle = (com.truecaller.truepay.app.ui.registration.views.c.d)this;
    paramView.a(paramBundle);
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = "registration_tab";
      paramView = paramView.getString(paramBundle);
      if (paramView != null) {}
    }
    else
    {
      paramView = "banking";
    }
    n = paramView;
    paramView = d;
    if (paramView == null)
    {
      paramBundle = "featuresRegistry";
      k.a(paramBundle);
    }
    paramView = paramView.p();
    boolean bool1 = paramView.a();
    if (bool1)
    {
      paramView = a;
      if (paramView == null)
      {
        paramBundle = "presenter";
        k.a(paramBundle);
      }
      paramView.a();
    }
    else
    {
      i();
    }
    int m = R.id.btnGetStarted;
    paramView = (AppCompatButton)a(m);
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/h$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    m = R.id.tvTermsAndConditions;
    paramView = (TextView)a(m);
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/h$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = n;
    if (paramView == null)
    {
      paramBundle = "registrationTab";
      k.a(paramBundle);
    }
    int i3 = paramView.hashCode();
    int i4 = -337045466;
    int i5 = 8;
    if (i3 != i4)
    {
      i4 = 1382682413;
      if (i3 == i4)
      {
        paramBundle = "payments";
        boolean bool2 = paramView.equals(paramBundle);
        if (bool2)
        {
          int i1 = R.id.bannerGetStarted;
          paramView = (ImageView)a(i1);
          paramBundle = "bannerGetStarted";
          k.a(paramView, paramBundle);
          paramView.setVisibility(i5);
        }
      }
    }
    else
    {
      paramBundle = "banking";
      boolean bool3 = paramView.equals(paramBundle);
      if (bool3)
      {
        i2 = R.id.tvIntroPaymentsSubTitle;
        paramView = (TextView)a(i2);
        paramBundle = "tvIntroPaymentsSubTitle";
        k.a(paramView, paramBundle);
        paramView.setVisibility(i5);
      }
    }
    int i2 = R.id.bannerGetStarted;
    paramView = (ImageView)a(i2);
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/h$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = m();
    k.b(paramBundle, "analyticContext");
    localObject = Truepay.getInstance();
    if (localObject != null)
    {
      localObject = ((Truepay)localObject).getAnalyticLoggerHelper();
      if (localObject != null)
      {
        paramView = h;
        bool4 = paramView.j();
        ((com.truecaller.truepay.data.b.a)localObject).a(paramBundle, paramBundle, bool4);
      }
    }
    paramView = com.truecaller.truepay.app.ui.registration.e.b.c;
    paramView = this;
    paramView = (com.truecaller.truepay.app.ui.registration.e.a)this;
    paramBundle = b.a.a(paramView);
    q = paramBundle;
    paramBundle = q;
    if (paramBundle == null) {
      k.a();
    }
    paramBundle.a(paramView);
    paramView = q;
    if (paramView == null) {
      k.a();
    }
    boolean bool4 = b;
    if (!bool4)
    {
      paramView = q;
      if (paramView != null)
      {
        paramBundle = getContext();
        if (paramBundle == null) {
          k.a();
        }
        k.a(paramBundle, "context!!");
        paramView.a(paramBundle);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */