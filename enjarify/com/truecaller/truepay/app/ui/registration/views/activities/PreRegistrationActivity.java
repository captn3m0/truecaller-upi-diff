package com.truecaller.truepay.app.ui.registration.views.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.j;
import android.widget.Toast;
import c.g.a.m;
import c.u;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.truepay.PayTempTokenCallBack;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.bankList.BankListActivity;
import com.truecaller.truepay.app.ui.registration.b.a.a;
import com.truecaller.truepay.app.ui.registration.d.k.a;
import com.truecaller.truepay.app.ui.registration.d.k.b;
import com.truecaller.truepay.app.ui.registration.d.k.c;
import com.truecaller.truepay.app.ui.registration.d.k.d;
import com.truecaller.truepay.app.ui.registration.d.k.e;
import com.truecaller.truepay.app.ui.registration.e.b.a;
import com.truecaller.truepay.app.ui.registration.views.b.n.a;
import com.truecaller.truepay.app.utils.at;
import com.truecaller.truepay.data.api.model.ae;
import com.truecaller.utils.l;
import io.reactivex.q;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;

public final class PreRegistrationActivity
  extends com.truecaller.truepay.app.ui.base.views.a.a
  implements com.truecaller.truepay.app.ui.registration.e.a, n.a, com.truecaller.truepay.app.ui.registration.views.c.f
{
  public static final PreRegistrationActivity.a c;
  public com.truecaller.truepay.app.ui.registration.d.k a;
  public com.truecaller.featuretoggles.e b;
  private int d = 2005;
  private com.truecaller.truepay.app.ui.registration.e.b e;
  
  static
  {
    PreRegistrationActivity.a locala = new com/truecaller/truepay/app/ui/registration/views/activities/PreRegistrationActivity$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private final void n()
  {
    Object localObject = b;
    String str;
    if (localObject == null)
    {
      str = "featuresRegistry";
      c.g.b.k.a(str);
    }
    localObject = ((com.truecaller.featuretoggles.e)localObject).D();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      bool = false;
      setVisible(false);
      localObject = a;
      if (localObject == null)
      {
        str = "presenter";
        c.g.b.k.a(str);
      }
      localObject = e;
      ((io.reactivex.a.a)localObject).a();
      c();
    }
  }
  
  public final void a()
  {
    Intent localIntent = new android/content/Intent;
    Uri localUri = Uri.parse("truecaller://home/tabs/banking");
    localIntent.<init>("android.intent.action.VIEW", localUri);
    startActivity(localIntent);
  }
  
  public final void a(int paramInt, String[] paramArrayOfString)
  {
    c.g.b.k.b(paramArrayOfString, "requiredPermissions");
    android.support.v4.app.a.a((Activity)this, paramArrayOfString, paramInt);
  }
  
  public final void a(SimInfo paramSimInfo, int paramInt)
  {
    c.g.b.k.b(paramSimInfo, "simInfo");
    com.truecaller.truepay.app.ui.registration.d.k localk = a;
    if (localk == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    String str = "simInfo";
    c.g.b.k.b(paramSimInfo, str);
    boolean bool = k;
    if (bool)
    {
      bool = true;
      l = bool;
    }
    Integer localInteger = Integer.valueOf(paramInt);
    h = localInteger;
    paramSimInfo = d;
    g = paramSimInfo;
    paramSimInfo = (com.truecaller.truepay.app.ui.registration.views.c.f)localk.af_();
    if (paramSimInfo != null)
    {
      paramSimInfo.i();
      return;
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.c.o paramo, com.truecaller.truepay.data.d.a parama, Integer paramInteger, boolean paramBoolean)
  {
    c.g.b.k.b(paramo, "responseDO");
    Intent localIntent = new android/content/Intent;
    Object localObject = this;
    localObject = (Context)this;
    localIntent.<init>((Context)localObject, AccountConnectionActivity.class);
    parama = (Serializable)parama;
    localIntent.putExtra("selected_bank", parama);
    paramInteger = (Serializable)paramInteger;
    localIntent.putExtra("selected_sim", paramInteger);
    paramo = (Serializable)paramo;
    localIntent.putExtra("cd_response", paramo);
    localIntent.putExtra("is_using_sms_data", paramBoolean);
    localIntent.putExtra("reg_source", "deeplink");
    int i = d;
    startActivityForResult(localIntent, i);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject = this;
    localObject = (Context)this;
    paramString = (CharSequence)paramString;
    Toast.makeText((Context)localObject, paramString, 1).show();
  }
  
  public final void b()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = x;
    localObject1 = a;
    int i = localObject1.length;
    localObject1 = (String[])Arrays.copyOf((Object[])localObject1, i);
    boolean bool = ((l)localObject2).a((String[])localObject1);
    if (bool)
    {
      localObject1 = a;
      if (localObject1 == null)
      {
        localObject2 = "presenter";
        c.g.b.k.a((String)localObject2);
      }
      localObject2 = Truepay.getInstance();
      if (localObject2 != null)
      {
        localObject2 = ((Truepay)localObject2).getListener();
        if (localObject2 != null)
        {
          Object localObject3 = new com/truecaller/truepay/app/ui/registration/d/k$c;
          ((k.c)localObject3).<init>((com.truecaller.truepay.app.ui.registration.d.k)localObject1);
          localObject3 = (PayTempTokenCallBack)localObject3;
          ((TcPaySDKListener)localObject2).fetchTempToken((PayTempTokenCallBack)localObject3);
          return;
        }
      }
      return;
    }
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = (com.truecaller.truepay.app.ui.registration.views.c.f)((com.truecaller.truepay.app.ui.registration.d.k)localObject1).af_();
    if (localObject2 != null)
    {
      i = j;
      localObject1 = a;
      ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject2).a(i, (String[])localObject1);
      return;
    }
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "tempToken");
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    c.g.b.k.b(paramString, "tempToken");
    Object localObject2 = v;
    boolean bool1 = ((at)localObject2).a();
    if (bool1)
    {
      paramString = (com.truecaller.truepay.app.ui.registration.views.c.f)((com.truecaller.truepay.app.ui.registration.d.k)localObject1).af_();
      if (paramString != null)
      {
        localObject2 = o;
        int i = R.string.intro_root_detected;
        Object[] arrayOfObject = new Object[0];
        localObject2 = ((com.truecaller.utils.n)localObject2).a(i, arrayOfObject);
        str = "resourceProvider.getStri…ring.intro_root_detected)";
        c.g.b.k.a(localObject2, str);
        paramString.a((String)localObject2);
      }
      paramString = (com.truecaller.truepay.app.ui.registration.views.c.f)((com.truecaller.truepay.app.ui.registration.d.k)localObject1).af_();
      if (paramString != null)
      {
        paramString.c();
        return;
      }
      return;
    }
    z.a("");
    y.a("");
    localObject2 = A;
    String str = "";
    ((com.truecaller.truepay.data.e.e)localObject2).a(str);
    s.a();
    localObject2 = new com/truecaller/truepay/data/api/model/ae;
    ((ae)localObject2).<init>();
    ((ae)localObject2).a(paramString);
    paramString = r.a((ae)localObject2);
    localObject2 = io.reactivex.g.a.b();
    paramString = paramString.b((io.reactivex.n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    paramString = paramString.a((io.reactivex.n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/registration/d/k$b;
    ((k.b)localObject2).<init>((com.truecaller.truepay.app.ui.registration.d.k)localObject1);
    localObject2 = (q)localObject2;
    paramString.a((q)localObject2);
    paramString = Truepay.getInstance();
    if (paramString != null)
    {
      paramString = paramString.getAnalyticLoggerHelper();
      if (paramString != null)
      {
        bool1 = k;
        boolean bool2 = l;
        localObject1 = q;
        boolean bool3 = ((h)localObject1).j();
        paramString.a(bool1, bool2, bool3);
      }
    }
    paramString = Truepay.getInstance();
    if (paramString != null)
    {
      paramString = paramString.getAnalyticLoggerHelper();
      if (paramString != null)
      {
        localObject1 = "PayRegistrationStartedDate";
        localObject2 = Truepay.getInstance();
        if (localObject2 != null)
        {
          localObject2 = ((Truepay)localObject2).getAnalyticLoggerHelper();
          if (localObject2 != null)
          {
            localObject2 = ((com.truecaller.truepay.data.b.a)localObject2).b();
            break label330;
          }
        }
        bool1 = false;
        localObject2 = null;
        label330:
        if (localObject2 == null) {
          c.g.b.k.a();
        }
        paramString.a((String)localObject1, localObject2);
        return;
      }
    }
  }
  
  public final void c()
  {
    setResult(0);
    finish();
  }
  
  public final void d()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = q.h();
    boolean bool1 = com.truecaller.multisim.b.b.c((List)localObject2);
    int j;
    if (bool1)
    {
      int i = 5;
    }
    else
    {
      localObject2 = D;
      localObject1 = q.h();
      boolean bool2 = ((com.truecaller.multisim.b.b)localObject2).b((List)localObject1);
      if (bool2)
      {
        j = 6;
      }
      else
      {
        j = 0;
        localObject1 = null;
      }
    }
    switch (j)
    {
    default: 
      break;
    case 6: 
      n();
      break;
    case 5: 
      n();
      return;
    }
  }
  
  public final void e()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localObject = E;
    boolean bool = ((com.truecaller.truepay.app.utils.c)localObject).a();
    int j = 4;
    int i;
    if (bool)
    {
      i = 4;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i == j) {
      n();
    }
  }
  
  public final void f()
  {
    setResult(-1);
    finish();
  }
  
  public final void g()
  {
    com.truecaller.truepay.app.ui.registration.views.b.n localn = new com/truecaller/truepay/app/ui/registration/views/b/n;
    localn.<init>();
    j localj = getSupportFragmentManager();
    String str = com.truecaller.truepay.app.ui.registration.views.b.n.class.getName();
    localn.show(localj, str);
  }
  
  public final int getLayoutId()
  {
    return R.layout.activity_pre_registration;
  }
  
  public final void h()
  {
    Intent localIntent = new android/content/Intent;
    Object localObject = this;
    localObject = (Context)this;
    localIntent.<init>((Context)localObject, BankListActivity.class);
    localIntent.putExtra("bank_selection_context", "registration");
    startActivityForResult(localIntent, 5001);
  }
  
  public final void i()
  {
    com.truecaller.truepay.app.ui.registration.d.k localk = a;
    if (localk == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localk.a();
  }
  
  public final void j()
  {
    n();
  }
  
  public final void k()
  {
    n();
  }
  
  public final void l()
  {
    n();
  }
  
  public final void m()
  {
    com.truecaller.truepay.app.ui.registration.d.k localk = a;
    if (localk == null)
    {
      localObject1 = "presenter";
      c.g.b.k.a((String)localObject1);
    }
    Object localObject1 = u.a();
    localObject1 = t.a((com.truecaller.truepay.app.ui.registration.c.n)localObject1);
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.o)localObject1).b((io.reactivex.n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.o)localObject1).a((io.reactivex.n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/registration/d/k$a;
    ((k.a)localObject2).<init>(localk);
    localObject2 = (q)localObject2;
    ((io.reactivex.o)localObject1).a((q)localObject2);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    com.truecaller.truepay.app.ui.registration.d.k localk = a;
    if (localk == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    int i = 5001;
    int j = -1;
    Object localObject1;
    if (paramInt1 != i)
    {
      switch (paramInt1)
      {
      default: 
        break;
      case 2005: 
        if (paramInt2 == j)
        {
          localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)localk.af_();
          if (localObject1 != null) {
            ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).f();
          }
          return;
        }
        localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)localk.af_();
        if (localObject1 != null) {
          ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).c();
        }
        return;
      case 2004: 
        if (paramInt2 == j)
        {
          localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)localk.af_();
          if (localObject1 != null) {
            ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).a();
          }
          return;
        }
        localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)localk.af_();
        if (localObject1 == null) {
          break;
        }
        ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).c();
        return;
      }
    }
    else if (paramInt2 == j)
    {
      paramInt1 = 0;
      localObject1 = null;
      Object localObject2;
      if (paramIntent != null)
      {
        localObject2 = paramIntent.getSerializableExtra("selected_bank");
      }
      else
      {
        paramInt2 = 0;
        localObject2 = null;
      }
      if (localObject2 != null)
      {
        localObject2 = (com.truecaller.truepay.data.d.a)localObject2;
        c = ((com.truecaller.truepay.data.d.a)localObject2);
        localObject2 = c;
        if (localObject2 != null) {
          localObject1 = ((com.truecaller.truepay.data.d.a)localObject2).b();
        }
        f = ((String)localObject1);
        paramInt1 = k;
        if (paramInt1 != 0)
        {
          paramInt1 = 1;
          l = paramInt1;
        }
        localObject1 = (com.truecaller.truepay.app.ui.registration.views.c.f)localk.af_();
        if (localObject1 != null) {
          ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject1).i();
        }
        return;
      }
      localObject1 = new c/u;
      ((u)localObject1).<init>("null cannot be cast to non-null type com.truecaller.truepay.data.model.Bank");
      throw ((Throwable)localObject1);
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.registration.b.a.a();
    Object localObject1 = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject1).a().a(this);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject1 = "presenter";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = this;
    localObject1 = (com.truecaller.truepay.app.ui.base.views.a)this;
    paramBundle.a((com.truecaller.truepay.app.ui.base.views.a)localObject1);
    paramBundle = getIntent();
    localObject1 = "tcpay_server_error";
    Object localObject2 = null;
    boolean bool1 = paramBundle.getBooleanExtra((String)localObject1, false);
    int i;
    if (bool1) {
      i = 2004;
    } else {
      i = 2005;
    }
    d = i;
    paramBundle = getIntent();
    localObject1 = "from_conversation";
    boolean bool2 = paramBundle.getBooleanExtra((String)localObject1, false);
    int j = 2;
    if (!bool2)
    {
      paramBundle = getIntent();
      localObject3 = "from_wizard";
      bool2 = paramBundle.getBooleanExtra((String)localObject3, false);
      if (!bool2)
      {
        paramBundle = b;
        if (paramBundle == null)
        {
          localObject2 = "featuresRegistry";
          c.g.b.k.a((String)localObject2);
        }
        paramBundle = paramBundle.p();
        bool2 = paramBundle.a();
        if (bool2)
        {
          paramBundle = a;
          if (paramBundle == null)
          {
            localObject2 = "presenter";
            c.g.b.k.a((String)localObject2);
          }
          localObject2 = (ag)bg.a;
          localObject3 = m;
          localObject4 = new com/truecaller/truepay/app/ui/registration/d/k$d;
          ((k.d)localObject4).<init>(paramBundle, null);
          localObject4 = (m)localObject4;
          localObject1 = kotlinx.coroutines.e.b((ag)localObject2, (c.d.f)localObject3, (m)localObject4, j);
          b = ((bn)localObject1);
          break label392;
        }
        paramBundle = a;
        if (paramBundle == null)
        {
          localObject1 = "presenter";
          c.g.b.k.a((String)localObject1);
        }
        paramBundle.a();
        break label392;
      }
    }
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = getIntent().getStringExtra("bank_symbol");
    Object localObject3 = getIntent();
    Object localObject4 = "sim_slot";
    int k = -1;
    int m = ((Intent)localObject3).getIntExtra((String)localObject4, k);
    if (m != k)
    {
      localObject3 = Integer.valueOf(m);
      h = ((Integer)localObject3);
    }
    localObject3 = (ag)bg.a;
    localObject4 = n;
    Object localObject5 = new com/truecaller/truepay/app/ui/registration/d/k$e;
    ((k.e)localObject5).<init>(paramBundle, (String)localObject2, null);
    localObject5 = (m)localObject5;
    kotlinx.coroutines.e.b((ag)localObject3, (c.d.f)localObject4, (m)localObject5, j);
    label392:
    paramBundle = com.truecaller.truepay.app.ui.registration.e.b.c;
    paramBundle = this;
    paramBundle = (com.truecaller.truepay.app.ui.registration.e.a)this;
    localObject1 = b.a.a(paramBundle);
    e = ((com.truecaller.truepay.app.ui.registration.e.b)localObject1);
    localObject1 = e;
    if (localObject1 == null) {
      c.g.b.k.a();
    }
    ((com.truecaller.truepay.app.ui.registration.e.b)localObject1).a(paramBundle);
    paramBundle = e;
    if (paramBundle == null) {
      c.g.b.k.a();
    }
    bool2 = b;
    if (!bool2)
    {
      paramBundle = e;
      if (paramBundle != null)
      {
        localObject1 = this;
        localObject1 = (Context)this;
        paramBundle.a((Context)localObject1);
        return;
      }
    }
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    com.truecaller.truepay.app.ui.registration.e.b localb = e;
    if (localb == null) {
      c.g.b.k.a();
    }
    boolean bool = b;
    if (bool)
    {
      localb = e;
      if (localb != null)
      {
        Object localObject = this;
        localObject = (Context)this;
        localb.b((Context)localObject);
        return;
      }
    }
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    c.g.b.k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    c.g.b.k.b(paramArrayOfString, "permissions");
    Object localObject2 = "grantResults";
    c.g.b.k.b(paramArrayOfInt, (String)localObject2);
    int i = j;
    if (paramInt == i)
    {
      Object localObject3 = x;
      localObject2 = a;
      int j = localObject2.length;
      localObject2 = (String[])Arrays.copyOf((Object[])localObject2, j);
      paramInt = ((l)localObject3).a(paramArrayOfString, paramArrayOfInt, (String[])localObject2);
      if (paramInt != 0)
      {
        localObject3 = (com.truecaller.truepay.app.ui.registration.views.c.f)((com.truecaller.truepay.app.ui.registration.d.k)localObject1).af_();
        if (localObject3 != null) {
          ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject3).b();
        }
        return;
      }
      localObject3 = (com.truecaller.truepay.app.ui.registration.views.c.f)((com.truecaller.truepay.app.ui.registration.d.k)localObject1).af_();
      if (localObject3 != null)
      {
        paramArrayOfString = o;
        int k = R.string.sms_permission_denied;
        localObject1 = new Object[0];
        paramArrayOfString = paramArrayOfString.a(k, (Object[])localObject1);
        c.g.b.k.a(paramArrayOfString, "resourceProvider.getStri…ng.sms_permission_denied)");
        ((com.truecaller.truepay.app.ui.registration.views.c.f)localObject3).a(paramArrayOfString);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.activities.PreRegistrationActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */