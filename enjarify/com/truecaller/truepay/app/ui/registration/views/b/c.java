package com.truecaller.truepay.app.ui.registration.views.b;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.e;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemAnimator;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.menu;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.app.ui.bankList.a.b;
import java.util.ArrayList;
import java.util.List;

public class c
  extends e
  implements com.truecaller.truepay.app.ui.registration.views.c.a
{
  RecyclerView a;
  Toolbar b;
  EditText c;
  a.b d;
  Context e;
  public Menu f;
  com.truecaller.truepay.app.ui.registration.views.a.a g;
  List h;
  
  public static c a(ArrayList paramArrayList)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("banks", paramArrayList);
    paramArrayList = new com/truecaller/truepay/app/ui/registration/views/b/c;
    paramArrayList.<init>();
    paramArrayList.setArguments(localBundle);
    return paramArrayList;
  }
  
  public final void a(com.truecaller.truepay.data.d.a parama)
  {
    a.b localb = d;
    if (localb != null) {
      localb.a(parama);
    }
    dismiss();
  }
  
  public void onAttach(Context paramContext)
  {
    e = paramContext;
    Object localObject = getActivity();
    boolean bool = localObject instanceof a.b;
    if (bool)
    {
      localObject = (a.b)getActivity();
      d = ((a.b)localObject);
      super.onAttach(paramContext);
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.getClass();
    localStringBuilder.append(paramContext);
    localStringBuilder.append("should implement the OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getActivity().getLayoutInflater();
    int i = R.layout.fragment_bank_search;
    paramBundle = paramBundle.inflate(i, null);
    i = R.id.rv_bank_search_list;
    Object localObject = (RecyclerView)paramBundle.findViewById(i);
    a = ((RecyclerView)localObject);
    i = R.id.toolbar;
    localObject = (Toolbar)paramBundle.findViewById(i);
    b = ((Toolbar)localObject);
    i = R.id.et_search_view;
    localObject = (EditText)paramBundle.findViewById(i);
    c = ((EditText)localObject);
    localObject = new android/app/AlertDialog$Builder;
    ContextThemeWrapper localContextThemeWrapper = new android/view/ContextThemeWrapper;
    f localf = getActivity();
    int j = R.style.popup_theme;
    localContextThemeWrapper.<init>(localf, j);
    ((AlertDialog.Builder)localObject).<init>(localContextThemeWrapper);
    localObject = ((AlertDialog.Builder)localObject).create();
    ((AlertDialog)localObject).setCancelable(false);
    ((AlertDialog)localObject).setCanceledOnTouchOutside(false);
    ((AlertDialog)localObject).getWindow().setSoftInputMode(5);
    ((AlertDialog)localObject).setView(paramBundle);
    return (Dialog)localObject;
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    int i = R.menu.menu_frag_search_banks;
    paramMenuInflater.inflate(i, paramMenu);
    i = R.id.action_search;
    MenuItem localMenuItem = paramMenu.findItem(i);
    if (localMenuItem != null) {
      localMenuItem.setVisible(false);
    }
    f = paramMenu;
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = 1;
    setHasOptionsMenu(i);
    Object localObject1 = (AppCompatActivity)getActivity();
    Object localObject2 = b;
    ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
    ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(i);
    localObject1 = b;
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/c$1;
    ((c.1)localObject2).<init>(this);
    ((Toolbar)localObject1).setNavigationOnClickListener((View.OnClickListener)localObject2);
    c.requestFocus();
    localObject1 = new android/support/v7/widget/LinearLayoutManager;
    localObject2 = e;
    ((LinearLayoutManager)localObject1).<init>((Context)localObject2);
    a.setLayoutManager((RecyclerView.LayoutManager)localObject1);
    localObject1 = a;
    localObject2 = new android/support/v7/widget/DefaultItemAnimator;
    ((DefaultItemAnimator)localObject2).<init>();
    ((RecyclerView)localObject1).setItemAnimator((RecyclerView.ItemAnimator)localObject2);
    localObject1 = a;
    localObject2 = new android/support/v7/widget/DividerItemDecoration;
    Context localContext = getContext();
    ((DividerItemDecoration)localObject2).<init>(localContext, i);
    ((RecyclerView)localObject1).addItemDecoration((RecyclerView.ItemDecoration)localObject2);
    a.setHasFixedSize(i);
    Object localObject3 = (ArrayList)getArguments().getSerializable("banks");
    h = ((List)localObject3);
    localObject3 = new com/truecaller/truepay/app/ui/registration/views/a/a;
    localObject1 = h;
    ((com.truecaller.truepay.app.ui.registration.views.a.a)localObject3).<init>((List)localObject1, this);
    g = ((com.truecaller.truepay.app.ui.registration.views.a.a)localObject3);
    localObject3 = a;
    localObject1 = g;
    ((RecyclerView)localObject3).setAdapter((RecyclerView.Adapter)localObject1);
    localObject3 = c;
    localObject1 = new com/truecaller/truepay/app/ui/registration/views/b/c$2;
    ((c.2)localObject1).<init>(this);
    ((EditText)localObject3).addTextChangedListener((TextWatcher)localObject1);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public void onDetach()
  {
    e = null;
    d = null;
    super.onDetach();
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    c.setText("");
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public void onStart()
  {
    super.onStart();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      localWindow.setBackgroundDrawableResource(17170445);
      int i = -1;
      localWindow.setLayout(i, i);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */