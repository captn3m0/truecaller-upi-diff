package com.truecaller.truepay.app.ui.registration.views.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.b.a.a;
import com.truecaller.truepay.app.ui.registration.d.p;
import com.truecaller.truepay.app.ui.registration.views.c.i;
import java.util.HashMap;

public final class m
  extends android.support.design.widget.b
  implements i
{
  public static final m.a b;
  public p a;
  private int c;
  private HashMap d;
  
  static
  {
    m.a locala = new com/truecaller/truepay/app/ui/registration/views/b/m$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    int i = R.id.imageViewIcon;
    Object localObject1 = (ImageView)a(i);
    Object localObject2 = getContext();
    if (localObject2 == null) {
      k.a();
    }
    int j = R.drawable.ic_flight_mode;
    localObject2 = android.support.v4.content.b.a((Context)localObject2, j);
    ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
    i = R.id.textViewHeading;
    localObject1 = (TextView)a(i);
    k.a(localObject1, "textViewHeading");
    int k = R.string.aeroplane_mode_turned_on;
    localObject2 = (CharSequence)getString(k);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    i = R.id.textViewContent;
    localObject1 = (TextView)a(i);
    k.a(localObject1, "textViewContent");
    k = R.string.aeroplane_mode_turned_on_content;
    localObject2 = (CharSequence)getString(k);
    ((TextView)localObject1).setText((CharSequence)localObject2);
  }
  
  public final void b()
  {
    int i = R.id.imageViewIcon;
    Object localObject1 = (ImageView)a(i);
    Object localObject2 = getContext();
    if (localObject2 == null) {
      k.a();
    }
    int j = R.drawable.ic_insert_sim;
    localObject2 = android.support.v4.content.b.a((Context)localObject2, j);
    ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
    i = R.id.textViewHeading;
    localObject1 = (TextView)a(i);
    k.a(localObject1, "textViewHeading");
    int k = R.string.insert_sim_to_continue_title;
    localObject2 = (CharSequence)getString(k);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    i = R.id.textViewContent;
    localObject1 = (TextView)a(i);
    k.a(localObject1, "textViewContent");
    k = R.string.insert_sim_to_continue_content;
    localObject2 = (CharSequence)getString(k);
    ((TextView)localObject1).setText((CharSequence)localObject2);
  }
  
  public final void c()
  {
    int i = R.id.imageViewIcon;
    Object localObject1 = (ImageView)a(i);
    Object localObject2 = getContext();
    if (localObject2 == null) {
      k.a();
    }
    int j = R.drawable.ic_sim_deactivated;
    localObject2 = android.support.v4.content.b.a((Context)localObject2, j);
    ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
    i = R.id.textViewHeading;
    localObject1 = (TextView)a(i);
    k.a(localObject1, "textViewHeading");
    int k = R.string.deactivated_sim_card;
    localObject2 = (CharSequence)getString(k);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    i = R.id.textViewContent;
    localObject1 = (TextView)a(i);
    k.a(localObject1, "textViewContent");
    k = R.string.deactivated_sim_card_content;
    localObject2 = (CharSequence)getString(k);
    ((TextView)localObject1).setText((CharSequence)localObject2);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.registration.b.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.layout_sim_aeroplane_notification;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = getArguments();
    if (paramView == null) {
      k.a();
    }
    paramBundle = "notification_type";
    int i = paramView.getInt(paramBundle);
    c = i;
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = this;
    paramBundle = (com.truecaller.truepay.app.ui.base.views.a)this;
    paramView.a(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    int j = c;
    paramView = (i)paramView.af_();
    if (paramView == null) {
      return;
    }
    switch (j)
    {
    default: 
      break;
    case 6: 
      paramView.c();
      break;
    case 5: 
      paramView.b();
      return;
    case 4: 
      paramView.a();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */