package com.truecaller.truepay.app.ui.registration.views.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;

public final class a$l
  extends BroadcastReceiver
{
  a$l(a parama) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    k.b(paramIntent, "intent");
    paramContext = a.b().D();
    boolean bool1 = paramContext.a();
    Object localObject = a.b().E();
    boolean bool2 = ((b)localObject).a();
    int j = getResultCode();
    boolean bool3 = true;
    int k = -1;
    int i;
    String str;
    if (j != k)
    {
      paramContext = a.a();
      i = 3;
      paramContext.b(i);
      paramContext = paramIntent.getAction();
      if (paramContext != null)
      {
        int m = paramContext.hashCode();
        i = -1000163555;
        if (m != i)
        {
          i = 2075318557;
          if (m == i)
          {
            paramIntent = "SMS_DELIVERED_INTENT";
            bool1 = paramContext.equals(paramIntent);
            if (bool1)
            {
              paramContext = a;
              paramIntent = "failure";
              localObject = "sim_verification_sms_delivered";
              a.a(paramContext, paramIntent, (String)localObject, bool3);
            }
          }
        }
        else
        {
          paramIntent = "SMS_SENT_INTENT";
          bool1 = paramContext.equals(paramIntent);
          if (bool1)
          {
            paramContext = a;
            paramIntent = "failure";
            localObject = "sim_verification_sms_sent";
            j = 0;
            str = null;
            a.a(paramContext, paramIntent, (String)localObject, false);
          }
        }
      }
    }
    else
    {
      new String[1][0] = "Sms sent or delivered";
      paramIntent = paramIntent.getAction();
      str = "SMS_DELIVERED_INTENT";
      boolean bool4 = k.a(paramIntent, str);
      if ((!bool4) && (i != 0)) {
        return;
      }
      a.a().a(bool1);
      paramContext = a.a();
      int n = a.c(a);
      paramContext.a(n);
      paramContext = a;
      paramIntent = "initiated";
      localObject = "sim_verification_started";
      a.a(paramContext, paramIntent, (String)localObject, bool3);
    }
    paramContext = a;
    a.a(paramContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.a.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */