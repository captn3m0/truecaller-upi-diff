package com.truecaller.truepay.app.ui.registration.views.b;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v4.app.e;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.b.a.a;
import com.truecaller.truepay.app.ui.registration.d.d;
import com.truecaller.truepay.app.ui.registration.views.c.c;
import com.truecaller.truepay.app.utils.au;
import java.util.ArrayList;
import java.util.List;

public class f
  extends e
  implements e.a, c
{
  RecyclerView a;
  TextView b;
  Group c;
  Context d;
  f.a e;
  com.truecaller.truepay.app.ui.registration.views.a.b f;
  ArrayList g;
  public d h;
  
  public static f a(ArrayList paramArrayList)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("accounts", paramArrayList);
    paramArrayList = new com/truecaller/truepay/app/ui/registration/views/b/f;
    paramArrayList.<init>();
    paramArrayList.setArguments(localBundle);
    return paramArrayList;
  }
  
  public final void a()
  {
    f.a locala = e;
    if (locala != null) {
      locala.b();
    }
    dismiss();
  }
  
  public final void a(f.a parama)
  {
    e = parama;
  }
  
  public final void a(String paramString)
  {
    f.a locala = e;
    if (locala != null) {
      locala.a(paramString);
    }
  }
  
  public final void b()
  {
    new String[1][0] = "onLoadClFailure";
  }
  
  public final void b(String paramString)
  {
    f.a locala = e;
    if (locala != null) {
      locala.a(paramString);
    }
  }
  
  public final void c()
  {
    f.a locala = e;
    if (locala != null) {
      locala.a();
    }
  }
  
  public void dismiss()
  {
    super.dismiss();
  }
  
  public void onAttach(Context paramContext)
  {
    d = paramContext;
    super.onAttach(paramContext);
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getActivity().getLayoutInflater();
    int i = R.layout.fragment_device_change_account_chooser;
    paramBundle = paramBundle.inflate(i, null);
    i = R.id.rv_acc_list;
    Object localObject1 = (RecyclerView)paramBundle.findViewById(i);
    a = ((RecyclerView)localObject1);
    i = R.id.tv_name_frag_change_device;
    localObject1 = (TextView)paramBundle.findViewById(i);
    b = ((TextView)localObject1);
    i = R.id.group_reg_v2_additions;
    localObject1 = (Group)paramBundle.findViewById(i);
    c = ((Group)localObject1);
    i = R.id.tv_new_user_frag_change_device;
    localObject1 = paramBundle.findViewById(i);
    Object localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$f$jxTxP9ANPYZTgnx9lc2LSrOifvM;
    ((-..Lambda.f.jxTxP9ANPYZTgnx9lc2LSrOifvM)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    i = R.id.btn_enter_pin_frag_change_device;
    localObject1 = paramBundle.findViewById(i);
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$f$mJbliYKvCuW9glbahufP3Zs5b1A;
    ((-..Lambda.f.mJbliYKvCuW9glbahufP3Zs5b1A)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    i = R.id.tv_forgot_pin_frag_change_device;
    localObject1 = paramBundle.findViewById(i);
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$f$zLrSHvXgZCaWSvuJyt-3Q3ZwCVw;
    ((-..Lambda.f.zLrSHvXgZCaWSvuJyt-3Q3ZwCVw)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = new android/app/AlertDialog$Builder;
    localObject2 = new android/view/ContextThemeWrapper;
    android.support.v4.app.f localf = getActivity();
    int j = R.style.popup_theme;
    ((ContextThemeWrapper)localObject2).<init>(localf, j);
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).create();
    localObject2 = ((AlertDialog)localObject1).getWindow();
    if (localObject2 != null)
    {
      localObject2 = ((AlertDialog)localObject1).getWindow();
      int k = 80;
      ((Window)localObject2).setGravity(k);
    }
    ((AlertDialog)localObject1).setCancelable(false);
    ((AlertDialog)localObject1).setCanceledOnTouchOutside(false);
    ((AlertDialog)localObject1).setView(paramBundle);
    return (Dialog)localObject1;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Object localObject1 = com.truecaller.truepay.app.ui.registration.b.a.a();
    Object localObject2 = Truepay.getApplicationComponent();
    ((a.a)localObject1).a((com.truecaller.truepay.app.a.a.a)localObject2).a().a(this);
    h.a(this);
    localObject1 = a;
    boolean bool = true;
    ((RecyclerView)localObject1).setHasFixedSize(bool);
    localObject1 = a;
    LinearLayoutManager localLinearLayoutManager = new android/support/v7/widget/LinearLayoutManager;
    Object localObject3 = getContext();
    localLinearLayoutManager.<init>((Context)localObject3);
    ((RecyclerView)localObject1).setLayoutManager(localLinearLayoutManager);
    localObject1 = (ArrayList)getArguments().getSerializable("accounts");
    g = ((ArrayList)localObject1);
    localObject1 = g;
    localLinearLayoutManager = null;
    if (localObject1 != null)
    {
      int i = ((ArrayList)localObject1).size();
      if (i > 0)
      {
        localObject1 = (com.truecaller.truepay.data.api.model.a)g.get(0);
        m = bool;
      }
    }
    localObject1 = new com/truecaller/truepay/app/ui/registration/views/a/b;
    localObject3 = g;
    ((com.truecaller.truepay.app.ui.registration.views.a.b)localObject1).<init>((List)localObject3);
    f = ((com.truecaller.truepay.app.ui.registration.views.a.b)localObject1);
    localObject1 = a;
    localObject3 = f;
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject3);
    c.setVisibility(0);
    localObject1 = b;
    int j = R.string.welcome_back_with_name;
    Object[] arrayOfObject = new Object[bool];
    Object localObject4 = g;
    int k = ((ArrayList)localObject4).size();
    if (k > 0) {
      localObject4 = g.get(0)).b;
    } else {
      localObject4 = "";
    }
    localObject2 = au.a((String)localObject4, bool);
    arrayOfObject[0] = localObject2;
    localObject2 = getString(j, arrayOfObject);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    d = null;
    e = null;
  }
  
  public void onStart()
  {
    super.onStart();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      localWindow.setBackgroundDrawableResource(17170445);
      int i = -1;
      int j = -2;
      localWindow.setLayout(i, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */