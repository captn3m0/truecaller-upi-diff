package com.truecaller.truepay.app.ui.registration.views.b;

import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.truecaller.multisim.h;

final class a$e
  implements View.OnClickListener
{
  a$e(a parama) {}
  
  public final void onClick(View paramView)
  {
    paramView = a.c();
    boolean bool = paramView.j();
    Object localObject;
    String str1;
    String str2;
    String str3;
    if (bool)
    {
      paramView = a;
      bool = a.e(paramView);
      if (!bool)
      {
        paramView = a.f(a);
        if (paramView != null)
        {
          localObject = a;
          str1 = "failure";
          str2 = "select_another_bank";
          paramView = paramView.b();
          str3 = "it.bankName";
          k.a(paramView, str3);
          a.a((a)localObject, str1, str2, paramView);
        }
        a.a(a, "retry");
        return;
      }
    }
    paramView = a.f(a);
    if (paramView != null)
    {
      localObject = a;
      str1 = "failure";
      str2 = "retry";
      paramView = paramView.b();
      str3 = "it.bankName";
      k.a(paramView, str3);
      a.a((a)localObject, str1, str2, paramView);
    }
    paramView = a.a();
    int i = 4;
    paramView.b(i);
    paramView = a.f(a);
    if (paramView != null)
    {
      localObject = a.a();
      str1 = paramView.c();
      k.a(str1, "it.accountProviderId");
      paramView = paramView.a();
      k.a(paramView, "it.id");
      ((b.a)localObject).a(str1, paramView);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */