package com.truecaller.truepay.app.ui.registration.views.a;

import android.support.v7.widget.RecyclerView.Adapter;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.b.a.a;
import com.truecaller.truepay.app.ui.registration.b.b;
import com.truecaller.truepay.app.utils.r;
import java.util.ArrayList;
import java.util.List;

public final class a
  extends RecyclerView.Adapter
{
  public final List a;
  public final List b;
  com.truecaller.truepay.app.ui.registration.views.c.a c;
  public r d;
  
  public a(List paramList, com.truecaller.truepay.app.ui.registration.views.c.a parama)
  {
    a = paramList;
    paramList = new java/util/ArrayList;
    paramList.<init>();
    b = paramList;
    c = parama;
    paramList = com.truecaller.truepay.app.ui.registration.b.a.a();
    parama = Truepay.getApplicationComponent();
    paramList.a(parama).a().a(this);
  }
  
  public final int getItemCount()
  {
    return b.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */