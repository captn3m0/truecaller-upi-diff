package com.truecaller.truepay.app.ui.registration.views.b;

import android.os.CountDownTimer;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public final class a$k
  extends CountDownTimer
{
  a$k(a parama, long paramLong1, long paramLong2)
  {
    super(paramLong2, 1000L);
  }
  
  public final void onFinish()
  {
    a.a().b(2);
    a.a(a, "failure", "sim_verification_started", true);
    a.a(a);
    a.b(a);
  }
  
  public final void onTick(long paramLong)
  {
    Object localObject1 = a;
    int i = R.id.tvTimerVerification;
    localObject1 = (TextView)((a)localObject1).b(i);
    if (localObject1 != null)
    {
      int j = 2;
      Object[] arrayOfObject = new Object[j];
      Long localLong = Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(paramLong));
      arrayOfObject[0] = localLong;
      long l = TimeUnit.MILLISECONDS.toSeconds(paramLong);
      TimeUnit localTimeUnit = TimeUnit.MINUTES;
      paramLong = TimeUnit.MILLISECONDS.toMinutes(paramLong);
      paramLong = localTimeUnit.toSeconds(paramLong);
      Object localObject2 = Long.valueOf(l - paramLong);
      arrayOfObject[1] = localObject2;
      localObject2 = Arrays.copyOf(arrayOfObject, j);
      localObject2 = String.format("%02d:%02d", (Object[])localObject2);
      k.a(localObject2, "java.lang.String.format(format, *args)");
      localObject2 = (CharSequence)localObject2;
      ((TextView)localObject1).setText((CharSequence)localObject2);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */