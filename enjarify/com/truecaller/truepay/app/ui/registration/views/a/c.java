package com.truecaller.truepay.app.ui.registration.views.a;

import android.support.v7.widget.RecyclerView.Adapter;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.b.a.a;
import com.truecaller.truepay.app.ui.registration.b.b;
import com.truecaller.truepay.app.utils.r;
import java.util.List;

public final class c
  extends RecyclerView.Adapter
{
  public final List a;
  public int b;
  public r c;
  private final com.truecaller.truepay.data.d.a d;
  
  public c(List paramList, com.truecaller.truepay.data.d.a parama)
  {
    a = paramList;
    d = parama;
    paramList = com.truecaller.truepay.app.ui.registration.b.a.a();
    parama = Truepay.getApplicationComponent();
    paramList.a(parama).a().a(this);
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */