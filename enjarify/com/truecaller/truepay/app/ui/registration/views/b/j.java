package com.truecaller.truepay.app.ui.registration.views.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import c.x;
import com.truecaller.log.d;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.base.widgets.PinEntryEditText;
import com.truecaller.truepay.app.ui.registration.b.a.a;
import com.truecaller.truepay.app.ui.registration.d.m;
import com.truecaller.truepay.app.ui.registration.views.c.h;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;

public class j
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements h
{
  public TextView a;
  public PinEntryEditText b;
  public PinEntryEditText c;
  public PinEntryEditText d;
  public TextView e;
  public TextView f;
  public ImageView g;
  public TextInputLayout h;
  Toolbar i;
  public m j;
  public com.truecaller.truepay.data.e.e k;
  j.a l;
  private com.truecaller.truepay.data.api.model.a n;
  
  public static j a(com.truecaller.truepay.data.api.model.a parama)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("connected_account", parama);
    parama = new com/truecaller/truepay/app/ui/registration/views/b/j;
    parama.<init>();
    parama.setArguments(localBundle);
    return parama;
  }
  
  private void a(android.support.v4.app.e parame)
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      boolean bool = isAdded();
      if (bool) {
        try
        {
          localObject1 = getActivity();
          localObject1 = ((f)localObject1).getSupportFragmentManager();
          Object localObject2 = parame.getClass();
          localObject2 = ((Class)localObject2).getSimpleName();
          parame.show((android.support.v4.app.j)localObject1, (String)localObject2);
          return;
        }
        catch (Exception parame)
        {
          d.a(parame);
        }
      }
    }
  }
  
  private static boolean b(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      int i1 = paramString.length();
      int m = 2;
      if (i1 == m) {
        return true;
      }
    }
    return false;
  }
  
  private static boolean c(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      int m = paramString.length();
      int i1 = 2;
      if (m == i1)
      {
        m = Integer.parseInt(paramString);
        if (m > 0)
        {
          int i2 = Integer.parseInt(paramString);
          m = 12;
          if (i2 <= m) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  private void d()
  {
    String str1 = b.getText().toString();
    Object localObject = c.getText().toString();
    String str2 = d.getText().toString();
    boolean bool1 = d(str1);
    boolean bool2 = c((String)localObject);
    boolean bool3 = b(str2);
    if ((bool1) && (bool2) && (bool3))
    {
      m localm = j;
      String str3 = n.a;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append((String)localObject);
      localStringBuilder.append(str2);
      localObject = localStringBuilder.toString();
      localm.a(str3, str1, (String)localObject);
      return;
    }
    str1 = null;
    if (!bool1)
    {
      e.setVisibility(0);
      return;
    }
    if ((!bool2) || (!bool3))
    {
      localObject = f;
      ((TextView)localObject).setVisibility(0);
    }
  }
  
  private static boolean d(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      int i1 = paramString.length();
      int m = 6;
      if (i1 == m) {
        return true;
      }
    }
    return false;
  }
  
  private void e()
  {
    PinEntryEditText localPinEntryEditText = b;
    int m = 2;
    t.a(localPinEntryEditText, false, m);
    t.a(c, false, m);
    t.a(d, false, m);
    l.b();
  }
  
  public final int a()
  {
    return R.layout.fragment_set_upi_pin;
  }
  
  x a(Editable paramEditable)
  {
    TextView localTextView = e;
    localTextView.setVisibility(8);
    int m = paramEditable.length();
    int i1 = 6;
    if (m == i1)
    {
      paramEditable = c;
      paramEditable.requestFocus();
    }
    else
    {
      int i2 = paramEditable.length();
      if (i2 == 0)
      {
        paramEditable = b;
        paramEditable.requestFocus();
      }
    }
    return x.a;
  }
  
  x a(boolean paramBoolean)
  {
    Object localObject1;
    Object localObject2;
    int i1;
    if (paramBoolean)
    {
      h.setHint("");
      localObject1 = g;
      localObject2 = getActivity();
      int m = R.drawable.ic_six_digits;
      localObject2 = android.support.v4.content.b.a((Context)localObject2, m);
      ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
      localObject1 = e;
      i1 = 8;
      ((TextView)localObject1).setVisibility(i1);
    }
    else
    {
      localObject1 = b.getText().toString();
      paramBoolean = d((String)localObject1);
      if (!paramBoolean)
      {
        localObject1 = e;
        i1 = 0;
        localObject2 = null;
        ((TextView)localObject1).setVisibility(0);
      }
    }
    return x.a;
  }
  
  public final void a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      int m = R.string.set_pin_error_body;
      paramString = getString(m);
    }
    paramString = i.a(n, paramString);
    a(paramString);
  }
  
  x b(Editable paramEditable)
  {
    TextView localTextView = f;
    localTextView.setVisibility(8);
    int m = paramEditable.length();
    int i1 = 2;
    if (m == i1)
    {
      paramEditable = d;
      paramEditable.requestFocus();
    }
    else
    {
      int i2 = paramEditable.length();
      if (i2 == 0)
      {
        paramEditable = b;
        paramEditable.requestFocus();
      }
    }
    return x.a;
  }
  
  x b(boolean paramBoolean)
  {
    Object localObject1;
    Object localObject2;
    int i1;
    if (paramBoolean)
    {
      localObject1 = g;
      localObject2 = getActivity();
      int m = R.drawable.ic_valid_upto;
      localObject2 = android.support.v4.content.b.a((Context)localObject2, m);
      ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
      localObject1 = f;
      i1 = 8;
      ((TextView)localObject1).setVisibility(i1);
    }
    else
    {
      localObject1 = c.getText().toString();
      paramBoolean = c((String)localObject1);
      if (!paramBoolean)
      {
        localObject1 = f;
        i1 = 0;
        localObject2 = null;
        ((TextView)localObject1).setVisibility(0);
      }
    }
    return x.a;
  }
  
  public final void b()
  {
    Object localObject1 = (com.truecaller.truepay.data.api.model.a)getArguments().getSerializable("connected_account");
    n = ((com.truecaller.truepay.data.api.model.a)localObject1);
    localObject1 = i;
    Object localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$j$QJHp8eNJHp1llp81cKSre6bsjhE;
    ((-..Lambda.j.QJHp8eNJHp1llp81cKSre6bsjhE)localObject2).<init>(this);
    ((Toolbar)localObject1).setNavigationOnClickListener((View.OnClickListener)localObject2);
    localObject1 = "account_add_success";
    boolean bool = com.truecaller.truepay.app.ui.registration.a.d;
    if (bool) {
      localObject1 = "manage_account";
    }
    localObject2 = k;
    String str = com.truecaller.truepay.data.b.a.a();
    ((com.truecaller.truepay.data.e.e)localObject2).a(str);
    Truepay.getInstance().getAnalyticLoggerHelper().a("app_payment_set_pin", "initiated", (String)localObject1, "set_pin");
  }
  
  x c(Editable paramEditable)
  {
    TextView localTextView = f;
    int m = 8;
    localTextView.setVisibility(m);
    int i1 = paramEditable.length();
    if (i1 == 0)
    {
      paramEditable = c;
      paramEditable.requestFocus();
    }
    return x.a;
  }
  
  x c(boolean paramBoolean)
  {
    Object localObject1;
    Object localObject2;
    int i1;
    if (paramBoolean)
    {
      localObject1 = g;
      localObject2 = getActivity();
      int m = R.drawable.ic_valid_upto;
      localObject2 = android.support.v4.content.b.a((Context)localObject2, m);
      ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
      localObject1 = f;
      i1 = 8;
      ((TextView)localObject1).setVisibility(i1);
    }
    else
    {
      localObject1 = d.getText().toString();
      paramBoolean = b((String)localObject1);
      if (!paramBoolean)
      {
        localObject1 = f;
        i1 = 0;
        localObject2 = null;
        ((TextView)localObject1).setVisibility(0);
      }
    }
    return x.a;
  }
  
  public final void c()
  {
    Object localObject = "account_add_success";
    boolean bool = com.truecaller.truepay.app.ui.registration.a.c;
    if (bool) {
      localObject = "retry_set_pin";
    }
    bool = com.truecaller.truepay.app.ui.registration.a.d;
    if (bool) {
      localObject = "manage_account";
    }
    Truepay.getInstance().getAnalyticLoggerHelper().a("app_payment_set_pin", "success", (String)localObject, "");
    localObject = l.a();
    a((android.support.v4.app.e)localObject);
  }
  
  public final void d(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      l.c();
      return;
    }
    l.d();
  }
  
  public final void e(boolean paramBoolean)
  {
    a.setEnabled(paramBoolean);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject = getActivity();
    boolean bool = localObject instanceof j.a;
    if (bool)
    {
      paramContext = (j.a)getActivity();
      l = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.getClass();
    localStringBuilder.append(paramContext);
    localStringBuilder.append("should implement the OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.registration.b.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    int m = R.id.action_search;
    MenuItem localMenuItem = paramMenu.findItem(m);
    if (localMenuItem != null) {
      localMenuItem.setVisible(false);
    }
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    setHasOptionsMenu(true);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    j.b();
  }
  
  public void onDetach()
  {
    super.onDetach();
    l = null;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int m = paramMenuItem.getItemId();
    int i1 = 16908332;
    if (m == i1) {
      e();
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int m = 112;
    if (paramInt == m) {
      d();
    }
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.toolbar;
    Object localObject1 = (Toolbar)paramView.findViewById(m);
    i = ((Toolbar)localObject1);
    m = R.id.generate_mpin;
    localObject1 = (TextView)paramView.findViewById(m);
    a = ((TextView)localObject1);
    m = R.id.et_dc_last_6_digits_frag_upi_mpin;
    localObject1 = (PinEntryEditText)paramView.findViewById(m);
    b = ((PinEntryEditText)localObject1);
    m = R.id.et_validity_month;
    localObject1 = (PinEntryEditText)paramView.findViewById(m);
    c = ((PinEntryEditText)localObject1);
    m = R.id.et_validity_year;
    localObject1 = (PinEntryEditText)paramView.findViewById(m);
    d = ((PinEntryEditText)localObject1);
    m = R.id.tv_error_last_6_digits;
    localObject1 = (TextView)paramView.findViewById(m);
    e = ((TextView)localObject1);
    m = R.id.tv_error_expiry_frag_upi_mpin;
    localObject1 = (TextView)paramView.findViewById(m);
    f = ((TextView)localObject1);
    m = R.id.iv_digits_expiry_helper_frag_upi_mpin;
    localObject1 = (ImageView)paramView.findViewById(m);
    g = ((ImageView)localObject1);
    m = R.id.til_dc_last_6_digits_frag_upi_mpin;
    localObject1 = (TextInputLayout)paramView.findViewById(m);
    h = ((TextInputLayout)localObject1);
    localObject1 = a;
    Object localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$j$MDr-NX0wV4oZin7PHWbi-Azt92E;
    ((-..Lambda.j.MDr-NX0wV4oZin7PHWbi-Azt92E)localObject2).<init>(this);
    ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    m = R.id.tv_set_default_expiry;
    localObject1 = paramView.findViewById(m);
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$j$A4VLvjAdTlPRkouqv3hXSgOB8vE;
    ((-..Lambda.j.A4VLvjAdTlPRkouqv3hXSgOB8vE)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = b;
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$pB_PKvFYSCYIJFMyeA6cILHsdN8;
    ((-..Lambda.pB_PKvFYSCYIJFMyeA6cILHsdN8)localObject2).<init>(this);
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    localObject1 = c;
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$6cCeZj5gJh8MGx3Qtm9a1IJZ7Gw;
    ((-..Lambda.6cCeZj5gJh8MGx3Qtm9a1IJZ7Gw)localObject2).<init>(this);
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    localObject1 = d;
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$QCCCgLaOGdtqMSO1a2pVemoCytM;
    ((-..Lambda.QCCCgLaOGdtqMSO1a2pVemoCytM)localObject2).<init>(this);
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    localObject1 = b;
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$72ZICjpVhlp9oiTbEDQHQkmhwRc;
    ((-..Lambda.72ZICjpVhlp9oiTbEDQHQkmhwRc)localObject2).<init>(this);
    t.a((View)localObject1, (c.g.a.b)localObject2);
    localObject1 = c;
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$eQSkZAPmZqvOtgxnFQqK4_4kd8M;
    ((-..Lambda.eQSkZAPmZqvOtgxnFQqK4_4kd8M)localObject2).<init>(this);
    t.a((View)localObject1, (c.g.a.b)localObject2);
    localObject1 = d;
    localObject2 = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$BkaMepsJBsTFe0ffC7dEVZwSemU;
    ((-..Lambda.BkaMepsJBsTFe0ffC7dEVZwSemU)localObject2).<init>(this);
    t.a((View)localObject1, (c.g.a.b)localObject2);
    localObject1 = (AppCompatActivity)getActivity();
    localObject2 = i;
    ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
    localObject1 = ((AppCompatActivity)getActivity()).getSupportActionBar();
    int i1 = R.string.manage_acc_set_upi_pin;
    localObject2 = getString(i1);
    ((ActionBar)localObject1).setTitle((CharSequence)localObject2);
    localObject1 = ((AppCompatActivity)getActivity()).getSupportActionBar();
    i1 = 1;
    ((ActionBar)localObject1).setDisplayShowTitleEnabled(i1);
    ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(i1);
    localObject1 = ((AppCompatActivity)getActivity()).getSupportActionBar();
    int i2 = R.drawable.ic_close_48_px;
    ((ActionBar)localObject1).setHomeAsUpIndicator(i2);
    super.onViewCreated(paramView, paramBundle);
    j.a(this);
    j.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */