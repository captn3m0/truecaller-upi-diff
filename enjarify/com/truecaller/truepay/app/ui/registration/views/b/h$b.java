package com.truecaller.truepay.app.ui.registration.views.b;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.d;
import com.truecaller.multisim.SimInfo;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.d.f;
import com.truecaller.truepay.app.ui.registrationv2.views.PayRegistrationActivity;
import com.truecaller.truepay.app.utils.c;
import com.truecaller.truepay.data.b.a;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class h$b
  implements View.OnClickListener
{
  h$b(h paramh) {}
  
  public final void onClick(View paramView)
  {
    paramView = a.d;
    if (paramView == null)
    {
      localObject1 = "featuresRegistry";
      k.a((String)localObject1);
    }
    paramView = paramView.ae();
    boolean bool1 = paramView.a();
    if (bool1)
    {
      paramView = a;
      localObject1 = new android/content/Intent;
      localObject2 = paramView.getContext();
      ((Intent)localObject1).<init>((Context)localObject2, PayRegistrationActivity.class);
      paramView.startActivityForResult((Intent)localObject1, 1010);
      return;
    }
    paramView = a.g();
    Object localObject1 = h.a(a);
    Object localObject2 = a;
    boolean bool2 = h.b((h)localObject2);
    k.b(localObject1, "analyticContext");
    Object localObject3 = Truepay.getInstance();
    if (localObject3 != null)
    {
      localObject3 = ((Truepay)localObject3).getAnalyticLoggerHelper();
      if (localObject3 != null)
      {
        paramView = h;
        bool1 = paramView.j();
        ((a)localObject3).a((String)localObject1, bool2, (String)localObject1, bool1);
      }
    }
    paramView = new java/util/HashMap;
    paramView.<init>();
    localObject2 = paramView;
    localObject2 = (Map)paramView;
    ((Map)localObject2).put("PayRegistrationInitiatedSource", localObject1);
    localObject1 = "PayRegistrationInitiatedDate";
    localObject3 = Truepay.getInstance();
    if (localObject3 != null)
    {
      localObject3 = ((Truepay)localObject3).getAnalyticLoggerHelper();
      if (localObject3 != null)
      {
        localObject3 = ((a)localObject3).b();
        break label215;
      }
    }
    boolean bool3 = false;
    localObject3 = null;
    label215:
    if (localObject3 == null) {
      k.a();
    }
    ((Map)localObject2).put(localObject1, localObject3);
    localObject1 = Truepay.getInstance();
    if (localObject1 != null)
    {
      localObject1 = ((Truepay)localObject1).getAnalyticLoggerHelper();
      if (localObject1 != null) {
        ((a)localObject1).b(paramView);
      }
    }
    paramView = a.g();
    localObject1 = h.c(a);
    localObject2 = h.d(a);
    localObject3 = y.D();
    bool3 = ((com.truecaller.featuretoggles.b)localObject3).a();
    Object localObject4 = q;
    boolean bool4 = ((c)localObject4).a();
    int i = 2;
    int j = 1;
    if ((bool4) && (bool3))
    {
      i = 4;
    }
    else
    {
      localObject4 = h.h();
      bool4 = com.truecaller.multisim.b.b.c((List)localObject4);
      if ((bool4) && (bool3))
      {
        i = 5;
      }
      else
      {
        localObject4 = p;
        List localList = h.h();
        bool4 = ((com.truecaller.multisim.b.b)localObject4).b(localList);
        if ((bool4) && (bool3))
        {
          i = 6;
        }
        else if ((localObject1 != null) && (localObject2 != null))
        {
          i = 1;
        }
        else if (localObject1 == null)
        {
          i = 3;
        }
        else
        {
          localObject1 = h.h();
          int k = ((List)localObject1).size();
          if (k == j)
          {
            localObject1 = h.h().get(0);
            localObject2 = "multiSimManager.allSimInfos[0]";
            k.a(localObject1, (String)localObject2);
            localObject1 = (SimInfo)localObject1;
            i = paramView.a((SimInfo)localObject1);
          }
          else
          {
            if (localObject2 == null)
            {
              localObject1 = h.h();
              k = ((List)localObject1).size();
              if (k > j)
              {
                localObject1 = (CharSequence)h.h().get(0)).d;
                if (localObject1 != null)
                {
                  k = ((CharSequence)localObject1).length();
                  if (k != 0)
                  {
                    k = 0;
                    localObject1 = null;
                    break label590;
                  }
                }
                k = 1;
                label590:
                if (k == 0)
                {
                  localObject1 = (CharSequence)h.h().get(j)).d;
                  if (localObject1 != null)
                  {
                    k = ((CharSequence)localObject1).length();
                    if (k != 0)
                    {
                      k = 0;
                      localObject1 = null;
                      break label652;
                    }
                  }
                  k = 1;
                  label652:
                  if (k == 0) {
                    break label1052;
                  }
                }
              }
            }
            if (localObject2 == null)
            {
              localObject1 = h.h();
              k = ((List)localObject1).size();
              if (k > j)
              {
                localObject1 = (CharSequence)h.h().get(0)).d;
                if (localObject1 != null)
                {
                  k = ((CharSequence)localObject1).length();
                  if (k != 0)
                  {
                    k = 0;
                    localObject1 = null;
                    break label746;
                  }
                }
                k = 1;
                label746:
                if (k != 0)
                {
                  localObject1 = (CharSequence)h.h().get(j)).d;
                  if (localObject1 != null)
                  {
                    k = ((CharSequence)localObject1).length();
                    if (k != 0)
                    {
                      k = 0;
                      localObject1 = null;
                      break label808;
                    }
                  }
                  k = 1;
                  label808:
                  if (k == 0)
                  {
                    localObject1 = h.h().get(j);
                    localObject2 = "multiSimManager.allSimInfos[1]";
                    k.a(localObject1, (String)localObject2);
                    localObject1 = (SimInfo)localObject1;
                    i = paramView.a((SimInfo)localObject1);
                    break label1052;
                  }
                }
              }
            }
            if (localObject2 == null)
            {
              localObject1 = h.h();
              k = ((List)localObject1).size();
              if (k > j)
              {
                localObject1 = (CharSequence)h.h().get(0)).d;
                if (localObject1 != null)
                {
                  k = ((CharSequence)localObject1).length();
                  if (k != 0)
                  {
                    k = 0;
                    localObject1 = null;
                    break label941;
                  }
                }
                k = 1;
                label941:
                if (k == 0)
                {
                  localObject1 = (CharSequence)h.h().get(j)).d;
                  if (localObject1 != null)
                  {
                    k = ((CharSequence)localObject1).length();
                    if (k != 0) {
                      j = 0;
                    }
                  }
                  if (j != 0)
                  {
                    localObject1 = h.h().get(0);
                    localObject2 = "multiSimManager.allSimInfos[0]";
                    k.a(localObject1, (String)localObject2);
                    localObject1 = (SimInfo)localObject1;
                    i = paramView.a((SimInfo)localObject1);
                    break label1052;
                  }
                }
              }
            }
            if (localObject2 != null) {
              i = 0;
            }
          }
        }
      }
    }
    switch (i)
    {
    default: 
      paramView = new java/lang/AssertionError;
      paramView.<init>("Unknown screen");
      d.a((Throwable)paramView);
      return;
    case 6: 
      h.g(a);
      return;
    case 5: 
      h.f(a);
      return;
    case 4: 
      h.e(a);
      return;
    case 3: 
      h.b(a, "registration");
      return;
    case 2: 
      label1052:
      h.a(a, "registration");
      return;
    }
    h.h(a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */