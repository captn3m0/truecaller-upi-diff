package com.truecaller.truepay.app.ui.registration.views.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.b.a.a;
import com.truecaller.truepay.app.ui.registration.d.l;
import com.truecaller.truepay.app.ui.registration.views.c.g;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.data.e.e;

public class k
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements g
{
  public TextView a;
  public Button b;
  public TextView c;
  public ImageView d;
  public l e;
  public e f;
  public r g;
  k.a h;
  private com.truecaller.truepay.data.api.model.a i;
  
  public static k a(com.truecaller.truepay.data.api.model.a parama)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("connected_account", parama);
    parama = new com/truecaller/truepay/app/ui/registration/views/b/k;
    parama.<init>();
    parama.setArguments(localBundle);
    return parama;
  }
  
  public final int a()
  {
    return R.layout.fragment_set_upi_pin_info;
  }
  
  public final void b()
  {
    Object localObject1 = getArguments();
    Object localObject2 = "connected_account";
    localObject1 = (com.truecaller.truepay.data.api.model.a)((Bundle)localObject1).getSerializable((String)localObject2);
    i = ((com.truecaller.truepay.data.api.model.a)localObject1);
    localObject1 = i;
    if (localObject1 != null)
    {
      localObject1 = j;
      if (localObject1 != null)
      {
        localObject1 = d;
        localObject2 = g;
        Object localObject3 = i.j.d;
        localObject2 = ((r)localObject2).b((String)localObject3);
        ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
        localObject1 = c;
        int j = 2;
        localObject3 = new Object[j];
        String str = i.j.b;
        localObject3[0] = str;
        int k = 1;
        str = i.c;
        localObject3[k] = str;
        localObject2 = String.format("%s\n%s", (Object[])localObject3);
        ((TextView)localObject1).setText((CharSequence)localObject2);
      }
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject = getActivity();
    boolean bool = localObject instanceof j.a;
    if (bool)
    {
      paramContext = (k.a)getActivity();
      h = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.getClass();
    localStringBuilder.append(paramContext);
    localStringBuilder.append("should implement the OnFragmentInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.registration.b.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    e.b();
  }
  
  public void onDetach()
  {
    super.onDetach();
    h = null;
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int j = R.id.button_i_have_upi_pin;
    paramBundle = (TextView)paramView.findViewById(j);
    a = paramBundle;
    j = R.id.button_set_upi_pin;
    paramBundle = (Button)paramView.findViewById(j);
    b = paramBundle;
    j = R.id.tv_bank_name;
    paramBundle = (TextView)paramView.findViewById(j);
    c = paramBundle;
    j = R.id.bank_image;
    paramView = (ImageView)paramView.findViewById(j);
    d = paramView;
    paramView = a;
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$k$pbbKsfStnVJiVbcLQVTejkYnOIE;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = b;
    paramBundle = new com/truecaller/truepay/app/ui/registration/views/b/-$$Lambda$k$utV7tya-HwLMJIjr6illoChnrPo;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    e.a(this);
    ((g)e.d).b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.registration.views.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */