package com.truecaller.truepay.app.ui.dashboard.a;

import android.content.SharedPreferences;
import com.truecaller.truepay.app.ui.dashboard.a.a.h;
import com.truecaller.truepay.app.ui.dashboard.views.activities.InvisibleFallbackActivity;
import com.truecaller.truepay.app.ui.dashboard.views.activities.SettingsActivity;
import com.truecaller.truepay.app.utils.aj;
import com.truecaller.truepay.app.utils.al;
import com.truecaller.truepay.app.utils.ao;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.av;
import com.truecaller.truepay.app.utils.q;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.i;
import com.truecaller.utils.n;
import javax.inject.Provider;

public final class a
  implements b
{
  private final com.truecaller.truepay.app.a.a.a a;
  private final com.truecaller.truepay.data.a.a b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  
  private a(com.truecaller.truepay.app.ui.dashboard.a.a.a parama, com.truecaller.truepay.data.a.a parama1, com.truecaller.truepay.app.a.a.a parama2)
  {
    a = parama2;
    b = parama1;
    Object localObject = new com/truecaller/truepay/app/ui/dashboard/a/a$d;
    ((a.d)localObject).<init>(parama2);
    c = ((Provider)localObject);
    localObject = com.truecaller.truepay.data.e.d.a(c);
    d = ((Provider)localObject);
    localObject = d;
    localObject = dagger.a.c.a(com.truecaller.truepay.app.ui.dashboard.a.a.f.a(parama, (Provider)localObject));
    e = ((Provider)localObject);
    localObject = d;
    localObject = dagger.a.c.a(com.truecaller.truepay.app.ui.dashboard.a.a.b.a(parama, (Provider)localObject));
    f = ((Provider)localObject);
    localObject = dagger.a.c.a(com.truecaller.truepay.data.a.d.a(parama1));
    g = ((Provider)localObject);
    localObject = g;
    localObject = com.truecaller.truepay.data.a.e.a(parama1, (Provider)localObject);
    h = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/dashboard/a/a$e;
    ((a.e)localObject).<init>(parama2);
    i = ((Provider)localObject);
    localObject = i;
    Provider localProvider = g;
    parama1 = com.truecaller.truepay.data.a.f.a(parama1, (Provider)localObject, localProvider);
    j = parama1;
    parama1 = h;
    localObject = j;
    parama1 = com.truecaller.truepay.data.f.u.a(parama1, (Provider)localObject);
    k = parama1;
    parama1 = k;
    parama1 = dagger.a.c.a(com.truecaller.truepay.app.ui.dashboard.a.a.g.a(parama, parama1));
    l = parama1;
    parama1 = dagger.a.c.a(com.truecaller.truepay.app.ui.dashboard.a.a.e.a(parama));
    m = parama1;
    parama1 = new com/truecaller/truepay/app/ui/dashboard/a/a$c;
    parama1.<init>(parama2);
    n = parama1;
    parama1 = new com/truecaller/truepay/app/ui/dashboard/a/a$b;
    parama1.<init>(parama2);
    o = parama1;
    parama1 = n;
    parama2 = o;
    parama1 = dagger.a.c.a(com.truecaller.truepay.app.ui.dashboard.a.a.d.a(parama, parama1, parama2));
    p = parama1;
    parama1 = d;
    parama1 = dagger.a.c.a(h.a(parama, parama1));
    q = parama1;
    parama1 = dagger.a.c.a(com.truecaller.truepay.a.a.e.f.a(i));
    r = parama1;
    parama = dagger.a.c.a(com.truecaller.truepay.app.ui.dashboard.a.a.c.a(parama));
    s = parama;
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/truepay/app/ui/dashboard/a/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  private com.truecaller.truepay.data.e.c b()
  {
    com.truecaller.truepay.data.e.c localc = new com/truecaller/truepay/data/e/c;
    SharedPreferences localSharedPreferences = (SharedPreferences)dagger.a.g.a(a.aa(), "Cannot return null from a non-@Nullable component method");
    localc.<init>(localSharedPreferences);
    return localc;
  }
  
  private com.truecaller.truepay.data.f.a c()
  {
    com.truecaller.truepay.data.f.a locala = new com/truecaller/truepay/data/f/a;
    com.truecaller.truepay.data.f.c localc1 = com.truecaller.truepay.data.a.b.a();
    com.truecaller.truepay.data.f.c localc2 = com.truecaller.truepay.data.a.c.a((com.truecaller.truepay.data.api.g)dagger.a.g.a(a.p(), "Cannot return null from a non-@Nullable component method"));
    locala.<init>(localc1, localc2);
    return locala;
  }
  
  public final void a(InvisibleFallbackActivity paramInvisibleFallbackActivity)
  {
    com.truecaller.truepay.app.ui.dashboard.c.d locald = (com.truecaller.truepay.app.ui.dashboard.c.d)s.get();
    a = locald;
  }
  
  public final void a(SettingsActivity paramSettingsActivity)
  {
    Object localObject = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    featuresRegistry = ((com.truecaller.featuretoggles.e)localObject);
    localObject = (com.truecaller.truepay.app.utils.u)dagger.a.g.a(a.x(), "Cannot return null from a non-@Nullable component method");
    instantRewardHandler = ((com.truecaller.truepay.app.utils.u)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.dashboard.views.b.c paramc)
  {
    Object localObject1 = (r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    g = ((r)localObject1);
    localObject1 = (n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    h = ((n)localObject1);
    localObject1 = (au)dagger.a.g.a(a.k(), "Cannot return null from a non-@Nullable component method");
    i = ((au)localObject1);
    localObject1 = (i)dagger.a.g.a(a.ai(), "Cannot return null from a non-@Nullable component method");
    j = ((i)localObject1);
    localObject1 = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    k = ((com.truecaller.truepay.app.utils.a)localObject1);
    localObject1 = com.truecaller.truepay.app.ui.dashboard.c.b.a();
    Object localObject2 = new com/truecaller/truepay/a/a/b/c;
    com.truecaller.truepay.data.f.a locala = c();
    ((com.truecaller.truepay.a.a.b.c)localObject2).<init>(locala);
    a = ((com.truecaller.truepay.a.a.b.c)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/b/a;
    locala = c();
    ((com.truecaller.truepay.a.a.b.a)localObject2).<init>(locala);
    b = ((com.truecaller.truepay.a.a.b.a)localObject2);
    localObject2 = (com.truecaller.truepay.app.ui.npci.e)dagger.a.g.a(a.as(), "Cannot return null from a non-@Nullable component method");
    c = ((com.truecaller.truepay.app.ui.npci.e)localObject2);
    localObject2 = (av)q.get();
    f = ((av)localObject2);
    localObject2 = (n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    g = ((n)localObject2);
    localObject2 = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.E(), "Cannot return null from a non-@Nullable component method");
    h = ((com.truecaller.truepay.data.e.a)localObject2);
    localObject2 = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    i = ((com.truecaller.featuretoggles.e)localObject2);
    n = ((com.truecaller.truepay.app.ui.dashboard.c.a)localObject1);
    localObject1 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.P(), "Cannot return null from a non-@Nullable component method");
    o = ((com.truecaller.truepay.data.e.e)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.dashboard.views.b.d paramd)
  {
    Object localObject1 = paramd;
    Object localObject2 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.U(), "Cannot return null from a non-@Nullable component method");
    M = ((com.truecaller.truepay.data.e.e)localObject2);
    localObject2 = (ao)dagger.a.g.a(a.j(), "Cannot return null from a non-@Nullable component method");
    N = ((ao)localObject2);
    localObject2 = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    O = ((com.truecaller.truepay.app.utils.a)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/dashboard/c/c;
    com.truecaller.truepay.data.e.b localb1 = (com.truecaller.truepay.data.e.b)e.get();
    com.truecaller.truepay.data.e.b localb2 = (com.truecaller.truepay.data.e.b)f.get();
    com.truecaller.truepay.data.e.e locale1 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.Q(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.a locala1 = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.T(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.e locale2 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.B(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.a locala2 = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.C(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.a locala3 = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.z(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.a.a.c.e locale = (com.truecaller.truepay.a.a.c.e)dagger.a.g.a(a.ag(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.a.a.c.c localc = (com.truecaller.truepay.a.a.c.c)dagger.a.g.a(a.ah(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.a.a.a locala = (com.truecaller.truepay.a.a.a)l.get();
    aj localaj = (aj)dagger.a.g.a(a.i(), "Cannot return null from a non-@Nullable component method");
    localObject1 = dagger.a.g.a(a.W(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject1;
    localObject3 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.Y(), "Cannot return null from a non-@Nullable component method");
    Object localObject4 = localObject1;
    localObject4 = (c.d.f)localObject1;
    localObject1 = dagger.a.g.a(a.Z(), "Cannot return null from a non-@Nullable component method");
    Object localObject5 = localObject1;
    localObject5 = (c.d.f)localObject1;
    localObject1 = dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject1;
    localObject6 = (com.truecaller.featuretoggles.e)localObject1;
    localObject1 = dagger.a.g.a(a.v(), "Cannot return null from a non-@Nullable component method");
    Object localObject7 = localObject1;
    localObject7 = (al)localObject1;
    com.truecaller.truepay.data.e.c localc1 = b();
    ((com.truecaller.truepay.app.ui.dashboard.c.c)localObject2).<init>(localb1, localb2, locale1, locala1, locale2, locala2, locala3, locale, localc, locala, localaj, (com.truecaller.truepay.data.e.e)localObject3, (c.d.f)localObject4, (c.d.f)localObject5, (com.truecaller.featuretoggles.e)localObject6, (al)localObject7, localc1);
    localObject1 = paramd;
    P = ((com.truecaller.truepay.app.ui.dashboard.c.c)localObject2);
    localObject2 = (com.truecaller.truepay.app.ui.growth.a.a.b)m.get();
    Q = ((com.truecaller.truepay.app.ui.growth.a.a.b)localObject2);
    localObject2 = (com.truecaller.truepay.app.ui.dashboard.views.a.g)p.get();
    R = ((com.truecaller.truepay.app.ui.dashboard.views.a.g)localObject2);
    localObject2 = (com.truecaller.truepay.app.utils.u)dagger.a.g.a(a.x(), "Cannot return null from a non-@Nullable component method");
    S = ((com.truecaller.truepay.app.utils.u)localObject2);
    localObject2 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.K(), "Cannot return null from a non-@Nullable component method");
    T = ((com.truecaller.truepay.data.e.e)localObject2);
    localObject2 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.Q(), "Cannot return null from a non-@Nullable component method");
    U = ((com.truecaller.truepay.data.e.e)localObject2);
    localObject2 = (r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    V = ((r)localObject2);
    localObject2 = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    W = ((com.truecaller.featuretoggles.e)localObject2);
    localObject2 = (com.google.gson.f)dagger.a.g.a(a.am(), "Cannot return null from a non-@Nullable component method");
    X = ((com.google.gson.f)localObject2);
  }
  
  public final void a(com.truecaller.truepay.app.ui.dashboard.views.b.g paramg)
  {
    com.truecaller.truepay.app.ui.dashboard.c.f localf = new com/truecaller/truepay/app/ui/dashboard/c/f;
    com.truecaller.truepay.a.a.e.g localg = new com/truecaller/truepay/a/a/e/g;
    Object localObject1 = (com.truecaller.truepay.data.api.g)dagger.a.g.a(a.p(), "Cannot return null from a non-@Nullable component method");
    localg.<init>((com.truecaller.truepay.data.api.g)localObject1);
    localObject1 = r.get();
    Object localObject2 = localObject1;
    localObject2 = (com.truecaller.truepay.a.a.e.e)localObject1;
    com.truecaller.truepay.data.e.c localc1 = b();
    localObject1 = dagger.a.g.a(a.f(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject1;
    localObject3 = (q)localObject1;
    localObject1 = dagger.a.g.a(a.T(), "Cannot return null from a non-@Nullable component method");
    Object localObject4 = localObject1;
    localObject4 = (com.truecaller.truepay.data.e.a)localObject1;
    localObject1 = dagger.a.g.a(a.S(), "Cannot return null from a non-@Nullable component method");
    Object localObject5 = localObject1;
    localObject5 = (com.truecaller.truepay.data.e.a)localObject1;
    localObject1 = dagger.a.g.a(a.U(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject1;
    localObject6 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    Object localObject7 = localObject1;
    localObject7 = (com.truecaller.truepay.app.utils.a)localObject1;
    localObject1 = dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    Object localObject8 = localObject1;
    localObject8 = (n)localObject1;
    localObject1 = dagger.a.g.a(a.g(), "Cannot return null from a non-@Nullable component method");
    Object localObject9 = localObject1;
    localObject9 = (com.truecaller.truepay.f)localObject1;
    localObject1 = dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    Object localObject10 = localObject1;
    localObject10 = (com.truecaller.featuretoggles.e)localObject1;
    com.truecaller.truepay.data.e.c localc2 = b();
    localObject1 = localf;
    localf.<init>(localg, (com.truecaller.truepay.a.a.e.e)localObject2, localc1, (q)localObject3, (com.truecaller.truepay.data.e.a)localObject4, (com.truecaller.truepay.data.e.a)localObject5, (com.truecaller.truepay.data.e.e)localObject6, (com.truecaller.truepay.app.utils.a)localObject7, (n)localObject8, (com.truecaller.truepay.f)localObject9, (com.truecaller.featuretoggles.e)localObject10, localc2);
    a = localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */