package com.truecaller.truepay.app.ui.dashboard.a;

import com.truecaller.truepay.app.ui.dashboard.views.activities.InvisibleFallbackActivity;
import com.truecaller.truepay.app.ui.dashboard.views.activities.SettingsActivity;
import com.truecaller.truepay.app.ui.dashboard.views.b.c;
import com.truecaller.truepay.app.ui.dashboard.views.b.d;
import com.truecaller.truepay.app.ui.dashboard.views.b.g;

public abstract interface b
{
  public abstract void a(InvisibleFallbackActivity paramInvisibleFallbackActivity);
  
  public abstract void a(SettingsActivity paramSettingsActivity);
  
  public abstract void a(c paramc);
  
  public abstract void a(d paramd);
  
  public abstract void a(g paramg);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */