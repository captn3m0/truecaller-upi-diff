package com.truecaller.truepay.app.ui.dashboard.views.widgets;

import android.support.v4.view.ViewPager.f;

final class CircularViewPager$a
  implements ViewPager.f
{
  private CircularViewPager$a(CircularViewPager paramCircularViewPager) {}
  
  public final void onPageScrollStateChanged(int paramInt) {}
  
  public final void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {}
  
  public final void onPageSelected(int paramInt)
  {
    CircularViewPager localCircularViewPager = a;
    CircularViewPager.a.1 local1 = new com/truecaller/truepay/app/ui/dashboard/views/widgets/CircularViewPager$a$1;
    local1.<init>(this, paramInt);
    localCircularViewPager.postDelayed(local1, 200L);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.widgets.CircularViewPager.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */