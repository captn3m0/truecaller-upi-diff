package com.truecaller.truepay.app.ui.dashboard.views.a;

import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.n;
import java.util.ArrayList;
import java.util.List;

public abstract class b
  extends n
{
  private final List a;
  
  public b(j paramj, int paramInt)
  {
    super(paramj);
    paramj = new java/util/ArrayList;
    paramj.<init>();
    a = paramj;
    c(paramInt);
  }
  
  public final Fragment a(int paramInt)
  {
    List localList = a;
    int i = localList.size() + -1;
    if (paramInt == i) {
      return b(0);
    }
    if (paramInt == 0)
    {
      paramInt = a.size() + -3;
      return b(paramInt);
    }
    paramInt = ((Integer)a.get(paramInt)).intValue() + -1;
    return b(paramInt);
  }
  
  protected abstract Fragment b(int paramInt);
  
  public final void c(int paramInt)
  {
    a.clear();
    Object localObject = a;
    Integer localInteger1 = Integer.valueOf(paramInt + 1);
    ((List)localObject).add(localInteger1);
    localObject = null;
    int i = 0;
    localInteger1 = null;
    while (i < paramInt)
    {
      List localList1 = a;
      i += 1;
      Integer localInteger2 = Integer.valueOf(i);
      localList1.add(localInteger2);
    }
    List localList2 = a;
    localObject = Integer.valueOf(0);
    localList2.add(localObject);
  }
  
  public int getCount()
  {
    List localList = a;
    int i = localList.size();
    int j = 2;
    if (i > j) {
      return a.size();
    }
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */