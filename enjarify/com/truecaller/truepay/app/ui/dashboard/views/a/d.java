package com.truecaller.truepay.app.ui.dashboard.views.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import com.google.gson.f;
import com.truecaller.truepay.app.ui.history.views.c.a;
import com.truecaller.truepay.app.ui.history.views.c.b;
import com.truecaller.truepay.app.ui.history.views.c.c;
import java.text.DecimalFormat;
import java.util.List;

public final class d
  extends RecyclerView.Adapter
{
  public List a;
  private final Context b;
  private final f c;
  private c d;
  private b e;
  private a f;
  private d.a g;
  private DecimalFormat h;
  
  public d(List paramList, Context paramContext, c paramc, b paramb, a parama, d.a parama1, f paramf)
  {
    DecimalFormat localDecimalFormat = new java/text/DecimalFormat;
    localDecimalFormat.<init>("#,###.##");
    h = localDecimalFormat;
    a = paramList;
    b = paramContext;
    f = parama;
    d = paramc;
    e = paramb;
    g = parama1;
    c = paramf;
  }
  
  public final void a(List paramList)
  {
    a = paramList;
    notifyDataSetChanged();
  }
  
  public final int getItemCount()
  {
    List localList = a;
    if (localList != null) {
      return localList.size();
    }
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */