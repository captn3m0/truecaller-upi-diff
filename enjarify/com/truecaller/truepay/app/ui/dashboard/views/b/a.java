package com.truecaller.truepay.app.ui.dashboard.views.b;

import android.os.Bundle;
import android.view.View;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;

public class a
  extends com.truecaller.truepay.app.ui.base.views.fragments.a
{
  a.a a;
  
  public static a c()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    a locala = new com/truecaller/truepay/app/ui/dashboard/views/b/a;
    locala.<init>();
    locala.setArguments(localBundle);
    return locala;
  }
  
  public final int a()
  {
    return R.layout.fragment_deregister_confirmation_dialog;
  }
  
  public final void b()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = getTargetFragment();
      boolean bool = localObject1 instanceof a.a;
      if (bool)
      {
        localObject1 = (a.a)getTargetFragment();
        a = ((a.a)localObject1);
        return;
      }
    }
    localObject1 = new java/lang/RuntimeException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("parent fragment should implement ");
    String str = a.a.class.getSimpleName();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((RuntimeException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btn_cancel;
    paramBundle = paramView.findViewById(i);
    -..Lambda.a.V03ZYci8CXOAaO0UL9gjluZhhxM localV03ZYci8CXOAaO0UL9gjluZhhxM = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$a$V03ZYci8CXOAaO0UL9gjluZhhxM;
    localV03ZYci8CXOAaO0UL9gjluZhhxM.<init>(this);
    paramBundle.setOnClickListener(localV03ZYci8CXOAaO0UL9gjluZhhxM);
    i = R.id.btn_confirm;
    paramView = paramView.findViewById(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$a$okxq1s3AZDZgbRb_pWuZtBuGM7o;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */