package com.truecaller.truepay.app.ui.dashboard.views.b;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.fragments.b;
import com.truecaller.truepay.app.ui.dashboard.views.c.f;
import java.util.HashMap;

public final class h
  extends b
  implements f
{
  public static final h.a a;
  private HashMap b;
  
  static
  {
    h.a locala = new com/truecaller/truepay/app/ui/dashboard/views/b/h$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final int a()
  {
    return R.layout.update_app_info_fragment;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btnAppUpdate;
    paramView = (AppCompatButton)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/h$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.btnCloseUpdate;
    paramView = (ImageView)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/h$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */