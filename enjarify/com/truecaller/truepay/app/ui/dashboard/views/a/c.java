package com.truecaller.truepay.app.ui.dashboard.views.a;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.c;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.TcPayCreditLoanItem;
import com.truecaller.truepay.app.utils.j;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.n;

public final class c
  implements g
{
  private final n a;
  private final com.truecaller.truepay.app.utils.g b;
  
  public c(n paramn, com.truecaller.truepay.app.utils.g paramg)
  {
    a = paramn;
    b = paramg;
  }
  
  public final void a(f paramf, TcPayCreditLoanItem paramTcPayCreditLoanItem)
  {
    k.b(paramf, "tcPayLoanHistoryItemViewHolder");
    k.b(paramTcPayCreditLoanItem, "loanData");
    Object localObject1 = a;
    int i = R.string.rs_amount;
    int n = 1;
    Object localObject2 = new Object[n];
    Object localObject3 = paramTcPayCreditLoanItem.getAmount();
    localObject2[0] = localObject3;
    localObject1 = ((n)localObject1).a(i, (Object[])localObject2);
    k.a(localObject1, "resourceProvider.getStri…_amount, loanData.amount)");
    k.b(localObject1, "loanAmount");
    i = R.id.textLoanAmount;
    Object localObject4 = (TextView)paramf.a(i);
    k.a(localObject4, "textLoanAmount");
    localObject1 = (CharSequence)localObject1;
    ((TextView)localObject4).setText((CharSequence)localObject1);
    localObject1 = paramTcPayCreditLoanItem.getName();
    k.b(localObject1, "loanName");
    i = R.id.textLoanName;
    localObject4 = (AppCompatTextView)paramf.a(i);
    k.a(localObject4, "textLoanName");
    localObject1 = (CharSequence)localObject1;
    ((AppCompatTextView)localObject4).setText((CharSequence)localObject1);
    localObject1 = paramTcPayCreditLoanItem.getCategoryIcon();
    k.b(localObject1, "url");
    localObject1 = w.a(a.getContext()).a((String)localObject1);
    i = R.drawable.ic_credit_category_place_holder;
    localObject1 = ((ab)localObject1).a(i);
    localObject4 = new com/truecaller/common/h/aq$c;
    ((aq.c)localObject4).<init>(4.0F);
    localObject4 = (ai)localObject4;
    localObject1 = ((ab)localObject1).a((ai)localObject4);
    i = R.id.imageCategory;
    localObject4 = (AppCompatImageView)paramf.a(i);
    int i1 = 0;
    localObject2 = null;
    ((ab)localObject1).a((ImageView)localObject4, null);
    localObject1 = paramTcPayCreditLoanItem.getStatus();
    localObject4 = "success";
    boolean bool4 = k.a(localObject1, localObject4) ^ n;
    int m;
    if (bool4)
    {
      localObject1 = paramTcPayCreditLoanItem.getStatusText();
      localObject4 = paramTcPayCreditLoanItem.getStatus();
      i1 = ((String)localObject4).hashCode();
      int i3 = -934535297;
      if (i1 != i3)
      {
        i3 = 96784904;
        if (i1 != i3)
        {
          i3 = 1638128981;
          if (i1 == i3)
          {
            localObject2 = "in_process";
            boolean bool1 = ((String)localObject4).equals(localObject2);
            if (bool1)
            {
              localObject4 = b;
              i1 = R.color.orange;
              int j = ((com.truecaller.truepay.app.utils.g)localObject4).a(i1);
              break label469;
            }
          }
        }
        else
        {
          localObject2 = "error";
          boolean bool2 = ((String)localObject4).equals(localObject2);
          if (bool2)
          {
            localObject4 = b;
            i1 = R.color.coral;
            int k = ((com.truecaller.truepay.app.utils.g)localObject4).a(i1);
            break label469;
          }
        }
      }
      else
      {
        localObject2 = "repaid";
        boolean bool3 = ((String)localObject4).equals(localObject2);
        if (bool3)
        {
          localObject4 = b;
          i1 = R.color.turquoise;
          m = ((com.truecaller.truepay.app.utils.g)localObject4).a(i1);
          break label469;
        }
      }
      localObject4 = b;
      i1 = R.color.blue_grey;
      m = ((com.truecaller.truepay.app.utils.g)localObject4).a(i1);
      label469:
      paramf.a((String)localObject1, m);
    }
    else
    {
      localObject1 = a;
      m = R.string.credit_emis_remaining;
      localObject2 = new Object[2];
      localObject3 = paramTcPayCreditLoanItem.getRemainingEmiCount();
      localObject2[0] = localObject3;
      localObject3 = paramTcPayCreditLoanItem.getEmiCount();
      localObject2[n] = localObject3;
      localObject1 = ((n)localObject1).a(m, (Object[])localObject2);
      k.a(localObject1, "resourceProvider.getStri…Count, loanData.emiCount)");
      localObject4 = b;
      i1 = R.color.blue_grey;
      m = ((com.truecaller.truepay.app.utils.g)localObject4).a(i1);
      paramf.a((String)localObject1, m);
    }
    localObject1 = paramTcPayCreditLoanItem.getRepaymentLink();
    int i2;
    if (localObject1 != null)
    {
      localObject1 = paramTcPayCreditLoanItem.getRepaymentMessage();
      if (localObject1 == null)
      {
        localObject1 = a;
        m = R.string.credit_pay_now_description;
        localObject2 = new Object[0];
        localObject1 = ((n)localObject1).a(m, (Object[])localObject2);
        localObject4 = "resourceProvider.getStri…edit_pay_now_description)";
        k.a(localObject1, (String)localObject4);
      }
      k.b(localObject1, "payNowDescription");
      m = R.id.creditPayNowInfo;
      localObject4 = (TextView)paramf.a(m);
      localObject2 = "creditPayNowInfo";
      k.a(localObject4, (String)localObject2);
      localObject1 = (CharSequence)localObject1;
      ((TextView)localObject4).setText((CharSequence)localObject1);
    }
    else
    {
      i2 = R.id.repayContainer;
      localObject1 = (LinearLayout)paramf.a(i2);
      localObject4 = "repayContainer";
      k.a(localObject1, (String)localObject4);
      localObject1 = (View)localObject1;
      t.b((View)localObject1);
    }
    paramTcPayCreditLoanItem = paramTcPayCreditLoanItem.getNextEmiDueDate();
    if (paramTcPayCreditLoanItem != null)
    {
      long l = ((Number)paramTcPayCreditLoanItem).longValue();
      paramTcPayCreditLoanItem = a;
      i1 = R.string.credit_due;
      Object[] arrayOfObject = new Object[n];
      localObject3 = a.getContext();
      l *= 1000L;
      localObject1 = j.a((Context)localObject3, l);
      arrayOfObject[0] = localObject1;
      paramTcPayCreditLoanItem = paramTcPayCreditLoanItem.a(i1, arrayOfObject);
      k.a(paramTcPayCreditLoanItem, "resourceProvider.getStri…IS)\n                    )");
      k.b(paramTcPayCreditLoanItem, "nextEmiDate");
      i2 = R.id.textEmiDescription;
      paramf = (TextView)paramf.a(i2);
      k.a(paramf, "textEmiDescription");
      paramTcPayCreditLoanItem = (CharSequence)paramTcPayCreditLoanItem;
      paramf.setText(paramTcPayCreditLoanItem);
      return;
    }
    int i4 = R.id.textEmiDescription;
    paramf = (TextView)paramf.a(i4);
    k.a(paramf, "textEmiDescription");
    t.b((View)paramf);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */