package com.truecaller.truepay.app.ui.dashboard.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.dashboard.a.a.a;
import com.truecaller.truepay.app.ui.dashboard.a.b;
import com.truecaller.truepay.app.ui.dashboard.views.b.f;
import com.truecaller.truepay.app.ui.dashboard.views.b.h;
import com.truecaller.truepay.app.ui.dashboard.views.c.c;

public final class InvisibleFallbackActivity
  extends com.truecaller.truepay.app.ui.base.views.a.a
  implements c
{
  public com.truecaller.truepay.app.ui.dashboard.c.d a;
  private boolean b;
  
  public final void a()
  {
    finish();
  }
  
  public final void b()
  {
    Object localObject = getSupportFragmentManager();
    if (localObject != null)
    {
      localObject = ((j)localObject).a();
      Fragment localFragment = (Fragment)f.c();
      String str = f.class.getSimpleName();
      ((o)localObject).a(localFragment, str).d();
      return;
    }
  }
  
  public final void c()
  {
    o localo = getSupportFragmentManager().a();
    int i = R.id.container;
    Object localObject = h.a;
    localObject = new com/truecaller/truepay/app/ui/dashboard/views/b/h;
    ((h)localObject).<init>();
    localObject = (Fragment)localObject;
    String str = h.class.getSimpleName();
    localo.a(i, (Fragment)localObject, str).c();
  }
  
  public final int getLayoutId()
  {
    return R.layout.activity_error_handling;
  }
  
  public final void onBackPressed()
  {
    super.onBackPressed();
    finish();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    Object localObject1 = getIntent();
    Object localObject2 = "intent";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((Intent)localObject1).getExtras();
    if (localObject1 != null)
    {
      localObject2 = "full_screen_mode";
      bool = ((Bundle)localObject1).getBoolean((String)localObject2, false);
      b = bool;
    }
    boolean bool = b;
    int i;
    if (bool) {
      i = R.style.AppTheme_NoTitleBar;
    } else {
      i = R.style.TransparentTheme;
    }
    setTheme(i);
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.dashboard.a.a.a();
    localObject2 = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject2).a().a(this);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    if (paramBundle != null)
    {
      localObject2 = this;
      localObject2 = (com.truecaller.truepay.app.ui.base.views.a)this;
      paramBundle.a((com.truecaller.truepay.app.ui.base.views.a)localObject2);
    }
    if (localObject1 != null) {
      paramBundle = ((Bundle)localObject1).getString("error_key");
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      localObject1 = a;
      if (localObject1 == null)
      {
        localObject2 = "presenter";
        k.a((String)localObject2);
      }
      if (localObject1 != null)
      {
        ((com.truecaller.truepay.app.ui.dashboard.c.d)localObject1).a(paramBundle);
        return;
      }
      return;
    }
    paramBundle = new java/lang/AssertionError;
    paramBundle.<init>("No error code passed. Finishing InvisibleFallbackActivity");
    com.truecaller.log.d.a((Throwable)paramBundle);
    finish();
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    com.truecaller.truepay.app.ui.dashboard.c.d locald = a;
    if (locald == null)
    {
      String str = "presenter";
      k.a(str);
    }
    if (locald != null)
    {
      locald.b();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.activities.InvisibleFallbackActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */