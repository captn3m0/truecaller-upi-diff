package com.truecaller.truepay.app.ui.dashboard.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.dashboard.a.a.a;
import com.truecaller.truepay.app.ui.dashboard.views.b.g;

public final class SettingsActivity
  extends com.truecaller.truepay.app.ui.base.views.a.b
  implements com.truecaller.truepay.app.ui.dashboard.views.c.d
{
  public final void a()
  {
    Object localObject = this;
    localObject = android.support.v4.content.d.a((Context)this);
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.truecaller.ACTION_FINISH");
    ((android.support.v4.content.d)localObject).a(localIntent);
    setResult(-1);
    finish();
  }
  
  public final int getLayoutId()
  {
    return R.layout.activity_settings;
  }
  
  public final void initDagger(com.truecaller.truepay.app.a.a.a parama)
  {
    k.b(parama, "applicationComponent");
    com.truecaller.truepay.app.ui.dashboard.a.a.a().a(parama).a().a(this);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getSupportFragmentManager().a();
    int i = R.id.container_settings;
    Object localObject = g.b;
    localObject = new com/truecaller/truepay/app/ui/dashboard/views/b/g;
    ((g)localObject).<init>();
    localObject = (Fragment)localObject;
    String str = g.class.getSimpleName();
    paramBundle.a(i, (Fragment)localObject, str).c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.activities.SettingsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */