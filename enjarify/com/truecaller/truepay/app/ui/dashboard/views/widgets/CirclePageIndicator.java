package com.truecaller.truepay.app.ui.dashboard.views.widgets;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.b;
import android.support.v4.view.ViewPager.f;
import android.support.v4.view.o;
import android.support.v4.view.s;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import com.truecaller.truepay.R.bool;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.dimen;
import com.truecaller.truepay.R.integer;
import com.truecaller.truepay.R.styleable;
import java.util.ArrayList;

public class CirclePageIndicator
  extends View
  implements a
{
  private float a;
  private final Paint b;
  private final Paint c;
  private final Paint d;
  private ViewPager e;
  private ViewPager.f f;
  private int g;
  private int h;
  private float i;
  private int j;
  private int k;
  private boolean l;
  private boolean m;
  private int n;
  private float o;
  private int p;
  private boolean q;
  
  public CirclePageIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, i1);
  }
  
  private CirclePageIndicator(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    Object localObject1 = new android/graphics/Paint;
    int i1 = 1;
    float f1 = Float.MIN_VALUE;
    ((Paint)localObject1).<init>(i1);
    b = ((Paint)localObject1);
    localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>(i1);
    c = ((Paint)localObject1);
    localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>(i1);
    d = ((Paint)localObject1);
    o = -1.0F;
    p = -1;
    boolean bool1 = isInEditMode();
    if (bool1) {
      return;
    }
    localObject1 = getResources();
    i1 = R.color.default_circle_indicator_page_color;
    i1 = ((Resources)localObject1).getColor(i1);
    int i2 = R.color.default_circle_indicator_fill_color;
    i2 = ((Resources)localObject1).getColor(i2);
    int i3 = R.integer.default_circle_indicator_orientation;
    i3 = ((Resources)localObject1).getInteger(i3);
    int i4 = R.color.default_circle_indicator_stroke_color;
    i4 = ((Resources)localObject1).getColor(i4);
    int i5 = R.dimen.default_circle_indicator_stroke_width;
    float f2 = ((Resources)localObject1).getDimension(i5);
    int i6 = R.dimen.default_circle_indicator_radius;
    float f3 = ((Resources)localObject1).getDimension(i6);
    int i7 = R.bool.default_circle_indicator_centered;
    boolean bool2 = ((Resources)localObject1).getBoolean(i7);
    int i8 = R.bool.default_circle_indicator_snap;
    bool1 = ((Resources)localObject1).getBoolean(i8);
    int[] arrayOfInt = R.styleable.CirclePageIndicator;
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt, paramInt, 0);
    paramInt = R.styleable.CirclePageIndicator_centered;
    paramInt = paramAttributeSet.getBoolean(paramInt, bool2);
    l = paramInt;
    paramInt = R.styleable.CirclePageIndicator_android_orientation;
    paramInt = paramAttributeSet.getInt(paramInt, i3);
    k = paramInt;
    Object localObject2 = b;
    Paint.Style localStyle1 = Paint.Style.FILL;
    ((Paint)localObject2).setStyle(localStyle1);
    localObject2 = b;
    i3 = R.styleable.CirclePageIndicator_pageColor;
    i1 = paramAttributeSet.getColor(i3, i1);
    ((Paint)localObject2).setColor(i1);
    localObject2 = c;
    Paint.Style localStyle2 = Paint.Style.STROKE;
    ((Paint)localObject2).setStyle(localStyle2);
    localObject2 = c;
    i1 = R.styleable.CirclePageIndicator_strokeColor;
    i1 = paramAttributeSet.getColor(i1, i4);
    ((Paint)localObject2).setColor(i1);
    localObject2 = c;
    i1 = R.styleable.CirclePageIndicator_strokeWidth;
    f1 = paramAttributeSet.getDimension(i1, f2);
    ((Paint)localObject2).setStrokeWidth(f1);
    localObject2 = d;
    localStyle2 = Paint.Style.FILL;
    ((Paint)localObject2).setStyle(localStyle2);
    localObject2 = d;
    i1 = R.styleable.CirclePageIndicator_fillColor;
    i1 = paramAttributeSet.getColor(i1, i2);
    ((Paint)localObject2).setColor(i1);
    paramInt = R.styleable.CirclePageIndicator_radius;
    float f4 = paramAttributeSet.getDimension(paramInt, f3);
    a = f4;
    paramInt = R.styleable.CirclePageIndicator_snap;
    paramInt = paramAttributeSet.getBoolean(paramInt, bool1);
    m = paramInt;
    paramInt = R.styleable.CirclePageIndicator_android_background;
    localObject2 = paramAttributeSet.getDrawable(paramInt);
    if (localObject2 != null) {
      setBackgroundDrawable((Drawable)localObject2);
    }
    paramAttributeSet.recycle();
    int i9 = s.a(ViewConfiguration.get(paramContext));
    n = i9;
  }
  
  private int a(int paramInt)
  {
    int i1 = View.MeasureSpec.getMode(paramInt);
    paramInt = View.MeasureSpec.getSize(paramInt);
    int i2 = 1073741824;
    float f1 = 2.0F;
    if (i1 != i2)
    {
      Object localObject = e;
      if (localObject != null)
      {
        localObject = ((ViewPager)localObject).getAdapter();
        i2 = ((o)localObject).getCount();
        int i3 = getPaddingLeft();
        int i4 = getPaddingRight();
        float f2 = i3 + i4;
        i4 = i2 * 2;
        float f3 = i4;
        float f4 = a;
        f3 *= f4;
        f2 += f3;
        f1 = (i2 + -1) * f4;
        f2 += f1;
        f1 = 1.0F;
        i2 = (int)(f2 + f1);
        i3 = -1 << -1;
        f2 = -0.0F;
        if (i1 == i3) {
          paramInt = Math.min(i2, paramInt);
        } else {
          paramInt = i2;
        }
      }
    }
    return paramInt;
  }
  
  private int b(int paramInt)
  {
    int i1 = View.MeasureSpec.getMode(paramInt);
    paramInt = View.MeasureSpec.getSize(paramInt);
    int i2 = 1073741824;
    float f1 = 2.0F;
    if (i1 != i2)
    {
      float f2 = a * 2.0F;
      f1 = getPaddingTop();
      f2 += f1;
      f1 = getPaddingBottom();
      f2 += f1;
      f1 = 1.0F;
      i2 = (int)(f2 + f1);
      int i3 = -1 << -1;
      f2 = -0.0F;
      if (i1 == i3) {
        paramInt = Math.min(i2, paramInt);
      } else {
        paramInt = i2;
      }
    }
    return paramInt;
  }
  
  public int getFillColor()
  {
    return d.getColor();
  }
  
  public int getOrientation()
  {
    return k;
  }
  
  public int getPageColor()
  {
    return b.getColor();
  }
  
  public float getRadius()
  {
    return a;
  }
  
  public int getStrokeColor()
  {
    return c.getColor();
  }
  
  public float getStrokeWidth()
  {
    return c.getStrokeWidth();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    Object localObject = e;
    if (localObject == null) {
      return;
    }
    localObject = ((ViewPager)localObject).getAdapter();
    int i1 = ((o)localObject).getCount();
    if (i1 == 0) {
      return;
    }
    int i3 = g;
    if (i3 >= i1)
    {
      i1 += -1;
      setCurrentItem(i1);
      return;
    }
    i3 = k;
    int i5;
    int i9;
    if (i3 == 0)
    {
      i3 = getWidth();
      i5 = getPaddingLeft();
      i8 = getPaddingRight();
      i9 = getPaddingTop();
    }
    else
    {
      i3 = getHeight();
      i5 = getPaddingTop();
      i8 = getPaddingBottom();
      i9 = getPaddingLeft();
    }
    float f1 = a;
    float f2 = 3.0F * f1;
    float f3 = i9 + f1;
    float f4 = i5 + f1;
    boolean bool3 = l;
    int i11 = 1073741824;
    float f5 = 2.0F;
    if (bool3)
    {
      i3 = i3 - i5 - i8;
      f6 = i3 / f5;
      f7 = i1 * f2 / f5;
      f6 -= f7;
      f4 += f6;
    }
    float f6 = a;
    Paint localPaint1 = c;
    float f7 = localPaint1.getStrokeWidth();
    int i8 = 0;
    float f8 = 0.0F;
    int i6 = f7 < 0.0F;
    if (i6 > 0)
    {
      localPaint1 = c;
      f7 = localPaint1.getStrokeWidth() / f5;
      f6 -= f7;
    }
    i6 = 0;
    f7 = 0.0F;
    localPaint1 = null;
    while (i6 < i1)
    {
      if (i6 != 0)
      {
        i8 = i1 + -1;
        if (i6 != i8)
        {
          f8 = i6 * f2 + f4;
          int i10 = k;
          if (i10 == 0)
          {
            f1 = f3;
          }
          else
          {
            f1 = f8;
            f8 = f3;
          }
          Paint localPaint2 = b;
          i11 = localPaint2.getAlpha();
          if (i11 > 0)
          {
            localPaint2 = b;
            paramCanvas.drawCircle(f8, f1, f6, localPaint2);
          }
          f5 = a;
          boolean bool4 = f6 < f5;
          if (bool4)
          {
            Paint localPaint3 = c;
            paramCanvas.drawCircle(f8, f1, f5, localPaint3);
          }
        }
      }
      int i7;
      i6 += 1;
    }
    boolean bool1 = m;
    int i2;
    if (bool1) {
      i2 = h;
    } else {
      i2 = g;
    }
    float f9 = i2 * f2;
    boolean bool2 = m;
    if (!bool2)
    {
      f6 = i * f2;
      f9 += f6;
    }
    int i4 = k;
    if (i4 == 0)
    {
      f9 += f4;
    }
    else
    {
      f9 += f4;
      float f10 = f3;
      f3 = f9;
      f9 = f10;
    }
    f6 = a;
    localPaint1 = d;
    paramCanvas.drawCircle(f9, f3, f6, localPaint1);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i1 = k;
    if (i1 == 0)
    {
      paramInt1 = a(paramInt1);
      paramInt2 = b(paramInt2);
      setMeasuredDimension(paramInt1, paramInt2);
      return;
    }
    paramInt1 = b(paramInt1);
    paramInt2 = a(paramInt2);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  public void onPageScrollStateChanged(int paramInt)
  {
    j = paramInt;
    ViewPager.f localf = f;
    if (localf != null) {
      localf.onPageScrollStateChanged(paramInt);
    }
  }
  
  public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2)
  {
    g = paramInt1;
    i = paramFloat;
    invalidate();
    ViewPager.f localf = f;
    if (localf != null) {
      localf.onPageScrolled(paramInt1, paramFloat, paramInt2);
    }
  }
  
  public void onPageSelected(int paramInt)
  {
    boolean bool = m;
    if (!bool)
    {
      int i1 = j;
      if (i1 != 0) {}
    }
    else
    {
      g = paramInt;
      h = paramInt;
      invalidate();
    }
    ViewPager.f localf = f;
    if (localf != null) {
      localf.onPageSelected(paramInt);
    }
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable)
  {
    paramParcelable = (CirclePageIndicator.SavedState)paramParcelable;
    Parcelable localParcelable = paramParcelable.getSuperState();
    super.onRestoreInstanceState(localParcelable);
    int i1 = a;
    g = i1;
    int i2 = a;
    h = i2;
    requestLayout();
  }
  
  public Parcelable onSaveInstanceState()
  {
    Parcelable localParcelable = super.onSaveInstanceState();
    CirclePageIndicator.SavedState localSavedState = new com/truecaller/truepay/app/ui/dashboard/views/widgets/CirclePageIndicator$SavedState;
    localSavedState.<init>(localParcelable);
    int i1 = g;
    a = i1;
    return localSavedState;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool1 = super.onTouchEvent(paramMotionEvent);
    int i4 = 1;
    if (bool1) {
      return i4;
    }
    Object localObject1 = e;
    int i5 = 0;
    float f1 = 0.0F;
    ViewPager.b localb1 = null;
    if (localObject1 != null)
    {
      localObject1 = ((ViewPager)localObject1).getAdapter();
      int i1 = ((o)localObject1).getCount();
      if (i1 != 0)
      {
        i1 = paramMotionEvent.getAction() & 0xFF;
        int i10;
        float f2;
        float f3;
        float f4;
        float f5;
        boolean bool6;
        Object localObject2;
        float f6;
        float f8;
        int i13;
        int i2;
        int i3;
        switch (i1)
        {
        case 4: 
        default: 
          break;
        case 6: 
          i1 = paramMotionEvent.getActionIndex();
          int i6 = paramMotionEvent.getPointerId(i1);
          i10 = p;
          if (i6 == i10)
          {
            if (i1 == 0)
            {
              i5 = 1;
              f1 = Float.MIN_VALUE;
            }
            i1 = paramMotionEvent.getPointerId(i5);
            p = i1;
          }
          i1 = p;
          i1 = paramMotionEvent.findPointerIndex(i1);
          f2 = paramMotionEvent.getX(i1);
          o = f2;
          break;
        case 5: 
          i1 = paramMotionEvent.getActionIndex();
          f1 = paramMotionEvent.getX(i1);
          o = f1;
          int i11 = paramMotionEvent.getPointerId(i1);
          p = i11;
          break;
        case 2: 
          i1 = p;
          i1 = paramMotionEvent.findPointerIndex(i1);
          f2 = paramMotionEvent.getX(i1);
          f3 = o;
          f3 = f2 - f3;
          boolean bool4 = q;
          if (!bool4)
          {
            f4 = Math.abs(f3);
            i10 = n;
            f5 = i10;
            bool4 = f4 < f5;
            if (bool4) {
              q = i4;
            }
          }
          bool4 = q;
          if (bool4)
          {
            o = f2;
            paramMotionEvent = e;
            bool6 = n;
            int i12;
            float f7;
            long l2;
            long l3;
            if (!bool6)
            {
              paramMotionEvent = e;
              bool4 = h;
              if (bool4)
              {
                bool6 = false;
                f2 = 0.0F;
                paramMotionEvent = null;
              }
              else
              {
                n = i4;
                paramMotionEvent.setScrollState(i4);
                bool4 = false;
                f4 = 0.0F;
                i = 0.0F;
                j = 0.0F;
                localObject2 = l;
                if (localObject2 == null)
                {
                  localObject2 = VelocityTracker.obtain();
                  l = ((VelocityTracker)localObject2);
                }
                else
                {
                  localObject2 = l;
                  ((VelocityTracker)localObject2).clear();
                }
                long l1 = SystemClock.uptimeMillis();
                i12 = 0;
                f6 = 0.0F;
                f7 = 0.0F;
                l2 = l1;
                l3 = l1;
                localObject2 = MotionEvent.obtain(l1, l1, 0, 0.0F, 0.0F, 0);
                VelocityTracker localVelocityTracker = l;
                localVelocityTracker.addMovement((MotionEvent)localObject2);
                ((MotionEvent)localObject2).recycle();
                o = l1;
                bool6 = true;
                f2 = Float.MIN_VALUE;
              }
              if (!bool6) {
                break;
              }
            }
            else
            {
              paramMotionEvent = e;
              bool4 = n;
              if (bool4)
              {
                localObject2 = c;
                if (localObject2 != null)
                {
                  f4 = i + f3;
                  i = f4;
                  int i7 = paramMotionEvent.getScrollX();
                  f4 = i7 - f3;
                  i1 = paramMotionEvent.getClientWidth();
                  f3 = i1;
                  f5 = e * f3;
                  f8 = f * f3;
                  localb1 = (ViewPager.b)b.get(0);
                  Object localObject3 = b;
                  Object localObject4 = b;
                  i13 = ((ArrayList)localObject4).size() - i4;
                  localObject3 = (ViewPager.b)((ArrayList)localObject3).get(i13);
                  i13 = b;
                  if (i13 != 0) {
                    f1 = e * f3;
                  } else {
                    f1 = f5;
                  }
                  i10 = b;
                  localObject4 = c;
                  i13 = ((o)localObject4).getCount() - i4;
                  if (i10 != i13) {
                    f5 = e * f3;
                  } else {
                    f5 = f8;
                  }
                  boolean bool2 = f4 < f1;
                  if (!bool2)
                  {
                    bool2 = f4 < f5;
                    if (bool2) {
                      f1 = f5;
                    } else {
                      f1 = f4;
                    }
                  }
                  f3 = i;
                  i7 = (int)f1;
                  f5 = i7;
                  f1 -= f5;
                  f3 += f1;
                  i = f3;
                  i2 = paramMotionEvent.getScrollY();
                  paramMotionEvent.scrollTo(i7, i2);
                  paramMotionEvent.a(i7);
                  l3 = SystemClock.uptimeMillis();
                  l2 = o;
                  i12 = 2;
                  f6 = 2.8E-45F;
                  f7 = i;
                  localObject1 = MotionEvent.obtain(l2, l3, i12, f7, 0.0F, 0);
                  paramMotionEvent = l;
                  paramMotionEvent.addMovement((MotionEvent)localObject1);
                  ((MotionEvent)localObject1).recycle();
                }
              }
              else
              {
                paramMotionEvent = new java/lang/IllegalStateException;
                paramMotionEvent.<init>("No fake drag in progress. Call beginFakeDrag first.");
                throw paramMotionEvent;
              }
            }
          }
          break;
        case 1: 
        case 3: 
          boolean bool5 = q;
          int i15;
          int i9;
          if (!bool5)
          {
            localObject2 = e.getAdapter();
            int i8 = ((o)localObject2).getCount();
            i10 = getWidth();
            f5 = i10;
            f8 = f5 / 2.0F;
            float f9 = 6.0F;
            f5 /= f9;
            int i14 = g;
            i13 = 3;
            if (i14 > 0)
            {
              f9 = paramMotionEvent.getX();
              f6 = f8 - f5;
              boolean bool7 = f9 < f6;
              if (bool7)
              {
                if (i2 != i13)
                {
                  paramMotionEvent = e;
                  i2 = g - i4;
                  paramMotionEvent.setCurrentItem(i2);
                }
                return i4;
              }
            }
            i15 = g;
            i8 -= i4;
            if (i15 < i9)
            {
              f2 = paramMotionEvent.getX();
              f8 += f5;
              bool6 = f2 < f8;
              if (bool6)
              {
                if (i2 != i13)
                {
                  paramMotionEvent = e;
                  i2 = g + i4;
                  paramMotionEvent.setCurrentItem(i2);
                }
                return i4;
              }
            }
          }
          q = false;
          f2 = 0.0F / 0.0F;
          p = -1;
          paramMotionEvent = e;
          bool6 = n;
          if (bool6)
          {
            paramMotionEvent = e;
            boolean bool3 = n;
            if (bool3)
            {
              localObject1 = c;
              if (localObject1 != null)
              {
                localObject1 = l;
                f5 = m;
                ((VelocityTracker)localObject1).computeCurrentVelocity(1000, f5);
                i9 = k;
                f3 = ((VelocityTracker)localObject1).getXVelocity(i9);
                i3 = (int)f3;
                g = i4;
                i9 = paramMotionEvent.getClientWidth();
                i10 = paramMotionEvent.getScrollX();
                ViewPager.b localb2 = paramMotionEvent.c();
                i15 = b;
                f5 = i10;
                f4 = i9;
                f5 /= f4;
                f4 = e;
                f5 -= f4;
                f4 = d;
                f5 /= f4;
                f4 = i;
                f8 = j;
                f4 -= f8;
                i9 = (int)f4;
                i9 = paramMotionEvent.a(i15, f5, i3, i9);
                paramMotionEvent.a(i9, i4, i4, i3);
              }
              paramMotionEvent.d();
              n = false;
            }
            else
            {
              paramMotionEvent = new java/lang/IllegalStateException;
              paramMotionEvent.<init>("No fake drag in progress. Call beginFakeDrag first.");
              throw paramMotionEvent;
            }
          }
          break;
        case 0: 
          i3 = paramMotionEvent.getPointerId(0);
          p = i3;
          f2 = paramMotionEvent.getX();
          o = f2;
        }
        return i4;
      }
    }
    return false;
  }
  
  public void setCentered(boolean paramBoolean)
  {
    l = paramBoolean;
    invalidate();
  }
  
  public void setCurrentItem(int paramInt)
  {
    ViewPager localViewPager = e;
    if (localViewPager != null)
    {
      localViewPager.setCurrentItem(paramInt);
      g = paramInt;
      invalidate();
      return;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("CustomViewPager has not been bound.");
    throw localIllegalStateException;
  }
  
  public void setFillColor(int paramInt)
  {
    d.setColor(paramInt);
    invalidate();
  }
  
  public void setOnPageChangeListener(ViewPager.f paramf)
  {
    f = paramf;
  }
  
  public void setOrientation(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("Orientation must be either HORIZONTAL or VERTICAL.");
      throw localIllegalArgumentException;
    }
    k = paramInt;
    requestLayout();
  }
  
  public void setPageColor(int paramInt)
  {
    b.setColor(paramInt);
    invalidate();
  }
  
  public void setRadius(float paramFloat)
  {
    a = paramFloat;
    invalidate();
  }
  
  public void setSnap(boolean paramBoolean)
  {
    m = paramBoolean;
    invalidate();
  }
  
  public void setStrokeColor(int paramInt)
  {
    c.setColor(paramInt);
    invalidate();
  }
  
  public void setStrokeWidth(float paramFloat)
  {
    c.setStrokeWidth(paramFloat);
    invalidate();
  }
  
  public void setViewPager(ViewPager paramViewPager)
  {
    Object localObject = e;
    if (localObject == paramViewPager) {
      return;
    }
    if (localObject != null) {
      ((ViewPager)localObject).setOnPageChangeListener(null);
    }
    localObject = paramViewPager.getAdapter();
    if (localObject != null)
    {
      e = paramViewPager;
      e.setOnPageChangeListener(this);
      invalidate();
      return;
    }
    paramViewPager = new java/lang/IllegalStateException;
    paramViewPager.<init>("CustomViewPager does not have adapter instance.");
    throw paramViewPager;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.widgets.CirclePageIndicator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */