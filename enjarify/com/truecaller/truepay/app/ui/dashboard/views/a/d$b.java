package com.truecaller.truepay.app.ui.dashboard.views.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;

final class d$b
  extends RecyclerView.ViewHolder
{
  final TextView a;
  final TextView b;
  final TextView c;
  final TextView d;
  final ImageView e;
  
  d$b(d paramd, View paramView)
  {
    super(paramView);
    int i = R.id.tv_title_txn;
    paramd = (TextView)paramView.findViewById(i);
    a = paramd;
    i = R.id.tv_status_txn;
    paramd = (TextView)paramView.findViewById(i);
    b = paramd;
    i = R.id.tv_amount_txn;
    paramd = (TextView)paramView.findViewById(i);
    c = paramd;
    i = R.id.tv_time_txn;
    paramd = (TextView)paramView.findViewById(i);
    d = paramd;
    i = R.id.img_profile_txn;
    paramd = (ImageView)paramView.findViewById(i);
    e = paramd;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.a.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */