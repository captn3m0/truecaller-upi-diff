package com.truecaller.truepay.app.ui.dashboard.views.b;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.fragments.a;
import com.truecaller.truepay.app.ui.dashboard.views.activities.InvisibleFallbackActivity;
import com.truecaller.truepay.app.ui.dashboard.views.c.c;

public class f
  extends a
{
  private c a;
  
  public static f c()
  {
    f localf = new com/truecaller/truepay/app/ui/dashboard/views/b/f;
    localf.<init>();
    return localf;
  }
  
  public final int a()
  {
    return R.layout.fragment_set_auto_time_hint;
  }
  
  public final void b()
  {
    Object localObject = getActivity();
    boolean bool = localObject instanceof InvisibleFallbackActivity;
    if (bool)
    {
      localObject = (c)getActivity();
      a = ((c)localObject);
      return;
    }
    localObject = new java/lang/RuntimeException;
    ((RuntimeException)localObject).<init>("parent fragment should implement the InvisibleFallbackActivity");
    throw ((Throwable)localObject);
  }
  
  public void onDetach()
  {
    super.onDetach();
    a = null;
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    paramDialogInterface = a;
    if (paramDialogInterface != null) {
      paramDialogInterface.a();
    }
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btn_may_be_later;
    paramBundle = paramView.findViewById(i);
    -..Lambda.f.pL6vhOZ6m0-bkOjb98SBsc80-j0 localpL6vhOZ6m0-bkOjb98SBsc80-j0 = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$f$pL6vhOZ6m0-bkOjb98SBsc80-j0;
    localpL6vhOZ6m0-bkOjb98SBsc80-j0.<init>(this);
    paramBundle.setOnClickListener(localpL6vhOZ6m0-bkOjb98SBsc80-j0);
    i = R.id.btn_go_to_settings;
    paramView = paramView.findViewById(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$f$bbpv7Sxd8pOOGZJAjMGIt5sLayA;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */