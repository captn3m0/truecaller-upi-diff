package com.truecaller.truepay.app.ui.dashboard.views.widgets;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.view.View.BaseSavedState;

class CirclePageIndicator$SavedState
  extends View.BaseSavedState
{
  public static final Parcelable.Creator CREATOR;
  int a;
  
  static
  {
    CirclePageIndicator.SavedState.1 local1 = new com/truecaller/truepay/app/ui/dashboard/views/widgets/CirclePageIndicator$SavedState$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private CirclePageIndicator$SavedState(Parcel paramParcel)
  {
    super(paramParcel);
    int i = paramParcel.readInt();
    a = i;
  }
  
  public CirclePageIndicator$SavedState(Parcelable paramParcelable)
  {
    super(paramParcelable);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramInt = a;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.widgets.CirclePageIndicator.SavedState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */