package com.truecaller.truepay.app.ui.dashboard.views.b;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.dashboard.c.f;
import com.truecaller.truepay.app.ui.dashboard.c.f.a;
import com.truecaller.truepay.app.ui.dashboard.c.f.b;
import com.truecaller.truepay.app.ui.history.views.activities.TruePayWebViewActivity;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.q;
import java.util.HashMap;

public final class g
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements PopupMenu.OnMenuItemClickListener, a.a, b.a, com.truecaller.truepay.app.ui.dashboard.views.c.e
{
  public static final g.a b;
  public f a;
  private com.truecaller.truepay.app.ui.dashboard.views.c.d c;
  private HashMap d;
  
  static
  {
    g.a locala = new com/truecaller/truepay/app/ui/dashboard/views/b/g$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  private void f(String paramString)
  {
    Object localObject = "message";
    k.b(paramString, (String)localObject);
    paramString = (CharSequence)paramString;
    boolean bool = m.a(paramString);
    if (!bool)
    {
      localObject = getContext();
      paramString = Toast.makeText((Context)localObject, paramString, 0);
      paramString.show();
    }
  }
  
  public final int a()
  {
    return R.layout.fragment_settings;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "termsUrl");
    Intent localIntent = new android/content/Intent;
    Object localObject = (Context)getActivity();
    localIntent.<init>((Context)localObject, TruePayWebViewActivity.class);
    localIntent.putExtra("url", paramString);
    localIntent.putExtra("show_toolbar", true);
    int i = R.string.term_conditions_header;
    localObject = getString(i);
    localIntent.putExtra("title", (String)localObject);
    startActivity(localIntent);
  }
  
  public final void a(String paramString1, boolean paramBoolean1, boolean paramBoolean2, String paramString2, boolean paramBoolean3)
  {
    int i = R.id.tv_upi_id_settings;
    Object localObject1 = (TextView)a(i);
    String str1 = "tv_upi_id_settings";
    k.a(localObject1, str1);
    paramString1 = (CharSequence)paramString1;
    ((TextView)localObject1).setText(paramString1);
    int j = R.id.swc_default_id_settings;
    paramString1 = (Switch)a(j);
    localObject1 = "swc_default_id_settings";
    k.a(paramString1, (String)localObject1);
    paramString1.setChecked(paramBoolean1);
    Object localObject2;
    String str2;
    if (paramBoolean2)
    {
      j = R.id.ll_set_default_id;
      paramString1 = (RelativeLayout)a(j);
      k.a(paramString1, "ll_set_default_id");
      paramBoolean1 = false;
      localObject2 = null;
      paramString1.setVisibility(0);
      j = R.id.divider_3;
      paramString1 = a(j);
      k.a(paramString1, "divider_3");
      paramString1.setVisibility(0);
      j = R.id.swc_default_id_settings;
      paramString1 = (Switch)a(j);
      str2 = "swc_default_id_settings";
      k.a(paramString1, str2);
      paramString1.setVisibility(0);
    }
    else
    {
      j = R.id.ll_set_default_id;
      paramString1 = (RelativeLayout)a(j);
      localObject2 = "ll_set_default_id";
      k.a(paramString1, (String)localObject2);
      paramBoolean1 = true;
      paramString1.setVisibility(paramBoolean1);
      j = R.id.divider_3;
      paramString1 = a(j);
      k.a(paramString1, "divider_3");
      paramString1.setVisibility(paramBoolean1);
      j = R.id.swc_default_id_settings;
      paramString1 = (Switch)a(j);
      str2 = "swc_default_id_settings";
      k.a(paramString1, str2);
      paramString1.setVisibility(paramBoolean1);
    }
    j = R.id.include_home_footer;
    paramString1 = a(j);
    paramBoolean1 = R.id.psp_logo;
    paramString1 = (ImageView)paramString1.findViewById(paramBoolean1);
    if (paramBoolean3)
    {
      if (paramString2 != null)
      {
        paramBoolean1 = paramString2.hashCode();
        paramBoolean2 = -1396222919;
        if (paramBoolean1 == paramBoolean2)
        {
          localObject2 = "baroda";
          paramBoolean1 = paramString2.equals(localObject2);
          if (paramBoolean1)
          {
            localObject2 = getResources();
            paramBoolean2 = R.drawable.bob_logo;
            localObject2 = ((Resources)localObject2).getDrawable(paramBoolean2);
            paramString1.setImageDrawable((Drawable)localObject2);
            return;
          }
        }
      }
      localObject2 = getResources();
      paramBoolean2 = R.drawable.icici_logo;
      localObject2 = ((Resources)localObject2).getDrawable(paramBoolean2);
      paramString1.setImageDrawable((Drawable)localObject2);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramBoolean = R.id.overlay_progress_frame_settings;
      localFrameLayout = (FrameLayout)a(paramBoolean);
      k.a(localFrameLayout, "overlay_progress_frame_settings");
      localFrameLayout.setVisibility(0);
      paramBoolean = R.id.overlay_progress_frame_settings;
      localFrameLayout = (FrameLayout)a(paramBoolean);
      k.a(localFrameLayout, "overlay_progress_frame_settings");
      localFrameLayout.setClickable(true);
      return;
    }
    paramBoolean = R.id.overlay_progress_frame_settings;
    FrameLayout localFrameLayout = (FrameLayout)a(paramBoolean);
    k.a(localFrameLayout, "overlay_progress_frame_settings");
    localFrameLayout.setVisibility(8);
  }
  
  public final void b()
  {
    f localf = a;
    if (localf == null)
    {
      localObject1 = "presenter";
      k.a((String)localObject1);
    }
    Object localObject1 = a.a();
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((o)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/dashboard/c/f$b;
    ((f.b)localObject2).<init>(localf);
    localObject2 = (q)localObject2;
    ((o)localObject1).a((q)localObject2);
  }
  
  public final void b(String paramString)
  {
    String str = "message";
    k.b(paramString, str);
    f(paramString);
    paramString = a;
    if (paramString == null)
    {
      str = "presenter";
      k.a(str);
    }
    paramString.a();
  }
  
  public final void c()
  {
    f localf = a;
    if (localf == null)
    {
      localObject1 = "presenter";
      k.a((String)localObject1);
    }
    Object localObject1 = b.a();
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((o)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/dashboard/c/f$a;
    ((f.a)localObject2).<init>(localf);
    localObject2 = (q)localObject2;
    ((o)localObject1).a((q)localObject2);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "message");
    f(paramString);
  }
  
  public final void d()
  {
    Object localObject = new com/truecaller/truepay/data/provider/e/g;
    ((com.truecaller.truepay.data.provider.e.g)localObject).<init>();
    Context localContext = (Context)getActivity();
    ((com.truecaller.truepay.data.provider.e.g)localObject).a(localContext);
    localObject = new com/truecaller/truepay/data/provider/c/d;
    ((com.truecaller.truepay.data.provider.c.d)localObject).<init>();
    localContext = (Context)getActivity();
    ((com.truecaller.truepay.data.provider.c.d)localObject).a(localContext);
    localObject = new com/truecaller/truepay/data/provider/d/d;
    ((com.truecaller.truepay.data.provider.d.d)localObject).<init>();
    localContext = (Context)getActivity();
    ((com.truecaller.truepay.data.provider.d.d)localObject).a(localContext);
    localObject = c;
    if (localObject != null)
    {
      ((com.truecaller.truepay.app.ui.dashboard.views.c.d)localObject).a();
      return;
    }
  }
  
  public final void d(String paramString)
  {
    String str = "message";
    k.b(paramString, str);
    f(paramString);
    paramString = a;
    if (paramString == null)
    {
      str = "presenter";
      k.a(str);
    }
    paramString.a();
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "message");
    f(paramString);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.dashboard.views.c.d;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.dashboard.views.c.d)paramContext;
      c = paramContext;
      return;
    }
    RuntimeException localRuntimeException = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = String.valueOf(paramContext);
    localStringBuilder.append(paramContext);
    localStringBuilder.append("should implement the RegistrationView");
    paramContext = localStringBuilder.toString();
    localRuntimeException.<init>(paramContext);
    throw ((Throwable)localRuntimeException);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.dashboard.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((f)localObject).b();
    c = null;
    localObject = d;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final boolean onMenuItemClick(MenuItem paramMenuItem)
  {
    Object localObject = "item";
    k.b(paramMenuItem, (String)localObject);
    int i = paramMenuItem.getItemId();
    int j = R.id.menu_item_deregister_upi_id;
    if (i == j)
    {
      paramMenuItem = a.c();
      localObject = this;
      localObject = (Fragment)this;
      int k = 1005;
      paramMenuItem.setTargetFragment((Fragment)localObject, k);
      localObject = getFragmentManager();
      String str = a.class.getSimpleName();
      paramMenuItem.show((j)localObject, str);
    }
    return false;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = this;
    paramBundle = (com.truecaller.truepay.app.ui.base.views.a)this;
    paramView.a(paramBundle);
    int i = R.id.img_more_Settings;
    paramView = (ImageView)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/g$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.ll_set_default_id;
    paramView = (RelativeLayout)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/g$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.swc_default_id_settings;
    paramView = (Switch)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/g$d;
    paramBundle.<init>(this);
    paramBundle = (CompoundButton.OnCheckedChangeListener)paramBundle;
    paramView.setOnCheckedChangeListener(paramBundle);
    i = R.id.tv_myqr_settings;
    paramView = (TextView)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/g$e;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.tv_deactivate_settings;
    paramView = (TextView)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/g$f;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.tv_terms;
    paramView = (TextView)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/g$g;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = getActivity();
    if (paramView != null)
    {
      paramView = (AppCompatActivity)paramView;
      int j = R.id.toolbar;
      paramBundle = (Toolbar)a(j);
      paramView.setSupportActionBar(paramBundle);
      paramView = getActivity();
      if (paramView != null)
      {
        paramView = ((AppCompatActivity)paramView).getSupportActionBar();
        if (paramView != null)
        {
          j = 1;
          paramView.setDisplayShowTitleEnabled(j);
          paramView.setDisplayHomeAsUpEnabled(j);
        }
        i = R.id.toolbar;
        paramView = (Toolbar)a(i);
        paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/g$h;
        paramBundle.<init>(this);
        paramBundle = (View.OnClickListener)paramBundle;
        paramView.setNavigationOnClickListener(paramBundle);
        paramView = a;
        if (paramView == null)
        {
          paramBundle = "presenter";
          k.a(paramBundle);
        }
        paramBundle = h.c();
        if (paramBundle != null)
        {
          paramBundle = paramBundle.b();
        }
        else
        {
          bool1 = false;
          paramBundle = null;
        }
        Bundle localBundle = paramBundle;
        paramBundle = f;
        boolean bool1 = paramBundle.b();
        if (!bool1)
        {
          paramBundle = f;
          localObject = Boolean.TRUE;
          paramBundle.a((Boolean)localObject);
        }
        paramBundle = f.a();
        k.a(paramBundle, "defaultId.get()");
        boolean bool2 = paramBundle.booleanValue();
        paramBundle = c.a();
        k.a(paramBundle, "prefPayViaUpi.get()");
        boolean bool3 = paramBundle.booleanValue();
        String str = g.a();
        paramBundle = paramView.af_();
        localObject = paramBundle;
        localObject = (com.truecaller.truepay.app.ui.dashboard.views.c.e)paramBundle;
        boolean bool4 = j.q().a();
        ((com.truecaller.truepay.app.ui.dashboard.views.c.e)localObject).a(localBundle, bool2, bool3, str, bool4);
        return;
      }
      paramView = new c/u;
      paramView.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
      throw paramView;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */