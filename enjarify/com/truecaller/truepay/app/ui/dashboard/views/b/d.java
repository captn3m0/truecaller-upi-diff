package com.truecaller.truepay.app.ui.dashboard.views.b;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.c;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.ui.base.views.fragments.TcPayOnFragmentInteractionListener;
import com.truecaller.truepay.app.ui.dashboard.a.a.a;
import com.truecaller.truepay.app.ui.dashboard.c.c.c;
import com.truecaller.truepay.app.ui.dashboard.c.c.d;
import com.truecaller.truepay.app.ui.dashboard.c.c.e;
import com.truecaller.truepay.app.ui.dashboard.views.a.d.a;
import com.truecaller.truepay.app.ui.dashboard.views.a.e.a;
import com.truecaller.truepay.app.ui.dashboard.views.a.g;
import com.truecaller.truepay.app.ui.dashboard.views.activities.SettingsActivity;
import com.truecaller.truepay.app.ui.dashboard.views.widgets.CirclePageIndicator;
import com.truecaller.truepay.app.ui.dashboard.views.widgets.CircularViewPager;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.history.views.activities.TransactionHistoryActivity;
import com.truecaller.truepay.app.ui.history.views.activities.TruePayWebViewActivity;
import com.truecaller.truepay.app.ui.rewards.views.activities.RewardsActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.truepay.app.utils.ao;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.app.utils.u;
import com.truecaller.truepay.app.utils.v.a;
import com.truecaller.truepay.data.api.model.ac;
import com.truecaller.truepay.data.api.model.w;
import com.truecaller.truepay.data.f.t;
import io.reactivex.n;
import io.reactivex.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;

public final class d
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements AppBarLayout.c, PopupMenu.OnMenuItemClickListener, com.truecaller.common.ui.b, d.a, e.a, com.truecaller.truepay.app.ui.dashboard.views.c.b, v.a
{
  TextView A;
  TextView B;
  ImageView C;
  ProgressBar D;
  TextView E;
  Group F;
  TextView G;
  TextView H;
  RecyclerView I;
  View J;
  RecyclerView K;
  View L;
  public com.truecaller.truepay.data.e.e M;
  public ao N;
  public com.truecaller.truepay.app.utils.a O;
  public com.truecaller.truepay.app.ui.dashboard.c.c P;
  public com.truecaller.truepay.app.ui.growth.a.a.b Q;
  public g R;
  public u S;
  public com.truecaller.truepay.data.e.e T;
  public com.truecaller.truepay.data.e.e U;
  public r V;
  public com.truecaller.featuretoggles.e W;
  public com.google.gson.f X;
  private Context Y;
  private TcPayOnFragmentInteractionListener Z;
  RecyclerView a;
  private com.truecaller.truepay.app.ui.dashboard.views.a.d aa;
  private com.truecaller.truepay.app.ui.history.views.c.c ab;
  private com.truecaller.truepay.app.ui.history.views.c.b ac;
  private com.truecaller.truepay.app.ui.history.views.c.a ad;
  private List ae;
  private com.truecaller.truepay.app.ui.dashboard.views.a.a af;
  private final List ag;
  private int ah;
  private com.truecaller.truepay.app.ui.growth.a.a.a ai;
  private final BroadcastReceiver aj;
  private final ContentObserver ak;
  private BroadcastReceiver al;
  ConstraintLayout b;
  ConstraintLayout c;
  ConstraintLayout d;
  AppBarLayout e;
  CircularViewPager f;
  CirclePageIndicator g;
  ImageView h;
  Toolbar i;
  ImageButton j;
  ImageButton k;
  ImageButton l;
  View n;
  ConstraintLayout o;
  ConstraintLayout p;
  ImageView q;
  TextView r;
  TextView s;
  Button t;
  View u;
  View v;
  LinearLayout w;
  TextView x;
  TextView y;
  TextView z;
  
  public d()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    ag = ((List)localObject);
    ah = 3;
    localObject = new com/truecaller/truepay/app/ui/dashboard/views/b/d$1;
    ((d.1)localObject).<init>(this);
    aj = ((BroadcastReceiver)localObject);
    localObject = new com/truecaller/truepay/app/ui/dashboard/views/b/d$2;
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    ((d.2)localObject).<init>(this, localHandler);
    ak = ((ContentObserver)localObject);
    localObject = new com/truecaller/truepay/app/ui/dashboard/views/b/d$3;
    ((d.3)localObject).<init>(this);
    al = ((BroadcastReceiver)localObject);
  }
  
  public static d a(String paramString, Boolean paramBoolean)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("instant_reward_content", paramString);
    boolean bool = paramBoolean.booleanValue();
    localBundle.putBoolean("show_instant_reward", bool);
    paramString = new com/truecaller/truepay/app/ui/dashboard/views/b/d;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  public static d a(String paramString1, String paramString2)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("bank_symbol", paramString1);
    localBundle.putString("account_number", paramString2);
    paramString1 = new com/truecaller/truepay/app/ui/dashboard/views/b/d;
    paramString1.<init>();
    paramString1.setArguments(localBundle);
    return paramString1;
  }
  
  private void a(boolean paramBoolean)
  {
    Object localObject = C;
    int m = 0;
    int i1;
    if (paramBoolean) {
      i1 = 0;
    } else {
      i1 = 8;
    }
    ((ImageView)localObject).setVisibility(i1);
    localObject = F;
    if (paramBoolean) {
      m = 8;
    }
    ((Group)localObject).setVisibility(m);
  }
  
  public static d b()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    d locald = new com/truecaller/truepay/app/ui/dashboard/views/b/d;
    locald.<init>();
    locald.setArguments(localBundle);
    return locald;
  }
  
  private void c(String paramString)
  {
    com.truecaller.truepay.app.ui.dashboard.views.a.a locala = new com/truecaller/truepay/app/ui/dashboard/views/a/a;
    android.support.v4.app.j localj = getChildFragmentManager();
    List localList = k();
    locala.<init>(localj, localList, paramString);
    af = locala;
    paramString = f;
    locala = af;
    paramString.setAdapter(locala);
  }
  
  private void d(String paramString)
  {
    try
    {
      Intent localIntent = new android/content/Intent;
      String str = "android.intent.action.VIEW";
      paramString = Uri.parse(paramString);
      localIntent.<init>(str, paramString);
      startActivity(localIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      com.truecaller.log.d.a(localActivityNotFoundException;
    }
  }
  
  private void h()
  {
    Object localObject1 = P.a;
    Object localObject2 = Truepay.getInstance().getAnalyticLoggerHelper();
    String str1 = e;
    String str2 = String.valueOf(f);
    ((com.truecaller.truepay.data.b.a)localObject2).b(str1, str2);
    localObject2 = new android/content/Intent;
    ((Intent)localObject2).<init>();
    ((Intent)localObject2).setAction("android.intent.action.VIEW");
    localObject1 = Uri.parse(c);
    ((Intent)localObject2).setData((Uri)localObject1);
    startActivity((Intent)localObject2);
  }
  
  private void i()
  {
    Object localObject1 = (ClipboardManager)getActivity().getSystemService("clipboard");
    String str = G.getText().toString();
    Object localObject2 = ClipData.newPlainText("text", str);
    if (localObject1 != null)
    {
      ((ClipboardManager)localObject1).setPrimaryClip((ClipData)localObject2);
      localObject1 = getActivity();
      int m = R.string.message_copy_upi_id;
      localObject2 = getString(m);
      str = null;
      localObject1 = Toast.makeText((Context)localObject1, (CharSequence)localObject2, 0);
      ((Toast)localObject1).show();
    }
  }
  
  private void j()
  {
    int m = 0;
    int i2;
    for (;;)
    {
      localObject = k();
      int i1 = ((List)localObject).size();
      i2 = 1;
      if (m >= i1) {
        break;
      }
      localObject = (com.truecaller.truepay.app.ui.dashboard.b.a)ag.get(m);
      boolean bool = d;
      if (bool) {
        break label57;
      }
      m += 1;
    }
    m = 1;
    label57:
    Object localObject = f;
    m += i2;
    ((CircularViewPager)localObject).setCurrentItem(m);
  }
  
  private List k()
  {
    ag.clear();
    Object localObject1 = O.a().iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (com.truecaller.truepay.data.api.model.a)((Iterator)localObject1).next();
      localObject3 = ag;
      com.truecaller.truepay.app.ui.dashboard.b.a locala = new com/truecaller/truepay/app/ui/dashboard/b/a;
      String str1 = j.b;
      String str2 = j.d;
      str3 = a;
      boolean bool2 = g;
      localObject4 = c;
      locala.<init>(str1, str2, str3, bool2, (String)localObject4, false);
      ((List)localObject3).add(locala);
    }
    localObject1 = ag;
    Object localObject4 = new com/truecaller/truepay/app/ui/dashboard/b/a;
    int m = R.string.multiple_accounts_dashboard;
    Object localObject3 = getString(m);
    m = R.string.manage_all_in_one_place_accounts;
    String str3 = getString(m);
    Object localObject2 = localObject4;
    ((com.truecaller.truepay.app.ui.dashboard.b.a)localObject4).<init>((String)localObject3, "MANAGE_ACCOUNTS", " ", false, str3, true);
    ((List)localObject1).add(localObject4);
    return ag;
  }
  
  private void l()
  {
    Object localObject = getContext();
    String str = "android.permission.READ_CONTACTS";
    int m = android.support.v4.app.a.a((Context)localObject, str);
    if (m != 0)
    {
      localObject = new String[] { "android.permission.READ_CONTACTS" };
      requestPermissions((String[])localObject, 1001);
      return;
    }
    P.d();
  }
  
  public final int a()
  {
    return R.layout.fragment_dashboard;
  }
  
  public final void a(AppBarLayout paramAppBarLayout, int paramInt)
  {
    int m = paramAppBarLayout.getTotalScrollRange();
    float f1 = Math.abs(paramInt);
    float f2 = m;
    f1 /= f2;
    paramAppBarLayout = d;
    float f3 = 2.0F * f1;
    float f4 = 1.0F;
    f3 = f4 - f3;
    paramAppBarLayout.setAlpha(f3);
    paramAppBarLayout = j;
    f4 -= f1;
    paramAppBarLayout.setScaleX(f4);
    j.setScaleY(f4);
    k.setScaleX(f4);
    k.setScaleY(f4);
    l.setScaleX(f4);
    l.setScaleY(f4);
  }
  
  public final void a(android.support.v4.f.j paramj)
  {
    int m = 8;
    if (paramj != null)
    {
      localObject1 = a;
      if (localObject1 != null)
      {
        localObject1 = (List)a;
        i1 = ((List)localObject1).isEmpty();
        if (i1 == 0) {
          break label75;
        }
      }
    }
    Object localObject1 = aa.a;
    int i1 = ((List)localObject1).isEmpty();
    if (i1 != 0)
    {
      c.setVisibility(0);
      b.setVisibility(m);
      return;
    }
    label75:
    localObject1 = c;
    ((ConstraintLayout)localObject1).setVisibility(m);
    b.setVisibility(0);
    Object localObject2 = b;
    if (localObject2 != null)
    {
      localObject2 = b).a;
      if (localObject2 != null)
      {
        localObject2 = b).a;
        m = ((Integer)localObject2).intValue();
        i1 = 1;
        if (m != i1) {
          return;
        }
        localObject2 = aa.a;
        paramj = (Collection)a;
        ((List)localObject2).addAll(0, paramj);
        int i3 = ((List)localObject2).size();
        i2 = ah;
        if (i3 < i2) {
          i2 = ((List)localObject2).size();
        }
        paramj = ((List)localObject2).subList(0, i2);
        aa.a(paramj);
        return;
      }
    }
    localObject2 = (List)a;
    localObject1 = (List)a;
    int i2 = ((List)localObject1).size();
    int i4 = ah;
    if (i2 < i4)
    {
      paramj = (List)a;
      i4 = paramj.size();
    }
    paramj = ((List)localObject2).subList(0, i4);
    localObject2 = aa;
    ((com.truecaller.truepay.app.ui.dashboard.views.a.d)localObject2).a(paramj);
  }
  
  public final void a(com.truecaller.common.payments.a.a parama)
  {
    Object localObject1 = v;
    Object localObject2 = null;
    ((View)localObject1).setVisibility(0);
    localObject1 = h;
    if (localObject1 != null)
    {
      localObject1 = i;
      if (localObject1 != null)
      {
        w.setVisibility(0);
        localObject1 = x;
        localObject3 = h;
        ((TextView)localObject1).setText((CharSequence)localObject3);
        localObject1 = y;
        localObject3 = i;
        ((TextView)localObject1).setText((CharSequence)localObject3);
        break label90;
      }
    }
    localObject1 = w;
    int m = 8;
    ((LinearLayout)localObject1).setVisibility(m);
    label90:
    localObject1 = B;
    Object localObject3 = g;
    ((TextView)localObject1).setText((CharSequence)localObject3);
    localObject1 = z;
    localObject3 = d;
    ((TextView)localObject1).setText((CharSequence)localObject3);
    localObject1 = A;
    localObject3 = e;
    ((TextView)localObject1).setText((CharSequence)localObject3);
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = c;
      int i1 = ((Integer)localObject1).intValue();
      m = 1;
      int i3;
      if (i1 == m)
      {
        a(m);
        localObject1 = l;
        if (localObject1 != null)
        {
          localObject1 = V;
          localObject2 = l;
          localObject3 = C;
          i3 = R.drawable.bg_warm_gray_solid;
          ((r)localObject1).a((String)localObject2, (ImageView)localObject3, i3, i3);
        }
      }
      else
      {
        localObject1 = c;
        int i2 = ((Integer)localObject1).intValue();
        i3 = 2;
        if (i2 == i3)
        {
          a(false);
          localObject1 = k;
          if (localObject1 != null)
          {
            localObject1 = D;
            Integer localInteger1 = k;
            i3 = localInteger1.intValue();
            ((ProgressBar)localObject1).setProgress(i3);
            localObject1 = E;
            i3 = R.string.progress_percentage;
            localObject3 = new Object[m];
            Integer localInteger2 = k;
            localObject3[0] = localInteger2;
            localObject2 = getString(i3, (Object[])localObject3);
            ((TextView)localObject1).setText((CharSequence)localObject2);
          }
        }
      }
    }
    localObject1 = B;
    localObject2 = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$-Z0uhbZSiLeRxTl8BrWOAYXKTbs;
    ((-..Lambda.d.-Z0uhbZSiLeRxTl8BrWOAYXKTbs)localObject2).<init>(this, parama);
    ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
  }
  
  public final void a(h paramh)
  {
    Intent localIntent = new android/content/Intent;
    android.support.v4.app.f localf = getActivity();
    localIntent.<init>(localf, TransactionHistoryActivity.class);
    localIntent.putExtra("history_detail", true);
    localIntent.putExtra("history_item", paramh);
    startActivity(localIntent);
  }
  
  public final void a(ac paramac)
  {
    Object localObject1 = u;
    Object localObject2 = null;
    ((View)localObject1).setVisibility(0);
    localObject1 = J;
    int m = 8;
    ((View)localObject1).setVisibility(m);
    int i1 = f;
    int i2 = 2;
    if (i1 == i2)
    {
      o.setVisibility(0);
      p.setVisibility(m);
      localObject1 = r;
      localObject2 = a;
      ((TextView)localObject1).setText((CharSequence)localObject2);
      localObject1 = s;
      localObject2 = b;
      ((TextView)localObject1).setText((CharSequence)localObject2);
      localObject1 = t;
      paramac = d;
      ((Button)localObject1).setText(paramac);
      return;
    }
    i1 = f;
    i2 = 1;
    if (i1 == i2)
    {
      o.setVisibility(m);
      p.setVisibility(0);
      localObject1 = V;
      paramac = g;
      localObject2 = q;
      m = R.drawable.bg_warm_gray_solid;
      ((r)localObject1).a(paramac, (ImageView)localObject2, m, m);
    }
  }
  
  public final void a(String paramString)
  {
    d(paramString);
  }
  
  public final void a(List paramList)
  {
    Object localObject = getContext();
    if (localObject != null)
    {
      J.setVisibility(0);
      localObject = u;
      int m = 8;
      ((View)localObject).setVisibility(m);
      localObject = new com/truecaller/truepay/app/ui/growth/a/a/a;
      Context localContext = getContext();
      r localr = V;
      com.truecaller.truepay.app.ui.growth.a.a.b localb = Q;
      ((com.truecaller.truepay.app.ui.growth.a.a.a)localObject).<init>(localContext, localr, paramList, localb);
      ai = ((com.truecaller.truepay.app.ui.growth.a.a.a)localObject);
      paramList = new android/support/v7/widget/LinearLayoutManager;
      localObject = getContext();
      paramList.<init>((Context)localObject, 0, false);
      I.setLayoutManager(paramList);
      paramList = I;
      boolean bool = true;
      paramList.setHasFixedSize(bool);
      paramList = I;
      localObject = ai;
      paramList.setAdapter((RecyclerView.Adapter)localObject);
    }
  }
  
  public final void b(String paramString)
  {
    d(paramString);
  }
  
  public final void b(List paramList)
  {
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      L.setVisibility(0);
      localObject1 = new com/truecaller/truepay/app/ui/dashboard/views/a/e;
      Object localObject2 = getContext();
      Object localObject3 = R;
      ((com.truecaller.truepay.app.ui.dashboard.views.a.e)localObject1).<init>((Context)localObject2, paramList, (g)localObject3, this);
      paramList = new android/support/v7/widget/LinearLayoutManager;
      localObject2 = getContext();
      paramList.<init>((Context)localObject2);
      localObject2 = new android/support/v7/widget/DividerItemDecoration;
      localObject3 = getContext();
      int m = 1;
      ((DividerItemDecoration)localObject2).<init>((Context)localObject3, m);
      localObject3 = getContext();
      int i1 = R.drawable.divider_gray;
      localObject3 = android.support.v4.content.b.a((Context)localObject3, i1);
      ((DividerItemDecoration)localObject2).setDrawable((Drawable)localObject3);
      localObject3 = K;
      ((RecyclerView)localObject3).setLayoutManager(paramList);
      K.setHasFixedSize(m);
      K.addItemDecoration((RecyclerView.ItemDecoration)localObject2);
      paramList = K;
      paramList.setAdapter((RecyclerView.Adapter)localObject1);
    }
  }
  
  public final void c()
  {
    com.truecaller.truepay.app.ui.dashboard.a.b = true;
  }
  
  public final void d()
  {
    v.setVisibility(8);
  }
  
  public final void e()
  {
    L.setVisibility(8);
  }
  
  public final int f()
  {
    return 8;
  }
  
  public final void g()
  {
    Object localObject = getArguments();
    if (localObject == null) {
      return;
    }
    localObject = getArguments().getString("instant_reward_content");
    Intent localIntent = new android/content/Intent;
    android.support.v4.app.f localf = getActivity();
    localIntent.<init>(localf, RewardsActivity.class);
    localIntent.putExtra("show_instant_reward", true);
    localIntent.putExtra("instant_reward_content", (String)localObject);
    startActivity(localIntent);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int m = 100;
    if (paramInt1 == m)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        TcPayOnFragmentInteractionListener localTcPayOnFragmentInteractionListener = Z;
        com.truecaller.truepay.app.ui.base.views.fragments.b localb = Truepay.getInstance().getBankingFragment();
        localTcPayOnFragmentInteractionListener.replaceFragment(localb);
      }
    }
  }
  
  public final void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    Y = paramActivity;
    Object localObject = getActivity();
    boolean bool = localObject instanceof TcPayOnFragmentInteractionListener;
    if (bool)
    {
      paramActivity = (TcPayOnFragmentInteractionListener)getActivity();
      Z = paramActivity;
      return;
    }
    localObject = new java/lang/IllegalStateException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramActivity);
    localStringBuilder.append(" must implemenet ");
    paramActivity = TcPayOnFragmentInteractionListener.class.getSimpleName();
    localStringBuilder.append(paramActivity);
    paramActivity = localStringBuilder.toString();
    ((IllegalStateException)localObject).<init>(paramActivity);
    throw ((Throwable)localObject);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Y = paramContext;
    Object localObject = getActivity();
    boolean bool = localObject instanceof TcPayOnFragmentInteractionListener;
    if (bool)
    {
      paramContext = (TcPayOnFragmentInteractionListener)getActivity();
      Z = paramContext;
      return;
    }
    localObject = new java/lang/IllegalStateException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implemenet ");
    paramContext = TcPayOnFragmentInteractionListener.class.getSimpleName();
    localStringBuilder.append(paramContext);
    paramContext = localStringBuilder.toString();
    ((IllegalStateException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.dashboard.a.a.a();
    Object localObject1 = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject1).a().a(this);
    paramBundle = P;
    localObject1 = q.r();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool)
    {
      localObject1 = s;
      Object localObject2 = "k7GS,p?7$%&,jke~";
      localObject1 = ((com.truecaller.truepay.data.e.c)localObject1).e((String)localObject2);
      bool = ((Boolean)localObject1).booleanValue();
      if (!bool)
      {
        paramBundle = s;
        localObject1 = "k7GS,p?7$%&,jke~";
        localObject2 = Boolean.TRUE;
        paramBundle.a((String)localObject1, (Boolean)localObject2);
        paramBundle = Truepay.getInstance();
        if (paramBundle != null)
        {
          paramBundle = creditHelper;
          if (paramBundle != null) {
            paramBundle.f();
          }
        }
        return;
      }
      paramBundle = Truepay.getInstance();
      if (paramBundle != null)
      {
        paramBundle = creditHelper;
        if (paramBundle != null)
        {
          paramBundle.e();
          return;
        }
      }
    }
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject1 = getContext().getContentResolver();
    Object localObject2 = ak;
    ((ContentResolver)localObject1).unregisterContentObserver((ContentObserver)localObject2);
    P.b();
    localObject1 = getContext();
    localObject2 = al;
    android.support.v4.content.d.a((Context)localObject1).a((BroadcastReceiver)localObject2);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    Y = null;
  }
  
  public final boolean onMenuItemClick(MenuItem paramMenuItem)
  {
    int m = paramMenuItem.getItemId();
    int i1 = R.id.menu_item_pending_request;
    Object localObject1;
    Object localObject2;
    if (m == i1)
    {
      paramMenuItem = new android/content/Intent;
      localObject1 = getActivity();
      paramMenuItem.<init>((Context)localObject1, TransactionActivity.class);
      localObject1 = "route";
      localObject2 = "route_pending_request";
      paramMenuItem.putExtra((String)localObject1, (String)localObject2);
      startActivity(paramMenuItem);
    }
    else
    {
      i1 = R.id.menu_item_transaction_history;
      if (m == i1)
      {
        paramMenuItem = new android/content/Intent;
        localObject1 = getActivity();
        localObject2 = TransactionHistoryActivity.class;
        paramMenuItem.<init>((Context)localObject1, (Class)localObject2);
        startActivity(paramMenuItem);
      }
      else
      {
        i1 = R.id.menu_item_manage_accounts;
        if (m == i1)
        {
          paramMenuItem = getActivity();
          localObject1 = "";
          localObject2 = null;
          ManageAccountsActivity.a(paramMenuItem, null, null, (String)localObject1);
        }
        else
        {
          i1 = R.id.menu_item_settings;
          if (m == i1)
          {
            paramMenuItem = new android/content/Intent;
            localObject1 = getActivity();
            localObject2 = SettingsActivity.class;
            paramMenuItem.<init>((Context)localObject1, (Class)localObject2);
            i1 = 100;
            startActivityForResult(paramMenuItem, i1);
          }
          else
          {
            i1 = R.id.menu_item_support;
            if (m == i1)
            {
              paramMenuItem = new android/content/Intent;
              localObject1 = getActivity();
              paramMenuItem.<init>((Context)localObject1, TruePayWebViewActivity.class);
              localObject1 = "url";
              localObject2 = U.a();
              paramMenuItem.putExtra((String)localObject1, (String)localObject2);
              startActivity(paramMenuItem);
            }
            else
            {
              i1 = R.id.menu_item_rewards;
              if (m == i1)
              {
                paramMenuItem = RewardsActivity.a(getActivity());
                startActivity(paramMenuItem);
              }
              else
              {
                i1 = R.id.menu_item_add_to_home;
                if (m == i1)
                {
                  paramMenuItem = Truepay.getInstance().getAnalyticLoggerHelper();
                  localObject1 = "dashboard_tab_menu";
                  paramMenuItem.g((String)localObject1);
                  paramMenuItem = Truepay.getInstance().getListener();
                  i1 = 3;
                  paramMenuItem.createShortcut(i1);
                }
              }
            }
          }
        }
      }
    }
    return false;
  }
  
  public final void onPause()
  {
    super.onPause();
    Object localObject = W.H();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      localObject = S;
      ((u)localObject).a();
    }
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int m = 1001;
    if (paramInt == m)
    {
      paramInt = paramArrayOfInt.length;
      m = 1;
      if (paramInt == m)
      {
        str = null;
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0)
        {
          l();
          return;
        }
      }
      paramInt = R.string.contacts_permission_denied_string;
      String str = getString(paramInt);
      m = 0;
      paramArrayOfString = null;
      a(str, null);
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    Object localObject1 = getContext().getContentResolver();
    Object localObject2 = ak;
    ((ContentResolver)localObject1).unregisterContentObserver((ContentObserver)localObject2);
    localObject1 = getContext().getContentResolver();
    localObject2 = com.truecaller.truepay.data.provider.e.d.a;
    Object localObject3 = ak;
    Object localObject4 = null;
    ((ContentResolver)localObject1).registerContentObserver((Uri)localObject2, false, (ContentObserver)localObject3);
    P.a();
    localObject1 = P;
    localObject2 = q.r();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject2).a();
    if (bool1)
    {
      localObject2 = (ag)bg.a;
      localObject3 = o;
      localObject4 = new com/truecaller/truepay/app/ui/dashboard/c/c$c;
      ((c.c)localObject4).<init>((com.truecaller.truepay.app.ui.dashboard.c.c)localObject1, null);
      localObject4 = (m)localObject4;
      int m = 2;
      localObject2 = kotlinx.coroutines.e.b((ag)localObject2, (c.d.f)localObject3, (m)localObject4, m);
      b = ((bn)localObject2);
    }
    boolean bool2 = com.truecaller.truepay.app.ui.dashboard.a.b;
    if (bool2)
    {
      localObject1 = P;
      ((com.truecaller.truepay.app.ui.dashboard.c.c)localObject1).d();
    }
    localObject1 = W.H();
    bool2 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool2)
    {
      localObject1 = S;
      ((u)localObject1).a(this);
    }
  }
  
  public final void onRewardReceived(Intent paramIntent)
  {
    startActivity(paramIntent);
  }
  
  public final void onStart()
  {
    super.onStart();
    android.support.v4.content.d locald = android.support.v4.content.d.a(getActivity());
    BroadcastReceiver localBroadcastReceiver = aj;
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>("notification_received");
    locald.a(localBroadcastReceiver, localIntentFilter);
  }
  
  public final void onStop()
  {
    super.onStop();
    android.support.v4.content.d locald = android.support.v4.content.d.a(getActivity());
    BroadcastReceiver localBroadcastReceiver = aj;
    locald.a(localBroadcastReceiver);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.rv_recent_transactions;
    paramBundle = (RecyclerView)paramView.findViewById(m);
    a = paramBundle;
    m = R.id.cl_recent_list_layout;
    paramBundle = (ConstraintLayout)paramView.findViewById(m);
    b = paramBundle;
    m = R.id.cl_recent_empty;
    paramBundle = (ConstraintLayout)paramView.findViewById(m);
    c = paramBundle;
    m = R.id.cl_actions_frag_dashboard;
    paramBundle = (ConstraintLayout)paramView.findViewById(m);
    d = paramBundle;
    m = R.id.app_bar_layout;
    paramBundle = (AppBarLayout)paramView.findViewById(m);
    e = paramBundle;
    m = R.id.vp_dashboard_frag_dashboard;
    paramBundle = (CircularViewPager)paramView.findViewById(m);
    f = paramBundle;
    m = R.id.pager_indicator;
    paramBundle = (CirclePageIndicator)paramView.findViewById(m);
    g = paramBundle;
    m = R.id.img_more;
    paramBundle = (ImageView)paramView.findViewById(m);
    h = paramBundle;
    m = R.id.toolbar;
    paramBundle = (Toolbar)paramView.findViewById(m);
    i = paramBundle;
    m = R.id.btn_send;
    paramBundle = (ImageButton)paramView.findViewById(m);
    j = paramBundle;
    m = R.id.btn_scan;
    paramBundle = (ImageButton)paramView.findViewById(m);
    k = paramBundle;
    m = R.id.btn_request;
    paramBundle = (ImageButton)paramView.findViewById(m);
    l = paramBundle;
    m = R.id.include_home_footer;
    paramBundle = paramView.findViewById(m);
    n = paramBundle;
    m = R.id.cl_banner_text;
    paramBundle = (ConstraintLayout)paramView.findViewById(m);
    o = paramBundle;
    m = R.id.cl_banner_image;
    paramBundle = (ConstraintLayout)paramView.findViewById(m);
    p = paramBundle;
    m = R.id.img_banner_image;
    paramBundle = (ImageView)paramView.findViewById(m);
    q = paramBundle;
    m = R.id.tv_banner_header;
    paramBundle = (TextView)paramView.findViewById(m);
    r = paramBundle;
    m = R.id.tv_banner_description;
    paramBundle = (TextView)paramView.findViewById(m);
    s = paramBundle;
    m = R.id.btn_banner_action;
    paramBundle = (Button)paramView.findViewById(m);
    t = paramBundle;
    m = R.id.layout_home_banner;
    paramBundle = paramView.findViewById(m);
    u = paramBundle;
    m = R.id.credit_banner;
    paramBundle = paramView.findViewById(m);
    v = paramBundle;
    m = R.id.llHeader;
    paramBundle = (LinearLayout)paramView.findViewById(m);
    w = paramBundle;
    m = R.id.tvHeaderLeft;
    paramBundle = (TextView)paramView.findViewById(m);
    x = paramBundle;
    m = R.id.tvHeaderRight;
    paramBundle = (TextView)paramView.findViewById(m);
    y = paramBundle;
    m = R.id.tvTitle;
    paramBundle = (TextView)paramView.findViewById(m);
    z = paramBundle;
    m = R.id.tvSubtitle;
    paramBundle = (TextView)paramView.findViewById(m);
    A = paramBundle;
    m = R.id.btnCta;
    paramBundle = (TextView)paramView.findViewById(m);
    B = paramBundle;
    m = R.id.ivRight;
    paramBundle = (ImageView)paramView.findViewById(m);
    C = paramBundle;
    m = R.id.pbPercentage;
    paramBundle = (ProgressBar)paramView.findViewById(m);
    D = paramBundle;
    m = R.id.tvPercentage;
    paramBundle = (TextView)paramView.findViewById(m);
    E = paramBundle;
    m = R.id.groupProgress;
    paramBundle = (Group)paramView.findViewById(m);
    F = paramBundle;
    m = R.id.tv_upi_id;
    paramBundle = (TextView)paramView.findViewById(m);
    G = paramBundle;
    m = R.id.tv_upi_id_logo;
    paramBundle = (TextView)paramView.findViewById(m);
    H = paramBundle;
    m = R.id.homePromoList;
    paramBundle = (RecyclerView)paramView.findViewById(m);
    I = paramBundle;
    m = R.id.home_carousal;
    paramBundle = paramView.findViewById(m);
    J = paramBundle;
    m = R.id.listLoanHistory;
    paramBundle = (RecyclerView)paramView.findViewById(m);
    K = paramBundle;
    m = R.id.credit_active_loans;
    paramBundle = paramView.findViewById(m);
    L = paramBundle;
    m = R.id.img_menu;
    paramBundle = paramView.findViewById(m);
    Object localObject1 = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$4u8ciQxkB3NpczdJ5rqa6OFXCNA;
    ((-..Lambda.d.4u8ciQxkB3NpczdJ5rqa6OFXCNA)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    m = R.id.tv_view_all;
    paramBundle = paramView.findViewById(m);
    localObject1 = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$wGKjo8vmIAFL7wIXSoQd5nm3Df4;
    ((-..Lambda.d.wGKjo8vmIAFL7wIXSoQd5nm3Df4)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    m = R.id.textViewDetails;
    paramView = paramView.findViewById(m);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$aPOLuUuU5VEXckmo1XpGK7tR8ww;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = t;
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$ZHqSmIA3xflRNXWnizGNApprltE;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = q;
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$cqQ2WRtlKtoUav_-bp8XQ-Xm9RM;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = j;
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$0jiFgc0kQkeMhZrX07Q8uzkc82M;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = k;
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$4-pIsKEmoE7WzxF8pBw7CB5FjBE;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = l;
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$CsfkFuLI-jjtr1l7oIzUJ3RNoIQ;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = h;
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$ciUxsBRRFXvV6tA9Mc-HrBr6Ydo;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = G;
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$ymXKs4nNlILCfRwOkCQPCCINoC0;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = H;
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$d$pT3pHtS-ChnKao7soBAmcpav5oE;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    P.a(this);
    paramView = e;
    paramView.a(this);
    int i1 = Build.VERSION.SDK_INT;
    m = 21;
    if (i1 >= m)
    {
      paramView = i;
      m = 0;
      paramBundle = null;
      paramView.setElevation(0.0F);
    }
    paramView = getArguments();
    m = 0;
    paramBundle = null;
    if (paramView != null) {
      paramBundle = paramView.getString("bank_symbol");
    }
    c(paramBundle);
    j();
    paramView = getContext();
    if (paramView != null)
    {
      paramView = getContext();
      paramBundle = al;
      paramView = android.support.v4.content.d.a(paramView);
      localObject1 = new android/content/IntentFilter;
      localObject2 = "ACCOUNTS_CHANGED";
      ((IntentFilter)localObject1).<init>((String)localObject2);
      paramView.a(paramBundle, (IntentFilter)localObject1);
    }
    paramView = g;
    paramBundle = f;
    paramView.setViewPager(paramBundle);
    paramView = g;
    paramBundle = getResources();
    int i2 = R.color.blue_color;
    m = paramBundle.getColor(i2);
    paramView.setFillColor(m);
    g.setRadius(8.0F);
    g.setStrokeWidth(1.0F);
    paramView = new android/support/v7/widget/LinearLayoutManager;
    paramBundle = getContext();
    paramView.<init>(paramBundle);
    a.setLayoutManager(paramView);
    paramView = a;
    paramBundle = new android/support/v7/widget/DefaultItemAnimator;
    paramBundle.<init>();
    paramView.setItemAnimator(paramBundle);
    paramView = new android/support/v7/widget/DividerItemDecoration;
    paramBundle = getContext();
    i2 = 1;
    paramView.<init>(paramBundle, i2);
    paramBundle = getContext();
    int i4 = R.drawable.divider_gray;
    paramBundle = android.support.v4.content.b.a(paramBundle, i4);
    paramView.setDrawable(paramBundle);
    a.addItemDecoration(paramView);
    paramView = new com/truecaller/truepay/app/ui/history/views/c/c;
    paramBundle = Y;
    paramView.<init>(paramBundle);
    ab = paramView;
    paramView = new com/truecaller/truepay/app/ui/history/views/c/b;
    paramBundle = Y;
    paramView.<init>(paramBundle);
    ac = paramView;
    paramView = new com/truecaller/truepay/app/ui/history/views/c/a;
    paramBundle = Y;
    paramView.<init>(paramBundle);
    ad = paramView;
    paramView = new java/util/ArrayList;
    paramView.<init>();
    ae = paramView;
    paramView = new com/truecaller/truepay/app/ui/dashboard/views/a/d;
    Object localObject3 = ae;
    Object localObject4 = getContext();
    com.truecaller.truepay.app.ui.history.views.c.c localc = ab;
    com.truecaller.truepay.app.ui.history.views.c.b localb = ac;
    com.truecaller.truepay.app.ui.history.views.c.a locala = ad;
    com.google.gson.f localf = X;
    Object localObject2 = paramView;
    paramView.<init>((List)localObject3, (Context)localObject4, localc, localb, locala, this, localf);
    aa = paramView;
    paramView = a;
    paramBundle = aa;
    paramView.setAdapter(paramBundle);
    l();
    paramView = P;
    paramBundle = f.a();
    localObject2 = "prefFeatureLastSyncTimestamp.get()";
    k.a(paramBundle, (String)localObject2);
    long l1 = paramBundle.longValue();
    long l2 = System.currentTimeMillis() - l1;
    l1 = 21600000L;
    m = 0;
    paramBundle = null;
    boolean bool4 = l2 < l1;
    if (bool4)
    {
      i4 = 1;
    }
    else
    {
      i4 = 0;
      localObject2 = null;
    }
    if (i4 == 0)
    {
      localObject2 = l.a.a();
      localObject3 = io.reactivex.g.a.b();
      localObject2 = ((o)localObject2).b((n)localObject3);
      localObject3 = io.reactivex.android.b.a.a();
      localObject2 = ((o)localObject2).a((n)localObject3);
      localObject3 = new com/truecaller/truepay/app/ui/dashboard/c/c$d;
      ((c.d)localObject3).<init>(paramView);
      localObject3 = (io.reactivex.c.d)localObject3;
      localObject4 = (io.reactivex.c.d)c.e.a;
      localObject2 = ((o)localObject2).a((io.reactivex.c.d)localObject3, (io.reactivex.c.d)localObject4);
      paramView = e;
      paramView.a((io.reactivex.a.b)localObject2);
    }
    setHasOptionsMenu(false);
    paramView = n;
    i4 = R.id.psp_logo;
    paramView = (ImageView)paramView.findViewById(i4);
    localObject2 = W.q();
    boolean bool3 = ((com.truecaller.featuretoggles.b)localObject2).a();
    if (bool3)
    {
      localObject2 = M.a();
      if (localObject2 == null) {
        localObject2 = "icici";
      }
      int i6 = ((String)localObject2).hashCode();
      int i7 = -1396222919;
      if (i6 != i7)
      {
        i2 = 100023093;
        if (i6 == i2)
        {
          localObject1 = "icici";
          boolean bool2 = ((String)localObject2).equals(localObject1);
          if (bool2)
          {
            bool2 = false;
            localObject1 = null;
            break label1634;
          }
        }
      }
      else
      {
        localObject4 = "baroda";
        bool3 = ((String)localObject2).equals(localObject4);
        if (bool3) {
          break label1634;
        }
      }
      int i3 = -1;
      label1634:
      int i5;
      switch (i3)
      {
      default: 
        break;
      case 1: 
        localObject1 = getResources();
        i5 = R.drawable.bob_logo;
        localObject1 = ((Resources)localObject1).getDrawable(i5);
        paramView.setImageDrawable((Drawable)localObject1);
        break;
      case 0: 
        localObject1 = getResources();
        i5 = R.drawable.icici_logo;
        localObject1 = ((Resources)localObject1).getDrawable(i5);
        paramView.setImageDrawable((Drawable)localObject1);
      }
    }
    paramView = new java/util/ArrayList;
    localObject1 = ag;
    paramView.<init>((Collection)localObject1);
    localObject1 = k();
    boolean bool1 = com.truecaller.truepay.app.ui.dashboard.c.c.a(paramView, (List)localObject1);
    if (bool1)
    {
      bool1 = com.truecaller.truepay.app.ui.dashboard.a.a;
      if (!bool1)
      {
        af.a((List)localObject1);
        paramView = af;
        paramView.notifyDataSetChanged();
      }
    }
    j();
    paramView = O.c();
    if (paramView != null)
    {
      H.setVisibility(0);
      localObject1 = G;
      ((TextView)localObject1).setVisibility(0);
      paramBundle = G;
      paramView = h;
      paramBundle.setText(paramView);
    }
    paramView = getArguments();
    if (paramView != null)
    {
      paramView = getArguments();
      bool1 = paramView.getBoolean("show_instant_reward");
      paramBundle = P;
      if (bool1)
      {
        paramView = (com.truecaller.truepay.app.ui.dashboard.views.c.b)d;
        if (paramView != null)
        {
          paramView.g();
          return;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */