package com.truecaller.truepay.app.ui.dashboard.views.c;

import android.support.v4.f.j;
import com.truecaller.truepay.data.api.model.ac;
import java.util.List;

public abstract interface b
  extends com.truecaller.truepay.app.ui.base.views.a
{
  public abstract void a(j paramj);
  
  public abstract void a(com.truecaller.common.payments.a.a parama);
  
  public abstract void a(ac paramac);
  
  public abstract void a(List paramList);
  
  public abstract void b(List paramList);
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract void g();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */