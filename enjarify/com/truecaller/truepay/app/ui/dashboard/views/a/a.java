package com.truecaller.truepay.app.ui.dashboard.views.a;

import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import com.truecaller.truepay.app.ui.dashboard.views.b.c;
import java.util.List;

public final class a
  extends b
{
  private List a;
  private String b;
  
  public a(j paramj, List paramList, String paramString)
  {
    super(paramj, i);
    a = paramList;
    b = paramString;
  }
  
  public final void a(List paramList)
  {
    a = paramList;
    int i = paramList.size();
    c(i);
    b = null;
  }
  
  protected final Fragment b(int paramInt)
  {
    com.truecaller.truepay.app.ui.dashboard.b.a locala = (com.truecaller.truepay.app.ui.dashboard.b.a)a.get(paramInt);
    String str = b;
    return c.a(locala, str);
  }
  
  public final int getItemPosition(Object paramObject)
  {
    return -2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */