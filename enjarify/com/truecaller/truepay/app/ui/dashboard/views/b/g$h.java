package com.truecaller.truepay.app.ui.dashboard.views.b;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import c.u;

final class g$h
  implements View.OnClickListener
{
  g$h(g paramg) {}
  
  public final void onClick(View paramView)
  {
    paramView = a.getActivity();
    if (paramView != null)
    {
      ((AppCompatActivity)paramView).onBackPressed();
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.b.g.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */