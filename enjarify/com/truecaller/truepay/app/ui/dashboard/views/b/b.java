package com.truecaller.truepay.app.ui.dashboard.views.b;

import android.os.Bundle;
import android.view.View;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.fragments.a;

public class b
  extends a
{
  b.a a;
  
  public static b c()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    b localb = new com/truecaller/truepay/app/ui/dashboard/views/b/b;
    localb.<init>();
    localb.setArguments(localBundle);
    return localb;
  }
  
  public final int a()
  {
    return R.layout.fragment_deactivate_confirmation_dialog;
  }
  
  public final void b()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = getTargetFragment();
      boolean bool = localObject1 instanceof b.a;
      if (bool)
      {
        localObject1 = (b.a)getTargetFragment();
        a = ((b.a)localObject1);
        return;
      }
    }
    localObject1 = new java/lang/RuntimeException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("parent fragment should implement ");
    String str = b.a.class.getSimpleName();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((RuntimeException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.btn_cancel;
    paramBundle = paramView.findViewById(i);
    -..Lambda.b.H69EHR7_lmo2f0lMQEkhfD5eBTo localH69EHR7_lmo2f0lMQEkhfD5eBTo = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$b$H69EHR7_lmo2f0lMQEkhfD5eBTo;
    localH69EHR7_lmo2f0lMQEkhfD5eBTo.<init>(this);
    paramBundle.setOnClickListener(localH69EHR7_lmo2f0lMQEkhfD5eBTo);
    i = R.id.btn_confirm;
    paramView = paramView.findViewById(i);
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$b$D4VMlBo6U_GzSoxtm-SvOLZpcTE;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */