package com.truecaller.truepay.app.ui.dashboard.views.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.l.g;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.log.d;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.ui.b.c.a.b;
import com.truecaller.truepay.app.ui.transaction.views.a.q;
import com.truecaller.truepay.app.ui.transaction.views.a.q.a;
import com.truecaller.truepay.app.utils.af;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.av;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.i;
import com.truecaller.utils.n;

public final class c
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements com.truecaller.truepay.app.ui.b.c.a.a, com.truecaller.truepay.app.ui.dashboard.views.c.a, q.a
{
  CardView a;
  ImageView b;
  TextView c;
  TextView d;
  TextView e;
  ProgressBar f;
  public r g;
  public n h;
  public au i;
  public i j;
  public com.truecaller.truepay.app.utils.a k;
  com.truecaller.truepay.app.ui.dashboard.b.a l;
  public com.truecaller.truepay.app.ui.dashboard.c.a n;
  public com.truecaller.truepay.data.e.e o;
  private com.truecaller.truepay.data.api.model.a p;
  private com.getkeepsafe.taptargetview.c q;
  
  public static c a(com.truecaller.truepay.app.ui.dashboard.b.a parama, String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    c localc = new com/truecaller/truepay/app/ui/dashboard/views/b/c;
    localc.<init>();
    localBundle.putSerializable("account", parama);
    localBundle.putSerializable("bank_symbol", paramString);
    localc.setArguments(localBundle);
    return localc;
  }
  
  private void c(String paramString)
  {
    Object localObject = k;
    paramString = ((com.truecaller.truepay.app.utils.a)localObject).a(paramString);
    p = paramString;
    paramString = p;
    if (paramString == null)
    {
      paramString = new java/lang/AssertionError;
      paramString.<init>("Account should not be null");
      d.a(paramString);
      return;
    }
    boolean bool = l;
    if (!bool)
    {
      paramString = p.j;
      bool = f;
      if (bool)
      {
        paramString = new com/truecaller/truepay/app/ui/transaction/views/a/q;
        paramString.<init>();
        paramString.setTargetFragment(this, 1006);
        localObject = getFragmentManager();
        String str = q.class.getSimpleName();
        paramString.show((j)localObject, str);
        return;
      }
    }
    h();
  }
  
  private void d(String paramString)
  {
    com.truecaller.truepay.data.b.a locala = Truepay.getInstance().getAnalyticLoggerHelper();
    String str = l.a;
    locala.b("app_payment_balance_check", "home", paramString, str);
  }
  
  private void h()
  {
    com.truecaller.truepay.app.ui.dashboard.a.a = true;
    com.truecaller.truepay.app.ui.dashboard.c.a locala = n;
    String str = p.a;
    locala.a(str);
  }
  
  private void i()
  {
    Object localObject = l.c;
    c((String)localObject);
    localObject = o;
    String str = com.truecaller.truepay.data.b.a.a();
    ((com.truecaller.truepay.data.e.e)localObject).a(str);
    d("initiated");
  }
  
  public final int a()
  {
    return R.layout.fragment_account_cardview;
  }
  
  public final void a(String paramString)
  {
    com.truecaller.truepay.app.ui.dashboard.a.a = false;
    Object localObject = e;
    int m = R.string.accounts_show_balance;
    String str1 = getString(m);
    ((TextView)localObject).setText(str1);
    localObject = com.truecaller.truepay.app.ui.b.c.a.c;
    int i1 = R.string.account_balance_check_failed;
    localObject = getString(i1);
    m = R.string.set_upi_pin;
    str1 = getString(m);
    int i2 = R.string.retry;
    String str2 = getString(i2);
    a.b.a(this, (String)localObject, paramString, str1, str2);
    d("failure");
  }
  
  public final void a(String paramString1, String paramString2)
  {
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    localf = getActivity();
    paramString1 = com.getkeepsafe.taptargetview.b.a(e, paramString1, paramString2);
    boolean bool = true;
    y = bool;
    x = bool;
    int m = R.color.colorPrimary;
    i = m;
    m = R.color.transparent;
    j = m;
    m = R.color.white;
    l = m;
    m = R.color.white_70;
    m = m;
    A = bool;
    d = 70;
    z = false;
    paramString2 = new com/truecaller/truepay/app/ui/dashboard/views/b/c$1;
    paramString2.<init>(this);
    paramString1 = com.getkeepsafe.taptargetview.c.a(localf, paramString1, paramString2);
    q = paramString1;
  }
  
  public final void a(boolean paramBoolean)
  {
    int m = 4;
    if (paramBoolean)
    {
      f.setVisibility(0);
      e.setVisibility(m);
      return;
    }
    f.setVisibility(m);
    e.setVisibility(0);
  }
  
  public final void b()
  {
    f();
  }
  
  public final void b(String paramString)
  {
    com.truecaller.truepay.app.ui.dashboard.a.a = false;
    e.setText(paramString);
    d("success");
  }
  
  public final void c()
  {
    i();
  }
  
  public final void d()
  {
    Object localObject1 = a;
    float f1 = ((CardView)localObject1).getCardElevation();
    float f2 = 4.0F;
    f1 *= f2;
    ((CardView)localObject1).setMaxCardElevation(f1);
    localObject1 = (com.truecaller.truepay.app.ui.dashboard.b.a)getArguments().getSerializable("account");
    l = ((com.truecaller.truepay.app.ui.dashboard.b.a)localObject1);
    localObject1 = c;
    Object localObject2 = l.a;
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject1 = l;
    boolean bool1 = f;
    if (!bool1)
    {
      localObject1 = d;
      localObject2 = au.a(l.e);
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    else
    {
      localObject1 = d;
      localObject2 = l.e;
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    localObject1 = l;
    bool1 = d;
    boolean bool2;
    if (bool1)
    {
      localObject1 = getActivity();
      int m = R.drawable.ic_primary_set;
      localObject1 = android.support.v4.content.b.a((Context)localObject1, m).mutate();
      localObject2 = d;
      af.a((TextView)localObject2, (Drawable)localObject1);
      localObject1 = l;
      bool1 = f;
      if (!bool1)
      {
        localObject1 = n;
        localObject2 = d;
        if (localObject2 != null)
        {
          localObject2 = f;
          bool2 = ((av)localObject2).b();
          if (!bool2)
          {
            localObject2 = i;
            localObject3 = h;
            Object localObject4 = com.truecaller.featuretoggles.e.a;
            int i2 = 24;
            localObject4 = localObject4[i2];
            localObject2 = ((e.a)localObject3).a((com.truecaller.featuretoggles.e)localObject2, (g)localObject4);
            bool2 = ((com.truecaller.featuretoggles.b)localObject2).a();
            if (bool2)
            {
              localObject2 = (com.truecaller.truepay.app.ui.dashboard.views.c.a)d;
              localObject3 = g;
              int i3 = R.string.title_balance_check_spot_light;
              i2 = 0;
              Object[] arrayOfObject1 = new Object[0];
              localObject3 = ((n)localObject3).a(i3, arrayOfObject1);
              localObject4 = g;
              int i4 = R.string.sub_title_balance_check_spot_light;
              Object[] arrayOfObject2 = new Object[0];
              localObject4 = ((n)localObject4).a(i4, arrayOfObject2);
              ((com.truecaller.truepay.app.ui.dashboard.views.c.a)localObject2).a((String)localObject3, (String)localObject4);
              ((com.truecaller.truepay.app.ui.dashboard.c.a)localObject1).a();
            }
          }
        }
      }
    }
    else
    {
      localObject1 = d;
      bool2 = false;
      f1 = 0.0F;
      localObject2 = null;
      af.a((TextView)localObject1, null);
    }
    localObject1 = b;
    localObject2 = g;
    Object localObject3 = l.b;
    localObject2 = ((r)localObject2).b((String)localObject3);
    ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
    localObject1 = l;
    bool1 = f;
    int i1;
    if (bool1)
    {
      localObject1 = e;
      i1 = R.string.bank_card_add_now;
      localObject2 = getString(i1);
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    else
    {
      localObject1 = e;
      i1 = R.string.accounts_show_balance;
      localObject2 = getString(i1);
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    localObject1 = getArguments();
    localObject2 = "bank_symbol";
    localObject1 = (String)((Bundle)localObject1).getSerializable((String)localObject2);
    boolean bool3 = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool3)
    {
      localObject2 = l.b;
      bool1 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
      if (bool1)
      {
        bool1 = com.truecaller.truepay.app.ui.dashboard.a.a;
        if (!bool1) {
          i();
        }
      }
    }
  }
  
  public final void e()
  {
    com.truecaller.truepay.app.ui.dashboard.a.a = false;
    Object localObject1 = com.truecaller.truepay.app.ui.b.c.a.c;
    int m = R.string.account_balance_check_failed;
    localObject1 = getString(m);
    Object localObject2 = h;
    int i1 = R.string.error_fetch_balance;
    Object localObject3 = new Object[0];
    localObject3 = ((n)localObject2).a(i1, (Object[])localObject3);
    int i2 = R.string.set_upi_pin;
    localObject2 = getString(i2);
    i1 = R.string.retry;
    String str = getString(i1);
    a.b.a(this, (String)localObject1, (String)localObject3, (String)localObject2, str);
    localObject3 = e;
    m = R.string.accounts_show_balance;
    localObject1 = getString(m);
    ((TextView)localObject3).setText((CharSequence)localObject1);
    d("failure");
  }
  
  public final void f()
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      localContext = getContext();
      String str1 = "action.page.forgot_upi_pin";
      com.truecaller.truepay.data.api.model.a locala = p;
      String str2 = "balance_check";
      ManageAccountsActivity.a(localContext, str1, locala, str2);
    }
  }
  
  public final void g()
  {
    Object localObject = p;
    if (localObject == null)
    {
      localObject = new java/lang/AssertionError;
      ((AssertionError)localObject).<init>("Account should not be null");
      d.a((Throwable)localObject);
      return;
    }
    l = true;
    k.c((com.truecaller.truepay.data.api.model.a)localObject);
    h();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.dashboard.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    n.b();
  }
  
  public final void onPause()
  {
    super.onPause();
    com.getkeepsafe.taptargetview.c localc = q;
    if (localc != null)
    {
      boolean bool = true;
      localc.a(bool);
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.cv_root;
    paramBundle = (CardView)paramView.findViewById(m);
    a = paramBundle;
    m = R.id.iv_bank_logo;
    paramBundle = (ImageView)paramView.findViewById(m);
    b = paramBundle;
    m = R.id.tv_bank_name;
    paramBundle = (TextView)paramView.findViewById(m);
    c = paramBundle;
    m = R.id.tv_ac_number;
    paramBundle = (TextView)paramView.findViewById(m);
    d = paramBundle;
    m = R.id.tv_show_bal;
    paramBundle = (TextView)paramView.findViewById(m);
    e = paramBundle;
    m = R.id.progress_wheel;
    paramView = (ProgressBar)paramView.findViewById(m);
    f = paramView;
    paramView = e;
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$c$ZVTGgwFFs68X7z_tmaGKOHmXhMk;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = a;
    paramBundle = new com/truecaller/truepay/app/ui/dashboard/views/b/-$$Lambda$c$pcV3HKDy2GrhSj4cXH4C58GM6YU;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    n.a(this);
    ((com.truecaller.truepay.app.ui.dashboard.views.c.a)n.d).d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */