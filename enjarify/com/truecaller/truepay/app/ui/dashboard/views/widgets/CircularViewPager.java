package com.truecaller.truepay.app.ui.dashboard.views.widgets;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.support.v4.view.ViewPager;
import android.support.v4.view.o;
import android.util.AttributeSet;
import com.truecaller.truepay.R.styleable;

public class CircularViewPager
  extends ViewPager
{
  private final CircularViewPager.a p;
  private int q;
  
  public CircularViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = getContext().getTheme();
    int[] arrayOfInt = R.styleable.CircularViewPager;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
    int i = R.styleable.CircularViewPager_pageCount;
    i = paramContext.getInteger(i, 0);
    q = i;
    paramContext.recycle();
    paramContext = new com/truecaller/truepay/app/ui/dashboard/views/widgets/CircularViewPager$a;
    paramContext.<init>(this, (byte)0);
    p = paramContext;
    setOverScrollMode(2);
    setOffscreenPageLimit(1);
  }
  
  public void setAdapter(o paramo)
  {
    super.setAdapter(paramo);
    paramo = p;
    a(paramo);
    setCurrentItem(1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.views.widgets.CircularViewPager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */