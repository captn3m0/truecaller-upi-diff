package com.truecaller.truepay.app.ui.dashboard.c;

import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.TcPayCreditLoanItem;
import com.truecaller.truepay.app.ui.dashboard.views.c.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

public final class c$c
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private ag e;
  
  public c$c(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/truepay/app/ui/dashboard/c/c$c;
    c localc1 = d;
    localc.<init>(localc1, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = c;
    int j = 1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label481;
      }
      throw a;
    case 1: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label587;
      }
      paramObject = d.p;
      localObject2 = new com/truecaller/truepay/app/ui/dashboard/c/c$c$b;
      ((c.c.b)localObject2).<init>(null);
      localObject2 = (m)localObject2;
      c = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (List)paramObject;
    Object localObject2 = paramObject;
    localObject2 = (Collection)paramObject;
    if (localObject2 != null)
    {
      bool1 = ((Collection)localObject2).isEmpty();
      if (!bool1)
      {
        bool1 = false;
        localObject2 = null;
        break label194;
      }
    }
    boolean bool1 = true;
    label194:
    if (!bool1)
    {
      c.a(false);
      localObject2 = (b)d.af_();
      if (localObject2 != null) {
        ((b)localObject2).b((List)paramObject);
      }
    }
    else
    {
      localObject2 = (b)d.af_();
      if (localObject2 != null) {
        ((b)localObject2).e();
      }
    }
    if (paramObject != null)
    {
      localObject2 = paramObject;
      localObject2 = (Iterable)paramObject;
      Object localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject3 = (Collection)localObject3;
      localObject2 = ((Iterable)localObject2).iterator();
      Object localObject5;
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject2).hasNext();
        if (!bool3) {
          break;
        }
        Object localObject4 = ((Iterator)localObject2).next();
        localObject5 = localObject4;
        localObject5 = ((TcPayCreditLoanItem)localObject4).getStatus();
        String str = "success";
        boolean bool4 = c.g.b.k.a(localObject5, str);
        if (bool4) {
          ((Collection)localObject3).add(localObject4);
        }
      }
      localObject2 = localObject3;
      localObject2 = (List)localObject3;
      int n = ((List)localObject2).size();
      int m = 2;
      if (n < m)
      {
        n = 1;
      }
      else
      {
        n = 0;
        localObject3 = null;
      }
      if (n == 0)
      {
        bool1 = false;
        localObject2 = null;
      }
      if (localObject2 != null)
      {
        localObject3 = d.p;
        localObject5 = new com/truecaller/truepay/app/ui/dashboard/c/c$c$a;
        ((c.c.a)localObject5).<init>(null);
        localObject5 = (m)localObject5;
        a = paramObject;
        b = localObject2;
        c = m;
        paramObject = g.a((f)localObject3, (m)localObject5, this);
        if (paramObject == localObject1) {
          return localObject1;
        }
        label481:
        paramObject = (com.truecaller.common.payments.a.a)paramObject;
        if (paramObject != null)
        {
          localObject1 = (CharSequence)d;
          if (localObject1 != null)
          {
            int k = ((CharSequence)localObject1).length();
            if (k != 0) {
              j = 0;
            }
          }
          if (j == 0)
          {
            localObject1 = (b)d.af_();
            if (localObject1 != null) {
              ((b)localObject1).a((com.truecaller.common.payments.a.a)paramObject);
            }
            localObject1 = d;
            ((c)localObject1).a(false, (com.truecaller.common.payments.a.a)paramObject);
            break label583;
          }
        }
        paramObject = (b)d.af_();
        if (paramObject != null) {
          ((b)paramObject).d();
        }
      }
    }
    label583:
    return x.a;
    label587:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.c.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */