package com.truecaller.truepay.app.ui.dashboard.c;

import c.g.b.k;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.dashboard.views.c.e;
import com.truecaller.utils.n;
import io.reactivex.a.a;
import io.reactivex.a.b;
import io.reactivex.q;

public final class f$a
  implements q
{
  public f$a(f paramf) {}
  
  public final void a(b paramb)
  {
    k.b(paramb, "d");
    e locale = (e)a.af_();
    if (locale != null)
    {
      boolean bool = true;
      locale.a(bool);
    }
    a.e.a(paramb);
  }
  
  public final void a(Throwable paramThrowable)
  {
    k.b(paramThrowable, "e");
    e locale = (e)a.af_();
    if (locale != null)
    {
      paramThrowable = paramThrowable.getMessage();
      if (paramThrowable == null)
      {
        paramThrowable = a.i;
        int i = R.string.settings_deregister_failure;
        Object[] arrayOfObject = new Object[0];
        paramThrowable = paramThrowable.a(i, arrayOfObject);
        String str = "resourceProvider.getStri…tings_deregister_failure)";
        k.a(paramThrowable, str);
      }
      locale.c(paramThrowable);
      locale.a(false);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.c.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */