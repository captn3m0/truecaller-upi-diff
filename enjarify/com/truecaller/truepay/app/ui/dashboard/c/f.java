package com.truecaller.truepay.app.ui.dashboard.c;

import com.truecaller.truepay.a.a.e.g;
import com.truecaller.truepay.app.utils.q;
import com.truecaller.truepay.data.e.c;
import com.truecaller.utils.n;

public final class f
  extends com.truecaller.truepay.app.ui.base.a.a
{
  public final g a;
  public final com.truecaller.truepay.a.a.e.e b;
  public final com.truecaller.truepay.data.e.a c;
  public final com.truecaller.truepay.data.e.a f;
  public final com.truecaller.truepay.data.e.e g;
  public final com.truecaller.truepay.app.utils.a h;
  final n i;
  public final com.truecaller.featuretoggles.e j;
  public final c k;
  private final c l;
  private final q m;
  private final com.truecaller.truepay.f n;
  
  public f(g paramg, com.truecaller.truepay.a.a.e.e parame, c paramc1, q paramq, com.truecaller.truepay.data.e.a parama1, com.truecaller.truepay.data.e.a parama2, com.truecaller.truepay.data.e.e parame1, com.truecaller.truepay.app.utils.a parama, n paramn, com.truecaller.truepay.f paramf, com.truecaller.featuretoggles.e parame2, c paramc2)
  {
    a = paramg;
    b = parame;
    l = paramc1;
    m = paramq;
    c = parama1;
    f = parama2;
    g = parame1;
    h = parama;
    i = paramn;
    n = paramf;
    j = parame2;
    k = paramc2;
  }
  
  public final void a()
  {
    n.a();
    l.a();
    Object localObject = m;
    String str = "upreftils_v1";
    ((q)localObject).b(str);
    localObject = (com.truecaller.truepay.app.ui.dashboard.views.c.e)af_();
    if (localObject != null)
    {
      ((com.truecaller.truepay.app.ui.dashboard.views.c.e)localObject).d();
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    com.truecaller.truepay.data.e.a locala = f;
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    locala.a(localBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.c.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */