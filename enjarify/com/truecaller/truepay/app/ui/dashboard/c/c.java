package com.truecaller.truepay.app.ui.dashboard.c;

import android.support.v4.f.j;
import c.d.f;
import c.g.a.m;
import c.l.g;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.utils.aj;
import com.truecaller.truepay.app.utils.al;
import com.truecaller.truepay.data.api.model.ac;
import io.reactivex.n;
import java.util.List;
import java.util.concurrent.Callable;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;

public final class c
  extends com.truecaller.truepay.app.ui.base.a.a
{
  public ac a;
  public bn b;
  final com.truecaller.truepay.data.e.b c;
  public final com.truecaller.truepay.data.e.b f;
  final com.truecaller.truepay.data.e.e g;
  final com.truecaller.truepay.data.e.a h;
  final com.truecaller.truepay.data.e.e i;
  final com.truecaller.truepay.data.e.a j;
  final com.truecaller.truepay.data.e.a k;
  public final com.truecaller.truepay.a.a.a l;
  final aj m;
  final com.truecaller.truepay.data.e.e n;
  public final f o;
  final f p;
  public final com.truecaller.featuretoggles.e q;
  final al r;
  public final com.truecaller.truepay.data.e.c s;
  private bn t;
  private final com.truecaller.truepay.a.a.c.e u;
  private final com.truecaller.truepay.a.a.c.c v;
  
  public c(com.truecaller.truepay.data.e.b paramb1, com.truecaller.truepay.data.e.b paramb2, com.truecaller.truepay.data.e.e parame1, com.truecaller.truepay.data.e.a parama1, com.truecaller.truepay.data.e.e parame2, com.truecaller.truepay.data.e.a parama2, com.truecaller.truepay.data.e.a parama3, com.truecaller.truepay.a.a.c.e parame, com.truecaller.truepay.a.a.c.c paramc, com.truecaller.truepay.a.a.a parama, aj paramaj, com.truecaller.truepay.data.e.e parame3, f paramf1, f paramf2, com.truecaller.featuretoggles.e parame4, al paramal, com.truecaller.truepay.data.e.c paramc1)
  {
    c = paramb1;
    f = paramb2;
    g = parame1;
    h = parama1;
    i = parame2;
    j = parama2;
    k = parama3;
    u = parame;
    v = paramc;
    l = parama;
    m = paramaj;
    n = parame3;
    o = paramf1;
    p = paramf2;
    q = parame4;
    r = paramal;
    s = paramc1;
  }
  
  public static void a(boolean paramBoolean)
  {
    getInstancecreditHelper.a(paramBoolean);
  }
  
  public static boolean a(List paramList1, List paramList2)
  {
    c.g.b.k.b(paramList1, "prevAccountDOs");
    String str = "accountDOs";
    c.g.b.k.b(paramList2, str);
    int i1 = paramList1.size();
    int i2 = paramList2.size();
    boolean bool1 = true;
    if (i1 != i2) {
      return bool1;
    }
    i1 = 0;
    str = null;
    i2 = 0;
    for (;;)
    {
      int i3 = paramList2.size();
      if (i2 >= i3) {
        break label187;
      }
      boolean bool2 = paramList1.isEmpty();
      if (bool2) {
        break label187;
      }
      Object localObject1 = ((com.truecaller.truepay.app.ui.dashboard.b.a)paramList2.get(i2)).c();
      Object localObject2 = ((com.truecaller.truepay.app.ui.dashboard.b.a)paramList1.get(i2)).c();
      bool2 = c.g.b.k.a(localObject1, localObject2) ^ bool1;
      if (bool2) {
        break;
      }
      localObject1 = (com.truecaller.truepay.app.ui.dashboard.b.a)paramList2.get(i2);
      bool2 = ((com.truecaller.truepay.app.ui.dashboard.b.a)localObject1).d();
      localObject2 = (com.truecaller.truepay.app.ui.dashboard.b.a)paramList1.get(i2);
      boolean bool3 = ((com.truecaller.truepay.app.ui.dashboard.b.a)localObject2).d();
      if (bool2 != bool3) {
        break;
      }
      i2 += 1;
    }
    return bool1;
    label187:
    return false;
  }
  
  public final void a()
  {
    Object localObject1 = v;
    Object localObject2 = new android/support/v4/f/j;
    ((j)localObject2).<init>(null, "recent");
    localObject1 = ((com.truecaller.truepay.a.a.c.c)localObject1).a((j)localObject2);
    localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.d)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.d)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/dashboard/c/c$f;
    ((c.f)localObject2).<init>(this);
    localObject2 = (io.reactivex.c.d)localObject2;
    io.reactivex.c.d locald1 = (io.reactivex.c.d)c.g.a;
    io.reactivex.c.a locala = (io.reactivex.c.a)c.h.a;
    io.reactivex.c.d locald2 = (io.reactivex.c.d)c.i.a;
    localObject1 = ((io.reactivex.d)localObject1).a((io.reactivex.c.d)localObject2, locald1, locala, locald2);
    e.a((io.reactivex.a.b)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.dashboard.views.c.b paramb)
  {
    Object localObject1 = paramb;
    localObject1 = (com.truecaller.truepay.app.ui.base.views.a)paramb;
    super.a((com.truecaller.truepay.app.ui.base.views.a)localObject1);
    localObject1 = q;
    Object localObject2 = s;
    Object localObject3 = com.truecaller.featuretoggles.e.a;
    int i1 = 54;
    localObject3 = localObject3[i1];
    localObject1 = ((e.a)localObject2).a((com.truecaller.featuretoggles.e)localObject1, (g)localObject3);
    boolean bool = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool)
    {
      localObject1 = (ag)bg.a;
      localObject2 = o;
      localObject3 = new com/truecaller/truepay/app/ui/dashboard/c/c$b;
      ((c.b)localObject3).<init>(this, paramb, null);
      localObject3 = (m)localObject3;
      paramb = kotlinx.coroutines.e.b((ag)localObject1, (f)localObject2, (m)localObject3, 2);
      t = paramb;
      return;
    }
    e();
  }
  
  public final void a(boolean paramBoolean, com.truecaller.common.payments.a.a parama)
  {
    c.g.b.k.b(parama, "tcPayCreditBanner");
    Object localObject = q.r();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      localObject = getInstancecreditHelper;
      String str = g;
      parama = b;
      ((com.truecaller.truepay.c)localObject).a(paramBoolean, str, parama);
    }
  }
  
  public final void b()
  {
    bn localbn = t;
    if (localbn != null) {
      localbn.n();
    }
    localbn = b;
    if (localbn != null) {
      localbn.n();
    }
    super.b();
  }
  
  public final void d()
  {
    Object localObject1 = u.a();
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.d)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.d)localObject1).a((n)localObject2);
    localObject2 = (io.reactivex.c.d)c.l.a;
    Object localObject3 = new com/truecaller/truepay/app/ui/dashboard/c/c$m;
    ((c.m)localObject3).<init>(this);
    localObject3 = (io.reactivex.c.d)localObject3;
    io.reactivex.c.a locala = (io.reactivex.c.a)c.n.a;
    io.reactivex.c.d locald = (io.reactivex.c.d)c.o.a;
    localObject1 = ((io.reactivex.d)localObject1).a((io.reactivex.c.d)localObject2, (io.reactivex.c.d)localObject3, locala, locald);
    e.a((io.reactivex.a.b)localObject1);
  }
  
  public final void e()
  {
    Object localObject1 = new com/truecaller/truepay/app/ui/dashboard/c/c$j;
    ((c.j)localObject1).<init>(this);
    localObject1 = io.reactivex.k.a((Callable)localObject1);
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.k)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.k)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/dashboard/c/c$k;
    ((c.k)localObject2).<init>(this);
    localObject2 = (io.reactivex.c.d)localObject2;
    localObject1 = ((io.reactivex.k)localObject1).a((io.reactivex.c.d)localObject2);
    e.a((io.reactivex.a.b)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.dashboard.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */