package com.truecaller.truepay.app.ui.payments.models;

import c.g.b.k;

public final class b
{
  private String a;
  private String b;
  
  public b(String paramString1, String paramString2)
  {
    a = paramString1;
    b = paramString2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof b;
      if (bool1)
      {
        paramObject = (b)paramObject;
        String str1 = a;
        String str2 = a;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = b;
          paramObject = b;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    String str1 = a;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = b;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CancelBookingRequest(refId=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", operationType=");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.models.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */