package com.truecaller.truepay.app.ui.payments.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class RedBusTicket
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  public final String b;
  public final String c;
  public final String d;
  public final List e;
  public final String f;
  private final String g;
  
  static
  {
    RedBusTicket.a locala = new com/truecaller/truepay/app/ui/payments/models/RedBusTicket$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public RedBusTicket(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, List paramList, String paramString6)
  {
    g = paramString1;
    a = paramString2;
    b = paramString3;
    c = paramString4;
    d = paramString5;
    e = paramList;
    f = paramString6;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof RedBusTicket;
      if (bool1)
      {
        paramObject = (RedBusTicket)paramObject;
        Object localObject1 = g;
        Object localObject2 = g;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = a;
          localObject2 = a;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = b;
            localObject2 = b;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = c;
              localObject2 = c;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = d;
                localObject2 = d;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = e;
                  localObject2 = e;
                  bool1 = k.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = f;
                    paramObject = f;
                    boolean bool2 = k.a(localObject1, paramObject);
                    if (bool2) {
                      break label178;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label178:
    return true;
  }
  
  public final int hashCode()
  {
    String str = g;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = a;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = b;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = d;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = e;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = f;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RedBusTicket(bookingId=");
    Object localObject = g;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", bookingAmount=");
    localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", refundableAmount=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", operatorName=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", routeDetails=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", policies=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", message=");
    localObject = f;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = g;
    paramParcel.writeString((String)localObject);
    localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = b;
    paramParcel.writeString((String)localObject);
    localObject = c;
    paramParcel.writeString((String)localObject);
    localObject = d;
    paramParcel.writeString((String)localObject);
    localObject = e;
    if (localObject != null)
    {
      paramParcel.writeInt(1);
      int i = ((Collection)localObject).size();
      paramParcel.writeInt(i);
      localObject = ((Collection)localObject).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject).hasNext();
        if (!bool) {
          break;
        }
        RedBusTicketPolicy localRedBusTicketPolicy = (RedBusTicketPolicy)((Iterator)localObject).next();
        localRedBusTicketPolicy.writeToParcel(paramParcel, 0);
      }
    }
    paramParcel.writeInt(0);
    localObject = f;
    paramParcel.writeString((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.models.RedBusTicket
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */