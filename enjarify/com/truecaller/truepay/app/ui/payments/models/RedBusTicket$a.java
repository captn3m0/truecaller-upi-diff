package com.truecaller.truepay.app.ui.payments.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.ArrayList;

public final class RedBusTicket$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    RedBusTicket localRedBusTicket = new com/truecaller/truepay/app/ui/payments/models/RedBusTicket;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    String str5 = paramParcel.readString();
    int i = paramParcel.readInt();
    if (i != 0)
    {
      i = paramParcel.readInt();
      localArrayList = new java/util/ArrayList;
      localArrayList.<init>(i);
      while (i != 0)
      {
        localObject = (RedBusTicketPolicy)RedBusTicketPolicy.CREATOR.createFromParcel(paramParcel);
        localArrayList.add(localObject);
        i += -1;
      }
    }
    i = 0;
    ArrayList localArrayList = null;
    Object localObject = paramParcel.readString();
    localRedBusTicket.<init>(str1, str2, str3, str4, str5, localArrayList, (String)localObject);
    return localRedBusTicket;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new RedBusTicket[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.models.RedBusTicket.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */