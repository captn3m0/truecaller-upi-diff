package com.truecaller.truepay.app.ui.payments.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class RedBusTicketPolicy$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    RedBusTicketPolicy localRedBusTicketPolicy = new com/truecaller/truepay/app/ui/payments/models/RedBusTicketPolicy;
    String str = paramParcel.readString();
    paramParcel = paramParcel.readString();
    localRedBusTicketPolicy.<init>(str, paramParcel);
    return localRedBusTicketPolicy;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new RedBusTicketPolicy[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.models.RedBusTicketPolicy.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */