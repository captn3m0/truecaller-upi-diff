package com.truecaller.truepay.app.ui.payments.d;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.payments.a.l;

public final class a
  extends com.truecaller.truepay.app.ui.base.views.c.a
  implements View.OnClickListener
{
  public final TextView b;
  public final TextView c;
  public final TextView d;
  public final TextView e;
  
  public a(View paramView, f paramf)
  {
    super(paramView, paramf);
    int i = R.id.tv_talktime_plan;
    paramf = (TextView)paramView.findViewById(i);
    b = paramf;
    i = R.id.tv_validity_plan;
    paramf = (TextView)paramView.findViewById(i);
    c = paramf;
    i = R.id.tv_desc_plan;
    paramf = (TextView)paramView.findViewById(i);
    d = paramf;
    i = R.id.tv_plan_amount;
    paramf = (TextView)paramView.findViewById(i);
    e = paramf;
    paramView.setOnClickListener(this);
  }
  
  public final void onClick(View paramView)
  {
    int i = getAdapterPosition();
    if (i >= 0)
    {
      paramView = (l)a;
      int j = getAdapterPosition();
      paramView.a(j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */