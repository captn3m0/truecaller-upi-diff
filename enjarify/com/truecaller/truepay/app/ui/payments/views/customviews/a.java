package com.truecaller.truepay.app.ui.payments.views.customviews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;

public final class a
{
  public final Context a;
  public final Float b;
  public final Bitmap c;
  public final Paint d;
  private final Integer e;
  
  private a(Context paramContext, Integer paramInteger, Float paramFloat, Bitmap paramBitmap, Paint paramPaint)
  {
    a = paramContext;
    e = paramInteger;
    b = paramFloat;
    c = paramBitmap;
    d = paramPaint;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.customviews.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */