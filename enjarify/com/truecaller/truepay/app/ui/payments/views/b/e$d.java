package com.truecaller.truepay.app.ui.payments.views.b;

import android.support.v7.util.SortedList.Callback;

public final class e$d
  extends SortedList.Callback
{
  e$d(e parame) {}
  
  public final void onChanged(int paramInt1, int paramInt2)
  {
    com.truecaller.truepay.app.ui.payments.a.e locale = e.a(a);
    if (locale != null)
    {
      locale.notifyItemRangeChanged(paramInt1, paramInt2);
      return;
    }
  }
  
  public final void onInserted(int paramInt1, int paramInt2)
  {
    com.truecaller.truepay.app.ui.payments.a.e locale = e.a(a);
    if (locale != null)
    {
      locale.notifyItemRangeInserted(paramInt1, paramInt2);
      return;
    }
  }
  
  public final void onMoved(int paramInt1, int paramInt2)
  {
    com.truecaller.truepay.app.ui.payments.a.e locale = e.a(a);
    if (locale != null)
    {
      locale.notifyItemMoved(paramInt1, paramInt2);
      return;
    }
  }
  
  public final void onRemoved(int paramInt1, int paramInt2)
  {
    com.truecaller.truepay.app.ui.payments.a.e locale = e.a(a);
    if (locale != null)
    {
      locale.notifyItemRangeRemoved(paramInt1, paramInt2);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.e.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */