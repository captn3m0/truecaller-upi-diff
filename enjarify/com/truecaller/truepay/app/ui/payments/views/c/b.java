package com.truecaller.truepay.app.ui.payments.views.c;

import com.truecaller.truepay.app.ui.transaction.b.o;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.n;
import java.util.List;

public abstract interface b
  extends com.truecaller.truepay.app.ui.base.views.a
{
  public abstract void a(o paramo, r paramr, n paramn);
  
  public abstract void a(p paramp);
  
  public abstract void a(r paramr, n paramn, au paramau);
  
  public abstract void a(com.truecaller.truepay.data.api.model.a parama, r paramr, String paramString);
  
  public abstract void a(List paramList);
  
  public abstract void b();
  
  public abstract void b(List paramList);
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract void f();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */