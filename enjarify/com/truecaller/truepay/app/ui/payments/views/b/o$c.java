package com.truecaller.truepay.app.ui.payments.views.b;

import android.support.constraint.ConstraintLayout;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import c.g.b.k;
import com.truecaller.truepay.R.id;

public final class o$c
  extends Animation
{
  o$c(o paramo, int paramInt) {}
  
  protected final void applyTransformation(float paramFloat, Transformation paramTransformation)
  {
    k.b(paramTransformation, "t");
    paramTransformation = a;
    int i = R.id.transactionIdSection;
    paramTransformation = (ConstraintLayout)paramTransformation.a(i);
    String str = "transactionIdSection";
    k.a(paramTransformation, str);
    paramTransformation = paramTransformation.getLayoutParams();
    float f = 1.0F;
    boolean bool = paramFloat < f;
    int k;
    if (!bool)
    {
      k = -2;
      paramFloat = 0.0F / 0.0F;
    }
    else
    {
      int j = b;
      f = j * paramFloat;
      k = (int)f;
    }
    height = k;
    o localo = a;
    int m = R.id.transactionIdSection;
    ((ConstraintLayout)localo.a(m)).requestLayout();
  }
  
  public final boolean willChangeBounds()
  {
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.o.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */