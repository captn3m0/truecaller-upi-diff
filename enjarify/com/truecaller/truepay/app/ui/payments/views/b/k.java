package com.truecaller.truepay.app.ui.payments.views.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.a.i;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.c.v;
import com.truecaller.truepay.app.ui.payments.models.c;
import com.truecaller.truepay.app.ui.payments.models.d;
import java.util.List;

public final class k
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements i, com.truecaller.truepay.app.ui.payments.views.c.g
{
  RecyclerView a;
  public v b;
  private List c;
  private com.truecaller.truepay.app.ui.payments.a.g d;
  private com.truecaller.truepay.app.ui.payments.a.k e;
  
  public static Fragment a(d paramd, com.truecaller.truepay.app.ui.payments.a.k paramk)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("plans_list_key", paramd);
    paramd = new com/truecaller/truepay/app/ui/payments/views/b/k;
    paramd.<init>();
    e = paramk;
    paramd.setArguments(localBundle);
    return paramd;
  }
  
  public final int a()
  {
    return R.layout.fragment_plan_list;
  }
  
  public final void a(c paramc)
  {
    e.a(paramc);
  }
  
  public final void b()
  {
    Object localObject1 = new com/truecaller/truepay/app/ui/payments/a/g;
    ((com.truecaller.truepay.app.ui.payments.a.g)localObject1).<init>(this);
    d = ((com.truecaller.truepay.app.ui.payments.a.g)localObject1);
    localObject1 = d;
    Object localObject2 = c;
    ((com.truecaller.truepay.app.ui.payments.a.g)localObject1).a(localObject2);
    localObject1 = a;
    int i = 1;
    ((RecyclerView)localObject1).setHasFixedSize(i);
    localObject1 = a;
    Object localObject3 = d;
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject3);
    a.setNestedScrollingEnabled(false);
    a.setHasFixedSize(i);
    localObject1 = a;
    LinearLayoutManager localLinearLayoutManager = new android/support/v7/widget/LinearLayoutManager;
    f localf = getActivity();
    localLinearLayoutManager.<init>(localf, i, false);
    ((RecyclerView)localObject1).setLayoutManager(localLinearLayoutManager);
    localObject1 = new android/support/v7/widget/DividerItemDecoration;
    localObject3 = getContext();
    ((DividerItemDecoration)localObject1).<init>((Context)localObject3, i);
    localObject2 = getContext();
    int j = R.drawable.divider_history;
    localObject2 = android.support.v4.content.b.a((Context)localObject2, j);
    ((DividerItemDecoration)localObject1).setDrawable((Drawable)localObject2);
    a.addItemDecoration((RecyclerView.ItemDecoration)localObject1);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    a.a locala = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala1 = Truepay.getApplicationComponent();
    locala.a(locala1).a().a(this);
    b.a(this);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    b.b();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.rv_plans;
    paramView = (RecyclerView)paramView.findViewById(i);
    a = paramView;
    paramView = getArguments();
    if (paramView != null)
    {
      paramView = getArguments();
      paramBundle = "plans_list_key";
      paramView = getSerializableb;
      c = paramView;
    }
    paramView = b;
    paramBundle = d;
    if (paramBundle != null)
    {
      paramView = (com.truecaller.truepay.app.ui.payments.views.c.g)d;
      paramView.b();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */