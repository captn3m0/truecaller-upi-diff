package com.truecaller.truepay.app.ui.payments.views.b;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.common.ui.PausingLottieAnimationView;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.c.a.e;
import com.truecaller.truepay.app.ui.payments.c.a.f;
import com.truecaller.truepay.app.ui.payments.views.c.f;
import com.truecaller.truepay.app.ui.payments.views.customviews.AddShortcutView;
import com.truecaller.truepay.app.ui.payments.views.customviews.AddShortcutView.a;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.data.api.model.ak;
import com.truecaller.utils.extensions.t;
import io.reactivex.o;
import io.reactivex.q;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

public final class a
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements com.truecaller.truepay.app.ui.payments.views.c.a
{
  public static final a.a b;
  public com.truecaller.truepay.app.ui.payments.c.a a;
  private f c;
  private ProgressDialog d;
  private final DecimalFormat e;
  private HashMap f;
  
  static
  {
    a.a locala = new com/truecaller/truepay/app/ui/payments/views/b/a$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public a()
  {
    DecimalFormat localDecimalFormat = new java/text/DecimalFormat;
    localDecimalFormat.<init>("#,###.##");
    e = localDecimalFormat;
  }
  
  public static final a a(p paramp, String paramString)
  {
    k.b(paramp, "txnModel");
    k.b(paramString, "rechargeContext");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramp = (Serializable)paramp;
    localBundle.putSerializable("transaction_details_key", paramp);
    paramString = (Serializable)paramString;
    localBundle.putSerializable("recharge_context_key", paramString);
    paramp = new com/truecaller/truepay/app/ui/payments/views/b/a;
    paramp.<init>();
    paramp.setArguments(localBundle);
    return paramp;
  }
  
  private final void a(int paramInt)
  {
    int i = R.id.buttonActionLeftBillConfirm;
    Button localButton1 = (Button)b(i);
    k.a(localButton1, "buttonActionLeftBillConfirm");
    localButton1.setVisibility(paramInt);
    paramInt = R.id.buttonActionRightBillConfirm;
    Button localButton2 = (Button)b(paramInt);
    k.a(localButton2, "buttonActionRightBillConfirm");
    localButton2.setVisibility(0);
  }
  
  private final void a(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = R.id.buttonActionLeftBillConfirm;
    Button localButton1 = (Button)b(i);
    paramInt1 = android.support.v4.content.b.c(requireContext(), paramInt1);
    localButton1.setTextColor(paramInt1);
    paramInt1 = R.id.buttonActionLeftBillConfirm;
    Button localButton2 = (Button)b(paramInt1);
    k.a(localButton2, "buttonActionLeftBillConfirm");
    Drawable localDrawable = android.support.v4.content.b.a(requireContext(), paramInt2);
    localButton2.setBackground(localDrawable);
    paramInt1 = R.id.buttonActionRightBillConfirm;
    localButton2 = (Button)b(paramInt1);
    k.a(localButton2, "buttonActionRightBillConfirm");
    localDrawable = android.support.v4.content.b.a(requireContext(), paramInt3);
    localButton2.setBackground(localDrawable);
  }
  
  private View b(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  private final void b(String paramString1, String paramString2)
  {
    int i = R.id.tvBillConfirmStatusTitle;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "tvBillConfirmStatusTitle");
    paramString1 = (CharSequence)paramString1;
    localTextView.setText(paramString1);
    int j = R.id.tvBillConfirmStatusSubTitle;
    paramString1 = (TextView)b(j);
    k.a(paramString1, "tvBillConfirmStatusSubTitle");
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
  }
  
  private final void c(l paraml)
  {
    int i = R.id.cardViewTxnSummary;
    Object localObject = (CardView)b(i);
    k.a(localObject, "cardViewTxnSummary");
    ((CardView)localObject).setVisibility(0);
    i = R.id.layoutBillStatus;
    localObject = (LinearLayout)b(i);
    int j = R.color.red_color;
    ((LinearLayout)localObject).setBackgroundResource(j);
    d("lottie_failed.json");
    i = R.id.tvBillConfirmStatusSubTitle;
    localObject = (TextView)b(i);
    String str = "tvBillConfirmStatusSubTitle";
    k.a(localObject, str);
    paraml = (CharSequence)paraml.f();
    ((TextView)localObject).setText(paraml);
    paraml = c;
    if (paraml != null)
    {
      localObject = getResources();
      j = R.color.red_color;
      i = ((Resources)localObject).getColor(j);
      paraml.a(i);
      return;
    }
  }
  
  private void d(String paramString)
  {
    k.b(paramString, "fileName");
    int i = R.id.billConfirmAnimationView;
    PausingLottieAnimationView localPausingLottieAnimationView = (PausingLottieAnimationView)b(i);
    localPausingLottieAnimationView.setAnimation(paramString);
    localPausingLottieAnimationView.a(true);
    localPausingLottieAnimationView.a();
  }
  
  private final void u()
  {
    int i = R.id.cardViewTxnSummary;
    Object localObject = (CardView)b(i);
    k.a(localObject, "cardViewTxnSummary");
    Resources localResources = null;
    ((CardView)localObject).setVisibility(0);
    i = R.id.layoutBillStatus;
    localObject = (LinearLayout)b(i);
    int j = R.color.orange_color;
    ((LinearLayout)localObject).setBackgroundResource(j);
    d("lottie_pending.json");
    localObject = c;
    if (localObject != null)
    {
      localResources = getResources();
      int k = R.color.orange_color;
      j = localResources.getColor(k);
      ((f)localObject).a(j);
      return;
    }
  }
  
  public final int a()
  {
    return R.layout.fragment_bill_pay_confirmation;
  }
  
  public final void a(l paraml)
  {
    k.b(paraml, "payResponseDO");
    a(0);
    int i = R.id.buttonActionLeftBillConfirm;
    Button localButton = (Button)b(i);
    k.a(localButton, "buttonActionLeftBillConfirm");
    Object localObject = paraml.g().get(1);
    k.a(localObject, "payResponseDO.actionData[1]");
    localObject = (CharSequence)((com.truecaller.truepay.app.ui.history.models.a)localObject).a();
    localButton.setText((CharSequence)localObject);
    i = R.id.buttonActionRightBillConfirm;
    localButton = (Button)b(i);
    k.a(localButton, "buttonActionRightBillConfirm");
    paraml = paraml.g().get(0);
    k.a(paraml, "payResponseDO.actionData[0]");
    paraml = (CharSequence)((com.truecaller.truepay.app.ui.history.models.a)paraml).a();
    localButton.setText(paraml);
  }
  
  public final void a(p paramp)
  {
    k.b(paramp, "txnModel");
    Object localObject1 = paramp.a();
    int i = R.id.buttonActionLeftBillConfirm;
    Object localObject2 = (Button)b(i);
    Resources localResources = getResources();
    int j = R.color.light_green_color;
    int k = localResources.getColor(j);
    ((Button)localObject2).setTextColor(k);
    i = R.id.layoutBillStatus;
    localObject2 = (LinearLayout)b(i);
    k = R.color.green_color;
    ((LinearLayout)localObject2).setBackgroundResource(k);
    d("lottie_success.json");
    i = R.id.cardViewTxnSummary;
    localObject2 = (CardView)b(i);
    k.a(localObject2, "cardViewTxnSummary");
    k = 0;
    localResources = null;
    ((CardView)localObject2).setVisibility(0);
    i = R.id.tvBillConfirmStatusTitle;
    localObject2 = (TextView)b(i);
    k.a(localObject2, "tvBillConfirmStatusTitle");
    k.a(localObject1, "payResponseDO");
    Object localObject3 = (CharSequence)((l)localObject1).i();
    ((TextView)localObject2).setText((CharSequence)localObject3);
    i = R.id.tvBillConfirmStatusSubTitle;
    localObject2 = (TextView)b(i);
    localObject3 = "tvBillConfirmStatusSubTitle";
    k.a(localObject2, (String)localObject3);
    localObject1 = (CharSequence)((l)localObject1).f();
    ((TextView)localObject2).setText((CharSequence)localObject1);
    int m = R.id.tvTxnConfirmDebitedFromLabel;
    localObject1 = (TextView)b(m);
    k.a(localObject1, "tvTxnConfirmDebitedFromLabel");
    i = R.string.confirmation_debited_from;
    localObject2 = (CharSequence)getString(i);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = getResources();
      j = R.color.light_green_color;
      i = ((Resources)localObject2).getColor(j);
      ((f)localObject1).a(i);
    }
    localObject1 = paramp.y();
    if (localObject1 != null)
    {
      m = R.id.tvBillDueDate;
      localObject1 = (TextView)b(m);
      localObject2 = "tvBillDueDate";
      k.a(localObject1, (String)localObject2);
      paramp = (CharSequence)paramp.y();
      ((TextView)localObject1).setText(paramp);
      int n = R.id.layoutBillDueDate;
      paramp = (RelativeLayout)b(n);
      k.a(paramp, "layoutBillDueDate");
      paramp.setVisibility(0);
      n = R.id.separatorImageBillDueDate;
      paramp = (ImageView)b(n);
      localObject1 = "separatorImageBillDueDate";
      k.a(paramp, (String)localObject1);
      paramp.setVisibility(0);
    }
  }
  
  public final void a(p paramp, String paramString, r paramr)
  {
    k.b(paramp, "txnModel");
    k.b(paramString, "displayBankName");
    k.b(paramr, "imageLoader");
    int i = R.id.tvBillConfirmAmount;
    Object localObject1 = (TextView)b(i);
    k.a(localObject1, "tvBillConfirmAmount");
    int j = R.string.amount_string_transaction;
    Object localObject2 = new Object[1];
    Object localObject3 = e;
    double d1 = Double.parseDouble(paramp.d());
    localObject3 = ((DecimalFormat)localObject3).format(d1);
    localObject2[0] = localObject3;
    Object localObject4 = (CharSequence)getString(j, (Object[])localObject2);
    ((TextView)localObject1).setText((CharSequence)localObject4);
    i = R.id.tvBillConfirmOperatorName;
    localObject1 = (TextView)b(i);
    k.a(localObject1, "tvBillConfirmOperatorName");
    localObject4 = (CharSequence)paramp.l();
    ((TextView)localObject1).setText((CharSequence)localObject4);
    i = R.id.tvBillConfirmRechargeNumber;
    localObject1 = (TextView)b(i);
    k.a(localObject1, "tvBillConfirmRechargeNumber");
    localObject4 = (CharSequence)paramp.m();
    ((TextView)localObject1).setText((CharSequence)localObject4);
    i = R.id.tvTxnConfirmBankDetails;
    localObject1 = (TextView)b(i);
    k.a(localObject1, "tvTxnConfirmBankDetails");
    paramString = (CharSequence)paramString;
    ((TextView)localObject1).setText(paramString);
    paramString = paramp.x();
    i = R.id.ivVendorLogo;
    localObject1 = (ImageView)b(i);
    k.a(localObject1, "ivVendorLogo");
    localObject4 = getResources();
    int k = R.drawable.ic_place_holder_square;
    localObject4 = ((Resources)localObject4).getDrawable(k);
    k.a(localObject4, "resources.getDrawable(R.…e.ic_place_holder_square)");
    localObject2 = getResources();
    int m = R.drawable.ic_place_holder_square;
    localObject2 = ((Resources)localObject2).getDrawable(m);
    k.a(localObject2, "resources.getDrawable(R.…e.ic_place_holder_square)");
    paramr.a(paramString, (ImageView)localObject1, (Drawable)localObject4, (Drawable)localObject2);
    paramp = paramp.o();
    int n = R.id.ivBillConfirmOperator_logo;
    paramString = (ImageView)b(n);
    k.a(paramString, "ivBillConfirmOperator_logo");
    localObject1 = getResources();
    j = R.drawable.ic_place_holder_circle;
    localObject1 = ((Resources)localObject1).getDrawable(j);
    k.a(localObject1, "resources.getDrawable(R.…e.ic_place_holder_circle)");
    localObject4 = getResources();
    k = R.drawable.ic_place_holder_circle;
    localObject4 = ((Resources)localObject4).getDrawable(k);
    k.a(localObject4, "resources.getDrawable(R.…e.ic_place_holder_circle)");
    paramr.a(paramp, paramString, (Drawable)localObject1, (Drawable)localObject4);
  }
  
  public final void a(p paramp, String paramString1, r paramr, String paramString2, String paramString3)
  {
    k.b(paramp, "txnModel");
    k.b(paramString1, "bankRegisteredName");
    k.b(paramr, "imageLoader");
    k.b(paramString2, "initiatorMsisdn");
    k.b(paramString3, "displayAccNo");
    Object localObject1 = requireContext();
    Object localObject2 = "android.permission.WRITE_EXTERNAL_STORAGE";
    int i = android.support.v4.app.a.a((Context)localObject1, (String)localObject2);
    if (i != 0)
    {
      paramp = (Activity)requireActivity();
      paramString1 = "android.permission.WRITE_EXTERNAL_STORAGE";
      boolean bool1 = android.support.v4.app.a.a(paramp, paramString1);
      if (bool1)
      {
        int j = R.string.read_phone_permission;
        paramp = getString(j);
        paramString1 = null;
        a(paramp, null);
      }
      paramp = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
      requestPermissions(paramp, 1005);
      return;
    }
    d locald = new com/truecaller/truepay/app/ui/payments/views/b/d;
    localObject2 = requireContext();
    k.a(localObject2, "requireContext()");
    localObject1 = locald;
    locald.<init>((Context)localObject2, paramp, paramString1, paramr, paramString2, paramString3);
    boolean bool2 = locald.a();
    paramString1 = a;
    if (paramString1 == null)
    {
      paramr = "presenter";
      k.a(paramr);
    }
    if (bool2)
    {
      paramp = (com.truecaller.truepay.app.ui.payments.views.c.a)paramString1.af_();
      if (paramp != null)
      {
        paramp.s();
        return;
      }
    }
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "account");
    f localf = c;
    if (localf != null)
    {
      localf.b(parama, "other");
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "bankRrn");
    int i = R.id.tvTxnConfirmTransactionId;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "tvTxnConfirmTransactionId");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "transactionId");
    k.b(paramString2, "useId");
    com.truecaller.truepay.app.ui.payments.c.a locala = a;
    if (locala == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    k.b(paramString1, "transactionId");
    k.b(paramString2, "userId");
    Object localObject = new com/truecaller/truepay/data/api/model/ak;
    ((ak)localObject).<init>(paramString1, paramString2);
    paramString1 = k.a((ak)localObject);
    paramString2 = io.reactivex.g.a.b();
    paramString1 = paramString1.b(paramString2);
    paramString2 = io.reactivex.android.b.a.a();
    paramString1 = paramString1.a(paramString2);
    paramString2 = new com/truecaller/truepay/app/ui/payments/c/a$b;
    paramString2.<init>(locala);
    paramString2 = (q)paramString2;
    paramString1.a(paramString2);
  }
  
  public final void a(Throwable paramThrowable)
  {
    boolean bool = isAdded();
    if (bool)
    {
      int i = R.string.error_status_check;
      String str = getString(i);
      a(str, paramThrowable);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.actionLayoutCardView;
    Object localObject = (CardView)b(i);
    k.a(localObject, "actionLayoutCardView");
    int j = 0;
    int k;
    if (paramBoolean)
    {
      k = 0;
      str = null;
    }
    else
    {
      k = 8;
    }
    ((CardView)localObject).setVisibility(k);
    i = R.id.buttonActionRightBillConfirm;
    localObject = (Button)b(i);
    String str = "buttonActionRightBillConfirm";
    k.a(localObject, str);
    if (!paramBoolean) {
      j = 8;
    }
    ((Button)localObject).setVisibility(j);
  }
  
  public final com.truecaller.truepay.app.ui.payments.c.a b()
  {
    com.truecaller.truepay.app.ui.payments.c.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void b(l paraml)
  {
    k.b(paraml, "payResponseDO");
    a(8);
    int i = R.id.buttonActionRightBillConfirm;
    Button localButton = (Button)b(i);
    k.a(localButton, "buttonActionRightBillConfirm");
    paraml = paraml.g().get(0);
    k.a(paraml, "payResponseDO.actionData[0]");
    paraml = (CharSequence)((com.truecaller.truepay.app.ui.history.models.a)paraml).a();
    localButton.setText(paraml);
  }
  
  public final void b(p paramp)
  {
    k.b(paramp, "txnModel");
    paramp = paramp.a();
    int i = R.id.tvBillConfirmStatusTitle;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "tvBillConfirmStatusTitle");
    int j = R.string.confirmation_payment_failed;
    CharSequence localCharSequence = (CharSequence)getString(j);
    localTextView.setText(localCharSequence);
    i = R.id.tvTxnConfirmDebitedFromLabel;
    localTextView = (TextView)b(i);
    k.a(localTextView, "tvTxnConfirmDebitedFromLabel");
    j = R.string.confirmation_debited_from_failure;
    localCharSequence = (CharSequence)getString(j);
    localTextView.setText(localCharSequence);
    k.a(paramp, "payResponseDO");
    c(paramp);
  }
  
  public final void b(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "account");
    f localf = c;
    if (localf != null)
    {
      localf.a(parama, "other");
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "bbpsTxnId");
    int i = R.id.tvTxnConfirmBbpsTransactionId;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "tvTxnConfirmBbpsTransactionId");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c()
  {
    int i = R.id.cardViewAddShortcut;
    CardView localCardView = (CardView)b(i);
    k.a(localCardView, "cardViewAddShortcut");
    t.b((View)localCardView);
  }
  
  public final void c(p paramp)
  {
    k.b(paramp, "txnModel");
    paramp = paramp.a();
    int i = R.id.tvBillConfirmStatusTitle;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "tvBillConfirmStatusTitle");
    k.a(paramp, "payResponseDO");
    CharSequence localCharSequence = (CharSequence)paramp.i();
    localTextView.setText(localCharSequence);
    i = R.id.tvTxnConfirmDebitedFromLabel;
    localTextView = (TextView)b(i);
    k.a(localTextView, "tvTxnConfirmDebitedFromLabel");
    int j = R.string.confirmation_debited_from;
    localCharSequence = (CharSequence)getString(j);
    localTextView.setText(localCharSequence);
    c(paramp);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "faqUrl");
    f localf = c;
    if (localf != null)
    {
      localf.b(paramString);
      return;
    }
  }
  
  public final void d()
  {
    int i = R.id.cardViewAddShortcut;
    CardView localCardView = (CardView)b(i);
    k.a(localCardView, "cardViewAddShortcut");
    t.a((View)localCardView);
  }
  
  public final void d(p paramp)
  {
    k.b(paramp, "txnModel");
    paramp = paramp.a();
    k.a(paramp, "payResponseDO");
    Object localObject = paramp.i();
    k.a(localObject, "payResponseDO.title");
    paramp = paramp.f();
    k.a(paramp, "payResponseDO.message");
    b((String)localObject, paramp);
    int i = R.id.tvTxnConfirmDebitedFromLabel;
    paramp = (TextView)b(i);
    k.a(paramp, "tvTxnConfirmDebitedFromLabel");
    int j = R.string.confirmation_debited_from;
    localObject = (CharSequence)getString(j);
    paramp.setText((CharSequence)localObject);
    u();
  }
  
  public final void e()
  {
    int i = R.id.buttonActionRightBillConfirm;
    Object localObject1 = (Button)b(i);
    k.a(localObject1, "buttonActionRightBillConfirm");
    Object localObject2 = getResources();
    int j = R.drawable.yellow_background;
    localObject2 = ((Resources)localObject2).getDrawable(j);
    ((Button)localObject1).setBackground((Drawable)localObject2);
    i = R.id.buttonActionLeftBillConfirm;
    localObject1 = (Button)b(i);
    localObject2 = getResources();
    j = R.color.orange_color;
    int k = ((Resources)localObject2).getColor(j);
    ((Button)localObject1).setTextColor(k);
    i = R.id.layoutBillStatus;
    localObject1 = (LinearLayout)b(i);
    k = R.color.orange_color;
    ((LinearLayout)localObject1).setBackgroundResource(k);
    d("lottie_pending.json");
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = getResources();
      j = R.color.orange_color;
      k = ((Resources)localObject2).getColor(j);
      ((f)localObject1).a(k);
    }
    i = R.id.tvTxnConfirmDebitedFromLabel;
    localObject1 = (TextView)b(i);
    k.a(localObject1, "tvTxnConfirmDebitedFromLabel");
    k = R.string.confirmation_debited_from_failure;
    localObject2 = (CharSequence)getString(k);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    i = R.string.bill_payment_processing;
    localObject1 = getString(i);
    k.a(localObject1, "getString(R.string.bill_payment_processing)");
    k = R.string.recharge_processing_sub_header;
    localObject2 = getString(k);
    k.a(localObject2, "getString(R.string.recharge_processing_sub_header)");
    b((String)localObject1, (String)localObject2);
  }
  
  public final void f()
  {
    int i = R.string.bill_payment_processing;
    Object localObject1 = getString(i);
    k.a(localObject1, "getString(R.string.bill_payment_processing)");
    int j = R.string.recharge_processing_sub_header;
    Object localObject2 = getString(j);
    k.a(localObject2, "getString(R.string.recharge_processing_sub_header)");
    b((String)localObject1, (String)localObject2);
    i = R.id.tvTxnConfirmDebitedFromLabel;
    localObject1 = (TextView)b(i);
    k.a(localObject1, "tvTxnConfirmDebitedFromLabel");
    j = R.string.confirmation_debited_from_failure;
    localObject2 = (CharSequence)getString(j);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    u();
  }
  
  public final void g()
  {
    int i = R.id.cardViewTxnSummary;
    Object localObject1 = (CardView)b(i);
    k.a(localObject1, "cardViewTxnSummary");
    ((CardView)localObject1).setVisibility(0);
    i = R.string.confirmation_bill_payment_pending_title;
    localObject1 = getString(i);
    k.a(localObject1, "getString(R.string.confi…ll_payment_pending_title)");
    int j = R.string.confirmation_recharge_pending_sub_title;
    Object localObject2 = getString(j);
    String str = "getString(R.string.confi…charge_pending_sub_title)";
    k.a(localObject2, str);
    b((String)localObject1, (String)localObject2);
    i = R.id.tvTxnConfirmDebitedFromLabel;
    localObject1 = (TextView)b(i);
    k.a(localObject1, "tvTxnConfirmDebitedFromLabel");
    j = R.string.confirmation_debited_from_failure;
    localObject2 = (CharSequence)getString(j);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    i = R.id.layoutBillStatus;
    localObject1 = (LinearLayout)b(i);
    j = R.color.orange_color;
    ((LinearLayout)localObject1).setBackgroundResource(j);
    d("lottie_pending.json");
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = getResources();
      int k = R.color.orange_color;
      j = ((Resources)localObject2).getColor(k);
      ((f)localObject1).a(j);
      return;
    }
  }
  
  public final void h()
  {
    a(8);
  }
  
  public final void i()
  {
    ProgressDialog localProgressDialog = d;
    if (localProgressDialog != null)
    {
      Object localObject = (CharSequence)"";
      localProgressDialog.setTitle((CharSequence)localObject);
      localObject = getResources();
      int i = R.string.check_status_text;
      localObject = (CharSequence)((Resources)localObject).getString(i);
      localProgressDialog.setMessage((CharSequence)localObject);
      localProgressDialog.show();
      return;
    }
  }
  
  public final void j()
  {
    ProgressDialog localProgressDialog = d;
    if (localProgressDialog != null)
    {
      localProgressDialog.dismiss();
      return;
    }
  }
  
  public final void k()
  {
    f localf = c;
    if (localf != null)
    {
      localf.a();
      return;
    }
  }
  
  public final void l()
  {
    int i = R.id.layoutBbpsTxnId;
    Object localObject = (RelativeLayout)b(i);
    k.a(localObject, "layoutBbpsTxnId");
    ((RelativeLayout)localObject).setVisibility(0);
    i = R.id.dividerBbpsTxnId;
    localObject = b(i);
    k.a(localObject, "dividerBbpsTxnId");
    ((View)localObject).setVisibility(0);
  }
  
  public final void m()
  {
    int i = R.id.layoutBbpsTxnId;
    Object localObject = (RelativeLayout)b(i);
    k.a(localObject, "layoutBbpsTxnId");
    int j = 8;
    ((RelativeLayout)localObject).setVisibility(j);
    i = R.id.dividerBbpsTxnId;
    localObject = b(i);
    k.a(localObject, "dividerBbpsTxnId");
    ((View)localObject).setVisibility(j);
  }
  
  public final void n()
  {
    int i = R.id.layoutTxnId;
    RelativeLayout localRelativeLayout = (RelativeLayout)b(i);
    k.a(localRelativeLayout, "layoutTxnId");
    localRelativeLayout.setVisibility(0);
  }
  
  public final void o()
  {
    int i = R.id.layoutTxnId;
    RelativeLayout localRelativeLayout = (RelativeLayout)b(i);
    k.a(localRelativeLayout, "layoutTxnId");
    localRelativeLayout.setVisibility(8);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof f;
    if (bool)
    {
      paramContext = (f)getActivity();
      c = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement PaymentsView");
    throw ((Throwable)paramContext);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    c = null;
    com.truecaller.truepay.app.ui.payments.c.a locala = a;
    String str;
    if (locala == null)
    {
      str = "presenter";
      k.a(str);
    }
    if (locala != null) {
      locala.b();
    }
    locala = a;
    if (locala == null)
    {
      str = "presenter";
      k.a(str);
    }
    if (locala != null)
    {
      a.b();
      return;
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject1 = "view";
    k.b(paramView, (String)localObject1);
    super.onViewCreated(paramView, paramBundle);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject1 = "presenter";
      k.a((String)localObject1);
    }
    if (paramBundle != null)
    {
      localObject1 = this;
      localObject1 = (com.truecaller.truepay.app.ui.base.views.a)this;
      paramBundle.a((com.truecaller.truepay.app.ui.base.views.a)localObject1);
    }
    paramBundle = getArguments();
    if (paramBundle == null) {
      return;
    }
    k.a(paramBundle, "arguments ?: return");
    localObject1 = paramBundle.getSerializable("transaction_details_key");
    if (localObject1 != null)
    {
      localObject1 = (p)localObject1;
      String str = "recharge_context_key";
      paramBundle = paramBundle.getString(str, "utilities");
      int i = 2;
      if (localObject1 != null)
      {
        com.truecaller.truepay.app.ui.payments.c.a locala = a;
        Object localObject2;
        if (locala == null)
        {
          localObject2 = "presenter";
          k.a((String)localObject2);
        }
        if (locala != null)
        {
          k.a(paramBundle, "rechargeContext");
          k.b(localObject1, "txnModel");
          k.b(paramBundle, "rechargeContext");
          h = paramBundle;
          k.b(localObject1, "txnModel");
          paramBundle = ((p)localObject1).g();
          boolean bool1 = false;
          localObject2 = null;
          if (paramBundle != null)
          {
            paramBundle = paramBundle.l();
            if (paramBundle != null)
            {
              paramBundle = paramBundle.b();
              break label195;
            }
          }
          boolean bool2 = false;
          paramBundle = null;
          label195:
          Object localObject3 = ((p)localObject1).g();
          if (localObject3 != null) {
            localObject2 = ((com.truecaller.truepay.data.api.model.a)localObject3).d();
          }
          paramBundle = au.a(paramBundle, (String)localObject2);
          localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)locala.af_();
          if (localObject2 != null)
          {
            k.a(paramBundle, "displayBankName");
            localObject3 = n;
            ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).a((p)localObject1, paramBundle, (r)localObject3);
          }
          locala.a((p)localObject1);
          paramBundle = j;
          localObject2 = ((p)localObject1).c();
          paramBundle.d((String)localObject2);
          localObject2 = ((p)localObject1).g();
          paramBundle.a((com.truecaller.truepay.data.api.model.a)localObject2);
          localObject2 = ((p)localObject1).y();
          paramBundle.s((String)localObject2);
          localObject2 = ((p)localObject1).e();
          paramBundle.a((com.truecaller.truepay.app.ui.transaction.b.n)localObject2);
          localObject2 = ((p)localObject1).l();
          paramBundle.j((String)localObject2);
          localObject2 = ((p)localObject1).m();
          paramBundle.k((String)localObject2);
          localObject2 = ((p)localObject1).o();
          paramBundle.m((String)localObject2);
          localObject2 = ((p)localObject1).x();
          paramBundle.r((String)localObject2);
          localObject2 = ((p)localObject1).d();
          paramBundle.e((String)localObject2);
          localObject2 = ((p)localObject1).a();
          paramBundle.a((l)localObject2);
          localObject2 = i;
          localObject3 = new java/util/Date;
          ((Date)localObject3).<init>();
          localObject2 = ((SimpleDateFormat)localObject2).format((Date)localObject3);
          paramBundle.g((String)localObject2);
          paramBundle = o;
          localObject2 = Boolean.TRUE;
          paramBundle.a((Boolean)localObject2);
          paramBundle = ((p)localObject1).a();
          if (paramBundle != null)
          {
            k.b(paramBundle, "payResponseDO");
            localObject2 = (CharSequence)paramBundle.b();
            boolean bool3 = true;
            if (localObject2 != null)
            {
              bool1 = c.n.m.a((CharSequence)localObject2);
              if (!bool1)
              {
                bool1 = false;
                localObject2 = null;
                break label500;
              }
            }
            bool1 = true;
            label500:
            Object localObject4;
            Object localObject5;
            if (!bool1)
            {
              localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)locala.af_();
              if (localObject2 != null)
              {
                localObject4 = paramBundle.b();
                localObject5 = "it.bankRRN";
                k.a(localObject4, (String)localObject5);
                ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).a((String)localObject4);
              }
              localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)locala.af_();
              if (localObject2 != null) {
                ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).n();
              }
            }
            else
            {
              localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)locala.af_();
              if (localObject2 != null) {
                ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).o();
              }
            }
            bool1 = ((p)localObject1).p();
            if (bool1)
            {
              paramBundle = (com.truecaller.truepay.app.ui.payments.views.c.a)locala.af_();
              if (paramBundle != null) {
                paramBundle.a(bool3);
              }
            }
            else
            {
              localObject2 = "pending";
              localObject4 = paramBundle.d();
              bool1 = k.a(localObject2, localObject4);
              if (!bool1)
              {
                localObject2 = "success";
                localObject4 = paramBundle.d();
                bool1 = k.a(localObject2, localObject4);
                if (!bool1)
                {
                  locala.a(paramBundle);
                  break label1218;
                }
              }
              paramBundle = new com/truecaller/truepay/app/ui/history/models/a;
              paramBundle.<init>();
              localObject2 = p;
              int k = R.string.action_home;
              localObject5 = new Object[0];
              localObject2 = ((com.truecaller.utils.n)localObject2).a(k, (Object[])localObject5);
              paramBundle.a((String)localObject2);
              paramBundle.b("action.page.home");
              localObject2 = new com/truecaller/truepay/app/ui/history/models/a;
              ((com.truecaller.truepay.app.ui.history.models.a)localObject2).<init>();
              localObject4 = p;
              int m = R.string.check_status_confirmation;
              Object[] arrayOfObject = new Object[0];
              localObject4 = ((com.truecaller.utils.n)localObject4).a(m, arrayOfObject);
              ((com.truecaller.truepay.app.ui.history.models.a)localObject2).a((String)localObject4);
              ((com.truecaller.truepay.app.ui.history.models.a)localObject2).b("action.check_status");
              localObject4 = new com.truecaller.truepay.app.ui.history.models.a[i];
              localObject4[0] = paramBundle;
              localObject4[bool3] = localObject2;
              paramBundle = c.a.m.b((Object[])localObject4);
              localObject2 = ((p)localObject1).a();
              if (localObject2 != null) {
                ((l)localObject2).a(paramBundle);
              }
              localObject2 = j.a();
              if (localObject2 != null) {
                ((l)localObject2).a(paramBundle);
              }
              paramBundle = (CharSequence)((p)localObject1).f();
              if (paramBundle != null)
              {
                bool2 = c.n.m.a(paramBundle);
                if (!bool2)
                {
                  bool2 = false;
                  paramBundle = null;
                  break label899;
                }
              }
              bool2 = true;
              label899:
              if (!bool2)
              {
                paramBundle = (CharSequence)l.a();
                if (paramBundle != null)
                {
                  bool2 = c.n.m.a(paramBundle);
                  if (!bool2)
                  {
                    bool3 = false;
                    localObject3 = null;
                  }
                }
                if (!bool3)
                {
                  paramBundle = j;
                  localObject2 = ((p)localObject1).f();
                  paramBundle.f((String)localObject2);
                  paramBundle = (com.truecaller.truepay.app.ui.payments.views.c.a)locala.af_();
                  if (paramBundle != null) {
                    paramBundle.a(false);
                  }
                  paramBundle = ((p)localObject1).f();
                  k.a(paramBundle, "txnModel.transactionId");
                  localObject2 = l.a();
                  if (localObject2 == null) {
                    k.a();
                  }
                  k.a(localObject2, "prefUserId.get()!!");
                  localObject3 = new com/truecaller/truepay/data/api/model/ak;
                  ((ak)localObject3).<init>(paramBundle, (String)localObject2);
                  paramBundle = new com/truecaller/truepay/app/ui/payments/c/a$c;
                  paramBundle.<init>(locala, (ak)localObject3);
                  paramBundle = o.a((Callable)paramBundle);
                  localObject2 = new com/truecaller/truepay/app/ui/payments/c/a$a;
                  int j = b;
                  ((com.truecaller.truepay.app.ui.payments.c.a.a)localObject2).<init>(j);
                  localObject2 = (io.reactivex.c.e)localObject2;
                  paramBundle = paramBundle.c((io.reactivex.c.e)localObject2);
                  localObject2 = io.reactivex.g.a.b();
                  paramBundle = paramBundle.b((io.reactivex.n)localObject2);
                  localObject2 = io.reactivex.android.b.a.a();
                  paramBundle = paramBundle.a((io.reactivex.n)localObject2);
                  localObject2 = new com/truecaller/truepay/app/ui/payments/c/a$d;
                  ((com.truecaller.truepay.app.ui.payments.c.a.d)localObject2).<init>(locala, (p)localObject1);
                  localObject2 = (io.reactivex.c.d)localObject2;
                  localObject1 = new com/truecaller/truepay/app/ui/payments/c/a$e;
                  ((a.e)localObject1).<init>(locala);
                  localObject1 = (io.reactivex.c.d)localObject1;
                  localObject3 = (io.reactivex.c.a)a.f.a;
                  paramBundle = paramBundle.a((io.reactivex.c.d)localObject2, (io.reactivex.c.d)localObject1, (io.reactivex.c.a)localObject3);
                  localObject1 = e;
                  ((io.reactivex.a.a)localObject1).a(paramBundle);
                  break label1218;
                }
              }
              paramBundle = new java/lang/AssertionError;
              localObject1 = "transaction id or user id should not be null";
              paramBundle.<init>(localObject1);
              com.truecaller.log.d.a((Throwable)paramBundle);
              paramBundle = (com.truecaller.truepay.app.ui.payments.views.c.a)locala.af_();
              if (paramBundle != null) {
                paramBundle.g();
              }
            }
          }
          label1218:
          paramBundle = q.ab();
          bool2 = paramBundle.a();
          if (bool2)
          {
            paramBundle = (com.truecaller.truepay.app.ui.payments.views.c.a)locala.af_();
            if (paramBundle != null) {
              paramBundle.d();
            }
          }
          else
          {
            paramBundle = (com.truecaller.truepay.app.ui.payments.views.c.a)locala.af_();
            if (paramBundle != null) {
              paramBundle.c();
            }
          }
        }
      }
      else
      {
        paramBundle = new java/lang/AssertionError;
        localObject1 = "txnModel should not null";
        paramBundle.<init>(localObject1);
        paramBundle = (Throwable)paramBundle;
        com.truecaller.log.d.a(paramBundle);
      }
      t.a(paramView, false, i);
      int n = R.id.buttonActionLeftBillConfirm;
      paramView = (Button)b(n);
      paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/a$b;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      n = R.id.buttonActionRightBillConfirm;
      paramView = (Button)b(n);
      paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/a$c;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      n = R.id.addShortcutView;
      paramView = (AddShortcutView)b(n);
      paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/a$d;
      paramBundle.<init>(this);
      paramBundle = (AddShortcutView.a)paramBundle;
      paramView.setAddClickListener(paramBundle);
      paramView = new android/app/ProgressDialog;
      paramBundle = requireContext();
      paramView.<init>(paramBundle);
      d = paramView;
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.transaction.models.TransactionModel");
    throw paramView;
  }
  
  public final void p()
  {
    int i = R.color.white;
    int j = R.drawable.rounded_green_border;
    int k = R.drawable.green_background;
    a(i, j, k);
  }
  
  public final void q()
  {
    int i = R.color.red_color;
    int j = R.drawable.rounded_red_border;
    int k = R.drawable.red_background;
    a(i, j, k);
  }
  
  public final void r()
  {
    int i = R.color.orange_color;
    int j = R.drawable.rounded_yellow_border;
    int k = R.drawable.yellow_background;
    a(i, j, k);
  }
  
  public final void s()
  {
    int i = R.string.receipt_download_message;
    String str = getString(i);
    a(str, null);
  }
  
  public final void t()
  {
    boolean bool = true;
    com.truecaller.truepay.app.ui.payments.a.a = bool;
    f localf = c;
    if (localf != null)
    {
      localf.a();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */