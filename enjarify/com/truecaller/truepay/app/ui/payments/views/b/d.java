package com.truecaller.truepay.app.ui.payments.views.b;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore.Images.Media;
import android.support.constraint.ConstraintLayout;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.r;
import java.io.OutputStream;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class d
{
  public static final d.a a;
  private static final String h = "AM";
  private static final String i = "PM";
  private final Context b;
  private final p c;
  private final String d;
  private final r e;
  private final String f;
  private final String g;
  
  static
  {
    d.a locala = new com/truecaller/truepay/app/ui/payments/views/b/d$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public d(Context paramContext, p paramp, String paramString1, r paramr, String paramString2, String paramString3)
  {
    b = paramContext;
    c = paramp;
    d = paramString1;
    e = paramr;
    f = paramString2;
    g = paramString3;
  }
  
  private Bitmap a(int paramInt)
  {
    ConstraintLayout localConstraintLayout = new android/support/constraint/ConstraintLayout;
    Object localObject1 = b;
    localConstraintLayout.<init>((Context)localObject1);
    localObject1 = b;
    int j = 0;
    Object localObject2 = null;
    Object localObject3;
    int k;
    if (localObject1 != null)
    {
      localObject3 = "layout_inflater";
      localObject1 = ((Context)localObject1).getSystemService((String)localObject3);
    }
    else
    {
      k = 0;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = (LayoutInflater)localObject1;
      int m = R.layout.layout_bill_download_receipt;
      Object localObject4 = localConstraintLayout;
      localObject4 = (ViewGroup)localConstraintLayout;
      boolean bool1 = true;
      localObject1 = ((LayoutInflater)localObject1).inflate(m, (ViewGroup)localObject4, bool1);
      localObject3 = new java/text/SimpleDateFormat;
      Object localObject5 = Locale.US;
      ((SimpleDateFormat)localObject3).<init>("yyyy-MM-dd HH:mm:ss", (Locale)localObject5);
      localObject4 = e;
      localObject5 = c.x();
      k.a(localObject1, "inflatedView");
      int n = R.id.ivBillReceiptVendorLogo;
      Object localObject6 = (ImageView)((View)localObject1).findViewById(n);
      k.a(localObject6, "inflatedView.ivBillReceiptVendorLogo");
      int i1 = R.drawable.ic_place_holder_square;
      ((r)localObject4).a((String)localObject5, (ImageView)localObject6, i1, i1);
      localObject4 = e;
      localObject5 = c.o();
      n = R.id.ivBillReceiptOperatorLogo;
      localObject6 = (ImageView)((View)localObject1).findViewById(n);
      Object localObject7 = "inflatedView.ivBillReceiptOperatorLogo";
      k.a(localObject6, (String)localObject7);
      i1 = R.drawable.ic_place_holder_square;
      ((r)localObject4).a((String)localObject5, (ImageView)localObject6, i1, i1);
      localObject4 = (CharSequence)c.y();
      localObject5 = null;
      if (localObject4 != null)
      {
        bool2 = m.a((CharSequence)localObject4);
        if (!bool2)
        {
          bool2 = false;
          localObject4 = null;
          break label277;
        }
      }
      boolean bool2 = true;
      label277:
      if (!bool2)
      {
        i2 = R.id.tvReceiptBillDueDate;
        localObject4 = (TextView)((View)localObject1).findViewById(i2);
        k.a(localObject4, "tvReceiptBillDueDate");
        localObject6 = (CharSequence)c.y();
        ((TextView)localObject4).setText((CharSequence)localObject6);
      }
      else
      {
        i2 = R.id.layoutReceiptBillDueDate;
        localObject4 = (RelativeLayout)((View)localObject1).findViewById(i2);
        localObject6 = "layoutReceiptBillDueDate";
        k.a(localObject4, (String)localObject6);
        n = 8;
        ((RelativeLayout)localObject4).setVisibility(n);
        i2 = R.id.separatorBillDueDate;
        localObject4 = (ImageView)((View)localObject1).findViewById(i2);
        localObject7 = "separatorBillDueDate";
        k.a(localObject4, (String)localObject7);
        ((ImageView)localObject4).setVisibility(n);
      }
      int i2 = R.id.tvReceiptMobileNumber;
      localObject4 = (TextView)((View)localObject1).findViewById(i2);
      k.a(localObject4, "tvReceiptMobileNumber");
      localObject6 = (CharSequence)f;
      ((TextView)localObject4).setText((CharSequence)localObject6);
      i2 = R.id.tvReceiptCustomerName;
      localObject4 = (TextView)((View)localObject1).findViewById(i2);
      k.a(localObject4, "tvReceiptCustomerName");
      localObject6 = (CharSequence)d;
      ((TextView)localObject4).setText((CharSequence)localObject6);
      i2 = R.id.tvTxnReceiptBankDetails;
      localObject4 = (TextView)((View)localObject1).findViewById(i2);
      k.a(localObject4, "tvTxnReceiptBankDetails");
      localObject6 = (CharSequence)g;
      ((TextView)localObject4).setText((CharSequence)localObject6);
      i2 = R.id.tvBillReceiptOperatorName;
      localObject4 = (TextView)((View)localObject1).findViewById(i2);
      k.a(localObject4, "tvBillReceiptOperatorName");
      localObject6 = (CharSequence)c.l();
      ((TextView)localObject4).setText((CharSequence)localObject6);
      i2 = R.id.tvBillReceiptRechargeNumber;
      localObject4 = (TextView)((View)localObject1).findViewById(i2);
      k.a(localObject4, "tvBillReceiptRechargeNumber");
      localObject6 = (CharSequence)c.m();
      ((TextView)localObject4).setText((CharSequence)localObject6);
      i2 = R.id.tvReceiptBbpsTransactionId;
      localObject4 = (TextView)((View)localObject1).findViewById(i2);
      k.a(localObject4, "tvReceiptBbpsTransactionId");
      localObject6 = c.a();
      if (localObject6 != null)
      {
        localObject6 = ((l)localObject6).j();
      }
      else
      {
        n = 0;
        localObject6 = null;
      }
      localObject6 = (CharSequence)localObject6;
      ((TextView)localObject4).setText((CharSequence)localObject6);
      i2 = R.id.tvReceiptTransactionId;
      localObject4 = (TextView)((View)localObject1).findViewById(i2);
      k.a(localObject4, "tvReceiptTransactionId");
      localObject6 = c.a();
      if (localObject6 != null) {
        localObject2 = ((l)localObject6).b();
      }
      localObject2 = (CharSequence)localObject2;
      ((TextView)localObject4).setText((CharSequence)localObject2);
      j = R.id.tvBillReceiptAmount;
      localObject2 = (TextView)((View)localObject1).findViewById(j);
      k.a(localObject2, "tvBillReceiptAmount");
      localObject4 = ((View)localObject1).getContext();
      n = R.string.rs_amount;
      localObject7 = new Object[bool1];
      String str = c.d();
      localObject7[0] = str;
      localObject4 = (CharSequence)((Context)localObject4).getString(n, (Object[])localObject7);
      ((TextView)localObject2).setText((CharSequence)localObject4);
      localObject2 = c.h();
      try
      {
        localObject4 = ((SimpleDateFormat)localObject3).parse((String)localObject2);
        localObject6 = new java/text/DateFormatSymbols;
        localObject7 = Locale.getDefault();
        ((DateFormatSymbols)localObject6).<init>((Locale)localObject7);
        i1 = 2;
        localObject7 = new String[i1];
        str = h;
        localObject7[0] = str;
        str = i;
        localObject7[bool1] = str;
        ((DateFormatSymbols)localObject6).setAmPmStrings((String[])localObject7);
        ((SimpleDateFormat)localObject3).setDateFormatSymbols((DateFormatSymbols)localObject6);
        m = R.id.tvConfirmBbpsTxnDate;
        localObject3 = ((View)localObject1).findViewById(m);
        localObject3 = (TextView)localObject3;
        Object localObject8 = "inflatedView.tvConfirmBbpsTxnDate";
        k.a(localObject3, (String)localObject8);
        localObject8 = "d MMM yyyy | h:mm a";
        localObject8 = (CharSequence)localObject8;
        localObject4 = DateFormat.format((CharSequence)localObject8, (Date)localObject4);
        ((TextView)localObject3).setText((CharSequence)localObject4);
      }
      catch (ParseException localParseException)
      {
        m = R.id.tvConfirmBbpsTxnDate;
        localObject1 = (TextView)((View)localObject1).findViewById(m);
        localObject3 = "inflatedView.tvConfirmBbpsTxnDate";
        k.a(localObject1, (String)localObject3);
        localObject2 = (CharSequence)localObject2;
        ((TextView)localObject1).setText((CharSequence)localObject2);
      }
      localObject1 = new android/widget/RelativeLayout$LayoutParams;
      ((RelativeLayout.LayoutParams)localObject1).<init>(-1, -2);
      localObject1 = (ViewGroup.LayoutParams)localObject1;
      localConstraintLayout.setLayoutParams((ViewGroup.LayoutParams)localObject1);
      paramInt = View.MeasureSpec.makeMeasureSpec(paramInt, -1 << -1);
      k = View.MeasureSpec.makeMeasureSpec(0, 0);
      localConstraintLayout.measure(paramInt, k);
      paramInt = localConstraintLayout.getMeasuredWidth();
      k = localConstraintLayout.getMeasuredHeight();
      localConstraintLayout.layout(0, 0, paramInt, k);
      paramInt = localConstraintLayout.getMeasuredWidth();
      k = localConstraintLayout.getMeasuredHeight();
      localObject2 = Bitmap.Config.ARGB_8888;
      localObject9 = Bitmap.createBitmap(paramInt, k, (Bitmap.Config)localObject2);
      localObject1 = new android/graphics/Canvas;
      ((Canvas)localObject1).<init>((Bitmap)localObject9);
      localConstraintLayout.draw((Canvas)localObject1);
      k.a(localObject9, "bitmap");
      return (Bitmap)localObject9;
    }
    Object localObject9 = new c/u;
    ((u)localObject9).<init>("null cannot be cast to non-null type android.view.LayoutInflater");
    throw ((Throwable)localObject9);
  }
  
  public final boolean a()
  {
    Object localObject1 = Resources.getSystem();
    k.a(localObject1, "Resources.getSystem()");
    int j = getDisplayMetricswidthPixels;
    localObject1 = a(j);
    if (localObject1 != null)
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>("android.intent.action.SEND");
      localIntent.setType("image/jpeg");
      Object localObject2 = new android/content/ContentValues;
      ((ContentValues)localObject2).<init>();
      ((ContentValues)localObject2).put("title", "title");
      Object localObject3 = "mime_type";
      Object localObject4 = "image/jpeg";
      ((ContentValues)localObject2).put((String)localObject3, (String)localObject4);
      try
      {
        localObject3 = b;
        int k = 0;
        localObject4 = null;
        if (localObject3 != null)
        {
          localObject3 = ((Context)localObject3).getContentResolver();
          if (localObject3 != null)
          {
            Uri localUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            localObject2 = ((ContentResolver)localObject3).insert(localUri, (ContentValues)localObject2);
            break label139;
          }
        }
        localObject2 = null;
        label139:
        localObject3 = b;
        if (localObject3 != null)
        {
          localObject3 = ((Context)localObject3).getContentResolver();
          if (localObject3 != null) {
            localObject4 = ((ContentResolver)localObject3).openOutputStream((Uri)localObject2);
          }
        }
        localObject3 = Bitmap.CompressFormat.JPEG;
        int m = 100;
        ((Bitmap)localObject1).compress((Bitmap.CompressFormat)localObject3, m, (OutputStream)localObject4);
        if (localObject4 != null) {
          ((OutputStream)localObject4).close();
        }
        localObject1 = "android.intent.extra.STREAM";
        localObject2 = (Parcelable)localObject2;
        localIntent.putExtra((String)localObject1, (Parcelable)localObject2);
        j = 1;
        localIntent.addFlags(j);
        localObject2 = b;
        if (localObject2 != null)
        {
          localObject3 = b;
          k = R.string.share_image;
          localObject3 = ((Context)localObject3).getString(k);
          localObject3 = (CharSequence)localObject3;
          localIntent = Intent.createChooser(localIntent, (CharSequence)localObject3);
          ((Context)localObject2).startActivity(localIntent);
        }
        return j;
      }
      catch (Exception localException)
      {
        return false;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */