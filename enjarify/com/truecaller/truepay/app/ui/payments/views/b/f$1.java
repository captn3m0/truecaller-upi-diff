package com.truecaller.truepay.app.ui.payments.views.b;

import android.support.v7.util.SortedList.Callback;

final class f$1
  extends SortedList.Callback
{
  f$1(f paramf) {}
  
  public final void onChanged(int paramInt1, int paramInt2)
  {
    f.a(a).notifyItemRangeChanged(paramInt1, paramInt2);
  }
  
  public final void onInserted(int paramInt1, int paramInt2)
  {
    f.a(a).notifyItemRangeInserted(paramInt1, paramInt2);
  }
  
  public final void onMoved(int paramInt1, int paramInt2)
  {
    f.a(a).notifyItemMoved(paramInt1, paramInt2);
  }
  
  public final void onRemoved(int paramInt1, int paramInt2)
  {
    f.a(a).notifyItemRangeRemoved(paramInt1, paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.f.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */