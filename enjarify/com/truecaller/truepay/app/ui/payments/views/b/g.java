package com.truecaller.truepay.app.ui.payments.views.b;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.base.widgets.CircleImageView;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.c.l.a;
import com.truecaller.truepay.app.ui.payments.c.l.b;
import com.truecaller.truepay.app.ui.payments.views.c.f;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.r;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.HashMap;

public final class g
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements l.b
{
  public static final g.a c;
  public l.a a;
  public r b;
  private f d;
  private final DecimalFormat e;
  private HashMap f;
  
  static
  {
    g.a locala = new com/truecaller/truepay/app/ui/payments/views/b/g$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public g()
  {
    DecimalFormat localDecimalFormat = new java/text/DecimalFormat;
    localDecimalFormat.<init>("#,###.##");
    e = localDecimalFormat;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final g a(p paramp, String paramString)
  {
    k.b(paramp, "txnModel");
    k.b(paramString, "rechargeContext");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramp = (Serializable)paramp;
    localBundle.putSerializable("transaction_details_key", paramp);
    localBundle.putString("recharge_context_key", paramString);
    paramp = new com/truecaller/truepay/app/ui/payments/views/b/g;
    paramp.<init>();
    paramp.setArguments(localBundle);
    return paramp;
  }
  
  public final int a()
  {
    return R.layout.fragment_payment_confirm;
  }
  
  public final void a(p paramp)
  {
    k.b(paramp, "txnModel");
    f localf = d;
    if (localf != null)
    {
      localf.a(paramp);
      return;
    }
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "selectedBank");
    l.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    if (locala != null)
    {
      locala.a(parama);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "bankName");
    int i = R.id.tvBankName;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvBankName");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "upiIntentUrl");
    k.b(paramString2, "upiIntentTitle");
    Intent localIntent = new android/content/Intent;
    paramString1 = Uri.parse(paramString1);
    localIntent.<init>("android.intent.action.VIEW", paramString1);
    paramString2 = (CharSequence)paramString2;
    paramString1 = Intent.createChooser(localIntent, paramString2);
    startActivityForResult(paramString1, 5001);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    k.b(paramString1, "utilityOpName");
    k.b(paramString2, "amount");
    k.b(paramString3, "operatorImageUrl");
    int i = R.id.tvName;
    Object localObject1 = (TextView)a(i);
    Object localObject2 = "tvName";
    k.a(localObject1, (String)localObject2);
    paramString1 = (CharSequence)paramString1;
    ((TextView)localObject1).setText(paramString1);
    paramString1 = m.a(paramString2);
    int k;
    if (paramString1 != null)
    {
      double d1 = ((Number)paramString1).doubleValue();
      i = R.id.tvAmount;
      localObject1 = (EditText)a(i);
      localObject2 = e;
      paramString1 = (CharSequence)((DecimalFormat)localObject2).format(d1);
      ((EditText)localObject1).setText(paramString1);
      int j = R.id.tvAmount;
      paramString1 = (EditText)a(j);
      k.a(paramString1, "tvAmount");
      k = 0;
      paramString2 = null;
      paramString1.setEnabled(false);
    }
    paramString1 = getContext();
    if (paramString1 != null)
    {
      paramString1 = b;
      if (paramString1 == null)
      {
        paramString2 = "imageLoader";
        k.a(paramString2);
      }
      k = R.id.imgProfile;
      paramString2 = (CircleImageView)a(k);
      localObject1 = "imgProfile";
      k.a(paramString2, (String)localObject1);
      paramString2 = (ImageView)paramString2;
      paramString1.a(paramString3, paramString2, paramInt, paramInt);
    }
  }
  
  public final void b()
  {
    f localf = d;
    if (localf != null)
    {
      localf.a();
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "bankSymbol");
    int i = R.id.imgBank;
    ImageView localImageView = (ImageView)a(i);
    r localr = b;
    if (localr == null)
    {
      String str = "imageLoader";
      k.a(str);
    }
    paramString = localr.b(paramString);
    localImageView.setImageDrawable(paramString);
  }
  
  public final void c()
  {
    f localf = d;
    if (localf != null)
    {
      localf.b();
      return;
    }
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "message");
    a(paramString, null);
  }
  
  public final void d()
  {
    f localf = d;
    if (localf != null)
    {
      localf.c();
      return;
    }
  }
  
  public final void d(String paramString)
  {
    int i = R.id.tvMessage;
    EditText localEditText = (EditText)a(i);
    paramString = (CharSequence)paramString;
    localEditText.setText(paramString);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    l.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a(paramInt1, paramInt2, paramIntent);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof f;
    if (bool)
    {
      paramContext = (f)getActivity();
      d = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement PaymentsView");
    throw ((Throwable)paramContext);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    d = null;
    l.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.y_();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    if (paramView != null) {
      paramView.a(this);
    }
    paramView = getArguments();
    if (paramView == null) {
      return;
    }
    k.a(paramView, "arguments ?: return");
    paramBundle = paramView.getSerializable("transaction_details_key");
    if (paramBundle != null)
    {
      paramBundle = (p)paramBundle;
      String str = "utilities";
      paramView = paramView.getString("recharge_context_key", str);
      localObject = a;
      if (localObject == null)
      {
        str = "presenter";
        k.a(str);
      }
      if (localObject != null)
      {
        str = "rechargeContext";
        k.a(paramView, str);
        ((l.a)localObject).a(paramBundle, paramView);
      }
      int i = R.id.btnClose;
      paramView = (ImageView)a(i);
      paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/g$b;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      i = R.id.btnDown;
      paramView = (ImageView)a(i);
      paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/g$c;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      i = R.id.tvBankName;
      paramView = (TextView)a(i);
      paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/g$d;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      i = R.id.view_bank_selection;
      paramView = a(i);
      paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/g$e;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      i = R.id.btnPay;
      paramView = (Button)a(i);
      paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/g$f;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.transaction.models.TransactionModel");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */