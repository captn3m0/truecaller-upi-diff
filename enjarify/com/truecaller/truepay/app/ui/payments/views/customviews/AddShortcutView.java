package com.truecaller.truepay.app.ui.payments.views.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable.Callback;
import android.support.v4.content.b;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.styleable;
import java.util.HashMap;

public final class AddShortcutView
  extends LinearLayout
  implements View.OnClickListener
{
  private final int a;
  private AddShortcutView.a b;
  private HashMap c;
  
  public AddShortcutView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private AddShortcutView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    int i = 600;
    a = i;
    paramContext = LayoutInflater.from(getContext());
    paramChar = R.layout.layout_add_shortcut;
    Object localObject2 = this;
    localObject2 = (ViewGroup)this;
    int j = 1;
    paramContext.inflate(paramChar, (ViewGroup)localObject2, j);
    setOrientation(j);
    if (paramAttributeSet != null)
    {
      paramContext = getContext();
      localObject1 = R.styleable.AddShortcutView;
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject1);
      char c1 = R.id.text;
      paramAttributeSet = (TextView)a(c1);
      k.a(paramAttributeSet, "text");
      paramChar = R.styleable.AddShortcutView_text;
      localObject1 = (CharSequence)paramContext.getString(paramChar);
      paramAttributeSet.setText((CharSequence)localObject1);
      c1 = R.id.button;
      paramAttributeSet = (TextView)a(c1);
      k.a(paramAttributeSet, "button");
      paramChar = R.styleable.AddShortcutView_button_text;
      localObject1 = (CharSequence)paramContext.getString(paramChar);
      paramAttributeSet.setText((CharSequence)localObject1);
      c1 = R.styleable.AddShortcutView_anim_drawable;
      paramChar = '￿';
      c1 = paramContext.getResourceId(c1, paramChar);
      if (c1 != paramChar)
      {
        paramChar = R.id.image;
        localObject1 = (ImageView)a(paramChar);
        localObject2 = getContext();
        paramAttributeSet = b.a((Context)localObject2, c1);
        ((ImageView)localObject1).setImageDrawable(paramAttributeSet);
        c1 = R.id.image;
        paramAttributeSet = (ImageView)a(c1);
        localObject1 = "image";
        k.a(paramAttributeSet, (String)localObject1);
        paramAttributeSet = paramAttributeSet.getDrawable();
        if (paramAttributeSet != null)
        {
          paramAttributeSet = (AnimationDrawable)paramAttributeSet;
          paramChar = R.id.image;
          localObject1 = (Drawable.Callback)a(paramChar);
          paramAttributeSet.setCallback((Drawable.Callback)localObject1);
          paramAttributeSet.setVisible(j, j);
          paramChar = a;
          paramAttributeSet.setExitFadeDuration(paramChar);
          paramAttributeSet.start();
        }
        else
        {
          paramContext = new c/u;
          paramContext.<init>("null cannot be cast to non-null type android.graphics.drawable.AnimationDrawable");
          throw paramContext;
        }
      }
      c1 = R.id.button;
      paramAttributeSet = (TextView)a(c1);
      localObject1 = this;
      localObject1 = (View.OnClickListener)this;
      paramAttributeSet.setOnClickListener((View.OnClickListener)localObject1);
      paramContext.recycle();
      return;
    }
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void onClick(View paramView)
  {
    int i = R.id.button;
    TextView localTextView = (TextView)a(i);
    boolean bool = k.a(paramView, localTextView);
    if (bool)
    {
      paramView = b;
      if (paramView != null)
      {
        paramView.onAddClicked();
        return;
      }
    }
  }
  
  public final void setAddClickListener(AddShortcutView.a parama)
  {
    k.b(parama, "listener");
    b = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.customviews.AddShortcutView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */