package com.truecaller.truepay.app.ui.payments.views.b;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.b.b;
import com.truecaller.truepay.app.ui.payments.c.ac.b;
import com.truecaller.truepay.app.ui.payments.c.ac.c;
import java.util.HashMap;

public final class n
  extends com.truecaller.truepay.app.ui.base.views.fragments.a
  implements ac.c
{
  public static final n.a b;
  public ac.b a;
  private com.truecaller.truepay.app.ui.payments.views.c.f c;
  private HashMap d;
  
  static
  {
    n.a locala = new com/truecaller/truepay/app/ui/payments/views/b/n$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final n b(String paramString)
  {
    k.b(paramString, "message");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("message_param", paramString);
    paramString = new com/truecaller/truepay/app/ui/payments/views/b/n;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  public final int a()
  {
    return R.layout.fragment_redbus_ticket_cancel_success;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "message");
    int i = R.id.tvCancelTicketSuccessMessage;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvCancelTicketSuccessMessage");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b()
  {
    Object localObject = getActivity();
    boolean bool = localObject instanceof com.truecaller.truepay.app.ui.payments.views.c.f;
    if (bool)
    {
      localObject = (com.truecaller.truepay.app.ui.payments.views.c.f)getActivity();
      c = ((com.truecaller.truepay.app.ui.payments.views.c.f)localObject);
      return;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Activity should implement PaymentsView");
    throw ((Throwable)localObject);
  }
  
  public final void c()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      int i = -1;
      localf.setResult(i);
    }
    localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final int getTheme()
  {
    return R.style.BottomSheetDialogTheme;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    a.a locala = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala1 = Truepay.getApplicationComponent();
    locala.a(locala1).a().a(this);
    super.onCreate(paramBundle);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/design/widget/a;
    Context localContext = requireContext();
    int i = getTheme();
    paramBundle.<init>(localContext, i);
    return (Dialog)paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_redbus_ticket_cancel_success;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    ac.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    if (localb != null) {
      localb.y_();
    }
    c = null;
  }
  
  public final void onDismiss(DialogInterface paramDialogInterface)
  {
    ac.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    if (localb != null) {
      localb.a();
    }
    super.onDismiss(paramDialogInterface);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = getArguments();
    if (paramView != null)
    {
      paramView = paramView.getString("message_param");
      paramBundle = a;
      if (paramBundle == null)
      {
        str = "presenter";
        k.a(str);
      }
      paramBundle.a(paramView);
    }
    int i = R.id.btnCancellationDone;
    paramView = (Button)a(i);
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/n$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */