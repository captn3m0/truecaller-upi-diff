package com.truecaller.truepay.app.ui.payments.views.b;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.DiffUtil;
import android.support.v7.util.DiffUtil.Callback;
import android.support.v7.util.DiffUtil.DiffResult;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.ui.base.widgets.RecommendedRechargesView;
import com.truecaller.truepay.app.ui.base.widgets.RecommendedRechargesView.a;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.views.a.q;
import com.truecaller.truepay.app.ui.transaction.views.a.q.a;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.extensions.t;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class i
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements RecommendedRechargesView.a, c.a, f.a, h.a, com.truecaller.truepay.app.ui.payments.views.c.d, com.truecaller.truepay.app.ui.payments.views.c.h, q.a
{
  private int A;
  private boolean B;
  private RecommendedRechargesView C;
  Toolbar a;
  LinearLayout b;
  ImageView c;
  ImageView d;
  TextView e;
  RecyclerView f;
  TextView g;
  ImageView h;
  Button i;
  View j;
  public r k;
  public com.truecaller.truepay.app.ui.payments.c.p l;
  public com.truecaller.truepay.app.ui.history.views.c.b n;
  private com.truecaller.truepay.app.ui.payments.a.o o;
  private ArrayList p;
  private ArrayList q;
  private com.truecaller.truepay.app.ui.payments.views.c.f r;
  private ProgressBar s;
  private ImageView t;
  private ImageView u;
  private EditText v;
  private TextInputLayout w;
  private com.truecaller.truepay.app.ui.transaction.b.p x;
  private TextView y;
  private TextView z;
  
  public i()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    p = localArrayList;
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    q = localArrayList;
  }
  
  public static i a(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.payments.models.a parama2, HashMap paramHashMap, String paramString1, String paramString2, String paramString3)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("utility_entry", parama1);
    localBundle.putSerializable("utility_operator", parama2);
    localBundle.putSerializable("recharge_params", paramHashMap);
    localBundle.putString("utility_operator_symbol", paramString1);
    localBundle.putString("utility_location_symbol", paramString2);
    localBundle.putString("recharge_context_key", paramString3);
    parama1 = new com/truecaller/truepay/app/ui/payments/views/b/i;
    parama1.<init>();
    parama1.setArguments(localBundle);
    return parama1;
  }
  
  private void a(Editable paramEditable, ImageView paramImageView1, ImageView paramImageView2, TextInputLayout paramTextInputLayout)
  {
    paramEditable = paramEditable.toString();
    int m = paramEditable.length() + -10;
    paramEditable = paramEditable.substring(m);
    boolean bool = TextUtils.isDigitsOnly(paramEditable);
    if (bool)
    {
      com.truecaller.truepay.app.ui.payments.c.p localp = l;
      localp.a(paramEditable);
    }
    paramImageView1.setVisibility(8);
    paramImageView2.setVisibility(0);
    paramTextInputLayout.setError("");
    paramTextInputLayout.setErrorEnabled(false);
  }
  
  public static i b(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.payments.models.a parama2, HashMap paramHashMap, String paramString1, String paramString2, String paramString3)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("utility_entry", parama1);
    localBundle.putSerializable("utility_operator", parama2);
    localBundle.putSerializable("recharge_params", paramHashMap);
    localBundle.putString("utility_operator_symbol", paramString1);
    localBundle.putString("utility_location_symbol", paramString2);
    boolean bool = true;
    localBundle.putBoolean("do_fetch_bill", bool);
    localBundle.putBoolean("is_from_recents", bool);
    localBundle.putString("recharge_context_key", paramString3);
    parama1 = new com/truecaller/truepay/app/ui/payments/views/b/i;
    parama1.<init>();
    parama1.setArguments(localBundle);
    return parama1;
  }
  
  private TextInputLayout d(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    Iterator localIterator = q.iterator();
    Object localObject;
    boolean bool2;
    do
    {
      com.truecaller.truepay.app.ui.payments.models.a locala;
      do
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localObject = (View)localIterator.next();
        locala = (com.truecaller.truepay.app.ui.payments.models.a)((View)localObject).getTag();
        localObject = (TextInputLayout)localObject;
      } while (locala == null);
      bool2 = locala.equals(parama);
    } while (!bool2);
    return (TextInputLayout)localObject;
    return null;
  }
  
  private static String h(String paramString)
  {
    String str1 = "\\s";
    String str2 = "%";
    try
    {
      paramString = paramString.replaceAll(str1, str2);
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return paramString;
  }
  
  private static String i(String paramString)
  {
    String str = "=";
    try
    {
      paramString = paramString.split(str);
      int m = 1;
      return paramString[m];
    }
    catch (Exception localException) {}
    return "";
  }
  
  private static String j(String paramString)
  {
    String str = "=";
    try
    {
      paramString = paramString.split(str);
      str = null;
      return paramString[0];
    }
    catch (Exception localException) {}
    return "";
  }
  
  private void p()
  {
    l.h();
    l.g();
  }
  
  public final int a()
  {
    return R.layout.fragment_payment_details_entry;
  }
  
  public final void a(int paramInt)
  {
    com.truecaller.truepay.app.ui.payments.models.c localc = new com/truecaller/truepay/app/ui/payments/models/c;
    localc.<init>();
    a = paramInt;
    Object localObject = l;
    int m = p.size();
    ((com.truecaller.truepay.app.ui.payments.c.p)localObject).a(m, localc);
    localObject = v;
    int i1 = ((EditText)localObject).getText().length();
    ((EditText)localObject).setSelection(i1);
    v.requestFocus();
  }
  
  public final void a(int paramInt, String paramString)
  {
    ArrayList localArrayList = q;
    if (localArrayList != null)
    {
      boolean bool = localArrayList.isEmpty();
      if (!bool)
      {
        localArrayList = q;
        TextInputLayout localTextInputLayout = (TextInputLayout)localArrayList.get(paramInt);
        if (localTextInputLayout != null)
        {
          localTextInputLayout.setErrorEnabled(true);
          int m = R.style.TextAppearence_TextInputLayout_Error;
          localTextInputLayout.setErrorTextAppearance(m);
          localTextInputLayout.setError(paramString);
        }
      }
    }
  }
  
  public final void a(int paramInt, boolean paramBoolean)
  {
    ArrayList localArrayList = p;
    boolean bool = localArrayList.isEmpty();
    if (!bool)
    {
      Object localObject = (View)p.get(paramInt);
      bool = false;
      localArrayList = null;
      int i1;
      if (paramBoolean) {
        i1 = 0;
      } else {
        i1 = 8;
      }
      ((View)localObject).setVisibility(i1);
      localObject = y;
      if (localObject != null)
      {
        if (paramBoolean) {
          i1 = 0;
        } else {
          i1 = 8;
        }
        ((TextView)localObject).setVisibility(i1);
      }
      localObject = t;
      int m;
      if (!paramBoolean) {
        m = 8;
      }
      ((ImageView)localObject).setVisibility(m);
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.history.models.h paramh)
  {
    com.truecaller.truepay.app.ui.payments.c.p localp = l;
    int m = p.size();
    localp.a(m, paramh);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    Object localObject1 = m;
    if (localObject1 != null)
    {
      localObject1 = m;
      int m = ((List)localObject1).size();
      int i1 = 2;
      if (m == i1)
      {
        localObject1 = (LayoutInflater)getContext().getSystemService("layout_inflater");
        i1 = R.layout.item_payment_entry_options;
        localObject1 = ((LayoutInflater)localObject1).inflate(i1, null);
        i1 = R.id.radioGroup;
        RadioGroup localRadioGroup = (RadioGroup)((View)localObject1).findViewById(i1);
        Object localObject2 = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$s-x8YkOROUuX6oD5naDuMqEsLPE;
        ((-..Lambda.i.s-x8YkOROUuX6oD5naDuMqEsLPE)localObject2).<init>(this);
        localRadioGroup.setOnCheckedChangeListener((RadioGroup.OnCheckedChangeListener)localObject2);
        localRadioGroup.setTag(parama);
        int i2 = R.id.radioButtonOption1;
        localObject2 = (RadioButton)((View)localObject1).findViewById(i2);
        int i3 = R.id.radioButtonOption2;
        RadioButton localRadioButton = (RadioButton)((View)localObject1).findViewById(i3);
        String str1 = (String)m.get(0);
        Object localObject3 = m;
        int i4 = 1;
        localObject3 = (String)((List)localObject3).get(i4);
        String str2 = s;
        if (str2 != null)
        {
          str2 = s;
          boolean bool = str2.equalsIgnoreCase(str1);
          if (bool)
          {
            ((RadioButton)localObject2).setChecked(i4);
          }
          else
          {
            str2 = s;
            bool = str2.equalsIgnoreCase((String)localObject3);
            if (bool) {
              localRadioButton.setChecked(i4);
            }
          }
        }
        ((RadioButton)localObject2).setText(str1);
        localRadioButton.setText((CharSequence)localObject3);
        i2 = R.id.tv_selection_title;
        localObject2 = (TextView)((View)localObject1).findViewById(i2);
        parama = b;
        ((TextView)localObject2).setText(parama);
        z = ((TextView)localObject2);
        p.add(localRadioGroup);
        q.add(null);
        parama = b;
        parama.addView((View)localObject1);
      }
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.payments.models.a parama2)
  {
    l.a(parama1, parama2);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.payments.models.a parama2, boolean paramBoolean, com.truecaller.truepay.app.ui.payments.models.a parama3, String paramString)
  {
    parama1 = f.a(parama1, parama2, true, paramBoolean, parama3, null, null, paramString);
    g = this;
    r.a(parama1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama, String paramString1, boolean paramBoolean1, boolean paramBoolean2, String paramString2)
  {
    i locali = this;
    Object localObject1 = parama;
    Object localObject2 = (LayoutInflater)getContext().getSystemService("layout_inflater");
    int m = R.layout.item_payment_entry;
    int i1 = 0;
    Object localObject3 = null;
    View localView = ((LayoutInflater)localObject2).inflate(m, null);
    Object localObject4 = f;
    int i2 = ((String)localObject4).hashCode();
    int i3 = -1413853096;
    int i4 = -1;
    int i5 = 3;
    int i6 = 2;
    int i7 = 1;
    Object localObject5;
    if (i2 != i3)
    {
      i3 = -500553564;
      boolean bool3;
      if (i2 != i3)
      {
        i3 = 106642798;
        if (i2 != i3)
        {
          i3 = 1552163056;
          if (i2 == i3)
          {
            localObject5 = "operator_location";
            boolean bool2 = ((String)localObject4).equals(localObject5);
            if (bool2)
            {
              int i8 = 2;
              break label224;
            }
          }
        }
        else
        {
          localObject5 = "phone";
          bool3 = ((String)localObject4).equals(localObject5);
          if (bool3)
          {
            bool3 = false;
            localObject4 = null;
            break label224;
          }
        }
      }
      else
      {
        localObject5 = "operator";
        bool3 = ((String)localObject4).equals(localObject5);
        if (bool3)
        {
          int i9 = 3;
          break label224;
        }
      }
    }
    else
    {
      localObject5 = "amount";
      boolean bool4 = ((String)localObject4).equals(localObject5);
      if (bool4)
      {
        bool4 = true;
        break label224;
      }
    }
    int i10 = -1;
    switch (i10)
    {
    default: 
      break;
    case 2: 
    case 3: 
      m = R.layout.item_payment_entry_operator;
      localView = ((LayoutInflater)localObject2).inflate(m, null);
      break;
    case 1: 
      m = R.layout.item_payment_entry_amount;
      localView = ((LayoutInflater)localObject2).inflate(m, null);
      break;
    case 0: 
      label224:
      m = R.layout.item_payment_entry_phone;
      localView = ((LayoutInflater)localObject2).inflate(m, null);
    }
    int i13 = R.id.til_payments_entry;
    localObject2 = (TextInputLayout)localView.findViewById(i13);
    ((TextInputLayout)localObject2).setTag(parama);
    ((TextInputLayout)localObject2).setHintEnabled(i7);
    localObject3 = b;
    ((TextInputLayout)localObject2).setHint((CharSequence)localObject3);
    i1 = R.id.tiet_payments_entry;
    localObject3 = (TextInputEditText)localView.findViewById(i1);
    localObject4 = s;
    if (localObject4 != null)
    {
      localObject4 = s;
      ((TextInputEditText)localObject3).setText((CharSequence)localObject4);
    }
    ((TextInputEditText)localObject3).setTag(parama);
    localObject4 = f;
    i2 = ((String)localObject4).hashCode();
    i3 = 6;
    boolean bool5;
    switch (i2)
    {
    default: 
      break;
    case 1552163056: 
      localObject5 = "operator_location";
      bool5 = ((String)localObject4).equals(localObject5);
      if (bool5) {
        i4 = 5;
      }
      break;
    case 1216985755: 
      localObject5 = "password";
      bool5 = ((String)localObject4).equals(localObject5);
      if (bool5) {
        i4 = 4;
      }
      break;
    case 106642798: 
      localObject5 = "phone";
      bool5 = ((String)localObject4).equals(localObject5);
      if (bool5) {
        i4 = 3;
      }
      break;
    case 96619420: 
      localObject5 = "email";
      bool5 = ((String)localObject4).equals(localObject5);
      if (bool5) {
        i4 = 7;
      }
      break;
    case 3556653: 
      localObject5 = "text";
      bool5 = ((String)localObject4).equals(localObject5);
      if (bool5) {
        i4 = 0;
      }
      break;
    case -500553564: 
      localObject5 = "operator";
      bool5 = ((String)localObject4).equals(localObject5);
      if (bool5) {
        i4 = 6;
      }
      break;
    case -1034364087: 
      localObject5 = "number";
      bool5 = ((String)localObject4).equals(localObject5);
      if (bool5) {
        i4 = 1;
      }
      break;
    case -1413853096: 
      localObject5 = "amount";
      bool5 = ((String)localObject4).equals(localObject5);
      if (bool5) {
        i4 = 2;
      }
      break;
    }
    int i14;
    int i15;
    switch (i4)
    {
    default: 
      break;
    case 7: 
      i14 = 33;
      ((TextInputEditText)localObject3).setInputType(i14);
      break;
    case 5: 
    case 6: 
      ((TextInputLayout)localObject2).setHintEnabled(false);
      int i11 = R.id.img_selected_operator_icon;
      localObject4 = (ImageView)localView.findViewById(i11);
      t = ((ImageView)localObject4);
      i11 = R.id.img_drop_down;
      localObject4 = (ImageView)localView.findViewById(i11);
      u = ((ImageView)localObject4);
      ((TextInputEditText)localObject3).setInputType(0);
      ((TextInputEditText)localObject3).setTextIsSelectable(i7);
      ((TextInputEditText)localObject3).setFocusable(false);
      ((TextInputEditText)localObject3).setClickable(i7);
      localObject4 = paramString1;
      ((TextInputEditText)localObject3).setText(paramString1);
      boolean bool6 = paramBoolean2;
      ((TextInputLayout)localObject2).setHintEnabled(paramBoolean2);
      l.k();
      l.f();
      localObject4 = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$zaPmNHQeX3SqIh3OaJZ478IYbks;
      ((-..Lambda.i.zaPmNHQeX3SqIh3OaJZ478IYbks)localObject4).<init>(this, parama);
      ((TextInputEditText)localObject3).setOnClickListener((View.OnClickListener)localObject4);
      i14 = R.id.pb_fetch_operator_location;
      localObject1 = (ProgressBar)localView.findViewById(i14);
      s = ((ProgressBar)localObject1);
      break;
    case 4: 
      ((TextInputEditText)localObject3).setInputType(i7);
      localObject1 = PasswordTransformationMethod.getInstance();
      ((TextInputEditText)localObject3).setTransformationMethod((TransformationMethod)localObject1);
      break;
    case 3: 
      ax.a((EditText)localObject3, "+91");
      localObject1 = new InputFilter[i7];
      localObject4 = new android/text/InputFilter$LengthFilter;
      ((InputFilter.LengthFilter)localObject4).<init>(13);
      localObject1[0] = localObject4;
      ((TextInputEditText)localObject3).setFilters((InputFilter[])localObject1);
      ((TextInputEditText)localObject3).setInputType(i5);
      localObject1 = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$17s9DYQ5s_EmNOtIL1gwsNmX1ts;
      ((-..Lambda.i.17s9DYQ5s_EmNOtIL1gwsNmX1ts)localObject1).<init>((TextInputEditText)localObject3);
      ((TextInputEditText)localObject3).setOnFocusChangeListener((View.OnFocusChangeListener)localObject1);
      i14 = R.id.img_contact;
      localObject1 = (ImageView)localView.findViewById(i14);
      localObject4 = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$rPiEb01M7N4dbsVTI3b81cfWJJ4;
      ((-..Lambda.i.rPiEb01M7N4dbsVTI3b81cfWJJ4)localObject4).<init>(this, (TextInputEditText)localObject3);
      ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject4);
      int i12 = R.id.img_clear;
      localObject4 = (ImageView)localView.findViewById(i12);
      localObject5 = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$kOLgkf5V5CqB2nmWo0VWM9qs5Zs;
      ((-..Lambda.i.kOLgkf5V5CqB2nmWo0VWM9qs5Zs)localObject5).<init>((TextInputEditText)localObject3, (ImageView)localObject1);
      ((ImageView)localObject4).setOnClickListener((View.OnClickListener)localObject5);
      localObject5 = l;
      Editable localEditable = ((TextInputEditText)localObject3).getText();
      i3 = localEditable.length();
      boolean bool1 = ((com.truecaller.truepay.app.ui.payments.c.p)localObject5).b(i3);
      if (bool1)
      {
        localObject5 = ((TextInputEditText)localObject3).getText();
        a((Editable)localObject5, (ImageView)localObject1, (ImageView)localObject4, (TextInputLayout)localObject2);
      }
      localObject5 = new com/truecaller/truepay/app/ui/payments/views/b/i$1;
      ((i.1)localObject5).<init>(this, (ImageView)localObject1, (ImageView)localObject4, (TextInputLayout)localObject2);
      ((TextInputEditText)localObject3).addTextChangedListener((TextWatcher)localObject5);
      ((TextInputEditText)localObject3).setInputType(i6);
      localObject1 = "+91";
      localObject4 = ((TextInputEditText)localObject3).getText().toString();
      boolean bool7 = ((String)localObject1).equals(localObject4);
      if (bool7)
      {
        bool7 = paramString2.isEmpty();
        if (!bool7)
        {
          i15 = R.string.msisdn_with_prefix;
          localObject4 = new Object[i6];
          localObject5 = "+91";
          localObject4[0] = localObject5;
          localObject4[i7] = paramString2;
          localObject1 = getString(i15, (Object[])localObject4);
          ((TextInputEditText)localObject3).setText((CharSequence)localObject1);
        }
      }
      break;
    case 2: 
      localObject1 = new InputFilter[i7];
      localObject4 = new android/text/InputFilter$LengthFilter;
      ((InputFilter.LengthFilter)localObject4).<init>(i3);
      localObject1[0] = localObject4;
      ((TextInputEditText)localObject3).setFilters((InputFilter[])localObject1);
      ((TextInputEditText)localObject3).setInputType(i6);
      localObject1 = "₹";
      ax.a((EditText)localObject3, (String)localObject1);
      if (paramBoolean1)
      {
        i15 = R.id.tv_view_plans;
        localObject1 = (TextView)localView.findViewById(i15);
        y = ((TextView)localObject1);
        localObject1 = y;
        if (localObject1 != null)
        {
          ((TextView)localObject1).setVisibility(0);
          localObject1 = y;
          localObject4 = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$i3H3lBOEpp4rnLq3WyCtIRgAKcI;
          ((-..Lambda.i.i3H3lBOEpp4rnLq3WyCtIRgAKcI)localObject4).<init>(this);
          ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject4);
        }
      }
      i15 = R.id.ravPaymentsEntry;
      localObject1 = (RecommendedRechargesView)localView.findViewById(i15);
      C = ((RecommendedRechargesView)localObject1);
      localObject1 = C;
      ((RecommendedRechargesView)localObject1).setRechargeAmountListener(this);
      v = ((EditText)localObject3);
      w = ((TextInputLayout)localObject2);
      break;
    case 1: 
      ((TextInputEditText)localObject3).setInputType(i6);
      break;
    case 0: 
      ((TextInputEditText)localObject3).setInputType(i7);
    }
    p.add(localObject3);
    q.add(localObject2);
    b.addView(localView);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.c paramc)
  {
    com.truecaller.truepay.app.ui.payments.c.p localp = l;
    int m = p.size();
    localp.a(m, paramc);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.o paramo, com.truecaller.truepay.app.ui.transaction.b.p paramp)
  {
    try
    {
      paramo = c.a(paramo, paramp);
      int m = 1002;
      paramo.setTargetFragment(this, m);
      paramp = getFragmentManager();
      Object localObject = c.class;
      localObject = ((Class)localObject).getSimpleName();
      paramo.show(paramp, (String)localObject);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      com.truecaller.log.d.a(localIllegalStateException;
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.p paramp)
  {
    l.a(paramp);
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    l.a(parama);
  }
  
  public final void a(String paramString)
  {
    setHasOptionsMenu(false);
    Object localObject = (AppCompatActivity)getActivity();
    Toolbar localToolbar = a;
    ((AppCompatActivity)localObject).setSupportActionBar(localToolbar);
    localObject = ((AppCompatActivity)getActivity()).getSupportActionBar();
    if (localObject != null)
    {
      ((ActionBar)localObject).setTitle(paramString);
      boolean bool = true;
      ((ActionBar)localObject).setDisplayShowTitleEnabled(bool);
      ((ActionBar)localObject).setDisplayHomeAsUpEnabled(bool);
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("utility_type", paramString1);
    localBundle.putSerializable("selected_operator_symbol", paramString2);
    localBundle.putSerializable("selected_location_symbol", paramString3);
    paramString1 = new com/truecaller/truepay/app/ui/payments/views/b/h;
    paramString1.<init>();
    paramString1.setArguments(localBundle);
    g = this;
    r.a(paramString1);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    boolean bool = TextUtils.isEmpty(paramString2);
    if (bool) {
      return;
    }
    bool = TextUtils.isEmpty(paramString1);
    if (bool) {
      paramString1 = "";
    } else {
      paramString1 = h(paramString1);
    }
    paramString2 = h(paramString2);
    Object[] arrayOfObject = new Object[5];
    arrayOfObject[0] = paramString1;
    arrayOfObject[1] = paramString2;
    arrayOfObject[2] = paramString3;
    arrayOfObject[3] = paramString4;
    arrayOfObject[4] = paramString5;
    paramString1 = String.format("upi://pay?pn=%s&pa=%s&am=%s&cu=INR&tr=%s&tid=%s&mc=5411", arrayOfObject);
    paramString2 = new android/content/Intent;
    paramString1 = Uri.parse(paramString1);
    paramString2.<init>("android.intent.action.VIEW", paramString1);
    int m = R.string.choose_desired_app;
    paramString1 = getString(m);
    paramString1 = Intent.createChooser(paramString2, paramString1);
    startActivityForResult(paramString1, 5451);
  }
  
  public final void a(List paramList)
  {
    g.setVisibility(0);
    f.setVisibility(0);
    com.truecaller.truepay.app.ui.payments.a.o localo = o;
    Object localObject = new com/truecaller/truepay/app/ui/history/views/a/f;
    List localList = (List)localo.a();
    ((com.truecaller.truepay.app.ui.history.views.a.f)localObject).<init>(localList, paramList);
    localObject = DiffUtil.calculateDiff((DiffUtil.Callback)localObject);
    localo.a(paramList);
    ((DiffUtil.DiffResult)localObject).dispatchUpdatesTo(localo);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = f;
    int m = 0;
    if (paramBoolean)
    {
      com.truecaller.truepay.app.ui.payments.a.o localo1 = o;
      i1 = localo1.getItemCount();
      if (i1 > 0)
      {
        i1 = 0;
        localo1 = null;
        break label42;
      }
    }
    int i1 = 8;
    label42:
    ((RecyclerView)localObject).setVisibility(i1);
    localObject = g;
    if (paramBoolean)
    {
      com.truecaller.truepay.app.ui.payments.a.o localo2 = o;
      paramBoolean = localo2.getItemCount();
      if (paramBoolean) {}
    }
    else
    {
      m = 8;
    }
    ((TextView)localObject).setVisibility(m);
  }
  
  public final com.truecaller.truepay.app.ui.payments.models.a b(int paramInt)
  {
    return (com.truecaller.truepay.app.ui.payments.models.a)((View)p.get(paramInt)).getTag();
  }
  
  public final void b()
  {
    Object localObject = v;
    if (localObject != null)
    {
      localObject = getContext();
      if (localObject != null)
      {
        localObject = v;
        int m = 2;
        t.a((View)localObject, false, m);
      }
    }
  }
  
  public final void b(int paramInt, String paramString)
  {
    Object localObject = p;
    boolean bool1 = ((ArrayList)localObject).isEmpty();
    if (!bool1)
    {
      localObject = (EditText)p.get(paramInt);
      ((EditText)localObject).setText(paramString);
      paramString = q;
      boolean bool2 = paramString.isEmpty();
      if (!bool2)
      {
        paramString = q;
        TextInputLayout localTextInputLayout = (TextInputLayout)paramString.get(paramInt);
        if (localTextInputLayout != null)
        {
          bool2 = false;
          paramString = null;
          localTextInputLayout.setErrorEnabled(false);
        }
      }
    }
  }
  
  public final void b(int paramInt, boolean paramBoolean)
  {
    Object localObject = q;
    boolean bool = ((ArrayList)localObject).isEmpty();
    if (!bool)
    {
      localObject = q.get(paramInt);
      if (localObject != null)
      {
        localObject = q;
        TextInputLayout localTextInputLayout = (TextInputLayout)((ArrayList)localObject).get(paramInt);
        if (paramBoolean) {
          paramBoolean = false;
        } else {
          paramBoolean = true;
        }
        localTextInputLayout.setVisibility(paramBoolean);
      }
    }
  }
  
  public final void b(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    Object localObject = (LayoutInflater)getContext().getSystemService("layout_inflater");
    int m = R.layout.item_payment_description;
    localObject = ((LayoutInflater)localObject).inflate(m, null);
    m = R.id.tvPaymentDisclaimerHeader;
    TextView localTextView1 = (TextView)((View)localObject).findViewById(m);
    int i1 = R.id.tvPaymentDisclaimer;
    TextView localTextView2 = (TextView)((View)localObject).findViewById(i1);
    int i2 = R.id.tvShowDisclaimerAction;
    TextView localTextView3 = (TextView)((View)localObject).findViewById(i2);
    String str = b;
    localTextView1.setText(str);
    parama = i;
    localTextView2.setText(parama);
    parama = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$jv7aV8l1TNAeI8XWKr4V1o7tybg;
    parama.<init>(this, localTextView2, localTextView3);
    localTextView2.post(parama);
    parama = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$w0Gd6VMHXpBri1B4ETBz0wecT-Y;
    parama.<init>(this, localTextView2, localTextView3);
    localTextView3.setOnClickListener(parama);
    b.addView((View)localObject);
  }
  
  public final void b(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.payments.models.a parama2)
  {
    Object localObject1 = u;
    if (localObject1 != null)
    {
      int m = 8;
      ((ImageView)localObject1).setVisibility(m);
    }
    localObject1 = p.iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (View)((Iterator)localObject1).next();
      com.truecaller.truepay.app.ui.payments.models.a locala = (com.truecaller.truepay.app.ui.payments.models.a)((View)localObject2).getTag();
      if (locala != null)
      {
        boolean bool2 = localObject2 instanceof EditText;
        if (bool2)
        {
          String str1 = "operator_location";
          String str2 = f;
          bool2 = str1.equalsIgnoreCase(str2);
          boolean bool3 = true;
          Object localObject3;
          if ((bool2) && (parama2 != null))
          {
            localObject2 = (EditText)localObject2;
            int i1 = 2;
            localObject3 = new Object[i1];
            String str3 = b;
            localObject3[0] = str3;
            str3 = b;
            localObject3[bool3] = str3;
            str1 = String.format("%s, %s", (Object[])localObject3);
            ((EditText)localObject2).setText(str1);
            localObject2 = d(locala);
            if (localObject2 != null)
            {
              ((TextInputLayout)localObject2).setHintEnabled(bool3);
              ((TextInputLayout)localObject2).setErrorEnabled(false);
            }
          }
          else
          {
            str1 = "operator";
            localObject3 = f;
            bool2 = str1.equalsIgnoreCase((String)localObject3);
            if (bool2)
            {
              localObject2 = (EditText)localObject2;
              str1 = b;
              ((EditText)localObject2).setText(str1);
              localObject2 = d(locala);
              if (localObject2 != null)
              {
                ((TextInputLayout)localObject2).setHintEnabled(bool3);
                ((TextInputLayout)localObject2).setErrorEnabled(false);
              }
            }
          }
        }
      }
    }
    l.f();
  }
  
  public final void b(com.truecaller.truepay.app.ui.transaction.b.p paramp)
  {
    com.truecaller.truepay.app.ui.payments.views.c.f localf = r;
    if (localf != null) {
      localf.a(paramp);
    }
  }
  
  public final void b(com.truecaller.truepay.data.api.model.a parama)
  {
    ManageAccountsActivity.a(getContext(), "action.page.forgot_upi_pin", parama, "utilities");
  }
  
  public final void b(String paramString)
  {
    e.setText(paramString);
  }
  
  public final void b(List paramList)
  {
    RecommendedRechargesView localRecommendedRechargesView = C;
    if (localRecommendedRechargesView != null)
    {
      localRecommendedRechargesView.setVisibility(0);
      localRecommendedRechargesView = C;
      String str = "list";
      k.b(paramList, str);
      a = paramList;
      paramList = b;
      if (paramList == null)
      {
        str = "radioGroup";
        k.a(str);
      }
      paramList.removeAllViews();
      localRecommendedRechargesView.a();
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    TextView localTextView = y;
    if (localTextView != null)
    {
      if (paramBoolean) {
        paramBoolean = false;
      } else {
        paramBoolean = true;
      }
      localTextView.setVisibility(paramBoolean);
    }
  }
  
  public final void c()
  {
    p();
  }
  
  public final void c(int paramInt, String paramString)
  {
    ArrayList localArrayList = p;
    boolean bool = localArrayList.isEmpty();
    if (!bool)
    {
      localArrayList = p;
      EditText localEditText = (EditText)localArrayList.get(paramInt);
      localEditText.append(paramString);
    }
  }
  
  public final void c(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    ImageView localImageView1 = t;
    if (localImageView1 != null)
    {
      if (parama != null)
      {
        localImageView1.setVisibility(0);
        r localr1 = k;
        String str1 = j;
        localImageView2 = t;
        int m = R.drawable.ic_place_holder_square;
        localr1.a(str1, localImageView2, m, m, false);
        return;
      }
      r localr2 = k;
      String str2 = "";
      int i1 = R.drawable.ic_place_holder_square;
      ImageView localImageView2 = null;
      localr2.a(str2, localImageView1, i1, i1, false);
      parama = t;
      int i2 = 8;
      parama.setVisibility(i2);
    }
  }
  
  public final void c(com.truecaller.truepay.app.ui.transaction.b.p paramp)
  {
    com.truecaller.truepay.app.ui.payments.views.c.f localf = r;
    if (localf != null) {
      localf.d(paramp);
    }
  }
  
  public final void c(String paramString)
  {
    ImageView localImageView = c;
    paramString = k.b(paramString);
    localImageView.setImageDrawable(paramString);
  }
  
  public final void c(boolean paramBoolean)
  {
    com.truecaller.truepay.app.ui.payments.views.c.f localf = r;
    if (localf != null)
    {
      if (paramBoolean)
      {
        localf.b();
        return;
      }
      localf.c();
    }
  }
  
  public final boolean c(int paramInt)
  {
    return p.get(paramInt) instanceof EditText;
  }
  
  public final int d()
  {
    return p.size();
  }
  
  public final String d(int paramInt)
  {
    return ((TextInputEditText)p.get(paramInt)).getText().toString();
  }
  
  public final void d(String paramString)
  {
    Toast.makeText(getContext(), paramString, 0).show();
  }
  
  public final void d(boolean paramBoolean)
  {
    Object localObject1 = u;
    if ((localObject1 != null) && (!paramBoolean)) {
      ((ImageView)localObject1).setVisibility(0);
    }
    Object localObject2 = p.iterator();
    com.truecaller.truepay.app.ui.payments.models.a locala;
    boolean bool2;
    do
    {
      do
      {
        do
        {
          boolean bool1 = ((Iterator)localObject2).hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = (View)((Iterator)localObject2).next();
          locala = (com.truecaller.truepay.app.ui.payments.models.a)((View)localObject1).getTag();
        } while (locala == null);
        bool2 = localObject1 instanceof EditText;
      } while (!bool2);
      String str1 = f;
      String str2 = "operator_location";
      bool2 = str1.equalsIgnoreCase(str2);
      if (bool2) {
        break;
      }
      str1 = f;
      str2 = "operator";
      bool2 = str1.equalsIgnoreCase(str2);
    } while (!bool2);
    localObject1 = (EditText)localObject1;
    ((EditText)localObject1).setText("");
    localObject2 = d(locala);
    if (localObject2 != null) {
      ((TextInputLayout)localObject2).setHintEnabled(false);
    }
    l.f();
  }
  
  public final void e()
  {
    com.truecaller.truepay.app.ui.payments.c.p localp = l;
    int m = p.size();
    localp.a(m);
  }
  
  public final void e(int paramInt)
  {
    ArrayList localArrayList = q;
    if (localArrayList != null)
    {
      boolean bool = localArrayList.isEmpty();
      if (!bool)
      {
        localArrayList = q;
        TextInputLayout localTextInputLayout = (TextInputLayout)localArrayList.get(paramInt);
        if (localTextInputLayout != null)
        {
          bool = false;
          localArrayList = null;
          localTextInputLayout.setErrorEnabled(false);
        }
      }
    }
  }
  
  public final void e(String paramString)
  {
    a(paramString, null);
  }
  
  public final void e(boolean paramBoolean)
  {
    ProgressBar localProgressBar = s;
    if (localProgressBar != null)
    {
      ImageView localImageView1 = u;
      if (localImageView1 != null)
      {
        localImageView1 = null;
        int m = 8;
        if (paramBoolean)
        {
          localProgressBar.setVisibility(0);
          u.setVisibility(m);
          return;
        }
        localProgressBar.setVisibility(m);
        ImageView localImageView2 = u;
        localImageView2.setVisibility(0);
      }
    }
  }
  
  public final void f()
  {
    l.m();
  }
  
  public final void f(String paramString)
  {
    h.setVisibility(0);
    r localr = k;
    ImageView localImageView = h;
    int m = R.drawable.ic_place_holder_square;
    localr.a(paramString, localImageView, m, m, false);
  }
  
  public final void f(boolean paramBoolean)
  {
    ImageView localImageView = u;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localImageView.setVisibility(paramBoolean);
  }
  
  public final boolean f(int paramInt)
  {
    return p.get(paramInt) instanceof RadioGroup;
  }
  
  public final String g(int paramInt)
  {
    RadioGroup localRadioGroup = (RadioGroup)p.get(paramInt);
    int m = localRadioGroup.getCheckedRadioButtonId();
    return ((RadioButton)localRadioGroup.findViewById(m)).getText().toString();
  }
  
  public final void g()
  {
    l.l();
    l.g();
  }
  
  public final void g(String paramString)
  {
    try
    {
      paramString = b.a(paramString);
      int m = 1002;
      paramString.setTargetFragment(this, m);
      j localj = getFragmentManager();
      Object localObject = b.class;
      localObject = ((Class)localObject).getSimpleName();
      paramString.show(localj, (String)localObject);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      com.truecaller.log.d.a(localIllegalStateException;
    }
  }
  
  public final void g(boolean paramBoolean)
  {
    TextView localTextView = z;
    if (localTextView != null)
    {
      if (paramBoolean) {
        paramBoolean = false;
      } else {
        paramBoolean = true;
      }
      localTextView.setVisibility(paramBoolean);
    }
  }
  
  public final void h()
  {
    getActivity().onBackPressed();
  }
  
  public final void i()
  {
    g.setVisibility(8);
  }
  
  public final void j()
  {
    Object localObject1 = new com/truecaller/truepay/app/ui/payments/a/o;
    Object localObject2 = n;
    ((com.truecaller.truepay.app.ui.payments.a.o)localObject1).<init>(this, (com.truecaller.truepay.app.ui.history.views.c.b)localObject2);
    o = ((com.truecaller.truepay.app.ui.payments.a.o)localObject1);
    localObject1 = f;
    localObject2 = new android/support/v7/widget/LinearLayoutManager;
    android.support.v4.app.f localf = getActivity();
    ((LinearLayoutManager)localObject2).<init>(localf);
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject2);
    localObject1 = f;
    localObject2 = o;
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
  }
  
  public final int k()
  {
    return q.size();
  }
  
  public final void l()
  {
    q localq = new com/truecaller/truepay/app/ui/transaction/views/a/q;
    localq.<init>();
    localq.setTargetFragment(this, 1001);
    j localj = getFragmentManager();
    String str = q.class.getSimpleName();
    localq.show(localj, str);
  }
  
  public final void m()
  {
    h.setVisibility(8);
  }
  
  public final void n()
  {
    Object localObject = i;
    int m = R.string.fetch_bill;
    String str = getString(m);
    ((Button)localObject).setText(str);
    localObject = j;
    m = 8;
    ((View)localObject).setVisibility(m);
    localObject = v;
    if (localObject != null)
    {
      TextInputLayout localTextInputLayout = w;
      if (localTextInputLayout != null)
      {
        ((EditText)localObject).setVisibility(m);
        localObject = w;
        ((TextInputLayout)localObject).setVisibility(m);
      }
    }
  }
  
  public final void o()
  {
    Object localObject = i;
    int m = R.string.continue_caps;
    String str = getString(m);
    ((Button)localObject).setText(str);
    localObject = j;
    m = 0;
    str = null;
    ((View)localObject).setVisibility(0);
    localObject = v;
    if (localObject != null)
    {
      TextInputLayout localTextInputLayout = w;
      if (localTextInputLayout != null)
      {
        ((EditText)localObject).setVisibility(0);
        localObject = w;
        ((TextInputLayout)localObject).setVisibility(0);
      }
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    l locall = null;
    int m = 5451;
    if (paramInt1 == m)
    {
      m = -1;
      if ((paramInt2 == m) && (paramIntent != null))
      {
        localObject1 = paramIntent.getExtras();
        Object localObject2;
        if (localObject1 != null)
        {
          localObject2 = "response";
          localObject1 = ((Bundle)localObject1).getString((String)localObject2);
        }
        else
        {
          m = 0;
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          localObject1 = ((String)localObject1).split("&");
          localObject2 = new java/util/HashMap;
          ((HashMap)localObject2).<init>();
          int i1 = localObject1.length;
          int i2 = 0;
          while (i2 < i1)
          {
            String str1 = localObject1[i2];
            String str2 = j(str1);
            str1 = i(str1);
            ((HashMap)localObject2).put(str2, str1);
            i2 += 1;
          }
          localObject1 = x;
          if (localObject1 != null)
          {
            locall = new com/truecaller/truepay/app/ui/transaction/b/l;
            locall.<init>();
            localObject1 = (String)((HashMap)localObject2).get("responseCode");
            e = ((String)localObject1);
            localObject1 = ((HashMap)localObject2).get("Status");
            if (localObject1 == null) {
              localObject1 = (String)((HashMap)localObject2).get("status");
            } else {
              localObject1 = (String)((HashMap)localObject2).get("Status");
            }
            if (localObject1 != null) {
              localObject1 = ((String)localObject1).toLowerCase();
            } else {
              localObject1 = "failure";
            }
            b = ((String)localObject1);
            localObject1 = (String)((HashMap)localObject2).get("txnRef");
            a = ((String)localObject1);
            localObject1 = (String)((HashMap)localObject2).get("message");
            d = ((String)localObject1);
            localObject1 = x;
            j = locall;
            b((com.truecaller.truepay.app.ui.transaction.b.p)localObject1);
            break label381;
          }
          m = R.string.intent_recharge_unknown;
          localObject1 = getString(m);
          a((String)localObject1, null);
          break label381;
        }
        m = R.string.intent_recharge_unknown;
        localObject1 = getString(m);
        a((String)localObject1, null);
        break label381;
      }
    }
    m = R.string.intent_recharge_unknown;
    Object localObject1 = getString(m);
    a((String)localObject1, null);
    label381:
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.payments.views.c.f;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.payments.views.c.f)getActivity();
      r = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement PaymentsView");
    throw paramContext;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    a.a locala = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala1 = Truepay.getApplicationComponent();
    locala.a(locala1).a().a(this);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    l.b();
    r = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.toolbar;
    paramBundle = (Toolbar)paramView.findViewById(m);
    a = paramBundle;
    m = R.id.utility_items_container;
    paramBundle = (LinearLayout)paramView.findViewById(m);
    b = paramBundle;
    m = R.id.img_bank;
    paramBundle = (ImageView)paramView.findViewById(m);
    c = paramBundle;
    m = R.id.btn_down;
    paramBundle = (ImageView)paramView.findViewById(m);
    d = paramBundle;
    m = R.id.tv_bank_name;
    paramBundle = (TextView)paramView.findViewById(m);
    e = paramBundle;
    m = R.id.recent_transaction_list;
    paramBundle = (RecyclerView)paramView.findViewById(m);
    f = paramBundle;
    m = R.id.title_recent;
    paramBundle = (TextView)paramView.findViewById(m);
    g = paramBundle;
    m = R.id.img_utility_vendor_logo;
    paramBundle = (ImageView)paramView.findViewById(m);
    h = paramBundle;
    m = R.id.btn_utility_continue;
    paramBundle = (Button)paramView.findViewById(m);
    i = paramBundle;
    m = R.id.container_bank_selection;
    paramView = paramView.findViewById(m);
    j = paramView;
    paramView = i;
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$IpJFeRHsRhpPuE_l7PgDp-mfNuI;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = j;
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$Ai9849HEovADY7oXSzpCdZIURs4;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    l.a(this);
    paramView = getArguments();
    if (paramView == null) {
      return;
    }
    l.a(paramView);
    l.a();
    l.e();
    l.d();
    paramView = a;
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$i$1Gs0DUjjxOEd5XpXoREMOzdppIQ;
    paramBundle.<init>(this);
    paramView.setNavigationOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */