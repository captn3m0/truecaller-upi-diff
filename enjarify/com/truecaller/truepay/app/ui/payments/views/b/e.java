package com.truecaller.truepay.app.ui.payments.views.b;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.SortedList;
import android.support.v7.util.SortedList.Callback;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.menu;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.c.d.a;
import com.truecaller.truepay.app.ui.payments.c.d.b;
import com.truecaller.utils.extensions.t;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public final class e
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements SearchView.OnQueryTextListener, d.b
{
  public static final e.a c;
  public d.a a;
  e.b b;
  private com.truecaller.truepay.app.ui.payments.a.e d;
  private final e.d e;
  private final SortedList f;
  private HashMap g;
  
  static
  {
    e.a locala = new com/truecaller/truepay/app/ui/payments/views/b/e$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public e()
  {
    Object localObject = new com/truecaller/truepay/app/ui/payments/views/b/e$d;
    ((e.d)localObject).<init>(this);
    e = ((e.d)localObject);
    localObject = new android/support/v7/util/SortedList;
    SortedList.Callback localCallback = (SortedList.Callback)e;
    ((SortedList)localObject).<init>(com.truecaller.truepay.app.ui.payments.models.a.class, localCallback);
    f = ((SortedList)localObject);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      g = ((HashMap)localObject1);
    }
    localObject1 = g;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = g;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final e b(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    k.b(parama, "loc");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    parama = (Serializable)parama;
    localBundle.putSerializable("location_key", parama);
    parama = new com/truecaller/truepay/app/ui/payments/views/b/e;
    parama.<init>();
    parama.setArguments(localBundle);
    return parama;
  }
  
  public final int a()
  {
    return R.layout.fragment_operator_selection;
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    k.b(parama, "utility");
    e.b localb = b;
    if (localb != null)
    {
      localb.a(parama);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = (AppCompatActivity)localObject;
      int i = R.id.toolbar;
      Toolbar localToolbar = (Toolbar)a(i);
      ((AppCompatActivity)localObject).setSupportActionBar(localToolbar);
      localObject = getActivity();
      if (localObject != null)
      {
        localObject = ((AppCompatActivity)localObject).getSupportActionBar();
        if (localObject != null)
        {
          paramString = (CharSequence)paramString;
          ((ActionBar)localObject).setTitle(paramString);
          boolean bool = true;
          ((ActionBar)localObject).setDisplayHomeAsUpEnabled(bool);
          ((ActionBar)localObject).setDisplayShowTitleEnabled(bool);
        }
        int j = R.id.toolbar;
        paramString = (Toolbar)a(j);
        if (paramString != null)
        {
          localObject = new com/truecaller/truepay/app/ui/payments/views/b/e$c;
          ((e.c)localObject).<init>(this);
          localObject = (View.OnClickListener)localObject;
          paramString.setNavigationOnClickListener((View.OnClickListener)localObject);
          return;
        }
        return;
      }
      paramString = new c/u;
      paramString.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
      throw paramString;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw paramString;
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "locationList");
    Object localObject1 = f;
    paramList = (Collection)paramList;
    ((SortedList)localObject1).replaceAll(paramList);
    paramList = new com/truecaller/truepay/app/ui/payments/a/e;
    localObject1 = f;
    Object localObject2 = a;
    if (localObject2 == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localObject2 = (com.truecaller.truepay.app.ui.payments.a.e.a)localObject2;
    paramList.<init>((SortedList)localObject1, (com.truecaller.truepay.app.ui.payments.a.e.a)localObject2);
    d = paramList;
    int i = R.id.rv_operator_list;
    paramList = (RecyclerView)a(i);
    if (paramList != null)
    {
      localObject1 = (RecyclerView.Adapter)d;
      paramList.setAdapter((RecyclerView.Adapter)localObject1);
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.emptyText;
    Object localObject = (TextView)a(i);
    k.a(localObject, "emptyText");
    int j = 0;
    int k;
    if (paramBoolean)
    {
      k = 0;
      str = null;
    }
    else
    {
      k = 8;
    }
    ((TextView)localObject).setVisibility(k);
    i = R.id.rv_operator_list;
    localObject = (RecyclerView)a(i);
    String str = "rv_operator_list";
    k.a(localObject, str);
    if (paramBoolean) {
      j = 8;
    }
    ((RecyclerView)localObject).setVisibility(j);
  }
  
  public final com.truecaller.truepay.app.ui.payments.models.a b()
  {
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "location_key";
      localObject = ((Bundle)localObject).getSerializable(str);
    }
    else
    {
      localObject = null;
    }
    boolean bool = localObject instanceof com.truecaller.truepay.app.ui.payments.models.a;
    if (!bool) {
      localObject = null;
    }
    return (com.truecaller.truepay.app.ui.payments.models.a)localObject;
  }
  
  public final void b(List paramList)
  {
    k.b(paramList, "list");
    SortedList localSortedList = f;
    paramList = (Collection)paramList;
    localSortedList.replaceAll(paramList);
  }
  
  public final void c()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.onBackPressed();
      return;
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setHasOptionsMenu(true);
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenuInflater != null)
    {
      i = R.menu.menu_operator_selection;
      paramMenuInflater.inflate(i, paramMenu);
    }
    int i = 0;
    Object localObject1 = null;
    int j;
    Object localObject2;
    if (paramMenu != null)
    {
      j = R.id.action_search;
      localObject2 = paramMenu.findItem(j);
    }
    else
    {
      j = 0;
      localObject2 = null;
    }
    if (localObject2 != null)
    {
      Object localObject3 = ((MenuItem)localObject2).getActionView();
      if (localObject3 != null)
      {
        localObject3 = (SearchView)localObject3;
        int k = R.string.bank_selection_search;
        Object localObject4 = (CharSequence)getString(k);
        ((SearchView)localObject3).setQueryHint((CharSequence)localObject4);
        localObject4 = this;
        localObject4 = (SearchView.OnQueryTextListener)this;
        ((SearchView)localObject3).setOnQueryTextListener((SearchView.OnQueryTextListener)localObject4);
        k = 1;
        ((MenuItem)localObject2).setVisible(k);
        j = R.id.search_edit_frame;
        localObject2 = (LinearLayout)((SearchView)localObject3).findViewById(j);
        if (localObject2 != null) {
          localObject1 = ((LinearLayout)localObject2).getLayoutParams();
        }
        if (localObject1 != null)
        {
          localObject1 = (LinearLayout.LayoutParams)localObject1;
          j = 0;
          localObject2 = null;
          leftMargin = 0;
        }
        else
        {
          paramMenu = new c/u;
          paramMenu.<init>("null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams");
          throw paramMenu;
        }
      }
      else
      {
        paramMenu = new c/u;
        paramMenu.<init>("null cannot be cast to non-null type android.support.v7.widget.SearchView");
        throw paramMenu;
      }
    }
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    String str = "inflater";
    k.b(paramLayoutInflater, str);
    paramLayoutInflater = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramViewGroup = com.truecaller.truepay.app.ui.payments.b.a.a();
    paramBundle = Truepay.getApplicationComponent();
    paramViewGroup.a(paramBundle).a().a(this);
    paramViewGroup = a;
    if (paramViewGroup == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramViewGroup.a(this);
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((d.a)localObject).y_();
    localObject = g;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final boolean onQueryTextChange(String paramString)
  {
    d.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a(paramString);
    return true;
  }
  
  public final boolean onQueryTextSubmit(String paramString)
  {
    return false;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject1 = "view";
    k.b(paramView, (String)localObject1);
    super.onViewCreated(paramView, paramBundle);
    paramBundle = getContext();
    if (paramBundle != null)
    {
      i = R.drawable.divider_gray;
      localObject1 = android.support.v4.content.b.a(paramBundle, i);
      if (localObject1 != null)
      {
        Object localObject2 = new android/support/v7/widget/DividerItemDecoration;
        int j = 1;
        ((DividerItemDecoration)localObject2).<init>(paramBundle, j);
        ((DividerItemDecoration)localObject2).setDrawable((Drawable)localObject1);
        k = R.id.rv_operator_list;
        paramBundle = (RecyclerView)a(k);
        k.a(paramBundle, "rv_operator_list");
        i = 0;
        localObject1 = null;
        paramBundle.setItemAnimator(null);
        k = R.id.rv_operator_list;
        paramBundle = (RecyclerView)a(k);
        localObject2 = (RecyclerView.ItemDecoration)localObject2;
        paramBundle.addItemDecoration((RecyclerView.ItemDecoration)localObject2);
      }
    }
    int k = 0;
    paramBundle = null;
    int i = 2;
    t.a(paramView, false, i);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */