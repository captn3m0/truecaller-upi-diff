package com.truecaller.truepay.app.ui.payments.views.b;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.common.ui.PausingLottieAnimationView;
import com.truecaller.truepay.R.anim;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.base.widgets.CircleImageView;
import com.truecaller.truepay.app.ui.history.views.c.d;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.c.af.b;
import com.truecaller.truepay.app.ui.payments.c.af.c;
import com.truecaller.truepay.app.ui.payments.views.customviews.AddShortcutView;
import com.truecaller.truepay.app.ui.payments.views.customviews.AddShortcutView.a;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.extensions.t;
import java.io.Serializable;
import java.util.HashMap;

public final class o
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements af.c
{
  public static final o.a c;
  public af.b a;
  public r b;
  private ProgressDialog d;
  private Interpolator e;
  private com.truecaller.truepay.app.ui.payments.views.c.f f;
  private final int g = 1;
  private final int h;
  private final float i = 0.77F;
  private final float j;
  private final float k = 0.175F;
  private final float l = 1.0F;
  private HashMap n;
  
  static
  {
    o.a locala = new com/truecaller/truepay/app/ui/payments/views/b/o$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private static int a(View paramView)
  {
    float f1 = paramView.getMeasuredHeight();
    paramView = paramView.getContext();
    k.a(paramView, "view.context");
    paramView = paramView.getResources();
    k.a(paramView, "view.context.resources");
    float f2 = getDisplayMetricsdensity;
    return (int)(f1 / f2);
  }
  
  public static final o a(p paramp, String paramString)
  {
    k.b(paramp, "txnModel");
    k.b(paramString, "rechargeContext");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramp = (Serializable)paramp;
    localBundle.putSerializable("transaction_details_key", paramp);
    paramString = (Serializable)paramString;
    localBundle.putSerializable("recharge_context_key", paramString);
    paramp = new com/truecaller/truepay/app/ui/payments/views/b/o;
    paramp.<init>();
    paramp.setArguments(localBundle);
    return paramp;
  }
  
  public final int a()
  {
    return R.layout.fragment_utilities_confirmation;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = n;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      n = ((HashMap)localObject1);
    }
    localObject1 = n;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = n;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    int m = R.id.statusLayout;
    LinearLayout localLinearLayout = (LinearLayout)a(m);
    localLinearLayout.setBackgroundColor(paramInt1);
    com.truecaller.truepay.app.ui.payments.views.c.f localf = f;
    if (localf != null)
    {
      localf.a(paramInt2);
      return;
    }
  }
  
  public final void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int m = R.id.buttonActionLeftButton;
    ((Button)a(m)).setTextColor(paramInt1);
    paramInt1 = R.id.buttonActionLeftButton;
    ((Button)a(paramInt1)).setBackgroundResource(paramInt2);
    paramInt1 = R.id.buttonActionRightButton;
    ((Button)a(paramInt1)).setTextColor(paramInt3);
    paramInt1 = R.id.buttonActionRightButton;
    ((Button)a(paramInt1)).setBackgroundResource(paramInt4);
  }
  
  public final void a(p paramp, String paramString, au paramau)
  {
    k.b(paramp, "txnModel");
    k.b(paramString, "bankRegisteredName");
    k.b(paramau, "stringUtils");
    Context localContext = getContext();
    if (localContext != null)
    {
      d locald = new com/truecaller/truepay/app/ui/history/views/c/d;
      locald.<init>(localContext, paramp, paramString, paramau);
      locald.a();
      return;
    }
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "account");
    com.truecaller.truepay.app.ui.payments.views.c.f localf = f;
    if (localf != null)
    {
      localf.a(parama, "other");
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "bankRRN");
    int m = R.id.tvTransactionId;
    TextView localTextView = (TextView)a(m);
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "leftButtonContent");
    k.b(paramString2, "rightButtonContent");
    int m = R.id.buttonActionLeftButton;
    Button localButton = (Button)a(m);
    k.a(localButton, "buttonActionLeftButton");
    localButton.setVisibility(0);
    m = R.id.buttonActionRightButton;
    localButton = (Button)a(m);
    k.a(localButton, "buttonActionRightButton");
    localButton.setVisibility(0);
    m = R.id.buttonActionLeftButton;
    localButton = (Button)a(m);
    k.a(localButton, "buttonActionLeftButton");
    paramString1 = (CharSequence)paramString1;
    localButton.setText(paramString1);
    int i1 = R.id.buttonActionRightButton;
    paramString1 = (Button)a(i1);
    k.a(paramString1, "buttonActionRightButton");
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "title");
    k.b(paramString2, "subTitle");
    k.b(paramString3, "bankHeader");
    int m = R.id.tvStatusTitle;
    TextView localTextView = (TextView)a(m);
    k.a(localTextView, "tvStatusTitle");
    paramString1 = (CharSequence)paramString1;
    localTextView.setText(paramString1);
    int i1 = R.id.tvStatusSubTitle;
    paramString1 = (TextView)a(i1);
    k.a(paramString1, "tvStatusSubTitle");
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
    i1 = R.id.tvBankSectionHeader;
    paramString1 = (TextView)a(i1);
    k.a(paramString1, "tvBankSectionHeader");
    paramString3 = (CharSequence)paramString3;
    paramString1.setText(paramString3);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt)
  {
    k.b(paramString1, "utilityOperatorName");
    k.b(paramString3, "bankSymbol");
    k.b(paramString4, "displayAmount");
    k.b(paramString5, "displayBankName");
    int m = R.id.tvAmount;
    Object localObject = (TextView)a(m);
    String str = "tvAmount";
    k.a(localObject, str);
    paramString4 = (CharSequence)paramString4;
    ((TextView)localObject).setText(paramString4);
    int i1 = R.id.tvOperatorName;
    paramString4 = (TextView)a(i1);
    localObject = "tvOperatorName";
    k.a(paramString4, (String)localObject);
    paramString1 = (CharSequence)paramString1;
    paramString4.setText(paramString1);
    int i2 = R.id.tvAccNumber;
    paramString1 = (TextView)a(i2);
    k.a(paramString1, "tvAccNumber");
    paramString5 = (CharSequence)paramString5;
    paramString1.setText(paramString5);
    i2 = R.id.bankIcon;
    paramString1 = (ImageView)a(i2);
    paramString4 = b;
    if (paramString4 == null)
    {
      paramString5 = "imageLoader";
      k.a(paramString5);
    }
    paramString3 = paramString4.b(paramString3);
    paramString1.setImageDrawable(paramString3);
    paramString1 = b;
    if (paramString1 == null)
    {
      paramString3 = "imageLoader";
      k.a(paramString3);
    }
    int i3 = R.id.imgOperatorIcon;
    paramString3 = (CircleImageView)a(i3);
    k.a(paramString3, "imgOperatorIcon");
    paramString3 = (ImageView)paramString3;
    paramString1.a(paramString2, paramString3, paramInt, paramInt);
  }
  
  public final void a(boolean paramBoolean)
  {
    int m = R.id.transactionIdSection;
    ConstraintLayout localConstraintLayout = (ConstraintLayout)a(m);
    k.a(localConstraintLayout, "transactionIdSection");
    t.a((View)localConstraintLayout, paramBoolean);
  }
  
  public final void b()
  {
    int m = R.id.buttonActionRightButton;
    Object localObject = (Button)a(m);
    k.a(localObject, "buttonActionRightButton");
    ((Button)localObject).setVisibility(0);
    m = R.id.actionLayoutCardView;
    localObject = (CardView)a(m);
    k.a(localObject, "actionLayoutCardView");
    ((CardView)localObject).setVisibility(0);
  }
  
  public final void b(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "account");
    com.truecaller.truepay.app.ui.payments.views.c.f localf = f;
    if (localf != null)
    {
      localf.b(parama, "other");
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "rightButtonContent");
    int m = R.id.buttonActionLeftButton;
    Button localButton = (Button)a(m);
    k.a(localButton, "buttonActionLeftButton");
    localButton.setVisibility(8);
    m = R.id.buttonActionRightButton;
    localButton = (Button)a(m);
    k.a(localButton, "buttonActionRightButton");
    localButton.setVisibility(0);
    m = R.id.buttonActionRightButton;
    localButton = (Button)a(m);
    k.a(localButton, "buttonActionRightButton");
    paramString = (CharSequence)paramString;
    localButton.setText(paramString);
  }
  
  public final void b(String paramString1, String paramString2)
  {
    k.b(paramString2, "referenceId");
    com.truecaller.truepay.app.ui.payments.views.c.f localf = f;
    if (localf != null)
    {
      Boolean localBoolean = Boolean.FALSE;
      localf.a(paramString1, paramString2, localBoolean);
      return;
    }
  }
  
  public final void c()
  {
    int m = R.id.buttonActionRightButton;
    Object localObject = (Button)a(m);
    k.a(localObject, "buttonActionRightButton");
    int i1 = 8;
    ((Button)localObject).setVisibility(i1);
    m = R.id.actionLayoutCardView;
    localObject = (CardView)a(m);
    k.a(localObject, "actionLayoutCardView");
    ((CardView)localObject).setVisibility(i1);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "faqUrl");
    com.truecaller.truepay.app.ui.payments.views.c.f localf = f;
    if (localf != null)
    {
      localf.b(paramString);
      return;
    }
  }
  
  public final void d()
  {
    int m = R.id.buttonActionLeftButton;
    Button localButton = (Button)a(m);
    k.a(localButton, "buttonActionLeftButton");
    localButton.setVisibility(4);
    m = R.id.buttonActionRightButton;
    localButton = (Button)a(m);
    k.a(localButton, "buttonActionRightButton");
    localButton.setVisibility(0);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "message");
    a(paramString, null);
  }
  
  public final void e()
  {
    Object localObject1 = getContext();
    int m = R.anim.rotate_down;
    localObject1 = AnimationUtils.loadAnimation((Context)localObject1, m);
    m = R.id.dropDownIcon;
    ((ImageView)a(m)).startAnimation((Animation)localObject1);
    int i1 = R.id.transactionIdSection;
    i1 = ((ConstraintLayout)a(i1)).getMeasuredHeight();
    Object localObject2 = new com/truecaller/truepay/app/ui/payments/views/b/o$b;
    ((o.b)localObject2).<init>(this, i1);
    localObject1 = e;
    ((o.b)localObject2).setInterpolator((Interpolator)localObject1);
    i1 = R.id.transactionIdSection;
    localObject1 = (ConstraintLayout)a(i1);
    k.a(localObject1, "transactionIdSection");
    long l1 = a((View)localObject1);
    ((o.b)localObject2).setDuration(l1);
    i1 = R.id.transactionIdSection;
    localObject1 = (ConstraintLayout)a(i1);
    localObject2 = (Animation)localObject2;
    ((ConstraintLayout)localObject1).startAnimation((Animation)localObject2);
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "message");
    a(paramString, null);
  }
  
  public final void f()
  {
    Object localObject1 = getContext();
    int m = R.anim.rotate_up;
    localObject1 = AnimationUtils.loadAnimation((Context)localObject1, m);
    m = R.id.dropDownIcon;
    Object localObject2 = (ImageView)a(m);
    if (localObject2 != null) {
      ((ImageView)localObject2).startAnimation((Animation)localObject1);
    }
    int i1 = R.id.transactionIdSection;
    localObject1 = (ConstraintLayout)a(i1);
    localObject2 = "transactionIdSection";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((ConstraintLayout)localObject1).getParent();
    if (localObject1 != null)
    {
      i1 = View.MeasureSpec.makeMeasureSpec(((View)localObject1).getWidth(), 1073741824);
      int i2 = View.MeasureSpec.makeMeasureSpec(0, 0);
      int i3 = R.id.transactionIdSection;
      ((ConstraintLayout)a(i3)).measure(i1, i2);
      i1 = R.id.transactionIdSection;
      i1 = ((ConstraintLayout)a(i1)).getMeasuredHeight();
      i2 = R.id.transactionIdSection;
      ConstraintLayout localConstraintLayout = (ConstraintLayout)a(i2);
      k.a(localConstraintLayout, "transactionIdSection");
      getLayoutParamsheight = 1;
      i2 = R.id.transactionIdSection;
      localConstraintLayout = (ConstraintLayout)a(i2);
      k.a(localConstraintLayout, "transactionIdSection");
      localConstraintLayout.setVisibility(0);
      localObject2 = new com/truecaller/truepay/app/ui/payments/views/b/o$c;
      ((o.c)localObject2).<init>(this, i1);
      localObject1 = e;
      ((o.c)localObject2).setInterpolator((Interpolator)localObject1);
      i1 = R.id.transactionIdSection;
      localObject1 = (ConstraintLayout)a(i1);
      k.a(localObject1, "transactionIdSection");
      long l1 = a((View)localObject1);
      ((o.c)localObject2).setDuration(l1);
      i1 = R.id.transactionIdSection;
      localObject1 = (ConstraintLayout)a(i1);
      localObject2 = (Animation)localObject2;
      ((ConstraintLayout)localObject1).startAnimation((Animation)localObject2);
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.view.View");
    throw ((Throwable)localObject1);
  }
  
  public final void f(String paramString)
  {
    k.b(paramString, "lottieFileName");
    int m = R.id.animationView;
    ((PausingLottieAnimationView)a(m)).setAnimation(paramString);
    int i1 = R.id.animationView;
    ((PausingLottieAnimationView)a(i1)).a(true);
    i1 = R.id.animationView;
    ((PausingLottieAnimationView)a(i1)).a();
  }
  
  public final void g()
  {
    com.truecaller.truepay.app.ui.payments.views.c.f localf = f;
    if (localf != null)
    {
      localf.a();
      return;
    }
  }
  
  public final void g(String paramString)
  {
    k.b(paramString, "utilityRechargeNumber");
    int m = R.id.tvRechargeNumber;
    TextView localTextView = (TextView)a(m);
    k.a(localTextView, "tvRechargeNumber");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void h()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void h(String paramString)
  {
    k.b(paramString, "rechargeRemarks");
    int m = R.id.tvRechargeRemarks;
    TextView localTextView = (TextView)a(m);
    k.a(localTextView, "tvRechargeRemarks");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
    int i1 = R.id.tvRechargeRemarks;
    paramString = (TextView)a(i1);
    k.a(paramString, "tvRechargeRemarks");
    t.a((View)paramString, true);
  }
  
  public final void i()
  {
    ProgressDialog localProgressDialog = d;
    if (localProgressDialog != null)
    {
      localProgressDialog.dismiss();
      return;
    }
  }
  
  public final void j()
  {
    String[] arrayOfString = { "android.permission.WRITE_EXTERNAL_STORAGE" };
    requestPermissions(arrayOfString, 1005);
  }
  
  public final void k()
  {
    int m = R.id.dropDownIcon;
    Object localObject = (ImageView)a(m);
    k.a(localObject, "dropDownIcon");
    int i1 = 8;
    ((ImageView)localObject).setVisibility(i1);
    m = R.id.transactionIdSection;
    localObject = (ConstraintLayout)a(m);
    k.a(localObject, "transactionIdSection");
    ((ConstraintLayout)localObject).setVisibility(i1);
  }
  
  public final void l()
  {
    int m = R.id.dropDownIcon;
    Object localObject = (ImageView)a(m);
    k.a(localObject, "dropDownIcon");
    ((ImageView)localObject).setVisibility(8);
    m = R.id.transactionIdSection;
    localObject = (ConstraintLayout)a(m);
    k.a(localObject, "transactionIdSection");
    ((ConstraintLayout)localObject).setVisibility(0);
  }
  
  public final void m()
  {
    int m = R.id.dropDownIcon;
    Object localObject = (ImageView)a(m);
    k.a(localObject, "dropDownIcon");
    ((ImageView)localObject).setVisibility(0);
    m = R.id.transactionIdSection;
    localObject = (ConstraintLayout)a(m);
    k.a(localObject, "transactionIdSection");
    ((ConstraintLayout)localObject).setVisibility(0);
  }
  
  public final void n()
  {
    int m = R.id.tvRechargeNumber;
    TextView localTextView = (TextView)a(m);
    k.a(localTextView, "tvRechargeNumber");
    localTextView.setVisibility(8);
  }
  
  public final void o()
  {
    int m = R.id.cardViewAddShortcut;
    CardView localCardView = (CardView)a(m);
    k.a(localCardView, "cardViewAddShortcut");
    t.b((View)localCardView);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.payments.views.c.f;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.payments.views.c.f)getActivity();
      f = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement PaymentsView");
    throw ((Throwable)paramContext);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    a.a locala = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala1 = Truepay.getApplicationComponent();
    locala.a(locala1).a().a(this);
    super.onCreate(paramBundle);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    af.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    if (localb != null) {
      localb.y_();
    }
    f = null;
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    af.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    if (localb != null)
    {
      localb.a(paramInt, paramArrayOfString, paramArrayOfInt);
      return;
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    paramBundle.a(this);
    paramBundle = getArguments();
    if (paramBundle == null) {
      return;
    }
    k.a(paramBundle, "arguments ?: return");
    localObject = paramBundle.getSerializable("transaction_details_key");
    if (localObject != null)
    {
      localObject = (p)localObject;
      String str = "utilities";
      paramBundle = paramBundle.getString("recharge_context_key", str);
      af.b localb = a;
      if (localb == null)
      {
        str = "presenter";
        k.a(str);
      }
      str = "rechargeContext";
      k.a(paramBundle, str);
      localb.a((p)localObject, paramBundle);
      paramBundle = new android/app/ProgressDialog;
      localObject = (Context)getActivity();
      paramBundle.<init>((Context)localObject);
      d = paramBundle;
      float f1 = i;
      float f2 = j;
      float f3 = k;
      float f4 = l;
      paramBundle = android.support.v4.view.b.f.a(f1, f2, f3, f4);
      e = paramBundle;
      f1 = 0.0F;
      paramBundle = null;
      int m = 2;
      f2 = 2.8E-45F;
      t.a(paramView, false, m);
      int i1 = R.id.buttonActionRightButton;
      paramView = (Button)a(i1);
      if (paramView != null)
      {
        paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/o$d;
        paramBundle.<init>(this);
        paramBundle = (View.OnClickListener)paramBundle;
        paramView.setOnClickListener(paramBundle);
      }
      i1 = R.id.buttonActionLeftButton;
      paramView = (Button)a(i1);
      if (paramView != null)
      {
        paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/o$e;
        paramBundle.<init>(this);
        paramBundle = (View.OnClickListener)paramBundle;
        paramView.setOnClickListener(paramBundle);
      }
      i1 = R.id.dropDownIcon;
      paramView = (ImageView)a(i1);
      if (paramView != null)
      {
        paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/o$f;
        paramBundle.<init>(this);
        paramBundle = (View.OnClickListener)paramBundle;
        paramView.setOnClickListener(paramBundle);
      }
      i1 = R.id.transactionIdCopyIcon;
      paramView = (ImageView)a(i1);
      if (paramView != null)
      {
        paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/o$g;
        paramBundle.<init>(this);
        paramBundle = (View.OnClickListener)paramBundle;
        paramView.setOnClickListener(paramBundle);
      }
      i1 = R.id.addShortcutView;
      paramView = (AddShortcutView)a(i1);
      paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/o$h;
      paramBundle.<init>(this);
      paramBundle = (AddShortcutView.a)paramBundle;
      paramView.setAddClickListener(paramBundle);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.transaction.models.TransactionModel");
    throw paramView;
  }
  
  public final void p()
  {
    int m = R.id.cardViewAddShortcut;
    CardView localCardView = (CardView)a(m);
    k.a(localCardView, "cardViewAddShortcut");
    t.a((View)localCardView);
  }
  
  public final af.b q()
  {
    af.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */