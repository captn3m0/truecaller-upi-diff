package com.truecaller.truepay.app.ui.payments.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.c.w.b;
import com.truecaller.truepay.app.ui.payments.c.w.c;
import com.truecaller.truepay.app.ui.payments.models.RedBusTicket;
import java.util.HashMap;

public final class l
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements w.c
{
  public static final l.a b;
  public w.b a;
  private com.truecaller.truepay.app.ui.payments.views.c.f c;
  private HashMap d;
  
  static
  {
    l.a locala = new com/truecaller/truepay/app/ui/payments/views/b/l$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final l a(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "bookingNumber");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("ref_id_key", paramString);
    localBundle.putBoolean("is_from_history", paramBoolean);
    paramString = new com/truecaller/truepay/app/ui/payments/views/b/l;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  public final int a()
  {
    return R.layout.fragment_redbus_ticket_cancel;
  }
  
  public final void a(RedBusTicket paramRedBusTicket)
  {
    k.b(paramRedBusTicket, "ticketDetails");
    com.truecaller.truepay.app.ui.payments.views.c.f localf = c;
    if (localf != null)
    {
      localf.a(paramRedBusTicket);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "message");
    a(paramString, null);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "bookingAmount");
    k.b(paramString2, "refundableAmount");
    int i = R.id.cancelTicketMaxRefundAmount;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "cancelTicketMaxRefundAmount");
    paramString2 = (CharSequence)paramString2;
    localTextView.setText(paramString2);
    i = R.id.tvPaidAmountUnderTotal;
    localTextView = (TextView)a(i);
    k.a(localTextView, "tvPaidAmountUnderTotal");
    paramString1 = (CharSequence)paramString1;
    localTextView.setText(paramString1);
    i = R.id.tvPaidAmountUnderFare;
    localTextView = (TextView)a(i);
    k.a(localTextView, "tvPaidAmountUnderFare");
    localTextView.setText(paramString1);
    int j = R.id.tvRefundableAmountIUnderTotal;
    paramString1 = (TextView)a(j);
    k.a(paramString1, "tvRefundableAmountIUnderTotal");
    paramString1.setText(paramString2);
    j = R.id.tvRefundableAmountUnderFare;
    paramString1 = (TextView)a(j);
    k.a(paramString1, "tvRefundableAmountUnderFare");
    paramString1.setText(paramString2);
  }
  
  public final void b()
  {
    com.truecaller.truepay.app.ui.payments.views.c.f localf = c;
    if (localf != null)
    {
      localf.b();
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "subTitle");
    int i = R.id.toolbarTicketCancellation;
    Toolbar localToolbar = (Toolbar)a(i);
    k.a(localToolbar, "toolbarTicketCancellation");
    paramString = (CharSequence)paramString;
    localToolbar.setSubtitle(paramString);
  }
  
  public final void c()
  {
    com.truecaller.truepay.app.ui.payments.views.c.f localf = c;
    if (localf != null)
    {
      localf.c();
      return;
    }
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "message");
    com.truecaller.truepay.app.ui.payments.views.c.f localf = c;
    if (localf != null)
    {
      localf.a(paramString);
      return;
    }
  }
  
  public final void d()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      localf.onBackPressed();
      return;
    }
  }
  
  public final w.b e()
  {
    w.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localb;
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.payments.views.c.f;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.payments.views.c.f)getActivity();
      c = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement PaymentsView");
    throw ((Throwable)paramContext);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    a.a locala = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala1 = Truepay.getApplicationComponent();
    locala.a(locala1).a().a(this);
    super.onCreate(paramBundle);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    w.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    if (localb != null) {
      localb.y_();
    }
    c = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = "is_from_history";
      boolean bool1 = paramView.getBoolean(paramBundle);
      localObject = "ref_id_key";
      paramView = paramView.getString((String)localObject);
      if (paramView != null)
      {
        localObject = a;
        if (localObject == null)
        {
          String str = "presenter";
          k.a(str);
        }
        ((w.b)localObject).a(paramView, bool1);
      }
    }
    int j = R.id.tvViewCancellationPolicy;
    paramView = (TextView)a(j);
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/l$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    j = R.id.btnContinueCancellation;
    paramView = (Button)a(j);
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/l$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    j = R.id.toolbarTicketCancellation;
    paramView = (Toolbar)a(j);
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/l$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setNavigationOnClickListener(paramBundle);
    paramView = getActivity();
    if (paramView != null)
    {
      paramView = (AppCompatActivity)paramView;
      int i = R.id.toolbarTicketCancellation;
      paramBundle = (Toolbar)a(i);
      paramView.setSupportActionBar(paramBundle);
      paramBundle = paramView.getSupportActionBar();
      if (paramBundle != null)
      {
        boolean bool2 = true;
        paramBundle.setDisplayHomeAsUpEnabled(bool2);
      }
      paramView = paramView.getSupportActionBar();
      if (paramView != null)
      {
        paramView.setElevation(0.0F);
        return;
      }
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */