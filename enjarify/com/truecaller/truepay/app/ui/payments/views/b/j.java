package com.truecaller.truepay.app.ui.payments.views.b;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.util.DiffUtil;
import android.support.v7.util.DiffUtil.Callback;
import android.support.v7.util.DiffUtil.DiffResult;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;
import c.l.g;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.ui.base.views.fragments.TcPayOnFragmentInteractionListener;
import com.truecaller.truepay.app.ui.dashboard.views.activities.SettingsActivity;
import com.truecaller.truepay.app.ui.history.views.activities.TransactionHistoryActivity;
import com.truecaller.truepay.app.ui.history.views.activities.TruePayWebViewActivity;
import com.truecaller.truepay.app.ui.payments.a.n.b;
import com.truecaller.truepay.app.ui.payments.a.p.a;
import com.truecaller.truepay.app.ui.payments.b.a;
import com.truecaller.truepay.app.ui.payments.b.b;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.c.s;
import com.truecaller.truepay.app.ui.payments.c.s.6;
import com.truecaller.truepay.app.ui.payments.views.a.c;
import com.truecaller.truepay.app.ui.payments.views.activities.PaymentsActivity;
import com.truecaller.truepay.app.ui.rewards.views.activities.RewardsActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.truepay.app.ui.webapps.WebAppActivity;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.app.utils.u;
import com.truecaller.truepay.app.utils.v.a;
import com.truecaller.truepay.data.f.t;
import com.truecaller.truepay.data.provider.e.d;
import io.reactivex.o;
import io.reactivex.q;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class j
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements PopupMenu.OnMenuItemClickListener, com.truecaller.common.ui.b, n.b, p.a, b.b, com.truecaller.truepay.app.ui.payments.views.c.h, v.a
{
  private com.truecaller.truepay.app.ui.payments.a.n A;
  private LinearLayoutManager B;
  private GridLayoutManager C;
  private c D;
  private boolean E;
  private TcPayOnFragmentInteractionListener F;
  private final ContentObserver G;
  LinearLayout a;
  RecyclerView b;
  ImageView c;
  TextView d;
  RecyclerView e;
  LinearLayout f;
  LinearLayout g;
  TextView h;
  TextView i;
  View j;
  TextView k;
  TextView l;
  public com.truecaller.truepay.app.utils.a n;
  public r o;
  public ax p;
  public s q;
  public com.truecaller.truepay.data.e.e r;
  public com.truecaller.truepay.data.e.a s;
  public com.truecaller.truepay.data.e.e t;
  public b.a u;
  public com.truecaller.featuretoggles.e v;
  public u w;
  public com.google.gson.f x;
  private List y;
  private com.truecaller.truepay.app.ui.payments.a.p z;
  
  public j()
  {
    j.1 local1 = new com/truecaller/truepay/app/ui/payments/views/b/j$1;
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    local1.<init>(this, localHandler);
    G = local1;
  }
  
  public static j b()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    j localj = new com/truecaller/truepay/app/ui/payments/views/b/j;
    localj.<init>();
    localj.setArguments(localBundle);
    return localj;
  }
  
  private void c()
  {
    Object localObject1 = (ClipboardManager)getActivity().getSystemService("clipboard");
    String str = k.getText().toString();
    Object localObject2 = ClipData.newPlainText("text", str);
    if (localObject1 != null)
    {
      ((ClipboardManager)localObject1).setPrimaryClip((ClipData)localObject2);
      localObject1 = getActivity();
      int m = R.string.message_copy_upi_id;
      localObject2 = getString(m);
      str = null;
      localObject1 = Toast.makeText((Context)localObject1, (CharSequence)localObject2, 0);
      ((Toast)localObject1).show();
    }
  }
  
  public final int a()
  {
    return R.layout.fragment_payment;
  }
  
  public final void a(com.truecaller.truepay.app.ui.history.models.h paramh)
  {
    Intent localIntent = new android/content/Intent;
    android.support.v4.app.f localf = getActivity();
    localIntent.<init>(localf, TransactionHistoryActivity.class);
    localIntent.putExtra("history_detail", true);
    localIntent.putExtra("history_item", paramh);
    startActivity(localIntent);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    parama = n;
    a.removeAllViews();
    Object localObject1 = getContext();
    Object localObject2 = "layout_inflater";
    localObject1 = (LayoutInflater)((Context)localObject1).getSystemService((String)localObject2);
    parama = parama.iterator();
    for (;;)
    {
      boolean bool = parama.hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (com.truecaller.truepay.app.ui.payments.models.a)parama.next();
      int i1 = R.layout.item_utilities_home;
      View localView = ((LayoutInflater)localObject1).inflate(i1, null);
      int i2 = R.id.tv_utilities_title;
      Object localObject3 = (TextView)localView.findViewById(i2);
      int i3 = R.id.tv_new_billers;
      TextView localTextView = (TextView)localView.findViewById(i3);
      int i4 = R.id.rv_utilities;
      RecyclerView localRecyclerView = (RecyclerView)localView.findViewById(i4);
      Object localObject4 = b;
      ((TextView)localObject3).setText((CharSequence)localObject4);
      localObject3 = new com/truecaller/truepay/app/ui/payments/a/p;
      localObject4 = new java/util/ArrayList;
      Object localObject5 = n;
      ((ArrayList)localObject4).<init>((Collection)localObject5);
      localObject5 = o;
      ((com.truecaller.truepay.app.ui.payments.a.p)localObject3).<init>((ArrayList)localObject4, this, (r)localObject5);
      z = ((com.truecaller.truepay.app.ui.payments.a.p)localObject3);
      localObject3 = new android/support/v7/widget/GridLayoutManager;
      localObject4 = getContext();
      int i5 = 4;
      ((GridLayoutManager)localObject3).<init>((Context)localObject4, i5);
      C = ((GridLayoutManager)localObject3);
      localObject3 = C;
      localRecyclerView.setLayoutManager((RecyclerView.LayoutManager)localObject3);
      localObject3 = z;
      localRecyclerView.setAdapter((RecyclerView.Adapter)localObject3);
      localObject3 = a;
      ((LinearLayout)localObject3).addView(localView);
      bool = t;
      int m;
      if (bool)
      {
        bool = false;
        localObject2 = null;
      }
      else
      {
        m = 8;
      }
      localTextView.setVisibility(m);
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.f paramf)
  {
    paramf = c;
    a(paramf, null);
  }
  
  public final void a(Throwable paramThrowable)
  {
    int m = R.string.recent_Recharge_failure_message;
    String str = getString(m);
    a(str, paramThrowable);
  }
  
  public final void a(List paramList)
  {
    f.setVisibility(8);
    g.setVisibility(0);
    c localc = D;
    Object localObject = new com/truecaller/truepay/app/ui/history/views/a/f;
    List localList = (List)localc.a();
    ((com.truecaller.truepay.app.ui.history.views.a.f)localObject).<init>(localList, paramList);
    localObject = DiffUtil.calculateDiff((DiffUtil.Callback)localObject);
    localc.a(paramList);
    ((DiffUtil.DiffResult)localObject).dispatchUpdatesTo(localc);
    localc.notifyDataSetChanged();
  }
  
  public final void a_(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    Object localObject1 = Truepay.getInstance();
    boolean bool1 = ((Truepay)localObject1).isRegistrationComplete();
    if (!bool1)
    {
      localObject1 = F;
      if (localObject1 != null)
      {
        parama = Truepay.getInstance().getPaymentsFragment();
        ((TcPayOnFragmentInteractionListener)localObject1).replaceFragment(parama);
        return;
      }
    }
    localObject1 = v.aa();
    bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool1)
    {
      localObject1 = "web_app";
      localObject2 = f;
      bool1 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
      if (bool1)
      {
        localObject1 = q;
        if (localObject1 == null) {
          return;
        }
        localObject1 = Truepay.getInstance().getAnalyticLoggerHelper();
        localObject2 = g;
        str = h;
        ((com.truecaller.truepay.data.b.a)localObject1).a((String)localObject2, "payment", str);
        b(parama);
        return;
      }
    }
    localObject1 = "google_play";
    Object localObject2 = g;
    bool1 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
    if (bool1)
    {
      localObject1 = v;
      localObject2 = r;
      localObject3 = com.truecaller.featuretoggles.e.a;
      int i1 = 51;
      localObject3 = localObject3[i1];
      localObject1 = ((e.a)localObject2).a((com.truecaller.featuretoggles.e)localObject1, (g)localObject3);
      bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
      if (!bool1)
      {
        parama = getContext();
        int m = R.string.pay_coming_soon;
        localObject1 = getString(m);
        Toast.makeText(parama, (CharSequence)localObject1, 0).show();
        return;
      }
    }
    boolean bool2 = t;
    E = bool2;
    localObject1 = u;
    localObject2 = g;
    ((b.a)localObject1).a((String)localObject2);
    localObject1 = Truepay.getInstance().getAnalyticLoggerHelper();
    localObject2 = g;
    Object localObject3 = "payment";
    String str = h;
    ((com.truecaller.truepay.data.b.a)localObject1).a((String)localObject2, (String)localObject3, str);
    localObject1 = getContext();
    localObject2 = "utilities";
    parama = PaymentsActivity.a((Context)localObject1, parama, (String)localObject2);
    startActivity(parama);
  }
  
  public final void ae_()
  {
    com.truecaller.truepay.data.e.a locala = s;
    Boolean localBoolean = Boolean.TRUE;
    locala.a(localBoolean);
  }
  
  public final void b(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    parama = WebAppActivity.a(getContext(), parama);
    startActivity(parama);
  }
  
  public final void b(com.truecaller.truepay.app.ui.transaction.b.p paramp) {}
  
  public final void b(String paramString1, String paramString2, Boolean paramBoolean) {}
  
  public final void c(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    u.a(parama);
  }
  
  public final void c(com.truecaller.truepay.app.ui.transaction.b.p paramp) {}
  
  public final void d()
  {
    g.setVisibility(8);
    f.setVisibility(0);
  }
  
  public final void d(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    q.a(parama);
  }
  
  public final void e()
  {
    com.truecaller.truepay.data.e.a locala = s;
    Boolean localBoolean = Boolean.TRUE;
    locala.a(localBoolean);
  }
  
  public final int f()
  {
    return 8;
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int m = 100;
    if (paramInt1 == m)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        TcPayOnFragmentInteractionListener localTcPayOnFragmentInteractionListener = F;
        com.truecaller.truepay.app.ui.base.views.fragments.b localb = Truepay.getInstance().getPaymentsFragment();
        localTcPayOnFragmentInteractionListener.replaceFragment(localb);
      }
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject = getActivity();
    boolean bool = localObject instanceof TcPayOnFragmentInteractionListener;
    if (bool)
    {
      paramContext = (TcPayOnFragmentInteractionListener)getActivity();
      F = paramContext;
      return;
    }
    localObject = new java/lang/IllegalStateException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(" must implement ");
    paramContext = TcPayOnFragmentInteractionListener.class.getSimpleName();
    localStringBuilder.append(paramContext);
    paramContext = localStringBuilder.toString();
    ((IllegalStateException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
    paramBundle = v.H();
    boolean bool = paramBundle.a();
    if (bool)
    {
      paramBundle = w;
      paramBundle.a(this);
    }
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    q.b();
    Object localObject = u;
    if (localObject != null)
    {
      ((b.a)localObject).y_();
      u = null;
    }
    localObject = getContext().getContentResolver();
    ContentObserver localContentObserver = G;
    ((ContentResolver)localObject).unregisterContentObserver(localContentObserver);
    localObject = v.H();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      localObject = w;
      ((u)localObject).a();
    }
    F = null;
  }
  
  public final boolean onMenuItemClick(MenuItem paramMenuItem)
  {
    int m = paramMenuItem.getItemId();
    int i1 = R.id.menu_item_pending_request;
    Object localObject1;
    Object localObject2;
    if (m == i1)
    {
      paramMenuItem = new android/content/Intent;
      localObject1 = getActivity();
      paramMenuItem.<init>((Context)localObject1, TransactionActivity.class);
      localObject1 = "route";
      localObject2 = "route_pending_request";
      paramMenuItem.putExtra((String)localObject1, (String)localObject2);
      startActivity(paramMenuItem);
    }
    else
    {
      i1 = R.id.menu_item_transaction_history;
      if (m == i1)
      {
        paramMenuItem = new android/content/Intent;
        localObject1 = getActivity();
        localObject2 = TransactionHistoryActivity.class;
        paramMenuItem.<init>((Context)localObject1, (Class)localObject2);
        startActivity(paramMenuItem);
      }
      else
      {
        i1 = R.id.menu_item_manage_accounts;
        if (m == i1)
        {
          paramMenuItem = getActivity();
          localObject1 = "";
          localObject2 = null;
          ManageAccountsActivity.a(paramMenuItem, null, null, (String)localObject1);
        }
        else
        {
          i1 = R.id.menu_item_settings;
          if (m == i1)
          {
            paramMenuItem = new android/content/Intent;
            localObject1 = getActivity();
            localObject2 = SettingsActivity.class;
            paramMenuItem.<init>((Context)localObject1, (Class)localObject2);
            i1 = 100;
            startActivityForResult(paramMenuItem, i1);
          }
          else
          {
            i1 = R.id.menu_item_support;
            if (m == i1)
            {
              paramMenuItem = new android/content/Intent;
              localObject1 = getActivity();
              paramMenuItem.<init>((Context)localObject1, TruePayWebViewActivity.class);
              localObject1 = "url";
              localObject2 = r.a();
              paramMenuItem.putExtra((String)localObject1, (String)localObject2);
              startActivity(paramMenuItem);
            }
            else
            {
              i1 = R.id.menu_item_rewards;
              if (m == i1)
              {
                paramMenuItem = RewardsActivity.a(getActivity());
                startActivity(paramMenuItem);
              }
              else
              {
                i1 = R.id.menu_item_add_to_home;
                if (m == i1)
                {
                  paramMenuItem = Truepay.getInstance().getAnalyticLoggerHelper();
                  localObject1 = "payment_tab_menu";
                  paramMenuItem.g((String)localObject1);
                  paramMenuItem = Truepay.getInstance().getListener();
                  i1 = 3;
                  paramMenuItem.createShortcut(i1);
                }
              }
            }
          }
        }
      }
    }
    return false;
  }
  
  public final void onPause()
  {
    super.onPause();
    Object localObject = v.H();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      localObject = w;
      ((u)localObject).a();
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    boolean bool = E;
    if (bool)
    {
      localObject1 = q;
      ((s)localObject1).a();
      E = false;
    }
    Object localObject1 = getContext().getContentResolver();
    Object localObject2 = G;
    ((ContentResolver)localObject1).unregisterContentObserver((ContentObserver)localObject2);
    localObject1 = getContext().getContentResolver();
    localObject2 = d.a;
    ContentObserver localContentObserver = G;
    ((ContentResolver)localObject1).registerContentObserver((Uri)localObject2, false, localContentObserver);
    q.e();
    localObject1 = s.a();
    bool = ((Boolean)localObject1).booleanValue();
    if (bool)
    {
      localObject1 = q;
      ((s)localObject1).f();
    }
    localObject1 = v.H();
    bool = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool)
    {
      localObject1 = w;
      ((u)localObject1).a(this);
    }
  }
  
  public final void onRewardReceived(Intent paramIntent)
  {
    startActivity(paramIntent);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    int m = R.id.layout_utilities_list;
    paramBundle = (LinearLayout)paramView.findViewById(m);
    a = paramBundle;
    m = R.id.rv_quick_payments_list;
    paramBundle = (RecyclerView)paramView.findViewById(m);
    b = paramBundle;
    m = R.id.img_more;
    paramBundle = (ImageView)paramView.findViewById(m);
    c = paramBundle;
    m = R.id.header_title;
    paramBundle = (TextView)paramView.findViewById(m);
    d = paramBundle;
    m = R.id.rv_recent_recharges_frag_payment;
    paramBundle = (RecyclerView)paramView.findViewById(m);
    e = paramBundle;
    m = R.id.ll_recent_empty;
    paramBundle = (LinearLayout)paramView.findViewById(m);
    f = paramBundle;
    m = R.id.ll_recent_recharges_header_frag_payment;
    paramBundle = (LinearLayout)paramView.findViewById(m);
    g = paramBundle;
    m = R.id.tv_recent_recharges_title_frag_payment;
    paramBundle = (TextView)paramView.findViewById(m);
    h = paramBundle;
    m = R.id.tv_view_all_frag_payments;
    paramBundle = (TextView)paramView.findViewById(m);
    i = paramBundle;
    m = R.id.include_home_footer;
    paramBundle = paramView.findViewById(m);
    j = paramBundle;
    m = R.id.tv_upi_id;
    paramBundle = (TextView)paramView.findViewById(m);
    k = paramBundle;
    m = R.id.tv_upi_id_logo;
    paramBundle = (TextView)paramView.findViewById(m);
    l = paramBundle;
    paramBundle = c;
    Object localObject1 = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$j$u1RDAMp8vNzshUj4CZyZsDTTndw;
    ((-..Lambda.j.u1RDAMp8vNzshUj4CZyZsDTTndw)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    m = R.id.img_menu;
    paramView = paramView.findViewById(m);
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$j$Cm9w8nsOHGknN6OALPE2popdk6w;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = i;
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$j$KCz8tG6FSyQr-5fG50XCLNwUyBo;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = k;
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$j$tbKCFSLd0DjCK74J5_paV9ShCxk;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = l;
    paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$j$-LhDgpYxRzBz6PylWJbEUNcPqoA;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    q.a(this);
    u.a(this);
    paramView = new java/util/ArrayList;
    paramView.<init>();
    y = paramView;
    paramView = y;
    int i1 = paramView.size();
    m = 8;
    if (i1 == 0)
    {
      paramView = d;
      paramView.setVisibility(m);
    }
    paramView = new com/truecaller/truepay/app/ui/payments/a/n;
    localObject1 = y;
    paramView.<init>((List)localObject1, this);
    A = paramView;
    paramView = b;
    localObject1 = A;
    paramView.setAdapter((RecyclerView.Adapter)localObject1);
    paramView = new android/support/v7/widget/LinearLayoutManager;
    localObject1 = getContext();
    paramView.<init>((Context)localObject1);
    B = paramView;
    paramView = b;
    localObject1 = B;
    paramView.setLayoutManager((RecyclerView.LayoutManager)localObject1);
    paramView = new com/truecaller/truepay/app/ui/payments/views/a/c;
    localObject1 = x;
    paramView.<init>(this, (com.google.gson.f)localObject1);
    D = paramView;
    paramView = e;
    localObject1 = new android/support/v7/widget/LinearLayoutManager;
    Object localObject2 = getActivity();
    ((LinearLayoutManager)localObject1).<init>((Context)localObject2);
    paramView.setLayoutManager((RecyclerView.LayoutManager)localObject1);
    paramView = e;
    localObject1 = D;
    paramView.setAdapter((RecyclerView.Adapter)localObject1);
    paramView = new android/support/v7/widget/DividerItemDecoration;
    localObject1 = getActivity();
    int i2 = 1;
    paramView.<init>((Context)localObject1, i2);
    localObject1 = getActivity();
    int i3 = R.drawable.divider_history;
    localObject1 = android.support.v4.content.b.a((Context)localObject1, i3);
    paramView.setDrawable((Drawable)localObject1);
    e.addItemDecoration(paramView);
    q.a();
    paramView = q;
    localObject1 = Truepay.getInstance();
    boolean bool2 = ((Truepay)localObject1).isRegistrationComplete();
    i3 = 0;
    if (bool2)
    {
      localObject1 = f.a();
      long l1 = ((Long)localObject1).longValue();
      long l2 = System.currentTimeMillis() - l1;
      l1 = 21600000L;
      bool2 = l2 < l1;
      if (bool2)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        localObject1 = null;
      }
      if (!bool2)
      {
        localObject1 = k.a.a();
        Object localObject3 = io.reactivex.g.a.b();
        localObject1 = ((o)localObject1).b((io.reactivex.n)localObject3);
        localObject3 = io.reactivex.android.b.a.a();
        localObject1 = ((o)localObject1).a((io.reactivex.n)localObject3);
        localObject3 = new com/truecaller/truepay/app/ui/payments/c/s$6;
        ((s.6)localObject3).<init>(paramView);
        ((o)localObject1).a((q)localObject3);
      }
    }
    paramView = j;
    int i4 = R.id.psp_logo;
    paramView = (ImageView)paramView.findViewById(i4);
    localObject1 = v.q();
    boolean bool3 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool3)
    {
      localObject1 = t.a();
      if (localObject1 == null) {
        localObject1 = "icici";
      }
      int i5 = ((String)localObject1).hashCode();
      int i6 = -1396222919;
      if (i5 != i6)
      {
        i2 = 100023093;
        if (i5 == i2)
        {
          localObject2 = "icici";
          bool3 = ((String)localObject1).equals(localObject2);
          if (bool3)
          {
            i2 = 0;
            localObject2 = null;
            break label875;
          }
        }
      }
      else
      {
        String str = "baroda";
        bool3 = ((String)localObject1).equals(str);
        if (bool3) {
          break label875;
        }
      }
      i2 = -1;
      switch (i2)
      {
      default: 
        break;
      case 1: 
        localObject1 = getResources();
        i2 = R.drawable.bob_logo;
        localObject1 = ((Resources)localObject1).getDrawable(i2);
        paramView.setImageDrawable((Drawable)localObject1);
        break;
      case 0: 
        label875:
        localObject1 = getResources();
        i2 = R.drawable.icici_logo;
        localObject1 = ((Resources)localObject1).getDrawable(i2);
        paramView.setImageDrawable((Drawable)localObject1);
      }
    }
    paramView = n.c();
    if (paramView != null)
    {
      l.setVisibility(0);
      k.setVisibility(0);
      localObject1 = k;
      paramView = h;
      ((TextView)localObject1).setText(paramView);
    }
    paramView = Truepay.getInstance();
    boolean bool1 = paramView.isRegistrationComplete();
    if (!bool1)
    {
      paramView = c;
      paramView.setVisibility(m);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */