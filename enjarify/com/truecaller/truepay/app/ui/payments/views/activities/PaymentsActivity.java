package com.truecaller.truepay.app.ui.payments.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.j.a;
import android.text.TextUtils;
import android.view.Window;
import android.widget.FrameLayout;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.ui.history.views.activities.TruePayWebViewActivity;
import com.truecaller.truepay.app.ui.payments.b.b;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.c.s;
import com.truecaller.truepay.app.ui.payments.models.RedBusTicket;
import com.truecaller.truepay.app.ui.payments.views.b.g;
import com.truecaller.truepay.app.ui.payments.views.b.h;
import com.truecaller.truepay.app.ui.payments.views.b.m;
import com.truecaller.truepay.app.ui.payments.views.b.n;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.ui.webapps.WebAppActivity;
import com.truecaller.truepay.app.ui.webapps.ixigo.IxigoPaymentRequest;
import com.truecaller.truepay.app.utils.ax;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import rb.wl.android.model.BookingFlowResponseSDK;
import rb.wl.android.model.CancellationFlowResponseSDK;
import rb.wl.android.sdk.UserInfo;
import rb.wl.android.sdk.WSDKL;

public class PaymentsActivity
  extends com.truecaller.truepay.app.ui.base.views.a.b
  implements b.b, com.truecaller.truepay.app.ui.payments.views.c.f
{
  FrameLayout a;
  public s b;
  public ax c;
  private PaymentsActivity.a d;
  private boolean e;
  private String f;
  private String g;
  private String h;
  private ArrayList i;
  private com.truecaller.truepay.app.ui.payments.models.a j;
  private String k;
  private String l;
  private String m;
  private Boolean n;
  private String o = "utilities";
  private HashMap p;
  
  public PaymentsActivity()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    p = localHashMap;
  }
  
  public static Intent a(Context paramContext, com.truecaller.truepay.app.ui.payments.models.a parama, IxigoPaymentRequest paramIxigoPaymentRequest, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, PaymentsActivity.class);
    paramContext = new android/os/Bundle;
    paramContext.<init>();
    paramContext.putSerializable("utility_entry", parama);
    Boolean localBoolean = Boolean.FALSE;
    paramContext.putSerializable("is_from_history", localBoolean);
    paramContext.putParcelable("payment_request", paramIxigoPaymentRequest);
    paramContext.putString("recharge_context_key", paramString);
    localIntent.putExtras(paramContext);
    return localIntent;
  }
  
  public static Intent a(Context paramContext, com.truecaller.truepay.app.ui.payments.models.a parama, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, PaymentsActivity.class);
    paramContext = new android/os/Bundle;
    paramContext.<init>();
    paramContext.putSerializable("utility_entry", parama);
    Boolean localBoolean = Boolean.FALSE;
    paramContext.putSerializable("is_from_history", localBoolean);
    paramContext.putString("recharge_context_key", paramString);
    localIntent.putExtras(paramContext);
    return localIntent;
  }
  
  public static Intent a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean, String paramString5)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, PaymentsActivity.class);
    paramContext = new android/os/Bundle;
    paramContext.<init>();
    paramContext.putSerializable("utility_type", paramString1);
    paramContext.putSerializable("utility_vendor_type", paramString2);
    paramContext.putSerializable("redbus_ref_id", paramString3);
    paramContext.putSerializable("redbus_booking_number", paramString4);
    paramString2 = Boolean.TRUE;
    paramContext.putSerializable("is_from_history", paramString2);
    paramContext.putString("recharge_context_key", paramString5);
    boolean bool = paramBoolean.booleanValue();
    paramContext.putBoolean("show_cancel_button", bool);
    localIntent.putExtras(paramContext);
    return localIntent;
  }
  
  public static Intent a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, PaymentsActivity.class);
    paramContext = new android/os/Bundle;
    paramContext.<init>();
    paramContext.putSerializable("utility_type", paramString1);
    paramContext.putSerializable("operator_symbol", paramString2);
    paramContext.putSerializable("location_symbol", paramString3);
    paramContext.putSerializable("recharge_params", paramString4);
    paramString2 = Boolean.TRUE;
    paramContext.putSerializable("is_from_history", paramString2);
    paramContext.putString("recharge_context_key", paramString5);
    localIntent.putExtras(paramContext);
    return localIntent;
  }
  
  private static HashMap a(String[] paramArrayOfString)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i1 = paramArrayOfString.length;
    int i2 = 0;
    while (i2 < i1)
    {
      String str1 = paramArrayOfString[i2];
      String str2 = d(str1);
      str1 = c(str1);
      localHashMap.put(str2, str1);
      i2 += 1;
    }
    return localHashMap;
  }
  
  private void a(Intent paramIntent)
  {
    Object localObject1 = paramIntent.getExtras();
    Object localObject2 = ((Bundle)localObject1).getString("utility_type");
    if (localObject2 != null)
    {
      localObject2 = ((Bundle)localObject1).getString("utility_type");
      k = ((String)localObject2);
      localObject2 = ((Bundle)localObject1).getString("utility_vendor_type");
      g = ((String)localObject2);
      localObject2 = "booking";
      String str = k;
      boolean bool = ((String)localObject2).equalsIgnoreCase(str);
      if (bool)
      {
        localObject2 = "redbus";
        str = g;
        bool = ((String)localObject2).equalsIgnoreCase(str);
        if (bool)
        {
          localObject2 = ((Bundle)localObject1).getString("redbus_booking_number");
          h = ((String)localObject2);
          localObject2 = ((Bundle)localObject1).getString("redbus_ref_id");
          f = ((String)localObject2);
          localObject2 = h;
          bool = TextUtils.isEmpty((CharSequence)localObject2);
          if (!bool)
          {
            localObject2 = f;
            bool = TextUtils.isEmpty((CharSequence)localObject2);
            if (!bool)
            {
              paramIntent = h;
              localObject1 = f;
              localObject2 = Boolean.FALSE;
              a(paramIntent, (String)localObject1, (Boolean)localObject2);
              return;
            }
          }
        }
      }
      localObject2 = ((Bundle)localObject1).getString("operator_symbol");
      if (localObject2 != null)
      {
        localObject2 = ((Bundle)localObject1).getString("operator_symbol");
        l = ((String)localObject2);
      }
      localObject2 = ((Bundle)localObject1).getString("location_symbol");
      if (localObject2 != null)
      {
        localObject2 = ((Bundle)localObject1).getString("location_symbol");
        m = ((String)localObject2);
      }
      localObject2 = "recharge_params";
      str = null;
      localObject1 = ((Bundle)localObject1).getString((String)localObject2, null);
      bool = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool)
      {
        paramIntent = ax.g((String)localObject1);
        p = paramIntent;
      }
      else
      {
        localObject1 = paramIntent.getData();
        if (localObject1 != null)
        {
          paramIntent = paramIntent.getData().getEncodedQuery();
          if (paramIntent != null)
          {
            localObject1 = "\\&";
            paramIntent = a(paramIntent.split((String)localObject1));
            p = paramIntent;
          }
        }
      }
      paramIntent = b;
      paramIntent.a();
    }
  }
  
  private void a(Fragment paramFragment, boolean paramBoolean1, boolean paramBoolean2)
  {
    try
    {
      Object localObject1 = getSupportFragmentManager();
      localObject1 = ((j)localObject1).a();
      if (paramBoolean1)
      {
        Object localObject2 = paramFragment.getClass();
        localObject2 = ((Class)localObject2).getName();
        ((android.support.v4.app.o)localObject1).a((String)localObject2);
      }
      if (paramBoolean2)
      {
        paramBoolean1 = R.id.payment_container;
        localObject3 = paramFragment.getClass();
        localObject3 = ((Class)localObject3).getSimpleName();
        paramFragment = ((android.support.v4.app.o)localObject1).b(paramBoolean1, paramFragment, (String)localObject3);
        paramFragment.d();
        return;
      }
      paramBoolean1 = R.id.payment_container;
      Object localObject3 = paramFragment.getClass();
      localObject3 = ((Class)localObject3).getSimpleName();
      paramFragment = ((android.support.v4.app.o)localObject1).a(paramBoolean1, paramFragment, (String)localObject3);
      paramFragment.d();
      return;
    }
    catch (Exception localException) {}
  }
  
  private static String c(String paramString)
  {
    String str = "\\=";
    try
    {
      paramString = paramString.split(str);
      int i1 = 1;
      return paramString[i1];
    }
    catch (Exception localException) {}
    return "";
  }
  
  private static String d(String paramString)
  {
    String str = "\\=";
    try
    {
      paramString = paramString.split(str);
      str = null;
      return paramString[0];
    }
    catch (Exception localException) {}
    return "";
  }
  
  private static com.truecaller.truepay.app.ui.payments.models.a e(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    com.truecaller.truepay.app.ui.payments.models.a locala = new com/truecaller/truepay/app/ui/payments/models/a;
    locala.<init>();
    String str = b;
    b = str;
    str = c;
    c = str;
    str = a;
    d = str;
    parama = l;
    k = parama;
    return locala;
  }
  
  private static UserInfo g()
  {
    UserInfo localUserInfo = new rb/wl/android/sdk/UserInfo;
    localUserInfo.<init>();
    localUserInfo.setSdkKey("iCPW1Q2sAVWvRToeL2BhuNg1KjJ7Pc");
    localUserInfo.setSdkSecret("oQmffZxr7JvbqE3X35GNVtCLCadlLc");
    return localUserInfo;
  }
  
  private void h()
  {
    Object localObject = WSDKL.getInstance();
    UserInfo localUserInfo = g();
    ((WSDKL)localObject).init(this, localUserInfo);
    localObject = WSDKL.getInstance().startBookingFlow();
    if (localObject != null)
    {
      int i1 = 102;
      startActivityForResult((Intent)localObject, i1);
    }
  }
  
  private String i()
  {
    j localj = getSupportFragmentManager();
    int i1 = localj.e();
    if (i1 > 0)
    {
      localj = getSupportFragmentManager();
      int i2 = getSupportFragmentManager().e() + -1;
      return localj.c(i2).h();
    }
    return "";
  }
  
  public final void a()
  {
    onBackPressed();
  }
  
  public final void a(int paramInt)
  {
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 21;
    if (i1 >= i2)
    {
      Window localWindow = getWindow();
      localWindow.setStatusBarColor(paramInt);
    }
  }
  
  public final void a(RedBusTicket paramRedBusTicket)
  {
    paramRedBusTicket = m.a(paramRedBusTicket);
    android.support.v4.app.o localo = getSupportFragmentManager().a();
    String str = m.class.getSimpleName();
    localo.a(paramRedBusTicket, str).d();
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    i = ((ArrayList)localObject1);
    parama = n.iterator();
    boolean bool1;
    Object localObject2;
    for (;;)
    {
      bool1 = parama.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (com.truecaller.truepay.app.ui.payments.models.a)parama.next();
      localObject2 = i;
      localObject1 = n;
      ((ArrayList)localObject2).addAll((Collection)localObject1);
    }
    boolean bool2 = false;
    parama = null;
    try
    {
      localObject1 = getSupportFragmentManager();
      Object localObject3;
      Object localObject4;
      if (localObject1 != null)
      {
        int i1 = ((j)localObject1).e();
        if (i1 > 0)
        {
          int i2 = 0;
          localObject3 = null;
          while (i2 < i1)
          {
            ((j)localObject1).d();
            i2 += 1;
          }
        }
        localObject2 = ((j)localObject1).f();
        localObject2 = ((List)localObject2).iterator();
        for (;;)
        {
          boolean bool4 = ((Iterator)localObject2).hasNext();
          if (!bool4) {
            break;
          }
          localObject3 = ((Iterator)localObject2).next();
          localObject3 = (Fragment)localObject3;
          localObject4 = ((j)localObject1).a();
          localObject3 = ((android.support.v4.app.o)localObject4).a((Fragment)localObject3);
          ((android.support.v4.app.o)localObject3).c();
        }
      }
      boolean bool3;
      Object localObject5;
      boolean bool5;
      label538:
      HashMap localHashMap1;
      String str1;
      String str2;
      HashMap localHashMap2;
      String str3;
      return;
    }
    catch (Exception localException)
    {
      localObject1 = k;
      if (localObject1 != null)
      {
        localObject1 = i.iterator();
        do
        {
          do
          {
            bool3 = ((Iterator)localObject1).hasNext();
            if (!bool3) {
              break;
            }
            localObject2 = ((Iterator)localObject1).next();
            localObject5 = localObject2;
            localObject5 = (com.truecaller.truepay.app.ui.payments.models.a)localObject2;
            localObject2 = k;
            localObject3 = g;
            bool3 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
          } while (!bool3);
          j = ((com.truecaller.truepay.app.ui.payments.models.a)localObject5);
          localObject2 = "redbus";
          localObject3 = l;
          bool3 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
          if (bool3)
          {
            h();
            return;
          }
          localObject2 = "ixigo";
          localObject3 = l;
          bool3 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
          if (!bool3) {
            break;
          }
          localObject2 = "web_app";
          localObject3 = j.f;
          bool3 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
        } while (!bool3);
        parama = b;
        localObject1 = a.aa();
        bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
        if (bool1)
        {
          localObject1 = q;
          if (localObject1 != null)
          {
            localObject1 = d;
            if (localObject1 != null)
            {
              localObject1 = Truepay.getInstance().getAnalyticLoggerHelper();
              localObject2 = g;
              localObject3 = "deeplink";
              localObject4 = h;
              ((com.truecaller.truepay.data.b.a)localObject1).a((String)localObject2, (String)localObject3, (String)localObject4);
              parama = (com.truecaller.truepay.app.ui.payments.views.c.e)d;
              parama.b((com.truecaller.truepay.app.ui.payments.models.a)localObject5);
            }
          }
        }
        return;
        bool1 = false;
        localObject1 = null;
        bool3 = e;
        bool5 = true;
        if (!bool3)
        {
          parama = "ola";
          localObject2 = j.a;
          bool2 = parama.equalsIgnoreCase((String)localObject2);
          if (!bool2)
          {
            parama = "google_play";
            localObject2 = j.a;
            bool2 = parama.equalsIgnoreCase((String)localObject2);
            if (!bool2)
            {
              localObject4 = null;
              break label538;
            }
          }
          parama = e(j);
          localObject4 = parama;
          localHashMap1 = p;
          str1 = l;
          parama = m;
          str2 = o;
          localObject3 = localObject5;
          localObject5 = parama;
          parama = com.truecaller.truepay.app.ui.payments.views.b.i.a((com.truecaller.truepay.app.ui.payments.models.a)localObject3, (com.truecaller.truepay.app.ui.payments.models.a)localObject4, localHashMap1, str1, parama, str2);
          a(parama, bool5, bool5);
          return;
        }
        str2 = l;
        localHashMap2 = p;
        str3 = o;
        localObject1 = com.truecaller.truepay.app.ui.payments.views.b.f.a(null, null, false, true, (com.truecaller.truepay.app.ui.payments.models.a)localObject5, str2, localHashMap2, str3);
        a((Fragment)localObject1, bool5, false);
        return;
        finish();
      }
    }
  }
  
  public final void a(PaymentsActivity.a parama)
  {
    d = parama;
    parama = new android/content/Intent;
    Uri localUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
    parama.<init>("android.intent.action.PICK", localUri);
    startActivityForResult(parama, 90);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.e parame)
  {
    a(parame, true, false);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.f paramf)
  {
    a(paramf, true, false);
  }
  
  public final void a(h paramh)
  {
    a(paramh, true, false);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.i parami, Boolean paramBoolean)
  {
    boolean bool = paramBoolean.booleanValue();
    a(parami, bool, true);
  }
  
  public final void a(p paramp)
  {
    s locals = b;
    String str1 = j.b;
    Object localObject = "success";
    boolean bool1 = ((String)localObject).equalsIgnoreCase(str1);
    if (!bool1)
    {
      localObject = "pending";
      bool2 = ((String)localObject).equalsIgnoreCase(str1);
      if (!bool2)
      {
        bool2 = false;
        str1 = null;
        break label63;
      }
    }
    boolean bool2 = true;
    label63:
    localObject = a.aa();
    bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool1)
    {
      localObject = "ixigo";
      String str2 = B;
      bool1 = ((String)localObject).equalsIgnoreCase(str2);
      if ((bool1) && (bool2))
      {
        ((com.truecaller.truepay.app.ui.payments.views.c.e)d).c(paramp);
        return;
      }
    }
    ((com.truecaller.truepay.app.ui.payments.views.c.e)d).b(paramp);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.views.a.i parami)
  {
    a(parami, true, false);
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama, String paramString)
  {
    ManageAccountsActivity.a(this, "action.page.reset_upi_pin", parama, paramString);
    finish();
  }
  
  public final void a(String paramString)
  {
    paramString = n.b(paramString);
    android.support.v4.app.o localo = getSupportFragmentManager().a();
    String str = n.class.getSimpleName();
    localo.a(paramString, str).d();
  }
  
  public final void a(String paramString1, String paramString2, Boolean paramBoolean)
  {
    Object localObject1 = getIntent();
    Object localObject2 = "show_cancel_button";
    boolean bool1 = ((Intent)localObject1).hasExtra((String)localObject2);
    if (bool1)
    {
      boolean bool2 = getIntent().getBooleanExtra("show_cancel_button", false);
      paramBoolean = Boolean.valueOf(bool2);
      localObject1 = getIntent();
      localObject2 = "show_cancel_button";
      ((Intent)localObject1).removeExtra((String)localObject2);
    }
    localObject1 = b;
    localObject2 = d;
    if (localObject2 != null)
    {
      localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.e)d;
      ((com.truecaller.truepay.app.ui.payments.views.c.e)localObject1).b(paramString1, paramString2, paramBoolean);
    }
  }
  
  public final void a(Throwable paramThrowable) {}
  
  public final void a(List paramList) {}
  
  public final void ae_() {}
  
  public final void b()
  {
    boolean bool = isFinishing();
    if (!bool)
    {
      bool = true;
      e = bool;
      a.setVisibility(0);
      a.setClickable(bool);
      Object localObject = getSupportFragmentManager();
      com.truecaller.truepay.app.ui.registration.views.a locala = new com/truecaller/truepay/app/ui/registration/views/a;
      locala.<init>();
      String str = "";
      a = str;
      localObject = ((j)localObject).a();
      int i1 = R.id.overlay_progress_frame;
      localObject = ((android.support.v4.app.o)localObject).b(i1, locala);
      ((android.support.v4.app.o)localObject).d();
    }
  }
  
  public final void b(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    parama = WebAppActivity.a(this, parama);
    startActivity(parama);
    finish();
  }
  
  public final void b(p paramp)
  {
    String str = o;
    paramp = com.truecaller.truepay.app.ui.payments.views.b.o.a(paramp, str);
    a(paramp, true, false);
  }
  
  public final void b(com.truecaller.truepay.data.api.model.a parama, String paramString)
  {
    ManageAccountsActivity.a(this, "action.page.forgot_upi_pin", parama, paramString);
  }
  
  public final void b(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(this, TruePayWebViewActivity.class);
    localIntent.putExtra("url", paramString);
    startActivity(localIntent);
    finish();
  }
  
  public final void b(String paramString1, String paramString2, Boolean paramBoolean)
  {
    Object localObject = getResources();
    int i1 = R.color.colorPrimaryDark;
    int i2 = ((Resources)localObject).getColor(i1);
    a(i2);
    f = paramString2;
    paramString2 = WSDKL.getInstance();
    localObject = g();
    paramString2.init(this, (UserInfo)localObject);
    paramString2 = WSDKL.getInstance();
    boolean bool = paramBoolean.booleanValue();
    paramString1 = paramString2.viewCancelTicket(paramString1, bool);
    if (paramString1 != null)
    {
      int i3 = 103;
      startActivityForResult(paramString1, i3);
    }
  }
  
  public final void c()
  {
    boolean bool = isFinishing();
    if (!bool)
    {
      bool = false;
      e = false;
      FrameLayout localFrameLayout = a;
      int i1 = 8;
      localFrameLayout.setVisibility(i1);
    }
  }
  
  public final void c(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    d(parama);
  }
  
  public final void c(p paramp)
  {
    Object localObject = getIntent().getExtras();
    String str = "payment_request";
    localObject = (IxigoPaymentRequest)((Bundle)localObject).getParcelable(str);
    if (localObject != null)
    {
      paramp = WebAppActivity.a(paramp, (IxigoPaymentRequest)localObject);
      setResult(-1, paramp);
      finish();
      return;
    }
    paramp = WebAppActivity.a(paramp);
    setResult(1, paramp);
    finish();
  }
  
  public final void d() {}
  
  public final void d(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    b.a(parama);
  }
  
  public final void d(p paramp)
  {
    String str = o;
    paramp = com.truecaller.truepay.app.ui.payments.views.b.a.a(paramp, str);
    a(paramp, true, false);
  }
  
  public final void e() {}
  
  public int getLayoutId()
  {
    return R.layout.activity_payment;
  }
  
  public void initDagger(com.truecaller.truepay.app.a.a.a parama)
  {
    parama = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    parama.a(locala).a().a(this);
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i1 = -1;
    if (paramInt2 == i1)
    {
      i1 = 90;
      PaymentsActivity.a locala;
      if (paramInt1 != i1)
      {
        i1 = 0;
        locala = null;
        boolean bool1 = true;
        Object localObject1;
        Object localObject2;
        switch (paramInt1)
        {
        default: 
          break;
        case 103: 
          if (paramIntent == null) {
            break;
          }
          localObject1 = (CancellationFlowResponseSDK)paramIntent.getParcelableExtra("cancel_code");
          if (localObject1 == null) {
            break;
          }
          localObject1 = f;
          if (localObject1 == null) {
            break;
          }
          localObject2 = n;
          boolean bool2 = ((Boolean)localObject2).booleanValue();
          localObject1 = com.truecaller.truepay.app.ui.payments.views.b.l.a((String)localObject1, bool2);
          a((Fragment)localObject1, bool1, false);
          break;
        case 102: 
          if (paramIntent == null) {
            break;
          }
          localObject1 = (BookingFlowResponseSDK)paramIntent.getParcelableExtra("bk_code");
          if (localObject1 == null) {
            break;
          }
          localObject2 = new com/truecaller/truepay/app/ui/transaction/b/p;
          ((p)localObject2).<init>();
          String str = ((BookingFlowResponseSDK)localObject1).getBlockKey();
          A = str;
          str = j.c;
          t = str;
          str = j.l;
          w = str;
          str = j.g;
          v = str;
          str = j.h;
          B = str;
          localObject1 = ((BookingFlowResponseSDK)localObject1).getTicketFare();
          e = ((String)localObject1);
          localObject1 = j.p;
          F = ((String)localObject1);
          localObject1 = o;
          localObject1 = g.a((p)localObject2, (String)localObject1);
          a((Fragment)localObject1, bool1, false);
          break;
        }
      }
      else
      {
        locala = d;
        if (locala != null)
        {
          Uri localUri = paramIntent.getData();
          locala.onCompleted(localUri);
        }
      }
    }
    else if (paramInt2 == 0)
    {
      finish();
    }
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public void onBackPressed()
  {
    boolean bool1 = e;
    if (bool1) {
      return;
    }
    Object localObject = getSupportFragmentManager();
    int i1 = ((j)localObject).e();
    int i3 = 1;
    if (i1 == i3)
    {
      finish();
      return;
    }
    localObject = com.truecaller.truepay.app.ui.payments.views.b.a.class.getName();
    String str = i();
    boolean bool2 = ((String)localObject).equalsIgnoreCase(str);
    if (!bool2)
    {
      localObject = com.truecaller.truepay.app.ui.payments.views.b.o.class.getName();
      str = i();
      bool2 = ((String)localObject).equalsIgnoreCase(str);
      if (!bool2)
      {
        localObject = com.truecaller.truepay.app.ui.payments.views.b.l.class.getName();
        str = i();
        bool2 = ((String)localObject).equalsIgnoreCase(str);
        if (!bool2)
        {
          super.onBackPressed();
          return;
        }
      }
    }
    bool2 = com.truecaller.truepay.app.ui.payments.a.a;
    if (bool2)
    {
      localObject = getResources();
      i3 = R.color.colorPrimaryDark;
      int i2 = ((Resources)localObject).getColor(i3);
      a(i2);
      com.truecaller.truepay.app.ui.payments.a.a = false;
      super.onBackPressed();
      return;
    }
    finish();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    boolean bool1 = isUserOnboarded();
    if (!bool1)
    {
      Truepay.getInstance().openPaymentsTab(this);
      finish();
      return;
    }
    int i1 = R.id.overlay_progress_frame;
    paramBundle = (FrameLayout)findViewById(i1);
    a = paramBundle;
    b.a(this);
    paramBundle = getIntent().getExtras();
    if (paramBundle != null)
    {
      Object localObject1 = paramBundle.getString("recharge_context_key", "deeplink");
      o = ((String)localObject1);
      boolean bool3 = paramBundle.getBoolean("is_from_history", false);
      localObject1 = Boolean.valueOf(bool3);
      n = ((Boolean)localObject1);
      localObject1 = paramBundle.getSerializable("utility_entry");
      if (localObject1 != null)
      {
        localObject1 = (com.truecaller.truepay.app.ui.payments.models.a)paramBundle.getSerializable("utility_entry");
        j = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1);
        localObject1 = "booking";
        String str1 = j.g;
        bool3 = ((String)localObject1).equalsIgnoreCase(str1);
        if (bool3)
        {
          localObject1 = "redbus";
          str1 = j.h;
          bool3 = ((String)localObject1).equalsIgnoreCase(str1);
          if (bool3)
          {
            h();
            return;
          }
        }
        localObject1 = "booking";
        str1 = j.g;
        bool3 = ((String)localObject1).equalsIgnoreCase(str1);
        boolean bool4 = true;
        if (bool3)
        {
          localObject1 = "ixigo";
          localObject2 = j.h;
          bool3 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
          if (bool3)
          {
            paramBundle = (IxigoPaymentRequest)paramBundle.getParcelable("payment_request");
            localObject1 = new com/truecaller/truepay/app/ui/transaction/b/p;
            ((p)localObject1).<init>();
            localObject2 = j.a;
            t = ((String)localObject2);
            localObject2 = j.g;
            v = ((String)localObject2);
            localObject2 = j.l;
            w = ((String)localObject2);
            localObject2 = paramBundle.getOrderId();
            D = ((String)localObject2);
            localObject2 = j.h;
            B = ((String)localObject2);
            localObject2 = String.valueOf(paramBundle.getProductType());
            E = ((String)localObject2);
            paramBundle = String.valueOf(paramBundle.getPayableAmount());
            e = paramBundle;
            paramBundle = j.p;
            F = paramBundle;
            paramBundle = o;
            paramBundle = g.a((p)localObject1, paramBundle);
            a(paramBundle, bool4, false);
            return;
          }
        }
        paramBundle = "ola";
        localObject1 = j.a;
        boolean bool2 = paramBundle.equalsIgnoreCase((String)localObject1);
        if (!bool2)
        {
          paramBundle = "google_play";
          localObject1 = j.a;
          bool2 = paramBundle.equalsIgnoreCase((String)localObject1);
          if (!bool2)
          {
            paramBundle = j;
            bool2 = e;
            if (!bool2)
            {
              localObject2 = j;
              str2 = l;
              localObject3 = m;
              str3 = o;
              paramBundle = com.truecaller.truepay.app.ui.payments.views.b.i.a((com.truecaller.truepay.app.ui.payments.models.a)localObject2, null, null, str2, (String)localObject3, str3);
              a(paramBundle, bool4, bool4);
              return;
            }
            localObject3 = j;
            str3 = l;
            HashMap localHashMap = p;
            String str4 = o;
            paramBundle = com.truecaller.truepay.app.ui.payments.views.b.f.a(null, null, false, true, (com.truecaller.truepay.app.ui.payments.models.a)localObject3, str3, localHashMap, str4);
            a(paramBundle, bool4, bool4);
            return;
          }
        }
        com.truecaller.truepay.app.ui.payments.models.a locala = e(j);
        Object localObject2 = j;
        String str2 = a;
        Object localObject3 = m;
        String str3 = o;
        paramBundle = com.truecaller.truepay.app.ui.payments.views.b.i.a((com.truecaller.truepay.app.ui.payments.models.a)localObject2, locala, null, str2, (String)localObject3, str3);
        a(paramBundle, bool4, bool4);
        return;
      }
      paramBundle = getIntent();
      a(paramBundle);
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    b.b();
  }
  
  public void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    a(paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.activities.PaymentsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */